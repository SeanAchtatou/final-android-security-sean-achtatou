package com.avast.android.generic.internet.c.a;

import android.support.v4.app.NotificationCompat;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import java.io.InputStream;

/* compiled from: MyAvastPairing */
public final class i extends GeneratedMessageLite implements k {

    /* renamed from: a  reason: collision with root package name */
    private static final i f764a = new i(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f765b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f766c;
    /* access modifiers changed from: private */
    public ByteString d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public Object f;
    /* access modifiers changed from: private */
    public ByteString g;
    /* access modifiers changed from: private */
    public w h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public Object k;
    /* access modifiers changed from: private */
    public Object l;
    /* access modifiers changed from: private */
    public Object m;
    private byte n;
    private int o;

    private i(j jVar) {
        super(jVar);
        this.n = -1;
        this.o = -1;
    }

    private i(boolean z) {
        this.n = -1;
        this.o = -1;
    }

    public static i a() {
        return f764a;
    }

    public boolean b() {
        return (this.f765b & 1) == 1;
    }

    public ByteString c() {
        return this.f766c;
    }

    public boolean d() {
        return (this.f765b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    public boolean f() {
        return (this.f765b & 4) == 4;
    }

    public int g() {
        return this.e;
    }

    public boolean h() {
        return (this.f765b & 8) == 8;
    }

    public String i() {
        Object obj = this.f;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString A() {
        Object obj = this.f;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean j() {
        return (this.f765b & 16) == 16;
    }

    public ByteString k() {
        return this.g;
    }

    public boolean l() {
        return (this.f765b & 32) == 32;
    }

    public w m() {
        return this.h;
    }

    public boolean n() {
        return (this.f765b & 64) == 64;
    }

    public boolean o() {
        return this.i;
    }

    public boolean p() {
        return (this.f765b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public int q() {
        return this.j;
    }

    public boolean r() {
        return (this.f765b & 256) == 256;
    }

    public String s() {
        Object obj = this.k;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.k = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString B() {
        Object obj = this.k;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.k = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean t() {
        return (this.f765b & 512) == 512;
    }

    public String u() {
        Object obj = this.l;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.l = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString C() {
        Object obj = this.l;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.l = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean v() {
        return (this.f765b & 1024) == 1024;
    }

    public String w() {
        Object obj = this.m;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.m = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString D() {
        Object obj = this.m;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.m = copyFromUtf8;
        return copyFromUtf8;
    }

    private void E() {
        this.f766c = ByteString.EMPTY;
        this.d = ByteString.EMPTY;
        this.e = 0;
        this.f = "";
        this.g = ByteString.EMPTY;
        this.h = w.a();
        this.i = false;
        this.j = 0;
        this.k = "";
        this.l = "";
        this.m = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.n;
        if (b2 == -1) {
            this.n = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f765b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f766c);
        }
        if ((this.f765b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
        if ((this.f765b & 4) == 4) {
            codedOutputStream.writeInt32(3, this.e);
        }
        if ((this.f765b & 16) == 16) {
            codedOutputStream.writeBytes(4, this.g);
        }
        if ((this.f765b & 8) == 8) {
            codedOutputStream.writeBytes(6, A());
        }
        if ((this.f765b & 32) == 32) {
            codedOutputStream.writeMessage(7, this.h);
        }
        if ((this.f765b & 64) == 64) {
            codedOutputStream.writeBool(8, this.i);
        }
        if ((this.f765b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeInt32(9, this.j);
        }
        if ((this.f765b & 256) == 256) {
            codedOutputStream.writeBytes(10, B());
        }
        if ((this.f765b & 512) == 512) {
            codedOutputStream.writeBytes(11, C());
        }
        if ((this.f765b & 1024) == 1024) {
            codedOutputStream.writeBytes(12, D());
        }
    }

    public int getSerializedSize() {
        int i2 = this.o;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f765b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, this.f766c);
            }
            if ((this.f765b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, this.d);
            }
            if ((this.f765b & 4) == 4) {
                i2 += CodedOutputStream.computeInt32Size(3, this.e);
            }
            if ((this.f765b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(4, this.g);
            }
            if ((this.f765b & 8) == 8) {
                i2 += CodedOutputStream.computeBytesSize(6, A());
            }
            if ((this.f765b & 32) == 32) {
                i2 += CodedOutputStream.computeMessageSize(7, this.h);
            }
            if ((this.f765b & 64) == 64) {
                i2 += CodedOutputStream.computeBoolSize(8, this.i);
            }
            if ((this.f765b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeInt32Size(9, this.j);
            }
            if ((this.f765b & 256) == 256) {
                i2 += CodedOutputStream.computeBytesSize(10, B());
            }
            if ((this.f765b & 512) == 512) {
                i2 += CodedOutputStream.computeBytesSize(11, C());
            }
            if ((this.f765b & 1024) == 1024) {
                i2 += CodedOutputStream.computeBytesSize(12, D());
            }
            this.o = i2;
        }
        return i2;
    }

    public static i a(InputStream inputStream) {
        return ((j) x().mergeFrom(inputStream)).j();
    }

    public static j x() {
        return j.i();
    }

    /* renamed from: y */
    public j newBuilderForType() {
        return x();
    }

    public static j a(i iVar) {
        return x().mergeFrom(iVar);
    }

    /* renamed from: z */
    public j toBuilder() {
        return a(this);
    }

    static {
        f764a.E();
    }
}
