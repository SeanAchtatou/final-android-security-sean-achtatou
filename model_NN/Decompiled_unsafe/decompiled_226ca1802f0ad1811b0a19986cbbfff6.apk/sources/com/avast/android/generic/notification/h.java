package com.avast.android.generic.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.avast.android.generic.ab;
import com.avast.android.generic.e;
import com.avast.android.generic.s;
import com.avast.android.generic.util.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/* compiled from: AvastNotificationManager */
public abstract class h {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f933a;

    /* renamed from: b  reason: collision with root package name */
    private NotificationManager f934b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Uri f935c;
    private ab d;
    private Notification e;
    private LinkedList<o> f;
    private Map<o, a> g;
    private Notification h;
    private LinkedList<o> i;
    private Map<o, a> j;
    private n k;

    public abstract int a(int i2);

    public abstract void a(Context context);

    public abstract void a(Context context, Intent intent);

    public abstract int f();

    public abstract int g();

    public synchronized void a() {
        a((a) null);
    }

    public synchronized void a(a aVar) {
        boolean c2 = c(aVar);
        if (this.e != null && c2) {
            this.f934b.notify(s.notification_manager, this.e);
        }
    }

    private boolean c(a aVar) {
        int i2;
        boolean z = true;
        boolean z2 = false;
        boolean z3 = aVar == null && this.e != null;
        if (aVar == null && !this.f.isEmpty()) {
            aVar = this.g.get(this.f.getLast());
        }
        if (aVar != null) {
            o oVar = new o(aVar.f920a, aVar.f921b, null);
            if (!this.f.contains(oVar)) {
                this.f.add(oVar);
            }
            int i3 = 0;
            for (Map.Entry<o, a> value : this.g.entrySet()) {
                a aVar2 = (a) value.getValue();
                if (aVar2.m > i3) {
                    i2 = aVar2.m;
                } else {
                    i2 = i3;
                }
                i3 = i2;
            }
            if (aVar.m >= i3 || (aVar.f & 2) == 0) {
                z2 = true;
            }
            this.g.put(oVar, aVar);
            e(aVar);
            this.e = this.k.a(aVar);
            z = z2;
        } else if (this.d.M()) {
            this.e = this.k.a();
        } else {
            this.e = null;
        }
        if (z3) {
            this.e.tickerText = null;
        }
        return z;
    }

    public synchronized void b(a aVar) {
        boolean d2 = d(aVar);
        if (this.h != null && d2) {
            this.f934b.notify(s.notification_manager_temporary, this.h);
        }
    }

    private boolean d(a aVar) {
        int i2;
        boolean z = false;
        if (aVar == null && !this.i.isEmpty()) {
            aVar = this.j.get(this.i.getLast());
        }
        if (aVar == null) {
            this.h = null;
            return true;
        }
        o oVar = new o(aVar.f920a, aVar.f921b, null);
        if (!this.i.contains(oVar)) {
            this.i.add(oVar);
        }
        int i3 = 0;
        for (Map.Entry<o, a> value : this.j.entrySet()) {
            a aVar2 = (a) value.getValue();
            if (aVar2.m > i3) {
                i2 = aVar2.m;
            } else {
                i2 = i3;
            }
            i3 = i2;
        }
        if (aVar.m >= i3) {
            z = true;
        }
        this.j.put(oVar, aVar);
        this.h = this.k.b(aVar);
        return z;
    }

    private void e(a aVar) {
        if (aVar == null) {
            return;
        }
        if ((aVar.f & 2) > 0) {
            this.f933a.getContentResolver().notifyChange(e.a(this.f935c), null);
        } else {
            b.a(new j(this, aVar), new Void[0]);
        }
    }

    public synchronized void a(long j2) {
        a(j2, (String) null);
    }

    public synchronized void a(long j2, String str) {
        o oVar = new o(j2, str, null);
        if (!this.f.isEmpty()) {
            this.f.remove(oVar);
            this.g.remove(oVar);
            b.a(new k(this, j2, str), new Void[0]);
            if (!this.f.getLast().equals(oVar)) {
                a();
            } else if (!this.f.isEmpty() || this.d.M()) {
                a();
            } else {
                this.f934b.cancel(s.notification_manager);
                this.e = null;
            }
        }
    }

    public synchronized void b(long j2) {
        b(j2, null);
    }

    public synchronized void b(long j2, String str) {
        o oVar = new o(j2, str, null);
        if (!this.i.isEmpty()) {
            this.i.remove(oVar);
            this.j.remove(oVar);
            if (!this.i.getLast().equals(oVar)) {
                b((a) null);
            } else if (this.i.isEmpty()) {
                this.f934b.cancel(s.notification_manager_temporary);
                this.h = null;
            } else {
                b((a) null);
            }
        }
    }

    public synchronized void b() {
        Iterator<Map.Entry<o, a>> it = this.g.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            if ((((a) next.getValue()).f & 2) == 0) {
                it.remove();
                this.f.remove(next.getKey());
                AvastPendingIntent avastPendingIntent = ((a) next.getValue()).h;
                if (avastPendingIntent != null) {
                    a(avastPendingIntent);
                }
            }
        }
        b.a(new l(this), new Void[0]);
        if (!this.f.isEmpty() || this.d.M()) {
            a();
        } else {
            this.e = null;
            this.f934b.cancel(s.notification_manager);
        }
        this.i.clear();
    }

    public synchronized void c() {
        for (Map.Entry<o, a> value : this.j.entrySet()) {
            AvastPendingIntent avastPendingIntent = ((a) value.getValue()).h;
            if (avastPendingIntent != null) {
                a(avastPendingIntent);
            }
        }
        this.i.clear();
        this.j.clear();
        this.f934b.cancel(s.notification_manager_temporary);
    }

    public void a(AvastPendingIntent avastPendingIntent) {
        Intent a2 = avastPendingIntent.a(this.f933a);
        if (a2 != null) {
            switch (m.f943a[avastPendingIntent.a().ordinal()]) {
                case 1:
                    this.f933a.startService(a2);
                    return;
                case 2:
                    this.f933a.sendBroadcast(a2);
                    return;
                default:
                    a(this.f933a, a2);
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized List<a> d() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        ListIterator<o> listIterator = this.f.listIterator(this.f.size());
        while (listIterator.hasPrevious()) {
            arrayList.add(this.g.get(listIterator.previous()));
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public synchronized List<a> e() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        ListIterator<o> listIterator = this.i.listIterator(this.i.size());
        while (listIterator.hasPrevious()) {
            arrayList.add(this.j.get(listIterator.previous()));
        }
        return arrayList;
    }

    public Uri h() {
        return this.f935c;
    }
}
