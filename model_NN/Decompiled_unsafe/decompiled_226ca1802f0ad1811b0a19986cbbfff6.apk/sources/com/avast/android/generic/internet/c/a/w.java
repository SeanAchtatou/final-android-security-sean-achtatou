package com.avast.android.generic.internet.c.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: MyAvastPairing */
public final class w extends GeneratedMessageLite implements ad {

    /* renamed from: a  reason: collision with root package name */
    private static final w f789a = new w(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f790b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f791c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public y e;
    private byte f;
    private int g;

    private w(x xVar) {
        super(xVar);
        this.f = -1;
        this.g = -1;
    }

    private w(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static w a() {
        return f789a;
    }

    public boolean b() {
        return (this.f790b & 1) == 1;
    }

    public ByteString c() {
        return this.f791c;
    }

    public boolean d() {
        return (this.f790b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString k() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f790b & 4) == 4;
    }

    public y g() {
        return this.e;
    }

    private void l() {
        this.f791c = ByteString.EMPTY;
        this.d = "";
        this.e = y.a();
    }

    public final boolean isInitialized() {
        byte b2 = this.f;
        if (b2 == -1) {
            this.f = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f790b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f791c);
        }
        if ((this.f790b & 2) == 2) {
            codedOutputStream.writeBytes(2, k());
        }
        if ((this.f790b & 4) == 4) {
            codedOutputStream.writeMessage(3, this.e);
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            if ((this.f790b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, this.f791c);
            }
            if ((this.f790b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, k());
            }
            if ((this.f790b & 4) == 4) {
                i += CodedOutputStream.computeMessageSize(3, this.e);
            }
            this.g = i;
        }
        return i;
    }

    public static x h() {
        return x.i();
    }

    /* renamed from: i */
    public x newBuilderForType() {
        return h();
    }

    public static x a(w wVar) {
        return h().mergeFrom(wVar);
    }

    /* renamed from: j */
    public x toBuilder() {
        return a(this);
    }

    static {
        f789a.l();
    }
}
