package com.avast.android.generic.util;

import android.text.TextUtils;
import com.avast.c.a.a.an;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* compiled from: WebPurchaseHelper */
public class ax {

    /* renamed from: a  reason: collision with root package name */
    static String f1200a = "https://store.avast.com/store/avast/en_NZ/buy/productID.288220900/quantity.1/Currency.USD/ThemeID.32793100/clearCart.1";

    /* renamed from: b  reason: collision with root package name */
    static Map<String, String> f1201b = new HashMap();

    static {
        f1201b.put("en_US", "https://store.avast.com/store/avast/en_US/buy/productID.288220900/quantity.1/Currency.USD/ThemeID.32793100/clearCart.1");
        f1201b.put("el_GR", "https://store.avast.com/store/avast/el_GR/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("de_DE", "https://store.avast.com/store/avast/de_DE/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("en_IE", "https://store.avast.com/store/avast/en_IE/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("es_ES", "https://store.avast.com/store/avast/es_ES/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("fi_FI", "https://store.avast.com/store/avast/fi_FI/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("fr_BE", "https://store.avast.com/store/avast/fr_BE/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("fr_FR", "https://store.avast.com/store/avast/fr_FR/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("it_IT", "https://store.avast.com/store/avast/it_IT/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("nl_BE", "https://store.avast.com/store/avast/nl_BE/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("nl_NL", "https://store.avast.com/store/avast/nl_NL/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("pt_PT", "https://store.avast.com/store/avast/pt_PT/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("pt_BR", "https://store.avast.com/store/avastbr/pt_BR/buy/productID.288220900/quantity.1/Currency.BRL/ThemeID.34413000/clearCart.1");
        f1201b.put("cs_CZ", "https://store.avast.com/store/avast/cs_CZ/buy/productID.288220900/quantity.1/Currency.EUR/ThemeID.32793100/clearCart.1");
        f1201b.put("en_ZA", "https://store.avast.com/store/avast/en_ZA/buy/productID.288220900/quantity.1/Currency.USD/ThemeID.32793100/clearCart.1");
    }

    public static String a(an anVar) {
        Locale locale = Locale.getDefault();
        if (!TextUtils.isEmpty(locale.getLanguage()) && !TextUtils.isEmpty(locale.getCountry())) {
            String str = locale.getLanguage() + "_" + locale.getCountry();
            if (anVar != null && anVar != an.SUITE) {
                if (anVar == an.VPN) {
                }
                return null;
            } else if (f1201b.containsKey(str)) {
                return f1201b.get(str);
            } else {
                return null;
            }
        } else if (anVar == null || anVar == an.SUITE) {
            return f1200a;
        } else {
            if (anVar == an.VPN) {
            }
            return null;
        }
    }
}
