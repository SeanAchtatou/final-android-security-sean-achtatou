package com.avast.android.generic.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.avast.android.generic.x;

@SuppressLint({"DefaultLocale"})
/* compiled from: ExceptionUtils */
public class w {

    /* renamed from: a  reason: collision with root package name */
    private static final x[] f1257a = {new x("HTTP status 400", x.l_error_http_status_400), new x("ECONNREFUSED", x.l_error_connection_refused), new x("ENETUNREACH", x.l_error_connection_refused), new x("unexpected end of stream", x.l_error_cut_down), new x("unable to resolve host", x.l_host_cannot_be_resolved), new x("INETNOTAVAILABLE", x.l_error_no_internet), new x("Device or resource busy", x.l_error_device_or_resource_busy)};

    public static String a(Fragment fragment, Throwable th) {
        if (fragment != null && fragment.isAdded()) {
            return a(fragment.getActivity(), th);
        }
        String message = th.getMessage();
        if (TextUtils.isEmpty(message)) {
            return "Reason unknown";
        }
        return message;
    }

    public static String a(Context context, Throwable th) {
        return a(context, th.getMessage());
    }

    public static String a(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            return context.getString(x.l_error_reason_unknown);
        }
        String lowerCase = str.toLowerCase();
        for (x xVar : f1257a) {
            if (lowerCase.contains(xVar.f1258a)) {
                return context.getString(xVar.f1259b);
            }
        }
        return str;
    }
}
