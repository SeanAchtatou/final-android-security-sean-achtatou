package com.avast.android.generic.util;

import java.lang.ref.WeakReference;

/* compiled from: BetterWeakReference */
public class k<T> extends WeakReference<T> {
    public k(T t) {
        super(t);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof WeakReference) || get() == null) {
            return false;
        }
        return get().equals(((WeakReference) obj).get());
    }

    public int hashCode() {
        if (get() != null) {
            return get().hashCode();
        }
        return super.hashCode();
    }
}
