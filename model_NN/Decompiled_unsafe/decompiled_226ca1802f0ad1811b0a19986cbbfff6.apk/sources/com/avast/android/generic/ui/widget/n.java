package com.avast.android.generic.ui.widget;

import android.os.Handler;
import android.os.Message;

/* compiled from: PasswordTextView */
class n extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PasswordTextView f1153a;

    private n(PasswordTextView passwordTextView) {
        this.f1153a = passwordTextView;
    }

    public void handleMessage(Message message) {
        String obj = this.f1153a.getText().toString();
        if (obj.length() > 0 && PasswordTextView.f1095a.matcher(obj).matches()) {
            int selectionStart = this.f1153a.getSelectionStart();
            this.f1153a.setText(a(obj, message.arg1, message.arg2));
            this.f1153a.setSelection(selectionStart);
        }
    }

    private String a(String str, int i, int i2) {
        char[] charArray = str.toCharArray();
        for (int i3 = 0; i3 < str.length(); i3++) {
            if (i3 < i || i3 >= i2) {
                charArray[i3] = '*';
            }
        }
        return String.valueOf(charArray);
    }
}
