package com.avast.android.generic.app.about;

import android.content.Context;
import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.avast.android.generic.ui.BaseActivity;
import com.avast.android.generic.ui.BaseSinglePaneActivity;

public class AboutActivity extends BaseSinglePaneActivity {
    public static void call(Context context, Bundle bundle) {
        ((BaseActivity) context).a(AboutActivity.class, bundle);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActionBar supportActionBar = getSupportActionBar();
        supportActionBar.setHomeButtonEnabled(true);
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        supportActionBar.setDisplayShowTitleEnabled(true);
        supportActionBar.setDisplayUseLogoEnabled(false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public SherlockFragment a() {
        return new AboutFragment();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        g().b();
    }
}
