package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.RelativeLayout;

public class CheckableRelativeLayout extends RelativeLayout implements Checkable {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f1083a = {16842912};

    /* renamed from: b  reason: collision with root package name */
    private boolean f1084b = false;

    public CheckableRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CheckableRelativeLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setChecked(boolean z) {
        this.f1084b = z;
        refreshDrawableState();
    }

    public boolean isChecked() {
        return this.f1084b;
    }

    public void toggle() {
        setChecked(!this.f1084b);
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (this.f1084b) {
            mergeDrawableStates(onCreateDrawableState, f1083a);
        }
        return onCreateDrawableState;
    }
}
