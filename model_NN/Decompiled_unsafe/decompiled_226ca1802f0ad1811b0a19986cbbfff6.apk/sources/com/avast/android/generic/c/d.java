package com.avast.android.generic.c;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ad;
import com.avast.android.generic.app.passwordrecovery.a;
import com.avast.android.generic.c.a.b;
import com.avast.android.generic.internet.k;
import com.avast.android.generic.service.AvastService;
import com.avast.android.generic.util.ae;
import com.avast.android.generic.util.ga.e;
import com.avast.android.generic.util.z;
import com.avast.b.a.a.a.m;
import java.util.List;

/* compiled from: CommandHandler */
public abstract class d extends e {

    /* renamed from: a  reason: collision with root package name */
    private AvastService f624a;

    /* renamed from: b  reason: collision with root package name */
    private f f625b;

    /* renamed from: c  reason: collision with root package name */
    private f f626c;
    private String d;
    private String e;
    private boolean f;
    private a g;
    private List h;
    private boolean i;
    private m j;
    private boolean k;
    private k l;
    private boolean m;
    private boolean n;
    private boolean o;

    public boolean a() {
        return this.k;
    }

    public boolean b() {
        return this.i;
    }

    public boolean c() {
        return !this.f;
    }

    public f d() {
        return this.f625b;
    }

    public String e() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public AvastService f() {
        return this.f624a;
    }

    public Object g() {
        return this.d;
    }

    public m h() {
        return this.f ? this.j : m.NONE;
    }

    public f i() {
        return this.f626c;
    }

    public String j() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public List k() {
        return this.h;
    }

    public static k a(Context context, String str) {
        boolean z;
        String str2;
        boolean z2;
        String c2;
        z.a("AvastGeneric", context, "Parsing " + str);
        h hVar = new h(str);
        try {
            String a2 = hVar.a(j.SPACED_STRING);
            ab abVar = (ab) aa.a(context, ad.class);
            boolean b2 = abVar.b(a2);
            if (b2 || !abVar.t() || !abVar.y().equals(a2)) {
                z = b2;
            } else {
                z = true;
            }
            if (!z && a.a(context, a2)) {
                z = true;
            }
            if (!z) {
                z.a("AvastGeneric", context, "Passwords not matching");
                return new k();
            }
            try {
                String b3 = hVar.b(j.SPACED_STRING);
                if (b3.equals("IC")) {
                    hVar.c(j.SPACED_STRING);
                    b3 = hVar.c(j.SPACED_STRING);
                    try {
                        if (hVar.b(j.SPACED_STRING).equals("-9999")) {
                            hVar.c(j.SPACED_STRING);
                            c2 = "-9999";
                        } else {
                            c2 = hVar.c(j.NUMBER);
                        }
                        str2 = c2;
                        z2 = false;
                    } catch (b e2) {
                        hVar.c(j.SPACED_STRING);
                        str2 = null;
                        z2 = true;
                    }
                    hVar.c(j.SPACED_STRING);
                } else {
                    str2 = null;
                    z2 = false;
                }
                try {
                    hVar.c(j.SPACED_STRING).toUpperCase();
                    if (z2) {
                        return new k();
                    }
                    if (b3.equals("ALL")) {
                        return new k(o.MULTIPLE, str2);
                    }
                    if (b3.equals("FO")) {
                        return new k(o.FIRST_ONE, str2);
                    }
                    try {
                        return new k(context, str2, b3);
                    } catch (Exception e3) {
                        return new k(context, str2, "AT");
                    }
                } catch (Exception e4) {
                    z.a("AvastGeneric", context, "Exception in parsing command part", e4);
                    return new k();
                }
            } catch (Exception e5) {
                z.a("AvastGeneric", context, "Exception in parsing command header", e5);
                return new k();
            }
        } catch (Exception e6) {
            z.a("AvastGeneric", "Exception in parsing password token", e6);
            return new k();
        }
    }

    /* access modifiers changed from: protected */
    public a l() {
        return this.g;
    }

    public boolean m() {
        return this.m;
    }

    public k n() {
        return this.l;
    }

    public static void a(Context context, Intent intent, String str, String str2, String str3, boolean z) {
        a(context, intent, a(context, str3), str, str2, str3, z);
    }

    public static void a(Context context, Intent intent, k kVar, String str, String str2, String str3, boolean z) {
        if (Build.VERSION.SDK_INT < 19 || !z) {
            switch (e.f627a[kVar.f639a.ordinal()]) {
                case 1:
                    z.a(context, "Skip dispatching invalid message " + str3);
                    return;
                case 2:
                    z.a(context, "Dispatch/cancel message " + str3 + " to tool " + kVar.f641c);
                    ae.a(context, intent, kVar.f641c);
                    return;
                case 3:
                    z.a(context, "Dispatch/cancel message " + str3 + " to myself (FIRST ONE) directly");
                    ae.a(context, intent, context.getPackageName());
                    return;
                case 4:
                    z.a(context, "Dispatch/cancel message " + str3 + " to myself (MULTIPLE) directly");
                    ae.a(context, intent, context.getPackageName());
                    z.a(context, "Additionally dispatch message " + str3 + " to MULTIPLE");
                    com.avast.android.generic.util.aa.a(context, str, str2, str3);
                    return;
                default:
                    return;
            }
        } else {
            String str4 = kVar.f641c;
            if (str4 != null) {
                String packageName = context.getPackageName();
                switch (e.f627a[kVar.f639a.ordinal()]) {
                    case 1:
                        z.a(context, "Skip dispatching invalid message " + str3);
                        return;
                    case 2:
                        if (TextUtils.equals(packageName, str4)) {
                            z.a(context, "Dispatch/cancel message " + str3 + " to myself");
                            ae.a(context, intent, context.getPackageName());
                            return;
                        }
                        return;
                    case 3:
                    default:
                        return;
                    case 4:
                        z.a(context, "Dispatch/cancel message " + str3 + " to myself (MULTIPLE), system handles distribution to all packages");
                        ae.a(context, intent, context.getPackageName());
                        return;
                }
            }
        }
    }

    public boolean o() {
        return this.n;
    }

    public boolean p() {
        return this.o;
    }
}
