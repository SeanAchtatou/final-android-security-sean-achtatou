package com.avast.android.generic.licensing;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.widget.Toast;
import com.actionbarsherlock.R;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.util.a;
import com.avast.android.generic.x;
import com.avast.c.a.a.an;

public class LicensingBroadcastReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private Fragment f837a;

    /* renamed from: b  reason: collision with root package name */
    private l f838b;

    /* renamed from: c  reason: collision with root package name */
    private Context f839c;
    private boolean d = false;
    private boolean e = false;
    private Uri f;

    public LicensingBroadcastReceiver(Context context, Fragment fragment, l lVar, boolean z, Uri uri) {
        this.f837a = fragment;
        this.f838b = lVar;
        this.e = z;
        this.f839c = context;
        this.f = uri;
    }

    public void a(boolean z, boolean z2) {
        if ((this.f837a == null || this.f837a.isAdded()) && this.f838b != null && z2) {
            this.f838b.a(m.PROGRESS, null);
        }
    }

    public void b(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.msg_no_internet_connectivity, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void c(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.l_offers_subscriptions_not_supported, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void d(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.msg_home_error_restoring_transactions_message, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void e(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.msg_home_error_restoring_too_often_message, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void a(boolean z, int i, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (this.f837a != null && !this.d && !z) {
                com.avast.android.generic.licensing.a.a.a(this.f837a, i, 255);
            }
        }
    }

    public void f(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.l_offers_google_play_invalid, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void a(boolean z, Intent intent, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (this.f837a != null && !this.d && !z) {
                com.avast.android.generic.licensing.a.a.a(this.f837a, intent, 255);
            }
        }
    }

    public void g(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.msg_home_error_restoring_transactions_no_google_account, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void h(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.NONE, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.msg_home_error_license_already_expired, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void i(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.NONE, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.msg_home_error_license_invalid, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void j(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.msg_home_error_restoring_transactions_message, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void k(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.msg_home_error_restoring_transactions_message_generic, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void l(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.msg_home_error_restoring_invalid_credentials, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void m(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.l_home_error_too_many_google_accounts_desc, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void n(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.l_home_error_too_many_devices_desc, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void o(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.UNKNOWN, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.l_home_error_non_unique_guid_desc, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void p(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.NONE, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.l_offers_license_not_found, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void q(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.NONE, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.l_offers_code_unknown, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void r(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.NONE, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.l_offers_code_locked, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void s(boolean z, boolean z2) {
        if (this.f837a == null || this.f837a.isAdded()) {
            if (this.f838b != null && z2) {
                this.f838b.a(m.NONE, null);
            }
            if (!this.d && !z) {
                Toast.makeText(this.f839c, x.l_offers_code_already_consumed, 1).show();
            }
            if (this.f837a != null && this.e) {
                a.a(this.f837a);
            }
        }
    }

    public void a(boolean z, m mVar, boolean z2) {
        if ((this.f837a == null || this.f837a.isAdded()) && this.f838b != null && z2) {
            this.f838b.a(mVar, null);
        }
    }

    public void a(boolean z, long j, m mVar, boolean z2, String str, boolean z3, String str2) {
        if ((this.f837a == null || this.f837a.isAdded()) && this.f838b != null && z3) {
            this.f838b.a(j);
            this.f838b.a(z2);
            this.f838b.a(str);
            this.f838b.a(mVar, str2);
        }
    }

    public void a(boolean z, boolean z2, long j, boolean z3, String str) {
        m mVar = z ? m.VALID : m.NONE;
        if ((this.f837a == null || this.f837a.isAdded()) && this.f838b != null && z2) {
            this.f838b.a(j);
            this.f838b.a(z3);
            this.f838b.a(str);
            this.f838b.a(mVar, null);
        }
    }

    public void t(boolean z, boolean z2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, int, boolean):void
     arg types: [boolean, int, int]
     candidates:
      com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, android.content.Intent, boolean):void
      com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, com.avast.android.generic.licensing.m, boolean):void
      com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, android.content.Intent, boolean):void
     arg types: [boolean, android.content.Intent, int]
     candidates:
      com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, int, boolean):void
      com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, com.avast.android.generic.licensing.m, boolean):void
      com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, android.content.Intent, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, com.avast.android.generic.licensing.m, boolean):void
     arg types: [boolean, com.avast.android.generic.licensing.m, int]
     candidates:
      com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, int, boolean):void
      com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, android.content.Intent, boolean):void
      com.avast.android.generic.licensing.LicensingBroadcastReceiver.a(boolean, com.avast.android.generic.licensing.m, boolean):void */
    public void onReceive(Context context, Intent intent) {
        int intExtra;
        int intExtra2 = intent.getIntExtra("toolGroupIdentifier", -1);
        if (intExtra2 != -1 && intExtra2 < an.values().length && an.values()[intExtra2] == ab.b(context) && (intExtra = intent.getIntExtra("state", -1)) != -1) {
            boolean booleanExtra = intent.getBooleanExtra("background", false);
            switch (a.f844b[k.values()[intExtra].ordinal()]) {
                case 1:
                    a(booleanExtra, true);
                    return;
                case 2:
                    t(booleanExtra, true);
                    return;
                case 3:
                    int intExtra3 = intent.getIntExtra("reason", -1);
                    if (intExtra3 == -1) {
                        d(booleanExtra, true);
                        return;
                    }
                    switch (a.f843a[i.values()[intExtra3].ordinal()]) {
                        case 1:
                            m(booleanExtra, true);
                            return;
                        case 2:
                            n(booleanExtra, true);
                            return;
                        case 3:
                            o(booleanExtra, true);
                            return;
                        case 4:
                            s(booleanExtra, true);
                            return;
                        case 5:
                            r(booleanExtra, true);
                            return;
                        case 6:
                            q(booleanExtra, true);
                            return;
                        case 7:
                            p(booleanExtra, true);
                            return;
                        case 8:
                            g(booleanExtra, true);
                            return;
                        case 9:
                            h(booleanExtra, true);
                            return;
                        case 10:
                            i(booleanExtra, true);
                            return;
                        case 11:
                            b(booleanExtra, true);
                            return;
                        case 12:
                            c(booleanExtra, true);
                            return;
                        case 13:
                        case 14:
                            d(booleanExtra, true);
                            return;
                        case 15:
                            e(booleanExtra, true);
                            return;
                        case 16:
                            int intExtra4 = intent.getIntExtra("gPlayErrorCode", -1);
                            if (intExtra4 == -1) {
                                l(booleanExtra, true);
                                return;
                            } else {
                                a(booleanExtra, intExtra4, true);
                                return;
                            }
                        case 17:
                            Intent intent2 = (Intent) intent.getExtras().getParcelable("gPlayErrorIntent");
                            if (intent2 == null) {
                                l(booleanExtra, true);
                                return;
                            } else {
                                a(booleanExtra, intent2, true);
                                return;
                            }
                        case 18:
                            f(booleanExtra, true);
                            return;
                        case R.styleable.SherlockTheme_buttonStyleSmall:
                            j(booleanExtra, true);
                            return;
                        case R.styleable.SherlockTheme_selectableItemBackground:
                            l(booleanExtra, true);
                            return;
                        case R.styleable.SherlockTheme_windowContentOverlay:
                            int intExtra5 = intent.getIntExtra("rowState", -1);
                            if (intExtra5 == -1) {
                                d(booleanExtra, false);
                                return;
                            }
                            int intExtra6 = intent.getIntExtra("reasonSub", -1);
                            if (intExtra6 != -1) {
                                switch (a.f843a[i.values()[intExtra6].ordinal()]) {
                                    case 1:
                                        m(booleanExtra, false);
                                        break;
                                    case 2:
                                        n(booleanExtra, false);
                                        break;
                                    case 3:
                                        o(booleanExtra, false);
                                        break;
                                    case 4:
                                        s(booleanExtra, false);
                                        break;
                                    case 5:
                                        r(booleanExtra, false);
                                        break;
                                    case 6:
                                        q(booleanExtra, false);
                                        break;
                                    case 7:
                                        p(booleanExtra, false);
                                        break;
                                    case 8:
                                        g(booleanExtra, false);
                                        break;
                                    case 9:
                                        h(booleanExtra, true);
                                        break;
                                    case 10:
                                        i(booleanExtra, true);
                                        break;
                                    default:
                                        d(booleanExtra, false);
                                        break;
                                }
                            }
                            a(booleanExtra, m.values()[intExtra5], true);
                            return;
                        case R.styleable.SherlockTheme_textAppearanceLargePopupMenu:
                            k(booleanExtra, true);
                            return;
                        default:
                            return;
                    }
                case 4:
                    long longExtra = intent.getLongExtra("expirationDate", -255);
                    if (longExtra == -255) {
                        d(booleanExtra, false);
                        return;
                    }
                    int intExtra7 = intent.getIntExtra("rowState", -1);
                    if (intExtra7 == -1) {
                        d(booleanExtra, false);
                        return;
                    }
                    boolean booleanExtra2 = intent.getBooleanExtra("isSubscription", false);
                    String stringExtra = intent.getStringExtra("sku");
                    if (stringExtra == null) {
                        d(booleanExtra, false);
                        return;
                    } else {
                        a(booleanExtra, longExtra, m.values()[intExtra7], booleanExtra2, stringExtra, true, intent.getStringExtra("sourcePackage"));
                        return;
                    }
                case 5:
                    boolean a2 = PremiumHelper.a(this.f839c);
                    long b2 = PremiumHelper.b(this.f839c);
                    if (a2 && b2 == -2) {
                        PurchaseConfirmationService.a(context, this.f, (ab) aa.a(this.f839c, ab.class));
                        b2 = PremiumHelper.b(context);
                    }
                    a(a2, true, b2, PremiumHelper.c(this.f839c), PremiumHelper.e(this.f839c));
                    return;
                default:
                    d(booleanExtra, false);
                    return;
            }
        }
    }
}
