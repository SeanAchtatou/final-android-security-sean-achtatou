package com.avast.android.generic.app.about;

import android.a.a.a;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.h.c;
import com.avast.android.generic.h.n;
import com.avast.android.generic.h.q;
import com.avast.android.generic.util.l;
import com.avast.android.generic.util.t;
import com.google.protobuf.ByteString;
import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.EntityUtils;

@TargetApi(8)
/* compiled from: FeedbackSender */
public class o {

    /* renamed from: a  reason: collision with root package name */
    private Context f353a;

    /* renamed from: b  reason: collision with root package name */
    private String f354b;

    /* renamed from: c  reason: collision with root package name */
    private a f355c;

    public o(Context context) {
        this.f353a = context.getApplicationContext();
        this.f354b = ((ab) aa.a(context, ab.class)).q();
    }

    public boolean a(String str, String str2, String str3, q qVar, c cVar, byte[] bArr, byte[] bArr2) {
        boolean z = false;
        a(this.f353a);
        HttpEntity httpEntity = null;
        try {
            com.avast.android.generic.h.o r = n.r();
            r.a(ByteString.copyFrom(l.a(this.f354b)));
            if (!TextUtils.isEmpty(str)) {
                r.a(str);
            }
            if (!TextUtils.isEmpty(str2)) {
                r.b(str2);
            }
            if (TextUtils.isEmpty(str3)) {
                str3 = "no description";
            }
            r.c(str3);
            r.a(qVar);
            if (cVar != null) {
                r.a(cVar);
            }
            if (bArr != null) {
                r.b(ByteString.copyFrom(bArr));
            }
            if (bArr2 != null) {
                r.c(ByteString.copyFrom(bArr2));
            }
            HttpPost httpPost = new HttpPost("http://af.ff.avast.com/feedback");
            t.b("FeedbackSender", "Sending feedback with GUID: " + this.f354b);
            httpPost.setEntity(new ByteArrayEntity(r.build().toByteArray()));
            HttpResponse execute = this.f355c.execute(httpPost);
            if (execute == null || execute.getStatusLine() == null || execute.getStatusLine().getStatusCode() != 200) {
                t.d("FeedbackSender", "Response code: " + execute.getStatusLine().getStatusCode());
                if (httpEntity != null) {
                    try {
                        httpEntity.consumeContent();
                    } catch (IOException e) {
                    }
                }
                a();
            } else {
                httpEntity = execute.getEntity();
                if ("OK".equals(EntityUtils.toString(httpEntity))) {
                    z = true;
                    if (httpEntity != null) {
                        try {
                            httpEntity.consumeContent();
                        } catch (IOException e2) {
                        }
                    }
                    a();
                } else {
                    t.d("FeedbackSender", "Response: " + EntityUtils.toString(httpEntity));
                    if (httpEntity != null) {
                        try {
                            httpEntity.consumeContent();
                        } catch (IOException e3) {
                        }
                    }
                    a();
                }
            }
            return z;
        } catch (ClientProtocolException e4) {
            throw new p(e4);
        } catch (IOException e5) {
            throw new p(e5);
        } catch (Exception e6) {
            throw new p(e6);
        } catch (Throwable th) {
            if (httpEntity != null) {
                try {
                    httpEntity.consumeContent();
                } catch (IOException e7) {
                }
            }
            a();
            throw th;
        }
    }

    private void a() {
        if (this.f355c != null) {
            this.f355c.a();
        }
        this.f355c = null;
    }

    private void a(Context context) {
        this.f355c = a.a("avast! Mobile Security");
        SchemeRegistry schemeRegistry = this.f355c.getConnectionManager().getSchemeRegistry();
        if (Build.VERSION.SDK_INT < 8) {
            schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        } else {
            q.a(context, schemeRegistry);
        }
    }
}
