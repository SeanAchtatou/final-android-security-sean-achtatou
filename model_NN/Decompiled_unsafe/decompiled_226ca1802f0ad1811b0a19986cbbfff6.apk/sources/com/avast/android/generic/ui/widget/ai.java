package com.avast.android.generic.ui.widget;

import android.widget.CompoundButton;
import com.avast.android.generic.d;
import com.avast.android.generic.e.c;

/* compiled from: WeekDaysRow */
class ai implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WeekDaysRow f1140a;

    ai(WeekDaysRow weekDaysRow) {
        this.f1140a = weekDaysRow;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        int i = 0;
        if (!this.f1140a.m) {
            int i2 = 1;
            for (int i3 = 0; i3 < 7; i3++) {
                if (this.f1140a.f1129a[i3].isChecked()) {
                    i += i2;
                }
                i2 *= 2;
            }
            int unused = this.f1140a.f1131c = d.a(i);
            c e = this.f1140a.e();
            if (e != null) {
                e.a(this.f1140a.g, this.f1140a.f1131c);
            }
            if (this.f1140a.n != null) {
                this.f1140a.n.a(this.f1140a, this.f1140a.f1131c);
            }
        }
    }
}
