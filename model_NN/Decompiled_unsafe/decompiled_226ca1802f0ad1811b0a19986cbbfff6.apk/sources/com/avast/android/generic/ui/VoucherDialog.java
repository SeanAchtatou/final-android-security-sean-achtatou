package com.avast.android.generic.ui;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.avast.android.generic.aa;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.util.y;
import com.avast.android.generic.x;

public class VoucherDialog extends DialogFragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public EditText f996a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public TextView f997b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ImageView f998c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ui.VoucherDialog.a(android.support.v4.app.FragmentManager, boolean):void
     arg types: [android.support.v4.app.FragmentManager, int]
     candidates:
      com.avast.android.generic.ui.VoucherDialog.a(int, java.lang.String):void
      com.avast.android.generic.ui.VoucherDialog.a(android.support.v4.app.FragmentManager, boolean):void */
    public static void a(FragmentManager fragmentManager) {
        a(fragmentManager, false);
    }

    public static void a(FragmentManager fragmentManager, boolean z) {
        VoucherDialog voucherDialog = new VoucherDialog();
        voucherDialog.setArguments(new Bundle());
        voucherDialog.show(fragmentManager, "dialog");
    }

    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        a(r.message_voucher_canceled);
    }

    private void a(int i) {
        y yVar;
        FragmentActivity activity = getActivity();
        if (activity != null && (yVar = (y) aa.a(activity, y.class)) != null) {
            yVar.a(i);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, String str) {
        y yVar;
        FragmentActivity activity = getActivity();
        if (activity != null && (yVar = (y) aa.a(activity, y.class)) != null) {
            Message message = new Message();
            message.what = i;
            message.obj = str;
            yVar.a(message);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @TargetApi(8)
    public Dialog onCreateDialog(Bundle bundle) {
        Context c2 = ak.c(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(c2);
        builder.setTitle(getString(x.pref_voucher_title));
        View inflate = LayoutInflater.from(c2).inflate(t.dialog_voucher, (ViewGroup) null, false);
        this.f996a = (EditText) inflate.findViewById(r.voucherCode);
        if (Build.VERSION.SDK_INT >= 14) {
            this.f996a.setAllCaps(true);
        }
        this.f998c = (ImageView) inflate.findViewById(r.voucherImage);
        this.f997b = (TextView) inflate.findViewById(r.voucherErrorMessage);
        this.f996a.addTextChangedListener(a());
        ae aeVar = new ae(this);
        this.f996a.setFilters(new InputFilter[]{aeVar});
        this.f998c.setVisibility(4);
        builder.setView(inflate);
        builder.setPositiveButton(x.bR, new af(this));
        builder.setNegativeButton(x.F, new ag(this));
        AlertDialog create = builder.create();
        create.setInverseBackgroundForced(true);
        create.setOnShowListener(new ah(this, create));
        return create;
    }

    private TextWatcher a() {
        return new aj(this);
    }
}
