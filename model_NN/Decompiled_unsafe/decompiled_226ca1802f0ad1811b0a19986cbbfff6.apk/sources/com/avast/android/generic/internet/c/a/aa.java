package com.avast.android.generic.internet.c.a;

import com.actionbarsherlock.R;
import com.google.protobuf.Internal;

/* compiled from: MyAvastPairing */
public enum aa implements Internal.EnumLite {
    GENERIC(0, 0),
    COMMAND_ALREADY_DONE(1, 1),
    COMMAND_ALREADY_IN_PROGRESS(2, 2),
    COMMAND_CANNOT_BE_SENT_AGAIN_BY_SMS(3, 3),
    COMMAND_NOT_FOUND(4, 4),
    DEVICE_ALREADY_EXISTING(5, 5),
    DEVICE_HAS_NO_NUMBER(6, 6),
    DEVICE_NOT_ACTIVATED(7, 7),
    DEVICE_NOT_FOUND(8, 8),
    DEVICE_TOO_OLD(9, 9),
    EMPTY_ARGUMENT(10, 10),
    EMPTY_SELECTION(11, 11),
    INVALID_ACCESS_CODE(12, 12),
    INVALID_ACCOUNT(13, 13),
    INVALID_C2DM_ID(14, 14),
    INVALID_COMMAND_TYPE(15, 15),
    INVALID_DEVICE_INFO(16, 16),
    INVALID_DEVICE(17, 17),
    INVALID_FILE(18, 18),
    INVALID_FORWARD_CHANNEL(19, 19),
    INVALID_FORWARD_COMBINATION(20, 20),
    INVALID_FORWARD_TYPE(21, 21),
    INVALID_FRIEND_NUMNBER(22, 22),
    INVALID_GATEWAY(23, 23),
    INVALID_INTERNET_PASSWORD(24, 24),
    INVALID_LOCK_TEXT(25, 25),
    INVALID_NAME(26, 26),
    INVALID_OWNER(27, 27),
    INVALID_PHONE_NUMBER(28, 28),
    INVALID_TWO_WAY_SMS_FLAG(29, 29),
    MISSING_ARGUMENT(30, 30),
    MISSING_CLIENT_VERSION(31, 31),
    MISSING_IMEI(32, 32),
    NOT_SUPPORTED_OPERATION(33, 33),
    SETTINGS_ALREADY_RUNNING(34, 34),
    TOO_LOW_CLIENT_VERSION(35, 35);
    
    private static Internal.EnumLiteMap<aa> K = new ab();
    private final int L;

    public final int getNumber() {
        return this.L;
    }

    public static aa a(int i) {
        switch (i) {
            case 0:
                return GENERIC;
            case 1:
                return COMMAND_ALREADY_DONE;
            case 2:
                return COMMAND_ALREADY_IN_PROGRESS;
            case 3:
                return COMMAND_CANNOT_BE_SENT_AGAIN_BY_SMS;
            case 4:
                return COMMAND_NOT_FOUND;
            case 5:
                return DEVICE_ALREADY_EXISTING;
            case 6:
                return DEVICE_HAS_NO_NUMBER;
            case 7:
                return DEVICE_NOT_ACTIVATED;
            case 8:
                return DEVICE_NOT_FOUND;
            case 9:
                return DEVICE_TOO_OLD;
            case 10:
                return EMPTY_ARGUMENT;
            case 11:
                return EMPTY_SELECTION;
            case 12:
                return INVALID_ACCESS_CODE;
            case 13:
                return INVALID_ACCOUNT;
            case 14:
                return INVALID_C2DM_ID;
            case 15:
                return INVALID_COMMAND_TYPE;
            case 16:
                return INVALID_DEVICE_INFO;
            case 17:
                return INVALID_DEVICE;
            case 18:
                return INVALID_FILE;
            case R.styleable.SherlockTheme_buttonStyleSmall:
                return INVALID_FORWARD_CHANNEL;
            case R.styleable.SherlockTheme_selectableItemBackground:
                return INVALID_FORWARD_COMBINATION;
            case R.styleable.SherlockTheme_windowContentOverlay:
                return INVALID_FORWARD_TYPE;
            case R.styleable.SherlockTheme_textAppearanceLargePopupMenu:
                return INVALID_FRIEND_NUMNBER;
            case R.styleable.SherlockTheme_textAppearanceSmallPopupMenu:
                return INVALID_GATEWAY;
            case R.styleable.SherlockTheme_textAppearanceSmall:
                return INVALID_INTERNET_PASSWORD;
            case R.styleable.SherlockTheme_textColorPrimary:
                return INVALID_LOCK_TEXT;
            case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                return INVALID_NAME;
            case R.styleable.SherlockTheme_textColorPrimaryInverse:
                return INVALID_OWNER;
            case R.styleable.SherlockTheme_spinnerItemStyle:
                return INVALID_PHONE_NUMBER;
            case R.styleable.SherlockTheme_spinnerDropDownItemStyle:
                return INVALID_TWO_WAY_SMS_FLAG;
            case R.styleable.SherlockTheme_searchAutoCompleteTextView:
                return MISSING_ARGUMENT;
            case R.styleable.SherlockTheme_searchDropdownBackground:
                return MISSING_CLIENT_VERSION;
            case R.styleable.SherlockTheme_searchViewCloseIcon:
                return MISSING_IMEI;
            case R.styleable.SherlockTheme_searchViewGoIcon:
                return NOT_SUPPORTED_OPERATION;
            case R.styleable.SherlockTheme_searchViewSearchIcon:
                return SETTINGS_ALREADY_RUNNING;
            case R.styleable.SherlockTheme_searchViewVoiceIcon:
                return TOO_LOW_CLIENT_VERSION;
            default:
                return null;
        }
    }

    private aa(int i, int i2) {
        this.L = i2;
    }
}
