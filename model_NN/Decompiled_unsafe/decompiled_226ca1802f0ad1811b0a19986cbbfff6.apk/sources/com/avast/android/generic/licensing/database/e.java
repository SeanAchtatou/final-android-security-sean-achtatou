package com.avast.android.generic.licensing.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;

/* compiled from: PurchaseDatabaseHelper */
class e implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentValues f885a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f886b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ d f887c;

    e(d dVar, ContentValues contentValues, String str) {
        this.f887c = dVar;
        this.f885a = contentValues;
        this.f886b = str;
    }

    public void a(Uri uri, ContentResolver contentResolver) {
        this.f887c.f883b.update(uri, this.f885a, "order_id= ?", new String[]{this.f886b});
    }
}
