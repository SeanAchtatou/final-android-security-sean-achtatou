package com.avast.android.generic.d;

/* compiled from: Base32 */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f653a = {255, 255, 26, 27, 28, 29, 30, 31, 255, 255, 255, 255, 255, 255, 255, 255, 255, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 255, 255, 255, 255, 255, 255, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 255, 255, 255, 255, 255};

    public static byte[] a(String str) {
        int i;
        int i2 = 0;
        byte[] bArr = new byte[((str.length() * 5) / 8)];
        int i3 = 0;
        for (int i4 = 0; i4 < str.length(); i4++) {
            int charAt = str.charAt(i4) - '0';
            if (charAt >= 0 && charAt < f653a.length && (i = f653a[charAt]) != 255) {
                if (i3 > 3) {
                    i3 = (i3 + 5) % 8;
                    bArr[i2] = (byte) (bArr[i2] | (i >>> i3));
                    i2++;
                    if (i2 >= bArr.length) {
                        break;
                    }
                    bArr[i2] = (byte) ((i << (8 - i3)) | bArr[i2]);
                } else {
                    i3 = (i3 + 5) % 8;
                    if (i3 == 0) {
                        bArr[i2] = (byte) (i | bArr[i2]);
                        i2++;
                        if (i2 >= bArr.length) {
                            break;
                        }
                    } else {
                        bArr[i2] = (byte) ((i << (8 - i3)) | bArr[i2]);
                    }
                }
            }
        }
        return bArr;
    }
}
