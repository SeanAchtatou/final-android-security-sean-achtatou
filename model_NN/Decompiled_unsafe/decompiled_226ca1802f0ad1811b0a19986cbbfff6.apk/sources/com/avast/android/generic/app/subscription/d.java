package com.avast.android.generic.app.subscription;

import android.util.SparseArray;
import java.util.EnumSet;
import java.util.Iterator;

/* compiled from: ErrorDialog */
public enum d {
    CANCELED(1),
    DISMISSED_POSITIVE(2),
    DISMISSED_NEGATIVE(3),
    DISMISSED_NEUTRAL(4);
    
    private static SparseArray<d> e = new SparseArray<>();
    private int f;

    static {
        Iterator it = EnumSet.allOf(d.class).iterator();
        while (it.hasNext()) {
            d dVar = (d) it.next();
            e.put(dVar.f, dVar);
        }
    }

    private d(int i) {
        this.f = i;
    }

    public int a() {
        return this.f;
    }
}
