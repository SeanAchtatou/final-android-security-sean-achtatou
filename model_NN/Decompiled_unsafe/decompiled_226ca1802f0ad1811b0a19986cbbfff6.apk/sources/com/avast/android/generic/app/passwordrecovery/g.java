package com.avast.android.generic.app.passwordrecovery;

import android.view.View;

/* compiled from: PasswordRecoveryDialog */
class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f488a;

    g(f fVar) {
        this.f488a = fVar;
    }

    public void onClick(View view) {
        this.f488a.f487b.a(this.f488a.f487b.getActivity());
        boolean a2 = a.a(this.f488a.f487b.getActivity(), new h(this), (String) null);
        if (!a2) {
            this.f488a.f487b.a(a2);
        }
    }
}
