package com.avast.android.generic.app.account;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.avast.android.generic.ui.BaseSinglePaneActivity;

public class ConnectionCheckActivity extends BaseSinglePaneActivity {

    /* renamed from: a  reason: collision with root package name */
    private ConnectionCheckFragment f361a;

    public static void call(Context context, Bundle bundle) {
        Intent intent = new Intent(context, ConnectionCheckActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeButtonEnabled(true);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public SherlockFragment a() {
        this.f361a = new ConnectionCheckFragment();
        return this.f361a;
    }
}
