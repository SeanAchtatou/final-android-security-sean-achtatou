package com.avast.android.generic.util;

import android.content.Context;
import android.text.format.DateFormat;
import java.util.Date;

/* compiled from: TimeFormatUtils */
public class aj {
    public static String a(Context context, int i) {
        return DateFormat.getTimeFormat(context).format(new Date(0, 0, 0, i / 60, i % 60));
    }
}
