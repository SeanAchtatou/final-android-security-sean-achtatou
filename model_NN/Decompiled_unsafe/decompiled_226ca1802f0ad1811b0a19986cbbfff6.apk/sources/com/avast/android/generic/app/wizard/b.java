package com.avast.android.generic.app.wizard;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.avast.android.generic.util.g;

/* compiled from: EulaFragment */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ EulaFragment f589a;

    b(EulaFragment eulaFragment) {
        this.f589a = eulaFragment;
    }

    public void onClick(View view) {
        if (this.f589a.getActivity() != null) {
            this.f589a.f.c(g.POST_INSTALL);
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("http://www.avast.com/privacy-policy"));
            this.f589a.getActivity().startActivity(intent);
        }
    }
}
