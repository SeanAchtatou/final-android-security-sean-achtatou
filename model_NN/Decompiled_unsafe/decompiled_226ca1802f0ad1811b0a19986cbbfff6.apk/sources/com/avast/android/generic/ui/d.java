package com.avast.android.generic.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;

/* compiled from: ChangePasswordDialog */
class d implements DialogInterface.OnShowListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AlertDialog f1029a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ChangePasswordDialog f1030b;

    d(ChangePasswordDialog changePasswordDialog, AlertDialog alertDialog) {
        this.f1030b = changePasswordDialog;
        this.f1029a = alertDialog;
    }

    public void onShow(DialogInterface dialogInterface) {
        this.f1029a.getButton(-1).setOnClickListener(new e(this));
    }
}
