package com.avast.android.generic.ui.widget;

import android.os.Parcel;
import android.os.Parcelable;
import com.avast.android.generic.ui.widget.SwitchRow;

/* compiled from: SwitchRow */
final class ae implements Parcelable.Creator<SwitchRow.SavedState> {
    ae() {
    }

    /* renamed from: a */
    public SwitchRow.SavedState createFromParcel(Parcel parcel) {
        return new SwitchRow.SavedState(parcel, null);
    }

    /* renamed from: a */
    public SwitchRow.SavedState[] newArray(int i) {
        return new SwitchRow.SavedState[i];
    }
}
