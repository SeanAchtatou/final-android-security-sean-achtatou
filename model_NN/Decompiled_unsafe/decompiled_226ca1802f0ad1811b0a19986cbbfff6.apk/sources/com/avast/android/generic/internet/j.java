package com.avast.android.generic.internet;

import com.avast.android.generic.ab;
import com.avast.android.generic.service.AvastService;
import com.avast.android.generic.util.aa;
import com.avast.android.generic.util.v;
import com.avast.android.generic.util.z;
import com.avast.b.a.a.a.m;

/* compiled from: InetSms */
public abstract class j {
    public static String a(AvastService avastService, ab abVar, String str, String str2, String str3, m mVar) {
        if (str3 != null) {
            try {
                if (!str3.equals("")) {
                    str3 = v.a(str3, true, true);
                }
            } catch (Exception e) {
                z.a("AvastComms", avastService, "InetSMS, Can not encode error message", e);
            }
        }
        return (((((str + " ") + abVar.w() + " E ") + aa.i(avastService) + " ") + str2 + " ") + mVar.ordinal()) + " " + (str3 != null ? " " + str3 : "");
    }

    public static String a(AvastService avastService, ab abVar, String str, String str2, boolean z) {
        String str3;
        if (str2 != null) {
            try {
                str2 = v.a(str2, true, true);
            } catch (Exception e) {
                z.a("AvastComms", avastService, "InetSMS, Can not encode success message", e);
            }
        }
        String str4 = abVar.y() + " ";
        if (z) {
            str3 = str4 + abVar.w() + " P ";
        } else {
            str3 = str4 + abVar.w() + " S ";
        }
        return str3 + str + " " + aa.i(avastService) + " " + (str2 != null ? " " + str2 : "");
    }
}
