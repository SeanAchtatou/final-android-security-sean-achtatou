package com.avast.android.generic.app.about;

import android.os.AsyncTask;
import com.avast.android.generic.h.c;
import com.avast.android.generic.h.q;
import com.avast.android.generic.util.t;

/* compiled from: FeedbackFragment */
class n extends AsyncTask<Void, Void, Boolean> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FeedbackFragment f352a;

    private n(FeedbackFragment feedbackFragment) {
        this.f352a = feedbackFragment;
    }

    /* synthetic */ n(FeedbackFragment feedbackFragment, f fVar) {
        this(feedbackFragment);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.f352a.a(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        q qVar;
        c cVar;
        byte[] bArr = null;
        o oVar = new o(this.f352a.getActivity());
        switch (this.f352a.f330b) {
            case 2:
                qVar = q.BUG_REPORT;
                break;
            case 3:
                qVar = q.CRASH_REPORT;
                break;
            case 4:
                qVar = q.FEATURE_REQUEST;
                break;
            default:
                qVar = q.CUSTOM_FEEDBACK;
                break;
        }
        try {
            String obj = this.f352a.d.getText().toString();
            String obj2 = this.f352a.g.getText().toString();
            String obj3 = this.f352a.h.getText().toString();
            if (this.f352a.l.c()) {
                cVar = this.f352a.s;
            } else {
                cVar = null;
            }
            byte[] a2 = this.f352a.m.c() ? this.f352a.q : null;
            if (this.f352a.n.c()) {
                bArr = this.f352a.r;
            }
            return Boolean.valueOf(oVar.a(obj, obj2, obj3, qVar, cVar, a2, bArr));
        } catch (p e) {
            t.c("FeedbackFragment", "Sending feedback failed.", e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        t.b("FeedbackFragment", "Feedback sending result: " + bool);
        if (this.f352a.isAdded()) {
            this.f352a.h();
            this.f352a.a(bool.booleanValue());
        }
    }
}
