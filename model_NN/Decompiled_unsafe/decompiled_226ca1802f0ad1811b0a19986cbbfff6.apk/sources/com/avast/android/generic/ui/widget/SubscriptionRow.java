package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.avast.android.generic.licensing.l;
import com.avast.android.generic.licensing.m;
import com.avast.android.generic.n;
import com.avast.android.generic.o;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.x;
import com.avast.android.generic.y;
import com.avast.android.generic.z;

public class SubscriptionRow extends Row implements l {

    /* renamed from: a  reason: collision with root package name */
    private m f1119a;

    /* renamed from: b  reason: collision with root package name */
    private TextView f1120b;

    /* renamed from: c  reason: collision with root package name */
    private View f1121c;
    private ImageView m;
    private int n;
    private long o = 0;

    public SubscriptionRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, context.obtainStyledAttributes(attributeSet, z.g, n.rowStyle, y.Row));
    }

    public SubscriptionRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, context.obtainStyledAttributes(attributeSet, z.g, i, y.Row));
    }

    /* access modifiers changed from: protected */
    public void a(Context context, TypedArray typedArray) {
        this.n = typedArray.getResourceId(3, 0);
        typedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void a() {
        inflate(getContext(), t.row_subscription, this);
        this.f1120b = (TextView) findViewById(r.row_subscription_state);
        this.f1121c = findViewById(r.row_subscription_progress);
        this.m = (ImageView) findViewById(r.row_icon);
        if (this.n != 0) {
            this.m.setVisibility(0);
            this.m.setImageResource(this.n);
        }
    }

    public void a(m mVar, String str) {
        this.f1119a = mVar;
        switch (aa.f1133a[mVar.ordinal()]) {
            case 1:
                this.f1120b.setText(x.l_subscription_valid);
                this.f1120b.setTextColor(getContext().getResources().getColor(o.subscription_green));
                break;
            case 2:
                this.f1120b.setText(x.l_subscription_unknown);
                this.f1120b.setTextColor(getContext().getResources().getColor(o.subscription_grey));
                break;
            case 3:
                this.f1120b.setText(x.l_subscription_not_available);
                this.f1120b.setTextColor(getContext().getResources().getColor(o.subscription_grey));
                break;
            case 4:
                b(true);
                setEnabled(false);
                break;
            case 5:
                this.f1120b.setText(x.l_subscription_none);
                this.f1120b.setTextColor(getContext().getResources().getColor(o.subscription_grey));
                break;
        }
        if (mVar != m.PROGRESS) {
            b(false);
            setEnabled(true);
        }
        c();
    }

    private void b(boolean z) {
        if (z) {
            this.f1120b.setVisibility(8);
            this.f1121c.setVisibility(0);
            return;
        }
        this.f1120b.setVisibility(0);
        this.f1121c.setVisibility(8);
    }

    public void a(long j) {
        this.o = j;
        c();
    }

    private void c() {
        if (this.f1119a.equals(m.VALID)) {
            if (this.o == -1) {
                c(getContext().getString(x.l_lifetime_license));
                return;
            }
            String a2 = com.avast.android.generic.util.r.a(getContext(), this.o, 65556);
            c(getContext().getString(x.l_subscription_renewed_on, a2));
        } else if (this.f1119a.equals(m.UNKNOWN)) {
            c(getContext().getString(x.l_subscription_subtitle_tap_to_refresh));
        } else {
            c("");
        }
    }

    public void a(boolean z) {
    }

    public void a(String str) {
    }
}
