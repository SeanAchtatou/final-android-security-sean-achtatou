package com.avast.android.generic.licensing;

import a.a.a.a.a.a;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.licensing.c.b;
import com.avast.android.generic.licensing.c.k;
import com.avast.android.generic.util.z;
import com.avast.c.a.a.q;
import com.avast.c.a.a.t;
import com.avast.c.a.a.v;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;

/* compiled from: OfferTask */
public class c extends AsyncTask<Void, g<?>, Boolean> {

    /* renamed from: a  reason: collision with root package name */
    private LicensingBroadcastReceiver f853a = null;

    /* renamed from: b  reason: collision with root package name */
    private b f854b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Semaphore f855c;
    private Context d;
    private f e;
    private ab f;
    /* access modifiers changed from: private */
    public b g;
    /* access modifiers changed from: private */
    public Boolean h;
    private List<k> i;
    private v j;
    private String k;
    private Set<com.avast.android.generic.licensing.a.b> l;
    private String m;
    private Uri n;

    public List<k> a() {
        return this.i;
    }

    public v b() {
        return this.j;
    }

    public String c() {
        return this.k;
    }

    public c(Context context, Uri uri) {
        this.d = context;
        this.f855c = new Semaphore(0);
        this.f = (ab) aa.a(context, ab.class);
        this.j = null;
        this.k = null;
        this.n = uri;
    }

    public void a(String str) {
        this.m = str;
    }

    public void a(f fVar) {
        this.e = fVar;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        z.a("AvastGenericLic", "Init started.");
        this.f854b = new b(this.f855c);
        this.f853a = new LicensingBroadcastReceiver(this.d, null, this.f854b, true, this.n);
        this.e.a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        z.a("AvastGenericLic", "Init finished with result: " + bool);
        k();
        if (bool.booleanValue()) {
            this.e.c();
        } else {
            this.e.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        k();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        try {
            if (!e() || isCancelled()) {
                return false;
            }
            if (!h() || isCancelled()) {
                return false;
            }
            if (!i() || isCancelled()) {
                return false;
            }
            if (!j() || isCancelled()) {
                return false;
            }
            if (!d() || isCancelled()) {
                return false;
            }
            if (!l() || isCancelled()) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            a.a().a("Issue in offer task", e2);
            publishProgress(new g(h.CAN_NOT_RETRIEVE_SERVER_CONFIG, e2));
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    /* access modifiers changed from: protected */
    public boolean d() {
        if (this.f.b("firstRunCommitted", false) || this.f.R() > 0) {
            return true;
        }
        this.d.registerReceiver(this.f853a, new IntentFilter("com.avast.android.generic.ACTION_LICENSING_UPDATE"));
        try {
            PurchaseConfirmationService.a(this.d);
            try {
                this.f855c.acquire();
                if (isCancelled()) {
                    try {
                        return false;
                    } catch (Exception e2) {
                        return false;
                    }
                } else {
                    try {
                        this.d.unregisterReceiver(this.f853a);
                    } catch (Exception e3) {
                    }
                    if (isCancelled()) {
                        return false;
                    }
                    if (this.f854b.a() != null && this.f854b.a() != m.NOT_AVAILABLE && this.f854b.a() != m.UNKNOWN && this.f854b.a() != m.VALID) {
                        return true;
                    }
                    publishProgress(new g(h.LICENSE_ALREADY_AVAILABLE, this.f854b));
                    return false;
                }
            } catch (InterruptedException e4) {
                try {
                    this.d.unregisterReceiver(this.f853a);
                    return false;
                } catch (Exception e5) {
                    return false;
                }
            }
        } finally {
            try {
                this.d.unregisterReceiver(this.f853a);
            } catch (Exception e6) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        boolean a2 = com.avast.android.generic.internet.b.a(this.d);
        g[] gVarArr = new g[1];
        gVarArr[0] = a2 ? new g(h.CONNECTION_STATUS_CONNECTED) : new g(h.CONNECTION_STATUS_NOT_CONNECTED);
        publishProgress(gVarArr);
        return a2;
    }

    private boolean h() {
        int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.d);
        if (isGooglePlayServicesAvailable == 0) {
            return true;
        }
        publishProgress(new g(h.GOOGLE_PLAY_SERVICES_NOT_AVAILABLE, Integer.valueOf(isGooglePlayServicesAvailable)));
        return false;
    }

    private boolean i() {
        try {
            this.l = com.avast.android.generic.licensing.a.a.a(this.d);
            if (this.l != null && !this.l.isEmpty()) {
                return true;
            }
            publishProgress(new g(h.NO_GOOGLE_ACCOUNT));
            return false;
        } catch (GooglePlayServicesAvailabilityException e2) {
            z.a("AvastGenericLic", "Can not get auth token", e2);
            publishProgress(new g(h.CAN_NOT_VALIDATE_GOOGLE_ACCOUNTS, Integer.valueOf(e2.getConnectionStatusCode())));
            return false;
        } catch (UserRecoverableAuthException e3) {
            z.a("AvastGenericLic", "Can not get auth token", e3);
            try {
                publishProgress(new g(h.CAN_NOT_VALIDATE_GOOGLE_ACCOUNTS_INTENT, e3.getIntent()));
            } catch (NullPointerException e4) {
                publishProgress(new g(h.CAN_NOT_VALIDATE_GOOGLE_ACCOUNTS_GENERIC));
            }
            return false;
        } catch (IOException e5) {
            z.a("AvastGenericLic", "Can not get auth token", e5);
            publishProgress(new g(h.GOOGLE_ACCOUNT_VALIDATION_ERROR_RETRY));
            return false;
        } catch (GoogleAuthException e6) {
            z.a("AvastGenericLic", "Can not get auth token", e6);
            publishProgress(new g(h.GOOGLE_ACCOUNT_NOT_RECOVERABLE));
            return false;
        } catch (Exception e7) {
            z.a("AvastGenericLic", "Can not get auth token", e7);
            publishProgress(new g(h.GOOGLE_ACCOUNT_VALIDATION_ERROR_RETRY));
            return false;
        }
    }

    private boolean j() {
        z.a("AvastGenericLic", "Binding billing service.");
        this.g = new b(this.d);
        this.g.a(new d(this));
        try {
            this.f855c.acquire();
        } catch (InterruptedException e2) {
            if (this.h == null) {
                return false;
            }
        }
        if (this.h != null && this.h.booleanValue()) {
            return true;
        }
        publishProgress(new g(h.BILLING_UNAVAILABLE));
        return false;
    }

    private void k() {
        if (this.g != null) {
            this.g.a();
        }
    }

    private boolean l() {
        String str = null;
        z.a("AvastGenericLic", "Retrieving offers.");
        z.a("AvastGenericLic", "Getting offers.");
        try {
            t a2 = com.avast.android.generic.internet.b.a(this.d, this.f, this.m);
            this.j = a2.d() ? a2.e() : null;
            if (a2.f()) {
                str = a2.g();
            }
            this.k = str;
            List<q> b2 = a2.b();
            if (b2 == null || b2.isEmpty()) {
                this.i = new LinkedList();
                return true;
            }
            this.i = this.g.a(b2);
            publishProgress(new g(h.SERVER_CONFIG_RETRIEVED));
            return true;
        } catch (Exception e2) {
            z.a("AvastGenericLic", "Can not get offers", e2);
            publishProgress(new g(h.CAN_NOT_RETRIEVE_SERVER_CONFIG, e2));
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(g<?>... gVarArr) {
        switch (e.f899a[gVarArr[0].f900a.ordinal()]) {
            case 1:
                this.e.d();
                return;
            case 2:
                this.e.a(true);
                return;
            case 3:
                this.e.a(false);
                return;
            case 4:
                this.e.a((Exception) gVarArr[0].a());
                return;
            case 5:
                this.e.f();
                return;
            case 6:
                this.e.a(((Integer) gVarArr[0].a()).intValue());
                return;
            case 7:
                this.e.b(((Integer) gVarArr[0].a()).intValue());
                return;
            case 8:
                this.e.g();
                return;
            case 9:
                this.e.a((Intent) gVarArr[0].a());
                return;
            case 10:
                this.e.e();
                return;
            case 11:
                this.e.a((b) gVarArr[0].a());
                return;
            case 12:
                this.e.h();
                return;
            case 13:
                this.e.i();
                return;
            default:
                return;
        }
    }

    public Set<com.avast.android.generic.licensing.a.b> f() {
        return this.l;
    }

    public void g() {
        try {
            if (this.l != null && !this.l.isEmpty()) {
                com.avast.android.generic.licensing.a.a.a(this.d, this.l);
                this.l = null;
            }
        } catch (Exception e2) {
            z.a("AvastGenericLic", "Can not invalidate auth tokens", e2);
        }
    }
}
