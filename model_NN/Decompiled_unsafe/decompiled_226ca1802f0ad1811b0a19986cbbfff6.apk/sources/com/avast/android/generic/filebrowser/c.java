package com.avast.android.generic.filebrowser;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

/* compiled from: AbstractFileBrowserFragment */
class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AbstractFileBrowserFragment f676a;

    c(AbstractFileBrowserFragment abstractFileBrowserFragment) {
        this.f676a = abstractFileBrowserFragment;
    }

    public void onClick(View view) {
        Intent intent = new Intent();
        intent.setData(Uri.parse(this.f676a.l + this.f676a.g.a()));
        this.f676a.getActivity().setResult(-1, intent);
        this.f676a.getActivity().finish();
    }
}
