package com.avast.android.generic.licensing.c;

/* compiled from: Purchase */
public enum m {
    PURCHASED,
    CANCELED,
    REFUNDED;

    public static m a(int i) {
        m[] values = values();
        if (i < 0 || i >= values.length) {
            return CANCELED;
        }
        return values[i];
    }
}
