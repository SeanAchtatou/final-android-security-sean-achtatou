package com.avast.android.generic.d;

import java.security.MessageDigest;
import java.security.PrivateKey;
import javax.crypto.Cipher;

/* compiled from: Signature */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f663a = {48, 33};

    /* renamed from: b  reason: collision with root package name */
    private byte[] f664b = {48, 9, 6, 5, 43, 14, 3, 2, 26, 5, 0};

    /* renamed from: c  reason: collision with root package name */
    private byte[] f665c = {4, 20};
    private Cipher d = Cipher.getInstance("RSA/ECB/PKCS1Padding");
    private MessageDigest e = MessageDigest.getInstance("SHA1");

    public void a(PrivateKey privateKey) {
        this.d.init(1, privateKey);
    }

    public void a(byte[] bArr) {
        this.e.update(bArr);
    }

    public byte[] a() {
        this.d.update(this.f663a);
        this.d.update(this.f664b);
        this.d.update(this.f665c);
        this.d.update(this.e.digest());
        return this.d.doFinal();
    }
}
