package com.avast.android.generic.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import com.avast.android.generic.r;
import com.avast.android.generic.t;

public abstract class BaseSinglePaneActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private Fragment f982a;

    /* access modifiers changed from: protected */
    public abstract Fragment a();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(t.activity_singlepane_empty);
        if (bundle == null) {
            this.f982a = a();
            Bundle arguments = this.f982a.getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            arguments.putAll(c(getIntent()));
            this.f982a.setArguments(arguments);
            FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
            beginTransaction.add(r.root_container, this.f982a, "singleFragment");
            beginTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            beginTransaction.commit();
        }
    }
}
