package com.avast.android.generic.app.account;

import android.util.Log;
import com.facebook.bg;
import com.facebook.bu;
import com.facebook.bz;

/* compiled from: FacebookUserAuthenticator */
class bi implements bu {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ be f420a;

    private bi(be beVar) {
        this.f420a = beVar;
    }

    public void a(bg bgVar, bz bzVar, Exception exc) {
        Log.v("FacebookUserAuthenticator", String.format("SessionStatusCallback(session=%s,state=%s,exception=%s)", bgVar, bzVar, exc));
        if (bzVar.a()) {
            if (bgVar.d() != null) {
                this.f420a.b(bgVar);
            }
        } else if (bzVar == bz.CLOSED_LOGIN_FAILED) {
            this.f420a.a(bgVar);
        }
    }
}
