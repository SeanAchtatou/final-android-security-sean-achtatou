package com.avast.android.generic.c;

import com.avast.android.generic.c.a.a;
import com.avast.android.generic.c.a.b;
import com.avast.android.generic.c.a.c;
import com.avast.android.generic.util.v;

/* compiled from: CommandParser */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private String f632a;

    /* renamed from: b  reason: collision with root package name */
    private int f633b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f634c = false;

    public h(String str) {
        String trim = (str == null ? "" : str).trim();
        int indexOf = trim.indexOf("##");
        this.f632a = indexOf > -1 ? trim.substring(0, indexOf).trim() : trim;
    }

    public String a(j jVar) {
        return a(jVar, false, true);
    }

    public String b(j jVar) {
        return a(jVar, true, false);
    }

    public String c(j jVar) {
        return a(jVar, false, false);
    }

    private String a(j jVar, boolean z, boolean z2) {
        String d;
        switch (i.f635a[jVar.ordinal()]) {
            case 1:
                d = b(z);
                break;
            case 2:
                d = a(z);
                break;
            case 3:
                d = c(z);
                break;
            case 4:
                d = d(z);
                break;
            default:
                return null;
        }
        if (this.f634c) {
            d = v.b(d, false, false);
        }
        if (!z2) {
            return d.toUpperCase();
        }
        return d;
    }

    private String a(boolean z) {
        boolean z2 = true;
        String str = "";
        if (this.f633b >= this.f632a.length()) {
            throw new a();
        }
        int i = this.f633b;
        boolean z3 = true;
        while (true) {
            if (i >= this.f632a.length()) {
                z2 = false;
                break;
            }
            char charAt = this.f632a.charAt(i);
            if (charAt != ' ' || !z3) {
                if (charAt == ' ') {
                    if (!z) {
                        this.f633b = i + 1;
                    }
                } else if (charAt == '1' || charAt == '2' || charAt == '3' || charAt == '4' || charAt == '5' || charAt == '6' || charAt == '7' || charAt == '8' || charAt == '9' || charAt == '0') {
                    str = str + charAt;
                    z3 = false;
                } else {
                    throw new b();
                }
            }
            i++;
        }
        if (!z2 && !z) {
            this.f633b = this.f632a.length();
        }
        if (!str.equals("")) {
            return str;
        }
        throw new c();
    }

    private String b(boolean z) {
        if (this.f633b >= this.f632a.length()) {
            throw new a();
        }
        String valueOf = String.valueOf(this.f632a.charAt(this.f633b));
        if (!z) {
            this.f633b++;
        }
        return valueOf;
    }

    private String c(boolean z) {
        String substring;
        if (this.f633b >= this.f632a.length()) {
            throw new a();
        }
        int i = this.f633b;
        int i2 = this.f633b;
        while (true) {
            if (i2 < this.f632a.length()) {
                if (this.f632a.charAt(i2) != ' ') {
                    break;
                }
                i2++;
            } else {
                i2 = i;
                break;
            }
        }
        if (i2 >= this.f632a.length()) {
            throw new a();
        }
        int indexOf = this.f632a.indexOf(32, i2);
        if (indexOf == -1) {
            substring = this.f632a.substring(i2);
            if (!z) {
                this.f633b = this.f632a.length();
            }
        } else {
            substring = this.f632a.substring(i2, indexOf);
            if (!z) {
                this.f633b = indexOf + 1;
            }
        }
        if (!substring.equals("")) {
            return substring;
        }
        throw new c();
    }

    private String d(boolean z) {
        if (this.f634c) {
            return c(z);
        }
        if (this.f633b >= this.f632a.length()) {
            throw new a();
        }
        String substring = this.f632a.substring(this.f633b);
        if (!z) {
            this.f633b = this.f632a.length();
        }
        String trim = substring.trim();
        if (!trim.equals("")) {
            return trim;
        }
        throw new c();
    }
}
