package com.avast.android.generic.app.subscription;

import android.content.DialogInterface;
import com.avast.android.generic.licensing.c.k;
import com.avast.c.a.a.ai;

/* compiled from: SubscriptionFragment */
class n implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f562a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f563b;

    n(SubscriptionFragment subscriptionFragment, k kVar) {
        this.f563b = subscriptionFragment;
        this.f562a = kVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        ai unused = this.f563b.A = (ai) this.f563b.G.get(i);
        this.f563b.d(this.f562a);
    }
}
