package com.avast.android.generic.app.account;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import com.avast.android.generic.x;
import com.facebook.al;
import com.facebook.bg;
import com.facebook.br;
import com.facebook.bs;
import com.facebook.bu;
import com.facebook.cb;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/* compiled from: FacebookUserAuthenticator */
abstract class be implements bp<bg, bh> {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f413a = false;

    /* renamed from: b  reason: collision with root package name */
    private bs f414b;

    /* renamed from: c  reason: collision with root package name */
    private bu f415c = new bi(this);
    private Fragment d;

    @SuppressLint({"NewApi"})
    be(Fragment fragment) {
        if (!f413a) {
            cb.a(al.INCLUDE_ACCESS_TOKENS);
            try {
                for (Signature byteArray : fragment.getActivity().getPackageManager().getPackageInfo(fragment.getActivity().getPackageName(), 64).signatures) {
                    MessageDigest instance = MessageDigest.getInstance("SHA");
                    instance.update(byteArray.toByteArray());
                    if (Build.VERSION.SDK_INT >= 8) {
                        Log.d("FacebookUserAuthenticator", "Key hash: " + Base64.encodeToString(instance.digest(), 0));
                    }
                }
            } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            }
            f413a = true;
        }
        this.d = fragment;
        this.f414b = new bs(fragment);
        this.f414b.a(Arrays.asList("email"));
        this.f414b.a(this.f415c);
    }

    public void a() {
        if (this.d.isAdded()) {
            bg i = bg.i();
            if (i == null || !i.a() || i.d() == null) {
                bg a2 = new br(this.d.getActivity()).a(this.d.getActivity().getString(x.applicationId)).a();
                bg.a(a2);
                bg.i().a(this.f415c);
                Log.v("FacebookUserAuthenticator", "Started listening on session changes");
                a2.a(this.f414b);
                return;
            }
            b(i);
        }
    }

    public void b() {
        if (bg.i() != null) {
            bg.i().a(this.f415c);
            Log.v("FacebookUserAuthenticator", "Started listening on session changes");
        }
    }

    public void c() {
        if (bg.i() != null) {
            bg.i().b(this.f415c);
            Log.v("FacebookUserAuthenticator", "Stopped listening on session changes");
        }
    }

    /* access modifiers changed from: private */
    public void a(bg bgVar) {
        bgVar.h();
        b(bh.UNKNOWN_ERROR);
    }

    /* access modifiers changed from: private */
    public void b(bg bgVar) {
        String d2 = bgVar.d();
        long time = bgVar.e().getTime();
        bgVar.h();
        a(new bg(d2, time));
    }

    public void a(int i, int i2, Intent intent) {
        if (this.d.isAdded()) {
            Log.v("FacebookUserAuthenticator", "onActivityResult");
            bg.i().a(this.d.getActivity(), i, i2, intent);
        }
    }

    public boolean b(int i, int i2, Intent intent) {
        return false;
    }
}
