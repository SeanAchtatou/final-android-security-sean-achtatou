package com.avast.android.generic.licensing;

/* compiled from: SubscriptionIndicator */
public enum m {
    VALID,
    UNKNOWN,
    PROGRESS,
    NOT_AVAILABLE,
    NONE
}
