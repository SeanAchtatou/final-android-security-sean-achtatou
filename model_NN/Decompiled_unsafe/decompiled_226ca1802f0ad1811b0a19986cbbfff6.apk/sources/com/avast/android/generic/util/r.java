package com.avast.android.generic.util;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.text.format.Time;
import com.avast.android.generic.x;
import java.util.Formatter;
import java.util.Locale;

/* compiled from: DateUtils */
public class r {
    public static String a(Context context, long j, int i) {
        return a(context, j, j, i);
    }

    public static String a(Context context, long j, long j2, int i) {
        return a(context, new Formatter(new StringBuilder(50), Locale.getDefault()), j, j2, i).toString();
    }

    public static Formatter a(Context context, Formatter formatter, long j, long j2, int i) {
        return a(context, formatter, j, j2, i, null);
    }

    public static Formatter a(Context context, Formatter formatter, long j, long j2, int i, String str) {
        Time time;
        Time time2;
        Time time3;
        int julianDay;
        String str2;
        boolean z;
        String string;
        String string2;
        String str3;
        String format;
        String str4;
        String str5;
        boolean is24HourFormat;
        String string3;
        String str6;
        String format2;
        String str7;
        Resources resources = context.getResources();
        boolean z2 = (i & 1) != 0;
        boolean z3 = (i & 2) != 0;
        boolean z4 = (i & 4) != 0;
        boolean z5 = (i & 8) != 0;
        boolean z6 = (i & FragmentTransaction.TRANSIT_EXIT_MASK) != 0;
        boolean z7 = (557056 & i) != 0;
        boolean z8 = (589824 & i) != 0;
        boolean z9 = (i & 32) != 0;
        boolean z10 = (131072 & i) != 0;
        boolean z11 = j == j2;
        if (str != null) {
            time = new Time(str);
        } else if (z6) {
            time = new Time("UTC");
        } else {
            time = new Time();
        }
        time.set(j);
        if (z11) {
            julianDay = 0;
            time3 = time;
        } else {
            if (str != null) {
                time2 = new Time(str);
            } else if (z6) {
                time2 = new Time("UTC");
            } else {
                time2 = new Time();
            }
            time2.set(j2);
            time3 = time2;
            julianDay = Time.getJulianDay(j2, time2.gmtoff) - Time.getJulianDay(j, time.gmtoff);
        }
        if (!z11 && (time3.hour | time3.minute | time3.second) == 0 && (!z2 || julianDay <= 1)) {
            time3.monthDay--;
            time3.normalize(true);
        }
        int i2 = time.monthDay;
        int i3 = time.month;
        int i4 = time.year;
        int i5 = time3.monthDay;
        int i6 = time3.month;
        int i7 = time3.year;
        String str8 = "";
        String str9 = "";
        if (z3) {
            if (z7) {
                str7 = "%a";
            } else {
                str7 = "%A";
            }
            str8 = time.format(str7);
            str9 = z11 ? str8 : time3.format(str7);
        }
        String str10 = "";
        if (z2) {
            String str11 = "";
            boolean z12 = (i & NotificationCompat.FLAG_HIGH_PRIORITY) != 0;
            boolean z13 = (i & 64) != 0;
            if (z12) {
                is24HourFormat = true;
            } else if (z13) {
                is24HourFormat = false;
            } else {
                is24HourFormat = DateFormat.is24HourFormat(context);
            }
            if (is24HourFormat) {
                str11 = resources.getString(x.hour_minute_24);
                str6 = str11;
            } else {
                boolean z14 = (540672 & i) != 0;
                boolean z15 = (i & 256) != 0;
                boolean z16 = (i & 512) != 0;
                boolean z17 = (i & 1024) != 0;
                boolean z18 = (i & 2048) != 0;
                boolean z19 = (i & FragmentTransaction.TRANSIT_ENTER_MASK) != 0;
                boolean z20 = time.minute == 0 && time.second == 0;
                boolean z21 = time3.minute == 0 && time3.second == 0;
                if (!z14 || !z20) {
                    if (z15) {
                        string3 = resources.getString(x.hour_minute_cap_ampm);
                    } else {
                        string3 = resources.getString(x.hour_minute_ampm);
                    }
                } else if (z15) {
                    string3 = resources.getString(x.hour_cap_ampm);
                } else {
                    string3 = resources.getString(x.hour_ampm);
                }
                if (!z11) {
                    if (!z14 || !z21) {
                        if (z15) {
                            str11 = resources.getString(x.hour_minute_cap_ampm);
                        } else {
                            str11 = resources.getString(x.hour_minute_ampm);
                        }
                    } else if (z15) {
                        str11 = resources.getString(x.hour_cap_ampm);
                    } else {
                        str11 = resources.getString(x.hour_ampm);
                    }
                    if (time3.hour != 12 || !z21 || z16) {
                        if (time3.hour == 0 && z21 && !z18) {
                            if (z19) {
                                str11 = resources.getString(x.Midnight);
                            } else {
                                str11 = resources.getString(x.midnight);
                            }
                        }
                    } else if (z17) {
                        str11 = resources.getString(x.Noon);
                    } else {
                        str11 = resources.getString(x.noon);
                    }
                }
                if (time.hour != 12 || !z20 || z16) {
                    str6 = string3;
                } else if (z17) {
                    str6 = resources.getString(x.Noon);
                } else {
                    str6 = resources.getString(x.noon);
                }
            }
            str10 = time.format(str6);
            if (z11) {
                format2 = str10;
            } else {
                format2 = time3.format(str11);
            }
            str2 = format2;
        } else {
            str2 = "";
        }
        if (z4) {
            z = z4;
        } else if (z5) {
            z = false;
        } else if (i4 != i7) {
            z = true;
        } else {
            Time time4 = new Time();
            time4.setToNow();
            z = i4 != time4.year;
        }
        if (z10) {
            string = resources.getString(x.numeric_date);
        } else if (z) {
            if (z8) {
                if (z9) {
                    string = resources.getString(x.abbrev_month_year);
                } else {
                    string = resources.getString(x.abbrev_month_day_year);
                }
            } else if (z9) {
                string = resources.getString(x.month_year);
            } else {
                string = resources.getString(x.month_day_year);
            }
        } else if (z8) {
            if (z9) {
                string = resources.getString(x.abbrev_month);
            } else {
                string = resources.getString(x.abbrev_month_day);
            }
        } else if (z9) {
            string = resources.getString(x.month);
        } else {
            string = resources.getString(x.month_day);
        }
        if (z3) {
            if (z2) {
                string2 = resources.getString(x.wday1_date1_time1_wday2_date2_time2);
            } else {
                string2 = resources.getString(x.wday1_date1_wday2_date2);
            }
        } else if (z2) {
            string2 = resources.getString(x.date1_time1_date2_time2);
        } else {
            string2 = resources.getString(x.date1_date2);
        }
        if (z9 && i3 == i6) {
            return formatter.format("%s", time.format(string));
        } else if (i4 != i7 || z9) {
            return formatter.format(string2, str8, time.format(string), str10, str9, time3.format(string), str2);
        } else {
            if (z10) {
                str3 = "%m";
            } else if (z8) {
                str3 = resources.getString(x.short_format_month);
            } else {
                str3 = "%B";
            }
            String format3 = time.format(str3);
            String format4 = time.format("%-d");
            String format5 = time.format("%Y");
            String format6 = z11 ? null : time3.format(str3);
            String format7 = z11 ? null : time3.format("%-d");
            if (z11) {
                format = null;
            } else {
                format = time3.format("%Y");
            }
            if (i3 != i6) {
                int i8 = 0;
                if (z3) {
                    i8 = 1;
                }
                if (z) {
                    i8 += 2;
                }
                if (z2) {
                    i8 += 4;
                }
                if (z10) {
                    i8 += 8;
                }
                return formatter.format(resources.getString(DateUtils.sameYearTable[i8]), str8, format3, format4, format5, str10, str9, format6, format7, format, str2);
            } else if (i2 != i5) {
                int i9 = 0;
                if (z3) {
                    i9 = 1;
                }
                if (z) {
                    i9 += 2;
                }
                if (z2) {
                    i9 += 4;
                }
                if (z10) {
                    i9 += 8;
                }
                return formatter.format(resources.getString(DateUtils.sameMonthTable[i9]), str8, format3, format4, format5, str10, str9, format6, format7, format, str2);
            } else {
                boolean z22 = (i & 16) != 0;
                if (!z2 && !z22 && !z3) {
                    z22 = true;
                }
                String str12 = "";
                if (z2) {
                    if (z11) {
                        str12 = str10;
                    } else {
                        str12 = String.format(resources.getString(x.time1_time2), str10, str2);
                    }
                }
                if (z22) {
                    str4 = time.format(string);
                    if (z3) {
                        if (z2) {
                            str5 = resources.getString(x.time_wday_date);
                        } else {
                            str5 = resources.getString(x.wday_date);
                        }
                    } else if (z2) {
                        str5 = resources.getString(x.time_date);
                    } else {
                        return formatter.format("%s", str4);
                    }
                } else if (z3) {
                    if (z2) {
                        str5 = resources.getString(x.time_wday);
                        str4 = "";
                    } else {
                        return formatter.format("%s", str8);
                    }
                } else if (z2) {
                    return formatter.format("%s", str12);
                } else {
                    str4 = "";
                    str5 = "";
                }
                return formatter.format(str5, str12, str8, str4);
            }
        }
    }
}
