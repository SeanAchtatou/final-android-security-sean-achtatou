package com.avast.android.generic.util.ga;

import com.google.analytics.tracking.android.Tracker;
import com.google.analytics.tracking.android.Transaction;

/* compiled from: EasyTrackerAdapter */
public class c {

    /* renamed from: a  reason: collision with root package name */
    Tracker f1233a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f1234b;

    private c(a aVar, Tracker tracker) {
        this.f1234b = aVar;
        this.f1233a = tracker;
    }

    public void a(String str, String str2, String str3, Long l) {
        if (this.f1234b.f1232c) {
            this.f1233a.sendEvent(str, str2, str3, l);
        }
    }

    public void a(String str, Throwable th, boolean z) {
        if (this.f1234b.f1232c) {
            this.f1233a.sendException(str, th, z);
        }
    }

    public void a(Transaction transaction) {
        if (this.f1234b.f1232c) {
            this.f1233a.sendTransaction(transaction);
        }
    }

    public void a(String str) {
        if (this.f1234b.f1232c) {
            this.f1233a.sendView(str);
        }
    }
}
