package com.avast.android.generic.app.passwordrecovery;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.text.TextUtils;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.util.ae;
import com.avast.android.generic.x;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

/* compiled from: PasswordRecovery */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static b f477a = b.UNINITIATED;

    /* renamed from: b  reason: collision with root package name */
    private static j f478b = null;

    /* renamed from: c  reason: collision with root package name */
    private static final Object f479c = new Object();
    private static final Object d = new Object();

    public static boolean a(Context context) {
        try {
            if (com.avast.android.generic.g.b.a.k(context) && com.avast.android.generic.g.b.a.d(context) == 5) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r8, com.avast.android.generic.app.passwordrecovery.j r9, java.lang.String r10) {
        /*
            r1 = 0
            boolean r0 = d(r8)     // Catch:{ Exception -> 0x0081 }
            if (r0 == 0) goto L_0x0009
            r0 = r1
        L_0x0008:
            return r0
        L_0x0009:
            boolean r0 = a(r8)     // Catch:{ Exception -> 0x0081 }
            if (r0 != 0) goto L_0x0011
            r0 = r1
            goto L_0x0008
        L_0x0011:
            a(r9)     // Catch:{ Exception -> 0x0081 }
            java.util.Calendar r0 = java.util.Calendar.getInstance()     // Catch:{ Exception -> 0x0081 }
            long r3 = r0.getTimeInMillis()     // Catch:{ Exception -> 0x0081 }
            r5 = 1800000(0x1b7740, double:8.89318E-318)
            long r5 = r5 + r3
            java.util.Random r0 = new java.util.Random     // Catch:{ Exception -> 0x0081 }
            r0.<init>()     // Catch:{ Exception -> 0x0081 }
            r2 = 1000000(0xf4240, float:1.401298E-39)
            int r0 = r0.nextInt(r2)     // Catch:{ Exception -> 0x0081 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0081 }
            r2.<init>()     // Catch:{ Exception -> 0x0081 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0081 }
            java.lang.String r2 = ""
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0081 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0081 }
            r2 = r0
        L_0x0040:
            int r0 = r2.length()     // Catch:{ Exception -> 0x0081 }
            r7 = 6
            if (r0 >= r7) goto L_0x005c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0081 }
            r0.<init>()     // Catch:{ Exception -> 0x0081 }
            java.lang.String r7 = "0"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0081 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0081 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0081 }
            r2 = r0
            goto L_0x0040
        L_0x005c:
            java.lang.Class<com.avast.android.generic.ab> r0 = com.avast.android.generic.ab.class
            java.lang.Object r0 = com.avast.android.generic.aa.a(r8, r0)     // Catch:{ Exception -> 0x0081 }
            com.avast.android.generic.ab r0 = (com.avast.android.generic.ab) r0     // Catch:{ Exception -> 0x0081 }
            java.lang.Object r7 = com.avast.android.generic.app.passwordrecovery.a.d     // Catch:{ Exception -> 0x0081 }
            monitor-enter(r7)     // Catch:{ Exception -> 0x0081 }
            r0.e(r2)     // Catch:{ all -> 0x007e }
            r0.b(r3)     // Catch:{ all -> 0x007e }
            r0.c(r5)     // Catch:{ all -> 0x007e }
            monitor-exit(r7)     // Catch:{ all -> 0x007e }
            c(r8)     // Catch:{ Exception -> 0x0081 }
            a(r8, r2, r10)     // Catch:{ Exception -> 0x0081 }
            com.avast.android.generic.app.passwordrecovery.b r0 = com.avast.android.generic.app.passwordrecovery.b.SMS_QUERIED
            a(r0)
            r0 = 1
            goto L_0x0008
        L_0x007e:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x007e }
            throw r0     // Catch:{ Exception -> 0x0081 }
        L_0x0081:
            r0 = move-exception
            com.avast.android.generic.app.passwordrecovery.b r0 = com.avast.android.generic.app.passwordrecovery.b.INIT_ERROR
            a(r8, r0)
            r0 = r1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.app.passwordrecovery.a.a(android.content.Context, com.avast.android.generic.app.passwordrecovery.j, java.lang.String):boolean");
    }

    public static boolean a(Context context, String str) {
        if (str == null || str == "") {
            return false;
        }
        ab abVar = (ab) aa.a(context, ab.class);
        synchronized (d) {
            if ("".equals(abVar.g())) {
                return false;
            }
            boolean c2 = abVar.c(str);
            return c2;
        }
    }

    public static void b(Context context) {
        if (d(context)) {
            c(context);
        }
    }

    public static void c(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        ab abVar = (ab) aa.a(context, ab.class);
        Intent intent = new Intent("com.avast.android.generic.RECOVERY_TIME_TICK");
        String uuid = UUID.randomUUID().toString();
        synchronized (d) {
            abVar.f(uuid);
        }
        intent.setComponent(new ComponentName(context, PasswordRecoveryReceiver.class.getCanonicalName()));
        intent.putExtra("auth_token", uuid);
        alarmManager.set(0, System.currentTimeMillis() + 60000, PendingIntent.getBroadcast(context, 0, intent, 268435456));
    }

    public static void a(Context context, boolean z) {
        if (d(context)) {
            if (z) {
                a(b.INITIATED_AND_VALID);
                Intent intent = new Intent("com.avast.android.generic.app.passwordrecovery.ACTION_PASSWORD_RECOVERY_INITIATED");
                ae.a(intent);
                context.sendBroadcast(intent, "com.avast.android.generic.COMM_PERMISSION");
                return;
            }
            a(context, b.SMS_SENDING_FAILED);
        }
    }

    public static boolean b(Context context, String str) {
        boolean equals;
        if (str == "") {
            return false;
        }
        ab abVar = (ab) aa.a(context, ab.class);
        synchronized (d) {
            equals = str.equals(abVar.l());
            abVar.f("");
        }
        return equals;
    }

    public static boolean c(Context context, String str) {
        boolean equals;
        if (str == "") {
            return false;
        }
        ab abVar = (ab) aa.a(context, ab.class);
        synchronized (d) {
            equals = str.equals(abVar.m());
            abVar.g("");
        }
        return equals;
    }

    public static boolean d(Context context) {
        ab abVar = (ab) aa.a(context, ab.class);
        synchronized (d) {
            if ("".equals(abVar.g())) {
                return false;
            }
            return e(context);
        }
    }

    private static synchronized boolean e(Context context) {
        long h;
        long j;
        long i;
        boolean z;
        synchronized (a.class) {
            ab abVar = (ab) aa.a(context, ab.class);
            synchronized (d) {
                h = abVar.h();
                j = abVar.j();
                i = abVar.i();
            }
            long timeInMillis = Calendar.getInstance().getTimeInMillis();
            if (h == -1 || i == -1) {
                z = false;
            } else if (timeInMillis < h - 120000) {
                a(context, b.INVALID);
                z = false;
            } else if (timeInMillis > 120000 + i) {
                a(context, b.EXPIRED);
                Intent intent = new Intent("com.avast.android.generic.app.passwordrecovery.ACTION_PASSWORD_RECOVERY_EXPIRED");
                ae.a(intent);
                context.sendBroadcast(intent, "com.avast.android.generic.COMM_PERMISSION");
                z = false;
            } else if (j == -1 || (timeInMillis >= j - 120000 && timeInMillis <= 120000 + j)) {
                synchronized (d) {
                    abVar.d(timeInMillis);
                }
                z = true;
            } else {
                a(context, b.INVALID);
                z = false;
            }
        }
        return z;
    }

    public static void a(Context context, b bVar) {
        ab abVar = (ab) aa.a(context, ab.class);
        synchronized (d) {
            abVar.e("");
            abVar.b(-1);
            abVar.d(-1);
            abVar.c(-1);
        }
        ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getBroadcast(context, 0, new Intent("com.avast.android.generic.RECOVERY_TIME_TICK"), 0));
        a(bVar);
    }

    private static void a(Context context, String str, String str2) {
        String k;
        SmsManager smsManager = SmsManager.getDefault();
        ab abVar = (ab) aa.a(context, ab.class);
        Intent intent = new Intent("com.avast.android.generic.RECOVERY_SMS");
        String uuid = UUID.randomUUID().toString();
        synchronized (d) {
            abVar.g(uuid);
            k = abVar.k();
        }
        if (!TextUtils.isEmpty(str2)) {
            k = str2;
        }
        intent.putExtra("auth_token", uuid);
        intent.setComponent(new ComponentName(context, PasswordRecoveryReceiver.class.getCanonicalName()));
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, intent, 268435456);
        String string = context.getString(x.msg_recovery_sms_text, str);
        ArrayList<String> divideMessage = smsManager.divideMessage(string);
        if (divideMessage.size() < 2) {
            smsManager.sendTextMessage(k, null, string, broadcast, null);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(broadcast);
        smsManager.sendMultipartTextMessage(k, null, divideMessage, arrayList, null);
    }

    private static void a(b bVar) {
        synchronized (f479c) {
            f477a = bVar;
            if (f478b != null) {
                f478b.a(f477a);
            }
        }
    }

    private static void a(j jVar) {
        synchronized (f479c) {
            f478b = jVar;
        }
    }
}
