package com.avast.android.generic.d;

import android.support.v4.app.NotificationCompat;
import java.io.ByteArrayOutputStream;
import java.security.SecureRandom;
import java.util.zip.ZipInputStream;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: CryptographyUtil */
public class c {
    private static byte[] a(byte[] bArr) {
        KeyGenerator instance = KeyGenerator.getInstance("AES");
        SecureRandom instance2 = SecureRandom.getInstance("SHA1PRNG", "Crypto");
        instance2.setSeed(bArr);
        instance.init((int) NotificationCompat.FLAG_HIGH_PRIORITY, instance2);
        return instance.generateKey().getEncoded();
    }

    public static byte[] a(String str, String str2) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(a(str2.getBytes()), "AES");
        Cipher instance = Cipher.getInstance("AES");
        instance.init(2, secretKeySpec);
        return instance.doFinal(a.a(str));
    }

    public static byte[] a(ZipInputStream zipInputStream, String str) {
        byte[] bArr = new byte[2048];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = zipInputStream.read(bArr, 0, 2048);
            if (read == -1) {
                return a(byteArrayOutputStream.toString(), str);
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }
}
