package com.avast.android.generic.ui.widget;

import com.avast.android.generic.licensing.m;

/* compiled from: SubscriptionRow */
/* synthetic */ class aa {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f1133a = new int[m.values().length];

    static {
        try {
            f1133a[m.VALID.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f1133a[m.UNKNOWN.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f1133a[m.NOT_AVAILABLE.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f1133a[m.PROGRESS.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f1133a[m.NONE.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
