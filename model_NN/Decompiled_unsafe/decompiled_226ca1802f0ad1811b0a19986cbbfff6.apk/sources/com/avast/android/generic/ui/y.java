package com.avast.android.generic.ui;

import android.view.View;
import com.avast.android.generic.q;

/* compiled from: PasswordDialog */
class y implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ x f1162a;

    y(x xVar) {
        this.f1162a = xVar;
    }

    public void onClick(View view) {
        this.f1162a.f1161b.b();
        String a2 = this.f1162a.f1161b.d.a();
        if (this.f1162a.f1161b.f995c.b(a2) || this.f1162a.f1161b.f995c.c(a2)) {
            this.f1162a.f1161b.c();
            return;
        }
        this.f1162a.f1161b.e.setVisibility(0);
        this.f1162a.f1161b.e.setImageResource(q.ic_scanner_result_problem);
    }
}
