package com.avast.android.generic.app.account;

/* compiled from: FacebookUserAuthenticator */
public class bg {

    /* renamed from: a  reason: collision with root package name */
    private String f416a;

    /* renamed from: b  reason: collision with root package name */
    private long f417b;

    public bg(String str, long j) {
        this.f416a = str;
        this.f417b = j;
    }

    public String a() {
        return this.f416a;
    }

    public long b() {
        return this.f417b;
    }
}
