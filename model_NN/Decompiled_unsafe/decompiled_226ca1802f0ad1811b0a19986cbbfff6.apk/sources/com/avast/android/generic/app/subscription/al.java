package com.avast.android.generic.app.subscription;

import android.content.Intent;
import android.view.View;

/* compiled from: WelcomePremiumFragment */
class al implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WelcomePremiumFragment f539a;

    al(WelcomePremiumFragment welcomePremiumFragment) {
        this.f539a = welcomePremiumFragment;
    }

    public void onClick(View view) {
        Intent intent = new Intent("android.intent.action.VIEW", WelcomePremiumFragment.f520a);
        intent.addFlags(268468224);
        this.f539a.startActivity(intent);
    }
}
