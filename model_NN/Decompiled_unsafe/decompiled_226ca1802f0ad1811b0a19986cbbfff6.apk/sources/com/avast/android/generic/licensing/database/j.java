package com.avast.android.generic.licensing.database;

import android.content.ContentResolver;
import android.net.Uri;
import com.avast.android.generic.util.z;

/* compiled from: PurchaseDatabaseHelper */
final class j implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentResolver f895a;

    j(ContentResolver contentResolver) {
        this.f895a = contentResolver;
    }

    public void a(Uri uri, ContentResolver contentResolver) {
        if (this.f895a.delete(uri, "order_id IS NULL", null) > 0) {
            z.a("AvastGenericLic", "Deleted invalid orders, why?");
        }
    }
}
