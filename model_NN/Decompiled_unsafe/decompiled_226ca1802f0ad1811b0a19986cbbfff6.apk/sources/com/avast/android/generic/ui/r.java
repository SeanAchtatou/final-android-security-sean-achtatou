package com.avast.android.generic.ui;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: CustomNumberDialog */
class r implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CustomNumberDialog f1056a;

    r(CustomNumberDialog customNumberDialog) {
        this.f1056a = customNumberDialog;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.f1056a.a(charSequence);
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
    }
}
