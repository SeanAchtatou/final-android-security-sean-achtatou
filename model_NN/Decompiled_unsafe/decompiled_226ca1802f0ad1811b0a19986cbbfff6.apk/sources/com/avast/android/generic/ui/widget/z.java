package com.avast.android.generic.ui.widget;

import com.avast.android.generic.licensing.m;

/* compiled from: SubscriptionButton */
/* synthetic */ class z {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f1159a = new int[m.values().length];

    static {
        try {
            f1159a[m.VALID.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f1159a[m.UNKNOWN.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f1159a[m.NOT_AVAILABLE.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f1159a[m.PROGRESS.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f1159a[m.NONE.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
