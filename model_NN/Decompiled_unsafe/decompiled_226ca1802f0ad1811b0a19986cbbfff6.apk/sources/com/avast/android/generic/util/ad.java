package com.avast.android.generic.util;

import android.content.Context;
import com.avast.android.generic.x;

/* compiled from: ReadableSizeHelper */
public class ad {

    /* renamed from: a  reason: collision with root package name */
    private String f1173a;

    /* renamed from: b  reason: collision with root package name */
    private String f1174b;

    /* renamed from: c  reason: collision with root package name */
    private String f1175c;
    private String d;
    private String e;

    public ad(Context context) {
        this.f1173a = context.getString(x.unit_b);
        this.f1174b = context.getString(x.unit_kb);
        this.f1175c = context.getString(x.unit_mb);
        this.d = context.getString(x.unit_gb);
        this.e = context.getString(x.unit_tb);
    }

    public String a(long j) {
        if (j <= 1) {
            return String.valueOf(j) + " " + this.f1173a;
        }
        if (j < 1024) {
            return String.valueOf(j) + " " + this.f1173a;
        }
        if (j < 1048576) {
            return String.valueOf(j / 1024) + " " + this.f1174b;
        }
        if (j < 1073741824) {
            return String.valueOf(j / 1048576) + " " + this.f1175c;
        }
        return String.valueOf(j / 1073741824) + " " + this.d;
    }
}
