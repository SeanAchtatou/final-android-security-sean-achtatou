package com.avast.android.generic.app.account;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.r;
import com.avast.android.generic.util.ae;
import com.avast.android.generic.util.d;
import com.avast.android.generic.util.g;
import com.avast.android.generic.util.t;
import com.avast.android.generic.util.y;
import com.avast.android.generic.x;

/* compiled from: DisconnectFragment */
class bd extends AsyncTask<String, Void, Boolean> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DisconnectFragment f410a;

    /* renamed from: b  reason: collision with root package name */
    private ab f411b;

    /* renamed from: c  reason: collision with root package name */
    private String f412c;
    private byte[] d;

    public bd(DisconnectFragment disconnectFragment, ab abVar) {
        this.f410a = disconnectFragment;
        this.f411b = abVar;
        this.f412c = abVar.w();
        this.d = abVar.x();
        disconnectFragment.a(this);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Boolean doInBackground(java.lang.String... r5) {
        /*
            r4 = this;
            com.avast.android.generic.app.account.DisconnectFragment r0 = r4.f410a
            boolean r0 = r0.isAdded()
            if (r0 != 0) goto L_0x000e
            r0 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
        L_0x000d:
            return r0
        L_0x000e:
            com.avast.android.generic.app.account.DisconnectFragment r0 = r4.f410a
            android.support.v4.app.FragmentActivity r0 = r0.getActivity()
            java.lang.String r0 = com.avast.android.generic.app.account.DisconnectFragment.a(r0)
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x0028
            java.lang.String r1 = "my_avast_pairing_server_address"
            r3.putString(r1, r0)
        L_0x0028:
            r2 = 0
            com.avast.android.generic.internet.c.c r1 = new com.avast.android.generic.internet.c.c     // Catch:{ InstantiationException -> 0x0046, h -> 0x005a, IOException -> 0x006e, all -> 0x0082 }
            com.avast.android.generic.app.account.DisconnectFragment r0 = r4.f410a     // Catch:{ InstantiationException -> 0x0046, h -> 0x005a, IOException -> 0x006e, all -> 0x0082 }
            android.support.v4.app.FragmentActivity r0 = r0.getActivity()     // Catch:{ InstantiationException -> 0x0046, h -> 0x005a, IOException -> 0x006e, all -> 0x0082 }
            r1.<init>(r0, r3)     // Catch:{ InstantiationException -> 0x0046, h -> 0x005a, IOException -> 0x006e, all -> 0x0082 }
            java.lang.String r0 = r4.f412c     // Catch:{ InstantiationException -> 0x0090, h -> 0x008e, IOException -> 0x008c }
            byte[] r2 = r4.d     // Catch:{ InstantiationException -> 0x0090, h -> 0x008e, IOException -> 0x008c }
            boolean r0 = r1.a(r0, r2)     // Catch:{ InstantiationException -> 0x0090, h -> 0x008e, IOException -> 0x008c }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ InstantiationException -> 0x0090, h -> 0x008e, IOException -> 0x008c }
            if (r1 == 0) goto L_0x000d
            r1.a()
            goto L_0x000d
        L_0x0046:
            r0 = move-exception
            r1 = r2
        L_0x0048:
            java.lang.String r2 = "breadcrumbs"
            java.lang.String r3 = "Could send disconnect to MyAvast."
            com.avast.android.generic.util.t.d(r2, r3, r0)     // Catch:{ all -> 0x008a }
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x000d
            r1.a()
            goto L_0x000d
        L_0x005a:
            r0 = move-exception
            r1 = r2
        L_0x005c:
            java.lang.String r2 = "breadcrumbs"
            java.lang.String r3 = "Disconnect from MyAvast failed."
            com.avast.android.generic.util.t.d(r2, r3, r0)     // Catch:{ all -> 0x008a }
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x000d
            r1.a()
            goto L_0x000d
        L_0x006e:
            r0 = move-exception
            r1 = r2
        L_0x0070:
            java.lang.String r2 = "breadcrumbs"
            java.lang.String r3 = "Disconnect from MyAvast failed."
            com.avast.android.generic.util.t.d(r2, r3, r0)     // Catch:{ all -> 0x008a }
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x000d
            r1.a()
            goto L_0x000d
        L_0x0082:
            r0 = move-exception
            r1 = r2
        L_0x0084:
            if (r1 == 0) goto L_0x0089
            r1.a()
        L_0x0089:
            throw r0
        L_0x008a:
            r0 = move-exception
            goto L_0x0084
        L_0x008c:
            r0 = move-exception
            goto L_0x0070
        L_0x008e:
            r0 = move-exception
            goto L_0x005c
        L_0x0090:
            r0 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.app.account.bd.doInBackground(java.lang.String[]):java.lang.Boolean");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        if (bool.booleanValue()) {
            t.b("breadcrumbs", "Successfully disconnected from the account.");
            FragmentActivity activity = this.f410a.getActivity();
            d.b(activity).a(g.ACCOUNT);
            this.f411b.u();
            ((y) aa.a(activity, y.class)).a(r.message_avast_account_disconnected);
            a();
            Toast.makeText(activity, x.msg_avast_account_disconnected, 0).show();
            if (this.f410a.isAdded()) {
                this.f410a.i();
            }
        } else {
            t.b("breadcrumbs", "Disconnection from the account failed.");
        }
        this.f410a.f();
    }

    private void a() {
        t.b("breadcrumbs", "Sending avast! account disconnected broadcast.");
        Intent intent = new Intent("com.avast.android.mobilesecurity.app.account.ACCOUNT_DISCONNECTED");
        ae.a(intent);
        this.f410a.getActivity().sendBroadcast(intent, "com.avast.android.generic.COMM_PERMISSION");
    }
}
