package com.avast.android.generic.ui.widget;

import android.content.DialogInterface;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.m;
import com.avast.android.generic.r;
import com.avast.android.generic.ui.widget.LanguageSelectorRow;
import com.avast.android.generic.util.y;

/* compiled from: LanguageSelectorRow */
class i implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LanguageSelectorRow.LanguageSelectDialog f1145a;

    i(LanguageSelectorRow.LanguageSelectDialog languageSelectDialog) {
        this.f1145a = languageSelectDialog;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        ((ab) aa.a(this.f1145a.getActivity(), ab.class)).h((String) this.f1145a.getResources().getTextArray(m.languages_code)[i]);
        ((y) aa.a(this.f1145a.getActivity(), y.class)).a(r.message_language_changed);
        dialogInterface.dismiss();
    }
}
