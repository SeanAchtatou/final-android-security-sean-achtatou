package com.avast.android.generic.app.passwordrecovery;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.x;

public class RecoveryResultDialog extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private boolean f476a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getArguments() != null) {
            this.f476a = getArguments().getBoolean("result", false);
        } else {
            this.f476a = false;
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ak.c(getActivity()));
        builder.setTitle(getString(x.l_password_recovery_dialog_title));
        if (this.f476a) {
            builder.setMessage(x.msg_password_recovery_message_sent);
        } else {
            builder.setMessage(x.msg_password_recovery_message_not_sent);
        }
        builder.setPositiveButton(x.bR, new k(this));
        AlertDialog create = builder.create();
        create.setInverseBackgroundForced(true);
        return create;
    }
}
