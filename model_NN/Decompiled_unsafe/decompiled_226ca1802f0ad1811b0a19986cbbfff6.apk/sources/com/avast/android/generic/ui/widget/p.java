package com.avast.android.generic.ui.widget;

import android.text.InputFilter;
import android.text.Spanned;

/* compiled from: PasswordTextView */
class p implements InputFilter {

    /* renamed from: a  reason: collision with root package name */
    StringBuilder f1155a = new StringBuilder();

    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        if (this.f1155a.length() <= 0 || !"".equals(spanned.toString())) {
            if (charSequence.length() == 0) {
                this.f1155a.replace(i3, i4, "");
            } else if (!PasswordTextView.f1096b.matcher(charSequence.toString()).matches()) {
                this.f1155a.replace(i3, i4, charSequence.toString());
            }
        }
        return null;
    }
}
