package com.avast.android.generic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import com.actionbarsherlock.view.Menu;
import com.avast.android.generic.notification.h;
import com.avast.android.generic.util.t;
import java.util.Locale;

/* compiled from: LanguageSwitcher */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private static String f680a = "";

    public static String a(Context context) {
        String c2 = c(context);
        if (!b(context)) {
            b(context, c2);
            h hVar = (h) aa.a(context, h.class);
            if (hVar != null) {
                hVar.a();
            }
        }
        return c2;
    }

    public static void a(Context context, String str) {
        if (c(context).equals(str)) {
            return;
        }
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            Intent intent = activity.getIntent();
            intent.addFlags(Menu.CATEGORY_CONTAINER);
            activity.getWindow().setWindowAnimations(0);
            activity.finish();
            activity.startActivity(intent);
            return;
        }
        throw new IllegalArgumentException("Context must be an Activity!");
    }

    public static void b(Context context, String str) {
        t.b("Switching language: " + str);
        if ("".equals(f680a) && str.length() > 0) {
            f680a = Locale.getDefault().getLanguage();
        }
        if (!"".equals(f680a) || !"".equals(str)) {
            if (f680a.length() > 0 && "".equals(str)) {
                str = f680a;
                f680a = "";
            }
            Locale locale = new Locale(str);
            if (str.contains("-")) {
                String[] split = str.split("-");
                if (split.length > 1) {
                    if (split[1].startsWith("r")) {
                        split[1] = split[1].replaceFirst("r", "");
                    }
                    locale = new Locale(split[0], split[1]);
                }
            }
            Locale.setDefault(locale);
            Configuration configuration = new Configuration();
            configuration.locale = locale;
            context.getResources().updateConfiguration(configuration, null);
        }
    }

    private static boolean b(Context context) {
        return c(context).equals(Locale.getDefault().getLanguage());
    }

    private static String c(Context context) {
        return ((ab) aa.a(context, ab.class)).p();
    }
}
