package com.avast.android.generic.ui;

import android.os.Bundle;

public abstract class BaseMultiPaneActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private Integer f981a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int i;
        super.onCreate(bundle);
        if (bundle != null && (i = bundle.getInt("last_navigation_mode", -1)) != -1) {
            this.f981a = Integer.valueOf(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.f981a != null) {
            bundle.putInt("last_navigation_mode", this.f981a.intValue());
        }
    }
}
