package com.avast.android.generic.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import com.avast.android.generic.l;
import com.avast.android.generic.ui.s;

/* compiled from: ActivityHelper */
public class a {

    /* renamed from: a  reason: collision with root package name */
    protected Activity f1164a;

    public static a a(Activity activity) {
        return new a(activity);
    }

    protected a(Activity activity) {
        this.f1164a = activity;
    }

    public boolean a(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        c();
        return true;
    }

    public void a() {
    }

    public void b() {
    }

    @TargetApi(5)
    public void c() {
        String str;
        boolean z = true;
        String packageName = this.f1164a.getPackageName();
        if (packageName.equals("com.avast.android.antitheft") || packageName.equals("com.avast.android.at_play") || packageName.equals("com.avast.android.backup") || packageName.equals("com.avast.android.vpn")) {
            str = packageName;
        } else {
            str = "com.avast.android.mobilesecurity";
        }
        if (packageName.equals("com.avast.android.backup")) {
            str = "com.avast.android.backup";
            if (packageName.equals("com.avast.android.backup") && aa.a(this.f1164a)) {
                packageName = "com.avast.android.mobilesecurity";
                str = "com.avast.android.mobilesecurity";
            }
            z = false;
        } else {
            if ((packageName.equals("com.avast.android.antitheft") || packageName.equals("com.avast.android.at_play")) && aa.a(this.f1164a)) {
                packageName = "com.avast.android.mobilesecurity";
                str = "com.avast.android.mobilesecurity";
            }
            z = false;
        }
        if (str.equals("com.avast.android.at_play")) {
            str = "com.avast.android.antitheft";
        }
        if (z || !(this.f1164a instanceof s)) {
            com.avast.android.generic.util.ga.a.a().a("common", "avastLogoGoHome", this.f1164a.getClass().getName(), 0L);
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(packageName, str + ".app.home.HomeActivity"));
            intent.addFlags(67108864);
            this.f1164a.startActivity(intent);
            if (!ak.a()) {
                this.f1164a.overridePendingTransition(l.home_enter, l.home_exit);
            }
        }
    }

    public static void a(Fragment fragment, Runnable runnable) {
        if (fragment != null && runnable != null) {
            try {
                FragmentActivity activity = fragment.getActivity();
                if (activity != null) {
                    activity.runOnUiThread(runnable);
                }
            } catch (Exception e) {
                z.a("AvastGeneric", "Error in running on UI thread", e);
            }
        }
    }

    public static void a(Fragment fragment) {
        if (fragment != null) {
            try {
                FragmentActivity activity = fragment.getActivity();
                if (activity != null) {
                    activity.finish();
                }
            } catch (Exception e) {
                z.a("AvastGeneric", "Error in finishing fragment activity", e);
            }
        }
    }
}
