package com.avast.android.generic.internet;

import android.content.Intent;
import android.net.NetworkInfo;
import android.support.v4.a.a;
import com.avast.android.generic.internet.HttpSender;

/* compiled from: HttpSender */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Intent f824a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ HttpSender.HttpStatusChangedBroadcastReceiver f825b;

    i(HttpSender.HttpStatusChangedBroadcastReceiver httpStatusChangedBroadcastReceiver, Intent intent) {
        this.f825b = httpStatusChangedBroadcastReceiver;
        this.f824a = intent;
    }

    public void run() {
        try {
            NetworkInfo a2 = a.a(this.f825b.f744a.k, this.f824a);
            NetworkInfo.State state = NetworkInfo.State.DISCONNECTED;
            if (a2 != null) {
                state = a2.getState();
            }
            if (state == NetworkInfo.State.CONNECTED) {
                this.f825b.f744a.a();
            } else {
                this.f825b.f744a.b();
            }
        } catch (Exception e) {
        }
    }
}
