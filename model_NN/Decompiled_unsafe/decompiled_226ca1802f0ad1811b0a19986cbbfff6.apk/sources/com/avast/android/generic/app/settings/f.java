package com.avast.android.generic.app.settings;

import android.os.Handler;
import android.os.Message;
import com.avast.android.generic.r;

/* compiled from: SetRecoveryNumberDialog */
class f implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SetRecoveryNumberDialog f505a;

    f(SetRecoveryNumberDialog setRecoveryNumberDialog) {
        this.f505a = setRecoveryNumberDialog;
    }

    public boolean handleMessage(Message message) {
        if (message.what == r.message_filter_custom_number_entered) {
            this.f505a.a((String) message.obj);
        }
        this.f505a.f496b.post(new g(this));
        return true;
    }
}
