package com.avast.android.generic;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.view.LayoutInflater;
import com.avast.android.generic.licensing.b.a;
import com.avast.android.generic.licensing.b.b;
import com.avast.android.generic.ui.rtl.d;
import com.avast.android.generic.util.s;
import com.avast.android.generic.util.t;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Application extends android.app.Application {

    /* renamed from: a  reason: collision with root package name */
    public static final boolean f317a = (Build.VERSION.SDK_INT >= 8);

    /* renamed from: b  reason: collision with root package name */
    public static final boolean f318b = false;

    /* renamed from: c  reason: collision with root package name */
    private static boolean f319c = false;
    private static boolean d = false;
    private static boolean e = false;
    private static String f = null;
    private static boolean g = false;
    private static Set<String> j = new HashSet();
    private static a k;
    private static d l;
    private boolean h = true;
    private int i = 0;

    public void onCreate() {
        boolean z;
        boolean z2 = false;
        super.onCreate();
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            this.h = packageInfo.versionCode > 0;
            this.i = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            t.b(e2.getMessage(), e2);
        }
        if (s.a(this)) {
            Logger.getLogger("com.google.api.client").setLevel(Level.FINEST);
        }
        try {
            z = new File(Environment.getExternalStorageDirectory(), "avast-debug").exists();
        } catch (Exception e3) {
            z = false;
        }
        if (s.a(this) || z) {
            z2 = true;
        }
        t.a(z2);
        i();
    }

    public boolean a() {
        return b() && s.b(this);
    }

    public boolean b() {
        return this.h;
    }

    public static void a(boolean z) {
        f319c = z;
    }

    public static boolean c() {
        return f319c;
    }

    public static void b(boolean z) {
        d = z;
    }

    public static boolean d() {
        return d;
    }

    public static void c(boolean z) {
        e = z;
    }

    public static boolean e() {
        return e;
    }

    public static void a(String str) {
        f = str;
    }

    public static String f() {
        return f;
    }

    public static void g() {
        g = true;
    }

    public static boolean h() {
        return g;
    }

    private void i() {
        if (Build.VERSION.SDK_INT < 9) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    public synchronized Object getSystemService(String str) {
        Object obj;
        if (a.class.toString().equals(str)) {
            if (k == null) {
                k = new b(this);
            }
            obj = k;
        } else if ("layout_inflater".equals(str) || d.class.toString().equals(str)) {
            if (l == null) {
                l = new d((LayoutInflater) super.getSystemService(str));
            }
            obj = l;
        } else {
            obj = super.getSystemService(str);
        }
        return obj;
    }
}
