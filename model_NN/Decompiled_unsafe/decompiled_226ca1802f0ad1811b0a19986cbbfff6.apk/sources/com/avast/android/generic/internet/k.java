package com.avast.android.generic.internet;

import java.io.File;

/* compiled from: MultiPartDescriptor */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private String f826a;

    /* renamed from: b  reason: collision with root package name */
    private File f827b;

    /* renamed from: c  reason: collision with root package name */
    private byte[] f828c;

    public String a() {
        return this.f826a;
    }

    public File b() {
        return this.f827b;
    }

    public byte[] c() {
        return this.f828c;
    }
}
