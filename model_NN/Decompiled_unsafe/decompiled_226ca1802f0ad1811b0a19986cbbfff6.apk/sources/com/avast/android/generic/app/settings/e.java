package com.avast.android.generic.app.settings;

import android.view.View;
import com.avast.android.generic.r;
import com.avast.android.generic.ui.PasswordDialog;

/* compiled from: RecoveryNumberDescriptionDialog */
class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f504a;

    e(d dVar) {
        this.f504a = dVar;
    }

    public void onClick(View view) {
        if (this.f504a.f503c.getFragmentManager() == null) {
            return;
        }
        if (!this.f504a.f503c.f492a.e() || this.f504a.f502b) {
            this.f504a.f503c.a();
        } else {
            PasswordDialog.a(this.f504a.f503c.getFragmentManager(), r.message_recovery_number_change_pin_check_after_description);
        }
    }
}
