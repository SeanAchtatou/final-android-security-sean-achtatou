package com.avast.android.generic.util;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.text.style.StyleSpan;
import android.view.ContextThemeWrapper;
import com.avast.android.generic.y;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* compiled from: UIUtils */
public class ak {

    /* renamed from: a  reason: collision with root package name */
    private static StyleSpan f1177a = new StyleSpan(1);

    public static boolean a() {
        return Build.VERSION.SDK_INT >= 11;
    }

    public static boolean a(Context context) {
        return (context.getResources().getConfiguration().screenLayout & 15) >= 3;
    }

    public static boolean b(Context context) {
        return a() && a(context);
    }

    public static Context c(Context context) {
        return new ContextThemeWrapper(context, y.Theme_Avast_Dialog);
    }

    public static StringBuilder a(Resources resources, int i) {
        BufferedReader bufferedReader;
        StringBuilder sb = new StringBuilder();
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(resources.openRawResource(i)));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            }
            bufferedReader.close();
        } catch (IOException e) {
            t.e("cannot parse eula text");
        } catch (Throwable th) {
            bufferedReader.close();
            throw th;
        }
        return sb;
    }
}
