package com.avast.android.generic;

import android.net.Uri;
import android.provider.BaseColumns;

/* compiled from: GenericContract */
public class e implements BaseColumns {
    public static Uri a(Uri uri) {
        Uri.Builder buildUpon = uri.buildUpon();
        buildUpon.appendPath("notifications");
        return buildUpon.build();
    }

    public static Uri a(Uri uri, long j, String str) {
        Uri.Builder buildUpon = uri.buildUpon();
        buildUpon.appendPath("notifications");
        buildUpon.appendPath(String.valueOf(j));
        buildUpon.appendPath(str);
        return buildUpon.build();
    }
}
