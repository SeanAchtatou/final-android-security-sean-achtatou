package com.avast.android.generic.app.account;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.MenuItem;
import com.avast.android.generic.ui.BaseActivity;
import com.avast.android.generic.ui.BaseSinglePaneActivity;

public class AccountActivity extends BaseSinglePaneActivity {
    public static void call(Context context, Bundle bundle) {
        Intent intent = new Intent(context, AccountActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        if (context instanceof BaseActivity) {
            ((BaseActivity) context).b(intent);
        } else {
            context.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeButtonEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(true);
            supportActionBar.setDisplayUseLogoEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public SherlockFragment a() {
        return new SignInPickerFragment();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Fragment findFragmentByTag = getSupportFragmentManager().findFragmentByTag("singleFragment");
        if (!(findFragmentByTag instanceof SignInPickerFragment)) {
            super.onActivityResult(i, i2, intent);
        } else if (!((SignInPickerFragment) findFragmentByTag).a(i, i2, intent)) {
            super.onActivityResult(i, i2, intent);
        }
    }
}
