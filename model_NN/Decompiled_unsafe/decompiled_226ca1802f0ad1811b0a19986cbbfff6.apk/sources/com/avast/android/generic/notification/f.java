package com.avast.android.generic.notification;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import java.text.DateFormat;
import java.util.Date;

/* compiled from: AvastNotificationFragment */
class f extends a {

    /* renamed from: a  reason: collision with root package name */
    Activity f927a;

    /* renamed from: b  reason: collision with root package name */
    h f928b;

    /* renamed from: c  reason: collision with root package name */
    DateFormat f929c;

    public f(Activity activity, h hVar) {
        super(activity, (Cursor) null, 0);
        this.f927a = activity;
        this.f928b = hVar;
        this.f929c = android.text.format.DateFormat.getTimeFormat(activity);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        AvastPendingIntent avastPendingIntent;
        g gVar = (g) view.getTag();
        gVar.f930a = cursor.getLong(cursor.getColumnIndex("notification_id"));
        gVar.f931b.setText(cursor.getString(cursor.getColumnIndex("contentTitle")));
        gVar.f932c.setText(cursor.getString(cursor.getColumnIndex("contentText")));
        gVar.e.setImageResource(this.f928b.a(cursor.getInt(cursor.getColumnIndex("_id"))));
        if (cursor.getInt(cursor.getColumnIndex("ongoing")) > 0) {
            float f = cursor.getFloat(cursor.getColumnIndex("percentage"));
            if (f > -1.0f) {
                gVar.d.setText(String.format("%.0f %%", Float.valueOf(f)));
            } else {
                gVar.d.setVisibility(4);
            }
        } else {
            gVar.d.setText(this.f929c.format(new Date(cursor.getLong(cursor.getColumnIndex("timestamp")))));
        }
        AvastPendingIntent avastPendingIntent2 = new AvastPendingIntent(cursor, "pendingIntentClass", "pendingIntentAction", "pendingIntentType", "pendingIntentData", "pendingIntentExtras", "pendingIntentFlags");
        if (avastPendingIntent2.f != null) {
            avastPendingIntent = avastPendingIntent2;
        } else {
            avastPendingIntent = null;
        }
        gVar.g = avastPendingIntent;
        if (avastPendingIntent2.a(this.f927a) == null) {
            gVar.f.setVisibility(4);
        } else {
            gVar.f.setVisibility(0);
        }
        AvastPendingIntent avastPendingIntent3 = new AvastPendingIntent(cursor, "deleteIntentClass", "deleteIntentAction", "deleteIntentType", "deleteIntentData", "deleteIntentExtras", "deleteIntentFlags");
        if (avastPendingIntent3.f == null) {
            avastPendingIntent3 = null;
        }
        gVar.h = avastPendingIntent3;
        gVar.i = cursor.getInt(cursor.getColumnIndex("flags"));
        gVar.j = cursor.getString(cursor.getColumnIndex("notification_tag"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View inflate = ((LayoutInflater) this.f927a.getSystemService("layout_inflater")).inflate(t.list_item_notification, viewGroup, false);
        g gVar = new g(null);
        gVar.f931b = (TextView) inflate.findViewById(r.bM);
        gVar.f932c = (TextView) inflate.findViewById(r.text);
        gVar.e = (ImageView) inflate.findViewById(r.icon);
        gVar.d = (TextView) inflate.findViewById(r.aV);
        gVar.f = (ImageView) inflate.findViewById(r.arrow);
        inflate.setTag(gVar);
        return inflate;
    }
}
