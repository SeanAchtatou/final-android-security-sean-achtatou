package com.avast.android.generic.ui.a;

import android.database.DataSetObserver;

/* compiled from: AdapterWrapper */
class b extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1004a;

    b(a aVar) {
        this.f1004a = aVar;
    }

    public void onChanged() {
        this.f1004a.notifyDataSetChanged();
    }

    public void onInvalidated() {
        this.f1004a.notifyDataSetInvalidated();
    }
}
