package com.avast.android.generic.app.about;

import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.ui.widget.SlideBlock;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.w;

/* compiled from: AboutFragment */
class c extends AsyncTask<Void, Void, CharSequence> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SlideBlock f336a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ AboutFragment f337b;

    c(AboutFragment aboutFragment, SlideBlock slideBlock) {
        this.f337b = aboutFragment;
        this.f336a = slideBlock;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public CharSequence doInBackground(Void... voidArr) {
        if (this.f337b.isAdded()) {
            return Html.fromHtml(ak.a(this.f337b.getResources(), w.eula).toString());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(CharSequence charSequence) {
        if (charSequence != null) {
            try {
                this.f336a.setEnabled(true);
                View inflate = ((LayoutInflater) this.f337b.getActivity().getSystemService("layout_inflater")).inflate(t.eula_slider_content, (ViewGroup) null);
                ((TextView) inflate.findViewById(r.about_eula_text)).setText(charSequence);
                this.f336a.a(inflate);
            } catch (Exception e) {
                com.avast.android.generic.util.t.a("AboutFragment: Can't inflate Eula slider content.", e);
            }
        }
    }
}
