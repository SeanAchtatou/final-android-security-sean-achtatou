package com.avast.android.generic.h;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ShepherdResponseProto */
public final class v extends GeneratedMessageLite.Builder<u, v> implements w {

    /* renamed from: a  reason: collision with root package name */
    private int f727a;

    /* renamed from: b  reason: collision with root package name */
    private int f728b;

    /* renamed from: c  reason: collision with root package name */
    private ByteString f729c = ByteString.EMPTY;

    private v() {
        g();
    }

    private void g() {
    }

    /* access modifiers changed from: private */
    public static v h() {
        return new v();
    }

    /* renamed from: a */
    public v clone() {
        return h().mergeFrom(d());
    }

    /* renamed from: b */
    public u getDefaultInstanceForType() {
        return u.a();
    }

    /* renamed from: c */
    public u build() {
        u d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    /* access modifiers changed from: private */
    public u i() {
        u d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d).asInvalidProtocolBufferException();
    }

    public u d() {
        int i = 1;
        u uVar = new u(this);
        int i2 = this.f727a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        int unused = uVar.f726c = this.f728b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        ByteString unused2 = uVar.d = this.f729c;
        int unused3 = uVar.f725b = i;
        return uVar;
    }

    /* renamed from: a */
    public v mergeFrom(u uVar) {
        if (uVar != u.a()) {
            if (uVar.b()) {
                a(uVar.c());
            }
            if (uVar.d()) {
                a(uVar.e());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (!e()) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public v mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    this.f727a |= 1;
                    this.f728b = codedInputStream.readInt32();
                    break;
                case 18:
                    this.f727a |= 2;
                    this.f729c = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f727a & 1) == 1;
    }

    public v a(int i) {
        this.f727a |= 1;
        this.f728b = i;
        return this;
    }

    public v a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f727a |= 2;
        this.f729c = byteString;
        return this;
    }
}
