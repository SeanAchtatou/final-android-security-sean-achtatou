package com.avast.android.generic.internet.c;

/* compiled from: AvastAccountException */
public class h extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private i f811a = null;

    protected h(String str, String str2) {
        super(str + ", error from id.avast.com: " + str2);
        this.f811a = i.a(str2);
    }

    protected h(String str, i iVar) {
        super(str + ", error from id.avast.com: " + iVar);
        this.f811a = iVar;
    }

    public h(Throwable th) {
        super(th);
    }

    public i a() {
        return this.f811a;
    }
}
