package com.avast.android.generic.app.account;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.avast.android.generic.util.w;
import com.avast.android.generic.util.z;

/* compiled from: ConnectAccountHelper */
class ab extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f374a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ aa f375b;

    ab(aa aaVar, r rVar) {
        this.f375b = aaVar;
        this.f374a = rVar;
    }

    public void onReceive(Context context, Intent intent) {
        Context applicationContext = context.getApplicationContext();
        String stringExtra = intent.getStringExtra("registration_id");
        String stringExtra2 = intent.getStringExtra("error");
        if (stringExtra2 != null) {
            z.a("AvastComms", applicationContext, "C2DM registration received C2DM registration error (" + stringExtra2 + ")");
            this.f375b.d(w.a(applicationContext, stringExtra2));
        } else if (intent.getStringExtra("unregistered") != null) {
            z.a("AvastComms", applicationContext, "C2DM registration received C2DM unregistered event");
        } else if (stringExtra != null) {
            z.a("AvastComms", applicationContext, "C2DM registration detected successful registration (ID " + stringExtra + ")");
            this.f375b.c(stringExtra);
        }
    }
}
