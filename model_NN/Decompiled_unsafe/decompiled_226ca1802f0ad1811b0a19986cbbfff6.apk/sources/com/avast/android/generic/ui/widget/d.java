package com.avast.android.generic.ui.widget;

import android.os.Parcel;
import android.os.Parcelable;
import com.avast.android.generic.ui.widget.CheckBoxRow;

/* compiled from: CheckBoxRow */
final class d implements Parcelable.Creator<CheckBoxRow.SavedState> {
    d() {
    }

    /* renamed from: a */
    public CheckBoxRow.SavedState createFromParcel(Parcel parcel) {
        return new CheckBoxRow.SavedState(parcel, null);
    }

    /* renamed from: a */
    public CheckBoxRow.SavedState[] newArray(int i) {
        return new CheckBoxRow.SavedState[i];
    }
}
