package com.avast.android.generic.app.subscription;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.text.TextUtils;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.avast.android.generic.util.z;

/* compiled from: WebPurchaseFragment */
class ag extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WebPurchaseFragment f534a;

    ag(WebPurchaseFragment webPurchaseFragment) {
        this.f534a = webPurchaseFragment;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return false;
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        z.a("AvastGenericLic", "Received web view error " + i + " (" + str + ", " + str2 + ")");
        if (TextUtils.isEmpty(str2)) {
            str2 = "unknown host";
        }
        WebPurchaseFragment.f517a = str2;
        if (WebPurchaseFragment.f517a.contains("?")) {
            WebPurchaseFragment.f517a = WebPurchaseFragment.f517a.substring(0, WebPurchaseFragment.f517a.indexOf("?"));
        }
        WebPurchaseFragment.f517a = TextUtils.isEmpty(WebPurchaseFragment.f517a) ? "unknown host" : WebPurchaseFragment.f517a;
        if (this.f534a.isAdded()) {
            Intent intent = this.f534a.getActivity().getIntent();
            intent.putExtra("resultDescription", str);
            this.f534a.getActivity().setResult(i, intent);
            this.f534a.getActivity().finish();
        }
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        z.a("AvastGenericLic", "Received web view SSL error " + sslError);
        if (this.f534a.isAdded()) {
            this.f534a.d();
            sslErrorHandler.cancel();
        }
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        z.a("AvastGenericLic", "Purchase web view starts loading URL " + str);
    }

    public void onPageFinished(WebView webView, String str) {
        z.a("AvastGenericLic", "Purchase web view finished loading URL " + str);
    }
}
