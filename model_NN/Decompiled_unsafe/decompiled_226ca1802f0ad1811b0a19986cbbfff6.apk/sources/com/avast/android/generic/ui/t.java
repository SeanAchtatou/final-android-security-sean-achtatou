package com.avast.android.generic.ui;

import android.os.Handler;
import android.os.Message;

/* compiled from: PasswordDialog */
final class t implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ac f1070a;

    t(ac acVar) {
        this.f1070a = acVar;
    }

    public boolean handleMessage(Message message) {
        if (message.arg1 == PasswordDialog.f993a) {
            this.f1070a.a();
            return true;
        } else if (message.arg1 != PasswordDialog.f994b) {
            return false;
        } else {
            this.f1070a.b();
            return true;
        }
    }
}
