package com.avast.android.generic.app.about;

/* compiled from: FeedbackUtils */
public class r {
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r2.flush();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x007b, code lost:
        if (r2 == null) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r2.flush();
        r2.closeEntry();
        r2.close();
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0094 A[SYNTHETIC, Splitter:B:27:0x0094] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b9 A[SYNTHETIC, Splitter:B:36:0x00b9] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00c4 A[SYNTHETIC, Splitter:B:39:0x00c4] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d0 A[SYNTHETIC, Splitter:B:42:0x00d0] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00dd A[SYNTHETIC, Splitter:B:48:0x00dd] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00e8 A[SYNTHETIC, Splitter:B:51:0x00e8] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00f3 A[SYNTHETIC, Splitter:B:54:0x00f3] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a() {
        /*
            r10 = 18
            r0 = 0
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            java.lang.String r2 = "MM-dd HH:mm:ss.SSS"
            r1.<init>(r2)
            java.util.Calendar r2 = java.util.Calendar.getInstance()
            java.util.Date r5 = r2.getTime()
            r3 = 12
            r4 = -30
            r2.add(r3, r4)
            java.util.Date r6 = r2.getTime()
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x010e, all -> 0x00d6 }
            r3.<init>()     // Catch:{ IOException -> 0x010e, all -> 0x00d6 }
            java.util.zip.ZipOutputStream r2 = new java.util.zip.ZipOutputStream     // Catch:{ IOException -> 0x0113, all -> 0x00ff }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0113, all -> 0x00ff }
            r4 = 8
            r2.setMethod(r4)     // Catch:{ IOException -> 0x0117, all -> 0x0104 }
            java.util.zip.ZipEntry r4 = new java.util.zip.ZipEntry     // Catch:{ IOException -> 0x0117, all -> 0x0104 }
            java.lang.String r7 = "log.txt"
            r4.<init>(r7)     // Catch:{ IOException -> 0x0117, all -> 0x0104 }
            r2.putNextEntry(r4)     // Catch:{ IOException -> 0x0117, all -> 0x0104 }
            java.lang.Runtime r4 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0117, all -> 0x0104 }
            java.lang.String r7 = "logcat -d -v time *:V"
            java.lang.Process r7 = r4.exec(r7)     // Catch:{ IOException -> 0x0117, all -> 0x0104 }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0117, all -> 0x0104 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0117, all -> 0x0104 }
            java.io.InputStream r7 = r7.getInputStream()     // Catch:{ IOException -> 0x0117, all -> 0x0104 }
            r8.<init>(r7)     // Catch:{ IOException -> 0x0117, all -> 0x0104 }
            r4.<init>(r8)     // Catch:{ IOException -> 0x0117, all -> 0x0104 }
        L_0x004e:
            java.lang.String r7 = r4.readLine()     // Catch:{ IOException -> 0x00b3 }
            if (r7 == 0) goto L_0x0078
            int r8 = r7.length()     // Catch:{ ParseException -> 0x0120 }
            if (r8 < r10) goto L_0x004e
            r8 = 0
            r9 = 18
            java.lang.String r8 = r7.substring(r8, r9)     // Catch:{ ParseException -> 0x0120 }
            java.util.Date r8 = r1.parse(r8)     // Catch:{ ParseException -> 0x0120 }
            int r9 = r6.getYear()     // Catch:{ ParseException -> 0x0120 }
            r8.setYear(r9)     // Catch:{ ParseException -> 0x0120 }
            boolean r9 = r8.before(r6)     // Catch:{ ParseException -> 0x0120 }
            if (r9 != 0) goto L_0x004e
            boolean r8 = r8.after(r5)     // Catch:{ ParseException -> 0x0120 }
            if (r8 == 0) goto L_0x0098
        L_0x0078:
            r2.flush()     // Catch:{ IOException -> 0x00b3 }
            if (r2 == 0) goto L_0x0086
            r2.flush()     // Catch:{ IOException -> 0x011d }
            r2.closeEntry()     // Catch:{ IOException -> 0x011d }
            r2.close()     // Catch:{ IOException -> 0x011d }
        L_0x0086:
            if (r3 == 0) goto L_0x0092
            r3.flush()     // Catch:{ IOException -> 0x011a }
            byte[] r0 = r3.toByteArray()     // Catch:{ IOException -> 0x011a }
            r3.close()     // Catch:{ IOException -> 0x011a }
        L_0x0092:
            if (r4 == 0) goto L_0x0097
            r4.close()     // Catch:{ IOException -> 0x00f7 }
        L_0x0097:
            return r0
        L_0x0098:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00b3 }
            r8.<init>()     // Catch:{ IOException -> 0x00b3 }
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ IOException -> 0x00b3 }
            java.lang.String r8 = "\n"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x00b3 }
            java.lang.String r7 = r7.toString()     // Catch:{ IOException -> 0x00b3 }
            byte[] r7 = r7.getBytes()     // Catch:{ IOException -> 0x00b3 }
            r2.write(r7)     // Catch:{ IOException -> 0x00b3 }
            goto L_0x004e
        L_0x00b3:
            r1 = move-exception
        L_0x00b4:
            r1.printStackTrace()     // Catch:{ all -> 0x0108 }
            if (r2 == 0) goto L_0x00c2
            r2.flush()     // Catch:{ IOException -> 0x010c }
            r2.closeEntry()     // Catch:{ IOException -> 0x010c }
            r2.close()     // Catch:{ IOException -> 0x010c }
        L_0x00c2:
            if (r3 == 0) goto L_0x00ce
            r3.flush()     // Catch:{ IOException -> 0x010a }
            byte[] r0 = r3.toByteArray()     // Catch:{ IOException -> 0x010a }
            r3.close()     // Catch:{ IOException -> 0x010a }
        L_0x00ce:
            if (r4 == 0) goto L_0x0097
            r4.close()     // Catch:{ IOException -> 0x00d4 }
            goto L_0x0097
        L_0x00d4:
            r1 = move-exception
            goto L_0x0097
        L_0x00d6:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r4 = r0
            r0 = r1
        L_0x00db:
            if (r2 == 0) goto L_0x00e6
            r2.flush()     // Catch:{ IOException -> 0x00fd }
            r2.closeEntry()     // Catch:{ IOException -> 0x00fd }
            r2.close()     // Catch:{ IOException -> 0x00fd }
        L_0x00e6:
            if (r3 == 0) goto L_0x00f1
            r3.flush()     // Catch:{ IOException -> 0x00fb }
            r3.toByteArray()     // Catch:{ IOException -> 0x00fb }
            r3.close()     // Catch:{ IOException -> 0x00fb }
        L_0x00f1:
            if (r4 == 0) goto L_0x00f6
            r4.close()     // Catch:{ IOException -> 0x00f9 }
        L_0x00f6:
            throw r0
        L_0x00f7:
            r1 = move-exception
            goto L_0x0097
        L_0x00f9:
            r1 = move-exception
            goto L_0x00f6
        L_0x00fb:
            r1 = move-exception
            goto L_0x00f1
        L_0x00fd:
            r1 = move-exception
            goto L_0x00e6
        L_0x00ff:
            r1 = move-exception
            r2 = r0
            r4 = r0
            r0 = r1
            goto L_0x00db
        L_0x0104:
            r1 = move-exception
            r4 = r0
            r0 = r1
            goto L_0x00db
        L_0x0108:
            r0 = move-exception
            goto L_0x00db
        L_0x010a:
            r1 = move-exception
            goto L_0x00ce
        L_0x010c:
            r1 = move-exception
            goto L_0x00c2
        L_0x010e:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r4 = r0
            goto L_0x00b4
        L_0x0113:
            r1 = move-exception
            r2 = r0
            r4 = r0
            goto L_0x00b4
        L_0x0117:
            r1 = move-exception
            r4 = r0
            goto L_0x00b4
        L_0x011a:
            r1 = move-exception
            goto L_0x0092
        L_0x011d:
            r1 = move-exception
            goto L_0x0086
        L_0x0120:
            r7 = move-exception
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.app.about.r.a():byte[]");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:115:0x017d, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x017e, code lost:
        r2 = null;
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00fc, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00fd, code lost:
        r2 = null;
        r4 = null;
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r2.flush();
        r2.closeEntry();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0059 A[SYNTHETIC, Splitter:B:16:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0061 A[SYNTHETIC, Splitter:B:19:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x009f A[Catch:{ IOException -> 0x00ba, all -> 0x0158 }, LOOP:1: B:30:0x0099->B:33:0x009f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c0 A[SYNTHETIC, Splitter:B:38:0x00c0] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00cb A[SYNTHETIC, Splitter:B:41:0x00cb] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00d7 A[SYNTHETIC, Splitter:B:44:0x00d7] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00fc A[ExcHandler: all (r1v3 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0102 A[SYNTHETIC, Splitter:B:63:0x0102] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x010a A[SYNTHETIC, Splitter:B:66:0x010a] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x010e A[EDGE_INSN: B:69:0x010e->B:70:? ?: BREAK  , SYNTHETIC, Splitter:B:69:0x010e] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0114 A[SYNTHETIC, Splitter:B:73:0x0114] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x011f A[SYNTHETIC, Splitter:B:76:0x011f] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x012b A[SYNTHETIC, Splitter:B:79:0x012b] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0134 A[SYNTHETIC, Splitter:B:84:0x0134] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x013f A[SYNTHETIC, Splitter:B:87:0x013f] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x014a A[SYNTHETIC, Splitter:B:90:0x014a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] b() {
        /*
            r0 = 0
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0177, all -> 0x00fc }
            r3.<init>()     // Catch:{ IOException -> 0x0177, all -> 0x00fc }
            java.util.zip.ZipOutputStream r2 = new java.util.zip.ZipOutputStream     // Catch:{ IOException -> 0x017d, all -> 0x00fc }
            r2.<init>(r3)     // Catch:{ IOException -> 0x017d, all -> 0x00fc }
            r1 = 8
            r2.setMethod(r1)     // Catch:{ IOException -> 0x0182, all -> 0x016e }
            java.util.zip.ZipEntry r1 = new java.util.zip.ZipEntry     // Catch:{ IOException -> 0x0182, all -> 0x016e }
            java.lang.String r4 = "dumpsys.txt"
            r1.<init>(r4)     // Catch:{ IOException -> 0x0182, all -> 0x016e }
            r2.putNextEntry(r1)     // Catch:{ IOException -> 0x0182, all -> 0x016e }
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0182, all -> 0x016e }
            java.lang.String r4 = "dumpsys"
            java.lang.Process r1 = r1.exec(r4)     // Catch:{ IOException -> 0x0182, all -> 0x016e }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0182, all -> 0x016e }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0182, all -> 0x016e }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ IOException -> 0x0182, all -> 0x016e }
            r5.<init>(r1)     // Catch:{ IOException -> 0x0182, all -> 0x016e }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0182, all -> 0x016e }
        L_0x0032:
            java.lang.String r1 = r4.readLine()     // Catch:{ IOException -> 0x0053 }
            if (r1 == 0) goto L_0x00db
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0053 }
            r5.<init>()     // Catch:{ IOException -> 0x0053 }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ IOException -> 0x0053 }
            java.lang.String r5 = "\n"
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ IOException -> 0x0053 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0053 }
            byte[] r1 = r1.getBytes()     // Catch:{ IOException -> 0x0053 }
            r2.write(r1)     // Catch:{ IOException -> 0x0053 }
            goto L_0x0032
        L_0x0053:
            r1 = move-exception
        L_0x0054:
            r1.printStackTrace()     // Catch:{ all -> 0x0172 }
            if (r2 == 0) goto L_0x005f
            r2.flush()     // Catch:{ IOException -> 0x0174 }
            r2.closeEntry()     // Catch:{ IOException -> 0x0174 }
        L_0x005f:
            if (r4 == 0) goto L_0x0064
            r4.close()     // Catch:{ IOException -> 0x00f6 }
        L_0x0064:
            r5 = r3
            r3 = r4
            r4 = r2
        L_0x0067:
            java.util.zip.ZipEntry r1 = new java.util.zip.ZipEntry     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            java.lang.String r2 = "anr_traces.txt"
            r1.<init>(r2)     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            r4.putNextEntry(r1)     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            java.lang.String r2 = "/data/anr/traces.txt"
            r1.<init>(r2)     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            boolean r2 = r1.exists()     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            if (r2 == 0) goto L_0x0112
            boolean r2 = r1.isFile()     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            if (r2 == 0) goto L_0x0112
            boolean r2 = r1.canRead()     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            if (r2 == 0) goto L_0x0112
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            r7.<init>(r1)     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            r6.<init>(r7)     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
            r2.<init>(r6)     // Catch:{ IOException -> 0x0164, all -> 0x0131 }
        L_0x0099:
            java.lang.String r1 = r2.readLine()     // Catch:{ IOException -> 0x00ba, all -> 0x0158 }
            if (r1 == 0) goto L_0x010e
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00ba, all -> 0x0158 }
            r3.<init>()     // Catch:{ IOException -> 0x00ba, all -> 0x0158 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ IOException -> 0x00ba, all -> 0x0158 }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ IOException -> 0x00ba, all -> 0x0158 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00ba, all -> 0x0158 }
            byte[] r1 = r1.getBytes()     // Catch:{ IOException -> 0x00ba, all -> 0x0158 }
            r4.write(r1)     // Catch:{ IOException -> 0x00ba, all -> 0x0158 }
            goto L_0x0099
        L_0x00ba:
            r1 = move-exception
        L_0x00bb:
            r1.printStackTrace()     // Catch:{ all -> 0x015b }
            if (r4 == 0) goto L_0x00c9
            r4.flush()     // Catch:{ IOException -> 0x0161 }
            r4.closeEntry()     // Catch:{ IOException -> 0x0161 }
            r4.close()     // Catch:{ IOException -> 0x0161 }
        L_0x00c9:
            if (r5 == 0) goto L_0x00d5
            r5.flush()     // Catch:{ IOException -> 0x015e }
            byte[] r0 = r5.toByteArray()     // Catch:{ IOException -> 0x015e }
            r5.close()     // Catch:{ IOException -> 0x015e }
        L_0x00d5:
            if (r2 == 0) goto L_0x00da
            r2.close()     // Catch:{ IOException -> 0x0150 }
        L_0x00da:
            return r0
        L_0x00db:
            r2.flush()     // Catch:{ IOException -> 0x0053 }
            if (r2 == 0) goto L_0x00e6
            r2.flush()     // Catch:{ IOException -> 0x0186 }
            r2.closeEntry()     // Catch:{ IOException -> 0x0186 }
        L_0x00e6:
            if (r4 == 0) goto L_0x00eb
            r4.close()     // Catch:{ IOException -> 0x00f0 }
        L_0x00eb:
            r5 = r3
            r3 = r4
            r4 = r2
            goto L_0x0067
        L_0x00f0:
            r1 = move-exception
            r5 = r3
            r3 = r4
            r4 = r2
            goto L_0x0067
        L_0x00f6:
            r1 = move-exception
            r5 = r3
            r3 = r4
            r4 = r2
            goto L_0x0067
        L_0x00fc:
            r1 = move-exception
            r2 = r0
            r4 = r0
            r0 = r1
        L_0x0100:
            if (r2 == 0) goto L_0x0108
            r2.flush()     // Catch:{ IOException -> 0x016c }
            r2.closeEntry()     // Catch:{ IOException -> 0x016c }
        L_0x0108:
            if (r4 == 0) goto L_0x010d
            r4.close()     // Catch:{ IOException -> 0x014e }
        L_0x010d:
            throw r0
        L_0x010e:
            r4.flush()     // Catch:{ IOException -> 0x00ba, all -> 0x0158 }
            r3 = r2
        L_0x0112:
            if (r4 == 0) goto L_0x011d
            r4.flush()     // Catch:{ IOException -> 0x016a }
            r4.closeEntry()     // Catch:{ IOException -> 0x016a }
            r4.close()     // Catch:{ IOException -> 0x016a }
        L_0x011d:
            if (r5 == 0) goto L_0x0129
            r5.flush()     // Catch:{ IOException -> 0x0168 }
            byte[] r0 = r5.toByteArray()     // Catch:{ IOException -> 0x0168 }
            r5.close()     // Catch:{ IOException -> 0x0168 }
        L_0x0129:
            if (r3 == 0) goto L_0x00da
            r3.close()     // Catch:{ IOException -> 0x012f }
            goto L_0x00da
        L_0x012f:
            r1 = move-exception
            goto L_0x00da
        L_0x0131:
            r0 = move-exception
        L_0x0132:
            if (r4 == 0) goto L_0x013d
            r4.flush()     // Catch:{ IOException -> 0x0156 }
            r4.closeEntry()     // Catch:{ IOException -> 0x0156 }
            r4.close()     // Catch:{ IOException -> 0x0156 }
        L_0x013d:
            if (r5 == 0) goto L_0x0148
            r5.flush()     // Catch:{ IOException -> 0x0154 }
            r5.toByteArray()     // Catch:{ IOException -> 0x0154 }
            r5.close()     // Catch:{ IOException -> 0x0154 }
        L_0x0148:
            if (r3 == 0) goto L_0x014d
            r3.close()     // Catch:{ IOException -> 0x0152 }
        L_0x014d:
            throw r0
        L_0x014e:
            r1 = move-exception
            goto L_0x010d
        L_0x0150:
            r1 = move-exception
            goto L_0x00da
        L_0x0152:
            r1 = move-exception
            goto L_0x014d
        L_0x0154:
            r1 = move-exception
            goto L_0x0148
        L_0x0156:
            r1 = move-exception
            goto L_0x013d
        L_0x0158:
            r0 = move-exception
            r3 = r2
            goto L_0x0132
        L_0x015b:
            r0 = move-exception
            r3 = r2
            goto L_0x0132
        L_0x015e:
            r1 = move-exception
            goto L_0x00d5
        L_0x0161:
            r1 = move-exception
            goto L_0x00c9
        L_0x0164:
            r1 = move-exception
            r2 = r3
            goto L_0x00bb
        L_0x0168:
            r1 = move-exception
            goto L_0x0129
        L_0x016a:
            r1 = move-exception
            goto L_0x011d
        L_0x016c:
            r1 = move-exception
            goto L_0x0108
        L_0x016e:
            r1 = move-exception
            r4 = r0
            r0 = r1
            goto L_0x0100
        L_0x0172:
            r0 = move-exception
            goto L_0x0100
        L_0x0174:
            r1 = move-exception
            goto L_0x005f
        L_0x0177:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r4 = r0
            goto L_0x0054
        L_0x017d:
            r1 = move-exception
            r2 = r0
            r4 = r0
            goto L_0x0054
        L_0x0182:
            r1 = move-exception
            r4 = r0
            goto L_0x0054
        L_0x0186:
            r1 = move-exception
            goto L_0x00e6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.app.about.r.b():byte[]");
    }
}
