package com.avast.android.generic.app.account;

import android.os.Bundle;
import android.view.View;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ui.widget.NextRow;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.x;

public abstract class AccountSettingsFragment extends DisconnectFragment {

    /* renamed from: a  reason: collision with root package name */
    private NextRow f356a;

    /* renamed from: b  reason: collision with root package name */
    private NextRow f357b;

    public abstract int e();

    public abstract int f();

    public void onViewCreated(View view, Bundle bundle) {
        View b2;
        super.onViewCreated(view, bundle);
        if (!ak.b(getActivity()) && (b2 = b(view)) != null) {
            b2.setVisibility(8);
        }
        this.f356a = (NextRow) view.findViewById(e());
        this.f357b = (NextRow) view.findViewById(f());
        this.f356a.setOnClickListener(new c(this));
    }

    public void onResume() {
        super.onResume();
        d_();
    }

    /* access modifiers changed from: protected */
    public void d_() {
        String v = ((ab) aa.a(getActivity(), ab.class)).v();
        if (v == null || "".equals(v)) {
            this.f356a.c(getString(x.pref_account_not_connected));
            this.f357b.setEnabled(false);
            return;
        }
        this.f356a.c(getString(x.pref_account_connected, v));
        this.f357b.setEnabled(true);
    }
}
