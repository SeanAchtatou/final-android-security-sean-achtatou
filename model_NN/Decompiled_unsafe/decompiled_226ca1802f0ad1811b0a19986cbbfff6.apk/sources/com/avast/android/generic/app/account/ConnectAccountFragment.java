package com.avast.android.generic.app.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.g.b.a;
import com.avast.android.generic.o;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.ui.c.b;
import com.avast.android.generic.ui.widget.CheckBoxRow;
import com.avast.android.generic.util.ae;
import com.avast.android.generic.util.ga.TrackedMultiToolFragment;
import com.avast.android.generic.x;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class ConnectAccountFragment extends TrackedMultiToolFragment {

    /* renamed from: a  reason: collision with root package name */
    private static String f358a = null;

    /* renamed from: b  reason: collision with root package name */
    private static String f359b = null;
    private static final Pattern i = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+");
    private ProgressDialog A;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public r f360c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public boolean e = true;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public long g;
    private String h;
    /* access modifiers changed from: private */
    public Context j = null;
    /* access modifiers changed from: private */
    public ScrollView k;
    /* access modifiers changed from: private */
    public AutoCompleteTextView l;
    /* access modifiers changed from: private */
    public ImageView m;
    /* access modifiers changed from: private */
    public TextView n;
    private View o;
    /* access modifiers changed from: private */
    public boolean p = false;
    /* access modifiers changed from: private */
    public EditText q;
    /* access modifiers changed from: private */
    public EditText r;
    /* access modifiers changed from: private */
    public View s;
    /* access modifiers changed from: private */
    public ImageView t;
    /* access modifiers changed from: private */
    public TextView u;
    /* access modifiers changed from: private */
    public Button v;
    /* access modifiers changed from: private */
    public Handler w;
    /* access modifiers changed from: private */
    public CheckBoxRow x;
    /* access modifiers changed from: private */
    public boolean y;
    /* access modifiers changed from: private */
    public TextView z;

    public final String c() {
        return "account/connectAccount";
    }

    public int a() {
        return x.l_avast_account;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.w = new Handler();
        setRetainInstance(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.k = (ScrollView) layoutInflater.inflate(t.fragment_connect_account, viewGroup, false);
        return this.k;
    }

    @TargetApi(5)
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.j = getActivity();
        this.d = getArguments().getBoolean("wizard", false);
        this.e = getArguments().getBoolean("queryPhoneNumber", true);
        this.f = getArguments().getString("facebookToken");
        this.g = getArguments().getLong("facebookTokenExpirationTime");
        this.h = getArguments().getString("googlePlusToken");
        View findViewById = view.findViewById(r.avast_account_username_wrapper);
        this.l = (AutoCompleteTextView) view.findViewById(r.avast_account_username);
        this.m = (ImageView) view.findViewById(r.avast_account_usernameImage);
        this.n = (TextView) view.findViewById(r.avast_account_username_error_message);
        this.l.addTextChangedListener(new q(this, null));
        this.o = view.findViewById(r.avast_account_password_wrapper);
        this.q = (EditText) view.findViewById(r.avast_account_password1);
        ((TextView) view.findViewById(r.avast_account_forgot_password)).setMovementMethod(LinkMovementMethod.getInstance());
        this.y = a.j(this.j);
        this.t = (ImageView) view.findViewById(r.avast_account_phone_numberImage);
        this.x = (CheckBoxRow) view.findViewById(r.avast_account_has_phone_number);
        this.u = (TextView) view.findViewById(r.avast_account_phone_number_error_message);
        p pVar = new p(this, null);
        this.r = (EditText) view.findViewById(r.avast_account_phone_number);
        this.r.addTextChangedListener(pVar);
        this.s = view.findViewById(r.avast_account_phone_number_wrapper);
        this.z = (TextView) view.findViewById(r.avast_account_setup_hint_wrapper);
        a(view, pVar, this.e, !this.y);
        this.v = (Button) view.findViewById(r.avast_account_button_connect);
        this.v.setOnClickListener(new g(this));
        ((Button) view.findViewById(r.y)).setOnClickListener(new h(this));
        if (o() || p()) {
            findViewById.setVisibility(8);
        } else if (this.l.getAdapter() == null) {
            String[] strArr = (String[]) a(this.j).toArray(new String[0]);
            if (strArr.length > 0) {
                this.l.setAdapter(new ArrayAdapter(this.j, t.dropdown_item, strArr));
                if (TextUtils.isEmpty(f358a)) {
                    this.l.setText(strArr[0]);
                }
            }
        }
        g();
    }

    private static Set<String> a(Context context) {
        HashSet hashSet = new HashSet();
        for (Account account : ((AccountManager) context.getSystemService("account")).getAccounts()) {
            if (account.type.equals("com.google")) {
                hashSet.add(account.name);
            }
        }
        return hashSet;
    }

    private void a(View view, p pVar, boolean z2, boolean z3) {
        LinearLayout linearLayout = (LinearLayout) view.findViewById(r.avast_account_has_phone_number_wrapper);
        if (z3 && z2) {
            linearLayout.setVisibility(0);
            this.s.setVisibility(8);
            this.z.setVisibility(8);
            this.x.a(false);
        } else if (z2) {
            linearLayout.setVisibility(8);
            this.s.setVisibility(0);
            this.z.setVisibility(0);
            this.z.setText(getString(x.l_avast_account_setup_why_is_phone_number_needed));
            this.x.a(true);
        } else {
            linearLayout.setVisibility(8);
            this.s.setVisibility(8);
            this.z.setVisibility(8);
        }
        this.x.a(new i(this, pVar));
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        getActivity().getWindow().setSoftInputMode(1);
        if (this.f360c != null) {
            d();
        }
    }

    public void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(f358a)) {
            this.l.setText(f358a);
        }
        if (!TextUtils.isEmpty(f359b)) {
            this.r.setText(f359b);
        }
    }

    public void onPause() {
        super.onPause();
        f358a = this.l.getText().toString();
        f359b = this.r.getText().toString();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.f360c != null) {
            this.f360c.b();
        }
    }

    public void onDetach() {
        super.onDetach();
        e();
    }

    private void d() {
        e();
        if (isAdded()) {
            this.A = new ProgressDialog(getActivity());
            this.A.setCancelable(true);
            this.A.setOnCancelListener(new j(this));
            this.A.setMessage(getString(x.l_avast_account_connecting));
            this.A.show();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.A != null) {
            if (isAdded()) {
                this.A.dismiss();
            }
            this.A = null;
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, String str3) {
        if (isAdded()) {
            this.f360c = new k(this, getActivity(), getActivity().getApplicationContext());
            this.v.setEnabled(false);
            d();
            this.f360c.a(str, str2, str3);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, long j2, String str2) {
        if (isAdded()) {
            this.f360c = new m(this, getActivity(), getActivity().getApplicationContext());
            this.v.setEnabled(false);
            d();
            this.f360c.a(new bg(str, j2), str2);
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, String str, String str2, String str3, String str4, String str5, String str6) {
        f();
        ((ab) aa.a(this.j, ab.class)).a(context, str, str2, str3, str4, str5, str6);
        com.avast.android.generic.util.t.b("ConnectAccountFragment", "Sending avast! account connected broadcast.");
        Intent intent = new Intent("com.avast.android.mobilesecurity.app.account.ACCOUNT_CONNECTED");
        ae.a(intent);
        context.sendBroadcast(intent, "com.avast.android.generic.COMM_PERMISSION");
        if (isAdded()) {
            a(true);
        }
        String string = context.getString(x.pref_account_connected, str);
        if ("com.avast.android.mobilesecurity".equals(context.getPackageName())) {
            b.a(string);
        } else {
            Toast.makeText(context, string, 0).show();
        }
    }

    private void f() {
        String str = "email";
        if (o()) {
            str = "facebook";
        } else if (p()) {
            str = "googlePlus";
        }
        String str2 = "ms-Account";
        if (j()) {
            str2 = "at-Account";
        } else if (k()) {
            str2 = "backup-Account";
        }
        a(str2, "connected", str, 0);
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.p) {
            this.o.setVisibility(0);
            this.z.setVisibility(0);
            this.z.setText(getString(x.l_avast_account_setup_enter_password_or_change_email));
            this.z.setTextColor(getResources().getColor(o.text_warning));
            return;
        }
        this.o.setVisibility(8);
        this.z.setText(getString(x.l_avast_account_setup_why_is_phone_number_needed));
        this.z.setTextColor(getResources().getColor(o.text_subtitle));
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        if (isAdded()) {
            if (this.d) {
                String packageName = this.j.getPackageName();
                if (packageName.equals("com.avast.android.mobilesecurity")) {
                    try {
                        Class.forName("com.avast.android.mobilesecurity.app.wizard.ScanWizardActivity").getMethod("call", Context.class).invoke(null, getActivity());
                    } catch (Exception e2) {
                        com.avast.android.generic.util.t.d("ConnectAccountFragment", "ScanWizardActivity not available.", e2);
                    }
                } else if (packageName.equals("com.avast.android.backup")) {
                    try {
                        Class.forName("com.avast.android.backup.app.wizard.WizardStepOneActivity").getMethod("call", Context.class).invoke(null, getActivity());
                    } catch (Exception e3) {
                        com.avast.android.generic.util.t.d("ConnectAccountFragment", "HomeActivity not available.", e3);
                    }
                } else if (packageName.equals("com.avast.android.antitheft") || packageName.equals("com.avast.android.at_play")) {
                    try {
                        Class.forName("com.avast.android.antitheft.app.wizard.WizardStepAccountFragment").getMethod("launchNext", Context.class, Boolean.TYPE).invoke(null, getActivity(), Boolean.valueOf(z2));
                    } catch (Exception e4) {
                        com.avast.android.generic.util.t.d("ConnectAccountFragment", "HomeActivity not available.", e4);
                    }
                }
            }
            i();
        }
    }

    /* access modifiers changed from: private */
    public boolean h() {
        return i.matcher(this.l.getText().toString()).matches();
    }

    /* access modifiers changed from: private */
    public boolean n() {
        String c2 = com.avast.android.generic.util.ab.c(this.r.getText().toString());
        if (c2.startsWith("+") || c2.startsWith("00")) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean o() {
        return this.f != null;
    }

    private boolean p() {
        return this.h != null;
    }

    /* access modifiers changed from: private */
    public void q() {
        new Handler().post(new o(this));
    }
}
