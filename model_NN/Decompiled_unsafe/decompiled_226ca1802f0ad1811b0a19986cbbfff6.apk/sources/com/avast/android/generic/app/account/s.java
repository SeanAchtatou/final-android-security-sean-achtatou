package com.avast.android.generic.app.account;

import com.avast.android.generic.internet.c.e;
import com.avast.android.generic.util.b;

/* compiled from: ConnectAccountHelper */
class s extends aa {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f452a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ bg f453b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ r f454c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    s(r rVar, String str, bg bgVar) {
        super(rVar);
        this.f454c = rVar;
        this.f452a = str;
        this.f453b = bgVar;
    }

    public void a(String str) {
        v unused = this.f454c.f450b = new v(this.f454c, e.FACEBOOK, null, null, this.f452a, str, this.f453b.a(), this.f453b.b());
        b.a(this.f454c.f450b, new Void[0]);
    }

    public void b(String str) {
        this.f454c.a(str);
    }
}
