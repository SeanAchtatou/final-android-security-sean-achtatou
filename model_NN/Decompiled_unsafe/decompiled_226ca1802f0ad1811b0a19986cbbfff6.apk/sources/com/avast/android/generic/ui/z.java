package com.avast.android.generic.ui;

import android.content.DialogInterface;
import android.view.KeyEvent;

/* compiled from: PasswordDialog */
class z implements DialogInterface.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PasswordDialog f1163a;

    z(PasswordDialog passwordDialog) {
        this.f1163a = passwordDialog;
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i == 84) {
            return true;
        }
        if (i != 82 || !keyEvent.isLongPress()) {
            return false;
        }
        return true;
    }
}
