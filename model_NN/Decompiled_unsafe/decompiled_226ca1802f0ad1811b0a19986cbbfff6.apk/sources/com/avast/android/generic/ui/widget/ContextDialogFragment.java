package com.avast.android.generic.ui.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.util.ak;
import java.util.ArrayList;
import java.util.HashMap;

public class ContextDialogFragment extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private String[] f1085a = null;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public long[] f1086b = null;

    /* renamed from: c  reason: collision with root package name */
    private String f1087c = "";

    public ContextDialogFragment a(String[] strArr) {
        if (this.f1086b == null || strArr == null || this.f1086b.length == strArr.length) {
            this.f1085a = strArr;
            return this;
        }
        throw new IllegalArgumentException("Items array length doesn't match ID array length");
    }

    public ContextDialogFragment a(String str) {
        this.f1087c = str;
        return this;
    }

    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("title", this.f1087c);
        bundle.putStringArray("items", this.f1085a);
        bundle.putLongArray("ids", this.f1086b);
        super.onSaveInstanceState(bundle);
    }

    public Dialog onCreateDialog(Bundle bundle) {
        if (bundle != null) {
            this.f1087c = bundle.getString("title");
            this.f1085a = bundle.getStringArray("items");
            this.f1086b = bundle.getLongArray("ids");
        }
        Context c2 = ak.c(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(c2);
        if (!TextUtils.isEmpty(this.f1087c)) {
            builder.setTitle(this.f1087c);
        }
        ArrayList arrayList = new ArrayList();
        if (this.f1085a != null) {
            for (String put : this.f1085a) {
                HashMap hashMap = new HashMap();
                hashMap.put("name", put);
                arrayList.add(hashMap);
            }
        }
        ListView listView = new ListView(c2);
        listView.setAdapter((ListAdapter) new SimpleAdapter(c2, arrayList, t.list_item_context, new String[]{"name"}, new int[]{r.name}));
        listView.setOnItemClickListener(new e(this));
        builder.setView(listView);
        AlertDialog create = builder.create();
        create.setInverseBackgroundForced(true);
        return create;
    }
}
