package com.avast.android.generic.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ad;
import com.avast.android.generic.app.passwordrecovery.a;
import com.avast.android.generic.c.d;
import com.avast.android.generic.util.aw;
import com.avast.android.generic.util.z;

public class SMSListener extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String str;
        boolean z;
        String action = intent.getAction();
        z.a("AvastComms", context, "Received intent in SMS received listener: " + action);
        Context applicationContext = context.getApplicationContext();
        Bundle extras = intent.getExtras();
        if (action.equals("android.provider.Telephony.SMS_RECEIVED") && extras != null) {
            ab abVar = (ab) aa.a(applicationContext, ad.class);
            Object[] objArr = (Object[]) extras.get("pdus");
            int i = 0;
            String str2 = null;
            String str3 = "";
            while (i < objArr.length) {
                SmsMessage createFromPdu = SmsMessage.createFromPdu((byte[]) objArr[i]);
                try {
                    str3 = str3 + createFromPdu.getDisplayMessageBody();
                    str2 = createFromPdu.getOriginatingAddress();
                    i++;
                } catch (Exception e) {
                    z.a("AvastGeneric", "Error parsing SMS", e);
                    return;
                }
            }
            if (str3 != null && str2 != null && !str3.equals("")) {
                String trim = str3.trim();
                String trim2 = str2.trim();
                z.b(applicationContext, "SMS", "SMS received from " + trim2 + ": " + trim);
                int indexOf = trim.indexOf(" ");
                if (indexOf > -1) {
                    str = trim.substring(0, indexOf);
                } else {
                    str = trim;
                }
                boolean b2 = abVar.b(str);
                if (b2 || !abVar.t() || !abVar.y().equals(str)) {
                    z = b2;
                } else {
                    z = true;
                }
                if (!z && a.a(applicationContext, str)) {
                    z = true;
                }
                if (z) {
                    aw.a(applicationContext);
                    z.a(applicationContext, "SMS has correct code, will be dispatched");
                    Intent intent2 = new Intent();
                    intent2.setAction("com.avast.android.generic.service.action.SMS_RECEIVED");
                    intent2.putExtra("number", trim2);
                    intent2.putExtra("text", trim);
                    d.a(applicationContext, intent2, trim2, null, trim, true);
                    z.a("AvastComms", applicationContext, "Broadcast aborting...");
                    abortBroadcast();
                    z.a("AvastComms", applicationContext, "Broadcast aborted");
                }
            }
        }
    }
}
