package com.avast.android.generic.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

/* compiled from: PackageService */
public class aa {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f1167a = {"com.p1.chompsms", "com.texty.sms", "com.facebook.orca", "com.Aetherpal.Device", "com.locationlabs.v3client", "com.sp.protector.free", "com.netqin.ps", "com.myboyfriendisageek.gotya", "com.antivirus", "com.lsdroid.cerberus", "org.antivirus", "cn.menue.callblocker", "com.koushikdutta.desktopsms", "com.iobit.mobilecare", "CN.MyPrivateMessages", "com.Cluster.cluBalancePro", "com.motricity.verizon.ssodownloadable", "name.pilgr.android.pibalance", "com.skt.skaf.OA00199800", "tw.nicky.HDCallerID", "com.Cluster.cluBalance", "eu.inmite.apps.smsjizdenka", "com.drivemode", "com.google.android.talk", "com.sec.chaton"};

    /* renamed from: b  reason: collision with root package name */
    private static String f1168b = null;

    /* renamed from: c  reason: collision with root package name */
    private static Object f1169c = new Object();

    public static boolean a(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            context.getPackageManager().getPackageInfo(str, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean a(Context context) {
        return a(context, "com.avast.android.mobilesecurity");
    }

    public static boolean a(Context context, int i) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.avast.android.mobilesecurity", 0);
            if (packageInfo.versionName == null || packageInfo.versionCode < i) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static String b(Context context, int i) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.avast.android.antitheft", 0);
            if (packageInfo.versionName != null) {
                if (packageInfo.versionCode >= i) {
                    return packageInfo.packageName;
                }
                return null;
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        try {
            PackageInfo packageInfo2 = context.getPackageManager().getPackageInfo("com.avast.android.at_play", 0);
            if (packageInfo2.versionName == null || packageInfo2.versionCode < i) {
                return null;
            }
            return packageInfo2.packageName;
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    public static String b(Context context) {
        if (a(context, "com.avast.android.antitheft")) {
            return "com.avast.android.antitheft";
        }
        if (a(context, "com.avast.android.at_play")) {
            return "com.avast.android.at_play";
        }
        return null;
    }

    public static String c(Context context) {
        if (new File("/system/app/com.avast.android.antitheft.apk").exists() || new File("/system/priv-app/com.avast.android.antitheft.apk").exists() || a(context, "com.avast.android.antitheft")) {
            return "antitheft";
        }
        return null;
    }

    public static boolean c(Context context, int i) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.avast.android.backup", 0);
            if (packageInfo.versionName == null || packageInfo.versionCode < i) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean d(Context context) {
        return a(context, "com.avast.android.backup");
    }

    public static boolean d(Context context, int i) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.avast.android.vpn", 0);
            if (packageInfo.versionName == null || packageInfo.versionCode < i) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean e(Context context) {
        return a(context, "com.avast.android.vpn");
    }

    public static String a(Context context, Intent intent) {
        String dataString = intent.getDataString();
        if (dataString == null) {
            return null;
        }
        if (dataString.startsWith("package:")) {
            dataString = dataString.substring("package:".length());
        }
        if (dataString.equals(context.getPackageName())) {
            return null;
        }
        if (!dataString.startsWith("com.avast.android") || dataString.equals("com.avast.android.vps")) {
            return null;
        }
        return dataString;
    }

    public static void a(Context context, String str, String str2, String str3) {
        if (str3 != null) {
            z.a(context, "ALL", "Message " + str3);
            Intent intent = new Intent();
            intent.setAction("com.avast.android.generic.action.MESSAGE_TO_SUITE");
            if (str != null) {
                intent.putExtra("number", str);
            }
            intent.putExtra("text", str3);
            if (str2 != null) {
                intent.putExtra("uid", str2);
            }
            intent.putExtra("sourcePackage", context.getPackageName());
            ae.a(intent);
            context.sendOrderedBroadcast(intent, "com.avast.android.generic.COMM_PERMISSION");
        }
    }

    public static String a(Context context, String str, String str2) {
        String str3;
        int i;
        String str4;
        int i2;
        int i3 = 0;
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent();
        if (str2 == null) {
            intent.setAction("android.provider.Telephony.SMS_RECEIVED");
        } else {
            intent.setAction(str2);
        }
        String str5 = null;
        int i4 = 0;
        for (ResolveInfo next : packageManager.queryBroadcastReceivers(intent, 0)) {
            if (next.activityInfo != null) {
                str3 = next.activityInfo.packageName;
            } else if (next.serviceInfo != null) {
                str3 = next.serviceInfo.packageName;
            } else {
                str3 = null;
            }
            if (str3 != null) {
                if (str3.equals(context.getPackageName())) {
                    i = next.priority;
                    str4 = str5;
                    i2 = i4;
                } else if (!str3.equals(str) && packageManager.checkSignatures(str3, context.getPackageName()) == 0 && next.priority >= i4) {
                    i2 = next.priority;
                    i = i3;
                    str4 = str3;
                }
                i4 = i2;
                str5 = str4;
                i3 = i;
            }
            i = i3;
            str4 = str5;
            i2 = i4;
            i4 = i2;
            str5 = str4;
            i3 = i;
        }
        if (i4 > i3) {
            return str5;
        }
        return null;
    }

    public static void a(Context context, String str, Intent intent) {
        ae.a(context, intent, str);
    }

    public static List<String> f(Context context) {
        String str;
        String substring;
        PackageManager packageManager = context.getPackageManager();
        LinkedList linkedList = new LinkedList();
        Intent intent = new Intent();
        intent.setAction("android.provider.Telephony.SMS_RECEIVED");
        for (ResolveInfo next : packageManager.queryBroadcastReceivers(intent, 0)) {
            if (next.activityInfo != null) {
                str = next.activityInfo.packageName;
            } else if (next.serviceInfo != null) {
                str = next.serviceInfo.packageName;
            } else {
                str = null;
            }
            if (str != null && str.startsWith("com.avast.android") && packageManager.checkSignatures(str, context.getPackageName()) == 0) {
                if (str.equals("com.avast.android.antitheft")) {
                    substring = "AT";
                } else if (str.equals("com.avast.android.at_play")) {
                    substring = "AT";
                } else if (str.equals("com.avast.android.mobilesecurity")) {
                    substring = "MS";
                } else {
                    substring = str.substring(str.lastIndexOf(".") + 1);
                }
                if (!linkedList.contains(substring)) {
                    linkedList.add(substring);
                }
            }
        }
        return linkedList;
    }

    public static boolean b(Context context, String str) {
        PackageManager packageManager = context.getPackageManager();
        try {
            if (packageManager.getPackageInfo(str, 0) != null && packageManager.checkSignatures(str, context.getPackageName()) == 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean g(Context context) {
        return a(context, "com.koushikdutta.rommanager");
    }

    public static void h(Context context) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setComponent(new ComponentName("com.koushikdutta.rommanager", "com.koushikdutta.rommanager.RomManager"));
        context.startActivity(intent);
    }

    private static synchronized void k(Context context) {
        synchronized (aa.class) {
            String str = "";
            for (String str2 : f(context)) {
                str = str + str2 + ",";
            }
            f1168b = str;
        }
    }

    public static synchronized String i(Context context) {
        String str;
        synchronized (aa.class) {
            synchronized (f1169c) {
                if (f1168b == null) {
                    k(context);
                }
                str = f1168b;
            }
        }
        return str;
    }

    public static synchronized void a() {
        synchronized (aa.class) {
            synchronized (f1169c) {
                f1168b = null;
            }
        }
    }

    public static boolean j(Context context) {
        return c(context) != null && a(context, "com.avast.android.at_play");
    }
}
