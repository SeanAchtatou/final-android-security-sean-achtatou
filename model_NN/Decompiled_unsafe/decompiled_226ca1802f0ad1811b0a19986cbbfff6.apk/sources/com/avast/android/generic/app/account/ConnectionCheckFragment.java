package com.avast.android.generic.app.account;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ad;
import com.avast.android.generic.r;
import com.avast.android.generic.service.AvastService;
import com.avast.android.generic.t;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.ae;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.x;

public class ConnectionCheckFragment extends DisconnectFragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public TextView f362a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Button f363b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Button f364c;
    /* access modifiers changed from: private */
    public boolean d = false;
    private Context e;
    private Activity f;
    /* access modifiers changed from: private */
    public ab g;
    /* access modifiers changed from: private */
    public ab h;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public View.OnClickListener k = new af(this);
    /* access modifiers changed from: private */
    public View.OnClickListener l = new ag(this);
    private BroadcastReceiver m = new ah(this);

    public final String c() {
        return "account/connectionCheck";
    }

    /* access modifiers changed from: protected */
    public int d() {
        return r.message_avast_account_conncheck_pin_check;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(t.fragment_connectioncheck, viewGroup, false);
        if (!ak.b(getActivity())) {
            b(inflate).setVisibility(8);
        }
        Intent intent = new Intent("com.avast.android.mobilesecurity.app.account.PUSH_ACCOUNT_COMMUNICATION");
        ae.a(intent);
        getActivity().sendBroadcast(intent, "com.avast.android.generic.COMM_PERMISSION");
        this.f = getActivity();
        this.e = this.f.getApplicationContext();
        this.g = (ab) aa.a(this.e, ad.class);
        this.h = (ab) aa.a(this.e, com.avast.android.generic.ae.class);
        this.f362a = (TextView) inflate.findViewById(r.l_connection_check_state);
        this.f363b = (Button) inflate.findViewById(r.A);
        this.f364c = (Button) inflate.findViewById(r.y);
        this.f364c.setOnClickListener(new ai(this));
        if (getActivity().getPackageName().equals("com.avast.android.mobilesecurity") || getActivity().getPackageName().equals("com.avast.android.backup")) {
            a(2);
            this.i = false;
            this.j = false;
        } else {
            a(1);
            this.i = true;
            this.j = AvastService.a(getActivity());
        }
        return inflate;
    }

    public void a(int i2) {
        new aj(this, i2).c(new Void[0]);
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 100) {
            a.a(this);
        } else {
            a(i2);
        }
    }

    public void onDestroy() {
        f();
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void e() {
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("com.avast.android.generic.action.ACTION_CONNECTION_CHECK_RECEIVED");
            getActivity().registerReceiver(this.m, intentFilter);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        try {
            getActivity().unregisterReceiver(this.m);
        } catch (Exception e2) {
        }
    }

    public boolean handleMessage(Message message) {
        if (!isAdded() || isDetached()) {
            return false;
        }
        if (message.what == r.message_avast_account_disconnected) {
            a.a(this);
        }
        return true;
    }

    public int a() {
        return x.l_connectioncheck;
    }
}
