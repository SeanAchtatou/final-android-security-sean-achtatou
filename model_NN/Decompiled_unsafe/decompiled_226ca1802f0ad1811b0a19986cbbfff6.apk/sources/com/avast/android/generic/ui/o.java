package com.avast.android.generic.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;

/* compiled from: CustomNumberDialog */
class o implements DialogInterface.OnShowListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AlertDialog f1052a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ CustomNumberDialog f1053b;

    o(CustomNumberDialog customNumberDialog, AlertDialog alertDialog) {
        this.f1053b = customNumberDialog;
        this.f1052a = alertDialog;
    }

    public void onShow(DialogInterface dialogInterface) {
        this.f1052a.getButton(-1).setOnClickListener(new p(this));
    }
}
