package com.avast.android.generic.h;

import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: FeedbackProto */
public final class o extends GeneratedMessageLite.Builder<n, o> implements p {

    /* renamed from: a  reason: collision with root package name */
    private int f718a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f719b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private q f720c = q.CUSTOM_FEEDBACK;
    private Object d = "";
    private Object e = "";
    private Object f = "";
    private c g = c.a();
    private ByteString h = ByteString.EMPTY;
    private ByteString i = ByteString.EMPTY;

    private o() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static o i() {
        return new o();
    }

    /* renamed from: a */
    public o clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public n getDefaultInstanceForType() {
        return n.a();
    }

    /* renamed from: c */
    public n build() {
        n d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public n d() {
        int i2 = 1;
        n nVar = new n(this);
        int i3 = this.f718a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        ByteString unused = nVar.f717c = this.f719b;
        if ((i3 & 2) == 2) {
            i2 |= 2;
        }
        q unused2 = nVar.d = this.f720c;
        if ((i3 & 4) == 4) {
            i2 |= 4;
        }
        Object unused3 = nVar.e = this.d;
        if ((i3 & 8) == 8) {
            i2 |= 8;
        }
        Object unused4 = nVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 16;
        }
        Object unused5 = nVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 32;
        }
        c unused6 = nVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 64;
        }
        ByteString unused7 = nVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        ByteString unused8 = nVar.j = this.i;
        int unused9 = nVar.f716b = i2;
        return nVar;
    }

    /* renamed from: a */
    public o mergeFrom(n nVar) {
        if (nVar != n.a()) {
            if (nVar.b()) {
                a(nVar.c());
            }
            if (nVar.d()) {
                a(nVar.e());
            }
            if (nVar.f()) {
                a(nVar.g());
            }
            if (nVar.h()) {
                b(nVar.i());
            }
            if (nVar.j()) {
                c(nVar.k());
            }
            if (nVar.l()) {
                b(nVar.m());
            }
            if (nVar.n()) {
                b(nVar.o());
            }
            if (nVar.p()) {
                c(nVar.q());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public o mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f718a |= 1;
                    this.f719b = codedInputStream.readBytes();
                    break;
                case 16:
                    q a2 = q.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f718a |= 2;
                        this.f720c = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f718a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f718a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f718a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                case 50:
                    d P = c.P();
                    if (e()) {
                        P.mergeFrom(f());
                    }
                    codedInputStream.readMessage(P, extensionRegistryLite);
                    a(P.d());
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    this.f718a |= 64;
                    this.h = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f718a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public o a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f718a |= 1;
        this.f719b = byteString;
        return this;
    }

    public o a(q qVar) {
        if (qVar == null) {
            throw new NullPointerException();
        }
        this.f718a |= 2;
        this.f720c = qVar;
        return this;
    }

    public o a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f718a |= 4;
        this.d = str;
        return this;
    }

    public o b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f718a |= 8;
        this.e = str;
        return this;
    }

    public o c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f718a |= 16;
        this.f = str;
        return this;
    }

    public boolean e() {
        return (this.f718a & 32) == 32;
    }

    public c f() {
        return this.g;
    }

    public o a(c cVar) {
        if (cVar == null) {
            throw new NullPointerException();
        }
        this.g = cVar;
        this.f718a |= 32;
        return this;
    }

    public o b(c cVar) {
        if ((this.f718a & 32) != 32 || this.g == c.a()) {
            this.g = cVar;
        } else {
            this.g = c.a(this.g).mergeFrom(cVar).d();
        }
        this.f718a |= 32;
        return this;
    }

    public o b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f718a |= 64;
        this.h = byteString;
        return this;
    }

    public o c(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f718a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = byteString;
        return this;
    }
}
