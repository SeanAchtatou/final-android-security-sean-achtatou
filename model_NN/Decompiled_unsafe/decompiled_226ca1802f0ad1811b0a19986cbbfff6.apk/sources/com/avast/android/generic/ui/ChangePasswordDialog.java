package com.avast.android.generic.ui;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.avast.android.generic.aa;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.ui.b.a;
import com.avast.android.generic.ui.widget.PasswordTextView;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.util.y;
import com.avast.android.generic.x;

public class ChangePasswordDialog extends DialogFragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public PasswordTextView f983a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public PasswordTextView f984b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public TextView f985c;
    /* access modifiers changed from: private */
    public ImageView d;
    /* access modifiers changed from: private */
    public ImageView e;
    /* access modifiers changed from: private */
    public boolean f;

    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        a(r.message_password_change_canceled);
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        y yVar;
        FragmentActivity activity = getActivity();
        if (activity != null && (yVar = (y) aa.a(activity, y.class)) != null) {
            yVar.a(i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @TargetApi(8)
    public Dialog onCreateDialog(Bundle bundle) {
        Context c2 = ak.c(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(c2);
        builder.setTitle(getString(x.pref_password_change_title));
        View inflate = LayoutInflater.from(c2).inflate(t.dialog_password_change, (ViewGroup) null, false);
        this.f983a = (PasswordTextView) inflate.findViewById(r.password1);
        this.f984b = (PasswordTextView) inflate.findViewById(r.password2);
        this.d = (ImageView) inflate.findViewById(r.passwordImage1);
        this.e = (ImageView) inflate.findViewById(r.passwordImage2);
        this.f985c = (TextView) inflate.findViewById(r.passwordErrorMessage);
        TextWatcher a2 = a();
        this.f983a.addTextChangedListener(a2);
        this.f984b.addTextChangedListener(a2);
        this.d.setVisibility(4);
        this.e.setVisibility(4);
        InputFilter[] inputFilterArr = {new InputFilter.LengthFilter(6)};
        this.f983a.setFilters(inputFilterArr);
        this.f984b.setFilters(inputFilterArr);
        TextView textView = (TextView) inflate.findViewById(r.password_desc);
        a aVar = new a(textView);
        TextView textView2 = (TextView) inflate.findViewById(r.password_show_desc);
        CharSequence text = getText(x.pref_password_show_desc);
        CharSequence text2 = getText(x.pref_password_hide_desc);
        textView2.setText(text);
        textView2.setOnClickListener(new a(this, aVar, textView, textView2, text2, text));
        builder.setView(inflate);
        builder.setPositiveButton(x.bR, new b(this));
        builder.setNegativeButton(x.F, new c(this));
        AlertDialog create = builder.create();
        create.setInverseBackgroundForced(true);
        create.setOnShowListener(new d(this, create));
        return create;
    }

    /* access modifiers changed from: private */
    public boolean a(String str, String str2) {
        return str.length() >= 4 && str.length() <= 6 && str.equals(str2) && TextUtils.isDigitsOnly(str);
    }

    private TextWatcher a() {
        return new f(this);
    }
}
