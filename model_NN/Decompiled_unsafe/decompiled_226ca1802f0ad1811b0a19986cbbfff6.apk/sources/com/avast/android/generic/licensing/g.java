package com.avast.android.generic.licensing;

/* compiled from: OfferTask */
public class g<T> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public h f900a;

    /* renamed from: b  reason: collision with root package name */
    private T f901b;

    public g(h hVar) {
        this(hVar, null);
    }

    public g(h hVar, T t) {
        this.f901b = null;
        this.f900a = hVar;
        this.f901b = t;
    }

    public T a() {
        return this.f901b;
    }
}
