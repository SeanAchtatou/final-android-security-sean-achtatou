package com.avast.android.generic.ui.widget;

import a.a.a.a.a.a;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.app.subscription.WelcomePremiumActivity;
import com.avast.android.generic.licensing.l;
import com.avast.android.generic.licensing.m;
import com.avast.android.generic.q;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.x;

public class SubscriptionButton extends FrameLayout implements l {

    /* renamed from: a  reason: collision with root package name */
    public static Object f1116a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static m f1117b;

    /* renamed from: c  reason: collision with root package name */
    private Button f1118c;
    private View d;

    public SubscriptionButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public SubscriptionButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        inflate(getContext(), t.button_subscription, this);
        this.f1118c = (Button) findViewById(r.button_subscription_button);
        this.d = findViewById(r.button_subscription_progress);
    }

    public void a(m mVar, String str) {
        m mVar2;
        m mVar3;
        int paddingBottom = this.f1118c.getPaddingBottom();
        int paddingTop = this.f1118c.getPaddingTop();
        int paddingRight = this.f1118c.getPaddingRight();
        int paddingLeft = this.f1118c.getPaddingLeft();
        if (mVar == null) {
            mVar2 = f1117b;
        } else {
            mVar2 = mVar;
        }
        if (mVar2 == null) {
            mVar3 = m.UNKNOWN;
        } else {
            mVar3 = mVar2;
        }
        f1117b = mVar3;
        switch (z.f1159a[mVar3.ordinal()]) {
            case 1:
                this.f1118c.setText(x.l_subscription_button_premium);
                this.f1118c.setBackgroundResource(q.j);
                this.f1118c.setCompoundDrawablesWithIntrinsicBounds(0, 0, q.ic_scanner_result_ok, 0);
                if (!(str == null || getContext() == null)) {
                    try {
                        if (str.equals(getContext().getPackageName())) {
                            synchronized (f1116a) {
                                ab abVar = (ab) aa.a(getContext(), ab.class);
                                if (!abVar.V()) {
                                    abVar.T();
                                    WelcomePremiumActivity.a(getContext());
                                }
                            }
                            break;
                        }
                    } catch (Exception e) {
                        a.a().a("Can not open welcome premium activity", e);
                        break;
                    }
                }
                break;
            case 2:
                this.f1118c.setText(x.l_subscription_button_unknown);
                this.f1118c.setBackgroundResource(q.xml_btn_warning);
                this.f1118c.setCompoundDrawablesWithIntrinsicBounds(0, 0, q.ic_scanner_result_warning, 0);
                break;
            case 3:
                this.f1118c.setText(x.l_subscription_button_na);
                this.f1118c.setBackgroundResource(q.xml_btn_error);
                this.f1118c.setCompoundDrawablesWithIntrinsicBounds(0, 0, q.ic_scanner_result_problem, 0);
                break;
            case 4:
                b(true);
                break;
            case 5:
                this.f1118c.setText(x.l_dashboard_go_premium);
                this.f1118c.setBackgroundResource(q.j);
                this.f1118c.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                if (getContext() != null) {
                    try {
                        synchronized (f1116a) {
                            ((ab) aa.a(getContext(), ab.class)).U();
                        }
                        break;
                    } catch (Exception e2) {
                        a.a().a("Can not reset welcome premium activity", e2);
                        break;
                    }
                }
                break;
        }
        if (mVar3 != m.PROGRESS) {
            b(false);
        }
        this.f1118c.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }

    private void b(boolean z) {
        if (z) {
            this.f1118c.setVisibility(4);
            this.d.setVisibility(0);
            return;
        }
        this.f1118c.setVisibility(0);
        this.d.setVisibility(4);
    }

    public void a(long j) {
    }

    public void a(boolean z) {
    }

    public void a(String str) {
    }
}
