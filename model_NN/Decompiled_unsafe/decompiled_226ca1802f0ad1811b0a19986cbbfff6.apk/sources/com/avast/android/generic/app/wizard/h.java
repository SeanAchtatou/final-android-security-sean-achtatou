package com.avast.android.generic.app.wizard;

import android.view.View;
import com.avast.android.generic.util.a;

/* compiled from: WizardIntroduceAccountFragment */
class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WizardIntroduceAccountFragment f596a;

    h(WizardIntroduceAccountFragment wizardIntroduceAccountFragment) {
        this.f596a = wizardIntroduceAccountFragment;
    }

    public void onClick(View view) {
        a.a(this.f596a);
        this.f596a.launchNext();
    }
}
