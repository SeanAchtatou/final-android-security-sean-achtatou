package com.avast.android.generic.ui;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: PasswordDialog */
class ab implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PasswordDialog f1006a;

    ab(PasswordDialog passwordDialog) {
        this.f1006a = passwordDialog;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        if (this.f1006a.e.getVisibility() != 8) {
            this.f1006a.e.setVisibility(8);
        }
        if (editable.length() >= 4) {
            this.f1006a.j.sendEmptyMessageDelayed(1, 500);
        }
    }
}
