package com.avast.android.generic.util;

/* compiled from: BaseTracker */
public enum h {
    EASY_INSTALL("easy_install"),
    ADVANCED_INSTALL("advanced_install");
    

    /* renamed from: c  reason: collision with root package name */
    String f1241c;

    private h(String str) {
        this.f1241c = str;
    }

    public String a() {
        return this.f1241c;
    }
}
