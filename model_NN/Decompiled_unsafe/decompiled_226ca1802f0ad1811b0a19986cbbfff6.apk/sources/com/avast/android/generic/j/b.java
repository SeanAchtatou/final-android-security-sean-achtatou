package com.avast.android.generic.j;

import a.a.a.a.a.a;
import android.content.Context;
import android.os.Bundle;

/* compiled from: Task */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f830a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f831b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ Bundle f832c;
    final /* synthetic */ c d;
    final /* synthetic */ a e;

    b(a aVar, Context context, String str, Bundle bundle, c cVar) {
        this.e = aVar;
        this.f830a = context;
        this.f831b = str;
        this.f832c = bundle;
        this.d = cVar;
    }

    public void run() {
        try {
            this.e.a(this.f830a, this.f831b, this.f832c);
        } catch (Exception e2) {
            a.a().a("Exception in executing task in own thread", e2);
        }
        this.d.a();
    }
}
