package com.avast.android.generic.ui.c;

import b.a.a.a.a.i;

/* compiled from: CroutonQueue */
class d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public CharSequence f1026a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1027b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public i f1028c;
    /* access modifiers changed from: private */
    public int d;

    private d(CharSequence charSequence, int i, int i2, i iVar) {
        this.f1026a = charSequence;
        this.f1027b = i;
        this.d = i2;
        this.f1028c = iVar;
    }
}
