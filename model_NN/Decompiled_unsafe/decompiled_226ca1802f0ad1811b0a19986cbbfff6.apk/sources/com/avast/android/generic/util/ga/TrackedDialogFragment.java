package com.avast.android.generic.util.ga;

import com.actionbarsherlock.app.SherlockDialogFragment;

public abstract class TrackedDialogFragment extends SherlockDialogFragment {
    public abstract String a();

    public void onResume() {
        super.onResume();
        c a2 = a.a();
        String a3 = a();
        if (a3 != null) {
            a2.a(a3);
        }
    }
}
