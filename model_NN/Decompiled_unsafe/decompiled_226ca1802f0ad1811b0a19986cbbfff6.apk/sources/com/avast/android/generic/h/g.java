package com.avast.android.generic.h;

import com.google.protobuf.Internal;

/* compiled from: CommunityIqProto */
public enum g implements Internal.EnumLite {
    UPDATE_CHECK_RESULT_UPDATE_AVAILABLE(0, 1),
    UPDATE_CHECK_RESULT_UP_TO_DATE(1, 2),
    UPDATE_CHECK_ERROR_OLD_APPLICATION_VERSION(2, 3),
    UPDATE_CHECK_ERROR_CONNECTION_PROBLEMS(3, 4),
    UPDATE_CHECK_ERROR_SIGNATURE_NOT_VALID(4, 5),
    UPDATE_CHECK_ERROR_WRONG_PROTO_FILE(5, 6),
    UPDATE_CHECK_ERROR_BROKEN_VERSION_STRINGS(6, 7),
    UPDATE_CHECK_ERROR_CURRENT_VPS_INVALID(7, 8);
    
    private static Internal.EnumLiteMap<g> i = new h();
    private final int j;

    public final int getNumber() {
        return this.j;
    }

    public static g a(int i2) {
        switch (i2) {
            case 1:
                return UPDATE_CHECK_RESULT_UPDATE_AVAILABLE;
            case 2:
                return UPDATE_CHECK_RESULT_UP_TO_DATE;
            case 3:
                return UPDATE_CHECK_ERROR_OLD_APPLICATION_VERSION;
            case 4:
                return UPDATE_CHECK_ERROR_CONNECTION_PROBLEMS;
            case 5:
                return UPDATE_CHECK_ERROR_SIGNATURE_NOT_VALID;
            case 6:
                return UPDATE_CHECK_ERROR_WRONG_PROTO_FILE;
            case 7:
                return UPDATE_CHECK_ERROR_BROKEN_VERSION_STRINGS;
            case 8:
                return UPDATE_CHECK_ERROR_CURRENT_VPS_INVALID;
            default:
                return null;
        }
    }

    private g(int i2, int i3) {
        this.j = i3;
    }
}
