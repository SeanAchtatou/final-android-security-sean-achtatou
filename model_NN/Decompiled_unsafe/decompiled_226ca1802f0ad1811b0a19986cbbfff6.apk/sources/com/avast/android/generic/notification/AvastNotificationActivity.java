package com.avast.android.generic.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.avast.android.generic.aa;
import com.avast.android.generic.ui.BaseSinglePaneActivity;
import com.avast.android.generic.util.t;
import java.util.List;

public class AvastNotificationActivity extends BaseSinglePaneActivity {
    public static void call(Context context, Intent intent) {
        context.startActivity(new Intent(context, AvastNotificationActivity.class));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.e = true;
        b();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        b();
    }

    private void b() {
        boolean z = false;
        try {
            z = d();
        } catch (Exception e) {
            t.b("Can not count notifications", e);
        }
        if (z) {
            finish();
        }
    }

    private boolean d() {
        List<a> d;
        h hVar = (h) aa.a(this, h.class);
        boolean equals = getIntent().getData().getLastPathSegment().equals("temporaryNotifications");
        if (equals) {
            d = hVar.e();
        } else {
            d = hVar.d();
        }
        if (d.size() > 1) {
            return false;
        }
        if (d.size() == 0) {
            hVar.a(this);
            return true;
        }
        a aVar = d.get(0);
        AvastPendingIntent avastPendingIntent = aVar.g;
        if (avastPendingIntent != null) {
            hVar.a(avastPendingIntent);
        }
        if ((aVar.f & 16) > 0) {
            if (equals) {
                hVar.b(aVar.f920a);
            } else {
                hVar.a(aVar.f920a);
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public Fragment a() {
        return new AvastNotificationFragment();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        g().b();
    }
}
