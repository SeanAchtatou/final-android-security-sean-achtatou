package com.avast.android.generic.app.account;

/* compiled from: BackupChecker */
public class d {
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0159 A[SYNTHETIC, Splitter:B:117:0x0159] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x015e A[SYNTHETIC, Splitter:B:120:0x015e] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0163 A[SYNTHETIC, Splitter:B:123:0x0163] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0168 A[SYNTHETIC, Splitter:B:126:0x0168] */
    /* JADX WARNING: Removed duplicated region for block: B:365:0x049a  */
    /* JADX WARNING: Removed duplicated region for block: B:366:0x049d  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00ec A[SYNTHETIC, Splitter:B:73:0x00ec] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x00f1 A[SYNTHETIC, Splitter:B:76:0x00f1] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x00f6 A[SYNTHETIC, Splitter:B:79:0x00f6] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00fb A[SYNTHETIC, Splitter:B:82:0x00fb] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.avast.android.generic.app.account.e a(android.content.Context r11, com.avast.android.generic.ab r12) {
        /*
            r3 = 0
            java.lang.String r0 = com.avast.android.generic.g.b.a.i(r11)
            r4 = 0
            r1 = 0
            r5 = 0
            android.content.SharedPreferences r2 = r12.c()     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            java.util.Map r6 = r2.getAll()     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            java.util.HashMap r7 = new java.util.HashMap     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            r7.<init>()     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            java.lang.String r8 = "/system/etc/com.avast.android.antitheft.backup.enc"
            r2.<init>(r8)     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            boolean r2 = r2.exists()     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            if (r2 != 0) goto L_0x004f
            com.avast.android.generic.app.account.e r0 = new com.avast.android.generic.app.account.e     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            com.avast.android.generic.app.account.f r2 = com.avast.android.generic.app.account.f.NO_BACKUP     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            java.lang.String r6 = "Backup not found"
            r0.<init>(r2, r6)     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            if (r3 == 0) goto L_0x0030
            r4.close()     // Catch:{ IOException -> 0x03c3 }
        L_0x0030:
            if (r3 == 0) goto L_0x0035
            r1.close()     // Catch:{ IOException -> 0x03c6 }
        L_0x0035:
            if (r3 == 0) goto L_0x04b8
            r5.close()     // Catch:{ IOException -> 0x03c9 }
        L_0x003a:
            r1 = r3
        L_0x003b:
            if (r3 == 0) goto L_0x0040
            r1.close()     // Catch:{ IOException -> 0x03cc }
        L_0x0040:
            java.lang.String r1 = "temp.xml"
            r11.deleteFile(r1)     // Catch:{ Exception -> 0x0046 }
        L_0x0045:
            return r0
        L_0x0046:
            r1 = move-exception
            java.lang.String r2 = "AvastBackupGeneric"
            java.lang.String r3 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r2, r3, r1)
            goto L_0x0045
        L_0x004f:
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            java.lang.String r2 = "/system/etc/com.avast.android.antitheft.backup.enc"
            r1.<init>(r2)     // Catch:{ IOException -> 0x0472, all -> 0x0447 }
            java.util.zip.ZipInputStream r2 = new java.util.zip.ZipInputStream     // Catch:{ IOException -> 0x047a, all -> 0x044e }
            r2.<init>(r1)     // Catch:{ IOException -> 0x047a, all -> 0x044e }
            java.util.zip.ZipEntry r8 = r2.getNextEntry()     // Catch:{ IOException -> 0x0482, all -> 0x0455 }
            if (r8 != 0) goto L_0x008d
            com.avast.android.generic.app.account.e r0 = new com.avast.android.generic.app.account.e     // Catch:{ IOException -> 0x0482, all -> 0x0455 }
            com.avast.android.generic.app.account.f r6 = com.avast.android.generic.app.account.f.INVALID_BACKUP_FORMAT     // Catch:{ IOException -> 0x0482, all -> 0x0455 }
            java.lang.String r7 = "Invalid ZIP format"
            r0.<init>(r6, r7)     // Catch:{ IOException -> 0x0482, all -> 0x0455 }
            if (r3 == 0) goto L_0x006f
            r4.close()     // Catch:{ IOException -> 0x03cf }
        L_0x006f:
            if (r2 == 0) goto L_0x0074
            r2.close()     // Catch:{ IOException -> 0x03d2 }
        L_0x0074:
            if (r3 == 0) goto L_0x04b5
            r5.close()     // Catch:{ IOException -> 0x03d5 }
        L_0x0079:
            if (r3 == 0) goto L_0x007e
            r3.close()     // Catch:{ IOException -> 0x03d8 }
        L_0x007e:
            java.lang.String r1 = "temp.xml"
            r11.deleteFile(r1)     // Catch:{ Exception -> 0x0084 }
            goto L_0x0045
        L_0x0084:
            r1 = move-exception
            java.lang.String r2 = "AvastBackupGeneric"
            java.lang.String r3 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r2, r3, r1)
            goto L_0x0045
        L_0x008d:
            java.lang.String r8 = r8.getName()     // Catch:{ IOException -> 0x0482, all -> 0x0455 }
            java.lang.String r9 = ".xml"
            boolean r8 = r8.endsWith(r9)     // Catch:{ IOException -> 0x0482, all -> 0x0455 }
            if (r8 == 0) goto L_0x0357
            java.lang.String r4 = "temp.xml"
            r8 = 0
            java.io.FileOutputStream r4 = r11.openFileOutput(r4, r8)     // Catch:{ IOException -> 0x0482, all -> 0x0455 }
            byte[] r0 = com.avast.android.generic.d.c.a(r2, r0)     // Catch:{ Exception -> 0x010f }
            r4.write(r0)     // Catch:{ IOException -> 0x048a, all -> 0x045b }
            r4.flush()     // Catch:{ IOException -> 0x048a, all -> 0x045b }
            r4.close()     // Catch:{ IOException -> 0x048a, all -> 0x045b }
            r5 = 0
            java.lang.String r0 = "temp.xml"
            java.io.FileInputStream r4 = r11.openFileInput(r0)     // Catch:{ IOException -> 0x0482, all -> 0x0455 }
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x0492, all -> 0x0461 }
            r1.<init>(r4)     // Catch:{ IOException -> 0x0492, all -> 0x0461 }
        L_0x00b9:
            int r0 = r1.available()     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            if (r0 <= 0) goto L_0x0191
            java.lang.String r0 = r1.readUTF()     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            int r8 = r1.readInt()     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            r9 = 1
            if (r8 != r9) goto L_0x0145
            java.lang.String r8 = r1.readUTF()     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            r7.put(r0, r8)     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            goto L_0x00b9
        L_0x00d2:
            r0 = move-exception
            r5 = r4
            r6 = r3
            r4 = r2
            r2 = r1
            r1 = r0
        L_0x00d8:
            java.lang.String r0 = "AvastBackupGeneric"
            java.lang.String r7 = "Invalid backup format"
            com.avast.android.generic.util.z.a(r0, r7, r1)     // Catch:{ all -> 0x046c }
            com.avast.android.generic.app.account.e r0 = new com.avast.android.generic.app.account.e     // Catch:{ all -> 0x046c }
            com.avast.android.generic.app.account.f r7 = com.avast.android.generic.app.account.f.GENERIC_ISSUE     // Catch:{ all -> 0x046c }
            java.lang.String r1 = com.avast.android.generic.util.w.a(r11, r1)     // Catch:{ all -> 0x046c }
            r0.<init>(r7, r1)     // Catch:{ all -> 0x046c }
            if (r6 == 0) goto L_0x00ef
            r6.close()     // Catch:{ IOException -> 0x042f }
        L_0x00ef:
            if (r4 == 0) goto L_0x00f4
            r4.close()     // Catch:{ IOException -> 0x0432 }
        L_0x00f4:
            if (r2 == 0) goto L_0x049d
            r2.close()     // Catch:{ IOException -> 0x0435 }
        L_0x00f9:
            if (r3 == 0) goto L_0x00fe
            r3.close()     // Catch:{ IOException -> 0x0438 }
        L_0x00fe:
            java.lang.String r1 = "temp.xml"
            r11.deleteFile(r1)     // Catch:{ Exception -> 0x0105 }
            goto L_0x0045
        L_0x0105:
            r1 = move-exception
            java.lang.String r2 = "AvastBackupGeneric"
            java.lang.String r3 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r2, r3, r1)
            goto L_0x0045
        L_0x010f:
            r0 = move-exception
            java.lang.String r6 = "AvastBackupGeneric"
            java.lang.String r7 = "Invalid backup format"
            com.avast.android.generic.util.z.a(r6, r7, r0)     // Catch:{ IOException -> 0x048a, all -> 0x045b }
            com.avast.android.generic.app.account.e r0 = new com.avast.android.generic.app.account.e     // Catch:{ IOException -> 0x048a, all -> 0x045b }
            com.avast.android.generic.app.account.f r6 = com.avast.android.generic.app.account.f.INVALID_BACKUP_FORMAT     // Catch:{ IOException -> 0x048a, all -> 0x045b }
            java.lang.String r7 = "Decryption failed"
            r0.<init>(r6, r7)     // Catch:{ IOException -> 0x048a, all -> 0x045b }
            if (r4 == 0) goto L_0x0125
            r4.close()     // Catch:{ IOException -> 0x03db }
        L_0x0125:
            if (r2 == 0) goto L_0x012a
            r2.close()     // Catch:{ IOException -> 0x03de }
        L_0x012a:
            if (r3 == 0) goto L_0x04b2
            r5.close()     // Catch:{ IOException -> 0x03e1 }
        L_0x012f:
            if (r3 == 0) goto L_0x0134
            r3.close()     // Catch:{ IOException -> 0x03e4 }
        L_0x0134:
            java.lang.String r1 = "temp.xml"
            r11.deleteFile(r1)     // Catch:{ Exception -> 0x013b }
            goto L_0x0045
        L_0x013b:
            r1 = move-exception
            java.lang.String r2 = "AvastBackupGeneric"
            java.lang.String r3 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r2, r3, r1)
            goto L_0x0045
        L_0x0145:
            r9 = 2
            if (r8 != r9) goto L_0x0171
            boolean r8 = r1.readBoolean()     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            r7.put(r0, r8)     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            goto L_0x00b9
        L_0x0155:
            r0 = move-exception
            r6 = r3
        L_0x0157:
            if (r6 == 0) goto L_0x015c
            r6.close()     // Catch:{ IOException -> 0x043b }
        L_0x015c:
            if (r2 == 0) goto L_0x0161
            r2.close()     // Catch:{ IOException -> 0x043e }
        L_0x0161:
            if (r1 == 0) goto L_0x049a
            r1.close()     // Catch:{ IOException -> 0x0441 }
        L_0x0166:
            if (r3 == 0) goto L_0x016b
            r3.close()     // Catch:{ IOException -> 0x0444 }
        L_0x016b:
            java.lang.String r1 = "temp.xml"
            r11.deleteFile(r1)     // Catch:{ Exception -> 0x03b9 }
        L_0x0170:
            throw r0
        L_0x0171:
            r9 = 3
            if (r8 != r9) goto L_0x0181
            int r8 = r1.readInt()     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            r7.put(r0, r8)     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            goto L_0x00b9
        L_0x0181:
            r9 = 4
            if (r8 != r9) goto L_0x00b9
            long r8 = r1.readLong()     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            java.lang.Long r8 = java.lang.Long.valueOf(r8)     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            r7.put(r0, r8)     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            goto L_0x00b9
        L_0x0191:
            r1.close()     // Catch:{ IOException -> 0x00d2, all -> 0x0155 }
            r4 = 0
            java.lang.String r0 = "temp.xml"
            r11.deleteFile(r0)     // Catch:{ Exception -> 0x01f7 }
        L_0x019a:
            java.util.Set r0 = r7.keySet()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
        L_0x01a2:
            boolean r0 = r1.hasNext()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r0 == 0) goto L_0x0281
            java.lang.Object r0 = r1.next()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            boolean r8 = a(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r8 != 0) goto L_0x01a2
            boolean r8 = r6.containsKey(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r8 != 0) goto L_0x0211
            java.lang.Object r8 = r7.get(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r8 == 0) goto L_0x0211
            com.avast.android.generic.app.account.e r1 = new com.avast.android.generic.app.account.e     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            com.avast.android.generic.app.account.f r6 = com.avast.android.generic.app.account.f.NOT_UP_TO_DATE_BACKUP     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            r7.<init>()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r8 = "Current state does not contain backup key "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            r1.<init>(r6, r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r3 == 0) goto L_0x01df
            r5.close()     // Catch:{ IOException -> 0x03e7 }
        L_0x01df:
            if (r2 == 0) goto L_0x01e4
            r2.close()     // Catch:{ IOException -> 0x03ea }
        L_0x01e4:
            if (r3 == 0) goto L_0x04af
            r4.close()     // Catch:{ IOException -> 0x03ed }
        L_0x01e9:
            r0 = r3
        L_0x01ea:
            if (r3 == 0) goto L_0x01ef
            r0.close()     // Catch:{ IOException -> 0x03f0 }
        L_0x01ef:
            java.lang.String r0 = "temp.xml"
            r11.deleteFile(r0)     // Catch:{ Exception -> 0x0208 }
        L_0x01f4:
            r0 = r1
            goto L_0x0045
        L_0x01f7:
            r0 = move-exception
            java.lang.String r1 = "AvastBackupGeneric"
            java.lang.String r8 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r1, r8, r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            goto L_0x019a
        L_0x0200:
            r0 = move-exception
            r1 = r0
            r4 = r2
            r5 = r3
            r6 = r3
            r2 = r3
            goto L_0x00d8
        L_0x0208:
            r0 = move-exception
            java.lang.String r2 = "AvastBackupGeneric"
            java.lang.String r3 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r2, r3, r0)
            goto L_0x01f4
        L_0x0211:
            java.lang.Object r8 = r7.get(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.Object r9 = r6.get(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            boolean r8 = a(r8, r9)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r8 != 0) goto L_0x01a2
            com.avast.android.generic.app.account.e r1 = new com.avast.android.generic.app.account.e     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            com.avast.android.generic.app.account.f r8 = com.avast.android.generic.app.account.f.NOT_UP_TO_DATE_BACKUP     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            r9.<init>()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r10 = "Current state does not equal backup key value ("
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r10 = ", "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.Object r7 = r7.get(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r9 = ", "
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.Object r0 = r6.get(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r6 = ")"
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            r1.<init>(r8, r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r3 == 0) goto L_0x0260
            r5.close()     // Catch:{ IOException -> 0x03f3 }
        L_0x0260:
            if (r2 == 0) goto L_0x0265
            r2.close()     // Catch:{ IOException -> 0x03f6 }
        L_0x0265:
            if (r3 == 0) goto L_0x04ac
            r4.close()     // Catch:{ IOException -> 0x03f9 }
        L_0x026a:
            r0 = r3
        L_0x026b:
            if (r3 == 0) goto L_0x0270
            r0.close()     // Catch:{ IOException -> 0x03fc }
        L_0x0270:
            java.lang.String r0 = "temp.xml"
            r11.deleteFile(r0)     // Catch:{ Exception -> 0x0278 }
        L_0x0275:
            r0 = r1
            goto L_0x0045
        L_0x0278:
            r0 = move-exception
            java.lang.String r2 = "AvastBackupGeneric"
            java.lang.String r3 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r2, r3, r0)
            goto L_0x0275
        L_0x0281:
            java.util.Set r0 = r6.keySet()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
        L_0x0289:
            boolean r0 = r1.hasNext()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r0 == 0) goto L_0x0385
            java.lang.Object r0 = r1.next()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            boolean r8 = a(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r8 != 0) goto L_0x0289
            boolean r8 = r7.containsKey(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r8 != 0) goto L_0x02e7
            java.lang.Object r8 = r6.get(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r8 == 0) goto L_0x02e7
            com.avast.android.generic.app.account.e r1 = new com.avast.android.generic.app.account.e     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            com.avast.android.generic.app.account.f r6 = com.avast.android.generic.app.account.f.NOT_UP_TO_DATE_BACKUP     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            r7.<init>()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r8 = "Backup does not contain current state key "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            r1.<init>(r6, r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r3 == 0) goto L_0x02c6
            r5.close()     // Catch:{ IOException -> 0x03ff }
        L_0x02c6:
            if (r2 == 0) goto L_0x02cb
            r2.close()     // Catch:{ IOException -> 0x0402 }
        L_0x02cb:
            if (r3 == 0) goto L_0x04a9
            r4.close()     // Catch:{ IOException -> 0x0405 }
        L_0x02d0:
            r0 = r3
        L_0x02d1:
            if (r3 == 0) goto L_0x02d6
            r0.close()     // Catch:{ IOException -> 0x0408 }
        L_0x02d6:
            java.lang.String r0 = "temp.xml"
            r11.deleteFile(r0)     // Catch:{ Exception -> 0x02de }
        L_0x02db:
            r0 = r1
            goto L_0x0045
        L_0x02de:
            r0 = move-exception
            java.lang.String r2 = "AvastBackupGeneric"
            java.lang.String r3 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r2, r3, r0)
            goto L_0x02db
        L_0x02e7:
            java.lang.Object r8 = r7.get(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.Object r9 = r6.get(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            boolean r8 = a(r8, r9)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r8 != 0) goto L_0x0289
            com.avast.android.generic.app.account.e r1 = new com.avast.android.generic.app.account.e     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            com.avast.android.generic.app.account.f r8 = com.avast.android.generic.app.account.f.NOT_UP_TO_DATE_BACKUP     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            r9.<init>()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r10 = "Backup does not equal current state key value ("
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r10 = ", "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.Object r7 = r7.get(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r9 = ", "
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.Object r0 = r6.get(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r6 = ")"
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            r1.<init>(r8, r0)     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r3 == 0) goto L_0x0336
            r5.close()     // Catch:{ IOException -> 0x040b }
        L_0x0336:
            if (r2 == 0) goto L_0x033b
            r2.close()     // Catch:{ IOException -> 0x040e }
        L_0x033b:
            if (r3 == 0) goto L_0x04a6
            r4.close()     // Catch:{ IOException -> 0x0411 }
        L_0x0340:
            r0 = r3
        L_0x0341:
            if (r3 == 0) goto L_0x0346
            r0.close()     // Catch:{ IOException -> 0x0414 }
        L_0x0346:
            java.lang.String r0 = "temp.xml"
            r11.deleteFile(r0)     // Catch:{ Exception -> 0x034e }
        L_0x034b:
            r0 = r1
            goto L_0x0045
        L_0x034e:
            r0 = move-exception
            java.lang.String r2 = "AvastBackupGeneric"
            java.lang.String r3 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r2, r3, r0)
            goto L_0x034b
        L_0x0357:
            com.avast.android.generic.app.account.e r0 = new com.avast.android.generic.app.account.e     // Catch:{ IOException -> 0x0482, all -> 0x0455 }
            com.avast.android.generic.app.account.f r6 = com.avast.android.generic.app.account.f.INVALID_BACKUP_FORMAT     // Catch:{ IOException -> 0x0482, all -> 0x0455 }
            java.lang.String r7 = "No XML file"
            r0.<init>(r6, r7)     // Catch:{ IOException -> 0x0482, all -> 0x0455 }
            if (r3 == 0) goto L_0x0365
            r4.close()     // Catch:{ IOException -> 0x0417 }
        L_0x0365:
            if (r2 == 0) goto L_0x036a
            r2.close()     // Catch:{ IOException -> 0x041a }
        L_0x036a:
            if (r3 == 0) goto L_0x04a3
            r5.close()     // Catch:{ IOException -> 0x041d }
        L_0x036f:
            if (r3 == 0) goto L_0x0374
            r3.close()     // Catch:{ IOException -> 0x0420 }
        L_0x0374:
            java.lang.String r1 = "temp.xml"
            r11.deleteFile(r1)     // Catch:{ Exception -> 0x037b }
            goto L_0x0045
        L_0x037b:
            r1 = move-exception
            java.lang.String r2 = "AvastBackupGeneric"
            java.lang.String r3 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r2, r3, r1)
            goto L_0x0045
        L_0x0385:
            r2.closeEntry()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            r2.close()     // Catch:{ IOException -> 0x0200, all -> 0x0466 }
            if (r3 == 0) goto L_0x0390
            r5.close()     // Catch:{ IOException -> 0x0423 }
        L_0x0390:
            if (r2 == 0) goto L_0x0395
            r2.close()     // Catch:{ IOException -> 0x0426 }
        L_0x0395:
            if (r3 == 0) goto L_0x04a0
            r4.close()     // Catch:{ IOException -> 0x0429 }
        L_0x039a:
            r0 = r3
        L_0x039b:
            if (r3 == 0) goto L_0x03a0
            r0.close()     // Catch:{ IOException -> 0x042c }
        L_0x03a0:
            java.lang.String r0 = "temp.xml"
            r11.deleteFile(r0)     // Catch:{ Exception -> 0x03b0 }
        L_0x03a5:
            com.avast.android.generic.app.account.e r0 = new com.avast.android.generic.app.account.e
            com.avast.android.generic.app.account.f r1 = com.avast.android.generic.app.account.f.VALID
            java.lang.String r2 = ""
            r0.<init>(r1, r2)
            goto L_0x0045
        L_0x03b0:
            r0 = move-exception
            java.lang.String r1 = "AvastBackupGeneric"
            java.lang.String r2 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r1, r2, r0)
            goto L_0x03a5
        L_0x03b9:
            r1 = move-exception
            java.lang.String r2 = "AvastBackupGeneric"
            java.lang.String r3 = "Can not delete temp file"
            com.avast.android.generic.util.z.a(r2, r3, r1)
            goto L_0x0170
        L_0x03c3:
            r2 = move-exception
            goto L_0x0030
        L_0x03c6:
            r1 = move-exception
            goto L_0x0035
        L_0x03c9:
            r1 = move-exception
            goto L_0x003a
        L_0x03cc:
            r1 = move-exception
            goto L_0x0040
        L_0x03cf:
            r4 = move-exception
            goto L_0x006f
        L_0x03d2:
            r2 = move-exception
            goto L_0x0074
        L_0x03d5:
            r1 = move-exception
            goto L_0x0079
        L_0x03d8:
            r1 = move-exception
            goto L_0x007e
        L_0x03db:
            r4 = move-exception
            goto L_0x0125
        L_0x03de:
            r2 = move-exception
            goto L_0x012a
        L_0x03e1:
            r1 = move-exception
            goto L_0x012f
        L_0x03e4:
            r1 = move-exception
            goto L_0x0134
        L_0x03e7:
            r0 = move-exception
            goto L_0x01df
        L_0x03ea:
            r0 = move-exception
            goto L_0x01e4
        L_0x03ed:
            r0 = move-exception
            goto L_0x01e9
        L_0x03f0:
            r0 = move-exception
            goto L_0x01ef
        L_0x03f3:
            r0 = move-exception
            goto L_0x0260
        L_0x03f6:
            r0 = move-exception
            goto L_0x0265
        L_0x03f9:
            r0 = move-exception
            goto L_0x026a
        L_0x03fc:
            r0 = move-exception
            goto L_0x0270
        L_0x03ff:
            r0 = move-exception
            goto L_0x02c6
        L_0x0402:
            r0 = move-exception
            goto L_0x02cb
        L_0x0405:
            r0 = move-exception
            goto L_0x02d0
        L_0x0408:
            r0 = move-exception
            goto L_0x02d6
        L_0x040b:
            r0 = move-exception
            goto L_0x0336
        L_0x040e:
            r0 = move-exception
            goto L_0x033b
        L_0x0411:
            r0 = move-exception
            goto L_0x0340
        L_0x0414:
            r0 = move-exception
            goto L_0x0346
        L_0x0417:
            r4 = move-exception
            goto L_0x0365
        L_0x041a:
            r2 = move-exception
            goto L_0x036a
        L_0x041d:
            r1 = move-exception
            goto L_0x036f
        L_0x0420:
            r1 = move-exception
            goto L_0x0374
        L_0x0423:
            r0 = move-exception
            goto L_0x0390
        L_0x0426:
            r0 = move-exception
            goto L_0x0395
        L_0x0429:
            r0 = move-exception
            goto L_0x039a
        L_0x042c:
            r0 = move-exception
            goto L_0x03a0
        L_0x042f:
            r1 = move-exception
            goto L_0x00ef
        L_0x0432:
            r1 = move-exception
            goto L_0x00f4
        L_0x0435:
            r1 = move-exception
            goto L_0x00f9
        L_0x0438:
            r1 = move-exception
            goto L_0x00fe
        L_0x043b:
            r5 = move-exception
            goto L_0x015c
        L_0x043e:
            r2 = move-exception
            goto L_0x0161
        L_0x0441:
            r1 = move-exception
            goto L_0x0166
        L_0x0444:
            r1 = move-exception
            goto L_0x016b
        L_0x0447:
            r0 = move-exception
            r1 = r3
            r2 = r3
            r4 = r3
            r6 = r3
            goto L_0x0157
        L_0x044e:
            r0 = move-exception
            r2 = r3
            r4 = r1
            r6 = r3
            r1 = r3
            goto L_0x0157
        L_0x0455:
            r0 = move-exception
            r4 = r1
            r6 = r3
            r1 = r3
            goto L_0x0157
        L_0x045b:
            r0 = move-exception
            r6 = r4
            r4 = r1
            r1 = r3
            goto L_0x0157
        L_0x0461:
            r0 = move-exception
            r1 = r3
            r6 = r3
            goto L_0x0157
        L_0x0466:
            r0 = move-exception
            r1 = r3
            r4 = r3
            r6 = r3
            goto L_0x0157
        L_0x046c:
            r0 = move-exception
            r1 = r2
            r2 = r4
            r4 = r5
            goto L_0x0157
        L_0x0472:
            r0 = move-exception
            r1 = r0
            r2 = r3
            r4 = r3
            r5 = r3
            r6 = r3
            goto L_0x00d8
        L_0x047a:
            r0 = move-exception
            r2 = r3
            r4 = r3
            r5 = r1
            r6 = r3
            r1 = r0
            goto L_0x00d8
        L_0x0482:
            r0 = move-exception
            r4 = r2
            r5 = r1
            r6 = r3
            r2 = r3
            r1 = r0
            goto L_0x00d8
        L_0x048a:
            r0 = move-exception
            r5 = r1
            r6 = r4
            r4 = r2
            r1 = r0
            r2 = r3
            goto L_0x00d8
        L_0x0492:
            r0 = move-exception
            r1 = r0
            r5 = r4
            r6 = r3
            r4 = r2
            r2 = r3
            goto L_0x00d8
        L_0x049a:
            r3 = r4
            goto L_0x0166
        L_0x049d:
            r3 = r5
            goto L_0x00f9
        L_0x04a0:
            r0 = r3
            goto L_0x039b
        L_0x04a3:
            r3 = r1
            goto L_0x036f
        L_0x04a6:
            r0 = r3
            goto L_0x0341
        L_0x04a9:
            r0 = r3
            goto L_0x02d1
        L_0x04ac:
            r0 = r3
            goto L_0x026b
        L_0x04af:
            r0 = r3
            goto L_0x01ea
        L_0x04b2:
            r3 = r1
            goto L_0x012f
        L_0x04b5:
            r3 = r1
            goto L_0x0079
        L_0x04b8:
            r1 = r3
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.app.account.d.a(android.content.Context, com.avast.android.generic.ab):com.avast.android.generic.app.account.e");
    }

    private static boolean a(Object obj, Object obj2) {
        if (obj != null && (obj instanceof String) && ((String) obj).equals("")) {
            obj = null;
        }
        if (obj2 != null && (obj2 instanceof String) && ((String) obj2).equals("")) {
            obj2 = null;
        }
        if (obj == null && obj2 == null) {
            return true;
        }
        if (obj == null || obj2 == null) {
            return false;
        }
        return obj.equals(obj2);
    }

    private static boolean a(String str) {
        if (str == null || str.equals("")) {
            return true;
        }
        if (str.equals("wipethorough") || str.equals("allowsms") || str.equals("locktext") || str.equals("disadb") || str.equals("endata") || str.equals("lockmode") || str.equals("audiofile") || str.equals("sirenonlock") || str.equals("battery") || str.equals("batteryalways") || str.equals("gpsautoon") || str.equals("hidegps") || str.equals("checkforupdates") || str.equals("lockprogrammgr") || str.equals("lockallsettings") || str.equals("owner") || str.equals("system") || str.equals("hidegps") || str.equals("forceEnableData") || str.equals("geofencingRadius") || str.equals("passwordCheckTheftEvent") || str.equals("passwordCheckTakePicture") || str.equals("passwordCheckSms") || str.equals("binarySmsMode") || str.equals("auid") || str.equals("accountCommPassword") || str.equals("accountEmail") || str.equals("accountEncKey") || str.equals("accountSmsGateway") || str.equals("not1") || str.equals("not2") || str.equals("c2dmowner") || str.equals("c2dmri") || str.equals("encaccesscode") || str.equals("accountSmsSending") || str.equals("splitcdma")) {
            return false;
        }
        return true;
    }
}
