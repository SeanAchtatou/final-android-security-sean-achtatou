package com.avast.android.generic.ui.b;

import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;
import com.actionbarsherlock.widget.ActivityChooserView;

/* compiled from: ExpandAnimation */
public class a extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private static final Interpolator f1018a = new AccelerateInterpolator();

    /* renamed from: b  reason: collision with root package name */
    private static final Interpolator f1019b = new AccelerateInterpolator();

    /* renamed from: c  reason: collision with root package name */
    private final View f1020c;
    private final int d;
    private int e = -1;
    private boolean f;

    public a(View view) {
        this.f1020c = view;
        this.f1020c.getLayoutParams().height = 0;
        this.d = 0;
        this.e = -1;
        setDuration((long) view.getContext().getResources().getInteger(17694721));
        setFillBefore(true);
        setFillAfter(true);
        setFillEnabled(true);
        a(false);
    }

    public void a(boolean z) {
        this.f = z;
        if (z) {
            setInterpolator(f1019b);
        } else {
            setInterpolator(f1018a);
        }
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        int i;
        if (!this.f) {
            i = (int) ((((float) (this.e - this.d)) * f2) + ((float) this.d));
        } else {
            i = (int) ((((float) (this.e - this.d)) * (1.0f - f2)) + ((float) this.d));
        }
        if (this.f1020c.getLayoutParams().height != i) {
            this.f1020c.getLayoutParams().height = i;
            this.f1020c.requestLayout();
        }
    }

    public void initialize(int i, int i2, int i3, int i4) {
        if (this.e < 0) {
            this.f1020c.measure(View.MeasureSpec.makeMeasureSpec(i, 1073741824), View.MeasureSpec.makeMeasureSpec(ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, Integer.MIN_VALUE));
            this.e = this.f1020c.getMeasuredHeight();
            this.f1020c.getLayoutParams().height = 0;
        }
        super.initialize(i, i2, i3, i4);
    }

    public boolean willChangeBounds() {
        return true;
    }
}
