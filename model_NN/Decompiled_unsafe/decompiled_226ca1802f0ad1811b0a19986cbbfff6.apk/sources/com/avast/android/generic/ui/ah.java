package com.avast.android.generic.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;

/* compiled from: VoucherDialog */
class ah implements DialogInterface.OnShowListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AlertDialog f1013a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ VoucherDialog f1014b;

    ah(VoucherDialog voucherDialog, AlertDialog alertDialog) {
        this.f1014b = voucherDialog;
        this.f1013a = alertDialog;
    }

    public void onShow(DialogInterface dialogInterface) {
        this.f1013a.getButton(-1).setOnClickListener(new ai(this));
    }
}
