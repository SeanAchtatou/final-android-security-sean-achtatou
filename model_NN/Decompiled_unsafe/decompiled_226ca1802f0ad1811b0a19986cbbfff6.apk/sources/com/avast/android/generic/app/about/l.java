package com.avast.android.generic.app.about;

import android.content.DialogInterface;

/* compiled from: FeedbackFragment */
class l implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f349a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ FeedbackFragment f350b;

    l(FeedbackFragment feedbackFragment, n nVar) {
        this.f350b = feedbackFragment;
        this.f349a = nVar;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f349a.cancel(true);
    }
}
