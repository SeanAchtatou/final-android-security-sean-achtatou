package com.avast.android.generic.ui.widget;

import android.graphics.Rect;
import android.view.TouchDelegate;
import com.avast.android.generic.p;

/* compiled from: SwitchRow */
class ab implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SwitchRow f1134a;

    ab(SwitchRow switchRow) {
        this.f1134a = switchRow;
    }

    public void run() {
        Rect rect = new Rect();
        this.f1134a.f1123b.getHitRect(rect);
        int dimensionPixelSize = this.f1134a.getContext().getResources().getDimensionPixelSize(p.content_marginHorizontal);
        int dimensionPixelSize2 = this.f1134a.getContext().getResources().getDimensionPixelSize(p.content_marginVertical);
        rect.top -= dimensionPixelSize2;
        rect.bottom = dimensionPixelSize2 + rect.bottom;
        rect.left -= dimensionPixelSize;
        rect.right = dimensionPixelSize + rect.right;
        this.f1134a.setTouchDelegate(new TouchDelegate(rect, this.f1134a.f1123b));
    }
}
