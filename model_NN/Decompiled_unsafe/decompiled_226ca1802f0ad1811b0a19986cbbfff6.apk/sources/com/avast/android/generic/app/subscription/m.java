package com.avast.android.generic.app.subscription;

import android.content.DialogInterface;
import com.avast.android.generic.licensing.c.k;

/* compiled from: SubscriptionFragment */
class m implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f560a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f561b;

    m(SubscriptionFragment subscriptionFragment, k kVar) {
        this.f561b = subscriptionFragment;
        this.f560a = kVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f561b.g(this.f560a);
    }
}
