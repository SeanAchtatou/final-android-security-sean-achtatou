package com.avast.android.generic.internet.c.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: MyAvastPairing */
public final class m extends GeneratedMessageLite.Builder<l, m> implements n {

    /* renamed from: a  reason: collision with root package name */
    private int f773a;

    /* renamed from: b  reason: collision with root package name */
    private Object f774b = "";

    private m() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static m g() {
        return new m();
    }

    /* renamed from: a */
    public m clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public l getDefaultInstanceForType() {
        return l.a();
    }

    /* renamed from: c */
    public l build() {
        l d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public l d() {
        int i = 1;
        l lVar = new l(this);
        if ((this.f773a & 1) != 1) {
            i = 0;
        }
        Object unused = lVar.f772c = this.f774b;
        int unused2 = lVar.f771b = i;
        return lVar;
    }

    /* renamed from: a */
    public m mergeFrom(l lVar) {
        if (lVar != l.a() && lVar.b()) {
            a(lVar.c());
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public m mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f773a |= 1;
                    this.f774b = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public m a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f773a |= 1;
        this.f774b = str;
        return this;
    }
}
