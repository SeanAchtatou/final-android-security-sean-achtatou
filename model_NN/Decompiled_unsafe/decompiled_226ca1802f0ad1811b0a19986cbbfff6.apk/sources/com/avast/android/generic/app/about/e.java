package com.avast.android.generic.app.about;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.ScrollView;
import android.widget.Scroller;

/* compiled from: AboutFragment */
class e extends Handler {

    /* renamed from: a  reason: collision with root package name */
    ScrollView f340a;

    /* renamed from: b  reason: collision with root package name */
    Scroller f341b;

    /* renamed from: c  reason: collision with root package name */
    int f342c;

    public e(Context context, ScrollView scrollView) {
        this.f340a = scrollView;
        this.f341b = new Scroller(context);
    }

    public void handleMessage(Message message) {
        if (message.what == 1) {
            this.f342c = 0;
            this.f341b.startScroll(0, 0, 0, message.arg1, message.arg2);
            sendEmptyMessage(2);
        } else if (message.what == 2) {
            boolean computeScrollOffset = this.f341b.computeScrollOffset();
            this.f340a.scrollBy(0, this.f341b.getCurrY() - this.f342c);
            this.f342c = this.f341b.getCurrY();
            if (computeScrollOffset) {
                sendEmptyMessageDelayed(2, 20);
            }
        }
    }
}
