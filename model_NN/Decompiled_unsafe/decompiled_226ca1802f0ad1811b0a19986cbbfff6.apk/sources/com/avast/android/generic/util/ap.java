package com.avast.android.generic.util;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/* compiled from: UserTask */
class ap extends FutureTask<Result> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f1186a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ap(am amVar, Callable callable) {
        super(callable);
        this.f1186a = amVar;
    }

    /* access modifiers changed from: protected */
    public void done() {
        Object obj = null;
        try {
            obj = get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e2) {
            throw new RuntimeException("An error occured while executing doInBackground()", e2.getCause());
        } catch (CancellationException e3) {
            am.d.obtainMessage(3, new at(this.f1186a, null)).sendToTarget();
            return;
        } catch (Throwable th) {
            throw new RuntimeException("An error occured while executing doInBackground()", th);
        }
        am.d.obtainMessage(1, new at(this.f1186a, obj)).sendToTarget();
    }
}
