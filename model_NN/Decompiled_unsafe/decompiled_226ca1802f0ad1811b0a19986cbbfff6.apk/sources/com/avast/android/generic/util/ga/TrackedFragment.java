package com.avast.android.generic.util.ga;

import a.a.a.a.a.a;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentBreadCrumbs;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.avast.android.generic.r;
import com.avast.android.generic.ui.BaseActivity;
import com.avast.android.generic.ui.BaseMultiPaneActivity;
import com.avast.android.generic.util.ak;

public abstract class TrackedFragment extends SherlockFragment {
    public abstract String b();

    public int a() {
        return 0;
    }

    public FragmentBreadCrumbs a(View view) {
        return (FragmentBreadCrumbs) view.findViewWithTag("breadcrumbs");
    }

    public View b(View view) {
        return view.findViewById(r.titlebar);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        c(view);
        c();
    }

    private void c(View view) {
        FragmentBreadCrumbs a2;
        if (!ak.b(getActivity()) && (a2 = a(view)) != null) {
            a2.setActivity(getActivity());
            if (a() > 0) {
                a2.setLastTitle(getString(a()), "");
            }
            if (getArguments() != null) {
                int i = getArguments().getInt("titleResourceId", 0);
                if (i > 0) {
                    a2.setLastTitle(getString(i), "");
                    return;
                }
                String string = getArguments().getString("title");
                if (!TextUtils.isEmpty(string)) {
                    a2.setLastTitle(string, "");
                }
            }
        }
    }

    private void c() {
        ActionBar supportActionBar = getSherlockActivity().getSupportActionBar();
        if (supportActionBar != null && (supportActionBar.getDisplayOptions() & 8) != 0) {
            if (a() > 0) {
                supportActionBar.setTitle(a());
            }
            if (getArguments() != null) {
                int i = getArguments().getInt("titleResourceId", 0);
                if (i > 0) {
                    supportActionBar.setTitle(i);
                    return;
                }
                String string = getArguments().getString("title");
                if (!TextUtils.isEmpty(string)) {
                    supportActionBar.setTitle(string);
                }
            }
        }
    }

    public void onResume() {
        super.onResume();
        c a2 = a.a();
        String b2 = b();
        if (b2 != null) {
            a2.a(b2);
        }
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (!ak.b(getActivity())) {
            getActivity().finish();
        } else if (getActivity() instanceof BaseMultiPaneActivity) {
            getFragmentManager().popBackStack();
        } else {
            getActivity().finish();
        }
    }

    public void a(String str, String str2, String str3, long j) {
        try {
            a.a().a(str, str2, str3, Long.valueOf(j));
        } catch (Exception e) {
            a.a().a("GA: trackEvent", e);
        }
    }

    public void a(IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4) {
        if (getActivity() == null) {
            throw new IllegalStateException("Fragment " + this + " not attached to Activity");
        } else if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).a(this, intentSender, i, intent, i2, i3, i4);
        } else {
            getActivity().startIntentSenderForResult(intentSender, i, intent, i2, i3, i4);
        }
    }

    public void a(Fragment fragment) {
        Bundle arguments = fragment.getArguments();
        if (arguments == null) {
            arguments = new Bundle();
        }
        arguments.putAll(getArguments());
        fragment.setArguments(arguments);
        FragmentTransaction beginTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        beginTransaction.replace(r.root_container, fragment, "singleFragment");
        beginTransaction.addToBackStack(null);
        beginTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        beginTransaction.commit();
    }
}
