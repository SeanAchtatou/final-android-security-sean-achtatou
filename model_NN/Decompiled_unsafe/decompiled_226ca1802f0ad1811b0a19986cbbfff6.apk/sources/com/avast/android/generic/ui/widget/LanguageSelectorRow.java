package com.avast.android.generic.ui.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.actionbarsherlock.view.Menu;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.m;
import com.avast.android.generic.n;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.util.ga.a;
import com.avast.android.generic.x;
import com.avast.android.generic.y;
import com.avast.android.generic.z;

public class LanguageSelectorRow extends Row implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    CharSequence[] f1089a;

    /* renamed from: b  reason: collision with root package name */
    CharSequence[] f1090b;

    /* renamed from: c  reason: collision with root package name */
    String f1091c;
    private ImageView m;

    public LanguageSelectorRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, i, y.Row_Next));
    }

    public LanguageSelectorRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, n.rowNextStyle);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, n.rowNextStyle, y.Row_Next));
    }

    private void a(Context context, TypedArray typedArray) {
        typedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void a() {
        inflate(getContext(), t.row_next, this);
        this.f1089a = getContext().getResources().getTextArray(m.languages_code);
        this.f1090b = getContext().getResources().getTextArray(m.languages_names);
        this.f1091c = ((ab) aa.a(getContext(), ab.class)).p();
        this.m = (ImageView) findViewById(r.c_next_arrow);
        a(new h(this));
        c();
        ((com.avast.android.generic.util.y) aa.a(getContext(), com.avast.android.generic.util.y.class)).a(r.message_language_changed, this);
    }

    public boolean handleMessage(Message message) {
        c();
        return true;
    }

    private void c() {
        CharSequence text = getContext().getText(x.pref_language_default);
        String p = ((ab) aa.a(getContext(), ab.class)).p();
        if (p.length() > 0) {
            CharSequence charSequence = text;
            for (int i = 0; i < this.f1089a.length; i++) {
                if (this.f1089a[i].toString().equals(this.f1091c)) {
                    charSequence = this.f1090b[i].toString();
                }
            }
            text = charSequence;
        }
        if (!p.equals(this.f1091c) && (getContext() instanceof FragmentActivity)) {
            FragmentActivity fragmentActivity = (FragmentActivity) getContext();
            Intent intent = fragmentActivity.getIntent();
            intent.addFlags(Menu.CATEGORY_CONTAINER);
            fragmentActivity.finish();
            fragmentActivity.startActivity(intent);
            a.a().a("ms-settings", "changeLanguage", p.length() > 0 ? p : "default", 0L);
        }
        c(getContext().getString(x.pref_language_current, text));
    }

    public class LanguageSelectDialog extends DialogFragment {
        public static void a(FragmentManager fragmentManager) {
            if (fragmentManager.findFragmentByTag("dialog") == null) {
                new LanguageSelectDialog().show(fragmentManager, "dialog");
            }
        }

        public Dialog onCreateDialog(Bundle bundle) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ak.c(getActivity()));
            builder.setItems(m.languages_names, new i(this));
            AlertDialog create = builder.create();
            create.setInverseBackgroundForced(true);
            return create;
        }
    }
}
