package com.avast.android.generic.util;

import java.util.TimerTask;

/* compiled from: CommandExecutor */
final class p extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f1249a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Thread f1250b;

    p(q qVar, Thread thread) {
        this.f1249a = qVar;
        this.f1250b = thread;
    }

    public void run() {
        this.f1249a.f1251a = true;
        this.f1250b.interrupt();
    }
}
