package com.avast.android.generic.util;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: AsyncTaskUtils */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final ThreadFactory f1205a = new c();

    /* renamed from: b  reason: collision with root package name */
    private static final BlockingQueue<Runnable> f1206b = new LinkedBlockingQueue(10);

    /* renamed from: c  reason: collision with root package name */
    private static final Executor f1207c = new ThreadPoolExecutor(10, 256, 1, TimeUnit.SECONDS, f1206b, f1205a);

    @TargetApi(11)
    public static <Params, Progress, Result> void a(AsyncTask<Params, Progress, Result> asyncTask, Params... paramsArr) {
        if (Build.VERSION.SDK_INT >= 11) {
            asyncTask.executeOnExecutor(f1207c, paramsArr);
        } else {
            asyncTask.execute(paramsArr);
        }
    }
}
