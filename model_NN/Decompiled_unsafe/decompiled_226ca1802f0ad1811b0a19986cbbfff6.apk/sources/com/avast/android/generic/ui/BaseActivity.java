package com.avast.android.generic.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.avast.android.generic.aa;
import com.avast.android.generic.g;
import com.avast.android.generic.i;
import com.avast.android.generic.l;
import com.avast.android.generic.ui.c.b;
import com.avast.android.generic.ui.rtl.c;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.util.d;

@TargetApi(14)
public abstract class BaseActivity extends SherlockFragmentActivity {
    public static long f;
    private static long h;

    /* renamed from: a  reason: collision with root package name */
    private boolean f978a = false;

    /* renamed from: b  reason: collision with root package name */
    private boolean f979b = false;

    /* renamed from: c  reason: collision with root package name */
    private boolean f980c;
    final a d = a.a(this);
    protected boolean e = false;
    private String g;
    private c i;
    private LayoutInflater j;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeButtonEnabled(true);
        }
        this.g = g.a(this);
        this.i = new c(this);
        ((d) aa.a(this, d.class)).c(this);
    }

    public int e() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        this.i.a(findViewById(16908290));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        f = SystemClock.elapsedRealtime() - h;
        com.avast.android.generic.util.ga.a.b().a((Activity) this);
        if (this.f980c) {
            com.avast.android.generic.util.ga.a.a().a("common", "firstRun", getPackageName(), 0L);
            com.avast.android.generic.util.ga.a.a().a("common", "firstRun-GA-v2", getPackageName(), 0L);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        g.a(this, this.g);
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        if (!this.e) {
            if (!this.f978a) {
                i.a(this);
            } else {
                this.f979b = true;
            }
        }
        this.f978a = false;
        if (!isFinishing()) {
            b.a(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.e && !this.f979b) {
            h = SystemClock.elapsedRealtime();
        }
        this.f979b = false;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        com.avast.android.generic.util.ga.a.b().b(this);
    }

    public void finish() {
        b.a.a.a.a.d.a();
        super.finish();
    }

    public boolean onKeyLongPress(int i2, KeyEvent keyEvent) {
        return this.d.a(i2, keyEvent) || super.onKeyLongPress(i2, keyEvent);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 82) {
            com.avast.android.generic.util.ga.a.a().a("common", "keyMenuPressed", getClass().getName(), 0L);
        }
        if (i2 == 4) {
            com.avast.android.generic.util.ga.a.a().a("common", "keyBackPressed", getClass().getName(), 0L);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                ActionBar supportActionBar = getSupportActionBar();
                if (supportActionBar == null || (supportActionBar.getDisplayOptions() & 4) == 0) {
                    this.d.c();
                    return true;
                }
                Intent c2 = c();
                if (c2 == null) {
                    finish();
                } else if (NavUtils.shouldUpRecreateTask(this, c2)) {
                    f();
                    finish();
                    if (!ak.a()) {
                        overridePendingTransition(l.home_enter, l.home_exit);
                    }
                } else {
                    a(c2);
                    if (!ak.a()) {
                        overridePendingTransition(l.home_enter, l.home_exit);
                    }
                }
                com.avast.android.generic.util.ga.a.a().a("common", "up", getClass().getName(), 0L);
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public Intent c() {
        return NavUtils.getParentActivityIntent(this);
    }

    /* access modifiers changed from: protected */
    public void f() {
        TaskStackBuilder.create(this).addParentStack(this).startActivities();
    }

    /* access modifiers changed from: protected */
    public void a(Intent intent) {
        intent.addFlags(67108864);
        startActivity(intent);
        finish();
    }

    /* access modifiers changed from: protected */
    public a g() {
        return this.d;
    }

    public void b(Intent intent) {
        startActivity(intent);
    }

    public void a(Class<? extends FragmentActivity> cls) {
        b(new Intent(this, cls));
    }

    public void a(Class<? extends FragmentActivity> cls, Bundle bundle) {
        Intent intent = new Intent(this, cls);
        intent.putExtras(bundle);
        b(intent);
    }

    public static Bundle c(Intent intent) {
        Bundle bundle = new Bundle();
        if (intent != null) {
            if (intent.getAction() != null) {
                bundle.putString("_action", intent.getAction());
            }
            Uri data = intent.getData();
            if (data != null) {
                bundle.putParcelable("_uri", data);
            }
            if (intent.getExtras() != null) {
                bundle.putAll(intent.getExtras());
            }
        }
        return bundle;
    }

    public static Intent a(Bundle bundle) {
        Intent intent = new Intent();
        if (bundle == null) {
            return intent;
        }
        intent.setAction(bundle.getString("_action"));
        Uri uri = (Uri) bundle.getParcelable("_uri");
        if (uri != null) {
            intent.setData(uri);
        }
        intent.putExtras(bundle);
        intent.removeExtra("_uri");
        return intent;
    }

    public void b(boolean z) {
        this.e = z;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
    }

    public void startIntentSenderForResult(IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5) {
        if (i2 == -1 || (-65536 & i2) == 0) {
            super.startIntentSenderForResult(intentSender, i2, intent, i3, i4, i5);
            return;
        }
        throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
    }

    public void a(Fragment fragment, IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5) {
        if (i2 == -1) {
            super.startIntentSenderForResult(intentSender, i2, intent, i3, i4, i5);
        } else if ((-65536 & i2) != 0) {
            throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
        } else {
            Bundle bundle = new Bundle();
            getSupportFragmentManager().putFragment(bundle, "mIndex", fragment);
            bundle.getInt("mIndex");
            super.startIntentSenderForResult(intentSender, ((bundle.getInt("mIndex") + 1) << 16) + (65535 & i2), intent, i3, i4, i5);
        }
    }

    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return super.getSystemService(str);
        }
        if (this.j == null) {
            this.j = LayoutInflater.from(getApplicationContext()).cloneInContext(this);
        }
        return this.j;
    }
}
