package com.avast.android.generic.util;

/* compiled from: BaseTracker */
public enum e {
    AGREE("agree"),
    DISAGREE("disagree");
    

    /* renamed from: c  reason: collision with root package name */
    String f1220c;

    private e(String str) {
        this.f1220c = str;
    }

    public String a() {
        return this.f1220c;
    }
}
