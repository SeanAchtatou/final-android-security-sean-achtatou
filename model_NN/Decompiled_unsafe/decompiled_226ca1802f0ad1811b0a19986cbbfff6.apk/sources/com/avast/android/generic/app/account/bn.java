package com.avast.android.generic.app.account;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.util.ae;
import com.avast.android.generic.util.t;

/* compiled from: SignInPickerFragment */
class bn extends r {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SignInPickerFragment f425a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bn(SignInPickerFragment signInPickerFragment, Context context) {
        super(context);
        this.f425a = signInPickerFragment;
    }

    public void a(String str) {
        if (this.f425a.isAdded()) {
            Toast.makeText(this.f425a.getActivity(), str, 1).show();
        }
    }

    public void a() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.app.account.SignInPickerFragment.a(com.avast.android.generic.app.account.SignInPickerFragment, boolean):void
     arg types: [com.avast.android.generic.app.account.SignInPickerFragment, int]
     candidates:
      com.avast.android.generic.app.account.SignInPickerFragment.a(com.avast.android.generic.app.account.SignInPickerFragment, com.avast.android.generic.app.account.bp):com.avast.android.generic.app.account.bp
      com.avast.android.generic.app.account.SignInPickerFragment.a(com.avast.android.generic.app.account.SignInPickerFragment, boolean):void */
    public void a(z zVar, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        if (this.f425a.isAdded()) {
            ((ab) aa.a(this.f425a.getActivity(), ab.class)).a(this.f425a.getActivity(), str, str2, str3, str4, str5, str6);
            a(zVar, str7);
            t.b("breadcrumbs", "Sending avast! account connected broadcast.");
            Intent intent = new Intent("com.avast.android.mobilesecurity.app.account.ACCOUNT_CONNECTED");
            ae.a(intent);
            this.f425a.getActivity().sendBroadcast(intent, "com.avast.android.generic.COMM_PERMISSION");
            this.f425a.a(true);
        }
    }
}
