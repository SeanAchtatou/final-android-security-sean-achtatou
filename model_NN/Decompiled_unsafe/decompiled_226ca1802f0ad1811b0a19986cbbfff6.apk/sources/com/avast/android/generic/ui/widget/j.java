package com.avast.android.generic.ui.widget;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import com.avast.android.generic.r;
import com.avast.android.generic.util.z;

/* compiled from: NextRow */
class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NextRow f1146a;

    j(NextRow nextRow) {
        this.f1146a = nextRow;
    }

    public void onClick(View view) {
        if (this.f1146a.f1094c != null && this.f1146a.f1093b != null) {
            try {
                FragmentTransaction beginTransaction = this.f1146a.f1094c.beginTransaction();
                Fragment fragment = (Fragment) Class.forName(this.f1146a.f1093b).newInstance();
                if (this.f1146a.m != 0) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("layoutId", this.f1146a.m);
                    fragment.setArguments(bundle);
                }
                beginTransaction.replace(r.root_container, fragment);
                beginTransaction.addToBackStack(null);
                beginTransaction.commit();
            } catch (Exception e) {
                z.a("AvastGeneric", "cannot open fragment " + this.f1146a.f1093b, e);
            }
        }
    }
}
