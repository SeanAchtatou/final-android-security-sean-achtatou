package com.avast.android.generic.app.wizard;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.o;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.ui.b.a;
import com.avast.android.generic.ui.widget.CheckBoxRow;
import com.avast.android.generic.util.ae;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.util.d;
import com.avast.android.generic.util.ga.TrackedMultiToolFragment;
import com.avast.android.generic.w;
import com.avast.android.generic.x;

public abstract class EulaFragment extends TrackedMultiToolFragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public TextView f584a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public TextView f585b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public int f586c = x.l_eula_show;
    /* access modifiers changed from: private */
    public long d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public d f;

    /* access modifiers changed from: protected */
    public abstract void a_();

    /* access modifiers changed from: protected */
    public abstract void b_();

    public final String c() {
        return "wizard/eula";
    }

    public int a() {
        return x.l_license;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f = d.b(getActivity());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(t.fragment_eula, viewGroup, false);
        this.e = true;
        if (!getActivity().getPackageName().equals("com.avast.android.mobilesecurity")) {
            ((LinearLayout) inflate.findViewById(r.v_community_iq)).setVisibility(8);
            this.e = false;
        }
        if (!ak.b(getActivity())) {
            b(inflate).setVisibility(8);
        }
        if (ak.b(getActivity())) {
            ((ScrollView) inflate).setBackgroundResource(o.bg_edge_color);
        }
        this.f584a = (TextView) inflate.findViewById(r.text);
        this.f585b = (TextView) inflate.findViewById(r.b_display);
        this.f584a.setText(Html.fromHtml(ak.a(getResources(), w.eula).toString()));
        a aVar = new a(this.f584a);
        aVar.setDuration(1500);
        this.f585b.setOnClickListener(new a(this, aVar));
        ((TextView) inflate.findViewById(r.b_privacy_policy)).setOnClickListener(new b(this));
        inflate.findViewById(r.b_agree).setOnClickListener(new c(this));
        inflate.findViewById(r.b_dont_agree).setOnClickListener(new d(this));
        Intent intent = new Intent("com.avast.android.generic.action.SHARE_SETTINGS");
        intent.putExtra("sourcePackage", getActivity().getPackageName());
        ae.a(intent);
        getActivity().sendBroadcast(intent);
        ab abVar = (ab) aa.a(getActivity(), ab.class);
        if (this.e) {
            CheckBoxRow checkBoxRow = (CheckBoxRow) inflate.findViewById(r.r_communityIQ);
            checkBoxRow.a(abVar.o());
            checkBoxRow.a(new e(this, abVar));
        }
        return inflate;
    }

    public void onResume() {
        super.onResume();
        this.d = SystemClock.uptimeMillis();
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
    }
}
