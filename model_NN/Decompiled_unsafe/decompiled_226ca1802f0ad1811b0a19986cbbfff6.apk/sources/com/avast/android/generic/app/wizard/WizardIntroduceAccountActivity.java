package com.avast.android.generic.app.wizard;

import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.avast.android.generic.ui.BaseSinglePaneActivity;

public abstract class WizardIntroduceAccountActivity extends BaseSinglePaneActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeButtonEnabled(false);
            supportActionBar.setDisplayShowTitleEnabled(true);
            if (!getPackageName().equals("com.avast.android.at_play") && !getPackageName().equals("com.avast.android.antitheft")) {
                supportActionBar.setDisplayUseLogoEnabled(false);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
