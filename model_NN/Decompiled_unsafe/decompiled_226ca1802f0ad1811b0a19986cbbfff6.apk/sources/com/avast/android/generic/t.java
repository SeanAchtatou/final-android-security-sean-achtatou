package com.avast.android.generic;

/* compiled from: R */
public final class t {

    /* renamed from: A */
    public static final int fragment_wizard_info_account = 2130903109;

    /* renamed from: B */
    public static final int hag__fragment_bread_crumb_item = 2130903110;

    /* renamed from: C */
    public static final int hag__fragment_bread_crumbs = 2130903111;

    /* renamed from: D */
    public static final int image_popup_window = 2130903112;

    /* renamed from: E */
    public static final int list_item_checker = 2130903113;

    /* renamed from: F */
    public static final int list_item_checker_header = 2130903114;

    /* renamed from: G */
    public static final int list_item_context = 2130903116;

    /* renamed from: H */
    public static final int list_item_filter_contacts_context = 2130903117;

    /* renamed from: I */
    public static final int list_item_header_notifications = 2130903118;

    /* renamed from: J */
    public static final int list_item_header_notifications_ongoing = 2130903119;

    /* renamed from: K */
    public static final int list_item_notification = 2130903120;

    /* renamed from: L */
    public static final int offer_discount = 2130903124;

    /* renamed from: M */
    public static final int row = 2130903127;

    /* renamed from: N */
    public static final int row_black_button = 2130903128;

    /* renamed from: O */
    public static final int row_check_box = 2130903129;

    /* renamed from: P */
    public static final int row_edit_text = 2130903130;

    /* renamed from: Q */
    public static final int row_image = 2130903132;

    /* renamed from: R */
    public static final int row_next = 2130903135;

    /* renamed from: S */
    public static final int row_subscription = 2130903136;

    /* renamed from: T */
    public static final int row_switch = 2130903137;

    /* renamed from: U */
    public static final int row_week_days = 2130903138;

    /* renamed from: V */
    public static final int textview_offer = 2130903141;

    /* renamed from: W */
    public static final int widget_slide_block_header = 2130903142;

    /* renamed from: a */
    public static final int activity_singlepane_empty = 2130903062;

    /* renamed from: b */
    public static final int button_offer = 2130903063;

    /* renamed from: c */
    public static final int button_offer_green = 2130903064;

    /* renamed from: d */
    public static final int button_subscription = 2130903065;

    /* renamed from: e */
    public static final int dialog_advertising = 2130903080;

    /* renamed from: f */
    public static final int dialog_avast_account_disconnect = 2130903081;

    /* renamed from: g */
    public static final int dialog_custom_number = 2130903082;

    /* renamed from: h */
    public static final int dialog_password = 2130903084;

    /* renamed from: i */
    public static final int dialog_password_change = 2130903085;

    /* renamed from: j */
    public static final int dialog_voucher = 2130903086;

    /* renamed from: k */
    public static final int dropdown_item = 2130903087;

    /* renamed from: l */
    public static final int eula_slider_content = 2130903088;

    /* renamed from: m */
    public static final int fragment_about = 2130903089;

    /* renamed from: n */
    public static final int fragment_avast_notifications = 2130903090;

    /* renamed from: o */
    public static final int fragment_checker = 2130903091;

    /* renamed from: p */
    public static final int fragment_connect_account = 2130903093;

    /* renamed from: q */
    public static final int fragment_connectioncheck = 2130903094;

    /* renamed from: r */
    public static final int fragment_empty_pane = 2130903096;

    /* renamed from: s */
    public static final int fragment_eula = 2130903097;

    /* renamed from: t */
    public static final int fragment_feedback = 2130903098;

    /* renamed from: u */
    public static final int fragment_filebrowser = 2130903099;

    /* renamed from: v */
    public static final int fragment_settings_general = 2130903104;

    /* renamed from: w */
    public static final int fragment_signin_picker = 2130903105;

    /* renamed from: x */
    public static final int fragment_subscription_suite = 2130903106;

    /* renamed from: y */
    public static final int fragment_web_purchase = 2130903107;

    /* renamed from: z */
    public static final int fragment_welcome_premium = 2130903108;
}
