package com.avast.android.generic.util;

import android.content.Context;
import android.content.pm.PackageManager;

/* compiled from: VersionInfo */
public class av {
    private static av e;

    /* renamed from: a  reason: collision with root package name */
    private int f1194a = 2;

    /* renamed from: b  reason: collision with root package name */
    private int f1195b = 0;

    /* renamed from: c  reason: collision with root package name */
    private int f1196c = 0;
    private boolean d = false;
    private boolean f = false;

    public int a() {
        return this.f1196c;
    }

    public String b() {
        if (this.f) {
            return String.valueOf(this.f1194a - 2);
        }
        return String.valueOf(this.f1194a);
    }

    public String c() {
        return String.valueOf(this.f1195b);
    }

    public String d() {
        return String.valueOf(this.f1196c + (this.d ? " BETA" : ""));
    }

    public static av a(Context context) {
        if (e != null) {
            return e;
        }
        e = new av(context);
        return e;
    }

    private av(Context context) {
        int i;
        try {
            String packageName = context.getPackageName();
            if (packageName == null) {
                throw new Exception();
            }
            if (packageName.equals("com.avast.android.antitheft") || packageName.equals("com.avast.android.at_play") || packageName.equals("com.avast.android.backup")) {
                this.f1194a = 5;
                this.f = true;
            } else {
                this.f1194a = 3;
            }
            this.f1195b = 0;
            try {
                i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e2) {
                t.b(e2.getMessage(), e2);
                i = -1;
            }
            if (i == -1) {
                throw new Exception();
            }
            this.f1196c = i;
            this.d = false;
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
