package com.avast.android.generic.ui;

import android.view.View;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.app.passwordrecovery.a;
import com.avast.android.generic.app.passwordrecovery.b;
import com.avast.android.generic.r;

/* compiled from: ChangePasswordDialog */
class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1041a;

    e(d dVar) {
        this.f1041a = dVar;
    }

    public void onClick(View view) {
        String a2 = this.f1041a.f1030b.f983a.a();
        if (this.f1041a.f1030b.a(a2, this.f1041a.f1030b.f984b.a()) && !"0000".equals(a2)) {
            ab abVar = (ab) aa.a(this.f1041a.f1030b.getActivity(), ab.class);
            abVar.d(a2);
            a.a(this.f1041a.f1030b.getActivity(), b.UNINITIATED);
            if (this.f1041a.f1030b.getArguments() != null && this.f1041a.f1030b.getArguments().getBoolean("enableProtection")) {
                abVar.a(true);
            }
            this.f1041a.f1030b.a(r.message_password_change_successful);
            this.f1041a.f1029a.dismiss();
        }
    }
}
