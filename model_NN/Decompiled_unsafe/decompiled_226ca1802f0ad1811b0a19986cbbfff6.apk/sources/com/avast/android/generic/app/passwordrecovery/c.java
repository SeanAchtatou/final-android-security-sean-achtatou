package com.avast.android.generic.app.passwordrecovery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: PasswordRecoveryDialog */
class c extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PasswordRecoveryDialog f483a;

    c(PasswordRecoveryDialog passwordRecoveryDialog) {
        this.f483a = passwordRecoveryDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.app.passwordrecovery.PasswordRecoveryDialog.a(com.avast.android.generic.app.passwordrecovery.PasswordRecoveryDialog, boolean):void
     arg types: [com.avast.android.generic.app.passwordrecovery.PasswordRecoveryDialog, int]
     candidates:
      com.avast.android.generic.app.passwordrecovery.PasswordRecoveryDialog.a(com.avast.android.generic.app.passwordrecovery.PasswordRecoveryDialog, android.content.Context):void
      com.avast.android.generic.app.passwordrecovery.PasswordRecoveryDialog.a(com.avast.android.generic.app.passwordrecovery.PasswordRecoveryDialog, boolean):void */
    public void onReceive(Context context, Intent intent) {
        switch (i.f490a[b.a(intent.getIntExtra("state_code", 0)).ordinal()]) {
            case 1:
                this.f483a.d.a(new Intent("com.avast.android.generic.app.passwordrecovery.ACTION_RECOVERY_INITIATED"));
                this.f483a.a(true);
                return;
            case 2:
            case 3:
                this.f483a.a(false);
                return;
            default:
                return;
        }
    }
}
