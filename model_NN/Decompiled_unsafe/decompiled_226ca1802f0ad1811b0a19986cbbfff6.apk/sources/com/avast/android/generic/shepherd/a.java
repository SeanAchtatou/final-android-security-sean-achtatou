package com.avast.android.generic.shepherd;

import android.content.Context;
import java.io.File;

/* compiled from: PersistentShepherdConfig */
class a {
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0041 A[SYNTHETIC, Splitter:B:22:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0058 A[SYNTHETIC, Splitter:B:30:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x006b A[SYNTHETIC, Splitter:B:37:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void a(android.content.Context r5, com.avast.e.a.i r6) {
        /*
            if (r5 == 0) goto L_0x0004
            if (r6 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            java.io.File r2 = b(r5)
            boolean r0 = r2.exists()
            if (r0 == 0) goto L_0x0012
            r2.delete()
        L_0x0012:
            r2.createNewFile()     // Catch:{ IOException -> 0x0034 }
            r1 = 0
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x003d, IOException -> 0x004e }
            r0.<init>(r2)     // Catch:{ FileNotFoundException -> 0x003d, IOException -> 0x004e }
            byte[] r1 = r6.toByteArray()     // Catch:{ FileNotFoundException -> 0x0082, IOException -> 0x007d, all -> 0x0078 }
            r0.write(r1)     // Catch:{ FileNotFoundException -> 0x0082, IOException -> 0x007d, all -> 0x0078 }
            r0.flush()     // Catch:{ FileNotFoundException -> 0x0082, IOException -> 0x007d, all -> 0x0078 }
            if (r0 == 0) goto L_0x0004
            r0.close()     // Catch:{ IOException -> 0x002b }
            goto L_0x0004
        L_0x002b:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.avast.android.generic.util.t.a(r1, r0)
            goto L_0x0004
        L_0x0034:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.avast.android.generic.util.t.b(r1, r0)
            goto L_0x0004
        L_0x003d:
            r0 = move-exception
            r0 = r1
        L_0x003f:
            if (r0 == 0) goto L_0x0004
            r0.close()     // Catch:{ IOException -> 0x0045 }
            goto L_0x0004
        L_0x0045:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.avast.android.generic.util.t.a(r1, r0)
            goto L_0x0004
        L_0x004e:
            r0 = move-exception
        L_0x004f:
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0068 }
            com.avast.android.generic.util.t.b(r3, r0)     // Catch:{ all -> 0x0068 }
            if (r1 == 0) goto L_0x005b
            r1.close()     // Catch:{ IOException -> 0x005f }
        L_0x005b:
            r2.delete()
            goto L_0x0004
        L_0x005f:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.avast.android.generic.util.t.a(r1, r0)
            goto L_0x005b
        L_0x0068:
            r0 = move-exception
        L_0x0069:
            if (r1 == 0) goto L_0x006e
            r1.close()     // Catch:{ IOException -> 0x006f }
        L_0x006e:
            throw r0
        L_0x006f:
            r1 = move-exception
            java.lang.String r2 = r1.getMessage()
            com.avast.android.generic.util.t.a(r2, r1)
            goto L_0x006e
        L_0x0078:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0069
        L_0x007d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x004f
        L_0x0082:
            r1 = move-exception
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.shepherd.a.a(android.content.Context, com.avast.e.a.i):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x003b A[SYNTHETIC, Splitter:B:26:0x003b] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0046 A[SYNTHETIC, Splitter:B:32:0x0046] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:15:0x0021=Splitter:B:15:0x0021, B:23:0x0032=Splitter:B:23:0x0032} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.avast.e.a.i a(android.content.Context r4) {
        /*
            r0 = 0
            if (r4 != 0) goto L_0x0004
        L_0x0003:
            return r0
        L_0x0004:
            java.io.File r1 = b(r4)
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x0003
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x001f, IOException -> 0x0030, all -> 0x0041 }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x001f, IOException -> 0x0030, all -> 0x0041 }
            com.avast.e.a.i r0 = com.avast.e.a.i.a(r2)     // Catch:{ FileNotFoundException -> 0x0050, IOException -> 0x004e }
            if (r2 == 0) goto L_0x0003
            r2.close()     // Catch:{ IOException -> 0x001d }
            goto L_0x0003
        L_0x001d:
            r1 = move-exception
            goto L_0x0003
        L_0x001f:
            r1 = move-exception
            r2 = r0
        L_0x0021:
            java.lang.String r3 = r1.getMessage()     // Catch:{ all -> 0x004c }
            com.avast.android.generic.util.t.b(r3, r1)     // Catch:{ all -> 0x004c }
            if (r2 == 0) goto L_0x0003
            r2.close()     // Catch:{ IOException -> 0x002e }
            goto L_0x0003
        L_0x002e:
            r1 = move-exception
            goto L_0x0003
        L_0x0030:
            r1 = move-exception
            r2 = r0
        L_0x0032:
            java.lang.String r3 = r1.getMessage()     // Catch:{ all -> 0x004c }
            com.avast.android.generic.util.t.b(r3, r1)     // Catch:{ all -> 0x004c }
            if (r2 == 0) goto L_0x0003
            r2.close()     // Catch:{ IOException -> 0x003f }
            goto L_0x0003
        L_0x003f:
            r1 = move-exception
            goto L_0x0003
        L_0x0041:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0044:
            if (r2 == 0) goto L_0x0049
            r2.close()     // Catch:{ IOException -> 0x004a }
        L_0x0049:
            throw r0
        L_0x004a:
            r1 = move-exception
            goto L_0x0049
        L_0x004c:
            r0 = move-exception
            goto L_0x0044
        L_0x004e:
            r1 = move-exception
            goto L_0x0032
        L_0x0050:
            r1 = move-exception
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.shepherd.a.a(android.content.Context):com.avast.e.a.i");
    }

    private static File b(Context context) {
        return new File(context.getDir("shepherd", 0), "shepherd.config");
    }
}
