package com.avast.android.generic.g.a;

/* compiled from: SmsSender */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f684a;

    c(b bVar) {
        this.f684a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.b, boolean):boolean
     arg types: [com.avast.android.generic.g.a.b, int]
     candidates:
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.b, java.lang.Thread):java.lang.Thread
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.b, com.avast.android.generic.g.a.e):void
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.b, boolean):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00b7 A[LOOP:2: B:44:0x00b7->B:54:0x00ce, LOOP_START, PHI: r0 
      PHI: (r0v21 'th' java.lang.Throwable) = (r0v20 'th' java.lang.Throwable), (r0v22 'th' java.lang.Throwable) binds: [B:65:0x00b7, B:54:0x00ce] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:44:0x00b7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            r2 = 1
            r3 = 0
            r6 = 0
            r1 = r2
        L_0x0005:
            com.avast.android.generic.g.a.b r0 = r12.f684a
            boolean r0 = r0.l
            if (r0 != 0) goto L_0x00b9
            if (r1 == 0) goto L_0x00b9
            java.lang.String r0 = "AvastComms"
            com.avast.android.generic.g.a.b r4 = r12.f684a
            android.content.Context r4 = r4.h
            java.lang.String r5 = "SMS sender thread begin"
            com.avast.android.generic.util.z.a(r0, r4, r5)
            com.avast.android.generic.g.a.b r0 = r12.f684a     // Catch:{ Exception -> 0x00a9 }
            java.util.LinkedList r8 = r0.f     // Catch:{ Exception -> 0x00a9 }
            monitor-enter(r8)     // Catch:{ Exception -> 0x00a9 }
            java.util.Date r0 = new java.util.Date     // Catch:{ all -> 0x00a6 }
            r0.<init>()     // Catch:{ all -> 0x00a6 }
            long r4 = r0.getTime()     // Catch:{ all -> 0x00a6 }
            com.avast.android.generic.g.a.b r0 = r12.f684a     // Catch:{ all -> 0x00a6 }
            java.util.LinkedList r0 = r0.f     // Catch:{ all -> 0x00a6 }
            int r0 = r0.size()     // Catch:{ all -> 0x00a6 }
            if (r0 <= 0) goto L_0x00d4
            com.avast.android.generic.g.a.b r0 = r12.f684a     // Catch:{ all -> 0x00a6 }
            java.util.LinkedList r0 = r0.f     // Catch:{ all -> 0x00a6 }
            java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x00a6 }
            com.avast.android.generic.g.a.e r0 = (com.avast.android.generic.g.a.e) r0     // Catch:{ all -> 0x00a6 }
            long r9 = r0.f     // Catch:{ all -> 0x00a6 }
            int r0 = (r9 > r6 ? 1 : (r9 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x00d4
            int r0 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x00d4
            long r4 = r9 - r4
        L_0x0050:
            monitor-exit(r8)     // Catch:{ all -> 0x00a6 }
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x007c
            java.lang.String r0 = "AvastComms"
            com.avast.android.generic.g.a.b r8 = r12.f684a     // Catch:{ InterruptedException -> 0x00d0 }
            android.content.Context r8 = r8.h     // Catch:{ InterruptedException -> 0x00d0 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x00d0 }
            r9.<init>()     // Catch:{ InterruptedException -> 0x00d0 }
            java.lang.String r10 = "SMS sender thread sleeping for "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ InterruptedException -> 0x00d0 }
            java.lang.StringBuilder r9 = r9.append(r4)     // Catch:{ InterruptedException -> 0x00d0 }
            java.lang.String r10 = " ms..."
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ InterruptedException -> 0x00d0 }
            java.lang.String r9 = r9.toString()     // Catch:{ InterruptedException -> 0x00d0 }
            com.avast.android.generic.util.z.a(r0, r8, r9)     // Catch:{ InterruptedException -> 0x00d0 }
            java.lang.Thread.sleep(r4)     // Catch:{ InterruptedException -> 0x00d0 }
        L_0x007c:
            com.avast.android.generic.g.a.b r0 = r12.f684a     // Catch:{ Exception -> 0x00a9 }
            r0.h()     // Catch:{ Exception -> 0x00a9 }
            com.avast.android.generic.g.a.b r0 = r12.f684a     // Catch:{ Exception -> 0x00c5 }
            java.util.LinkedList r4 = r0.f     // Catch:{ Exception -> 0x00c5 }
            monitor-enter(r4)     // Catch:{ Exception -> 0x00c5 }
            com.avast.android.generic.g.a.b r0 = r12.f684a     // Catch:{ all -> 0x00b5 }
            java.util.LinkedList r0 = r0.f     // Catch:{ all -> 0x00b5 }
            int r0 = r0.size()     // Catch:{ all -> 0x00b5 }
            if (r0 <= 0) goto L_0x00d2
            r0 = r2
        L_0x0095:
            monitor-exit(r4)     // Catch:{ all -> 0x00c9 }
        L_0x0096:
            java.lang.String r1 = "AvastComms"
            com.avast.android.generic.g.a.b r4 = r12.f684a
            android.content.Context r4 = r4.h
            java.lang.String r5 = "SMS sender thread end"
            com.avast.android.generic.util.z.a(r1, r4, r5)
            r1 = r0
            goto L_0x0005
        L_0x00a6:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x00a6 }
            throw r0     // Catch:{ Exception -> 0x00a9 }
        L_0x00a9:
            r0 = move-exception
            r11 = r0
            r0 = r1
            r1 = r11
        L_0x00ad:
            java.lang.String r4 = "AvastComms"
            java.lang.String r5 = "Error in SMS sender"
            com.avast.android.generic.util.z.a(r4, r5, r1)
            goto L_0x0096
        L_0x00b5:
            r0 = move-exception
            r1 = r3
        L_0x00b7:
            monitor-exit(r4)     // Catch:{ all -> 0x00ce }
            throw r0     // Catch:{ Exception -> 0x00a9 }
        L_0x00b9:
            com.avast.android.generic.g.a.b r0 = r12.f684a
            boolean unused = r0.l = r3
            com.avast.android.generic.g.a.b r0 = r12.f684a
            r1 = 0
            java.lang.Thread unused = r0.k = r1
            return
        L_0x00c5:
            r0 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x00ad
        L_0x00c9:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x00b7
        L_0x00ce:
            r0 = move-exception
            goto L_0x00b7
        L_0x00d0:
            r0 = move-exception
            goto L_0x007c
        L_0x00d2:
            r0 = r3
            goto L_0x0095
        L_0x00d4:
            r4 = r6
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.g.a.c.run():void");
    }
}
