package com.avast.android.generic.app.account;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.text.TextUtils;
import com.avast.android.generic.internet.b;
import com.avast.android.generic.o;
import com.avast.android.generic.q;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.aa;
import com.avast.android.generic.util.ah;
import com.avast.android.generic.util.am;
import com.avast.android.generic.util.w;
import com.avast.android.generic.x;
import com.avast.b.a.a.c;

/* compiled from: ConnectionCheckFragment */
class aj extends am<Void, Void, Void> {

    /* renamed from: a  reason: collision with root package name */
    boolean f385a = false;

    /* renamed from: b  reason: collision with root package name */
    boolean f386b = false;

    /* renamed from: c  reason: collision with root package name */
    boolean f387c = false;
    String d = "";
    boolean e = false;
    boolean f = false;
    boolean g = true;
    boolean h = true;
    boolean i = true;
    boolean j = false;
    boolean k = false;
    boolean l = false;
    boolean m = false;
    boolean n = false;
    boolean o = false;
    e p = new e(f.VALID, "");
    c q = c.NONE;
    final /* synthetic */ int r;
    final /* synthetic */ ConnectionCheckFragment s;
    private Context t;

    aj(ConnectionCheckFragment connectionCheckFragment, int i2) {
        this.s = connectionCheckFragment;
        this.r = i2;
    }

    public void a() {
        if (this.s.isAdded()) {
            this.t = this.s.getActivity();
            this.s.f362a.setTextColor(this.s.getResources().getColor(o.g));
            this.s.f363b.setOnClickListener(null);
            this.s.f363b.setEnabled(false);
            this.s.f363b.setBackgroundResource(q.i);
            this.s.f364c.setEnabled(false);
            switch (this.r) {
                case 1:
                    this.s.f362a.setText(x.l_conn_check_anti_theft_enabled_check);
                    return;
                case 2:
                    this.s.f362a.setText(x.l_conn_check_version_check);
                    return;
                case 3:
                    this.s.f362a.setText(x.l_conn_check_android_check);
                    return;
                case 4:
                    this.s.f362a.setText(x.l_conn_check_settings_check);
                    return;
                case 5:
                    this.s.f362a.setText(x.l_conn_check_web_connection_check);
                    return;
                case 6:
                    this.s.f362a.setText(this.s.getString(x.l_conn_check_receive_message_check, 20));
                    return;
                default:
                    return;
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.app.account.ConnectionCheckFragment.a(com.avast.android.generic.app.account.ConnectionCheckFragment, boolean):boolean
     arg types: [com.avast.android.generic.app.account.ConnectionCheckFragment, int]
     candidates:
      com.avast.android.generic.app.account.DisconnectFragment.a(com.avast.android.generic.app.account.DisconnectFragment, com.avast.android.generic.app.account.bd):void
      com.avast.android.generic.app.account.ConnectionCheckFragment.a(com.avast.android.generic.app.account.ConnectionCheckFragment, boolean):boolean */
    public Void a(Void... voidArr) {
        boolean z;
        if (this.s.isAdded()) {
            try {
                Thread.sleep(1000);
                if (this.s.isAdded() && this.t != null) {
                    switch (this.r) {
                        case 1:
                            this.j = this.s.g.b("system", false);
                            this.f387c = false;
                            break;
                        case 2:
                            if (aa.j(this.t)) {
                                this.l = true;
                            }
                            if (aa.a(this.t) && !aa.a(this.t, 4586)) {
                                this.k = true;
                            }
                            if (!this.k && aa.b(this.t) != null && aa.b(this.t, 4586) == null) {
                                this.m = true;
                            }
                            if (!this.m && aa.d(this.t) && !aa.c(this.t, 4586)) {
                                this.n = true;
                            }
                            if (!this.n && aa.e(this.t) && !aa.d(this.t, 4586)) {
                                this.o = true;
                            }
                            this.f387c = false;
                            break;
                        case 3:
                            try {
                                this.g = Integer.parseInt(Settings.Secure.getString(this.t.getContentResolver(), "background_data")) > 0;
                            } catch (Exception e2) {
                            }
                            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.t.getSystemService("connectivity")).getActiveNetworkInfo();
                            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                                z = false;
                            } else {
                                z = true;
                            }
                            this.h = z;
                            this.i = ah.a(this.t);
                            this.f387c = false;
                            break;
                        case 4:
                            String w = this.s.g.w();
                            String y = this.s.g.y();
                            String D = this.s.h.D();
                            if (!TextUtils.isEmpty(w) && !TextUtils.isEmpty(y)) {
                                this.e = true;
                            }
                            if (!TextUtils.isEmpty(D)) {
                                this.f = true;
                            }
                            if (!this.e || !this.f || !this.s.i || !this.s.j) {
                                this.p = new e(f.VALID, "");
                            } else {
                                this.p = d.a(this.t, this.s.g);
                            }
                            this.f387c = false;
                            break;
                        case 5:
                            this.s.f();
                            boolean unused = this.s.d = false;
                            this.s.e();
                            this.q = b.a(this.t, this.s.g, this.s.g.w(), this.s.g.y());
                            if (this.q != c.NONE) {
                                this.s.f();
                            }
                            this.f387c = false;
                            break;
                        case 6:
                        default:
                            this.f387c = false;
                            break;
                    }
                }
            } catch (Exception e3) {
                this.s.f();
                throw e3;
            } catch (Exception e4) {
                if (this.s.isAdded()) {
                    this.f385a = true;
                    this.f387c = true;
                    this.d = this.s.getString(x.ay, w.a(this.t, e4));
                }
            }
        }
        return null;
    }

    public void a(Void voidR) {
        if (this.s.isAdded()) {
            this.f386b = false;
            a.a(this.s, new ak(this));
        }
    }
}
