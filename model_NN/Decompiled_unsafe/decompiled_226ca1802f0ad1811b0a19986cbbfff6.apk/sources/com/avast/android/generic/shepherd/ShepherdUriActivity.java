package com.avast.android.generic.shepherd;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.util.t;
import java.util.HashSet;
import java.util.List;

public class ShepherdUriActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z;
        boolean z2;
        super.onCreate(bundle);
        Uri data = getIntent().getData();
        ab abVar = (ab) aa.a(this, ab.class);
        t.c("ShepherdUriActivity: URI=" + data.toString());
        List<String> pathSegments = data.getPathSegments();
        if (pathSegments == null || pathSegments.size() < 3) {
            finish();
        }
        if (TextUtils.isEmpty(pathSegments.get(1)) || !"tags".equals(pathSegments.get(1).toLowerCase())) {
            finish();
        }
        String lastPathSegment = data.getLastPathSegment();
        if ("delall".equals(lastPathSegment)) {
            if (!abVar.aa().isEmpty()) {
                t.c("ShepherdUriActivity: deleting all tags");
                abVar.a((String[]) null);
                z2 = true;
            } else {
                z2 = false;
            }
            z = z2;
        } else {
            if ("add".equals(lastPathSegment)) {
                String query = data.getQuery();
                if (!TextUtils.isEmpty(query)) {
                    HashSet<String> aa = abVar.aa();
                    String[] split = query.split(",");
                    z = false;
                    for (int i = 0; i < split.length; i++) {
                        t.c("ShepherdUriActivity: adding tag=" + split[i].toLowerCase());
                        if (aa.add(split[i].toLowerCase()) || z) {
                            z = true;
                        } else {
                            z = false;
                        }
                    }
                    if (z) {
                        abVar.a(aa);
                    }
                }
            } else if ("del".equals(lastPathSegment)) {
                String query2 = data.getQuery();
                if (!TextUtils.isEmpty(query2)) {
                    HashSet<String> aa2 = abVar.aa();
                    if (!aa2.isEmpty()) {
                        String[] split2 = query2.split(",");
                        boolean z3 = false;
                        for (int i2 = 0; i2 < split2.length; i2++) {
                            t.c("ShepherdUriActivity: deleting tag=" + split2[i2].toLowerCase());
                            if (aa2.remove(split2[i2].toLowerCase()) || z) {
                                z3 = true;
                            } else {
                                z3 = false;
                            }
                        }
                        if (z) {
                            abVar.a(aa2);
                        }
                    }
                }
            }
            z = false;
        }
        if (z) {
            ShepherdDownloadService.a(this, true);
        }
        finish();
    }
}
