package com.avast.android.generic.ui.rtl;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;

public class TableLayoutLtrToRtlConverter extends a {
    public TableLayoutLtrToRtlConverter(String str) {
        super(str);
    }

    public View ltrToRtlView(View view) {
        if (view instanceof TableLayout) {
            return ltrToRtlIfTableLayout((TableLayout) view);
        }
        throw new IllegalArgumentException("The view must be an instance of TableLayout and it's " + view.getClass() + ".");
    }

    private View ltrToRtlIfTableLayout(TableLayout tableLayout) {
        int i;
        int childCount = tableLayout.getChildCount();
        int i2 = 0;
        int i3 = 0;
        while (i2 < childCount) {
            View childAt = tableLayout.getChildAt(i2);
            if (childAt instanceof ViewGroup) {
                i = Math.max(i3, ((ViewGroup) childAt).getChildCount());
            } else {
                i = i3;
            }
            swapMarginAndPadding(childAt);
            i2++;
            i3 = i;
        }
        return tableLayout;
    }

    private void swapColumnProperties(TableLayout tableLayout, int i) {
        for (int i2 = 0; i2 < i / 2; i2++) {
            int i3 = (i - 1) - i2;
            boolean isColumnStretchable = tableLayout.isColumnStretchable(i2);
            tableLayout.setColumnStretchable(i2, tableLayout.isColumnStretchable(i3));
            tableLayout.setColumnStretchable(i3, isColumnStretchable);
            boolean isColumnShrinkable = tableLayout.isColumnShrinkable(i2);
            tableLayout.setColumnShrinkable(i2, tableLayout.isColumnShrinkable(i3));
            tableLayout.setColumnShrinkable(i3, isColumnShrinkable);
            boolean isColumnCollapsed = tableLayout.isColumnCollapsed(i2);
            tableLayout.setColumnCollapsed(i2, tableLayout.isColumnCollapsed(i3));
            tableLayout.setColumnCollapsed(i3, isColumnCollapsed);
        }
    }

    private void swapMarginAndPadding(View view) {
        if (view.getLayoutParams() instanceof TableRow.LayoutParams) {
            TableRow.LayoutParams layoutParams = (TableRow.LayoutParams) view.getLayoutParams();
            int i = layoutParams.leftMargin;
            layoutParams.leftMargin = layoutParams.rightMargin;
            layoutParams.rightMargin = i;
        }
        view.setPadding(view.getPaddingRight(), view.getPaddingTop(), view.getPaddingLeft(), view.getPaddingBottom());
    }
}
