package com.avast.android.generic.ui;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import com.avast.android.generic.q;
import com.avast.android.generic.x;

/* compiled from: ChangePasswordDialog */
class f implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChangePasswordDialog f1042a;

    f(ChangePasswordDialog changePasswordDialog) {
        this.f1042a = changePasswordDialog;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        String string;
        boolean z5 = true;
        if (this.f1042a.isAdded()) {
            if (this.f1042a.f983a.length() > 0) {
                this.f1042a.d.setVisibility(0);
                if (this.f1042a.f983a.length() < 4) {
                    z = true;
                } else {
                    z = false;
                }
                if (this.f1042a.f983a.length() > 6) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                boolean equals = "0000".equals(this.f1042a.f983a.a());
                if (!TextUtils.isDigitsOnly(this.f1042a.f983a.a())) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                if (z || z2 || equals || z3) {
                    z4 = true;
                } else {
                    z4 = false;
                }
                if (z4) {
                    this.f1042a.d.setImageResource(q.ic_scanner_result_problem);
                } else {
                    this.f1042a.d.setImageResource(q.ic_scanner_result_ok);
                }
                if (this.f1042a.f984b.length() < this.f1042a.f983a.length() || this.f1042a.f984b.length() <= 0) {
                    this.f1042a.e.setVisibility(4);
                } else {
                    this.f1042a.e.setVisibility(0);
                }
                if (this.f1042a.f984b.length() <= 0 || this.f1042a.f983a.a().equals(this.f1042a.f984b.a())) {
                    z5 = false;
                }
                if (z4 || z5) {
                    if (z5) {
                        string = this.f1042a.getString(x.pref_password_change_not_match);
                    } else if (z3) {
                        string = this.f1042a.getString(x.pref_password_non_digits);
                    } else if (z2) {
                        string = this.f1042a.getString(x.pref_password_too_long);
                    } else if (equals) {
                        string = this.f1042a.getString(x.pref_password_default);
                    } else {
                        string = this.f1042a.getString(x.pref_password_too_short);
                    }
                    this.f1042a.f985c.setText(string);
                    this.f1042a.f985c.setVisibility(0);
                    this.f1042a.e.setImageResource(q.ic_scanner_result_problem);
                    return;
                }
                this.f1042a.f985c.setVisibility(8);
                this.f1042a.e.setImageResource(q.ic_scanner_result_ok);
                return;
            }
            this.f1042a.d.setVisibility(4);
            this.f1042a.f985c.setVisibility(8);
        }
    }
}
