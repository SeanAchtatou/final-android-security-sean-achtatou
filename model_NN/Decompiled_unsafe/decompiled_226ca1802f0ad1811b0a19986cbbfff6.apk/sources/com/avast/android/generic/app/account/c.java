package com.avast.android.generic.app.account;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import com.avast.android.generic.a;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.x;

/* compiled from: AccountSettingsFragment */
class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AccountSettingsFragment f427a;

    c(AccountSettingsFragment accountSettingsFragment) {
        this.f427a = accountSettingsFragment;
    }

    public void onClick(View view) {
        if (this.f427a.isAdded()) {
            if (Build.VERSION.SDK_INT < 8) {
                a.a(this.f427a.getActivity(), this.f427a.getString(x.msg_avast_account_c2dm_needs_api_level_8));
            } else if (this.f427a.h().v() == null || this.f427a.h().v().equals("")) {
                Bundle bundle = new Bundle();
                if (ak.b(this.f427a.getActivity())) {
                    bundle.putAll(this.f427a.getArguments());
                } else if (!(this.f427a.getActivity().getIntent() == null || this.f427a.getActivity().getIntent().getExtras() == null)) {
                    bundle.putAll(this.f427a.getActivity().getIntent().getExtras());
                }
                if (this.f427a.l()) {
                    bundle.putBoolean("queryPhoneNumber", false);
                }
                AccountActivity.call(this.f427a.getActivity(), bundle);
            } else {
                this.f427a.g();
            }
        }
    }
}
