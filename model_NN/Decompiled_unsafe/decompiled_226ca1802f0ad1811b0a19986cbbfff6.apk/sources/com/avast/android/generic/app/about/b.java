package com.avast.android.generic.app.about;

import android.os.Handler;
import com.avast.android.generic.ui.widget.SlideBlock;
import com.avast.android.generic.ui.widget.y;
import com.avast.android.generic.x;

/* compiled from: AboutFragment */
class b implements y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SlideBlock f333a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Handler f334b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ float f335c;
    final /* synthetic */ AboutFragment d;

    b(AboutFragment aboutFragment, SlideBlock slideBlock, Handler handler, float f) {
        this.d = aboutFragment;
        this.f333a = slideBlock;
        this.f334b = handler;
        this.f335c = f;
    }

    public void a(SlideBlock slideBlock) {
        this.d.a("ms-About", "eulaText", "show", 0);
        this.f333a.a(this.d.getString(x.about_eula_hide));
        this.f334b.sendMessageDelayed(this.f334b.obtainMessage(1, (int) (100.0f * this.f335c), 500), 300);
    }

    public void b(SlideBlock slideBlock) {
        this.d.a("ms-About", "eulaText", "hide", 0);
        this.f333a.a(this.d.getString(x.about_eula_show));
    }
}
