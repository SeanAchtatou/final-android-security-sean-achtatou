package com.avast.android.generic.licensing.c;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: Inventory */
public class j {

    /* renamed from: a  reason: collision with root package name */
    Map<String, n> f867a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    Map<String, l> f868b = new HashMap();

    j() {
    }

    public n a(String str) {
        return this.f867a.get(str);
    }

    public boolean b(String str) {
        return this.f867a.containsKey(str);
    }

    /* access modifiers changed from: package-private */
    public List<String> c(String str) {
        ArrayList arrayList = new ArrayList();
        for (l next : this.f868b.values()) {
            if (next.a().equals(str)) {
                arrayList.add(next.c());
            }
        }
        return arrayList;
    }

    public List<l> a() {
        return new ArrayList(this.f868b.values());
    }

    /* access modifiers changed from: package-private */
    public void a(n nVar) {
        this.f867a.put(nVar.a(), nVar);
    }

    /* access modifiers changed from: package-private */
    public void a(l lVar) {
        this.f868b.put(lVar.c(), lVar);
    }
}
