package com.avast.android.generic.filebrowser;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.actionbarsherlock.view.ActionMode;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.ui.BaseActivity;
import com.avast.android.generic.util.ga.TrackedMultiToolListFragment;
import com.avast.android.generic.v;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractFileBrowserFragment extends TrackedMultiToolListFragment implements LoaderManager.LoaderCallbacks<List<e>>, AdapterView.OnItemLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    protected a f671a;

    /* renamed from: b  reason: collision with root package name */
    protected String f672b;

    /* renamed from: c  reason: collision with root package name */
    private d f673c;
    private boolean d;
    /* access modifiers changed from: private */
    public ListView e;
    private LinkedList<e> f;
    /* access modifiers changed from: private */
    public e g;
    private View h;
    private View i;
    /* access modifiers changed from: private */
    public Button j;
    private TextView k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public ActionMode m;
    private ActionMode.Callback n = new b(this);
    /* access modifiers changed from: private */
    public ActionMode.Callback o;

    /* access modifiers changed from: protected */
    public abstract ActionMode.Callback a();

    /* access modifiers changed from: protected */
    public abstract Bundle b(e eVar);

    /* access modifiers changed from: protected */
    public abstract String f();

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f672b = BaseActivity.a(getArguments()).getAction();
        if (this.f672b == null) {
            this.f672b = "";
        }
        this.f = new LinkedList<>();
        this.f673c = h();
        this.d = getArguments().getBoolean("pick_multiple", false);
        this.o = a();
        this.l = f() + "://";
        setRetainInstance(true);
    }

    private d h() {
        d dVar = d.BOTH;
        if (!getArguments().containsKey("pick")) {
            return dVar;
        }
        try {
            return d.valueOf(getArguments().getString("pick"));
        } catch (IllegalArgumentException e2) {
            return d.BOTH;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(t.fragment_filebrowser, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.h = view.findViewById(r.filebrowser_progress);
        this.i = view.findViewById(r.filebrowser_empty);
        this.k = (TextView) view.findViewById(r.filebrowser_path);
        this.j = (Button) view.findViewById(r.filebrowser_button_pick);
        if (m()) {
            this.j.setVisibility(0);
            this.j.setOnClickListener(new c(this));
        }
        if (this.f671a != null && getListAdapter() == null) {
            setListAdapter(this.f671a);
        }
        i();
        getLoaderManager().initLoader(986198516, b(this.g), this);
        this.e = getListView();
        this.e.setChoiceMode(2);
        this.e.setOnItemLongClickListener(this);
        if (this.m != null) {
            this.m = getSherlockActivity().startActionMode(this.n);
        }
    }

    private void i() {
        if (this.g != null) {
            this.k.setText(j());
            c(this.g);
        }
    }

    private String j() {
        return this.g.b();
    }

    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        if (this.m != null) {
            a(i2);
            return;
        }
        this.e.setItemChecked(i2, false);
        e a2 = this.f671a.a(i2);
        if (a2.d()) {
            this.f.add(this.g);
            a(a2);
        } else if (l()) {
            Intent intent = new Intent();
            intent.setData(Uri.parse(this.l + a2.a()));
            getActivity().setResult(-1, intent);
            getActivity().finish();
        }
    }

    private void a(int i2) {
        if (this.f672b.equals("android.intent.action.GET_CONTENT") || this.o != null) {
            if (this.f672b.equals("android.intent.action.GET_CONTENT") && ((this.f671a.a(i2).d() && this.f673c.equals(d.FILE)) || (this.f671a.a(i2).c() && this.f673c.equals(d.DIR)))) {
                this.e.setItemChecked(i2, false);
            }
            if (b().size() > 0) {
                if (this.m == null) {
                    this.m = getSherlockActivity().startActionMode(this.n);
                }
                int size = b().size();
                this.m.setSubtitle(getResources().getQuantityString(v.l_filebrowser_selected, size, Integer.valueOf(size)));
            } else if (this.m != null) {
                this.m.finish();
            }
        } else {
            this.e.setItemChecked(i2, false);
        }
    }

    /* access modifiers changed from: protected */
    public final List<Integer> b() {
        SparseBooleanArray checkedItemPositions = this.e.getCheckedItemPositions();
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < checkedItemPositions.size(); i2++) {
            int keyAt = checkedItemPositions.keyAt(i2);
            if (checkedItemPositions.get(keyAt)) {
                arrayList.add(Integer.valueOf(keyAt));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(e eVar) {
        getLoaderManager().restartLoader(986198516, b(eVar), this);
    }

    private boolean l() {
        return this.f672b.equals("android.intent.action.GET_CONTENT") && (this.f673c.equals(d.FILE) || this.f673c.equals(d.BOTH));
    }

    private boolean m() {
        return this.f672b.equals("android.intent.action.GET_CONTENT") && (this.f673c.equals(d.DIR) || this.f673c.equals(d.BOTH));
    }

    public boolean c() {
        if (e()) {
            return true;
        }
        getActivity().setResult(0);
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        if (this.f.size() <= 0) {
            return false;
        }
        a(this.f.removeLast());
        return true;
    }

    /* access modifiers changed from: protected */
    public void c(e eVar) {
    }
}
