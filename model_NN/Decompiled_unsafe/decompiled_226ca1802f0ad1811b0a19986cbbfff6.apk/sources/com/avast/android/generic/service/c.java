package com.avast.android.generic.service;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

/* compiled from: WakefulIntentService */
class c extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WakefulIntentService f965a;

    c(WakefulIntentService wakefulIntentService) {
        this.f965a = wakefulIntentService;
    }

    public void handleMessage(Message message) {
        Bundle bundle = (Bundle) message.obj;
        Toast.makeText(this.f965a.getApplicationContext(), bundle.getInt("res"), bundle.getInt("dur")).show();
    }
}
