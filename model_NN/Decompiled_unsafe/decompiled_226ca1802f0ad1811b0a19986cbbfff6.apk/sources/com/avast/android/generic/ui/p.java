package com.avast.android.generic.ui;

import android.os.Message;
import android.view.View;
import com.avast.android.generic.aa;
import com.avast.android.generic.r;
import com.avast.android.generic.util.t;
import com.avast.android.generic.util.y;

/* compiled from: CustomNumberDialog */
class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f1054a;

    p(o oVar) {
        this.f1054a = oVar;
    }

    public void onClick(View view) {
        if (CustomNumberDialog.a(this.f1054a.f1053b.f991c.getText().toString(), this.f1054a.f1053b.e.isChecked())) {
            Message obtain = Message.obtain();
            obtain.what = r.message_filter_custom_number_entered;
            obtain.arg1 = this.f1054a.f1053b.e.isChecked() ? 1 : 0;
            obtain.obj = this.f1054a.f1053b.f991c.getText().toString();
            ((y) aa.a(this.f1054a.f1053b.getActivity(), y.class)).a(obtain);
            if (this.f1054a.f1053b.isAdded()) {
                try {
                    this.f1054a.f1053b.dismiss();
                } catch (Exception e) {
                    t.b("Error dismissing custom number dialog.", e);
                }
            }
        }
    }
}
