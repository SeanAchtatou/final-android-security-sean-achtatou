package com.avast.android.generic.app.account;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.avast.android.generic.util.m;
import com.avast.android.generic.util.n;
import com.avast.android.generic.util.t;
import com.avast.android.generic.x;

/* compiled from: ConnectAccountHelper */
abstract class aa {

    /* renamed from: a  reason: collision with root package name */
    private BroadcastReceiver f372a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f373b = false;
    final /* synthetic */ r e;

    public abstract void a(String str);

    public abstract void b(String str);

    public aa(r rVar) {
        this.e = rVar;
        this.f372a = new ab(this, rVar);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        try {
            if (Build.VERSION.SDK_INT >= 8) {
                a();
                n.a(this.e.f449a);
                new Handler(Looper.getMainLooper()).postDelayed(new ac(this), (long) i);
                return;
            }
            d(this.e.f449a.getString(x.msg_avast_account_c2dm_needs_api_level_8));
        } catch (m e2) {
            t.a("ConnectAccountHelper", "C2DM registration error", e2);
            b();
            d(this.e.f449a.getString(x.msg_avast_account_c2dm_error));
        }
    }

    private void a() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.google.android.c2dm.intent.REGISTRATION");
        intentFilter.addCategory(this.e.f449a.getPackageName());
        this.e.f449a.registerReceiver(this.f372a, intentFilter, "com.google.android.c2dm.permission.SEND", null);
        this.f373b = true;
    }

    private void b() {
        if (this.f373b) {
            try {
                this.e.f449a.unregisterReceiver(this.f372a);
            } catch (Exception e2) {
            }
            this.f373b = false;
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        b();
        f(str);
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        b();
        e(str);
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.f373b) {
            t.b("ConnectAccountHelper", "C2DM registration error (timeout)");
            b();
            e(this.e.f449a.getString(x.msg_avast_account_c2dm_error));
        }
    }

    private void e(String str) {
        new Handler(Looper.getMainLooper()).post(new ad(this, str));
    }

    private void f(String str) {
        new Handler(Looper.getMainLooper()).post(new ae(this, str));
    }
}
