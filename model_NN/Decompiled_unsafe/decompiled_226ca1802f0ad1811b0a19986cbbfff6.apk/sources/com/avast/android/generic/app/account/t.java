package com.avast.android.generic.app.account;

import com.avast.android.generic.internet.c.e;
import com.avast.android.generic.util.b;

/* compiled from: ConnectAccountHelper */
class t extends aa {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f455a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f456b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ String f457c;
    final /* synthetic */ r d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    t(r rVar, String str, String str2, String str3) {
        super(rVar);
        this.d = rVar;
        this.f455a = str;
        this.f456b = str2;
        this.f457c = str3;
    }

    public void a(String str) {
        v unused = this.d.f450b = new v(this.d, e.EMAIL, this.f455a, this.f456b, this.f457c, str, null, 0);
        b.a(this.d.f450b, new Void[0]);
    }

    public void b(String str) {
        this.d.a(str);
    }
}
