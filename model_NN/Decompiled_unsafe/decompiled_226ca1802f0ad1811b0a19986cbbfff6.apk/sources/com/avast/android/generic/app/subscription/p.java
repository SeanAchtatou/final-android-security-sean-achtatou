package com.avast.android.generic.app.subscription;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import com.avast.android.generic.a;
import com.avast.android.generic.internet.b;
import com.avast.android.generic.licensing.PurchaseConfirmationService;
import com.avast.android.generic.licensing.database.d;
import com.avast.android.generic.util.w;
import com.avast.android.generic.util.z;
import com.avast.android.generic.x;
import com.avast.c.a.a.ad;

/* compiled from: SubscriptionFragment */
class p extends AsyncTask<Void, Void, Boolean> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f567a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f568b;

    /* renamed from: c  reason: collision with root package name */
    private ad f569c = null;
    private Context d;
    private Exception e = null;

    p(SubscriptionFragment subscriptionFragment, String str) {
        this.f568b = subscriptionFragment;
        this.f567a = str;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.f568b.isAdded()) {
            this.d = this.f568b.getActivity();
            this.f568b.q();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        if (!this.f568b.isAdded()) {
            return false;
        }
        try {
            String a2 = new d(this.d, this.f568b.e()).a(this.f567a);
            if (a2 == null) {
                return false;
            }
            this.f569c = b.a(this.f568b.getActivity(), this.f568b.f516c, PurchaseConfirmationService.a(this.f568b.getActivity(), this.f568b.f516c, this.f568b.getActivity().getPackageName(), a2, this.f567a, this.f568b.f516c.W()));
            if (this.f569c == null) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            z.a("AvastGenericLic", "Could not get manage subscription response", e2);
            this.e = e2;
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        String string;
        if (this.f568b.isAdded()) {
            this.f568b.r();
            if (!bool.booleanValue()) {
                Context context = this.d;
                if (this.e != null) {
                    string = this.f568b.getString(x.l_cannot_query_manage_subscription_error, w.a(this.d, this.e));
                } else {
                    string = this.f568b.getString(x.l_cannot_query_manage_subscription_unknown);
                }
                a.a(context, string);
                return;
            }
            if (this.f569c.d()) {
                this.f568b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + this.f569c.e())));
            } else if (this.f569c.b()) {
                this.f568b.a(this.f569c.c());
            } else if (this.f569c.f()) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse(this.f569c.g()));
                this.f568b.startActivity(intent);
            }
            this.f568b.a("subscriptionFragment", "manage", (String) null, 0);
        }
    }
}
