package com.avast.android.generic.util.a;

import android.content.Context;
import com.avast.android.generic.util.ad;

/* compiled from: NotEnoughFreeSpaceException */
public class a extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private long f1165a;

    /* renamed from: b  reason: collision with root package name */
    private long f1166b;

    public a(long j, long j2) {
        this.f1165a = j;
        this.f1166b = j2;
    }

    public String a(Context context) {
        return a(context, this.f1165a);
    }

    public String b(Context context) {
        return a(context, this.f1166b);
    }

    private String a(Context context, long j) {
        return new ad(context).a(j);
    }
}
