package com.avast.android.generic.app.wizard;

import android.os.SystemClock;
import android.view.View;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.util.e;

/* compiled from: EulaFragment */
class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ EulaFragment f590a;

    c(EulaFragment eulaFragment) {
        this.f590a = eulaFragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void */
    public void onClick(View view) {
        ab abVar = (ab) aa.a(this.f590a.getActivity(), ab.class);
        this.f590a.f.a(e.AGREE);
        this.f590a.a("ms-Wizard", "accept", "yes", (long) ((int) (SystemClock.uptimeMillis() - this.f590a.d)));
        if (this.f590a.e) {
            this.f590a.a("ms-Wizard", "communityIQ allowed during accept", abVar.o() ? "yes" : "no", 0);
        }
        this.f590a.a_();
        abVar.a("eulaDone2", true);
        abVar.b();
        this.f590a.b_();
    }
}
