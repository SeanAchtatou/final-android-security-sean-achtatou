package com.avast.android.generic.app.subscription;

import android.net.Uri;
import android.view.View;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.licensing.database.b;
import com.avast.android.generic.licensing.database.d;

/* compiled from: SubscriptionFragment */
class z implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f583a;

    z(SubscriptionFragment subscriptionFragment) {
        this.f583a = subscriptionFragment;
    }

    public void onClick(View view) {
        d.a(this.f583a.getActivity(), b.a(Uri.parse("content://" + (this.f583a.r.getPackageName() + ".database.billing"))));
        ((ab) aa.a(this.f583a.getActivity(), ab.class)).a(false, -2, false, "");
        this.f583a.d();
    }
}
