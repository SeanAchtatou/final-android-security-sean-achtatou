package com.avast.android.generic.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: AsyncTaskUtils */
final class c implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f1214a = new AtomicInteger(1);

    c() {
    }

    public Thread newThread(Runnable runnable) {
        int andIncrement = this.f1214a.getAndIncrement();
        if (andIncrement < 200) {
            t.c("");
        } else {
            z.a("avast! Mobile Security", "");
        }
        return new Thread(runnable, "avast! AsyncTask #" + andIncrement);
    }
}
