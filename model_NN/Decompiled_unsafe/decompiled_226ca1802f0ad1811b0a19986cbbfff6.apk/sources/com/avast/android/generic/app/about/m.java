package com.avast.android.generic.app.about;

import android.os.AsyncTask;
import com.avast.android.generic.util.t;

/* compiled from: FeedbackFragment */
class m extends AsyncTask<Void, Void, Boolean> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FeedbackFragment f351a;

    private m(FeedbackFragment feedbackFragment) {
        this.f351a = feedbackFragment;
    }

    /* synthetic */ m(FeedbackFragment feedbackFragment, f fVar) {
        this(feedbackFragment);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        t.b("FeedbackFragment", "Preparing zipped logs.");
        byte[] unused = this.f351a.q = r.a();
        byte[] unused2 = this.f351a.r = r.b();
        return Boolean.valueOf((this.f351a.q == null || this.f351a.r == null) ? false : true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        t.b("FeedbackFragment", "Done preparing logs, result: " + bool);
        if (bool.booleanValue()) {
            this.f351a.d();
        }
    }
}
