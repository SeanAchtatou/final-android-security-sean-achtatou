package com.avast.android.generic.app.wizard;

import android.view.View;
import com.avast.android.generic.util.g;
import com.avast.android.generic.x;

/* compiled from: EulaFragment */
class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ com.avast.android.generic.ui.b.a f587a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ EulaFragment f588b;

    a(EulaFragment eulaFragment, com.avast.android.generic.ui.b.a aVar) {
        this.f588b = eulaFragment;
        this.f587a = aVar;
    }

    public void onClick(View view) {
        if (this.f588b.f586c == x.l_eula_show) {
            int unused = this.f588b.f586c = x.l_eula_hide;
            this.f587a.a(false);
            this.f588b.f584a.startAnimation(this.f587a);
            this.f588b.f584a.invalidate();
            this.f588b.a("ms-Wizard", "eulaText", "show", 0);
            this.f588b.f.b(g.POST_INSTALL);
        } else {
            int unused2 = this.f588b.f586c = x.l_eula_show;
            this.f587a.a(true);
            this.f588b.f584a.invalidate();
            this.f588b.f584a.startAnimation(this.f587a);
            this.f588b.a("ms-Wizard", "eulaText", "hide", 0);
        }
        this.f588b.f585b.setText(this.f588b.f586c);
    }
}
