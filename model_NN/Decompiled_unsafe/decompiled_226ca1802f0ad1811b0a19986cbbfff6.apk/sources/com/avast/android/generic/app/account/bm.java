package com.avast.android.generic.app.account;

import android.support.v4.app.Fragment;

/* compiled from: SignInPickerFragment */
class bm extends be {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SignInPickerFragment f424a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bm(SignInPickerFragment signInPickerFragment, Fragment fragment) {
        super(fragment);
        this.f424a = signInPickerFragment;
    }

    /* renamed from: a */
    public void b(bh bhVar) {
    }

    public void a(bg bgVar) {
        if (!this.f424a.isAdded()) {
            return;
        }
        if (!this.f424a.f370c) {
            this.f424a.d().a(bgVar, (String) null);
            return;
        }
        this.f424a.getArguments().putBoolean("wizard", this.f424a.f369b);
        this.f424a.getArguments().putBoolean("queryPhoneNumber", this.f424a.f370c);
        this.f424a.getArguments().putString("facebookToken", bgVar.a());
        this.f424a.getArguments().putLong("facebookTokenExpirationTime", bgVar.b());
        this.f424a.getArguments().putString("googlePlusToken", null);
        this.f424a.a(new ConnectAccountFragment());
    }
}
