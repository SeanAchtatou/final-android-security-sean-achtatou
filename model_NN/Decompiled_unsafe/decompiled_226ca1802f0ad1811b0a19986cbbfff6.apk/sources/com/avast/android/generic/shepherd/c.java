package com.avast.android.generic.shepherd;

import com.avast.e.a.bb;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;

/* compiled from: Shepherd */
public enum c {
    MOBILE_SECURITY(bb.AMS),
    ANTI_THEFT(bb.AAT),
    BACKUP(bb.ABCK),
    SECURELINE(bb.ASL);
    
    private static final HashMap<bb, c> e = new HashMap<>();
    private final bb f;

    static {
        Iterator it = EnumSet.allOf(c.class).iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            e.put(cVar.a(), cVar);
        }
    }

    private c(bb bbVar) {
        this.f = bbVar;
    }

    public final bb a() {
        return this.f;
    }
}
