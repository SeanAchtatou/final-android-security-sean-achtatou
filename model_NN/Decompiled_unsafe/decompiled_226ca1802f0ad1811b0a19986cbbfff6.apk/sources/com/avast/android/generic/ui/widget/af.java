package com.avast.android.generic.ui.widget;

import android.app.TimePickerDialog;
import android.view.View;

/* compiled from: TimeButtonRow */
class af implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f1136a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ TimeButtonRow f1137b;

    af(TimeButtonRow timeButtonRow, boolean z) {
        this.f1137b = timeButtonRow;
        this.f1136a = z;
    }

    public void onClick(View view) {
        new TimePickerDialog(this.f1137b.getContext(), new ag(this), this.f1137b.f1127b / 60, this.f1137b.f1127b % 60, this.f1136a).show();
    }
}
