package com.avast.android.generic.shepherd;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.util.t;

public class ShepherdConnectivityChangeReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (((ab) aa.a(context, ab.class)).X()) {
            String action = intent.getAction();
            boolean equals = action.equals("intent.action.SHEPHERD_CONNECTIVITY_FAILSAFE");
            boolean z = action.equals("android.net.conn.CONNECTIVITY_CHANGE") && !intent.getBooleanExtra("noConnectivity", false);
            if (z || equals) {
                if (z) {
                    a(context);
                }
                if (System.currentTimeMillis() > ((ab) aa.a(context, ab.class)).Z() + 3600000) {
                    ShepherdDownloadService.a(context, false);
                }
            }
        }
    }

    private static void a(Context context) {
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, new Intent("intent.action.SHEPHERD_CONNECTIVITY_FAILSAFE"), 268435456);
        ((AlarmManager) context.getSystemService("alarm")).set(1, System.currentTimeMillis() + 180000, broadcast);
        t.c("ShepherdConnectivityChangeReceiver failsafe scheduled");
    }
}
