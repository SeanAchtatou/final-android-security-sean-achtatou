package com.avast.b.a.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToWeb */
public final class au extends GeneratedMessageLite.Builder<at, au> implements av {

    /* renamed from: a  reason: collision with root package name */
    private int f1361a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1362b = "";

    private au() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static au g() {
        return new au();
    }

    /* renamed from: a */
    public au clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public at getDefaultInstanceForType() {
        return at.a();
    }

    /* renamed from: c */
    public at build() {
        at d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public at d() {
        int i = 1;
        at atVar = new at(this);
        if ((this.f1361a & 1) != 1) {
            i = 0;
        }
        Object unused = atVar.f1360c = this.f1362b;
        int unused2 = atVar.f1359b = i;
        return atVar;
    }

    /* renamed from: a */
    public au mergeFrom(at atVar) {
        if (atVar != at.a() && atVar.b()) {
            a(atVar.c());
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public au mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1361a |= 1;
                    this.f1362b = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public au a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1361a |= 1;
        this.f1362b = str;
        return this;
    }
}
