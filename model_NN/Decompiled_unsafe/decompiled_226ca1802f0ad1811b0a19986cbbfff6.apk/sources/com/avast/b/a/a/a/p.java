package com.avast.b.a.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.actionbarsherlock.view.Menu;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ATProtoGenerics */
public final class p extends GeneratedMessageLite.Builder<o, p> implements q {
    private Object A = "";
    private long B;
    private long C;
    private int D;
    private Object E = "";

    /* renamed from: a  reason: collision with root package name */
    private int f1305a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1306b = "";

    /* renamed from: c  reason: collision with root package name */
    private Object f1307c = "";
    private Object d = "";
    private Object e = "";
    private Object f = "";
    private Object g = "";
    private long h;
    private long i;
    private long j;
    private ab k = ab.INTERNAL;
    private long l;
    private u m = u.IMAGE;
    private Object n = "";
    private Object o = "";
    private Object p = "";
    private Object q = "";
    private long r;
    private int s;
    private ByteString t = ByteString.EMPTY;
    private Object u = "";
    private Object v = "";
    private int w;
    private int x;
    private Object y = "";
    private Object z = "";

    private p() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static p g() {
        return new p();
    }

    /* renamed from: a */
    public p clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public o getDefaultInstanceForType() {
        return o.a();
    }

    /* renamed from: c */
    public o build() {
        o d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public o d() {
        o oVar = new o(this);
        int i2 = this.f1305a;
        int i3 = 0;
        if ((i2 & 1) == 1) {
            i3 = 1;
        }
        Object unused = oVar.f1304c = this.f1306b;
        if ((i2 & 2) == 2) {
            i3 |= 2;
        }
        Object unused2 = oVar.d = this.f1307c;
        if ((i2 & 4) == 4) {
            i3 |= 4;
        }
        Object unused3 = oVar.e = this.d;
        if ((i2 & 8) == 8) {
            i3 |= 8;
        }
        Object unused4 = oVar.f = this.e;
        if ((i2 & 16) == 16) {
            i3 |= 16;
        }
        Object unused5 = oVar.g = this.f;
        if ((i2 & 32) == 32) {
            i3 |= 32;
        }
        Object unused6 = oVar.h = this.g;
        if ((i2 & 64) == 64) {
            i3 |= 64;
        }
        long unused7 = oVar.i = this.h;
        if ((i2 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i3 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        long unused8 = oVar.j = this.i;
        if ((i2 & 256) == 256) {
            i3 |= 256;
        }
        long unused9 = oVar.k = this.j;
        if ((i2 & 512) == 512) {
            i3 |= 512;
        }
        ab unused10 = oVar.l = this.k;
        if ((i2 & 1024) == 1024) {
            i3 |= 1024;
        }
        long unused11 = oVar.m = this.l;
        if ((i2 & 2048) == 2048) {
            i3 |= 2048;
        }
        u unused12 = oVar.n = this.m;
        if ((i2 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i3 |= FragmentTransaction.TRANSIT_ENTER_MASK;
        }
        Object unused13 = oVar.o = this.n;
        if ((i2 & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            i3 |= FragmentTransaction.TRANSIT_EXIT_MASK;
        }
        Object unused14 = oVar.p = this.o;
        if ((i2 & 16384) == 16384) {
            i3 |= 16384;
        }
        Object unused15 = oVar.q = this.p;
        if ((i2 & 32768) == 32768) {
            i3 |= 32768;
        }
        Object unused16 = oVar.r = this.q;
        if ((i2 & Menu.CATEGORY_CONTAINER) == 65536) {
            i3 |= Menu.CATEGORY_CONTAINER;
        }
        long unused17 = oVar.s = this.r;
        if ((i2 & Menu.CATEGORY_SYSTEM) == 131072) {
            i3 |= Menu.CATEGORY_SYSTEM;
        }
        int unused18 = oVar.t = this.s;
        if ((i2 & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            i3 |= Menu.CATEGORY_ALTERNATIVE;
        }
        ByteString unused19 = oVar.u = this.t;
        if ((i2 & 524288) == 524288) {
            i3 |= 524288;
        }
        Object unused20 = oVar.v = this.u;
        if ((1048576 & i2) == 1048576) {
            i3 |= 1048576;
        }
        Object unused21 = oVar.w = this.v;
        if ((2097152 & i2) == 2097152) {
            i3 |= 2097152;
        }
        int unused22 = oVar.x = this.w;
        if ((4194304 & i2) == 4194304) {
            i3 |= 4194304;
        }
        int unused23 = oVar.y = this.x;
        if ((8388608 & i2) == 8388608) {
            i3 |= 8388608;
        }
        Object unused24 = oVar.z = this.y;
        if ((16777216 & i2) == 16777216) {
            i3 |= 16777216;
        }
        Object unused25 = oVar.A = this.z;
        if ((33554432 & i2) == 33554432) {
            i3 |= 33554432;
        }
        Object unused26 = oVar.B = this.A;
        if ((67108864 & i2) == 67108864) {
            i3 |= 67108864;
        }
        long unused27 = oVar.C = this.B;
        if ((134217728 & i2) == 134217728) {
            i3 |= 134217728;
        }
        long unused28 = oVar.D = this.C;
        if ((268435456 & i2) == 268435456) {
            i3 |= 268435456;
        }
        int unused29 = oVar.E = this.D;
        if ((i2 & 536870912) == 536870912) {
            i3 |= 536870912;
        }
        Object unused30 = oVar.F = this.E;
        int unused31 = oVar.f1303b = i3;
        return oVar;
    }

    /* renamed from: a */
    public p mergeFrom(o oVar) {
        if (oVar != o.a()) {
            if (oVar.b()) {
                a(oVar.c());
            }
            if (oVar.d()) {
                b(oVar.e());
            }
            if (oVar.f()) {
                c(oVar.g());
            }
            if (oVar.h()) {
                d(oVar.i());
            }
            if (oVar.j()) {
                e(oVar.k());
            }
            if (oVar.l()) {
                f(oVar.m());
            }
            if (oVar.n()) {
                a(oVar.o());
            }
            if (oVar.p()) {
                b(oVar.q());
            }
            if (oVar.r()) {
                c(oVar.s());
            }
            if (oVar.t()) {
                a(oVar.u());
            }
            if (oVar.v()) {
                d(oVar.w());
            }
            if (oVar.x()) {
                a(oVar.y());
            }
            if (oVar.z()) {
                g(oVar.A());
            }
            if (oVar.B()) {
                h(oVar.C());
            }
            if (oVar.D()) {
                i(oVar.E());
            }
            if (oVar.F()) {
                j(oVar.G());
            }
            if (oVar.H()) {
                e(oVar.I());
            }
            if (oVar.J()) {
                a(oVar.K());
            }
            if (oVar.L()) {
                a(oVar.M());
            }
            if (oVar.N()) {
                k(oVar.O());
            }
            if (oVar.P()) {
                l(oVar.Q());
            }
            if (oVar.R()) {
                b(oVar.S());
            }
            if (oVar.T()) {
                c(oVar.U());
            }
            if (oVar.V()) {
                m(oVar.W());
            }
            if (oVar.X()) {
                n(oVar.Y());
            }
            if (oVar.Z()) {
                o(oVar.aa());
            }
            if (oVar.ab()) {
                f(oVar.ac());
            }
            if (oVar.ad()) {
                g(oVar.ae());
            }
            if (oVar.af()) {
                d(oVar.ag());
            }
            if (oVar.ah()) {
                p(oVar.ai());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public p mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1305a |= 1;
                    this.f1306b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1305a |= 2;
                    this.f1307c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1305a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f1305a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f1305a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                case 50:
                    this.f1305a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    this.f1305a |= 64;
                    this.h = codedInputStream.readUInt64();
                    break;
                case R.styleable.SherlockTheme_activityChooserViewStyle:
                    this.f1305a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readUInt64();
                    break;
                case 72:
                    this.f1305a |= 256;
                    this.j = codedInputStream.readUInt64();
                    break;
                case 80:
                    ab a2 = ab.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1305a |= 512;
                        this.k = a2;
                        break;
                    }
                case 88:
                    this.f1305a |= 1024;
                    this.l = codedInputStream.readUInt64();
                    break;
                case 96:
                    u a3 = u.a(codedInputStream.readEnum());
                    if (a3 == null) {
                        break;
                    } else {
                        this.f1305a |= 2048;
                        this.m = a3;
                        break;
                    }
                case 106:
                    this.f1305a |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.n = codedInputStream.readBytes();
                    break;
                case 114:
                    this.f1305a |= FragmentTransaction.TRANSIT_EXIT_MASK;
                    this.o = codedInputStream.readBytes();
                    break;
                case 122:
                    this.f1305a |= 16384;
                    this.p = codedInputStream.readBytes();
                    break;
                case 130:
                    this.f1305a |= 32768;
                    this.q = codedInputStream.readBytes();
                    break;
                case 136:
                    this.f1305a |= Menu.CATEGORY_CONTAINER;
                    this.r = codedInputStream.readUInt64();
                    break;
                case 144:
                    this.f1305a |= Menu.CATEGORY_SYSTEM;
                    this.s = codedInputStream.readInt32();
                    break;
                case 154:
                    this.f1305a |= Menu.CATEGORY_ALTERNATIVE;
                    this.t = codedInputStream.readBytes();
                    break;
                case 162:
                    this.f1305a |= 524288;
                    this.u = codedInputStream.readBytes();
                    break;
                case 170:
                    this.f1305a |= 1048576;
                    this.v = codedInputStream.readBytes();
                    break;
                case 176:
                    this.f1305a |= 2097152;
                    this.w = codedInputStream.readInt32();
                    break;
                case 184:
                    this.f1305a |= 4194304;
                    this.x = codedInputStream.readInt32();
                    break;
                case 194:
                    this.f1305a |= 8388608;
                    this.y = codedInputStream.readBytes();
                    break;
                case 202:
                    this.f1305a |= 16777216;
                    this.z = codedInputStream.readBytes();
                    break;
                case 210:
                    this.f1305a |= 33554432;
                    this.A = codedInputStream.readBytes();
                    break;
                case 216:
                    this.f1305a |= 67108864;
                    this.B = codedInputStream.readInt64();
                    break;
                case 224:
                    this.f1305a |= 134217728;
                    this.C = codedInputStream.readInt64();
                    break;
                case 232:
                    this.f1305a |= 268435456;
                    this.D = codedInputStream.readInt32();
                    break;
                case 242:
                    this.f1305a |= 536870912;
                    this.E = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public p a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 1;
        this.f1306b = str;
        return this;
    }

    public p b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 2;
        this.f1307c = str;
        return this;
    }

    public p c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 4;
        this.d = str;
        return this;
    }

    public p d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 8;
        this.e = str;
        return this;
    }

    public p e(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 16;
        this.f = str;
        return this;
    }

    public p f(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 32;
        this.g = str;
        return this;
    }

    public p a(long j2) {
        this.f1305a |= 64;
        this.h = j2;
        return this;
    }

    public p b(long j2) {
        this.f1305a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = j2;
        return this;
    }

    public p c(long j2) {
        this.f1305a |= 256;
        this.j = j2;
        return this;
    }

    public p a(ab abVar) {
        if (abVar == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 512;
        this.k = abVar;
        return this;
    }

    public p d(long j2) {
        this.f1305a |= 1024;
        this.l = j2;
        return this;
    }

    public p a(u uVar) {
        if (uVar == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 2048;
        this.m = uVar;
        return this;
    }

    public p g(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.n = str;
        return this;
    }

    public p h(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= FragmentTransaction.TRANSIT_EXIT_MASK;
        this.o = str;
        return this;
    }

    public p i(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 16384;
        this.p = str;
        return this;
    }

    public p j(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 32768;
        this.q = str;
        return this;
    }

    public p e(long j2) {
        this.f1305a |= Menu.CATEGORY_CONTAINER;
        this.r = j2;
        return this;
    }

    public p a(int i2) {
        this.f1305a |= Menu.CATEGORY_SYSTEM;
        this.s = i2;
        return this;
    }

    public p a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1305a |= Menu.CATEGORY_ALTERNATIVE;
        this.t = byteString;
        return this;
    }

    public p k(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 524288;
        this.u = str;
        return this;
    }

    public p l(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 1048576;
        this.v = str;
        return this;
    }

    public p b(int i2) {
        this.f1305a |= 2097152;
        this.w = i2;
        return this;
    }

    public p c(int i2) {
        this.f1305a |= 4194304;
        this.x = i2;
        return this;
    }

    public p m(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 8388608;
        this.y = str;
        return this;
    }

    public p n(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 16777216;
        this.z = str;
        return this;
    }

    public p o(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 33554432;
        this.A = str;
        return this;
    }

    public p f(long j2) {
        this.f1305a |= 67108864;
        this.B = j2;
        return this;
    }

    public p g(long j2) {
        this.f1305a |= 134217728;
        this.C = j2;
        return this;
    }

    public p d(int i2) {
        this.f1305a |= 268435456;
        this.D = i2;
        return this;
    }

    public p p(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1305a |= 536870912;
        this.E = str;
        return this;
    }
}
