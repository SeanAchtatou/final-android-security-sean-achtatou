package com.avast.b.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: AvastToWeb */
public final class ag extends GeneratedMessageLite implements ai {

    /* renamed from: a  reason: collision with root package name */
    private static final ag f1335a = new ag(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1336b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1337c;
    private byte d;
    private int e;

    private ag(ah ahVar) {
        super(ahVar);
        this.d = -1;
        this.e = -1;
    }

    private ag(boolean z) {
        this.d = -1;
        this.e = -1;
    }

    public static ag a() {
        return f1335a;
    }

    public boolean b() {
        return (this.f1336b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1337c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1337c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString g() {
        Object obj = this.f1337c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1337c = copyFromUtf8;
        return copyFromUtf8;
    }

    private void h() {
        this.f1337c = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.d;
        if (b2 == -1) {
            this.d = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1336b & 1) == 1) {
            codedOutputStream.writeBytes(1, g());
        }
    }

    public int getSerializedSize() {
        int i = this.e;
        if (i == -1) {
            i = 0;
            if ((this.f1336b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, g());
            }
            this.e = i;
        }
        return i;
    }

    public static ah d() {
        return ah.g();
    }

    /* renamed from: e */
    public ah newBuilderForType() {
        return d();
    }

    public static ah a(ag agVar) {
        return d().mergeFrom(agVar);
    }

    /* renamed from: f */
    public ah toBuilder() {
        return a(this);
    }

    static {
        f1335a.h();
    }
}
