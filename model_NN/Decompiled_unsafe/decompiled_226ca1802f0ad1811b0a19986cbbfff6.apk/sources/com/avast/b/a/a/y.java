package com.avast.b.a.a;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToWeb */
public final class y extends GeneratedMessageLite implements ac {

    /* renamed from: a  reason: collision with root package name */
    private static final y f1402a = new y(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1403b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public aa f1404c;
    private byte d;
    private int e;

    private y(z zVar) {
        super(zVar);
        this.d = -1;
        this.e = -1;
    }

    private y(boolean z) {
        this.d = -1;
        this.e = -1;
    }

    public static y a() {
        return f1402a;
    }

    public boolean b() {
        return (this.f1403b & 1) == 1;
    }

    public aa c() {
        return this.f1404c;
    }

    private void g() {
        this.f1404c = aa.C2DM_SUCCESS;
    }

    public final boolean isInitialized() {
        byte b2 = this.d;
        if (b2 != -1) {
            if (b2 == 1) {
                return true;
            }
            return false;
        } else if (!b()) {
            this.d = 0;
            return false;
        } else {
            this.d = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1403b & 1) == 1) {
            codedOutputStream.writeEnum(1, this.f1404c.getNumber());
        }
    }

    public int getSerializedSize() {
        int i = this.e;
        if (i == -1) {
            i = 0;
            if ((this.f1403b & 1) == 1) {
                i = 0 + CodedOutputStream.computeEnumSize(1, this.f1404c.getNumber());
            }
            this.e = i;
        }
        return i;
    }

    public static z d() {
        return z.h();
    }

    /* renamed from: e */
    public z newBuilderForType() {
        return d();
    }

    public static z a(y yVar) {
        return d().mergeFrom(yVar);
    }

    /* renamed from: f */
    public z toBuilder() {
        return a(this);
    }

    static {
        f1402a.g();
    }
}
