package com.avast.b.a.a.a;

import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ATProtoGenerics */
public final class z extends GeneratedMessageLite.Builder<y, z> implements aa {

    /* renamed from: a  reason: collision with root package name */
    private int f1323a;

    /* renamed from: b  reason: collision with root package name */
    private long f1324b;

    /* renamed from: c  reason: collision with root package name */
    private long f1325c;
    private boolean d;
    private Object e = "";
    private Object f = "";
    private Object g = "";
    private long h;
    private Object i = "";

    private z() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static z i() {
        return new z();
    }

    /* renamed from: a */
    public z clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public y getDefaultInstanceForType() {
        return y.a();
    }

    /* renamed from: c */
    public y build() {
        y d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public y d() {
        int i2 = 1;
        y yVar = new y(this);
        int i3 = this.f1323a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        long unused = yVar.f1322c = this.f1324b;
        if ((i3 & 2) == 2) {
            i2 |= 2;
        }
        long unused2 = yVar.d = this.f1325c;
        if ((i3 & 4) == 4) {
            i2 |= 4;
        }
        boolean unused3 = yVar.e = this.d;
        if ((i3 & 8) == 8) {
            i2 |= 8;
        }
        Object unused4 = yVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 16;
        }
        Object unused5 = yVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 32;
        }
        Object unused6 = yVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 64;
        }
        long unused7 = yVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        Object unused8 = yVar.j = this.i;
        int unused9 = yVar.f1321b = i2;
        return yVar;
    }

    /* renamed from: a */
    public z mergeFrom(y yVar) {
        if (yVar != y.a()) {
            if (yVar.b()) {
                a(yVar.c());
            }
            if (yVar.d()) {
                b(yVar.e());
            }
            if (yVar.f()) {
                a(yVar.g());
            }
            if (yVar.h()) {
                a(yVar.i());
            }
            if (yVar.j()) {
                b(yVar.k());
            }
            if (yVar.l()) {
                c(yVar.m());
            }
            if (yVar.n()) {
                c(yVar.o());
            }
            if (yVar.p()) {
                d(yVar.q());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (e() && f()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public z mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    this.f1323a |= 1;
                    this.f1324b = codedInputStream.readInt64();
                    break;
                case 16:
                    this.f1323a |= 2;
                    this.f1325c = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    this.f1323a |= 4;
                    this.d = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f1323a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f1323a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                case 50:
                    this.f1323a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    this.f1323a |= 64;
                    this.h = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f1323a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1323a & 1) == 1;
    }

    public z a(long j) {
        this.f1323a |= 1;
        this.f1324b = j;
        return this;
    }

    public boolean f() {
        return (this.f1323a & 2) == 2;
    }

    public z b(long j) {
        this.f1323a |= 2;
        this.f1325c = j;
        return this;
    }

    public z a(boolean z) {
        this.f1323a |= 4;
        this.d = z;
        return this;
    }

    public z a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1323a |= 8;
        this.e = str;
        return this;
    }

    public z b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1323a |= 16;
        this.f = str;
        return this;
    }

    public z c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1323a |= 32;
        this.g = str;
        return this;
    }

    public z c(long j) {
        this.f1323a |= 64;
        this.h = j;
        return this;
    }

    public z d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1323a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = str;
        return this;
    }
}
