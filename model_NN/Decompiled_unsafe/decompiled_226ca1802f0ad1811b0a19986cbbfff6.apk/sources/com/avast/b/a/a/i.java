package com.avast.b.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.LazyStringArrayList;
import com.google.protobuf.LazyStringList;
import com.google.protobuf.UnmodifiableLazyStringList;

/* compiled from: AvastToDevice */
public final class i extends GeneratedMessageLite.Builder<h, i> implements j {

    /* renamed from: a  reason: collision with root package name */
    private int f1375a;

    /* renamed from: b  reason: collision with root package name */
    private int f1376b;

    /* renamed from: c  reason: collision with root package name */
    private LazyStringList f1377c = LazyStringArrayList.EMPTY;
    private boolean d;
    private boolean e;
    private boolean f;
    private boolean g;
    private boolean h;
    private boolean i;
    private boolean j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    private int o;
    private boolean p;
    private Object q = "";

    private i() {
        g();
    }

    private void g() {
    }

    /* access modifiers changed from: private */
    public static i h() {
        return new i();
    }

    /* renamed from: a */
    public i clone() {
        return h().mergeFrom(d());
    }

    /* renamed from: b */
    public h getDefaultInstanceForType() {
        return h.a();
    }

    /* renamed from: c */
    public h build() {
        h d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    /* access modifiers changed from: private */
    public h i() {
        h d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2).asInvalidProtocolBufferException();
    }

    public h d() {
        int i2 = 1;
        h hVar = new h(this);
        int i3 = this.f1375a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        int unused = hVar.f1374c = this.f1376b;
        if ((this.f1375a & 2) == 2) {
            this.f1377c = new UnmodifiableLazyStringList(this.f1377c);
            this.f1375a &= -3;
        }
        LazyStringList unused2 = hVar.d = this.f1377c;
        if ((i3 & 4) == 4) {
            i2 |= 2;
        }
        boolean unused3 = hVar.e = this.d;
        if ((i3 & 8) == 8) {
            i2 |= 4;
        }
        boolean unused4 = hVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 8;
        }
        boolean unused5 = hVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 16;
        }
        boolean unused6 = hVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 32;
        }
        boolean unused7 = hVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= 64;
        }
        boolean unused8 = hVar.j = this.i;
        if ((i3 & 256) == 256) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        boolean unused9 = hVar.k = this.j;
        if ((i3 & 512) == 512) {
            i2 |= 256;
        }
        boolean unused10 = hVar.l = this.k;
        if ((i3 & 1024) == 1024) {
            i2 |= 512;
        }
        boolean unused11 = hVar.m = this.l;
        if ((i3 & 2048) == 2048) {
            i2 |= 1024;
        }
        boolean unused12 = hVar.n = this.m;
        if ((i3 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i2 |= 2048;
        }
        boolean unused13 = hVar.o = this.n;
        if ((i3 & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            i2 |= FragmentTransaction.TRANSIT_ENTER_MASK;
        }
        int unused14 = hVar.p = this.o;
        if ((i3 & 16384) == 16384) {
            i2 |= FragmentTransaction.TRANSIT_EXIT_MASK;
        }
        boolean unused15 = hVar.q = this.p;
        if ((i3 & 32768) == 32768) {
            i2 |= 16384;
        }
        Object unused16 = hVar.r = this.q;
        int unused17 = hVar.f1373b = i2;
        return hVar;
    }

    /* renamed from: a */
    public i mergeFrom(h hVar) {
        if (hVar != h.a()) {
            if (hVar.b()) {
                a(hVar.c());
            }
            if (!hVar.d.isEmpty()) {
                if (this.f1377c.isEmpty()) {
                    this.f1377c = hVar.d;
                    this.f1375a &= -3;
                } else {
                    j();
                    this.f1377c.addAll(hVar.d);
                }
            }
            if (hVar.f()) {
                a(hVar.g());
            }
            if (hVar.h()) {
                b(hVar.i());
            }
            if (hVar.j()) {
                c(hVar.k());
            }
            if (hVar.l()) {
                d(hVar.m());
            }
            if (hVar.n()) {
                e(hVar.o());
            }
            if (hVar.p()) {
                f(hVar.q());
            }
            if (hVar.r()) {
                g(hVar.s());
            }
            if (hVar.t()) {
                h(hVar.u());
            }
            if (hVar.v()) {
                i(hVar.w());
            }
            if (hVar.x()) {
                j(hVar.y());
            }
            if (hVar.z()) {
                k(hVar.A());
            }
            if (hVar.B()) {
                b(hVar.C());
            }
            if (hVar.D()) {
                l(hVar.E());
            }
            if (hVar.F()) {
                a(hVar.G());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (!e()) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public i mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    this.f1375a |= 1;
                    this.f1376b = codedInputStream.readInt32();
                    break;
                case 18:
                    j();
                    this.f1377c.add(codedInputStream.readBytes());
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    this.f1375a |= 4;
                    this.d = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    this.f1375a |= 8;
                    this.e = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f1375a |= 16;
                    this.f = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                    this.f1375a |= 32;
                    this.g = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    this.f1375a |= 64;
                    this.h = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_activityChooserViewStyle:
                    this.f1375a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBool();
                    break;
                case 72:
                    this.f1375a |= 256;
                    this.j = codedInputStream.readBool();
                    break;
                case 80:
                    this.f1375a |= 512;
                    this.k = codedInputStream.readBool();
                    break;
                case 88:
                    this.f1375a |= 1024;
                    this.l = codedInputStream.readBool();
                    break;
                case 96:
                    this.f1375a |= 2048;
                    this.m = codedInputStream.readBool();
                    break;
                case 104:
                    this.f1375a |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.n = codedInputStream.readBool();
                    break;
                case 112:
                    this.f1375a |= FragmentTransaction.TRANSIT_EXIT_MASK;
                    this.o = codedInputStream.readInt32();
                    break;
                case 120:
                    this.f1375a |= 16384;
                    this.p = codedInputStream.readBool();
                    break;
                case 130:
                    this.f1375a |= 32768;
                    this.q = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1375a & 1) == 1;
    }

    public i a(int i2) {
        this.f1375a |= 1;
        this.f1376b = i2;
        return this;
    }

    private void j() {
        if ((this.f1375a & 2) != 2) {
            this.f1377c = new LazyStringArrayList(this.f1377c);
            this.f1375a |= 2;
        }
    }

    public i a(boolean z) {
        this.f1375a |= 4;
        this.d = z;
        return this;
    }

    public i b(boolean z) {
        this.f1375a |= 8;
        this.e = z;
        return this;
    }

    public i c(boolean z) {
        this.f1375a |= 16;
        this.f = z;
        return this;
    }

    public i d(boolean z) {
        this.f1375a |= 32;
        this.g = z;
        return this;
    }

    public i e(boolean z) {
        this.f1375a |= 64;
        this.h = z;
        return this;
    }

    public i f(boolean z) {
        this.f1375a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = z;
        return this;
    }

    public i g(boolean z) {
        this.f1375a |= 256;
        this.j = z;
        return this;
    }

    public i h(boolean z) {
        this.f1375a |= 512;
        this.k = z;
        return this;
    }

    public i i(boolean z) {
        this.f1375a |= 1024;
        this.l = z;
        return this;
    }

    public i j(boolean z) {
        this.f1375a |= 2048;
        this.m = z;
        return this;
    }

    public i k(boolean z) {
        this.f1375a |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.n = z;
        return this;
    }

    public i b(int i2) {
        this.f1375a |= FragmentTransaction.TRANSIT_EXIT_MASK;
        this.o = i2;
        return this;
    }

    public i l(boolean z) {
        this.f1375a |= 16384;
        this.p = z;
        return this;
    }

    public i a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1375a |= 32768;
        this.q = str;
        return this;
    }
}
