package com.avast.b.a.a.a;

import android.support.v4.app.NotificationCompat;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import java.util.Collections;
import java.util.List;

/* compiled from: ATProtoGenerics */
public final class ad extends GeneratedMessageLite implements af {

    /* renamed from: a  reason: collision with root package name */
    private static final ad f1272a = new ad(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1273b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public List<j> f1274c;
    /* access modifiers changed from: private */
    public List<y> d;
    /* access modifiers changed from: private */
    public List<g> e;
    /* access modifiers changed from: private */
    public r f;
    /* access modifiers changed from: private */
    public List<o> g;
    /* access modifiers changed from: private */
    public Object h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public long k;
    /* access modifiers changed from: private */
    public Object l;
    /* access modifiers changed from: private */
    public long m;
    /* access modifiers changed from: private */
    public long n;
    /* access modifiers changed from: private */
    public long o;
    private byte p;
    private int q;

    private ad(ae aeVar) {
        super(aeVar);
        this.p = -1;
        this.q = -1;
    }

    private ad(boolean z) {
        this.p = -1;
        this.q = -1;
    }

    public static ad a() {
        return f1272a;
    }

    public int b() {
        return this.f1274c.size();
    }

    public j a(int i2) {
        return this.f1274c.get(i2);
    }

    public int c() {
        return this.d.size();
    }

    public y b(int i2) {
        return this.d.get(i2);
    }

    public int d() {
        return this.e.size();
    }

    public g c(int i2) {
        return this.e.get(i2);
    }

    public boolean e() {
        return (this.f1273b & 1) == 1;
    }

    public r f() {
        return this.f;
    }

    public boolean g() {
        return (this.f1273b & 2) == 2;
    }

    public String h() {
        Object obj = this.h;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.h = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString z() {
        Object obj = this.h;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.h = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean i() {
        return (this.f1273b & 4) == 4;
    }

    public int j() {
        return this.i;
    }

    public boolean k() {
        return (this.f1273b & 8) == 8;
    }

    public boolean l() {
        return this.j;
    }

    public boolean m() {
        return (this.f1273b & 16) == 16;
    }

    public long n() {
        return this.k;
    }

    public boolean o() {
        return (this.f1273b & 32) == 32;
    }

    public String p() {
        Object obj = this.l;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.l = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString A() {
        Object obj = this.l;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.l = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean q() {
        return (this.f1273b & 64) == 64;
    }

    public long r() {
        return this.m;
    }

    public boolean s() {
        return (this.f1273b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public long t() {
        return this.n;
    }

    public boolean u() {
        return (this.f1273b & 256) == 256;
    }

    public long v() {
        return this.o;
    }

    private void B() {
        this.f1274c = Collections.emptyList();
        this.d = Collections.emptyList();
        this.e = Collections.emptyList();
        this.f = r.a();
        this.g = Collections.emptyList();
        this.h = "";
        this.i = 0;
        this.j = false;
        this.k = 0;
        this.l = "";
        this.m = 0;
        this.n = 0;
        this.o = 0;
    }

    public final boolean isInitialized() {
        boolean z = true;
        byte b2 = this.p;
        if (b2 != -1) {
            if (b2 != 1) {
                z = false;
            }
            return z;
        }
        for (int i2 = 0; i2 < b(); i2++) {
            if (!a(i2).isInitialized()) {
                this.p = 0;
                return false;
            }
        }
        for (int i3 = 0; i3 < c(); i3++) {
            if (!b(i3).isInitialized()) {
                this.p = 0;
                return false;
            }
        }
        for (int i4 = 0; i4 < d(); i4++) {
            if (!c(i4).isInitialized()) {
                this.p = 0;
                return false;
            }
        }
        if (!e() || f().isInitialized()) {
            this.p = 1;
            return true;
        }
        this.p = 0;
        return false;
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        for (int i2 = 0; i2 < this.f1274c.size(); i2++) {
            codedOutputStream.writeMessage(1, this.f1274c.get(i2));
        }
        for (int i3 = 0; i3 < this.d.size(); i3++) {
            codedOutputStream.writeMessage(2, this.d.get(i3));
        }
        for (int i4 = 0; i4 < this.e.size(); i4++) {
            codedOutputStream.writeMessage(3, this.e.get(i4));
        }
        if ((this.f1273b & 1) == 1) {
            codedOutputStream.writeMessage(4, this.f);
        }
        for (int i5 = 0; i5 < this.g.size(); i5++) {
            codedOutputStream.writeMessage(5, this.g.get(i5));
        }
        if ((this.f1273b & 2) == 2) {
            codedOutputStream.writeBytes(6, z());
        }
        if ((this.f1273b & 4) == 4) {
            codedOutputStream.writeUInt32(7, this.i);
        }
        if ((this.f1273b & 8) == 8) {
            codedOutputStream.writeBool(8, this.j);
        }
        if ((this.f1273b & 16) == 16) {
            codedOutputStream.writeInt64(9, this.k);
        }
        if ((this.f1273b & 32) == 32) {
            codedOutputStream.writeBytes(10, A());
        }
        if ((this.f1273b & 64) == 64) {
            codedOutputStream.writeInt64(11, this.m);
        }
        if ((this.f1273b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeInt64(12, this.n);
        }
        if ((this.f1273b & 256) == 256) {
            codedOutputStream.writeInt64(13, this.o);
        }
    }

    public int getSerializedSize() {
        int i2 = this.q;
        if (i2 == -1) {
            i2 = 0;
            for (int i3 = 0; i3 < this.f1274c.size(); i3++) {
                i2 += CodedOutputStream.computeMessageSize(1, this.f1274c.get(i3));
            }
            for (int i4 = 0; i4 < this.d.size(); i4++) {
                i2 += CodedOutputStream.computeMessageSize(2, this.d.get(i4));
            }
            for (int i5 = 0; i5 < this.e.size(); i5++) {
                i2 += CodedOutputStream.computeMessageSize(3, this.e.get(i5));
            }
            if ((this.f1273b & 1) == 1) {
                i2 += CodedOutputStream.computeMessageSize(4, this.f);
            }
            for (int i6 = 0; i6 < this.g.size(); i6++) {
                i2 += CodedOutputStream.computeMessageSize(5, this.g.get(i6));
            }
            if ((this.f1273b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(6, z());
            }
            if ((this.f1273b & 4) == 4) {
                i2 += CodedOutputStream.computeUInt32Size(7, this.i);
            }
            if ((this.f1273b & 8) == 8) {
                i2 += CodedOutputStream.computeBoolSize(8, this.j);
            }
            if ((this.f1273b & 16) == 16) {
                i2 += CodedOutputStream.computeInt64Size(9, this.k);
            }
            if ((this.f1273b & 32) == 32) {
                i2 += CodedOutputStream.computeBytesSize(10, A());
            }
            if ((this.f1273b & 64) == 64) {
                i2 += CodedOutputStream.computeInt64Size(11, this.m);
            }
            if ((this.f1273b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeInt64Size(12, this.n);
            }
            if ((this.f1273b & 256) == 256) {
                i2 += CodedOutputStream.computeInt64Size(13, this.o);
            }
            this.q = i2;
        }
        return i2;
    }

    public static ae w() {
        return ae.l();
    }

    /* renamed from: x */
    public ae newBuilderForType() {
        return w();
    }

    public static ae a(ad adVar) {
        return w().mergeFrom(adVar);
    }

    /* renamed from: y */
    public ae toBuilder() {
        return a(this);
    }

    static {
        f1272a.B();
    }
}
