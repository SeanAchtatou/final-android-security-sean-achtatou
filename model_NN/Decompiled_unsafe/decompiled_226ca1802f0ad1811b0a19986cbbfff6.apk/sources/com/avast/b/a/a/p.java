package com.avast.b.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.view.Menu;
import com.avast.b.a.a.a.ag;
import com.avast.b.a.a.a.c;
import com.avast.b.a.a.a.w;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: AvastToWeb */
public final class p extends GeneratedMessageLite implements r {

    /* renamed from: a  reason: collision with root package name */
    private static final p f1384a = new p(true);
    private int A;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1385b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public boolean f1386c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public ag f;
    /* access modifiers changed from: private */
    public ag g;
    /* access modifiers changed from: private */
    public ag h;
    /* access modifiers changed from: private */
    public ag i;
    /* access modifiers changed from: private */
    public ag j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public Object l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public boolean p;
    /* access modifiers changed from: private */
    public boolean q;
    /* access modifiers changed from: private */
    public boolean r;
    /* access modifiers changed from: private */
    public c s;
    /* access modifiers changed from: private */
    public boolean t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public boolean v;
    /* access modifiers changed from: private */
    public boolean w;
    /* access modifiers changed from: private */
    public w x;
    /* access modifiers changed from: private */
    public boolean y;
    private byte z;

    private p(q qVar) {
        super(qVar);
        this.z = -1;
        this.A = -1;
    }

    private p(boolean z2) {
        this.z = -1;
        this.A = -1;
    }

    public static p a() {
        return f1384a;
    }

    public boolean b() {
        return (this.f1385b & 1) == 1;
    }

    public boolean c() {
        return this.f1386c;
    }

    public boolean d() {
        return (this.f1385b & 2) == 2;
    }

    public boolean e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1385b & 4) == 4;
    }

    public boolean g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1385b & 8) == 8;
    }

    public ag i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1385b & 16) == 16;
    }

    public ag k() {
        return this.g;
    }

    public boolean l() {
        return (this.f1385b & 32) == 32;
    }

    public ag m() {
        return this.h;
    }

    public boolean n() {
        return (this.f1385b & 64) == 64;
    }

    public ag o() {
        return this.i;
    }

    public boolean p() {
        return (this.f1385b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public ag q() {
        return this.j;
    }

    public boolean r() {
        return (this.f1385b & 256) == 256;
    }

    public boolean s() {
        return this.k;
    }

    public boolean t() {
        return (this.f1385b & 512) == 512;
    }

    public String u() {
        Object obj = this.l;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.l = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString Y() {
        Object obj = this.l;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.l = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean v() {
        return (this.f1385b & 1024) == 1024;
    }

    public boolean w() {
        return this.m;
    }

    public boolean x() {
        return (this.f1385b & 2048) == 2048;
    }

    public boolean y() {
        return this.n;
    }

    public boolean z() {
        return (this.f1385b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096;
    }

    public boolean A() {
        return this.o;
    }

    public boolean B() {
        return (this.f1385b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192;
    }

    public boolean C() {
        return this.p;
    }

    public boolean D() {
        return (this.f1385b & 16384) == 16384;
    }

    public boolean E() {
        return this.q;
    }

    public boolean F() {
        return (this.f1385b & 32768) == 32768;
    }

    public boolean G() {
        return this.r;
    }

    public boolean H() {
        return (this.f1385b & Menu.CATEGORY_CONTAINER) == 65536;
    }

    public c I() {
        return this.s;
    }

    public boolean J() {
        return (this.f1385b & Menu.CATEGORY_SYSTEM) == 131072;
    }

    public boolean K() {
        return this.t;
    }

    public boolean L() {
        return (this.f1385b & Menu.CATEGORY_ALTERNATIVE) == 262144;
    }

    public boolean M() {
        return this.u;
    }

    public boolean N() {
        return (this.f1385b & 524288) == 524288;
    }

    public boolean O() {
        return this.v;
    }

    public boolean P() {
        return (this.f1385b & 1048576) == 1048576;
    }

    public boolean Q() {
        return this.w;
    }

    public boolean R() {
        return (this.f1385b & 2097152) == 2097152;
    }

    public w S() {
        return this.x;
    }

    public boolean T() {
        return (this.f1385b & 4194304) == 4194304;
    }

    public boolean U() {
        return this.y;
    }

    private void Z() {
        this.f1386c = false;
        this.d = false;
        this.e = false;
        this.f = ag.NONE1;
        this.g = ag.NONE1;
        this.h = ag.NONE1;
        this.i = ag.NONE1;
        this.j = ag.NONE1;
        this.k = false;
        this.l = "";
        this.m = false;
        this.n = false;
        this.o = false;
        this.p = false;
        this.q = false;
        this.r = false;
        this.s = c.NO_RESTRICTION;
        this.t = false;
        this.u = false;
        this.v = false;
        this.w = false;
        this.x = w.NO_SIZE_RESTRICTION;
        this.y = false;
    }

    public final boolean isInitialized() {
        byte b2 = this.z;
        if (b2 == -1) {
            this.z = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1385b & 1) == 1) {
            codedOutputStream.writeBool(1, this.f1386c);
        }
        if ((this.f1385b & 2) == 2) {
            codedOutputStream.writeBool(2, this.d);
        }
        if ((this.f1385b & 4) == 4) {
            codedOutputStream.writeBool(3, this.e);
        }
        if ((this.f1385b & 8) == 8) {
            codedOutputStream.writeEnum(4, this.f.getNumber());
        }
        if ((this.f1385b & 16) == 16) {
            codedOutputStream.writeEnum(5, this.g.getNumber());
        }
        if ((this.f1385b & 32) == 32) {
            codedOutputStream.writeEnum(6, this.h.getNumber());
        }
        if ((this.f1385b & 64) == 64) {
            codedOutputStream.writeEnum(7, this.i.getNumber());
        }
        if ((this.f1385b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeEnum(8, this.j.getNumber());
        }
        if ((this.f1385b & 256) == 256) {
            codedOutputStream.writeBool(9, this.k);
        }
        if ((this.f1385b & 512) == 512) {
            codedOutputStream.writeBytes(10, Y());
        }
        if ((this.f1385b & 1024) == 1024) {
            codedOutputStream.writeBool(11, this.m);
        }
        if ((this.f1385b & 2048) == 2048) {
            codedOutputStream.writeBool(12, this.n);
        }
        if ((this.f1385b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            codedOutputStream.writeBool(13, this.o);
        }
        if ((this.f1385b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            codedOutputStream.writeBool(14, this.p);
        }
        if ((this.f1385b & 16384) == 16384) {
            codedOutputStream.writeBool(15, this.q);
        }
        if ((this.f1385b & 32768) == 32768) {
            codedOutputStream.writeBool(16, this.r);
        }
        if ((this.f1385b & Menu.CATEGORY_CONTAINER) == 65536) {
            codedOutputStream.writeEnum(17, this.s.getNumber());
        }
        if ((this.f1385b & Menu.CATEGORY_SYSTEM) == 131072) {
            codedOutputStream.writeBool(18, this.t);
        }
        if ((this.f1385b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            codedOutputStream.writeBool(19, this.u);
        }
        if ((this.f1385b & 524288) == 524288) {
            codedOutputStream.writeBool(20, this.v);
        }
        if ((this.f1385b & 1048576) == 1048576) {
            codedOutputStream.writeBool(21, this.w);
        }
        if ((this.f1385b & 2097152) == 2097152) {
            codedOutputStream.writeEnum(22, this.x.getNumber());
        }
        if ((this.f1385b & 4194304) == 4194304) {
            codedOutputStream.writeBool(23, this.y);
        }
    }

    public int getSerializedSize() {
        int i2 = this.A;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1385b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBoolSize(1, this.f1386c);
            }
            if ((this.f1385b & 2) == 2) {
                i2 += CodedOutputStream.computeBoolSize(2, this.d);
            }
            if ((this.f1385b & 4) == 4) {
                i2 += CodedOutputStream.computeBoolSize(3, this.e);
            }
            if ((this.f1385b & 8) == 8) {
                i2 += CodedOutputStream.computeEnumSize(4, this.f.getNumber());
            }
            if ((this.f1385b & 16) == 16) {
                i2 += CodedOutputStream.computeEnumSize(5, this.g.getNumber());
            }
            if ((this.f1385b & 32) == 32) {
                i2 += CodedOutputStream.computeEnumSize(6, this.h.getNumber());
            }
            if ((this.f1385b & 64) == 64) {
                i2 += CodedOutputStream.computeEnumSize(7, this.i.getNumber());
            }
            if ((this.f1385b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeEnumSize(8, this.j.getNumber());
            }
            if ((this.f1385b & 256) == 256) {
                i2 += CodedOutputStream.computeBoolSize(9, this.k);
            }
            if ((this.f1385b & 512) == 512) {
                i2 += CodedOutputStream.computeBytesSize(10, Y());
            }
            if ((this.f1385b & 1024) == 1024) {
                i2 += CodedOutputStream.computeBoolSize(11, this.m);
            }
            if ((this.f1385b & 2048) == 2048) {
                i2 += CodedOutputStream.computeBoolSize(12, this.n);
            }
            if ((this.f1385b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
                i2 += CodedOutputStream.computeBoolSize(13, this.o);
            }
            if ((this.f1385b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
                i2 += CodedOutputStream.computeBoolSize(14, this.p);
            }
            if ((this.f1385b & 16384) == 16384) {
                i2 += CodedOutputStream.computeBoolSize(15, this.q);
            }
            if ((this.f1385b & 32768) == 32768) {
                i2 += CodedOutputStream.computeBoolSize(16, this.r);
            }
            if ((this.f1385b & Menu.CATEGORY_CONTAINER) == 65536) {
                i2 += CodedOutputStream.computeEnumSize(17, this.s.getNumber());
            }
            if ((this.f1385b & Menu.CATEGORY_SYSTEM) == 131072) {
                i2 += CodedOutputStream.computeBoolSize(18, this.t);
            }
            if ((this.f1385b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
                i2 += CodedOutputStream.computeBoolSize(19, this.u);
            }
            if ((this.f1385b & 524288) == 524288) {
                i2 += CodedOutputStream.computeBoolSize(20, this.v);
            }
            if ((this.f1385b & 1048576) == 1048576) {
                i2 += CodedOutputStream.computeBoolSize(21, this.w);
            }
            if ((this.f1385b & 2097152) == 2097152) {
                i2 += CodedOutputStream.computeEnumSize(22, this.x.getNumber());
            }
            if ((this.f1385b & 4194304) == 4194304) {
                i2 += CodedOutputStream.computeBoolSize(23, this.y);
            }
            this.A = i2;
        }
        return i2;
    }

    public static q V() {
        return q.g();
    }

    /* renamed from: W */
    public q newBuilderForType() {
        return V();
    }

    public static q a(p pVar) {
        return V().mergeFrom(pVar);
    }

    /* renamed from: X */
    public q toBuilder() {
        return a(this);
    }

    static {
        f1384a.Z();
    }
}
