package com.avast.b.a.a;

import android.support.v4.app.NotificationCompat;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: AvastToWeb */
public final class am extends GeneratedMessageLite implements as {

    /* renamed from: a  reason: collision with root package name */
    private static final am f1346a = new am(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1347b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public aq f1348c;
    /* access modifiers changed from: private */
    public long d;
    /* access modifiers changed from: private */
    public Object e;
    /* access modifiers changed from: private */
    public ao f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public Object h;
    /* access modifiers changed from: private */
    public Object i;
    /* access modifiers changed from: private */
    public Object j;
    /* access modifiers changed from: private */
    public Object k;
    private byte l;
    private int m;

    private am(an anVar) {
        super(anVar);
        this.l = -1;
        this.m = -1;
    }

    private am(boolean z) {
        this.l = -1;
        this.m = -1;
    }

    public static am a() {
        return f1346a;
    }

    public boolean b() {
        return (this.f1347b & 1) == 1;
    }

    public aq c() {
        return this.f1348c;
    }

    public boolean d() {
        return (this.f1347b & 2) == 2;
    }

    public long e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1347b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString w() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean h() {
        return (this.f1347b & 8) == 8;
    }

    public ao i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1347b & 16) == 16;
    }

    public int k() {
        return this.g;
    }

    public boolean l() {
        return (this.f1347b & 32) == 32;
    }

    public String m() {
        Object obj = this.h;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.h = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString x() {
        Object obj = this.h;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.h = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean n() {
        return (this.f1347b & 64) == 64;
    }

    public String o() {
        Object obj = this.i;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.i = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString y() {
        Object obj = this.i;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.i = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean p() {
        return (this.f1347b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public String q() {
        Object obj = this.j;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.j = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString z() {
        Object obj = this.j;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.j = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean r() {
        return (this.f1347b & 256) == 256;
    }

    public String s() {
        Object obj = this.k;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.k = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString A() {
        Object obj = this.k;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.k = copyFromUtf8;
        return copyFromUtf8;
    }

    private void B() {
        this.f1348c = aq.UPDATE_SIMULATION;
        this.d = 0;
        this.e = "";
        this.f = ao.NONE;
        this.g = -1;
        this.h = "";
        this.i = "";
        this.j = "";
        this.k = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.l;
        if (b2 != -1) {
            if (b2 == 1) {
                return true;
            }
            return false;
        } else if (!b()) {
            this.l = 0;
            return false;
        } else if (!d()) {
            this.l = 0;
            return false;
        } else {
            this.l = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1347b & 1) == 1) {
            codedOutputStream.writeEnum(1, this.f1348c.getNumber());
        }
        if ((this.f1347b & 2) == 2) {
            codedOutputStream.writeInt64(2, this.d);
        }
        if ((this.f1347b & 4) == 4) {
            codedOutputStream.writeBytes(3, w());
        }
        if ((this.f1347b & 8) == 8) {
            codedOutputStream.writeEnum(4, this.f.getNumber());
        }
        if ((this.f1347b & 16) == 16) {
            codedOutputStream.writeInt32(5, this.g);
        }
        if ((this.f1347b & 32) == 32) {
            codedOutputStream.writeBytes(6, x());
        }
        if ((this.f1347b & 64) == 64) {
            codedOutputStream.writeBytes(7, y());
        }
        if ((this.f1347b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBytes(8, z());
        }
        if ((this.f1347b & 256) == 256) {
            codedOutputStream.writeBytes(9, A());
        }
    }

    public int getSerializedSize() {
        int i2 = this.m;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1347b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeEnumSize(1, this.f1348c.getNumber());
            }
            if ((this.f1347b & 2) == 2) {
                i2 += CodedOutputStream.computeInt64Size(2, this.d);
            }
            if ((this.f1347b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, w());
            }
            if ((this.f1347b & 8) == 8) {
                i2 += CodedOutputStream.computeEnumSize(4, this.f.getNumber());
            }
            if ((this.f1347b & 16) == 16) {
                i2 += CodedOutputStream.computeInt32Size(5, this.g);
            }
            if ((this.f1347b & 32) == 32) {
                i2 += CodedOutputStream.computeBytesSize(6, x());
            }
            if ((this.f1347b & 64) == 64) {
                i2 += CodedOutputStream.computeBytesSize(7, y());
            }
            if ((this.f1347b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeBytesSize(8, z());
            }
            if ((this.f1347b & 256) == 256) {
                i2 += CodedOutputStream.computeBytesSize(9, A());
            }
            this.m = i2;
        }
        return i2;
    }

    public static an t() {
        return an.i();
    }

    /* renamed from: u */
    public an newBuilderForType() {
        return t();
    }

    public static an a(am amVar) {
        return t().mergeFrom(amVar);
    }

    /* renamed from: v */
    public an toBuilder() {
        return a(this);
    }

    static {
        f1346a.B();
    }
}
