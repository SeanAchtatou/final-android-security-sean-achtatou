package com.avast.b.a.a.a;

import com.google.protobuf.Internal;

/* compiled from: ATProtoGenerics */
public enum w implements Internal.EnumLite {
    NO_SIZE_RESTRICTION(0, 0),
    MAX_100_MB(1, 1),
    MAX_50_MB(2, 2),
    MAX_10_MB(3, 3),
    MAX_2_MB(4, 4);
    
    private static Internal.EnumLiteMap<w> f = new x();
    private final int g;

    public final int getNumber() {
        return this.g;
    }

    public static w a(int i) {
        switch (i) {
            case 0:
                return NO_SIZE_RESTRICTION;
            case 1:
                return MAX_100_MB;
            case 2:
                return MAX_50_MB;
            case 3:
                return MAX_10_MB;
            case 4:
                return MAX_2_MB;
            default:
                return null;
        }
    }

    private w(int i, int i2) {
        this.g = i2;
    }
}
