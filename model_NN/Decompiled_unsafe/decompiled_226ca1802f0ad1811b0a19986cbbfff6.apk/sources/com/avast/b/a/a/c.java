package com.avast.b.a.a;

import com.google.protobuf.Internal;

/* compiled from: AvastToDevice */
public enum c implements Internal.EnumLite {
    NONE(0, 0),
    NO_AUID(1, 1),
    NO_PASSWORD(2, 2),
    DEVICE_NOT_FOUND(3, 3),
    DEVICE_ID_EMPTY(4, 4),
    ACCOUNT_ID_EMPTY(5, 5),
    NO_C2DM_ID_ON_SERVER(6, 6),
    NOT_REGISTERED_FOR_C2DM(7, 7),
    GENERIC_C2DM_ERROR_FROM_GOOGLE(8, 8),
    GENERIC_C2DM_ERROR(9, 9),
    INVALID_RESPONSE(10, 10),
    CLIENT_TOO_OLD(11, 11),
    DEVICE_DEPRECATED(12, 12),
    DEVICE_NOT_ACTIVATED(13, 13);
    
    private static Internal.EnumLiteMap<c> o = new d();
    private final int p;

    public final int getNumber() {
        return this.p;
    }

    public static c a(int i) {
        switch (i) {
            case 0:
                return NONE;
            case 1:
                return NO_AUID;
            case 2:
                return NO_PASSWORD;
            case 3:
                return DEVICE_NOT_FOUND;
            case 4:
                return DEVICE_ID_EMPTY;
            case 5:
                return ACCOUNT_ID_EMPTY;
            case 6:
                return NO_C2DM_ID_ON_SERVER;
            case 7:
                return NOT_REGISTERED_FOR_C2DM;
            case 8:
                return GENERIC_C2DM_ERROR_FROM_GOOGLE;
            case 9:
                return GENERIC_C2DM_ERROR;
            case 10:
                return INVALID_RESPONSE;
            case 11:
                return CLIENT_TOO_OLD;
            case 12:
                return DEVICE_DEPRECATED;
            case 13:
                return DEVICE_NOT_ACTIVATED;
            default:
                return null;
        }
    }

    private c(int i, int i2) {
        this.p = i2;
    }
}
