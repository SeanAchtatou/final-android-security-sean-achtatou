package com.avast.b.a.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToWeb */
public final class ah extends GeneratedMessageLite.Builder<ag, ah> implements ai {

    /* renamed from: a  reason: collision with root package name */
    private int f1338a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1339b = "";

    private ah() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static ah g() {
        return new ah();
    }

    /* renamed from: a */
    public ah clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public ag getDefaultInstanceForType() {
        return ag.a();
    }

    /* renamed from: c */
    public ag build() {
        ag d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public ag d() {
        int i = 1;
        ag agVar = new ag(this);
        if ((this.f1338a & 1) != 1) {
            i = 0;
        }
        Object unused = agVar.f1337c = this.f1339b;
        int unused2 = agVar.f1336b = i;
        return agVar;
    }

    /* renamed from: a */
    public ah mergeFrom(ag agVar) {
        if (agVar != ag.a() && agVar.b()) {
            a(agVar.c());
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public ah mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1338a |= 1;
                    this.f1339b = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public ah a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1338a |= 1;
        this.f1339b = str;
        return this;
    }
}
