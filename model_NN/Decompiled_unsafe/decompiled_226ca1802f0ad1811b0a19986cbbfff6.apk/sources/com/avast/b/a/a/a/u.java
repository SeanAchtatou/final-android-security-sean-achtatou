package com.avast.b.a.a.a;

import com.google.protobuf.Internal;

/* compiled from: ATProtoGenerics */
public enum u implements Internal.EnumLite {
    IMAGE(0, 0),
    AUDIO(1, 1),
    VIDEO(2, 2),
    APK(3, 3),
    CALL(4, 4),
    SMS(5, 5),
    CONTACT(6, 6);
    
    private static Internal.EnumLiteMap<u> h = new v();
    private final int i;

    public final int getNumber() {
        return this.i;
    }

    public static u a(int i2) {
        switch (i2) {
            case 0:
                return IMAGE;
            case 1:
                return AUDIO;
            case 2:
                return VIDEO;
            case 3:
                return APK;
            case 4:
                return CALL;
            case 5:
                return SMS;
            case 6:
                return CONTACT;
            default:
                return null;
        }
    }

    private u(int i2, int i3) {
        this.i = i3;
    }
}
