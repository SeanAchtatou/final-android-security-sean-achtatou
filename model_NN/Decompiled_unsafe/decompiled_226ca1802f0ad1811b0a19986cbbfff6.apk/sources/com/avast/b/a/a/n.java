package com.avast.b.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.actionbarsherlock.view.Menu;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToWeb */
public final class n extends GeneratedMessageLite.Builder<m, n> implements o {
    private boolean A;
    private Object B = "";
    private boolean C;
    private boolean D;
    private boolean E;
    private boolean F;
    private boolean G = true;
    private boolean H;
    private boolean I = true;
    private boolean J = true;
    private boolean K;
    private boolean L;
    private boolean M;
    private Object N = "";
    private int O = -1;
    private int P = -1;
    private int Q = -1;
    private boolean R;
    private boolean S;
    private boolean T;
    private boolean U;
    private boolean V;
    private boolean W;
    private boolean X;
    private boolean Y;
    private boolean Z;

    /* renamed from: a  reason: collision with root package name */
    private int f1381a;
    private boolean aa = true;
    private boolean ab;
    private boolean ac = true;
    private boolean ad = true;
    private boolean ae;
    private boolean af;
    private int ag;
    private double ah;
    private double ai;
    private int aj;
    private boolean ak;
    private boolean al;
    private boolean am;
    private boolean an;

    /* renamed from: b  reason: collision with root package name */
    private int f1382b;

    /* renamed from: c  reason: collision with root package name */
    private Object f1383c = "";
    private Object d = "";
    private Object e = "";
    private boolean f;
    private boolean g;
    private boolean h;
    private boolean i;
    private boolean j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    private int o;
    private boolean p;
    private boolean q;
    private Object r = "";
    private Object s = "";
    private Object t = "";
    private Object u = "";
    private Object v = "";
    private Object w = "";
    private Object x = "";
    private Object y = "";
    private boolean z;

    private n() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static n g() {
        return new n();
    }

    /* renamed from: a */
    public n clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public m getDefaultInstanceForType() {
        return m.a();
    }

    /* renamed from: c */
    public m build() {
        m d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public m d() {
        m mVar = new m(this);
        int i2 = this.f1381a;
        int i3 = this.f1382b;
        int i4 = 0;
        int i5 = 0;
        if ((i2 & 1) == 1) {
            i4 = 1;
        }
        Object unused = mVar.d = this.f1383c;
        if ((i2 & 2) == 2) {
            i4 |= 2;
        }
        Object unused2 = mVar.e = this.d;
        if ((i2 & 4) == 4) {
            i4 |= 4;
        }
        Object unused3 = mVar.f = this.e;
        if ((i2 & 8) == 8) {
            i4 |= 8;
        }
        boolean unused4 = mVar.g = this.f;
        if ((i2 & 16) == 16) {
            i4 |= 16;
        }
        boolean unused5 = mVar.h = this.g;
        if ((i2 & 32) == 32) {
            i4 |= 32;
        }
        boolean unused6 = mVar.i = this.h;
        if ((i2 & 64) == 64) {
            i4 |= 64;
        }
        boolean unused7 = mVar.j = this.i;
        if ((i2 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i4 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        boolean unused8 = mVar.k = this.j;
        if ((i2 & 256) == 256) {
            i4 |= 256;
        }
        boolean unused9 = mVar.l = this.k;
        if ((i2 & 512) == 512) {
            i4 |= 512;
        }
        boolean unused10 = mVar.m = this.l;
        if ((i2 & 1024) == 1024) {
            i4 |= 1024;
        }
        boolean unused11 = mVar.n = this.m;
        if ((i2 & 2048) == 2048) {
            i4 |= 2048;
        }
        boolean unused12 = mVar.o = this.n;
        if ((i2 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i4 |= FragmentTransaction.TRANSIT_ENTER_MASK;
        }
        int unused13 = mVar.p = this.o;
        if ((i2 & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            i4 |= FragmentTransaction.TRANSIT_EXIT_MASK;
        }
        boolean unused14 = mVar.q = this.p;
        if ((i2 & 16384) == 16384) {
            i4 |= 16384;
        }
        boolean unused15 = mVar.r = this.q;
        if ((i2 & 32768) == 32768) {
            i4 |= 32768;
        }
        Object unused16 = mVar.s = this.r;
        if ((i2 & Menu.CATEGORY_CONTAINER) == 65536) {
            i4 |= Menu.CATEGORY_CONTAINER;
        }
        Object unused17 = mVar.t = this.s;
        if ((i2 & Menu.CATEGORY_SYSTEM) == 131072) {
            i4 |= Menu.CATEGORY_SYSTEM;
        }
        Object unused18 = mVar.u = this.t;
        if ((i2 & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            i4 |= Menu.CATEGORY_ALTERNATIVE;
        }
        Object unused19 = mVar.v = this.u;
        if ((524288 & i2) == 524288) {
            i4 |= 524288;
        }
        Object unused20 = mVar.w = this.v;
        if ((1048576 & i2) == 1048576) {
            i4 |= 1048576;
        }
        Object unused21 = mVar.x = this.w;
        if ((2097152 & i2) == 2097152) {
            i4 |= 2097152;
        }
        Object unused22 = mVar.y = this.x;
        if ((4194304 & i2) == 4194304) {
            i4 |= 4194304;
        }
        Object unused23 = mVar.z = this.y;
        if ((8388608 & i2) == 8388608) {
            i4 |= 8388608;
        }
        boolean unused24 = mVar.A = this.z;
        if ((16777216 & i2) == 16777216) {
            i4 |= 16777216;
        }
        boolean unused25 = mVar.B = this.A;
        if ((33554432 & i2) == 33554432) {
            i4 |= 33554432;
        }
        Object unused26 = mVar.C = this.B;
        if ((67108864 & i2) == 67108864) {
            i4 |= 67108864;
        }
        boolean unused27 = mVar.D = this.C;
        if ((134217728 & i2) == 134217728) {
            i4 |= 134217728;
        }
        boolean unused28 = mVar.E = this.D;
        if ((268435456 & i2) == 268435456) {
            i4 |= 268435456;
        }
        boolean unused29 = mVar.F = this.E;
        if ((536870912 & i2) == 536870912) {
            i4 |= 536870912;
        }
        boolean unused30 = mVar.G = this.F;
        if ((1073741824 & i2) == 1073741824) {
            i4 |= 1073741824;
        }
        boolean unused31 = mVar.H = this.G;
        if ((i2 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            i4 |= Integer.MIN_VALUE;
        }
        boolean unused32 = mVar.I = this.H;
        if ((i3 & 1) == 1) {
            i5 = 1;
        }
        boolean unused33 = mVar.J = this.I;
        if ((i3 & 2) == 2) {
            i5 |= 2;
        }
        boolean unused34 = mVar.K = this.J;
        if ((i3 & 4) == 4) {
            i5 |= 4;
        }
        boolean unused35 = mVar.L = this.K;
        if ((i3 & 8) == 8) {
            i5 |= 8;
        }
        boolean unused36 = mVar.M = this.L;
        if ((i3 & 16) == 16) {
            i5 |= 16;
        }
        boolean unused37 = mVar.N = this.M;
        if ((i3 & 32) == 32) {
            i5 |= 32;
        }
        Object unused38 = mVar.O = this.N;
        if ((i3 & 64) == 64) {
            i5 |= 64;
        }
        int unused39 = mVar.P = this.O;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i5 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        int unused40 = mVar.Q = this.P;
        if ((i3 & 256) == 256) {
            i5 |= 256;
        }
        int unused41 = mVar.R = this.Q;
        if ((i3 & 512) == 512) {
            i5 |= 512;
        }
        boolean unused42 = mVar.S = this.R;
        if ((i3 & 1024) == 1024) {
            i5 |= 1024;
        }
        boolean unused43 = mVar.T = this.S;
        if ((i3 & 2048) == 2048) {
            i5 |= 2048;
        }
        boolean unused44 = mVar.U = this.T;
        if ((i3 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i5 |= FragmentTransaction.TRANSIT_ENTER_MASK;
        }
        boolean unused45 = mVar.V = this.U;
        if ((i3 & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            i5 |= FragmentTransaction.TRANSIT_EXIT_MASK;
        }
        boolean unused46 = mVar.W = this.V;
        if ((i3 & 16384) == 16384) {
            i5 |= 16384;
        }
        boolean unused47 = mVar.X = this.W;
        if ((i3 & 32768) == 32768) {
            i5 |= 32768;
        }
        boolean unused48 = mVar.Y = this.X;
        if ((i3 & Menu.CATEGORY_CONTAINER) == 65536) {
            i5 |= Menu.CATEGORY_CONTAINER;
        }
        boolean unused49 = mVar.Z = this.Y;
        if ((i3 & Menu.CATEGORY_SYSTEM) == 131072) {
            i5 |= Menu.CATEGORY_SYSTEM;
        }
        boolean unused50 = mVar.aa = this.Z;
        if ((i3 & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            i5 |= Menu.CATEGORY_ALTERNATIVE;
        }
        boolean unused51 = mVar.ab = this.aa;
        if ((524288 & i3) == 524288) {
            i5 |= 524288;
        }
        boolean unused52 = mVar.ac = this.ab;
        if ((1048576 & i3) == 1048576) {
            i5 |= 1048576;
        }
        boolean unused53 = mVar.ad = this.ac;
        if ((2097152 & i3) == 2097152) {
            i5 |= 2097152;
        }
        boolean unused54 = mVar.ae = this.ad;
        if ((4194304 & i3) == 4194304) {
            i5 |= 4194304;
        }
        boolean unused55 = mVar.af = this.ae;
        if ((8388608 & i3) == 8388608) {
            i5 |= 8388608;
        }
        boolean unused56 = mVar.ag = this.af;
        if ((16777216 & i3) == 16777216) {
            i5 |= 16777216;
        }
        int unused57 = mVar.ah = this.ag;
        if ((33554432 & i3) == 33554432) {
            i5 |= 33554432;
        }
        double unused58 = mVar.ai = this.ah;
        if ((67108864 & i3) == 67108864) {
            i5 |= 67108864;
        }
        double unused59 = mVar.aj = this.ai;
        if ((134217728 & i3) == 134217728) {
            i5 |= 134217728;
        }
        int unused60 = mVar.ak = this.aj;
        if ((268435456 & i3) == 268435456) {
            i5 |= 268435456;
        }
        boolean unused61 = mVar.al = this.ak;
        if ((536870912 & i3) == 536870912) {
            i5 |= 536870912;
        }
        boolean unused62 = mVar.am = this.al;
        if ((1073741824 & i3) == 1073741824) {
            i5 |= 1073741824;
        }
        boolean unused63 = mVar.an = this.am;
        if ((i3 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            i5 |= Integer.MIN_VALUE;
        }
        boolean unused64 = mVar.ao = this.an;
        int unused65 = mVar.f1379b = i4;
        int unused66 = mVar.f1380c = i5;
        return mVar;
    }

    /* renamed from: a */
    public n mergeFrom(m mVar) {
        if (mVar != m.a()) {
            if (mVar.b()) {
                a(mVar.c());
            }
            if (mVar.d()) {
                b(mVar.e());
            }
            if (mVar.f()) {
                c(mVar.g());
            }
            if (mVar.h()) {
                a(mVar.i());
            }
            if (mVar.j()) {
                b(mVar.k());
            }
            if (mVar.l()) {
                c(mVar.m());
            }
            if (mVar.n()) {
                d(mVar.o());
            }
            if (mVar.p()) {
                e(mVar.q());
            }
            if (mVar.r()) {
                f(mVar.s());
            }
            if (mVar.t()) {
                g(mVar.u());
            }
            if (mVar.v()) {
                h(mVar.w());
            }
            if (mVar.x()) {
                i(mVar.y());
            }
            if (mVar.z()) {
                a(mVar.A());
            }
            if (mVar.B()) {
                j(mVar.C());
            }
            if (mVar.D()) {
                k(mVar.E());
            }
            if (mVar.F()) {
                d(mVar.G());
            }
            if (mVar.H()) {
                e(mVar.I());
            }
            if (mVar.J()) {
                f(mVar.K());
            }
            if (mVar.L()) {
                g(mVar.M());
            }
            if (mVar.N()) {
                h(mVar.O());
            }
            if (mVar.P()) {
                i(mVar.Q());
            }
            if (mVar.R()) {
                j(mVar.S());
            }
            if (mVar.T()) {
                k(mVar.U());
            }
            if (mVar.V()) {
                l(mVar.W());
            }
            if (mVar.X()) {
                m(mVar.Y());
            }
            if (mVar.Z()) {
                l(mVar.aa());
            }
            if (mVar.ab()) {
                n(mVar.ac());
            }
            if (mVar.ad()) {
                o(mVar.ae());
            }
            if (mVar.af()) {
                p(mVar.ag());
            }
            if (mVar.ah()) {
                q(mVar.ai());
            }
            if (mVar.aj()) {
                r(mVar.ak());
            }
            if (mVar.al()) {
                s(mVar.am());
            }
            if (mVar.an()) {
                t(mVar.ao());
            }
            if (mVar.ap()) {
                u(mVar.aq());
            }
            if (mVar.ar()) {
                v(mVar.as());
            }
            if (mVar.at()) {
                w(mVar.au());
            }
            if (mVar.av()) {
                x(mVar.aw());
            }
            if (mVar.ax()) {
                m(mVar.ay());
            }
            if (mVar.az()) {
                b(mVar.aA());
            }
            if (mVar.aB()) {
                c(mVar.aC());
            }
            if (mVar.aD()) {
                d(mVar.aE());
            }
            if (mVar.aF()) {
                y(mVar.aG());
            }
            if (mVar.aH()) {
                z(mVar.aI());
            }
            if (mVar.aJ()) {
                A(mVar.aK());
            }
            if (mVar.aL()) {
                B(mVar.aM());
            }
            if (mVar.aN()) {
                C(mVar.aO());
            }
            if (mVar.aP()) {
                D(mVar.aQ());
            }
            if (mVar.aR()) {
                E(mVar.aS());
            }
            if (mVar.aT()) {
                F(mVar.aU());
            }
            if (mVar.aV()) {
                G(mVar.aW());
            }
            if (mVar.aX()) {
                H(mVar.aY());
            }
            if (mVar.aZ()) {
                I(mVar.ba());
            }
            if (mVar.bb()) {
                J(mVar.bc());
            }
            if (mVar.bd()) {
                K(mVar.be());
            }
            if (mVar.bf()) {
                L(mVar.bg());
            }
            if (mVar.bh()) {
                M(mVar.bi());
            }
            if (mVar.bj()) {
                e(mVar.bk());
            }
            if (mVar.bl()) {
                a(mVar.bm());
            }
            if (mVar.bn()) {
                b(mVar.bo());
            }
            if (mVar.bp()) {
                f(mVar.bq());
            }
            if (mVar.br()) {
                N(mVar.bs());
            }
            if (mVar.bt()) {
                O(mVar.bu());
            }
            if (mVar.bv()) {
                P(mVar.bw());
            }
            if (mVar.bx()) {
                Q(mVar.by());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public n mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1381a |= 1;
                    this.f1383c = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1381a |= 2;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1381a |= 4;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    this.f1381a |= 8;
                    this.f = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f1381a |= 16;
                    this.g = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                    this.f1381a |= 32;
                    this.h = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    this.f1381a |= 64;
                    this.i = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_activityChooserViewStyle:
                    this.f1381a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.j = codedInputStream.readBool();
                    break;
                case 72:
                    this.f1381a |= 256;
                    this.k = codedInputStream.readBool();
                    break;
                case 80:
                    this.f1381a |= 512;
                    this.l = codedInputStream.readBool();
                    break;
                case 88:
                    this.f1381a |= 1024;
                    this.m = codedInputStream.readBool();
                    break;
                case 96:
                    this.f1381a |= 2048;
                    this.n = codedInputStream.readBool();
                    break;
                case 104:
                    this.f1381a |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.o = codedInputStream.readInt32();
                    break;
                case 112:
                    this.f1381a |= FragmentTransaction.TRANSIT_EXIT_MASK;
                    this.p = codedInputStream.readBool();
                    break;
                case 120:
                    this.f1381a |= 16384;
                    this.q = codedInputStream.readBool();
                    break;
                case 130:
                    this.f1381a |= 32768;
                    this.r = codedInputStream.readBytes();
                    break;
                case 138:
                    this.f1381a |= Menu.CATEGORY_CONTAINER;
                    this.s = codedInputStream.readBytes();
                    break;
                case 146:
                    this.f1381a |= Menu.CATEGORY_SYSTEM;
                    this.t = codedInputStream.readBytes();
                    break;
                case 154:
                    this.f1381a |= Menu.CATEGORY_ALTERNATIVE;
                    this.u = codedInputStream.readBytes();
                    break;
                case 162:
                    this.f1381a |= 524288;
                    this.v = codedInputStream.readBytes();
                    break;
                case 170:
                    this.f1381a |= 1048576;
                    this.w = codedInputStream.readBytes();
                    break;
                case 178:
                    this.f1381a |= 2097152;
                    this.x = codedInputStream.readBytes();
                    break;
                case 186:
                    this.f1381a |= 4194304;
                    this.y = codedInputStream.readBytes();
                    break;
                case 192:
                    this.f1381a |= 8388608;
                    this.z = codedInputStream.readBool();
                    break;
                case 200:
                    this.f1381a |= 16777216;
                    this.A = codedInputStream.readBool();
                    break;
                case 210:
                    this.f1381a |= 33554432;
                    this.B = codedInputStream.readBytes();
                    break;
                case 216:
                    this.f1381a |= 67108864;
                    this.C = codedInputStream.readBool();
                    break;
                case 224:
                    this.f1381a |= 134217728;
                    this.D = codedInputStream.readBool();
                    break;
                case 232:
                    this.f1381a |= 268435456;
                    this.E = codedInputStream.readBool();
                    break;
                case 240:
                    this.f1381a |= 536870912;
                    this.F = codedInputStream.readBool();
                    break;
                case 248:
                    this.f1381a |= 1073741824;
                    this.G = codedInputStream.readBool();
                    break;
                case 256:
                    this.f1381a |= Integer.MIN_VALUE;
                    this.H = codedInputStream.readBool();
                    break;
                case 264:
                    this.f1382b |= 1;
                    this.I = codedInputStream.readBool();
                    break;
                case 272:
                    this.f1382b |= 2;
                    this.J = codedInputStream.readBool();
                    break;
                case 280:
                    this.f1382b |= 4;
                    this.K = codedInputStream.readBool();
                    break;
                case 288:
                    this.f1382b |= 8;
                    this.L = codedInputStream.readBool();
                    break;
                case 296:
                    this.f1382b |= 16;
                    this.M = codedInputStream.readBool();
                    break;
                case 306:
                    this.f1382b |= 32;
                    this.N = codedInputStream.readBytes();
                    break;
                case 312:
                    this.f1382b |= 64;
                    this.O = codedInputStream.readInt32();
                    break;
                case 320:
                    this.f1382b |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.P = codedInputStream.readInt32();
                    break;
                case 328:
                    this.f1382b |= 256;
                    this.Q = codedInputStream.readInt32();
                    break;
                case 336:
                    this.f1382b |= 512;
                    this.R = codedInputStream.readBool();
                    break;
                case 344:
                    this.f1382b |= 1024;
                    this.S = codedInputStream.readBool();
                    break;
                case 352:
                    this.f1382b |= 2048;
                    this.T = codedInputStream.readBool();
                    break;
                case 360:
                    this.f1382b |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.U = codedInputStream.readBool();
                    break;
                case 368:
                    this.f1382b |= FragmentTransaction.TRANSIT_EXIT_MASK;
                    this.V = codedInputStream.readBool();
                    break;
                case 376:
                    this.f1382b |= 16384;
                    this.W = codedInputStream.readBool();
                    break;
                case 384:
                    this.f1382b |= 32768;
                    this.X = codedInputStream.readBool();
                    break;
                case 392:
                    this.f1382b |= Menu.CATEGORY_CONTAINER;
                    this.Y = codedInputStream.readBool();
                    break;
                case 400:
                    this.f1382b |= Menu.CATEGORY_SYSTEM;
                    this.Z = codedInputStream.readBool();
                    break;
                case 408:
                    this.f1382b |= Menu.CATEGORY_ALTERNATIVE;
                    this.aa = codedInputStream.readBool();
                    break;
                case 416:
                    this.f1382b |= 524288;
                    this.ab = codedInputStream.readBool();
                    break;
                case 424:
                    this.f1382b |= 1048576;
                    this.ac = codedInputStream.readBool();
                    break;
                case 432:
                    this.f1382b |= 2097152;
                    this.ad = codedInputStream.readBool();
                    break;
                case 440:
                    this.f1382b |= 4194304;
                    this.ae = codedInputStream.readBool();
                    break;
                case 448:
                    this.f1382b |= 8388608;
                    this.af = codedInputStream.readBool();
                    break;
                case 456:
                    this.f1382b |= 16777216;
                    this.ag = codedInputStream.readInt32();
                    break;
                case 465:
                    this.f1382b |= 33554432;
                    this.ah = codedInputStream.readDouble();
                    break;
                case 473:
                    this.f1382b |= 67108864;
                    this.ai = codedInputStream.readDouble();
                    break;
                case 480:
                    this.f1382b |= 134217728;
                    this.aj = codedInputStream.readInt32();
                    break;
                case 488:
                    this.f1382b |= 268435456;
                    this.ak = codedInputStream.readBool();
                    break;
                case 496:
                    this.f1382b |= 536870912;
                    this.al = codedInputStream.readBool();
                    break;
                case 504:
                    this.f1382b |= 1073741824;
                    this.am = codedInputStream.readBool();
                    break;
                case 512:
                    this.f1382b |= Integer.MIN_VALUE;
                    this.an = codedInputStream.readBool();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public n a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= 1;
        this.f1383c = str;
        return this;
    }

    public n b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= 2;
        this.d = str;
        return this;
    }

    public n c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= 4;
        this.e = str;
        return this;
    }

    public n a(boolean z2) {
        this.f1381a |= 8;
        this.f = z2;
        return this;
    }

    public n b(boolean z2) {
        this.f1381a |= 16;
        this.g = z2;
        return this;
    }

    public n c(boolean z2) {
        this.f1381a |= 32;
        this.h = z2;
        return this;
    }

    public n d(boolean z2) {
        this.f1381a |= 64;
        this.i = z2;
        return this;
    }

    public n e(boolean z2) {
        this.f1381a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.j = z2;
        return this;
    }

    public n f(boolean z2) {
        this.f1381a |= 256;
        this.k = z2;
        return this;
    }

    public n g(boolean z2) {
        this.f1381a |= 512;
        this.l = z2;
        return this;
    }

    public n h(boolean z2) {
        this.f1381a |= 1024;
        this.m = z2;
        return this;
    }

    public n i(boolean z2) {
        this.f1381a |= 2048;
        this.n = z2;
        return this;
    }

    public n a(int i2) {
        this.f1381a |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.o = i2;
        return this;
    }

    public n j(boolean z2) {
        this.f1381a |= FragmentTransaction.TRANSIT_EXIT_MASK;
        this.p = z2;
        return this;
    }

    public n k(boolean z2) {
        this.f1381a |= 16384;
        this.q = z2;
        return this;
    }

    public n d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= 32768;
        this.r = str;
        return this;
    }

    public n e(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= Menu.CATEGORY_CONTAINER;
        this.s = str;
        return this;
    }

    public n f(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= Menu.CATEGORY_SYSTEM;
        this.t = str;
        return this;
    }

    public n g(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= Menu.CATEGORY_ALTERNATIVE;
        this.u = str;
        return this;
    }

    public n h(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= 524288;
        this.v = str;
        return this;
    }

    public n i(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= 1048576;
        this.w = str;
        return this;
    }

    public n j(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= 2097152;
        this.x = str;
        return this;
    }

    public n k(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= 4194304;
        this.y = str;
        return this;
    }

    public n l(boolean z2) {
        this.f1381a |= 8388608;
        this.z = z2;
        return this;
    }

    public n m(boolean z2) {
        this.f1381a |= 16777216;
        this.A = z2;
        return this;
    }

    public n l(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1381a |= 33554432;
        this.B = str;
        return this;
    }

    public n n(boolean z2) {
        this.f1381a |= 67108864;
        this.C = z2;
        return this;
    }

    public n o(boolean z2) {
        this.f1381a |= 134217728;
        this.D = z2;
        return this;
    }

    public n p(boolean z2) {
        this.f1381a |= 268435456;
        this.E = z2;
        return this;
    }

    public n q(boolean z2) {
        this.f1381a |= 536870912;
        this.F = z2;
        return this;
    }

    public n r(boolean z2) {
        this.f1381a |= 1073741824;
        this.G = z2;
        return this;
    }

    public n s(boolean z2) {
        this.f1381a |= Integer.MIN_VALUE;
        this.H = z2;
        return this;
    }

    public n t(boolean z2) {
        this.f1382b |= 1;
        this.I = z2;
        return this;
    }

    public n u(boolean z2) {
        this.f1382b |= 2;
        this.J = z2;
        return this;
    }

    public n v(boolean z2) {
        this.f1382b |= 4;
        this.K = z2;
        return this;
    }

    public n w(boolean z2) {
        this.f1382b |= 8;
        this.L = z2;
        return this;
    }

    public n x(boolean z2) {
        this.f1382b |= 16;
        this.M = z2;
        return this;
    }

    public n m(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1382b |= 32;
        this.N = str;
        return this;
    }

    public n b(int i2) {
        this.f1382b |= 64;
        this.O = i2;
        return this;
    }

    public n c(int i2) {
        this.f1382b |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.P = i2;
        return this;
    }

    public n d(int i2) {
        this.f1382b |= 256;
        this.Q = i2;
        return this;
    }

    public n y(boolean z2) {
        this.f1382b |= 512;
        this.R = z2;
        return this;
    }

    public n z(boolean z2) {
        this.f1382b |= 1024;
        this.S = z2;
        return this;
    }

    public n A(boolean z2) {
        this.f1382b |= 2048;
        this.T = z2;
        return this;
    }

    public n B(boolean z2) {
        this.f1382b |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.U = z2;
        return this;
    }

    public n C(boolean z2) {
        this.f1382b |= FragmentTransaction.TRANSIT_EXIT_MASK;
        this.V = z2;
        return this;
    }

    public n D(boolean z2) {
        this.f1382b |= 16384;
        this.W = z2;
        return this;
    }

    public n E(boolean z2) {
        this.f1382b |= 32768;
        this.X = z2;
        return this;
    }

    public n F(boolean z2) {
        this.f1382b |= Menu.CATEGORY_CONTAINER;
        this.Y = z2;
        return this;
    }

    public n G(boolean z2) {
        this.f1382b |= Menu.CATEGORY_SYSTEM;
        this.Z = z2;
        return this;
    }

    public n H(boolean z2) {
        this.f1382b |= Menu.CATEGORY_ALTERNATIVE;
        this.aa = z2;
        return this;
    }

    public n I(boolean z2) {
        this.f1382b |= 524288;
        this.ab = z2;
        return this;
    }

    public n J(boolean z2) {
        this.f1382b |= 1048576;
        this.ac = z2;
        return this;
    }

    public n K(boolean z2) {
        this.f1382b |= 2097152;
        this.ad = z2;
        return this;
    }

    public n L(boolean z2) {
        this.f1382b |= 4194304;
        this.ae = z2;
        return this;
    }

    public n M(boolean z2) {
        this.f1382b |= 8388608;
        this.af = z2;
        return this;
    }

    public n e(int i2) {
        this.f1382b |= 16777216;
        this.ag = i2;
        return this;
    }

    public n a(double d2) {
        this.f1382b |= 33554432;
        this.ah = d2;
        return this;
    }

    public n b(double d2) {
        this.f1382b |= 67108864;
        this.ai = d2;
        return this;
    }

    public n f(int i2) {
        this.f1382b |= 134217728;
        this.aj = i2;
        return this;
    }

    public n N(boolean z2) {
        this.f1382b |= 268435456;
        this.ak = z2;
        return this;
    }

    public n O(boolean z2) {
        this.f1382b |= 536870912;
        this.al = z2;
        return this;
    }

    public n P(boolean z2) {
        this.f1382b |= 1073741824;
        this.am = z2;
        return this;
    }

    public n Q(boolean z2) {
        this.f1382b |= Integer.MIN_VALUE;
        this.an = z2;
        return this;
    }
}
