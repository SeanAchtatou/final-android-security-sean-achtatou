package com.avast.b.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: AvastToWeb */
public final class at extends GeneratedMessageLite implements av {

    /* renamed from: a  reason: collision with root package name */
    private static final at f1358a = new at(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1359b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1360c;
    private byte d;
    private int e;

    private at(au auVar) {
        super(auVar);
        this.d = -1;
        this.e = -1;
    }

    private at(boolean z) {
        this.d = -1;
        this.e = -1;
    }

    public static at a() {
        return f1358a;
    }

    public boolean b() {
        return (this.f1359b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1360c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1360c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString g() {
        Object obj = this.f1360c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1360c = copyFromUtf8;
        return copyFromUtf8;
    }

    private void h() {
        this.f1360c = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.d;
        if (b2 == -1) {
            this.d = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1359b & 1) == 1) {
            codedOutputStream.writeBytes(1, g());
        }
    }

    public int getSerializedSize() {
        int i = this.e;
        if (i == -1) {
            i = 0;
            if ((this.f1359b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, g());
            }
            this.e = i;
        }
        return i;
    }

    public static au d() {
        return au.g();
    }

    /* renamed from: e */
    public au newBuilderForType() {
        return d();
    }

    public static au a(at atVar) {
        return d().mergeFrom(atVar);
    }

    /* renamed from: f */
    public au toBuilder() {
        return a(this);
    }

    static {
        f1358a.h();
    }
}
