package com.avast.b.a.a.a;

import com.google.protobuf.Internal;

/* compiled from: ATProtoGenerics */
public enum ag implements Internal.EnumLite {
    NONE1(0, 1),
    SELECTED(1, 2),
    ALL(2, 3);
    
    private static Internal.EnumLiteMap<ag> d = new ah();
    private final int e;

    public final int getNumber() {
        return this.e;
    }

    public static ag a(int i) {
        switch (i) {
            case 1:
                return NONE1;
            case 2:
                return SELECTED;
            case 3:
                return ALL;
            default:
                return null;
        }
    }

    private ag(int i, int i2) {
        this.e = i2;
    }
}
