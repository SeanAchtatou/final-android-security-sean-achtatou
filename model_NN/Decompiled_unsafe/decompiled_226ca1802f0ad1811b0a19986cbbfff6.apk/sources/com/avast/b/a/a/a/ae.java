package com.avast.b.a.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ATProtoGenerics */
public final class ae extends GeneratedMessageLite.Builder<ad, ae> implements af {

    /* renamed from: a  reason: collision with root package name */
    private int f1275a;

    /* renamed from: b  reason: collision with root package name */
    private List<j> f1276b = Collections.emptyList();

    /* renamed from: c  reason: collision with root package name */
    private List<y> f1277c = Collections.emptyList();
    private List<g> d = Collections.emptyList();
    private r e = r.a();
    private List<o> f = Collections.emptyList();
    private Object g = "";
    private int h;
    private boolean i;
    private long j;
    private Object k = "";
    private long l;
    private long m;
    private long n;

    private ae() {
        k();
    }

    private void k() {
    }

    /* access modifiers changed from: private */
    public static ae l() {
        return new ae();
    }

    /* renamed from: a */
    public ae clone() {
        return l().mergeFrom(d());
    }

    /* renamed from: b */
    public ad getDefaultInstanceForType() {
        return ad.a();
    }

    /* renamed from: c */
    public ad build() {
        ad d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.b.a.a.a.ad.a(com.avast.b.a.a.a.ad, java.util.List):java.util.List
     arg types: [com.avast.b.a.a.a.ad, java.util.List<com.avast.b.a.a.a.j>]
     candidates:
      com.avast.b.a.a.a.ad.a(com.avast.b.a.a.a.ad, int):int
      com.avast.b.a.a.a.ad.a(com.avast.b.a.a.a.ad, long):long
      com.avast.b.a.a.a.ad.a(com.avast.b.a.a.a.ad, com.avast.b.a.a.a.r):com.avast.b.a.a.a.r
      com.avast.b.a.a.a.ad.a(com.avast.b.a.a.a.ad, java.lang.Object):java.lang.Object
      com.avast.b.a.a.a.ad.a(com.avast.b.a.a.a.ad, boolean):boolean
      com.avast.b.a.a.a.ad.a(com.avast.b.a.a.a.ad, java.util.List):java.util.List */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.b.a.a.a.ad.b(com.avast.b.a.a.a.ad, java.util.List):java.util.List
     arg types: [com.avast.b.a.a.a.ad, java.util.List<com.avast.b.a.a.a.y>]
     candidates:
      com.avast.b.a.a.a.ad.b(com.avast.b.a.a.a.ad, int):int
      com.avast.b.a.a.a.ad.b(com.avast.b.a.a.a.ad, long):long
      com.avast.b.a.a.a.ad.b(com.avast.b.a.a.a.ad, java.lang.Object):java.lang.Object
      com.avast.b.a.a.a.ad.b(com.avast.b.a.a.a.ad, java.util.List):java.util.List */
    public ad d() {
        int i2 = 1;
        ad adVar = new ad(this);
        int i3 = this.f1275a;
        if ((this.f1275a & 1) == 1) {
            this.f1276b = Collections.unmodifiableList(this.f1276b);
            this.f1275a &= -2;
        }
        List unused = adVar.f1274c = (List) this.f1276b;
        if ((this.f1275a & 2) == 2) {
            this.f1277c = Collections.unmodifiableList(this.f1277c);
            this.f1275a &= -3;
        }
        List unused2 = adVar.d = (List) this.f1277c;
        if ((this.f1275a & 4) == 4) {
            this.d = Collections.unmodifiableList(this.d);
            this.f1275a &= -5;
        }
        List unused3 = adVar.e = this.d;
        if ((i3 & 8) != 8) {
            i2 = 0;
        }
        r unused4 = adVar.f = this.e;
        if ((this.f1275a & 16) == 16) {
            this.f = Collections.unmodifiableList(this.f);
            this.f1275a &= -17;
        }
        List unused5 = adVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 2;
        }
        Object unused6 = adVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 4;
        }
        int unused7 = adVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= 8;
        }
        boolean unused8 = adVar.j = this.i;
        if ((i3 & 256) == 256) {
            i2 |= 16;
        }
        long unused9 = adVar.k = this.j;
        if ((i3 & 512) == 512) {
            i2 |= 32;
        }
        Object unused10 = adVar.l = this.k;
        if ((i3 & 1024) == 1024) {
            i2 |= 64;
        }
        long unused11 = adVar.m = this.l;
        if ((i3 & 2048) == 2048) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        long unused12 = adVar.n = this.m;
        if ((i3 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i2 |= 256;
        }
        long unused13 = adVar.o = this.n;
        int unused14 = adVar.f1273b = i2;
        return adVar;
    }

    /* renamed from: a */
    public ae mergeFrom(ad adVar) {
        if (adVar != ad.a()) {
            if (!adVar.f1274c.isEmpty()) {
                if (this.f1276b.isEmpty()) {
                    this.f1276b = adVar.f1274c;
                    this.f1275a &= -2;
                } else {
                    m();
                    this.f1276b.addAll(adVar.f1274c);
                }
            }
            if (!adVar.d.isEmpty()) {
                if (this.f1277c.isEmpty()) {
                    this.f1277c = adVar.d;
                    this.f1275a &= -3;
                } else {
                    n();
                    this.f1277c.addAll(adVar.d);
                }
            }
            if (!adVar.e.isEmpty()) {
                if (this.d.isEmpty()) {
                    this.d = adVar.e;
                    this.f1275a &= -5;
                } else {
                    o();
                    this.d.addAll(adVar.e);
                }
            }
            if (adVar.e()) {
                b(adVar.f());
            }
            if (!adVar.g.isEmpty()) {
                if (this.f.isEmpty()) {
                    this.f = adVar.g;
                    this.f1275a &= -17;
                } else {
                    p();
                    this.f.addAll(adVar.g);
                }
            }
            if (adVar.g()) {
                a(adVar.h());
            }
            if (adVar.i()) {
                d(adVar.j());
            }
            if (adVar.k()) {
                a(adVar.l());
            }
            if (adVar.m()) {
                a(adVar.n());
            }
            if (adVar.o()) {
                b(adVar.p());
            }
            if (adVar.q()) {
                b(adVar.r());
            }
            if (adVar.s()) {
                c(adVar.t());
            }
            if (adVar.u()) {
                d(adVar.v());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        for (int i2 = 0; i2 < e(); i2++) {
            if (!a(i2).isInitialized()) {
                return false;
            }
        }
        for (int i3 = 0; i3 < f(); i3++) {
            if (!b(i3).isInitialized()) {
                return false;
            }
        }
        for (int i4 = 0; i4 < g(); i4++) {
            if (!c(i4).isInitialized()) {
                return false;
            }
        }
        if (!h() || i().isInitialized()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public ae mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    k p = j.p();
                    codedInputStream.readMessage(p, extensionRegistryLite);
                    a(p.d());
                    break;
                case 18:
                    z r = y.r();
                    codedInputStream.readMessage(r, extensionRegistryLite);
                    a(r.d());
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    h r2 = g.r();
                    codedInputStream.readMessage(r2, extensionRegistryLite);
                    a(r2.d());
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    s Z = r.Z();
                    if (h()) {
                        Z.mergeFrom(i());
                    }
                    codedInputStream.readMessage(Z, extensionRegistryLite);
                    a(Z.d());
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    p aj = o.aj();
                    codedInputStream.readMessage(aj, extensionRegistryLite);
                    a(aj.d());
                    break;
                case 50:
                    this.f1275a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    this.f1275a |= 64;
                    this.h = codedInputStream.readUInt32();
                    break;
                case R.styleable.SherlockTheme_activityChooserViewStyle:
                    this.f1275a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBool();
                    break;
                case 72:
                    this.f1275a |= 256;
                    this.j = codedInputStream.readInt64();
                    break;
                case 82:
                    this.f1275a |= 512;
                    this.k = codedInputStream.readBytes();
                    break;
                case 88:
                    this.f1275a |= 1024;
                    this.l = codedInputStream.readInt64();
                    break;
                case 96:
                    this.f1275a |= 2048;
                    this.m = codedInputStream.readInt64();
                    break;
                case 104:
                    this.f1275a |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.n = codedInputStream.readInt64();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    private void m() {
        if ((this.f1275a & 1) != 1) {
            this.f1276b = new ArrayList(this.f1276b);
            this.f1275a |= 1;
        }
    }

    public int e() {
        return this.f1276b.size();
    }

    public j a(int i2) {
        return this.f1276b.get(i2);
    }

    public ae a(j jVar) {
        if (jVar == null) {
            throw new NullPointerException();
        }
        m();
        this.f1276b.add(jVar);
        return this;
    }

    private void n() {
        if ((this.f1275a & 2) != 2) {
            this.f1277c = new ArrayList(this.f1277c);
            this.f1275a |= 2;
        }
    }

    public int f() {
        return this.f1277c.size();
    }

    public y b(int i2) {
        return this.f1277c.get(i2);
    }

    public ae a(y yVar) {
        if (yVar == null) {
            throw new NullPointerException();
        }
        n();
        this.f1277c.add(yVar);
        return this;
    }

    private void o() {
        if ((this.f1275a & 4) != 4) {
            this.d = new ArrayList(this.d);
            this.f1275a |= 4;
        }
    }

    public int g() {
        return this.d.size();
    }

    public g c(int i2) {
        return this.d.get(i2);
    }

    public ae a(g gVar) {
        if (gVar == null) {
            throw new NullPointerException();
        }
        o();
        this.d.add(gVar);
        return this;
    }

    public boolean h() {
        return (this.f1275a & 8) == 8;
    }

    public r i() {
        return this.e;
    }

    public ae a(r rVar) {
        if (rVar == null) {
            throw new NullPointerException();
        }
        this.e = rVar;
        this.f1275a |= 8;
        return this;
    }

    public ae b(r rVar) {
        if ((this.f1275a & 8) != 8 || this.e == r.a()) {
            this.e = rVar;
        } else {
            this.e = r.a(this.e).mergeFrom(rVar).d();
        }
        this.f1275a |= 8;
        return this;
    }

    private void p() {
        if ((this.f1275a & 16) != 16) {
            this.f = new ArrayList(this.f);
            this.f1275a |= 16;
        }
    }

    public ae a(o oVar) {
        if (oVar == null) {
            throw new NullPointerException();
        }
        p();
        this.f.add(oVar);
        return this;
    }

    public ae a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1275a |= 32;
        this.g = str;
        return this;
    }

    public ae d(int i2) {
        this.f1275a |= 64;
        this.h = i2;
        return this;
    }

    public ae a(boolean z) {
        this.f1275a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = z;
        return this;
    }

    public ae a(long j2) {
        this.f1275a |= 256;
        this.j = j2;
        return this;
    }

    public ae b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1275a |= 512;
        this.k = str;
        return this;
    }

    public ae b(long j2) {
        this.f1275a |= 1024;
        this.l = j2;
        return this;
    }

    public ae c(long j2) {
        this.f1275a |= 2048;
        this.m = j2;
        return this;
    }

    public ae d(long j2) {
        this.f1275a |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.n = j2;
        return this;
    }
}
