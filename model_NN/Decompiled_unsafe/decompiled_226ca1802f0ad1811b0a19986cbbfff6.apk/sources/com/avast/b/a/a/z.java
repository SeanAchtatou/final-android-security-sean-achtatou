package com.avast.b.a.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToWeb */
public final class z extends GeneratedMessageLite.Builder<y, z> implements ac {

    /* renamed from: a  reason: collision with root package name */
    private int f1405a;

    /* renamed from: b  reason: collision with root package name */
    private aa f1406b = aa.C2DM_SUCCESS;

    private z() {
        g();
    }

    private void g() {
    }

    /* access modifiers changed from: private */
    public static z h() {
        return new z();
    }

    /* renamed from: a */
    public z clone() {
        return h().mergeFrom(d());
    }

    /* renamed from: b */
    public y getDefaultInstanceForType() {
        return y.a();
    }

    /* renamed from: c */
    public y build() {
        y d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public y d() {
        int i = 1;
        y yVar = new y(this);
        if ((this.f1405a & 1) != 1) {
            i = 0;
        }
        aa unused = yVar.f1404c = this.f1406b;
        int unused2 = yVar.f1403b = i;
        return yVar;
    }

    /* renamed from: a */
    public z mergeFrom(y yVar) {
        if (yVar != y.a() && yVar.b()) {
            a(yVar.c());
        }
        return this;
    }

    public final boolean isInitialized() {
        if (!e()) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public z mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    aa a2 = aa.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1405a |= 1;
                        this.f1406b = a2;
                        break;
                    }
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1405a & 1) == 1;
    }

    public z a(aa aaVar) {
        if (aaVar == null) {
            throw new NullPointerException();
        }
        this.f1405a |= 1;
        this.f1406b = aaVar;
        return this;
    }
}
