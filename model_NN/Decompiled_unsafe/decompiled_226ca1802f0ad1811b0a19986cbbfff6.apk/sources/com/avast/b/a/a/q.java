package com.avast.b.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.actionbarsherlock.view.Menu;
import com.avast.b.a.a.a.ag;
import com.avast.b.a.a.a.c;
import com.avast.b.a.a.a.w;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToWeb */
public final class q extends GeneratedMessageLite.Builder<p, q> implements r {

    /* renamed from: a  reason: collision with root package name */
    private int f1387a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1388b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1389c;
    private boolean d;
    private ag e = ag.NONE1;
    private ag f = ag.NONE1;
    private ag g = ag.NONE1;
    private ag h = ag.NONE1;
    private ag i = ag.NONE1;
    private boolean j;
    private Object k = "";
    private boolean l;
    private boolean m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private c r = c.NO_RESTRICTION;
    private boolean s;
    private boolean t;
    private boolean u;
    private boolean v;
    private w w = w.NO_SIZE_RESTRICTION;
    private boolean x;

    private q() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static q g() {
        return new q();
    }

    /* renamed from: a */
    public q clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public p getDefaultInstanceForType() {
        return p.a();
    }

    /* renamed from: c */
    public p build() {
        p d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public p d() {
        p pVar = new p(this);
        int i2 = this.f1387a;
        int i3 = 0;
        if ((i2 & 1) == 1) {
            i3 = 1;
        }
        boolean unused = pVar.f1386c = this.f1388b;
        if ((i2 & 2) == 2) {
            i3 |= 2;
        }
        boolean unused2 = pVar.d = this.f1389c;
        if ((i2 & 4) == 4) {
            i3 |= 4;
        }
        boolean unused3 = pVar.e = this.d;
        if ((i2 & 8) == 8) {
            i3 |= 8;
        }
        ag unused4 = pVar.f = this.e;
        if ((i2 & 16) == 16) {
            i3 |= 16;
        }
        ag unused5 = pVar.g = this.f;
        if ((i2 & 32) == 32) {
            i3 |= 32;
        }
        ag unused6 = pVar.h = this.g;
        if ((i2 & 64) == 64) {
            i3 |= 64;
        }
        ag unused7 = pVar.i = this.h;
        if ((i2 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i3 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        ag unused8 = pVar.j = this.i;
        if ((i2 & 256) == 256) {
            i3 |= 256;
        }
        boolean unused9 = pVar.k = this.j;
        if ((i2 & 512) == 512) {
            i3 |= 512;
        }
        Object unused10 = pVar.l = this.k;
        if ((i2 & 1024) == 1024) {
            i3 |= 1024;
        }
        boolean unused11 = pVar.m = this.l;
        if ((i2 & 2048) == 2048) {
            i3 |= 2048;
        }
        boolean unused12 = pVar.n = this.m;
        if ((i2 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i3 |= FragmentTransaction.TRANSIT_ENTER_MASK;
        }
        boolean unused13 = pVar.o = this.n;
        if ((i2 & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            i3 |= FragmentTransaction.TRANSIT_EXIT_MASK;
        }
        boolean unused14 = pVar.p = this.o;
        if ((i2 & 16384) == 16384) {
            i3 |= 16384;
        }
        boolean unused15 = pVar.q = this.p;
        if ((i2 & 32768) == 32768) {
            i3 |= 32768;
        }
        boolean unused16 = pVar.r = this.q;
        if ((i2 & Menu.CATEGORY_CONTAINER) == 65536) {
            i3 |= Menu.CATEGORY_CONTAINER;
        }
        c unused17 = pVar.s = this.r;
        if ((i2 & Menu.CATEGORY_SYSTEM) == 131072) {
            i3 |= Menu.CATEGORY_SYSTEM;
        }
        boolean unused18 = pVar.t = this.s;
        if ((i2 & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            i3 |= Menu.CATEGORY_ALTERNATIVE;
        }
        boolean unused19 = pVar.u = this.t;
        if ((i2 & 524288) == 524288) {
            i3 |= 524288;
        }
        boolean unused20 = pVar.v = this.u;
        if ((1048576 & i2) == 1048576) {
            i3 |= 1048576;
        }
        boolean unused21 = pVar.w = this.v;
        if ((2097152 & i2) == 2097152) {
            i3 |= 2097152;
        }
        w unused22 = pVar.x = this.w;
        if ((i2 & 4194304) == 4194304) {
            i3 |= 4194304;
        }
        boolean unused23 = pVar.y = this.x;
        int unused24 = pVar.f1385b = i3;
        return pVar;
    }

    /* renamed from: a */
    public q mergeFrom(p pVar) {
        if (pVar != p.a()) {
            if (pVar.b()) {
                a(pVar.c());
            }
            if (pVar.d()) {
                b(pVar.e());
            }
            if (pVar.f()) {
                c(pVar.g());
            }
            if (pVar.h()) {
                a(pVar.i());
            }
            if (pVar.j()) {
                b(pVar.k());
            }
            if (pVar.l()) {
                c(pVar.m());
            }
            if (pVar.n()) {
                d(pVar.o());
            }
            if (pVar.p()) {
                e(pVar.q());
            }
            if (pVar.r()) {
                d(pVar.s());
            }
            if (pVar.t()) {
                a(pVar.u());
            }
            if (pVar.v()) {
                e(pVar.w());
            }
            if (pVar.x()) {
                f(pVar.y());
            }
            if (pVar.z()) {
                g(pVar.A());
            }
            if (pVar.B()) {
                h(pVar.C());
            }
            if (pVar.D()) {
                i(pVar.E());
            }
            if (pVar.F()) {
                j(pVar.G());
            }
            if (pVar.H()) {
                a(pVar.I());
            }
            if (pVar.J()) {
                k(pVar.K());
            }
            if (pVar.L()) {
                l(pVar.M());
            }
            if (pVar.N()) {
                m(pVar.O());
            }
            if (pVar.P()) {
                n(pVar.Q());
            }
            if (pVar.R()) {
                a(pVar.S());
            }
            if (pVar.T()) {
                o(pVar.U());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public q mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    this.f1387a |= 1;
                    this.f1388b = codedInputStream.readBool();
                    break;
                case 16:
                    this.f1387a |= 2;
                    this.f1389c = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    this.f1387a |= 4;
                    this.d = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    ag a2 = ag.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1387a |= 8;
                        this.e = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    ag a3 = ag.a(codedInputStream.readEnum());
                    if (a3 == null) {
                        break;
                    } else {
                        this.f1387a |= 16;
                        this.f = a3;
                        break;
                    }
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                    ag a4 = ag.a(codedInputStream.readEnum());
                    if (a4 == null) {
                        break;
                    } else {
                        this.f1387a |= 32;
                        this.g = a4;
                        break;
                    }
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    ag a5 = ag.a(codedInputStream.readEnum());
                    if (a5 == null) {
                        break;
                    } else {
                        this.f1387a |= 64;
                        this.h = a5;
                        break;
                    }
                case R.styleable.SherlockTheme_activityChooserViewStyle:
                    ag a6 = ag.a(codedInputStream.readEnum());
                    if (a6 == null) {
                        break;
                    } else {
                        this.f1387a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                        this.i = a6;
                        break;
                    }
                case 72:
                    this.f1387a |= 256;
                    this.j = codedInputStream.readBool();
                    break;
                case 82:
                    this.f1387a |= 512;
                    this.k = codedInputStream.readBytes();
                    break;
                case 88:
                    this.f1387a |= 1024;
                    this.l = codedInputStream.readBool();
                    break;
                case 96:
                    this.f1387a |= 2048;
                    this.m = codedInputStream.readBool();
                    break;
                case 104:
                    this.f1387a |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.n = codedInputStream.readBool();
                    break;
                case 112:
                    this.f1387a |= FragmentTransaction.TRANSIT_EXIT_MASK;
                    this.o = codedInputStream.readBool();
                    break;
                case 120:
                    this.f1387a |= 16384;
                    this.p = codedInputStream.readBool();
                    break;
                case NotificationCompat.FLAG_HIGH_PRIORITY:
                    this.f1387a |= 32768;
                    this.q = codedInputStream.readBool();
                    break;
                case 136:
                    c a7 = c.a(codedInputStream.readEnum());
                    if (a7 == null) {
                        break;
                    } else {
                        this.f1387a |= Menu.CATEGORY_CONTAINER;
                        this.r = a7;
                        break;
                    }
                case 144:
                    this.f1387a |= Menu.CATEGORY_SYSTEM;
                    this.s = codedInputStream.readBool();
                    break;
                case 152:
                    this.f1387a |= Menu.CATEGORY_ALTERNATIVE;
                    this.t = codedInputStream.readBool();
                    break;
                case 160:
                    this.f1387a |= 524288;
                    this.u = codedInputStream.readBool();
                    break;
                case 168:
                    this.f1387a |= 1048576;
                    this.v = codedInputStream.readBool();
                    break;
                case 176:
                    w a8 = w.a(codedInputStream.readEnum());
                    if (a8 == null) {
                        break;
                    } else {
                        this.f1387a |= 2097152;
                        this.w = a8;
                        break;
                    }
                case 184:
                    this.f1387a |= 4194304;
                    this.x = codedInputStream.readBool();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public q a(boolean z) {
        this.f1387a |= 1;
        this.f1388b = z;
        return this;
    }

    public q b(boolean z) {
        this.f1387a |= 2;
        this.f1389c = z;
        return this;
    }

    public q c(boolean z) {
        this.f1387a |= 4;
        this.d = z;
        return this;
    }

    public q a(ag agVar) {
        if (agVar == null) {
            throw new NullPointerException();
        }
        this.f1387a |= 8;
        this.e = agVar;
        return this;
    }

    public q b(ag agVar) {
        if (agVar == null) {
            throw new NullPointerException();
        }
        this.f1387a |= 16;
        this.f = agVar;
        return this;
    }

    public q c(ag agVar) {
        if (agVar == null) {
            throw new NullPointerException();
        }
        this.f1387a |= 32;
        this.g = agVar;
        return this;
    }

    public q d(ag agVar) {
        if (agVar == null) {
            throw new NullPointerException();
        }
        this.f1387a |= 64;
        this.h = agVar;
        return this;
    }

    public q e(ag agVar) {
        if (agVar == null) {
            throw new NullPointerException();
        }
        this.f1387a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = agVar;
        return this;
    }

    public q d(boolean z) {
        this.f1387a |= 256;
        this.j = z;
        return this;
    }

    public q a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1387a |= 512;
        this.k = str;
        return this;
    }

    public q e(boolean z) {
        this.f1387a |= 1024;
        this.l = z;
        return this;
    }

    public q f(boolean z) {
        this.f1387a |= 2048;
        this.m = z;
        return this;
    }

    public q g(boolean z) {
        this.f1387a |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.n = z;
        return this;
    }

    public q h(boolean z) {
        this.f1387a |= FragmentTransaction.TRANSIT_EXIT_MASK;
        this.o = z;
        return this;
    }

    public q i(boolean z) {
        this.f1387a |= 16384;
        this.p = z;
        return this;
    }

    public q j(boolean z) {
        this.f1387a |= 32768;
        this.q = z;
        return this;
    }

    public q a(c cVar) {
        if (cVar == null) {
            throw new NullPointerException();
        }
        this.f1387a |= Menu.CATEGORY_CONTAINER;
        this.r = cVar;
        return this;
    }

    public q k(boolean z) {
        this.f1387a |= Menu.CATEGORY_SYSTEM;
        this.s = z;
        return this;
    }

    public q l(boolean z) {
        this.f1387a |= Menu.CATEGORY_ALTERNATIVE;
        this.t = z;
        return this;
    }

    public q m(boolean z) {
        this.f1387a |= 524288;
        this.u = z;
        return this;
    }

    public q n(boolean z) {
        this.f1387a |= 1048576;
        this.v = z;
        return this;
    }

    public q a(w wVar) {
        if (wVar == null) {
            throw new NullPointerException();
        }
        this.f1387a |= 2097152;
        this.w = wVar;
        return this;
    }

    public q o(boolean z) {
        this.f1387a |= 4194304;
        this.x = z;
        return this;
    }
}
