package com.avast.b.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.actionbarsherlock.view.Menu;
import com.avast.b.a.a.a.ad;
import com.avast.b.a.a.a.ae;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToWeb */
public final class ak extends GeneratedMessageLite.ExtendableBuilder<aj, ak> implements al {
    private int A;
    private Object B = "";
    private boolean C;
    private Object D = "";
    private at E = at.a();

    /* renamed from: a  reason: collision with root package name */
    private int f1343a;

    /* renamed from: b  reason: collision with root package name */
    private int f1344b;

    /* renamed from: c  reason: collision with root package name */
    private Object f1345c = "";
    private Object d = "";
    private Object e = "";
    private Object f = "";
    private Object g = "";
    private Object h = "";
    private Object i = "";
    private Object j = "";
    private boolean k;
    private Object l = "";
    private Object m = "";
    private Object n = "";
    private int o;
    private int p;
    private s q = s.a();
    private y r = y.a();
    private ad s = ad.a();
    private am t = am.a();
    private m u = m.a();
    private ag v = ag.a();
    private boolean w;
    private Object x = "";
    private p y = p.a();
    private ad z = ad.a();

    private ak() {
        A();
    }

    private void A() {
    }

    /* access modifiers changed from: private */
    public static ak B() {
        return new ak();
    }

    /* renamed from: a */
    public ak clone() {
        return B().mergeFrom(d());
    }

    /* renamed from: b */
    public aj getDefaultInstanceForType() {
        return aj.a();
    }

    /* renamed from: c */
    public aj build() {
        aj d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public aj d() {
        aj ajVar = new aj(this);
        int i2 = this.f1343a;
        int i3 = 0;
        if ((i2 & 1) == 1) {
            i3 = 1;
        }
        int unused = ajVar.f1342c = this.f1344b;
        if ((i2 & 2) == 2) {
            i3 |= 2;
        }
        Object unused2 = ajVar.d = this.f1345c;
        if ((i2 & 4) == 4) {
            i3 |= 4;
        }
        Object unused3 = ajVar.e = this.d;
        if ((i2 & 8) == 8) {
            i3 |= 8;
        }
        Object unused4 = ajVar.f = this.e;
        if ((i2 & 16) == 16) {
            i3 |= 16;
        }
        Object unused5 = ajVar.g = this.f;
        if ((i2 & 32) == 32) {
            i3 |= 32;
        }
        Object unused6 = ajVar.h = this.g;
        if ((i2 & 64) == 64) {
            i3 |= 64;
        }
        Object unused7 = ajVar.i = this.h;
        if ((i2 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i3 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        Object unused8 = ajVar.j = this.i;
        if ((i2 & 256) == 256) {
            i3 |= 256;
        }
        Object unused9 = ajVar.k = this.j;
        if ((i2 & 512) == 512) {
            i3 |= 512;
        }
        boolean unused10 = ajVar.l = this.k;
        if ((i2 & 1024) == 1024) {
            i3 |= 1024;
        }
        Object unused11 = ajVar.m = this.l;
        if ((i2 & 2048) == 2048) {
            i3 |= 2048;
        }
        Object unused12 = ajVar.n = this.m;
        if ((i2 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i3 |= FragmentTransaction.TRANSIT_ENTER_MASK;
        }
        Object unused13 = ajVar.o = this.n;
        if ((i2 & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            i3 |= FragmentTransaction.TRANSIT_EXIT_MASK;
        }
        int unused14 = ajVar.p = this.o;
        if ((i2 & 16384) == 16384) {
            i3 |= 16384;
        }
        int unused15 = ajVar.q = this.p;
        if ((i2 & 32768) == 32768) {
            i3 |= 32768;
        }
        s unused16 = ajVar.r = this.q;
        if ((i2 & Menu.CATEGORY_CONTAINER) == 65536) {
            i3 |= Menu.CATEGORY_CONTAINER;
        }
        y unused17 = ajVar.s = this.r;
        if ((i2 & Menu.CATEGORY_SYSTEM) == 131072) {
            i3 |= Menu.CATEGORY_SYSTEM;
        }
        ad unused18 = ajVar.t = this.s;
        if ((i2 & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            i3 |= Menu.CATEGORY_ALTERNATIVE;
        }
        am unused19 = ajVar.u = this.t;
        if ((i2 & 524288) == 524288) {
            i3 |= 524288;
        }
        m unused20 = ajVar.v = this.u;
        if ((1048576 & i2) == 1048576) {
            i3 |= 1048576;
        }
        ag unused21 = ajVar.w = this.v;
        if ((2097152 & i2) == 2097152) {
            i3 |= 2097152;
        }
        boolean unused22 = ajVar.x = this.w;
        if ((4194304 & i2) == 4194304) {
            i3 |= 4194304;
        }
        Object unused23 = ajVar.y = this.x;
        if ((8388608 & i2) == 8388608) {
            i3 |= 8388608;
        }
        p unused24 = ajVar.z = this.y;
        if ((16777216 & i2) == 16777216) {
            i3 |= 16777216;
        }
        ad unused25 = ajVar.A = this.z;
        if ((33554432 & i2) == 33554432) {
            i3 |= 33554432;
        }
        int unused26 = ajVar.B = this.A;
        if ((67108864 & i2) == 67108864) {
            i3 |= 67108864;
        }
        Object unused27 = ajVar.C = this.B;
        if ((134217728 & i2) == 134217728) {
            i3 |= 134217728;
        }
        boolean unused28 = ajVar.D = this.C;
        if ((268435456 & i2) == 268435456) {
            i3 |= 268435456;
        }
        Object unused29 = ajVar.E = this.D;
        if ((i2 & 536870912) == 536870912) {
            i3 |= 536870912;
        }
        at unused30 = ajVar.F = this.E;
        int unused31 = ajVar.f1341b = i3;
        return ajVar;
    }

    /* renamed from: a */
    public ak mergeFrom(aj ajVar) {
        if (ajVar != aj.a()) {
            if (ajVar.b()) {
                a(ajVar.c());
            }
            if (ajVar.d()) {
                a(ajVar.e());
            }
            if (ajVar.f()) {
                b(ajVar.g());
            }
            if (ajVar.h()) {
                c(ajVar.i());
            }
            if (ajVar.j()) {
                d(ajVar.k());
            }
            if (ajVar.l()) {
                e(ajVar.m());
            }
            if (ajVar.n()) {
                f(ajVar.o());
            }
            if (ajVar.p()) {
                g(ajVar.q());
            }
            if (ajVar.r()) {
                h(ajVar.s());
            }
            if (ajVar.t()) {
                a(ajVar.u());
            }
            if (ajVar.v()) {
                i(ajVar.w());
            }
            if (ajVar.x()) {
                j(ajVar.y());
            }
            if (ajVar.z()) {
                k(ajVar.A());
            }
            if (ajVar.B()) {
                b(ajVar.C());
            }
            if (ajVar.D()) {
                c(ajVar.E());
            }
            if (ajVar.F()) {
                b(ajVar.G());
            }
            if (ajVar.H()) {
                b(ajVar.I());
            }
            if (ajVar.J()) {
                b(ajVar.K());
            }
            if (ajVar.L()) {
                b(ajVar.M());
            }
            if (ajVar.N()) {
                b(ajVar.O());
            }
            if (ajVar.P()) {
                b(ajVar.Q());
            }
            if (ajVar.R()) {
                b(ajVar.S());
            }
            if (ajVar.T()) {
                l(ajVar.U());
            }
            if (ajVar.V()) {
                b(ajVar.W());
            }
            if (ajVar.X()) {
                b(ajVar.Y());
            }
            if (ajVar.Z()) {
                d(ajVar.aa());
            }
            if (ajVar.ab()) {
                m(ajVar.ac());
            }
            if (ajVar.ad()) {
                c(ajVar.ae());
            }
            if (ajVar.af()) {
                n(ajVar.ag());
            }
            if (ajVar.ah()) {
                b(ajVar.ai());
            }
            mergeExtensionFields(ajVar);
        }
        return this;
    }

    public final boolean isInitialized() {
        if (!e() || !f() || !g()) {
            return false;
        }
        if (h() && !i().isInitialized()) {
            return false;
        }
        if (j() && !k().isInitialized()) {
            return false;
        }
        if (l() && !m().isInitialized()) {
            return false;
        }
        if (n() && !o().isInitialized()) {
            return false;
        }
        if ((!v() || w().isInitialized()) && extensionsAreInitialized()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public ak mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    this.f1343a |= 1;
                    this.f1344b = codedInputStream.readInt32();
                    break;
                case 18:
                    this.f1343a |= 2;
                    this.f1345c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1343a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f1343a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f1343a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                case 50:
                    this.f1343a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    this.f1343a |= 64;
                    this.h = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f1343a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                case 74:
                    this.f1343a |= 256;
                    this.j = codedInputStream.readBytes();
                    break;
                case 80:
                    this.f1343a |= 512;
                    this.k = codedInputStream.readBool();
                    break;
                case 90:
                    this.f1343a |= 1024;
                    this.l = codedInputStream.readBytes();
                    break;
                case 98:
                    this.f1343a |= 2048;
                    this.m = codedInputStream.readBytes();
                    break;
                case 106:
                    this.f1343a |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.n = codedInputStream.readBytes();
                    break;
                case 112:
                    this.f1343a |= FragmentTransaction.TRANSIT_EXIT_MASK;
                    this.o = codedInputStream.readInt32();
                    break;
                case 120:
                    this.f1343a |= 16384;
                    this.p = codedInputStream.readInt32();
                    break;
                case 130:
                    t l2 = s.l();
                    if (h()) {
                        l2.mergeFrom(i());
                    }
                    codedInputStream.readMessage(l2, extensionRegistryLite);
                    a(l2.d());
                    break;
                case 138:
                    z d2 = y.d();
                    if (j()) {
                        d2.mergeFrom(k());
                    }
                    codedInputStream.readMessage(d2, extensionRegistryLite);
                    a(d2.d());
                    break;
                case 146:
                    ae w2 = ad.w();
                    if (l()) {
                        w2.mergeFrom(m());
                    }
                    codedInputStream.readMessage(w2, extensionRegistryLite);
                    a(w2.d());
                    break;
                case 154:
                    an t2 = am.t();
                    if (n()) {
                        t2.mergeFrom(o());
                    }
                    codedInputStream.readMessage(t2, extensionRegistryLite);
                    a(t2.d());
                    break;
                case 162:
                    n bz = m.bz();
                    if (p()) {
                        bz.mergeFrom(q());
                    }
                    codedInputStream.readMessage(bz, extensionRegistryLite);
                    a(bz.d());
                    break;
                case 170:
                    ah d3 = ag.d();
                    if (r()) {
                        d3.mergeFrom(s());
                    }
                    codedInputStream.readMessage(d3, extensionRegistryLite);
                    a(d3.d());
                    break;
                case 176:
                    this.f1343a |= 2097152;
                    this.w = codedInputStream.readBool();
                    break;
                case 1602:
                    this.f1343a |= 4194304;
                    this.x = codedInputStream.readBytes();
                    break;
                case 1610:
                    q V = p.V();
                    if (t()) {
                        V.mergeFrom(u());
                    }
                    codedInputStream.readMessage(V, extensionRegistryLite);
                    a(V.d());
                    break;
                case 1618:
                    ae l3 = ad.l();
                    if (v()) {
                        l3.mergeFrom(w());
                    }
                    codedInputStream.readMessage(l3, extensionRegistryLite);
                    a(l3.d());
                    break;
                case 1624:
                    this.f1343a |= 33554432;
                    this.A = codedInputStream.readInt32();
                    break;
                case 1634:
                    this.f1343a |= 67108864;
                    this.B = codedInputStream.readBytes();
                    break;
                case 1640:
                    this.f1343a |= 134217728;
                    this.C = codedInputStream.readBool();
                    break;
                case 1650:
                    this.f1343a |= 268435456;
                    this.D = codedInputStream.readBytes();
                    break;
                case 1658:
                    au d4 = at.d();
                    if (x()) {
                        d4.mergeFrom(y());
                    }
                    codedInputStream.readMessage(d4, extensionRegistryLite);
                    a(d4.d());
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1343a & 1) == 1;
    }

    public ak a(int i2) {
        this.f1343a |= 1;
        this.f1344b = i2;
        return this;
    }

    public ak a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 2;
        this.f1345c = str;
        return this;
    }

    public ak b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 4;
        this.d = str;
        return this;
    }

    public boolean f() {
        return (this.f1343a & 8) == 8;
    }

    public ak c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 8;
        this.e = str;
        return this;
    }

    public ak d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 16;
        this.f = str;
        return this;
    }

    public boolean g() {
        return (this.f1343a & 32) == 32;
    }

    public ak e(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 32;
        this.g = str;
        return this;
    }

    public ak f(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 64;
        this.h = str;
        return this;
    }

    public ak g(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = str;
        return this;
    }

    public ak h(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 256;
        this.j = str;
        return this;
    }

    public ak a(boolean z2) {
        this.f1343a |= 512;
        this.k = z2;
        return this;
    }

    public ak i(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 1024;
        this.l = str;
        return this;
    }

    public ak j(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 2048;
        this.m = str;
        return this;
    }

    public ak k(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.n = str;
        return this;
    }

    public ak b(int i2) {
        this.f1343a |= FragmentTransaction.TRANSIT_EXIT_MASK;
        this.o = i2;
        return this;
    }

    public ak c(int i2) {
        this.f1343a |= 16384;
        this.p = i2;
        return this;
    }

    public boolean h() {
        return (this.f1343a & 32768) == 32768;
    }

    public s i() {
        return this.q;
    }

    public ak a(s sVar) {
        if (sVar == null) {
            throw new NullPointerException();
        }
        this.q = sVar;
        this.f1343a |= 32768;
        return this;
    }

    public ak b(s sVar) {
        if ((this.f1343a & 32768) != 32768 || this.q == s.a()) {
            this.q = sVar;
        } else {
            this.q = s.a(this.q).mergeFrom(sVar).d();
        }
        this.f1343a |= 32768;
        return this;
    }

    public boolean j() {
        return (this.f1343a & Menu.CATEGORY_CONTAINER) == 65536;
    }

    public y k() {
        return this.r;
    }

    public ak a(y yVar) {
        if (yVar == null) {
            throw new NullPointerException();
        }
        this.r = yVar;
        this.f1343a |= Menu.CATEGORY_CONTAINER;
        return this;
    }

    public ak b(y yVar) {
        if ((this.f1343a & Menu.CATEGORY_CONTAINER) != 65536 || this.r == y.a()) {
            this.r = yVar;
        } else {
            this.r = y.a(this.r).mergeFrom(yVar).d();
        }
        this.f1343a |= Menu.CATEGORY_CONTAINER;
        return this;
    }

    public boolean l() {
        return (this.f1343a & Menu.CATEGORY_SYSTEM) == 131072;
    }

    public ad m() {
        return this.s;
    }

    public ak a(ad adVar) {
        if (adVar == null) {
            throw new NullPointerException();
        }
        this.s = adVar;
        this.f1343a |= Menu.CATEGORY_SYSTEM;
        return this;
    }

    public ak b(ad adVar) {
        if ((this.f1343a & Menu.CATEGORY_SYSTEM) != 131072 || this.s == ad.a()) {
            this.s = adVar;
        } else {
            this.s = ad.a(this.s).mergeFrom(adVar).d();
        }
        this.f1343a |= Menu.CATEGORY_SYSTEM;
        return this;
    }

    public boolean n() {
        return (this.f1343a & Menu.CATEGORY_ALTERNATIVE) == 262144;
    }

    public am o() {
        return this.t;
    }

    public ak a(am amVar) {
        if (amVar == null) {
            throw new NullPointerException();
        }
        this.t = amVar;
        this.f1343a |= Menu.CATEGORY_ALTERNATIVE;
        return this;
    }

    public ak b(am amVar) {
        if ((this.f1343a & Menu.CATEGORY_ALTERNATIVE) != 262144 || this.t == am.a()) {
            this.t = amVar;
        } else {
            this.t = am.a(this.t).mergeFrom(amVar).d();
        }
        this.f1343a |= Menu.CATEGORY_ALTERNATIVE;
        return this;
    }

    public boolean p() {
        return (this.f1343a & 524288) == 524288;
    }

    public m q() {
        return this.u;
    }

    public ak a(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.u = mVar;
        this.f1343a |= 524288;
        return this;
    }

    public ak b(m mVar) {
        if ((this.f1343a & 524288) != 524288 || this.u == m.a()) {
            this.u = mVar;
        } else {
            this.u = m.a(this.u).mergeFrom(mVar).d();
        }
        this.f1343a |= 524288;
        return this;
    }

    public boolean r() {
        return (this.f1343a & 1048576) == 1048576;
    }

    public ag s() {
        return this.v;
    }

    public ak a(ag agVar) {
        if (agVar == null) {
            throw new NullPointerException();
        }
        this.v = agVar;
        this.f1343a |= 1048576;
        return this;
    }

    public ak b(ag agVar) {
        if ((this.f1343a & 1048576) != 1048576 || this.v == ag.a()) {
            this.v = agVar;
        } else {
            this.v = ag.a(this.v).mergeFrom(agVar).d();
        }
        this.f1343a |= 1048576;
        return this;
    }

    public ak b(boolean z2) {
        this.f1343a |= 2097152;
        this.w = z2;
        return this;
    }

    public ak l(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 4194304;
        this.x = str;
        return this;
    }

    public boolean t() {
        return (this.f1343a & 8388608) == 8388608;
    }

    public p u() {
        return this.y;
    }

    public ak a(p pVar) {
        if (pVar == null) {
            throw new NullPointerException();
        }
        this.y = pVar;
        this.f1343a |= 8388608;
        return this;
    }

    public ak b(p pVar) {
        if ((this.f1343a & 8388608) != 8388608 || this.y == p.a()) {
            this.y = pVar;
        } else {
            this.y = p.a(this.y).mergeFrom(pVar).d();
        }
        this.f1343a |= 8388608;
        return this;
    }

    public boolean v() {
        return (this.f1343a & 16777216) == 16777216;
    }

    public ad w() {
        return this.z;
    }

    public ak a(ad adVar) {
        if (adVar == null) {
            throw new NullPointerException();
        }
        this.z = adVar;
        this.f1343a |= 16777216;
        return this;
    }

    public ak b(ad adVar) {
        if ((this.f1343a & 16777216) != 16777216 || this.z == ad.a()) {
            this.z = adVar;
        } else {
            this.z = ad.a(this.z).mergeFrom(adVar).d();
        }
        this.f1343a |= 16777216;
        return this;
    }

    public ak d(int i2) {
        this.f1343a |= 33554432;
        this.A = i2;
        return this;
    }

    public ak m(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 67108864;
        this.B = str;
        return this;
    }

    public ak c(boolean z2) {
        this.f1343a |= 134217728;
        this.C = z2;
        return this;
    }

    public ak n(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1343a |= 268435456;
        this.D = str;
        return this;
    }

    public boolean x() {
        return (this.f1343a & 536870912) == 536870912;
    }

    public at y() {
        return this.E;
    }

    public ak a(at atVar) {
        if (atVar == null) {
            throw new NullPointerException();
        }
        this.E = atVar;
        this.f1343a |= 536870912;
        return this;
    }

    public ak b(at atVar) {
        if ((this.f1343a & 536870912) != 536870912 || this.E == at.a()) {
            this.E = atVar;
        } else {
            this.E = at.a(this.E).mergeFrom(atVar).d();
        }
        this.f1343a |= 536870912;
        return this;
    }
}
