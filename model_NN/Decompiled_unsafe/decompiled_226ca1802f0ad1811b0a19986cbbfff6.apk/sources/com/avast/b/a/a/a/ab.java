package com.avast.b.a.a.a;

import com.google.protobuf.Internal;

/* compiled from: ATProtoGenerics */
public enum ab implements Internal.EnumLite {
    INTERNAL(0, 0),
    GOOGLE_DRIVE(1, 1);
    

    /* renamed from: c  reason: collision with root package name */
    private static Internal.EnumLiteMap<ab> f1271c = new ac();
    private final int d;

    public final int getNumber() {
        return this.d;
    }

    public static ab a(int i) {
        switch (i) {
            case 0:
                return INTERNAL;
            case 1:
                return GOOGLE_DRIVE;
            default:
                return null;
        }
    }

    private ab(int i, int i2) {
        this.d = i2;
    }
}
