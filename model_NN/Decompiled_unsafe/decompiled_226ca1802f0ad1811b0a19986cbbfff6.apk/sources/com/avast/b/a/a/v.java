package com.avast.b.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: AvastToWeb */
public final class v extends GeneratedMessageLite implements x {

    /* renamed from: a  reason: collision with root package name */
    private static final v f1396a = new v(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1397b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public int f1398c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public Object e;
    private byte f;
    private int g;

    private v(w wVar) {
        super(wVar);
        this.f = -1;
        this.g = -1;
    }

    private v(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static v a() {
        return f1396a;
    }

    public boolean b() {
        return (this.f1397b & 1) == 1;
    }

    public int c() {
        return this.f1398c;
    }

    public boolean d() {
        return (this.f1397b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString k() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1397b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString l() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    private void m() {
        this.f1398c = 0;
        this.d = "";
        this.e = "";
    }

    public final boolean isInitialized() {
        boolean z = true;
        byte b2 = this.f;
        if (b2 != -1) {
            if (b2 != 1) {
                z = false;
            }
            return z;
        } else if (!b()) {
            this.f = 0;
            return false;
        } else if (!d()) {
            this.f = 0;
            return false;
        } else if (!f()) {
            this.f = 0;
            return false;
        } else {
            this.f = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1397b & 1) == 1) {
            codedOutputStream.writeInt32(1, this.f1398c);
        }
        if ((this.f1397b & 2) == 2) {
            codedOutputStream.writeBytes(2, k());
        }
        if ((this.f1397b & 4) == 4) {
            codedOutputStream.writeBytes(3, l());
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            if ((this.f1397b & 1) == 1) {
                i = 0 + CodedOutputStream.computeInt32Size(1, this.f1398c);
            }
            if ((this.f1397b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, k());
            }
            if ((this.f1397b & 4) == 4) {
                i += CodedOutputStream.computeBytesSize(3, l());
            }
            this.g = i;
        }
        return i;
    }

    public static w h() {
        return w.j();
    }

    /* renamed from: i */
    public w newBuilderForType() {
        return h();
    }

    public static w a(v vVar) {
        return h().mergeFrom(vVar);
    }

    /* renamed from: j */
    public w toBuilder() {
        return a(this);
    }

    static {
        f1396a.m();
    }
}
