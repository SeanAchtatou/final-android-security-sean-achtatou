package com.avast.b.a.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.LazyStringArrayList;
import com.google.protobuf.LazyStringList;
import java.util.List;

/* compiled from: ATProtoGenerics */
public final class j extends GeneratedMessageLite implements l {

    /* renamed from: a  reason: collision with root package name */
    private static final j f1293a = new j(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1294b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public long f1295c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public LazyStringList e;
    /* access modifiers changed from: private */
    public LazyStringList f;
    /* access modifiers changed from: private */
    public long g;
    /* access modifiers changed from: private */
    public Object h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public ByteString j;
    private byte k;
    private int l;

    private j(k kVar) {
        super(kVar);
        this.k = -1;
        this.l = -1;
    }

    private j(boolean z) {
        this.k = -1;
        this.l = -1;
    }

    public static j a() {
        return f1293a;
    }

    public boolean b() {
        return (this.f1294b & 1) == 1;
    }

    public long c() {
        return this.f1295c;
    }

    public boolean d() {
        return (this.f1294b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString s() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public List<String> f() {
        return this.e;
    }

    public List<String> g() {
        return this.f;
    }

    public boolean h() {
        return (this.f1294b & 4) == 4;
    }

    public long i() {
        return this.g;
    }

    public boolean j() {
        return (this.f1294b & 8) == 8;
    }

    public String k() {
        Object obj = this.h;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.h = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString t() {
        Object obj = this.h;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.h = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean l() {
        return (this.f1294b & 16) == 16;
    }

    public int m() {
        return this.i;
    }

    public boolean n() {
        return (this.f1294b & 32) == 32;
    }

    public ByteString o() {
        return this.j;
    }

    private void u() {
        this.f1295c = 0;
        this.d = "";
        this.e = LazyStringArrayList.EMPTY;
        this.f = LazyStringArrayList.EMPTY;
        this.g = 0;
        this.h = "";
        this.i = 0;
        this.j = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.k;
        if (b2 != -1) {
            if (b2 == 1) {
                return true;
            }
            return false;
        } else if (!b()) {
            this.k = 0;
            return false;
        } else {
            this.k = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1294b & 1) == 1) {
            codedOutputStream.writeInt64(1, this.f1295c);
        }
        if ((this.f1294b & 2) == 2) {
            codedOutputStream.writeBytes(2, s());
        }
        for (int i2 = 0; i2 < this.e.size(); i2++) {
            codedOutputStream.writeBytes(3, this.e.getByteString(i2));
        }
        for (int i3 = 0; i3 < this.f.size(); i3++) {
            codedOutputStream.writeBytes(4, this.f.getByteString(i3));
        }
        if ((this.f1294b & 4) == 4) {
            codedOutputStream.writeInt64(5, this.g);
        }
        if ((this.f1294b & 8) == 8) {
            codedOutputStream.writeBytes(6, t());
        }
        if ((this.f1294b & 16) == 16) {
            codedOutputStream.writeInt32(7, this.i);
        }
        if ((this.f1294b & 32) == 32) {
            codedOutputStream.writeBytes(8, this.j);
        }
    }

    public int getSerializedSize() {
        int i2;
        int i3 = this.l;
        if (i3 == -1) {
            if ((this.f1294b & 1) == 1) {
                i2 = CodedOutputStream.computeInt64Size(1, this.f1295c) + 0;
            } else {
                i2 = 0;
            }
            if ((this.f1294b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, s());
            }
            int i4 = 0;
            for (int i5 = 0; i5 < this.e.size(); i5++) {
                i4 += CodedOutputStream.computeBytesSizeNoTag(this.e.getByteString(i5));
            }
            int size = (f().size() * 1) + i2 + i4;
            int i6 = 0;
            for (int i7 = 0; i7 < this.f.size(); i7++) {
                i6 += CodedOutputStream.computeBytesSizeNoTag(this.f.getByteString(i7));
            }
            i3 = i6 + size + (g().size() * 1);
            if ((this.f1294b & 4) == 4) {
                i3 += CodedOutputStream.computeInt64Size(5, this.g);
            }
            if ((this.f1294b & 8) == 8) {
                i3 += CodedOutputStream.computeBytesSize(6, t());
            }
            if ((this.f1294b & 16) == 16) {
                i3 += CodedOutputStream.computeInt32Size(7, this.i);
            }
            if ((this.f1294b & 32) == 32) {
                i3 += CodedOutputStream.computeBytesSize(8, this.j);
            }
            this.l = i3;
        }
        return i3;
    }

    public static k p() {
        return k.h();
    }

    /* renamed from: q */
    public k newBuilderForType() {
        return p();
    }

    public static k a(j jVar) {
        return p().mergeFrom(jVar);
    }

    /* renamed from: r */
    public k toBuilder() {
        return a(this);
    }

    static {
        f1293a.u();
    }
}
