package com.avast.b.a.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToWeb */
public final class ae extends GeneratedMessageLite.Builder<ad, ae> implements af {

    /* renamed from: a  reason: collision with root package name */
    private int f1332a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1333b = "";

    /* renamed from: c  reason: collision with root package name */
    private Object f1334c = "";
    private int d;
    private Object e = "";
    private Object f = "";

    private ae() {
        k();
    }

    private void k() {
    }

    /* access modifiers changed from: private */
    public static ae l() {
        return new ae();
    }

    /* renamed from: a */
    public ae clone() {
        return l().mergeFrom(d());
    }

    /* renamed from: b */
    public ad getDefaultInstanceForType() {
        return ad.a();
    }

    /* renamed from: c */
    public ad build() {
        ad d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public ad d() {
        int i = 1;
        ad adVar = new ad(this);
        int i2 = this.f1332a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        Object unused = adVar.f1331c = this.f1333b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        Object unused2 = adVar.d = this.f1334c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        int unused3 = adVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 8;
        }
        Object unused4 = adVar.f = this.e;
        if ((i2 & 16) == 16) {
            i |= 16;
        }
        Object unused5 = adVar.g = this.f;
        int unused6 = adVar.f1330b = i;
        return adVar;
    }

    /* renamed from: a */
    public ae mergeFrom(ad adVar) {
        if (adVar != ad.a()) {
            if (adVar.b()) {
                a(adVar.c());
            }
            if (adVar.d()) {
                b(adVar.e());
            }
            if (adVar.f()) {
                a(adVar.g());
            }
            if (adVar.h()) {
                c(adVar.i());
            }
            if (adVar.j()) {
                d(adVar.k());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (e() && f() && g() && h() && i()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public ae mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1332a |= 1;
                    this.f1333b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1332a |= 2;
                    this.f1334c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    this.f1332a |= 4;
                    this.d = codedInputStream.readInt32();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f1332a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f1332a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1332a & 1) == 1;
    }

    public ae a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1332a |= 1;
        this.f1333b = str;
        return this;
    }

    public boolean f() {
        return (this.f1332a & 2) == 2;
    }

    public ae b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1332a |= 2;
        this.f1334c = str;
        return this;
    }

    public boolean g() {
        return (this.f1332a & 4) == 4;
    }

    public ae a(int i) {
        this.f1332a |= 4;
        this.d = i;
        return this;
    }

    public boolean h() {
        return (this.f1332a & 8) == 8;
    }

    public ae c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1332a |= 8;
        this.e = str;
        return this;
    }

    public boolean i() {
        return (this.f1332a & 16) == 16;
    }

    public ae d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1332a |= 16;
        this.f = str;
        return this;
    }
}
