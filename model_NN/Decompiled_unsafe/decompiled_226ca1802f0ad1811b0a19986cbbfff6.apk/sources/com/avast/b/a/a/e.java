package com.avast.b.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: AvastToDevice */
public final class e extends GeneratedMessageLite implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final e f1366a = new e(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1367b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public c f1368c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public Object g;
    private byte h;
    private int i;

    private e(f fVar) {
        super(fVar);
        this.h = -1;
        this.i = -1;
    }

    private e(boolean z) {
        this.h = -1;
        this.i = -1;
    }

    public static e a() {
        return f1366a;
    }

    public boolean b() {
        return (this.f1367b & 1) == 1;
    }

    public c c() {
        return this.f1368c;
    }

    public boolean d() {
        return (this.f1367b & 2) == 2;
    }

    public int e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1367b & 4) == 4;
    }

    public boolean g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1367b & 8) == 8;
    }

    public boolean i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1367b & 16) == 16;
    }

    public String k() {
        Object obj = this.g;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.g = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString o() {
        Object obj = this.g;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.g = copyFromUtf8;
        return copyFromUtf8;
    }

    private void p() {
        this.f1368c = c.NONE;
        this.d = 0;
        this.e = false;
        this.f = false;
        this.g = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.h;
        if (b2 == -1) {
            this.h = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1367b & 1) == 1) {
            codedOutputStream.writeEnum(1, this.f1368c.getNumber());
        }
        if ((this.f1367b & 2) == 2) {
            codedOutputStream.writeInt32(2, this.d);
        }
        if ((this.f1367b & 4) == 4) {
            codedOutputStream.writeBool(3, this.e);
        }
        if ((this.f1367b & 8) == 8) {
            codedOutputStream.writeBool(4, this.f);
        }
        if ((this.f1367b & 16) == 16) {
            codedOutputStream.writeBytes(5, o());
        }
    }

    public int getSerializedSize() {
        int i2 = this.i;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1367b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeEnumSize(1, this.f1368c.getNumber());
            }
            if ((this.f1367b & 2) == 2) {
                i2 += CodedOutputStream.computeInt32Size(2, this.d);
            }
            if ((this.f1367b & 4) == 4) {
                i2 += CodedOutputStream.computeBoolSize(3, this.e);
            }
            if ((this.f1367b & 8) == 8) {
                i2 += CodedOutputStream.computeBoolSize(4, this.f);
            }
            if ((this.f1367b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(5, o());
            }
            this.i = i2;
        }
        return i2;
    }

    public static e a(byte[] bArr) {
        return ((f) l().mergeFrom(bArr)).h();
    }

    public static f l() {
        return f.g();
    }

    /* renamed from: m */
    public f newBuilderForType() {
        return l();
    }

    public static f a(e eVar) {
        return l().mergeFrom(eVar);
    }

    /* renamed from: n */
    public f toBuilder() {
        return a(this);
    }

    static {
        f1366a.p();
    }
}
