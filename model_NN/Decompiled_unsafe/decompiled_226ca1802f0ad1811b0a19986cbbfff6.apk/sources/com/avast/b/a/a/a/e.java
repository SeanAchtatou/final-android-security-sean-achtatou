package com.avast.b.a.a.a;

import com.google.protobuf.Internal;

/* compiled from: ATProtoGenerics */
public enum e implements Internal.EnumLite {
    INCOMING(0, 0),
    OUTGOING(1, 1),
    MISSED(2, 2);
    
    private static Internal.EnumLiteMap<e> d = new f();
    private final int e;

    public final int getNumber() {
        return this.e;
    }

    public static e a(int i) {
        switch (i) {
            case 0:
                return INCOMING;
            case 1:
                return OUTGOING;
            case 2:
                return MISSED;
            default:
                return null;
        }
    }

    private e(int i, int i2) {
        this.e = i2;
    }
}
