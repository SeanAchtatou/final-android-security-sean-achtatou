package com.avast.b.a.a.a;

import android.support.v4.app.NotificationCompat;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: ATProtoGenerics */
public final class g extends GeneratedMessageLite implements i {

    /* renamed from: a  reason: collision with root package name */
    private static final g f1287a = new g(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1288b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public long f1289c;
    /* access modifiers changed from: private */
    public long d;
    /* access modifiers changed from: private */
    public e e;
    /* access modifiers changed from: private */
    public long f;
    /* access modifiers changed from: private */
    public Object g;
    /* access modifiers changed from: private */
    public Object h;
    /* access modifiers changed from: private */
    public long i;
    /* access modifiers changed from: private */
    public Object j;
    private byte k;
    private int l;

    private g(h hVar) {
        super(hVar);
        this.k = -1;
        this.l = -1;
    }

    private g(boolean z) {
        this.k = -1;
        this.l = -1;
    }

    public static g a() {
        return f1287a;
    }

    public boolean b() {
        return (this.f1288b & 1) == 1;
    }

    public long c() {
        return this.f1289c;
    }

    public boolean d() {
        return (this.f1288b & 2) == 2;
    }

    public long e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1288b & 4) == 4;
    }

    public e g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1288b & 8) == 8;
    }

    public long i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1288b & 16) == 16;
    }

    public String k() {
        Object obj = this.g;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.g = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString u() {
        Object obj = this.g;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.g = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean l() {
        return (this.f1288b & 32) == 32;
    }

    public String m() {
        Object obj = this.h;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.h = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString v() {
        Object obj = this.h;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.h = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean n() {
        return (this.f1288b & 64) == 64;
    }

    public long o() {
        return this.i;
    }

    public boolean p() {
        return (this.f1288b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public String q() {
        Object obj = this.j;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.j = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString w() {
        Object obj = this.j;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.j = copyFromUtf8;
        return copyFromUtf8;
    }

    private void x() {
        this.f1289c = 0;
        this.d = 0;
        this.e = e.INCOMING;
        this.f = 0;
        this.g = "";
        this.h = "";
        this.i = 0;
        this.j = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.k;
        if (b2 != -1) {
            if (b2 == 1) {
                return true;
            }
            return false;
        } else if (!b()) {
            this.k = 0;
            return false;
        } else if (!d()) {
            this.k = 0;
            return false;
        } else {
            this.k = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1288b & 1) == 1) {
            codedOutputStream.writeInt64(1, this.f1289c);
        }
        if ((this.f1288b & 2) == 2) {
            codedOutputStream.writeInt64(2, this.d);
        }
        if ((this.f1288b & 4) == 4) {
            codedOutputStream.writeEnum(3, this.e.getNumber());
        }
        if ((this.f1288b & 8) == 8) {
            codedOutputStream.writeInt64(4, this.f);
        }
        if ((this.f1288b & 16) == 16) {
            codedOutputStream.writeBytes(5, u());
        }
        if ((this.f1288b & 32) == 32) {
            codedOutputStream.writeBytes(6, v());
        }
        if ((this.f1288b & 64) == 64) {
            codedOutputStream.writeInt64(7, this.i);
        }
        if ((this.f1288b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBytes(8, w());
        }
    }

    public int getSerializedSize() {
        int i2 = this.l;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1288b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeInt64Size(1, this.f1289c);
            }
            if ((this.f1288b & 2) == 2) {
                i2 += CodedOutputStream.computeInt64Size(2, this.d);
            }
            if ((this.f1288b & 4) == 4) {
                i2 += CodedOutputStream.computeEnumSize(3, this.e.getNumber());
            }
            if ((this.f1288b & 8) == 8) {
                i2 += CodedOutputStream.computeInt64Size(4, this.f);
            }
            if ((this.f1288b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(5, u());
            }
            if ((this.f1288b & 32) == 32) {
                i2 += CodedOutputStream.computeBytesSize(6, v());
            }
            if ((this.f1288b & 64) == 64) {
                i2 += CodedOutputStream.computeInt64Size(7, this.i);
            }
            if ((this.f1288b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeBytesSize(8, w());
            }
            this.l = i2;
        }
        return i2;
    }

    public static h r() {
        return h.i();
    }

    /* renamed from: s */
    public h newBuilderForType() {
        return r();
    }

    public static h a(g gVar) {
        return r().mergeFrom(gVar);
    }

    /* renamed from: t */
    public h toBuilder() {
        return a(this);
    }

    static {
        f1287a.x();
    }
}
