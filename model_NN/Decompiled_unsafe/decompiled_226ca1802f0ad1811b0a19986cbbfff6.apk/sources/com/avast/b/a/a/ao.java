package com.avast.b.a.a;

import com.google.protobuf.Internal;

/* compiled from: AvastToWeb */
public enum ao implements Internal.EnumLite {
    NONE(0, 0),
    GPS(1, 1),
    NETWORK(2, 2),
    WIFI(3, 3);
    
    private static Internal.EnumLiteMap<ao> e = new ap();
    private final int f;

    public final int getNumber() {
        return this.f;
    }

    public static ao a(int i) {
        switch (i) {
            case 0:
                return NONE;
            case 1:
                return GPS;
            case 2:
                return NETWORK;
            case 3:
                return WIFI;
            default:
                return null;
        }
    }

    private ao(int i, int i2) {
        this.f = i2;
    }
}
