package com.avast.b.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: AvastToWeb */
public final class ad extends GeneratedMessageLite implements af {

    /* renamed from: a  reason: collision with root package name */
    private static final ad f1329a = new ad(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1330b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1331c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public Object f;
    /* access modifiers changed from: private */
    public Object g;
    private byte h;
    private int i;

    private ad(ae aeVar) {
        super(aeVar);
        this.h = -1;
        this.i = -1;
    }

    private ad(boolean z) {
        this.h = -1;
        this.i = -1;
    }

    public static ad a() {
        return f1329a;
    }

    public boolean b() {
        return (this.f1330b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1331c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1331c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString o() {
        Object obj = this.f1331c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1331c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1330b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString p() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1330b & 4) == 4;
    }

    public int g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1330b & 8) == 8;
    }

    public String i() {
        Object obj = this.f;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString q() {
        Object obj = this.f;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean j() {
        return (this.f1330b & 16) == 16;
    }

    public String k() {
        Object obj = this.g;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.g = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString r() {
        Object obj = this.g;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.g = copyFromUtf8;
        return copyFromUtf8;
    }

    private void s() {
        this.f1331c = "";
        this.d = "";
        this.e = 0;
        this.f = "";
        this.g = "";
    }

    public final boolean isInitialized() {
        boolean z = true;
        byte b2 = this.h;
        if (b2 != -1) {
            if (b2 != 1) {
                z = false;
            }
            return z;
        } else if (!b()) {
            this.h = 0;
            return false;
        } else if (!d()) {
            this.h = 0;
            return false;
        } else if (!f()) {
            this.h = 0;
            return false;
        } else if (!h()) {
            this.h = 0;
            return false;
        } else if (!j()) {
            this.h = 0;
            return false;
        } else {
            this.h = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1330b & 1) == 1) {
            codedOutputStream.writeBytes(1, o());
        }
        if ((this.f1330b & 2) == 2) {
            codedOutputStream.writeBytes(2, p());
        }
        if ((this.f1330b & 4) == 4) {
            codedOutputStream.writeInt32(3, this.e);
        }
        if ((this.f1330b & 8) == 8) {
            codedOutputStream.writeBytes(4, q());
        }
        if ((this.f1330b & 16) == 16) {
            codedOutputStream.writeBytes(5, r());
        }
    }

    public int getSerializedSize() {
        int i2 = this.i;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1330b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, o());
            }
            if ((this.f1330b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, p());
            }
            if ((this.f1330b & 4) == 4) {
                i2 += CodedOutputStream.computeInt32Size(3, this.e);
            }
            if ((this.f1330b & 8) == 8) {
                i2 += CodedOutputStream.computeBytesSize(4, q());
            }
            if ((this.f1330b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(5, r());
            }
            this.i = i2;
        }
        return i2;
    }

    public static ae l() {
        return ae.l();
    }

    /* renamed from: m */
    public ae newBuilderForType() {
        return l();
    }

    public static ae a(ad adVar) {
        return l().mergeFrom(adVar);
    }

    /* renamed from: n */
    public ae toBuilder() {
        return a(this);
    }

    static {
        f1329a.s();
    }
}
