package com.avast.b.a.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToWeb */
public final class w extends GeneratedMessageLite.Builder<v, w> implements x {

    /* renamed from: a  reason: collision with root package name */
    private int f1399a;

    /* renamed from: b  reason: collision with root package name */
    private int f1400b;

    /* renamed from: c  reason: collision with root package name */
    private Object f1401c = "";
    private Object d = "";

    private w() {
        i();
    }

    private void i() {
    }

    /* access modifiers changed from: private */
    public static w j() {
        return new w();
    }

    /* renamed from: a */
    public w clone() {
        return j().mergeFrom(d());
    }

    /* renamed from: b */
    public v getDefaultInstanceForType() {
        return v.a();
    }

    /* renamed from: c */
    public v build() {
        v d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public v d() {
        int i = 1;
        v vVar = new v(this);
        int i2 = this.f1399a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        int unused = vVar.f1398c = this.f1400b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        Object unused2 = vVar.d = this.f1401c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        Object unused3 = vVar.e = this.d;
        int unused4 = vVar.f1397b = i;
        return vVar;
    }

    /* renamed from: a */
    public w mergeFrom(v vVar) {
        if (vVar != v.a()) {
            if (vVar.b()) {
                a(vVar.c());
            }
            if (vVar.d()) {
                a(vVar.e());
            }
            if (vVar.f()) {
                b(vVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (e() && f() && g()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public w mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    this.f1399a |= 1;
                    this.f1400b = codedInputStream.readInt32();
                    break;
                case 18:
                    this.f1399a |= 2;
                    this.f1401c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1399a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1399a & 1) == 1;
    }

    public w a(int i) {
        this.f1399a |= 1;
        this.f1400b = i;
        return this;
    }

    public boolean f() {
        return (this.f1399a & 2) == 2;
    }

    public w a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1399a |= 2;
        this.f1401c = str;
        return this;
    }

    public boolean g() {
        return (this.f1399a & 4) == 4;
    }

    public w b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1399a |= 4;
        this.d = str;
        return this;
    }
}
