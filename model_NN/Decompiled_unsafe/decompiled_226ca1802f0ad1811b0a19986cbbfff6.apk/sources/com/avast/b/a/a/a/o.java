package com.avast.b.a.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.view.Menu;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: ATProtoGenerics */
public final class o extends GeneratedMessageLite implements q {

    /* renamed from: a  reason: collision with root package name */
    private static final o f1302a = new o(true);
    /* access modifiers changed from: private */
    public Object A;
    /* access modifiers changed from: private */
    public Object B;
    /* access modifiers changed from: private */
    public long C;
    /* access modifiers changed from: private */
    public long D;
    /* access modifiers changed from: private */
    public int E;
    /* access modifiers changed from: private */
    public Object F;
    private byte G;
    private int H;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1303b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1304c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public Object e;
    /* access modifiers changed from: private */
    public Object f;
    /* access modifiers changed from: private */
    public Object g;
    /* access modifiers changed from: private */
    public Object h;
    /* access modifiers changed from: private */
    public long i;
    /* access modifiers changed from: private */
    public long j;
    /* access modifiers changed from: private */
    public long k;
    /* access modifiers changed from: private */
    public ab l;
    /* access modifiers changed from: private */
    public long m;
    /* access modifiers changed from: private */
    public u n;
    /* access modifiers changed from: private */
    public Object o;
    /* access modifiers changed from: private */
    public Object p;
    /* access modifiers changed from: private */
    public Object q;
    /* access modifiers changed from: private */
    public Object r;
    /* access modifiers changed from: private */
    public long s;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public ByteString u;
    /* access modifiers changed from: private */
    public Object v;
    /* access modifiers changed from: private */
    public Object w;
    /* access modifiers changed from: private */
    public int x;
    /* access modifiers changed from: private */
    public int y;
    /* access modifiers changed from: private */
    public Object z;

    private o(p pVar) {
        super(pVar);
        this.G = -1;
        this.H = -1;
    }

    private o(boolean z2) {
        this.G = -1;
        this.H = -1;
    }

    public static o a() {
        return f1302a;
    }

    public boolean b() {
        return (this.f1303b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1304c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1304c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString am() {
        Object obj = this.f1304c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1304c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1303b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString an() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1303b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ao() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean h() {
        return (this.f1303b & 8) == 8;
    }

    public String i() {
        Object obj = this.f;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ap() {
        Object obj = this.f;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean j() {
        return (this.f1303b & 16) == 16;
    }

    public String k() {
        Object obj = this.g;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.g = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString aq() {
        Object obj = this.g;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.g = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean l() {
        return (this.f1303b & 32) == 32;
    }

    public String m() {
        Object obj = this.h;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.h = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ar() {
        Object obj = this.h;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.h = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean n() {
        return (this.f1303b & 64) == 64;
    }

    public long o() {
        return this.i;
    }

    public boolean p() {
        return (this.f1303b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public long q() {
        return this.j;
    }

    public boolean r() {
        return (this.f1303b & 256) == 256;
    }

    public long s() {
        return this.k;
    }

    public boolean t() {
        return (this.f1303b & 512) == 512;
    }

    public ab u() {
        return this.l;
    }

    public boolean v() {
        return (this.f1303b & 1024) == 1024;
    }

    public long w() {
        return this.m;
    }

    public boolean x() {
        return (this.f1303b & 2048) == 2048;
    }

    public u y() {
        return this.n;
    }

    public boolean z() {
        return (this.f1303b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096;
    }

    public String A() {
        Object obj = this.o;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.o = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString as() {
        Object obj = this.o;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.o = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean B() {
        return (this.f1303b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192;
    }

    public String C() {
        Object obj = this.p;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.p = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString at() {
        Object obj = this.p;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.p = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean D() {
        return (this.f1303b & 16384) == 16384;
    }

    public String E() {
        Object obj = this.q;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.q = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString au() {
        Object obj = this.q;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.q = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean F() {
        return (this.f1303b & 32768) == 32768;
    }

    public String G() {
        Object obj = this.r;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.r = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString av() {
        Object obj = this.r;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.r = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean H() {
        return (this.f1303b & Menu.CATEGORY_CONTAINER) == 65536;
    }

    public long I() {
        return this.s;
    }

    public boolean J() {
        return (this.f1303b & Menu.CATEGORY_SYSTEM) == 131072;
    }

    public int K() {
        return this.t;
    }

    public boolean L() {
        return (this.f1303b & Menu.CATEGORY_ALTERNATIVE) == 262144;
    }

    public ByteString M() {
        return this.u;
    }

    public boolean N() {
        return (this.f1303b & 524288) == 524288;
    }

    public String O() {
        Object obj = this.v;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.v = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString aw() {
        Object obj = this.v;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.v = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean P() {
        return (this.f1303b & 1048576) == 1048576;
    }

    public String Q() {
        Object obj = this.w;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.w = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ax() {
        Object obj = this.w;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.w = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean R() {
        return (this.f1303b & 2097152) == 2097152;
    }

    public int S() {
        return this.x;
    }

    public boolean T() {
        return (this.f1303b & 4194304) == 4194304;
    }

    public int U() {
        return this.y;
    }

    public boolean V() {
        return (this.f1303b & 8388608) == 8388608;
    }

    public String W() {
        Object obj = this.z;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.z = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ay() {
        Object obj = this.z;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.z = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean X() {
        return (this.f1303b & 16777216) == 16777216;
    }

    public String Y() {
        Object obj = this.A;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.A = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString az() {
        Object obj = this.A;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.A = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean Z() {
        return (this.f1303b & 33554432) == 33554432;
    }

    public String aa() {
        Object obj = this.B;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.B = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString aA() {
        Object obj = this.B;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.B = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean ab() {
        return (this.f1303b & 67108864) == 67108864;
    }

    public long ac() {
        return this.C;
    }

    public boolean ad() {
        return (this.f1303b & 134217728) == 134217728;
    }

    public long ae() {
        return this.D;
    }

    public boolean af() {
        return (this.f1303b & 268435456) == 268435456;
    }

    public int ag() {
        return this.E;
    }

    public boolean ah() {
        return (this.f1303b & 536870912) == 536870912;
    }

    public String ai() {
        Object obj = this.F;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.F = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString aB() {
        Object obj = this.F;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.F = copyFromUtf8;
        return copyFromUtf8;
    }

    private void aC() {
        this.f1304c = "";
        this.d = "";
        this.e = "";
        this.f = "";
        this.g = "";
        this.h = "";
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = ab.INTERNAL;
        this.m = 0;
        this.n = u.IMAGE;
        this.o = "";
        this.p = "";
        this.q = "";
        this.r = "";
        this.s = 0;
        this.t = 0;
        this.u = ByteString.EMPTY;
        this.v = "";
        this.w = "";
        this.x = 0;
        this.y = 0;
        this.z = "";
        this.A = "";
        this.B = "";
        this.C = 0;
        this.D = 0;
        this.E = 0;
        this.F = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.G;
        if (b2 == -1) {
            this.G = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1303b & 1) == 1) {
            codedOutputStream.writeBytes(1, am());
        }
        if ((this.f1303b & 2) == 2) {
            codedOutputStream.writeBytes(2, an());
        }
        if ((this.f1303b & 4) == 4) {
            codedOutputStream.writeBytes(3, ao());
        }
        if ((this.f1303b & 8) == 8) {
            codedOutputStream.writeBytes(4, ap());
        }
        if ((this.f1303b & 16) == 16) {
            codedOutputStream.writeBytes(5, aq());
        }
        if ((this.f1303b & 32) == 32) {
            codedOutputStream.writeBytes(6, ar());
        }
        if ((this.f1303b & 64) == 64) {
            codedOutputStream.writeUInt64(7, this.i);
        }
        if ((this.f1303b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeUInt64(8, this.j);
        }
        if ((this.f1303b & 256) == 256) {
            codedOutputStream.writeUInt64(9, this.k);
        }
        if ((this.f1303b & 512) == 512) {
            codedOutputStream.writeEnum(10, this.l.getNumber());
        }
        if ((this.f1303b & 1024) == 1024) {
            codedOutputStream.writeUInt64(11, this.m);
        }
        if ((this.f1303b & 2048) == 2048) {
            codedOutputStream.writeEnum(12, this.n.getNumber());
        }
        if ((this.f1303b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            codedOutputStream.writeBytes(13, as());
        }
        if ((this.f1303b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            codedOutputStream.writeBytes(14, at());
        }
        if ((this.f1303b & 16384) == 16384) {
            codedOutputStream.writeBytes(15, au());
        }
        if ((this.f1303b & 32768) == 32768) {
            codedOutputStream.writeBytes(16, av());
        }
        if ((this.f1303b & Menu.CATEGORY_CONTAINER) == 65536) {
            codedOutputStream.writeUInt64(17, this.s);
        }
        if ((this.f1303b & Menu.CATEGORY_SYSTEM) == 131072) {
            codedOutputStream.writeInt32(18, this.t);
        }
        if ((this.f1303b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            codedOutputStream.writeBytes(19, this.u);
        }
        if ((this.f1303b & 524288) == 524288) {
            codedOutputStream.writeBytes(20, aw());
        }
        if ((this.f1303b & 1048576) == 1048576) {
            codedOutputStream.writeBytes(21, ax());
        }
        if ((this.f1303b & 2097152) == 2097152) {
            codedOutputStream.writeInt32(22, this.x);
        }
        if ((this.f1303b & 4194304) == 4194304) {
            codedOutputStream.writeInt32(23, this.y);
        }
        if ((this.f1303b & 8388608) == 8388608) {
            codedOutputStream.writeBytes(24, ay());
        }
        if ((this.f1303b & 16777216) == 16777216) {
            codedOutputStream.writeBytes(25, az());
        }
        if ((this.f1303b & 33554432) == 33554432) {
            codedOutputStream.writeBytes(26, aA());
        }
        if ((this.f1303b & 67108864) == 67108864) {
            codedOutputStream.writeInt64(27, this.C);
        }
        if ((this.f1303b & 134217728) == 134217728) {
            codedOutputStream.writeInt64(28, this.D);
        }
        if ((this.f1303b & 268435456) == 268435456) {
            codedOutputStream.writeInt32(29, this.E);
        }
        if ((this.f1303b & 536870912) == 536870912) {
            codedOutputStream.writeBytes(30, aB());
        }
    }

    public int getSerializedSize() {
        int i2 = this.H;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1303b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, am());
            }
            if ((this.f1303b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, an());
            }
            if ((this.f1303b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, ao());
            }
            if ((this.f1303b & 8) == 8) {
                i2 += CodedOutputStream.computeBytesSize(4, ap());
            }
            if ((this.f1303b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(5, aq());
            }
            if ((this.f1303b & 32) == 32) {
                i2 += CodedOutputStream.computeBytesSize(6, ar());
            }
            if ((this.f1303b & 64) == 64) {
                i2 += CodedOutputStream.computeUInt64Size(7, this.i);
            }
            if ((this.f1303b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeUInt64Size(8, this.j);
            }
            if ((this.f1303b & 256) == 256) {
                i2 += CodedOutputStream.computeUInt64Size(9, this.k);
            }
            if ((this.f1303b & 512) == 512) {
                i2 += CodedOutputStream.computeEnumSize(10, this.l.getNumber());
            }
            if ((this.f1303b & 1024) == 1024) {
                i2 += CodedOutputStream.computeUInt64Size(11, this.m);
            }
            if ((this.f1303b & 2048) == 2048) {
                i2 += CodedOutputStream.computeEnumSize(12, this.n.getNumber());
            }
            if ((this.f1303b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
                i2 += CodedOutputStream.computeBytesSize(13, as());
            }
            if ((this.f1303b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
                i2 += CodedOutputStream.computeBytesSize(14, at());
            }
            if ((this.f1303b & 16384) == 16384) {
                i2 += CodedOutputStream.computeBytesSize(15, au());
            }
            if ((this.f1303b & 32768) == 32768) {
                i2 += CodedOutputStream.computeBytesSize(16, av());
            }
            if ((this.f1303b & Menu.CATEGORY_CONTAINER) == 65536) {
                i2 += CodedOutputStream.computeUInt64Size(17, this.s);
            }
            if ((this.f1303b & Menu.CATEGORY_SYSTEM) == 131072) {
                i2 += CodedOutputStream.computeInt32Size(18, this.t);
            }
            if ((this.f1303b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
                i2 += CodedOutputStream.computeBytesSize(19, this.u);
            }
            if ((this.f1303b & 524288) == 524288) {
                i2 += CodedOutputStream.computeBytesSize(20, aw());
            }
            if ((this.f1303b & 1048576) == 1048576) {
                i2 += CodedOutputStream.computeBytesSize(21, ax());
            }
            if ((this.f1303b & 2097152) == 2097152) {
                i2 += CodedOutputStream.computeInt32Size(22, this.x);
            }
            if ((this.f1303b & 4194304) == 4194304) {
                i2 += CodedOutputStream.computeInt32Size(23, this.y);
            }
            if ((this.f1303b & 8388608) == 8388608) {
                i2 += CodedOutputStream.computeBytesSize(24, ay());
            }
            if ((this.f1303b & 16777216) == 16777216) {
                i2 += CodedOutputStream.computeBytesSize(25, az());
            }
            if ((this.f1303b & 33554432) == 33554432) {
                i2 += CodedOutputStream.computeBytesSize(26, aA());
            }
            if ((this.f1303b & 67108864) == 67108864) {
                i2 += CodedOutputStream.computeInt64Size(27, this.C);
            }
            if ((this.f1303b & 134217728) == 134217728) {
                i2 += CodedOutputStream.computeInt64Size(28, this.D);
            }
            if ((this.f1303b & 268435456) == 268435456) {
                i2 += CodedOutputStream.computeInt32Size(29, this.E);
            }
            if ((this.f1303b & 536870912) == 536870912) {
                i2 += CodedOutputStream.computeBytesSize(30, aB());
            }
            this.H = i2;
        }
        return i2;
    }

    public static p aj() {
        return p.g();
    }

    /* renamed from: ak */
    public p newBuilderForType() {
        return aj();
    }

    public static p a(o oVar) {
        return aj().mergeFrom(oVar);
    }

    /* renamed from: al */
    public p toBuilder() {
        return a(this);
    }

    static {
        f1302a.aC();
    }
}
