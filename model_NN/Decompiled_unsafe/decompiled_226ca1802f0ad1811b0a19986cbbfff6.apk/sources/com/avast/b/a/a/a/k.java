package com.avast.b.a.a.a;

import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.LazyStringArrayList;
import com.google.protobuf.LazyStringList;
import com.google.protobuf.UnmodifiableLazyStringList;

/* compiled from: ATProtoGenerics */
public final class k extends GeneratedMessageLite.Builder<j, k> implements l {

    /* renamed from: a  reason: collision with root package name */
    private int f1296a;

    /* renamed from: b  reason: collision with root package name */
    private long f1297b;

    /* renamed from: c  reason: collision with root package name */
    private Object f1298c = "";
    private LazyStringList d = LazyStringArrayList.EMPTY;
    private LazyStringList e = LazyStringArrayList.EMPTY;
    private long f;
    private Object g = "";
    private int h;
    private ByteString i = ByteString.EMPTY;

    private k() {
        g();
    }

    private void g() {
    }

    /* access modifiers changed from: private */
    public static k h() {
        return new k();
    }

    /* renamed from: a */
    public k clone() {
        return h().mergeFrom(d());
    }

    /* renamed from: b */
    public j getDefaultInstanceForType() {
        return j.a();
    }

    /* renamed from: c */
    public j build() {
        j d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public j d() {
        int i2 = 1;
        j jVar = new j(this);
        int i3 = this.f1296a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        long unused = jVar.f1295c = this.f1297b;
        if ((i3 & 2) == 2) {
            i2 |= 2;
        }
        Object unused2 = jVar.d = this.f1298c;
        if ((this.f1296a & 4) == 4) {
            this.d = new UnmodifiableLazyStringList(this.d);
            this.f1296a &= -5;
        }
        LazyStringList unused3 = jVar.e = this.d;
        if ((this.f1296a & 8) == 8) {
            this.e = new UnmodifiableLazyStringList(this.e);
            this.f1296a &= -9;
        }
        LazyStringList unused4 = jVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 4;
        }
        long unused5 = jVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 8;
        }
        Object unused6 = jVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 16;
        }
        int unused7 = jVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= 32;
        }
        ByteString unused8 = jVar.j = this.i;
        int unused9 = jVar.f1294b = i2;
        return jVar;
    }

    /* renamed from: a */
    public k mergeFrom(j jVar) {
        if (jVar != j.a()) {
            if (jVar.b()) {
                a(jVar.c());
            }
            if (jVar.d()) {
                a(jVar.e());
            }
            if (!jVar.e.isEmpty()) {
                if (this.d.isEmpty()) {
                    this.d = jVar.e;
                    this.f1296a &= -5;
                } else {
                    i();
                    this.d.addAll(jVar.e);
                }
            }
            if (!jVar.f.isEmpty()) {
                if (this.e.isEmpty()) {
                    this.e = jVar.f;
                    this.f1296a &= -9;
                } else {
                    j();
                    this.e.addAll(jVar.f);
                }
            }
            if (jVar.h()) {
                b(jVar.i());
            }
            if (jVar.j()) {
                b(jVar.k());
            }
            if (jVar.l()) {
                a(jVar.m());
            }
            if (jVar.n()) {
                a(jVar.o());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (!e()) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public k mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    this.f1296a |= 1;
                    this.f1297b = codedInputStream.readInt64();
                    break;
                case 18:
                    this.f1296a |= 2;
                    this.f1298c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    i();
                    this.d.add(codedInputStream.readBytes());
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    j();
                    this.e.add(codedInputStream.readBytes());
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f1296a |= 16;
                    this.f = codedInputStream.readInt64();
                    break;
                case 50:
                    this.f1296a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    this.f1296a |= 64;
                    this.h = codedInputStream.readInt32();
                    break;
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f1296a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1296a & 1) == 1;
    }

    public k a(long j) {
        this.f1296a |= 1;
        this.f1297b = j;
        return this;
    }

    public k a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1296a |= 2;
        this.f1298c = str;
        return this;
    }

    private void i() {
        if ((this.f1296a & 4) != 4) {
            this.d = new LazyStringArrayList(this.d);
            this.f1296a |= 4;
        }
    }

    private void j() {
        if ((this.f1296a & 8) != 8) {
            this.e = new LazyStringArrayList(this.e);
            this.f1296a |= 8;
        }
    }

    public k b(long j) {
        this.f1296a |= 16;
        this.f = j;
        return this;
    }

    public k b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1296a |= 32;
        this.g = str;
        return this;
    }

    public k a(int i2) {
        this.f1296a |= 64;
        this.h = i2;
        return this;
    }

    public k a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1296a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = byteString;
        return this;
    }
}
