package com.avast.b.a.a;

import com.actionbarsherlock.R;
import com.avast.b.a.a.a.m;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToWeb */
public final class t extends GeneratedMessageLite.Builder<s, t> implements u {

    /* renamed from: a  reason: collision with root package name */
    private int f1393a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1394b = "";

    /* renamed from: c  reason: collision with root package name */
    private m f1395c = m.NONE;
    private boolean d;
    private Object e = "";
    private Object f = "";

    private t() {
        g();
    }

    private void g() {
    }

    /* access modifiers changed from: private */
    public static t h() {
        return new t();
    }

    /* renamed from: a */
    public t clone() {
        return h().mergeFrom(d());
    }

    /* renamed from: b */
    public s getDefaultInstanceForType() {
        return s.a();
    }

    /* renamed from: c */
    public s build() {
        s d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public s d() {
        int i = 1;
        s sVar = new s(this);
        int i2 = this.f1393a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        Object unused = sVar.f1392c = this.f1394b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        m unused2 = sVar.d = this.f1395c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        boolean unused3 = sVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 8;
        }
        Object unused4 = sVar.f = this.e;
        if ((i2 & 16) == 16) {
            i |= 16;
        }
        Object unused5 = sVar.g = this.f;
        int unused6 = sVar.f1391b = i;
        return sVar;
    }

    /* renamed from: a */
    public t mergeFrom(s sVar) {
        if (sVar != s.a()) {
            if (sVar.b()) {
                a(sVar.c());
            }
            if (sVar.d()) {
                a(sVar.e());
            }
            if (sVar.f()) {
                a(sVar.g());
            }
            if (sVar.h()) {
                b(sVar.i());
            }
            if (sVar.j()) {
                c(sVar.k());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (!e()) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public t mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1393a |= 1;
                    this.f1394b = codedInputStream.readBytes();
                    break;
                case 16:
                    m a2 = m.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1393a |= 2;
                        this.f1395c = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    this.f1393a |= 4;
                    this.d = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f1393a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f1393a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1393a & 1) == 1;
    }

    public t a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1393a |= 1;
        this.f1394b = str;
        return this;
    }

    public t a(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        this.f1393a |= 2;
        this.f1395c = mVar;
        return this;
    }

    public t a(boolean z) {
        this.f1393a |= 4;
        this.d = z;
        return this;
    }

    public t b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1393a |= 8;
        this.e = str;
        return this;
    }

    public t c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1393a |= 16;
        this.f = str;
        return this;
    }
}
