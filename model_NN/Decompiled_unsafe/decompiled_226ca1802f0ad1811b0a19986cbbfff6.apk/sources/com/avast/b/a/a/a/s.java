package com.avast.b.a.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.actionbarsherlock.view.Menu;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ATProtoGenerics */
public final class s extends GeneratedMessageLite.Builder<r, s> implements t {

    /* renamed from: a  reason: collision with root package name */
    private int f1311a;

    /* renamed from: b  reason: collision with root package name */
    private long f1312b;

    /* renamed from: c  reason: collision with root package name */
    private Object f1313c = "";
    private Object d = "";
    private Object e = "";
    private long f;
    private long g;
    private u h = u.IMAGE;
    private Object i = "";
    private Object j = "";
    private Object k = "";
    private Object l = "";
    private long m;
    private int n;
    private ByteString o = ByteString.EMPTY;
    private Object p = "";
    private Object q = "";
    private int r;
    private int s;
    private Object t = "";
    private Object u = "";
    private Object v = "";
    private Object w = "";
    private long x;
    private long y;
    private int z;

    private s() {
        g();
    }

    private void g() {
    }

    /* access modifiers changed from: private */
    public static s h() {
        return new s();
    }

    /* renamed from: a */
    public s clone() {
        return h().mergeFrom(d());
    }

    /* renamed from: b */
    public r getDefaultInstanceForType() {
        return r.a();
    }

    /* renamed from: c */
    public r build() {
        r d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public r d() {
        r rVar = new r(this);
        int i2 = this.f1311a;
        int i3 = 0;
        if ((i2 & 1) == 1) {
            i3 = 1;
        }
        long unused = rVar.f1310c = this.f1312b;
        if ((i2 & 2) == 2) {
            i3 |= 2;
        }
        Object unused2 = rVar.d = this.f1313c;
        if ((i2 & 4) == 4) {
            i3 |= 4;
        }
        Object unused3 = rVar.e = this.d;
        if ((i2 & 8) == 8) {
            i3 |= 8;
        }
        Object unused4 = rVar.f = this.e;
        if ((i2 & 16) == 16) {
            i3 |= 16;
        }
        long unused5 = rVar.g = this.f;
        if ((i2 & 32) == 32) {
            i3 |= 32;
        }
        long unused6 = rVar.h = this.g;
        if ((i2 & 64) == 64) {
            i3 |= 64;
        }
        u unused7 = rVar.i = this.h;
        if ((i2 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i3 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        Object unused8 = rVar.j = this.i;
        if ((i2 & 256) == 256) {
            i3 |= 256;
        }
        Object unused9 = rVar.k = this.j;
        if ((i2 & 512) == 512) {
            i3 |= 512;
        }
        Object unused10 = rVar.l = this.k;
        if ((i2 & 1024) == 1024) {
            i3 |= 1024;
        }
        Object unused11 = rVar.m = this.l;
        if ((i2 & 2048) == 2048) {
            i3 |= 2048;
        }
        long unused12 = rVar.n = this.m;
        if ((i2 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i3 |= FragmentTransaction.TRANSIT_ENTER_MASK;
        }
        int unused13 = rVar.o = this.n;
        if ((i2 & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            i3 |= FragmentTransaction.TRANSIT_EXIT_MASK;
        }
        ByteString unused14 = rVar.p = this.o;
        if ((i2 & 16384) == 16384) {
            i3 |= 16384;
        }
        Object unused15 = rVar.q = this.p;
        if ((i2 & 32768) == 32768) {
            i3 |= 32768;
        }
        Object unused16 = rVar.r = this.q;
        if ((i2 & Menu.CATEGORY_CONTAINER) == 65536) {
            i3 |= Menu.CATEGORY_CONTAINER;
        }
        int unused17 = rVar.s = this.r;
        if ((i2 & Menu.CATEGORY_SYSTEM) == 131072) {
            i3 |= Menu.CATEGORY_SYSTEM;
        }
        int unused18 = rVar.t = this.s;
        if ((i2 & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            i3 |= Menu.CATEGORY_ALTERNATIVE;
        }
        Object unused19 = rVar.u = this.t;
        if ((i2 & 524288) == 524288) {
            i3 |= 524288;
        }
        Object unused20 = rVar.v = this.u;
        if ((1048576 & i2) == 1048576) {
            i3 |= 1048576;
        }
        Object unused21 = rVar.w = this.v;
        if ((2097152 & i2) == 2097152) {
            i3 |= 2097152;
        }
        Object unused22 = rVar.x = this.w;
        if ((4194304 & i2) == 4194304) {
            i3 |= 4194304;
        }
        long unused23 = rVar.y = this.x;
        if ((8388608 & i2) == 8388608) {
            i3 |= 8388608;
        }
        long unused24 = rVar.z = this.y;
        if ((i2 & 16777216) == 16777216) {
            i3 |= 16777216;
        }
        int unused25 = rVar.A = this.z;
        int unused26 = rVar.f1309b = i3;
        return rVar;
    }

    /* renamed from: a */
    public s mergeFrom(r rVar) {
        if (rVar != r.a()) {
            if (rVar.b()) {
                a(rVar.c());
            }
            if (rVar.d()) {
                a(rVar.e());
            }
            if (rVar.f()) {
                b(rVar.g());
            }
            if (rVar.h()) {
                c(rVar.i());
            }
            if (rVar.j()) {
                b(rVar.k());
            }
            if (rVar.l()) {
                c(rVar.m());
            }
            if (rVar.n()) {
                a(rVar.o());
            }
            if (rVar.p()) {
                d(rVar.q());
            }
            if (rVar.r()) {
                e(rVar.s());
            }
            if (rVar.t()) {
                f(rVar.u());
            }
            if (rVar.v()) {
                g(rVar.w());
            }
            if (rVar.x()) {
                d(rVar.y());
            }
            if (rVar.z()) {
                a(rVar.A());
            }
            if (rVar.B()) {
                a(rVar.C());
            }
            if (rVar.D()) {
                h(rVar.E());
            }
            if (rVar.F()) {
                i(rVar.G());
            }
            if (rVar.H()) {
                b(rVar.I());
            }
            if (rVar.J()) {
                c(rVar.K());
            }
            if (rVar.L()) {
                j(rVar.M());
            }
            if (rVar.N()) {
                k(rVar.O());
            }
            if (rVar.P()) {
                l(rVar.Q());
            }
            if (rVar.R()) {
                m(rVar.S());
            }
            if (rVar.T()) {
                e(rVar.U());
            }
            if (rVar.V()) {
                f(rVar.W());
            }
            if (rVar.X()) {
                d(rVar.Y());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (!e()) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public s mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    this.f1311a |= 1;
                    this.f1312b = codedInputStream.readInt64();
                    break;
                case 18:
                    this.f1311a |= 2;
                    this.f1313c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1311a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f1311a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f1311a |= 16;
                    this.f = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                    this.f1311a |= 32;
                    this.g = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    u a2 = u.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1311a |= 64;
                        this.h = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f1311a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                case 74:
                    this.f1311a |= 256;
                    this.j = codedInputStream.readBytes();
                    break;
                case 82:
                    this.f1311a |= 512;
                    this.k = codedInputStream.readBytes();
                    break;
                case 90:
                    this.f1311a |= 1024;
                    this.l = codedInputStream.readBytes();
                    break;
                case 96:
                    this.f1311a |= 2048;
                    this.m = codedInputStream.readInt64();
                    break;
                case 104:
                    this.f1311a |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.n = codedInputStream.readInt32();
                    break;
                case 114:
                    this.f1311a |= FragmentTransaction.TRANSIT_EXIT_MASK;
                    this.o = codedInputStream.readBytes();
                    break;
                case 122:
                    this.f1311a |= 16384;
                    this.p = codedInputStream.readBytes();
                    break;
                case 130:
                    this.f1311a |= 32768;
                    this.q = codedInputStream.readBytes();
                    break;
                case 136:
                    this.f1311a |= Menu.CATEGORY_CONTAINER;
                    this.r = codedInputStream.readInt32();
                    break;
                case 144:
                    this.f1311a |= Menu.CATEGORY_SYSTEM;
                    this.s = codedInputStream.readInt32();
                    break;
                case 154:
                    this.f1311a |= Menu.CATEGORY_ALTERNATIVE;
                    this.t = codedInputStream.readBytes();
                    break;
                case 162:
                    this.f1311a |= 524288;
                    this.u = codedInputStream.readBytes();
                    break;
                case 170:
                    this.f1311a |= 1048576;
                    this.v = codedInputStream.readBytes();
                    break;
                case 178:
                    this.f1311a |= 2097152;
                    this.w = codedInputStream.readBytes();
                    break;
                case 184:
                    this.f1311a |= 4194304;
                    this.x = codedInputStream.readInt64();
                    break;
                case 192:
                    this.f1311a |= 8388608;
                    this.y = codedInputStream.readInt64();
                    break;
                case 200:
                    this.f1311a |= 16777216;
                    this.z = codedInputStream.readInt32();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1311a & 1) == 1;
    }

    public s a(long j2) {
        this.f1311a |= 1;
        this.f1312b = j2;
        return this;
    }

    public s a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 2;
        this.f1313c = str;
        return this;
    }

    public s b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 4;
        this.d = str;
        return this;
    }

    public s c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 8;
        this.e = str;
        return this;
    }

    public s b(long j2) {
        this.f1311a |= 16;
        this.f = j2;
        return this;
    }

    public s c(long j2) {
        this.f1311a |= 32;
        this.g = j2;
        return this;
    }

    public s a(u uVar) {
        if (uVar == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 64;
        this.h = uVar;
        return this;
    }

    public s d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = str;
        return this;
    }

    public s e(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 256;
        this.j = str;
        return this;
    }

    public s f(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 512;
        this.k = str;
        return this;
    }

    public s g(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 1024;
        this.l = str;
        return this;
    }

    public s d(long j2) {
        this.f1311a |= 2048;
        this.m = j2;
        return this;
    }

    public s a(int i2) {
        this.f1311a |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.n = i2;
        return this;
    }

    public s a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1311a |= FragmentTransaction.TRANSIT_EXIT_MASK;
        this.o = byteString;
        return this;
    }

    public s h(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 16384;
        this.p = str;
        return this;
    }

    public s i(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 32768;
        this.q = str;
        return this;
    }

    public s b(int i2) {
        this.f1311a |= Menu.CATEGORY_CONTAINER;
        this.r = i2;
        return this;
    }

    public s c(int i2) {
        this.f1311a |= Menu.CATEGORY_SYSTEM;
        this.s = i2;
        return this;
    }

    public s j(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= Menu.CATEGORY_ALTERNATIVE;
        this.t = str;
        return this;
    }

    public s k(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 524288;
        this.u = str;
        return this;
    }

    public s l(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 1048576;
        this.v = str;
        return this;
    }

    public s m(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1311a |= 2097152;
        this.w = str;
        return this;
    }

    public s e(long j2) {
        this.f1311a |= 4194304;
        this.x = j2;
        return this;
    }

    public s f(long j2) {
        this.f1311a |= 8388608;
        this.y = j2;
        return this;
    }

    public s d(int i2) {
        this.f1311a |= 16777216;
        this.z = i2;
        return this;
    }
}
