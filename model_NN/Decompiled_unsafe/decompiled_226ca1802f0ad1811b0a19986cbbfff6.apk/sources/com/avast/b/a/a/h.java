package com.avast.b.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.LazyStringArrayList;
import com.google.protobuf.LazyStringList;
import java.io.InputStream;
import java.util.List;

/* compiled from: AvastToDevice */
public final class h extends GeneratedMessageLite implements j {

    /* renamed from: a  reason: collision with root package name */
    private static final h f1372a = new h(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1373b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public int f1374c;
    /* access modifiers changed from: private */
    public LazyStringList d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public boolean q;
    /* access modifiers changed from: private */
    public Object r;
    private byte s;
    private int t;

    private h(i iVar) {
        super(iVar);
        this.s = -1;
        this.t = -1;
    }

    private h(boolean z) {
        this.s = -1;
        this.t = -1;
    }

    public static h a() {
        return f1372a;
    }

    public boolean b() {
        return (this.f1373b & 1) == 1;
    }

    public int c() {
        return this.f1374c;
    }

    public List<String> d() {
        return this.d;
    }

    public int e() {
        return this.d.size();
    }

    public boolean f() {
        return (this.f1373b & 2) == 2;
    }

    public boolean g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1373b & 4) == 4;
    }

    public boolean i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1373b & 8) == 8;
    }

    public boolean k() {
        return this.g;
    }

    public boolean l() {
        return (this.f1373b & 16) == 16;
    }

    public boolean m() {
        return this.h;
    }

    public boolean n() {
        return (this.f1373b & 32) == 32;
    }

    public boolean o() {
        return this.i;
    }

    public boolean p() {
        return (this.f1373b & 64) == 64;
    }

    public boolean q() {
        return this.j;
    }

    public boolean r() {
        return (this.f1373b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public boolean s() {
        return this.k;
    }

    public boolean t() {
        return (this.f1373b & 256) == 256;
    }

    public boolean u() {
        return this.l;
    }

    public boolean v() {
        return (this.f1373b & 512) == 512;
    }

    public boolean w() {
        return this.m;
    }

    public boolean x() {
        return (this.f1373b & 1024) == 1024;
    }

    public boolean y() {
        return this.n;
    }

    public boolean z() {
        return (this.f1373b & 2048) == 2048;
    }

    public boolean A() {
        return this.o;
    }

    public boolean B() {
        return (this.f1373b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096;
    }

    public int C() {
        return this.p;
    }

    public boolean D() {
        return (this.f1373b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192;
    }

    public boolean E() {
        return this.q;
    }

    public boolean F() {
        return (this.f1373b & 16384) == 16384;
    }

    public String G() {
        Object obj = this.r;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.r = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString K() {
        Object obj = this.r;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.r = copyFromUtf8;
        return copyFromUtf8;
    }

    private void L() {
        this.f1374c = 0;
        this.d = LazyStringArrayList.EMPTY;
        this.e = false;
        this.f = false;
        this.g = false;
        this.h = false;
        this.i = false;
        this.j = false;
        this.k = false;
        this.l = false;
        this.m = false;
        this.n = false;
        this.o = false;
        this.p = 0;
        this.q = false;
        this.r = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.s;
        if (b2 != -1) {
            if (b2 == 1) {
                return true;
            }
            return false;
        } else if (!b()) {
            this.s = 0;
            return false;
        } else {
            this.s = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1373b & 1) == 1) {
            codedOutputStream.writeInt32(1, this.f1374c);
        }
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            codedOutputStream.writeBytes(2, this.d.getByteString(i2));
        }
        if ((this.f1373b & 2) == 2) {
            codedOutputStream.writeBool(3, this.e);
        }
        if ((this.f1373b & 4) == 4) {
            codedOutputStream.writeBool(4, this.f);
        }
        if ((this.f1373b & 8) == 8) {
            codedOutputStream.writeBool(5, this.g);
        }
        if ((this.f1373b & 16) == 16) {
            codedOutputStream.writeBool(6, this.h);
        }
        if ((this.f1373b & 32) == 32) {
            codedOutputStream.writeBool(7, this.i);
        }
        if ((this.f1373b & 64) == 64) {
            codedOutputStream.writeBool(8, this.j);
        }
        if ((this.f1373b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBool(9, this.k);
        }
        if ((this.f1373b & 256) == 256) {
            codedOutputStream.writeBool(10, this.l);
        }
        if ((this.f1373b & 512) == 512) {
            codedOutputStream.writeBool(11, this.m);
        }
        if ((this.f1373b & 1024) == 1024) {
            codedOutputStream.writeBool(12, this.n);
        }
        if ((this.f1373b & 2048) == 2048) {
            codedOutputStream.writeBool(13, this.o);
        }
        if ((this.f1373b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            codedOutputStream.writeInt32(14, this.p);
        }
        if ((this.f1373b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            codedOutputStream.writeBool(15, this.q);
        }
        if ((this.f1373b & 16384) == 16384) {
            codedOutputStream.writeBytes(16, K());
        }
    }

    public int getSerializedSize() {
        int i2;
        int i3 = this.t;
        if (i3 == -1) {
            if ((this.f1373b & 1) == 1) {
                i2 = CodedOutputStream.computeInt32Size(1, this.f1374c) + 0;
            } else {
                i2 = 0;
            }
            int i4 = 0;
            for (int i5 = 0; i5 < this.d.size(); i5++) {
                i4 += CodedOutputStream.computeBytesSizeNoTag(this.d.getByteString(i5));
            }
            i3 = i2 + i4 + (d().size() * 1);
            if ((this.f1373b & 2) == 2) {
                i3 += CodedOutputStream.computeBoolSize(3, this.e);
            }
            if ((this.f1373b & 4) == 4) {
                i3 += CodedOutputStream.computeBoolSize(4, this.f);
            }
            if ((this.f1373b & 8) == 8) {
                i3 += CodedOutputStream.computeBoolSize(5, this.g);
            }
            if ((this.f1373b & 16) == 16) {
                i3 += CodedOutputStream.computeBoolSize(6, this.h);
            }
            if ((this.f1373b & 32) == 32) {
                i3 += CodedOutputStream.computeBoolSize(7, this.i);
            }
            if ((this.f1373b & 64) == 64) {
                i3 += CodedOutputStream.computeBoolSize(8, this.j);
            }
            if ((this.f1373b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i3 += CodedOutputStream.computeBoolSize(9, this.k);
            }
            if ((this.f1373b & 256) == 256) {
                i3 += CodedOutputStream.computeBoolSize(10, this.l);
            }
            if ((this.f1373b & 512) == 512) {
                i3 += CodedOutputStream.computeBoolSize(11, this.m);
            }
            if ((this.f1373b & 1024) == 1024) {
                i3 += CodedOutputStream.computeBoolSize(12, this.n);
            }
            if ((this.f1373b & 2048) == 2048) {
                i3 += CodedOutputStream.computeBoolSize(13, this.o);
            }
            if ((this.f1373b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
                i3 += CodedOutputStream.computeInt32Size(14, this.p);
            }
            if ((this.f1373b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
                i3 += CodedOutputStream.computeBoolSize(15, this.q);
            }
            if ((this.f1373b & 16384) == 16384) {
                i3 += CodedOutputStream.computeBytesSize(16, K());
            }
            this.t = i3;
        }
        return i3;
    }

    public static h a(byte[] bArr) {
        return ((i) H().mergeFrom(bArr)).i();
    }

    public static h a(InputStream inputStream) {
        return ((i) H().mergeFrom(inputStream)).i();
    }

    public static i H() {
        return i.h();
    }

    /* renamed from: I */
    public i newBuilderForType() {
        return H();
    }

    public static i a(h hVar) {
        return H().mergeFrom(hVar);
    }

    /* renamed from: J */
    public i toBuilder() {
        return a(this);
    }

    static {
        f1372a.L();
    }
}
