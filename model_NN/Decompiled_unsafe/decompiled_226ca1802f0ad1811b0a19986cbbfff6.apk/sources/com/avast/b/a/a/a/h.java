package com.avast.b.a.a.a;

import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ATProtoGenerics */
public final class h extends GeneratedMessageLite.Builder<g, h> implements i {

    /* renamed from: a  reason: collision with root package name */
    private int f1290a;

    /* renamed from: b  reason: collision with root package name */
    private long f1291b;

    /* renamed from: c  reason: collision with root package name */
    private long f1292c;
    private e d = e.INCOMING;
    private long e;
    private Object f = "";
    private Object g = "";
    private long h;
    private Object i = "";

    private h() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static h i() {
        return new h();
    }

    /* renamed from: a */
    public h clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public g getDefaultInstanceForType() {
        return g.a();
    }

    /* renamed from: c */
    public g build() {
        g d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public g d() {
        int i2 = 1;
        g gVar = new g(this);
        int i3 = this.f1290a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        long unused = gVar.f1289c = this.f1291b;
        if ((i3 & 2) == 2) {
            i2 |= 2;
        }
        long unused2 = gVar.d = this.f1292c;
        if ((i3 & 4) == 4) {
            i2 |= 4;
        }
        e unused3 = gVar.e = this.d;
        if ((i3 & 8) == 8) {
            i2 |= 8;
        }
        long unused4 = gVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 16;
        }
        Object unused5 = gVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 32;
        }
        Object unused6 = gVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 64;
        }
        long unused7 = gVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        Object unused8 = gVar.j = this.i;
        int unused9 = gVar.f1288b = i2;
        return gVar;
    }

    /* renamed from: a */
    public h mergeFrom(g gVar) {
        if (gVar != g.a()) {
            if (gVar.b()) {
                a(gVar.c());
            }
            if (gVar.d()) {
                b(gVar.e());
            }
            if (gVar.f()) {
                a(gVar.g());
            }
            if (gVar.h()) {
                c(gVar.i());
            }
            if (gVar.j()) {
                a(gVar.k());
            }
            if (gVar.l()) {
                b(gVar.m());
            }
            if (gVar.n()) {
                d(gVar.o());
            }
            if (gVar.p()) {
                c(gVar.q());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (e() && f()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public h mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    this.f1290a |= 1;
                    this.f1291b = codedInputStream.readInt64();
                    break;
                case 16:
                    this.f1290a |= 2;
                    this.f1292c = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    e a2 = e.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1290a |= 4;
                        this.d = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    this.f1290a |= 8;
                    this.e = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f1290a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                case 50:
                    this.f1290a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    this.f1290a |= 64;
                    this.h = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f1290a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1290a & 1) == 1;
    }

    public h a(long j) {
        this.f1290a |= 1;
        this.f1291b = j;
        return this;
    }

    public boolean f() {
        return (this.f1290a & 2) == 2;
    }

    public h b(long j) {
        this.f1290a |= 2;
        this.f1292c = j;
        return this;
    }

    public h a(e eVar) {
        if (eVar == null) {
            throw new NullPointerException();
        }
        this.f1290a |= 4;
        this.d = eVar;
        return this;
    }

    public h c(long j) {
        this.f1290a |= 8;
        this.e = j;
        return this;
    }

    public h a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1290a |= 16;
        this.f = str;
        return this;
    }

    public h b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1290a |= 32;
        this.g = str;
        return this;
    }

    public h d(long j) {
        this.f1290a |= 64;
        this.h = j;
        return this;
    }

    public h c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1290a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = str;
        return this;
    }
}
