package com.avast.b.a.a;

import com.google.protobuf.Internal;

/* compiled from: AvastToWeb */
public enum aq implements Internal.EnumLite {
    UPDATE_SIMULATION(0, 0),
    UPDATE_SIM_CHANGE(1, 1),
    UPDATE_NORMAL(2, 2),
    UPDATE_BATTERY(3, 3),
    UPDATE_GEOFENCE(4, 4),
    UPDATE_PASSWORD_CHECK(5, 5);
    
    private static Internal.EnumLiteMap<aq> g = new ar();
    private final int h;

    public final int getNumber() {
        return this.h;
    }

    public static aq a(int i2) {
        switch (i2) {
            case 0:
                return UPDATE_SIMULATION;
            case 1:
                return UPDATE_SIM_CHANGE;
            case 2:
                return UPDATE_NORMAL;
            case 3:
                return UPDATE_BATTERY;
            case 4:
                return UPDATE_GEOFENCE;
            case 5:
                return UPDATE_PASSWORD_CHECK;
            default:
                return null;
        }
    }

    private aq(int i2, int i3) {
        this.h = i3;
    }
}
