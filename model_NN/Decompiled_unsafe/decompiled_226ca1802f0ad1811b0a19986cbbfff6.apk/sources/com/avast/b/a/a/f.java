package com.avast.b.a.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToDevice */
public final class f extends GeneratedMessageLite.Builder<e, f> implements g {

    /* renamed from: a  reason: collision with root package name */
    private int f1369a;

    /* renamed from: b  reason: collision with root package name */
    private c f1370b = c.NONE;

    /* renamed from: c  reason: collision with root package name */
    private int f1371c;
    private boolean d;
    private boolean e;
    private Object f = "";

    private f() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static f g() {
        return new f();
    }

    /* renamed from: a */
    public f clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public e getDefaultInstanceForType() {
        return e.a();
    }

    /* renamed from: c */
    public e build() {
        e d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    /* access modifiers changed from: private */
    public e h() {
        e d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2).asInvalidProtocolBufferException();
    }

    public e d() {
        int i = 1;
        e eVar = new e(this);
        int i2 = this.f1369a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        c unused = eVar.f1368c = this.f1370b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        int unused2 = eVar.d = this.f1371c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        boolean unused3 = eVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 8;
        }
        boolean unused4 = eVar.f = this.e;
        if ((i2 & 16) == 16) {
            i |= 16;
        }
        Object unused5 = eVar.g = this.f;
        int unused6 = eVar.f1367b = i;
        return eVar;
    }

    /* renamed from: a */
    public f mergeFrom(e eVar) {
        if (eVar != e.a()) {
            if (eVar.b()) {
                a(eVar.c());
            }
            if (eVar.d()) {
                a(eVar.e());
            }
            if (eVar.f()) {
                a(eVar.g());
            }
            if (eVar.h()) {
                b(eVar.i());
            }
            if (eVar.j()) {
                a(eVar.k());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public f mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    c a2 = c.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1369a |= 1;
                        this.f1370b = a2;
                        break;
                    }
                case 16:
                    this.f1369a |= 2;
                    this.f1371c = codedInputStream.readInt32();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    this.f1369a |= 4;
                    this.d = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    this.f1369a |= 8;
                    this.e = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f1369a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public f a(c cVar) {
        if (cVar == null) {
            throw new NullPointerException();
        }
        this.f1369a |= 1;
        this.f1370b = cVar;
        return this;
    }

    public f a(int i) {
        this.f1369a |= 2;
        this.f1371c = i;
        return this;
    }

    public f a(boolean z) {
        this.f1369a |= 4;
        this.d = z;
        return this;
    }

    public f b(boolean z) {
        this.f1369a |= 8;
        this.e = z;
        return this;
    }

    public f a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1369a |= 16;
        this.f = str;
        return this;
    }
}
