package com.avast.b.a.a.a;

import android.support.v4.app.NotificationCompat;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: ATProtoGenerics */
public final class y extends GeneratedMessageLite implements aa {

    /* renamed from: a  reason: collision with root package name */
    private static final y f1320a = new y(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1321b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public long f1322c;
    /* access modifiers changed from: private */
    public long d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public Object f;
    /* access modifiers changed from: private */
    public Object g;
    /* access modifiers changed from: private */
    public Object h;
    /* access modifiers changed from: private */
    public long i;
    /* access modifiers changed from: private */
    public Object j;
    private byte k;
    private int l;

    private y(z zVar) {
        super(zVar);
        this.k = -1;
        this.l = -1;
    }

    private y(boolean z) {
        this.k = -1;
        this.l = -1;
    }

    public static y a() {
        return f1320a;
    }

    public boolean b() {
        return (this.f1321b & 1) == 1;
    }

    public long c() {
        return this.f1322c;
    }

    public boolean d() {
        return (this.f1321b & 2) == 2;
    }

    public long e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1321b & 4) == 4;
    }

    public boolean g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1321b & 8) == 8;
    }

    public String i() {
        Object obj = this.f;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString u() {
        Object obj = this.f;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean j() {
        return (this.f1321b & 16) == 16;
    }

    public String k() {
        Object obj = this.g;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.g = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString v() {
        Object obj = this.g;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.g = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean l() {
        return (this.f1321b & 32) == 32;
    }

    public String m() {
        Object obj = this.h;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.h = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString w() {
        Object obj = this.h;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.h = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean n() {
        return (this.f1321b & 64) == 64;
    }

    public long o() {
        return this.i;
    }

    public boolean p() {
        return (this.f1321b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public String q() {
        Object obj = this.j;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.j = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString x() {
        Object obj = this.j;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.j = copyFromUtf8;
        return copyFromUtf8;
    }

    private void y() {
        this.f1322c = 0;
        this.d = 0;
        this.e = false;
        this.f = "";
        this.g = "";
        this.h = "";
        this.i = 0;
        this.j = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.k;
        if (b2 != -1) {
            if (b2 == 1) {
                return true;
            }
            return false;
        } else if (!b()) {
            this.k = 0;
            return false;
        } else if (!d()) {
            this.k = 0;
            return false;
        } else {
            this.k = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1321b & 1) == 1) {
            codedOutputStream.writeInt64(1, this.f1322c);
        }
        if ((this.f1321b & 2) == 2) {
            codedOutputStream.writeInt64(2, this.d);
        }
        if ((this.f1321b & 4) == 4) {
            codedOutputStream.writeBool(3, this.e);
        }
        if ((this.f1321b & 8) == 8) {
            codedOutputStream.writeBytes(4, u());
        }
        if ((this.f1321b & 16) == 16) {
            codedOutputStream.writeBytes(5, v());
        }
        if ((this.f1321b & 32) == 32) {
            codedOutputStream.writeBytes(6, w());
        }
        if ((this.f1321b & 64) == 64) {
            codedOutputStream.writeInt64(7, this.i);
        }
        if ((this.f1321b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBytes(8, x());
        }
    }

    public int getSerializedSize() {
        int i2 = this.l;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1321b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeInt64Size(1, this.f1322c);
            }
            if ((this.f1321b & 2) == 2) {
                i2 += CodedOutputStream.computeInt64Size(2, this.d);
            }
            if ((this.f1321b & 4) == 4) {
                i2 += CodedOutputStream.computeBoolSize(3, this.e);
            }
            if ((this.f1321b & 8) == 8) {
                i2 += CodedOutputStream.computeBytesSize(4, u());
            }
            if ((this.f1321b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(5, v());
            }
            if ((this.f1321b & 32) == 32) {
                i2 += CodedOutputStream.computeBytesSize(6, w());
            }
            if ((this.f1321b & 64) == 64) {
                i2 += CodedOutputStream.computeInt64Size(7, this.i);
            }
            if ((this.f1321b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeBytesSize(8, x());
            }
            this.l = i2;
        }
        return i2;
    }

    public static z r() {
        return z.i();
    }

    /* renamed from: s */
    public z newBuilderForType() {
        return r();
    }

    public static z a(y yVar) {
        return r().mergeFrom(yVar);
    }

    /* renamed from: t */
    public z toBuilder() {
        return a(this);
    }

    static {
        f1320a.y();
    }
}
