package com.avast.b.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.view.Menu;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: AvastToWeb */
public final class m extends GeneratedMessageLite implements o {

    /* renamed from: a  reason: collision with root package name */
    private static final m f1378a = new m(true);
    /* access modifiers changed from: private */
    public boolean A;
    /* access modifiers changed from: private */
    public boolean B;
    /* access modifiers changed from: private */
    public Object C;
    /* access modifiers changed from: private */
    public boolean D;
    /* access modifiers changed from: private */
    public boolean E;
    /* access modifiers changed from: private */
    public boolean F;
    /* access modifiers changed from: private */
    public boolean G;
    /* access modifiers changed from: private */
    public boolean H;
    /* access modifiers changed from: private */
    public boolean I;
    /* access modifiers changed from: private */
    public boolean J;
    /* access modifiers changed from: private */
    public boolean K;
    /* access modifiers changed from: private */
    public boolean L;
    /* access modifiers changed from: private */
    public boolean M;
    /* access modifiers changed from: private */
    public boolean N;
    /* access modifiers changed from: private */
    public Object O;
    /* access modifiers changed from: private */
    public int P;
    /* access modifiers changed from: private */
    public int Q;
    /* access modifiers changed from: private */
    public int R;
    /* access modifiers changed from: private */
    public boolean S;
    /* access modifiers changed from: private */
    public boolean T;
    /* access modifiers changed from: private */
    public boolean U;
    /* access modifiers changed from: private */
    public boolean V;
    /* access modifiers changed from: private */
    public boolean W;
    /* access modifiers changed from: private */
    public boolean X;
    /* access modifiers changed from: private */
    public boolean Y;
    /* access modifiers changed from: private */
    public boolean Z;
    /* access modifiers changed from: private */
    public boolean aa;
    /* access modifiers changed from: private */
    public boolean ab;
    /* access modifiers changed from: private */
    public boolean ac;
    /* access modifiers changed from: private */
    public boolean ad;
    /* access modifiers changed from: private */
    public boolean ae;
    /* access modifiers changed from: private */
    public boolean af;
    /* access modifiers changed from: private */
    public boolean ag;
    /* access modifiers changed from: private */
    public int ah;
    /* access modifiers changed from: private */
    public double ai;
    /* access modifiers changed from: private */
    public double aj;
    /* access modifiers changed from: private */
    public int ak;
    /* access modifiers changed from: private */
    public boolean al;
    /* access modifiers changed from: private */
    public boolean am;
    /* access modifiers changed from: private */
    public boolean an;
    /* access modifiers changed from: private */
    public boolean ao;
    private byte ap;
    private int aq;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1379b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public int f1380c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public Object e;
    /* access modifiers changed from: private */
    public Object f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public boolean q;
    /* access modifiers changed from: private */
    public boolean r;
    /* access modifiers changed from: private */
    public Object s;
    /* access modifiers changed from: private */
    public Object t;
    /* access modifiers changed from: private */
    public Object u;
    /* access modifiers changed from: private */
    public Object v;
    /* access modifiers changed from: private */
    public Object w;
    /* access modifiers changed from: private */
    public Object x;
    /* access modifiers changed from: private */
    public Object y;
    /* access modifiers changed from: private */
    public Object z;

    private m(n nVar) {
        super(nVar);
        this.ap = -1;
        this.aq = -1;
    }

    private m(boolean z2) {
        this.ap = -1;
        this.aq = -1;
    }

    public static m a() {
        return f1378a;
    }

    public boolean b() {
        return (this.f1379b & 1) == 1;
    }

    public String c() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bC() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1379b & 2) == 2;
    }

    public String e() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bD() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1379b & 4) == 4;
    }

    public String g() {
        Object obj = this.f;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bE() {
        Object obj = this.f;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean h() {
        return (this.f1379b & 8) == 8;
    }

    public boolean i() {
        return this.g;
    }

    public boolean j() {
        return (this.f1379b & 16) == 16;
    }

    public boolean k() {
        return this.h;
    }

    public boolean l() {
        return (this.f1379b & 32) == 32;
    }

    public boolean m() {
        return this.i;
    }

    public boolean n() {
        return (this.f1379b & 64) == 64;
    }

    public boolean o() {
        return this.j;
    }

    public boolean p() {
        return (this.f1379b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public boolean q() {
        return this.k;
    }

    public boolean r() {
        return (this.f1379b & 256) == 256;
    }

    public boolean s() {
        return this.l;
    }

    public boolean t() {
        return (this.f1379b & 512) == 512;
    }

    public boolean u() {
        return this.m;
    }

    public boolean v() {
        return (this.f1379b & 1024) == 1024;
    }

    public boolean w() {
        return this.n;
    }

    public boolean x() {
        return (this.f1379b & 2048) == 2048;
    }

    public boolean y() {
        return this.o;
    }

    public boolean z() {
        return (this.f1379b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096;
    }

    public int A() {
        return this.p;
    }

    public boolean B() {
        return (this.f1379b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192;
    }

    public boolean C() {
        return this.q;
    }

    public boolean D() {
        return (this.f1379b & 16384) == 16384;
    }

    public boolean E() {
        return this.r;
    }

    public boolean F() {
        return (this.f1379b & 32768) == 32768;
    }

    public String G() {
        Object obj = this.s;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.s = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bF() {
        Object obj = this.s;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.s = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean H() {
        return (this.f1379b & Menu.CATEGORY_CONTAINER) == 65536;
    }

    public String I() {
        Object obj = this.t;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.t = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bG() {
        Object obj = this.t;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.t = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean J() {
        return (this.f1379b & Menu.CATEGORY_SYSTEM) == 131072;
    }

    public String K() {
        Object obj = this.u;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.u = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bH() {
        Object obj = this.u;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.u = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean L() {
        return (this.f1379b & Menu.CATEGORY_ALTERNATIVE) == 262144;
    }

    public String M() {
        Object obj = this.v;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.v = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bI() {
        Object obj = this.v;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.v = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean N() {
        return (this.f1379b & 524288) == 524288;
    }

    public String O() {
        Object obj = this.w;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.w = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bJ() {
        Object obj = this.w;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.w = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean P() {
        return (this.f1379b & 1048576) == 1048576;
    }

    public String Q() {
        Object obj = this.x;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.x = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bK() {
        Object obj = this.x;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.x = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean R() {
        return (this.f1379b & 2097152) == 2097152;
    }

    public String S() {
        Object obj = this.y;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.y = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bL() {
        Object obj = this.y;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.y = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean T() {
        return (this.f1379b & 4194304) == 4194304;
    }

    public String U() {
        Object obj = this.z;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.z = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bM() {
        Object obj = this.z;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.z = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean V() {
        return (this.f1379b & 8388608) == 8388608;
    }

    public boolean W() {
        return this.A;
    }

    public boolean X() {
        return (this.f1379b & 16777216) == 16777216;
    }

    public boolean Y() {
        return this.B;
    }

    public boolean Z() {
        return (this.f1379b & 33554432) == 33554432;
    }

    public String aa() {
        Object obj = this.C;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.C = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bN() {
        Object obj = this.C;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.C = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean ab() {
        return (this.f1379b & 67108864) == 67108864;
    }

    public boolean ac() {
        return this.D;
    }

    public boolean ad() {
        return (this.f1379b & 134217728) == 134217728;
    }

    public boolean ae() {
        return this.E;
    }

    public boolean af() {
        return (this.f1379b & 268435456) == 268435456;
    }

    public boolean ag() {
        return this.F;
    }

    public boolean ah() {
        return (this.f1379b & 536870912) == 536870912;
    }

    public boolean ai() {
        return this.G;
    }

    public boolean aj() {
        return (this.f1379b & 1073741824) == 1073741824;
    }

    public boolean ak() {
        return this.H;
    }

    public boolean al() {
        return (this.f1379b & Integer.MIN_VALUE) == Integer.MIN_VALUE;
    }

    public boolean am() {
        return this.I;
    }

    public boolean an() {
        return (this.f1380c & 1) == 1;
    }

    public boolean ao() {
        return this.J;
    }

    public boolean ap() {
        return (this.f1380c & 2) == 2;
    }

    public boolean aq() {
        return this.K;
    }

    public boolean ar() {
        return (this.f1380c & 4) == 4;
    }

    public boolean as() {
        return this.L;
    }

    public boolean at() {
        return (this.f1380c & 8) == 8;
    }

    public boolean au() {
        return this.M;
    }

    public boolean av() {
        return (this.f1380c & 16) == 16;
    }

    public boolean aw() {
        return this.N;
    }

    public boolean ax() {
        return (this.f1380c & 32) == 32;
    }

    public String ay() {
        Object obj = this.O;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.O = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString bO() {
        Object obj = this.O;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.O = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean az() {
        return (this.f1380c & 64) == 64;
    }

    public int aA() {
        return this.P;
    }

    public boolean aB() {
        return (this.f1380c & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public int aC() {
        return this.Q;
    }

    public boolean aD() {
        return (this.f1380c & 256) == 256;
    }

    public int aE() {
        return this.R;
    }

    public boolean aF() {
        return (this.f1380c & 512) == 512;
    }

    public boolean aG() {
        return this.S;
    }

    public boolean aH() {
        return (this.f1380c & 1024) == 1024;
    }

    public boolean aI() {
        return this.T;
    }

    public boolean aJ() {
        return (this.f1380c & 2048) == 2048;
    }

    public boolean aK() {
        return this.U;
    }

    public boolean aL() {
        return (this.f1380c & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096;
    }

    public boolean aM() {
        return this.V;
    }

    public boolean aN() {
        return (this.f1380c & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192;
    }

    public boolean aO() {
        return this.W;
    }

    public boolean aP() {
        return (this.f1380c & 16384) == 16384;
    }

    public boolean aQ() {
        return this.X;
    }

    public boolean aR() {
        return (this.f1380c & 32768) == 32768;
    }

    public boolean aS() {
        return this.Y;
    }

    public boolean aT() {
        return (this.f1380c & Menu.CATEGORY_CONTAINER) == 65536;
    }

    public boolean aU() {
        return this.Z;
    }

    public boolean aV() {
        return (this.f1380c & Menu.CATEGORY_SYSTEM) == 131072;
    }

    public boolean aW() {
        return this.aa;
    }

    public boolean aX() {
        return (this.f1380c & Menu.CATEGORY_ALTERNATIVE) == 262144;
    }

    public boolean aY() {
        return this.ab;
    }

    public boolean aZ() {
        return (this.f1380c & 524288) == 524288;
    }

    public boolean ba() {
        return this.ac;
    }

    public boolean bb() {
        return (this.f1380c & 1048576) == 1048576;
    }

    public boolean bc() {
        return this.ad;
    }

    public boolean bd() {
        return (this.f1380c & 2097152) == 2097152;
    }

    public boolean be() {
        return this.ae;
    }

    public boolean bf() {
        return (this.f1380c & 4194304) == 4194304;
    }

    public boolean bg() {
        return this.af;
    }

    public boolean bh() {
        return (this.f1380c & 8388608) == 8388608;
    }

    public boolean bi() {
        return this.ag;
    }

    public boolean bj() {
        return (this.f1380c & 16777216) == 16777216;
    }

    public int bk() {
        return this.ah;
    }

    public boolean bl() {
        return (this.f1380c & 33554432) == 33554432;
    }

    public double bm() {
        return this.ai;
    }

    public boolean bn() {
        return (this.f1380c & 67108864) == 67108864;
    }

    public double bo() {
        return this.aj;
    }

    public boolean bp() {
        return (this.f1380c & 134217728) == 134217728;
    }

    public int bq() {
        return this.ak;
    }

    public boolean br() {
        return (this.f1380c & 268435456) == 268435456;
    }

    public boolean bs() {
        return this.al;
    }

    public boolean bt() {
        return (this.f1380c & 536870912) == 536870912;
    }

    public boolean bu() {
        return this.am;
    }

    public boolean bv() {
        return (this.f1380c & 1073741824) == 1073741824;
    }

    public boolean bw() {
        return this.an;
    }

    public boolean bx() {
        return (this.f1380c & Integer.MIN_VALUE) == Integer.MIN_VALUE;
    }

    public boolean by() {
        return this.ao;
    }

    private void bP() {
        this.d = "";
        this.e = "";
        this.f = "";
        this.g = false;
        this.h = false;
        this.i = false;
        this.j = false;
        this.k = false;
        this.l = false;
        this.m = false;
        this.n = false;
        this.o = false;
        this.p = 0;
        this.q = false;
        this.r = false;
        this.s = "";
        this.t = "";
        this.u = "";
        this.v = "";
        this.w = "";
        this.x = "";
        this.y = "";
        this.z = "";
        this.A = false;
        this.B = false;
        this.C = "";
        this.D = false;
        this.E = false;
        this.F = false;
        this.G = false;
        this.H = true;
        this.I = false;
        this.J = true;
        this.K = true;
        this.L = false;
        this.M = false;
        this.N = false;
        this.O = "";
        this.P = -1;
        this.Q = -1;
        this.R = -1;
        this.S = false;
        this.T = false;
        this.U = false;
        this.V = false;
        this.W = false;
        this.X = false;
        this.Y = false;
        this.Z = false;
        this.aa = false;
        this.ab = true;
        this.ac = false;
        this.ad = true;
        this.ae = true;
        this.af = false;
        this.ag = false;
        this.ah = 0;
        this.ai = 0.0d;
        this.aj = 0.0d;
        this.ak = 0;
        this.al = false;
        this.am = false;
        this.an = false;
        this.ao = false;
    }

    public final boolean isInitialized() {
        byte b2 = this.ap;
        if (b2 == -1) {
            this.ap = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1379b & 1) == 1) {
            codedOutputStream.writeBytes(1, bC());
        }
        if ((this.f1379b & 2) == 2) {
            codedOutputStream.writeBytes(2, bD());
        }
        if ((this.f1379b & 4) == 4) {
            codedOutputStream.writeBytes(3, bE());
        }
        if ((this.f1379b & 8) == 8) {
            codedOutputStream.writeBool(4, this.g);
        }
        if ((this.f1379b & 16) == 16) {
            codedOutputStream.writeBool(5, this.h);
        }
        if ((this.f1379b & 32) == 32) {
            codedOutputStream.writeBool(6, this.i);
        }
        if ((this.f1379b & 64) == 64) {
            codedOutputStream.writeBool(7, this.j);
        }
        if ((this.f1379b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBool(8, this.k);
        }
        if ((this.f1379b & 256) == 256) {
            codedOutputStream.writeBool(9, this.l);
        }
        if ((this.f1379b & 512) == 512) {
            codedOutputStream.writeBool(10, this.m);
        }
        if ((this.f1379b & 1024) == 1024) {
            codedOutputStream.writeBool(11, this.n);
        }
        if ((this.f1379b & 2048) == 2048) {
            codedOutputStream.writeBool(12, this.o);
        }
        if ((this.f1379b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            codedOutputStream.writeInt32(13, this.p);
        }
        if ((this.f1379b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            codedOutputStream.writeBool(14, this.q);
        }
        if ((this.f1379b & 16384) == 16384) {
            codedOutputStream.writeBool(15, this.r);
        }
        if ((this.f1379b & 32768) == 32768) {
            codedOutputStream.writeBytes(16, bF());
        }
        if ((this.f1379b & Menu.CATEGORY_CONTAINER) == 65536) {
            codedOutputStream.writeBytes(17, bG());
        }
        if ((this.f1379b & Menu.CATEGORY_SYSTEM) == 131072) {
            codedOutputStream.writeBytes(18, bH());
        }
        if ((this.f1379b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            codedOutputStream.writeBytes(19, bI());
        }
        if ((this.f1379b & 524288) == 524288) {
            codedOutputStream.writeBytes(20, bJ());
        }
        if ((this.f1379b & 1048576) == 1048576) {
            codedOutputStream.writeBytes(21, bK());
        }
        if ((this.f1379b & 2097152) == 2097152) {
            codedOutputStream.writeBytes(22, bL());
        }
        if ((this.f1379b & 4194304) == 4194304) {
            codedOutputStream.writeBytes(23, bM());
        }
        if ((this.f1379b & 8388608) == 8388608) {
            codedOutputStream.writeBool(24, this.A);
        }
        if ((this.f1379b & 16777216) == 16777216) {
            codedOutputStream.writeBool(25, this.B);
        }
        if ((this.f1379b & 33554432) == 33554432) {
            codedOutputStream.writeBytes(26, bN());
        }
        if ((this.f1379b & 67108864) == 67108864) {
            codedOutputStream.writeBool(27, this.D);
        }
        if ((this.f1379b & 134217728) == 134217728) {
            codedOutputStream.writeBool(28, this.E);
        }
        if ((this.f1379b & 268435456) == 268435456) {
            codedOutputStream.writeBool(29, this.F);
        }
        if ((this.f1379b & 536870912) == 536870912) {
            codedOutputStream.writeBool(30, this.G);
        }
        if ((this.f1379b & 1073741824) == 1073741824) {
            codedOutputStream.writeBool(31, this.H);
        }
        if ((this.f1379b & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            codedOutputStream.writeBool(32, this.I);
        }
        if ((this.f1380c & 1) == 1) {
            codedOutputStream.writeBool(33, this.J);
        }
        if ((this.f1380c & 2) == 2) {
            codedOutputStream.writeBool(34, this.K);
        }
        if ((this.f1380c & 4) == 4) {
            codedOutputStream.writeBool(35, this.L);
        }
        if ((this.f1380c & 8) == 8) {
            codedOutputStream.writeBool(36, this.M);
        }
        if ((this.f1380c & 16) == 16) {
            codedOutputStream.writeBool(37, this.N);
        }
        if ((this.f1380c & 32) == 32) {
            codedOutputStream.writeBytes(38, bO());
        }
        if ((this.f1380c & 64) == 64) {
            codedOutputStream.writeInt32(39, this.P);
        }
        if ((this.f1380c & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeInt32(40, this.Q);
        }
        if ((this.f1380c & 256) == 256) {
            codedOutputStream.writeInt32(41, this.R);
        }
        if ((this.f1380c & 512) == 512) {
            codedOutputStream.writeBool(42, this.S);
        }
        if ((this.f1380c & 1024) == 1024) {
            codedOutputStream.writeBool(43, this.T);
        }
        if ((this.f1380c & 2048) == 2048) {
            codedOutputStream.writeBool(44, this.U);
        }
        if ((this.f1380c & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            codedOutputStream.writeBool(45, this.V);
        }
        if ((this.f1380c & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            codedOutputStream.writeBool(46, this.W);
        }
        if ((this.f1380c & 16384) == 16384) {
            codedOutputStream.writeBool(47, this.X);
        }
        if ((this.f1380c & 32768) == 32768) {
            codedOutputStream.writeBool(48, this.Y);
        }
        if ((this.f1380c & Menu.CATEGORY_CONTAINER) == 65536) {
            codedOutputStream.writeBool(49, this.Z);
        }
        if ((this.f1380c & Menu.CATEGORY_SYSTEM) == 131072) {
            codedOutputStream.writeBool(50, this.aa);
        }
        if ((this.f1380c & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            codedOutputStream.writeBool(51, this.ab);
        }
        if ((this.f1380c & 524288) == 524288) {
            codedOutputStream.writeBool(52, this.ac);
        }
        if ((this.f1380c & 1048576) == 1048576) {
            codedOutputStream.writeBool(53, this.ad);
        }
        if ((this.f1380c & 2097152) == 2097152) {
            codedOutputStream.writeBool(54, this.ae);
        }
        if ((this.f1380c & 4194304) == 4194304) {
            codedOutputStream.writeBool(55, this.af);
        }
        if ((this.f1380c & 8388608) == 8388608) {
            codedOutputStream.writeBool(56, this.ag);
        }
        if ((this.f1380c & 16777216) == 16777216) {
            codedOutputStream.writeInt32(57, this.ah);
        }
        if ((this.f1380c & 33554432) == 33554432) {
            codedOutputStream.writeDouble(58, this.ai);
        }
        if ((this.f1380c & 67108864) == 67108864) {
            codedOutputStream.writeDouble(59, this.aj);
        }
        if ((this.f1380c & 134217728) == 134217728) {
            codedOutputStream.writeInt32(60, this.ak);
        }
        if ((this.f1380c & 268435456) == 268435456) {
            codedOutputStream.writeBool(61, this.al);
        }
        if ((this.f1380c & 536870912) == 536870912) {
            codedOutputStream.writeBool(62, this.am);
        }
        if ((this.f1380c & 1073741824) == 1073741824) {
            codedOutputStream.writeBool(63, this.an);
        }
        if ((this.f1380c & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            codedOutputStream.writeBool(64, this.ao);
        }
    }

    public int getSerializedSize() {
        int i2 = this.aq;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1379b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, bC());
            }
            if ((this.f1379b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, bD());
            }
            if ((this.f1379b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, bE());
            }
            if ((this.f1379b & 8) == 8) {
                i2 += CodedOutputStream.computeBoolSize(4, this.g);
            }
            if ((this.f1379b & 16) == 16) {
                i2 += CodedOutputStream.computeBoolSize(5, this.h);
            }
            if ((this.f1379b & 32) == 32) {
                i2 += CodedOutputStream.computeBoolSize(6, this.i);
            }
            if ((this.f1379b & 64) == 64) {
                i2 += CodedOutputStream.computeBoolSize(7, this.j);
            }
            if ((this.f1379b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeBoolSize(8, this.k);
            }
            if ((this.f1379b & 256) == 256) {
                i2 += CodedOutputStream.computeBoolSize(9, this.l);
            }
            if ((this.f1379b & 512) == 512) {
                i2 += CodedOutputStream.computeBoolSize(10, this.m);
            }
            if ((this.f1379b & 1024) == 1024) {
                i2 += CodedOutputStream.computeBoolSize(11, this.n);
            }
            if ((this.f1379b & 2048) == 2048) {
                i2 += CodedOutputStream.computeBoolSize(12, this.o);
            }
            if ((this.f1379b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
                i2 += CodedOutputStream.computeInt32Size(13, this.p);
            }
            if ((this.f1379b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
                i2 += CodedOutputStream.computeBoolSize(14, this.q);
            }
            if ((this.f1379b & 16384) == 16384) {
                i2 += CodedOutputStream.computeBoolSize(15, this.r);
            }
            if ((this.f1379b & 32768) == 32768) {
                i2 += CodedOutputStream.computeBytesSize(16, bF());
            }
            if ((this.f1379b & Menu.CATEGORY_CONTAINER) == 65536) {
                i2 += CodedOutputStream.computeBytesSize(17, bG());
            }
            if ((this.f1379b & Menu.CATEGORY_SYSTEM) == 131072) {
                i2 += CodedOutputStream.computeBytesSize(18, bH());
            }
            if ((this.f1379b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
                i2 += CodedOutputStream.computeBytesSize(19, bI());
            }
            if ((this.f1379b & 524288) == 524288) {
                i2 += CodedOutputStream.computeBytesSize(20, bJ());
            }
            if ((this.f1379b & 1048576) == 1048576) {
                i2 += CodedOutputStream.computeBytesSize(21, bK());
            }
            if ((this.f1379b & 2097152) == 2097152) {
                i2 += CodedOutputStream.computeBytesSize(22, bL());
            }
            if ((this.f1379b & 4194304) == 4194304) {
                i2 += CodedOutputStream.computeBytesSize(23, bM());
            }
            if ((this.f1379b & 8388608) == 8388608) {
                i2 += CodedOutputStream.computeBoolSize(24, this.A);
            }
            if ((this.f1379b & 16777216) == 16777216) {
                i2 += CodedOutputStream.computeBoolSize(25, this.B);
            }
            if ((this.f1379b & 33554432) == 33554432) {
                i2 += CodedOutputStream.computeBytesSize(26, bN());
            }
            if ((this.f1379b & 67108864) == 67108864) {
                i2 += CodedOutputStream.computeBoolSize(27, this.D);
            }
            if ((this.f1379b & 134217728) == 134217728) {
                i2 += CodedOutputStream.computeBoolSize(28, this.E);
            }
            if ((this.f1379b & 268435456) == 268435456) {
                i2 += CodedOutputStream.computeBoolSize(29, this.F);
            }
            if ((this.f1379b & 536870912) == 536870912) {
                i2 += CodedOutputStream.computeBoolSize(30, this.G);
            }
            if ((this.f1379b & 1073741824) == 1073741824) {
                i2 += CodedOutputStream.computeBoolSize(31, this.H);
            }
            if ((this.f1379b & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
                i2 += CodedOutputStream.computeBoolSize(32, this.I);
            }
            if ((this.f1380c & 1) == 1) {
                i2 += CodedOutputStream.computeBoolSize(33, this.J);
            }
            if ((this.f1380c & 2) == 2) {
                i2 += CodedOutputStream.computeBoolSize(34, this.K);
            }
            if ((this.f1380c & 4) == 4) {
                i2 += CodedOutputStream.computeBoolSize(35, this.L);
            }
            if ((this.f1380c & 8) == 8) {
                i2 += CodedOutputStream.computeBoolSize(36, this.M);
            }
            if ((this.f1380c & 16) == 16) {
                i2 += CodedOutputStream.computeBoolSize(37, this.N);
            }
            if ((this.f1380c & 32) == 32) {
                i2 += CodedOutputStream.computeBytesSize(38, bO());
            }
            if ((this.f1380c & 64) == 64) {
                i2 += CodedOutputStream.computeInt32Size(39, this.P);
            }
            if ((this.f1380c & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeInt32Size(40, this.Q);
            }
            if ((this.f1380c & 256) == 256) {
                i2 += CodedOutputStream.computeInt32Size(41, this.R);
            }
            if ((this.f1380c & 512) == 512) {
                i2 += CodedOutputStream.computeBoolSize(42, this.S);
            }
            if ((this.f1380c & 1024) == 1024) {
                i2 += CodedOutputStream.computeBoolSize(43, this.T);
            }
            if ((this.f1380c & 2048) == 2048) {
                i2 += CodedOutputStream.computeBoolSize(44, this.U);
            }
            if ((this.f1380c & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
                i2 += CodedOutputStream.computeBoolSize(45, this.V);
            }
            if ((this.f1380c & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
                i2 += CodedOutputStream.computeBoolSize(46, this.W);
            }
            if ((this.f1380c & 16384) == 16384) {
                i2 += CodedOutputStream.computeBoolSize(47, this.X);
            }
            if ((this.f1380c & 32768) == 32768) {
                i2 += CodedOutputStream.computeBoolSize(48, this.Y);
            }
            if ((this.f1380c & Menu.CATEGORY_CONTAINER) == 65536) {
                i2 += CodedOutputStream.computeBoolSize(49, this.Z);
            }
            if ((this.f1380c & Menu.CATEGORY_SYSTEM) == 131072) {
                i2 += CodedOutputStream.computeBoolSize(50, this.aa);
            }
            if ((this.f1380c & Menu.CATEGORY_ALTERNATIVE) == 262144) {
                i2 += CodedOutputStream.computeBoolSize(51, this.ab);
            }
            if ((this.f1380c & 524288) == 524288) {
                i2 += CodedOutputStream.computeBoolSize(52, this.ac);
            }
            if ((this.f1380c & 1048576) == 1048576) {
                i2 += CodedOutputStream.computeBoolSize(53, this.ad);
            }
            if ((this.f1380c & 2097152) == 2097152) {
                i2 += CodedOutputStream.computeBoolSize(54, this.ae);
            }
            if ((this.f1380c & 4194304) == 4194304) {
                i2 += CodedOutputStream.computeBoolSize(55, this.af);
            }
            if ((this.f1380c & 8388608) == 8388608) {
                i2 += CodedOutputStream.computeBoolSize(56, this.ag);
            }
            if ((this.f1380c & 16777216) == 16777216) {
                i2 += CodedOutputStream.computeInt32Size(57, this.ah);
            }
            if ((this.f1380c & 33554432) == 33554432) {
                i2 += CodedOutputStream.computeDoubleSize(58, this.ai);
            }
            if ((this.f1380c & 67108864) == 67108864) {
                i2 += CodedOutputStream.computeDoubleSize(59, this.aj);
            }
            if ((this.f1380c & 134217728) == 134217728) {
                i2 += CodedOutputStream.computeInt32Size(60, this.ak);
            }
            if ((this.f1380c & 268435456) == 268435456) {
                i2 += CodedOutputStream.computeBoolSize(61, this.al);
            }
            if ((this.f1380c & 536870912) == 536870912) {
                i2 += CodedOutputStream.computeBoolSize(62, this.am);
            }
            if ((this.f1380c & 1073741824) == 1073741824) {
                i2 += CodedOutputStream.computeBoolSize(63, this.an);
            }
            if ((this.f1380c & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
                i2 += CodedOutputStream.computeBoolSize(64, this.ao);
            }
            this.aq = i2;
        }
        return i2;
    }

    public static n bz() {
        return n.g();
    }

    /* renamed from: bA */
    public n newBuilderForType() {
        return bz();
    }

    public static n a(m mVar) {
        return bz().mergeFrom(mVar);
    }

    /* renamed from: bB */
    public n toBuilder() {
        return a(this);
    }

    static {
        f1378a.bP();
    }
}
