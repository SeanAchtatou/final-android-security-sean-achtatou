package com.avast.b.a.a;

import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: AvastToWeb */
public final class an extends GeneratedMessageLite.Builder<am, an> implements as {

    /* renamed from: a  reason: collision with root package name */
    private int f1349a;

    /* renamed from: b  reason: collision with root package name */
    private aq f1350b = aq.UPDATE_SIMULATION;

    /* renamed from: c  reason: collision with root package name */
    private long f1351c;
    private Object d = "";
    private ao e = ao.NONE;
    private int f = -1;
    private Object g = "";
    private Object h = "";
    private Object i = "";
    private Object j = "";

    private an() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static an i() {
        return new an();
    }

    /* renamed from: a */
    public an clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public am getDefaultInstanceForType() {
        return am.a();
    }

    /* renamed from: c */
    public am build() {
        am d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public am d() {
        int i2 = 1;
        am amVar = new am(this);
        int i3 = this.f1349a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        aq unused = amVar.f1348c = this.f1350b;
        if ((i3 & 2) == 2) {
            i2 |= 2;
        }
        long unused2 = amVar.d = this.f1351c;
        if ((i3 & 4) == 4) {
            i2 |= 4;
        }
        Object unused3 = amVar.e = this.d;
        if ((i3 & 8) == 8) {
            i2 |= 8;
        }
        ao unused4 = amVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 16;
        }
        int unused5 = amVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 32;
        }
        Object unused6 = amVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 64;
        }
        Object unused7 = amVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        Object unused8 = amVar.j = this.i;
        if ((i3 & 256) == 256) {
            i2 |= 256;
        }
        Object unused9 = amVar.k = this.j;
        int unused10 = amVar.f1347b = i2;
        return amVar;
    }

    /* renamed from: a */
    public an mergeFrom(am amVar) {
        if (amVar != am.a()) {
            if (amVar.b()) {
                a(amVar.c());
            }
            if (amVar.d()) {
                a(amVar.e());
            }
            if (amVar.f()) {
                a(amVar.g());
            }
            if (amVar.h()) {
                a(amVar.i());
            }
            if (amVar.j()) {
                a(amVar.k());
            }
            if (amVar.l()) {
                b(amVar.m());
            }
            if (amVar.n()) {
                c(amVar.o());
            }
            if (amVar.p()) {
                d(amVar.q());
            }
            if (amVar.r()) {
                e(amVar.s());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (e() && f()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public an mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    aq a2 = aq.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1349a |= 1;
                        this.f1350b = a2;
                        break;
                    }
                case 16:
                    this.f1349a |= 2;
                    this.f1351c = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1349a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    ao a3 = ao.a(codedInputStream.readEnum());
                    if (a3 == null) {
                        break;
                    } else {
                        this.f1349a |= 8;
                        this.e = a3;
                        break;
                    }
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f1349a |= 16;
                    this.f = codedInputStream.readInt32();
                    break;
                case 50:
                    this.f1349a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    this.f1349a |= 64;
                    this.h = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f1349a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                case 74:
                    this.f1349a |= 256;
                    this.j = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1349a & 1) == 1;
    }

    public an a(aq aqVar) {
        if (aqVar == null) {
            throw new NullPointerException();
        }
        this.f1349a |= 1;
        this.f1350b = aqVar;
        return this;
    }

    public boolean f() {
        return (this.f1349a & 2) == 2;
    }

    public an a(long j2) {
        this.f1349a |= 2;
        this.f1351c = j2;
        return this;
    }

    public an a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1349a |= 4;
        this.d = str;
        return this;
    }

    public an a(ao aoVar) {
        if (aoVar == null) {
            throw new NullPointerException();
        }
        this.f1349a |= 8;
        this.e = aoVar;
        return this;
    }

    public an a(int i2) {
        this.f1349a |= 16;
        this.f = i2;
        return this;
    }

    public an b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1349a |= 32;
        this.g = str;
        return this;
    }

    public an c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1349a |= 64;
        this.h = str;
        return this;
    }

    public an d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1349a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = str;
        return this;
    }

    public an e(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1349a |= 256;
        this.j = str;
        return this;
    }
}
