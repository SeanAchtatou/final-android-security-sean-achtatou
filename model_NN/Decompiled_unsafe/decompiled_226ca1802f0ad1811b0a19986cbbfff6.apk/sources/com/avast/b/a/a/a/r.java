package com.avast.b.a.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.view.Menu;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: ATProtoGenerics */
public final class r extends GeneratedMessageLite implements t {

    /* renamed from: a  reason: collision with root package name */
    private static final r f1308a = new r(true);
    /* access modifiers changed from: private */
    public int A;
    private byte B;
    private int C;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1309b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public long f1310c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public Object e;
    /* access modifiers changed from: private */
    public Object f;
    /* access modifiers changed from: private */
    public long g;
    /* access modifiers changed from: private */
    public long h;
    /* access modifiers changed from: private */
    public u i;
    /* access modifiers changed from: private */
    public Object j;
    /* access modifiers changed from: private */
    public Object k;
    /* access modifiers changed from: private */
    public Object l;
    /* access modifiers changed from: private */
    public Object m;
    /* access modifiers changed from: private */
    public long n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public ByteString p;
    /* access modifiers changed from: private */
    public Object q;
    /* access modifiers changed from: private */
    public Object r;
    /* access modifiers changed from: private */
    public int s;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public Object u;
    /* access modifiers changed from: private */
    public Object v;
    /* access modifiers changed from: private */
    public Object w;
    /* access modifiers changed from: private */
    public Object x;
    /* access modifiers changed from: private */
    public long y;
    /* access modifiers changed from: private */
    public long z;

    private r(s sVar) {
        super(sVar);
        this.B = -1;
        this.C = -1;
    }

    private r(boolean z2) {
        this.B = -1;
        this.C = -1;
    }

    public static r a() {
        return f1308a;
    }

    public boolean b() {
        return (this.f1309b & 1) == 1;
    }

    public long c() {
        return this.f1310c;
    }

    public boolean d() {
        return (this.f1309b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ac() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1309b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ad() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean h() {
        return (this.f1309b & 8) == 8;
    }

    public String i() {
        Object obj = this.f;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ae() {
        Object obj = this.f;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean j() {
        return (this.f1309b & 16) == 16;
    }

    public long k() {
        return this.g;
    }

    public boolean l() {
        return (this.f1309b & 32) == 32;
    }

    public long m() {
        return this.h;
    }

    public boolean n() {
        return (this.f1309b & 64) == 64;
    }

    public u o() {
        return this.i;
    }

    public boolean p() {
        return (this.f1309b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public String q() {
        Object obj = this.j;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.j = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString af() {
        Object obj = this.j;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.j = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean r() {
        return (this.f1309b & 256) == 256;
    }

    public String s() {
        Object obj = this.k;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.k = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ag() {
        Object obj = this.k;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.k = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean t() {
        return (this.f1309b & 512) == 512;
    }

    public String u() {
        Object obj = this.l;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.l = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ah() {
        Object obj = this.l;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.l = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean v() {
        return (this.f1309b & 1024) == 1024;
    }

    public String w() {
        Object obj = this.m;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.m = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ai() {
        Object obj = this.m;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.m = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean x() {
        return (this.f1309b & 2048) == 2048;
    }

    public long y() {
        return this.n;
    }

    public boolean z() {
        return (this.f1309b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096;
    }

    public int A() {
        return this.o;
    }

    public boolean B() {
        return (this.f1309b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192;
    }

    public ByteString C() {
        return this.p;
    }

    public boolean D() {
        return (this.f1309b & 16384) == 16384;
    }

    public String E() {
        Object obj = this.q;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.q = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString aj() {
        Object obj = this.q;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.q = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean F() {
        return (this.f1309b & 32768) == 32768;
    }

    public String G() {
        Object obj = this.r;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.r = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ak() {
        Object obj = this.r;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.r = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean H() {
        return (this.f1309b & Menu.CATEGORY_CONTAINER) == 65536;
    }

    public int I() {
        return this.s;
    }

    public boolean J() {
        return (this.f1309b & Menu.CATEGORY_SYSTEM) == 131072;
    }

    public int K() {
        return this.t;
    }

    public boolean L() {
        return (this.f1309b & Menu.CATEGORY_ALTERNATIVE) == 262144;
    }

    public String M() {
        Object obj = this.u;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.u = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString al() {
        Object obj = this.u;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.u = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean N() {
        return (this.f1309b & 524288) == 524288;
    }

    public String O() {
        Object obj = this.v;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.v = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString am() {
        Object obj = this.v;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.v = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean P() {
        return (this.f1309b & 1048576) == 1048576;
    }

    public String Q() {
        Object obj = this.w;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.w = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString an() {
        Object obj = this.w;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.w = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean R() {
        return (this.f1309b & 2097152) == 2097152;
    }

    public String S() {
        Object obj = this.x;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.x = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ao() {
        Object obj = this.x;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.x = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean T() {
        return (this.f1309b & 4194304) == 4194304;
    }

    public long U() {
        return this.y;
    }

    public boolean V() {
        return (this.f1309b & 8388608) == 8388608;
    }

    public long W() {
        return this.z;
    }

    public boolean X() {
        return (this.f1309b & 16777216) == 16777216;
    }

    public int Y() {
        return this.A;
    }

    private void ap() {
        this.f1310c = 0;
        this.d = "";
        this.e = "";
        this.f = "";
        this.g = 0;
        this.h = 0;
        this.i = u.IMAGE;
        this.j = "";
        this.k = "";
        this.l = "";
        this.m = "";
        this.n = 0;
        this.o = 0;
        this.p = ByteString.EMPTY;
        this.q = "";
        this.r = "";
        this.s = 0;
        this.t = 0;
        this.u = "";
        this.v = "";
        this.w = "";
        this.x = "";
        this.y = 0;
        this.z = 0;
        this.A = 0;
    }

    public final boolean isInitialized() {
        byte b2 = this.B;
        if (b2 != -1) {
            if (b2 == 1) {
                return true;
            }
            return false;
        } else if (!b()) {
            this.B = 0;
            return false;
        } else {
            this.B = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1309b & 1) == 1) {
            codedOutputStream.writeInt64(1, this.f1310c);
        }
        if ((this.f1309b & 2) == 2) {
            codedOutputStream.writeBytes(2, ac());
        }
        if ((this.f1309b & 4) == 4) {
            codedOutputStream.writeBytes(3, ad());
        }
        if ((this.f1309b & 8) == 8) {
            codedOutputStream.writeBytes(4, ae());
        }
        if ((this.f1309b & 16) == 16) {
            codedOutputStream.writeInt64(5, this.g);
        }
        if ((this.f1309b & 32) == 32) {
            codedOutputStream.writeInt64(6, this.h);
        }
        if ((this.f1309b & 64) == 64) {
            codedOutputStream.writeEnum(7, this.i.getNumber());
        }
        if ((this.f1309b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBytes(8, af());
        }
        if ((this.f1309b & 256) == 256) {
            codedOutputStream.writeBytes(9, ag());
        }
        if ((this.f1309b & 512) == 512) {
            codedOutputStream.writeBytes(10, ah());
        }
        if ((this.f1309b & 1024) == 1024) {
            codedOutputStream.writeBytes(11, ai());
        }
        if ((this.f1309b & 2048) == 2048) {
            codedOutputStream.writeInt64(12, this.n);
        }
        if ((this.f1309b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            codedOutputStream.writeInt32(13, this.o);
        }
        if ((this.f1309b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            codedOutputStream.writeBytes(14, this.p);
        }
        if ((this.f1309b & 16384) == 16384) {
            codedOutputStream.writeBytes(15, aj());
        }
        if ((this.f1309b & 32768) == 32768) {
            codedOutputStream.writeBytes(16, ak());
        }
        if ((this.f1309b & Menu.CATEGORY_CONTAINER) == 65536) {
            codedOutputStream.writeInt32(17, this.s);
        }
        if ((this.f1309b & Menu.CATEGORY_SYSTEM) == 131072) {
            codedOutputStream.writeInt32(18, this.t);
        }
        if ((this.f1309b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            codedOutputStream.writeBytes(19, al());
        }
        if ((this.f1309b & 524288) == 524288) {
            codedOutputStream.writeBytes(20, am());
        }
        if ((this.f1309b & 1048576) == 1048576) {
            codedOutputStream.writeBytes(21, an());
        }
        if ((this.f1309b & 2097152) == 2097152) {
            codedOutputStream.writeBytes(22, ao());
        }
        if ((this.f1309b & 4194304) == 4194304) {
            codedOutputStream.writeInt64(23, this.y);
        }
        if ((this.f1309b & 8388608) == 8388608) {
            codedOutputStream.writeInt64(24, this.z);
        }
        if ((this.f1309b & 16777216) == 16777216) {
            codedOutputStream.writeInt32(25, this.A);
        }
    }

    public int getSerializedSize() {
        int i2 = this.C;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1309b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeInt64Size(1, this.f1310c);
            }
            if ((this.f1309b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, ac());
            }
            if ((this.f1309b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, ad());
            }
            if ((this.f1309b & 8) == 8) {
                i2 += CodedOutputStream.computeBytesSize(4, ae());
            }
            if ((this.f1309b & 16) == 16) {
                i2 += CodedOutputStream.computeInt64Size(5, this.g);
            }
            if ((this.f1309b & 32) == 32) {
                i2 += CodedOutputStream.computeInt64Size(6, this.h);
            }
            if ((this.f1309b & 64) == 64) {
                i2 += CodedOutputStream.computeEnumSize(7, this.i.getNumber());
            }
            if ((this.f1309b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeBytesSize(8, af());
            }
            if ((this.f1309b & 256) == 256) {
                i2 += CodedOutputStream.computeBytesSize(9, ag());
            }
            if ((this.f1309b & 512) == 512) {
                i2 += CodedOutputStream.computeBytesSize(10, ah());
            }
            if ((this.f1309b & 1024) == 1024) {
                i2 += CodedOutputStream.computeBytesSize(11, ai());
            }
            if ((this.f1309b & 2048) == 2048) {
                i2 += CodedOutputStream.computeInt64Size(12, this.n);
            }
            if ((this.f1309b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
                i2 += CodedOutputStream.computeInt32Size(13, this.o);
            }
            if ((this.f1309b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
                i2 += CodedOutputStream.computeBytesSize(14, this.p);
            }
            if ((this.f1309b & 16384) == 16384) {
                i2 += CodedOutputStream.computeBytesSize(15, aj());
            }
            if ((this.f1309b & 32768) == 32768) {
                i2 += CodedOutputStream.computeBytesSize(16, ak());
            }
            if ((this.f1309b & Menu.CATEGORY_CONTAINER) == 65536) {
                i2 += CodedOutputStream.computeInt32Size(17, this.s);
            }
            if ((this.f1309b & Menu.CATEGORY_SYSTEM) == 131072) {
                i2 += CodedOutputStream.computeInt32Size(18, this.t);
            }
            if ((this.f1309b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
                i2 += CodedOutputStream.computeBytesSize(19, al());
            }
            if ((this.f1309b & 524288) == 524288) {
                i2 += CodedOutputStream.computeBytesSize(20, am());
            }
            if ((this.f1309b & 1048576) == 1048576) {
                i2 += CodedOutputStream.computeBytesSize(21, an());
            }
            if ((this.f1309b & 2097152) == 2097152) {
                i2 += CodedOutputStream.computeBytesSize(22, ao());
            }
            if ((this.f1309b & 4194304) == 4194304) {
                i2 += CodedOutputStream.computeInt64Size(23, this.y);
            }
            if ((this.f1309b & 8388608) == 8388608) {
                i2 += CodedOutputStream.computeInt64Size(24, this.z);
            }
            if ((this.f1309b & 16777216) == 16777216) {
                i2 += CodedOutputStream.computeInt32Size(25, this.A);
            }
            this.C = i2;
        }
        return i2;
    }

    public static s Z() {
        return s.h();
    }

    /* renamed from: aa */
    public s newBuilderForType() {
        return Z();
    }

    public static s a(r rVar) {
        return Z().mergeFrom(rVar);
    }

    /* renamed from: ab */
    public s toBuilder() {
        return a(this);
    }

    static {
        f1308a.ap();
    }
}
