package com.avast.b.a.a;

import com.google.protobuf.Internal;

/* compiled from: AvastToWeb */
public enum aa implements Internal.EnumLite {
    C2DM_SUCCESS(0, 0),
    BATTERY_LOW(1, 1),
    UPDATE_PREPARATION(2, 2),
    BATTERY_LOW_PREPARATION(3, 3),
    BATTERY_LOW_BUSY(4, 4),
    BATTERY_LOW_ERROR(5, 5),
    GEOFENCE_BUSY(6, 6),
    GEOFENCE_ERROR(7, 7),
    GEOFENCE_PREPARATION(8, 8),
    PASSWORD_CHECK_BUSY(9, 9),
    PASSWORD_CHECK_ERROR(10, 10),
    PASSWORD_CHECK_PREPARATION(11, 11),
    PASSWORD_CHECK_CAMERA_ERROR(12, 12),
    PASSWORD_CHECK_PICTURE(13, 13);
    
    private static Internal.EnumLiteMap<aa> o = new ab();
    private final int p;

    public final int getNumber() {
        return this.p;
    }

    public static aa a(int i) {
        switch (i) {
            case 0:
                return C2DM_SUCCESS;
            case 1:
                return BATTERY_LOW;
            case 2:
                return UPDATE_PREPARATION;
            case 3:
                return BATTERY_LOW_PREPARATION;
            case 4:
                return BATTERY_LOW_BUSY;
            case 5:
                return BATTERY_LOW_ERROR;
            case 6:
                return GEOFENCE_BUSY;
            case 7:
                return GEOFENCE_ERROR;
            case 8:
                return GEOFENCE_PREPARATION;
            case 9:
                return PASSWORD_CHECK_BUSY;
            case 10:
                return PASSWORD_CHECK_ERROR;
            case 11:
                return PASSWORD_CHECK_PREPARATION;
            case 12:
                return PASSWORD_CHECK_CAMERA_ERROR;
            case 13:
                return PASSWORD_CHECK_PICTURE;
            default:
                return null;
        }
    }

    private aa(int i, int i2) {
        this.p = i2;
    }
}
