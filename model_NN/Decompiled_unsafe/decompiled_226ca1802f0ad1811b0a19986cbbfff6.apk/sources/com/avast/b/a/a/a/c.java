package com.avast.b.a.a.a;

import com.google.protobuf.Internal;

/* compiled from: ATProtoGenerics */
public enum c implements Internal.EnumLite {
    NO_RESTRICTION(0, 0),
    FIRST_BATTERY_LEVEL(1, 1),
    SECOND_BATTERY_LEVEL(2, 2),
    THIRD_BATTERY_LEVEL(3, 3);
    
    private static Internal.EnumLiteMap<c> e = new d();
    private final int f;

    public final int getNumber() {
        return this.f;
    }

    public static c a(int i) {
        switch (i) {
            case 0:
                return NO_RESTRICTION;
            case 1:
                return FIRST_BATTERY_LEVEL;
            case 2:
                return SECOND_BATTERY_LEVEL;
            case 3:
                return THIRD_BATTERY_LEVEL;
            default:
                return null;
        }
    }

    private c(int i, int i2) {
        this.f = i2;
    }
}
