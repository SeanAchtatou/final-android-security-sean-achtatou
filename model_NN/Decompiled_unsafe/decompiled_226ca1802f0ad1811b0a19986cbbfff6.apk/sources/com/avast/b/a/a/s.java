package com.avast.b.a.a;

import com.avast.b.a.a.a.m;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: AvastToWeb */
public final class s extends GeneratedMessageLite implements u {

    /* renamed from: a  reason: collision with root package name */
    private static final s f1390a = new s(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1391b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1392c;
    /* access modifiers changed from: private */
    public m d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public Object f;
    /* access modifiers changed from: private */
    public Object g;
    private byte h;
    private int i;

    private s(t tVar) {
        super(tVar);
        this.h = -1;
        this.i = -1;
    }

    private s(boolean z) {
        this.h = -1;
        this.i = -1;
    }

    public static s a() {
        return f1390a;
    }

    public boolean b() {
        return (this.f1391b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1392c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1392c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString o() {
        Object obj = this.f1392c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1392c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1391b & 2) == 2;
    }

    public m e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1391b & 4) == 4;
    }

    public boolean g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1391b & 8) == 8;
    }

    public String i() {
        Object obj = this.f;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString p() {
        Object obj = this.f;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean j() {
        return (this.f1391b & 16) == 16;
    }

    public String k() {
        Object obj = this.g;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.g = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString q() {
        Object obj = this.g;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.g = copyFromUtf8;
        return copyFromUtf8;
    }

    private void r() {
        this.f1392c = "";
        this.d = m.NONE;
        this.e = false;
        this.f = "";
        this.g = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.h;
        if (b2 != -1) {
            if (b2 == 1) {
                return true;
            }
            return false;
        } else if (!b()) {
            this.h = 0;
            return false;
        } else {
            this.h = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1391b & 1) == 1) {
            codedOutputStream.writeBytes(1, o());
        }
        if ((this.f1391b & 2) == 2) {
            codedOutputStream.writeEnum(2, this.d.getNumber());
        }
        if ((this.f1391b & 4) == 4) {
            codedOutputStream.writeBool(3, this.e);
        }
        if ((this.f1391b & 8) == 8) {
            codedOutputStream.writeBytes(4, p());
        }
        if ((this.f1391b & 16) == 16) {
            codedOutputStream.writeBytes(5, q());
        }
    }

    public int getSerializedSize() {
        int i2 = this.i;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1391b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, o());
            }
            if ((this.f1391b & 2) == 2) {
                i2 += CodedOutputStream.computeEnumSize(2, this.d.getNumber());
            }
            if ((this.f1391b & 4) == 4) {
                i2 += CodedOutputStream.computeBoolSize(3, this.e);
            }
            if ((this.f1391b & 8) == 8) {
                i2 += CodedOutputStream.computeBytesSize(4, p());
            }
            if ((this.f1391b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(5, q());
            }
            this.i = i2;
        }
        return i2;
    }

    public static t l() {
        return t.h();
    }

    /* renamed from: m */
    public t newBuilderForType() {
        return l();
    }

    public static t a(s sVar) {
        return l().mergeFrom(sVar);
    }

    /* renamed from: n */
    public t toBuilder() {
        return a(this);
    }

    static {
        f1390a.r();
    }
}
