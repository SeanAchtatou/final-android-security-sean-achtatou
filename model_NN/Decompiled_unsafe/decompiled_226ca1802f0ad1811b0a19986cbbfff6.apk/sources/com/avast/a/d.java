package com.avast.a;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/* compiled from: SymKeyUtility */
public class d {
    public static byte[] a(byte[] bArr, long j) {
        try {
            return b.a(b.a(e.a(j), bArr), "".getBytes(), bArr.length);
        } catch (IOException e) {
            throw new c(e);
        } catch (InvalidKeyException e2) {
            throw new c(e2);
        } catch (NoSuchAlgorithmException e3) {
            throw new c(e3);
        }
    }
}
