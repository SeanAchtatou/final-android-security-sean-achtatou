package com.avast.a;

import java.security.Key;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: HKDF */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public static String f224a = "HmacSHA1";

    /* renamed from: b  reason: collision with root package name */
    static final /* synthetic */ boolean f225b = (!b.class.desiredAssertionStatus());

    private static Key a(byte[] bArr) {
        if (bArr.length == 0) {
            bArr = new byte[20];
        }
        return new SecretKeySpec(bArr, f224a);
    }

    private static byte[] a(byte[] bArr, Mac mac) {
        mac.update(bArr);
        byte[] doFinal = mac.doFinal();
        mac.reset();
        return doFinal;
    }

    private static Mac b(byte[] bArr) {
        Mac instance = Mac.getInstance(f224a);
        if (f225b || instance != null) {
            instance.init(a(bArr));
            return instance;
        }
        throw new AssertionError();
    }

    private static byte[] a(String str) {
        if (str.length() % 2 == 1) {
            str = "0" + str;
        }
        byte[] bArr = new byte[(str.length() / 2)];
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = (byte) Integer.parseInt(str.substring(i * 2, (i * 2) + 2), 16);
        }
        return bArr;
    }

    private static byte[] a(byte[] bArr, byte[]... bArr2) {
        int length = bArr.length + 0;
        for (byte[] length2 : bArr2) {
            length += length2.length;
        }
        byte[] bArr3 = new byte[length];
        int length3 = bArr.length;
        System.arraycopy(bArr, 0, bArr3, 0, length3);
        int i = length3;
        for (byte[] bArr4 : bArr2) {
            System.arraycopy(bArr4, 0, bArr3, i, bArr4.length);
            i += bArr4.length;
        }
        return bArr3;
    }

    public static byte[] a(byte[] bArr, byte[] bArr2) {
        return a(bArr2, b(bArr));
    }

    public static byte[] a(byte[] bArr, byte[] bArr2, int i) {
        Mac b2 = b(bArr);
        int ceil = (int) Math.ceil(((double) i) / 20.0d);
        byte[] bArr3 = new byte[0];
        byte[] bArr4 = new byte[0];
        for (int i2 = 0; i2 < ceil; i2++) {
            bArr3 = a(a(bArr3, bArr2, a(Integer.toHexString(i2 + 1))), b2);
            bArr4 = a(bArr4, bArr3);
        }
        byte[] bArr5 = new byte[i];
        System.arraycopy(bArr4, 0, bArr5, 0, i);
        return bArr5;
    }
}
