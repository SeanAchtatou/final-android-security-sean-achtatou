package com.avast.a.a;

import java.io.IOException;

/* compiled from: ByteRingBuffer */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f211a;

    /* renamed from: b  reason: collision with root package name */
    private final int f212b;

    /* renamed from: c  reason: collision with root package name */
    private int f213c = 0;
    private int d = 0;
    private int e = 0;

    public a(int i) {
        this.f212b = i;
        this.f211a = new byte[i];
    }

    public int a() {
        return this.e;
    }

    public int b() {
        return this.f212b - this.e;
    }

    public int c() {
        return this.f212b;
    }

    public int a(byte[] bArr) {
        int i;
        int min = Math.min(bArr.length, this.e);
        int min2 = Math.min(this.f212b - this.f213c, min);
        if (min2 > 0) {
            System.arraycopy(this.f211a, this.f213c, bArr, 0, min2);
            i = 0 + min2;
            min -= min2;
            this.f213c = min2 + this.f213c;
        } else {
            i = 0;
        }
        if (this.f213c == this.f212b) {
            this.f213c = 0;
        }
        if (min > 0) {
            System.arraycopy(this.f211a, this.f213c, bArr, i, min);
            i += min;
            this.f213c += min;
        }
        this.e -= i;
        return i;
    }

    public void a(byte[] bArr, int i, int i2) {
        int i3;
        if (i2 > b()) {
            throw new IOException("RingBuffer overflow (len " + i2 + ", c " + this.f212b + ", r " + this.f213c + ", w " + this.d + ", v " + this.e + ").");
        }
        int min = Math.min(this.f212b - this.d, i2);
        if (min > 0) {
            System.arraycopy(bArr, 0 + i, this.f211a, this.d, min);
            i3 = 0 + min;
            i2 -= min;
            this.d = min + this.d;
        } else {
            i3 = 0;
        }
        if (this.d == this.f212b) {
            this.d = 0;
        }
        if (i2 > 0) {
            System.arraycopy(bArr, i3 + i, this.f211a, this.d, i2);
            i3 += i2;
            this.d += i2;
        }
        this.e = i3 + this.e;
    }
}
