package com.avast.a.a;

import java.io.OutputStream;
import javax.crypto.Mac;

/* compiled from: HmacOutputStream */
public class e extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final Mac f222a;

    /* renamed from: b  reason: collision with root package name */
    private final OutputStream f223b;

    public e(Mac mac, OutputStream outputStream) {
        this.f222a = mac;
        this.f223b = outputStream;
    }

    public byte[] a() {
        return this.f222a.doFinal();
    }

    public void write(int i) {
        this.f222a.update((byte) i);
        this.f223b.write(i);
    }

    public void write(byte[] bArr) {
        this.f222a.update(bArr);
        this.f223b.write(bArr);
    }

    public void write(byte[] bArr, int i, int i2) {
        this.f222a.update(bArr, i, i2);
        this.f223b.write(bArr, i, i2);
    }

    public void flush() {
        this.f223b.flush();
    }

    public void close() {
        this.f223b.close();
    }
}
