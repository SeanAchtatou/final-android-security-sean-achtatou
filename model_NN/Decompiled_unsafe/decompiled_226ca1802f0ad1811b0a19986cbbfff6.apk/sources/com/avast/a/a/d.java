package com.avast.a.a;

import com.avast.a.a;
import java.io.OutputStream;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.Mac;

/* compiled from: EncryptingOutputStream */
public class d extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final OutputStream f219a;

    /* renamed from: b  reason: collision with root package name */
    private final CipherOutputStream f220b;

    /* renamed from: c  reason: collision with root package name */
    private final Mac f221c;

    public d(OutputStream outputStream, byte[] bArr) {
        byte[] bArr2 = new byte[16];
        new SecureRandom().nextBytes(bArr2);
        outputStream.write(bArr2);
        Cipher a2 = a.a(bArr, bArr2, 1);
        this.f221c = a.a(bArr);
        this.f219a = outputStream;
        this.f220b = new CipherOutputStream(new c(outputStream, false), a2);
    }

    public void write(int i) {
        this.f221c.update((byte) i);
        this.f220b.write(i);
    }

    public void write(byte[] bArr) {
        this.f221c.update(bArr);
        this.f220b.write(bArr);
    }

    public void write(byte[] bArr, int i, int i2) {
        this.f221c.update(bArr, i, i2);
        this.f220b.write(bArr, i, i2);
    }

    public void flush() {
        this.f220b.flush();
    }

    public void close() {
        this.f220b.close();
        this.f219a.write(this.f221c.doFinal());
        this.f219a.close();
    }
}
