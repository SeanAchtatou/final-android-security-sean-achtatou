package com.avast.a.a;

import java.io.OutputStream;

/* compiled from: DelegateOutputStream */
public class c extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final OutputStream f217a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f218b;

    public c(OutputStream outputStream, boolean z) {
        this.f217a = outputStream;
        this.f218b = z;
    }

    public void write(int i) {
        this.f217a.write(i);
    }

    public void write(byte[] bArr) {
        this.f217a.write(bArr);
    }

    public void write(byte[] bArr, int i, int i2) {
        this.f217a.write(bArr, i, i2);
    }

    public void flush() {
        this.f217a.flush();
    }

    public void close() {
        if (this.f218b) {
            this.f217a.close();
        }
    }
}
