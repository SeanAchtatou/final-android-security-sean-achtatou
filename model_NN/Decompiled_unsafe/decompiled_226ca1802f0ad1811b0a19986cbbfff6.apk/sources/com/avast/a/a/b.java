package com.avast.a.a;

import com.avast.a.a;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import javax.crypto.CipherOutputStream;

/* compiled from: DecryptingHmacOutputStream */
public class b extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f214a = (!b.class.desiredAssertionStatus());

    /* renamed from: b  reason: collision with root package name */
    private final OutputStream f215b;

    /* renamed from: c  reason: collision with root package name */
    private final byte[] f216c;
    private final a d = new a(Math.max(Math.max(16, 20), 1024));
    private CipherOutputStream e = null;
    private e f = null;

    public b(OutputStream outputStream, byte[] bArr) {
        this.f215b = outputStream;
        this.f216c = bArr;
    }

    public void write(int i) {
        write(new byte[]{(byte) i});
    }

    public void write(byte[] bArr, int i, int i2) {
        if (this.e == null && this.d.a() + i2 >= 16) {
            byte[] bArr2 = new byte[16];
            int a2 = this.d.a(bArr2);
            int i3 = 16 - a2;
            if (i3 > 0) {
                System.arraycopy(bArr, i, bArr2, a2, i3);
            }
            try {
                this.f = new e(a.a(this.f216c), this.f215b);
                try {
                    this.e = new CipherOutputStream(this.f, a.a(this.f216c, bArr2, 2));
                    if (i3 > 0) {
                        write(bArr, i + i3, i2 - i3);
                    }
                } catch (Exception e2) {
                    throw new IOException("Can't initialize Cipher", e2);
                }
            } catch (Exception e3) {
                throw new IOException("Can't initialize HMac", e3);
            }
        } else if (this.e == null) {
            this.d.a(bArr, i, i2);
        } else {
            int a3 = this.d.a();
            int c2 = (i2 + a3) - this.d.c();
            if (c2 > 0) {
                byte[] bArr3 = new byte[c2];
                int a4 = this.d.a(bArr3);
                if (f214a || a4 == a3) {
                    int i4 = c2 - a4;
                    this.e.write(bArr3, 0, a4);
                    if (i4 > 0) {
                        this.e.write(bArr, i, i4);
                        i += i4;
                        i2 -= i4;
                    }
                } else {
                    throw new AssertionError("Readed too few bytes");
                }
            }
            this.d.a(bArr, i, i2);
        }
    }

    public void flush() {
        this.f215b.flush();
    }

    public void close() {
        if (this.e == null) {
            throw new IOException("to few bytes read to initialize");
        }
        byte[] bArr = new byte[this.d.a()];
        if (bArr.length < 20) {
            throw new IOException("to few bytes read for checksum");
        }
        this.d.a(bArr);
        this.e.write(bArr, 0, bArr.length - 20);
        this.e.close();
        byte[] bArr2 = new byte[20];
        System.arraycopy(bArr, bArr.length - 20, bArr2, 0, 20);
        if (!Arrays.equals(this.f.a(), bArr2)) {
            throw new IOException("HMac verification fails");
        }
    }
}
