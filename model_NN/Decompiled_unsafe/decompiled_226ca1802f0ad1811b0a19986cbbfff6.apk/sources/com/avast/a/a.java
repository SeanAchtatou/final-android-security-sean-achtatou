package com.avast.a;

import com.avast.a.a.b;
import com.avast.a.a.d;
import java.io.OutputStream;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: CryptoHelper */
public class a {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f210a = (!a.class.desiredAssertionStatus());

    private a() {
    }

    public static Mac a(byte[] bArr) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "HmacSHA1");
        Mac instance = Mac.getInstance("HmacSHA1");
        instance.init(secretKeySpec);
        return instance;
    }

    public static byte[] a(byte[] bArr, long j) {
        return d.a(bArr, j);
    }

    public static OutputStream a(OutputStream outputStream, byte[] bArr) {
        return new d(outputStream, bArr);
    }

    public static OutputStream b(OutputStream outputStream, byte[] bArr) {
        return new b(outputStream, bArr);
    }

    public static Cipher a(byte[] bArr, byte[] bArr2, int i) {
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(i, new SecretKeySpec(bArr, "AES"), new IvParameterSpec(bArr2));
        return instance;
    }
}
