package com.avast.a;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

/* compiled from: Utility */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f226a = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70};

    public static byte[] a(long j) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8);
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        dataOutputStream.writeLong(j);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        dataOutputStream.close();
        return byteArray;
    }
}
