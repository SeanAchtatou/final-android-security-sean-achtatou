package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class ax extends GeneratedMessageLite.Builder<aw, ax> implements ay {

    /* renamed from: a  reason: collision with root package name */
    private int f1605a;

    /* renamed from: b  reason: collision with root package name */
    private aq f1606b = aq.a();

    /* renamed from: c  reason: collision with root package name */
    private ByteString f1607c = ByteString.EMPTY;

    private ax() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static ax i() {
        return new ax();
    }

    /* renamed from: a */
    public ax clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public aw getDefaultInstanceForType() {
        return aw.a();
    }

    /* renamed from: c */
    public aw build() {
        aw d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public aw d() {
        int i = 1;
        aw awVar = new aw(this);
        int i2 = this.f1605a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        aq unused = awVar.f1604c = this.f1606b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        ByteString unused2 = awVar.d = this.f1607c;
        int unused3 = awVar.f1603b = i;
        return awVar;
    }

    /* renamed from: a */
    public ax mergeFrom(aw awVar) {
        if (awVar != aw.a()) {
            if (awVar.b()) {
                b(awVar.c());
            }
            if (awVar.d()) {
                a(awVar.e());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public ax mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    ar j = aq.j();
                    if (e()) {
                        j.mergeFrom(f());
                    }
                    codedInputStream.readMessage(j, extensionRegistryLite);
                    a(j.d());
                    break;
                case 18:
                    this.f1605a |= 2;
                    this.f1607c = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1605a & 1) == 1;
    }

    public aq f() {
        return this.f1606b;
    }

    public ax a(aq aqVar) {
        if (aqVar == null) {
            throw new NullPointerException();
        }
        this.f1606b = aqVar;
        this.f1605a |= 1;
        return this;
    }

    public ax b(aq aqVar) {
        if ((this.f1605a & 1) != 1 || this.f1606b == aq.a()) {
            this.f1606b = aqVar;
        } else {
            this.f1606b = aq.a(this.f1606b).mergeFrom(aqVar).d();
        }
        this.f1605a |= 1;
        return this;
    }

    public ax a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1605a |= 2;
        this.f1607c = byteString;
        return this;
    }
}
