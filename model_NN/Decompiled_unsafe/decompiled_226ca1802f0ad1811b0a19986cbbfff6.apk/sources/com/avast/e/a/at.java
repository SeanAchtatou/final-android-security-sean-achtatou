package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class at extends GeneratedMessageLite implements av {

    /* renamed from: a  reason: collision with root package name */
    private static final at f1596a = new at(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1597b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f1598c;
    /* access modifiers changed from: private */
    public ByteString d;
    private byte e;
    private int f;

    private at(au auVar) {
        super(auVar);
        this.e = -1;
        this.f = -1;
    }

    private at(boolean z) {
        this.e = -1;
        this.f = -1;
    }

    public static at a() {
        return f1596a;
    }

    public boolean b() {
        return (this.f1597b & 1) == 1;
    }

    public ByteString c() {
        return this.f1598c;
    }

    public boolean d() {
        return (this.f1597b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    private void i() {
        this.f1598c = ByteString.EMPTY;
        this.d = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.e;
        if (b2 == -1) {
            this.e = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1597b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f1598c);
        }
        if ((this.f1597b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
    }

    public int getSerializedSize() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if ((this.f1597b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, this.f1598c);
            }
            if ((this.f1597b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, this.d);
            }
            this.f = i;
        }
        return i;
    }

    public static au f() {
        return au.g();
    }

    /* renamed from: g */
    public au newBuilderForType() {
        return f();
    }

    public static au a(at atVar) {
        return f().mergeFrom(atVar);
    }

    /* renamed from: h */
    public au toBuilder() {
        return a(this);
    }

    static {
        f1596a.i();
    }
}
