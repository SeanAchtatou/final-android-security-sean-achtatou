package com.avast.e.a;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class ar extends GeneratedMessageLite.Builder<aq, ar> implements as {

    /* renamed from: a  reason: collision with root package name */
    private int f1593a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f1594b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private ByteString f1595c = ByteString.EMPTY;
    private ByteString d = ByteString.EMPTY;
    private ByteString e = ByteString.EMPTY;

    private ar() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static ar g() {
        return new ar();
    }

    /* renamed from: a */
    public ar clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public aq getDefaultInstanceForType() {
        return aq.a();
    }

    /* renamed from: c */
    public aq build() {
        aq d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public aq d() {
        int i = 1;
        aq aqVar = new aq(this);
        int i2 = this.f1593a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        ByteString unused = aqVar.f1592c = this.f1594b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        ByteString unused2 = aqVar.d = this.f1595c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        ByteString unused3 = aqVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 8;
        }
        ByteString unused4 = aqVar.f = this.e;
        int unused5 = aqVar.f1591b = i;
        return aqVar;
    }

    /* renamed from: a */
    public ar mergeFrom(aq aqVar) {
        if (aqVar != aq.a()) {
            if (aqVar.b()) {
                a(aqVar.c());
            }
            if (aqVar.d()) {
                b(aqVar.e());
            }
            if (aqVar.f()) {
                c(aqVar.g());
            }
            if (aqVar.h()) {
                d(aqVar.i());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public ar mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1593a |= 1;
                    this.f1594b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1593a |= 2;
                    this.f1595c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1593a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f1593a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public ar a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1593a |= 1;
        this.f1594b = byteString;
        return this;
    }

    public ar b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1593a |= 2;
        this.f1595c = byteString;
        return this;
    }

    public ar c(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1593a |= 4;
        this.d = byteString;
        return this;
    }

    public ar d(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1593a |= 8;
        this.e = byteString;
        return this;
    }
}
