package com.avast.e.a;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ParamsProto */
public final class ba extends GeneratedMessageLite.Builder<az, ba> implements bd {

    /* renamed from: a  reason: collision with root package name */
    private int f1611a;

    /* renamed from: b  reason: collision with root package name */
    private bb f1612b = bb.AMS;

    /* renamed from: c  reason: collision with root package name */
    private List<ByteString> f1613c = Collections.emptyList();
    private an d = an.a();
    private af e = af.a();
    private aw f = aw.a();
    private v g = v.a();
    private aa h = aa.a();

    private ba() {
        p();
    }

    private void p() {
    }

    /* access modifiers changed from: private */
    public static ba q() {
        return new ba();
    }

    /* renamed from: a */
    public ba clone() {
        return q().mergeFrom(d());
    }

    /* renamed from: b */
    public az getDefaultInstanceForType() {
        return az.a();
    }

    /* renamed from: c */
    public az build() {
        az d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public az d() {
        int i = 1;
        az azVar = new az(this);
        int i2 = this.f1611a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        bb unused = azVar.f1610c = this.f1612b;
        if ((this.f1611a & 2) == 2) {
            this.f1613c = Collections.unmodifiableList(this.f1613c);
            this.f1611a &= -3;
        }
        List unused2 = azVar.d = this.f1613c;
        if ((i2 & 4) == 4) {
            i |= 2;
        }
        an unused3 = azVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 4;
        }
        af unused4 = azVar.f = this.e;
        if ((i2 & 16) == 16) {
            i |= 8;
        }
        aw unused5 = azVar.g = this.f;
        if ((i2 & 32) == 32) {
            i |= 16;
        }
        v unused6 = azVar.h = this.g;
        if ((i2 & 64) == 64) {
            i |= 32;
        }
        aa unused7 = azVar.i = this.h;
        int unused8 = azVar.f1609b = i;
        return azVar;
    }

    /* renamed from: a */
    public ba mergeFrom(az azVar) {
        if (azVar != az.a()) {
            if (azVar.b()) {
                a(azVar.c());
            }
            if (!azVar.d.isEmpty()) {
                if (this.f1613c.isEmpty()) {
                    this.f1613c = azVar.d;
                    this.f1611a &= -3;
                } else {
                    r();
                    this.f1613c.addAll(azVar.d);
                }
            }
            if (azVar.e()) {
                b(azVar.f());
            }
            if (azVar.g()) {
                b(azVar.h());
            }
            if (azVar.i()) {
                b(azVar.j());
            }
            if (azVar.k()) {
                b(azVar.l());
            }
            if (azVar.m()) {
                b(azVar.n());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public ba mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    bb a2 = bb.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1611a |= 1;
                        this.f1612b = a2;
                        break;
                    }
                case 18:
                    r();
                    this.f1613c.add(codedInputStream.readBytes());
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    ao l = an.l();
                    if (e()) {
                        l.mergeFrom(f());
                    }
                    codedInputStream.readMessage(l, extensionRegistryLite);
                    a(l.d());
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    ai h2 = af.h();
                    if (g()) {
                        h2.mergeFrom(h());
                    }
                    codedInputStream.readMessage(h2, extensionRegistryLite);
                    a(h2.d());
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    ax f2 = aw.f();
                    if (i()) {
                        f2.mergeFrom(j());
                    }
                    codedInputStream.readMessage(f2, extensionRegistryLite);
                    a(f2.d());
                    break;
                case 50:
                    y f3 = v.f();
                    if (k()) {
                        f3.mergeFrom(l());
                    }
                    codedInputStream.readMessage(f3, extensionRegistryLite);
                    a(f3.d());
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    ad f4 = aa.f();
                    if (m()) {
                        f4.mergeFrom(n());
                    }
                    codedInputStream.readMessage(f4, extensionRegistryLite);
                    a(f4.d());
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public ba a(bb bbVar) {
        if (bbVar == null) {
            throw new NullPointerException();
        }
        this.f1611a |= 1;
        this.f1612b = bbVar;
        return this;
    }

    private void r() {
        if ((this.f1611a & 2) != 2) {
            this.f1613c = new ArrayList(this.f1613c);
            this.f1611a |= 2;
        }
    }

    public ba a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        r();
        this.f1613c.add(byteString);
        return this;
    }

    public boolean e() {
        return (this.f1611a & 4) == 4;
    }

    public an f() {
        return this.d;
    }

    public ba a(an anVar) {
        if (anVar == null) {
            throw new NullPointerException();
        }
        this.d = anVar;
        this.f1611a |= 4;
        return this;
    }

    public ba a(ao aoVar) {
        this.d = aoVar.build();
        this.f1611a |= 4;
        return this;
    }

    public ba b(an anVar) {
        if ((this.f1611a & 4) != 4 || this.d == an.a()) {
            this.d = anVar;
        } else {
            this.d = an.a(this.d).mergeFrom(anVar).d();
        }
        this.f1611a |= 4;
        return this;
    }

    public boolean g() {
        return (this.f1611a & 8) == 8;
    }

    public af h() {
        return this.e;
    }

    public ba a(af afVar) {
        if (afVar == null) {
            throw new NullPointerException();
        }
        this.e = afVar;
        this.f1611a |= 8;
        return this;
    }

    public ba a(ai aiVar) {
        this.e = aiVar.build();
        this.f1611a |= 8;
        return this;
    }

    public ba b(af afVar) {
        if ((this.f1611a & 8) != 8 || this.e == af.a()) {
            this.e = afVar;
        } else {
            this.e = af.a(this.e).mergeFrom(afVar).d();
        }
        this.f1611a |= 8;
        return this;
    }

    public boolean i() {
        return (this.f1611a & 16) == 16;
    }

    public aw j() {
        return this.f;
    }

    public ba a(aw awVar) {
        if (awVar == null) {
            throw new NullPointerException();
        }
        this.f = awVar;
        this.f1611a |= 16;
        return this;
    }

    public ba b(aw awVar) {
        if ((this.f1611a & 16) != 16 || this.f == aw.a()) {
            this.f = awVar;
        } else {
            this.f = aw.a(this.f).mergeFrom(awVar).d();
        }
        this.f1611a |= 16;
        return this;
    }

    public boolean k() {
        return (this.f1611a & 32) == 32;
    }

    public v l() {
        return this.g;
    }

    public ba a(v vVar) {
        if (vVar == null) {
            throw new NullPointerException();
        }
        this.g = vVar;
        this.f1611a |= 32;
        return this;
    }

    public ba a(y yVar) {
        this.g = yVar.build();
        this.f1611a |= 32;
        return this;
    }

    public ba b(v vVar) {
        if ((this.f1611a & 32) != 32 || this.g == v.a()) {
            this.g = vVar;
        } else {
            this.g = v.a(this.g).mergeFrom(vVar).d();
        }
        this.f1611a |= 32;
        return this;
    }

    public boolean m() {
        return (this.f1611a & 64) == 64;
    }

    public aa n() {
        return this.h;
    }

    public ba a(aa aaVar) {
        if (aaVar == null) {
            throw new NullPointerException();
        }
        this.h = aaVar;
        this.f1611a |= 64;
        return this;
    }

    public ba a(ad adVar) {
        this.h = adVar.build();
        this.f1611a |= 64;
        return this;
    }

    public ba b(aa aaVar) {
        if ((this.f1611a & 64) != 64 || this.h == aa.a()) {
            this.h = aaVar;
        } else {
            this.h = aa.a(this.h).mergeFrom(aaVar).d();
        }
        this.f1611a |= 64;
        return this;
    }
}
