package com.avast.e.a;

import com.google.protobuf.Internal;

/* compiled from: ConfigProto */
public enum o implements Internal.EnumLite {
    VERBOSE(0, 1),
    DEBUG(1, 2),
    INFO(2, 3),
    WARNING(3, 4),
    ERROR(4, 5),
    ASSERT(5, 6);
    
    private static Internal.EnumLiteMap<o> g = new p();
    private final int h;

    public final int getNumber() {
        return this.h;
    }

    public static o a(int i2) {
        switch (i2) {
            case 1:
                return VERBOSE;
            case 2:
                return DEBUG;
            case 3:
                return INFO;
            case 4:
                return WARNING;
            case 5:
                return ERROR;
            case 6:
                return ASSERT;
            default:
                return null;
        }
    }

    private o(int i2, int i3) {
        this.h = i3;
    }
}
