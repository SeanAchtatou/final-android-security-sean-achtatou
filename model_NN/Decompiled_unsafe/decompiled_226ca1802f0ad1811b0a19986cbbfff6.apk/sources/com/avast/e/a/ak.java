package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class ak extends GeneratedMessageLite implements am {

    /* renamed from: a  reason: collision with root package name */
    private static final ak f1578a = new ak(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1579b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f1580c;
    /* access modifiers changed from: private */
    public ByteString d;
    /* access modifiers changed from: private */
    public ByteString e;
    /* access modifiers changed from: private */
    public ByteString f;
    /* access modifiers changed from: private */
    public ByteString g;
    private byte h;
    private int i;

    private ak(al alVar) {
        super(alVar);
        this.h = -1;
        this.i = -1;
    }

    private ak(boolean z) {
        this.h = -1;
        this.i = -1;
    }

    public static ak a() {
        return f1578a;
    }

    public boolean b() {
        return (this.f1579b & 1) == 1;
    }

    public ByteString c() {
        return this.f1580c;
    }

    public boolean d() {
        return (this.f1579b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1579b & 4) == 4;
    }

    public ByteString g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1579b & 8) == 8;
    }

    public ByteString i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1579b & 16) == 16;
    }

    public ByteString k() {
        return this.g;
    }

    private void o() {
        this.f1580c = ByteString.EMPTY;
        this.d = ByteString.EMPTY;
        this.e = ByteString.EMPTY;
        this.f = ByteString.EMPTY;
        this.g = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.h;
        if (b2 == -1) {
            this.h = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1579b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f1580c);
        }
        if ((this.f1579b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
        if ((this.f1579b & 4) == 4) {
            codedOutputStream.writeBytes(3, this.e);
        }
        if ((this.f1579b & 8) == 8) {
            codedOutputStream.writeBytes(4, this.f);
        }
        if ((this.f1579b & 16) == 16) {
            codedOutputStream.writeBytes(5, this.g);
        }
    }

    public int getSerializedSize() {
        int i2 = this.i;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1579b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, this.f1580c);
            }
            if ((this.f1579b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, this.d);
            }
            if ((this.f1579b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, this.e);
            }
            if ((this.f1579b & 8) == 8) {
                i2 += CodedOutputStream.computeBytesSize(4, this.f);
            }
            if ((this.f1579b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(5, this.g);
            }
            this.i = i2;
        }
        return i2;
    }

    public static al l() {
        return al.g();
    }

    /* renamed from: m */
    public al newBuilderForType() {
        return l();
    }

    public static al a(ak akVar) {
        return l().mergeFrom(akVar);
    }

    /* renamed from: n */
    public al toBuilder() {
        return a(this);
    }

    static {
        f1578a.o();
    }
}
