package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import java.util.Collections;
import java.util.List;

/* compiled from: ConfigProto */
public final class f extends GeneratedMessageLite implements h {

    /* renamed from: a  reason: collision with root package name */
    private static final f f1622a = new f(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1623b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f1624c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public List<q> e;
    /* access modifiers changed from: private */
    public o f;
    private byte g;
    private int h;

    private f(g gVar) {
        super(gVar);
        this.g = -1;
        this.h = -1;
    }

    private f(boolean z) {
        this.g = -1;
        this.h = -1;
    }

    public static f a() {
        return f1622a;
    }

    public boolean b() {
        return (this.f1623b & 1) == 1;
    }

    public ByteString c() {
        return this.f1624c;
    }

    public boolean d() {
        return (this.f1623b & 2) == 2;
    }

    public boolean e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1623b & 4) == 4;
    }

    public o g() {
        return this.f;
    }

    private void k() {
        this.f1624c = ByteString.EMPTY;
        this.d = false;
        this.e = Collections.emptyList();
        this.f = o.VERBOSE;
    }

    public final boolean isInitialized() {
        byte b2 = this.g;
        if (b2 == -1) {
            this.g = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1623b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f1624c);
        }
        if ((this.f1623b & 2) == 2) {
            codedOutputStream.writeBool(2, this.d);
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.e.size()) {
                break;
            }
            codedOutputStream.writeMessage(3, this.e.get(i2));
            i = i2 + 1;
        }
        if ((this.f1623b & 4) == 4) {
            codedOutputStream.writeEnum(4, this.f.getNumber());
        }
    }

    public int getSerializedSize() {
        int i;
        int i2 = 0;
        int i3 = this.h;
        if (i3 == -1) {
            if ((this.f1623b & 1) == 1) {
                i = CodedOutputStream.computeBytesSize(1, this.f1624c) + 0;
            } else {
                i = 0;
            }
            if ((this.f1623b & 2) == 2) {
                i += CodedOutputStream.computeBoolSize(2, this.d);
            }
            while (true) {
                i3 = i;
                if (i2 >= this.e.size()) {
                    break;
                }
                i = CodedOutputStream.computeMessageSize(3, this.e.get(i2)) + i3;
                i2++;
            }
            if ((this.f1623b & 4) == 4) {
                i3 += CodedOutputStream.computeEnumSize(4, this.f.getNumber());
            }
            this.h = i3;
        }
        return i3;
    }

    public static g h() {
        return g.g();
    }

    /* renamed from: i */
    public g newBuilderForType() {
        return h();
    }

    public static g a(f fVar) {
        return h().mergeFrom(fVar);
    }

    /* renamed from: j */
    public g toBuilder() {
        return a(this);
    }

    static {
        f1622a.k();
    }
}
