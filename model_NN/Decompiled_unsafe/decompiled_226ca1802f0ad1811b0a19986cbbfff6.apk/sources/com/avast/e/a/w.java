package com.avast.e.a;

import com.google.protobuf.Internal;

/* compiled from: ParamsProto */
public enum w implements Internal.EnumLite {
    ADVANCED_FREE(0, 1),
    ADVANCED_PREMIUM(1, 2),
    SIMPLE_FREE(2, 3),
    SIMPLE_PREMIUM(3, 4);
    
    private static Internal.EnumLiteMap<w> e = new x();
    private final int f;

    public final int getNumber() {
        return this.f;
    }

    public static w a(int i) {
        switch (i) {
            case 1:
                return ADVANCED_FREE;
            case 2:
                return ADVANCED_PREMIUM;
            case 3:
                return SIMPLE_FREE;
            case 4:
                return SIMPLE_PREMIUM;
            default:
                return null;
        }
    }

    private w(int i, int i2) {
        this.f = i2;
    }
}
