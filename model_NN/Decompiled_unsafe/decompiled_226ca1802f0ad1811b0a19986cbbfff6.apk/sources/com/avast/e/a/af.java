package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class af extends GeneratedMessageLite implements aj {

    /* renamed from: a  reason: collision with root package name */
    private static final af f1569a = new af(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1570b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public aq f1571c;
    /* access modifiers changed from: private */
    public ByteString d;
    /* access modifiers changed from: private */
    public ag e;
    private byte f;
    private int g;

    private af(ai aiVar) {
        super(aiVar);
        this.f = -1;
        this.g = -1;
    }

    private af(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static af a() {
        return f1569a;
    }

    public boolean b() {
        return (this.f1570b & 1) == 1;
    }

    public aq c() {
        return this.f1571c;
    }

    public boolean d() {
        return (this.f1570b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1570b & 4) == 4;
    }

    public ag g() {
        return this.e;
    }

    private void k() {
        this.f1571c = aq.a();
        this.d = ByteString.EMPTY;
        this.e = ag.FREE;
    }

    public final boolean isInitialized() {
        byte b2 = this.f;
        if (b2 == -1) {
            this.f = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1570b & 1) == 1) {
            codedOutputStream.writeMessage(1, this.f1571c);
        }
        if ((this.f1570b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
        if ((this.f1570b & 4) == 4) {
            codedOutputStream.writeEnum(3, this.e.getNumber());
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            if ((this.f1570b & 1) == 1) {
                i = 0 + CodedOutputStream.computeMessageSize(1, this.f1571c);
            }
            if ((this.f1570b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, this.d);
            }
            if ((this.f1570b & 4) == 4) {
                i += CodedOutputStream.computeEnumSize(3, this.e.getNumber());
            }
            this.g = i;
        }
        return i;
    }

    public static ai h() {
        return ai.i();
    }

    /* renamed from: i */
    public ai newBuilderForType() {
        return h();
    }

    public static ai a(af afVar) {
        return h().mergeFrom(afVar);
    }

    /* renamed from: j */
    public ai toBuilder() {
        return a(this);
    }

    static {
        f1569a.k();
    }
}
