package com.avast.e.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ConfigProto */
public final class m extends GeneratedMessageLite.Builder<l, m> implements n {
    private m() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static m g() {
        return new m();
    }

    /* renamed from: a */
    public m clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public l getDefaultInstanceForType() {
        return l.a();
    }

    /* renamed from: c */
    public l build() {
        l d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public l d() {
        return new l(this);
    }

    /* renamed from: a */
    public m mergeFrom(l lVar) {
        if (lVar == l.a()) {
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public m mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        int readTag;
        do {
            readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    return this;
            }
        } while (parseUnknownField(codedInputStream, extensionRegistryLite, readTag));
        return this;
    }
}
