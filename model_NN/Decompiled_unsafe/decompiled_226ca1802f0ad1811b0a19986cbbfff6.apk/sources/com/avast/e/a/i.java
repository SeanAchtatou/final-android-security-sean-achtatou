package com.avast.e.a;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import java.io.InputStream;

/* compiled from: ConfigProto */
public final class i extends GeneratedMessageLite implements k {

    /* renamed from: a  reason: collision with root package name */
    private static final i f1628a = new i(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1629b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public f f1630c;
    /* access modifiers changed from: private */
    public c d;
    /* access modifiers changed from: private */
    public l e;
    private byte f;
    private int g;

    private i(j jVar) {
        super(jVar);
        this.f = -1;
        this.g = -1;
    }

    private i(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static i a() {
        return f1628a;
    }

    public boolean b() {
        return (this.f1629b & 1) == 1;
    }

    public f c() {
        return this.f1630c;
    }

    public boolean d() {
        return (this.f1629b & 2) == 2;
    }

    public c e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1629b & 4) == 4;
    }

    public l g() {
        return this.e;
    }

    private void k() {
        this.f1630c = f.a();
        this.d = c.a();
        this.e = l.a();
    }

    public final boolean isInitialized() {
        byte b2 = this.f;
        if (b2 == -1) {
            this.f = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1629b & 1) == 1) {
            codedOutputStream.writeMessage(2, this.f1630c);
        }
        if ((this.f1629b & 2) == 2) {
            codedOutputStream.writeMessage(3, this.d);
        }
        if ((this.f1629b & 4) == 4) {
            codedOutputStream.writeMessage(4, this.e);
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            if ((this.f1629b & 1) == 1) {
                i = 0 + CodedOutputStream.computeMessageSize(2, this.f1630c);
            }
            if ((this.f1629b & 2) == 2) {
                i += CodedOutputStream.computeMessageSize(3, this.d);
            }
            if ((this.f1629b & 4) == 4) {
                i += CodedOutputStream.computeMessageSize(4, this.e);
            }
            this.g = i;
        }
        return i;
    }

    public static i a(byte[] bArr) {
        return ((j) h().mergeFrom(bArr)).n();
    }

    public static i a(InputStream inputStream) {
        return ((j) h().mergeFrom(inputStream)).n();
    }

    public static j h() {
        return j.m();
    }

    /* renamed from: i */
    public j newBuilderForType() {
        return h();
    }

    public static j a(i iVar) {
        return h().mergeFrom(iVar);
    }

    /* renamed from: j */
    public j toBuilder() {
        return a(this);
    }

    static {
        f1628a.k();
    }
}
