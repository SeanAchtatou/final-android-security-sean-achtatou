package com.avast.e.a;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class ao extends GeneratedMessageLite.Builder<an, ao> implements ap {

    /* renamed from: a  reason: collision with root package name */
    private int f1587a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f1588b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private ByteString f1589c = ByteString.EMPTY;
    private ak d = ak.a();
    private at e = at.a();
    private ByteString f = ByteString.EMPTY;

    private ao() {
        j();
    }

    private void j() {
    }

    /* access modifiers changed from: private */
    public static ao k() {
        return new ao();
    }

    /* renamed from: a */
    public ao clone() {
        return k().mergeFrom(d());
    }

    /* renamed from: b */
    public an getDefaultInstanceForType() {
        return an.a();
    }

    /* renamed from: c */
    public an build() {
        an d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public an d() {
        int i = 1;
        an anVar = new an(this);
        int i2 = this.f1587a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        ByteString unused = anVar.f1586c = this.f1588b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        ByteString unused2 = anVar.d = this.f1589c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        ak unused3 = anVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 8;
        }
        at unused4 = anVar.f = this.e;
        if ((i2 & 16) == 16) {
            i |= 16;
        }
        ByteString unused5 = anVar.g = this.f;
        int unused6 = anVar.f1585b = i;
        return anVar;
    }

    /* renamed from: a */
    public ao mergeFrom(an anVar) {
        if (anVar != an.a()) {
            if (anVar.b()) {
                a(anVar.c());
            }
            if (anVar.d()) {
                b(anVar.e());
            }
            if (anVar.f()) {
                b(anVar.g());
            }
            if (anVar.h()) {
                b(anVar.i());
            }
            if (anVar.j()) {
                c(anVar.k());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public ao mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1587a |= 1;
                    this.f1588b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1587a |= 2;
                    this.f1589c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    al l = ak.l();
                    if (e()) {
                        l.mergeFrom(f());
                    }
                    codedInputStream.readMessage(l, extensionRegistryLite);
                    a(l.d());
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    au f2 = at.f();
                    if (g()) {
                        f2.mergeFrom(h());
                    }
                    codedInputStream.readMessage(f2, extensionRegistryLite);
                    a(f2.d());
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f1587a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public ao a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1587a |= 1;
        this.f1588b = byteString;
        return this;
    }

    public ao b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1587a |= 2;
        this.f1589c = byteString;
        return this;
    }

    public boolean e() {
        return (this.f1587a & 4) == 4;
    }

    public ak f() {
        return this.d;
    }

    public ao a(ak akVar) {
        if (akVar == null) {
            throw new NullPointerException();
        }
        this.d = akVar;
        this.f1587a |= 4;
        return this;
    }

    public ao a(al alVar) {
        this.d = alVar.build();
        this.f1587a |= 4;
        return this;
    }

    public ao b(ak akVar) {
        if ((this.f1587a & 4) != 4 || this.d == ak.a()) {
            this.d = akVar;
        } else {
            this.d = ak.a(this.d).mergeFrom(akVar).d();
        }
        this.f1587a |= 4;
        return this;
    }

    public boolean g() {
        return (this.f1587a & 8) == 8;
    }

    public at h() {
        return this.e;
    }

    public ao a(at atVar) {
        if (atVar == null) {
            throw new NullPointerException();
        }
        this.e = atVar;
        this.f1587a |= 8;
        return this;
    }

    public ao b(at atVar) {
        if ((this.f1587a & 8) != 8 || this.e == at.a()) {
            this.e = atVar;
        } else {
            this.e = at.a(this.e).mergeFrom(atVar).d();
        }
        this.f1587a |= 8;
        return this;
    }

    public ao c(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1587a |= 16;
        this.f = byteString;
        return this;
    }
}
