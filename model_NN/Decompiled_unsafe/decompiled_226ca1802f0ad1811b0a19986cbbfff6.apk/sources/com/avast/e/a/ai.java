package com.avast.e.a;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class ai extends GeneratedMessageLite.Builder<af, ai> implements aj {

    /* renamed from: a  reason: collision with root package name */
    private int f1575a;

    /* renamed from: b  reason: collision with root package name */
    private aq f1576b = aq.a();

    /* renamed from: c  reason: collision with root package name */
    private ByteString f1577c = ByteString.EMPTY;
    private ag d = ag.FREE;

    private ai() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static ai i() {
        return new ai();
    }

    /* renamed from: a */
    public ai clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public af getDefaultInstanceForType() {
        return af.a();
    }

    /* renamed from: c */
    public af build() {
        af d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public af d() {
        int i = 1;
        af afVar = new af(this);
        int i2 = this.f1575a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        aq unused = afVar.f1571c = this.f1576b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        ByteString unused2 = afVar.d = this.f1577c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        ag unused3 = afVar.e = this.d;
        int unused4 = afVar.f1570b = i;
        return afVar;
    }

    /* renamed from: a */
    public ai mergeFrom(af afVar) {
        if (afVar != af.a()) {
            if (afVar.b()) {
                b(afVar.c());
            }
            if (afVar.d()) {
                a(afVar.e());
            }
            if (afVar.f()) {
                a(afVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public ai mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    ar j = aq.j();
                    if (e()) {
                        j.mergeFrom(f());
                    }
                    codedInputStream.readMessage(j, extensionRegistryLite);
                    a(j.d());
                    break;
                case 18:
                    this.f1575a |= 2;
                    this.f1577c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    ag a2 = ag.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1575a |= 4;
                        this.d = a2;
                        break;
                    }
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1575a & 1) == 1;
    }

    public aq f() {
        return this.f1576b;
    }

    public ai a(aq aqVar) {
        if (aqVar == null) {
            throw new NullPointerException();
        }
        this.f1576b = aqVar;
        this.f1575a |= 1;
        return this;
    }

    public ai a(ar arVar) {
        this.f1576b = arVar.build();
        this.f1575a |= 1;
        return this;
    }

    public ai b(aq aqVar) {
        if ((this.f1575a & 1) != 1 || this.f1576b == aq.a()) {
            this.f1576b = aqVar;
        } else {
            this.f1576b = aq.a(this.f1576b).mergeFrom(aqVar).d();
        }
        this.f1575a |= 1;
        return this;
    }

    public ai a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1575a |= 2;
        this.f1577c = byteString;
        return this;
    }

    public ai a(ag agVar) {
        if (agVar == null) {
            throw new NullPointerException();
        }
        this.f1575a |= 4;
        this.d = agVar;
        return this;
    }
}
