package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class aq extends GeneratedMessageLite implements as {

    /* renamed from: a  reason: collision with root package name */
    private static final aq f1590a = new aq(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1591b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f1592c;
    /* access modifiers changed from: private */
    public ByteString d;
    /* access modifiers changed from: private */
    public ByteString e;
    /* access modifiers changed from: private */
    public ByteString f;
    private byte g;
    private int h;

    private aq(ar arVar) {
        super(arVar);
        this.g = -1;
        this.h = -1;
    }

    private aq(boolean z) {
        this.g = -1;
        this.h = -1;
    }

    public static aq a() {
        return f1590a;
    }

    public boolean b() {
        return (this.f1591b & 1) == 1;
    }

    public ByteString c() {
        return this.f1592c;
    }

    public boolean d() {
        return (this.f1591b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1591b & 4) == 4;
    }

    public ByteString g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1591b & 8) == 8;
    }

    public ByteString i() {
        return this.f;
    }

    private void m() {
        this.f1592c = ByteString.EMPTY;
        this.d = ByteString.EMPTY;
        this.e = ByteString.EMPTY;
        this.f = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.g;
        if (b2 == -1) {
            this.g = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1591b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f1592c);
        }
        if ((this.f1591b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
        if ((this.f1591b & 4) == 4) {
            codedOutputStream.writeBytes(3, this.e);
        }
        if ((this.f1591b & 8) == 8) {
            codedOutputStream.writeBytes(4, this.f);
        }
    }

    public int getSerializedSize() {
        int i = this.h;
        if (i == -1) {
            i = 0;
            if ((this.f1591b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, this.f1592c);
            }
            if ((this.f1591b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, this.d);
            }
            if ((this.f1591b & 4) == 4) {
                i += CodedOutputStream.computeBytesSize(3, this.e);
            }
            if ((this.f1591b & 8) == 8) {
                i += CodedOutputStream.computeBytesSize(4, this.f);
            }
            this.h = i;
        }
        return i;
    }

    public static ar j() {
        return ar.g();
    }

    /* renamed from: k */
    public ar newBuilderForType() {
        return j();
    }

    public static ar a(aq aqVar) {
        return j().mergeFrom(aqVar);
    }

    /* renamed from: l */
    public ar toBuilder() {
        return a(this);
    }

    static {
        f1590a.m();
    }
}
