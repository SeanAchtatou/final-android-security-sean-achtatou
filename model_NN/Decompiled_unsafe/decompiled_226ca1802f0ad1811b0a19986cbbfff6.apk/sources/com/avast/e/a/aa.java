package com.avast.e.a;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class aa extends GeneratedMessageLite implements ae {

    /* renamed from: a  reason: collision with root package name */
    private static final aa f1560a = new aa(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1561b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public aq f1562c;
    /* access modifiers changed from: private */
    public ab d;
    private byte e;
    private int f;

    private aa(ad adVar) {
        super(adVar);
        this.e = -1;
        this.f = -1;
    }

    private aa(boolean z) {
        this.e = -1;
        this.f = -1;
    }

    public static aa a() {
        return f1560a;
    }

    public boolean b() {
        return (this.f1561b & 1) == 1;
    }

    public aq c() {
        return this.f1562c;
    }

    public boolean d() {
        return (this.f1561b & 2) == 2;
    }

    public ab e() {
        return this.d;
    }

    private void i() {
        this.f1562c = aq.a();
        this.d = ab.FREE;
    }

    public final boolean isInitialized() {
        byte b2 = this.e;
        if (b2 == -1) {
            this.e = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1561b & 1) == 1) {
            codedOutputStream.writeMessage(1, this.f1562c);
        }
        if ((this.f1561b & 2) == 2) {
            codedOutputStream.writeEnum(2, this.d.getNumber());
        }
    }

    public int getSerializedSize() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if ((this.f1561b & 1) == 1) {
                i = 0 + CodedOutputStream.computeMessageSize(1, this.f1562c);
            }
            if ((this.f1561b & 2) == 2) {
                i += CodedOutputStream.computeEnumSize(2, this.d.getNumber());
            }
            this.f = i;
        }
        return i;
    }

    public static ad f() {
        return ad.i();
    }

    /* renamed from: g */
    public ad newBuilderForType() {
        return f();
    }

    public static ad a(aa aaVar) {
        return f().mergeFrom(aaVar);
    }

    /* renamed from: h */
    public ad toBuilder() {
        return a(this);
    }

    static {
        f1560a.i();
    }
}
