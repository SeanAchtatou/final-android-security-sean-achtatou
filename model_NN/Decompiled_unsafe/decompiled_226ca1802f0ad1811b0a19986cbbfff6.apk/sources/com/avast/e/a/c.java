package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ConfigProto */
public final class c extends GeneratedMessageLite implements e {

    /* renamed from: a  reason: collision with root package name */
    private static final c f1617a = new c(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1618b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f1619c;
    private byte d;
    private int e;

    private c(d dVar) {
        super(dVar);
        this.d = -1;
        this.e = -1;
    }

    private c(boolean z) {
        this.d = -1;
        this.e = -1;
    }

    public static c a() {
        return f1617a;
    }

    public boolean b() {
        return (this.f1618b & 1) == 1;
    }

    public ByteString c() {
        return this.f1619c;
    }

    private void g() {
        this.f1619c = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.d;
        if (b2 == -1) {
            this.d = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1618b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f1619c);
        }
    }

    public int getSerializedSize() {
        int i = this.e;
        if (i == -1) {
            i = 0;
            if ((this.f1618b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, this.f1619c);
            }
            this.e = i;
        }
        return i;
    }

    public static d d() {
        return d.g();
    }

    /* renamed from: e */
    public d newBuilderForType() {
        return d();
    }

    public static d a(c cVar) {
        return d().mergeFrom(cVar);
    }

    /* renamed from: f */
    public d toBuilder() {
        return a(this);
    }

    static {
        f1617a.g();
    }
}
