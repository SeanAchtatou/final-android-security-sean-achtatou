package com.avast.e.a;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class al extends GeneratedMessageLite.Builder<ak, al> implements am {

    /* renamed from: a  reason: collision with root package name */
    private int f1581a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f1582b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private ByteString f1583c = ByteString.EMPTY;
    private ByteString d = ByteString.EMPTY;
    private ByteString e = ByteString.EMPTY;
    private ByteString f = ByteString.EMPTY;

    private al() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static al g() {
        return new al();
    }

    /* renamed from: a */
    public al clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public ak getDefaultInstanceForType() {
        return ak.a();
    }

    /* renamed from: c */
    public ak build() {
        ak d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public ak d() {
        int i = 1;
        ak akVar = new ak(this);
        int i2 = this.f1581a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        ByteString unused = akVar.f1580c = this.f1582b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        ByteString unused2 = akVar.d = this.f1583c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        ByteString unused3 = akVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 8;
        }
        ByteString unused4 = akVar.f = this.e;
        if ((i2 & 16) == 16) {
            i |= 16;
        }
        ByteString unused5 = akVar.g = this.f;
        int unused6 = akVar.f1579b = i;
        return akVar;
    }

    /* renamed from: a */
    public al mergeFrom(ak akVar) {
        if (akVar != ak.a()) {
            if (akVar.b()) {
                a(akVar.c());
            }
            if (akVar.d()) {
                b(akVar.e());
            }
            if (akVar.f()) {
                c(akVar.g());
            }
            if (akVar.h()) {
                d(akVar.i());
            }
            if (akVar.j()) {
                e(akVar.k());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public al mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1581a |= 1;
                    this.f1582b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1581a |= 2;
                    this.f1583c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1581a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f1581a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f1581a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public al a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1581a |= 1;
        this.f1582b = byteString;
        return this;
    }

    public al b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1581a |= 2;
        this.f1583c = byteString;
        return this;
    }

    public al c(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1581a |= 4;
        this.d = byteString;
        return this;
    }

    public al d(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1581a |= 8;
        this.e = byteString;
        return this;
    }

    public al e(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1581a |= 16;
        this.f = byteString;
        return this;
    }
}
