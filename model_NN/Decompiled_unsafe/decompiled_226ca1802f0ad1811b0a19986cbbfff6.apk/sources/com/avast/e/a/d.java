package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ConfigProto */
public final class d extends GeneratedMessageLite.Builder<c, d> implements e {

    /* renamed from: a  reason: collision with root package name */
    private int f1620a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f1621b = ByteString.EMPTY;

    private d() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static d g() {
        return new d();
    }

    /* renamed from: a */
    public d clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public c getDefaultInstanceForType() {
        return c.a();
    }

    /* renamed from: c */
    public c build() {
        c d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public c d() {
        int i = 1;
        c cVar = new c(this);
        if ((this.f1620a & 1) != 1) {
            i = 0;
        }
        ByteString unused = cVar.f1619c = this.f1621b;
        int unused2 = cVar.f1618b = i;
        return cVar;
    }

    /* renamed from: a */
    public d mergeFrom(c cVar) {
        if (cVar != c.a() && cVar.b()) {
            a(cVar.c());
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public d mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1620a |= 1;
                    this.f1621b = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public d a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1620a |= 1;
        this.f1621b = byteString;
        return this;
    }
}
