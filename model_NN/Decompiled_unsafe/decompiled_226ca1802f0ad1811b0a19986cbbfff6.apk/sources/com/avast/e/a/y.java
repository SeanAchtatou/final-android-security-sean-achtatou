package com.avast.e.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class y extends GeneratedMessageLite.Builder<v, y> implements z {

    /* renamed from: a  reason: collision with root package name */
    private int f1652a;

    /* renamed from: b  reason: collision with root package name */
    private aq f1653b = aq.a();

    /* renamed from: c  reason: collision with root package name */
    private w f1654c = w.ADVANCED_FREE;

    private y() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static y i() {
        return new y();
    }

    /* renamed from: a */
    public y clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public v getDefaultInstanceForType() {
        return v.a();
    }

    /* renamed from: c */
    public v build() {
        v d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public v d() {
        int i = 1;
        v vVar = new v(this);
        int i2 = this.f1652a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        aq unused = vVar.f1648c = this.f1653b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        w unused2 = vVar.d = this.f1654c;
        int unused3 = vVar.f1647b = i;
        return vVar;
    }

    /* renamed from: a */
    public y mergeFrom(v vVar) {
        if (vVar != v.a()) {
            if (vVar.b()) {
                b(vVar.c());
            }
            if (vVar.d()) {
                a(vVar.e());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public y mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    ar j = aq.j();
                    if (e()) {
                        j.mergeFrom(f());
                    }
                    codedInputStream.readMessage(j, extensionRegistryLite);
                    a(j.d());
                    break;
                case 16:
                    w a2 = w.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1652a |= 2;
                        this.f1654c = a2;
                        break;
                    }
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1652a & 1) == 1;
    }

    public aq f() {
        return this.f1653b;
    }

    public y a(aq aqVar) {
        if (aqVar == null) {
            throw new NullPointerException();
        }
        this.f1653b = aqVar;
        this.f1652a |= 1;
        return this;
    }

    public y a(ar arVar) {
        this.f1653b = arVar.build();
        this.f1652a |= 1;
        return this;
    }

    public y b(aq aqVar) {
        if ((this.f1652a & 1) != 1 || this.f1653b == aq.a()) {
            this.f1653b = aqVar;
        } else {
            this.f1653b = aq.a(this.f1653b).mergeFrom(aqVar).d();
        }
        this.f1652a |= 1;
        return this;
    }

    public y a(w wVar) {
        if (wVar == null) {
            throw new NullPointerException();
        }
        this.f1652a |= 2;
        this.f1654c = wVar;
        return this;
    }
}
