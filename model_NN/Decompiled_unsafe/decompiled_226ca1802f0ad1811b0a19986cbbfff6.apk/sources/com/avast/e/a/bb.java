package com.avast.e.a;

import com.google.protobuf.Internal;

/* compiled from: ParamsProto */
public enum bb implements Internal.EnumLite {
    AMS(0, 1),
    ISL(1, 2),
    AAT(2, 3),
    ABCK(3, 4),
    ASL(4, 5);
    
    private static Internal.EnumLiteMap<bb> f = new bc();
    private final int g;

    public final int getNumber() {
        return this.g;
    }

    public static bb a(int i) {
        switch (i) {
            case 1:
                return AMS;
            case 2:
                return ISL;
            case 3:
                return AAT;
            case 4:
                return ABCK;
            case 5:
                return ASL;
            default:
                return null;
        }
    }

    private bb(int i, int i2) {
        this.g = i2;
    }
}
