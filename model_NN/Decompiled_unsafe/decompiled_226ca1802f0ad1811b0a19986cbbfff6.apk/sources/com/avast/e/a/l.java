package com.avast.e.a;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ConfigProto */
public final class l extends GeneratedMessageLite implements n {

    /* renamed from: a  reason: collision with root package name */
    private static final l f1634a = new l(true);

    /* renamed from: b  reason: collision with root package name */
    private byte f1635b;

    /* renamed from: c  reason: collision with root package name */
    private int f1636c;

    private l(m mVar) {
        super(mVar);
        this.f1635b = -1;
        this.f1636c = -1;
    }

    private l(boolean z) {
        this.f1635b = -1;
        this.f1636c = -1;
    }

    public static l a() {
        return f1634a;
    }

    private void e() {
    }

    public final boolean isInitialized() {
        byte b2 = this.f1635b;
        if (b2 == -1) {
            this.f1635b = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
    }

    public int getSerializedSize() {
        int i = this.f1636c;
        if (i != -1) {
            return i;
        }
        this.f1636c = 0;
        return 0;
    }

    public static m b() {
        return m.g();
    }

    /* renamed from: c */
    public m newBuilderForType() {
        return b();
    }

    public static m a(l lVar) {
        return b().mergeFrom(lVar);
    }

    /* renamed from: d */
    public m toBuilder() {
        return a(this);
    }

    static {
        f1634a.e();
    }
}
