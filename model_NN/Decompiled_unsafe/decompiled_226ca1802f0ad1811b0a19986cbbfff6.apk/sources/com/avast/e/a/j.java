package com.avast.e.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ConfigProto */
public final class j extends GeneratedMessageLite.Builder<i, j> implements k {

    /* renamed from: a  reason: collision with root package name */
    private int f1631a;

    /* renamed from: b  reason: collision with root package name */
    private f f1632b = f.a();

    /* renamed from: c  reason: collision with root package name */
    private c f1633c = c.a();
    private l d = l.a();

    private j() {
        l();
    }

    private void l() {
    }

    /* access modifiers changed from: private */
    public static j m() {
        return new j();
    }

    /* renamed from: a */
    public j clone() {
        return m().mergeFrom(d());
    }

    /* renamed from: b */
    public i getDefaultInstanceForType() {
        return i.a();
    }

    /* renamed from: c */
    public i build() {
        i d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    /* access modifiers changed from: private */
    public i n() {
        i d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2).asInvalidProtocolBufferException();
    }

    public i d() {
        int i = 1;
        i iVar = new i(this);
        int i2 = this.f1631a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        f unused = iVar.f1630c = this.f1632b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        c unused2 = iVar.d = this.f1633c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        l unused3 = iVar.e = this.d;
        int unused4 = iVar.f1629b = i;
        return iVar;
    }

    /* renamed from: a */
    public j mergeFrom(i iVar) {
        if (iVar != i.a()) {
            if (iVar.b()) {
                b(iVar.c());
            }
            if (iVar.d()) {
                b(iVar.e());
            }
            if (iVar.f()) {
                b(iVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public j mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 18:
                    g h = f.h();
                    if (e()) {
                        h.mergeFrom(f());
                    }
                    codedInputStream.readMessage(h, extensionRegistryLite);
                    a(h.d());
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    d d2 = c.d();
                    if (g()) {
                        d2.mergeFrom(h());
                    }
                    codedInputStream.readMessage(d2, extensionRegistryLite);
                    a(d2.d());
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    m b2 = l.b();
                    if (i()) {
                        b2.mergeFrom(j());
                    }
                    codedInputStream.readMessage(b2, extensionRegistryLite);
                    a(b2.d());
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1631a & 1) == 1;
    }

    public f f() {
        return this.f1632b;
    }

    public j a(f fVar) {
        if (fVar == null) {
            throw new NullPointerException();
        }
        this.f1632b = fVar;
        this.f1631a |= 1;
        return this;
    }

    public j a(g gVar) {
        this.f1632b = gVar.build();
        this.f1631a |= 1;
        return this;
    }

    public j b(f fVar) {
        if ((this.f1631a & 1) != 1 || this.f1632b == f.a()) {
            this.f1632b = fVar;
        } else {
            this.f1632b = f.a(this.f1632b).mergeFrom(fVar).d();
        }
        this.f1631a |= 1;
        return this;
    }

    public boolean g() {
        return (this.f1631a & 2) == 2;
    }

    public c h() {
        return this.f1633c;
    }

    public j a(c cVar) {
        if (cVar == null) {
            throw new NullPointerException();
        }
        this.f1633c = cVar;
        this.f1631a |= 2;
        return this;
    }

    public j a(d dVar) {
        this.f1633c = dVar.build();
        this.f1631a |= 2;
        return this;
    }

    public j b(c cVar) {
        if ((this.f1631a & 2) != 2 || this.f1633c == c.a()) {
            this.f1633c = cVar;
        } else {
            this.f1633c = c.a(this.f1633c).mergeFrom(cVar).d();
        }
        this.f1631a |= 2;
        return this;
    }

    public boolean i() {
        return (this.f1631a & 4) == 4;
    }

    public l j() {
        return this.d;
    }

    public j a(l lVar) {
        if (lVar == null) {
            throw new NullPointerException();
        }
        this.d = lVar;
        this.f1631a |= 4;
        return this;
    }

    public j b(l lVar) {
        if ((this.f1631a & 4) != 4 || this.d == l.a()) {
            this.d = lVar;
        } else {
            this.d = l.a(this.d).mergeFrom(lVar).d();
        }
        this.f1631a |= 4;
        return this;
    }
}
