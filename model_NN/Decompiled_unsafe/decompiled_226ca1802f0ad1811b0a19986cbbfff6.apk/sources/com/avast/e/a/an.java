package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class an extends GeneratedMessageLite implements ap {

    /* renamed from: a  reason: collision with root package name */
    private static final an f1584a = new an(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1585b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f1586c;
    /* access modifiers changed from: private */
    public ByteString d;
    /* access modifiers changed from: private */
    public ak e;
    /* access modifiers changed from: private */
    public at f;
    /* access modifiers changed from: private */
    public ByteString g;
    private byte h;
    private int i;

    private an(ao aoVar) {
        super(aoVar);
        this.h = -1;
        this.i = -1;
    }

    private an(boolean z) {
        this.h = -1;
        this.i = -1;
    }

    public static an a() {
        return f1584a;
    }

    public boolean b() {
        return (this.f1585b & 1) == 1;
    }

    public ByteString c() {
        return this.f1586c;
    }

    public boolean d() {
        return (this.f1585b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1585b & 4) == 4;
    }

    public ak g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1585b & 8) == 8;
    }

    public at i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1585b & 16) == 16;
    }

    public ByteString k() {
        return this.g;
    }

    private void o() {
        this.f1586c = ByteString.EMPTY;
        this.d = ByteString.EMPTY;
        this.e = ak.a();
        this.f = at.a();
        this.g = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.h;
        if (b2 == -1) {
            this.h = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1585b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f1586c);
        }
        if ((this.f1585b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
        if ((this.f1585b & 4) == 4) {
            codedOutputStream.writeMessage(3, this.e);
        }
        if ((this.f1585b & 8) == 8) {
            codedOutputStream.writeMessage(4, this.f);
        }
        if ((this.f1585b & 16) == 16) {
            codedOutputStream.writeBytes(5, this.g);
        }
    }

    public int getSerializedSize() {
        int i2 = this.i;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1585b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, this.f1586c);
            }
            if ((this.f1585b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, this.d);
            }
            if ((this.f1585b & 4) == 4) {
                i2 += CodedOutputStream.computeMessageSize(3, this.e);
            }
            if ((this.f1585b & 8) == 8) {
                i2 += CodedOutputStream.computeMessageSize(4, this.f);
            }
            if ((this.f1585b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(5, this.g);
            }
            this.i = i2;
        }
        return i2;
    }

    public static ao l() {
        return ao.k();
    }

    /* renamed from: m */
    public ao newBuilderForType() {
        return l();
    }

    public static ao a(an anVar) {
        return l().mergeFrom(anVar);
    }

    /* renamed from: n */
    public ao toBuilder() {
        return a(this);
    }

    static {
        f1584a.o();
    }
}
