package com.avast.e.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class ad extends GeneratedMessageLite.Builder<aa, ad> implements ae {

    /* renamed from: a  reason: collision with root package name */
    private int f1566a;

    /* renamed from: b  reason: collision with root package name */
    private aq f1567b = aq.a();

    /* renamed from: c  reason: collision with root package name */
    private ab f1568c = ab.FREE;

    private ad() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static ad i() {
        return new ad();
    }

    /* renamed from: a */
    public ad clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public aa getDefaultInstanceForType() {
        return aa.a();
    }

    /* renamed from: c */
    public aa build() {
        aa d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public aa d() {
        int i = 1;
        aa aaVar = new aa(this);
        int i2 = this.f1566a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        aq unused = aaVar.f1562c = this.f1567b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        ab unused2 = aaVar.d = this.f1568c;
        int unused3 = aaVar.f1561b = i;
        return aaVar;
    }

    /* renamed from: a */
    public ad mergeFrom(aa aaVar) {
        if (aaVar != aa.a()) {
            if (aaVar.b()) {
                b(aaVar.c());
            }
            if (aaVar.d()) {
                a(aaVar.e());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public ad mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    ar j = aq.j();
                    if (e()) {
                        j.mergeFrom(f());
                    }
                    codedInputStream.readMessage(j, extensionRegistryLite);
                    a(j.d());
                    break;
                case 16:
                    ab a2 = ab.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1566a |= 2;
                        this.f1568c = a2;
                        break;
                    }
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1566a & 1) == 1;
    }

    public aq f() {
        return this.f1567b;
    }

    public ad a(aq aqVar) {
        if (aqVar == null) {
            throw new NullPointerException();
        }
        this.f1567b = aqVar;
        this.f1566a |= 1;
        return this;
    }

    public ad a(ar arVar) {
        this.f1567b = arVar.build();
        this.f1566a |= 1;
        return this;
    }

    public ad b(aq aqVar) {
        if ((this.f1566a & 1) != 1 || this.f1567b == aq.a()) {
            this.f1567b = aqVar;
        } else {
            this.f1567b = aq.a(this.f1567b).mergeFrom(aqVar).d();
        }
        this.f1566a |= 1;
        return this;
    }

    public ad a(ab abVar) {
        if (abVar == null) {
            throw new NullPointerException();
        }
        this.f1566a |= 2;
        this.f1568c = abVar;
        return this;
    }
}
