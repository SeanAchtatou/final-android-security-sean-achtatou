package com.avast.e.a;

import com.google.protobuf.Internal;

/* compiled from: ParamsProto */
public enum ab implements Internal.EnumLite {
    FREE(0, 1),
    PREMIUM(1, 2);
    

    /* renamed from: c  reason: collision with root package name */
    private static Internal.EnumLiteMap<ab> f1565c = new ac();
    private final int d;

    public final int getNumber() {
        return this.d;
    }

    public static ab a(int i) {
        switch (i) {
            case 1:
                return FREE;
            case 2:
                return PREMIUM;
            default:
                return null;
        }
    }

    private ab(int i, int i2) {
        this.d = i2;
    }
}
