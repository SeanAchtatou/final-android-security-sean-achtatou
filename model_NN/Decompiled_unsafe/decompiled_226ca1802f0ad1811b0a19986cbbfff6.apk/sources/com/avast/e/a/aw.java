package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class aw extends GeneratedMessageLite implements ay {

    /* renamed from: a  reason: collision with root package name */
    private static final aw f1602a = new aw(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1603b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public aq f1604c;
    /* access modifiers changed from: private */
    public ByteString d;
    private byte e;
    private int f;

    private aw(ax axVar) {
        super(axVar);
        this.e = -1;
        this.f = -1;
    }

    private aw(boolean z) {
        this.e = -1;
        this.f = -1;
    }

    public static aw a() {
        return f1602a;
    }

    public boolean b() {
        return (this.f1603b & 1) == 1;
    }

    public aq c() {
        return this.f1604c;
    }

    public boolean d() {
        return (this.f1603b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    private void i() {
        this.f1604c = aq.a();
        this.d = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.e;
        if (b2 == -1) {
            this.e = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1603b & 1) == 1) {
            codedOutputStream.writeMessage(1, this.f1604c);
        }
        if ((this.f1603b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
    }

    public int getSerializedSize() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if ((this.f1603b & 1) == 1) {
                i = 0 + CodedOutputStream.computeMessageSize(1, this.f1604c);
            }
            if ((this.f1603b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, this.d);
            }
            this.f = i;
        }
        return i;
    }

    public static ax f() {
        return ax.i();
    }

    /* renamed from: g */
    public ax newBuilderForType() {
        return f();
    }

    public static ax a(aw awVar) {
        return f().mergeFrom(awVar);
    }

    /* renamed from: h */
    public ax toBuilder() {
        return a(this);
    }

    static {
        f1602a.i();
    }
}
