package com.avast.e.a;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class v extends GeneratedMessageLite implements z {

    /* renamed from: a  reason: collision with root package name */
    private static final v f1646a = new v(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1647b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public aq f1648c;
    /* access modifiers changed from: private */
    public w d;
    private byte e;
    private int f;

    private v(y yVar) {
        super(yVar);
        this.e = -1;
        this.f = -1;
    }

    private v(boolean z) {
        this.e = -1;
        this.f = -1;
    }

    public static v a() {
        return f1646a;
    }

    public boolean b() {
        return (this.f1647b & 1) == 1;
    }

    public aq c() {
        return this.f1648c;
    }

    public boolean d() {
        return (this.f1647b & 2) == 2;
    }

    public w e() {
        return this.d;
    }

    private void i() {
        this.f1648c = aq.a();
        this.d = w.ADVANCED_FREE;
    }

    public final boolean isInitialized() {
        byte b2 = this.e;
        if (b2 == -1) {
            this.e = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1647b & 1) == 1) {
            codedOutputStream.writeMessage(1, this.f1648c);
        }
        if ((this.f1647b & 2) == 2) {
            codedOutputStream.writeEnum(2, this.d.getNumber());
        }
    }

    public int getSerializedSize() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if ((this.f1647b & 1) == 1) {
                i = 0 + CodedOutputStream.computeMessageSize(1, this.f1648c);
            }
            if ((this.f1647b & 2) == 2) {
                i += CodedOutputStream.computeEnumSize(2, this.d.getNumber());
            }
            this.f = i;
        }
        return i;
    }

    public static y f() {
        return y.i();
    }

    /* renamed from: g */
    public y newBuilderForType() {
        return f();
    }

    public static y a(v vVar) {
        return f().mergeFrom(vVar);
    }

    /* renamed from: h */
    public y toBuilder() {
        return a(this);
    }

    static {
        f1646a.i();
    }
}
