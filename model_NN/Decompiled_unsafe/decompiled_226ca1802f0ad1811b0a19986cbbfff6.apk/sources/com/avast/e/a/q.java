package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ConfigProto */
public final class q extends GeneratedMessageLite implements s {

    /* renamed from: a  reason: collision with root package name */
    private static final q f1640a = new q(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1641b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f1642c;
    /* access modifiers changed from: private */
    public o d;
    private byte e;
    private int f;

    private q(r rVar) {
        super(rVar);
        this.e = -1;
        this.f = -1;
    }

    private q(boolean z) {
        this.e = -1;
        this.f = -1;
    }

    public static q a() {
        return f1640a;
    }

    public boolean b() {
        return (this.f1641b & 1) == 1;
    }

    public ByteString c() {
        return this.f1642c;
    }

    public boolean d() {
        return (this.f1641b & 2) == 2;
    }

    public o e() {
        return this.d;
    }

    private void i() {
        this.f1642c = ByteString.EMPTY;
        this.d = o.VERBOSE;
    }

    public final boolean isInitialized() {
        byte b2 = this.e;
        if (b2 == -1) {
            this.e = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1641b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f1642c);
        }
        if ((this.f1641b & 2) == 2) {
            codedOutputStream.writeEnum(2, this.d.getNumber());
        }
    }

    public int getSerializedSize() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if ((this.f1641b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, this.f1642c);
            }
            if ((this.f1641b & 2) == 2) {
                i += CodedOutputStream.computeEnumSize(2, this.d.getNumber());
            }
            this.f = i;
        }
        return i;
    }

    public static r f() {
        return r.g();
    }

    /* renamed from: g */
    public r newBuilderForType() {
        return f();
    }

    public static r a(q qVar) {
        return f().mergeFrom(qVar);
    }

    /* renamed from: h */
    public r toBuilder() {
        return a(this);
    }

    static {
        f1640a.i();
    }
}
