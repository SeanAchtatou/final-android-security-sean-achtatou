package com.avast.e.a;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ConfigProto */
public final class g extends GeneratedMessageLite.Builder<f, g> implements h {

    /* renamed from: a  reason: collision with root package name */
    private int f1625a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f1626b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1627c;
    private List<q> d = Collections.emptyList();
    private o e = o.VERBOSE;

    private g() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static g g() {
        return new g();
    }

    /* renamed from: a */
    public g clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public f getDefaultInstanceForType() {
        return f.a();
    }

    /* renamed from: c */
    public f build() {
        f d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public f d() {
        int i = 1;
        f fVar = new f(this);
        int i2 = this.f1625a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        ByteString unused = fVar.f1624c = this.f1626b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        boolean unused2 = fVar.d = this.f1627c;
        if ((this.f1625a & 4) == 4) {
            this.d = Collections.unmodifiableList(this.d);
            this.f1625a &= -5;
        }
        List unused3 = fVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 4;
        }
        o unused4 = fVar.f = this.e;
        int unused5 = fVar.f1623b = i;
        return fVar;
    }

    /* renamed from: a */
    public g mergeFrom(f fVar) {
        if (fVar != f.a()) {
            if (fVar.b()) {
                a(fVar.c());
            }
            if (fVar.d()) {
                a(fVar.e());
            }
            if (!fVar.e.isEmpty()) {
                if (this.d.isEmpty()) {
                    this.d = fVar.e;
                    this.f1625a &= -5;
                } else {
                    h();
                    this.d.addAll(fVar.e);
                }
            }
            if (fVar.f()) {
                a(fVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public g mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1625a |= 1;
                    this.f1626b = codedInputStream.readBytes();
                    break;
                case 16:
                    this.f1625a |= 2;
                    this.f1627c = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    r f = q.f();
                    codedInputStream.readMessage(f, extensionRegistryLite);
                    a(f.d());
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    o a2 = o.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1625a |= 8;
                        this.e = a2;
                        break;
                    }
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public g a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1625a |= 1;
        this.f1626b = byteString;
        return this;
    }

    public g a(boolean z) {
        this.f1625a |= 2;
        this.f1627c = z;
        return this;
    }

    private void h() {
        if ((this.f1625a & 4) != 4) {
            this.d = new ArrayList(this.d);
            this.f1625a |= 4;
        }
    }

    public g a(q qVar) {
        if (qVar == null) {
            throw new NullPointerException();
        }
        h();
        this.d.add(qVar);
        return this;
    }

    public g a(r rVar) {
        h();
        this.d.add(rVar.build());
        return this;
    }

    public g a(o oVar) {
        if (oVar == null) {
            throw new NullPointerException();
        }
        this.f1625a |= 8;
        this.e = oVar;
        return this;
    }
}
