package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import java.util.Collections;
import java.util.List;

/* compiled from: ParamsProto */
public final class az extends GeneratedMessageLite implements bd {

    /* renamed from: a  reason: collision with root package name */
    private static final az f1608a = new az(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1609b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public bb f1610c;
    /* access modifiers changed from: private */
    public List<ByteString> d;
    /* access modifiers changed from: private */
    public an e;
    /* access modifiers changed from: private */
    public af f;
    /* access modifiers changed from: private */
    public aw g;
    /* access modifiers changed from: private */
    public v h;
    /* access modifiers changed from: private */
    public aa i;
    private byte j;
    private int k;

    private az(ba baVar) {
        super(baVar);
        this.j = -1;
        this.k = -1;
    }

    private az(boolean z) {
        this.j = -1;
        this.k = -1;
    }

    public static az a() {
        return f1608a;
    }

    public boolean b() {
        return (this.f1609b & 1) == 1;
    }

    public bb c() {
        return this.f1610c;
    }

    public List<ByteString> d() {
        return this.d;
    }

    public boolean e() {
        return (this.f1609b & 2) == 2;
    }

    public an f() {
        return this.e;
    }

    public boolean g() {
        return (this.f1609b & 4) == 4;
    }

    public af h() {
        return this.f;
    }

    public boolean i() {
        return (this.f1609b & 8) == 8;
    }

    public aw j() {
        return this.g;
    }

    public boolean k() {
        return (this.f1609b & 16) == 16;
    }

    public v l() {
        return this.h;
    }

    public boolean m() {
        return (this.f1609b & 32) == 32;
    }

    public aa n() {
        return this.i;
    }

    private void r() {
        this.f1610c = bb.AMS;
        this.d = Collections.emptyList();
        this.e = an.a();
        this.f = af.a();
        this.g = aw.a();
        this.h = v.a();
        this.i = aa.a();
    }

    public final boolean isInitialized() {
        byte b2 = this.j;
        if (b2 == -1) {
            this.j = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1609b & 1) == 1) {
            codedOutputStream.writeEnum(1, this.f1610c.getNumber());
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.d.size()) {
                break;
            }
            codedOutputStream.writeBytes(2, this.d.get(i3));
            i2 = i3 + 1;
        }
        if ((this.f1609b & 2) == 2) {
            codedOutputStream.writeMessage(3, this.e);
        }
        if ((this.f1609b & 4) == 4) {
            codedOutputStream.writeMessage(4, this.f);
        }
        if ((this.f1609b & 8) == 8) {
            codedOutputStream.writeMessage(5, this.g);
        }
        if ((this.f1609b & 16) == 16) {
            codedOutputStream.writeMessage(6, this.h);
        }
        if ((this.f1609b & 32) == 32) {
            codedOutputStream.writeMessage(7, this.i);
        }
    }

    public int getSerializedSize() {
        int i2;
        int i3 = 0;
        int i4 = this.k;
        if (i4 == -1) {
            if ((this.f1609b & 1) == 1) {
                i2 = CodedOutputStream.computeEnumSize(1, this.f1610c.getNumber()) + 0;
            } else {
                i2 = 0;
            }
            int i5 = 0;
            while (i3 < this.d.size()) {
                i3++;
                i5 = CodedOutputStream.computeBytesSizeNoTag(this.d.get(i3)) + i5;
            }
            i4 = i2 + i5 + (d().size() * 1);
            if ((this.f1609b & 2) == 2) {
                i4 += CodedOutputStream.computeMessageSize(3, this.e);
            }
            if ((this.f1609b & 4) == 4) {
                i4 += CodedOutputStream.computeMessageSize(4, this.f);
            }
            if ((this.f1609b & 8) == 8) {
                i4 += CodedOutputStream.computeMessageSize(5, this.g);
            }
            if ((this.f1609b & 16) == 16) {
                i4 += CodedOutputStream.computeMessageSize(6, this.h);
            }
            if ((this.f1609b & 32) == 32) {
                i4 += CodedOutputStream.computeMessageSize(7, this.i);
            }
            this.k = i4;
        }
        return i4;
    }

    public static ba o() {
        return ba.q();
    }

    /* renamed from: p */
    public ba newBuilderForType() {
        return o();
    }

    public static ba a(az azVar) {
        return o().mergeFrom(azVar);
    }

    /* renamed from: q */
    public ba toBuilder() {
        return a(this);
    }

    static {
        f1608a.r();
    }
}
