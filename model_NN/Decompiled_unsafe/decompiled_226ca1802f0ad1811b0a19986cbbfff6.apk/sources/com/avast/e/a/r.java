package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ConfigProto */
public final class r extends GeneratedMessageLite.Builder<q, r> implements s {

    /* renamed from: a  reason: collision with root package name */
    private int f1643a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f1644b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private o f1645c = o.VERBOSE;

    private r() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static r g() {
        return new r();
    }

    /* renamed from: a */
    public r clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public q getDefaultInstanceForType() {
        return q.a();
    }

    /* renamed from: c */
    public q build() {
        q d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public q d() {
        int i = 1;
        q qVar = new q(this);
        int i2 = this.f1643a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        ByteString unused = qVar.f1642c = this.f1644b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        o unused2 = qVar.d = this.f1645c;
        int unused3 = qVar.f1641b = i;
        return qVar;
    }

    /* renamed from: a */
    public r mergeFrom(q qVar) {
        if (qVar != q.a()) {
            if (qVar.b()) {
                a(qVar.c());
            }
            if (qVar.d()) {
                a(qVar.e());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public r mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1643a |= 1;
                    this.f1644b = codedInputStream.readBytes();
                    break;
                case 16:
                    o a2 = o.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1643a |= 2;
                        this.f1645c = a2;
                        break;
                    }
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public r a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1643a |= 1;
        this.f1644b = byteString;
        return this;
    }

    public r a(o oVar) {
        if (oVar == null) {
            throw new NullPointerException();
        }
        this.f1643a |= 2;
        this.f1645c = oVar;
        return this;
    }
}
