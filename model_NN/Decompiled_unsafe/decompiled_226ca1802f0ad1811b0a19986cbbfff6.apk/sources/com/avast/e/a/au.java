package com.avast.e.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ParamsProto */
public final class au extends GeneratedMessageLite.Builder<at, au> implements av {

    /* renamed from: a  reason: collision with root package name */
    private int f1599a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f1600b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private ByteString f1601c = ByteString.EMPTY;

    private au() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static au g() {
        return new au();
    }

    /* renamed from: a */
    public au clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public at getDefaultInstanceForType() {
        return at.a();
    }

    /* renamed from: c */
    public at build() {
        at d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public at d() {
        int i = 1;
        at atVar = new at(this);
        int i2 = this.f1599a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        ByteString unused = atVar.f1598c = this.f1600b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        ByteString unused2 = atVar.d = this.f1601c;
        int unused3 = atVar.f1597b = i;
        return atVar;
    }

    /* renamed from: a */
    public au mergeFrom(at atVar) {
        if (atVar != at.a()) {
            if (atVar.b()) {
                a(atVar.c());
            }
            if (atVar.d()) {
                b(atVar.e());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public au mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1599a |= 1;
                    this.f1600b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1599a |= 2;
                    this.f1601c = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public au a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1599a |= 1;
        this.f1600b = byteString;
        return this;
    }

    public au b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1599a |= 2;
        this.f1601c = byteString;
        return this;
    }
}
