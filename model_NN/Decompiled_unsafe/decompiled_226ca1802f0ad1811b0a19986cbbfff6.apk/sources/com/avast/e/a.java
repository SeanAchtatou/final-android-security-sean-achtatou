package com.avast.e;

import com.avast.android.generic.util.t;
import com.avast.e.a.c;
import com.avast.e.a.d;
import com.avast.e.a.f;
import com.avast.e.a.g;
import com.avast.e.a.i;
import com.avast.e.a.j;
import com.avast.e.a.o;
import com.avast.e.a.q;
import com.avast.e.a.r;
import com.google.protobuf.ByteString;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: DefaultConfigFactory */
public class a {
    public static i a() {
        j h = i.h();
        g h2 = f.h();
        h2.a(o.DEBUG);
        r f = q.f();
        f.a(ByteString.copyFromUtf8(".*"));
        f.a(o.WARNING);
        h2.a(f);
        h.a(h2);
        d d = c.d();
        d.a(ByteString.copyFromUtf8("http://au.ff.avast.com:80/android/"));
        h.a(d);
        return a(h);
    }

    private static i a(j jVar) {
        try {
            Method method = Class.forName("com.avast.shepherd.DefaultConfigCustomLayer").getMethod("getModifiedConfig", i.class);
            t.c("DefaultConfigFactory", "A customization layer for the default Shepherd config has been found. Calling the getModifiedConfig method.");
            return (i) method.invoke(null, jVar.build());
        } catch (ClassNotFoundException e) {
            t.b("DefaultConfigFactory", "A customization layer for the default Shepherd config not found.", e);
        } catch (NoSuchMethodException e2) {
            t.d("DefaultConfigFactory", "The found default Shepherd config customization layer class isn't compatible.", e2);
        } catch (IllegalAccessException e3) {
            t.d("DefaultConfigFactory", "The found default Shepherd config customization layer class isn't compatible.", e3);
        } catch (InvocationTargetException e4) {
            t.d("DefaultConfigFactory", "The found default Shepherd config customization layer class isn't compatible.", e4);
        }
        return jVar.build();
    }
}
