package com.avast.c.b.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: MyBackendInfrastructureGenerics */
public final class c extends GeneratedMessageLite implements e {

    /* renamed from: a  reason: collision with root package name */
    private static final c f1491a = new c(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1492b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1493c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public f e;
    private byte f;
    private int g;

    private c(d dVar) {
        super(dVar);
        this.f = -1;
        this.g = -1;
    }

    private c(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static c a() {
        return f1491a;
    }

    public boolean b() {
        return (this.f1492b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1493c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1493c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString k() {
        Object obj = this.f1493c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1493c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1492b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString l() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1492b & 4) == 4;
    }

    public f g() {
        return this.e;
    }

    private void m() {
        this.f1493c = "";
        this.d = "";
        this.e = f.ADMIN;
    }

    public final boolean isInitialized() {
        byte b2 = this.f;
        if (b2 == -1) {
            this.f = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1492b & 1) == 1) {
            codedOutputStream.writeBytes(1, k());
        }
        if ((this.f1492b & 2) == 2) {
            codedOutputStream.writeBytes(2, l());
        }
        if ((this.f1492b & 4) == 4) {
            codedOutputStream.writeEnum(3, this.e.getNumber());
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            if ((this.f1492b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, k());
            }
            if ((this.f1492b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, l());
            }
            if ((this.f1492b & 4) == 4) {
                i += CodedOutputStream.computeEnumSize(3, this.e.getNumber());
            }
            this.g = i;
        }
        return i;
    }

    public static d h() {
        return d.g();
    }

    /* renamed from: i */
    public d newBuilderForType() {
        return h();
    }

    public static d a(c cVar) {
        return h().mergeFrom(cVar);
    }

    /* renamed from: j */
    public d toBuilder() {
        return a(this);
    }

    static {
        f1491a.m();
    }
}
