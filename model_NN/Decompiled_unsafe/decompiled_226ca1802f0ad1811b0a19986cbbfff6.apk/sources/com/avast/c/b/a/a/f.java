package com.avast.c.b.a.a;

import com.google.protobuf.Internal;

/* compiled from: MyBackendInfrastructureGenerics */
public enum f implements Internal.EnumLite {
    ADMIN(0, 1),
    SUPPORT(1, 2),
    READ_ONLY(2, 3),
    BUSINESS_INTELLIGENCE(3, 4);
    
    private static Internal.EnumLiteMap<f> e = new g();
    private final int f;

    public final int getNumber() {
        return this.f;
    }

    public static f a(int i) {
        switch (i) {
            case 1:
                return ADMIN;
            case 2:
                return SUPPORT;
            case 3:
                return READ_ONLY;
            case 4:
                return BUSINESS_INTELLIGENCE;
            default:
                return null;
        }
    }

    private f(int i, int i2) {
        this.f = i2;
    }
}
