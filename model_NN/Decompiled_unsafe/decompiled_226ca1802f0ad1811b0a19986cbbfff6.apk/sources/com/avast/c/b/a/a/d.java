package com.avast.c.b.a.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: MyBackendInfrastructureGenerics */
public final class d extends GeneratedMessageLite.Builder<c, d> implements e {

    /* renamed from: a  reason: collision with root package name */
    private int f1494a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1495b = "";

    /* renamed from: c  reason: collision with root package name */
    private Object f1496c = "";
    private f d = f.ADMIN;

    private d() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static d g() {
        return new d();
    }

    /* renamed from: a */
    public d clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public c getDefaultInstanceForType() {
        return c.a();
    }

    /* renamed from: c */
    public c build() {
        c d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public c d() {
        int i = 1;
        c cVar = new c(this);
        int i2 = this.f1494a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        Object unused = cVar.f1493c = this.f1495b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        Object unused2 = cVar.d = this.f1496c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        f unused3 = cVar.e = this.d;
        int unused4 = cVar.f1492b = i;
        return cVar;
    }

    /* renamed from: a */
    public d mergeFrom(c cVar) {
        if (cVar != c.a()) {
            if (cVar.b()) {
                a(cVar.c());
            }
            if (cVar.d()) {
                b(cVar.e());
            }
            if (cVar.f()) {
                a(cVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public d mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1494a |= 1;
                    this.f1495b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1494a |= 2;
                    this.f1496c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    f a2 = f.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1494a |= 4;
                        this.d = a2;
                        break;
                    }
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public d a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1494a |= 1;
        this.f1495b = str;
        return this;
    }

    public d b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1494a |= 2;
        this.f1496c = str;
        return this;
    }

    public d a(f fVar) {
        if (fVar == null) {
            throw new NullPointerException();
        }
        this.f1494a |= 4;
        this.d = fVar;
        return this;
    }
}
