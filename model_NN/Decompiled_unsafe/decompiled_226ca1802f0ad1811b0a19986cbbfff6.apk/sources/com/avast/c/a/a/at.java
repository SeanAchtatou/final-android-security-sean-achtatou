package com.avast.c.a.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: Billing */
public final class at extends GeneratedMessageLite.Builder<as, at> implements au {

    /* renamed from: a  reason: collision with root package name */
    private int f1443a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1444b = "";

    /* renamed from: c  reason: collision with root package name */
    private i f1445c = i.GV_GENERIC;

    private at() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static at g() {
        return new at();
    }

    /* renamed from: a */
    public at clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public as getDefaultInstanceForType() {
        return as.a();
    }

    /* renamed from: c */
    public as build() {
        as d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    /* access modifiers changed from: private */
    public as h() {
        as d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d).asInvalidProtocolBufferException();
    }

    public as d() {
        int i = 1;
        as asVar = new as(this);
        int i2 = this.f1443a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        Object unused = asVar.f1442c = this.f1444b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        i unused2 = asVar.d = this.f1445c;
        int unused3 = asVar.f1441b = i;
        return asVar;
    }

    /* renamed from: a */
    public at mergeFrom(as asVar) {
        if (asVar != as.a()) {
            if (asVar.b()) {
                a(asVar.c());
            }
            if (asVar.d()) {
                a(asVar.e());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public at mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1443a |= 1;
                    this.f1444b = codedInputStream.readBytes();
                    break;
                case 16:
                    i a2 = i.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1443a |= 2;
                        this.f1445c = a2;
                        break;
                    }
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public at a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1443a |= 1;
        this.f1444b = str;
        return this;
    }

    public at a(i iVar) {
        if (iVar == null) {
            throw new NullPointerException();
        }
        this.f1443a |= 2;
        this.f1445c = iVar;
        return this;
    }
}
