package com.avast.c.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: Billing */
public final class as extends GeneratedMessageLite implements au {

    /* renamed from: a  reason: collision with root package name */
    private static final as f1440a = new as(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1441b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1442c;
    /* access modifiers changed from: private */
    public i d;
    private byte e;
    private int f;

    private as(at atVar) {
        super(atVar);
        this.e = -1;
        this.f = -1;
    }

    private as(boolean z) {
        this.e = -1;
        this.f = -1;
    }

    public static as a() {
        return f1440a;
    }

    public boolean b() {
        return (this.f1441b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1442c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1442c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString i() {
        Object obj = this.f1442c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1442c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1441b & 2) == 2;
    }

    public i e() {
        return this.d;
    }

    private void j() {
        this.f1442c = "";
        this.d = i.GV_GENERIC;
    }

    public final boolean isInitialized() {
        byte b2 = this.e;
        if (b2 == -1) {
            this.e = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1441b & 1) == 1) {
            codedOutputStream.writeBytes(1, i());
        }
        if ((this.f1441b & 2) == 2) {
            codedOutputStream.writeEnum(2, this.d.getNumber());
        }
    }

    public int getSerializedSize() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if ((this.f1441b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, i());
            }
            if ((this.f1441b & 2) == 2) {
                i += CodedOutputStream.computeEnumSize(2, this.d.getNumber());
            }
            this.f = i;
        }
        return i;
    }

    public static as a(byte[] bArr) {
        return ((at) f().mergeFrom(bArr)).h();
    }

    public static at f() {
        return at.g();
    }

    /* renamed from: g */
    public at newBuilderForType() {
        return f();
    }

    public static at a(as asVar) {
        return f().mergeFrom(asVar);
    }

    /* renamed from: h */
    public at toBuilder() {
        return a(this);
    }

    static {
        f1440a.j();
    }
}
