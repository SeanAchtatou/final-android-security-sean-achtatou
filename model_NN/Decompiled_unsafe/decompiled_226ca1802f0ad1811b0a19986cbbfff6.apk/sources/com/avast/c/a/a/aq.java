package com.avast.c.a.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: Billing */
public final class aq extends GeneratedMessageLite.Builder<ap, aq> implements ar {

    /* renamed from: a  reason: collision with root package name */
    private int f1437a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1438b = "";

    /* renamed from: c  reason: collision with root package name */
    private Object f1439c = "";
    private ag d = ag.GOOGLE_PLAY;
    private k e = k.a();
    private int f;

    private aq() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static aq i() {
        return new aq();
    }

    /* renamed from: a */
    public aq clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public ap getDefaultInstanceForType() {
        return ap.a();
    }

    /* renamed from: c */
    public ap build() {
        ap d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public ap d() {
        int i = 1;
        ap apVar = new ap(this);
        int i2 = this.f1437a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        Object unused = apVar.f1436c = this.f1438b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        Object unused2 = apVar.d = this.f1439c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        ag unused3 = apVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 8;
        }
        k unused4 = apVar.f = this.e;
        if ((i2 & 16) == 16) {
            i |= 16;
        }
        int unused5 = apVar.g = this.f;
        int unused6 = apVar.f1435b = i;
        return apVar;
    }

    /* renamed from: a */
    public aq mergeFrom(ap apVar) {
        if (apVar != ap.a()) {
            if (apVar.b()) {
                a(apVar.c());
            }
            if (apVar.d()) {
                b(apVar.e());
            }
            if (apVar.f()) {
                a(apVar.g());
            }
            if (apVar.h()) {
                b(apVar.i());
            }
            if (apVar.j()) {
                a(apVar.k());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (!e() || f().isInitialized()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public aq mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1437a |= 1;
                    this.f1438b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1437a |= 2;
                    this.f1439c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    ag a2 = ag.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1437a |= 4;
                        this.d = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    l K = k.K();
                    if (e()) {
                        K.mergeFrom(f());
                    }
                    codedInputStream.readMessage(K, extensionRegistryLite);
                    a(K.d());
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f1437a |= 16;
                    this.f = codedInputStream.readInt32();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public aq a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1437a |= 1;
        this.f1438b = str;
        return this;
    }

    public aq b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1437a |= 2;
        this.f1439c = str;
        return this;
    }

    public aq a(ag agVar) {
        if (agVar == null) {
            throw new NullPointerException();
        }
        this.f1437a |= 4;
        this.d = agVar;
        return this;
    }

    public boolean e() {
        return (this.f1437a & 8) == 8;
    }

    public k f() {
        return this.e;
    }

    public aq a(k kVar) {
        if (kVar == null) {
            throw new NullPointerException();
        }
        this.e = kVar;
        this.f1437a |= 8;
        return this;
    }

    public aq b(k kVar) {
        if ((this.f1437a & 8) != 8 || this.e == k.a()) {
            this.e = kVar;
        } else {
            this.e = k.a(this.e).mergeFrom(kVar).d();
        }
        this.f1437a |= 8;
        return this;
    }

    public aq a(int i) {
        this.f1437a |= 16;
        this.f = i;
        return this;
    }
}
