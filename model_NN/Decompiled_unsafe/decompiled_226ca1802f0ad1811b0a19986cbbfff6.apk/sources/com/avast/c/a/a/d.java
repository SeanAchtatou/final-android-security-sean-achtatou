package com.avast.c.a.a;

import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: Billing */
public final class d extends GeneratedMessageLite.Builder<c, d> implements e {

    /* renamed from: a  reason: collision with root package name */
    private int f1449a;

    /* renamed from: b  reason: collision with root package name */
    private int f1450b;

    /* renamed from: c  reason: collision with root package name */
    private Object f1451c = "";
    private Object d = "";
    private Object e = "";
    private int f;
    private an g = an.SUITE;
    private int h;
    private Object i = "";

    private d() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static d g() {
        return new d();
    }

    /* renamed from: a */
    public d clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public c getDefaultInstanceForType() {
        return c.a();
    }

    /* renamed from: c */
    public c build() {
        c d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public c d() {
        int i2 = 1;
        c cVar = new c(this);
        int i3 = this.f1449a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        int unused = cVar.f1448c = this.f1450b;
        if ((i3 & 2) == 2) {
            i2 |= 2;
        }
        Object unused2 = cVar.d = this.f1451c;
        if ((i3 & 4) == 4) {
            i2 |= 4;
        }
        Object unused3 = cVar.e = this.d;
        if ((i3 & 8) == 8) {
            i2 |= 8;
        }
        Object unused4 = cVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 16;
        }
        int unused5 = cVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 32;
        }
        an unused6 = cVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 64;
        }
        int unused7 = cVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        Object unused8 = cVar.j = this.i;
        int unused9 = cVar.f1447b = i2;
        return cVar;
    }

    /* renamed from: a */
    public d mergeFrom(c cVar) {
        if (cVar != c.a()) {
            if (cVar.b()) {
                a(cVar.c());
            }
            if (cVar.d()) {
                a(cVar.e());
            }
            if (cVar.f()) {
                b(cVar.g());
            }
            if (cVar.h()) {
                c(cVar.i());
            }
            if (cVar.j()) {
                b(cVar.k());
            }
            if (cVar.l()) {
                a(cVar.m());
            }
            if (cVar.n()) {
                c(cVar.o());
            }
            if (cVar.p()) {
                d(cVar.q());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public d mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    this.f1449a |= 1;
                    this.f1450b = codedInputStream.readInt32();
                    break;
                case 18:
                    this.f1449a |= 2;
                    this.f1451c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1449a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f1449a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f1449a |= 16;
                    this.f = codedInputStream.readInt32();
                    break;
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                    an a2 = an.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1449a |= 32;
                        this.g = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    this.f1449a |= 64;
                    this.h = codedInputStream.readInt32();
                    break;
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f1449a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public d a(int i2) {
        this.f1449a |= 1;
        this.f1450b = i2;
        return this;
    }

    public d a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1449a |= 2;
        this.f1451c = str;
        return this;
    }

    public d b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1449a |= 4;
        this.d = str;
        return this;
    }

    public d c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1449a |= 8;
        this.e = str;
        return this;
    }

    public d b(int i2) {
        this.f1449a |= 16;
        this.f = i2;
        return this;
    }

    public d a(an anVar) {
        if (anVar == null) {
            throw new NullPointerException();
        }
        this.f1449a |= 32;
        this.g = anVar;
        return this;
    }

    public d c(int i2) {
        this.f1449a |= 64;
        this.h = i2;
        return this;
    }

    public d d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1449a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = str;
        return this;
    }
}
