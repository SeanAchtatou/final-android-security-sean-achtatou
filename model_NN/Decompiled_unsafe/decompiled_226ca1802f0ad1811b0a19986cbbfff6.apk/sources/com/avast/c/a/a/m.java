package com.avast.c.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: Billing */
public final class m extends GeneratedMessageLite implements o {

    /* renamed from: a  reason: collision with root package name */
    private static final m f1467a = new m(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1468b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1469c;
    /* access modifiers changed from: private */
    public Object d;
    private byte e;
    private int f;

    private m(n nVar) {
        super(nVar);
        this.e = -1;
        this.f = -1;
    }

    private m(boolean z) {
        this.e = -1;
        this.f = -1;
    }

    public static m a() {
        return f1467a;
    }

    public boolean b() {
        return (this.f1468b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1469c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1469c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString i() {
        Object obj = this.f1469c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1469c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1468b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString j() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    private void k() {
        this.f1469c = "";
        this.d = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.e;
        if (b2 == -1) {
            this.e = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1468b & 1) == 1) {
            codedOutputStream.writeBytes(1, i());
        }
        if ((this.f1468b & 2) == 2) {
            codedOutputStream.writeBytes(2, j());
        }
    }

    public int getSerializedSize() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if ((this.f1468b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, i());
            }
            if ((this.f1468b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, j());
            }
            this.f = i;
        }
        return i;
    }

    public static n f() {
        return n.g();
    }

    /* renamed from: g */
    public n newBuilderForType() {
        return f();
    }

    public static n a(m mVar) {
        return f().mergeFrom(mVar);
    }

    /* renamed from: h */
    public n toBuilder() {
        return a(this);
    }

    static {
        f1467a.k();
    }
}
