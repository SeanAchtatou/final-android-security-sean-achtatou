package com.avast.c.a.a;

import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: Billing */
public final class ab extends GeneratedMessageLite.Builder<aa, ab> implements ac {

    /* renamed from: a  reason: collision with root package name */
    private int f1410a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1411b = "";

    /* renamed from: c  reason: collision with root package name */
    private Object f1412c = "";
    private Object d = "";
    private an e = an.SUITE;
    private Object f = "";
    private int g;
    private Object h = "";
    private int i;
    private Object j = "";
    private Object k = "";
    private Object l = "";
    private Object m = "";

    private ab() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static ab g() {
        return new ab();
    }

    /* renamed from: a */
    public ab clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public aa getDefaultInstanceForType() {
        return aa.a();
    }

    /* renamed from: c */
    public aa build() {
        aa d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public aa d() {
        int i2 = 1;
        aa aaVar = new aa(this);
        int i3 = this.f1410a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        Object unused = aaVar.f1409c = this.f1411b;
        if ((i3 & 2) == 2) {
            i2 |= 2;
        }
        Object unused2 = aaVar.d = this.f1412c;
        if ((i3 & 4) == 4) {
            i2 |= 4;
        }
        Object unused3 = aaVar.e = this.d;
        if ((i3 & 8) == 8) {
            i2 |= 8;
        }
        an unused4 = aaVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 16;
        }
        Object unused5 = aaVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 32;
        }
        int unused6 = aaVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 64;
        }
        Object unused7 = aaVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        int unused8 = aaVar.j = this.i;
        if ((i3 & 256) == 256) {
            i2 |= 256;
        }
        Object unused9 = aaVar.k = this.j;
        if ((i3 & 512) == 512) {
            i2 |= 512;
        }
        Object unused10 = aaVar.l = this.k;
        if ((i3 & 1024) == 1024) {
            i2 |= 1024;
        }
        Object unused11 = aaVar.m = this.l;
        if ((i3 & 2048) == 2048) {
            i2 |= 2048;
        }
        Object unused12 = aaVar.n = this.m;
        int unused13 = aaVar.f1408b = i2;
        return aaVar;
    }

    /* renamed from: a */
    public ab mergeFrom(aa aaVar) {
        if (aaVar != aa.a()) {
            if (aaVar.b()) {
                a(aaVar.c());
            }
            if (aaVar.d()) {
                b(aaVar.e());
            }
            if (aaVar.f()) {
                c(aaVar.g());
            }
            if (aaVar.h()) {
                a(aaVar.i());
            }
            if (aaVar.j()) {
                d(aaVar.k());
            }
            if (aaVar.l()) {
                a(aaVar.m());
            }
            if (aaVar.n()) {
                e(aaVar.o());
            }
            if (aaVar.p()) {
                b(aaVar.q());
            }
            if (aaVar.r()) {
                f(aaVar.s());
            }
            if (aaVar.t()) {
                g(aaVar.u());
            }
            if (aaVar.v()) {
                h(aaVar.w());
            }
            if (aaVar.x()) {
                i(aaVar.y());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public ab mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1410a |= 1;
                    this.f1411b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1410a |= 2;
                    this.f1412c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1410a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    an a2 = an.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1410a |= 8;
                        this.e = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f1410a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                    this.f1410a |= 32;
                    this.g = codedInputStream.readInt32();
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    this.f1410a |= 64;
                    this.h = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_activityChooserViewStyle:
                    this.f1410a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readInt32();
                    break;
                case 74:
                    this.f1410a |= 256;
                    this.j = codedInputStream.readBytes();
                    break;
                case 82:
                    this.f1410a |= 512;
                    this.k = codedInputStream.readBytes();
                    break;
                case 90:
                    this.f1410a |= 1024;
                    this.l = codedInputStream.readBytes();
                    break;
                case 98:
                    this.f1410a |= 2048;
                    this.m = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public ab a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1410a |= 1;
        this.f1411b = str;
        return this;
    }

    public ab b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1410a |= 2;
        this.f1412c = str;
        return this;
    }

    public ab c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1410a |= 4;
        this.d = str;
        return this;
    }

    public ab a(an anVar) {
        if (anVar == null) {
            throw new NullPointerException();
        }
        this.f1410a |= 8;
        this.e = anVar;
        return this;
    }

    public ab d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1410a |= 16;
        this.f = str;
        return this;
    }

    public ab a(int i2) {
        this.f1410a |= 32;
        this.g = i2;
        return this;
    }

    public ab e(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1410a |= 64;
        this.h = str;
        return this;
    }

    public ab b(int i2) {
        this.f1410a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = i2;
        return this;
    }

    public ab f(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1410a |= 256;
        this.j = str;
        return this;
    }

    public ab g(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1410a |= 512;
        this.k = str;
        return this;
    }

    public ab h(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1410a |= 1024;
        this.l = str;
        return this;
    }

    public ab i(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1410a |= 2048;
        this.m = str;
        return this;
    }
}
