package com.avast.c.a.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: Billing */
public final class n extends GeneratedMessageLite.Builder<m, n> implements o {

    /* renamed from: a  reason: collision with root package name */
    private int f1470a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1471b = "";

    /* renamed from: c  reason: collision with root package name */
    private Object f1472c = "";

    private n() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static n g() {
        return new n();
    }

    /* renamed from: a */
    public n clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public m getDefaultInstanceForType() {
        return m.a();
    }

    /* renamed from: c */
    public m build() {
        m d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public m d() {
        int i = 1;
        m mVar = new m(this);
        int i2 = this.f1470a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        Object unused = mVar.f1469c = this.f1471b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        Object unused2 = mVar.d = this.f1472c;
        int unused3 = mVar.f1468b = i;
        return mVar;
    }

    /* renamed from: a */
    public n mergeFrom(m mVar) {
        if (mVar != m.a()) {
            if (mVar.b()) {
                a(mVar.c());
            }
            if (mVar.d()) {
                b(mVar.e());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public n mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1470a |= 1;
                    this.f1471b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1470a |= 2;
                    this.f1472c = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public n a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1470a |= 1;
        this.f1471b = str;
        return this;
    }

    public n b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1470a |= 2;
        this.f1472c = str;
        return this;
    }
}
