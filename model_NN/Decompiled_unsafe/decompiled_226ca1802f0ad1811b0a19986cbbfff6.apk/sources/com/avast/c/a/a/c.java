package com.avast.c.a.a;

import android.support.v4.app.NotificationCompat;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: Billing */
public final class c extends GeneratedMessageLite implements e {

    /* renamed from: a  reason: collision with root package name */
    private static final c f1446a = new c(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1447b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public int f1448c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public Object e;
    /* access modifiers changed from: private */
    public Object f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public an h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public Object j;
    private byte k;
    private int l;

    private c(d dVar) {
        super(dVar);
        this.k = -1;
        this.l = -1;
    }

    private c(boolean z) {
        this.k = -1;
        this.l = -1;
    }

    public static c a() {
        return f1446a;
    }

    public boolean b() {
        return (this.f1447b & 1) == 1;
    }

    public int c() {
        return this.f1448c;
    }

    public boolean d() {
        return (this.f1447b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString u() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1447b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString v() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean h() {
        return (this.f1447b & 8) == 8;
    }

    public String i() {
        Object obj = this.f;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString w() {
        Object obj = this.f;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean j() {
        return (this.f1447b & 16) == 16;
    }

    public int k() {
        return this.g;
    }

    public boolean l() {
        return (this.f1447b & 32) == 32;
    }

    public an m() {
        return this.h;
    }

    public boolean n() {
        return (this.f1447b & 64) == 64;
    }

    public int o() {
        return this.i;
    }

    public boolean p() {
        return (this.f1447b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public String q() {
        Object obj = this.j;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.j = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString x() {
        Object obj = this.j;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.j = copyFromUtf8;
        return copyFromUtf8;
    }

    private void y() {
        this.f1448c = 0;
        this.d = "";
        this.e = "";
        this.f = "";
        this.g = 0;
        this.h = an.SUITE;
        this.i = 0;
        this.j = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.k;
        if (b2 == -1) {
            this.k = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1447b & 1) == 1) {
            codedOutputStream.writeInt32(1, this.f1448c);
        }
        if ((this.f1447b & 2) == 2) {
            codedOutputStream.writeBytes(2, u());
        }
        if ((this.f1447b & 4) == 4) {
            codedOutputStream.writeBytes(3, v());
        }
        if ((this.f1447b & 8) == 8) {
            codedOutputStream.writeBytes(4, w());
        }
        if ((this.f1447b & 16) == 16) {
            codedOutputStream.writeInt32(5, this.g);
        }
        if ((this.f1447b & 32) == 32) {
            codedOutputStream.writeEnum(6, this.h.getNumber());
        }
        if ((this.f1447b & 64) == 64) {
            codedOutputStream.writeInt32(7, this.i);
        }
        if ((this.f1447b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBytes(8, x());
        }
    }

    public int getSerializedSize() {
        int i2 = this.l;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1447b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeInt32Size(1, this.f1448c);
            }
            if ((this.f1447b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, u());
            }
            if ((this.f1447b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, v());
            }
            if ((this.f1447b & 8) == 8) {
                i2 += CodedOutputStream.computeBytesSize(4, w());
            }
            if ((this.f1447b & 16) == 16) {
                i2 += CodedOutputStream.computeInt32Size(5, this.g);
            }
            if ((this.f1447b & 32) == 32) {
                i2 += CodedOutputStream.computeEnumSize(6, this.h.getNumber());
            }
            if ((this.f1447b & 64) == 64) {
                i2 += CodedOutputStream.computeInt32Size(7, this.i);
            }
            if ((this.f1447b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeBytesSize(8, x());
            }
            this.l = i2;
        }
        return i2;
    }

    public static d r() {
        return d.g();
    }

    /* renamed from: s */
    public d newBuilderForType() {
        return r();
    }

    public static d a(c cVar) {
        return r().mergeFrom(cVar);
    }

    /* renamed from: t */
    public d toBuilder() {
        return a(this);
    }

    static {
        f1446a.y();
    }
}
