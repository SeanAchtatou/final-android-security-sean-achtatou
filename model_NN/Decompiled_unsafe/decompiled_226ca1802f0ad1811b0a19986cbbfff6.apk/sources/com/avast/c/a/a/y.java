package com.avast.c.a.a;

import com.google.protobuf.Internal;

/* compiled from: Billing */
public enum y implements Internal.EnumLite {
    SUBSCRIPTION_MONTHLY(0, 1),
    SUBSCRIPTION_YEARLY(1, 2),
    ONE_TIME_PURCHASE_ONE_MONTH(2, 3),
    ONE_TIME_PURCHASE_ONE_YEAR(3, 4),
    ONE_TIME_PURCHASE_LIFETIME(4, 5),
    ONE_TIME_PURCHASE_CUSTOM_DURATION(5, 6);
    
    private static Internal.EnumLiteMap<y> g = new z();
    private final int h;

    public final int getNumber() {
        return this.h;
    }

    public static y a(int i2) {
        switch (i2) {
            case 1:
                return SUBSCRIPTION_MONTHLY;
            case 2:
                return SUBSCRIPTION_YEARLY;
            case 3:
                return ONE_TIME_PURCHASE_ONE_MONTH;
            case 4:
                return ONE_TIME_PURCHASE_ONE_YEAR;
            case 5:
                return ONE_TIME_PURCHASE_LIFETIME;
            case 6:
                return ONE_TIME_PURCHASE_CUSTOM_DURATION;
            default:
                return null;
        }
    }

    private y(int i2, int i3) {
        this.h = i3;
    }
}
