package com.avast.c.a.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: Billing */
public final class u extends GeneratedMessageLite.Builder<t, u> implements x {

    /* renamed from: a  reason: collision with root package name */
    private int f1482a;

    /* renamed from: b  reason: collision with root package name */
    private List<q> f1483b = Collections.emptyList();

    /* renamed from: c  reason: collision with root package name */
    private v f1484c = v.VOUCHER_CODE_UNKNOWN;
    private Object d = "";

    private u() {
        g();
    }

    private void g() {
    }

    /* access modifiers changed from: private */
    public static u h() {
        return new u();
    }

    /* renamed from: a */
    public u clone() {
        return h().mergeFrom(d());
    }

    /* renamed from: b */
    public t getDefaultInstanceForType() {
        return t.a();
    }

    /* renamed from: c */
    public t build() {
        t d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    /* access modifiers changed from: private */
    public t i() {
        t d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2).asInvalidProtocolBufferException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.c.a.a.t.a(com.avast.c.a.a.t, java.util.List):java.util.List
     arg types: [com.avast.c.a.a.t, java.util.List<com.avast.c.a.a.q>]
     candidates:
      com.avast.c.a.a.t.a(com.avast.c.a.a.t, int):int
      com.avast.c.a.a.t.a(com.avast.c.a.a.t, com.avast.c.a.a.v):com.avast.c.a.a.v
      com.avast.c.a.a.t.a(com.avast.c.a.a.t, java.lang.Object):java.lang.Object
      com.avast.c.a.a.t.a(com.avast.c.a.a.t, java.util.List):java.util.List */
    public t d() {
        int i = 1;
        t tVar = new t(this);
        int i2 = this.f1482a;
        if ((this.f1482a & 1) == 1) {
            this.f1483b = Collections.unmodifiableList(this.f1483b);
            this.f1482a &= -2;
        }
        List unused = tVar.f1481c = (List) this.f1483b;
        if ((i2 & 2) != 2) {
            i = 0;
        }
        v unused2 = tVar.d = this.f1484c;
        if ((i2 & 4) == 4) {
            i |= 2;
        }
        Object unused3 = tVar.e = this.d;
        int unused4 = tVar.f1480b = i;
        return tVar;
    }

    /* renamed from: a */
    public u mergeFrom(t tVar) {
        if (tVar != t.a()) {
            if (!tVar.f1481c.isEmpty()) {
                if (this.f1483b.isEmpty()) {
                    this.f1483b = tVar.f1481c;
                    this.f1482a &= -2;
                } else {
                    j();
                    this.f1483b.addAll(tVar.f1481c);
                }
            }
            if (tVar.d()) {
                a(tVar.e());
            }
            if (tVar.f()) {
                a(tVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        for (int i = 0; i < e(); i++) {
            if (!a(i).isInitialized()) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public u mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    r T = q.T();
                    codedInputStream.readMessage(T, extensionRegistryLite);
                    a(T.d());
                    break;
                case 16:
                    v a2 = v.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1482a |= 2;
                        this.f1484c = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1482a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    private void j() {
        if ((this.f1482a & 1) != 1) {
            this.f1483b = new ArrayList(this.f1483b);
            this.f1482a |= 1;
        }
    }

    public int e() {
        return this.f1483b.size();
    }

    public q a(int i) {
        return this.f1483b.get(i);
    }

    public u a(q qVar) {
        if (qVar == null) {
            throw new NullPointerException();
        }
        j();
        this.f1483b.add(qVar);
        return this;
    }

    public u a(v vVar) {
        if (vVar == null) {
            throw new NullPointerException();
        }
        this.f1482a |= 2;
        this.f1484c = vVar;
        return this;
    }

    public u a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1482a |= 4;
        this.d = str;
        return this;
    }
}
