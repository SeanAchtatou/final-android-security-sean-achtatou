package com.avast.c.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: Billing */
public final class ap extends GeneratedMessageLite implements ar {

    /* renamed from: a  reason: collision with root package name */
    private static final ap f1434a = new ap(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1435b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1436c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public ag e;
    /* access modifiers changed from: private */
    public k f;
    /* access modifiers changed from: private */
    public int g;
    private byte h;
    private int i;

    private ap(aq aqVar) {
        super(aqVar);
        this.h = -1;
        this.i = -1;
    }

    private ap(boolean z) {
        this.h = -1;
        this.i = -1;
    }

    public static ap a() {
        return f1434a;
    }

    public boolean b() {
        return (this.f1435b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1436c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1436c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString o() {
        Object obj = this.f1436c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1436c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1435b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString p() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1435b & 4) == 4;
    }

    public ag g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1435b & 8) == 8;
    }

    public k i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1435b & 16) == 16;
    }

    public int k() {
        return this.g;
    }

    private void q() {
        this.f1436c = "";
        this.d = "";
        this.e = ag.GOOGLE_PLAY;
        this.f = k.a();
        this.g = 0;
    }

    public final boolean isInitialized() {
        byte b2 = this.h;
        if (b2 != -1) {
            if (b2 == 1) {
                return true;
            }
            return false;
        } else if (!h() || i().isInitialized()) {
            this.h = 1;
            return true;
        } else {
            this.h = 0;
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1435b & 1) == 1) {
            codedOutputStream.writeBytes(1, o());
        }
        if ((this.f1435b & 2) == 2) {
            codedOutputStream.writeBytes(2, p());
        }
        if ((this.f1435b & 4) == 4) {
            codedOutputStream.writeEnum(3, this.e.getNumber());
        }
        if ((this.f1435b & 8) == 8) {
            codedOutputStream.writeMessage(4, this.f);
        }
        if ((this.f1435b & 16) == 16) {
            codedOutputStream.writeInt32(5, this.g);
        }
    }

    public int getSerializedSize() {
        int i2 = this.i;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1435b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, o());
            }
            if ((this.f1435b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, p());
            }
            if ((this.f1435b & 4) == 4) {
                i2 += CodedOutputStream.computeEnumSize(3, this.e.getNumber());
            }
            if ((this.f1435b & 8) == 8) {
                i2 += CodedOutputStream.computeMessageSize(4, this.f);
            }
            if ((this.f1435b & 16) == 16) {
                i2 += CodedOutputStream.computeInt32Size(5, this.g);
            }
            this.i = i2;
        }
        return i2;
    }

    public static aq l() {
        return aq.i();
    }

    /* renamed from: m */
    public aq newBuilderForType() {
        return l();
    }

    public static aq a(ap apVar) {
        return l().mergeFrom(apVar);
    }

    /* renamed from: n */
    public aq toBuilder() {
        return a(this);
    }

    static {
        f1434a.q();
    }
}
