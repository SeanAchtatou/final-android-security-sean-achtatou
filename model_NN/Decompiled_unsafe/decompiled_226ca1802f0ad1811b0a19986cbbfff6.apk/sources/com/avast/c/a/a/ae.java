package com.avast.c.a.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: Billing */
public final class ae extends GeneratedMessageLite.Builder<ad, ae> implements af {

    /* renamed from: a  reason: collision with root package name */
    private int f1416a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1417b = "";

    /* renamed from: c  reason: collision with root package name */
    private Object f1418c = "";
    private Object d = "";

    private ae() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static ae g() {
        return new ae();
    }

    /* renamed from: a */
    public ae clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public ad getDefaultInstanceForType() {
        return ad.a();
    }

    /* renamed from: c */
    public ad build() {
        ad d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    /* access modifiers changed from: private */
    public ad h() {
        ad d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2).asInvalidProtocolBufferException();
    }

    public ad d() {
        int i = 1;
        ad adVar = new ad(this);
        int i2 = this.f1416a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        Object unused = adVar.f1415c = this.f1417b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        Object unused2 = adVar.d = this.f1418c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        Object unused3 = adVar.e = this.d;
        int unused4 = adVar.f1414b = i;
        return adVar;
    }

    /* renamed from: a */
    public ae mergeFrom(ad adVar) {
        if (adVar != ad.a()) {
            if (adVar.b()) {
                a(adVar.c());
            }
            if (adVar.d()) {
                b(adVar.e());
            }
            if (adVar.f()) {
                c(adVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public ae mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1416a |= 1;
                    this.f1417b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1416a |= 2;
                    this.f1418c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1416a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public ae a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1416a |= 1;
        this.f1417b = str;
        return this;
    }

    public ae b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1416a |= 2;
        this.f1418c = str;
        return this;
    }

    public ae c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1416a |= 4;
        this.d = str;
        return this;
    }
}
