package com.avast.c.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: Billing */
public final class ai extends GeneratedMessageLite implements ak {

    /* renamed from: a  reason: collision with root package name */
    private static final ai f1422a = new ai(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1423b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public al f1424c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public Object e;
    private byte f;
    private int g;

    private ai(aj ajVar) {
        super(ajVar);
        this.f = -1;
        this.g = -1;
    }

    private ai(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static ai a() {
        return f1422a;
    }

    public boolean b() {
        return (this.f1423b & 1) == 1;
    }

    public al c() {
        return this.f1424c;
    }

    public boolean d() {
        return (this.f1423b & 2) == 2;
    }

    public int e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1423b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString k() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    private void l() {
        this.f1424c = al.MANUFACTURER_STORE;
        this.d = 0;
        this.e = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.f;
        if (b2 == -1) {
            this.f = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1423b & 1) == 1) {
            codedOutputStream.writeEnum(1, this.f1424c.getNumber());
        }
        if ((this.f1423b & 2) == 2) {
            codedOutputStream.writeInt32(2, this.d);
        }
        if ((this.f1423b & 4) == 4) {
            codedOutputStream.writeBytes(3, k());
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            if ((this.f1423b & 1) == 1) {
                i = 0 + CodedOutputStream.computeEnumSize(1, this.f1424c.getNumber());
            }
            if ((this.f1423b & 2) == 2) {
                i += CodedOutputStream.computeInt32Size(2, this.d);
            }
            if ((this.f1423b & 4) == 4) {
                i += CodedOutputStream.computeBytesSize(3, k());
            }
            this.g = i;
        }
        return i;
    }

    public static aj h() {
        return aj.g();
    }

    /* renamed from: i */
    public aj newBuilderForType() {
        return h();
    }

    public static aj a(ai aiVar) {
        return h().mergeFrom(aiVar);
    }

    /* renamed from: j */
    public aj toBuilder() {
        return a(this);
    }

    static {
        f1422a.l();
    }
}
