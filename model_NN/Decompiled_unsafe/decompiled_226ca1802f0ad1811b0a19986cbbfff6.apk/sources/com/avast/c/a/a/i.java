package com.avast.c.a.a;

import com.google.protobuf.Internal;

/* compiled from: Billing */
public enum i implements Internal.EnumLite {
    GV_GENERIC(0, 1),
    GV_TOO_MANY_ACCOUNTS(1, 2),
    GV_TOO_MANY_GUIDS(2, 3),
    GV_NON_UNIQUE_GUID(3, 4),
    GV_VOUCHER_CODE_UNKNOWN(4, 5),
    GV_VOUCHER_CODE_ALREADY_CONSUMED(5, 6),
    GV_VOUCHER_CODE_LOCKED(6, 9),
    GV_VOUCHER_CODE_WRONG_LICENSE(7, 10),
    GV_LOCK_LICENSE_AS_IT_IS_INVALID(8, 11),
    GV_NO_IDENTITIES(9, 12),
    GV_LICENSE_ALREADY_EXPIRED(10, 13);
    
    private static Internal.EnumLiteMap<i> l = new j();
    private final int m;

    public final int getNumber() {
        return this.m;
    }

    public static i a(int i) {
        switch (i) {
            case 1:
                return GV_GENERIC;
            case 2:
                return GV_TOO_MANY_ACCOUNTS;
            case 3:
                return GV_TOO_MANY_GUIDS;
            case 4:
                return GV_NON_UNIQUE_GUID;
            case 5:
                return GV_VOUCHER_CODE_UNKNOWN;
            case 6:
                return GV_VOUCHER_CODE_ALREADY_CONSUMED;
            case 7:
            case 8:
            default:
                return null;
            case 9:
                return GV_VOUCHER_CODE_LOCKED;
            case 10:
                return GV_VOUCHER_CODE_WRONG_LICENSE;
            case 11:
                return GV_LOCK_LICENSE_AS_IT_IS_INVALID;
            case 12:
                return GV_NO_IDENTITIES;
            case 13:
                return GV_LICENSE_ALREADY_EXPIRED;
        }
    }

    private i(int i, int i2) {
        this.m = i2;
    }
}
