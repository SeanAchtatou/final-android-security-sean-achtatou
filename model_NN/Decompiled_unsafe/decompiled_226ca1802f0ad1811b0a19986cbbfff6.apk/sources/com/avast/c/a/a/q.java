package com.avast.c.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.view.Menu;
import com.avast.c.b.a.a.c;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import java.util.Collections;
import java.util.List;

/* compiled from: Billing */
public final class q extends GeneratedMessageLite implements s {

    /* renamed from: a  reason: collision with root package name */
    private static final q f1473a = new q(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1474b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1475c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public y e;
    /* access modifiers changed from: private */
    public float f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public long h;
    /* access modifiers changed from: private */
    public long i;
    /* access modifiers changed from: private */
    public Object j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public Object l;
    /* access modifiers changed from: private */
    public Object m;
    /* access modifiers changed from: private */
    public Object n;
    /* access modifiers changed from: private */
    public Object o;
    /* access modifiers changed from: private */
    public Object p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public int r;
    /* access modifiers changed from: private */
    public int s;
    /* access modifiers changed from: private */
    public c t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public List<ai> v;
    /* access modifiers changed from: private */
    public Object w;
    /* access modifiers changed from: private */
    public Object x;
    private byte y;
    private int z;

    private q(r rVar) {
        super(rVar);
        this.y = -1;
        this.z = -1;
    }

    private q(boolean z2) {
        this.y = -1;
        this.z = -1;
    }

    public static q a() {
        return f1473a;
    }

    public boolean b() {
        return (this.f1474b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1475c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1475c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString W() {
        Object obj = this.f1475c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1475c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1474b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString X() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1474b & 4) == 4;
    }

    public y g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1474b & 8) == 8;
    }

    public float i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1474b & 16) == 16;
    }

    public boolean k() {
        return this.g;
    }

    public boolean l() {
        return (this.f1474b & 32) == 32;
    }

    public long m() {
        return this.h;
    }

    public boolean n() {
        return (this.f1474b & 64) == 64;
    }

    public long o() {
        return this.i;
    }

    public boolean p() {
        return (this.f1474b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public String q() {
        Object obj = this.j;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.j = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString Y() {
        Object obj = this.j;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.j = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean r() {
        return (this.f1474b & 256) == 256;
    }

    public boolean s() {
        return this.k;
    }

    public boolean t() {
        return (this.f1474b & 512) == 512;
    }

    public String u() {
        Object obj = this.l;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.l = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString Z() {
        Object obj = this.l;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.l = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean v() {
        return (this.f1474b & 1024) == 1024;
    }

    public String w() {
        Object obj = this.m;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.m = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString aa() {
        Object obj = this.m;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.m = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean x() {
        return (this.f1474b & 2048) == 2048;
    }

    public String y() {
        Object obj = this.n;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.n = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ab() {
        Object obj = this.n;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.n = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean z() {
        return (this.f1474b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096;
    }

    public String A() {
        Object obj = this.o;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.o = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ac() {
        Object obj = this.o;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.o = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean B() {
        return (this.f1474b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192;
    }

    public String C() {
        Object obj = this.p;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.p = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ad() {
        Object obj = this.p;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.p = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean D() {
        return (this.f1474b & 16384) == 16384;
    }

    public int E() {
        return this.q;
    }

    public boolean F() {
        return (this.f1474b & 32768) == 32768;
    }

    public int G() {
        return this.r;
    }

    public boolean H() {
        return (this.f1474b & Menu.CATEGORY_CONTAINER) == 65536;
    }

    public int I() {
        return this.s;
    }

    public boolean J() {
        return (this.f1474b & Menu.CATEGORY_SYSTEM) == 131072;
    }

    public c K() {
        return this.t;
    }

    public boolean L() {
        return (this.f1474b & Menu.CATEGORY_ALTERNATIVE) == 262144;
    }

    public int M() {
        return this.u;
    }

    public List<ai> N() {
        return this.v;
    }

    public int O() {
        return this.v.size();
    }

    public boolean P() {
        return (this.f1474b & 524288) == 524288;
    }

    public String Q() {
        Object obj = this.w;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.w = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString ae() {
        Object obj = this.w;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.w = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean R() {
        return (this.f1474b & 1048576) == 1048576;
    }

    public String S() {
        Object obj = this.x;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.x = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString af() {
        Object obj = this.x;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.x = copyFromUtf8;
        return copyFromUtf8;
    }

    private void ag() {
        this.f1475c = "";
        this.d = "";
        this.e = y.SUBSCRIPTION_MONTHLY;
        this.f = 0.0f;
        this.g = false;
        this.h = 0;
        this.i = 0;
        this.j = "";
        this.k = false;
        this.l = "";
        this.m = "";
        this.n = "";
        this.o = "";
        this.p = "";
        this.q = 0;
        this.r = 0;
        this.s = 0;
        this.t = c.a();
        this.u = 0;
        this.v = Collections.emptyList();
        this.w = "";
        this.x = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.y;
        if (b2 != -1) {
            if (b2 == 1) {
                return true;
            }
            return false;
        } else if (!b()) {
            this.y = 0;
            return false;
        } else {
            this.y = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1474b & 1) == 1) {
            codedOutputStream.writeBytes(1, W());
        }
        if ((this.f1474b & 2) == 2) {
            codedOutputStream.writeBytes(2, X());
        }
        if ((this.f1474b & 4) == 4) {
            codedOutputStream.writeEnum(3, this.e.getNumber());
        }
        if ((this.f1474b & 8) == 8) {
            codedOutputStream.writeFloat(4, this.f);
        }
        if ((this.f1474b & 16) == 16) {
            codedOutputStream.writeBool(5, this.g);
        }
        if ((this.f1474b & 32) == 32) {
            codedOutputStream.writeInt64(6, this.h);
        }
        if ((this.f1474b & 64) == 64) {
            codedOutputStream.writeInt64(7, this.i);
        }
        if ((this.f1474b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBytes(8, Y());
        }
        if ((this.f1474b & 256) == 256) {
            codedOutputStream.writeBool(9, this.k);
        }
        if ((this.f1474b & 512) == 512) {
            codedOutputStream.writeBytes(10, Z());
        }
        if ((this.f1474b & 1024) == 1024) {
            codedOutputStream.writeBytes(11, aa());
        }
        if ((this.f1474b & 2048) == 2048) {
            codedOutputStream.writeBytes(12, ab());
        }
        if ((this.f1474b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            codedOutputStream.writeBytes(13, ac());
        }
        if ((this.f1474b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            codedOutputStream.writeBytes(14, ad());
        }
        if ((this.f1474b & 16384) == 16384) {
            codedOutputStream.writeInt32(15, this.q);
        }
        if ((this.f1474b & 32768) == 32768) {
            codedOutputStream.writeInt32(16, this.r);
        }
        if ((this.f1474b & Menu.CATEGORY_CONTAINER) == 65536) {
            codedOutputStream.writeInt32(17, this.s);
        }
        if ((this.f1474b & Menu.CATEGORY_SYSTEM) == 131072) {
            codedOutputStream.writeMessage(18, this.t);
        }
        if ((this.f1474b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            codedOutputStream.writeInt32(19, this.u);
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.v.size()) {
                break;
            }
            codedOutputStream.writeMessage(20, this.v.get(i3));
            i2 = i3 + 1;
        }
        if ((this.f1474b & 524288) == 524288) {
            codedOutputStream.writeBytes(21, ae());
        }
        if ((this.f1474b & 1048576) == 1048576) {
            codedOutputStream.writeBytes(22, af());
        }
    }

    public int getSerializedSize() {
        int i2;
        int i3 = 0;
        int i4 = this.z;
        if (i4 == -1) {
            if ((this.f1474b & 1) == 1) {
                i2 = CodedOutputStream.computeBytesSize(1, W()) + 0;
            } else {
                i2 = 0;
            }
            if ((this.f1474b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, X());
            }
            if ((this.f1474b & 4) == 4) {
                i2 += CodedOutputStream.computeEnumSize(3, this.e.getNumber());
            }
            if ((this.f1474b & 8) == 8) {
                i2 += CodedOutputStream.computeFloatSize(4, this.f);
            }
            if ((this.f1474b & 16) == 16) {
                i2 += CodedOutputStream.computeBoolSize(5, this.g);
            }
            if ((this.f1474b & 32) == 32) {
                i2 += CodedOutputStream.computeInt64Size(6, this.h);
            }
            if ((this.f1474b & 64) == 64) {
                i2 += CodedOutputStream.computeInt64Size(7, this.i);
            }
            if ((this.f1474b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeBytesSize(8, Y());
            }
            if ((this.f1474b & 256) == 256) {
                i2 += CodedOutputStream.computeBoolSize(9, this.k);
            }
            if ((this.f1474b & 512) == 512) {
                i2 += CodedOutputStream.computeBytesSize(10, Z());
            }
            if ((this.f1474b & 1024) == 1024) {
                i2 += CodedOutputStream.computeBytesSize(11, aa());
            }
            if ((this.f1474b & 2048) == 2048) {
                i2 += CodedOutputStream.computeBytesSize(12, ab());
            }
            if ((this.f1474b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
                i2 += CodedOutputStream.computeBytesSize(13, ac());
            }
            if ((this.f1474b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
                i2 += CodedOutputStream.computeBytesSize(14, ad());
            }
            if ((this.f1474b & 16384) == 16384) {
                i2 += CodedOutputStream.computeInt32Size(15, this.q);
            }
            if ((this.f1474b & 32768) == 32768) {
                i2 += CodedOutputStream.computeInt32Size(16, this.r);
            }
            if ((this.f1474b & Menu.CATEGORY_CONTAINER) == 65536) {
                i2 += CodedOutputStream.computeInt32Size(17, this.s);
            }
            if ((this.f1474b & Menu.CATEGORY_SYSTEM) == 131072) {
                i2 += CodedOutputStream.computeMessageSize(18, this.t);
            }
            if ((this.f1474b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
                i2 += CodedOutputStream.computeInt32Size(19, this.u);
            }
            while (true) {
                i4 = i2;
                if (i3 >= this.v.size()) {
                    break;
                }
                i2 = CodedOutputStream.computeMessageSize(20, this.v.get(i3)) + i4;
                i3++;
            }
            if ((this.f1474b & 524288) == 524288) {
                i4 += CodedOutputStream.computeBytesSize(21, ae());
            }
            if ((this.f1474b & 1048576) == 1048576) {
                i4 += CodedOutputStream.computeBytesSize(22, af());
            }
            this.z = i4;
        }
        return i4;
    }

    public static r T() {
        return r.j();
    }

    /* renamed from: U */
    public r newBuilderForType() {
        return T();
    }

    public static r a(q qVar) {
        return T().mergeFrom(qVar);
    }

    /* renamed from: V */
    public r toBuilder() {
        return a(this);
    }

    static {
        f1473a.ag();
    }
}
