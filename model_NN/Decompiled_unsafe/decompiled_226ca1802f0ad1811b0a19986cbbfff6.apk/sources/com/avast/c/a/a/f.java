package com.avast.c.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: Billing */
public final class f extends GeneratedMessageLite implements h {

    /* renamed from: a  reason: collision with root package name */
    private static final f f1452a = new f(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1453b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1454c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public Object e;
    private byte f;
    private int g;

    private f(g gVar) {
        super(gVar);
        this.f = -1;
        this.g = -1;
    }

    private f(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static f a() {
        return f1452a;
    }

    public boolean b() {
        return (this.f1453b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1454c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1454c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString k() {
        Object obj = this.f1454c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1454c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1453b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString l() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1453b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString m() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    private void n() {
        this.f1454c = "";
        this.d = "";
        this.e = "";
    }

    public final boolean isInitialized() {
        boolean z = true;
        byte b2 = this.f;
        if (b2 != -1) {
            if (b2 != 1) {
                z = false;
            }
            return z;
        } else if (!b()) {
            this.f = 0;
            return false;
        } else if (!d()) {
            this.f = 0;
            return false;
        } else if (!f()) {
            this.f = 0;
            return false;
        } else {
            this.f = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1453b & 1) == 1) {
            codedOutputStream.writeBytes(1, k());
        }
        if ((this.f1453b & 2) == 2) {
            codedOutputStream.writeBytes(2, l());
        }
        if ((this.f1453b & 4) == 4) {
            codedOutputStream.writeBytes(3, m());
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            if ((this.f1453b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, k());
            }
            if ((this.f1453b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, l());
            }
            if ((this.f1453b & 4) == 4) {
                i += CodedOutputStream.computeBytesSize(3, m());
            }
            this.g = i;
        }
        return i;
    }

    public static g h() {
        return g.j();
    }

    /* renamed from: i */
    public g newBuilderForType() {
        return h();
    }

    public static g a(f fVar) {
        return h().mergeFrom(fVar);
    }

    /* renamed from: j */
    public g toBuilder() {
        return a(this);
    }

    static {
        f1452a.n();
    }
}
