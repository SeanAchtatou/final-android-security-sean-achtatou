package com.avast.c.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: Billing */
public final class ad extends GeneratedMessageLite implements af {

    /* renamed from: a  reason: collision with root package name */
    private static final ad f1413a = new ad(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1414b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1415c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public Object e;
    private byte f;
    private int g;

    private ad(ae aeVar) {
        super(aeVar);
        this.f = -1;
        this.g = -1;
    }

    private ad(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static ad a() {
        return f1413a;
    }

    public boolean b() {
        return (this.f1414b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1415c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1415c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString k() {
        Object obj = this.f1415c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1415c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1414b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString l() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1414b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString m() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    private void n() {
        this.f1415c = "";
        this.d = "";
        this.e = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.f;
        if (b2 == -1) {
            this.f = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1414b & 1) == 1) {
            codedOutputStream.writeBytes(1, k());
        }
        if ((this.f1414b & 2) == 2) {
            codedOutputStream.writeBytes(2, l());
        }
        if ((this.f1414b & 4) == 4) {
            codedOutputStream.writeBytes(3, m());
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            if ((this.f1414b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, k());
            }
            if ((this.f1414b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, l());
            }
            if ((this.f1414b & 4) == 4) {
                i += CodedOutputStream.computeBytesSize(3, m());
            }
            this.g = i;
        }
        return i;
    }

    public static ad a(byte[] bArr) {
        return ((ae) h().mergeFrom(bArr)).h();
    }

    public static ae h() {
        return ae.g();
    }

    /* renamed from: i */
    public ae newBuilderForType() {
        return h();
    }

    public static ae a(ad adVar) {
        return h().mergeFrom(adVar);
    }

    /* renamed from: j */
    public ae toBuilder() {
        return a(this);
    }

    static {
        f1413a.n();
    }
}
