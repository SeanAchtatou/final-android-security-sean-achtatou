package com.avast.c.a.a;

import com.google.protobuf.Internal;

/* compiled from: Billing */
public enum al implements Internal.EnumLite {
    MANUFACTURER_STORE(0, 1),
    WEB_REDIRECT(1, 2);
    

    /* renamed from: c  reason: collision with root package name */
    private static Internal.EnumLiteMap<al> f1430c = new am();
    private final int d;

    public final int getNumber() {
        return this.d;
    }

    public static al a(int i) {
        switch (i) {
            case 1:
                return MANUFACTURER_STORE;
            case 2:
                return WEB_REDIRECT;
            default:
                return null;
        }
    }

    private al(int i, int i2) {
        this.d = i2;
    }
}
