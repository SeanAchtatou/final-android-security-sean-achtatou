package com.avast.c.a.a;

import com.google.protobuf.Internal;

/* compiled from: Billing */
public enum v implements Internal.EnumLite {
    VOUCHER_CODE_UNKNOWN(0, 1),
    VOUCHER_CODE_ALREADY_CONSUMED(1, 2),
    VOUCHER_CODE_NOT_YET_VALID(2, 3),
    VOUCHER_CODE_NOT_VALID_ANYMORE(3, 4),
    VOUCHER_CODE_LOCKED(4, 5),
    VOUCHER_CODE_LICENSE_NOT_PUBLISHED(5, 6),
    VOUCHER_CODE_INVALID_OPERATOR(6, 7),
    VOUCHER_CODE_INVALID_COUNTRY(7, 8),
    GENERIC_ERROR(8, 9);
    
    private static Internal.EnumLiteMap<v> j = new w();
    private final int k;

    public final int getNumber() {
        return this.k;
    }

    public static v a(int i) {
        switch (i) {
            case 1:
                return VOUCHER_CODE_UNKNOWN;
            case 2:
                return VOUCHER_CODE_ALREADY_CONSUMED;
            case 3:
                return VOUCHER_CODE_NOT_YET_VALID;
            case 4:
                return VOUCHER_CODE_NOT_VALID_ANYMORE;
            case 5:
                return VOUCHER_CODE_LOCKED;
            case 6:
                return VOUCHER_CODE_LICENSE_NOT_PUBLISHED;
            case 7:
                return VOUCHER_CODE_INVALID_OPERATOR;
            case 8:
                return VOUCHER_CODE_INVALID_COUNTRY;
            case 9:
                return GENERIC_ERROR;
            default:
                return null;
        }
    }

    private v(int i, int i2) {
        this.k = i2;
    }
}
