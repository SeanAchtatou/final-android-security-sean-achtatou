package com.avast.c.a.a;

import com.google.protobuf.Internal;

/* compiled from: Billing */
public enum ag implements Internal.EnumLite {
    GOOGLE_PLAY(0, 1),
    DIMOCO(1, 2);
    

    /* renamed from: c  reason: collision with root package name */
    private static Internal.EnumLiteMap<ag> f1421c = new ah();
    private final int d;

    public final int getNumber() {
        return this.d;
    }

    public static ag a(int i) {
        switch (i) {
            case 1:
                return GOOGLE_PLAY;
            case 2:
                return DIMOCO;
            default:
                return null;
        }
    }

    private ag(int i, int i2) {
        this.d = i2;
    }
}
