package com.avast.c.a.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: Billing */
public final class g extends GeneratedMessageLite.Builder<f, g> implements h {

    /* renamed from: a  reason: collision with root package name */
    private int f1455a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1456b = "";

    /* renamed from: c  reason: collision with root package name */
    private Object f1457c = "";
    private Object d = "";

    private g() {
        i();
    }

    private void i() {
    }

    /* access modifiers changed from: private */
    public static g j() {
        return new g();
    }

    /* renamed from: a */
    public g clone() {
        return j().mergeFrom(d());
    }

    /* renamed from: b */
    public f getDefaultInstanceForType() {
        return f.a();
    }

    /* renamed from: c */
    public f build() {
        f d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public f d() {
        int i = 1;
        f fVar = new f(this);
        int i2 = this.f1455a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        Object unused = fVar.f1454c = this.f1456b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        Object unused2 = fVar.d = this.f1457c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        Object unused3 = fVar.e = this.d;
        int unused4 = fVar.f1453b = i;
        return fVar;
    }

    /* renamed from: a */
    public g mergeFrom(f fVar) {
        if (fVar != f.a()) {
            if (fVar.b()) {
                a(fVar.c());
            }
            if (fVar.d()) {
                b(fVar.e());
            }
            if (fVar.f()) {
                c(fVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (e() && f() && g()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public g mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1455a |= 1;
                    this.f1456b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1455a |= 2;
                    this.f1457c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1455a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1455a & 1) == 1;
    }

    public g a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1455a |= 1;
        this.f1456b = str;
        return this;
    }

    public boolean f() {
        return (this.f1455a & 2) == 2;
    }

    public g b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1455a |= 2;
        this.f1457c = str;
        return this;
    }

    public boolean g() {
        return (this.f1455a & 4) == 4;
    }

    public g c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1455a |= 4;
        this.d = str;
        return this;
    }
}
