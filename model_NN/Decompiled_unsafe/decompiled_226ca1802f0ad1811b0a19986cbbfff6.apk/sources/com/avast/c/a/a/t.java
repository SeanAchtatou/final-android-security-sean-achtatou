package com.avast.c.a.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import java.util.Collections;
import java.util.List;

/* compiled from: Billing */
public final class t extends GeneratedMessageLite implements x {

    /* renamed from: a  reason: collision with root package name */
    private static final t f1479a = new t(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1480b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public List<q> f1481c;
    /* access modifiers changed from: private */
    public v d;
    /* access modifiers changed from: private */
    public Object e;
    private byte f;
    private int g;

    private t(u uVar) {
        super(uVar);
        this.f = -1;
        this.g = -1;
    }

    private t(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static t a() {
        return f1479a;
    }

    public List<q> b() {
        return this.f1481c;
    }

    public int c() {
        return this.f1481c.size();
    }

    public q a(int i) {
        return this.f1481c.get(i);
    }

    public boolean d() {
        return (this.f1480b & 1) == 1;
    }

    public v e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1480b & 2) == 2;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString k() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    private void l() {
        this.f1481c = Collections.emptyList();
        this.d = v.VOUCHER_CODE_UNKNOWN;
        this.e = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.f;
        if (b2 == -1) {
            for (int i = 0; i < c(); i++) {
                if (!a(i).isInitialized()) {
                    this.f = 0;
                    return false;
                }
            }
            this.f = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f1481c.size()) {
                break;
            }
            codedOutputStream.writeMessage(1, this.f1481c.get(i2));
            i = i2 + 1;
        }
        if ((this.f1480b & 1) == 1) {
            codedOutputStream.writeEnum(2, this.d.getNumber());
        }
        if ((this.f1480b & 2) == 2) {
            codedOutputStream.writeBytes(3, k());
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            for (int i2 = 0; i2 < this.f1481c.size(); i2++) {
                i += CodedOutputStream.computeMessageSize(1, this.f1481c.get(i2));
            }
            if ((this.f1480b & 1) == 1) {
                i += CodedOutputStream.computeEnumSize(2, this.d.getNumber());
            }
            if ((this.f1480b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(3, k());
            }
            this.g = i;
        }
        return i;
    }

    public static t a(byte[] bArr) {
        return ((u) h().mergeFrom(bArr)).i();
    }

    public static u h() {
        return u.h();
    }

    /* renamed from: i */
    public u newBuilderForType() {
        return h();
    }

    public static u a(t tVar) {
        return h().mergeFrom(tVar);
    }

    /* renamed from: j */
    public u toBuilder() {
        return a(this);
    }

    static {
        f1479a.l();
    }
}
