package com.avast.c.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.actionbarsherlock.view.Menu;
import com.avast.c.b.a.a.c;
import com.avast.c.b.a.a.d;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: Billing */
public final class l extends GeneratedMessageLite.Builder<k, l> implements p {

    /* renamed from: a  reason: collision with root package name */
    private int f1464a;

    /* renamed from: b  reason: collision with root package name */
    private List<f> f1465b = Collections.emptyList();

    /* renamed from: c  reason: collision with root package name */
    private Object f1466c = "";
    private Object d = "";
    private Object e = "";
    private an f = an.SUITE;
    private Object g = "";
    private List<m> h = Collections.emptyList();
    private boolean i;
    private long j;
    private k k = k.a();
    private int l;
    private Object m = "";
    private int n;
    private Object o = "";
    private int p;
    private c q = c.a();
    private Object r = "";
    private boolean s;
    private Object t = "";

    private l() {
        l();
    }

    private void l() {
    }

    /* access modifiers changed from: private */
    public static l m() {
        return new l();
    }

    /* renamed from: a */
    public l clone() {
        return m().mergeFrom(d());
    }

    /* renamed from: b */
    public k getDefaultInstanceForType() {
        return k.a();
    }

    /* renamed from: c */
    public k build() {
        k d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.c.a.a.k.a(com.avast.c.a.a.k, java.util.List):java.util.List
     arg types: [com.avast.c.a.a.k, java.util.List<com.avast.c.a.a.f>]
     candidates:
      com.avast.c.a.a.k.a(com.avast.c.a.a.k, int):int
      com.avast.c.a.a.k.a(com.avast.c.a.a.k, long):long
      com.avast.c.a.a.k.a(com.avast.c.a.a.k, com.avast.c.a.a.an):com.avast.c.a.a.an
      com.avast.c.a.a.k.a(com.avast.c.a.a.k, com.avast.c.a.a.k):com.avast.c.a.a.k
      com.avast.c.a.a.k.a(com.avast.c.a.a.k, com.avast.c.b.a.a.c):com.avast.c.b.a.a.c
      com.avast.c.a.a.k.a(com.avast.c.a.a.k, java.lang.Object):java.lang.Object
      com.avast.c.a.a.k.a(com.avast.c.a.a.k, boolean):boolean
      com.avast.c.a.a.k.a(com.avast.c.a.a.k, java.util.List):java.util.List */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.c.a.a.k.b(com.avast.c.a.a.k, java.util.List):java.util.List
     arg types: [com.avast.c.a.a.k, java.util.List<com.avast.c.a.a.m>]
     candidates:
      com.avast.c.a.a.k.b(com.avast.c.a.a.k, int):int
      com.avast.c.a.a.k.b(com.avast.c.a.a.k, java.lang.Object):java.lang.Object
      com.avast.c.a.a.k.b(com.avast.c.a.a.k, boolean):boolean
      com.avast.c.a.a.k.b(com.avast.c.a.a.k, java.util.List):java.util.List */
    public k d() {
        int i2 = 1;
        k kVar = new k(this);
        int i3 = this.f1464a;
        if ((this.f1464a & 1) == 1) {
            this.f1465b = Collections.unmodifiableList(this.f1465b);
            this.f1464a &= -2;
        }
        List unused = kVar.f1463c = (List) this.f1465b;
        if ((i3 & 2) != 2) {
            i2 = 0;
        }
        Object unused2 = kVar.d = this.f1466c;
        if ((i3 & 4) == 4) {
            i2 |= 2;
        }
        Object unused3 = kVar.e = this.d;
        if ((i3 & 8) == 8) {
            i2 |= 4;
        }
        Object unused4 = kVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 8;
        }
        an unused5 = kVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 16;
        }
        Object unused6 = kVar.h = this.g;
        if ((this.f1464a & 64) == 64) {
            this.h = Collections.unmodifiableList(this.h);
            this.f1464a &= -65;
        }
        List unused7 = kVar.i = (List) this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= 32;
        }
        boolean unused8 = kVar.j = this.i;
        if ((i3 & 256) == 256) {
            i2 |= 64;
        }
        long unused9 = kVar.k = this.j;
        if ((i3 & 512) == 512) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        k unused10 = kVar.l = this.k;
        if ((i3 & 1024) == 1024) {
            i2 |= 256;
        }
        int unused11 = kVar.m = this.l;
        if ((i3 & 2048) == 2048) {
            i2 |= 512;
        }
        Object unused12 = kVar.n = this.m;
        if ((i3 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i2 |= 1024;
        }
        int unused13 = kVar.o = this.n;
        if ((i3 & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            i2 |= 2048;
        }
        Object unused14 = kVar.p = this.o;
        if ((i3 & 16384) == 16384) {
            i2 |= FragmentTransaction.TRANSIT_ENTER_MASK;
        }
        int unused15 = kVar.q = this.p;
        if ((i3 & 32768) == 32768) {
            i2 |= FragmentTransaction.TRANSIT_EXIT_MASK;
        }
        c unused16 = kVar.r = this.q;
        if ((i3 & Menu.CATEGORY_CONTAINER) == 65536) {
            i2 |= 16384;
        }
        Object unused17 = kVar.s = this.r;
        if ((i3 & Menu.CATEGORY_SYSTEM) == 131072) {
            i2 |= 32768;
        }
        boolean unused18 = kVar.t = this.s;
        if ((i3 & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            i2 |= Menu.CATEGORY_CONTAINER;
        }
        Object unused19 = kVar.u = this.t;
        int unused20 = kVar.f1462b = i2;
        return kVar;
    }

    /* renamed from: a */
    public l mergeFrom(k kVar) {
        if (kVar != k.a()) {
            if (!kVar.f1463c.isEmpty()) {
                if (this.f1465b.isEmpty()) {
                    this.f1465b = kVar.f1463c;
                    this.f1464a &= -2;
                } else {
                    n();
                    this.f1465b.addAll(kVar.f1463c);
                }
            }
            if (kVar.c()) {
                a(kVar.d());
            }
            if (kVar.e()) {
                b(kVar.f());
            }
            if (kVar.g()) {
                c(kVar.h());
            }
            if (kVar.i()) {
                a(kVar.j());
            }
            if (kVar.k()) {
                d(kVar.l());
            }
            if (!kVar.i.isEmpty()) {
                if (this.h.isEmpty()) {
                    this.h = kVar.i;
                    this.f1464a &= -65;
                } else {
                    o();
                    this.h.addAll(kVar.i);
                }
            }
            if (kVar.m()) {
                a(kVar.n());
            }
            if (kVar.o()) {
                a(kVar.p());
            }
            if (kVar.q()) {
                c(kVar.r());
            }
            if (kVar.s()) {
                b(kVar.t());
            }
            if (kVar.u()) {
                e(kVar.v());
            }
            if (kVar.w()) {
                c(kVar.x());
            }
            if (kVar.y()) {
                f(kVar.z());
            }
            if (kVar.A()) {
                d(kVar.B());
            }
            if (kVar.C()) {
                b(kVar.D());
            }
            if (kVar.E()) {
                g(kVar.F());
            }
            if (kVar.G()) {
                b(kVar.H());
            }
            if (kVar.I()) {
                h(kVar.J());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (!f()) {
            return false;
        }
        for (int i2 = 0; i2 < e(); i2++) {
            if (!a(i2).isInitialized()) {
                return false;
            }
        }
        if (!g() || h().isInitialized()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public l mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    g h2 = f.h();
                    codedInputStream.readMessage(h2, extensionRegistryLite);
                    a(h2.d());
                    break;
                case 18:
                    this.f1464a |= 2;
                    this.f1466c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1464a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f1464a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    an a2 = an.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1464a |= 16;
                        this.f = a2;
                        break;
                    }
                case 50:
                    this.f1464a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    n f2 = m.f();
                    codedInputStream.readMessage(f2, extensionRegistryLite);
                    a(f2.d());
                    break;
                case R.styleable.SherlockTheme_activityChooserViewStyle:
                    this.f1464a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBool();
                    break;
                case 72:
                    this.f1464a |= 256;
                    this.j = codedInputStream.readInt64();
                    break;
                case 82:
                    l K = k.K();
                    if (g()) {
                        K.mergeFrom(h());
                    }
                    codedInputStream.readMessage(K, extensionRegistryLite);
                    b(K.d());
                    break;
                case 88:
                    this.f1464a |= 1024;
                    this.l = codedInputStream.readInt32();
                    break;
                case 98:
                    this.f1464a |= 2048;
                    this.m = codedInputStream.readBytes();
                    break;
                case 104:
                    this.f1464a |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.n = codedInputStream.readInt32();
                    break;
                case 114:
                    this.f1464a |= FragmentTransaction.TRANSIT_EXIT_MASK;
                    this.o = codedInputStream.readBytes();
                    break;
                case 120:
                    this.f1464a |= 16384;
                    this.p = codedInputStream.readInt32();
                    break;
                case 130:
                    d h3 = c.h();
                    if (i()) {
                        h3.mergeFrom(j());
                    }
                    codedInputStream.readMessage(h3, extensionRegistryLite);
                    a(h3.d());
                    break;
                case 138:
                    this.f1464a |= Menu.CATEGORY_CONTAINER;
                    this.r = codedInputStream.readBytes();
                    break;
                case 144:
                    this.f1464a |= Menu.CATEGORY_SYSTEM;
                    this.s = codedInputStream.readBool();
                    break;
                case 154:
                    this.f1464a |= Menu.CATEGORY_ALTERNATIVE;
                    this.t = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    private void n() {
        if ((this.f1464a & 1) != 1) {
            this.f1465b = new ArrayList(this.f1465b);
            this.f1464a |= 1;
        }
    }

    public int e() {
        return this.f1465b.size();
    }

    public f a(int i2) {
        return this.f1465b.get(i2);
    }

    public l a(f fVar) {
        if (fVar == null) {
            throw new NullPointerException();
        }
        n();
        this.f1465b.add(fVar);
        return this;
    }

    public boolean f() {
        return (this.f1464a & 2) == 2;
    }

    public l a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1464a |= 2;
        this.f1466c = str;
        return this;
    }

    public l b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1464a |= 4;
        this.d = str;
        return this;
    }

    public l c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1464a |= 8;
        this.e = str;
        return this;
    }

    public l a(an anVar) {
        if (anVar == null) {
            throw new NullPointerException();
        }
        this.f1464a |= 16;
        this.f = anVar;
        return this;
    }

    public l d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1464a |= 32;
        this.g = str;
        return this;
    }

    private void o() {
        if ((this.f1464a & 64) != 64) {
            this.h = new ArrayList(this.h);
            this.f1464a |= 64;
        }
    }

    public l a(m mVar) {
        if (mVar == null) {
            throw new NullPointerException();
        }
        o();
        this.h.add(mVar);
        return this;
    }

    public l a(boolean z) {
        this.f1464a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = z;
        return this;
    }

    public l a(long j2) {
        this.f1464a |= 256;
        this.j = j2;
        return this;
    }

    public boolean g() {
        return (this.f1464a & 512) == 512;
    }

    public k h() {
        return this.k;
    }

    public l b(k kVar) {
        if (kVar == null) {
            throw new NullPointerException();
        }
        this.k = kVar;
        this.f1464a |= 512;
        return this;
    }

    public l c(k kVar) {
        if ((this.f1464a & 512) != 512 || this.k == k.a()) {
            this.k = kVar;
        } else {
            this.k = k.a(this.k).mergeFrom(kVar).d();
        }
        this.f1464a |= 512;
        return this;
    }

    public l b(int i2) {
        this.f1464a |= 1024;
        this.l = i2;
        return this;
    }

    public l e(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1464a |= 2048;
        this.m = str;
        return this;
    }

    public l c(int i2) {
        this.f1464a |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.n = i2;
        return this;
    }

    public l f(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1464a |= FragmentTransaction.TRANSIT_EXIT_MASK;
        this.o = str;
        return this;
    }

    public l d(int i2) {
        this.f1464a |= 16384;
        this.p = i2;
        return this;
    }

    public boolean i() {
        return (this.f1464a & 32768) == 32768;
    }

    public c j() {
        return this.q;
    }

    public l a(c cVar) {
        if (cVar == null) {
            throw new NullPointerException();
        }
        this.q = cVar;
        this.f1464a |= 32768;
        return this;
    }

    public l b(c cVar) {
        if ((this.f1464a & 32768) != 32768 || this.q == c.a()) {
            this.q = cVar;
        } else {
            this.q = c.a(this.q).mergeFrom(cVar).d();
        }
        this.f1464a |= 32768;
        return this;
    }

    public l g(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1464a |= Menu.CATEGORY_CONTAINER;
        this.r = str;
        return this;
    }

    public l b(boolean z) {
        this.f1464a |= Menu.CATEGORY_SYSTEM;
        this.s = z;
        return this;
    }

    public l h(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1464a |= Menu.CATEGORY_ALTERNATIVE;
        this.t = str;
        return this;
    }
}
