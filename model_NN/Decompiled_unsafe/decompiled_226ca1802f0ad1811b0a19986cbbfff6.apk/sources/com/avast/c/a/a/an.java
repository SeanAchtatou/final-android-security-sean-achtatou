package com.avast.c.a.a;

import com.google.protobuf.Internal;

/* compiled from: Billing */
public enum an implements Internal.EnumLite {
    SUITE(0, 1),
    VPN(1, 2);
    

    /* renamed from: c  reason: collision with root package name */
    private static Internal.EnumLiteMap<an> f1433c = new ao();
    private final int d;

    public final int getNumber() {
        return this.d;
    }

    public static an a(int i) {
        switch (i) {
            case 1:
                return SUITE;
            case 2:
                return VPN;
            default:
                return null;
        }
    }

    private an(int i, int i2) {
        this.d = i2;
    }
}
