package com.avast.c.a.a;

import android.support.v4.app.NotificationCompat;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: Billing */
public final class aa extends GeneratedMessageLite implements ac {

    /* renamed from: a  reason: collision with root package name */
    private static final aa f1407a = new aa(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1408b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f1409c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public Object e;
    /* access modifiers changed from: private */
    public an f;
    /* access modifiers changed from: private */
    public Object g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public Object i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public Object k;
    /* access modifiers changed from: private */
    public Object l;
    /* access modifiers changed from: private */
    public Object m;
    /* access modifiers changed from: private */
    public Object n;
    private byte o;
    private int p;

    private aa(ab abVar) {
        super(abVar);
        this.o = -1;
        this.p = -1;
    }

    private aa(boolean z) {
        this.o = -1;
        this.p = -1;
    }

    public static aa a() {
        return f1407a;
    }

    public boolean b() {
        return (this.f1408b & 1) == 1;
    }

    public String c() {
        Object obj = this.f1409c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f1409c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString C() {
        Object obj = this.f1409c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f1409c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f1408b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString D() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f1408b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString E() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean h() {
        return (this.f1408b & 8) == 8;
    }

    public an i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1408b & 16) == 16;
    }

    public String k() {
        Object obj = this.g;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.g = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString F() {
        Object obj = this.g;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.g = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean l() {
        return (this.f1408b & 32) == 32;
    }

    public int m() {
        return this.h;
    }

    public boolean n() {
        return (this.f1408b & 64) == 64;
    }

    public String o() {
        Object obj = this.i;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.i = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString G() {
        Object obj = this.i;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.i = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean p() {
        return (this.f1408b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public int q() {
        return this.j;
    }

    public boolean r() {
        return (this.f1408b & 256) == 256;
    }

    public String s() {
        Object obj = this.k;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.k = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString H() {
        Object obj = this.k;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.k = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean t() {
        return (this.f1408b & 512) == 512;
    }

    public String u() {
        Object obj = this.l;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.l = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString I() {
        Object obj = this.l;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.l = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean v() {
        return (this.f1408b & 1024) == 1024;
    }

    public String w() {
        Object obj = this.m;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.m = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString J() {
        Object obj = this.m;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.m = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean x() {
        return (this.f1408b & 2048) == 2048;
    }

    public String y() {
        Object obj = this.n;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.n = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString K() {
        Object obj = this.n;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.n = copyFromUtf8;
        return copyFromUtf8;
    }

    private void L() {
        this.f1409c = "";
        this.d = "";
        this.e = "";
        this.f = an.SUITE;
        this.g = "";
        this.h = 0;
        this.i = "";
        this.j = 0;
        this.k = "";
        this.l = "";
        this.m = "";
        this.n = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.o;
        if (b2 == -1) {
            this.o = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1408b & 1) == 1) {
            codedOutputStream.writeBytes(1, C());
        }
        if ((this.f1408b & 2) == 2) {
            codedOutputStream.writeBytes(2, D());
        }
        if ((this.f1408b & 4) == 4) {
            codedOutputStream.writeBytes(3, E());
        }
        if ((this.f1408b & 8) == 8) {
            codedOutputStream.writeEnum(4, this.f.getNumber());
        }
        if ((this.f1408b & 16) == 16) {
            codedOutputStream.writeBytes(5, F());
        }
        if ((this.f1408b & 32) == 32) {
            codedOutputStream.writeInt32(6, this.h);
        }
        if ((this.f1408b & 64) == 64) {
            codedOutputStream.writeBytes(7, G());
        }
        if ((this.f1408b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeInt32(8, this.j);
        }
        if ((this.f1408b & 256) == 256) {
            codedOutputStream.writeBytes(9, H());
        }
        if ((this.f1408b & 512) == 512) {
            codedOutputStream.writeBytes(10, I());
        }
        if ((this.f1408b & 1024) == 1024) {
            codedOutputStream.writeBytes(11, J());
        }
        if ((this.f1408b & 2048) == 2048) {
            codedOutputStream.writeBytes(12, K());
        }
    }

    public int getSerializedSize() {
        int i2 = this.p;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1408b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, C());
            }
            if ((this.f1408b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, D());
            }
            if ((this.f1408b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, E());
            }
            if ((this.f1408b & 8) == 8) {
                i2 += CodedOutputStream.computeEnumSize(4, this.f.getNumber());
            }
            if ((this.f1408b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(5, F());
            }
            if ((this.f1408b & 32) == 32) {
                i2 += CodedOutputStream.computeInt32Size(6, this.h);
            }
            if ((this.f1408b & 64) == 64) {
                i2 += CodedOutputStream.computeBytesSize(7, G());
            }
            if ((this.f1408b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeInt32Size(8, this.j);
            }
            if ((this.f1408b & 256) == 256) {
                i2 += CodedOutputStream.computeBytesSize(9, H());
            }
            if ((this.f1408b & 512) == 512) {
                i2 += CodedOutputStream.computeBytesSize(10, I());
            }
            if ((this.f1408b & 1024) == 1024) {
                i2 += CodedOutputStream.computeBytesSize(11, J());
            }
            if ((this.f1408b & 2048) == 2048) {
                i2 += CodedOutputStream.computeBytesSize(12, K());
            }
            this.p = i2;
        }
        return i2;
    }

    public static ab z() {
        return ab.g();
    }

    /* renamed from: A */
    public ab newBuilderForType() {
        return z();
    }

    public static ab a(aa aaVar) {
        return z().mergeFrom(aaVar);
    }

    /* renamed from: B */
    public ab toBuilder() {
        return a(this);
    }

    static {
        f1407a.L();
    }
}
