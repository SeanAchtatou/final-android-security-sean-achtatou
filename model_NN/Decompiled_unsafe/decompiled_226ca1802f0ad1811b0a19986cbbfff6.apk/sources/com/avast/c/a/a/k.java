package com.avast.c.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.view.Menu;
import com.avast.c.b.a.a.c;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import java.util.Collections;
import java.util.List;

/* compiled from: Billing */
public final class k extends GeneratedMessageLite implements p {

    /* renamed from: a  reason: collision with root package name */
    private static final k f1461a = new k(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1462b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public List<f> f1463c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public Object e;
    /* access modifiers changed from: private */
    public Object f;
    /* access modifiers changed from: private */
    public an g;
    /* access modifiers changed from: private */
    public Object h;
    /* access modifiers changed from: private */
    public List<m> i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public long k;
    /* access modifiers changed from: private */
    public k l;
    /* access modifiers changed from: private */
    public int m;
    /* access modifiers changed from: private */
    public Object n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public Object p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public c r;
    /* access modifiers changed from: private */
    public Object s;
    /* access modifiers changed from: private */
    public boolean t;
    /* access modifiers changed from: private */
    public Object u;
    private byte v;
    private int w;

    private k(l lVar) {
        super(lVar);
        this.v = -1;
        this.w = -1;
    }

    private k(boolean z) {
        this.v = -1;
        this.w = -1;
    }

    public static k a() {
        return f1461a;
    }

    public int b() {
        return this.f1463c.size();
    }

    public f a(int i2) {
        return this.f1463c.get(i2);
    }

    public boolean c() {
        return (this.f1462b & 1) == 1;
    }

    public String d() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString N() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean e() {
        return (this.f1462b & 2) == 2;
    }

    public String f() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString O() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean g() {
        return (this.f1462b & 4) == 4;
    }

    public String h() {
        Object obj = this.f;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString P() {
        Object obj = this.f;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean i() {
        return (this.f1462b & 8) == 8;
    }

    public an j() {
        return this.g;
    }

    public boolean k() {
        return (this.f1462b & 16) == 16;
    }

    public String l() {
        Object obj = this.h;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.h = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString Q() {
        Object obj = this.h;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.h = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean m() {
        return (this.f1462b & 32) == 32;
    }

    public boolean n() {
        return this.j;
    }

    public boolean o() {
        return (this.f1462b & 64) == 64;
    }

    public long p() {
        return this.k;
    }

    public boolean q() {
        return (this.f1462b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public k r() {
        return this.l;
    }

    public boolean s() {
        return (this.f1462b & 256) == 256;
    }

    public int t() {
        return this.m;
    }

    public boolean u() {
        return (this.f1462b & 512) == 512;
    }

    public String v() {
        Object obj = this.n;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.n = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString R() {
        Object obj = this.n;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.n = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean w() {
        return (this.f1462b & 1024) == 1024;
    }

    public int x() {
        return this.o;
    }

    public boolean y() {
        return (this.f1462b & 2048) == 2048;
    }

    public String z() {
        Object obj = this.p;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.p = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString S() {
        Object obj = this.p;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.p = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean A() {
        return (this.f1462b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096;
    }

    public int B() {
        return this.q;
    }

    public boolean C() {
        return (this.f1462b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192;
    }

    public c D() {
        return this.r;
    }

    public boolean E() {
        return (this.f1462b & 16384) == 16384;
    }

    public String F() {
        Object obj = this.s;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.s = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString T() {
        Object obj = this.s;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.s = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean G() {
        return (this.f1462b & 32768) == 32768;
    }

    public boolean H() {
        return this.t;
    }

    public boolean I() {
        return (this.f1462b & Menu.CATEGORY_CONTAINER) == 65536;
    }

    public String J() {
        Object obj = this.u;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.u = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString U() {
        Object obj = this.u;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.u = copyFromUtf8;
        return copyFromUtf8;
    }

    private void V() {
        this.f1463c = Collections.emptyList();
        this.d = "";
        this.e = "";
        this.f = "";
        this.g = an.SUITE;
        this.h = "";
        this.i = Collections.emptyList();
        this.j = false;
        this.k = 0;
        this.l = a();
        this.m = 0;
        this.n = "";
        this.o = 0;
        this.p = "";
        this.q = 0;
        this.r = c.a();
        this.s = "";
        this.t = false;
        this.u = "";
    }

    public final boolean isInitialized() {
        boolean z = true;
        byte b2 = this.v;
        if (b2 != -1) {
            if (b2 != 1) {
                z = false;
            }
            return z;
        } else if (!c()) {
            this.v = 0;
            return false;
        } else {
            for (int i2 = 0; i2 < b(); i2++) {
                if (!a(i2).isInitialized()) {
                    this.v = 0;
                    return false;
                }
            }
            if (!q() || r().isInitialized()) {
                this.v = 1;
                return true;
            }
            this.v = 0;
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        for (int i2 = 0; i2 < this.f1463c.size(); i2++) {
            codedOutputStream.writeMessage(1, this.f1463c.get(i2));
        }
        if ((this.f1462b & 1) == 1) {
            codedOutputStream.writeBytes(2, N());
        }
        if ((this.f1462b & 2) == 2) {
            codedOutputStream.writeBytes(3, O());
        }
        if ((this.f1462b & 4) == 4) {
            codedOutputStream.writeBytes(4, P());
        }
        if ((this.f1462b & 8) == 8) {
            codedOutputStream.writeEnum(5, this.g.getNumber());
        }
        if ((this.f1462b & 16) == 16) {
            codedOutputStream.writeBytes(6, Q());
        }
        for (int i3 = 0; i3 < this.i.size(); i3++) {
            codedOutputStream.writeMessage(7, this.i.get(i3));
        }
        if ((this.f1462b & 32) == 32) {
            codedOutputStream.writeBool(8, this.j);
        }
        if ((this.f1462b & 64) == 64) {
            codedOutputStream.writeInt64(9, this.k);
        }
        if ((this.f1462b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeMessage(10, this.l);
        }
        if ((this.f1462b & 256) == 256) {
            codedOutputStream.writeInt32(11, this.m);
        }
        if ((this.f1462b & 512) == 512) {
            codedOutputStream.writeBytes(12, R());
        }
        if ((this.f1462b & 1024) == 1024) {
            codedOutputStream.writeInt32(13, this.o);
        }
        if ((this.f1462b & 2048) == 2048) {
            codedOutputStream.writeBytes(14, S());
        }
        if ((this.f1462b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            codedOutputStream.writeInt32(15, this.q);
        }
        if ((this.f1462b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            codedOutputStream.writeMessage(16, this.r);
        }
        if ((this.f1462b & 16384) == 16384) {
            codedOutputStream.writeBytes(17, T());
        }
        if ((this.f1462b & 32768) == 32768) {
            codedOutputStream.writeBool(18, this.t);
        }
        if ((this.f1462b & Menu.CATEGORY_CONTAINER) == 65536) {
            codedOutputStream.writeBytes(19, U());
        }
    }

    public int getSerializedSize() {
        int i2 = this.w;
        if (i2 == -1) {
            i2 = 0;
            for (int i3 = 0; i3 < this.f1463c.size(); i3++) {
                i2 += CodedOutputStream.computeMessageSize(1, this.f1463c.get(i3));
            }
            if ((this.f1462b & 1) == 1) {
                i2 += CodedOutputStream.computeBytesSize(2, N());
            }
            if ((this.f1462b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(3, O());
            }
            if ((this.f1462b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(4, P());
            }
            if ((this.f1462b & 8) == 8) {
                i2 += CodedOutputStream.computeEnumSize(5, this.g.getNumber());
            }
            if ((this.f1462b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(6, Q());
            }
            for (int i4 = 0; i4 < this.i.size(); i4++) {
                i2 += CodedOutputStream.computeMessageSize(7, this.i.get(i4));
            }
            if ((this.f1462b & 32) == 32) {
                i2 += CodedOutputStream.computeBoolSize(8, this.j);
            }
            if ((this.f1462b & 64) == 64) {
                i2 += CodedOutputStream.computeInt64Size(9, this.k);
            }
            if ((this.f1462b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeMessageSize(10, this.l);
            }
            if ((this.f1462b & 256) == 256) {
                i2 += CodedOutputStream.computeInt32Size(11, this.m);
            }
            if ((this.f1462b & 512) == 512) {
                i2 += CodedOutputStream.computeBytesSize(12, R());
            }
            if ((this.f1462b & 1024) == 1024) {
                i2 += CodedOutputStream.computeInt32Size(13, this.o);
            }
            if ((this.f1462b & 2048) == 2048) {
                i2 += CodedOutputStream.computeBytesSize(14, S());
            }
            if ((this.f1462b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
                i2 += CodedOutputStream.computeInt32Size(15, this.q);
            }
            if ((this.f1462b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
                i2 += CodedOutputStream.computeMessageSize(16, this.r);
            }
            if ((this.f1462b & 16384) == 16384) {
                i2 += CodedOutputStream.computeBytesSize(17, T());
            }
            if ((this.f1462b & 32768) == 32768) {
                i2 += CodedOutputStream.computeBoolSize(18, this.t);
            }
            if ((this.f1462b & Menu.CATEGORY_CONTAINER) == 65536) {
                i2 += CodedOutputStream.computeBytesSize(19, U());
            }
            this.w = i2;
        }
        return i2;
    }

    public static l K() {
        return l.m();
    }

    /* renamed from: L */
    public l newBuilderForType() {
        return K();
    }

    public static l a(k kVar) {
        return K().mergeFrom(kVar);
    }

    /* renamed from: M */
    public l toBuilder() {
        return a(this);
    }

    static {
        f1461a.V();
    }
}
