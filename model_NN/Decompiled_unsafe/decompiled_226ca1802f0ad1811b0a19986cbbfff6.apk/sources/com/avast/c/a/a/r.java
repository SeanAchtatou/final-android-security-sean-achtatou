package com.avast.c.a.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.actionbarsherlock.view.Menu;
import com.avast.c.b.a.a.c;
import com.avast.c.b.a.a.d;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: Billing */
public final class r extends GeneratedMessageLite.Builder<q, r> implements s {

    /* renamed from: a  reason: collision with root package name */
    private int f1476a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1477b = "";

    /* renamed from: c  reason: collision with root package name */
    private Object f1478c = "";
    private y d = y.SUBSCRIPTION_MONTHLY;
    private float e;
    private boolean f;
    private long g;
    private long h;
    private Object i = "";
    private boolean j;
    private Object k = "";
    private Object l = "";
    private Object m = "";
    private Object n = "";
    private Object o = "";
    private int p;
    private int q;
    private int r;
    private c s = c.a();
    private int t;
    private List<ai> u = Collections.emptyList();
    private Object v = "";
    private Object w = "";

    private r() {
        i();
    }

    private void i() {
    }

    /* access modifiers changed from: private */
    public static r j() {
        return new r();
    }

    /* renamed from: a */
    public r clone() {
        return j().mergeFrom(d());
    }

    /* renamed from: b */
    public q getDefaultInstanceForType() {
        return q.a();
    }

    /* renamed from: c */
    public q build() {
        q d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.c.a.a.q.a(com.avast.c.a.a.q, java.util.List):java.util.List
     arg types: [com.avast.c.a.a.q, java.util.List<com.avast.c.a.a.ai>]
     candidates:
      com.avast.c.a.a.q.a(com.avast.c.a.a.q, float):float
      com.avast.c.a.a.q.a(com.avast.c.a.a.q, int):int
      com.avast.c.a.a.q.a(com.avast.c.a.a.q, long):long
      com.avast.c.a.a.q.a(com.avast.c.a.a.q, com.avast.c.a.a.y):com.avast.c.a.a.y
      com.avast.c.a.a.q.a(com.avast.c.a.a.q, com.avast.c.b.a.a.c):com.avast.c.b.a.a.c
      com.avast.c.a.a.q.a(com.avast.c.a.a.q, java.lang.Object):java.lang.Object
      com.avast.c.a.a.q.a(com.avast.c.a.a.q, boolean):boolean
      com.avast.c.a.a.q.a(com.avast.c.a.a.q, java.util.List):java.util.List */
    public q d() {
        q qVar = new q(this);
        int i2 = this.f1476a;
        int i3 = 0;
        if ((i2 & 1) == 1) {
            i3 = 1;
        }
        Object unused = qVar.f1475c = this.f1477b;
        if ((i2 & 2) == 2) {
            i3 |= 2;
        }
        Object unused2 = qVar.d = this.f1478c;
        if ((i2 & 4) == 4) {
            i3 |= 4;
        }
        y unused3 = qVar.e = this.d;
        if ((i2 & 8) == 8) {
            i3 |= 8;
        }
        float unused4 = qVar.f = this.e;
        if ((i2 & 16) == 16) {
            i3 |= 16;
        }
        boolean unused5 = qVar.g = this.f;
        if ((i2 & 32) == 32) {
            i3 |= 32;
        }
        long unused6 = qVar.h = this.g;
        if ((i2 & 64) == 64) {
            i3 |= 64;
        }
        long unused7 = qVar.i = this.h;
        if ((i2 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i3 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        Object unused8 = qVar.j = this.i;
        if ((i2 & 256) == 256) {
            i3 |= 256;
        }
        boolean unused9 = qVar.k = this.j;
        if ((i2 & 512) == 512) {
            i3 |= 512;
        }
        Object unused10 = qVar.l = this.k;
        if ((i2 & 1024) == 1024) {
            i3 |= 1024;
        }
        Object unused11 = qVar.m = this.l;
        if ((i2 & 2048) == 2048) {
            i3 |= 2048;
        }
        Object unused12 = qVar.n = this.m;
        if ((i2 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i3 |= FragmentTransaction.TRANSIT_ENTER_MASK;
        }
        Object unused13 = qVar.o = this.n;
        if ((i2 & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            i3 |= FragmentTransaction.TRANSIT_EXIT_MASK;
        }
        Object unused14 = qVar.p = this.o;
        if ((i2 & 16384) == 16384) {
            i3 |= 16384;
        }
        int unused15 = qVar.q = this.p;
        if ((i2 & 32768) == 32768) {
            i3 |= 32768;
        }
        int unused16 = qVar.r = this.q;
        if ((i2 & Menu.CATEGORY_CONTAINER) == 65536) {
            i3 |= Menu.CATEGORY_CONTAINER;
        }
        int unused17 = qVar.s = this.r;
        if ((i2 & Menu.CATEGORY_SYSTEM) == 131072) {
            i3 |= Menu.CATEGORY_SYSTEM;
        }
        c unused18 = qVar.t = this.s;
        if ((i2 & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            i3 |= Menu.CATEGORY_ALTERNATIVE;
        }
        int unused19 = qVar.u = this.t;
        if ((this.f1476a & 524288) == 524288) {
            this.u = Collections.unmodifiableList(this.u);
            this.f1476a &= -524289;
        }
        List unused20 = qVar.v = (List) this.u;
        if ((1048576 & i2) == 1048576) {
            i3 |= 524288;
        }
        Object unused21 = qVar.w = this.v;
        if ((i2 & 2097152) == 2097152) {
            i3 |= 1048576;
        }
        Object unused22 = qVar.x = this.w;
        int unused23 = qVar.f1474b = i3;
        return qVar;
    }

    /* renamed from: a */
    public r mergeFrom(q qVar) {
        if (qVar != q.a()) {
            if (qVar.b()) {
                a(qVar.c());
            }
            if (qVar.d()) {
                b(qVar.e());
            }
            if (qVar.f()) {
                a(qVar.g());
            }
            if (qVar.h()) {
                a(qVar.i());
            }
            if (qVar.j()) {
                a(qVar.k());
            }
            if (qVar.l()) {
                a(qVar.m());
            }
            if (qVar.n()) {
                b(qVar.o());
            }
            if (qVar.p()) {
                c(qVar.q());
            }
            if (qVar.r()) {
                b(qVar.s());
            }
            if (qVar.t()) {
                d(qVar.u());
            }
            if (qVar.v()) {
                e(qVar.w());
            }
            if (qVar.x()) {
                f(qVar.y());
            }
            if (qVar.z()) {
                g(qVar.A());
            }
            if (qVar.B()) {
                h(qVar.C());
            }
            if (qVar.D()) {
                a(qVar.E());
            }
            if (qVar.F()) {
                b(qVar.G());
            }
            if (qVar.H()) {
                c(qVar.I());
            }
            if (qVar.J()) {
                b(qVar.K());
            }
            if (qVar.L()) {
                d(qVar.M());
            }
            if (!qVar.v.isEmpty()) {
                if (this.u.isEmpty()) {
                    this.u = qVar.v;
                    this.f1476a &= -524289;
                } else {
                    k();
                    this.u.addAll(qVar.v);
                }
            }
            if (qVar.P()) {
                i(qVar.Q());
            }
            if (qVar.R()) {
                j(qVar.S());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        if (!e()) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public r mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1476a |= 1;
                    this.f1477b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1476a |= 2;
                    this.f1478c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    y a2 = y.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1476a |= 4;
                        this.d = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_searchViewEditQueryBackground:
                    this.f1476a |= 8;
                    this.e = codedInputStream.readFloat();
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f1476a |= 16;
                    this.f = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                    this.f1476a |= 32;
                    this.g = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    this.f1476a |= 64;
                    this.h = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f1476a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                case 72:
                    this.f1476a |= 256;
                    this.j = codedInputStream.readBool();
                    break;
                case 82:
                    this.f1476a |= 512;
                    this.k = codedInputStream.readBytes();
                    break;
                case 90:
                    this.f1476a |= 1024;
                    this.l = codedInputStream.readBytes();
                    break;
                case 98:
                    this.f1476a |= 2048;
                    this.m = codedInputStream.readBytes();
                    break;
                case 106:
                    this.f1476a |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.n = codedInputStream.readBytes();
                    break;
                case 114:
                    this.f1476a |= FragmentTransaction.TRANSIT_EXIT_MASK;
                    this.o = codedInputStream.readBytes();
                    break;
                case 120:
                    this.f1476a |= 16384;
                    this.p = codedInputStream.readInt32();
                    break;
                case NotificationCompat.FLAG_HIGH_PRIORITY:
                    this.f1476a |= 32768;
                    this.q = codedInputStream.readInt32();
                    break;
                case 136:
                    this.f1476a |= Menu.CATEGORY_CONTAINER;
                    this.r = codedInputStream.readInt32();
                    break;
                case 146:
                    d h2 = c.h();
                    if (f()) {
                        h2.mergeFrom(g());
                    }
                    codedInputStream.readMessage(h2, extensionRegistryLite);
                    a(h2.d());
                    break;
                case 152:
                    this.f1476a |= Menu.CATEGORY_ALTERNATIVE;
                    this.t = codedInputStream.readInt32();
                    break;
                case 162:
                    aj h3 = ai.h();
                    codedInputStream.readMessage(h3, extensionRegistryLite);
                    a(h3.d());
                    break;
                case 170:
                    this.f1476a |= 1048576;
                    this.v = codedInputStream.readBytes();
                    break;
                case 178:
                    this.f1476a |= 2097152;
                    this.w = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1476a & 1) == 1;
    }

    public r a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1476a |= 1;
        this.f1477b = str;
        return this;
    }

    public r b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1476a |= 2;
        this.f1478c = str;
        return this;
    }

    public r a(y yVar) {
        if (yVar == null) {
            throw new NullPointerException();
        }
        this.f1476a |= 4;
        this.d = yVar;
        return this;
    }

    public r a(float f2) {
        this.f1476a |= 8;
        this.e = f2;
        return this;
    }

    public r a(boolean z) {
        this.f1476a |= 16;
        this.f = z;
        return this;
    }

    public r a(long j2) {
        this.f1476a |= 32;
        this.g = j2;
        return this;
    }

    public r b(long j2) {
        this.f1476a |= 64;
        this.h = j2;
        return this;
    }

    public r c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1476a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = str;
        return this;
    }

    public r b(boolean z) {
        this.f1476a |= 256;
        this.j = z;
        return this;
    }

    public r d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1476a |= 512;
        this.k = str;
        return this;
    }

    public r e(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1476a |= 1024;
        this.l = str;
        return this;
    }

    public r f(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1476a |= 2048;
        this.m = str;
        return this;
    }

    public r g(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1476a |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.n = str;
        return this;
    }

    public r h(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1476a |= FragmentTransaction.TRANSIT_EXIT_MASK;
        this.o = str;
        return this;
    }

    public r a(int i2) {
        this.f1476a |= 16384;
        this.p = i2;
        return this;
    }

    public r b(int i2) {
        this.f1476a |= 32768;
        this.q = i2;
        return this;
    }

    public r c(int i2) {
        this.f1476a |= Menu.CATEGORY_CONTAINER;
        this.r = i2;
        return this;
    }

    public boolean f() {
        return (this.f1476a & Menu.CATEGORY_SYSTEM) == 131072;
    }

    public c g() {
        return this.s;
    }

    public r a(c cVar) {
        if (cVar == null) {
            throw new NullPointerException();
        }
        this.s = cVar;
        this.f1476a |= Menu.CATEGORY_SYSTEM;
        return this;
    }

    public r b(c cVar) {
        if ((this.f1476a & Menu.CATEGORY_SYSTEM) != 131072 || this.s == c.a()) {
            this.s = cVar;
        } else {
            this.s = c.a(this.s).mergeFrom(cVar).d();
        }
        this.f1476a |= Menu.CATEGORY_SYSTEM;
        return this;
    }

    public r d(int i2) {
        this.f1476a |= Menu.CATEGORY_ALTERNATIVE;
        this.t = i2;
        return this;
    }

    private void k() {
        if ((this.f1476a & 524288) != 524288) {
            this.u = new ArrayList(this.u);
            this.f1476a |= 524288;
        }
    }

    public r a(ai aiVar) {
        if (aiVar == null) {
            throw new NullPointerException();
        }
        k();
        this.u.add(aiVar);
        return this;
    }

    public r i(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1476a |= 1048576;
        this.v = str;
        return this;
    }

    public r j(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1476a |= 2097152;
        this.w = str;
        return this;
    }
}
