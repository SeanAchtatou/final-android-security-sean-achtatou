package com.avast.c.a.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: Billing */
public final class aj extends GeneratedMessageLite.Builder<ai, aj> implements ak {

    /* renamed from: a  reason: collision with root package name */
    private int f1425a;

    /* renamed from: b  reason: collision with root package name */
    private al f1426b = al.MANUFACTURER_STORE;

    /* renamed from: c  reason: collision with root package name */
    private int f1427c;
    private Object d = "";

    private aj() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static aj g() {
        return new aj();
    }

    /* renamed from: a */
    public aj clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public ai getDefaultInstanceForType() {
        return ai.a();
    }

    /* renamed from: c */
    public ai build() {
        ai d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public ai d() {
        int i = 1;
        ai aiVar = new ai(this);
        int i2 = this.f1425a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        al unused = aiVar.f1424c = this.f1426b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        int unused2 = aiVar.d = this.f1427c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        Object unused3 = aiVar.e = this.d;
        int unused4 = aiVar.f1423b = i;
        return aiVar;
    }

    /* renamed from: a */
    public aj mergeFrom(ai aiVar) {
        if (aiVar != ai.a()) {
            if (aiVar.b()) {
                a(aiVar.c());
            }
            if (aiVar.d()) {
                a(aiVar.e());
            }
            if (aiVar.f()) {
                a(aiVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public aj mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    al a2 = al.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1425a |= 1;
                        this.f1426b = a2;
                        break;
                    }
                case 16:
                    this.f1425a |= 2;
                    this.f1427c = codedInputStream.readInt32();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1425a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public aj a(al alVar) {
        if (alVar == null) {
            throw new NullPointerException();
        }
        this.f1425a |= 1;
        this.f1426b = alVar;
        return this;
    }

    public aj a(int i) {
        this.f1425a |= 2;
        this.f1427c = i;
        return this;
    }

    public aj a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f1425a |= 4;
        this.d = str;
        return this;
    }
}
