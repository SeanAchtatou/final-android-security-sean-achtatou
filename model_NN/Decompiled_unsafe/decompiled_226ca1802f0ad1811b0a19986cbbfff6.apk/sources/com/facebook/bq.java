package com.facebook;

import android.content.Context;
import android.os.AsyncTask;
import com.facebook.b.u;

/* compiled from: Session */
class bq extends AsyncTask<Void, Void, Void> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bg f1766a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1767b;

    /* renamed from: c  reason: collision with root package name */
    private final Context f1768c;

    public bq(bg bgVar, String str, Context context) {
        this.f1766a = bgVar;
        this.f1767b = str;
        this.f1768c = context.getApplicationContext();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        try {
            cb.a(this.f1768c, this.f1767b);
            return null;
        } catch (Exception e) {
            u.a("Facebook-publish", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Void voidR) {
        synchronized (this.f1766a) {
            bq unused = this.f1766a.o = (bq) null;
        }
    }
}
