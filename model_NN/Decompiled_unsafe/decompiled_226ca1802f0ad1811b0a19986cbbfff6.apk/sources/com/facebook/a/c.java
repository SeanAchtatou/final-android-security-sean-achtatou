package com.facebook.a;

/* compiled from: R */
public final class c {

    /* renamed from: a */
    public static final int com_facebook_loginview_height = 2131427352;

    /* renamed from: b */
    public static final int com_facebook_loginview_padding_bottom = 2131427353;

    /* renamed from: c */
    public static final int com_facebook_loginview_padding_left = 2131427354;

    /* renamed from: d */
    public static final int com_facebook_loginview_padding_right = 2131427355;

    /* renamed from: e */
    public static final int com_facebook_loginview_padding_top = 2131427356;

    /* renamed from: f */
    public static final int com_facebook_loginview_text_size = 2131427357;

    /* renamed from: g */
    public static final int com_facebook_loginview_width = 2131427358;

    /* renamed from: h */
    public static final int com_facebook_profilepictureview_preset_size_large = 2131427359;

    /* renamed from: i */
    public static final int com_facebook_profilepictureview_preset_size_normal = 2131427360;

    /* renamed from: j */
    public static final int com_facebook_profilepictureview_preset_size_small = 2131427361;

    /* renamed from: k */
    public static final int com_facebook_usersettingsfragment_profile_picture_height = 2131427362;

    /* renamed from: l */
    public static final int com_facebook_usersettingsfragment_profile_picture_width = 2131427363;
}
