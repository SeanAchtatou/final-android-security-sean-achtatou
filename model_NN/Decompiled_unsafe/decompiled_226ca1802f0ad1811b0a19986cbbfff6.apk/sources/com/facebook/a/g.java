package com.facebook.a;

/* compiled from: R */
public final class g {

    /* renamed from: a */
    public static final int com_facebook_choose_friends = 2131623992;

    /* renamed from: b */
    public static final int com_facebook_internet_permission_error_message = 2131623994;

    /* renamed from: c */
    public static final int com_facebook_internet_permission_error_title = 2131623995;

    /* renamed from: d */
    public static final int com_facebook_loading = 2131623996;

    /* renamed from: e */
    public static final int com_facebook_loginview_cancel_action = 2131623997;

    /* renamed from: f */
    public static final int com_facebook_loginview_log_in_button = 2131623998;

    /* renamed from: g */
    public static final int com_facebook_loginview_log_out_action = 2131623999;

    /* renamed from: h */
    public static final int com_facebook_loginview_log_out_button = 2131624000;

    /* renamed from: i */
    public static final int com_facebook_loginview_logged_in_as = 2131624001;

    /* renamed from: j */
    public static final int com_facebook_loginview_logged_in_using_facebook = 2131624002;

    /* renamed from: k */
    public static final int com_facebook_nearby = 2131624004;

    /* renamed from: l */
    public static final int com_facebook_picker_done_button_text = 2131624005;

    /* renamed from: m */
    public static final int com_facebook_placepicker_subtitle_catetory_only_format = 2131624006;

    /* renamed from: n */
    public static final int com_facebook_placepicker_subtitle_format = 2131624007;

    /* renamed from: o */
    public static final int com_facebook_placepicker_subtitle_were_here_only_format = 2131624008;

    /* renamed from: p */
    public static final int com_facebook_requesterror_password_changed = 2131624009;

    /* renamed from: q */
    public static final int com_facebook_requesterror_permissions = 2131624010;

    /* renamed from: r */
    public static final int com_facebook_requesterror_reconnect = 2131624011;

    /* renamed from: s */
    public static final int com_facebook_requesterror_relogin = 2131624012;

    /* renamed from: t */
    public static final int com_facebook_requesterror_web_login = 2131624013;

    /* renamed from: u */
    public static final int com_facebook_usersettingsfragment_logged_in = 2131624015;

    /* renamed from: v */
    public static final int com_facebook_usersettingsfragment_not_logged_in = 2131624016;
}
