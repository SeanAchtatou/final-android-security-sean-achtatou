package com.facebook;

import android.content.Intent;
import java.io.Serializable;

/* compiled from: AuthorizationClient */
abstract class j implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f1817a;

    /* access modifiers changed from: package-private */
    public abstract boolean a(k kVar);

    j(c cVar) {
        this.f1817a = cVar;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2, Intent intent) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void c() {
    }
}
