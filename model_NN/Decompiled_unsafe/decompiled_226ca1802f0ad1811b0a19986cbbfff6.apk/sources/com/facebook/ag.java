package com.facebook;

/* compiled from: FacebookServiceException */
public class ag extends z {

    /* renamed from: a  reason: collision with root package name */
    private final ac f1672a;

    public ag(ac acVar, String str) {
        super(str);
        this.f1672a = acVar;
    }

    public final String toString() {
        return "{FacebookServiceException: " + "httpResponseCode: " + this.f1672a.a() + ", facebookErrorCode: " + this.f1672a.b() + ", facebookErrorType: " + this.f1672a.c() + ", message: " + this.f1672a.d() + "}";
    }
}
