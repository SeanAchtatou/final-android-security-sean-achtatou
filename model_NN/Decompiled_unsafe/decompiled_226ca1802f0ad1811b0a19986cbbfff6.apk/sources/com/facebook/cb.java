package com.facebook;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import com.facebook.b.u;
import com.facebook.c.b;
import com.facebook.c.c;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Settings */
public final class cb {

    /* renamed from: a  reason: collision with root package name */
    private static final HashSet<al> f1800a = new HashSet<>(Arrays.asList(al.DEVELOPER_ERRORS));

    /* renamed from: b  reason: collision with root package name */
    private static volatile Executor f1801b;

    /* renamed from: c  reason: collision with root package name */
    private static volatile boolean f1802c;
    private static final Object d = new Object();
    private static final Uri e = Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider");
    private static final BlockingQueue<Runnable> f = new LinkedBlockingQueue(10);
    private static final ThreadFactory g = new cc();

    public static final void a(al alVar) {
        synchronized (f1800a) {
            f1800a.add(alVar);
        }
    }

    public static final boolean b(al alVar) {
        synchronized (f1800a) {
        }
        return false;
    }

    public static Executor a() {
        synchronized (d) {
            if (f1801b == null) {
                Executor c2 = c();
                if (c2 == null) {
                    c2 = new ThreadPoolExecutor(5, (int) NotificationCompat.FLAG_HIGH_PRIORITY, 1, TimeUnit.SECONDS, f, g);
                }
                f1801b = c2;
            }
        }
        return f1801b;
    }

    private static Executor c() {
        try {
            Field field = AsyncTask.class.getField("THREAD_POOL_EXECUTOR");
            if (field == null) {
                return null;
            }
            try {
                Object obj = field.get(null);
                if (obj == null) {
                    return null;
                }
                if (!(obj instanceof Executor)) {
                    return null;
                }
                return (Executor) obj;
            } catch (IllegalAccessException e2) {
                return null;
            }
        } catch (NoSuchFieldException e3) {
            return null;
        }
    }

    public static boolean b() {
        return f1802c;
    }

    public static boolean a(Context context, String str) {
        bc b2 = b(context, str);
        return b2 != null && b2.a() == null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.bc.a(java.lang.String, java.net.HttpURLConnection, com.facebook.ba, boolean):java.util.List<com.facebook.bc>
     arg types: [java.lang.String, ?[OBJECT, ARRAY], com.facebook.ba, int]
     candidates:
      com.facebook.bc.a(java.io.InputStream, java.net.HttpURLConnection, com.facebook.ba, boolean):java.util.List<com.facebook.bc>
      com.facebook.bc.a(java.net.HttpURLConnection, java.util.List<com.facebook.ap>, java.lang.Object, boolean):java.util.List<com.facebook.bc>
      com.facebook.bc.a(java.lang.String, java.net.HttpURLConnection, com.facebook.ba, boolean):java.util.List<com.facebook.bc> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.bc.<init>(com.facebook.ap, java.net.HttpURLConnection, com.facebook.c.b, boolean):void
     arg types: [?[OBJECT, ARRAY], ?[OBJECT, ARRAY], com.facebook.c.b, int]
     candidates:
      com.facebook.bc.<init>(com.facebook.ap, java.net.HttpURLConnection, com.facebook.c.g<com.facebook.c.b>, boolean):void
      com.facebook.bc.<init>(com.facebook.ap, java.net.HttpURLConnection, com.facebook.c.b, boolean):void */
    public static bc b(Context context, String str) {
        b bVar;
        b bVar2;
        if (context == null || str == null) {
            try {
                throw new IllegalArgumentException("Both context and applicationId must be non-null");
            } catch (Exception e2) {
                Exception exc = e2;
                u.a("Facebook-publish", exc);
                return new bc(null, null, new ac(null, exc));
            }
        } else {
            String a2 = a(context.getContentResolver());
            SharedPreferences sharedPreferences = context.getSharedPreferences("com.facebook.sdk.attributionTracking", 0);
            String str2 = str + "ping";
            String str3 = str + "json";
            long j = sharedPreferences.getLong(str2, 0);
            String string = sharedPreferences.getString(str3, null);
            b a3 = c.a();
            a3.a("event", "MOBILE_APP_INSTALL");
            a3.a("attribution", a2);
            ap a4 = ap.a((bg) null, String.format("%s/activities", str), a3, (au) null);
            if (j != 0) {
                if (string != null) {
                    try {
                        bVar = c.a(new JSONObject(string));
                    } catch (JSONException e3) {
                        bVar2 = null;
                    }
                } else {
                    bVar = null;
                }
                bVar2 = bVar;
                if (bVar2 != null) {
                    return new bc((ap) null, (HttpURLConnection) null, bVar2, true);
                }
                return bc.a("true", (HttpURLConnection) null, new ba(a4), true).get(0);
            } else if (a2 == null) {
                throw new z("No attribution id returned from the Facebook application");
            } else if (!u.c(str)) {
                throw new z("Install attribution has been disabled on the server.");
            } else {
                bc c2 = a4.c();
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putLong(str2, System.currentTimeMillis());
                if (!(c2.b() == null || c2.b().d() == null)) {
                    edit.putString(str3, c2.b().d().toString());
                }
                edit.commit();
                return c2;
            }
        }
    }

    public static String a(ContentResolver contentResolver) {
        ContentResolver contentResolver2 = contentResolver;
        Cursor query = contentResolver2.query(e, new String[]{"aid"}, null, null, null);
        if (query == null || !query.moveToFirst()) {
            return null;
        }
        String string = query.getString(query.getColumnIndex("aid"));
        query.close();
        return string;
    }
}
