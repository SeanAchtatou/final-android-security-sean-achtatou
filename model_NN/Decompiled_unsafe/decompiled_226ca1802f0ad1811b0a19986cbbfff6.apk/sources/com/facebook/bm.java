package com.facebook;

import android.app.Activity;
import android.support.v4.app.Fragment;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/* compiled from: Session */
public class bm implements Serializable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final bt f1758a;

    /* renamed from: b  reason: collision with root package name */
    private by f1759b = by.SSO_WITH_FALLBACK;

    /* renamed from: c  reason: collision with root package name */
    private int f1760c = 64206;
    private bu d;
    /* access modifiers changed from: private */
    public boolean e = false;
    private List<String> f = Collections.emptyList();
    private bx g = bx.FRIENDS;
    private String h;
    private String i;

    bm(Activity activity) {
        this.f1758a = new bn(this, activity);
    }

    bm(Fragment fragment) {
        this.f1758a = new bo(this, fragment);
    }

    /* access modifiers changed from: package-private */
    public bm a(bu buVar) {
        this.d = buVar;
        return this;
    }

    /* access modifiers changed from: package-private */
    public bu a() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public bm a(by byVar) {
        if (byVar != null) {
            this.f1759b = byVar;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public by b() {
        return this.f1759b;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.f1760c;
    }

    /* access modifiers changed from: package-private */
    public bm a(List<String> list) {
        if (list != null) {
            this.f = list;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public List<String> d() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public bm a(bx bxVar) {
        if (bxVar != null) {
            this.g = bxVar;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public bt e() {
        return this.f1758a;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.h = str;
    }

    /* access modifiers changed from: package-private */
    public k f() {
        return new k(this.f1759b, this.f1760c, this.e, this.f, this.g, this.h, this.i, new bp(this));
    }
}
