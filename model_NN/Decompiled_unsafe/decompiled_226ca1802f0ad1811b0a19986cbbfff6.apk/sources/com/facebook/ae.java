package com.facebook;

/* compiled from: FacebookRequestError */
public enum ae {
    AUTHENTICATION_RETRY,
    AUTHENTICATION_REOPEN_SESSION,
    PERMISSION,
    SERVER,
    THROTTLING,
    OTHER,
    BAD_REQUEST,
    CLIENT
}
