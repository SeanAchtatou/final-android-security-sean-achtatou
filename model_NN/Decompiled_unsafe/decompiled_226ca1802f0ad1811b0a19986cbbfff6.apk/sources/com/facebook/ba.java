package com.facebook;

import android.os.Handler;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: RequestBatch */
public class ba extends AbstractList<ap> {

    /* renamed from: a  reason: collision with root package name */
    private static AtomicInteger f1739a = new AtomicInteger();

    /* renamed from: b  reason: collision with root package name */
    private Handler f1740b;

    /* renamed from: c  reason: collision with root package name */
    private List<ap> f1741c;
    private int d;
    private final String e;
    private List<bb> f;
    private String g;

    public ba() {
        this.f1741c = new ArrayList();
        this.d = 0;
        this.e = Integer.valueOf(f1739a.incrementAndGet()).toString();
        this.f = new ArrayList();
        this.f1741c = new ArrayList();
    }

    public ba(Collection<ap> collection) {
        this.f1741c = new ArrayList();
        this.d = 0;
        this.e = Integer.valueOf(f1739a.incrementAndGet()).toString();
        this.f = new ArrayList();
        this.f1741c = new ArrayList(collection);
    }

    public ba(ap... apVarArr) {
        this.f1741c = new ArrayList();
        this.d = 0;
        this.e = Integer.valueOf(f1739a.incrementAndGet()).toString();
        this.f = new ArrayList();
        this.f1741c = Arrays.asList(apVarArr);
    }

    public int a() {
        return this.d;
    }

    public void a(bb bbVar) {
        if (!this.f.contains(bbVar)) {
            this.f.add(bbVar);
        }
    }

    /* renamed from: a */
    public final boolean add(ap apVar) {
        return this.f1741c.add(apVar);
    }

    /* renamed from: a */
    public final void add(int i, ap apVar) {
        this.f1741c.add(i, apVar);
    }

    public final void clear() {
        this.f1741c.clear();
    }

    /* renamed from: a */
    public final ap get(int i) {
        return this.f1741c.get(i);
    }

    /* renamed from: b */
    public final ap remove(int i) {
        return this.f1741c.remove(i);
    }

    /* renamed from: b */
    public final ap set(int i, ap apVar) {
        return this.f1741c.set(i, apVar);
    }

    public final int size() {
        return this.f1741c.size();
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final Handler c() {
        return this.f1740b;
    }

    /* access modifiers changed from: package-private */
    public final void a(Handler handler) {
        this.f1740b = handler;
    }

    /* access modifiers changed from: package-private */
    public final List<ap> d() {
        return this.f1741c;
    }

    /* access modifiers changed from: package-private */
    public final List<bb> e() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.g = str;
    }

    public final List<bc> g() {
        return i();
    }

    public final az h() {
        return j();
    }

    /* access modifiers changed from: package-private */
    public List<bc> i() {
        return ap.b(this);
    }

    /* access modifiers changed from: package-private */
    public az j() {
        return ap.c(this);
    }
}
