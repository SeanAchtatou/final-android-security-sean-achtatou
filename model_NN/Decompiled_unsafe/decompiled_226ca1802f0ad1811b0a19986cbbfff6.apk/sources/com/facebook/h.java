package com.facebook;

import java.util.ArrayList;

/* compiled from: AuthorizationClient */
class h implements bb {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f1814a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ s f1815b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ ArrayList f1816c;
    final /* synthetic */ c d;

    h(c cVar, ArrayList arrayList, s sVar, ArrayList arrayList2) {
        this.d = cVar;
        this.f1814a = arrayList;
        this.f1815b = sVar;
        this.f1816c = arrayList2;
    }

    public void a(ba baVar) {
        s a2;
        try {
            if (this.f1814a.size() != 2 || this.f1814a.get(0) == null || this.f1814a.get(1) == null || !((String) this.f1814a.get(0)).equals(this.f1814a.get(1))) {
                a2 = s.a("User logged in as different Facebook user.", null);
            } else {
                a2 = s.a(a.a(this.f1815b.f1829b, this.f1816c));
            }
            this.d.b(a2);
        } catch (Exception e) {
            this.d.b(s.a("Caught exception", e.getMessage()));
        } finally {
            this.d.j();
        }
    }
}
