package com.facebook;

import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;

/* compiled from: AuthorizationClient */
class p extends o {

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ c f1826c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    p(c cVar) {
        super(cVar);
        this.f1826c = cVar;
    }

    /* access modifiers changed from: package-private */
    public boolean a(k kVar) {
        return a(ao.a(this.f1826c.f1788c, kVar.f(), new ArrayList(kVar.b()), kVar.e().a()), kVar.d());
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2, Intent intent) {
        s sVar = null;
        if (intent == null) {
            sVar = s.a("Operation canceled");
        } else if (!ao.a(intent)) {
            if (i2 == 0) {
                sVar = s.a(intent.getStringExtra("com.facebook.platform.status.ERROR_DESCRIPTION"));
            } else if (i2 != -1) {
                sVar = s.a("Unexpected resultCode from authorization.", null);
            } else {
                sVar = a(intent);
            }
        }
        if (sVar != null) {
            this.f1826c.a(sVar);
            return true;
        }
        this.f1826c.e();
        return true;
    }

    private s a(Intent intent) {
        Bundle extras = intent.getExtras();
        String string = extras.getString("com.facebook.platform.status.ERROR_TYPE");
        if (string == null) {
            return s.a(a.a(extras, b.FACEBOOK_APPLICATION_NATIVE));
        }
        if ("ServiceDisabled".equals(string)) {
            return null;
        }
        if ("UserCanceled".equals(string)) {
            return s.a((String) null);
        }
        return s.a(string, extras.getString("error_description"));
    }
}
