package com.facebook;

import android.app.Activity;
import android.content.Intent;

/* compiled from: AuthorizationClient */
class d implements u {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f1807a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ c f1808b;

    d(c cVar, Activity activity) {
        this.f1808b = cVar;
        this.f1807a = activity;
    }

    public void a(Intent intent, int i) {
        this.f1807a.startActivityForResult(intent, i);
    }

    public Activity a() {
        return this.f1807a;
    }
}
