package com.facebook;

import java.io.Serializable;
import java.util.List;

/* compiled from: AuthorizationClient */
class k implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final transient u f1818a;

    /* renamed from: b  reason: collision with root package name */
    private by f1819b;

    /* renamed from: c  reason: collision with root package name */
    private int f1820c;
    private boolean d = false;
    private List<String> e;
    private bx f;
    private String g;
    private String h;

    k(by byVar, int i, boolean z, List<String> list, bx bxVar, String str, String str2, u uVar) {
        this.f1819b = byVar;
        this.f1820c = i;
        this.d = z;
        this.e = list;
        this.f = bxVar;
        this.g = str;
        this.h = str2;
        this.f1818a = uVar;
    }

    /* access modifiers changed from: package-private */
    public u a() {
        return this.f1818a;
    }

    /* access modifiers changed from: package-private */
    public List<String> b() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void a(List<String> list) {
        this.e = list;
    }

    /* access modifiers changed from: package-private */
    public by c() {
        return this.f1819b;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.f1820c;
    }

    /* access modifiers changed from: package-private */
    public bx e() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public String h() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        return this.h != null && !this.d;
    }
}
