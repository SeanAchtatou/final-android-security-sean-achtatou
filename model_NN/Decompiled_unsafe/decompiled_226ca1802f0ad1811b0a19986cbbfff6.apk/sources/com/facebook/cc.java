package com.facebook;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: Settings */
final class cc implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f1803a = new AtomicInteger(0);

    cc() {
    }

    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, "FacebookSdk #" + this.f1803a.incrementAndGet());
    }
}
