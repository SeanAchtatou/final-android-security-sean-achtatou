package com.facebook;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import java.util.Date;

/* compiled from: Session */
class bv implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final Messenger f1772a = new Messenger(new bw(this.f1774c, this));

    /* renamed from: b  reason: collision with root package name */
    Messenger f1773b = null;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ bg f1774c;

    bv(bg bgVar) {
        this.f1774c = bgVar;
    }

    public void a() {
        Intent a2 = ao.a(bg.j());
        if (a2 == null || !bg.d.bindService(a2, new bv(this.f1774c), 1)) {
            b();
        } else {
            this.f1774c.a(new Date());
        }
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.f1773b = new Messenger(iBinder);
        c();
    }

    public void onServiceDisconnected(ComponentName componentName) {
        b();
        bg.d.unbindService(this);
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.f1774c.r == this) {
            bv unused = this.f1774c.r = (bv) null;
        }
    }

    private void c() {
        Bundle bundle = new Bundle();
        bundle.putString("access_token", this.f1774c.n().a());
        Message obtain = Message.obtain();
        obtain.setData(bundle);
        obtain.replyTo = this.f1772a;
        try {
            this.f1773b.send(obtain);
        } catch (RemoteException e) {
            b();
        }
    }
}
