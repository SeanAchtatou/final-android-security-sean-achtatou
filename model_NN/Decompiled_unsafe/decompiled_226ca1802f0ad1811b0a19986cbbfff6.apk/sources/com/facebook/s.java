package com.facebook;

import java.io.Serializable;

/* compiled from: AuthorizationClient */
class s implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    final t f1828a;

    /* renamed from: b  reason: collision with root package name */
    final a f1829b;

    /* renamed from: c  reason: collision with root package name */
    final String f1830c;

    private s(t tVar, a aVar, String str) {
        this.f1829b = aVar;
        this.f1830c = str;
        this.f1828a = tVar;
    }

    static s a(a aVar) {
        return new s(t.SUCCESS, aVar, null);
    }

    static s a(String str) {
        return new s(t.CANCEL, null, str);
    }

    static s a(String str, String str2) {
        if (str2 != null) {
            str = str + ": " + str2;
        }
        return new s(t.ERROR, null, str);
    }
}
