package com.facebook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.a.g;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AuthorizationClient */
class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    List<j> f1786a;

    /* renamed from: b  reason: collision with root package name */
    j f1787b;

    /* renamed from: c  reason: collision with root package name */
    transient Context f1788c;
    transient u d;
    transient r e;
    transient l f;
    transient boolean g;
    k h;

    c() {
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        this.f1788c = context;
        this.d = null;
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity) {
        this.f1788c = activity;
        this.d = new d(this, activity);
    }

    /* access modifiers changed from: package-private */
    public void a(k kVar) {
        if (b()) {
            a();
        } else {
            b(kVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(k kVar) {
        if (kVar != null) {
            if (this.h != null) {
                throw new z("Attempted to authorize while a request is pending.");
            } else if (!kVar.i() || d()) {
                this.h = kVar;
                this.f1786a = c(kVar);
                e();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.h == null || this.f1787b == null) {
            throw new z("Attempted to continue authorization without a pending request.");
        } else if (this.f1787b.a()) {
            this.f1787b.c();
            f();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return (this.h == null || this.f1787b == null) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.f1787b != null) {
            this.f1787b.c();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2, Intent intent) {
        if (i == this.h.d()) {
            return this.f1787b.a(i, i2, intent);
        }
        return false;
    }

    private List<j> c(k kVar) {
        ArrayList arrayList = new ArrayList();
        by c2 = kVar.c();
        if (c2.a()) {
            if (!kVar.g()) {
                arrayList.add(new m(this));
                arrayList.add(new p(this));
            }
            arrayList.add(new q(this));
        }
        if (c2.b()) {
            arrayList.add(new v(this));
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        if (this.g) {
            return true;
        }
        if (a("android.permission.INTERNET") != 0) {
            b(s.a(this.f1788c.getString(g.com_facebook_internet_permission_error_title), this.f1788c.getString(g.com_facebook_internet_permission_error_message)));
            return false;
        }
        this.g = true;
        return true;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        while (this.f1786a != null && !this.f1786a.isEmpty()) {
            this.f1787b = this.f1786a.remove(0);
            if (f()) {
                return;
            }
        }
        if (this.h != null) {
            h();
        }
    }

    private void h() {
        b(s.a("Login attempt failed.", null));
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        if (!this.f1787b.b() || d()) {
            return this.f1787b.a(this.h);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(s sVar) {
        if (sVar.f1829b == null || !this.h.i()) {
            b(sVar);
        } else {
            c(sVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(s sVar) {
        this.f1786a = null;
        this.f1787b = null;
        this.h = null;
        e(sVar);
    }

    /* access modifiers changed from: package-private */
    public void a(r rVar) {
        this.e = rVar;
    }

    /* access modifiers changed from: package-private */
    public void a(l lVar) {
        this.f = lVar;
    }

    /* access modifiers changed from: package-private */
    public u g() {
        if (this.d != null) {
            return this.d;
        }
        if (this.h != null) {
            return new e(this);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public int a(String str) {
        return this.f1788c.checkCallingOrSelfPermission(str);
    }

    /* access modifiers changed from: package-private */
    public void c(s sVar) {
        if (sVar.f1829b == null) {
            throw new z("Can't validate without a token");
        }
        ba d2 = d(sVar);
        i();
        d2.h();
    }

    /* access modifiers changed from: package-private */
    public ba d(s sVar) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        String a2 = sVar.f1829b.a();
        f fVar = new f(this, arrayList);
        String h2 = this.h.h();
        ap c2 = c(h2);
        c2.a((au) fVar);
        ap c3 = c(a2);
        c3.a((au) fVar);
        ap b2 = b(h2);
        b2.a((au) new g(this, arrayList2));
        ba baVar = new ba(c2, c3, b2);
        baVar.a(this.h.f());
        baVar.a(new h(this, arrayList, sVar, arrayList2));
        return baVar;
    }

    /* access modifiers changed from: package-private */
    public ap b(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id");
        bundle.putString("access_token", str);
        return new ap(null, "me/permissions", bundle, ak.GET, null);
    }

    /* access modifiers changed from: package-private */
    public ap c(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id");
        bundle.putString("access_token", str);
        return new ap(null, "me", bundle, ak.GET, null);
    }

    private void e(s sVar) {
        if (this.e != null) {
            this.e.a(sVar);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.f != null) {
            this.f.a();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.f != null) {
            this.f.b();
        }
    }
}
