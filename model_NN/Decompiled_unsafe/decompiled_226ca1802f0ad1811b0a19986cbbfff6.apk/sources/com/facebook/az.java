package com.facebook;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.concurrent.Executor;

@TargetApi(3)
/* compiled from: RequestAsyncTask */
public class az extends AsyncTask<Void, Void, List<bc>> {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1697a = az.class.getCanonicalName();

    /* renamed from: b  reason: collision with root package name */
    private static Method f1698b;

    /* renamed from: c  reason: collision with root package name */
    private final HttpURLConnection f1699c;
    private final ba d;
    private Exception e;

    static {
        for (Method method : AsyncTask.class.getMethods()) {
            if ("executeOnExecutor".equals(method.getName())) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length == 2 && parameterTypes[0] == Executor.class && parameterTypes[1].isArray()) {
                    f1698b = method;
                    return;
                }
            }
        }
    }

    public az(ba baVar) {
        this(null, baVar);
    }

    public az(HttpURLConnection httpURLConnection, ba baVar) {
        this.d = baVar;
        this.f1699c = httpURLConnection;
    }

    public String toString() {
        return "{RequestAsyncTask: " + " connection: " + this.f1699c + ", requests: " + this.d + "}";
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        if (this.d.c() == null) {
            this.d.a(new Handler());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(List<bc> list) {
        super.onPostExecute(list);
        if (this.e != null) {
            Log.d(f1697a, String.format("onPostExecute: exception encountered during request: %s", this.e.getMessage()));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public List<bc> doInBackground(Void... voidArr) {
        try {
            if (this.f1699c == null) {
                return this.d.g();
            }
            return ap.a(this.f1699c, this.d);
        } catch (Exception e2) {
            this.e = e2;
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public az a() {
        try {
            if (f1698b != null) {
                f1698b.invoke(this, cb.a(), null);
                return this;
            }
        } catch (IllegalAccessException | InvocationTargetException e2) {
        }
        execute(new Void[0]);
        return this;
    }
}
