package com.facebook;

/* compiled from: SessionLoginBehavior */
public enum by {
    SSO_WITH_FALLBACK(true, true),
    SSO_ONLY(true, false),
    SUPPRESS_SSO(false, true);
    
    private final boolean d;
    private final boolean e;

    private by(boolean z, boolean z2) {
        this.d = z;
        this.e = z2;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.e;
    }
}
