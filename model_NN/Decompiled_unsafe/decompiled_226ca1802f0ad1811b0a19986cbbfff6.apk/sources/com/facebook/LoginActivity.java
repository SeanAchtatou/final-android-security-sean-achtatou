package com.facebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.a.e;
import com.facebook.a.f;

public class LoginActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private String f1655a;

    /* renamed from: b  reason: collision with root package name */
    private c f1656b;

    /* renamed from: c  reason: collision with root package name */
    private k f1657c;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(f.com_facebook_login_activity_layout);
        if (bundle != null) {
            this.f1655a = bundle.getString("callingPackage");
            this.f1656b = (c) bundle.getSerializable("authorizationClient");
        } else {
            this.f1655a = getCallingPackage();
            this.f1656b = new c();
            this.f1657c = (k) getIntent().getSerializableExtra("request");
        }
        this.f1656b.a((Activity) this);
        this.f1656b.a(new am(this));
        this.f1656b.a(new an(this));
    }

    /* access modifiers changed from: private */
    public void a(s sVar) {
        this.f1657c = null;
        int i = sVar.f1828a == t.f1832b ? 0 : -1;
        Bundle bundle = new Bundle();
        bundle.putSerializable("com.facebook.LoginActivity:Result", sVar);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(i, intent);
        finish();
    }

    public void onResume() {
        super.onResume();
        if (this.f1655a == null) {
            throw new z("Cannot call LoginActivity with a null calling package. This can occur if the launchMode of the caller is singleInstance.");
        }
        this.f1656b.a(this.f1657c);
    }

    public void onPause() {
        super.onPause();
        this.f1656b.c();
        findViewById(e.com_facebook_login_activity_progress_bar).setVisibility(8);
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("callingPackage", this.f1655a);
        bundle.putSerializable("authorizationClient", this.f1656b);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        this.f1656b.a(i, i2, intent);
    }

    static Bundle a(k kVar) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("request", kVar);
        return bundle;
    }
}
