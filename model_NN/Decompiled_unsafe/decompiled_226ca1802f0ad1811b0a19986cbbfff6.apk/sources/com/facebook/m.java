package com.facebook;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AuthorizationClient */
class m extends j {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ c f1821b;

    /* renamed from: c  reason: collision with root package name */
    private transient ah f1822c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    m(c cVar) {
        super(cVar);
        this.f1821b = cVar;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.f1822c != null) {
            this.f1822c.b();
            this.f1822c = null;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(k kVar) {
        this.f1822c = new ah(this.f1821b.f1788c, kVar.f());
        if (!this.f1822c.a()) {
            return false;
        }
        this.f1821b.i();
        this.f1822c.a(new n(this, kVar));
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(k kVar, Bundle bundle) {
        this.f1822c = null;
        this.f1821b.j();
        if (bundle != null) {
            ArrayList<String> stringArrayList = bundle.getStringArrayList("com.facebook.platform.extra.PERMISSIONS");
            List<String> b2 = kVar.b();
            if (stringArrayList == null || (b2 != null && !stringArrayList.containsAll(b2))) {
                ArrayList arrayList = new ArrayList();
                for (String next : b2) {
                    if (!stringArrayList.contains(next)) {
                        arrayList.add(next);
                    }
                }
                kVar.a(arrayList);
            } else {
                this.f1821b.a(s.a(a.a(bundle, b.FACEBOOK_APPLICATION_SERVICE)));
                return;
            }
        }
        this.f1821b.e();
    }
}
