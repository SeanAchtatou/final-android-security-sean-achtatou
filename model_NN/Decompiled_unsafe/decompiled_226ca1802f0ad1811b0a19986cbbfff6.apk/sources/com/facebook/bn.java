package com.facebook;

import android.app.Activity;
import android.content.Intent;

/* compiled from: Session */
class bn implements bt {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f1761a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ bm f1762b;

    bn(bm bmVar, Activity activity) {
        this.f1762b = bmVar;
        this.f1761a = activity;
    }

    public void a(Intent intent, int i) {
        this.f1761a.startActivityForResult(intent, i);
    }

    public Activity a() {
        return this.f1761a;
    }
}
