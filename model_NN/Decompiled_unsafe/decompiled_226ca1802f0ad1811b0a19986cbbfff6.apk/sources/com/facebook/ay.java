package com.facebook;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.facebook.b.n;
import java.io.BufferedOutputStream;

/* compiled from: Request */
class ay implements ax {

    /* renamed from: a  reason: collision with root package name */
    private final BufferedOutputStream f1694a;

    /* renamed from: b  reason: collision with root package name */
    private final n f1695b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1696c = true;

    public ay(BufferedOutputStream bufferedOutputStream, n nVar) {
        this.f1694a = bufferedOutputStream;
        this.f1695b = nVar;
    }

    public void a(String str, Object obj) {
        if (ap.d(obj)) {
            a(str, ap.e(obj));
        } else if (obj instanceof Bitmap) {
            a(str, (Bitmap) obj);
        } else if (obj instanceof byte[]) {
            a(str, (byte[]) obj);
        } else if (obj instanceof ParcelFileDescriptor) {
            a(str, (ParcelFileDescriptor) obj);
        } else {
            throw new IllegalArgumentException("value is not a supported type: String, Bitmap, byte[]");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.n.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.b.n.a(java.lang.String, java.lang.String):void
      com.facebook.b.n.a(java.lang.String, java.lang.Object[]):void
      com.facebook.b.n.a(java.lang.String, java.lang.Object):void */
    public void a(String str, String str2) {
        a(str, null, null);
        b("%s", str2);
        a();
        if (this.f1695b != null) {
            this.f1695b.a("    " + str, (Object) str2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.n.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.b.n.a(java.lang.String, java.lang.String):void
      com.facebook.b.n.a(java.lang.String, java.lang.Object[]):void
      com.facebook.b.n.a(java.lang.String, java.lang.Object):void */
    public void a(String str, Bitmap bitmap) {
        a(str, str, "image/png");
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, this.f1694a);
        b("", new Object[0]);
        a();
        this.f1695b.a("    " + str, (Object) "<Image>");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.n.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.b.n.a(java.lang.String, java.lang.String):void
      com.facebook.b.n.a(java.lang.String, java.lang.Object[]):void
      com.facebook.b.n.a(java.lang.String, java.lang.Object):void */
    public void a(String str, byte[] bArr) {
        a(str, str, "content/unknown");
        this.f1694a.write(bArr);
        b("", new Object[0]);
        a();
        this.f1695b.a("    " + str, (Object) String.format("<Data: %d>", Integer.valueOf(bArr.length)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.n.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.b.n.a(java.lang.String, java.lang.String):void
      com.facebook.b.n.a(java.lang.String, java.lang.Object[]):void
      com.facebook.b.n.a(java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x006a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r9, android.os.ParcelFileDescriptor r10) {
        /*
            r8 = this;
            r2 = 0
            r4 = 0
            java.lang.String r0 = "content/unknown"
            r8.a(r9, r9, r0)
            android.os.ParcelFileDescriptor$AutoCloseInputStream r3 = new android.os.ParcelFileDescriptor$AutoCloseInputStream     // Catch:{ all -> 0x0061 }
            r3.<init>(r10)     // Catch:{ all -> 0x0061 }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x006e }
            r1.<init>(r3)     // Catch:{ all -> 0x006e }
            r0 = 8192(0x2000, float:1.14794E-41)
            byte[] r2 = new byte[r0]     // Catch:{ all -> 0x0072 }
            r0 = r4
        L_0x0016:
            int r5 = r1.read(r2)     // Catch:{ all -> 0x0072 }
            r6 = -1
            if (r5 == r6) goto L_0x0025
            java.io.BufferedOutputStream r6 = r8.f1694a     // Catch:{ all -> 0x0072 }
            r7 = 0
            r6.write(r2, r7, r5)     // Catch:{ all -> 0x0072 }
            int r0 = r0 + r5
            goto L_0x0016
        L_0x0025:
            if (r1 == 0) goto L_0x002a
            r1.close()
        L_0x002a:
            if (r3 == 0) goto L_0x002f
            r3.close()
        L_0x002f:
            java.lang.String r1 = ""
            java.lang.Object[] r2 = new java.lang.Object[r4]
            r8.b(r1, r2)
            r8.a()
            com.facebook.b.n r1 = r8.f1695b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "    "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r9)
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = "<Data: %d>"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r5[r4] = r0
            java.lang.String r0 = java.lang.String.format(r3, r5)
            r1.a(r2, r0)
            return
        L_0x0061:
            r0 = move-exception
            r1 = r2
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            r1.close()
        L_0x0068:
            if (r2 == 0) goto L_0x006d
            r2.close()
        L_0x006d:
            throw r0
        L_0x006e:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0063
        L_0x0072:
            r0 = move-exception
            r2 = r3
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ay.a(java.lang.String, android.os.ParcelFileDescriptor):void");
    }

    public void a() {
        b("--%s", "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
    }

    public void a(String str, String str2, String str3) {
        a("Content-Disposition: form-data; name=\"%s\"", str);
        if (str2 != null) {
            a("; filename=\"%s\"", str2);
        }
        b("", new Object[0]);
        if (str3 != null) {
            b("%s: %s", "Content-Type", str3);
        }
        b("", new Object[0]);
    }

    public void a(String str, Object... objArr) {
        if (this.f1696c) {
            this.f1694a.write("--".getBytes());
            this.f1694a.write("3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f".getBytes());
            this.f1694a.write("\r\n".getBytes());
            this.f1696c = false;
        }
        this.f1694a.write(String.format(str, objArr).getBytes());
    }

    public void b(String str, Object... objArr) {
        a(str, objArr);
        a("\r\n", new Object[0]);
    }
}
