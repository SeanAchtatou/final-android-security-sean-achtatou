package com.facebook;

import android.content.Intent;
import android.os.Bundle;
import com.facebook.b.o;

/* compiled from: AuthorizationClient */
class q extends o {

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ c f1827c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    q(c cVar) {
        super(cVar);
        this.f1827c = cVar;
    }

    /* access modifiers changed from: package-private */
    public boolean a(k kVar) {
        return a(ao.a(this.f1827c.f1788c, kVar.f(), kVar.b()), kVar.d());
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2, Intent intent) {
        s a2;
        if (intent == null) {
            a2 = s.a("Operation canceled");
        } else if (i2 == 0) {
            a2 = s.a(intent.getStringExtra("error"));
        } else if (i2 != -1) {
            a2 = s.a("Unexpected resultCode from authorization.", null);
        } else {
            a2 = a(intent);
        }
        if (a2 != null) {
            this.f1827c.a(a2);
            return true;
        }
        this.f1827c.e();
        return true;
    }

    private s a(Intent intent) {
        Bundle extras = intent.getExtras();
        String string = extras.getString("error");
        if (string == null) {
            string = extras.getString("error_type");
        }
        if (string == null) {
            return s.a(a.a(this.f1827c.h.b(), extras, b.FACEBOOK_APPLICATION_WEB));
        }
        if (o.f1725a.contains(string)) {
            return null;
        }
        if (o.f1726b.contains(string)) {
            return s.a((String) null);
        }
        return s.a(string, extras.getString("error_description"));
    }
}
