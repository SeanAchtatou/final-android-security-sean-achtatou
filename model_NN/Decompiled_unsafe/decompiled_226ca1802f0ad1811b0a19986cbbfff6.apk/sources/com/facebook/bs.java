package com.facebook;

import android.app.Activity;
import android.support.v4.app.Fragment;
import java.util.List;

/* compiled from: Session */
public final class bs extends bm {
    public bs(Activity activity) {
        super(activity);
    }

    public bs(Fragment fragment) {
        super(fragment);
    }

    /* renamed from: b */
    public final bs a(bu buVar) {
        super.a(buVar);
        return this;
    }

    /* renamed from: b */
    public final bs a(by byVar) {
        super.a(byVar);
        return this;
    }

    /* renamed from: b */
    public final bs a(List<String> list) {
        super.a(list);
        return this;
    }

    /* renamed from: b */
    public final bs a(bx bxVar) {
        super.a(bxVar);
        return this;
    }
}
