package com.facebook;

import android.content.ActivityNotFoundException;
import android.content.Intent;

/* compiled from: AuthorizationClient */
abstract class o extends j {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ c f1825b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    o(c cVar) {
        super(cVar);
        this.f1825b = cVar;
    }

    /* access modifiers changed from: protected */
    public boolean a(Intent intent, int i) {
        if (intent == null) {
            return false;
        }
        try {
            this.f1825b.g().a(intent, i);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }
}
