package com.facebook;

import com.facebook.a.g;
import com.facebook.b.u;
import java.net.HttpURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: FacebookRequestError */
public final class ac {

    /* renamed from: a  reason: collision with root package name */
    private static final af f1664a = new af(200, 299);

    /* renamed from: b  reason: collision with root package name */
    private static final af f1665b = new af(200, 299);

    /* renamed from: c  reason: collision with root package name */
    private static final af f1666c = new af(400, 499);
    private static final af d = new af(500, 599);
    private final int e;
    private final boolean f;
    private final ae g;
    private final int h;
    private final int i;
    private final int j;
    private final String k;
    private final String l;
    private final JSONObject m;
    private final JSONObject n;
    private final Object o;
    private final HttpURLConnection p;
    private final z q;

    private ac(int i2, int i3, int i4, String str, String str2, JSONObject jSONObject, JSONObject jSONObject2, Object obj, HttpURLConnection httpURLConnection, z zVar) {
        boolean z;
        int i5;
        boolean z2 = false;
        this.h = i2;
        this.i = i3;
        this.j = i4;
        this.k = str;
        this.l = str2;
        this.n = jSONObject;
        this.m = jSONObject2;
        this.o = obj;
        this.p = httpURLConnection;
        if (zVar != null) {
            this.q = zVar;
            z = true;
        } else {
            this.q = new ag(this, str2);
            z = false;
        }
        ae aeVar = null;
        if (z) {
            aeVar = ae.CLIENT;
            i5 = 0;
        } else {
            if (i3 == 1 || i3 == 2) {
                aeVar = ae.SERVER;
                i5 = 0;
            } else if (i3 == 4 || i3 == 17) {
                aeVar = ae.THROTTLING;
                i5 = 0;
            } else if (i3 == 10 || f1664a.a(i3)) {
                aeVar = ae.PERMISSION;
                i5 = g.com_facebook_requesterror_permissions;
            } else if (i3 != 102 && i3 != 190) {
                i5 = 0;
            } else if (i4 == 459 || i4 == 464) {
                aeVar = ae.AUTHENTICATION_RETRY;
                i5 = g.com_facebook_requesterror_web_login;
                z2 = true;
            } else {
                aeVar = ae.AUTHENTICATION_REOPEN_SESSION;
                if (i4 == 458 || i4 == 463) {
                    i5 = g.com_facebook_requesterror_relogin;
                } else if (i4 == 460) {
                    i5 = g.com_facebook_requesterror_password_changed;
                } else {
                    i5 = g.com_facebook_requesterror_reconnect;
                    z2 = true;
                }
            }
            if (aeVar == null) {
                if (f1666c.a(i2)) {
                    aeVar = ae.BAD_REQUEST;
                } else if (d.a(i2)) {
                    aeVar = ae.SERVER;
                } else {
                    aeVar = ae.OTHER;
                }
            }
        }
        this.g = aeVar;
        this.e = i5;
        this.f = z2;
    }

    private ac(int i2, int i3, int i4, String str, String str2, JSONObject jSONObject, JSONObject jSONObject2, Object obj, HttpURLConnection httpURLConnection) {
        this(i2, i3, i4, str, str2, jSONObject, jSONObject2, obj, httpURLConnection, null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    ac(HttpURLConnection httpURLConnection, Exception exc) {
        this(-1, -1, -1, null, null, null, null, null, httpURLConnection, exc instanceof z ? (z) exc : new z(exc));
    }

    public ac(int i2, String str, String str2) {
        this(-1, i2, -1, str, str2, null, null, null, null, null);
    }

    public int a() {
        return this.h;
    }

    public int b() {
        return this.i;
    }

    public String c() {
        return this.k;
    }

    public String d() {
        if (this.l != null) {
            return this.l;
        }
        return this.q.getLocalizedMessage();
    }

    public z e() {
        return this.q;
    }

    public String toString() {
        return "{HttpStatus: " + this.h + ", errorCode: " + this.i + ", errorType: " + this.k + ", errorMessage: " + this.l + "}";
    }

    static ac a(JSONObject jSONObject, Object obj, HttpURLConnection httpURLConnection) {
        String optString;
        String optString2;
        int i2;
        int i3 = -1;
        try {
            if (jSONObject.has("code")) {
                int i4 = jSONObject.getInt("code");
                Object a2 = u.a(jSONObject, "body", "FACEBOOK_NON_JSON_RESULT");
                if (a2 != null && (a2 instanceof JSONObject)) {
                    JSONObject jSONObject2 = (JSONObject) a2;
                    boolean z = false;
                    if (jSONObject2.has("error")) {
                        JSONObject jSONObject3 = (JSONObject) u.a(jSONObject2, "error", (String) null);
                        String optString3 = jSONObject3.optString("type", null);
                        optString2 = jSONObject3.optString("message", null);
                        int optInt = jSONObject3.optInt("code", -1);
                        i3 = jSONObject3.optInt("error_subcode", -1);
                        z = true;
                        i2 = optInt;
                        optString = optString3;
                    } else if (jSONObject2.has("error_code") || jSONObject2.has("error_msg") || jSONObject2.has("error_reason")) {
                        optString = jSONObject2.optString("error_reason", null);
                        optString2 = jSONObject2.optString("error_msg", null);
                        int optInt2 = jSONObject2.optInt("error_code", -1);
                        i3 = jSONObject2.optInt("error_subcode", -1);
                        i2 = optInt2;
                        z = true;
                    } else {
                        i2 = -1;
                        optString2 = null;
                        optString = null;
                    }
                    if (z) {
                        return new ac(i4, i2, i3, optString, optString2, jSONObject2, jSONObject, obj, httpURLConnection);
                    }
                }
                if (!f1665b.a(i4)) {
                    return new ac(i4, -1, -1, null, null, jSONObject.has("body") ? (JSONObject) u.a(jSONObject, "body", "FACEBOOK_NON_JSON_RESULT") : null, jSONObject, obj, httpURLConnection);
                }
            }
        } catch (JSONException e2) {
        }
        return null;
    }
}
