package com.facebook;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.b.u;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/* compiled from: AccessToken */
public final class a implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f1658a = (!a.class.desiredAssertionStatus());

    /* renamed from: b  reason: collision with root package name */
    private static final Date f1659b = new Date(Long.MIN_VALUE);

    /* renamed from: c  reason: collision with root package name */
    private static final Date f1660c = new Date(Long.MAX_VALUE);
    private static final Date d = f1660c;
    private static final Date e = new Date();
    private static final b f = b.FACEBOOK_APPLICATION_WEB;
    private static final Date g = f1659b;
    private final Date h;
    private final List<String> i;
    private final String j;
    private final b k;
    private final Date l;

    a(String str, Date date, List<String> list, b bVar, Date date2) {
        list = list == null ? Collections.emptyList() : list;
        this.h = date;
        this.i = Collections.unmodifiableList(list);
        this.j = str;
        this.k = bVar;
        this.l = date2;
    }

    public String a() {
        return this.j;
    }

    public Date b() {
        return this.h;
    }

    public List<String> c() {
        return this.i;
    }

    public b d() {
        return this.k;
    }

    public Date e() {
        return this.l;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{AccessToken");
        sb.append(" token:").append(h());
        a(sb);
        sb.append("}");
        return sb.toString();
    }

    static a a(List<String> list) {
        return new a("", g, list, b.NONE, e);
    }

    static a a(Bundle bundle, b bVar) {
        return a(bundle.getStringArrayList("com.facebook.platform.extra.PERMISSIONS"), bundle.getString("com.facebook.platform.extra.ACCESS_TOKEN"), a(bundle, "com.facebook.platform.extra.EXPIRES_SECONDS_SINCE_EPOCH", new Date(0)), bVar);
    }

    static a a(List<String> list, Bundle bundle, b bVar) {
        return a(list, bundle.getString("access_token"), a(bundle, "expires_in", new Date()), bVar);
    }

    @SuppressLint({"FieldGetter"})
    static a a(a aVar, Bundle bundle) {
        if (f1658a || aVar.k == b.FACEBOOK_APPLICATION_WEB || aVar.k == b.FACEBOOK_APPLICATION_NATIVE || aVar.k == b.FACEBOOK_APPLICATION_SERVICE) {
            Date a2 = a(bundle, "expires_in", new Date(0));
            return a(aVar.c(), bundle.getString("access_token"), a2, aVar.k);
        }
        throw new AssertionError();
    }

    static a a(a aVar, List<String> list) {
        return new a(aVar.j, aVar.h, list, aVar.k, aVar.l);
    }

    private static a a(List<String> list, String str, Date date, b bVar) {
        if (u.a(str) || date == null) {
            return a(list);
        }
        return new a(str, date, list, bVar, new Date());
    }

    static a a(Bundle bundle) {
        List unmodifiableList;
        ArrayList<String> stringArrayList = bundle.getStringArrayList("com.facebook.TokenCachingStrategy.Permissions");
        if (stringArrayList == null) {
            unmodifiableList = Collections.emptyList();
        } else {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(stringArrayList));
        }
        return new a(bundle.getString("com.facebook.TokenCachingStrategy.Token"), ce.a(bundle, "com.facebook.TokenCachingStrategy.ExpirationDate"), unmodifiableList, ce.c(bundle), ce.a(bundle, "com.facebook.TokenCachingStrategy.LastRefreshDate"));
    }

    /* access modifiers changed from: package-private */
    public Bundle f() {
        Bundle bundle = new Bundle();
        bundle.putString("com.facebook.TokenCachingStrategy.Token", this.j);
        ce.a(bundle, "com.facebook.TokenCachingStrategy.ExpirationDate", this.h);
        bundle.putStringArrayList("com.facebook.TokenCachingStrategy.Permissions", new ArrayList(this.i));
        bundle.putSerializable("com.facebook.TokenCachingStrategy.AccessTokenSource", this.k);
        ce.a(bundle, "com.facebook.TokenCachingStrategy.LastRefreshDate", this.l);
        return bundle;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return u.a(this.j) || new Date().after(this.h);
    }

    private String h() {
        if (this.j == null) {
            return "null";
        }
        if (cb.b(al.INCLUDE_ACCESS_TOKENS)) {
            return this.j;
        }
        return "ACCESS_TOKEN_REMOVED";
    }

    private void a(StringBuilder sb) {
        sb.append(" permissions:");
        if (this.i == null) {
            sb.append("null");
            return;
        }
        sb.append("[");
        sb.append(TextUtils.join(", ", this.i));
        sb.append("]");
    }

    private static Date a(Bundle bundle, String str, Date date) {
        long parseLong;
        if (bundle == null) {
            return null;
        }
        Object obj = bundle.get(str);
        if (obj instanceof Long) {
            parseLong = ((Long) obj).longValue();
        } else if (!(obj instanceof String)) {
            return null;
        } else {
            try {
                parseLong = Long.parseLong((String) obj);
            } catch (NumberFormatException e2) {
                return null;
            }
        }
        if (parseLong == 0) {
            return new Date(Long.MAX_VALUE);
        }
        return new Date((parseLong * 1000) + date.getTime());
    }
}
