package com.facebook;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;

/* compiled from: Session */
class bo implements bt {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Fragment f1763a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ bm f1764b;

    bo(bm bmVar, Fragment fragment) {
        this.f1764b = bmVar;
        this.f1763a = fragment;
    }

    public void a(Intent intent, int i) {
        this.f1763a.startActivityForResult(intent, i);
    }

    public Activity a() {
        return this.f1763a.getActivity();
    }
}
