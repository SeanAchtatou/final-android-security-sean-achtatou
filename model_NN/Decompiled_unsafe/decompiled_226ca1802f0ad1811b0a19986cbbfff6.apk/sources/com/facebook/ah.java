package com.facebook;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.actionbarsherlock.view.Menu;

/* compiled from: GetTokenClient */
final class ah implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final Context f1673a;

    /* renamed from: b  reason: collision with root package name */
    final String f1674b;

    /* renamed from: c  reason: collision with root package name */
    final Handler f1675c;
    aj d;
    boolean e;
    Messenger f;

    ah(Context context, String str) {
        Context applicationContext = context.getApplicationContext();
        this.f1673a = applicationContext != null ? applicationContext : context;
        this.f1674b = str;
        this.f1675c = new ai(this);
    }

    /* access modifiers changed from: package-private */
    public void a(aj ajVar) {
        this.d = ajVar;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        Intent intent = new Intent("com.facebook.platform.PLATFORM_SERVICE");
        intent.addCategory("android.intent.category.DEFAULT");
        Intent b2 = ao.b(this.f1673a, intent);
        if (b2 == null) {
            a((Bundle) null);
            return false;
        }
        this.e = true;
        this.f1673a.bindService(b2, this, 1);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.e = false;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.f = new Messenger(iBinder);
        c();
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.f = null;
        this.f1673a.unbindService(this);
        a((Bundle) null);
    }

    private void c() {
        Bundle bundle = new Bundle();
        bundle.putString("com.facebook.platform.extra.APPLICATION_ID", this.f1674b);
        Message obtain = Message.obtain((Handler) null, (int) Menu.CATEGORY_CONTAINER);
        obtain.arg1 = 20121101;
        obtain.setData(bundle);
        obtain.replyTo = new Messenger(this.f1675c);
        try {
            this.f.send(obtain);
        } catch (RemoteException e2) {
            a((Bundle) null);
        }
    }

    /* access modifiers changed from: private */
    public void a(Message message) {
        if (message.what == 65537) {
            Bundle data = message.getData();
            if (data.getString("com.facebook.platform.status.ERROR_TYPE") != null) {
                a((Bundle) null);
            } else {
                a(data);
            }
            this.f1673a.unbindService(this);
        }
    }

    private void a(Bundle bundle) {
        if (this.e) {
            this.e = false;
            aj ajVar = this.d;
            if (ajVar != null) {
                ajVar.a(bundle);
            }
        }
    }
}
