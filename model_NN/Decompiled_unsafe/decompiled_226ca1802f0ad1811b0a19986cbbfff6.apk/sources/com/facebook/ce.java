package com.facebook;

import android.os.Bundle;
import com.facebook.b.v;
import java.util.Date;

/* compiled from: TokenCachingStrategy */
public abstract class ce {
    public abstract Bundle a();

    public abstract void a(Bundle bundle);

    public abstract void b();

    public static boolean b(Bundle bundle) {
        String string;
        if (bundle == null || (string = bundle.getString("com.facebook.TokenCachingStrategy.Token")) == null || string.length() == 0 || bundle.getLong("com.facebook.TokenCachingStrategy.ExpirationDate", 0) == 0) {
            return false;
        }
        return true;
    }

    public static b c(Bundle bundle) {
        v.a(bundle, "bundle");
        if (bundle.containsKey("com.facebook.TokenCachingStrategy.AccessTokenSource")) {
            return (b) bundle.getSerializable("com.facebook.TokenCachingStrategy.AccessTokenSource");
        }
        return bundle.getBoolean("com.facebook.TokenCachingStrategy.IsSSO") ? b.FACEBOOK_APPLICATION_WEB : b.WEB_VIEW;
    }

    static Date a(Bundle bundle, String str) {
        if (bundle == null) {
            return null;
        }
        long j = bundle.getLong(str, Long.MIN_VALUE);
        if (j != Long.MIN_VALUE) {
            return new Date(j);
        }
        return null;
    }

    static void a(Bundle bundle, String str, Date date) {
        bundle.putLong(str, date.getTime());
    }
}
