package com.facebook;

import android.content.Context;
import com.facebook.b.b;
import com.facebook.b.j;
import com.facebook.b.n;
import com.facebook.b.u;
import com.facebook.c.c;
import com.facebook.c.g;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: Response */
public class bc {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f1742a = (!bc.class.desiredAssertionStatus());
    private static b h;

    /* renamed from: b  reason: collision with root package name */
    private final HttpURLConnection f1743b;

    /* renamed from: c  reason: collision with root package name */
    private final com.facebook.c.b f1744c;
    private final g<com.facebook.c.b> d;
    private final boolean e;
    private final ac f;
    private final ap g;

    bc(ap apVar, HttpURLConnection httpURLConnection, com.facebook.c.b bVar, boolean z) {
        this.g = apVar;
        this.f1743b = httpURLConnection;
        this.f1744c = bVar;
        this.d = null;
        this.e = z;
        this.f = null;
    }

    bc(ap apVar, HttpURLConnection httpURLConnection, g<com.facebook.c.b> gVar, boolean z) {
        this.g = apVar;
        this.f1743b = httpURLConnection;
        this.f1744c = null;
        this.d = gVar;
        this.e = z;
        this.f = null;
    }

    bc(ap apVar, HttpURLConnection httpURLConnection, ac acVar) {
        this.g = apVar;
        this.f1743b = httpURLConnection;
        this.f1744c = null;
        this.d = null;
        this.e = false;
        this.f = acVar;
    }

    public final ac a() {
        return this.f;
    }

    public final com.facebook.c.b b() {
        return this.f1744c;
    }

    public final <T extends com.facebook.c.b> T a(Class cls) {
        if (this.f1744c == null) {
            return null;
        }
        if (cls != null) {
            return this.f1744c.a(cls);
        }
        throw new NullPointerException("Must pass in a valid interface that extends GraphObject");
    }

    public ap c() {
        return this.g;
    }

    public ap a(be beVar) {
        String str;
        bf a2;
        if (this.f1744c == null || (a2 = ((bd) this.f1744c.a(bd.class)).a()) == null) {
            str = null;
        } else if (beVar == be.NEXT) {
            str = a2.a();
        } else {
            str = a2.b();
        }
        if (u.a(str)) {
            return null;
        }
        if (str != null && str.equals(this.g.e())) {
            return null;
        }
        try {
            return new ap(this.g.b(), new URL(str));
        } catch (MalformedURLException e2) {
            return null;
        }
    }

    public String toString() {
        String str;
        try {
            Object[] objArr = new Object[1];
            objArr[0] = Integer.valueOf(this.f1743b != null ? this.f1743b.getResponseCode() : 200);
            str = String.format("%d", objArr);
        } catch (IOException e2) {
            str = "unknown";
        }
        return "{Response: " + " responseCode: " + str + ", graphObject: " + this.f1744c + ", error: " + this.f + ", isFromCache:" + this.e + "}";
    }

    public final boolean d() {
        return this.e;
    }

    static b e() {
        Context j;
        if (h == null && (j = bg.j()) != null) {
            h = new b(j, "ResponseCache", new j());
        }
        return h;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.bc.a(java.io.InputStream, java.net.HttpURLConnection, com.facebook.ba, boolean):java.util.List<com.facebook.bc>
     arg types: [java.io.InputStream, ?[OBJECT, ARRAY], com.facebook.ba, int]
     candidates:
      com.facebook.bc.a(java.lang.String, java.net.HttpURLConnection, com.facebook.ba, boolean):java.util.List<com.facebook.bc>
      com.facebook.bc.a(java.net.HttpURLConnection, java.util.List<com.facebook.ap>, java.lang.Object, boolean):java.util.List<com.facebook.bc>
      com.facebook.bc.a(java.io.InputStream, java.net.HttpURLConnection, com.facebook.ba, boolean):java.util.List<com.facebook.bc> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.bc.a(java.io.InputStream, java.net.HttpURLConnection, com.facebook.ba, boolean):java.util.List<com.facebook.bc>
     arg types: [java.io.InputStream, java.net.HttpURLConnection, com.facebook.ba, int]
     candidates:
      com.facebook.bc.a(java.lang.String, java.net.HttpURLConnection, com.facebook.ba, boolean):java.util.List<com.facebook.bc>
      com.facebook.bc.a(java.net.HttpURLConnection, java.util.List<com.facebook.ap>, java.lang.Object, boolean):java.util.List<com.facebook.bc>
      com.facebook.bc.a(java.io.InputStream, java.net.HttpURLConnection, com.facebook.ba, boolean):java.util.List<com.facebook.bc> */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x006b, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0074, code lost:
        com.facebook.b.u.a((java.io.Closeable) null);
        r0 = r3;
        r7 = r1;
        r1 = null;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007d, code lost:
        com.facebook.b.u.a((java.io.Closeable) null);
        r0 = r3;
        r7 = r1;
        r1 = null;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0085, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0086, code lost:
        com.facebook.b.u.a((java.io.Closeable) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0089, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005d A[Catch:{ z -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2, all -> 0x00f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0073 A[ExcHandler: JSONException (e org.json.JSONException), Splitter:B:12:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x007c A[ExcHandler: IOException (e java.io.IOException), Splitter:B:12:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0085 A[ExcHandler: all (r0v19 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:12:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x008a A[SYNTHETIC, Splitter:B:38:0x008a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.util.List<com.facebook.bc> a(java.net.HttpURLConnection r8, com.facebook.ba r9) {
        /*
            r2 = 0
            r6 = 1
            r5 = 0
            boolean r0 = r9 instanceof com.facebook.b.a
            if (r0 == 0) goto L_0x00ff
            r0 = r9
            com.facebook.b.a r0 = (com.facebook.b.a) r0
            com.facebook.b.b r3 = e()
            java.lang.String r1 = r0.k()
            boolean r4 = com.facebook.b.u.a(r1)
            if (r4 == 0) goto L_0x0026
            int r4 = r9.size()
            if (r4 != r6) goto L_0x0044
            com.facebook.ap r1 = r9.get(r5)
            java.lang.String r1 = r1.e()
        L_0x0026:
            boolean r0 = r0.l()
            if (r0 != 0) goto L_0x00f9
            if (r3 == 0) goto L_0x00f9
            boolean r0 = com.facebook.b.u.a(r1)
            if (r0 != 0) goto L_0x00f9
            java.io.InputStream r2 = r3.a(r1)     // Catch:{ z -> 0x006a, JSONException -> 0x0073, IOException -> 0x007c, all -> 0x0085 }
            if (r2 == 0) goto L_0x004e
            r0 = 0
            r4 = 1
            java.util.List r0 = a(r2, r0, r9, r4)     // Catch:{ z -> 0x00f5, JSONException -> 0x0073, IOException -> 0x007c, all -> 0x0085 }
            com.facebook.b.u.a(r2)
        L_0x0043:
            return r0
        L_0x0044:
            com.facebook.al r4 = com.facebook.al.REQUESTS
            java.lang.String r5 = "ResponseCache"
            java.lang.String r6 = "Not using cache for cacheable request because no key was specified"
            com.facebook.b.n.a(r4, r5, r6)
            goto L_0x0026
        L_0x004e:
            com.facebook.b.u.a(r2)
            r0 = r3
            r7 = r1
            r1 = r2
            r2 = r7
        L_0x0055:
            int r3 = r8.getResponseCode()     // Catch:{ z -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2 }
            r4 = 400(0x190, float:5.6E-43)
            if (r3 < r4) goto L_0x008a
            java.io.InputStream r1 = r8.getErrorStream()     // Catch:{ z -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2 }
        L_0x0061:
            r0 = 0
            java.util.List r0 = a(r1, r8, r9, r0)     // Catch:{ z -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2 }
            com.facebook.b.u.a(r1)
            goto L_0x0043
        L_0x006a:
            r0 = move-exception
            r0 = r2
        L_0x006c:
            com.facebook.b.u.a(r0)
            r2 = r1
            r1 = r0
            r0 = r3
            goto L_0x0055
        L_0x0073:
            r0 = move-exception
            com.facebook.b.u.a(r2)
            r0 = r3
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x0055
        L_0x007c:
            r0 = move-exception
            com.facebook.b.u.a(r2)
            r0 = r3
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x0055
        L_0x0085:
            r0 = move-exception
            com.facebook.b.u.a(r2)
            throw r0
        L_0x008a:
            java.io.InputStream r1 = r8.getInputStream()     // Catch:{ z -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2 }
            if (r0 == 0) goto L_0x0061
            if (r2 == 0) goto L_0x0061
            if (r1 == 0) goto L_0x0061
            java.io.InputStream r0 = r0.a(r2, r1)     // Catch:{ z -> 0x009c, JSONException -> 0x00b4, IOException -> 0x00d2 }
            if (r0 == 0) goto L_0x0061
            r1 = r0
            goto L_0x0061
        L_0x009c:
            r0 = move-exception
            com.facebook.al r2 = com.facebook.al.REQUESTS     // Catch:{ all -> 0x00f0 }
            java.lang.String r3 = "Response"
            java.lang.String r4 = "Response <Error>: %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00f0 }
            r6 = 0
            r5[r6] = r0     // Catch:{ all -> 0x00f0 }
            com.facebook.b.n.a(r2, r3, r4, r5)     // Catch:{ all -> 0x00f0 }
            java.util.List r0 = a(r9, r8, r0)     // Catch:{ all -> 0x00f0 }
            com.facebook.b.u.a(r1)
            goto L_0x0043
        L_0x00b4:
            r0 = move-exception
            com.facebook.al r2 = com.facebook.al.REQUESTS     // Catch:{ all -> 0x00f0 }
            java.lang.String r3 = "Response"
            java.lang.String r4 = "Response <Error>: %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00f0 }
            r6 = 0
            r5[r6] = r0     // Catch:{ all -> 0x00f0 }
            com.facebook.b.n.a(r2, r3, r4, r5)     // Catch:{ all -> 0x00f0 }
            com.facebook.z r2 = new com.facebook.z     // Catch:{ all -> 0x00f0 }
            r2.<init>(r0)     // Catch:{ all -> 0x00f0 }
            java.util.List r0 = a(r9, r8, r2)     // Catch:{ all -> 0x00f0 }
            com.facebook.b.u.a(r1)
            goto L_0x0043
        L_0x00d2:
            r0 = move-exception
            com.facebook.al r2 = com.facebook.al.REQUESTS     // Catch:{ all -> 0x00f0 }
            java.lang.String r3 = "Response"
            java.lang.String r4 = "Response <Error>: %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00f0 }
            r6 = 0
            r5[r6] = r0     // Catch:{ all -> 0x00f0 }
            com.facebook.b.n.a(r2, r3, r4, r5)     // Catch:{ all -> 0x00f0 }
            com.facebook.z r2 = new com.facebook.z     // Catch:{ all -> 0x00f0 }
            r2.<init>(r0)     // Catch:{ all -> 0x00f0 }
            java.util.List r0 = a(r9, r8, r2)     // Catch:{ all -> 0x00f0 }
            com.facebook.b.u.a(r1)
            goto L_0x0043
        L_0x00f0:
            r0 = move-exception
            com.facebook.b.u.a(r1)
            throw r0
        L_0x00f5:
            r0 = move-exception
            r0 = r2
            goto L_0x006c
        L_0x00f9:
            r0 = r3
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x0055
        L_0x00ff:
            r0 = r2
            r1 = r2
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.bc.a(java.net.HttpURLConnection, com.facebook.ba):java.util.List");
    }

    static List<bc> a(InputStream inputStream, HttpURLConnection httpURLConnection, ba baVar, boolean z) {
        String a2 = u.a(inputStream);
        n.a(al.INCLUDE_RAW_RESPONSES, "Response", "Response (raw)\n  Size: %d\n  Response:\n%s\n", Integer.valueOf(a2.length()), a2);
        return a(a2, httpURLConnection, baVar, z);
    }

    static List<bc> a(String str, HttpURLConnection httpURLConnection, ba baVar, boolean z) {
        List<bc> a2 = a(httpURLConnection, baVar, new JSONTokener(str).nextValue(), z);
        n.a(al.REQUESTS, "Response", "Response\n  Id: %s\n  Size: %d\n  Responses:\n%s\n", baVar.b(), Integer.valueOf(str.length()), a2);
        return a2;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private static List<bc> a(HttpURLConnection httpURLConnection, List<ap> list, Object obj, boolean z) {
        JSONArray jSONArray;
        if (f1742a || httpURLConnection != null || z) {
            int size = list.size();
            ArrayList arrayList = new ArrayList(size);
            if (size == 1) {
                ap apVar = list.get(0);
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("body", obj);
                    jSONObject.put("code", httpURLConnection != null ? httpURLConnection.getResponseCode() : 200);
                    JSONArray jSONArray2 = new JSONArray();
                    jSONArray2.put(jSONObject);
                    jSONArray = jSONArray2;
                } catch (JSONException e2) {
                    arrayList.add(new bc(apVar, httpURLConnection, new ac(httpURLConnection, e2)));
                    jSONArray = obj;
                } catch (IOException e3) {
                    arrayList.add(new bc(apVar, httpURLConnection, new ac(httpURLConnection, e3)));
                }
                if ((jSONArray instanceof JSONArray) || ((JSONArray) jSONArray).length() != size) {
                    throw new z("Unexpected number of results");
                }
                JSONArray jSONArray3 = (JSONArray) jSONArray;
                for (int i = 0; i < jSONArray3.length(); i++) {
                    ap apVar2 = list.get(i);
                    try {
                        arrayList.add(a(apVar2, httpURLConnection, jSONArray3.get(i), z, obj));
                    } catch (JSONException e4) {
                        arrayList.add(new bc(apVar2, httpURLConnection, new ac(httpURLConnection, e4)));
                    } catch (z e5) {
                        arrayList.add(new bc(apVar2, httpURLConnection, new ac(httpURLConnection, e5)));
                    }
                }
                return arrayList;
            }
            jSONArray = obj;
            if (jSONArray instanceof JSONArray) {
            }
            throw new z("Unexpected number of results");
        }
        throw new AssertionError();
    }

    private static bc a(ap apVar, HttpURLConnection httpURLConnection, Object obj, boolean z, Object obj2) {
        bg b2;
        if (obj instanceof JSONObject) {
            JSONObject jSONObject = (JSONObject) obj;
            ac a2 = ac.a(jSONObject, obj2, httpURLConnection);
            if (a2 != null) {
                if (a2.b() == 190 && (b2 = apVar.b()) != null) {
                    b2.h();
                }
                return new bc(apVar, httpURLConnection, a2);
            }
            Object a3 = u.a(jSONObject, "body", "FACEBOOK_NON_JSON_RESULT");
            if (a3 instanceof JSONObject) {
                return new bc(apVar, httpURLConnection, c.a((JSONObject) a3), z);
            }
            if (a3 instanceof JSONArray) {
                return new bc(apVar, httpURLConnection, c.a((JSONArray) a3, com.facebook.c.b.class), z);
            }
            obj = JSONObject.NULL;
        }
        if (obj == JSONObject.NULL) {
            return new bc(apVar, httpURLConnection, (com.facebook.c.b) null, z);
        }
        throw new z("Got unexpected object type in response, class: " + obj.getClass().getSimpleName());
    }

    static List<bc> a(List<ap> list, HttpURLConnection httpURLConnection, z zVar) {
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            arrayList.add(new bc(list.get(i), httpURLConnection, new ac(httpURLConnection, zVar)));
        }
        return arrayList;
    }
}
