package com.facebook.b;

import java.io.File;
import java.io.FilenameFilter;

/* compiled from: FileLruCache */
final class g implements FilenameFilter {
    g() {
    }

    public boolean accept(File file, String str) {
        return str.startsWith("buffer");
    }
}
