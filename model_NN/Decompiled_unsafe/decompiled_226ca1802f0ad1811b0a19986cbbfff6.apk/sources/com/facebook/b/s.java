package com.facebook.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.facebook.bg;

/* compiled from: SessionTracker */
class s extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f1733a;

    private s(q qVar) {
        this.f1733a = qVar;
    }

    public void onReceive(Context context, Intent intent) {
        bg i;
        if ("com.facebook.sdk.ACTIVE_SESSION_SET".equals(intent.getAction()) && (i = bg.i()) != null) {
            i.a(this.f1733a.f1731b);
        }
    }
}
