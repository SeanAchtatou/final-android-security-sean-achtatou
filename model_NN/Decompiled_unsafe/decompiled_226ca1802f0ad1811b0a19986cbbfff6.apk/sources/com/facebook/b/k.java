package com.facebook.b;

import java.io.File;

/* compiled from: FileLruCache */
final class k implements Comparable<k> {

    /* renamed from: a  reason: collision with root package name */
    private final File f1720a;

    /* renamed from: b  reason: collision with root package name */
    private final long f1721b;

    k(File file) {
        this.f1720a = file;
        this.f1721b = file.lastModified();
    }

    /* access modifiers changed from: package-private */
    public File a() {
        return this.f1720a;
    }

    /* access modifiers changed from: package-private */
    public long b() {
        return this.f1721b;
    }

    /* renamed from: a */
    public int compareTo(k kVar) {
        if (b() < kVar.b()) {
            return -1;
        }
        if (b() > kVar.b()) {
            return 1;
        }
        return a().compareTo(kVar.a());
    }

    public boolean equals(Object obj) {
        return (obj instanceof k) && compareTo((k) obj) == 0;
    }
}
