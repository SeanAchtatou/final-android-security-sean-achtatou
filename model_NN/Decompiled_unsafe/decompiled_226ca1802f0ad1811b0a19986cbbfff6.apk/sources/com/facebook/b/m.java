package com.facebook.b;

import com.facebook.al;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: FileLruCache */
final class m {
    static void a(OutputStream outputStream, JSONObject jSONObject) {
        byte[] bytes = jSONObject.toString().getBytes();
        outputStream.write(0);
        outputStream.write((bytes.length >> 16) & 255);
        outputStream.write((bytes.length >> 8) & 255);
        outputStream.write((bytes.length >> 0) & 255);
        outputStream.write(bytes);
    }

    static JSONObject a(InputStream inputStream) {
        int i = 0;
        if (inputStream.read() != 0) {
            return null;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < 3; i3++) {
            int read = inputStream.read();
            if (read == -1) {
                n.a(al.CACHE, b.f1705a, "readHeader: stream.read returned -1 while reading header size");
                return null;
            }
            i2 = (i2 << 8) + (read & 255);
        }
        byte[] bArr = new byte[i2];
        while (i < bArr.length) {
            int read2 = inputStream.read(bArr, i, bArr.length - i);
            if (read2 < 1) {
                n.a(al.CACHE, b.f1705a, "readHeader: stream.read stopped at " + Integer.valueOf(i) + " when expected " + bArr.length);
                return null;
            }
            i += read2;
        }
        try {
            Object nextValue = new JSONTokener(new String(bArr)).nextValue();
            if (nextValue instanceof JSONObject) {
                return (JSONObject) nextValue;
            }
            n.a(al.CACHE, b.f1705a, "readHeader: expected JSONObject, got " + nextValue.getClass().getCanonicalName());
            return null;
        } catch (JSONException e) {
            throw new IOException(e.getMessage());
        }
    }
}
