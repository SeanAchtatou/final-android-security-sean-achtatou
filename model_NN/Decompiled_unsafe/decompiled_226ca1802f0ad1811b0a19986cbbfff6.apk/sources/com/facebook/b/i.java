package com.facebook.b;

import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: FileLruCache */
final class i extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    final InputStream f1716a;

    /* renamed from: b  reason: collision with root package name */
    final OutputStream f1717b;

    i(InputStream inputStream, OutputStream outputStream) {
        this.f1716a = inputStream;
        this.f1717b = outputStream;
    }

    public int available() {
        return this.f1716a.available();
    }

    public void close() {
        try {
            this.f1716a.close();
        } finally {
            this.f1717b.close();
        }
    }

    public void mark(int i) {
        throw new UnsupportedOperationException();
    }

    public boolean markSupported() {
        return false;
    }

    public int read(byte[] bArr) {
        int read = this.f1716a.read(bArr);
        if (read > 0) {
            this.f1717b.write(bArr, 0, read);
        }
        return read;
    }

    public int read() {
        int read = this.f1716a.read();
        if (read >= 0) {
            this.f1717b.write(read);
        }
        return read;
    }

    public int read(byte[] bArr, int i, int i2) {
        int read = this.f1716a.read(bArr, i, i2);
        if (read > 0) {
            this.f1717b.write(bArr, i, read);
        }
        return read;
    }

    public synchronized void reset() {
        throw new UnsupportedOperationException();
    }

    public long skip(long j) {
        int read;
        byte[] bArr = new byte[1024];
        long j2 = 0;
        while (j2 < j && (read = read(bArr, 0, (int) Math.min(j - j2, (long) bArr.length))) >= 0) {
            j2 += (long) read;
        }
        return j2;
    }
}
