package com.facebook.b;

import java.io.OutputStream;

/* compiled from: FileLruCache */
class h extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    final OutputStream f1714a;

    /* renamed from: b  reason: collision with root package name */
    final l f1715b;

    h(OutputStream outputStream, l lVar) {
        this.f1714a = outputStream;
        this.f1715b = lVar;
    }

    public void close() {
        try {
            this.f1714a.close();
        } finally {
            this.f1715b.a();
        }
    }

    public void flush() {
        this.f1714a.flush();
    }

    public void write(byte[] bArr, int i, int i2) {
        this.f1714a.write(bArr, i, i2);
    }

    public void write(byte[] bArr) {
        this.f1714a.write(bArr);
    }

    public void write(int i) {
        this.f1714a.write(i);
    }
}
