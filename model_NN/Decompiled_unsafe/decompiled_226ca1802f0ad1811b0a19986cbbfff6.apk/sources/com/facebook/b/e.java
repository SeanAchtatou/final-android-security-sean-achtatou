package com.facebook.b;

import java.io.File;
import java.io.FilenameFilter;

/* compiled from: FileLruCache */
class e {

    /* renamed from: a  reason: collision with root package name */
    private static final FilenameFilter f1712a = new f();

    /* renamed from: b  reason: collision with root package name */
    private static final FilenameFilter f1713b = new g();

    static void a(File file) {
        for (File delete : file.listFiles(b())) {
            delete.delete();
        }
    }

    static FilenameFilter a() {
        return f1712a;
    }

    static FilenameFilter b() {
        return f1713b;
    }

    static File b(File file) {
        return new File(file, "buffer" + Long.valueOf(b.f1706b.incrementAndGet()).toString());
    }
}
