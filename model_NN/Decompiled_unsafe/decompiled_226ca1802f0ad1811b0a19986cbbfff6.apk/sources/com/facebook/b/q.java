package com.facebook.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.content.r;
import com.facebook.bg;
import com.facebook.bu;

/* compiled from: SessionTracker */
public class q {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public bg f1730a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final bu f1731b;

    /* renamed from: c  reason: collision with root package name */
    private final BroadcastReceiver f1732c;
    private final r d;
    private boolean e;

    public q(Context context, bu buVar) {
        this(context, buVar, null);
    }

    q(Context context, bu buVar, bg bgVar) {
        this(context, buVar, bgVar, true);
    }

    public q(Context context, bu buVar, bg bgVar, boolean z) {
        this.e = false;
        this.f1731b = new t(this, buVar);
        this.f1730a = bgVar;
        this.f1732c = new s(this);
        this.d = r.a(context);
        if (z) {
            c();
        }
    }

    public bg a() {
        return this.f1730a == null ? bg.i() : this.f1730a;
    }

    public bg b() {
        bg a2 = a();
        if (a2 == null || !a2.a()) {
            return null;
        }
        return a2;
    }

    public void a(bg bgVar) {
        if (bgVar != null) {
            if (this.f1730a == null) {
                bg i = bg.i();
                if (i != null) {
                    i.b(this.f1731b);
                }
                this.d.a(this.f1732c);
            } else {
                this.f1730a.b(this.f1731b);
            }
            this.f1730a = bgVar;
            this.f1730a.a(this.f1731b);
        } else if (this.f1730a != null) {
            this.f1730a.b(this.f1731b);
            this.f1730a = null;
            f();
            if (a() != null) {
                a().a(this.f1731b);
            }
        }
    }

    public void c() {
        if (!this.e) {
            if (this.f1730a == null) {
                f();
            }
            if (a() != null) {
                a().a(this.f1731b);
            }
            this.e = true;
        }
    }

    public void d() {
        if (this.e) {
            bg a2 = a();
            if (a2 != null) {
                a2.b(this.f1731b);
            }
            this.d.a(this.f1732c);
            this.e = false;
        }
    }

    public boolean e() {
        return this.e;
    }

    private void f() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.facebook.sdk.ACTIVE_SESSION_SET");
        intentFilter.addAction("com.facebook.sdk.ACTIVE_SESSION_UNSET");
        this.d.a(this.f1732c, intentFilter);
    }
}
