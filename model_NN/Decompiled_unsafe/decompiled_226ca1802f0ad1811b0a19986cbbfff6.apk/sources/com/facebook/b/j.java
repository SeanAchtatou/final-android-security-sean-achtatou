package com.facebook.b;

/* compiled from: FileLruCache */
public final class j {

    /* renamed from: a  reason: collision with root package name */
    private int f1718a = 1048576;

    /* renamed from: b  reason: collision with root package name */
    private int f1719b = 1024;

    /* access modifiers changed from: package-private */
    public int a() {
        return this.f1718a;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.f1719b;
    }
}
