package com.facebook.b;

import java.util.Collection;

/* compiled from: ServerProtocol */
public final class o {

    /* renamed from: a  reason: collision with root package name */
    public static final Collection<String> f1725a = u.a("service_disabled", "AndroidAuthKillSwitchException");

    /* renamed from: b  reason: collision with root package name */
    public static final Collection<String> f1726b = u.a("access_denied", "OAuthAccessDeniedException");
}
