package com.facebook.b;

import android.util.Log;
import com.facebook.al;
import com.facebook.cb;
import java.util.HashMap;
import java.util.Map;

/* compiled from: Logger */
public class n {

    /* renamed from: a  reason: collision with root package name */
    private static final HashMap<String, String> f1722a = new HashMap<>();

    /* renamed from: b  reason: collision with root package name */
    private final al f1723b;

    /* renamed from: c  reason: collision with root package name */
    private final String f1724c;
    private StringBuilder d;
    private int e = 3;

    public static synchronized void a(String str, String str2) {
        synchronized (n.class) {
            f1722a.put(str, str2);
        }
    }

    public static synchronized void a(String str) {
        synchronized (n.class) {
            if (!cb.b(al.INCLUDE_ACCESS_TOKENS)) {
                a(str, "ACCESS_TOKEN_REMOVED");
            }
        }
    }

    public static void a(al alVar, String str, String str2) {
        a(alVar, 3, str, str2);
    }

    public static void a(al alVar, String str, String str2, Object... objArr) {
        if (cb.b(alVar)) {
            a(alVar, 3, str, String.format(str2, objArr));
        }
    }

    public static void a(al alVar, int i, String str, String str2) {
        if (cb.b(alVar)) {
            String d2 = d(str2);
            if (!str.startsWith("FacebookSDK.")) {
                str = "FacebookSDK." + str;
            }
            Log.println(i, str, d2);
            if (alVar == al.DEVELOPER_ERRORS) {
                new Exception().printStackTrace();
            }
        }
    }

    private static synchronized String d(String str) {
        synchronized (n.class) {
            for (Map.Entry next : f1722a.entrySet()) {
                str = str.replace((CharSequence) next.getKey(), (CharSequence) next.getValue());
            }
        }
        return str;
    }

    public n(al alVar, String str) {
        v.a(str, "tag");
        this.f1723b = alVar;
        this.f1724c = "FacebookSDK." + str;
        this.d = new StringBuilder();
    }

    public void a() {
        b(this.d.toString());
        this.d = new StringBuilder();
    }

    public void b(String str) {
        a(this.f1723b, this.e, this.f1724c, str);
    }

    public void c(String str) {
        if (b()) {
            this.d.append(str);
        }
    }

    public void a(String str, Object... objArr) {
        if (b()) {
            this.d.append(String.format(str, objArr));
        }
    }

    public void a(String str, Object obj) {
        a("  %s:\t%s\n", str, obj);
    }

    private boolean b() {
        return cb.b(this.f1723b);
    }
}
