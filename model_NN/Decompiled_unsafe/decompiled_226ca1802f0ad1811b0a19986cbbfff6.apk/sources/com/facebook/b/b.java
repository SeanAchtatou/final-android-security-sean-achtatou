package com.facebook.b;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import com.facebook.al;
import com.facebook.cb;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: FileLruCache */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    static final String f1705a = b.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final AtomicLong f1706b = new AtomicLong();

    /* renamed from: c  reason: collision with root package name */
    private final String f1707c;
    private final j d;
    private final File e;
    private boolean f;
    private final Object g = new Object();

    public b(Context context, String str, j jVar) {
        this.f1707c = str;
        this.d = jVar;
        this.e = new File(context.getCacheDir(), str);
        this.e.mkdirs();
        e.a(this.e);
    }

    public InputStream a(String str) {
        return a(str, (String) null);
    }

    public InputStream a(String str, String str2) {
        File file = new File(this.e, u.b(str));
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file), FragmentTransaction.TRANSIT_EXIT_MASK);
            try {
                JSONObject a2 = m.a(bufferedInputStream);
                if (a2 == null) {
                    return null;
                }
                String optString = a2.optString("key");
                if (optString == null || !optString.equals(str)) {
                    bufferedInputStream.close();
                    return null;
                }
                String optString2 = a2.optString("tag", null);
                if ((str2 != null || optString2 == null) && (str2 == null || str2.equals(optString2))) {
                    long time = new Date().getTime();
                    n.a(al.CACHE, f1705a, "Setting lastModified to " + Long.valueOf(time) + " for " + file.getName());
                    file.setLastModified(time);
                    return bufferedInputStream;
                }
                bufferedInputStream.close();
                return null;
            } finally {
                bufferedInputStream.close();
            }
        } catch (IOException e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public OutputStream b(String str) {
        return b(str, null);
    }

    public OutputStream b(String str, String str2) {
        File b2 = e.b(this.e);
        b2.delete();
        if (!b2.createNewFile()) {
            throw new IOException("Could not create file at " + b2.getAbsolutePath());
        }
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new h(new FileOutputStream(b2), new c(this, str, b2)), FragmentTransaction.TRANSIT_EXIT_MASK);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("key", str);
                if (!u.a(str2)) {
                    jSONObject.put("tag", str2);
                }
                m.a(bufferedOutputStream, jSONObject);
                return bufferedOutputStream;
            } catch (JSONException e2) {
                n.a(al.CACHE, 5, f1705a, "Error creating JSON header for cache file: " + e2);
                throw new IOException(e2.getMessage());
            } catch (Throwable th) {
                bufferedOutputStream.close();
                throw th;
            }
        } catch (FileNotFoundException e3) {
            n.a(al.CACHE, 5, f1705a, "Error creating buffer output stream: " + e3);
            throw new IOException(e3.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, File file) {
        if (!file.renameTo(new File(this.e, u.b(str)))) {
            file.delete();
        }
        b();
    }

    public InputStream a(String str, InputStream inputStream) {
        return new i(inputStream, b(str));
    }

    public String toString() {
        return "{FileLruCache: tag:" + this.f1707c + " file:" + this.e.getName() + "}";
    }

    private void b() {
        synchronized (this.g) {
            if (!this.f) {
                this.f = true;
                cb.a().execute(new d(this));
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        try {
            n.a(al.CACHE, f1705a, "trim started");
            PriorityQueue priorityQueue = new PriorityQueue();
            File[] listFiles = this.e.listFiles(e.a());
            int length = listFiles.length;
            long j = 0;
            long j2 = 0;
            int i = 0;
            while (i < length) {
                File file = listFiles[i];
                k kVar = new k(file);
                priorityQueue.add(kVar);
                n.a(al.CACHE, f1705a, "  trim considering time=" + Long.valueOf(kVar.b()) + " name=" + kVar.a().getName());
                i++;
                j++;
                j2 = file.length() + j2;
            }
            long j3 = j2;
            long j4 = j;
            while (true) {
                if (j3 > ((long) this.d.a()) || j4 > ((long) this.d.b())) {
                    File a2 = ((k) priorityQueue.remove()).a();
                    n.a(al.CACHE, f1705a, "  trim removing " + a2.getName());
                    j3 -= a2.length();
                    long j5 = j4 - 1;
                    a2.delete();
                    j4 = j5;
                } else {
                    synchronized (this.g) {
                        this.f = false;
                        this.g.notifyAll();
                    }
                    return;
                }
            }
        } catch (Throwable th) {
            synchronized (this.g) {
                this.f = false;
                this.g.notifyAll();
                throw th;
            }
        }
    }
}
