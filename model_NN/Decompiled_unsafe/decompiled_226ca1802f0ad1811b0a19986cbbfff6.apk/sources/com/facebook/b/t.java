package com.facebook.b;

import com.facebook.bg;
import com.facebook.bu;
import com.facebook.bz;

/* compiled from: SessionTracker */
class t implements bu {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f1734a;

    /* renamed from: b  reason: collision with root package name */
    private final bu f1735b;

    public t(q qVar, bu buVar) {
        this.f1734a = qVar;
        this.f1735b = buVar;
    }

    public void a(bg bgVar, bz bzVar, Exception exc) {
        if (this.f1735b != null && this.f1734a.e()) {
            this.f1735b.a(bgVar, bzVar, exc);
        }
        if (bgVar == this.f1734a.f1730a && bzVar.b()) {
            this.f1734a.a((bg) null);
        }
    }
}
