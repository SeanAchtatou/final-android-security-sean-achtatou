package com.facebook;

/* compiled from: FacebookRequestError */
class af {

    /* renamed from: a  reason: collision with root package name */
    private final int f1670a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1671b;

    private af(int i, int i2) {
        this.f1670a = i;
        this.f1671b = i2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i) {
        return this.f1670a <= i && i <= this.f1671b;
    }
}
