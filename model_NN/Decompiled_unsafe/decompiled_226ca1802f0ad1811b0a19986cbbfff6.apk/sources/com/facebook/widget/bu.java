package com.facebook.widget;

import com.facebook.aw;
import com.facebook.bc;
import com.facebook.bg;
import com.facebook.c.i;

/* compiled from: UserSettingsFragment */
class bu implements aw {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bg f1911a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ UserSettingsFragment f1912b;

    bu(UserSettingsFragment userSettingsFragment, bg bgVar) {
        this.f1912b = userSettingsFragment;
        this.f1911a = bgVar;
    }

    public void a(i iVar, bc bcVar) {
        if (this.f1911a == this.f1912b.a()) {
            i unused = this.f1912b.e = iVar;
            this.f1912b.d();
        }
        if (bcVar.a() != null) {
            this.f1912b.f1850b.a(bcVar.a().e());
        }
    }
}
