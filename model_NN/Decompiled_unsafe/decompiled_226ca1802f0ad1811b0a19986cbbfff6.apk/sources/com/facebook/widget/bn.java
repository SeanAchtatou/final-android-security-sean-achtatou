package com.facebook.widget;

import com.facebook.c.h;

/* compiled from: PlacePickerFragment */
class bn extends ay {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PlacePickerFragment f1901a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private bn(PlacePickerFragment placePickerFragment) {
        super(placePickerFragment);
        this.f1901a = placePickerFragment;
    }

    /* synthetic */ bn(PlacePickerFragment placePickerFragment, bk bkVar) {
        this(placePickerFragment);
    }

    public void a(f<h> fVar) {
        super.a(fVar);
        this.f1888c.a(new bo(this));
    }

    /* access modifiers changed from: protected */
    public void a(r<h> rVar, bs<h> bsVar) {
        super.a(rVar, bsVar);
        if (bsVar != null && !rVar.d()) {
            this.f1901a.n();
            if (bsVar.g()) {
                rVar.a(bsVar.a() ? 2000 : 0);
            }
        }
    }
}
