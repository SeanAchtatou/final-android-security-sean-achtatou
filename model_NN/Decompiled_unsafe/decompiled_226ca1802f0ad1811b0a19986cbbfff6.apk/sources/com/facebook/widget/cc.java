package com.facebook.widget;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.facebook.a.i;
import com.facebook.ac;
import com.facebook.ag;
import com.facebook.b.u;
import com.facebook.y;

/* compiled from: WebDialog */
class cc extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bw f1924a;

    private cc(bw bwVar) {
        this.f1924a = bwVar;
    }

    /* synthetic */ cc(bw bwVar, bx bxVar) {
        this(bwVar);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        int i;
        u.b("FacebookSDK.WebDialog", "Redirect URL: " + str);
        if (str.startsWith("fbconnect://success")) {
            Bundle b2 = i.b(str);
            String string = b2.getString("error");
            if (string == null) {
                string = b2.getString("error_type");
            }
            String string2 = b2.getString("error_msg");
            if (string2 == null) {
                string2 = b2.getString("error_description");
            }
            String string3 = b2.getString("error_code");
            if (!u.a(string3)) {
                try {
                    i = Integer.parseInt(string3);
                } catch (NumberFormatException e) {
                    i = -1;
                }
            } else {
                i = -1;
            }
            if (u.a(string) && u.a(string2) && i == -1) {
                this.f1924a.a(b2);
            } else if (string == null || (!string.equals("access_denied") && !string.equals("OAuthAccessDeniedException"))) {
                this.f1924a.a(new ag(new ac(i, string, string2), string2));
            } else {
                this.f1924a.a();
            }
            this.f1924a.dismiss();
            return true;
        } else if (str.startsWith("fbconnect://cancel")) {
            this.f1924a.a();
            this.f1924a.dismiss();
            return true;
        } else if (str.contains("touch")) {
            return false;
        } else {
            this.f1924a.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        }
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        this.f1924a.a(new y(str, i, str2));
        this.f1924a.dismiss();
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
        this.f1924a.a(new y(null, -11, null));
        sslErrorHandler.cancel();
        this.f1924a.dismiss();
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        u.b("FacebookSDK.WebDialog", "Webview loading URL: " + str);
        super.onPageStarted(webView, str, bitmap);
        if (!this.f1924a.h) {
            this.f1924a.d.show();
        }
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (!this.f1924a.h) {
            this.f1924a.d.dismiss();
        }
        this.f1924a.f.setBackgroundColor(0);
        this.f1924a.f1916c.setVisibility(0);
        this.f1924a.e.setVisibility(0);
    }
}
