package com.facebook.widget;

/* compiled from: WorkQueue */
class ch implements cg {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f1930a = (!ce.class.desiredAssertionStatus());

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ce f1931b;

    /* renamed from: c  reason: collision with root package name */
    private final Runnable f1932c;
    private ch d;
    private ch e;
    private boolean f;

    ch(ce ceVar, Runnable runnable) {
        this.f1931b = ceVar;
        this.f1932c = runnable;
    }

    public boolean a() {
        synchronized (this.f1931b.f1926b) {
            if (c()) {
                return false;
            }
            ch unused = this.f1931b.f1927c = a(this.f1931b.f1927c);
            return true;
        }
    }

    public void b() {
        synchronized (this.f1931b.f1926b) {
            if (!c()) {
                ch unused = this.f1931b.f1927c = a(this.f1931b.f1927c);
                ch unused2 = this.f1931b.f1927c = a(this.f1931b.f1927c, true);
            }
        }
    }

    public boolean c() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public Runnable d() {
        return this.f1932c;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f = z;
    }

    /* access modifiers changed from: package-private */
    public ch a(ch chVar, boolean z) {
        ch chVar2;
        if (!f1930a && this.d != null) {
            throw new AssertionError();
        } else if (f1930a || this.e == null) {
            if (chVar == null) {
                this.e = this;
                this.d = this;
                chVar2 = this;
            } else {
                this.d = chVar;
                this.e = chVar.e;
                ch chVar3 = this.d;
                this.e.d = this;
                chVar3.e = this;
                chVar2 = chVar;
            }
            if (z) {
                return this;
            }
            return chVar2;
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    public ch a(ch chVar) {
        if (!f1930a && this.d == null) {
            throw new AssertionError();
        } else if (f1930a || this.e != null) {
            if (chVar == this) {
                if (this.d == this) {
                    chVar = null;
                } else {
                    chVar = this.d;
                }
            }
            this.d.e = this.e;
            this.e.d = this.d;
            this.e = null;
            this.d = null;
            return chVar;
        } else {
            throw new AssertionError();
        }
    }
}
