package com.facebook.widget;

import android.content.Context;

/* compiled from: ImageDownloader */
class z implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f1962a;

    /* renamed from: b  reason: collision with root package name */
    private ac f1963b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1964c;

    z(Context context, ac acVar, boolean z) {
        this.f1962a = context;
        this.f1963b = acVar;
        this.f1964c = z;
    }

    public void run() {
        x.b(this.f1963b, this.f1962a, this.f1964c);
    }
}
