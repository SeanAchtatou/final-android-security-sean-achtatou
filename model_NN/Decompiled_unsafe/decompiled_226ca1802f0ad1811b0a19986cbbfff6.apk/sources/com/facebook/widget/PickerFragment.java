package com.facebook.widget;

import android.app.Activity;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.AlphaAnimation;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.a.e;
import com.facebook.a.g;
import com.facebook.a.h;
import com.facebook.ap;
import com.facebook.b.q;
import com.facebook.bg;
import com.facebook.c.b;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

public abstract class PickerFragment<T extends b> extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    HashSet<String> f1842a = new HashSet<>();

    /* renamed from: b  reason: collision with root package name */
    f<T> f1843b;

    /* renamed from: c  reason: collision with root package name */
    private final int f1844c;
    /* access modifiers changed from: private */
    public bf d;
    private bd e;
    private bg f;
    /* access modifiers changed from: private */
    public be g;
    private ax<T> h;
    private boolean i = true;
    private boolean j = true;
    private ListView k;
    /* access modifiers changed from: private */
    public final Class<T> l;
    private PickerFragment<T>.ay m;
    /* access modifiers changed from: private */
    public PickerFragment<T>.bi n;
    private ProgressBar o;
    private q p;
    private String q;
    private String r;
    private TextView s;
    private Button t;
    private Drawable u;
    private Drawable v;
    private AbsListView.OnScrollListener w = new aw(this);

    /* access modifiers changed from: package-private */
    public abstract ap a(bg bgVar);

    /* access modifiers changed from: package-private */
    public abstract PickerFragment<T>.com/facebook/widget/bh<T> a();

    /* access modifiers changed from: package-private */
    public abstract PickerFragment<T>.ay b();

    /* access modifiers changed from: package-private */
    public abstract PickerFragment<T>.bi c();

    PickerFragment(Class<T> cls, int i2, Bundle bundle) {
        this.l = cls;
        this.f1844c = i2;
        c(bundle);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f1843b = a();
        this.f1843b.a(new ar(this));
    }

    public void onInflate(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        super.onInflate(activity, attributeSet, bundle);
        TypedArray obtainStyledAttributes = activity.obtainStyledAttributes(attributeSet, h.n);
        b(obtainStyledAttributes.getBoolean(0, this.i));
        String string = obtainStyledAttributes.getString(1);
        if (string != null) {
            a(Arrays.asList(string.split(",")));
        }
        this.j = obtainStyledAttributes.getBoolean(2, this.j);
        this.q = obtainStyledAttributes.getString(3);
        this.r = obtainStyledAttributes.getString(4);
        this.u = obtainStyledAttributes.getDrawable(5);
        this.v = obtainStyledAttributes.getDrawable(6);
        obtainStyledAttributes.recycle();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewGroup viewGroup2 = (ViewGroup) layoutInflater.inflate(this.f1844c, viewGroup, false);
        this.k = (ListView) viewGroup2.findViewById(e.com_facebook_picker_list_view);
        this.k.setOnItemClickListener(new as(this));
        this.k.setOnLongClickListener(new at(this));
        this.k.setOnScrollListener(this.w);
        this.k.setAdapter((ListAdapter) this.f1843b);
        this.o = (ProgressBar) viewGroup2.findViewById(e.com_facebook_picker_activity_circle);
        return viewGroup2;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        this.p = new q(getActivity(), new au(this));
        a(bundle);
        this.m = b();
        this.m.a(this.f1843b);
        this.n = c();
        this.n.b(bundle, "com.facebook.android.PickerFragment.Selection");
        if (this.j) {
            a((ViewGroup) getView());
        }
        if (this.o != null && bundle != null) {
            if (bundle.getBoolean("com.facebook.android.PickerFragment.ActivityCircleShown", false)) {
                l();
            } else {
                n();
            }
        }
    }

    public void onDetach() {
        super.onDetach();
        this.k.setOnScrollListener(null);
        this.k.setAdapter((ListAdapter) null);
        this.m.a();
        this.p.d();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        b(bundle);
        this.n.a(bundle, "com.facebook.android.PickerFragment.Selection");
        if (this.o != null) {
            bundle.putBoolean("com.facebook.android.PickerFragment.ActivityCircleShown", this.o.getVisibility() == 0);
        }
    }

    public void setArguments(Bundle bundle) {
        super.setArguments(bundle);
        a(bundle);
    }

    public bf e() {
        return this.d;
    }

    public bg f() {
        return this.p.a();
    }

    public boolean g() {
        return this.i;
    }

    public void b(boolean z) {
        this.i = z;
    }

    public void a(Collection<String> collection) {
        this.f1842a = new HashSet<>();
        if (collection != null) {
            this.f1842a.addAll(collection);
        }
    }

    public String h() {
        if (this.q == null) {
            this.q = d();
        }
        return this.q;
    }

    public String i() {
        if (this.r == null) {
            this.r = k();
        }
        return this.r;
    }

    public void c(boolean z) {
        if (z || !this.m.c()) {
            o();
        }
    }

    public void a(Bundle bundle) {
        c(bundle);
    }

    /* access modifiers changed from: package-private */
    public boolean a(b bVar) {
        if (this.h != null) {
            return this.h.a(bVar);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(Bundle bundle) {
        bundle.putBoolean("com.facebook.widget.PickerFragment.ShowPictures", this.i);
        if (!this.f1842a.isEmpty()) {
            bundle.putString("com.facebook.widget.PickerFragment.ExtraFields", TextUtils.join(",", this.f1842a));
        }
        bundle.putBoolean("com.facebook.widget.PickerFragment.ShowTitleBar", this.j);
        bundle.putString("com.facebook.widget.PickerFragment.TitleText", this.q);
        bundle.putString("com.facebook.widget.PickerFragment.DoneButtonText", this.r);
    }

    /* access modifiers changed from: package-private */
    public void j() {
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public String k() {
        return getString(g.com_facebook_picker_done_button_text);
    }

    /* access modifiers changed from: package-private */
    public void l() {
        if (this.o != null) {
            m();
            this.o.setVisibility(0);
        }
    }

    /* access modifiers changed from: package-private */
    public void m() {
        a(this.o, !this.f1843b.isEmpty() ? 0.25f : 1.0f);
    }

    /* access modifiers changed from: package-private */
    public void n() {
        if (this.o != null) {
            this.o.clearAnimation();
            this.o.setVisibility(4);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(PickerFragment<T>.bi biVar) {
        if (biVar != this.n) {
            this.n = biVar;
            if (this.f1843b != null) {
                this.f1843b.notifyDataSetChanged();
            }
        }
    }

    private static void a(View view, float f2) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f2);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        view.startAnimation(alphaAnimation);
    }

    private void c(Bundle bundle) {
        if (bundle != null) {
            this.i = bundle.getBoolean("com.facebook.widget.PickerFragment.ShowPictures", this.i);
            String string = bundle.getString("com.facebook.widget.PickerFragment.ExtraFields");
            if (string != null) {
                a(Arrays.asList(string.split(",")));
            }
            this.j = bundle.getBoolean("com.facebook.widget.PickerFragment.ShowTitleBar", this.j);
            String string2 = bundle.getString("com.facebook.widget.PickerFragment.TitleText");
            if (string2 != null) {
                this.q = string2;
                if (this.s != null) {
                    this.s.setText(this.q);
                }
            }
            String string3 = bundle.getString("com.facebook.widget.PickerFragment.DoneButtonText");
            if (string3 != null) {
                this.r = string3;
                if (this.t != null) {
                    this.t.setText(this.r);
                }
            }
        }
    }

    private void a(ViewGroup viewGroup) {
        ViewStub viewStub = (ViewStub) viewGroup.findViewById(e.com_facebook_picker_title_bar_stub);
        if (viewStub != null) {
            View inflate = viewStub.inflate();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            layoutParams.addRule(3, e.com_facebook_picker_title_bar);
            this.k.setLayoutParams(layoutParams);
            if (this.u != null) {
                inflate.setBackgroundDrawable(this.u);
            }
            this.t = (Button) viewGroup.findViewById(e.com_facebook_picker_done_button);
            if (this.t != null) {
                this.t.setOnClickListener(new av(this));
                if (i() != null) {
                    this.t.setText(i());
                }
                if (this.v != null) {
                    this.t.setBackgroundDrawable(this.v);
                }
            }
            this.s = (TextView) viewGroup.findViewById(e.com_facebook_picker_title);
            if (this.s != null && h() != null) {
                this.s.setText(h());
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(ListView listView, View view, int i2) {
        this.n.b(this.f1843b.f((b) listView.getItemAtPosition(i2)));
        this.f1843b.notifyDataSetChanged();
        if (this.f != null) {
            this.f.a(this);
        }
    }

    private void o() {
        p();
        ap a2 = a(f());
        if (a2 != null) {
            j();
            this.m.a(a2);
        }
    }

    /* access modifiers changed from: private */
    public void p() {
        boolean z = true;
        if (this.f1843b != null) {
            boolean z2 = !this.n.b();
            if (this.f1843b.isEmpty()) {
                z = false;
            }
            this.m.b();
            this.n.a();
            this.f1843b.notifyDataSetChanged();
            if (z && this.e != null) {
                this.e.a(this);
            }
            if (z2 && this.f != null) {
                this.f.a(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(bs bsVar) {
        int a2;
        if (this.f1843b != null) {
            View childAt = this.k.getChildAt(1);
            int firstVisiblePosition = this.k.getFirstVisiblePosition();
            if (firstVisiblePosition > 0) {
                firstVisiblePosition++;
            }
            o<T> a3 = this.f1843b.a(firstVisiblePosition);
            int top = (childAt == null || a3.a() == p.f1948c) ? 0 : childAt.getTop();
            boolean a4 = this.f1843b.a(bsVar);
            if (!(childAt == null || a3 == null || (a2 = this.f1843b.a(a3.f1944a, a3.f1945b)) == -1)) {
                this.k.setSelectionFromTop(a2, top);
            }
            if (a4 && this.e != null) {
                this.e.a(this);
            }
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        int lastVisiblePosition = this.k.getLastVisiblePosition();
        if (lastVisiblePosition >= 0) {
            this.f1843b.a(this.k.getFirstVisiblePosition(), lastVisiblePosition, 5);
        }
    }
}
