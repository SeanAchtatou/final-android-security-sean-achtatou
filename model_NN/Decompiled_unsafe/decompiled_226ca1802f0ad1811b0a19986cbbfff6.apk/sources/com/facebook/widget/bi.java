package com.facebook.widget;

import android.os.Bundle;

/* compiled from: PickerFragment */
abstract class bi {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ PickerFragment f1895b;

    /* access modifiers changed from: package-private */
    public abstract void a();

    /* access modifiers changed from: package-private */
    public abstract void a(Bundle bundle, String str);

    /* access modifiers changed from: package-private */
    public abstract boolean a(String str);

    /* access modifiers changed from: package-private */
    public abstract void b(Bundle bundle, String str);

    /* access modifiers changed from: package-private */
    public abstract void b(String str);

    /* access modifiers changed from: package-private */
    public abstract boolean b();

    /* access modifiers changed from: package-private */
    public abstract boolean c();

    bi(PickerFragment pickerFragment) {
        this.f1895b = pickerFragment;
    }
}
