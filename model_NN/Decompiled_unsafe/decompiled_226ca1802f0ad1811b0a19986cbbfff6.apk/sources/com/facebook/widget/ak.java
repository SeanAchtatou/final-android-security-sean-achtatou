package com.facebook.widget;

import com.facebook.aw;
import com.facebook.bc;
import com.facebook.bg;
import com.facebook.c.i;

/* compiled from: LoginButton */
class ak implements aw {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bg f1872a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ LoginButton f1873b;

    ak(LoginButton loginButton, bg bgVar) {
        this.f1873b = loginButton;
        this.f1872a = bgVar;
    }

    public void a(i iVar, bc bcVar) {
        if (this.f1872a == this.f1873b.f1841c.b()) {
            i unused = this.f1873b.d = iVar;
            if (this.f1873b.j != null) {
                this.f1873b.j.a(this.f1873b.d);
            }
        }
        if (bcVar.a() != null) {
            this.f1873b.a(bcVar.a().e());
        }
    }
}
