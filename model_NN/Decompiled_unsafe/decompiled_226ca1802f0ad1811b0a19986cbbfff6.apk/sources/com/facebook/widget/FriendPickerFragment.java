package com.facebook.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.a.f;
import com.facebook.a.g;
import com.facebook.a.h;
import com.facebook.ap;
import com.facebook.au;
import com.facebook.bg;
import com.facebook.c.i;
import com.facebook.z;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class FriendPickerFragment extends PickerFragment<i> {

    /* renamed from: c  reason: collision with root package name */
    private String f1838c;
    private boolean d;

    public FriendPickerFragment() {
        this(null);
    }

    @SuppressLint({"ValidFragment"})
    public FriendPickerFragment(Bundle bundle) {
        super(i.class, f.com_facebook_friendpickerfragment, bundle);
        this.d = true;
        c(bundle);
    }

    public void a(String str) {
        this.f1838c = str;
    }

    /*  JADX ERROR: NullPointerException in pass: MethodInvokeVisitor
        java.lang.NullPointerException
        	at jadx.core.dex.visitors.typeinference.TypeCompare.compareObjects(TypeCompare.java:180)
        	at jadx.core.dex.visitors.typeinference.TypeCompare.compareTypes(TypeCompare.java:76)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.isMethodAcceptable(MethodInvokeVisitor.java:323)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.filterApplicableMethods(MethodInvokeVisitor.java:305)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.isOverloadResolved(MethodInvokeVisitor.java:282)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.searchCastTypes(MethodInvokeVisitor.java:222)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:117)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void a(boolean r2) {
        /*
            r1 = this;
            boolean r0 = r1.d
            if (r0 == r2) goto L_0x000d
            r1.d = r2
            com.facebook.widget.bi r0 = r1.c()
            r1.a(r0)
        L_0x000d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.FriendPickerFragment.a(boolean):void");
    }

    public void onInflate(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        super.onInflate(activity, attributeSet, bundle);
        TypedArray obtainStyledAttributes = activity.obtainStyledAttributes(attributeSet, h.l);
        a(obtainStyledAttributes.getBoolean(0, this.d));
        obtainStyledAttributes.recycle();
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        c(bundle);
    }

    /* access modifiers changed from: package-private */
    public void b(Bundle bundle) {
        super.b(bundle);
        bundle.putString("com.facebook.widget.FriendPickerFragment.UserId", this.f1838c);
        bundle.putBoolean("com.facebook.widget.FriendPickerFragment.MultiSelect", this.d);
    }

    /* access modifiers changed from: package-private */
    public PickerFragment<i>.com/facebook/widget/bh<i> a() {
        PickerFragment<i>.com/facebook/widget/bh<i> dVar = new d(this, getActivity());
        dVar.b(true);
        dVar.a(g());
        dVar.a(Arrays.asList("name"));
        dVar.a("name");
        return dVar;
    }

    /* access modifiers changed from: package-private */
    public PickerFragment<i>.ay b() {
        return new e(this, null);
    }

    /* access modifiers changed from: package-private */
    public PickerFragment<i>.bi c() {
        return this.d ? new bc(this) : new bj(this);
    }

    /* access modifiers changed from: package-private */
    public ap a(bg bgVar) {
        if (this.f1843b == null) {
            throw new z("Can't issue requests until Fragment has been created.");
        }
        return a(this.f1838c != null ? this.f1838c : "me", this.f1842a, bgVar);
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return getString(g.com_facebook_choose_friends);
    }

    private ap a(String str, Set<String> set, bg bgVar) {
        ap a2 = ap.a(bgVar, str + "/friends", (au) null);
        HashSet hashSet = new HashSet(set);
        hashSet.addAll(Arrays.asList("id", "name"));
        String e = this.f1843b.e();
        if (e != null) {
            hashSet.add(e);
        }
        Bundle a3 = a2.a();
        a3.putString("fields", TextUtils.join(",", hashSet));
        a2.a(a3);
        return a2;
    }

    private void c(Bundle bundle) {
        if (bundle != null) {
            if (bundle.containsKey("com.facebook.widget.FriendPickerFragment.UserId")) {
                a(bundle.getString("com.facebook.widget.FriendPickerFragment.UserId"));
            }
            a(bundle.getBoolean("com.facebook.widget.FriendPickerFragment.MultiSelect", this.d));
        }
    }
}
