package com.facebook.widget;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: PlacePickerFragment */
class bp implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PlacePickerFragment f1903a;

    private bp(PlacePickerFragment placePickerFragment) {
        this.f1903a = placePickerFragment;
    }

    /* synthetic */ bp(PlacePickerFragment placePickerFragment, bk bkVar) {
        this(placePickerFragment);
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.widget.PlacePickerFragment.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.facebook.widget.PickerFragment.a(android.view.View, float):void
      com.facebook.widget.PlacePickerFragment.a(java.lang.String, boolean):void */
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.f1903a.a(charSequence.toString(), false);
    }

    public void afterTextChanged(Editable editable) {
    }
}
