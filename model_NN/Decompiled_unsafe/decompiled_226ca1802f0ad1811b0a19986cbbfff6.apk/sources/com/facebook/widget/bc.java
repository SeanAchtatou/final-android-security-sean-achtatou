package com.facebook.widget;

import android.os.Bundle;
import android.text.TextUtils;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: PickerFragment */
class bc extends bi {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PickerFragment f1892a;

    /* renamed from: c  reason: collision with root package name */
    private Set<String> f1893c = new HashSet();

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bc(PickerFragment pickerFragment) {
        super(pickerFragment);
        this.f1892a = pickerFragment;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        return str != null && this.f1893c.contains(str);
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (str == null) {
            return;
        }
        if (this.f1893c.contains(str)) {
            this.f1893c.remove(str);
        } else {
            this.f1893c.add(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Bundle bundle, String str) {
        if (!this.f1893c.isEmpty()) {
            bundle.putString(str, TextUtils.join(",", this.f1893c));
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Bundle bundle, String str) {
        String string;
        if (bundle != null && (string = bundle.getString(str)) != null) {
            String[] split = TextUtils.split(string, ",");
            this.f1893c.clear();
            Collections.addAll(this.f1893c, split);
        }
    }

    public void a() {
        this.f1893c.clear();
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.f1893c.isEmpty();
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return true;
    }
}
