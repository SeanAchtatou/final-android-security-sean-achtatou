package com.facebook.widget;

import com.facebook.c.b;

/* compiled from: GraphObjectAdapter */
public class o<T extends b> {

    /* renamed from: a  reason: collision with root package name */
    public String f1944a;

    /* renamed from: b  reason: collision with root package name */
    public T f1945b;

    public o(String str, T t) {
        this.f1944a = str;
        this.f1945b = t;
    }

    public p a() {
        if (this.f1944a == null) {
            return p.ACTIVITY_CIRCLE;
        }
        if (this.f1945b == null) {
            return p.SECTION_HEADER;
        }
        return p.GRAPH_OBJECT;
    }
}
