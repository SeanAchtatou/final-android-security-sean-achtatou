package com.facebook.widget;

import com.facebook.ap;

/* compiled from: PickerFragment */
abstract class ay {

    /* renamed from: b  reason: collision with root package name */
    protected r<T> f1887b;

    /* renamed from: c  reason: collision with root package name */
    protected f<T> f1888c;
    final /* synthetic */ PickerFragment d;

    ay(PickerFragment pickerFragment) {
        this.d = pickerFragment;
    }

    public void a(f<T> fVar) {
        this.f1887b = (r) this.d.getLoaderManager().initLoader(0, null, new az(this));
        this.f1887b.a(new ba(this));
        this.f1888c = fVar;
        this.f1888c.a(this.f1887b.b());
        this.f1888c.a(new bb(this));
    }

    public void a() {
        this.f1888c.a((j) null);
        this.f1888c.a((n) null);
        this.f1887b.a((v) null);
        this.f1887b = null;
        this.f1888c = null;
    }

    public void b() {
        if (this.f1887b != null) {
            this.f1887b.c();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.widget.r.a(com.facebook.ap, boolean):void
     arg types: [com.facebook.ap, int]
     candidates:
      com.facebook.widget.r.a(com.facebook.widget.r, com.facebook.bc):void
      android.support.v4.content.o.a(int, android.support.v4.content.q):void
      com.facebook.widget.r.a(com.facebook.ap, boolean):void */
    public void a(ap apVar) {
        if (this.f1887b != null) {
            this.f1887b.a(apVar, true);
            a(this.f1887b, apVar);
        }
    }

    public boolean c() {
        return !this.f1888c.isEmpty() || this.f1887b.d();
    }

    /* access modifiers changed from: protected */
    public r<T> d() {
        return new r<>(this.d.getActivity(), this.d.l);
    }

    /* access modifiers changed from: protected */
    public void a(r<T> rVar, ap apVar) {
        this.d.l();
    }

    /* access modifiers changed from: protected */
    public void a(r<T> rVar) {
        this.f1888c.a((q) null);
    }

    /* access modifiers changed from: protected */
    public void a(r<T> rVar, bs<T> bsVar) {
        this.d.a(bsVar);
    }
}
