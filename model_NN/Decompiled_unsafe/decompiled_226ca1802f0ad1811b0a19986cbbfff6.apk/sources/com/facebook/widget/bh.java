package com.facebook.widget;

import android.content.Context;
import android.widget.CheckBox;
import com.facebook.c.b;

/* compiled from: PickerFragment */
abstract class bh<U extends b> extends f<T> {

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ PickerFragment f1894c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bh(PickerFragment pickerFragment, Context context) {
        super(context);
        this.f1894c = pickerFragment;
    }

    /* access modifiers changed from: package-private */
    public boolean b(String str) {
        return this.f1894c.n.a(str);
    }

    /* access modifiers changed from: package-private */
    public void a(CheckBox checkBox, boolean z) {
        checkBox.setChecked(z);
        checkBox.setVisibility((z || this.f1894c.n.c()) ? 0 : 8);
    }
}
