package com.facebook.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.widget.Button;
import com.facebook.a.b;
import com.facebook.a.c;
import com.facebook.a.d;
import com.facebook.a.g;
import com.facebook.a.h;
import com.facebook.ap;
import com.facebook.b.q;
import com.facebook.b.u;
import com.facebook.bg;
import com.facebook.c.i;
import com.facebook.z;

public class LoginButton extends Button {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1839a = LoginButton.class.getName();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public String f1840b = null;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public q f1841c;
    /* access modifiers changed from: private */
    public i d = null;
    private bg e = null;
    /* access modifiers changed from: private */
    public boolean f;
    private boolean g;
    private String h;
    private String i;
    /* access modifiers changed from: private */
    public aq j;
    /* access modifiers changed from: private */
    public Fragment k;
    /* access modifiers changed from: private */
    public am l = new am();

    public LoginButton(Context context) {
        super(context);
        a(context);
        a();
    }

    public LoginButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (attributeSet.getStyleAttribute() == 0) {
            setTextColor(getResources().getColor(b.com_facebook_loginview_text_color));
            setTextSize(0, getResources().getDimension(c.com_facebook_loginview_text_size));
            setPadding(getResources().getDimensionPixelSize(c.com_facebook_loginview_padding_left), getResources().getDimensionPixelSize(c.com_facebook_loginview_padding_top), getResources().getDimensionPixelSize(c.com_facebook_loginview_padding_right), getResources().getDimensionPixelSize(c.com_facebook_loginview_padding_bottom));
            setWidth(getResources().getDimensionPixelSize(c.com_facebook_loginview_width));
            setHeight(getResources().getDimensionPixelSize(c.com_facebook_loginview_height));
            setGravity(17);
            if (isInEditMode()) {
                setBackgroundColor(getResources().getColor(b.com_facebook_blue));
                this.h = "Log in";
            } else {
                setBackgroundResource(d.com_facebook_loginbutton_blue);
            }
        }
        a(attributeSet);
        if (!isInEditMode()) {
            a(context);
        }
    }

    public LoginButton(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(attributeSet);
        a(context);
    }

    public void a(bg bgVar) {
        this.f1841c.a(bgVar);
        c();
        b();
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        a();
    }

    private void a() {
        setOnClickListener(new an(this, null));
        b();
        if (!isInEditMode()) {
            this.f1841c = new q(getContext(), new al(this, null), null, false);
            c();
        }
    }

    public void a(Fragment fragment) {
        this.k = fragment;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f1841c != null && !this.f1841c.e()) {
            this.f1841c.c();
            c();
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f1841c != null) {
            this.f1841c.d();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(am amVar) {
        this.l = amVar;
    }

    private void a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, h.m);
        this.f = obtainStyledAttributes.getBoolean(0, true);
        this.g = obtainStyledAttributes.getBoolean(1, true);
        this.h = obtainStyledAttributes.getString(2);
        this.i = obtainStyledAttributes.getString(3);
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.f1841c == null || this.f1841c.b() == null) {
            setText(this.h != null ? this.h : getResources().getString(g.com_facebook_loginview_log_in_button));
        } else {
            setText(this.i != null ? this.i : getResources().getString(g.com_facebook_loginview_log_out_button));
        }
    }

    private boolean a(Context context) {
        if (context == null) {
            return false;
        }
        bg i2 = bg.i();
        if (i2 != null) {
            return i2.a();
        }
        if (u.a(context) == null || bg.a(context) == null) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.g) {
            bg b2 = this.f1841c.b();
            if (b2 == null) {
                this.d = null;
                if (this.j != null) {
                    this.j.a(this.d);
                }
            } else if (b2 != this.e) {
                ap.b(ap.a(b2, new ak(this, b2)));
                this.e = b2;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Exception exc) {
        if (this.l.d == null) {
            return;
        }
        if (exc instanceof z) {
            this.l.d.a((z) exc);
        } else {
            this.l.d.a(new z(exc));
        }
    }
}
