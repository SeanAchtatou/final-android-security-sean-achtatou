package com.facebook.widget;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import com.facebook.b.b;
import com.facebook.b.j;
import com.facebook.b.u;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/* compiled from: UrlRedirectCache */
class bt {

    /* renamed from: a  reason: collision with root package name */
    static final String f1908a = bt.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static final String f1909b = (f1908a + "_Redirect");

    /* renamed from: c  reason: collision with root package name */
    private static volatile b f1910c;

    bt() {
    }

    static synchronized b a(Context context) {
        b bVar;
        synchronized (bt.class) {
            if (f1910c == null) {
                f1910c = new b(context.getApplicationContext(), f1908a, new j());
            }
            bVar = f1910c;
        }
        return bVar;
    }

    static URL a(Context context, URL url) {
        InputStreamReader inputStreamReader;
        InputStreamReader inputStreamReader2;
        URL url2;
        boolean z = false;
        if (url == null) {
            return null;
        }
        String url3 = url.toString();
        try {
            b a2 = a(context);
            String str = url3;
            inputStreamReader2 = null;
            while (true) {
                try {
                    InputStream a3 = a2.a(str, f1909b);
                    if (a3 == null) {
                        break;
                    }
                    inputStreamReader = new InputStreamReader(a3);
                    try {
                        char[] cArr = new char[NotificationCompat.FLAG_HIGH_PRIORITY];
                        StringBuilder sb = new StringBuilder();
                        while (true) {
                            int read = inputStreamReader.read(cArr, 0, cArr.length);
                            if (read <= 0) {
                                break;
                            }
                            sb.append(cArr, 0, read);
                        }
                        u.a(inputStreamReader);
                        str = sb.toString();
                        inputStreamReader2 = inputStreamReader;
                        z = true;
                    } catch (MalformedURLException e) {
                        u.a(inputStreamReader);
                        return null;
                    } catch (IOException e2) {
                        u.a(inputStreamReader);
                        return null;
                    } catch (Throwable th) {
                        inputStreamReader2 = inputStreamReader;
                        th = th;
                        u.a(inputStreamReader2);
                        throw th;
                    }
                } catch (MalformedURLException e3) {
                    inputStreamReader = inputStreamReader2;
                    u.a(inputStreamReader);
                    return null;
                } catch (IOException e4) {
                    inputStreamReader = inputStreamReader2;
                    u.a(inputStreamReader);
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    u.a(inputStreamReader2);
                    throw th;
                }
            }
            if (z) {
                url2 = new URL(str);
            } else {
                url2 = null;
            }
            u.a(inputStreamReader2);
            return url2;
        } catch (MalformedURLException e5) {
            inputStreamReader = null;
            u.a(inputStreamReader);
            return null;
        } catch (IOException e6) {
            inputStreamReader = null;
            u.a(inputStreamReader);
            return null;
        } catch (Throwable th3) {
            th = th3;
            inputStreamReader2 = null;
            u.a(inputStreamReader2);
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0028, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0029, code lost:
        r4 = r1;
        r1 = null;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        com.facebook.b.u.a((java.io.Closeable) null);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:3:0x0006] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(android.content.Context r5, java.net.URL r6, java.net.URL r7) {
        /*
            if (r6 == 0) goto L_0x0004
            if (r7 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            r0 = 0
            com.facebook.b.b r1 = a(r5)     // Catch:{ IOException -> 0x0023, all -> 0x0028 }
            java.lang.String r2 = r6.toString()     // Catch:{ IOException -> 0x0023, all -> 0x0028 }
            java.lang.String r3 = com.facebook.widget.bt.f1909b     // Catch:{ IOException -> 0x0023, all -> 0x0028 }
            java.io.OutputStream r0 = r1.b(r2, r3)     // Catch:{ IOException -> 0x0023, all -> 0x0028 }
            java.lang.String r1 = r7.toString()     // Catch:{ IOException -> 0x0023, all -> 0x0030 }
            byte[] r1 = r1.getBytes()     // Catch:{ IOException -> 0x0023, all -> 0x0030 }
            r0.write(r1)     // Catch:{ IOException -> 0x0023, all -> 0x0030 }
            com.facebook.b.u.a(r0)
            goto L_0x0004
        L_0x0023:
            r1 = move-exception
            com.facebook.b.u.a(r0)
            goto L_0x0004
        L_0x0028:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x002c:
            com.facebook.b.u.a(r1)
            throw r0
        L_0x0030:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.bt.a(android.content.Context, java.net.URL, java.net.URL):void");
    }
}
