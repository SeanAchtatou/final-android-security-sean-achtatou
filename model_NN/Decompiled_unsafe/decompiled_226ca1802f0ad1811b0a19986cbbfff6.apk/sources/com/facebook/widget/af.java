package com.facebook.widget;

import android.content.Context;
import com.facebook.b.v;
import java.net.URL;

/* compiled from: ImageRequest */
class af {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1863a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public URL f1864b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ag f1865c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public Object e;

    af(Context context, URL url) {
        v.a(url, "imageUrl");
        this.f1863a = context;
        this.f1864b = url;
    }

    /* access modifiers changed from: package-private */
    public af a(ag agVar) {
        this.f1865c = agVar;
        return this;
    }

    /* access modifiers changed from: package-private */
    public af a(Object obj) {
        this.e = obj;
        return this;
    }

    /* access modifiers changed from: package-private */
    public af a(boolean z) {
        this.d = z;
        return this;
    }

    /* access modifiers changed from: package-private */
    public ad a() {
        return new ad(this);
    }
}
