package com.facebook.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.facebook.a.c;
import com.facebook.a.d;
import com.facebook.a.h;
import com.facebook.al;
import com.facebook.b.n;
import com.facebook.z;
import java.net.MalformedURLException;

public class ProfilePictureView extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    public static final String f1846a = ProfilePictureView.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private String f1847b;

    /* renamed from: c  reason: collision with root package name */
    private int f1848c = 0;
    private int d = 0;
    private boolean e = true;
    private Bitmap f;
    private ImageView g;
    private int h = -1;
    private ad i;
    private br j;

    public ProfilePictureView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
        a(attributeSet);
    }

    public ProfilePictureView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
        a(attributeSet);
    }

    public final void a(int i2) {
        switch (i2) {
            case -4:
            case -3:
            case -2:
            case -1:
                break;
            default:
                throw new IllegalArgumentException("Must use a predefined preset size");
        }
        this.h = i2;
        requestLayout();
    }

    public final boolean a() {
        return this.e;
    }

    public final String b() {
        return this.f1847b;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        boolean z = true;
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        boolean z2 = false;
        int size = View.MeasureSpec.getSize(i3);
        int size2 = View.MeasureSpec.getSize(i2);
        if (View.MeasureSpec.getMode(i3) != 1073741824 && layoutParams.height == -2) {
            size = c(true);
            i3 = View.MeasureSpec.makeMeasureSpec(size, 1073741824);
            z2 = true;
        }
        if (View.MeasureSpec.getMode(i2) == 1073741824 || layoutParams.width != -2) {
            z = z2;
            i4 = size2;
        } else {
            i4 = c(true);
            i2 = View.MeasureSpec.makeMeasureSpec(i4, 1073741824);
        }
        if (z) {
            setMeasuredDimension(i4, size);
            measureChildren(i2, i3);
            return;
        }
        super.onMeasure(i2, i3);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        a(false);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putParcelable("ProfilePictureView_superState", onSaveInstanceState);
        bundle.putString("ProfilePictureView_profileId", this.f1847b);
        bundle.putInt("ProfilePictureView_presetSize", this.h);
        bundle.putBoolean("ProfilePictureView_isCropped", this.e);
        bundle.putParcelable("ProfilePictureView_bitmap", this.f);
        bundle.putInt("ProfilePictureView_width", this.d);
        bundle.putInt("ProfilePictureView_height", this.f1848c);
        bundle.putBoolean("ProfilePictureView_refresh", this.i != null);
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable.getClass() != Bundle.class) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        Bundle bundle = (Bundle) parcelable;
        super.onRestoreInstanceState(bundle.getParcelable("ProfilePictureView_superState"));
        this.f1847b = bundle.getString("ProfilePictureView_profileId");
        this.h = bundle.getInt("ProfilePictureView_presetSize");
        this.e = bundle.getBoolean("ProfilePictureView_isCropped");
        this.d = bundle.getInt("ProfilePictureView_width");
        this.f1848c = bundle.getInt("ProfilePictureView_height");
        a((Bitmap) bundle.getParcelable("ProfilePictureView_bitmap"));
        if (bundle.getBoolean("ProfilePictureView_refresh")) {
            a(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.i = null;
    }

    private void a(Context context) {
        removeAllViews();
        this.g = new ImageView(context);
        this.g.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.g.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        addView(this.g);
    }

    private void a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, h.p);
        a(obtainStyledAttributes.getInt(0, -1));
        this.e = obtainStyledAttributes.getBoolean(1, true);
        obtainStyledAttributes.recycle();
    }

    private void a(boolean z) {
        boolean d2 = d();
        if (this.f1847b == null || this.f1847b.length() == 0 || (this.d == 0 && this.f1848c == 0)) {
            c();
        } else if (d2 || z) {
            b(true);
        }
    }

    private void c() {
        a(BitmapFactory.decodeResource(getResources(), a() ? d.com_facebook_profile_picture_blank_square : d.com_facebook_profile_picture_blank_portrait));
    }

    private void a(Bitmap bitmap) {
        if (this.g != null && bitmap != null) {
            this.f = bitmap;
            this.g.setImageBitmap(bitmap);
        }
    }

    private void b(boolean z) {
        try {
            ad a2 = new af(getContext(), ad.a(this.f1847b, this.d, this.f1848c)).a(z).a(this).a((ag) new bq(this)).a();
            if (this.i != null) {
                x.b(this.i);
            }
            this.i = a2;
            x.a(a2);
        } catch (MalformedURLException e2) {
            n.a(al.REQUESTS, 6, f1846a, e2.toString());
        }
    }

    /* access modifiers changed from: private */
    public void a(ah ahVar) {
        if (ahVar.a() == this.i) {
            this.i = null;
            Bitmap c2 = ahVar.c();
            Exception b2 = ahVar.b();
            if (b2 != null) {
                br brVar = this.j;
                if (brVar != null) {
                    brVar.a(new z("Error in downloading profile picture for profileId: " + b(), b2));
                } else {
                    n.a(al.REQUESTS, 6, f1846a, b2.toString());
                }
            } else if (c2 != null) {
                a(c2);
                if (ahVar.d()) {
                    b(false);
                }
            }
        }
    }

    private boolean d() {
        int i2;
        boolean z = false;
        int height = getHeight();
        int width = getWidth();
        if (width >= 1 && height >= 1) {
            int c2 = c(false);
            if (c2 != 0) {
                height = c2;
            } else {
                c2 = width;
            }
            if (c2 <= height) {
                i2 = a() ? c2 : 0;
            } else {
                c2 = a() ? height : 0;
                i2 = height;
            }
            if (!(c2 == this.d && i2 == this.f1848c)) {
                z = true;
            }
            this.d = c2;
            this.f1848c = i2;
        }
        return z;
    }

    private int c(boolean z) {
        int i2;
        switch (this.h) {
            case -4:
                i2 = c.com_facebook_profilepictureview_preset_size_large;
                break;
            case -3:
                i2 = c.com_facebook_profilepictureview_preset_size_normal;
                break;
            case -2:
                i2 = c.com_facebook_profilepictureview_preset_size_small;
                break;
            case -1:
                if (z) {
                    i2 = c.com_facebook_profilepictureview_preset_size_normal;
                    break;
                } else {
                    return 0;
                }
            default:
                return 0;
        }
        return getResources().getDimensionPixelSize(i2);
    }
}
