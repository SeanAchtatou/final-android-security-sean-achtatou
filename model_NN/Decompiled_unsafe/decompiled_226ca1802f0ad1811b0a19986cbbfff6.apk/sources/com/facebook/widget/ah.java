package com.facebook.widget;

import android.graphics.Bitmap;

/* compiled from: ImageResponse */
class ah {

    /* renamed from: a  reason: collision with root package name */
    private ad f1866a;

    /* renamed from: b  reason: collision with root package name */
    private Exception f1867b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1868c;
    private Bitmap d;

    ah(ad adVar, Exception exc, boolean z, Bitmap bitmap) {
        this.f1866a = adVar;
        this.f1867b = exc;
        this.d = bitmap;
        this.f1868c = z;
    }

    /* access modifiers changed from: package-private */
    public ad a() {
        return this.f1866a;
    }

    /* access modifiers changed from: package-private */
    public Exception b() {
        return this.f1867b;
    }

    /* access modifiers changed from: package-private */
    public Bitmap c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.f1868c;
    }
}
