package com.facebook.widget;

import android.content.Context;
import com.facebook.a.f;
import com.facebook.c.i;

/* compiled from: FriendPickerFragment */
class d extends PickerFragment<i>.com/facebook/widget/bh<i> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FriendPickerFragment f1933a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(FriendPickerFragment friendPickerFragment, Context context) {
        super(friendPickerFragment, context);
        this.f1933a = friendPickerFragment;
    }

    /* access modifiers changed from: protected */
    public int a(i iVar) {
        return f.com_facebook_picker_list_row;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return com.facebook.a.d.com_facebook_profile_default_icon;
    }
}
