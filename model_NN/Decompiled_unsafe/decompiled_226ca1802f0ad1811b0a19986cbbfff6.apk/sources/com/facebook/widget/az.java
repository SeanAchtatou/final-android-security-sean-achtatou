package com.facebook.widget;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.o;
import com.facebook.z;

/* compiled from: PickerFragment */
class az implements LoaderManager.LoaderCallbacks<bs<T>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ay f1889a;

    az(ay ayVar) {
        this.f1889a = ayVar;
    }

    public o<bs<T>> onCreateLoader(int i, Bundle bundle) {
        return this.f1889a.d();
    }

    /* renamed from: a */
    public void onLoadFinished(o<bs<T>> oVar, bs<T> bsVar) {
        if (oVar != this.f1889a.f1887b) {
            throw new z("Received callback for unknown loader.");
        }
        this.f1889a.a((r) oVar, bsVar);
    }

    public void onLoaderReset(o<bs<T>> oVar) {
        if (oVar != this.f1889a.f1887b) {
            throw new z("Received callback for unknown loader.");
        }
        this.f1889a.a((r) oVar);
    }
}
