package com.facebook.widget;

import android.app.Activity;
import android.content.res.TypedArray;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.facebook.a.e;
import com.facebook.a.f;
import com.facebook.a.g;
import com.facebook.ap;
import com.facebook.b.u;
import com.facebook.bg;
import com.facebook.c.h;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;

public class PlacePickerFragment extends PickerFragment<h> {

    /* renamed from: c  reason: collision with root package name */
    private Location f1845c;
    private int d;
    private int e;
    private String f;
    private Timer g;
    private boolean h;
    private boolean i;
    private EditText j;

    public PlacePickerFragment() {
        this(null);
    }

    public PlacePickerFragment(Bundle bundle) {
        super(h.class, f.com_facebook_placepickerfragment, bundle);
        this.d = 1000;
        this.e = 100;
        this.i = true;
        c(bundle);
    }

    public void a(Location location) {
        this.f1845c = location;
    }

    public void a(int i2) {
        this.d = i2;
    }

    public void b(int i2) {
        this.e = i2;
    }

    public void a(String str) {
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.f = str;
        if (this.j != null) {
            this.j.setText(str);
        }
    }

    public void a(String str, boolean z) {
        if (z || !u.a(this.f, str)) {
            if (TextUtils.isEmpty(str)) {
                str = null;
            }
            this.f = str;
            this.h = true;
            if (this.g == null) {
                this.g = o();
            }
        }
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        c(bundle);
    }

    public void onInflate(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        super.onInflate(activity, attributeSet, bundle);
        TypedArray obtainStyledAttributes = activity.obtainStyledAttributes(attributeSet, com.facebook.a.h.o);
        a(obtainStyledAttributes.getInt(0, this.d));
        b(obtainStyledAttributes.getInt(1, this.e));
        if (obtainStyledAttributes.hasValue(1)) {
            a(obtainStyledAttributes.getString(2));
        }
        this.i = obtainStyledAttributes.getBoolean(3, this.i);
        obtainStyledAttributes.recycle();
    }

    public void onActivityCreated(Bundle bundle) {
        ViewStub viewStub;
        super.onActivityCreated(bundle);
        ViewGroup viewGroup = (ViewGroup) getView();
        if (this.i && (viewStub = (ViewStub) viewGroup.findViewById(e.com_facebook_placepickerfragment_search_box_stub)) != null) {
            this.j = (EditText) viewStub.inflate();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            layoutParams.addRule(3, e.search_box);
            ((ListView) viewGroup.findViewById(e.com_facebook_picker_list_view)).setLayoutParams(layoutParams);
            if (viewGroup.findViewById(e.com_facebook_picker_title_bar) != null) {
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
                layoutParams2.addRule(3, e.com_facebook_picker_title_bar);
                this.j.setLayoutParams(layoutParams2);
            }
            this.j.addTextChangedListener(new bp(this, null));
            if (!TextUtils.isEmpty(this.f)) {
                this.j.setText(this.f);
            }
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (this.j != null) {
            ((InputMethodManager) getActivity().getSystemService("input_method")).showSoftInput(this.j, 1);
        }
    }

    public void onDetach() {
        super.onDetach();
        if (this.j != null) {
            ((InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.j.getWindowToken(), 0);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Bundle bundle) {
        super.b(bundle);
        bundle.putInt("com.facebook.widget.PlacePickerFragment.RadiusInMeters", this.d);
        bundle.putInt("com.facebook.widget.PlacePickerFragment.ResultsLimit", this.e);
        bundle.putString("com.facebook.widget.PlacePickerFragment.SearchText", this.f);
        bundle.putParcelable("com.facebook.widget.PlacePickerFragment.Location", this.f1845c);
        bundle.putBoolean("com.facebook.widget.PlacePickerFragment.ShowSearchBox", this.i);
    }

    /* access modifiers changed from: package-private */
    public void j() {
        this.h = false;
    }

    /* access modifiers changed from: package-private */
    public ap a(bg bgVar) {
        return a(this.f1845c, this.d, this.e, this.f, this.f1842a, bgVar);
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return getString(g.com_facebook_nearby);
    }

    /* access modifiers changed from: package-private */
    public PickerFragment<h>.com/facebook/widget/bh<h> a() {
        PickerFragment<h>.com/facebook/widget/bh<h> bkVar = new bk(this, getActivity());
        bkVar.b(false);
        bkVar.a(g());
        return bkVar;
    }

    /* access modifiers changed from: package-private */
    public PickerFragment<h>.ay b() {
        return new bn(this, null);
    }

    /* access modifiers changed from: package-private */
    public PickerFragment<h>.bi c() {
        return new bj(this);
    }

    private ap a(Location location, int i2, int i3, String str, Set<String> set, bg bgVar) {
        ap a2 = ap.a(bgVar, location, i2, i3, str, null);
        HashSet hashSet = new HashSet(set);
        hashSet.addAll(Arrays.asList("id", "name", "location", "category", "were_here_count"));
        String e2 = this.f1843b.e();
        if (e2 != null) {
            hashSet.add(e2);
        }
        Bundle a3 = a2.a();
        a3.putString("fields", TextUtils.join(",", hashSet));
        a2.a(a3);
        return a2;
    }

    private void c(Bundle bundle) {
        if (bundle != null) {
            a(bundle.getInt("com.facebook.widget.PlacePickerFragment.RadiusInMeters", this.d));
            b(bundle.getInt("com.facebook.widget.PlacePickerFragment.ResultsLimit", this.e));
            if (bundle.containsKey("com.facebook.widget.PlacePickerFragment.SearchText")) {
                a(bundle.getString("com.facebook.widget.PlacePickerFragment.SearchText"));
            }
            if (bundle.containsKey("com.facebook.widget.PlacePickerFragment.Location")) {
                a((Location) bundle.getParcelable("com.facebook.widget.PlacePickerFragment.Location"));
            }
            this.i = bundle.getBoolean("com.facebook.widget.PlacePickerFragment.ShowSearchBox", this.i);
        }
    }

    private Timer o() {
        Timer timer = new Timer();
        timer.schedule(new bl(this), 0, 2000);
        return timer;
    }

    /* access modifiers changed from: private */
    public void p() {
        if (this.h) {
            new Handler(Looper.getMainLooper()).post(new bm(this));
            return;
        }
        this.g.cancel();
        this.g = null;
    }
}
