package com.facebook.widget;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.facebook.b.q;
import com.facebook.bg;
import com.facebook.bz;

/* compiled from: FacebookFragment */
class a extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private q f1852a;

    a() {
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        this.f1852a = new q(getActivity(), new c(this));
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        this.f1852a.a().a(getActivity(), i, i2, intent);
    }

    public void onDestroy() {
        super.onDestroy();
        this.f1852a.d();
    }

    /* access modifiers changed from: protected */
    public void a(bz bzVar, Exception exc) {
    }

    /* access modifiers changed from: protected */
    public final bg a() {
        if (this.f1852a != null) {
            return this.f1852a.a();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        if (this.f1852a == null || this.f1852a.b() == null) {
            return false;
        }
        return true;
    }
}
