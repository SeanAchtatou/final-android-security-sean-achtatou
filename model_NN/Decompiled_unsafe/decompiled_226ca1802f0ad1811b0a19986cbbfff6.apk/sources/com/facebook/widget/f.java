package com.facebook.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.facebook.a.d;
import com.facebook.a.e;
import com.facebook.c.b;
import com.facebook.c.c;
import com.facebook.z;
import java.net.URL;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: GraphObjectAdapter */
class f<T extends b> extends BaseAdapter implements SectionIndexer {

    /* renamed from: b  reason: collision with root package name */
    static final /* synthetic */ boolean f1935b = (!f.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, ad> f1936a = new HashMap();

    /* renamed from: c  reason: collision with root package name */
    private final LayoutInflater f1937c;
    private List<String> d = new ArrayList();
    private Map<String, ArrayList<T>> e = new HashMap();
    private Map<String, T> f = new HashMap();
    private boolean g;
    /* access modifiers changed from: private */
    public List<String> h;
    private String i;
    private boolean j;
    private boolean k;
    private k<T> l;
    private j m;
    private q<T> n;
    private Context o;
    private Map<String, ah> p = new HashMap();
    private ArrayList<String> q = new ArrayList<>();
    private n r;

    public f(Context context) {
        this.o = context;
        this.f1937c = LayoutInflater.from(context);
    }

    public void a(List<String> list) {
        this.h = list;
    }

    public void a(String str) {
        this.i = str;
    }

    public boolean b() {
        return this.j;
    }

    public void a(boolean z) {
        this.j = z;
    }

    public boolean c() {
        return this.k;
    }

    public void b(boolean z) {
        this.k = z;
    }

    public void a(j jVar) {
        this.m = jVar;
    }

    public void a(n nVar) {
        this.r = nVar;
    }

    public boolean a(q qVar) {
        if (this.n == qVar) {
            return false;
        }
        if (this.n != null) {
            this.n.f();
        }
        this.n = qVar;
        d();
        return true;
    }

    public void d() {
        g();
        notifyDataSetChanged();
    }

    public void a(int i2, int i3, int i4) {
        ad adVar;
        if (i3 >= i2) {
            for (int i5 = i3; i5 >= 0; i5--) {
                o a2 = a(i5);
                if (!(a2.f1945b == null || (adVar = this.f1936a.get(f(a2.f1945b))) == null)) {
                    x.c(adVar);
                }
            }
            int min = Math.min(i3 + i4, getCount() - 1);
            ArrayList arrayList = new ArrayList();
            for (int max = Math.max(0, i2 - i4); max < i2; max++) {
                o a3 = a(max);
                if (a3.f1945b != null) {
                    arrayList.add(a3.f1945b);
                }
            }
            for (int i6 = i3 + 1; i6 <= min; i6++) {
                o a4 = a(i6);
                if (a4.f1945b != null) {
                    arrayList.add(a4.f1945b);
                }
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                b bVar = (b) it.next();
                URL e2 = e(bVar);
                String f2 = f(bVar);
                boolean remove = this.q.remove(f2);
                this.q.add(f2);
                if (!remove) {
                    a(f2, e2, (ImageView) null);
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected java.lang.String b(T r4) {
        /*
            r3 = this;
            r0 = 0
            java.lang.String r1 = r3.i
            if (r1 == 0) goto L_0x001f
            java.lang.String r0 = r3.i
            java.lang.Object r0 = r4.a(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x001f
            int r1 = r0.length()
            if (r1 <= 0) goto L_0x001f
            r1 = 0
            r2 = 1
            java.lang.String r0 = r0.substring(r1, r2)
            java.lang.String r0 = r0.toUpperCase()
        L_0x001f:
            if (r0 == 0) goto L_0x0022
        L_0x0021:
            return r0
        L_0x0022:
            java.lang.String r0 = ""
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.f.b(com.facebook.c.b):java.lang.String");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected java.lang.CharSequence c(T r2) {
        /*
            r1 = this;
            java.lang.String r0 = "name"
            java.lang.Object r0 = r2.a(r0)
            java.lang.String r0 = (java.lang.String) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.f.c(com.facebook.c.b):java.lang.CharSequence");
    }

    /* access modifiers changed from: protected */
    public CharSequence d(T t) {
        return null;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected java.net.URL e(T r4) {
        /*
            r3 = this;
            r1 = 0
            java.lang.String r0 = "picture"
            java.lang.Object r0 = r4.a(r0)
            boolean r2 = r0 instanceof java.lang.String
            if (r2 == 0) goto L_0x0016
            java.lang.String r0 = (java.lang.String) r0
            r2 = r0
        L_0x000e:
            if (r2 == 0) goto L_0x0035
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0034 }
            r0.<init>(r2)     // Catch:{ MalformedURLException -> 0x0034 }
        L_0x0015:
            return r0
        L_0x0016:
            boolean r2 = r0 instanceof org.json.JSONObject
            if (r2 == 0) goto L_0x0037
            org.json.JSONObject r0 = (org.json.JSONObject) r0
            com.facebook.c.b r0 = com.facebook.c.c.a(r0)
            java.lang.Class<com.facebook.widget.l> r2 = com.facebook.widget.l.class
            com.facebook.c.b r0 = r0.a(r2)
            com.facebook.widget.l r0 = (com.facebook.widget.l) r0
            com.facebook.widget.m r0 = r0.a()
            if (r0 == 0) goto L_0x0037
            java.lang.String r0 = r0.a()
            r2 = r0
            goto L_0x000e
        L_0x0034:
            r0 = move-exception
        L_0x0035:
            r0 = r1
            goto L_0x0015
        L_0x0037:
            r2 = r1
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.f.e(com.facebook.c.b):java.net.URL");
    }

    /* access modifiers changed from: protected */
    public View a(String str, View view, ViewGroup viewGroup) {
        TextView textView;
        TextView textView2 = (TextView) view;
        if (textView2 == null) {
            textView = (TextView) this.f1937c.inflate(com.facebook.a.f.com_facebook_picker_list_section_header, (ViewGroup) null);
        } else {
            textView = textView2;
        }
        textView.setText(str);
        return textView;
    }

    /* access modifiers changed from: protected */
    public View a(b bVar, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = a(bVar, view);
        }
        a(view, bVar);
        return view;
    }

    private View a(View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.f1937c.inflate(com.facebook.a.f.com_facebook_picker_activity_circle_row, (ViewGroup) null);
        }
        ((ProgressBar) view.findViewById(e.com_facebook_picker_row_activity_circle)).setVisibility(0);
        return view;
    }

    /* access modifiers changed from: protected */
    public int a(b bVar) {
        return com.facebook.a.f.com_facebook_picker_list_row;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return d.com_facebook_profile_default_icon;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.widget.f.a(android.widget.CheckBox, boolean):void
     arg types: [android.widget.CheckBox, int]
     candidates:
      com.facebook.widget.f.a(android.view.View, android.view.ViewGroup):android.view.View
      com.facebook.widget.f.a(java.lang.String, com.facebook.c.b):int
      com.facebook.widget.f.a(com.facebook.c.b, android.view.View):android.view.View
      com.facebook.widget.f.a(android.view.View, com.facebook.c.b):void
      com.facebook.widget.f.a(android.widget.CheckBox, boolean):void */
    /* access modifiers changed from: protected */
    public View a(b bVar, View view) {
        View inflate = this.f1937c.inflate(a(bVar), (ViewGroup) null);
        ViewStub viewStub = (ViewStub) inflate.findViewById(e.com_facebook_picker_checkbox_stub);
        if (viewStub != null) {
            if (!c()) {
                viewStub.setVisibility(8);
            } else {
                a((CheckBox) viewStub.inflate(), false);
            }
        }
        ViewStub viewStub2 = (ViewStub) inflate.findViewById(e.com_facebook_picker_profile_pic_stub);
        if (!b()) {
            viewStub2.setVisibility(8);
        } else {
            ((ImageView) viewStub2.inflate()).setVisibility(0);
        }
        return inflate;
    }

    /* access modifiers changed from: protected */
    public void a(View view, b bVar) {
        URL e2;
        String f2 = f(bVar);
        view.setTag(f2);
        CharSequence c2 = c(bVar);
        TextView textView = (TextView) view.findViewById(e.com_facebook_picker_title);
        if (textView != null) {
            textView.setText(c2, TextView.BufferType.SPANNABLE);
        }
        CharSequence d2 = d(bVar);
        TextView textView2 = (TextView) view.findViewById(e.picker_subtitle);
        if (textView2 != null) {
            if (d2 != null) {
                textView2.setText(d2, TextView.BufferType.SPANNABLE);
                textView2.setVisibility(0);
            } else {
                textView2.setVisibility(8);
            }
        }
        if (c()) {
            a((CheckBox) view.findViewById(e.com_facebook_picker_checkbox), b(f2));
        }
        if (b() && (e2 = e(bVar)) != null) {
            ImageView imageView = (ImageView) view.findViewById(e.com_facebook_picker_image);
            if (this.p.containsKey(f2)) {
                ah ahVar = this.p.get(f2);
                imageView.setImageBitmap(ahVar.c());
                imageView.setTag(ahVar.a().b());
                return;
            }
            a(f2, e2, imageView);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    java.lang.String f(T r3) {
        /*
            r2 = this;
            java.util.Map r0 = r3.c()
            java.lang.String r1 = "id"
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x0019
            java.lang.String r0 = "id"
            java.lang.Object r0 = r3.a(r0)
            boolean r1 = r0 instanceof java.lang.String
            if (r1 == 0) goto L_0x0019
            java.lang.String r0 = (java.lang.String) r0
            return r0
        L_0x0019:
            com.facebook.z r0 = new com.facebook.z
            java.lang.String r1 = "Received an object without an ID."
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.f.f(com.facebook.c.b):java.lang.String");
    }

    /* access modifiers changed from: package-private */
    public boolean g(T t) {
        return this.l == null || this.l.a(t);
    }

    /* access modifiers changed from: package-private */
    public void a(k kVar) {
        this.l = kVar;
    }

    /* access modifiers changed from: package-private */
    public boolean b(String str) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(CheckBox checkBox, boolean z) {
    }

    /* access modifiers changed from: package-private */
    public String e() {
        ImageView imageView = (ImageView) a((b) null, (View) null).findViewById(e.com_facebook_picker_image);
        if (imageView == null) {
            return null;
        }
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        return String.format("picture.height(%d).width(%d)", Integer.valueOf(layoutParams.height), Integer.valueOf(layoutParams.width));
    }

    private boolean f() {
        return this.n != null && this.n.a() && this.m != null && !isEmpty();
    }

    private void g() {
        int i2;
        boolean z = false;
        this.d = new ArrayList();
        this.e = new HashMap();
        this.f = new HashMap();
        this.g = false;
        if (this.n != null && this.n.b() != 0) {
            this.n.c();
            int i3 = 0;
            while (true) {
                T e2 = this.n.e();
                if (!g(e2)) {
                    i2 = i3;
                } else {
                    i2 = i3 + 1;
                    String b2 = b((b) e2);
                    if (!this.e.containsKey(b2)) {
                        this.d.add(b2);
                        this.e.put(b2, new ArrayList());
                    }
                    this.e.get(b2).add(e2);
                    this.f.put(f(e2), e2);
                }
                if (!this.n.d()) {
                    break;
                }
                i3 = i2;
            }
            if (this.h != null) {
                Collator instance = Collator.getInstance();
                for (ArrayList<T> sort : this.e.values()) {
                    Collections.sort(sort, new g(this, instance));
                }
            }
            Collections.sort(this.d, Collator.getInstance());
            if (this.d.size() > 1 && i2 > 1) {
                z = true;
            }
            this.g = z;
        }
    }

    /* access modifiers changed from: package-private */
    public o<T> a(int i2) {
        b bVar;
        String str = null;
        if (this.d.size() == 0) {
            return null;
        }
        if (this.g) {
            Iterator<String> it = this.d.iterator();
            while (true) {
                if (!it.hasNext()) {
                    bVar = null;
                    break;
                }
                String next = it.next();
                int i3 = i2 - 1;
                if (i2 == 0) {
                    bVar = null;
                    str = next;
                    break;
                }
                List list = this.e.get(next);
                if (i3 < list.size()) {
                    bVar = (b) list.get(i3);
                    str = next;
                    break;
                }
                i2 = i3 - list.size();
            }
        } else {
            String str2 = this.d.get(0);
            List list2 = this.e.get(str2);
            if (i2 >= 0 && i2 < list2.size()) {
                bVar = (b) this.e.get(str2).get(i2);
                str = str2;
            } else if (f1935b || (this.m != null && this.n.a())) {
                return new o<>(null, null);
            } else {
                throw new AssertionError();
            }
        }
        if (str != null) {
            return new o<>(str, bVar);
        }
        throw new IndexOutOfBoundsException("position");
    }

    /* access modifiers changed from: package-private */
    public int a(String str, b bVar) {
        boolean z;
        int i2 = 0;
        Iterator<String> it = this.d.iterator();
        int i3 = 0;
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            String next = it.next();
            if (this.g) {
                i3++;
            }
            if (next.equals(str)) {
                z = true;
                break;
            }
            i3 = this.e.get(next).size() + i3;
        }
        if (!z) {
            return -1;
        }
        if (bVar == null) {
            if (this.g) {
                i2 = 1;
            }
            return i3 - i2;
        }
        Iterator it2 = this.e.get(str).iterator();
        while (it2.hasNext()) {
            if (c.a((b) it2.next(), bVar)) {
                return i3;
            }
            i3++;
        }
        return -1;
    }

    public boolean isEmpty() {
        return this.d.size() == 0;
    }

    public int getCount() {
        int i2;
        int i3 = 0;
        if (this.d.size() == 0) {
            return 0;
        }
        if (this.g) {
            i3 = this.d.size();
        }
        Iterator<ArrayList<T>> it = this.e.values().iterator();
        while (true) {
            i2 = i3;
            if (!it.hasNext()) {
                break;
            }
            i3 = it.next().size() + i2;
        }
        if (f()) {
            i2++;
        }
        return i2;
    }

    public boolean areAllItemsEnabled() {
        return this.g;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEnabled(int i2) {
        return a(i2).a() == p.GRAPH_OBJECT;
    }

    public Object getItem(int i2) {
        o a2 = a(i2);
        if (a2.a() == p.GRAPH_OBJECT) {
            return a2.f1945b;
        }
        return null;
    }

    public long getItemId(int i2) {
        String f2;
        o a2 = a(i2);
        if (a2 == null || a2.f1945b == null || (f2 = f(a2.f1945b)) == null) {
            return 0;
        }
        return Long.parseLong(f2);
    }

    public int getViewTypeCount() {
        return 3;
    }

    public int getItemViewType(int i2) {
        switch (i.f1943a[a(i2).a().ordinal()]) {
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            default:
                throw new z("Unexpected type of section and item.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.widget.f.a(com.facebook.c.b, android.view.View, android.view.ViewGroup):android.view.View
     arg types: [T, android.view.View, android.view.ViewGroup]
     candidates:
      com.facebook.widget.f.a(com.facebook.widget.ah, java.lang.String, android.widget.ImageView):void
      com.facebook.widget.f.a(java.lang.String, java.net.URL, android.widget.ImageView):void
      com.facebook.widget.f.a(java.lang.String, android.view.View, android.view.ViewGroup):android.view.View
      com.facebook.widget.f.a(int, int, int):void
      com.facebook.widget.f.a(com.facebook.c.b, android.view.View, android.view.ViewGroup):android.view.View */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        o a2 = a(i2);
        switch (i.f1943a[a2.a().ordinal()]) {
            case 1:
                return a(a2.f1944a, view, viewGroup);
            case 2:
                return a((b) a2.f1945b, view, viewGroup);
            case 3:
                if (f1935b || (this.n.a() && this.m != null)) {
                    this.m.a();
                    return a(view, viewGroup);
                }
                throw new AssertionError();
            default:
                throw new z("Unexpected type of section and item.");
        }
    }

    public Object[] getSections() {
        if (this.g) {
            return this.d.toArray();
        }
        return new Object[0];
    }

    public int getPositionForSection(int i2) {
        int max;
        if (!this.g || (max = Math.max(0, Math.min(i2, this.d.size() - 1))) >= this.d.size()) {
            return 0;
        }
        return a(this.d.get(max), (b) null);
    }

    public int getSectionForPosition(int i2) {
        o a2 = a(i2);
        if (a2 == null || a2.a() == p.ACTIVITY_CIRCLE) {
            return 0;
        }
        return Math.max(0, Math.min(this.d.indexOf(a2.f1944a), this.d.size() - 1));
    }

    private void a(String str, URL url, ImageView imageView) {
        if (url != null) {
            boolean z = imageView == null;
            if (z || !url.equals(imageView.getTag())) {
                if (!z) {
                    imageView.setTag(str);
                    imageView.setImageResource(a());
                }
                ad a2 = new af(this.o.getApplicationContext(), url).a(this).a((ag) new h(this, str, imageView)).a();
                this.f1936a.put(str, a2);
                x.a(a2);
            }
        }
    }

    private void a(Exception exc) {
        Exception exc2;
        if (this.r != null) {
            if (!(exc instanceof z)) {
                exc2 = new z(exc);
            } else {
                exc2 = exc;
            }
            this.r.a(this, (z) exc2);
        }
    }

    /* access modifiers changed from: private */
    public void a(ah ahVar, String str, ImageView imageView) {
        this.f1936a.remove(str);
        if (ahVar.b() != null) {
            a(ahVar.b());
        }
        if (imageView == null) {
            if (ahVar.c() != null) {
                if (this.p.size() >= 20) {
                    this.p.remove(this.q.remove(0));
                }
                this.p.put(str, ahVar);
            }
        } else if (imageView != null && str.equals(imageView.getTag())) {
            Exception b2 = ahVar.b();
            Bitmap c2 = ahVar.c();
            if (b2 == null && c2 != null) {
                imageView.setImageBitmap(c2);
                imageView.setTag(ahVar.a().b());
            }
        }
    }

    /* access modifiers changed from: private */
    public static int b(b bVar, b bVar2, Collection<String> collection, Collator collator) {
        for (String next : collection) {
            String str = (String) bVar.a(next);
            String str2 = (String) bVar2.a(next);
            if (str != null && str2 != null) {
                int compare = collator.compare(str, str2);
                if (compare != 0) {
                    return compare;
                }
            } else if (str != null || str2 != null) {
                return str == null ? -1 : 1;
            }
        }
        return 0;
    }
}
