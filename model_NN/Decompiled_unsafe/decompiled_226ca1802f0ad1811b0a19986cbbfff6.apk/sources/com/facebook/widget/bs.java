package com.facebook.widget;

import android.database.CursorIndexOutOfBoundsException;
import com.facebook.c.b;
import java.util.ArrayList;
import java.util.Collection;

/* compiled from: SimpleGraphObjectCursor */
class bs<T extends b> implements q<T> {

    /* renamed from: a  reason: collision with root package name */
    private int f1905a = -1;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1906b = false;

    /* renamed from: c  reason: collision with root package name */
    private ArrayList<T> f1907c = new ArrayList<>();
    private boolean d = false;
    private boolean e = false;

    bs() {
    }

    bs(bs<T> bsVar) {
        this.f1905a = bsVar.f1905a;
        this.f1906b = bsVar.f1906b;
        this.f1907c = new ArrayList<>();
        this.f1907c.addAll(bsVar.f1907c);
        this.e = bsVar.e;
    }

    public void a(Collection<T> collection, boolean z) {
        this.f1907c.addAll(collection);
        this.e |= z;
    }

    public boolean g() {
        return this.e;
    }

    public void a(boolean z) {
        this.e = z;
    }

    public boolean a() {
        return this.d;
    }

    public void b(boolean z) {
        this.d = z;
    }

    public int b() {
        return this.f1907c.size();
    }

    public boolean a(int i) {
        int b2 = b();
        if (i >= b2) {
            this.f1905a = b2;
            return false;
        } else if (i < 0) {
            this.f1905a = -1;
            return false;
        } else {
            this.f1905a = i;
            return true;
        }
    }

    public boolean c() {
        return a(0);
    }

    public boolean d() {
        return a(this.f1905a + 1);
    }

    public T e() {
        if (this.f1905a < 0) {
            throw new CursorIndexOutOfBoundsException("Before first object.");
        } else if (this.f1905a < this.f1907c.size()) {
            return (b) this.f1907c.get(this.f1905a);
        } else {
            throw new CursorIndexOutOfBoundsException("After last object.");
        }
    }

    public void f() {
        this.f1906b = true;
    }

    public boolean h() {
        return this.f1906b;
    }
}
