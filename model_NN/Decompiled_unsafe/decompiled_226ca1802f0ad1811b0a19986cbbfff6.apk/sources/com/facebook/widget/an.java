package com.facebook.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import com.facebook.a.g;
import com.facebook.b.p;
import com.facebook.bg;
import com.facebook.br;
import com.facebook.bs;

/* compiled from: LoginButton */
class an implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LoginButton f1878a;

    private an(LoginButton loginButton) {
        this.f1878a = loginButton;
    }

    /* synthetic */ an(LoginButton loginButton, ak akVar) {
        this(loginButton);
    }

    public void onClick(View view) {
        bs bsVar;
        String string;
        Context context = this.f1878a.getContext();
        bg b2 = this.f1878a.f1841c.b();
        if (b2 == null) {
            bg a2 = this.f1878a.f1841c.a();
            if (a2 == null || a2.b().b()) {
                this.f1878a.f1841c.a((bg) null);
                a2 = new br(context).a(this.f1878a.f1840b).a();
                bg.a(a2);
            }
            if (!a2.a()) {
                if (this.f1878a.k != null) {
                    bsVar = new bs(this.f1878a.k);
                } else {
                    bsVar = context instanceof Activity ? new bs((Activity) context) : null;
                }
                if (bsVar != null) {
                    bsVar.a(this.f1878a.l.f1875a);
                    bsVar.a(this.f1878a.l.f1876b);
                    bsVar.a(this.f1878a.l.e);
                    if (p.PUBLISH.equals(this.f1878a.l.f1877c)) {
                        a2.b(bsVar);
                    } else {
                        a2.a(bsVar);
                    }
                }
            }
        } else if (this.f1878a.f) {
            String string2 = this.f1878a.getResources().getString(g.com_facebook_loginview_log_out_action);
            String string3 = this.f1878a.getResources().getString(g.com_facebook_loginview_cancel_action);
            if (this.f1878a.d == null || this.f1878a.d.b() == null) {
                string = this.f1878a.getResources().getString(g.com_facebook_loginview_logged_in_using_facebook);
            } else {
                string = String.format(this.f1878a.getResources().getString(g.com_facebook_loginview_logged_in_as), this.f1878a.d.b());
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(string).setCancelable(true).setPositiveButton(string2, new ao(this, b2)).setNegativeButton(string3, (DialogInterface.OnClickListener) null);
            builder.create().show();
        } else {
            b2.h();
        }
    }
}
