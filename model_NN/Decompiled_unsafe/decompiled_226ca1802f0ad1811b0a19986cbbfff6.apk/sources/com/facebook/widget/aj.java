package com.facebook.widget;

import android.support.v4.app.FragmentTransaction;
import com.facebook.b.u;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;

/* compiled from: ImageResponseCache */
class aj extends BufferedInputStream {

    /* renamed from: a  reason: collision with root package name */
    HttpURLConnection f1871a;

    aj(InputStream inputStream, HttpURLConnection httpURLConnection) {
        super(inputStream, FragmentTransaction.TRANSIT_EXIT_MASK);
        this.f1871a = httpURLConnection;
    }

    public void close() {
        super.close();
        u.a(this.f1871a);
    }
}
