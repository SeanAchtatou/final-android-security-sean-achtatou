package com.facebook.widget;

import android.content.Context;
import com.facebook.al;
import com.facebook.b.b;
import com.facebook.b.j;
import com.facebook.b.n;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: ImageResponseCache */
class ai {

    /* renamed from: a  reason: collision with root package name */
    static final String f1869a = ai.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static volatile b f1870b;

    ai() {
    }

    static synchronized b a(Context context) {
        b bVar;
        synchronized (ai.class) {
            if (f1870b == null) {
                f1870b = new b(context.getApplicationContext(), f1869a, new j());
            }
            bVar = f1870b;
        }
        return bVar;
    }

    static InputStream a(URL url, Context context) {
        if (url == null || !a(url)) {
            return null;
        }
        try {
            return a(context).a(url.toString());
        } catch (IOException e) {
            n.a(al.CACHE, 5, f1869a, e.toString());
            return null;
        }
    }

    static InputStream a(Context context, HttpURLConnection httpURLConnection) {
        if (httpURLConnection.getResponseCode() != 200) {
            return null;
        }
        URL url = httpURLConnection.getURL();
        InputStream inputStream = httpURLConnection.getInputStream();
        if (!a(url)) {
            return inputStream;
        }
        try {
            return a(context).a(url.toString(), new aj(inputStream, httpURLConnection));
        } catch (IOException e) {
            return inputStream;
        }
    }

    private static boolean a(URL url) {
        if (url != null) {
            String host = url.getHost();
            if (host.endsWith("fbcdn.net")) {
                return true;
            }
            if (!host.startsWith("fbcdn") || !host.endsWith("akamaihd.net")) {
                return false;
            }
            return true;
        }
        return false;
    }
}
