package com.facebook.widget;

import com.facebook.al;
import com.facebook.b.n;
import com.facebook.z;

/* compiled from: PlacePickerFragment */
class bm implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PlacePickerFragment f1900a;

    bm(PlacePickerFragment placePickerFragment) {
        this.f1900a = placePickerFragment;
    }

    public void run() {
        try {
            this.f1900a.c(true);
            if (0 != 0) {
                bf e = this.f1900a.e();
                if (e != null) {
                    e.a(this.f1900a, null);
                    return;
                }
                n.a(al.REQUESTS, "PlacePickerFragment", "Error loading data : %s", null);
            }
        } catch (z e2) {
            if (e2 != null) {
                bf e3 = this.f1900a.e();
                if (e3 != null) {
                    e3.a(this.f1900a, e2);
                    return;
                }
                n.a(al.REQUESTS, "PlacePickerFragment", "Error loading data : %s", e2);
            }
        } catch (Exception e4) {
            z zVar = new z(e4);
            if (zVar != null) {
                bf e5 = this.f1900a.e();
                if (e5 != null) {
                    e5.a(this.f1900a, zVar);
                    return;
                }
                n.a(al.REQUESTS, "PlacePickerFragment", "Error loading data : %s", zVar);
            }
        } catch (Throwable th) {
            if (0 != 0) {
                bf e6 = this.f1900a.e();
                if (e6 != null) {
                    e6.a(this.f1900a, null);
                } else {
                    n.a(al.REQUESTS, "PlacePickerFragment", "Error loading data : %s", null);
                }
            }
            throw th;
        }
    }
}
