package com.facebook.widget;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.o;
import com.facebook.ac;
import com.facebook.ap;
import com.facebook.au;
import com.facebook.b.a;
import com.facebook.ba;
import com.facebook.bc;
import com.facebook.be;
import com.facebook.c.b;
import com.facebook.c.g;
import com.facebook.z;

/* compiled from: GraphObjectPagingLoader */
class r<T extends b> extends o<bs<T>> {

    /* renamed from: a  reason: collision with root package name */
    private final Class<T> f1949a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1950b;

    /* renamed from: c  reason: collision with root package name */
    private ap f1951c;
    private ap d;
    private ap e;
    private v f;
    private bs<T> g;
    private boolean h = false;
    private boolean i = false;

    public r(Context context, Class<T> cls) {
        super(context);
        this.f1949a = cls;
    }

    public void a(v vVar) {
        this.f = vVar;
    }

    public bs<T> b() {
        return this.g;
    }

    public void c() {
        this.e = null;
        this.f1951c = null;
        this.d = null;
        b((bs) null);
    }

    public boolean d() {
        return this.i;
    }

    public void a(ap apVar, boolean z) {
        this.f1951c = apVar;
        a(apVar, z, 0);
    }

    public void a(long j) {
        if (this.f1951c == null) {
            throw new z("refreshOriginalRequest may not be called until after startLoading has been called.");
        }
        a(this.f1951c, false, j);
    }

    public void e() {
        if (this.e != null) {
            this.h = true;
            this.d = this.e;
            this.d.a((au) new s(this));
            this.i = true;
            ap.c((ba) b(this.d, this.f1950b));
        }
    }

    /* renamed from: a */
    public void b(bs bsVar) {
        bs<T> bsVar2 = this.g;
        this.g = bsVar;
        if (l()) {
            super.b(bsVar);
            if (bsVar2 != null && bsVar2 != bsVar && !bsVar2.h()) {
                bsVar2.f();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void g() {
        super.g();
        if (this.g != null) {
            b(this.g);
        }
    }

    private void a(ap apVar, boolean z, long j) {
        this.f1950b = z;
        this.h = false;
        this.e = null;
        this.d = apVar;
        this.d.a((au) new t(this));
        this.i = true;
        u uVar = new u(this, b(apVar, z));
        if (j == 0) {
            uVar.run();
        } else {
            new Handler().postDelayed(uVar, j);
        }
    }

    private a b(ap apVar, boolean z) {
        boolean z2 = true;
        a aVar = new a(apVar);
        if (z) {
            z2 = false;
        }
        aVar.a(z2);
        return aVar;
    }

    /* access modifiers changed from: private */
    public void a(bc bcVar) {
        if (bcVar.c() == this.d) {
            this.i = false;
            this.d = null;
            ac a2 = bcVar.a();
            z e2 = a2 == null ? null : a2.e();
            if (bcVar.b() == null && e2 == null) {
                e2 = new z("GraphObjectPagingLoader received neither a result nor an error.");
            }
            if (e2 != null) {
                this.e = null;
                if (this.f != null) {
                    this.f.a(e2, this);
                    return;
                }
                return;
            }
            b(bcVar);
        }
    }

    private void b(bc bcVar) {
        boolean z;
        bs bsVar = (this.g == null || !this.h) ? new bs() : new bs(this.g);
        boolean d2 = bcVar.d();
        g<U> a2 = ((w) bcVar.a(w.class)).a().a(this.f1949a);
        if (a2.size() > 0) {
            z = true;
        } else {
            z = false;
        }
        if (z) {
            this.e = bcVar.a(be.NEXT);
            bsVar.a(a2, d2);
            bsVar.b(true);
        }
        if (!z) {
            bsVar.b(false);
            bsVar.a(d2);
            this.e = null;
        }
        if (!d2) {
            this.f1950b = false;
        }
        b(bsVar);
    }
}
