package com.facebook.widget;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

/* compiled from: PickerFragment */
class as implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PickerFragment f1882a;

    as(PickerFragment pickerFragment) {
        this.f1882a = pickerFragment;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        this.f1882a.a((ListView) adapterView, view, i);
    }
}
