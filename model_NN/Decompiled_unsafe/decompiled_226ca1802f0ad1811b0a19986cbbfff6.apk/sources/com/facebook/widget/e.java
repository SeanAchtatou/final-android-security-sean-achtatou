package com.facebook.widget;

import com.facebook.c.i;

/* compiled from: FriendPickerFragment */
class e extends ay {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FriendPickerFragment f1934a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private e(FriendPickerFragment friendPickerFragment) {
        super(friendPickerFragment);
        this.f1934a = friendPickerFragment;
    }

    /* synthetic */ e(FriendPickerFragment friendPickerFragment, d dVar) {
        this(friendPickerFragment);
    }

    /* access modifiers changed from: protected */
    public void a(r<i> rVar, bs<i> bsVar) {
        super.a(rVar, bsVar);
        if (bsVar != null && !rVar.d()) {
            if (bsVar.a()) {
                e();
                return;
            }
            this.f1934a.n();
            if (bsVar.g()) {
                rVar.a(bsVar.b() == 0 ? 2000 : 0);
            }
        }
    }

    private void e() {
        this.f1934a.l();
        this.f1887b.e();
    }
}
