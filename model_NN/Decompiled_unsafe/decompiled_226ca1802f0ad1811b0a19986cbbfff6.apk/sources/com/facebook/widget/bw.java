package com.facebook.widget;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.facebook.a.d;
import com.facebook.a.g;
import com.facebook.ab;
import com.facebook.b.u;
import com.facebook.z;

/* compiled from: WebDialog */
public class bw extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private String f1914a;

    /* renamed from: b  reason: collision with root package name */
    private cd f1915b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public WebView f1916c;
    /* access modifiers changed from: private */
    public ProgressDialog d;
    /* access modifiers changed from: private */
    public ImageView e;
    /* access modifiers changed from: private */
    public FrameLayout f;
    private boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;

    public bw(Context context, String str, Bundle bundle, int i, cd cdVar) {
        super(context, i);
        bundle = bundle == null ? new Bundle() : bundle;
        bundle.putString("display", "touch");
        bundle.putString("type", "user_agent");
        this.f1914a = u.a("m.facebook.com", "dialog/" + str, bundle).toString();
        this.f1915b = cdVar;
    }

    public void dismiss() {
        if (this.f1916c != null) {
            this.f1916c.stopLoading();
        }
        if (!this.h) {
            if (this.d.isShowing()) {
                this.d.dismiss();
            }
            super.dismiss();
        }
    }

    public void onDetachedFromWindow() {
        this.h = true;
        super.onDetachedFromWindow();
    }

    public void onAttachedToWindow() {
        this.h = false;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setOnCancelListener(new bx(this));
        this.d = new ProgressDialog(getContext());
        this.d.requestWindowFeature(1);
        this.d.setMessage(getContext().getString(g.com_facebook_loading));
        this.d.setOnCancelListener(new by(this));
        requestWindowFeature(1);
        this.f = new FrameLayout(getContext());
        b();
        a(this.e.getDrawable().getIntrinsicWidth() / 2);
        this.f.addView(this.e, new ViewGroup.LayoutParams(-2, -2));
        addContentView(this.f, new ViewGroup.LayoutParams(-1, -1));
    }

    /* access modifiers changed from: private */
    public void a(Bundle bundle) {
        if (this.f1915b != null && !this.g) {
            this.g = true;
            this.f1915b.a(bundle, null);
        }
    }

    /* access modifiers changed from: private */
    public void a(Throwable th) {
        z zVar;
        if (this.f1915b != null && !this.g) {
            this.g = true;
            if (th instanceof z) {
                zVar = (z) th;
            } else {
                zVar = new z(th);
            }
            this.f1915b.a(null, zVar);
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        a(new ab());
    }

    private void b() {
        this.e = new ImageView(getContext());
        this.e.setOnClickListener(new bz(this));
        this.e.setImageDrawable(getContext().getResources().getDrawable(d.com_facebook_close));
        this.e.setVisibility(4);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void a(int i) {
        LinearLayout linearLayout = new LinearLayout(getContext());
        this.f1916c = new WebView(getContext());
        this.f1916c.setVerticalScrollBarEnabled(false);
        this.f1916c.setHorizontalScrollBarEnabled(false);
        this.f1916c.setWebViewClient(new cc(this, null));
        this.f1916c.getSettings().setJavaScriptEnabled(true);
        this.f1916c.loadUrl(this.f1914a);
        this.f1916c.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.f1916c.setVisibility(4);
        this.f1916c.getSettings().setSavePassword(false);
        linearLayout.setPadding(i, i, i, i);
        linearLayout.addView(this.f1916c);
        this.f.addView(linearLayout);
    }
}
