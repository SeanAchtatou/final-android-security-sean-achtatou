package com.facebook.widget;

import android.content.Context;
import com.facebook.a.d;
import com.facebook.a.f;
import com.facebook.a.g;
import com.facebook.c.h;

/* compiled from: PlacePickerFragment */
class bk extends PickerFragment<h>.com/facebook/widget/bh<h> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PlacePickerFragment f1898a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bk(PlacePickerFragment placePickerFragment, Context context) {
        super(placePickerFragment, context);
        this.f1898a = placePickerFragment;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public CharSequence d(h hVar) {
        String a2 = hVar.a();
        Integer num = (Integer) hVar.a("were_here_count");
        if (a2 != null && num != null) {
            return this.f1898a.getString(g.com_facebook_placepicker_subtitle_format, a2, num);
        } else if (a2 == null && num != null) {
            return this.f1898a.getString(g.com_facebook_placepicker_subtitle_were_here_only_format, num);
        } else if (a2 == null || num != null) {
            return null;
        } else {
            return this.f1898a.getString(g.com_facebook_placepicker_subtitle_catetory_only_format, a2);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public int a(h hVar) {
        return f.com_facebook_placepickerfragment_list_row;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return d.com_facebook_place_default_icon;
    }
}
