package com.facebook.widget;

import android.content.Context;
import android.net.Uri;
import com.facebook.b.v;
import java.net.URL;

/* compiled from: ImageRequest */
class ad {

    /* renamed from: a  reason: collision with root package name */
    private Context f1860a;

    /* renamed from: b  reason: collision with root package name */
    private URL f1861b;

    /* renamed from: c  reason: collision with root package name */
    private ag f1862c;
    private boolean d;
    private Object e;

    static URL a(String str, int i, int i2) {
        v.a(str, "userId");
        int max = Math.max(i, 0);
        int max2 = Math.max(i2, 0);
        if (max == 0 && max2 == 0) {
            throw new IllegalArgumentException("Either width or height must be greater than 0");
        }
        Uri.Builder encodedPath = new Uri.Builder().encodedPath(String.format("https://graph.facebook.com/%s/picture", str));
        if (max2 != 0) {
            encodedPath.appendQueryParameter("height", String.valueOf(max2));
        }
        if (max != 0) {
            encodedPath.appendQueryParameter("width", String.valueOf(max));
        }
        encodedPath.appendQueryParameter("migration_overrides", "{october_2012:true}");
        return new URL(encodedPath.toString());
    }

    private ad(af afVar) {
        this.f1860a = afVar.f1863a;
        this.f1861b = afVar.f1864b;
        this.f1862c = afVar.f1865c;
        this.d = afVar.d;
        this.e = afVar.e == null ? new Object() : afVar.e;
    }

    /* access modifiers changed from: package-private */
    public Context a() {
        return this.f1860a;
    }

    /* access modifiers changed from: package-private */
    public URL b() {
        return this.f1861b;
    }

    /* access modifiers changed from: package-private */
    public ag c() {
        return this.f1862c;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public Object e() {
        return this.e;
    }
}
