package com.facebook.widget;

import android.graphics.Bitmap;

/* compiled from: ImageDownloader */
final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f1959a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Exception f1960b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ boolean f1961c;
    final /* synthetic */ Bitmap d;
    final /* synthetic */ ag e;

    y(ad adVar, Exception exc, boolean z, Bitmap bitmap, ag agVar) {
        this.f1959a = adVar;
        this.f1960b = exc;
        this.f1961c = z;
        this.d = bitmap;
        this.e = agVar;
    }

    public void run() {
        this.e.a(new ah(this.f1959a, this.f1960b, this.f1961c, this.d));
    }
}
