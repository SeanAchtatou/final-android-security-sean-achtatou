package com.facebook.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import com.facebook.b.u;
import java.io.Closeable;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ImageDownloader */
class x {

    /* renamed from: a  reason: collision with root package name */
    private static final Handler f1956a = new Handler();

    /* renamed from: b  reason: collision with root package name */
    private static ce f1957b = new ce(8);

    /* renamed from: c  reason: collision with root package name */
    private static ce f1958c = new ce(2);
    private static final Map<ac, ab> d = new HashMap();

    static void a(ad adVar) {
        if (adVar != null) {
            ac acVar = new ac(adVar.b(), adVar.e());
            synchronized (d) {
                ab abVar = d.get(acVar);
                if (abVar != null) {
                    abVar.f1856b = adVar;
                    abVar.f1857c = false;
                    abVar.f1855a.b();
                } else {
                    a(adVar, acVar, adVar.d());
                }
            }
        }
    }

    static boolean b(ad adVar) {
        boolean z;
        ac acVar = new ac(adVar.b(), adVar.e());
        synchronized (d) {
            ab abVar = d.get(acVar);
            if (abVar == null) {
                z = false;
            } else if (abVar.f1855a.a()) {
                d.remove(acVar);
                z = true;
            } else {
                abVar.f1857c = true;
                z = true;
            }
        }
        return z;
    }

    static void c(ad adVar) {
        ac acVar = new ac(adVar.b(), adVar.e());
        synchronized (d) {
            ab abVar = d.get(acVar);
            if (abVar != null) {
                abVar.f1855a.b();
            }
        }
    }

    private static void a(ad adVar, ac acVar, boolean z) {
        a(adVar, acVar, f1958c, new z(adVar.a(), acVar, z));
    }

    private static void a(ad adVar, ac acVar) {
        a(adVar, acVar, f1957b, new aa(adVar.a(), acVar));
    }

    private static void a(ad adVar, ac acVar, ce ceVar, Runnable runnable) {
        synchronized (d) {
            ab abVar = new ab(null);
            abVar.f1856b = adVar;
            d.put(acVar, abVar);
            abVar.f1855a = ceVar.a(runnable);
        }
    }

    private static void a(ac acVar, Exception exc, Bitmap bitmap, boolean z) {
        ad adVar;
        ag c2;
        ab a2 = a(acVar);
        if (a2 != null && !a2.f1857c && (c2 = (adVar = a2.f1856b).c()) != null) {
            f1956a.post(new y(adVar, exc, z, bitmap, c2));
        }
    }

    /* access modifiers changed from: private */
    public static void b(ac acVar, Context context, boolean z) {
        boolean z2;
        InputStream inputStream;
        URL a2;
        boolean z3 = false;
        if (!z || (a2 = bt.a(context, acVar.f1858a)) == null) {
            z2 = false;
            inputStream = null;
        } else {
            InputStream a3 = ai.a(a2, context);
            if (a3 != null) {
                z3 = true;
            }
            boolean z4 = z3;
            inputStream = a3;
            z2 = z4;
        }
        if (!z2) {
            inputStream = ai.a(acVar.f1858a, context);
        }
        if (inputStream != null) {
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
            u.a((Closeable) inputStream);
            a(acVar, (Exception) null, decodeStream, z2);
            return;
        }
        ab a4 = a(acVar);
        if (a4 != null && !a4.f1857c) {
            a(a4.f1856b, acVar);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.io.Closeable] */
    /* JADX WARN: Type inference failed for: r3v2, types: [android.graphics.Bitmap] */
    /* JADX WARN: Type inference failed for: r3v3 */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.widget.x.a(com.facebook.widget.ac, java.lang.Exception, android.graphics.Bitmap, boolean):void
     arg types: [com.facebook.widget.ac, java.lang.Exception, ?, int]
     candidates:
      com.facebook.widget.x.a(com.facebook.widget.ad, com.facebook.widget.ac, com.facebook.widget.ce, java.lang.Runnable):void
      com.facebook.widget.x.a(com.facebook.widget.ac, java.lang.Exception, android.graphics.Bitmap, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.widget.x.a(com.facebook.widget.ad, com.facebook.widget.ac, boolean):void
     arg types: [com.facebook.widget.ad, com.facebook.widget.ac, int]
     candidates:
      com.facebook.widget.x.a(com.facebook.widget.ac, android.content.Context, boolean):void
      com.facebook.widget.x.a(com.facebook.widget.ad, com.facebook.widget.ac, boolean):void */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a5, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00a6, code lost:
        r10 = r1;
        r1 = r0;
        r0 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00ae, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00af, code lost:
        r5 = null;
        r10 = r4;
        r4 = r0;
        r0 = r10;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a5 A[ExcHandler: all (r1v5 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x000c] */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(com.facebook.widget.ac r11, android.content.Context r12) {
        /*
            r3 = 0
            r2 = 0
            r1 = 1
            java.net.URL r0 = r11.f1858a     // Catch:{ IOException -> 0x00aa, all -> 0x00a2 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x00aa, all -> 0x00a2 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x00aa, all -> 0x00a2 }
            r4 = 0
            r0.setInstanceFollowRedirects(r4)     // Catch:{ IOException -> 0x00ae, all -> 0x00a5 }
            int r4 = r0.getResponseCode()     // Catch:{ IOException -> 0x00ae, all -> 0x00a5 }
            switch(r4) {
                case 200: goto L_0x007d;
                case 301: goto L_0x0046;
                case 302: goto L_0x0046;
                default: goto L_0x0016;
            }     // Catch:{ IOException -> 0x00ae, all -> 0x00a5 }
        L_0x0016:
            java.io.InputStream r5 = r0.getErrorStream()     // Catch:{ IOException -> 0x00ae, all -> 0x00a5 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            r6 = 128(0x80, float:1.794E-43)
            char[] r6 = new char[r6]     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            r7.<init>()     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
        L_0x0028:
            r8 = 0
            int r9 = r6.length     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            int r8 = r4.read(r6, r8, r9)     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            if (r8 <= 0) goto L_0x0089
            r9 = 0
            r7.append(r6, r9, r8)     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            goto L_0x0028
        L_0x0035:
            r4 = move-exception
            r10 = r4
            r4 = r0
            r0 = r10
        L_0x0039:
            com.facebook.b.u.a(r5)
            com.facebook.b.u.a(r4)
            r4 = r0
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            a(r11, r4, r3, r2)
        L_0x0045:
            return
        L_0x0046:
            java.lang.String r1 = "location"
            java.lang.String r1 = r0.getHeaderField(r1)     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            boolean r4 = com.facebook.b.u.a(r1)     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            if (r4 != 0) goto L_0x00ba
            java.net.URL r4 = new java.net.URL     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            java.net.URL r1 = r11.f1858a     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            com.facebook.widget.bt.a(r12, r1, r4)     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            com.facebook.widget.ab r1 = a(r11)     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            if (r1 == 0) goto L_0x0073
            boolean r5 = r1.f1857c     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            if (r5 != 0) goto L_0x0073
            com.facebook.widget.ad r1 = r1.f1856b     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            com.facebook.widget.ac r5 = new com.facebook.widget.ac     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            java.lang.Object r6 = r11.f1859b     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            r5.<init>(r4, r6)     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
            r4 = 0
            a(r1, r5, r4)     // Catch:{ IOException -> 0x00b4, all -> 0x00a5 }
        L_0x0073:
            r1 = r2
            r4 = r3
            r5 = r3
        L_0x0076:
            com.facebook.b.u.a(r5)
            com.facebook.b.u.a(r0)
            goto L_0x0040
        L_0x007d:
            java.io.InputStream r5 = com.facebook.widget.ai.a(r12, r0)     // Catch:{ IOException -> 0x00ae, all -> 0x00a5 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeStream(r5)     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x0076
        L_0x0089:
            com.facebook.b.u.a(r4)     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            com.facebook.z r4 = new com.facebook.z     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            java.lang.String r6 = r7.toString()     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            r4.<init>(r6)     // Catch:{ IOException -> 0x0035, all -> 0x0096 }
            goto L_0x0076
        L_0x0096:
            r1 = move-exception
            r3 = r5
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x009b:
            com.facebook.b.u.a(r3)
            com.facebook.b.u.a(r1)
            throw r0
        L_0x00a2:
            r0 = move-exception
            r1 = r3
            goto L_0x009b
        L_0x00a5:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x009b
        L_0x00aa:
            r0 = move-exception
            r5 = r3
            r4 = r3
            goto L_0x0039
        L_0x00ae:
            r4 = move-exception
            r5 = r3
            r10 = r4
            r4 = r0
            r0 = r10
            goto L_0x0039
        L_0x00b4:
            r1 = move-exception
            r5 = r3
            r4 = r0
            r0 = r1
            r1 = r2
            goto L_0x0039
        L_0x00ba:
            r1 = r2
            r4 = r3
            r5 = r3
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.widget.x.b(com.facebook.widget.ac, android.content.Context):void");
    }

    private static ab a(ac acVar) {
        ab remove;
        synchronized (d) {
            remove = d.remove(acVar);
        }
        return remove;
    }
}
