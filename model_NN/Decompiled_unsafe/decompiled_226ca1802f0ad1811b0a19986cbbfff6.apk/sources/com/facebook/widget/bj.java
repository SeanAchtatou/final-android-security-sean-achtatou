package com.facebook.widget;

import android.os.Bundle;
import android.text.TextUtils;

/* compiled from: PickerFragment */
class bj extends bi {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PickerFragment f1896a;

    /* renamed from: c  reason: collision with root package name */
    private String f1897c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bj(PickerFragment pickerFragment) {
        super(pickerFragment);
        this.f1896a = pickerFragment;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        return (this.f1897c == null || str == null || !this.f1897c.equals(str)) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (this.f1897c == null || !this.f1897c.equals(str)) {
            this.f1897c = str;
        } else {
            this.f1897c = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Bundle bundle, String str) {
        if (!TextUtils.isEmpty(this.f1897c)) {
            bundle.putString(str, this.f1897c);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Bundle bundle, String str) {
        if (bundle != null) {
            this.f1897c = bundle.getString(str);
        }
    }

    public void a() {
        this.f1897c = null;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.f1897c == null;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return false;
    }
}
