package com.facebook.widget;

import com.facebook.cb;
import java.util.concurrent.Executor;

/* compiled from: WorkQueue */
class ce {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f1925a = (!ce.class.desiredAssertionStatus());
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final Object f1926b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ch f1927c;
    private final int d;
    private final Executor e;
    private ch f;
    private int g;

    ce() {
        this(8);
    }

    ce(int i) {
        this(i, cb.a());
    }

    ce(int i, Executor executor) {
        this.f1926b = new Object();
        this.f = null;
        this.g = 0;
        this.d = i;
        this.e = executor;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.widget.ce.a(java.lang.Runnable, boolean):com.facebook.widget.cg
     arg types: [java.lang.Runnable, int]
     candidates:
      com.facebook.widget.ce.a(com.facebook.widget.ce, com.facebook.widget.ch):void
      com.facebook.widget.ce.a(java.lang.Runnable, boolean):com.facebook.widget.cg */
    /* access modifiers changed from: package-private */
    public cg a(Runnable runnable) {
        return a(runnable, true);
    }

    /* access modifiers changed from: package-private */
    public cg a(Runnable runnable, boolean z) {
        ch chVar = new ch(this, runnable);
        synchronized (this.f1926b) {
            this.f1927c = chVar.a(this.f1927c, z);
        }
        a();
        return chVar;
    }

    private void a() {
        a((ch) null);
    }

    /* access modifiers changed from: private */
    public void a(ch chVar) {
        ch chVar2 = null;
        synchronized (this.f1926b) {
            if (chVar != null) {
                this.f = chVar.a(this.f);
                this.g--;
            }
            if (this.g < this.d && (chVar2 = this.f1927c) != null) {
                this.f1927c = chVar2.a(this.f1927c);
                this.f = chVar2.a(this.f, false);
                this.g++;
                chVar2.a(true);
            }
        }
        if (chVar2 != null) {
            b(chVar2);
        }
    }

    private void b(ch chVar) {
        this.e.execute(new cf(this, chVar));
    }
}
