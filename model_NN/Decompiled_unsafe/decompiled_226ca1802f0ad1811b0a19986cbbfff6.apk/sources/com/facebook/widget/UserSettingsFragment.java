package com.facebook.widget;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.a.b;
import com.facebook.a.c;
import com.facebook.a.d;
import com.facebook.a.e;
import com.facebook.a.f;
import com.facebook.a.g;
import com.facebook.ap;
import com.facebook.bg;
import com.facebook.bu;
import com.facebook.bz;
import com.facebook.c.i;
import java.net.MalformedURLException;
import java.net.URL;

public class UserSettingsFragment extends a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1849a = TextUtils.join(",", new String[]{"id", "name", "picture"});
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public LoginButton f1850b;

    /* renamed from: c  reason: collision with root package name */
    private am f1851c = new am();
    private TextView d;
    /* access modifiers changed from: private */
    public i e;
    private bg f;
    private Drawable g;
    private String h;
    private bu i;

    public /* bridge */ /* synthetic */ void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    public /* bridge */ /* synthetic */ void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
    }

    public /* bridge */ /* synthetic */ void onDestroy() {
        super.onDestroy();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(f.com_facebook_usersettingsfragment, viewGroup, false);
        this.f1850b = (LoginButton) inflate.findViewById(e.com_facebook_usersettingsfragment_login_button);
        this.f1850b.a(this.f1851c);
        this.f1850b.a(this);
        bg a2 = a();
        if (a2 != null && !a2.equals(bg.i())) {
            this.f1850b.a(a2);
        }
        this.d = (TextView) inflate.findViewById(e.com_facebook_usersettingsfragment_profile_name);
        if (inflate.getBackground() == null) {
            inflate.setBackgroundColor(getResources().getColor(b.com_facebook_blue));
        } else {
            inflate.getBackground().setDither(true);
        }
        return inflate;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
    }

    public void onResume() {
        super.onResume();
        c();
        d();
    }

    /* access modifiers changed from: protected */
    public void a(bz bzVar, Exception exc) {
        c();
        d();
        if (this.i != null) {
            this.i.a(a(), bzVar, exc);
        }
    }

    private void c() {
        bg a2 = a();
        if (a2 == null || !a2.a()) {
            this.e = null;
        } else if (a2 != this.f) {
            ap a3 = ap.a(a2, new bu(this, a2));
            Bundle bundle = new Bundle();
            bundle.putString("fields", f1849a);
            a3.a(bundle);
            ap.b(a3);
            this.f = a2;
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (isAdded()) {
            if (b()) {
                this.d.setTextColor(getResources().getColor(b.com_facebook_usersettingsfragment_connected_text_color));
                this.d.setShadowLayer(1.0f, 0.0f, -1.0f, getResources().getColor(b.com_facebook_usersettingsfragment_connected_shadow_color));
                if (this.e != null) {
                    ad e2 = e();
                    if (e2 != null) {
                        URL b2 = e2.b();
                        if (!b2.equals(this.d.getTag())) {
                            if (this.e.a().equals(this.h)) {
                                this.d.setCompoundDrawables(null, this.g, null, null);
                                this.d.setTag(b2);
                            } else {
                                x.a(e2);
                            }
                        }
                    }
                    this.d.setText(this.e.b());
                    return;
                }
                this.d.setText(getResources().getString(g.com_facebook_usersettingsfragment_logged_in));
                Drawable drawable = getResources().getDrawable(d.com_facebook_profile_default_icon);
                drawable.setBounds(0, 0, getResources().getDimensionPixelSize(c.com_facebook_usersettingsfragment_profile_picture_width), getResources().getDimensionPixelSize(c.com_facebook_usersettingsfragment_profile_picture_height));
                this.d.setCompoundDrawables(null, drawable, null, null);
                return;
            }
            int color = getResources().getColor(b.com_facebook_usersettingsfragment_not_connected_text_color);
            this.d.setTextColor(color);
            this.d.setShadowLayer(0.0f, 0.0f, 0.0f, color);
            this.d.setText(getResources().getString(g.com_facebook_usersettingsfragment_not_logged_in));
            this.d.setCompoundDrawables(null, null, null, null);
            this.d.setTag(null);
        }
    }

    private ad e() {
        try {
            return new af(getActivity(), ad.a(this.e.a(), getResources().getDimensionPixelSize(c.com_facebook_usersettingsfragment_profile_picture_width), getResources().getDimensionPixelSize(c.com_facebook_usersettingsfragment_profile_picture_height))).a(this).a((ag) new bv(this)).a();
        } catch (MalformedURLException e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, ah ahVar) {
        Bitmap c2;
        if (ahVar != null && (c2 = ahVar.c()) != null) {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), c2);
            bitmapDrawable.setBounds(0, 0, getResources().getDimensionPixelSize(c.com_facebook_usersettingsfragment_profile_picture_width), getResources().getDimensionPixelSize(c.com_facebook_usersettingsfragment_profile_picture_height));
            this.g = bitmapDrawable;
            this.h = str;
            this.d.setCompoundDrawables(null, bitmapDrawable, null, null);
            this.d.setTag(ahVar.a().b());
        }
    }
}
