package com.facebook.widget;

import android.content.Context;
import android.os.Bundle;
import com.facebook.b.v;
import com.facebook.bg;
import com.facebook.widget.cb;

/* compiled from: WebDialog */
class cb<CONCRETE extends cb<?>> {

    /* renamed from: a  reason: collision with root package name */
    private Context f1921a;

    /* renamed from: b  reason: collision with root package name */
    private bg f1922b;

    /* renamed from: c  reason: collision with root package name */
    private String f1923c;
    private String d;
    private int e = 16973840;
    private cd f;
    private Bundle g;

    protected cb(Context context, String str, String str2, Bundle bundle) {
        v.a(str, "applicationId");
        this.f1923c = str;
        a(context, str2, bundle);
    }

    public CONCRETE a(cd cdVar) {
        this.f = cdVar;
        return this;
    }

    public bw a() {
        if (this.f1922b == null || !this.f1922b.a()) {
            this.g.putString("app_id", this.f1923c);
        } else {
            this.g.putString("app_id", this.f1922b.c());
            this.g.putString("access_token", this.f1922b.d());
        }
        if (!this.g.containsKey("redirect_uri")) {
            this.g.putString("redirect_uri", "fbconnect://success");
        }
        return new bw(this.f1921a, this.d, this.g, this.e, this.f);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return this.f1923c;
    }

    /* access modifiers changed from: protected */
    public Context c() {
        return this.f1921a;
    }

    /* access modifiers changed from: protected */
    public int d() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public Bundle e() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public cd f() {
        return this.f;
    }

    private void a(Context context, String str, Bundle bundle) {
        this.f1921a = context;
        this.d = str;
        if (bundle != null) {
            this.g = bundle;
        } else {
            this.g = new Bundle();
        }
    }
}
