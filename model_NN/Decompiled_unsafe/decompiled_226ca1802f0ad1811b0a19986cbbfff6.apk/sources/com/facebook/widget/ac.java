package com.facebook.widget;

import java.net.URL;

/* compiled from: ImageDownloader */
class ac {

    /* renamed from: a  reason: collision with root package name */
    URL f1858a;

    /* renamed from: b  reason: collision with root package name */
    Object f1859b;

    ac(URL url, Object obj) {
        this.f1858a = url;
        this.f1859b = obj;
    }

    public int hashCode() {
        return ((this.f1858a.hashCode() + 1073) * 37) + this.f1859b.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ac)) {
            return false;
        }
        ac acVar = (ac) obj;
        if (acVar.f1858a == this.f1858a && acVar.f1859b == this.f1859b) {
            return true;
        }
        return false;
    }
}
