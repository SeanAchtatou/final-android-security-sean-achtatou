package com.facebook.widget;

import com.facebook.c.b;
import java.text.Collator;
import java.util.Comparator;

/* compiled from: GraphObjectAdapter */
class g implements Comparator<b> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Collator f1938a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ f f1939b;

    g(f fVar, Collator collator) {
        this.f1939b = fVar;
        this.f1938a = collator;
    }

    /* renamed from: a */
    public int compare(b bVar, b bVar2) {
        return f.b(bVar, bVar2, this.f1939b.h, this.f1938a);
    }
}
