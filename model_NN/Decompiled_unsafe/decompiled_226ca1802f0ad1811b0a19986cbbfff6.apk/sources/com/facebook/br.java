package com.facebook;

import android.content.Context;

/* compiled from: Session */
public final class br {

    /* renamed from: a  reason: collision with root package name */
    private final Context f1769a;

    /* renamed from: b  reason: collision with root package name */
    private String f1770b;

    /* renamed from: c  reason: collision with root package name */
    private ce f1771c;

    public br(Context context) {
        this.f1769a = context;
    }

    public br a(String str) {
        this.f1770b = str;
        return this;
    }

    public bg a() {
        return new bg(this.f1769a, this.f1770b, this.f1771c);
    }
}
