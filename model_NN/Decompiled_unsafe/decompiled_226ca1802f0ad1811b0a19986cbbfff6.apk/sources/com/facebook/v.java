package com.facebook;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import com.facebook.b.u;
import com.facebook.widget.bw;
import com.facebook.widget.ca;

/* compiled from: AuthorizationClient */
class v extends j {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ c f1834b;

    /* renamed from: c  reason: collision with root package name */
    private transient bw f1835c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    v(c cVar) {
        super(cVar);
        this.f1834b = cVar;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.f1835c != null) {
            this.f1835c.dismiss();
            this.f1835c = null;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(k kVar) {
        String f = kVar.f();
        Bundle bundle = new Bundle();
        if (!u.a(kVar.b())) {
            bundle.putString("scope", TextUtils.join(",", kVar.b()));
        }
        String h = kVar.h();
        if (u.a(h) || !h.equals(d())) {
            u.b(this.f1834b.f1788c);
        } else {
            bundle.putString("access_token", h);
        }
        this.f1835c = ((ca) new i(this.f1834b.g().a(), f, bundle).a(new w(this, kVar))).a();
        this.f1835c.show();
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(k kVar, Bundle bundle, z zVar) {
        s a2;
        if (bundle != null) {
            a a3 = a.a(kVar.b(), bundle, b.WEB_VIEW);
            a2 = s.a(a3);
            CookieSyncManager.createInstance(this.f1834b.f1788c).sync();
            a(a3.a());
        } else if (zVar instanceof ab) {
            a2 = s.a("User canceled log in.");
        } else {
            a2 = s.a(zVar.getMessage(), null);
        }
        this.f1834b.a(a2);
    }

    private void a(String str) {
        SharedPreferences.Editor edit = this.f1834b.g().a().getSharedPreferences("com.facebook.AuthorizationClient.WebViewAuthHandler.TOKEN_STORE_KEY", 0).edit();
        edit.putString("TOKEN", str);
        if (!edit.commit()) {
            u.b("Facebook-AuthorizationClient", "Could not update saved web view auth handler token.");
        }
    }

    private String d() {
        return this.f1834b.g().a().getSharedPreferences("com.facebook.AuthorizationClient.WebViewAuthHandler.TOKEN_STORE_KEY", 0).getString("TOKEN", "");
    }
}
