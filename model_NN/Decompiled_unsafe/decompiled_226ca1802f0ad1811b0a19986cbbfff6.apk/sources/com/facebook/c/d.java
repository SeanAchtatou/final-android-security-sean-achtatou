package com.facebook.c;

import com.facebook.aa;
import com.facebook.b.v;
import java.util.AbstractList;
import java.util.Collection;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: GraphObject */
final class d<T> extends AbstractList<T> implements g<T> {

    /* renamed from: a  reason: collision with root package name */
    private final JSONArray f1791a;

    /* renamed from: b  reason: collision with root package name */
    private final Class<?> f1792b;

    public d(JSONArray jSONArray, Class<?> cls) {
        v.a(jSONArray, "state");
        v.a(cls, "itemType");
        this.f1791a = jSONArray;
        this.f1792b = cls;
    }

    public String toString() {
        return String.format("GraphObjectList{itemType=%s, state=%s}", this.f1792b.getSimpleName(), this.f1791a);
    }

    public void add(int i, T t) {
        if (i < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i < size()) {
            throw new UnsupportedOperationException("Only adding items at the end of the list is supported.");
        } else {
            a(i, t);
        }
    }

    public T set(int i, T t) {
        a(i);
        T t2 = get(i);
        a(i, t);
        return t2;
    }

    public int hashCode() {
        return this.f1791a.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.f1791a.equals(((d) obj).f1791a);
    }

    public T get(int i) {
        a(i);
        return c.a(this.f1791a.opt(i), this.f1792b, null);
    }

    public int size() {
        return this.f1791a.length();
    }

    public final <U extends b> g<U> a(Class<U> cls) {
        if (b.class.isAssignableFrom(this.f1792b)) {
            return cls.isAssignableFrom(this.f1792b) ? this : c.a(this.f1791a, cls);
        }
        throw new aa("Can't cast GraphObjectCollection of non-GraphObject type " + this.f1792b);
    }

    public final JSONArray a() {
        return this.f1791a;
    }

    public void clear() {
        throw new UnsupportedOperationException();
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    private void a(int i) {
        if (i < 0 || i >= this.f1791a.length()) {
            throw new IndexOutOfBoundsException();
        }
    }

    private void a(int i, T t) {
        try {
            this.f1791a.put(i, c.b((Object) t));
        } catch (JSONException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
