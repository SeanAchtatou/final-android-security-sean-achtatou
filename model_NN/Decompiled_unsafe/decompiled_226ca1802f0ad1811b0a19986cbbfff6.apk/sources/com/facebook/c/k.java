package com.facebook.c;

import android.annotation.SuppressLint;
import java.util.Map;

/* compiled from: JsonUtil */
final class k implements Map.Entry<String, Object> {

    /* renamed from: a  reason: collision with root package name */
    private final String f1795a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f1796b;

    k(String str, Object obj) {
        this.f1795a = str;
        this.f1796b = obj;
    }

    @SuppressLint({"FieldGetter"})
    /* renamed from: a */
    public String getKey() {
        return this.f1795a;
    }

    public Object getValue() {
        return this.f1796b;
    }

    public Object setValue(Object obj) {
        throw new UnsupportedOperationException("JSONObjectEntry is immutable");
    }
}
