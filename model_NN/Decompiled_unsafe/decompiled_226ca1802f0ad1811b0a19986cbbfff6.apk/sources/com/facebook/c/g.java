package com.facebook.c;

import java.util.List;
import org.json.JSONArray;

/* compiled from: GraphObjectList */
public interface g<T> extends List<T> {
    <U extends b> g<U> a(Class<U> cls);

    JSONArray a();
}
