package com.facebook.c;

import com.facebook.aa;
import com.facebook.b.u;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: GraphObject */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final HashSet<Class<?>> f1789a = new HashSet<>();

    /* renamed from: b  reason: collision with root package name */
    private static final SimpleDateFormat[] f1790b = {new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US), new SimpleDateFormat("yyyy-MM-dd", Locale.US)};

    public static b a(JSONObject jSONObject) {
        return a(jSONObject, b.class);
    }

    public static <T extends b> T a(JSONObject jSONObject, Class cls) {
        return b(cls, jSONObject);
    }

    public static b a() {
        return a(b.class);
    }

    public static <T extends b> T a(Class cls) {
        return b(cls, new JSONObject());
    }

    public static boolean a(b bVar, b bVar2) {
        if (bVar == null || bVar2 == null || !bVar.c().containsKey("id") || !bVar2.c().containsKey("id")) {
            return false;
        }
        if (bVar.equals(bVar2)) {
            return true;
        }
        Object a2 = bVar.a("id");
        Object a3 = bVar2.a("id");
        if (a2 == null || a3 == null || !(a2 instanceof String) || !(a3 instanceof String)) {
            return false;
        }
        return a2.equals(a3);
    }

    public static <T> g<T> a(JSONArray jSONArray, Class cls) {
        return new d(jSONArray, cls);
    }

    /* access modifiers changed from: private */
    public static <T extends b> T b(Class<T> cls, JSONObject jSONObject) {
        d(cls);
        return (b) Proxy.newProxyInstance(b.class.getClassLoader(), new Class[]{cls}, new e(jSONObject, cls));
    }

    /* access modifiers changed from: private */
    public static Map<String, Object> c(JSONObject jSONObject) {
        return (Map) Proxy.newProxyInstance(b.class.getClassLoader(), new Class[]{Map.class}, new e(jSONObject, Map.class));
    }

    private static synchronized <T extends b> boolean b(Class<T> cls) {
        boolean contains;
        synchronized (c.class) {
            contains = f1789a.contains(cls);
        }
        return contains;
    }

    private static synchronized <T extends b> void c(Class<T> cls) {
        synchronized (c.class) {
            f1789a.add(cls);
        }
    }

    private static <T extends b> void d(Class<T> cls) {
        if (!b((Class) cls)) {
            if (!cls.isInterface()) {
                throw new aa("Factory can only wrap interfaces, not class: " + cls.getName());
            }
            for (Method method : cls.getMethods()) {
                String name = method.getName();
                int length = method.getParameterTypes().length;
                Class<?> returnType = method.getReturnType();
                boolean isAnnotationPresent = method.isAnnotationPresent(l.class);
                if (!method.getDeclaringClass().isAssignableFrom(b.class)) {
                    if (length == 1 && returnType == Void.TYPE) {
                        if (isAnnotationPresent) {
                            if (u.a(((l) method.getAnnotation(l.class)).a())) {
                            }
                        } else if (name.startsWith("set") && name.length() > 3) {
                        }
                    } else if (length == 0 && returnType != Void.TYPE) {
                        if (isAnnotationPresent) {
                            if (!u.a(((l) method.getAnnotation(l.class)).a())) {
                            }
                        } else if (name.startsWith("get") && name.length() > 3) {
                        }
                    }
                    throw new aa("Factory can't proxy method: " + method.toString());
                }
            }
            c(cls);
        }
    }

    static <U> U a(Object obj, Class<U> cls, ParameterizedType parameterizedType) {
        if (obj == null) {
            return null;
        }
        Class<?> cls2 = obj.getClass();
        if (cls.isAssignableFrom(cls2) || cls.isPrimitive()) {
            return obj;
        }
        if (b.class.isAssignableFrom(cls)) {
            if (JSONObject.class.isAssignableFrom(cls2)) {
                return b(cls, (JSONObject) obj);
            }
            if (b.class.isAssignableFrom(cls2)) {
                return ((b) obj).a(cls);
            }
            throw new aa("Can't create GraphObject from " + cls2.getName());
        } else if (!Iterable.class.equals(cls) && !Collection.class.equals(cls) && !List.class.equals(cls) && !g.class.equals(cls)) {
            if (String.class.equals(cls)) {
                if (Double.class.isAssignableFrom(cls2) || Float.class.isAssignableFrom(cls2)) {
                    return String.format("%f", obj);
                } else if (Number.class.isAssignableFrom(cls2)) {
                    return String.format("%d", obj);
                }
            } else if (Date.class.equals(cls) && String.class.isAssignableFrom(cls2)) {
                SimpleDateFormat[] simpleDateFormatArr = f1790b;
                int length = simpleDateFormatArr.length;
                int i = 0;
                while (i < length) {
                    try {
                        U parse = simpleDateFormatArr[i].parse((String) obj);
                        if (parse != null) {
                            return parse;
                        }
                        i++;
                    } catch (ParseException e) {
                    }
                }
            }
            throw new aa("Can't convert type" + cls2.getName() + " to " + cls.getName());
        } else if (parameterizedType == null) {
            throw new aa("can't infer generic type of: " + cls.toString());
        } else {
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            if (actualTypeArguments == null || actualTypeArguments.length != 1 || !(actualTypeArguments[0] instanceof Class)) {
                throw new aa("Expect collection properties to be of a type with exactly one generic parameter.");
            }
            Class cls3 = (Class) actualTypeArguments[0];
            if (JSONArray.class.isAssignableFrom(cls2)) {
                return a((JSONArray) obj, cls3);
            }
            throw new aa("Can't create Collection from " + cls2.getName());
        }
    }

    static String a(String str) {
        return str.replaceAll("([a-z])([A-Z])", "$1_$2").toLowerCase(Locale.US);
    }

    /* access modifiers changed from: private */
    public static Object b(Object obj) {
        Class<?> cls = obj.getClass();
        if (b.class.isAssignableFrom(cls)) {
            return ((b) obj).d();
        }
        if (g.class.isAssignableFrom(cls)) {
            return ((g) obj).a();
        }
        return obj;
    }
}
