package com.facebook.c;

import com.facebook.aa;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/* compiled from: GraphObject */
abstract class f<STATE> implements InvocationHandler {

    /* renamed from: a  reason: collision with root package name */
    protected final STATE f1794a;

    protected f(STATE state) {
        this.f1794a = state;
    }

    /* access modifiers changed from: protected */
    public final Object a(Method method) {
        throw new aa(getClass().getName() + " got an unexpected method signature: " + method.toString());
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: STATE
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected final java.lang.Object a(java.lang.Object r4, java.lang.reflect.Method r5, java.lang.Object[] r6) {
        /*
            r3 = this;
            r2 = 0
            java.lang.String r0 = r5.getName()
            java.lang.String r1 = "equals"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0032
            r0 = r6[r2]
            if (r0 != 0) goto L_0x0016
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)
        L_0x0015:
            return r0
        L_0x0016:
            java.lang.reflect.InvocationHandler r0 = java.lang.reflect.Proxy.getInvocationHandler(r0)
            boolean r1 = r0 instanceof com.facebook.c.e
            if (r1 != 0) goto L_0x0023
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)
            goto L_0x0015
        L_0x0023:
            com.facebook.c.e r0 = (com.facebook.c.e) r0
            STATE r1 = r3.f1794a
            java.lang.Object r0 = r0.f1794a
            boolean r0 = r1.equals(r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            goto L_0x0015
        L_0x0032:
            java.lang.String r1 = "toString"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x003f
            java.lang.String r0 = r3.toString()
            goto L_0x0015
        L_0x003f:
            STATE r0 = r3.f1794a
            java.lang.Object r0 = r5.invoke(r0, r6)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.c.f.a(java.lang.Object, java.lang.reflect.Method, java.lang.Object[]):java.lang.Object");
    }
}
