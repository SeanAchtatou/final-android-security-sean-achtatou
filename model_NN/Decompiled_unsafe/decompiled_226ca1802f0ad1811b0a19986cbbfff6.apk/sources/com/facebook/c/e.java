package com.facebook.c;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: GraphObject */
final class e extends f<JSONObject> {

    /* renamed from: b  reason: collision with root package name */
    private final Class<?> f1793b;

    public e(JSONObject jSONObject, Class<?> cls) {
        super(jSONObject);
        this.f1793b = cls;
    }

    public String toString() {
        return String.format("GraphObject{graphObjectClass=%s, state=%s}", this.f1793b.getSimpleName(), this.f1794a);
    }

    public final Object invoke(Object obj, Method method, Object[] objArr) {
        Class<?> declaringClass = method.getDeclaringClass();
        if (declaringClass == Object.class) {
            return a(obj, method, objArr);
        }
        if (declaringClass == Map.class) {
            return a(method, objArr);
        }
        if (declaringClass == b.class) {
            return b(obj, method, objArr);
        }
        if (b.class.isAssignableFrom(declaringClass)) {
            return b(method, objArr);
        }
        return a(method);
    }

    private final Object a(Method method, Object[] objArr) {
        Map<String, Object> map;
        String name = method.getName();
        if (name.equals("clear")) {
            j.a((JSONObject) this.f1794a);
            return null;
        } else if (name.equals("containsKey")) {
            return Boolean.valueOf(((JSONObject) this.f1794a).has((String) objArr[0]));
        } else {
            if (name.equals("containsValue")) {
                return Boolean.valueOf(j.a((JSONObject) this.f1794a, objArr[0]));
            }
            if (name.equals("entrySet")) {
                return j.b((JSONObject) this.f1794a);
            }
            if (name.equals("get")) {
                return ((JSONObject) this.f1794a).opt((String) objArr[0]);
            }
            if (name.equals("isEmpty")) {
                return Boolean.valueOf(((JSONObject) this.f1794a).length() == 0);
            } else if (name.equals("keySet")) {
                return j.c((JSONObject) this.f1794a);
            } else {
                if (name.equals("put")) {
                    return a(objArr);
                }
                if (name.equals("putAll")) {
                    if (objArr[0] instanceof Map) {
                        map = (Map) objArr[0];
                    } else if (objArr[0] instanceof b) {
                        map = ((b) objArr[0]).c();
                    } else {
                        map = null;
                    }
                    j.a((JSONObject) this.f1794a, map);
                    return null;
                } else if (name.equals("remove")) {
                    ((JSONObject) this.f1794a).remove((String) objArr[0]);
                    return null;
                } else if (name.equals("size")) {
                    return Integer.valueOf(((JSONObject) this.f1794a).length());
                } else {
                    if (name.equals("values")) {
                        return j.d((JSONObject) this.f1794a);
                    }
                    return a(method);
                }
            }
        }
    }

    private final Object b(Object obj, Method method, Object[] objArr) {
        String name = method.getName();
        if (name.equals("cast")) {
            Class cls = (Class) objArr[0];
            if (cls == null || !cls.isAssignableFrom(this.f1793b)) {
                return c.b(cls, (JSONObject) this.f1794a);
            }
            return obj;
        } else if (name.equals("getInnerJSONObject")) {
            return ((e) Proxy.getInvocationHandler(obj)).f1794a;
        } else {
            if (name.equals("asMap")) {
                return c.c((JSONObject) this.f1794a);
            }
            if (name.equals("getProperty")) {
                return ((JSONObject) this.f1794a).opt((String) objArr[0]);
            }
            if (name.equals("setProperty")) {
                return a(objArr);
            }
            if (!name.equals("removeProperty")) {
                return a(method);
            }
            ((JSONObject) this.f1794a).remove((String) objArr[0]);
            return null;
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:31:0x005a */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v7, resolved type: org.json.JSONArray} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: org.json.JSONArray} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: org.json.JSONArray} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v10, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v20, resolved type: org.json.JSONObject} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: org.json.JSONArray} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.lang.Object b(java.lang.reflect.Method r8, java.lang.Object[] r9) {
        /*
            r7 = this;
            r1 = 0
            java.lang.String r2 = r8.getName()
            java.lang.Class[] r0 = r8.getParameterTypes()
            int r4 = r0.length
            java.lang.Class<com.facebook.c.l> r0 = com.facebook.c.l.class
            java.lang.annotation.Annotation r0 = r8.getAnnotation(r0)
            com.facebook.c.l r0 = (com.facebook.c.l) r0
            if (r0 == 0) goto L_0x0036
            java.lang.String r0 = r0.a()
            r3 = r0
        L_0x0019:
            if (r4 != 0) goto L_0x0041
            java.lang.Object r0 = r7.f1794a
            org.json.JSONObject r0 = (org.json.JSONObject) r0
            java.lang.Object r2 = r0.opt(r3)
            java.lang.Class r3 = r8.getReturnType()
            java.lang.reflect.Type r0 = r8.getGenericReturnType()
            boolean r4 = r0 instanceof java.lang.reflect.ParameterizedType
            if (r4 == 0) goto L_0x00b8
            java.lang.reflect.ParameterizedType r0 = (java.lang.reflect.ParameterizedType) r0
        L_0x0031:
            java.lang.Object r1 = com.facebook.c.c.a(r2, r3, r0)
        L_0x0035:
            return r1
        L_0x0036:
            r0 = 3
            java.lang.String r0 = r2.substring(r0)
            java.lang.String r0 = com.facebook.c.c.a(r0)
            r3 = r0
            goto L_0x0019
        L_0x0041:
            r0 = 1
            if (r4 != r0) goto L_0x00b1
            r0 = 0
            r0 = r9[r0]
            java.lang.Class<com.facebook.c.b> r2 = com.facebook.c.b.class
            java.lang.Class r4 = r0.getClass()
            boolean r2 = r2.isAssignableFrom(r4)
            if (r2 == 0) goto L_0x0062
            com.facebook.c.b r0 = (com.facebook.c.b) r0
            org.json.JSONObject r0 = r0.d()
            r2 = r0
        L_0x005a:
            java.lang.Object r0 = r7.f1794a
            org.json.JSONObject r0 = (org.json.JSONObject) r0
            r0.putOpt(r3, r2)
            goto L_0x0035
        L_0x0062:
            java.lang.Class<com.facebook.c.g> r2 = com.facebook.c.g.class
            java.lang.Class r4 = r0.getClass()
            boolean r2 = r2.isAssignableFrom(r4)
            if (r2 == 0) goto L_0x0076
            com.facebook.c.g r0 = (com.facebook.c.g) r0
            org.json.JSONArray r0 = r0.a()
            r2 = r0
            goto L_0x005a
        L_0x0076:
            java.lang.Class<java.lang.Iterable> r2 = java.lang.Iterable.class
            java.lang.Class r4 = r0.getClass()
            boolean r2 = r2.isAssignableFrom(r4)
            if (r2 == 0) goto L_0x00b6
            org.json.JSONArray r2 = new org.json.JSONArray
            r2.<init>()
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            java.util.Iterator r4 = r0.iterator()
        L_0x008d:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x005a
            java.lang.Object r0 = r4.next()
            java.lang.Class<com.facebook.c.b> r5 = com.facebook.c.b.class
            java.lang.Class r6 = r0.getClass()
            boolean r5 = r5.isAssignableFrom(r6)
            if (r5 == 0) goto L_0x00ad
            com.facebook.c.b r0 = (com.facebook.c.b) r0
            org.json.JSONObject r0 = r0.d()
            r2.put(r0)
            goto L_0x008d
        L_0x00ad:
            r2.put(r0)
            goto L_0x008d
        L_0x00b1:
            java.lang.Object r1 = r7.a(r8)
            goto L_0x0035
        L_0x00b6:
            r2 = r0
            goto L_0x005a
        L_0x00b8:
            r0 = r1
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.c.e.b(java.lang.reflect.Method, java.lang.Object[]):java.lang.Object");
    }

    private Object a(Object[] objArr) {
        try {
            ((JSONObject) this.f1794a).putOpt((String) objArr[0], c.b(objArr[1]));
            return null;
        } catch (JSONException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
