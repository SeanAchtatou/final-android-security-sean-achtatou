package com.facebook;

/* compiled from: SessionDefaultAudience */
public enum bx {
    NONE(null),
    ONLY_ME("SELF"),
    FRIENDS("ALL_FRIENDS"),
    EVERYONE("EVERYONE");
    
    private final String e;

    private bx(String str) {
        this.e = str;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.e;
    }
}
