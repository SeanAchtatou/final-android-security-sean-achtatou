package com.facebook;

/* compiled from: SessionState */
public enum bz {
    CREATED(ca.CREATED_CATEGORY),
    CREATED_TOKEN_LOADED(ca.CREATED_CATEGORY),
    OPENING(ca.CREATED_CATEGORY),
    OPENED(ca.OPENED_CATEGORY),
    OPENED_TOKEN_UPDATED(ca.OPENED_CATEGORY),
    CLOSED_LOGIN_FAILED(ca.CLOSED_CATEGORY),
    CLOSED(ca.CLOSED_CATEGORY);
    
    private final ca h;

    private bz(ca caVar) {
        this.h = caVar;
    }

    public boolean a() {
        return this.h == ca.OPENED_CATEGORY;
    }

    public boolean b() {
        return this.h == ca.CLOSED_CATEGORY;
    }
}
