package com.facebook;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

/* compiled from: Session */
class bw extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<bg> f1775a;

    /* renamed from: b  reason: collision with root package name */
    private WeakReference<bv> f1776b;

    bw(bg bgVar, bv bvVar) {
        super(Looper.getMainLooper());
        this.f1775a = new WeakReference<>(bgVar);
        this.f1776b = new WeakReference<>(bvVar);
    }

    public void handleMessage(Message message) {
        String string = message.getData().getString("access_token");
        bg bgVar = this.f1775a.get();
        if (!(bgVar == null || string == null)) {
            bgVar.a(message.getData());
        }
        bv bvVar = this.f1776b.get();
        if (bvVar != null) {
            bg.d.unbindService(bvVar);
            bvVar.b();
        }
    }
}
