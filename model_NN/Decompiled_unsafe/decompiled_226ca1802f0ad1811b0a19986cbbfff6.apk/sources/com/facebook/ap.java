package com.facebook;

import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.b.n;
import com.facebook.b.u;
import com.facebook.b.v;
import com.facebook.c.a;
import com.facebook.c.b;
import com.facebook.c.g;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Request */
public class ap {

    /* renamed from: a  reason: collision with root package name */
    private static String f1685a;
    private static volatile String m;

    /* renamed from: b  reason: collision with root package name */
    private bg f1686b;

    /* renamed from: c  reason: collision with root package name */
    private ak f1687c;
    private String d;
    private b e;
    private String f;
    private String g;
    private String h;
    private boolean i;
    private Bundle j;
    private au k;
    private String l;

    public ap() {
        this(null, null, null, null, null);
    }

    public ap(bg bgVar, String str, Bundle bundle, ak akVar, au auVar) {
        this.i = true;
        this.f1686b = bgVar;
        this.d = str;
        this.k = auVar;
        a(akVar);
        if (bundle != null) {
            this.j = new Bundle(bundle);
        } else {
            this.j = new Bundle();
        }
        if (!this.j.containsKey("migration_bundle")) {
            this.j.putString("migration_bundle", "fbsdk:20121026");
        }
    }

    ap(bg bgVar, URL url) {
        this.i = true;
        this.f1686b = bgVar;
        this.l = url.toString();
        a(ak.GET);
        this.j = new Bundle();
    }

    public static ap a(bg bgVar, String str, b bVar, au auVar) {
        ap apVar = new ap(bgVar, str, null, ak.POST, auVar);
        apVar.a(bVar);
        return apVar;
    }

    public static ap a(bg bgVar, aw awVar) {
        return new ap(bgVar, "me", null, null, new aq(awVar));
    }

    public static ap a(bg bgVar, String str, au auVar) {
        return new ap(bgVar, str, null, null, auVar);
    }

    public static ap a(bg bgVar, Location location, int i2, int i3, String str, av avVar) {
        if (location != null || !u.a(str)) {
            Bundle bundle = new Bundle(5);
            bundle.putString("type", "place");
            bundle.putInt("limit", i3);
            if (location != null) {
                bundle.putString("center", String.format(Locale.US, "%f,%f", Double.valueOf(location.getLatitude()), Double.valueOf(location.getLongitude())));
                bundle.putInt("distance", i2);
            }
            if (!u.a(str)) {
                bundle.putString("q", str);
            }
            return new ap(bgVar, "search", bundle, ak.GET, new ar(avVar));
        }
        throw new z("Either location or searchText must be specified.");
    }

    public final void a(b bVar) {
        this.e = bVar;
    }

    public final void a(ak akVar) {
        if (this.l == null || akVar == ak.GET) {
            if (akVar == null) {
                akVar = ak.GET;
            }
            this.f1687c = akVar;
            return;
        }
        throw new z("Can't change HTTP method on request with overridden URL.");
    }

    public final Bundle a() {
        return this.j;
    }

    public final void a(Bundle bundle) {
        this.j = bundle;
    }

    public final bg b() {
        return this.f1686b;
    }

    public final void a(au auVar) {
        this.k = auVar;
    }

    public final bc c() {
        return a(this);
    }

    public static HttpURLConnection a(ba baVar) {
        URL url;
        Iterator it = baVar.iterator();
        while (it.hasNext()) {
            ((ap) it.next()).g();
        }
        try {
            if (baVar.size() == 1) {
                url = new URL(baVar.get(0).e());
            } else {
                url = new URL("https://graph.facebook.com");
            }
            try {
                HttpURLConnection a2 = a(url);
                a(baVar, a2);
                return a2;
            } catch (IOException e2) {
                throw new z("could not construct request body", e2);
            } catch (JSONException e3) {
                throw new z("could not construct request body", e3);
            }
        } catch (MalformedURLException e4) {
            throw new z("could not construct URL for request", e4);
        }
    }

    public static bc a(ap apVar) {
        List<bc> a2 = a(apVar);
        if (a2 != null && a2.size() == 1) {
            return a2.get(0);
        }
        throw new z("invalid state: expected a single response");
    }

    public static List<bc> a(ap... apVarArr) {
        v.a(apVarArr, "requests");
        return a((Collection<ap>) Arrays.asList(apVarArr));
    }

    public static List<bc> a(Collection<ap> collection) {
        return b(new ba(collection));
    }

    public static List<bc> b(ba baVar) {
        v.c(baVar, "requests");
        try {
            return a(a(baVar), baVar);
        } catch (Exception e2) {
            List<bc> a2 = bc.a(baVar.d(), null, new z(e2));
            a(baVar, a2);
            return a2;
        }
    }

    public static az b(ap... apVarArr) {
        v.a(apVarArr, "requests");
        return b((Collection<ap>) Arrays.asList(apVarArr));
    }

    public static az b(Collection<ap> collection) {
        return c(new ba(collection));
    }

    public static az c(ba baVar) {
        v.c(baVar, "requests");
        az azVar = new az(baVar);
        azVar.a();
        return azVar;
    }

    public static List<bc> a(HttpURLConnection httpURLConnection, ba baVar) {
        List<bc> a2 = bc.a(httpURLConnection, baVar);
        u.a(httpURLConnection);
        int size = baVar.size();
        if (size != a2.size()) {
            throw new z(String.format("Received %d responses while expecting %d", Integer.valueOf(a2.size()), Integer.valueOf(size)));
        }
        a(baVar, a2);
        HashSet hashSet = new HashSet();
        Iterator it = baVar.iterator();
        while (it.hasNext()) {
            ap apVar = (ap) it.next();
            if (apVar.f1686b != null) {
                hashSet.add(apVar.f1686b);
            }
        }
        Iterator it2 = hashSet.iterator();
        while (it2.hasNext()) {
            ((bg) it2.next()).k();
        }
        return a2;
    }

    public String toString() {
        return "{Request: " + " session: " + this.f1686b + ", graphPath: " + this.d + ", graphObject: " + this.e + ", restMethod: " + this.f + ", httpMethod: " + this.f1687c + ", parameters: " + this.j + "}";
    }

    static void a(ba baVar, List<bc> list) {
        int size = baVar.size();
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < size; i2++) {
            ap a2 = baVar.get(i2);
            if (a2.k != null) {
                arrayList.add(new Pair(a2.k, list.get(i2)));
            }
        }
        if (arrayList.size() > 0) {
            as asVar = new as(arrayList, baVar);
            Handler c2 = baVar.c();
            if (c2 == null) {
                asVar.run();
            } else {
                c2.post(asVar);
            }
        }
    }

    static HttpURLConnection a(URL url) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestProperty("User-Agent", i());
        httpURLConnection.setRequestProperty("Content-Type", h());
        httpURLConnection.setChunkedStreamingMode(0);
        return httpURLConnection;
    }

    private void f() {
        if (this.f1686b != null) {
            if (!this.f1686b.a()) {
                throw new z("Session provided to a Request in un-opened state.");
            } else if (!this.j.containsKey("access_token")) {
                String d2 = this.f1686b.d();
                n.a(d2);
                this.j.putString("access_token", d2);
            }
        }
        this.j.putString("sdk", "android");
        this.j.putString("format", "json");
    }

    private String a(String str) {
        Uri.Builder encodedPath = new Uri.Builder().encodedPath(str);
        for (String next : this.j.keySet()) {
            Object obj = this.j.get(next);
            if (obj == null) {
                obj = "";
            }
            if (d(obj)) {
                encodedPath.appendQueryParameter(next, e(obj).toString());
            } else if (this.f1687c == ak.GET) {
                throw new IllegalArgumentException(String.format("Unsupported parameter type for GET request: %s", obj.getClass().getSimpleName()));
            }
        }
        return encodedPath.toString();
    }

    /* access modifiers changed from: package-private */
    public final String d() {
        String str;
        if (this.l != null) {
            throw new z("Can't override URL for a batch request");
        }
        if (this.f != null) {
            str = "method/" + this.f;
        } else {
            str = this.d;
        }
        f();
        return a(str);
    }

    /* access modifiers changed from: package-private */
    public final String e() {
        String str;
        if (this.l != null) {
            return this.l.toString();
        }
        if (this.f != null) {
            str = "https://api.facebook.com/method/" + this.f;
        } else {
            str = "https://graph.facebook.com/" + this.d;
        }
        f();
        return a(str);
    }

    private void a(JSONArray jSONArray, Bundle bundle) {
        JSONObject jSONObject = new JSONObject();
        if (this.g != null) {
            jSONObject.put("name", this.g);
            jSONObject.put("omit_response_on_success", this.i);
        }
        if (this.h != null) {
            jSONObject.put("depends_on", this.h);
        }
        String d2 = d();
        jSONObject.put("relative_url", d2);
        jSONObject.put("method", this.f1687c);
        if (this.f1686b != null) {
            n.a(this.f1686b.d());
        }
        ArrayList arrayList = new ArrayList();
        for (String str : this.j.keySet()) {
            Object obj = this.j.get(str);
            if (c(obj)) {
                String format = String.format("%s%d", "file", Integer.valueOf(bundle.size()));
                arrayList.add(format);
                u.a(bundle, format, obj);
            }
        }
        if (!arrayList.isEmpty()) {
            jSONObject.put("attached_files", TextUtils.join(",", arrayList));
        }
        if (this.e != null) {
            ArrayList arrayList2 = new ArrayList();
            a(this.e, d2, new at(this, arrayList2));
            jSONObject.put("body", TextUtils.join("&", arrayList2));
        }
        jSONArray.put(jSONObject);
    }

    private void g() {
        if (this.d != null && this.f != null) {
            throw new IllegalArgumentException("Only one of a graph path or REST method may be specified per request.");
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.n.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.b.n.a(java.lang.String, java.lang.String):void
      com.facebook.b.n.a(java.lang.String, java.lang.Object[]):void
      com.facebook.b.n.a(java.lang.String, java.lang.Object):void */
    static final void a(ba baVar, HttpURLConnection httpURLConnection) {
        n nVar = new n(al.REQUESTS, "Request");
        int size = baVar.size();
        ak akVar = size == 1 ? baVar.get(0).f1687c : ak.POST;
        httpURLConnection.setRequestMethod(akVar.name());
        URL url = httpURLConnection.getURL();
        nVar.c("Request:\n");
        nVar.a("Id", (Object) baVar.b());
        nVar.a("URL", url);
        nVar.a("Method", (Object) httpURLConnection.getRequestMethod());
        nVar.a("User-Agent", (Object) httpURLConnection.getRequestProperty("User-Agent"));
        nVar.a("Content-Type", (Object) httpURLConnection.getRequestProperty("Content-Type"));
        httpURLConnection.setConnectTimeout(baVar.a());
        httpURLConnection.setReadTimeout(baVar.a());
        if (!(akVar == ak.POST)) {
            nVar.a();
            return;
        }
        httpURLConnection.setDoOutput(true);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
        try {
            ay ayVar = new ay(bufferedOutputStream, nVar);
            if (size == 1) {
                ap a2 = baVar.get(0);
                nVar.c("  Parameters:\n");
                a(a2.j, ayVar);
                nVar.c("  Attachments:\n");
                b(a2.j, ayVar);
                if (a2.e != null) {
                    a(a2.e, url.getPath(), ayVar);
                }
            } else {
                String d2 = d(baVar);
                if (u.a(d2)) {
                    throw new z("At least one request in a batch must have an open Session, or a default app ID must be specified.");
                }
                ayVar.a("batch_app_id", d2);
                Bundle bundle = new Bundle();
                a(ayVar, baVar, bundle);
                nVar.c("  Attachments:\n");
                b(bundle, ayVar);
            }
            bufferedOutputStream.close();
            nVar.a();
        } catch (Throwable th) {
            bufferedOutputStream.close();
            throw th;
        }
    }

    private static void a(b bVar, String str, ax axVar) {
        boolean z;
        boolean z2;
        if (str.startsWith("me/") || str.startsWith("/me/")) {
            int indexOf = str.indexOf(":");
            int indexOf2 = str.indexOf("?");
            z = indexOf > 3 && (indexOf2 == -1 || indexOf < indexOf2);
        } else {
            z = false;
        }
        for (Map.Entry next : bVar.c().entrySet()) {
            if (!z || !((String) next.getKey()).equalsIgnoreCase("image")) {
                z2 = false;
            } else {
                z2 = true;
            }
            a((String) next.getKey(), next.getValue(), axVar, z2);
        }
    }

    private static void a(String str, Object obj, ax axVar, boolean z) {
        Class<?> cls;
        JSONObject jSONObject;
        Class<?> cls2 = obj.getClass();
        if (b.class.isAssignableFrom(cls2)) {
            JSONObject d2 = ((b) obj).d();
            cls = d2.getClass();
            jSONObject = d2;
        } else if (g.class.isAssignableFrom(cls2)) {
            JSONArray a2 = ((g) obj).a();
            cls = a2.getClass();
            jSONObject = a2;
        } else {
            cls = cls2;
            jSONObject = obj;
        }
        if (JSONObject.class.isAssignableFrom(cls)) {
            JSONObject jSONObject2 = (JSONObject) jSONObject;
            if (z) {
                Iterator<String> keys = jSONObject2.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    a(String.format("%s[%s]", str, next), jSONObject2.opt(next), axVar, z);
                }
            } else if (jSONObject2.has("id")) {
                a(str, jSONObject2.optString("id"), axVar, z);
            } else if (jSONObject2.has("url")) {
                a(str, jSONObject2.optString("url"), axVar, z);
            }
        } else if (JSONArray.class.isAssignableFrom(cls)) {
            JSONArray jSONArray = (JSONArray) jSONObject;
            int length = jSONArray.length();
            for (int i2 = 0; i2 < length; i2++) {
                a(String.format("%s[%d]", str, Integer.valueOf(i2)), jSONArray.opt(i2), axVar, z);
            }
        } else if (String.class.isAssignableFrom(cls) || Number.class.isAssignableFrom(cls) || Boolean.class.isAssignableFrom(cls)) {
            axVar.a(str, jSONObject.toString());
        } else if (Date.class.isAssignableFrom(cls)) {
            axVar.a(str, new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).format((Date) jSONObject));
        }
    }

    private static void a(Bundle bundle, ay ayVar) {
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if (d(obj)) {
                ayVar.a(next, obj);
            }
        }
    }

    private static void b(Bundle bundle, ay ayVar) {
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if (c(obj)) {
                ayVar.a(next, obj);
            }
        }
    }

    private static void a(ay ayVar, Collection<ap> collection, Bundle bundle) {
        JSONArray jSONArray = new JSONArray();
        for (ap a2 : collection) {
            a2.a(jSONArray, bundle);
        }
        ayVar.a("batch", jSONArray.toString());
    }

    private static String h() {
        return String.format("multipart/form-data; boundary=%s", "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
    }

    private static String i() {
        if (m == null) {
            m = String.format("%s.%s", "FBAndroidSDK", "3.0.1");
        }
        return m;
    }

    private static String d(ba baVar) {
        if (!u.a(baVar.f())) {
            return baVar.f();
        }
        Iterator it = baVar.iterator();
        while (it.hasNext()) {
            bg bgVar = ((ap) it.next()).f1686b;
            if (bgVar != null) {
                return bgVar.c();
            }
        }
        return f1685a;
    }

    /* access modifiers changed from: private */
    public static <T extends b> List<T> b(bc bcVar, Class<T> cls) {
        a aVar = (a) bcVar.a(a.class);
        if (aVar == null) {
            return null;
        }
        g<b> a2 = aVar.a();
        if (a2 == null) {
            return null;
        }
        return a2.a(cls);
    }

    private static boolean c(Object obj) {
        return (obj instanceof Bitmap) || (obj instanceof byte[]) || (obj instanceof ParcelFileDescriptor);
    }

    /* access modifiers changed from: private */
    public static boolean d(Object obj) {
        return (obj instanceof String) || (obj instanceof Boolean) || (obj instanceof Number) || (obj instanceof Date);
    }

    /* access modifiers changed from: private */
    public static String e(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if ((obj instanceof Boolean) || (obj instanceof Number)) {
            return obj.toString();
        }
        if (obj instanceof Date) {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).format(obj);
        }
        throw new IllegalArgumentException("Unsupported parameter type.");
    }
}
