package com.facebook;

import android.content.Context;
import android.os.Bundle;
import com.facebook.widget.bw;
import com.facebook.widget.ca;

/* compiled from: AuthorizationClient */
class i extends ca {
    public i(Context context, String str, Bundle bundle) {
        super(context, str, "oauth", bundle);
    }

    public bw a() {
        Bundle e = e();
        e.putString("redirect_uri", "fbconnect://success");
        e.putString("client_id", b());
        return new bw(c(), "oauth", e, d(), f());
    }
}
