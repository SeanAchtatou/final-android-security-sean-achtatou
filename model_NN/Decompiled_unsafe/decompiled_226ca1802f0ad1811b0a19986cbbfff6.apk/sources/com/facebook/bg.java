package com.facebook;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.r;
import android.util.Log;
import com.facebook.b.p;
import com.facebook.b.u;
import com.facebook.b.v;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/* compiled from: Session */
public class bg implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final String f1748a = bg.class.getCanonicalName();

    /* renamed from: b  reason: collision with root package name */
    private static final Object f1749b = new Object();

    /* renamed from: c  reason: collision with root package name */
    private static bg f1750c;
    /* access modifiers changed from: private */
    public static volatile Context d;
    private static final Set<String> e = new bh();
    private String f;
    private bz g;
    private a h;
    private Date i;
    private bm j;
    private c k;
    private volatile Bundle l;
    /* access modifiers changed from: private */
    public final List<bu> m;
    /* access modifiers changed from: private */
    public Handler n;
    /* access modifiers changed from: private */
    public bq o;
    private final Object p;
    private ce q;
    /* access modifiers changed from: private */
    public volatile bv r;

    bg(Context context, String str, ce ceVar) {
        this(context, str, ceVar, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.b.v.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.facebook.b.v.a(java.lang.String, java.lang.String):void
      com.facebook.b.v.a(java.util.Collection, java.lang.String):void
      com.facebook.b.v.a(java.lang.Object, java.lang.String):void */
    bg(Context context, String str, ce ceVar, boolean z) {
        Bundle bundle = null;
        this.i = new Date(0);
        this.p = new Object();
        if (context != null && str == null) {
            str = u.a(context);
        }
        v.a((Object) str, "applicationId");
        b(context);
        ceVar = ceVar == null ? new cd(d) : ceVar;
        this.f = str;
        this.q = ceVar;
        this.g = bz.CREATED;
        this.j = null;
        this.m = new ArrayList();
        this.n = new Handler(Looper.getMainLooper());
        bundle = z ? ceVar.a() : bundle;
        if (ce.b(bundle)) {
            Date a2 = ce.a(bundle, "com.facebook.TokenCachingStrategy.ExpirationDate");
            Date date = new Date();
            if (a2 == null || a2.before(date)) {
                ceVar.b();
                this.h = a.a(Collections.emptyList());
                return;
            }
            this.h = a.a(bundle);
            this.g = bz.CREATED_TOKEN_LOADED;
            return;
        }
        this.h = a.a(Collections.emptyList());
    }

    public final boolean a() {
        boolean a2;
        synchronized (this.p) {
            a2 = this.g.a();
        }
        return a2;
    }

    public final bz b() {
        bz bzVar;
        synchronized (this.p) {
            bzVar = this.g;
        }
        return bzVar;
    }

    public final String c() {
        return this.f;
    }

    public final String d() {
        String a2;
        synchronized (this.p) {
            a2 = this.h == null ? null : this.h.a();
        }
        return a2;
    }

    public final Date e() {
        Date b2;
        synchronized (this.p) {
            b2 = this.h == null ? null : this.h.b();
        }
        return b2;
    }

    public final List<String> f() {
        List<String> c2;
        synchronized (this.p) {
            c2 = this.h == null ? null : this.h.c();
        }
        return c2;
    }

    public final void a(bs bsVar) {
        a(bsVar, p.READ);
    }

    public final void b(bs bsVar) {
        a(bsVar, p.PUBLISH);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.bg.a(com.facebook.a, java.lang.Exception):void
     arg types: [?[OBJECT, ARRAY], com.facebook.ab]
     candidates:
      com.facebook.bg.a(com.facebook.bg, com.facebook.bq):com.facebook.bq
      com.facebook.bg.a(com.facebook.bg, com.facebook.bv):com.facebook.bv
      com.facebook.bg.a(int, com.facebook.s):void
      com.facebook.bg.a(android.os.Handler, java.lang.Runnable):void
      com.facebook.bg.a(com.facebook.bm, com.facebook.b.p):void
      com.facebook.bg.a(com.facebook.bs, com.facebook.b.p):void
      com.facebook.bg.a(java.lang.Object, java.lang.Object):boolean
      com.facebook.bg.a(com.facebook.a, java.lang.Exception):void */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        if (r8 == null) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
        r0 = (com.facebook.s) r8.getSerializableExtra("com.facebook.LoginActivity:Result");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        if (r0 == null) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
        a(r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0033, code lost:
        if (r4.k == null) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        r4.k.a(r6, r7, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003c, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003d, code lost:
        a((com.facebook.a) null, (java.lang.Exception) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0042, code lost:
        if (r7 != 0) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        r0 = new com.facebook.ab("User canceled operation.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004c, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.app.Activity r5, int r6, int r7, android.content.Intent r8) {
        /*
            r4 = this;
            r1 = 0
            r2 = 1
            java.lang.String r0 = "currentActivity"
            com.facebook.b.v.a(r5, r0)
            b(r5)
            java.lang.Object r3 = r4.p
            monitor-enter(r3)
            com.facebook.bm r0 = r4.j     // Catch:{ all -> 0x002e }
            if (r0 == 0) goto L_0x0019
            com.facebook.bm r0 = r4.j     // Catch:{ all -> 0x002e }
            int r0 = r0.c()     // Catch:{ all -> 0x002e }
            if (r6 == r0) goto L_0x001c
        L_0x0019:
            r0 = 0
            monitor-exit(r3)     // Catch:{ all -> 0x002e }
        L_0x001b:
            return r0
        L_0x001c:
            monitor-exit(r3)     // Catch:{ all -> 0x002e }
            if (r8 == 0) goto L_0x0042
            java.lang.String r0 = "com.facebook.LoginActivity:Result"
            java.io.Serializable r0 = r8.getSerializableExtra(r0)
            com.facebook.s r0 = (com.facebook.s) r0
            if (r0 == 0) goto L_0x0031
            r4.a(r7, r0)
            r0 = r2
            goto L_0x001b
        L_0x002e:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x002e }
            throw r0
        L_0x0031:
            com.facebook.c r0 = r4.k
            if (r0 == 0) goto L_0x003c
            com.facebook.c r0 = r4.k
            r0.a(r6, r7, r8)
            r0 = r2
            goto L_0x001b
        L_0x003c:
            r0 = r1
        L_0x003d:
            r4.a(r1, r0)
            r0 = r2
            goto L_0x001b
        L_0x0042:
            if (r7 != 0) goto L_0x004c
            com.facebook.ab r0 = new com.facebook.ab
            java.lang.String r3 = "User canceled operation."
            r0.<init>(r3)
            goto L_0x003d
        L_0x004c:
            r0 = r1
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.bg.a(android.app.Activity, int, int, android.content.Intent):boolean");
    }

    public final void g() {
        synchronized (this.p) {
            bz bzVar = this.g;
            switch (bl.f1757a[this.g.ordinal()]) {
                case 1:
                case 2:
                    this.g = bz.CLOSED_LOGIN_FAILED;
                    a(bzVar, this.g, new z("Log in attempt aborted."));
                    break;
                case 3:
                case 4:
                case 5:
                    this.g = bz.CLOSED;
                    a(bzVar, this.g, (Exception) null);
                    break;
            }
        }
    }

    public final void h() {
        if (this.q != null) {
            this.q.b();
        }
        u.b(d);
        g();
    }

    public final void a(bu buVar) {
        synchronized (this.m) {
            if (buVar != null) {
                if (!this.m.contains(buVar)) {
                    this.m.add(buVar);
                }
            }
        }
    }

    public final void b(bu buVar) {
        synchronized (this.m) {
            this.m.remove(buVar);
        }
    }

    public String toString() {
        return "{Session" + " state:" + this.g + ", token:" + (this.h == null ? "null" : this.h) + ", appId:" + (this.f == null ? "null" : this.f) + "}";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.os.Bundle r5) {
        /*
            r4 = this;
            java.lang.Object r1 = r4.p
            monitor-enter(r1)
            com.facebook.bz r0 = r4.g     // Catch:{ all -> 0x0051 }
            int[] r2 = com.facebook.bl.f1757a     // Catch:{ all -> 0x0051 }
            com.facebook.bz r3 = r4.g     // Catch:{ all -> 0x0051 }
            int r3 = r3.ordinal()     // Catch:{ all -> 0x0051 }
            r2 = r2[r3]     // Catch:{ all -> 0x0051 }
            switch(r2) {
                case 4: goto L_0x002e;
                case 5: goto L_0x0038;
                default: goto L_0x0012;
            }     // Catch:{ all -> 0x0051 }
        L_0x0012:
            java.lang.String r0 = com.facebook.bg.f1748a     // Catch:{ all -> 0x0051 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0051 }
            r2.<init>()     // Catch:{ all -> 0x0051 }
            java.lang.String r3 = "refreshToken ignored in state "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0051 }
            com.facebook.bz r3 = r4.g     // Catch:{ all -> 0x0051 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0051 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0051 }
            android.util.Log.d(r0, r2)     // Catch:{ all -> 0x0051 }
            monitor-exit(r1)     // Catch:{ all -> 0x0051 }
        L_0x002d:
            return
        L_0x002e:
            com.facebook.bz r2 = com.facebook.bz.OPENED_TOKEN_UPDATED     // Catch:{ all -> 0x0051 }
            r4.g = r2     // Catch:{ all -> 0x0051 }
            com.facebook.bz r2 = r4.g     // Catch:{ all -> 0x0051 }
            r3 = 0
            r4.a(r0, r2, r3)     // Catch:{ all -> 0x0051 }
        L_0x0038:
            com.facebook.a r0 = r4.h     // Catch:{ all -> 0x0051 }
            com.facebook.a r0 = com.facebook.a.a(r0, r5)     // Catch:{ all -> 0x0051 }
            r4.h = r0     // Catch:{ all -> 0x0051 }
            com.facebook.ce r0 = r4.q     // Catch:{ all -> 0x0051 }
            if (r0 == 0) goto L_0x004f
            com.facebook.ce r0 = r4.q     // Catch:{ all -> 0x0051 }
            com.facebook.a r2 = r4.h     // Catch:{ all -> 0x0051 }
            android.os.Bundle r2 = r2.f()     // Catch:{ all -> 0x0051 }
            r0.a(r2)     // Catch:{ all -> 0x0051 }
        L_0x004f:
            monitor-exit(r1)     // Catch:{ all -> 0x0051 }
            goto L_0x002d
        L_0x0051:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0051 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.bg.a(android.os.Bundle):void");
    }

    public static final bg i() {
        bg bgVar;
        synchronized (f1749b) {
            bgVar = f1750c;
        }
        return bgVar;
    }

    public static final void a(bg bgVar) {
        synchronized (f1749b) {
            if (bgVar != f1750c) {
                bg bgVar2 = f1750c;
                if (bgVar2 != null) {
                    bgVar2.g();
                }
                f1750c = bgVar;
                if (bgVar2 != null) {
                    b("com.facebook.sdk.ACTIVE_SESSION_UNSET");
                }
                if (bgVar != null) {
                    b("com.facebook.sdk.ACTIVE_SESSION_SET");
                    if (bgVar.a()) {
                        b("com.facebook.sdk.ACTIVE_SESSION_OPENED");
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.bg.a(android.content.Context, boolean, com.facebook.bs):com.facebook.bg
     arg types: [android.content.Context, int, ?[OBJECT, ARRAY]]
     candidates:
      com.facebook.bg.a(com.facebook.bg, int, com.facebook.s):void
      com.facebook.bg.a(com.facebook.bz, com.facebook.bz, java.lang.Exception):void
      com.facebook.bg.a(android.content.Context, boolean, com.facebook.bs):com.facebook.bg */
    public static bg a(Context context) {
        return a(context, false, (bs) null);
    }

    private static bg a(Context context, boolean z, bs bsVar) {
        bg a2 = new br(context).a();
        if (!bz.CREATED_TOKEN_LOADED.equals(a2.b()) && !z) {
            return null;
        }
        a(a2);
        a2.a(bsVar);
        return a2;
    }

    static Context j() {
        return d;
    }

    static void b(Context context) {
        if (context != null && d == null) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            d = context;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(bm bmVar) {
        bmVar.a(this.f);
        p();
        boolean c2 = c(bmVar);
        if (!c2 && bmVar.e) {
            c2 = e(bmVar);
        }
        if (!c2) {
            synchronized (this.p) {
                bz bzVar = this.g;
                switch (bl.f1757a[this.g.ordinal()]) {
                    case 6:
                    case 7:
                        return;
                    default:
                        this.g = bz.CLOSED_LOGIN_FAILED;
                        a(bzVar, this.g, new z("Log in attempt failed."));
                        return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.bg.a(com.facebook.bm, com.facebook.b.p):void
     arg types: [com.facebook.bs, com.facebook.b.p]
     candidates:
      com.facebook.bg.a(com.facebook.bg, com.facebook.bq):com.facebook.bq
      com.facebook.bg.a(com.facebook.bg, com.facebook.bv):com.facebook.bv
      com.facebook.bg.a(int, com.facebook.s):void
      com.facebook.bg.a(android.os.Handler, java.lang.Runnable):void
      com.facebook.bg.a(com.facebook.bs, com.facebook.b.p):void
      com.facebook.bg.a(java.lang.Object, java.lang.Object):boolean
      com.facebook.bg.a(com.facebook.a, java.lang.Exception):void
      com.facebook.bg.a(com.facebook.bm, com.facebook.b.p):void */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0057, code lost:
        if (r0 != com.facebook.bz.f1785c) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0059, code lost:
        a((com.facebook.bm) r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.facebook.bs r6, com.facebook.b.p r7) {
        /*
            r5 = this;
            r5.a(r6, r7)
            r5.b(r6)
            java.lang.Object r1 = r5.p
            monitor-enter(r1)
            com.facebook.bm r0 = r5.j     // Catch:{ all -> 0x0034 }
            if (r0 == 0) goto L_0x001d
            com.facebook.bz r0 = r5.g     // Catch:{ all -> 0x0034 }
            com.facebook.bz r2 = r5.g     // Catch:{ all -> 0x0034 }
            java.lang.UnsupportedOperationException r3 = new java.lang.UnsupportedOperationException     // Catch:{ all -> 0x0034 }
            java.lang.String r4 = "Session: an attempt was made to open a session that has a pending request."
            r3.<init>(r4)     // Catch:{ all -> 0x0034 }
            r5.a(r0, r2, r3)     // Catch:{ all -> 0x0034 }
            monitor-exit(r1)     // Catch:{ all -> 0x0034 }
        L_0x001c:
            return
        L_0x001d:
            com.facebook.bz r2 = r5.g     // Catch:{ all -> 0x0034 }
            int[] r0 = com.facebook.bl.f1757a     // Catch:{ all -> 0x0034 }
            com.facebook.bz r3 = r5.g     // Catch:{ all -> 0x0034 }
            int r3 = r3.ordinal()     // Catch:{ all -> 0x0034 }
            r0 = r0[r3]     // Catch:{ all -> 0x0034 }
            switch(r0) {
                case 1: goto L_0x0037;
                case 2: goto L_0x002c;
                case 3: goto L_0x005d;
                default: goto L_0x002c;
            }     // Catch:{ all -> 0x0034 }
        L_0x002c:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException     // Catch:{ all -> 0x0034 }
            java.lang.String r2 = "Session: an attempt was made to open an already opened session."
            r0.<init>(r2)     // Catch:{ all -> 0x0034 }
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x0034:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0034 }
            throw r0
        L_0x0037:
            com.facebook.bz r0 = com.facebook.bz.OPENING     // Catch:{ all -> 0x0034 }
            r5.g = r0     // Catch:{ all -> 0x0034 }
            if (r6 != 0) goto L_0x0045
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0034 }
            java.lang.String r2 = "openRequest cannot be null when opening a new Session"
            r0.<init>(r2)     // Catch:{ all -> 0x0034 }
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x0045:
            r5.j = r6     // Catch:{ all -> 0x0034 }
        L_0x0047:
            if (r6 == 0) goto L_0x0050
            com.facebook.bu r3 = r6.a()     // Catch:{ all -> 0x0034 }
            r5.a(r3)     // Catch:{ all -> 0x0034 }
        L_0x0050:
            r3 = 0
            r5.a(r2, r0, r3)     // Catch:{ all -> 0x0034 }
            monitor-exit(r1)     // Catch:{ all -> 0x0034 }
            com.facebook.bz r1 = com.facebook.bz.OPENING
            if (r0 != r1) goto L_0x001c
            r5.a(r6)
            goto L_0x001c
        L_0x005d:
            if (r6 == 0) goto L_0x0079
            java.util.List r0 = r6.d()     // Catch:{ all -> 0x0034 }
            boolean r0 = com.facebook.b.u.a(r0)     // Catch:{ all -> 0x0034 }
            if (r0 != 0) goto L_0x0079
            java.util.List r0 = r6.d()     // Catch:{ all -> 0x0034 }
            java.util.List r3 = r5.f()     // Catch:{ all -> 0x0034 }
            boolean r0 = com.facebook.b.u.a(r0, r3)     // Catch:{ all -> 0x0034 }
            if (r0 != 0) goto L_0x0079
            r5.j = r6     // Catch:{ all -> 0x0034 }
        L_0x0079:
            com.facebook.bm r0 = r5.j     // Catch:{ all -> 0x0034 }
            if (r0 != 0) goto L_0x0082
            com.facebook.bz r0 = com.facebook.bz.OPENED     // Catch:{ all -> 0x0034 }
            r5.g = r0     // Catch:{ all -> 0x0034 }
            goto L_0x0047
        L_0x0082:
            com.facebook.bz r0 = com.facebook.bz.OPENING     // Catch:{ all -> 0x0034 }
            r5.g = r0     // Catch:{ all -> 0x0034 }
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.bg.a(com.facebook.bs, com.facebook.b.p):void");
    }

    private void b(bm bmVar) {
        if (bmVar != null && !bmVar.e) {
            Intent intent = new Intent();
            intent.setClass(j(), LoginActivity.class);
            if (!a(intent)) {
                throw new z(String.format("Cannot use SessionLoginBehavior %s when %s is not declared as an activity in AndroidManifest.xml", bmVar.b(), LoginActivity.class.getName()));
            }
        }
    }

    private void a(bm bmVar, p pVar) {
        if (bmVar != null && !u.a(bmVar.d())) {
            for (String next : bmVar.d()) {
                if (a(next)) {
                    if (p.READ.equals(pVar)) {
                        throw new z(String.format("Cannot pass a publish or manage permission (%s) to a request for read authorization", next));
                    }
                } else if (p.PUBLISH.equals(pVar)) {
                    Log.w(f1748a, String.format("Should not pass a read permission (%s) to a request for publish or manage authorization", next));
                }
            }
        } else if (p.PUBLISH.equals(pVar)) {
            throw new z("Cannot request publish or manage authorization with no permissions.");
        }
    }

    static boolean a(String str) {
        return str != null && (str.startsWith("publish") || str.startsWith("manage") || e.contains(str));
    }

    /* access modifiers changed from: private */
    public void a(int i2, s sVar) {
        Exception exc;
        a aVar;
        if (i2 == -1) {
            if (sVar.f1828a == t.SUCCESS) {
                aVar = sVar.f1829b;
                exc = null;
            } else {
                exc = new x(sVar.f1830c);
                aVar = null;
            }
        } else if (i2 == 0) {
            exc = new ab(sVar.f1830c);
            aVar = null;
        } else {
            exc = null;
            aVar = null;
        }
        this.k = null;
        a(aVar, exc);
    }

    private boolean c(bm bmVar) {
        Intent d2 = d(bmVar);
        if (!a(d2)) {
            return false;
        }
        try {
            bmVar.e().a(d2, bmVar.c());
            return true;
        } catch (ActivityNotFoundException e2) {
            return false;
        }
    }

    private boolean a(Intent intent) {
        if (j().getPackageManager().resolveActivity(intent, 0) == null) {
            return false;
        }
        return true;
    }

    private Intent d(bm bmVar) {
        Intent intent = new Intent();
        intent.setClass(j(), LoginActivity.class);
        intent.setAction(bmVar.b().toString());
        intent.putExtras(LoginActivity.a(bmVar.f()));
        return intent;
    }

    private boolean e(bm bmVar) {
        this.k = new c();
        this.k.a(new bi(this));
        this.k.a(j());
        this.k.a(bmVar.f());
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar, Exception exc) {
        if (aVar != null && aVar.g()) {
            aVar = null;
            exc = new z("Invalid access token.");
        }
        synchronized (this.p) {
            switch (bl.f1757a[this.g.ordinal()]) {
                case 2:
                    b(aVar, exc);
                    break;
                case 4:
                case 5:
                    c(aVar, exc);
                    break;
            }
        }
    }

    private void b(a aVar, Exception exc) {
        bz bzVar = this.g;
        if (aVar != null) {
            this.h = aVar;
            a(aVar);
            this.g = bz.OPENED;
        } else if (exc != null) {
            this.g = bz.CLOSED_LOGIN_FAILED;
        }
        this.j = null;
        a(bzVar, this.g, exc);
    }

    private void c(a aVar, Exception exc) {
        bz bzVar = this.g;
        if (aVar != null) {
            this.h = aVar;
            a(aVar);
            this.g = bz.OPENED_TOKEN_UPDATED;
        }
        this.j = null;
        a(bzVar, this.g, exc);
    }

    private void a(a aVar) {
        if (aVar != null && this.q != null) {
            this.q.a(aVar.f());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(bz bzVar, bz bzVar2, Exception exc) {
        if (bzVar != bzVar2 || bzVar == bz.OPENED_TOKEN_UPDATED || exc != null) {
            if (bzVar2.b()) {
                this.h = a.a(Collections.emptyList());
            }
            synchronized (this.m) {
                b(this.n, new bj(this, bzVar2, exc));
            }
            if (this == f1750c && bzVar.a() != bzVar2.a()) {
                if (bzVar2.a()) {
                    b("com.facebook.sdk.ACTIVE_SESSION_OPENED");
                } else {
                    b("com.facebook.sdk.ACTIVE_SESSION_CLOSED");
                }
            }
        }
    }

    static void b(String str) {
        r.a(j()).a(new Intent(str));
    }

    /* access modifiers changed from: private */
    public static void b(Handler handler, Runnable runnable) {
        if (handler != null) {
            handler.post(runnable);
        } else {
            cb.a().execute(runnable);
        }
    }

    /* access modifiers changed from: package-private */
    public void k() {
        if (m()) {
            l();
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        bv bvVar = null;
        synchronized (this.p) {
            if (this.r == null) {
                bvVar = new bv(this);
                this.r = bvVar;
            }
        }
        if (bvVar != null) {
            bvVar.a();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean m() {
        if (this.r != null) {
            return false;
        }
        Date date = new Date();
        if (!this.g.a() || !this.h.d().a() || date.getTime() - this.i.getTime() <= 3600000 || date.getTime() - this.h.e().getTime() <= 86400000) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public a n() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public void a(Date date) {
        this.i = date;
    }

    public int hashCode() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof bg)) {
            return false;
        }
        bg bgVar = (bg) obj;
        if (!a(bgVar.f, this.f) || !a(bgVar.l, this.l) || !a(bgVar.g, this.g) || !a(bgVar.e(), e())) {
            return false;
        }
        return true;
    }

    private static boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    private void p() {
        String str;
        bq bqVar = null;
        synchronized (this) {
            if (this.o == null && cb.b() && (str = this.f) != null) {
                bqVar = new bq(this, str, d);
                this.o = bqVar;
            }
        }
        if (bqVar != null) {
            bqVar.execute(new Void[0]);
        }
    }
}
