package com.facebook;

import android.util.Pair;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: Request */
final class as implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f1690a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ba f1691b;

    as(ArrayList arrayList, ba baVar) {
        this.f1690a = arrayList;
        this.f1691b = baVar;
    }

    public void run() {
        Iterator it = this.f1690a.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            ((au) pair.first).a((bc) pair.second);
        }
        for (bb a2 : this.f1691b.e()) {
            a2.a(this.f1691b);
        }
    }
}
