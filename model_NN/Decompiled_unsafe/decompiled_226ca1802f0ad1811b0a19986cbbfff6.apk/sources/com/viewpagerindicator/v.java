package com.viewpagerindicator;

/* compiled from: TitlePageIndicator */
public enum v {
    Bottom(0),
    Top(1);
    

    /* renamed from: c  reason: collision with root package name */
    public final int f2083c;

    private v(int i) {
        this.f2083c = i;
    }

    public static v a(int i) {
        for (v vVar : values()) {
            if (vVar.f2083c == i) {
                return vVar;
            }
        }
        return null;
    }
}
