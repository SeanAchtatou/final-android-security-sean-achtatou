package com.viewpagerindicator;

/* compiled from: R */
public final class l {

    /* renamed from: a */
    public static final int default_circle_indicator_radius = 2131427386;

    /* renamed from: b */
    public static final int default_circle_indicator_stroke_width = 2131427387;

    /* renamed from: c */
    public static final int default_drawable_indicator_drawable_height = 2131427388;

    /* renamed from: d */
    public static final int default_drawable_indicator_drawable_width = 2131427389;

    /* renamed from: e */
    public static final int default_line_indicator_gap_width = 2131427391;

    /* renamed from: f */
    public static final int default_line_indicator_line_width = 2131427392;

    /* renamed from: g */
    public static final int default_line_indicator_stroke_width = 2131427393;

    /* renamed from: h */
    public static final int default_title_indicator_clip_padding = 2131427394;

    /* renamed from: i */
    public static final int default_title_indicator_footer_indicator_height = 2131427395;

    /* renamed from: j */
    public static final int default_title_indicator_footer_indicator_underline_padding = 2131427396;

    /* renamed from: k */
    public static final int default_title_indicator_footer_line_height = 2131427397;

    /* renamed from: l */
    public static final int default_title_indicator_footer_padding = 2131427398;

    /* renamed from: m */
    public static final int default_title_indicator_text_size = 2131427399;

    /* renamed from: n */
    public static final int default_title_indicator_title_padding = 2131427400;

    /* renamed from: o */
    public static final int default_title_indicator_top_padding = 2131427401;
}
