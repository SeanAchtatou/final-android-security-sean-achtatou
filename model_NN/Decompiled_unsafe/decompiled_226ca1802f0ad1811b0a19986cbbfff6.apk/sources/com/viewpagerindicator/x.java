package com.viewpagerindicator;

import android.os.Parcel;
import android.os.Parcelable;
import com.viewpagerindicator.TitlePageIndicator;

/* compiled from: TitlePageIndicator */
final class x implements Parcelable.Creator<TitlePageIndicator.SavedState> {
    x() {
    }

    /* renamed from: a */
    public TitlePageIndicator.SavedState createFromParcel(Parcel parcel) {
        return new TitlePageIndicator.SavedState(parcel, null);
    }

    /* renamed from: a */
    public TitlePageIndicator.SavedState[] newArray(int i) {
        return new TitlePageIndicator.SavedState[i];
    }
}
