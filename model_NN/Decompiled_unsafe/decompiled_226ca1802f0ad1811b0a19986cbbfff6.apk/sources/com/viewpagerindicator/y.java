package com.viewpagerindicator;

/* compiled from: UnderlinePageIndicator */
class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UnderlinePageIndicator f2084a;

    y(UnderlinePageIndicator underlinePageIndicator) {
        this.f2084a = underlinePageIndicator;
    }

    public void run() {
        if (this.f2084a.f2057b) {
            int max = Math.max(this.f2084a.f2056a.getAlpha() - this.f2084a.e, 0);
            this.f2084a.f2056a.setAlpha(max);
            this.f2084a.invalidate();
            if (max > 0) {
                this.f2084a.postDelayed(this, 30);
            }
        }
    }
}
