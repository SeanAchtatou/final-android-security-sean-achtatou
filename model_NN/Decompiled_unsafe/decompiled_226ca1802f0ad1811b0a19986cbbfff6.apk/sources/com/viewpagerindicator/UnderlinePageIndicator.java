package com.viewpagerindicator;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.bg;
import android.support.v4.view.by;
import android.support.v4.view.z;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

public class UnderlinePageIndicator extends View implements g {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Paint f2056a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f2057b;

    /* renamed from: c  reason: collision with root package name */
    private int f2058c;
    private int d;
    /* access modifiers changed from: private */
    public int e;
    private ViewPager f;
    private by g;
    private int h;
    private int i;
    private float j;
    private int k;
    private float l;
    private int m;
    private boolean n;
    private final Runnable o;

    public UnderlinePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, i.vpiUnderlinePageIndicatorStyle);
    }

    public UnderlinePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f2056a = new Paint(1);
        this.l = -1.0f;
        this.m = -1;
        this.o = new y(this);
        if (!isInEditMode()) {
            Resources resources = getResources();
            boolean z = resources.getBoolean(j.default_underline_indicator_fades);
            int integer = resources.getInteger(n.default_underline_indicator_fade_delay);
            int integer2 = resources.getInteger(n.default_underline_indicator_fade_length);
            int color = resources.getColor(k.default_underline_indicator_selected_color);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, o.p, i2, 0);
            a(obtainStyledAttributes.getBoolean(2, z));
            e(obtainStyledAttributes.getColor(1, color));
            c(obtainStyledAttributes.getInteger(3, integer));
            d(obtainStyledAttributes.getInteger(4, integer2));
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            if (drawable != null) {
                setBackgroundDrawable(drawable);
            }
            obtainStyledAttributes.recycle();
            this.k = bg.a(ViewConfiguration.get(context));
        }
    }

    public void a(boolean z) {
        if (z != this.f2057b) {
            this.f2057b = z;
            if (z) {
                post(this.o);
                return;
            }
            removeCallbacks(this.o);
            this.f2056a.setAlpha(255);
            invalidate();
        }
    }

    public void c(int i2) {
        this.f2058c = i2;
    }

    public void d(int i2) {
        this.d = i2;
        this.e = 255 / (this.d / 30);
    }

    public void e(int i2) {
        this.f2056a.setColor(i2);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int count;
        super.onDraw(canvas);
        if (this.f != null && (count = this.f.b().getCount()) != 0) {
            if (this.i >= count) {
                f(count - 1);
                return;
            }
            int paddingLeft = getPaddingLeft();
            float width = ((float) ((getWidth() - paddingLeft) - getPaddingRight())) / (((float) count) * 1.0f);
            float f2 = ((float) paddingLeft) + ((((float) this.i) + this.j) * width);
            canvas.drawRect(f2, (float) getPaddingTop(), f2 + width, (float) (getHeight() - getPaddingBottom()), this.f2056a);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = 0;
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        if (this.f == null || this.f.b().getCount() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        switch (action) {
            case 0:
                this.m = z.b(motionEvent, 0);
                this.l = motionEvent.getX();
                return true;
            case 1:
            case 3:
                if (!this.n) {
                    int count = this.f.b().getCount();
                    int width = getWidth();
                    float f2 = ((float) width) / 2.0f;
                    float f3 = ((float) width) / 6.0f;
                    if (this.i <= 0 || motionEvent.getX() >= f2 - f3) {
                        if (this.i < count - 1 && motionEvent.getX() > f3 + f2) {
                            if (action == 3) {
                                return true;
                            }
                            this.f.a(this.i + 1);
                            return true;
                        }
                    } else if (action == 3) {
                        return true;
                    } else {
                        this.f.a(this.i - 1);
                        return true;
                    }
                }
                this.n = false;
                this.m = -1;
                if (!this.f.g()) {
                    return true;
                }
                this.f.f();
                return true;
            case 2:
                float c2 = z.c(motionEvent, z.a(motionEvent, this.m));
                float f4 = c2 - this.l;
                if (!this.n && Math.abs(f4) > ((float) this.k)) {
                    this.n = true;
                }
                if (!this.n) {
                    return true;
                }
                this.l = c2;
                if (!this.f.g() && !this.f.e()) {
                    return true;
                }
                this.f.b(f4);
                return true;
            case 4:
            default:
                return true;
            case 5:
                int b2 = z.b(motionEvent);
                this.l = z.c(motionEvent, b2);
                this.m = z.b(motionEvent, b2);
                return true;
            case 6:
                int b3 = z.b(motionEvent);
                if (z.b(motionEvent, b3) == this.m) {
                    if (b3 == 0) {
                        i2 = 1;
                    }
                    this.m = z.b(motionEvent, i2);
                }
                this.l = z.c(motionEvent, z.a(motionEvent, this.m));
                return true;
        }
    }

    public void f(int i2) {
        if (this.f == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.f.a(i2);
        this.i = i2;
        invalidate();
    }

    public void b(int i2) {
        this.h = i2;
        if (this.g != null) {
            this.g.b(i2);
        }
    }

    public void a(int i2, float f2, int i3) {
        this.i = i2;
        this.j = f2;
        if (this.f2057b) {
            if (i3 > 0) {
                removeCallbacks(this.o);
                this.f2056a.setAlpha(255);
            } else if (this.h != 1) {
                postDelayed(this.o, (long) this.f2058c);
            }
        }
        invalidate();
        if (this.g != null) {
            this.g.a(i2, f2, i3);
        }
    }

    public void a(int i2) {
        if (this.h == 0) {
            this.i = i2;
            this.j = 0.0f;
            invalidate();
            this.o.run();
        }
        if (this.g != null) {
            this.g.a(i2);
        }
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.i = savedState.f2059a;
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f2059a = this.i;
        return savedState;
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new z();

        /* renamed from: a  reason: collision with root package name */
        int f2059a;

        /* synthetic */ SavedState(Parcel parcel, y yVar) {
            this(parcel);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f2059a = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f2059a);
        }
    }
}
