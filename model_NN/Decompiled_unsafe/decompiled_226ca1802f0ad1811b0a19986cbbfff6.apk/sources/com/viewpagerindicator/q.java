package com.viewpagerindicator;

import android.view.View;

/* compiled from: TabPageIndicator */
class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f2073a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ TabPageIndicator f2074b;

    q(TabPageIndicator tabPageIndicator, View view) {
        this.f2074b = tabPageIndicator;
        this.f2073a = view;
    }

    public void run() {
        this.f2074b.smoothScrollTo(this.f2073a.getLeft() - ((this.f2074b.getWidth() - this.f2073a.getWidth()) / 2), 0);
        Runnable unused = this.f2074b.f2050b = null;
    }
}
