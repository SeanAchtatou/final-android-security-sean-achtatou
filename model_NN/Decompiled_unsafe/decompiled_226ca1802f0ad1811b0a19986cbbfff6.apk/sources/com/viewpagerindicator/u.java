package com.viewpagerindicator;

/* compiled from: TitlePageIndicator */
public enum u {
    None(0),
    Triangle(1),
    Underline(2);
    
    public final int d;

    private u(int i) {
        this.d = i;
    }

    public static u a(int i) {
        for (u uVar : values()) {
            if (uVar.d == i) {
                return uVar;
            }
        }
        return null;
    }
}
