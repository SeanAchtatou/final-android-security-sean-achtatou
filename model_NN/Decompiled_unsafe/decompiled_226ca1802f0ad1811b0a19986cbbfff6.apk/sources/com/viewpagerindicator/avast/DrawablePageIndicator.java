package com.viewpagerindicator.avast;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.bg;
import android.support.v4.view.by;
import android.support.v4.view.z;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import com.viewpagerindicator.g;
import com.viewpagerindicator.i;
import com.viewpagerindicator.j;
import com.viewpagerindicator.l;
import com.viewpagerindicator.m;
import com.viewpagerindicator.o;

public class DrawablePageIndicator extends View implements g {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f2060a;

    /* renamed from: b  reason: collision with root package name */
    private Drawable f2061b;

    /* renamed from: c  reason: collision with root package name */
    private ViewPager f2062c;
    private by d;
    private int e;
    private boolean f;
    private int g;
    private int h;
    private int i;
    private int j;
    private float k;
    private int l;
    private boolean m;

    public DrawablePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, i.vpiLinePageIndicatorStyle);
    }

    public DrawablePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.k = -1.0f;
        this.l = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(l.default_drawable_indicator_drawable_width);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(l.default_line_indicator_gap_width);
            int dimensionPixelSize3 = resources.getDimensionPixelSize(l.default_drawable_indicator_drawable_height);
            boolean z = resources.getBoolean(j.default_drawable_indicator_centered);
            Drawable drawable = resources.getDrawable(m.default_drawable_indicator_unselected_drawable);
            Drawable drawable2 = resources.getDrawable(m.default_drawable_indicator_selected_drawable);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, o.f2070b, i2, 0);
            this.f = obtainStyledAttributes.getBoolean(1, z);
            this.g = obtainStyledAttributes.getDimensionPixelSize(5, dimensionPixelSize);
            this.i = obtainStyledAttributes.getDimensionPixelSize(2, dimensionPixelSize2);
            this.h = obtainStyledAttributes.getDimensionPixelSize(6, dimensionPixelSize3);
            Drawable drawable3 = obtainStyledAttributes.getDrawable(0);
            if (drawable3 != null) {
                setBackgroundDrawable(drawable3);
            }
            this.f2060a = obtainStyledAttributes.getDrawable(3);
            if (this.f2060a == null) {
                this.f2060a = drawable;
            }
            this.f2061b = obtainStyledAttributes.getDrawable(4);
            if (this.f2061b == null) {
                this.f2061b = drawable2;
            }
            a();
            obtainStyledAttributes.recycle();
            this.j = bg.a(ViewConfiguration.get(context));
        }
    }

    private void a() {
        this.f2061b.setBounds(0, 0, this.g, this.h);
        this.f2060a.setBounds(0, 0, this.g, this.h);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int count;
        super.onDraw(canvas);
        if (this.f2062c != null && (count = this.f2062c.b().getCount()) != 0) {
            if (this.e >= count) {
                c(count - 1);
                return;
            }
            float f2 = (float) (this.g + this.i);
            float f3 = (((float) count) * f2) - ((float) this.i);
            float paddingTop = (float) getPaddingTop();
            float paddingLeft = (float) getPaddingLeft();
            float paddingRight = (float) getPaddingRight();
            float height = (paddingTop + (((((float) getHeight()) - paddingTop) - ((float) getPaddingBottom())) / 2.0f)) - ((float) (this.h / 2));
            if (this.f) {
                paddingLeft += (((((float) getWidth()) - paddingLeft) - paddingRight) / 2.0f) - (f3 / 2.0f);
            }
            for (int i2 = 0; i2 < count; i2++) {
                canvas.save(1);
                canvas.translate((((float) i2) * f2) + paddingLeft, height);
                if (i2 == this.e) {
                    this.f2061b.draw(canvas);
                } else {
                    this.f2060a.draw(canvas);
                }
                canvas.restore();
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a2;
        int i2 = 0;
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        if (this.f2062c == null || this.f2062c.b().getCount() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        switch (action) {
            case 0:
                this.l = z.b(motionEvent, 0);
                this.k = motionEvent.getX();
                return true;
            case 1:
            case 3:
                if (this.m || this.e == (a2 = a(motionEvent)) || action == 3) {
                    this.m = false;
                    this.l = -1;
                    if (!this.f2062c.g()) {
                        return true;
                    }
                    this.f2062c.f();
                    return true;
                }
                this.f2062c.a(a2);
                return true;
            case 2:
                float c2 = z.c(motionEvent, z.a(motionEvent, this.l));
                float f2 = c2 - this.k;
                if (!this.m && Math.abs(f2) > ((float) this.j)) {
                    this.m = true;
                }
                if (!this.m) {
                    return true;
                }
                this.k = c2;
                if (!this.f2062c.g() && !this.f2062c.e()) {
                    return true;
                }
                this.f2062c.b(f2);
                return true;
            case 4:
            default:
                return true;
            case 5:
                int b2 = z.b(motionEvent);
                this.k = z.c(motionEvent, b2);
                this.l = z.b(motionEvent, b2);
                return true;
            case 6:
                int b3 = z.b(motionEvent);
                if (z.b(motionEvent, b3) == this.l) {
                    if (b3 == 0) {
                        i2 = 1;
                    }
                    this.l = z.b(motionEvent, i2);
                }
                this.k = z.c(motionEvent, z.a(motionEvent, this.l));
                return true;
        }
    }

    private int a(MotionEvent motionEvent) {
        float x;
        int count = this.f2062c.b().getCount();
        int width = getWidth();
        float f2 = (((float) (this.g + this.i)) * ((float) count)) - ((float) this.i);
        if (f2 <= ((float) width)) {
            x = motionEvent.getX() - ((((float) width) - f2) / 2.0f);
        } else {
            x = ((f2 - ((float) width)) / 2.0f) + motionEvent.getX();
        }
        if (x < 0.0f) {
            return 0;
        }
        if (x > f2) {
            return count - 1;
        }
        return (int) ((x / f2) * ((float) count));
    }

    public void c(int i2) {
        if (this.f2062c == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.f2062c.a(i2);
        this.e = i2;
        invalidate();
    }

    public void b(int i2) {
        if (this.d != null) {
            this.d.b(i2);
        }
    }

    public void a(int i2, float f2, int i3) {
        if (this.d != null) {
            this.d.a(i2, f2, i3);
        }
    }

    public void a(int i2) {
        this.e = i2;
        invalidate();
        if (this.d != null) {
            this.d.a(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        setMeasuredDimension(d(i2), e(i3));
    }

    private int d(int i2) {
        float f2;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || this.f2062c == null) {
            f2 = (float) size;
        } else {
            int count = this.f2062c.b().getCount();
            f2 = (float) (((count - 1) * this.i) + getPaddingLeft() + getPaddingRight() + (this.g * count));
            if (mode == Integer.MIN_VALUE) {
                f2 = Math.min(f2, (float) size);
            }
        }
        return (int) FloatMath.ceil(f2);
    }

    private int e(int i2) {
        float paddingTop;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            paddingTop = (float) size;
        } else {
            paddingTop = (float) (this.h + getPaddingTop() + getPaddingBottom());
            if (mode == Integer.MIN_VALUE) {
                paddingTop = Math.min(paddingTop, (float) size);
            }
        }
        return (int) FloatMath.ceil(paddingTop);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.e = savedState.f2063a;
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f2063a = this.e;
        return savedState;
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new b();

        /* renamed from: a  reason: collision with root package name */
        int f2063a;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f2063a = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f2063a);
        }
    }
}
