package com.viewpagerindicator.avast;

import android.os.Parcel;
import android.os.Parcelable;
import com.viewpagerindicator.avast.DrawablePageIndicator;

/* compiled from: DrawablePageIndicator */
final class b implements Parcelable.Creator<DrawablePageIndicator.SavedState> {
    b() {
    }

    /* renamed from: a */
    public DrawablePageIndicator.SavedState createFromParcel(Parcel parcel) {
        return new DrawablePageIndicator.SavedState(parcel);
    }

    /* renamed from: a */
    public DrawablePageIndicator.SavedState[] newArray(int i) {
        return new DrawablePageIndicator.SavedState[i];
    }
}
