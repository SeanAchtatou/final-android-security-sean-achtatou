package com.viewpagerindicator;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.view.by;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;

public class IconPageIndicator extends HorizontalScrollView implements g {

    /* renamed from: a  reason: collision with root package name */
    private final d f2042a;

    /* renamed from: b  reason: collision with root package name */
    private ViewPager f2043b;

    /* renamed from: c  reason: collision with root package name */
    private by f2044c;
    /* access modifiers changed from: private */
    public Runnable d;
    private int e;

    public IconPageIndicator(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setHorizontalScrollBarEnabled(false);
        this.f2042a = new d(context, i.vpiIconPageIndicatorStyle);
        addView(this.f2042a, new FrameLayout.LayoutParams(-2, -1, 17));
    }

    private void d(int i) {
        View childAt = this.f2042a.getChildAt(i);
        if (this.d != null) {
            removeCallbacks(this.d);
        }
        this.d = new c(this, childAt);
        post(this.d);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.d != null) {
            post(this.d);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.d != null) {
            removeCallbacks(this.d);
        }
    }

    public void b(int i) {
        if (this.f2044c != null) {
            this.f2044c.b(i);
        }
    }

    public void a(int i, float f, int i2) {
        if (this.f2044c != null) {
            this.f2044c.a(i, f, i2);
        }
    }

    public void a(int i) {
        c(i);
        if (this.f2044c != null) {
            this.f2044c.a(i);
        }
    }

    public void c(int i) {
        boolean z;
        if (this.f2043b == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.e = i;
        this.f2043b.a(i);
        int childCount = this.f2042a.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = this.f2042a.getChildAt(i2);
            if (i2 == i) {
                z = true;
            } else {
                z = false;
            }
            childAt.setSelected(z);
            if (z) {
                d(i);
            }
        }
    }
}
