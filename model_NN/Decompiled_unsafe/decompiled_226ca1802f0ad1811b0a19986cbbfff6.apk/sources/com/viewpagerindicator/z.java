package com.viewpagerindicator;

import android.os.Parcel;
import android.os.Parcelable;
import com.viewpagerindicator.UnderlinePageIndicator;

/* compiled from: UnderlinePageIndicator */
final class z implements Parcelable.Creator<UnderlinePageIndicator.SavedState> {
    z() {
    }

    /* renamed from: a */
    public UnderlinePageIndicator.SavedState createFromParcel(Parcel parcel) {
        return new UnderlinePageIndicator.SavedState(parcel, null);
    }

    /* renamed from: a */
    public UnderlinePageIndicator.SavedState[] newArray(int i) {
        return new UnderlinePageIndicator.SavedState[i];
    }
}
