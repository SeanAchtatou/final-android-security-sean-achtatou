package com.viewpagerindicator;

import android.view.View;

/* compiled from: IconPageIndicator */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f2064a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ IconPageIndicator f2065b;

    c(IconPageIndicator iconPageIndicator, View view) {
        this.f2065b = iconPageIndicator;
        this.f2064a = view;
    }

    public void run() {
        this.f2065b.smoothScrollTo(this.f2064a.getLeft() - ((this.f2065b.getWidth() - this.f2064a.getWidth()) / 2), 0);
        Runnable unused = this.f2065b.d = null;
    }
}
