package com.viewpagerindicator;

import android.os.Parcel;
import android.os.Parcelable;
import com.viewpagerindicator.LinePageIndicator;

/* compiled from: LinePageIndicator */
final class f implements Parcelable.Creator<LinePageIndicator.SavedState> {
    f() {
    }

    /* renamed from: a */
    public LinePageIndicator.SavedState createFromParcel(Parcel parcel) {
        return new LinePageIndicator.SavedState(parcel);
    }

    /* renamed from: a */
    public LinePageIndicator.SavedState[] newArray(int i) {
        return new LinePageIndicator.SavedState[i];
    }
}
