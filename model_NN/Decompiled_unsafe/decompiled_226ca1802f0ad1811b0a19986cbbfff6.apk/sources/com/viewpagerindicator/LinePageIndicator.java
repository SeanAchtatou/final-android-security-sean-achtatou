package com.viewpagerindicator;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.bg;
import android.support.v4.view.by;
import android.support.v4.view.z;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

public class LinePageIndicator extends View implements g {

    /* renamed from: a  reason: collision with root package name */
    private final Paint f2045a;

    /* renamed from: b  reason: collision with root package name */
    private final Paint f2046b;

    /* renamed from: c  reason: collision with root package name */
    private ViewPager f2047c;
    private by d;
    private int e;
    private boolean f;
    private float g;
    private float h;
    private int i;
    private float j;
    private int k;
    private boolean l;

    public LinePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, i.vpiLinePageIndicatorStyle);
    }

    public LinePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f2045a = new Paint(1);
        this.f2046b = new Paint(1);
        this.j = -1.0f;
        this.k = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int color = resources.getColor(k.default_line_indicator_selected_color);
            int color2 = resources.getColor(k.default_line_indicator_unselected_color);
            float dimension = resources.getDimension(l.default_line_indicator_line_width);
            float dimension2 = resources.getDimension(l.default_line_indicator_gap_width);
            float dimension3 = resources.getDimension(l.default_line_indicator_stroke_width);
            boolean z = resources.getBoolean(j.default_line_indicator_centered);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, o.f2071c, i2, 0);
            this.f = obtainStyledAttributes.getBoolean(1, z);
            this.g = obtainStyledAttributes.getDimension(6, dimension);
            this.h = obtainStyledAttributes.getDimension(2, dimension2);
            a(obtainStyledAttributes.getDimension(4, dimension3));
            this.f2045a.setColor(obtainStyledAttributes.getColor(5, color2));
            this.f2046b.setColor(obtainStyledAttributes.getColor(3, color));
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            if (drawable != null) {
                setBackgroundDrawable(drawable);
            }
            obtainStyledAttributes.recycle();
            this.i = bg.a(ViewConfiguration.get(context));
        }
    }

    public void a(float f2) {
        this.f2046b.setStrokeWidth(f2);
        this.f2045a.setStrokeWidth(f2);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int count;
        float f2;
        super.onDraw(canvas);
        if (this.f2047c != null && (count = this.f2047c.b().getCount()) != 0) {
            if (this.e >= count) {
                c(count - 1);
                return;
            }
            float f3 = this.g + this.h;
            float f4 = (((float) count) * f3) - this.h;
            float paddingTop = (float) getPaddingTop();
            float paddingLeft = (float) getPaddingLeft();
            float paddingRight = (float) getPaddingRight();
            float height = paddingTop + (((((float) getHeight()) - paddingTop) - ((float) getPaddingBottom())) / 2.0f);
            if (this.f) {
                f2 = paddingLeft + ((((((float) getWidth()) - paddingLeft) - paddingRight) / 2.0f) - (f4 / 2.0f));
            } else {
                f2 = paddingLeft;
            }
            int i2 = 0;
            while (i2 < count) {
                float f5 = f2 + (((float) i2) * f3);
                canvas.drawLine(f5, height, f5 + this.g, height, i2 == this.e ? this.f2046b : this.f2045a);
                i2++;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = 0;
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        if (this.f2047c == null || this.f2047c.b().getCount() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        switch (action) {
            case 0:
                this.k = z.b(motionEvent, 0);
                this.j = motionEvent.getX();
                return true;
            case 1:
            case 3:
                if (!this.l) {
                    int count = this.f2047c.b().getCount();
                    int width = getWidth();
                    float f2 = ((float) width) / 2.0f;
                    float f3 = ((float) width) / 6.0f;
                    if (this.e <= 0 || motionEvent.getX() >= f2 - f3) {
                        if (this.e < count - 1 && motionEvent.getX() > f3 + f2) {
                            if (action == 3) {
                                return true;
                            }
                            this.f2047c.a(this.e + 1);
                            return true;
                        }
                    } else if (action == 3) {
                        return true;
                    } else {
                        this.f2047c.a(this.e - 1);
                        return true;
                    }
                }
                this.l = false;
                this.k = -1;
                if (!this.f2047c.g()) {
                    return true;
                }
                this.f2047c.f();
                return true;
            case 2:
                float c2 = z.c(motionEvent, z.a(motionEvent, this.k));
                float f4 = c2 - this.j;
                if (!this.l && Math.abs(f4) > ((float) this.i)) {
                    this.l = true;
                }
                if (!this.l) {
                    return true;
                }
                this.j = c2;
                if (!this.f2047c.g() && !this.f2047c.e()) {
                    return true;
                }
                this.f2047c.b(f4);
                return true;
            case 4:
            default:
                return true;
            case 5:
                int b2 = z.b(motionEvent);
                this.j = z.c(motionEvent, b2);
                this.k = z.b(motionEvent, b2);
                return true;
            case 6:
                int b3 = z.b(motionEvent);
                if (z.b(motionEvent, b3) == this.k) {
                    if (b3 == 0) {
                        i2 = 1;
                    }
                    this.k = z.b(motionEvent, i2);
                }
                this.j = z.c(motionEvent, z.a(motionEvent, this.k));
                return true;
        }
    }

    public void c(int i2) {
        if (this.f2047c == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.f2047c.a(i2);
        this.e = i2;
        invalidate();
    }

    public void b(int i2) {
        if (this.d != null) {
            this.d.b(i2);
        }
    }

    public void a(int i2, float f2, int i3) {
        if (this.d != null) {
            this.d.a(i2, f2, i3);
        }
    }

    public void a(int i2) {
        this.e = i2;
        invalidate();
        if (this.d != null) {
            this.d.a(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        setMeasuredDimension(d(i2), e(i3));
    }

    private int d(int i2) {
        float f2;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || this.f2047c == null) {
            f2 = (float) size;
        } else {
            int count = this.f2047c.b().getCount();
            f2 = (((float) (count - 1)) * this.h) + ((float) (getPaddingLeft() + getPaddingRight())) + (((float) count) * this.g);
            if (mode == Integer.MIN_VALUE) {
                f2 = Math.min(f2, (float) size);
            }
        }
        return (int) FloatMath.ceil(f2);
    }

    private int e(int i2) {
        float strokeWidth;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            strokeWidth = (float) size;
        } else {
            strokeWidth = this.f2046b.getStrokeWidth() + ((float) getPaddingTop()) + ((float) getPaddingBottom());
            if (mode == Integer.MIN_VALUE) {
                strokeWidth = Math.min(strokeWidth, (float) size);
            }
        }
        return (int) FloatMath.ceil(strokeWidth);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.e = savedState.f2048a;
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f2048a = this.e;
        return savedState;
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new f();

        /* renamed from: a  reason: collision with root package name */
        int f2048a;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f2048a = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f2048a);
        }
    }
}
