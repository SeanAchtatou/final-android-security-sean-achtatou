package com.viewpagerindicator;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.bg;
import android.support.v4.view.by;
import android.support.v4.view.z;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

public class CirclePageIndicator extends View implements g {

    /* renamed from: a  reason: collision with root package name */
    private float f2038a;

    /* renamed from: b  reason: collision with root package name */
    private final Paint f2039b;

    /* renamed from: c  reason: collision with root package name */
    private final Paint f2040c;
    private final Paint d;
    private ViewPager e;
    private by f;
    private int g;
    private int h;
    private float i;
    private int j;
    private int k;
    private boolean l;
    private boolean m;
    private int n;
    private float o;
    private int p;
    private boolean q;

    public CirclePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, i.vpiCirclePageIndicatorStyle);
    }

    public CirclePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f2039b = new Paint(1);
        this.f2040c = new Paint(1);
        this.d = new Paint(1);
        this.o = -1.0f;
        this.p = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int color = resources.getColor(k.default_circle_indicator_page_color);
            int color2 = resources.getColor(k.default_circle_indicator_fill_color);
            int integer = resources.getInteger(n.default_circle_indicator_orientation);
            int color3 = resources.getColor(k.default_circle_indicator_stroke_color);
            float dimension = resources.getDimension(l.default_circle_indicator_stroke_width);
            float dimension2 = resources.getDimension(l.default_circle_indicator_radius);
            boolean z = resources.getBoolean(j.default_circle_indicator_centered);
            boolean z2 = resources.getBoolean(j.default_circle_indicator_snap);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, o.f2069a, i2, 0);
            this.l = obtainStyledAttributes.getBoolean(2, z);
            this.k = obtainStyledAttributes.getInt(0, integer);
            this.f2039b.setStyle(Paint.Style.FILL);
            this.f2039b.setColor(obtainStyledAttributes.getColor(5, color));
            this.f2040c.setStyle(Paint.Style.STROKE);
            this.f2040c.setColor(obtainStyledAttributes.getColor(8, color3));
            this.f2040c.setStrokeWidth(obtainStyledAttributes.getDimension(3, dimension));
            this.d.setStyle(Paint.Style.FILL);
            this.d.setColor(obtainStyledAttributes.getColor(4, color2));
            this.f2038a = obtainStyledAttributes.getDimension(6, dimension2);
            this.m = obtainStyledAttributes.getBoolean(7, z2);
            Drawable drawable = obtainStyledAttributes.getDrawable(1);
            if (drawable != null) {
                setBackgroundDrawable(drawable);
            }
            obtainStyledAttributes.recycle();
            this.n = bg.a(ViewConfiguration.get(context));
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int count;
        int height;
        int paddingTop;
        int paddingBottom;
        int paddingLeft;
        float f2;
        float f3;
        super.onDraw(canvas);
        if (this.e != null && (count = this.e.b().getCount()) != 0) {
            if (this.g >= count) {
                c(count - 1);
                return;
            }
            if (this.k == 0) {
                height = getWidth();
                paddingTop = getPaddingLeft();
                paddingBottom = getPaddingRight();
                paddingLeft = getPaddingTop();
            } else {
                height = getHeight();
                paddingTop = getPaddingTop();
                paddingBottom = getPaddingBottom();
                paddingLeft = getPaddingLeft();
            }
            float f4 = this.f2038a * 3.0f;
            float f5 = this.f2038a + ((float) paddingLeft);
            float f6 = ((float) paddingTop) + this.f2038a;
            if (this.l) {
                f6 += (((float) ((height - paddingTop) - paddingBottom)) / 2.0f) - ((((float) count) * f4) / 2.0f);
            }
            float f7 = this.f2038a;
            if (this.f2040c.getStrokeWidth() > 0.0f) {
                f7 -= this.f2040c.getStrokeWidth() / 2.0f;
            }
            for (int i2 = 0; i2 < count; i2++) {
                float f8 = (((float) i2) * f4) + f6;
                if (this.k == 0) {
                    f3 = f8;
                    f8 = f5;
                } else {
                    f3 = f5;
                }
                if (this.f2039b.getAlpha() > 0) {
                    canvas.drawCircle(f3, f8, f7, this.f2039b);
                }
                if (f7 != this.f2038a) {
                    canvas.drawCircle(f3, f8, this.f2038a, this.f2040c);
                }
            }
            float f9 = ((float) (this.m ? this.h : this.g)) * f4;
            if (!this.m) {
                f9 += this.i * f4;
            }
            if (this.k == 0) {
                f2 = f6 + f9;
            } else {
                float f10 = f6 + f9;
                f2 = f5;
                f5 = f10;
            }
            canvas.drawCircle(f2, f5, this.f2038a, this.d);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = 0;
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        if (this.e == null || this.e.b().getCount() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        switch (action) {
            case 0:
                this.p = z.b(motionEvent, 0);
                this.o = motionEvent.getX();
                return true;
            case 1:
            case 3:
                if (!this.q) {
                    int count = this.e.b().getCount();
                    int width = getWidth();
                    float f2 = ((float) width) / 2.0f;
                    float f3 = ((float) width) / 6.0f;
                    if (this.g <= 0 || motionEvent.getX() >= f2 - f3) {
                        if (this.g < count - 1 && motionEvent.getX() > f3 + f2) {
                            if (action == 3) {
                                return true;
                            }
                            this.e.a(this.g + 1);
                            return true;
                        }
                    } else if (action == 3) {
                        return true;
                    } else {
                        this.e.a(this.g - 1);
                        return true;
                    }
                }
                this.q = false;
                this.p = -1;
                if (!this.e.g()) {
                    return true;
                }
                this.e.f();
                return true;
            case 2:
                float c2 = z.c(motionEvent, z.a(motionEvent, this.p));
                float f4 = c2 - this.o;
                if (!this.q && Math.abs(f4) > ((float) this.n)) {
                    this.q = true;
                }
                if (!this.q) {
                    return true;
                }
                this.o = c2;
                if (!this.e.g() && !this.e.e()) {
                    return true;
                }
                this.e.b(f4);
                return true;
            case 4:
            default:
                return true;
            case 5:
                int b2 = z.b(motionEvent);
                this.o = z.c(motionEvent, b2);
                this.p = z.b(motionEvent, b2);
                return true;
            case 6:
                int b3 = z.b(motionEvent);
                if (z.b(motionEvent, b3) == this.p) {
                    if (b3 == 0) {
                        i2 = 1;
                    }
                    this.p = z.b(motionEvent, i2);
                }
                this.o = z.c(motionEvent, z.a(motionEvent, this.p));
                return true;
        }
    }

    public void c(int i2) {
        if (this.e == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.e.a(i2);
        this.g = i2;
        invalidate();
    }

    public void b(int i2) {
        this.j = i2;
        if (this.f != null) {
            this.f.b(i2);
        }
    }

    public void a(int i2, float f2, int i3) {
        this.g = i2;
        this.i = f2;
        invalidate();
        if (this.f != null) {
            this.f.a(i2, f2, i3);
        }
    }

    public void a(int i2) {
        if (this.m || this.j == 0) {
            this.g = i2;
            this.h = i2;
            invalidate();
        }
        if (this.f != null) {
            this.f.a(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.k == 0) {
            setMeasuredDimension(d(i2), e(i3));
        } else {
            setMeasuredDimension(e(i2), d(i3));
        }
    }

    private int d(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || this.e == null) {
            return size;
        }
        int count = this.e.b().getCount();
        int paddingLeft = (int) ((((float) (count - 1)) * this.f2038a) + ((float) (getPaddingLeft() + getPaddingRight())) + (((float) (count * 2)) * this.f2038a) + 1.0f);
        if (mode == Integer.MIN_VALUE) {
            return Math.min(paddingLeft, size);
        }
        return paddingLeft;
    }

    private int e(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        int paddingTop = (int) ((2.0f * this.f2038a) + ((float) getPaddingTop()) + ((float) getPaddingBottom()) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingTop, size) : paddingTop;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.g = savedState.f2041a;
        this.h = savedState.f2041a;
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f2041a = this.g;
        return savedState;
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new b();

        /* renamed from: a  reason: collision with root package name */
        int f2041a;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f2041a = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f2041a);
        }
    }
}
