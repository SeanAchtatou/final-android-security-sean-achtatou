package com.viewpagerindicator;

import android.os.Parcel;
import android.os.Parcelable;
import com.viewpagerindicator.CirclePageIndicator;

/* compiled from: CirclePageIndicator */
final class b implements Parcelable.Creator<CirclePageIndicator.SavedState> {
    b() {
    }

    /* renamed from: a */
    public CirclePageIndicator.SavedState createFromParcel(Parcel parcel) {
        return new CirclePageIndicator.SavedState(parcel);
    }

    /* renamed from: a */
    public CirclePageIndicator.SavedState[] newArray(int i) {
        return new CirclePageIndicator.SavedState[i];
    }
}
