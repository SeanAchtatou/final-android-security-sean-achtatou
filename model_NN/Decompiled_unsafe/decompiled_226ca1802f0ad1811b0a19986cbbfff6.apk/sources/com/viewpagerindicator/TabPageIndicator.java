package com.viewpagerindicator;

import android.content.Context;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v4.view.by;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;

public class TabPageIndicator extends HorizontalScrollView implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final CharSequence f2049a = "";
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Runnable f2050b;

    /* renamed from: c  reason: collision with root package name */
    private final View.OnClickListener f2051c = new p(this);
    private final d d;
    /* access modifiers changed from: private */
    public ViewPager e;
    private by f;
    /* access modifiers changed from: private */
    public int g;
    private int h;
    /* access modifiers changed from: private */
    public r i;

    public TabPageIndicator(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setHorizontalScrollBarEnabled(false);
        this.d = new d(context, i.vpiTabPageIndicatorStyle);
        if (Build.VERSION.SDK_INT >= 17) {
            this.d.setLayoutDirection(0);
        }
        addView(this.d, new ViewGroup.LayoutParams(-2, -1));
    }

    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        boolean z = mode == 1073741824;
        setFillViewport(z);
        int childCount = this.d.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.g = -1;
        } else if (childCount > 2) {
            this.g = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
        } else {
            this.g = View.MeasureSpec.getSize(i2) / 2;
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, i3);
        int measuredWidth2 = getMeasuredWidth();
        if (z && measuredWidth != measuredWidth2) {
            c(this.h);
        }
    }

    private void d(int i2) {
        View childAt = this.d.getChildAt(i2);
        if (this.f2050b != null) {
            removeCallbacks(this.f2050b);
        }
        this.f2050b = new q(this, childAt);
        post(this.f2050b);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f2050b != null) {
            post(this.f2050b);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f2050b != null) {
            removeCallbacks(this.f2050b);
        }
    }

    public void b(int i2) {
        if (this.f != null) {
            this.f.b(i2);
        }
    }

    public void a(int i2, float f2, int i3) {
        if (this.f != null) {
            this.f.a(i2, f2, i3);
        }
    }

    public void a(int i2) {
        c(i2);
        if (this.f != null) {
            this.f.a(i2);
        }
    }

    public void c(int i2) {
        boolean z;
        if (this.e == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.h = i2;
        this.e.a(i2);
        int childCount = this.d.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = this.d.getChildAt(i3);
            if (i3 == i2) {
                z = true;
            } else {
                z = false;
            }
            childAt.setSelected(z);
            if (z) {
                d(i2);
            }
        }
    }
}
