package com.viewpagerindicator;

/* compiled from: R */
public final class k {

    /* renamed from: a */
    public static final int default_circle_indicator_fill_color = 2131296285;

    /* renamed from: b */
    public static final int default_circle_indicator_page_color = 2131296286;

    /* renamed from: c */
    public static final int default_circle_indicator_stroke_color = 2131296287;

    /* renamed from: d */
    public static final int default_line_indicator_selected_color = 2131296288;

    /* renamed from: e */
    public static final int default_line_indicator_unselected_color = 2131296289;

    /* renamed from: f */
    public static final int default_title_indicator_footer_color = 2131296290;

    /* renamed from: g */
    public static final int default_title_indicator_selected_color = 2131296291;

    /* renamed from: h */
    public static final int default_title_indicator_text_color = 2131296292;

    /* renamed from: i */
    public static final int default_underline_indicator_selected_color = 2131296293;
}
