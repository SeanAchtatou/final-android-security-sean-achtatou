package com.viewpagerindicator;

import android.view.View;

/* compiled from: TabPageIndicator */
class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TabPageIndicator f2072a;

    p(TabPageIndicator tabPageIndicator) {
        this.f2072a = tabPageIndicator;
    }

    public void onClick(View view) {
        int c2 = this.f2072a.e.c();
        int a2 = ((s) view).a();
        this.f2072a.e.a(a2);
        if (c2 == a2 && this.f2072a.i != null) {
            this.f2072a.i.a(a2);
        }
    }
}
