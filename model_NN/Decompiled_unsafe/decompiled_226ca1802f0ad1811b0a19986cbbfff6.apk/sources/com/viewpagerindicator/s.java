package com.viewpagerindicator;

import android.view.View;
import android.widget.TextView;

/* compiled from: TabPageIndicator */
class s extends TextView {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TabPageIndicator f2075a;

    /* renamed from: b  reason: collision with root package name */
    private int f2076b;

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f2075a.g > 0 && getMeasuredWidth() > this.f2075a.g) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(this.f2075a.g, 1073741824), i2);
        }
    }

    public int a() {
        return this.f2076b;
    }
}
