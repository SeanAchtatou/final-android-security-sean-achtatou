package com.google.protobuf;

public interface MessageLiteOrBuilder {
    boolean isInitialized();
}
