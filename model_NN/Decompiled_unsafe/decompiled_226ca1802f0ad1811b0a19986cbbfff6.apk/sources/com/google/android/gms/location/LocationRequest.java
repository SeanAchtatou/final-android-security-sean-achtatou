package com.google.android.gms.location;

import android.os.Parcel;
import android.os.SystemClock;
import com.actionbarsherlock.widget.ActivityChooserView;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.r;

public final class LocationRequest implements SafeParcelable {
    public static final LocationRequestCreator CREATOR = new LocationRequestCreator();
    private final int ab;
    long fB;
    long fC;
    boolean fD;
    int fE;
    float fF;
    long fw;
    int mPriority;

    public LocationRequest() {
        this.ab = 1;
        this.mPriority = 102;
        this.fB = 3600000;
        this.fC = 600000;
        this.fD = false;
        this.fw = Long.MAX_VALUE;
        this.fE = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        this.fF = 0.0f;
    }

    LocationRequest(int i, int i2, long j, long j2, boolean z, long j3, int i3, float f) {
        this.ab = i;
        this.mPriority = i2;
        this.fB = j;
        this.fC = j2;
        this.fD = z;
        this.fw = j3;
        this.fE = i3;
        this.fF = f;
    }

    public static String N(int i) {
        switch (i) {
            case 100:
                return "PRIORITY_HIGH_ACCURACY";
            case 101:
            case 103:
            default:
                return "???";
            case 102:
                return "PRIORITY_BALANCED_POWER_ACCURACY";
            case 104:
                return "PRIORITY_LOW_POWER";
            case 105:
                return "PRIORITY_NO_POWER";
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocationRequest)) {
            return false;
        }
        LocationRequest locationRequest = (LocationRequest) obj;
        return this.mPriority == locationRequest.mPriority && this.fB == locationRequest.fB && this.fC == locationRequest.fC && this.fD == locationRequest.fD && this.fw == locationRequest.fw && this.fE == locationRequest.fE && this.fF == locationRequest.fF;
    }

    public int hashCode() {
        return r.hashCode(Integer.valueOf(this.mPriority), Long.valueOf(this.fB), Long.valueOf(this.fC), Boolean.valueOf(this.fD), Long.valueOf(this.fw), Integer.valueOf(this.fE), Float.valueOf(this.fF));
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Request[").append(N(this.mPriority));
        if (this.mPriority != 105) {
            sb.append(" requested=");
            sb.append(this.fB + "ms");
        }
        sb.append(" fastest=");
        sb.append(this.fC + "ms");
        if (this.fw != Long.MAX_VALUE) {
            long elapsedRealtime = this.fw - SystemClock.elapsedRealtime();
            sb.append(" expireIn=");
            sb.append(elapsedRealtime + "ms");
        }
        if (this.fE != Integer.MAX_VALUE) {
            sb.append(" num=").append(this.fE);
        }
        sb.append(']');
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        LocationRequestCreator.a(this, parcel, i);
    }
}
