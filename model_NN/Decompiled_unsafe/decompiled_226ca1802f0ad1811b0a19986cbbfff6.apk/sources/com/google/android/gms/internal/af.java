package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.ae;

public class af implements Parcelable.Creator<ae.a> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.z, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(ae.a aVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, aVar.i());
        b.c(parcel, 2, aVar.R());
        b.a(parcel, 3, aVar.X());
        b.c(parcel, 4, aVar.S());
        b.a(parcel, 5, aVar.Y());
        b.a(parcel, 6, aVar.Z(), false);
        b.c(parcel, 7, aVar.aa());
        b.a(parcel, 8, aVar.ac(), false);
        b.a(parcel, 9, (Parcelable) aVar.ae(), i, false);
        b.C(parcel, d);
    }

    /* renamed from: i */
    public ae.a createFromParcel(Parcel parcel) {
        z zVar = null;
        int i = 0;
        int c2 = a.c(parcel);
        String str = null;
        String str2 = null;
        boolean z = false;
        int i2 = 0;
        boolean z2 = false;
        int i3 = 0;
        int i4 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i4 = a.f(parcel, b2);
                    break;
                case 2:
                    i3 = a.f(parcel, b2);
                    break;
                case 3:
                    z2 = a.c(parcel, b2);
                    break;
                case 4:
                    i2 = a.f(parcel, b2);
                    break;
                case 5:
                    z = a.c(parcel, b2);
                    break;
                case 6:
                    str2 = a.l(parcel, b2);
                    break;
                case 7:
                    i = a.f(parcel, b2);
                    break;
                case 8:
                    str = a.l(parcel, b2);
                    break;
                case 9:
                    zVar = (z) a.a(parcel, b2, z.CREATOR);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new ae.a(i4, i3, z2, i2, z, str2, i, str, zVar);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: r */
    public ae.a[] newArray(int i) {
        return new ae.a[i];
    }
}
