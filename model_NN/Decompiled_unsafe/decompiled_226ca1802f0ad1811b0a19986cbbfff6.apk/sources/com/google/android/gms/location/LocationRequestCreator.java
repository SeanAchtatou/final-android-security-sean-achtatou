package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.actionbarsherlock.widget.ActivityChooserView;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;

public class LocationRequestCreator implements Parcelable.Creator<LocationRequest> {
    static void a(LocationRequest locationRequest, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, locationRequest.mPriority);
        b.c(parcel, 1000, locationRequest.i());
        b.a(parcel, 2, locationRequest.fB);
        b.a(parcel, 3, locationRequest.fC);
        b.a(parcel, 4, locationRequest.fD);
        b.a(parcel, 5, locationRequest.fw);
        b.c(parcel, 6, locationRequest.fE);
        b.a(parcel, 7, locationRequest.fF);
        b.C(parcel, d);
    }

    public LocationRequest createFromParcel(Parcel parcel) {
        boolean z = false;
        int c2 = a.c(parcel);
        int i = 102;
        long j = 3600000;
        long j2 = 600000;
        long j3 = Long.MAX_VALUE;
        int i2 = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        float f = 0.0f;
        int i3 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i = a.f(parcel, b2);
                    break;
                case 2:
                    j = a.g(parcel, b2);
                    break;
                case 3:
                    j2 = a.g(parcel, b2);
                    break;
                case 4:
                    z = a.c(parcel, b2);
                    break;
                case 5:
                    j3 = a.g(parcel, b2);
                    break;
                case 6:
                    i2 = a.f(parcel, b2);
                    break;
                case 7:
                    f = a.i(parcel, b2);
                    break;
                case 1000:
                    i3 = a.f(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new LocationRequest(i3, i, j, j2, z, j3, i2, f);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    public LocationRequest[] newArray(int i) {
        return new LocationRequest[i];
    }
}
