package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;

public class PolygonOptionsCreator implements Parcelable.Creator<PolygonOptions> {
    static void a(PolygonOptions polygonOptions, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, polygonOptions.i());
        b.b(parcel, 2, polygonOptions.getPoints(), false);
        b.c(parcel, 3, polygonOptions.br(), false);
        b.a(parcel, 4, polygonOptions.getStrokeWidth());
        b.c(parcel, 5, polygonOptions.getStrokeColor());
        b.c(parcel, 6, polygonOptions.getFillColor());
        b.a(parcel, 7, polygonOptions.getZIndex());
        b.a(parcel, 8, polygonOptions.isVisible());
        b.a(parcel, 9, polygonOptions.isGeodesic());
        b.C(parcel, d);
    }

    public PolygonOptions createFromParcel(Parcel parcel) {
        float f = 0.0f;
        boolean z = false;
        int c2 = a.c(parcel);
        ArrayList arrayList = null;
        ArrayList arrayList2 = new ArrayList();
        boolean z2 = false;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        int i3 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i3 = a.f(parcel, b2);
                    break;
                case 2:
                    arrayList = a.c(parcel, b2, LatLng.CREATOR);
                    break;
                case 3:
                    a.a(parcel, b2, arrayList2, getClass().getClassLoader());
                    break;
                case 4:
                    f2 = a.i(parcel, b2);
                    break;
                case 5:
                    i2 = a.f(parcel, b2);
                    break;
                case 6:
                    i = a.f(parcel, b2);
                    break;
                case 7:
                    f = a.i(parcel, b2);
                    break;
                case 8:
                    z2 = a.c(parcel, b2);
                    break;
                case 9:
                    z = a.c(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new PolygonOptions(i3, arrayList, arrayList2, f2, i2, i, f, z2, z);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    public PolygonOptions[] newArray(int i) {
        return new PolygonOptions[i];
    }
}
