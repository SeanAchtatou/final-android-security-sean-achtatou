package com.google.android.gms.plus;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public final class PlusOneButtonWithPopup extends ViewGroup {
    private int O;
    private View ic;
    private int id;
    private View.OnClickListener ij;

    public PlusOneButtonWithPopup(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.O = PlusOneButton.getSize(context, attributeSet);
        this.id = PlusOneButton.getAnnotation(context, attributeSet);
        this.ic = new PlusOneDummyView(context, this.O);
        addView(this.ic);
    }

    private int c(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        switch (mode) {
            case Integer.MIN_VALUE:
            case 1073741824:
                return View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i) - i2, mode);
            default:
                return i;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.ic.layout(getPaddingLeft(), getPaddingTop(), (i3 - i) - getPaddingRight(), (i4 - i2) - getPaddingBottom());
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        this.ic.measure(c(i, paddingLeft), c(i2, paddingTop));
        setMeasuredDimension(paddingLeft + this.ic.getMeasuredWidth(), paddingTop + this.ic.getMeasuredHeight());
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.ij = onClickListener;
        this.ic.setOnClickListener(onClickListener);
    }
}
