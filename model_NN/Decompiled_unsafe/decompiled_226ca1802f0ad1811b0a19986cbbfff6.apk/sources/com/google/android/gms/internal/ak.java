package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.ae;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ak extends ae implements SafeParcelable {
    public static final al CREATOR = new al();
    private final int ab;
    private final ah cB;
    private final Parcel cJ;
    private final int cK = 2;
    private int cL;
    private int cM;
    private final String mClassName;

    ak(int i, Parcel parcel, ah ahVar) {
        this.ab = i;
        this.cJ = (Parcel) s.d(parcel);
        this.cB = ahVar;
        if (this.cB == null) {
            this.mClassName = null;
        } else {
            this.mClassName = this.cB.aj();
        }
        this.cL = 2;
    }

    public static HashMap<String, String> a(Bundle bundle) {
        HashMap<String, String> hashMap = new HashMap<>();
        for (String next : bundle.keySet()) {
            hashMap.put(next, bundle.getString(next));
        }
        return hashMap;
    }

    private void a(StringBuilder sb, int i, Object obj) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                sb.append(obj);
                return;
            case 7:
                sb.append("\"").append(aq.r(obj.toString())).append("\"");
                return;
            case 8:
                sb.append("\"").append(an.a((byte[]) obj)).append("\"");
                return;
            case 9:
                sb.append("\"").append(an.b((byte[]) obj));
                sb.append("\"");
                return;
            case 10:
                ar.a(sb, (HashMap) obj);
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown type = " + i);
        }
    }

    private void a(StringBuilder sb, ae.a<?, ?> aVar, Parcel parcel, int i) {
        switch (aVar.S()) {
            case 0:
                b(sb, aVar, a(aVar, Integer.valueOf(a.f(parcel, i))));
                return;
            case 1:
                b(sb, aVar, a(aVar, a.h(parcel, i)));
                return;
            case 2:
                b(sb, aVar, a(aVar, Long.valueOf(a.g(parcel, i))));
                return;
            case 3:
                b(sb, aVar, a(aVar, Float.valueOf(a.i(parcel, i))));
                return;
            case 4:
                b(sb, aVar, a(aVar, Double.valueOf(a.j(parcel, i))));
                return;
            case 5:
                b(sb, aVar, a(aVar, a.k(parcel, i)));
                return;
            case 6:
                b(sb, aVar, a(aVar, Boolean.valueOf(a.c(parcel, i))));
                return;
            case 7:
                b(sb, aVar, a(aVar, a.l(parcel, i)));
                return;
            case 8:
            case 9:
                b(sb, aVar, a(aVar, a.o(parcel, i)));
                return;
            case 10:
                b(sb, aVar, a(aVar, a(a.n(parcel, i))));
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown field out type = " + aVar.S());
        }
    }

    private void a(StringBuilder sb, String str, ae.a<?, ?> aVar, Parcel parcel, int i) {
        sb.append("\"").append(str).append("\":");
        if (aVar.ad()) {
            a(sb, aVar, parcel, i);
        } else {
            b(sb, aVar, parcel, i);
        }
    }

    private void a(StringBuilder sb, HashMap<String, ae.a<?, ?>> hashMap, Parcel parcel) {
        HashMap<Integer, Map.Entry<String, ae.a<?, ?>>> b2 = b(hashMap);
        sb.append('{');
        int c2 = a.c(parcel);
        boolean z = false;
        while (parcel.dataPosition() < c2) {
            int b3 = a.b(parcel);
            Map.Entry entry = b2.get(Integer.valueOf(a.m(b3)));
            if (entry != null) {
                if (z) {
                    sb.append(",");
                }
                a(sb, (String) entry.getKey(), (ae.a) entry.getValue(), parcel, b3);
                z = true;
            }
        }
        if (parcel.dataPosition() != c2) {
            throw new a.C0001a("Overread allowed size end=" + c2, parcel);
        }
        sb.append('}');
    }

    private static HashMap<Integer, Map.Entry<String, ae.a<?, ?>>> b(HashMap<String, ae.a<?, ?>> hashMap) {
        HashMap<Integer, Map.Entry<String, ae.a<?, ?>>> hashMap2 = new HashMap<>();
        for (Map.Entry next : hashMap.entrySet()) {
            hashMap2.put(Integer.valueOf(((ae.a) next.getValue()).aa()), next);
        }
        return hashMap2;
    }

    private void b(StringBuilder sb, ae.a<?, ?> aVar, Parcel parcel, int i) {
        if (aVar.Y()) {
            sb.append("[");
            switch (aVar.S()) {
                case 0:
                    am.a(sb, a.q(parcel, i));
                    break;
                case 1:
                    am.a(sb, a.s(parcel, i));
                    break;
                case 2:
                    am.a(sb, a.r(parcel, i));
                    break;
                case 3:
                    am.a(sb, a.t(parcel, i));
                    break;
                case 4:
                    am.a(sb, a.u(parcel, i));
                    break;
                case 5:
                    am.a(sb, a.v(parcel, i));
                    break;
                case 6:
                    am.a(sb, a.p(parcel, i));
                    break;
                case 7:
                    am.a(sb, a.w(parcel, i));
                    break;
                case 8:
                case 9:
                case 10:
                    throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                case 11:
                    Parcel[] z = a.z(parcel, i);
                    int length = z.length;
                    for (int i2 = 0; i2 < length; i2++) {
                        if (i2 > 0) {
                            sb.append(",");
                        }
                        z[i2].setDataPosition(0);
                        a(sb, aVar.af(), z[i2]);
                    }
                    break;
                default:
                    throw new IllegalStateException("Unknown field type out.");
            }
            sb.append("]");
            return;
        }
        switch (aVar.S()) {
            case 0:
                sb.append(a.f(parcel, i));
                return;
            case 1:
                sb.append(a.h(parcel, i));
                return;
            case 2:
                sb.append(a.g(parcel, i));
                return;
            case 3:
                sb.append(a.i(parcel, i));
                return;
            case 4:
                sb.append(a.j(parcel, i));
                return;
            case 5:
                sb.append(a.k(parcel, i));
                return;
            case 6:
                sb.append(a.c(parcel, i));
                return;
            case 7:
                sb.append("\"").append(aq.r(a.l(parcel, i))).append("\"");
                return;
            case 8:
                sb.append("\"").append(an.a(a.o(parcel, i))).append("\"");
                return;
            case 9:
                sb.append("\"").append(an.b(a.o(parcel, i)));
                sb.append("\"");
                return;
            case 10:
                Bundle n = a.n(parcel, i);
                Set<String> keySet = n.keySet();
                keySet.size();
                sb.append("{");
                boolean z2 = true;
                for (String next : keySet) {
                    if (!z2) {
                        sb.append(",");
                    }
                    sb.append("\"").append(next).append("\"");
                    sb.append(":");
                    sb.append("\"").append(aq.r(n.getString(next))).append("\"");
                    z2 = false;
                }
                sb.append("}");
                return;
            case 11:
                Parcel y = a.y(parcel, i);
                y.setDataPosition(0);
                a(sb, aVar.af(), y);
                return;
            default:
                throw new IllegalStateException("Unknown field type out");
        }
    }

    private void b(StringBuilder sb, ae.a<?, ?> aVar, Object obj) {
        if (aVar.X()) {
            b(sb, aVar, (ArrayList<?>) ((ArrayList) obj));
        } else {
            a(sb, aVar.R(), obj);
        }
    }

    private void b(StringBuilder sb, ae.a<?, ?> aVar, ArrayList<?> arrayList) {
        sb.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                sb.append(",");
            }
            a(sb, aVar.R(), arrayList.get(i));
        }
        sb.append("]");
    }

    public HashMap<String, ae.a<?, ?>> T() {
        if (this.cB == null) {
            return null;
        }
        return this.cB.q(this.mClassName);
    }

    public Parcel al() {
        switch (this.cL) {
            case 0:
                this.cM = b.d(this.cJ);
                b.C(this.cJ, this.cM);
                this.cL = 2;
                break;
            case 1:
                b.C(this.cJ, this.cM);
                this.cL = 2;
                break;
        }
        return this.cJ;
    }

    /* access modifiers changed from: package-private */
    public ah am() {
        switch (this.cK) {
            case 0:
                return null;
            case 1:
                return this.cB;
            case 2:
                return this.cB;
            default:
                throw new IllegalStateException("Invalid creation type: " + this.cK);
        }
    }

    public int describeContents() {
        al alVar = CREATOR;
        return 0;
    }

    public int i() {
        return this.ab;
    }

    /* access modifiers changed from: protected */
    public Object m(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    /* access modifiers changed from: protected */
    public boolean n(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    public String toString() {
        s.b(this.cB, "Cannot convert to JSON on client side.");
        Parcel al = al();
        al.setDataPosition(0);
        StringBuilder sb = new StringBuilder(100);
        a(sb, this.cB.q(this.mClassName), al);
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        al alVar = CREATOR;
        al.a(this, parcel, i);
    }
}
