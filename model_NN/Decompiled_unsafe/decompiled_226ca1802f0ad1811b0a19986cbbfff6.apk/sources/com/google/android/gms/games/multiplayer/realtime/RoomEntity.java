package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import com.google.android.gms.internal.av;
import com.google.android.gms.internal.r;
import java.util.ArrayList;

public final class RoomEntity extends av implements Room {
    public static final Parcelable.Creator<RoomEntity> CREATOR = new a();
    private final int ab;
    private final String dV;
    private final String di;
    private final long eG;
    private final ArrayList<ParticipantEntity> eJ;
    private final int eK;
    private final Bundle fa;
    private final String fe;
    private final int ff;
    private final int fg;

    final class a extends b {
        a() {
        }

        /* renamed from: s */
        public RoomEntity createFromParcel(Parcel parcel) {
            if (RoomEntity.c(RoomEntity.v()) || RoomEntity.h(RoomEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            long readLong = parcel.readLong();
            int readInt = parcel.readInt();
            String readString3 = parcel.readString();
            int readInt2 = parcel.readInt();
            Bundle readBundle = parcel.readBundle();
            int readInt3 = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt3);
            for (int i = 0; i < readInt3; i++) {
                arrayList.add(ParticipantEntity.CREATOR.createFromParcel(parcel));
            }
            return new RoomEntity(2, readString, readString2, readLong, readInt, readString3, readInt2, readBundle, arrayList, -1);
        }
    }

    RoomEntity(int i, String str, String str2, long j, int i2, String str3, int i3, Bundle bundle, ArrayList<ParticipantEntity> arrayList, int i4) {
        this.ab = i;
        this.dV = str;
        this.fe = str2;
        this.eG = j;
        this.ff = i2;
        this.di = str3;
        this.eK = i3;
        this.fa = bundle;
        this.eJ = arrayList;
        this.fg = i4;
    }

    public RoomEntity(Room room) {
        this.ab = 2;
        this.dV = room.getRoomId();
        this.fe = room.getCreatorId();
        this.eG = room.getCreationTimestamp();
        this.ff = room.getStatus();
        this.di = room.getDescription();
        this.eK = room.getVariant();
        this.fa = room.getAutoMatchCriteria();
        ArrayList<Participant> participants = room.getParticipants();
        int size = participants.size();
        this.eJ = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            this.eJ.add((ParticipantEntity) participants.get(i).freeze());
        }
        this.fg = room.getAutoMatchWaitEstimateSeconds();
    }

    static int a(Room room) {
        return r.hashCode(room.getRoomId(), room.getCreatorId(), Long.valueOf(room.getCreationTimestamp()), Integer.valueOf(room.getStatus()), room.getDescription(), Integer.valueOf(room.getVariant()), room.getAutoMatchCriteria(), room.getParticipants(), Integer.valueOf(room.getAutoMatchWaitEstimateSeconds()));
    }

    static boolean a(Room room, Object obj) {
        if (!(obj instanceof Room)) {
            return false;
        }
        if (room == obj) {
            return true;
        }
        Room room2 = (Room) obj;
        return r.a(room2.getRoomId(), room.getRoomId()) && r.a(room2.getCreatorId(), room.getCreatorId()) && r.a(Long.valueOf(room2.getCreationTimestamp()), Long.valueOf(room.getCreationTimestamp())) && r.a(Integer.valueOf(room2.getStatus()), Integer.valueOf(room.getStatus())) && r.a(room2.getDescription(), room.getDescription()) && r.a(Integer.valueOf(room2.getVariant()), Integer.valueOf(room.getVariant())) && r.a(room2.getAutoMatchCriteria(), room.getAutoMatchCriteria()) && r.a(room2.getParticipants(), room.getParticipants()) && r.a(Integer.valueOf(room2.getAutoMatchWaitEstimateSeconds()), Integer.valueOf(room.getAutoMatchWaitEstimateSeconds()));
    }

    static String b(Room room) {
        return r.c(room).a("RoomId", room.getRoomId()).a("CreatorId", room.getCreatorId()).a("CreationTimestamp", Long.valueOf(room.getCreationTimestamp())).a("RoomStatus", Integer.valueOf(room.getStatus())).a("Description", room.getDescription()).a("Variant", Integer.valueOf(room.getVariant())).a("AutoMatchCriteria", room.getAutoMatchCriteria()).a("Participants", room.getParticipants()).a("AutoMatchWaitEstimateSeconds", Integer.valueOf(room.getAutoMatchWaitEstimateSeconds())).toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public Room freeze() {
        return this;
    }

    public Bundle getAutoMatchCriteria() {
        return this.fa;
    }

    public int getAutoMatchWaitEstimateSeconds() {
        return this.fg;
    }

    public long getCreationTimestamp() {
        return this.eG;
    }

    public String getCreatorId() {
        return this.fe;
    }

    public String getDescription() {
        return this.di;
    }

    public ArrayList<Participant> getParticipants() {
        return new ArrayList<>(this.eJ);
    }

    public String getRoomId() {
        return this.dV;
    }

    public int getStatus() {
        return this.ff;
    }

    public int getVariant() {
        return this.eK;
    }

    public int hashCode() {
        return a(this);
    }

    public int i() {
        return this.ab;
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (!w()) {
            b.a(this, parcel, i);
            return;
        }
        parcel.writeString(this.dV);
        parcel.writeString(this.fe);
        parcel.writeLong(this.eG);
        parcel.writeInt(this.ff);
        parcel.writeString(this.di);
        parcel.writeInt(this.eK);
        parcel.writeBundle(this.fa);
        int size = this.eJ.size();
        parcel.writeInt(size);
        for (int i2 = 0; i2 < size; i2++) {
            this.eJ.get(i2).writeToParcel(parcel, i);
        }
    }
}
