package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;

public class y implements Parcelable.Creator<x> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(x xVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, xVar.getType());
        b.c(parcel, 1000, xVar.i());
        b.c(parcel, 2, xVar.I());
        b.a(parcel, 3, xVar.J(), false);
        b.a(parcel, 4, xVar.K(), false);
        b.a(parcel, 5, xVar.getDisplayName(), false);
        b.a(parcel, 6, xVar.L(), false);
        b.C(parcel, d);
    }

    /* renamed from: e */
    public x createFromParcel(Parcel parcel) {
        int i = 0;
        String str = null;
        int c2 = a.c(parcel);
        String str2 = null;
        String str3 = null;
        String str4 = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i2 = a.f(parcel, b2);
                    break;
                case 2:
                    i = a.f(parcel, b2);
                    break;
                case 3:
                    str4 = a.l(parcel, b2);
                    break;
                case 4:
                    str3 = a.l(parcel, b2);
                    break;
                case 5:
                    str2 = a.l(parcel, b2);
                    break;
                case 6:
                    str = a.l(parcel, b2);
                    break;
                case 1000:
                    i3 = a.f(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new x(i3, i2, i, str4, str3, str2, str);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: n */
    public x[] newArray(int i) {
        return new x[i];
    }
}
