package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.internal.q;
import com.google.android.gms.maps.model.internal.g;

public final class TileOverlayOptions implements SafeParcelable {
    public static final TileOverlayOptionsCreator CREATOR = new TileOverlayOptionsCreator();
    private final int ab;
    /* access modifiers changed from: private */
    public g hG;
    private TileProvider hH;
    private float hb;
    private boolean hc;

    public TileOverlayOptions() {
        this.hc = true;
        this.ab = 1;
    }

    TileOverlayOptions(int i, IBinder iBinder, boolean z, float f) {
        this.hc = true;
        this.ab = i;
        this.hG = g.a.U(iBinder);
        this.hH = this.hG == null ? null : new TileProvider() {
            private final g hI = TileOverlayOptions.this.hG;
        };
        this.hc = z;
        this.hb = f;
    }

    /* access modifiers changed from: package-private */
    public IBinder bs() {
        return this.hG.asBinder();
    }

    public int describeContents() {
        return 0;
    }

    public float getZIndex() {
        return this.hb;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    public boolean isVisible() {
        return this.hc;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (q.bn()) {
            j.a(this, parcel, i);
        } else {
            TileOverlayOptionsCreator.a(this, parcel, i);
        }
    }
}
