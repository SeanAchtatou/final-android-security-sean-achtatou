package com.google.android.gms.internal;

import android.content.Intent;
import android.net.Uri;

public class m {
    private static final Uri bV = Uri.parse("http://plus.google.com/");
    private static final Uri bW = bV.buildUpon().appendPath("circles").appendPath("find").build();

    public static Intent i(String str) {
        Uri fromParts = Uri.fromParts("package", str, null);
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(fromParts);
        return intent;
    }

    private static Uri j(String str) {
        return Uri.parse("market://details").buildUpon().appendQueryParameter("id", str).build();
    }

    public static Intent k(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(j(str));
        intent.setPackage("com.android.vending");
        intent.addFlags(524288);
        return intent;
    }

    public static Intent l(String str) {
        Uri parse = Uri.parse("bazaar://search?q=pname:" + str);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(parse);
        intent.setFlags(524288);
        return intent;
    }
}
