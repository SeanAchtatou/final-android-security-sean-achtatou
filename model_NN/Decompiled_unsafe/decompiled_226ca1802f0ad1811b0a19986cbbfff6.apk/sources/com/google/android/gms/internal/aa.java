package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;

public class aa implements Parcelable.Creator<z> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.ab, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(z zVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, zVar.i());
        b.a(parcel, 2, (Parcelable) zVar.O(), i, false);
        b.C(parcel, d);
    }

    /* renamed from: f */
    public z createFromParcel(Parcel parcel) {
        int c2 = a.c(parcel);
        int i = 0;
        ab abVar = null;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i = a.f(parcel, b2);
                    break;
                case 2:
                    abVar = (ab) a.a(parcel, b2, ab.CREATOR);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new z(i, abVar);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: o */
    public z[] newArray(int i) {
        return new z[i];
    }
}
