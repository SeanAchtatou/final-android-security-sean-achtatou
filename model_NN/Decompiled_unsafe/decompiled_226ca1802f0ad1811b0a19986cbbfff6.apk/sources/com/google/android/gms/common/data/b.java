package com.google.android.gms.common.data;

import android.net.Uri;
import com.google.android.gms.internal.r;
import com.google.android.gms.internal.s;

public abstract class b {
    protected final d S;
    protected final int V;
    private final int W;

    public b(d dVar, int i) {
        this.S = (d) s.d(dVar);
        s.a(i >= 0 && i < dVar.getCount());
        this.V = i;
        this.W = dVar.e(this.V);
    }

    /* access modifiers changed from: protected */
    public Uri d(String str) {
        return this.S.f(str, this.V, this.W);
    }

    /* access modifiers changed from: protected */
    public boolean e(String str) {
        return this.S.g(str, this.V, this.W);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return r.a(Integer.valueOf(bVar.V), Integer.valueOf(this.V)) && r.a(Integer.valueOf(bVar.W), Integer.valueOf(this.W)) && bVar.S == this.S;
    }

    /* access modifiers changed from: protected */
    public boolean getBoolean(String str) {
        return this.S.d(str, this.V, this.W);
    }

    /* access modifiers changed from: protected */
    public int getInteger(String str) {
        return this.S.b(str, this.V, this.W);
    }

    /* access modifiers changed from: protected */
    public long getLong(String str) {
        return this.S.a(str, this.V, this.W);
    }

    /* access modifiers changed from: protected */
    public String getString(String str) {
        return this.S.c(str, this.V, this.W);
    }

    public int hashCode() {
        return r.hashCode(Integer.valueOf(this.V), Integer.valueOf(this.W), this.S);
    }
}
