package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.b;
import com.google.android.gms.games.Player;

public final class d extends b implements Participant {
    private final com.google.android.gms.games.d eS;

    public d(com.google.android.gms.common.data.d dVar, int i) {
        super(dVar, i);
        this.eS = new com.google.android.gms.games.d(dVar, i);
    }

    public String aM() {
        return getString("client_address");
    }

    public int aN() {
        return getInteger("capabilities");
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return ParticipantEntity.a(this, obj);
    }

    public Participant freeze() {
        return new ParticipantEntity(this);
    }

    public String getDisplayName() {
        return e("external_player_id") ? getString("default_display_name") : this.eS.getDisplayName();
    }

    public Uri getHiResImageUri() {
        if (e("external_player_id")) {
            return null;
        }
        return this.eS.getHiResImageUri();
    }

    public Uri getIconImageUri() {
        return e("external_player_id") ? d("default_display_image_uri") : this.eS.getIconImageUri();
    }

    public String getParticipantId() {
        return getString("external_participant_id");
    }

    public Player getPlayer() {
        if (e("external_player_id")) {
            return null;
        }
        return this.eS;
    }

    public int getStatus() {
        return getInteger("player_status");
    }

    public int hashCode() {
        return ParticipantEntity.a(this);
    }

    public boolean isConnectedToRoom() {
        return getInteger("connected") > 0;
    }

    public String toString() {
        return ParticipantEntity.b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        ((ParticipantEntity) freeze()).writeToParcel(parcel, i);
    }
}
