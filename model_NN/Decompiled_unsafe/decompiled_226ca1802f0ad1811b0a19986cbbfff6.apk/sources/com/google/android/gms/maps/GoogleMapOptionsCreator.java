package com.google.android.gms.maps;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.maps.model.CameraPosition;

public class GoogleMapOptionsCreator implements Parcelable.Creator<GoogleMapOptions> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.CameraPosition, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(GoogleMapOptions googleMapOptions, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, googleMapOptions.i());
        b.a(parcel, 2, googleMapOptions.aZ());
        b.a(parcel, 3, googleMapOptions.ba());
        b.c(parcel, 4, googleMapOptions.getMapType());
        b.a(parcel, 5, (Parcelable) googleMapOptions.getCamera(), i, false);
        b.a(parcel, 6, googleMapOptions.bb());
        b.a(parcel, 7, googleMapOptions.bc());
        b.a(parcel, 8, googleMapOptions.bd());
        b.a(parcel, 9, googleMapOptions.be());
        b.a(parcel, 10, googleMapOptions.bf());
        b.a(parcel, 11, googleMapOptions.bg());
        b.C(parcel, d);
    }

    public GoogleMapOptions createFromParcel(Parcel parcel) {
        byte b2 = 0;
        int c2 = a.c(parcel);
        CameraPosition cameraPosition = null;
        byte b3 = 0;
        byte b4 = 0;
        byte b5 = 0;
        byte b6 = 0;
        byte b7 = 0;
        int i = 0;
        byte b8 = 0;
        byte b9 = 0;
        int i2 = 0;
        while (parcel.dataPosition() < c2) {
            int b10 = a.b(parcel);
            switch (a.m(b10)) {
                case 1:
                    i2 = a.f(parcel, b10);
                    break;
                case 2:
                    b9 = a.d(parcel, b10);
                    break;
                case 3:
                    b8 = a.d(parcel, b10);
                    break;
                case 4:
                    i = a.f(parcel, b10);
                    break;
                case 5:
                    cameraPosition = (CameraPosition) a.a(parcel, b10, CameraPosition.CREATOR);
                    break;
                case 6:
                    b7 = a.d(parcel, b10);
                    break;
                case 7:
                    b6 = a.d(parcel, b10);
                    break;
                case 8:
                    b5 = a.d(parcel, b10);
                    break;
                case 9:
                    b4 = a.d(parcel, b10);
                    break;
                case 10:
                    b3 = a.d(parcel, b10);
                    break;
                case 11:
                    b2 = a.d(parcel, b10);
                    break;
                default:
                    a.b(parcel, b10);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new GoogleMapOptions(i2, b9, b8, i, cameraPosition, b7, b6, b5, b4, b3, b2);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    public GoogleMapOptions[] newArray(int i) {
        return new GoogleMapOptions[i];
    }
}
