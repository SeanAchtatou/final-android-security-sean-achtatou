package com.google.android.gms.common;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import com.google.android.gms.dynamic.e;
import com.google.android.gms.internal.s;
import com.google.android.gms.internal.t;
import com.google.android.gms.internal.u;

public final class SignInButton extends FrameLayout implements View.OnClickListener {
    private int O;
    private int P;
    private View Q;
    private View.OnClickListener R;

    public SignInButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SignInButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.R = null;
        setStyle(0, 0);
    }

    private static Button c(Context context, int i, int i2) {
        u uVar = new u(context);
        uVar.a(context.getResources(), i, i2);
        return uVar;
    }

    private void d(Context context) {
        if (this.Q != null) {
            removeView(this.Q);
        }
        try {
            this.Q = t.d(context, this.O, this.P);
        } catch (e.a e) {
            Log.w("SignInButton", "Sign in button not found, using placeholder instead");
            this.Q = c(context, this.O, this.P);
        }
        addView(this.Q);
        this.Q.setEnabled(isEnabled());
        this.Q.setOnClickListener(this);
    }

    public void onClick(View view) {
        if (this.R != null && view == this.Q) {
            this.R.onClick(this);
        }
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.Q.setEnabled(z);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.R = onClickListener;
        if (this.Q != null) {
            this.Q.setOnClickListener(this);
        }
    }

    public void setStyle(int i, int i2) {
        boolean z = true;
        s.a(i >= 0 && i < 3, "Unknown button size " + i);
        if (i2 < 0 || i2 >= 2) {
            z = false;
        }
        s.a(z, "Unknown color scheme " + i2);
        this.O = i;
        this.P = i2;
        d(getContext());
    }
}
