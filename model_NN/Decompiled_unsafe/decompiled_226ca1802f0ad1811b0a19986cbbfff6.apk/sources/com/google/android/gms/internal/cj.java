package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.cc;
import java.util.HashSet;
import java.util.Set;

public class cj implements Parcelable.Creator<cc.d> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(cc.d dVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        Set<Integer> bH = dVar.bH();
        if (bH.contains(1)) {
            b.c(parcel, 1, dVar.i());
        }
        if (bH.contains(2)) {
            b.a(parcel, 2, dVar.getFamilyName(), true);
        }
        if (bH.contains(3)) {
            b.a(parcel, 3, dVar.getFormatted(), true);
        }
        if (bH.contains(4)) {
            b.a(parcel, 4, dVar.getGivenName(), true);
        }
        if (bH.contains(5)) {
            b.a(parcel, 5, dVar.getHonorificPrefix(), true);
        }
        if (bH.contains(6)) {
            b.a(parcel, 6, dVar.getHonorificSuffix(), true);
        }
        if (bH.contains(7)) {
            b.a(parcel, 7, dVar.getMiddleName(), true);
        }
        b.C(parcel, d);
    }

    /* renamed from: E */
    public cc.d createFromParcel(Parcel parcel) {
        String str = null;
        int c2 = a.c(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i = a.f(parcel, b2);
                    hashSet.add(1);
                    break;
                case 2:
                    str6 = a.l(parcel, b2);
                    hashSet.add(2);
                    break;
                case 3:
                    str5 = a.l(parcel, b2);
                    hashSet.add(3);
                    break;
                case 4:
                    str4 = a.l(parcel, b2);
                    hashSet.add(4);
                    break;
                case 5:
                    str3 = a.l(parcel, b2);
                    hashSet.add(5);
                    break;
                case 6:
                    str2 = a.l(parcel, b2);
                    hashSet.add(6);
                    break;
                case 7:
                    str = a.l(parcel, b2);
                    hashSet.add(7);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new cc.d(hashSet, i, str6, str5, str4, str3, str2, str);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: ae */
    public cc.d[] newArray(int i) {
        return new cc.d[i];
    }
}
