package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.cc;
import java.util.HashSet;
import java.util.Set;

public class ck implements Parcelable.Creator<cc.f> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(cc.f fVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        Set<Integer> bH = fVar.bH();
        if (bH.contains(1)) {
            b.c(parcel, 1, fVar.i());
        }
        if (bH.contains(2)) {
            b.a(parcel, 2, fVar.getDepartment(), true);
        }
        if (bH.contains(3)) {
            b.a(parcel, 3, fVar.getDescription(), true);
        }
        if (bH.contains(4)) {
            b.a(parcel, 4, fVar.getEndDate(), true);
        }
        if (bH.contains(5)) {
            b.a(parcel, 5, fVar.getLocation(), true);
        }
        if (bH.contains(6)) {
            b.a(parcel, 6, fVar.getName(), true);
        }
        if (bH.contains(7)) {
            b.a(parcel, 7, fVar.isPrimary());
        }
        if (bH.contains(8)) {
            b.a(parcel, 8, fVar.getStartDate(), true);
        }
        if (bH.contains(9)) {
            b.a(parcel, 9, fVar.getTitle(), true);
        }
        if (bH.contains(10)) {
            b.c(parcel, 10, fVar.getType());
        }
        b.C(parcel, d);
    }

    /* renamed from: F */
    public cc.f createFromParcel(Parcel parcel) {
        int i = 0;
        String str = null;
        int c2 = a.c(parcel);
        HashSet hashSet = new HashSet();
        String str2 = null;
        boolean z = false;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        int i2 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i2 = a.f(parcel, b2);
                    hashSet.add(1);
                    break;
                case 2:
                    str7 = a.l(parcel, b2);
                    hashSet.add(2);
                    break;
                case 3:
                    str6 = a.l(parcel, b2);
                    hashSet.add(3);
                    break;
                case 4:
                    str5 = a.l(parcel, b2);
                    hashSet.add(4);
                    break;
                case 5:
                    str4 = a.l(parcel, b2);
                    hashSet.add(5);
                    break;
                case 6:
                    str3 = a.l(parcel, b2);
                    hashSet.add(6);
                    break;
                case 7:
                    z = a.c(parcel, b2);
                    hashSet.add(7);
                    break;
                case 8:
                    str2 = a.l(parcel, b2);
                    hashSet.add(8);
                    break;
                case 9:
                    str = a.l(parcel, b2);
                    hashSet.add(9);
                    break;
                case 10:
                    i = a.f(parcel, b2);
                    hashSet.add(10);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new cc.f(hashSet, i2, str7, str6, str5, str4, str3, z, str2, str, i);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: af */
    public cc.f[] newArray(int i) {
        return new cc.f[i];
    }
}
