package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.cc;
import java.util.HashSet;
import java.util.Set;

public class cg implements Parcelable.Creator<cc.b.a> {
    static void a(cc.b.a aVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        Set<Integer> bH = aVar.bH();
        if (bH.contains(1)) {
            b.c(parcel, 1, aVar.i());
        }
        if (bH.contains(2)) {
            b.c(parcel, 2, aVar.getLeftImageOffset());
        }
        if (bH.contains(3)) {
            b.c(parcel, 3, aVar.getTopImageOffset());
        }
        b.C(parcel, d);
    }

    /* renamed from: B */
    public cc.b.a createFromParcel(Parcel parcel) {
        int i = 0;
        int c2 = a.c(parcel);
        HashSet hashSet = new HashSet();
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i3 = a.f(parcel, b2);
                    hashSet.add(1);
                    break;
                case 2:
                    i2 = a.f(parcel, b2);
                    hashSet.add(2);
                    break;
                case 3:
                    i = a.f(parcel, b2);
                    hashSet.add(3);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new cc.b.a(hashSet, i3, i2, i);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: ab */
    public cc.b.a[] newArray(int i) {
        return new cc.b.a[i];
    }
}
