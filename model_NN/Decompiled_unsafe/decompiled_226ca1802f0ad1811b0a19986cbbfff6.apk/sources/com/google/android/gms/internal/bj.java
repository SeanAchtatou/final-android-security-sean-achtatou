package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;

public class bj implements Parcelable.Creator<bi> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(bi biVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.a(parcel, 1, biVar.getRequestId(), false);
        b.c(parcel, 1000, biVar.i());
        b.a(parcel, 2, biVar.getExpirationTime());
        b.a(parcel, 3, biVar.aT());
        b.a(parcel, 4, biVar.getLatitude());
        b.a(parcel, 5, biVar.getLongitude());
        b.a(parcel, 6, biVar.aU());
        b.c(parcel, 7, biVar.aV());
        b.C(parcel, d);
    }

    /* renamed from: R */
    public bi[] newArray(int i) {
        return new bi[i];
    }

    /* renamed from: t */
    public bi createFromParcel(Parcel parcel) {
        double d = 0.0d;
        short s = 0;
        int c2 = a.c(parcel);
        String str = null;
        float f = 0.0f;
        long j = 0;
        double d2 = 0.0d;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    str = a.l(parcel, b2);
                    break;
                case 2:
                    j = a.g(parcel, b2);
                    break;
                case 3:
                    s = a.e(parcel, b2);
                    break;
                case 4:
                    d2 = a.j(parcel, b2);
                    break;
                case 5:
                    d = a.j(parcel, b2);
                    break;
                case 6:
                    f = a.i(parcel, b2);
                    break;
                case 7:
                    i = a.f(parcel, b2);
                    break;
                case 1000:
                    i2 = a.f(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new bi(i2, str, i, s, d2, d, f, j);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }
}
