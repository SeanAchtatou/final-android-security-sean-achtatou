package com.google.android.gms.dynamic;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.dynamic.LifecycleDelegate;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class a<T extends LifecycleDelegate> {
    /* access modifiers changed from: private */
    public T cP;
    /* access modifiers changed from: private */
    public Bundle cQ;
    /* access modifiers changed from: private */
    public LinkedList<C0002a> cR;
    private final d<T> cS = new d<T>() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.dynamic.a.a(com.google.android.gms.dynamic.a, com.google.android.gms.dynamic.LifecycleDelegate):com.google.android.gms.dynamic.LifecycleDelegate
         arg types: [com.google.android.gms.dynamic.a, T]
         candidates:
          com.google.android.gms.dynamic.a.a(com.google.android.gms.dynamic.a, android.os.Bundle):android.os.Bundle
          com.google.android.gms.dynamic.a.a(android.os.Bundle, com.google.android.gms.dynamic.a$a):void
          com.google.android.gms.dynamic.a.a(com.google.android.gms.dynamic.a, com.google.android.gms.dynamic.LifecycleDelegate):com.google.android.gms.dynamic.LifecycleDelegate */
        public void a(T t) {
            LifecycleDelegate unused = a.this.cP = (LifecycleDelegate) t;
            Iterator it = a.this.cR.iterator();
            while (it.hasNext()) {
                ((C0002a) it.next()).b(a.this.cP);
            }
            a.this.cR.clear();
            Bundle unused2 = a.this.cQ = (Bundle) null;
        }
    };

    /* renamed from: com.google.android.gms.dynamic.a$a  reason: collision with other inner class name */
    interface C0002a {
        void b(LifecycleDelegate lifecycleDelegate);

        int getState();
    }

    private void a(Bundle bundle, C0002a aVar) {
        if (this.cP != null) {
            aVar.b(this.cP);
            return;
        }
        if (this.cR == null) {
            this.cR = new LinkedList<>();
        }
        this.cR.add(aVar);
        if (bundle != null) {
            if (this.cQ == null) {
                this.cQ = (Bundle) bundle.clone();
            } else {
                this.cQ.putAll(bundle);
            }
        }
        a(this.cS);
    }

    private void y(int i) {
        while (!this.cR.isEmpty() && this.cR.getLast().getState() >= i) {
            this.cR.removeLast();
        }
    }

    public void a(FrameLayout frameLayout) {
        final Context context = frameLayout.getContext();
        final int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        String b2 = GooglePlayServicesUtil.b(context, isGooglePlayServicesAvailable, -1);
        String a2 = GooglePlayServicesUtil.a(context, isGooglePlayServicesAvailable);
        LinearLayout linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        frameLayout.addView(linearLayout);
        TextView textView = new TextView(frameLayout.getContext());
        textView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        textView.setText(b2);
        linearLayout.addView(textView);
        if (a2 != null) {
            Button button = new Button(context);
            button.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
            button.setText(a2);
            linearLayout.addView(button);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    context.startActivity(GooglePlayServicesUtil.a(context, isGooglePlayServicesAvailable, -1));
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(d<T> dVar);

    public T at() {
        return this.cP;
    }

    public void onCreate(final Bundle bundle) {
        a(bundle, new C0002a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.cP.onCreate(bundle);
            }

            public int getState() {
                return 1;
            }
        });
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        final FrameLayout frameLayout = new FrameLayout(layoutInflater.getContext());
        final LayoutInflater layoutInflater2 = layoutInflater;
        final ViewGroup viewGroup2 = viewGroup;
        final Bundle bundle2 = bundle;
        a(bundle, new C0002a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                frameLayout.removeAllViews();
                frameLayout.addView(a.this.cP.onCreateView(layoutInflater2, viewGroup2, bundle2));
            }

            public int getState() {
                return 2;
            }
        });
        if (this.cP == null) {
            a(frameLayout);
        }
        return frameLayout;
    }

    public void onDestroy() {
        if (this.cP != null) {
            this.cP.onDestroy();
        } else {
            y(1);
        }
    }

    public void onDestroyView() {
        if (this.cP != null) {
            this.cP.onDestroyView();
        } else {
            y(2);
        }
    }

    public void onInflate(final Activity activity, final Bundle bundle, final Bundle bundle2) {
        a(bundle2, new C0002a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.cP.onInflate(activity, bundle, bundle2);
            }

            public int getState() {
                return 0;
            }
        });
    }

    public void onLowMemory() {
        if (this.cP != null) {
            this.cP.onLowMemory();
        }
    }

    public void onPause() {
        if (this.cP != null) {
            this.cP.onPause();
        } else {
            y(3);
        }
    }

    public void onResume() {
        a((Bundle) null, new C0002a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.cP.onResume();
            }

            public int getState() {
                return 3;
            }
        });
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onSaveInstanceState(android.os.Bundle r2) {
        /*
            r1 = this;
            T r0 = r1.cP
            if (r0 == 0) goto L_0x000a
            T r0 = r1.cP
            r0.onSaveInstanceState(r2)
        L_0x0009:
            return
        L_0x000a:
            android.os.Bundle r0 = r1.cQ
            if (r0 == 0) goto L_0x0009
            android.os.Bundle r0 = r1.cQ
            r2.putAll(r0)
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamic.a.onSaveInstanceState(android.os.Bundle):void");
    }
}
