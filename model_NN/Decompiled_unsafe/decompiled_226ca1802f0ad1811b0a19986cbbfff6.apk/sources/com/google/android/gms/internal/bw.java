package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;

public class bw implements Parcelable.Creator<bv> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(bv bvVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.a(parcel, 1, bvVar.getDescription(), false);
        b.c(parcel, 1000, bvVar.i());
        b.b(parcel, 2, bvVar.bE(), false);
        b.b(parcel, 3, bvVar.bF(), false);
        b.a(parcel, 4, bvVar.bG());
        b.C(parcel, d);
    }

    /* renamed from: V */
    public bv[] newArray(int i) {
        return new bv[i];
    }

    /* renamed from: v */
    public bv createFromParcel(Parcel parcel) {
        boolean z = false;
        ArrayList arrayList = null;
        int c2 = a.c(parcel);
        ArrayList arrayList2 = null;
        String str = null;
        int i = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    str = a.l(parcel, b2);
                    break;
                case 2:
                    arrayList2 = a.c(parcel, b2, x.CREATOR);
                    break;
                case 3:
                    arrayList = a.c(parcel, b2, x.CREATOR);
                    break;
                case 4:
                    z = a.c(parcel, b2);
                    break;
                case 1000:
                    i = a.f(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new bv(i, str, arrayList2, arrayList, z);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }
}
