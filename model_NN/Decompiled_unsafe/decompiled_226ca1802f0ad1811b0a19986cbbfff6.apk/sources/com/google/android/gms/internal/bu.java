package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.view.View;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.dynamic.c;
import com.google.android.gms.internal.bq;
import com.google.android.gms.plus.PlusOneDummyView;

public final class bu {
    private static Context gN;
    private static bq iy;

    public class a extends Exception {
        public a(String str) {
            super(str);
        }
    }

    public static View a(Context context, int i, int i2, String str, int i3) {
        if (str != null) {
            return (View) c.a(m(context).a(c.f(context), i, i2, str, i3));
        }
        try {
            throw new NullPointerException();
        } catch (Exception e) {
            return new PlusOneDummyView(context, i);
        }
    }

    private static bq m(Context context) {
        s.d(context);
        if (iy == null) {
            if (gN == null) {
                gN = GooglePlayServicesUtil.getRemoteContext(context);
                if (gN == null) {
                    throw new a("Could not get remote context.");
                }
            }
            try {
                iy = bq.a.Z((IBinder) gN.getClassLoader().loadClass("com.google.android.gms.plus.plusone.PlusOneButtonCreatorImpl").newInstance());
            } catch (ClassNotFoundException e) {
                throw new a("Could not load creator class.");
            } catch (InstantiationException e2) {
                throw new a("Could not instantiate creator.");
            } catch (IllegalAccessException e3) {
                throw new a("Could not access creator.");
            }
        }
        return iy;
    }
}
