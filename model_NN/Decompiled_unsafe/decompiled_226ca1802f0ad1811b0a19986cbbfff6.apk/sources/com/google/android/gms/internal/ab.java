package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ae;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public final class ab implements SafeParcelable, ae.b<String, Integer> {
    public static final ac CREATOR = new ac();
    private final int ab;
    private final HashMap<String, Integer> co;
    private final HashMap<Integer, String> cp;
    private final ArrayList<a> cq;

    public final class a implements SafeParcelable {
        public static final ad CREATOR = new ad();
        final String cr;
        final int cs;
        final int versionCode;

        a(int i, String str, int i2) {
            this.versionCode = i;
            this.cr = str;
            this.cs = i2;
        }

        a(String str, int i) {
            this.versionCode = 1;
            this.cr = str;
            this.cs = i;
        }

        public int describeContents() {
            ad adVar = CREATOR;
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            ad adVar = CREATOR;
            ad.a(this, parcel, i);
        }
    }

    public ab() {
        this.ab = 1;
        this.co = new HashMap<>();
        this.cp = new HashMap<>();
        this.cq = null;
    }

    ab(int i, ArrayList<a> arrayList) {
        this.ab = i;
        this.co = new HashMap<>();
        this.cp = new HashMap<>();
        this.cq = null;
        a(arrayList);
    }

    private void a(ArrayList<a> arrayList) {
        Iterator<a> it = arrayList.iterator();
        while (it.hasNext()) {
            a next = it.next();
            b(next.cr, next.cs);
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<a> Q() {
        ArrayList<a> arrayList = new ArrayList<>();
        for (String next : this.co.keySet()) {
            arrayList.add(new a(next, this.co.get(next).intValue()));
        }
        return arrayList;
    }

    public int R() {
        return 7;
    }

    public int S() {
        return 0;
    }

    /* renamed from: a */
    public String e(Integer num) {
        String str = this.cp.get(num);
        return (str != null || !this.co.containsKey("gms_unknown")) ? str : "gms_unknown";
    }

    public ab b(String str, int i) {
        this.co.put(str, Integer.valueOf(i));
        this.cp.put(Integer.valueOf(i), str);
        return this;
    }

    public int describeContents() {
        ac acVar = CREATOR;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    public void writeToParcel(Parcel parcel, int i) {
        ac acVar = CREATOR;
        ac.a(this, parcel, i);
    }
}
