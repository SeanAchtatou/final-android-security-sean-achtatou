package com.google.android.gms.plus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.internal.bu;
import com.google.android.gms.internal.v;

public final class PlusOneButton extends FrameLayout {
    private int O;
    /* access modifiers changed from: private */
    public View ic;
    private int id;
    private String ie;
    /* access modifiers changed from: private */

    /* renamed from: if  reason: not valid java name */
    public int f1if = -1;
    private OnPlusOneClickListener ig;

    public class DefaultOnPlusOneClickListener implements View.OnClickListener, OnPlusOneClickListener {
        private final OnPlusOneClickListener ih;

        public DefaultOnPlusOneClickListener(OnPlusOneClickListener onPlusOneClickListener) {
            this.ih = onPlusOneClickListener;
        }

        public void onClick(View view) {
            Intent intent = (Intent) PlusOneButton.this.ic.getTag();
            if (this.ih != null) {
                this.ih.onPlusOneClick(intent);
            } else {
                onPlusOneClick(intent);
            }
        }

        public void onPlusOneClick(Intent intent) {
            Context context = PlusOneButton.this.getContext();
            if ((context instanceof Activity) && intent != null) {
                ((Activity) context).startActivityForResult(intent, PlusOneButton.this.f1if);
            }
        }
    }

    public interface OnPlusOneClickListener {
        void onPlusOneClick(Intent intent);
    }

    public PlusOneButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.O = getSize(context, attributeSet);
        this.id = getAnnotation(context, attributeSet);
        d(getContext());
        if (isInEditMode()) {
        }
    }

    private void d(Context context) {
        if (this.ic != null) {
            removeView(this.ic);
        }
        this.ic = bu.a(context, this.O, this.id, this.ie, this.f1if);
        setOnPlusOneClickListener(this.ig);
        addView(this.ic);
    }

    protected static int getAnnotation(Context context, AttributeSet attributeSet) {
        String a2 = v.a("http://schemas.android.com/apk/lib/com.google.android.gms.plus", "annotation", context, attributeSet, true, false, "PlusOneButton");
        if ("INLINE".equalsIgnoreCase(a2)) {
            return 2;
        }
        return !"NONE".equalsIgnoreCase(a2) ? 1 : 0;
    }

    protected static int getSize(Context context, AttributeSet attributeSet) {
        String a2 = v.a("http://schemas.android.com/apk/lib/com.google.android.gms.plus", "size", context, attributeSet, true, false, "PlusOneButton");
        if ("SMALL".equalsIgnoreCase(a2)) {
            return 0;
        }
        if ("MEDIUM".equalsIgnoreCase(a2)) {
            return 1;
        }
        return "TALL".equalsIgnoreCase(a2) ? 2 : 3;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.ic.layout(0, 0, i3 - i, i4 - i2);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        View view = this.ic;
        measureChild(view, i, i2);
        setMeasuredDimension(view.getMeasuredWidth(), view.getMeasuredHeight());
    }

    public void setOnPlusOneClickListener(OnPlusOneClickListener onPlusOneClickListener) {
        this.ig = onPlusOneClickListener;
        this.ic.setOnClickListener(new DefaultOnPlusOneClickListener(onPlusOneClickListener));
    }
}
