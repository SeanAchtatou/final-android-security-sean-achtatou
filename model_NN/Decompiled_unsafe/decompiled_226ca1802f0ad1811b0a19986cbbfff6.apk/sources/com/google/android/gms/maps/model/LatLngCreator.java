package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;

public class LatLngCreator implements Parcelable.Creator<LatLng> {
    static void a(LatLng latLng, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, latLng.i());
        b.a(parcel, 2, latLng.latitude);
        b.a(parcel, 3, latLng.longitude);
        b.C(parcel, d);
    }

    public LatLng createFromParcel(Parcel parcel) {
        double d = 0.0d;
        int c2 = a.c(parcel);
        int i = 0;
        double d2 = 0.0d;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i = a.f(parcel, b2);
                    break;
                case 2:
                    d2 = a.j(parcel, b2);
                    break;
                case 3:
                    d = a.j(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new LatLng(i, d2, d);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    public LatLng[] newArray(int i) {
        return new LatLng[i];
    }
}
