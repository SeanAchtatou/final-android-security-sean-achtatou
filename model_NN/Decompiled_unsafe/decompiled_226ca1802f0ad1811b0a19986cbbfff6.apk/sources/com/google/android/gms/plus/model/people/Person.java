package com.google.android.gms.plus.model.people;

import com.google.android.gms.common.data.Freezable;

public interface Person extends Freezable<Person> {

    public interface AgeRange extends Freezable<AgeRange> {
    }

    public interface Cover extends Freezable<Cover> {

        public interface CoverInfo extends Freezable<CoverInfo> {
        }

        public interface CoverPhoto extends Freezable<CoverPhoto> {
        }
    }

    public interface Image extends Freezable<Image> {
    }

    public interface Name extends Freezable<Name> {
    }

    public interface Organizations extends Freezable<Organizations> {
    }

    public interface PlacesLived extends Freezable<PlacesLived> {
    }

    public interface Urls extends Freezable<Urls> {
    }
}
