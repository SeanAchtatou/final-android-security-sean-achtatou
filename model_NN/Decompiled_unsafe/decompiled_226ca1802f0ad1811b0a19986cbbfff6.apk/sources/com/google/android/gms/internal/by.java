package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.actionbarsherlock.R;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class by implements Parcelable.Creator<bx> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.bx, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
     arg types: [android.os.Parcel, int, java.util.List<java.lang.String>, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(bx bxVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        Set<Integer> bH = bxVar.bH();
        if (bH.contains(1)) {
            b.c(parcel, 1, bxVar.i());
        }
        if (bH.contains(2)) {
            b.a(parcel, 2, (Parcelable) bxVar.bI(), i, true);
        }
        if (bH.contains(3)) {
            b.a(parcel, 3, bxVar.getAdditionalName(), true);
        }
        if (bH.contains(4)) {
            b.a(parcel, 4, (Parcelable) bxVar.bJ(), i, true);
        }
        if (bH.contains(5)) {
            b.a(parcel, 5, bxVar.getAddressCountry(), true);
        }
        if (bH.contains(6)) {
            b.a(parcel, 6, bxVar.getAddressLocality(), true);
        }
        if (bH.contains(7)) {
            b.a(parcel, 7, bxVar.getAddressRegion(), true);
        }
        if (bH.contains(8)) {
            b.b(parcel, 8, bxVar.bK(), true);
        }
        if (bH.contains(9)) {
            b.c(parcel, 9, bxVar.getAttendeeCount());
        }
        if (bH.contains(10)) {
            b.b(parcel, 10, bxVar.bL(), true);
        }
        if (bH.contains(11)) {
            b.a(parcel, 11, (Parcelable) bxVar.bM(), i, true);
        }
        if (bH.contains(12)) {
            b.b(parcel, 12, bxVar.bN(), true);
        }
        if (bH.contains(13)) {
            b.a(parcel, 13, bxVar.getBestRating(), true);
        }
        if (bH.contains(14)) {
            b.a(parcel, 14, bxVar.getBirthDate(), true);
        }
        if (bH.contains(15)) {
            b.a(parcel, 15, (Parcelable) bxVar.bO(), i, true);
        }
        if (bH.contains(17)) {
            b.a(parcel, 17, bxVar.getContentSize(), true);
        }
        if (bH.contains(16)) {
            b.a(parcel, 16, bxVar.getCaption(), true);
        }
        if (bH.contains(19)) {
            b.b(parcel, 19, bxVar.bP(), true);
        }
        if (bH.contains(18)) {
            b.a(parcel, 18, bxVar.getContentUrl(), true);
        }
        if (bH.contains(21)) {
            b.a(parcel, 21, bxVar.getDateModified(), true);
        }
        if (bH.contains(20)) {
            b.a(parcel, 20, bxVar.getDateCreated(), true);
        }
        if (bH.contains(23)) {
            b.a(parcel, 23, bxVar.getDescription(), true);
        }
        if (bH.contains(22)) {
            b.a(parcel, 22, bxVar.getDatePublished(), true);
        }
        if (bH.contains(25)) {
            b.a(parcel, 25, bxVar.getEmbedUrl(), true);
        }
        if (bH.contains(24)) {
            b.a(parcel, 24, bxVar.getDuration(), true);
        }
        if (bH.contains(27)) {
            b.a(parcel, 27, bxVar.getFamilyName(), true);
        }
        if (bH.contains(26)) {
            b.a(parcel, 26, bxVar.getEndDate(), true);
        }
        if (bH.contains(29)) {
            b.a(parcel, 29, (Parcelable) bxVar.bQ(), i, true);
        }
        if (bH.contains(28)) {
            b.a(parcel, 28, bxVar.getGender(), true);
        }
        if (bH.contains(31)) {
            b.a(parcel, 31, bxVar.getHeight(), true);
        }
        if (bH.contains(30)) {
            b.a(parcel, 30, bxVar.getGivenName(), true);
        }
        if (bH.contains(34)) {
            b.a(parcel, 34, (Parcelable) bxVar.bR(), i, true);
        }
        if (bH.contains(32)) {
            b.a(parcel, 32, bxVar.getId(), true);
        }
        if (bH.contains(33)) {
            b.a(parcel, 33, bxVar.getImage(), true);
        }
        if (bH.contains(38)) {
            b.a(parcel, 38, bxVar.getLongitude());
        }
        if (bH.contains(39)) {
            b.a(parcel, 39, bxVar.getName(), true);
        }
        if (bH.contains(36)) {
            b.a(parcel, 36, bxVar.getLatitude());
        }
        if (bH.contains(37)) {
            b.a(parcel, 37, (Parcelable) bxVar.bS(), i, true);
        }
        if (bH.contains(42)) {
            b.a(parcel, 42, bxVar.getPlayerType(), true);
        }
        if (bH.contains(43)) {
            b.a(parcel, 43, bxVar.getPostOfficeBoxNumber(), true);
        }
        if (bH.contains(40)) {
            b.a(parcel, 40, (Parcelable) bxVar.bT(), i, true);
        }
        if (bH.contains(41)) {
            b.b(parcel, 41, bxVar.bU(), true);
        }
        if (bH.contains(46)) {
            b.a(parcel, 46, (Parcelable) bxVar.bV(), i, true);
        }
        if (bH.contains(47)) {
            b.a(parcel, 47, bxVar.getStartDate(), true);
        }
        if (bH.contains(44)) {
            b.a(parcel, 44, bxVar.getPostalCode(), true);
        }
        if (bH.contains(45)) {
            b.a(parcel, 45, bxVar.getRatingValue(), true);
        }
        if (bH.contains(51)) {
            b.a(parcel, 51, bxVar.getThumbnailUrl(), true);
        }
        if (bH.contains(50)) {
            b.a(parcel, 50, (Parcelable) bxVar.bW(), i, true);
        }
        if (bH.contains(49)) {
            b.a(parcel, 49, bxVar.getText(), true);
        }
        if (bH.contains(48)) {
            b.a(parcel, 48, bxVar.getStreetAddress(), true);
        }
        if (bH.contains(55)) {
            b.a(parcel, 55, bxVar.getWidth(), true);
        }
        if (bH.contains(54)) {
            b.a(parcel, 54, bxVar.getUrl(), true);
        }
        if (bH.contains(53)) {
            b.a(parcel, 53, bxVar.getType(), true);
        }
        if (bH.contains(52)) {
            b.a(parcel, 52, bxVar.getTickerSymbol(), true);
        }
        if (bH.contains(56)) {
            b.a(parcel, 56, bxVar.getWorstRating(), true);
        }
        b.C(parcel, d);
    }

    /* renamed from: W */
    public bx[] newArray(int i) {
        return new bx[i];
    }

    /* renamed from: w */
    public bx createFromParcel(Parcel parcel) {
        int c2 = a.c(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        bx bxVar = null;
        ArrayList<String> arrayList = null;
        bx bxVar2 = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        ArrayList arrayList2 = null;
        int i2 = 0;
        ArrayList arrayList3 = null;
        bx bxVar3 = null;
        ArrayList arrayList4 = null;
        String str4 = null;
        String str5 = null;
        bx bxVar4 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        ArrayList arrayList5 = null;
        String str9 = null;
        String str10 = null;
        String str11 = null;
        String str12 = null;
        String str13 = null;
        String str14 = null;
        String str15 = null;
        String str16 = null;
        String str17 = null;
        bx bxVar5 = null;
        String str18 = null;
        String str19 = null;
        String str20 = null;
        String str21 = null;
        bx bxVar6 = null;
        double d = 0.0d;
        bx bxVar7 = null;
        double d2 = 0.0d;
        String str22 = null;
        bx bxVar8 = null;
        ArrayList arrayList6 = null;
        String str23 = null;
        String str24 = null;
        String str25 = null;
        String str26 = null;
        bx bxVar9 = null;
        String str27 = null;
        String str28 = null;
        String str29 = null;
        bx bxVar10 = null;
        String str30 = null;
        String str31 = null;
        String str32 = null;
        String str33 = null;
        String str34 = null;
        String str35 = null;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i = a.f(parcel, b2);
                    hashSet.add(1);
                    break;
                case 2:
                    hashSet.add(2);
                    bxVar = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case 3:
                    arrayList = a.x(parcel, b2);
                    hashSet.add(3);
                    break;
                case 4:
                    hashSet.add(4);
                    bxVar2 = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case 5:
                    str = a.l(parcel, b2);
                    hashSet.add(5);
                    break;
                case 6:
                    str2 = a.l(parcel, b2);
                    hashSet.add(6);
                    break;
                case 7:
                    str3 = a.l(parcel, b2);
                    hashSet.add(7);
                    break;
                case 8:
                    arrayList2 = a.c(parcel, b2, bx.CREATOR);
                    hashSet.add(8);
                    break;
                case 9:
                    i2 = a.f(parcel, b2);
                    hashSet.add(9);
                    break;
                case 10:
                    arrayList3 = a.c(parcel, b2, bx.CREATOR);
                    hashSet.add(10);
                    break;
                case 11:
                    hashSet.add(11);
                    bxVar3 = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case 12:
                    arrayList4 = a.c(parcel, b2, bx.CREATOR);
                    hashSet.add(12);
                    break;
                case 13:
                    str4 = a.l(parcel, b2);
                    hashSet.add(13);
                    break;
                case 14:
                    str5 = a.l(parcel, b2);
                    hashSet.add(14);
                    break;
                case 15:
                    hashSet.add(15);
                    bxVar4 = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case 16:
                    str6 = a.l(parcel, b2);
                    hashSet.add(16);
                    break;
                case 17:
                    str7 = a.l(parcel, b2);
                    hashSet.add(17);
                    break;
                case 18:
                    str8 = a.l(parcel, b2);
                    hashSet.add(18);
                    break;
                case R.styleable.SherlockTheme_buttonStyleSmall:
                    arrayList5 = a.c(parcel, b2, bx.CREATOR);
                    hashSet.add(19);
                    break;
                case R.styleable.SherlockTheme_selectableItemBackground:
                    str9 = a.l(parcel, b2);
                    hashSet.add(20);
                    break;
                case R.styleable.SherlockTheme_windowContentOverlay:
                    str10 = a.l(parcel, b2);
                    hashSet.add(21);
                    break;
                case R.styleable.SherlockTheme_textAppearanceLargePopupMenu:
                    str11 = a.l(parcel, b2);
                    hashSet.add(22);
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmallPopupMenu:
                    str12 = a.l(parcel, b2);
                    hashSet.add(23);
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    str13 = a.l(parcel, b2);
                    hashSet.add(24);
                    break;
                case R.styleable.SherlockTheme_textColorPrimary:
                    str14 = a.l(parcel, b2);
                    hashSet.add(25);
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    str15 = a.l(parcel, b2);
                    hashSet.add(26);
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryInverse:
                    str16 = a.l(parcel, b2);
                    hashSet.add(27);
                    break;
                case R.styleable.SherlockTheme_spinnerItemStyle:
                    str17 = a.l(parcel, b2);
                    hashSet.add(28);
                    break;
                case R.styleable.SherlockTheme_spinnerDropDownItemStyle:
                    hashSet.add(29);
                    bxVar5 = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case R.styleable.SherlockTheme_searchAutoCompleteTextView:
                    str18 = a.l(parcel, b2);
                    hashSet.add(30);
                    break;
                case R.styleable.SherlockTheme_searchDropdownBackground:
                    str19 = a.l(parcel, b2);
                    hashSet.add(31);
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    str20 = a.l(parcel, b2);
                    hashSet.add(32);
                    break;
                case R.styleable.SherlockTheme_searchViewGoIcon:
                    str21 = a.l(parcel, b2);
                    hashSet.add(33);
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    hashSet.add(34);
                    bxVar6 = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case R.styleable.SherlockTheme_searchViewVoiceIcon:
                default:
                    a.b(parcel, b2);
                    break;
                case R.styleable.SherlockTheme_searchViewEditQuery:
                    d = a.j(parcel, b2);
                    hashSet.add(36);
                    break;
                case R.styleable.SherlockTheme_searchViewEditQueryBackground:
                    hashSet.add(37);
                    bxVar7 = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case R.styleable.SherlockTheme_searchViewTextField:
                    d2 = a.j(parcel, b2);
                    hashSet.add(38);
                    break;
                case R.styleable.SherlockTheme_searchViewTextFieldRight:
                    str22 = a.l(parcel, b2);
                    hashSet.add(39);
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    hashSet.add(40);
                    bxVar8 = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case R.styleable.SherlockTheme_searchResultListItemHeight:
                    arrayList6 = a.c(parcel, b2, bx.CREATOR);
                    hashSet.add(41);
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    str23 = a.l(parcel, b2);
                    hashSet.add(42);
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultSubtitle:
                    str24 = a.l(parcel, b2);
                    hashSet.add(43);
                    break;
                case R.styleable.SherlockTheme_listPreferredItemHeightSmall:
                    str25 = a.l(parcel, b2);
                    hashSet.add(44);
                    break;
                case R.styleable.SherlockTheme_listPreferredItemPaddingLeft:
                    str26 = a.l(parcel, b2);
                    hashSet.add(45);
                    break;
                case R.styleable.SherlockTheme_listPreferredItemPaddingRight:
                    hashSet.add(46);
                    bxVar9 = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case R.styleable.SherlockTheme_textAppearanceListItemSmall:
                    str27 = a.l(parcel, b2);
                    hashSet.add(47);
                    break;
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                    str28 = a.l(parcel, b2);
                    hashSet.add(48);
                    break;
                case R.styleable.SherlockTheme_windowMinWidthMinor:
                    str29 = a.l(parcel, b2);
                    hashSet.add(49);
                    break;
                case 50:
                    hashSet.add(50);
                    bxVar10 = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case R.styleable.SherlockTheme_actionDropDownStyle:
                    str30 = a.l(parcel, b2);
                    hashSet.add(51);
                    break;
                case R.styleable.SherlockTheme_actionButtonStyle:
                    str31 = a.l(parcel, b2);
                    hashSet.add(52);
                    break;
                case R.styleable.SherlockTheme_homeAsUpIndicator:
                    str32 = a.l(parcel, b2);
                    hashSet.add(53);
                    break;
                case R.styleable.SherlockTheme_dropDownListViewStyle:
                    str33 = a.l(parcel, b2);
                    hashSet.add(54);
                    break;
                case R.styleable.SherlockTheme_popupMenuStyle:
                    str34 = a.l(parcel, b2);
                    hashSet.add(55);
                    break;
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                    str35 = a.l(parcel, b2);
                    hashSet.add(56);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new bx(hashSet, i, bxVar, arrayList, bxVar2, str, str2, str3, arrayList2, i2, arrayList3, bxVar3, arrayList4, str4, str5, bxVar4, str6, str7, str8, arrayList5, str9, str10, str11, str12, str13, str14, str15, str16, str17, bxVar5, str18, str19, str20, str21, bxVar6, d, bxVar7, d2, str22, bxVar8, arrayList6, str23, str24, str25, str26, bxVar9, str27, str28, str29, bxVar10, str30, str31, str32, str33, str34, str35);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }
}
