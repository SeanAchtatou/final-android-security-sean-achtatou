package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.internal.av;
import com.google.android.gms.internal.r;
import com.google.android.gms.internal.s;
import java.util.ArrayList;

public final class InvitationEntity extends av implements Invitation {
    public static final Parcelable.Creator<InvitationEntity> CREATOR = new a();
    private final int ab;
    private final GameEntity eE;
    private final String eF;
    private final long eG;
    private final int eH;
    private final ParticipantEntity eI;
    private final ArrayList<ParticipantEntity> eJ;
    private final int eK;

    final class a extends a {
        a() {
        }

        /* renamed from: p */
        public InvitationEntity createFromParcel(Parcel parcel) {
            if (InvitationEntity.c(InvitationEntity.v()) || InvitationEntity.h(InvitationEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            GameEntity createFromParcel = GameEntity.CREATOR.createFromParcel(parcel);
            String readString = parcel.readString();
            long readLong = parcel.readLong();
            int readInt = parcel.readInt();
            ParticipantEntity createFromParcel2 = ParticipantEntity.CREATOR.createFromParcel(parcel);
            int readInt2 = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt2);
            for (int i = 0; i < readInt2; i++) {
                arrayList.add(ParticipantEntity.CREATOR.createFromParcel(parcel));
            }
            return new InvitationEntity(1, createFromParcel, readString, readLong, readInt, createFromParcel2, arrayList, -1);
        }
    }

    InvitationEntity(int i, GameEntity gameEntity, String str, long j, int i2, ParticipantEntity participantEntity, ArrayList<ParticipantEntity> arrayList, int i3) {
        this.ab = i;
        this.eE = gameEntity;
        this.eF = str;
        this.eG = j;
        this.eH = i2;
        this.eI = participantEntity;
        this.eJ = arrayList;
        this.eK = i3;
    }

    InvitationEntity(Invitation invitation) {
        this.ab = 1;
        this.eE = new GameEntity(invitation.getGame());
        this.eF = invitation.getInvitationId();
        this.eG = invitation.getCreationTimestamp();
        this.eH = invitation.aL();
        this.eK = invitation.getVariant();
        String participantId = invitation.getInviter().getParticipantId();
        Participant participant = null;
        ArrayList<Participant> participants = invitation.getParticipants();
        int size = participants.size();
        this.eJ = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            Participant participant2 = participants.get(i);
            if (participant2.getParticipantId().equals(participantId)) {
                participant = participant2;
            }
            this.eJ.add((ParticipantEntity) participant2.freeze());
        }
        s.b(participant, "Must have a valid inviter!");
        this.eI = (ParticipantEntity) participant.freeze();
    }

    static int a(Invitation invitation) {
        return r.hashCode(invitation.getGame(), invitation.getInvitationId(), Long.valueOf(invitation.getCreationTimestamp()), Integer.valueOf(invitation.aL()), invitation.getInviter(), invitation.getParticipants(), Integer.valueOf(invitation.getVariant()));
    }

    static boolean a(Invitation invitation, Object obj) {
        if (!(obj instanceof Invitation)) {
            return false;
        }
        if (invitation == obj) {
            return true;
        }
        Invitation invitation2 = (Invitation) obj;
        return r.a(invitation2.getGame(), invitation.getGame()) && r.a(invitation2.getInvitationId(), invitation.getInvitationId()) && r.a(Long.valueOf(invitation2.getCreationTimestamp()), Long.valueOf(invitation.getCreationTimestamp())) && r.a(Integer.valueOf(invitation2.aL()), Integer.valueOf(invitation.aL())) && r.a(invitation2.getInviter(), invitation.getInviter()) && r.a(invitation2.getParticipants(), invitation.getParticipants()) && r.a(Integer.valueOf(invitation2.getVariant()), Integer.valueOf(invitation.getVariant()));
    }

    static String b(Invitation invitation) {
        return r.c(invitation).a("Game", invitation.getGame()).a("InvitationId", invitation.getInvitationId()).a("CreationTimestamp", Long.valueOf(invitation.getCreationTimestamp())).a("InvitationType", Integer.valueOf(invitation.aL())).a("Inviter", invitation.getInviter()).a("Participants", invitation.getParticipants()).a("Variant", Integer.valueOf(invitation.getVariant())).toString();
    }

    public int aL() {
        return this.eH;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public Invitation freeze() {
        return this;
    }

    public long getCreationTimestamp() {
        return this.eG;
    }

    public Game getGame() {
        return this.eE;
    }

    public String getInvitationId() {
        return this.eF;
    }

    public Participant getInviter() {
        return this.eI;
    }

    public ArrayList<Participant> getParticipants() {
        return new ArrayList<>(this.eJ);
    }

    public int getVariant() {
        return this.eK;
    }

    public int hashCode() {
        return a(this);
    }

    public int i() {
        return this.ab;
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (!w()) {
            a.a(this, parcel, i);
            return;
        }
        this.eE.writeToParcel(parcel, i);
        parcel.writeString(this.eF);
        parcel.writeLong(this.eG);
        parcel.writeInt(this.eH);
        this.eI.writeToParcel(parcel, i);
        int size = this.eJ.size();
        parcel.writeInt(size);
        for (int i2 = 0; i2 < size; i2++) {
            this.eJ.get(i2).writeToParcel(parcel, i);
        }
    }
}
