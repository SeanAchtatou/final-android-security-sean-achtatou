package com.google.android.gms.common.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.util.Log;
import com.google.android.gms.common.images.a;
import com.google.android.gms.internal.h;
import com.google.android.gms.internal.w;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

public final class ImageManager {
    /* access modifiers changed from: private */
    public static final Object aq = new Object();
    /* access modifiers changed from: private */
    public static HashSet<Uri> ar = new HashSet<>();
    /* access modifiers changed from: private */
    public final ExecutorService au;
    /* access modifiers changed from: private */
    public final b av;
    /* access modifiers changed from: private */
    public final Map<a, ImageReceiver> aw;
    /* access modifiers changed from: private */
    public final Map<Uri, ImageReceiver> ax;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final Handler mHandler;

    final class ImageReceiver extends ResultReceiver {
        final /* synthetic */ ImageManager aA;
        /* access modifiers changed from: private */
        public final ArrayList<a> ay;
        boolean az;
        private final Uri mUri;

        public void onReceiveResult(int i, Bundle bundle) {
            this.aA.au.execute(new c(this.mUri, (ParcelFileDescriptor) bundle.getParcelable("com.google.android.gms.extra.fileDescriptor")));
        }
    }

    public interface OnImageLoadedListener {
        void onImageLoaded(Uri uri, Drawable drawable);
    }

    final class b extends w<a.C0000a, Bitmap> {
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int sizeOf(a.C0000a aVar, Bitmap bitmap) {
            return bitmap.getHeight() * bitmap.getRowBytes();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void entryRemoved(boolean z, a.C0000a aVar, Bitmap bitmap, Bitmap bitmap2) {
            super.entryRemoved(z, aVar, bitmap, bitmap2);
        }
    }

    final class c implements Runnable {
        private final ParcelFileDescriptor aB;
        private final Uri mUri;

        public c(Uri uri, ParcelFileDescriptor parcelFileDescriptor) {
            this.mUri = uri;
            this.aB = parcelFileDescriptor;
        }

        public void run() {
            h.g("LoadBitmapFromDiskRunnable can't be executed in the main thread");
            boolean z = false;
            Bitmap bitmap = null;
            if (this.aB != null) {
                try {
                    bitmap = BitmapFactory.decodeFileDescriptor(this.aB.getFileDescriptor());
                } catch (OutOfMemoryError e) {
                    Log.e("ImageManager", "OOM while loading bitmap for uri: " + this.mUri, e);
                    z = true;
                }
                try {
                    this.aB.close();
                } catch (IOException e2) {
                    Log.e("ImageManager", "closed failed", e2);
                }
            }
            CountDownLatch countDownLatch = new CountDownLatch(1);
            ImageManager.this.mHandler.post(new f(this.mUri, bitmap, z, countDownLatch));
            try {
                countDownLatch.await();
            } catch (InterruptedException e3) {
                Log.w("ImageManager", "Latch interrupted while posting " + this.mUri);
            }
        }
    }

    final class f implements Runnable {
        private final Bitmap aD;
        private final CountDownLatch aE;
        private boolean aF;
        private final Uri mUri;

        public f(Uri uri, Bitmap bitmap, boolean z, CountDownLatch countDownLatch) {
            this.mUri = uri;
            this.aD = bitmap;
            this.aF = z;
            this.aE = countDownLatch;
        }

        private void a(ImageReceiver imageReceiver, boolean z) {
            imageReceiver.az = true;
            ArrayList a2 = imageReceiver.ay;
            int size = a2.size();
            for (int i = 0; i < size; i++) {
                a aVar = (a) a2.get(i);
                if (z) {
                    aVar.a(ImageManager.this.mContext, this.aD, false);
                } else {
                    aVar.b(ImageManager.this.mContext, false);
                }
                if (aVar.aJ != 1) {
                    ImageManager.this.aw.remove(aVar);
                }
            }
            imageReceiver.az = false;
        }

        public void run() {
            h.f("OnBitmapLoadedRunnable must be executed in the main thread");
            boolean z = this.aD != null;
            if (ImageManager.this.av != null) {
                if (this.aF) {
                    ImageManager.this.av.evictAll();
                    System.gc();
                    this.aF = false;
                    ImageManager.this.mHandler.post(this);
                    return;
                } else if (z) {
                    ImageManager.this.av.put(new a.C0000a(this.mUri), this.aD);
                }
            }
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.ax.remove(this.mUri);
            if (imageReceiver != null) {
                a(imageReceiver, z);
            }
            this.aE.countDown();
            synchronized (ImageManager.aq) {
                ImageManager.ar.remove(this.mUri);
            }
        }
    }
}
