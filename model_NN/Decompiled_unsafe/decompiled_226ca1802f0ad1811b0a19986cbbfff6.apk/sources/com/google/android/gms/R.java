package com.google.android.gms;

public final class R {

    public final class color {
        public static final int common_signin_btn_text_dark = 2131296330;
        public static final int common_signin_btn_text_light = 2131296331;
    }

    public final class drawable {
        public static final int common_signin_btn_icon_dark = 2130837733;
        public static final int common_signin_btn_icon_light = 2130837740;
        public static final int common_signin_btn_text_dark = 2130837745;
        public static final int common_signin_btn_text_light = 2130837752;
    }

    public final class string {
        public static final int common_google_play_services_enable_button = 2131624017;
        public static final int common_google_play_services_enable_text = 2131624018;
        public static final int common_google_play_services_enable_title = 2131624019;
        public static final int common_google_play_services_install_button = 2131624020;
        public static final int common_google_play_services_install_text_phone = 2131624021;
        public static final int common_google_play_services_install_text_tablet = 2131624022;
        public static final int common_google_play_services_install_title = 2131624023;
        public static final int common_google_play_services_invalid_account_text = 2131624024;
        public static final int common_google_play_services_invalid_account_title = 2131624025;
        public static final int common_google_play_services_network_error_text = 2131624026;
        public static final int common_google_play_services_network_error_title = 2131624027;
        public static final int common_google_play_services_unknown_issue = 2131624028;
        public static final int common_google_play_services_unsupported_text = 2131624029;
        public static final int common_google_play_services_unsupported_title = 2131624030;
        public static final int common_google_play_services_update_button = 2131624031;
        public static final int common_google_play_services_update_text = 2131624032;
        public static final int common_google_play_services_update_title = 2131624033;
        public static final int common_signin_button_text = 2131624034;
        public static final int common_signin_button_text_long = 2131624035;
    }

    public final class styleable {
        public static final int[] MapAttrs = {com.avast.android.antitheft_setup.R.attr.mapType, com.avast.android.antitheft_setup.R.attr.cameraBearing, com.avast.android.antitheft_setup.R.attr.cameraTargetLat, com.avast.android.antitheft_setup.R.attr.cameraTargetLng, com.avast.android.antitheft_setup.R.attr.cameraTilt, com.avast.android.antitheft_setup.R.attr.cameraZoom, com.avast.android.antitheft_setup.R.attr.uiCompass, com.avast.android.antitheft_setup.R.attr.uiRotateGestures, com.avast.android.antitheft_setup.R.attr.uiScrollGestures, com.avast.android.antitheft_setup.R.attr.uiTiltGestures, com.avast.android.antitheft_setup.R.attr.uiZoomControls, com.avast.android.antitheft_setup.R.attr.uiZoomGestures, com.avast.android.antitheft_setup.R.attr.useViewLifecycle, com.avast.android.antitheft_setup.R.attr.zOrderOnTop};
    }
}
