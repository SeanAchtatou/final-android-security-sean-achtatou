package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;

public class ActivityRecognitionResultCreator implements Parcelable.Creator<ActivityRecognitionResult> {
    static void a(ActivityRecognitionResult activityRecognitionResult, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.b(parcel, 1, activityRecognitionResult.fp, false);
        b.c(parcel, 1000, activityRecognitionResult.i());
        b.a(parcel, 2, activityRecognitionResult.fq);
        b.a(parcel, 3, activityRecognitionResult.fr);
        b.C(parcel, d);
    }

    public ActivityRecognitionResult createFromParcel(Parcel parcel) {
        long j = 0;
        int c2 = a.c(parcel);
        int i = 0;
        ArrayList arrayList = null;
        long j2 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    arrayList = a.c(parcel, b2, DetectedActivity.CREATOR);
                    break;
                case 2:
                    j2 = a.g(parcel, b2);
                    break;
                case 3:
                    j = a.g(parcel, b2);
                    break;
                case 1000:
                    i = a.f(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new ActivityRecognitionResult(i, arrayList, j2, j);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    public ActivityRecognitionResult[] newArray(int i) {
        return new ActivityRecognitionResult[i];
    }
}
