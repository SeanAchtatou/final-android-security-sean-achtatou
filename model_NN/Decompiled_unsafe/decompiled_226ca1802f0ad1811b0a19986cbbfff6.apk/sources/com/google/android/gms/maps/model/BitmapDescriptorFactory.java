package com.google.android.gms.maps.model;

import com.google.android.gms.internal.s;
import com.google.android.gms.maps.model.internal.a;

public final class BitmapDescriptorFactory {
    private static a gQ;

    public static void a(a aVar) {
        if (gQ == null) {
            gQ = (a) s.d(aVar);
        }
    }
}
