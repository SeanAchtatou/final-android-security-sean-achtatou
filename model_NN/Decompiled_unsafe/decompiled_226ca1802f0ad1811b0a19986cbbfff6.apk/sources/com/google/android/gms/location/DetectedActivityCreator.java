package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;

public class DetectedActivityCreator implements Parcelable.Creator<DetectedActivity> {
    static void a(DetectedActivity detectedActivity, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, detectedActivity.fs);
        b.c(parcel, 1000, detectedActivity.i());
        b.c(parcel, 2, detectedActivity.ft);
        b.C(parcel, d);
    }

    public DetectedActivity createFromParcel(Parcel parcel) {
        int i = 0;
        int c2 = a.c(parcel);
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i2 = a.f(parcel, b2);
                    break;
                case 2:
                    i = a.f(parcel, b2);
                    break;
                case 1000:
                    i3 = a.f(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new DetectedActivity(i3, i2, i);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    public DetectedActivity[] newArray(int i) {
        return new DetectedActivity[i];
    }
}
