package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;

public class cr implements Parcelable.Creator<cq> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    static void a(cq cqVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1000, cqVar.i());
        b.b(parcel, 2, cqVar.cK(), false);
        b.b(parcel, 3, cqVar.cL(), false);
        b.a(parcel, 4, cqVar.cM(), false);
        b.a(parcel, 5, cqVar.cN());
        b.c(parcel, 6, cqVar.cJ());
        b.C(parcel, d);
    }

    /* renamed from: J */
    public cq createFromParcel(Parcel parcel) {
        Bundle bundle = null;
        int i = 0;
        int c2 = a.c(parcel);
        boolean z = false;
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        int i2 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 2:
                    arrayList2 = a.c(parcel, b2, x.CREATOR);
                    break;
                case 3:
                    arrayList = a.c(parcel, b2, x.CREATOR);
                    break;
                case 4:
                    bundle = a.n(parcel, b2);
                    break;
                case 5:
                    z = a.c(parcel, b2);
                    break;
                case 6:
                    i = a.f(parcel, b2);
                    break;
                case 1000:
                    i2 = a.f(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new cq(i2, arrayList2, arrayList, bundle, z, i);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: aj */
    public cq[] newArray(int i) {
        return new cq[i];
    }
}
