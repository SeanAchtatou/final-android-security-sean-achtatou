package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.av;
import com.google.android.gms.internal.r;

public final class GameEntity extends av implements Game {
    public static final Parcelable.Creator<GameEntity> CREATOR = new a();
    private final int ab;
    private final String cl;
    private final String df;
    private final String dg;
    private final String dh;
    private final String di;
    private final String dj;
    private final Uri dk;
    private final Uri dl;
    private final Uri dm;
    private final boolean dn;

    /* renamed from: do  reason: not valid java name */
    private final boolean f0do;
    private final String dp;
    private final int dq;
    private final int dr;
    private final int ds;

    final class a extends a {
        a() {
        }

        /* renamed from: n */
        public GameEntity createFromParcel(Parcel parcel) {
            if (GameEntity.c(GameEntity.v()) || GameEntity.h(GameEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            String readString5 = parcel.readString();
            String readString6 = parcel.readString();
            String readString7 = parcel.readString();
            Uri parse = readString7 == null ? null : Uri.parse(readString7);
            String readString8 = parcel.readString();
            Uri parse2 = readString8 == null ? null : Uri.parse(readString8);
            String readString9 = parcel.readString();
            return new GameEntity(1, readString, readString2, readString3, readString4, readString5, readString6, parse, parse2, readString9 == null ? null : Uri.parse(readString9), parcel.readInt() > 0, parcel.readInt() > 0, parcel.readString(), parcel.readInt(), parcel.readInt(), parcel.readInt());
        }
    }

    GameEntity(int i, String str, String str2, String str3, String str4, String str5, String str6, Uri uri, Uri uri2, Uri uri3, boolean z, boolean z2, String str7, int i2, int i3, int i4) {
        this.ab = i;
        this.df = str;
        this.cl = str2;
        this.dg = str3;
        this.dh = str4;
        this.di = str5;
        this.dj = str6;
        this.dk = uri;
        this.dl = uri2;
        this.dm = uri3;
        this.dn = z;
        this.f0do = z2;
        this.dp = str7;
        this.dq = i2;
        this.dr = i3;
        this.ds = i4;
    }

    public GameEntity(Game game) {
        this.ab = 1;
        this.df = game.getApplicationId();
        this.dg = game.getPrimaryCategory();
        this.dh = game.getSecondaryCategory();
        this.di = game.getDescription();
        this.dj = game.getDeveloperName();
        this.cl = game.getDisplayName();
        this.dk = game.getIconImageUri();
        this.dl = game.getHiResImageUri();
        this.dm = game.getFeaturedImageUri();
        this.dn = game.isPlayEnabledGame();
        this.f0do = game.isInstanceInstalled();
        this.dp = game.getInstancePackageName();
        this.dq = game.getGameplayAclStatus();
        this.dr = game.getAchievementTotalCount();
        this.ds = game.getLeaderboardCount();
    }

    static int a(Game game) {
        return r.hashCode(game.getApplicationId(), game.getDisplayName(), game.getPrimaryCategory(), game.getSecondaryCategory(), game.getDescription(), game.getDeveloperName(), game.getIconImageUri(), game.getHiResImageUri(), game.getFeaturedImageUri(), Boolean.valueOf(game.isPlayEnabledGame()), Boolean.valueOf(game.isInstanceInstalled()), game.getInstancePackageName(), Integer.valueOf(game.getGameplayAclStatus()), Integer.valueOf(game.getAchievementTotalCount()), Integer.valueOf(game.getLeaderboardCount()));
    }

    static boolean a(Game game, Object obj) {
        if (!(obj instanceof Game)) {
            return false;
        }
        if (game == obj) {
            return true;
        }
        Game game2 = (Game) obj;
        return r.a(game2.getApplicationId(), game.getApplicationId()) && r.a(game2.getDisplayName(), game.getDisplayName()) && r.a(game2.getPrimaryCategory(), game.getPrimaryCategory()) && r.a(game2.getSecondaryCategory(), game.getSecondaryCategory()) && r.a(game2.getDescription(), game.getDescription()) && r.a(game2.getDeveloperName(), game.getDeveloperName()) && r.a(game2.getIconImageUri(), game.getIconImageUri()) && r.a(game2.getHiResImageUri(), game.getHiResImageUri()) && r.a(game2.getFeaturedImageUri(), game.getFeaturedImageUri()) && r.a(Boolean.valueOf(game2.isPlayEnabledGame()), Boolean.valueOf(game.isPlayEnabledGame())) && r.a(Boolean.valueOf(game2.isInstanceInstalled()), Boolean.valueOf(game.isInstanceInstalled())) && r.a(game2.getInstancePackageName(), game.getInstancePackageName()) && r.a(Integer.valueOf(game2.getGameplayAclStatus()), Integer.valueOf(game.getGameplayAclStatus())) && r.a(Integer.valueOf(game2.getAchievementTotalCount()), Integer.valueOf(game.getAchievementTotalCount())) && r.a(Integer.valueOf(game2.getLeaderboardCount()), Integer.valueOf(game.getLeaderboardCount()));
    }

    static String b(Game game) {
        return r.c(game).a("ApplicationId", game.getApplicationId()).a("DisplayName", game.getDisplayName()).a("PrimaryCategory", game.getPrimaryCategory()).a("SecondaryCategory", game.getSecondaryCategory()).a("Description", game.getDescription()).a("DeveloperName", game.getDeveloperName()).a("IconImageUri", game.getIconImageUri()).a("HiResImageUri", game.getHiResImageUri()).a("FeaturedImageUri", game.getFeaturedImageUri()).a("PlayEnabledGame", Boolean.valueOf(game.isPlayEnabledGame())).a("InstanceInstalled", Boolean.valueOf(game.isInstanceInstalled())).a("InstancePackageName", game.getInstancePackageName()).a("GameplayAclStatus", Integer.valueOf(game.getGameplayAclStatus())).a("AchievementTotalCount", Integer.valueOf(game.getAchievementTotalCount())).a("LeaderboardCount", Integer.valueOf(game.getLeaderboardCount())).toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public Game freeze() {
        return this;
    }

    public int getAchievementTotalCount() {
        return this.dr;
    }

    public String getApplicationId() {
        return this.df;
    }

    public String getDescription() {
        return this.di;
    }

    public String getDeveloperName() {
        return this.dj;
    }

    public String getDisplayName() {
        return this.cl;
    }

    public Uri getFeaturedImageUri() {
        return this.dm;
    }

    public int getGameplayAclStatus() {
        return this.dq;
    }

    public Uri getHiResImageUri() {
        return this.dl;
    }

    public Uri getIconImageUri() {
        return this.dk;
    }

    public String getInstancePackageName() {
        return this.dp;
    }

    public int getLeaderboardCount() {
        return this.ds;
    }

    public String getPrimaryCategory() {
        return this.dg;
    }

    public String getSecondaryCategory() {
        return this.dh;
    }

    public int hashCode() {
        return a(this);
    }

    public int i() {
        return this.ab;
    }

    public boolean isInstanceInstalled() {
        return this.f0do;
    }

    public boolean isPlayEnabledGame() {
        return this.dn;
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        String str = null;
        if (!w()) {
            a.a(this, parcel, i);
            return;
        }
        parcel.writeString(this.df);
        parcel.writeString(this.cl);
        parcel.writeString(this.dg);
        parcel.writeString(this.dh);
        parcel.writeString(this.di);
        parcel.writeString(this.dj);
        parcel.writeString(this.dk == null ? null : this.dk.toString());
        parcel.writeString(this.dl == null ? null : this.dl.toString());
        if (this.dm != null) {
            str = this.dm.toString();
        }
        parcel.writeString(str);
        parcel.writeInt(this.dn ? 1 : 0);
        if (!this.f0do) {
            i2 = 0;
        }
        parcel.writeInt(i2);
        parcel.writeString(this.dp);
        parcel.writeInt(this.dq);
        parcel.writeInt(this.dr);
        parcel.writeInt(this.ds);
    }
}
