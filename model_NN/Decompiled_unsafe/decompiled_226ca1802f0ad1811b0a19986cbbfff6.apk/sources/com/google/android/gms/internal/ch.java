package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.cc;
import java.util.HashSet;
import java.util.Set;

public class ch implements Parcelable.Creator<cc.b.C0007b> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(cc.b.C0007b bVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        Set<Integer> bH = bVar.bH();
        if (bH.contains(1)) {
            b.c(parcel, 1, bVar.i());
        }
        if (bH.contains(2)) {
            b.c(parcel, 2, bVar.getHeight());
        }
        if (bH.contains(3)) {
            b.a(parcel, 3, bVar.getUrl(), true);
        }
        if (bH.contains(4)) {
            b.c(parcel, 4, bVar.getWidth());
        }
        b.C(parcel, d);
    }

    /* renamed from: C */
    public cc.b.C0007b createFromParcel(Parcel parcel) {
        int i = 0;
        int c2 = a.c(parcel);
        HashSet hashSet = new HashSet();
        String str = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i3 = a.f(parcel, b2);
                    hashSet.add(1);
                    break;
                case 2:
                    i2 = a.f(parcel, b2);
                    hashSet.add(2);
                    break;
                case 3:
                    str = a.l(parcel, b2);
                    hashSet.add(3);
                    break;
                case 4:
                    i = a.f(parcel, b2);
                    hashSet.add(4);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new cc.b.C0007b(hashSet, i3, i2, str, i);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: ac */
    public cc.b.C0007b[] newArray(int i) {
        return new cc.b.C0007b[i];
    }
}
