package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ae;
import com.google.android.gms.plus.model.moments.Moment;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public final class bz extends ae implements SafeParcelable, Moment {
    public static final ca CREATOR = new ca();
    private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
    private final int ab;
    private final Set<Integer> iD;
    private bx jB;
    private bx jC;
    private String jh;
    private String js;
    private String jy;

    static {
        iC.put("id", ae.a.f("id", 2));
        iC.put("result", ae.a.a("result", 4, bx.class));
        iC.put("startDate", ae.a.f("startDate", 5));
        iC.put("target", ae.a.a("target", 6, bx.class));
        iC.put("type", ae.a.f("type", 7));
    }

    public bz() {
        this.ab = 1;
        this.iD = new HashSet();
    }

    bz(Set<Integer> set, int i, String str, bx bxVar, String str2, bx bxVar2, String str3) {
        this.iD = set;
        this.ab = i;
        this.jh = str;
        this.jB = bxVar;
        this.js = str2;
        this.jC = bxVar2;
        this.jy = str3;
    }

    public HashMap<String, ae.a<?, ?>> T() {
        return iC;
    }

    /* access modifiers changed from: protected */
    public boolean a(ae.a aVar) {
        return this.iD.contains(Integer.valueOf(aVar.aa()));
    }

    /* access modifiers changed from: protected */
    public Object b(ae.a aVar) {
        switch (aVar.aa()) {
            case 2:
                return this.jh;
            case 3:
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
            case 4:
                return this.jB;
            case 5:
                return this.js;
            case 6:
                return this.jC;
            case 7:
                return this.jy;
        }
    }

    /* access modifiers changed from: package-private */
    public Set<Integer> bH() {
        return this.iD;
    }

    /* access modifiers changed from: package-private */
    public bx bY() {
        return this.jB;
    }

    /* access modifiers changed from: package-private */
    public bx bZ() {
        return this.jC;
    }

    /* renamed from: ca */
    public bz freeze() {
        return this;
    }

    public int describeContents() {
        ca caVar = CREATOR;
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof bz)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        bz bzVar = (bz) obj;
        for (ae.a next : iC.values()) {
            if (a(next)) {
                if (!bzVar.a(next)) {
                    return false;
                }
                if (!b(next).equals(bzVar.b(next))) {
                    return false;
                }
            } else if (bzVar.a(next)) {
                return false;
            }
        }
        return true;
    }

    public String getId() {
        return this.jh;
    }

    public String getStartDate() {
        return this.js;
    }

    public String getType() {
        return this.jy;
    }

    public int hashCode() {
        int i = 0;
        Iterator<ae.a<?, ?>> it = iC.values().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            ae.a next = it.next();
            if (a(next)) {
                i = b(next).hashCode() + i2 + next.aa();
            } else {
                i = i2;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    /* access modifiers changed from: protected */
    public Object m(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean n(String str) {
        return false;
    }

    public void writeToParcel(Parcel parcel, int i) {
        ca caVar = CREATOR;
        ca.a(this, parcel, i);
    }
}
