package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.av;
import com.google.android.gms.internal.h;
import com.google.android.gms.internal.r;

public final class PlayerEntity extends av implements Player {
    public static final Parcelable.Creator<PlayerEntity> CREATOR = new a();
    private final int ab;
    private final String cl;
    private final Uri dk;
    private final Uri dl;
    private final String dx;
    private final long dy;

    final class a extends c {
        a() {
        }

        /* renamed from: o */
        public PlayerEntity createFromParcel(Parcel parcel) {
            Uri uri = null;
            if (PlayerEntity.c(PlayerEntity.v()) || PlayerEntity.h(PlayerEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            Uri parse = readString3 == null ? null : Uri.parse(readString3);
            if (readString4 != null) {
                uri = Uri.parse(readString4);
            }
            return new PlayerEntity(1, readString, readString2, parse, uri, parcel.readLong());
        }
    }

    PlayerEntity(int i, String str, String str2, Uri uri, Uri uri2, long j) {
        this.ab = i;
        this.dx = str;
        this.cl = str2;
        this.dk = uri;
        this.dl = uri2;
        this.dy = j;
    }

    public PlayerEntity(Player player) {
        boolean z = true;
        this.ab = 1;
        this.dx = player.getPlayerId();
        this.cl = player.getDisplayName();
        this.dk = player.getIconImageUri();
        this.dl = player.getHiResImageUri();
        this.dy = player.getRetrievedTimestamp();
        h.b(this.dx);
        h.b(this.cl);
        h.a(this.dy <= 0 ? false : z);
    }

    static int a(Player player) {
        return r.hashCode(player.getPlayerId(), player.getDisplayName(), player.getIconImageUri(), player.getHiResImageUri(), Long.valueOf(player.getRetrievedTimestamp()));
    }

    static boolean a(Player player, Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        if (player == obj) {
            return true;
        }
        Player player2 = (Player) obj;
        return r.a(player2.getPlayerId(), player.getPlayerId()) && r.a(player2.getDisplayName(), player.getDisplayName()) && r.a(player2.getIconImageUri(), player.getIconImageUri()) && r.a(player2.getHiResImageUri(), player.getHiResImageUri()) && r.a(Long.valueOf(player2.getRetrievedTimestamp()), Long.valueOf(player.getRetrievedTimestamp()));
    }

    static String b(Player player) {
        return r.c(player).a("PlayerId", player.getPlayerId()).a("DisplayName", player.getDisplayName()).a("IconImageUri", player.getIconImageUri()).a("HiResImageUri", player.getHiResImageUri()).a("RetrievedTimestamp", Long.valueOf(player.getRetrievedTimestamp())).toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public Player freeze() {
        return this;
    }

    public String getDisplayName() {
        return this.cl;
    }

    public Uri getHiResImageUri() {
        return this.dl;
    }

    public Uri getIconImageUri() {
        return this.dk;
    }

    public String getPlayerId() {
        return this.dx;
    }

    public long getRetrievedTimestamp() {
        return this.dy;
    }

    public int hashCode() {
        return a(this);
    }

    public int i() {
        return this.ab;
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        String str = null;
        if (!w()) {
            c.a(this, parcel, i);
            return;
        }
        parcel.writeString(this.dx);
        parcel.writeString(this.cl);
        parcel.writeString(this.dk == null ? null : this.dk.toString());
        if (this.dl != null) {
            str = this.dl.toString();
        }
        parcel.writeString(str);
        parcel.writeLong(this.dy);
    }
}
