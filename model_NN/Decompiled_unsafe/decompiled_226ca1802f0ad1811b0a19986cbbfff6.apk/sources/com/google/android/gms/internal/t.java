package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.view.View;
import com.google.android.gms.dynamic.c;
import com.google.android.gms.dynamic.e;
import com.google.android.gms.internal.q;

public final class t extends e<q> {
    private static final t ca = new t();

    private t() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }

    public static View d(Context context, int i, int i2) {
        return ca.e(context, i, i2);
    }

    private View e(Context context, int i, int i2) {
        try {
            return (View) c.a(((q) h(context)).a(c.f(context), i, i2));
        } catch (Exception e) {
            throw new e.a("Could not get button with size " + i + " and color " + i2, e);
        }
    }

    /* renamed from: j */
    public q k(IBinder iBinder) {
        return q.a.i(iBinder);
    }
}
