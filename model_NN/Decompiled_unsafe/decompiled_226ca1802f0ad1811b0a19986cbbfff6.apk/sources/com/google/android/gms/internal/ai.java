package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.ah;
import java.util.ArrayList;

public class ai implements Parcelable.Creator<ah> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(ah ahVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, ahVar.i());
        b.b(parcel, 2, ahVar.ai(), false);
        b.a(parcel, 3, ahVar.aj(), false);
        b.C(parcel, d);
    }

    /* renamed from: k */
    public ah createFromParcel(Parcel parcel) {
        String str = null;
        int c2 = a.c(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i = a.f(parcel, b2);
                    break;
                case 2:
                    arrayList = a.c(parcel, b2, ah.a.CREATOR);
                    break;
                case 3:
                    str = a.l(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new ah(i, arrayList, str);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: t */
    public ah[] newArray(int i) {
        return new ah[i];
    }
}
