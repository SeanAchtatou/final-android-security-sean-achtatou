package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.internal.av;
import com.google.android.gms.internal.r;

public final class ParticipantEntity extends av implements Participant {
    public static final Parcelable.Creator<ParticipantEntity> CREATOR = new a();
    private final int ab;
    private final String cl;
    private final String dX;
    private final Uri dk;
    private final Uri dl;
    private final int eN;
    private final String eO;
    private final boolean eP;
    private final PlayerEntity eQ;
    private final int eR;

    final class a extends c {
        a() {
        }

        /* renamed from: q */
        public ParticipantEntity createFromParcel(Parcel parcel) {
            boolean z = false;
            if (ParticipantEntity.c(ParticipantEntity.v()) || ParticipantEntity.h(ParticipantEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            Uri parse = readString3 == null ? null : Uri.parse(readString3);
            String readString4 = parcel.readString();
            Uri parse2 = readString4 == null ? null : Uri.parse(readString4);
            int readInt = parcel.readInt();
            String readString5 = parcel.readString();
            boolean z2 = parcel.readInt() > 0;
            if (parcel.readInt() > 0) {
                z = true;
            }
            return new ParticipantEntity(1, readString, readString2, parse, parse2, readInt, readString5, z2, z ? PlayerEntity.CREATOR.createFromParcel(parcel) : null, 7);
        }
    }

    ParticipantEntity(int i, String str, String str2, Uri uri, Uri uri2, int i2, String str3, boolean z, PlayerEntity playerEntity, int i3) {
        this.ab = i;
        this.dX = str;
        this.cl = str2;
        this.dk = uri;
        this.dl = uri2;
        this.eN = i2;
        this.eO = str3;
        this.eP = z;
        this.eQ = playerEntity;
        this.eR = i3;
    }

    public ParticipantEntity(Participant participant) {
        this.ab = 1;
        this.dX = participant.getParticipantId();
        this.cl = participant.getDisplayName();
        this.dk = participant.getIconImageUri();
        this.dl = participant.getHiResImageUri();
        this.eN = participant.getStatus();
        this.eO = participant.aM();
        this.eP = participant.isConnectedToRoom();
        Player player = participant.getPlayer();
        this.eQ = player == null ? null : new PlayerEntity(player);
        this.eR = participant.aN();
    }

    static int a(Participant participant) {
        return r.hashCode(participant.getPlayer(), Integer.valueOf(participant.getStatus()), participant.aM(), Boolean.valueOf(participant.isConnectedToRoom()), participant.getDisplayName(), participant.getIconImageUri(), participant.getHiResImageUri(), Integer.valueOf(participant.aN()));
    }

    static boolean a(Participant participant, Object obj) {
        if (!(obj instanceof Participant)) {
            return false;
        }
        if (participant == obj) {
            return true;
        }
        Participant participant2 = (Participant) obj;
        return r.a(participant2.getPlayer(), participant.getPlayer()) && r.a(Integer.valueOf(participant2.getStatus()), Integer.valueOf(participant.getStatus())) && r.a(participant2.aM(), participant.aM()) && r.a(Boolean.valueOf(participant2.isConnectedToRoom()), Boolean.valueOf(participant.isConnectedToRoom())) && r.a(participant2.getDisplayName(), participant.getDisplayName()) && r.a(participant2.getIconImageUri(), participant.getIconImageUri()) && r.a(participant2.getHiResImageUri(), participant.getHiResImageUri()) && r.a(Integer.valueOf(participant2.aN()), Integer.valueOf(participant.aN()));
    }

    static String b(Participant participant) {
        return r.c(participant).a("Player", participant.getPlayer()).a("Status", Integer.valueOf(participant.getStatus())).a("ClientAddress", participant.aM()).a("ConnectedToRoom", Boolean.valueOf(participant.isConnectedToRoom())).a("DisplayName", participant.getDisplayName()).a("IconImage", participant.getIconImageUri()).a("HiResImage", participant.getHiResImageUri()).a("Capabilities", Integer.valueOf(participant.aN())).toString();
    }

    public String aM() {
        return this.eO;
    }

    public int aN() {
        return this.eR;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public Participant freeze() {
        return this;
    }

    public String getDisplayName() {
        return this.eQ == null ? this.cl : this.eQ.getDisplayName();
    }

    public Uri getHiResImageUri() {
        return this.eQ == null ? this.dl : this.eQ.getHiResImageUri();
    }

    public Uri getIconImageUri() {
        return this.eQ == null ? this.dk : this.eQ.getIconImageUri();
    }

    public String getParticipantId() {
        return this.dX;
    }

    public Player getPlayer() {
        return this.eQ;
    }

    public int getStatus() {
        return this.eN;
    }

    public int hashCode() {
        return a(this);
    }

    public int i() {
        return this.ab;
    }

    public boolean isConnectedToRoom() {
        return this.eP;
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        String str = null;
        int i2 = 0;
        if (!w()) {
            c.a(this, parcel, i);
            return;
        }
        parcel.writeString(this.dX);
        parcel.writeString(this.cl);
        parcel.writeString(this.dk == null ? null : this.dk.toString());
        if (this.dl != null) {
            str = this.dl.toString();
        }
        parcel.writeString(str);
        parcel.writeInt(this.eN);
        parcel.writeString(this.eO);
        parcel.writeInt(this.eP ? 1 : 0);
        if (this.eQ != null) {
            i2 = 1;
        }
        parcel.writeInt(i2);
        if (this.eQ != null) {
            this.eQ.writeToParcel(parcel, i);
        }
    }
}
