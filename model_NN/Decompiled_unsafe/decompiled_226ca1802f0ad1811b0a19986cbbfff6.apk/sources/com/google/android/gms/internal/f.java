package com.google.android.gms.internal;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;

public final class f extends Drawable implements Drawable.Callback {
    private boolean aP;
    private int aR;
    private long aS;
    private int aT;
    private int aU;
    private int aV;
    private int aW;
    private int aX;
    private boolean aY;
    private b aZ;
    private Drawable ba;
    private Drawable bb;
    private boolean bc;
    private boolean bd;
    private boolean be;
    private int bf;

    final class a extends Drawable {
        /* access modifiers changed from: private */
        public static final a bg = new a();
        private static final C0008a bh = new C0008a();

        /* renamed from: com.google.android.gms.internal.f$a$a  reason: collision with other inner class name */
        final class C0008a extends Drawable.ConstantState {
            private C0008a() {
            }

            public int getChangingConfigurations() {
                return 0;
            }

            public Drawable newDrawable() {
                return a.bg;
            }
        }

        private a() {
        }

        public void draw(Canvas canvas) {
        }

        public Drawable.ConstantState getConstantState() {
            return bh;
        }

        public int getOpacity() {
            return -2;
        }

        public void setAlpha(int i) {
        }

        public void setColorFilter(ColorFilter colorFilter) {
        }
    }

    final class b extends Drawable.ConstantState {
        int bi;
        int bj;

        b(b bVar) {
            if (bVar != null) {
                this.bi = bVar.bi;
                this.bj = bVar.bj;
            }
        }

        public int getChangingConfigurations() {
            return this.bi;
        }

        public Drawable newDrawable() {
            return new f(this);
        }
    }

    public f(Drawable drawable, Drawable drawable2) {
        this(null);
        drawable = drawable == null ? a.bg : drawable;
        this.ba = drawable;
        drawable.setCallback(this);
        this.aZ.bj |= drawable.getChangingConfigurations();
        drawable2 = drawable2 == null ? a.bg : drawable2;
        this.bb = drawable2;
        drawable2.setCallback(this);
        this.aZ.bj |= drawable2.getChangingConfigurations();
    }

    f(b bVar) {
        this.aR = 0;
        this.aV = 255;
        this.aX = 0;
        this.aP = true;
        this.aZ = new b(bVar);
    }

    public boolean canConstantState() {
        if (!this.bc) {
            this.bd = (this.ba.getConstantState() == null || this.bb.getConstantState() == null) ? false : true;
            this.bc = true;
        }
        return this.bd;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void draw(Canvas canvas) {
        boolean z = true;
        boolean z2 = false;
        switch (this.aR) {
            case 1:
                this.aS = SystemClock.uptimeMillis();
                this.aR = 2;
                break;
            case 2:
                if (this.aS >= 0) {
                    float uptimeMillis = ((float) (SystemClock.uptimeMillis() - this.aS)) / ((float) this.aW);
                    if (uptimeMillis < 1.0f) {
                        z = false;
                    }
                    if (z) {
                        this.aR = 0;
                    }
                    this.aX = (int) ((Math.min(uptimeMillis, 1.0f) * ((float) (this.aU - this.aT))) + ((float) this.aT));
                }
            default:
                z2 = z;
                break;
        }
        int i = this.aX;
        boolean z3 = this.aP;
        Drawable drawable = this.ba;
        Drawable drawable2 = this.bb;
        if (z2) {
            if (!z3 || i == 0) {
                drawable.draw(canvas);
            }
            if (i == this.aV) {
                drawable2.setAlpha(this.aV);
                drawable2.draw(canvas);
                return;
            }
            return;
        }
        if (z3) {
            drawable.setAlpha(this.aV - i);
        }
        drawable.draw(canvas);
        if (z3) {
            drawable.setAlpha(this.aV);
        }
        if (i > 0) {
            drawable2.setAlpha(i);
            drawable2.draw(canvas);
            drawable2.setAlpha(this.aV);
        }
        invalidateSelf();
    }

    public int getChangingConfigurations() {
        return super.getChangingConfigurations() | this.aZ.bi | this.aZ.bj;
    }

    public Drawable.ConstantState getConstantState() {
        if (!canConstantState()) {
            return null;
        }
        this.aZ.bi = getChangingConfigurations();
        return this.aZ;
    }

    public int getIntrinsicHeight() {
        return Math.max(this.ba.getIntrinsicHeight(), this.bb.getIntrinsicHeight());
    }

    public int getIntrinsicWidth() {
        return Math.max(this.ba.getIntrinsicWidth(), this.bb.getIntrinsicWidth());
    }

    public int getOpacity() {
        if (!this.be) {
            this.bf = Drawable.resolveOpacity(this.ba.getOpacity(), this.bb.getOpacity());
            this.be = true;
        }
        return this.bf;
    }

    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback;
        if (as.an() && (callback = getCallback()) != null) {
            callback.invalidateDrawable(this);
        }
    }

    public Drawable mutate() {
        if (!this.aY && super.mutate() == this) {
            if (!canConstantState()) {
                throw new IllegalStateException("One or more children of this LayerDrawable does not have constant state; this drawable cannot be mutated.");
            }
            this.ba.mutate();
            this.bb.mutate();
            this.aY = true;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.ba.setBounds(rect);
        this.bb.setBounds(rect);
    }

    public Drawable r() {
        return this.bb;
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        Drawable.Callback callback;
        if (as.an() && (callback = getCallback()) != null) {
            callback.scheduleDrawable(this, runnable, j);
        }
    }

    public void setAlpha(int i) {
        if (this.aX == this.aV) {
            this.aX = i;
        }
        this.aV = i;
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.ba.setColorFilter(colorFilter);
        this.bb.setColorFilter(colorFilter);
    }

    public void startTransition(int i) {
        this.aT = 0;
        this.aU = this.aV;
        this.aX = 0;
        this.aW = i;
        this.aR = 1;
        invalidateSelf();
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback;
        if (as.an() && (callback = getCallback()) != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }
}
