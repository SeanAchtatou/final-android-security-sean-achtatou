package com.google.android.gms.maps.model;

import com.google.android.gms.dynamic.b;
import com.google.android.gms.internal.s;

public final class BitmapDescriptor {
    private final b fV;

    public BitmapDescriptor(b bVar) {
        this.fV = (b) s.d(bVar);
    }

    public b aW() {
        return this.fV;
    }
}
