package com.google.android.gms.internal;

import android.os.Build;

public final class as {
    public static boolean an() {
        return x(11);
    }

    public static boolean ap() {
        return x(13);
    }

    public static boolean as() {
        return x(17);
    }

    private static boolean x(int i) {
        return Build.VERSION.SDK_INT >= i;
    }
}
