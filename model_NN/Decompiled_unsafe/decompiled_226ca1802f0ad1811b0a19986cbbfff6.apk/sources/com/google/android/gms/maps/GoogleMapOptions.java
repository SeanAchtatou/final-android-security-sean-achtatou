package com.google.android.gms.maps;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.util.AttributeSet;
import com.google.android.gms.R;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.internal.a;
import com.google.android.gms.maps.internal.q;
import com.google.android.gms.maps.model.CameraPosition;

public final class GoogleMapOptions implements SafeParcelable {
    public static final GoogleMapOptionsCreator CREATOR = new GoogleMapOptionsCreator();
    private final int ab;
    private Boolean go;
    private Boolean gp;
    private int gq;
    private CameraPosition gr;
    private Boolean gs;
    private Boolean gt;
    private Boolean gu;
    private Boolean gv;
    private Boolean gw;
    private Boolean gx;

    public GoogleMapOptions() {
        this.gq = -1;
        this.ab = 1;
    }

    GoogleMapOptions(int i, byte b2, byte b3, int i2, CameraPosition cameraPosition, byte b4, byte b5, byte b6, byte b7, byte b8, byte b9) {
        this.gq = -1;
        this.ab = i;
        this.go = a.a(b2);
        this.gp = a.a(b3);
        this.gq = i2;
        this.gr = cameraPosition;
        this.gs = a.a(b4);
        this.gt = a.a(b5);
        this.gu = a.a(b6);
        this.gv = a.a(b7);
        this.gw = a.a(b8);
        this.gx = a.a(b9);
    }

    public static GoogleMapOptions createFromAttributes(Context context, AttributeSet attributeSet) {
        if (attributeSet == null) {
            return null;
        }
        TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, R.styleable.MapAttrs);
        GoogleMapOptions googleMapOptions = new GoogleMapOptions();
        if (obtainAttributes.hasValue(0)) {
            googleMapOptions.mapType(obtainAttributes.getInt(0, -1));
        }
        if (obtainAttributes.hasValue(13)) {
            googleMapOptions.zOrderOnTop(obtainAttributes.getBoolean(13, false));
        }
        if (obtainAttributes.hasValue(12)) {
            googleMapOptions.useViewLifecycleInFragment(obtainAttributes.getBoolean(12, false));
        }
        if (obtainAttributes.hasValue(6)) {
            googleMapOptions.compassEnabled(obtainAttributes.getBoolean(6, true));
        }
        if (obtainAttributes.hasValue(7)) {
            googleMapOptions.rotateGesturesEnabled(obtainAttributes.getBoolean(7, true));
        }
        if (obtainAttributes.hasValue(8)) {
            googleMapOptions.scrollGesturesEnabled(obtainAttributes.getBoolean(8, true));
        }
        if (obtainAttributes.hasValue(9)) {
            googleMapOptions.tiltGesturesEnabled(obtainAttributes.getBoolean(9, true));
        }
        if (obtainAttributes.hasValue(11)) {
            googleMapOptions.zoomGesturesEnabled(obtainAttributes.getBoolean(11, true));
        }
        if (obtainAttributes.hasValue(10)) {
            googleMapOptions.zoomControlsEnabled(obtainAttributes.getBoolean(10, true));
        }
        googleMapOptions.camera(CameraPosition.createFromAttributes(context, attributeSet));
        obtainAttributes.recycle();
        return googleMapOptions;
    }

    /* access modifiers changed from: package-private */
    public byte aZ() {
        return a.b(this.go);
    }

    /* access modifiers changed from: package-private */
    public byte ba() {
        return a.b(this.gp);
    }

    /* access modifiers changed from: package-private */
    public byte bb() {
        return a.b(this.gs);
    }

    /* access modifiers changed from: package-private */
    public byte bc() {
        return a.b(this.gt);
    }

    /* access modifiers changed from: package-private */
    public byte bd() {
        return a.b(this.gu);
    }

    /* access modifiers changed from: package-private */
    public byte be() {
        return a.b(this.gv);
    }

    /* access modifiers changed from: package-private */
    public byte bf() {
        return a.b(this.gw);
    }

    /* access modifiers changed from: package-private */
    public byte bg() {
        return a.b(this.gx);
    }

    public GoogleMapOptions camera(CameraPosition cameraPosition) {
        this.gr = cameraPosition;
        return this;
    }

    public GoogleMapOptions compassEnabled(boolean z) {
        this.gt = Boolean.valueOf(z);
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public CameraPosition getCamera() {
        return this.gr;
    }

    public int getMapType() {
        return this.gq;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    public GoogleMapOptions mapType(int i) {
        this.gq = i;
        return this;
    }

    public GoogleMapOptions rotateGesturesEnabled(boolean z) {
        this.gx = Boolean.valueOf(z);
        return this;
    }

    public GoogleMapOptions scrollGesturesEnabled(boolean z) {
        this.gu = Boolean.valueOf(z);
        return this;
    }

    public GoogleMapOptions tiltGesturesEnabled(boolean z) {
        this.gw = Boolean.valueOf(z);
        return this;
    }

    public GoogleMapOptions useViewLifecycleInFragment(boolean z) {
        this.gp = Boolean.valueOf(z);
        return this;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (q.bn()) {
            a.a(this, parcel, i);
        } else {
            GoogleMapOptionsCreator.a(this, parcel, i);
        }
    }

    public GoogleMapOptions zOrderOnTop(boolean z) {
        this.go = Boolean.valueOf(z);
        return this;
    }

    public GoogleMapOptions zoomControlsEnabled(boolean z) {
        this.gs = Boolean.valueOf(z);
        return this;
    }

    public GoogleMapOptions zoomGesturesEnabled(boolean z) {
        this.gv = Boolean.valueOf(z);
        return this;
    }
}
