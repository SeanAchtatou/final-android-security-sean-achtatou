package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;

public class CircleOptionsCreator implements Parcelable.Creator<CircleOptions> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(CircleOptions circleOptions, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, circleOptions.i());
        b.a(parcel, 2, (Parcelable) circleOptions.getCenter(), i, false);
        b.a(parcel, 3, circleOptions.getRadius());
        b.a(parcel, 4, circleOptions.getStrokeWidth());
        b.c(parcel, 5, circleOptions.getStrokeColor());
        b.c(parcel, 6, circleOptions.getFillColor());
        b.a(parcel, 7, circleOptions.getZIndex());
        b.a(parcel, 8, circleOptions.isVisible());
        b.C(parcel, d);
    }

    public CircleOptions createFromParcel(Parcel parcel) {
        float f = 0.0f;
        boolean z = false;
        int c2 = a.c(parcel);
        LatLng latLng = null;
        double d = 0.0d;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        int i3 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i3 = a.f(parcel, b2);
                    break;
                case 2:
                    latLng = (LatLng) a.a(parcel, b2, LatLng.CREATOR);
                    break;
                case 3:
                    d = a.j(parcel, b2);
                    break;
                case 4:
                    f2 = a.i(parcel, b2);
                    break;
                case 5:
                    i2 = a.f(parcel, b2);
                    break;
                case 6:
                    i = a.f(parcel, b2);
                    break;
                case 7:
                    f = a.i(parcel, b2);
                    break;
                case 8:
                    z = a.c(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new CircleOptions(i3, latLng, d, f2, i2, i, f, z);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    public CircleOptions[] newArray(int i) {
        return new CircleOptions[i];
    }
}
