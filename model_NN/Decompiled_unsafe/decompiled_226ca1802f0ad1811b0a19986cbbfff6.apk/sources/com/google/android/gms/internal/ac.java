package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.ab;
import java.util.ArrayList;

public class ac implements Parcelable.Creator<ab> {
    static void a(ab abVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, abVar.i());
        b.b(parcel, 2, abVar.Q(), false);
        b.C(parcel, d);
    }

    /* renamed from: g */
    public ab createFromParcel(Parcel parcel) {
        int c2 = a.c(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i = a.f(parcel, b2);
                    break;
                case 2:
                    arrayList = a.c(parcel, b2, ab.a.CREATOR);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new ab(i, arrayList);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: p */
    public ab[] newArray(int i) {
        return new ab[i];
    }
}
