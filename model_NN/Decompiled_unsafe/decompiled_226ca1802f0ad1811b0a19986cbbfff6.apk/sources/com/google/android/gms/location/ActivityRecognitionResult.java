package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.List;

public class ActivityRecognitionResult implements SafeParcelable {
    public static final ActivityRecognitionResultCreator CREATOR = new ActivityRecognitionResultCreator();
    private final int ab = 1;
    List<DetectedActivity> fp;
    long fq;
    long fr;

    public ActivityRecognitionResult(int i, List<DetectedActivity> list, long j, long j2) {
        this.fp = list;
        this.fq = j;
        this.fr = j2;
    }

    public int describeContents() {
        return 0;
    }

    public int i() {
        return this.ab;
    }

    public String toString() {
        return "ActivityRecognitionResult [probableActivities=" + this.fp + ", timeMillis=" + this.fq + ", elapsedRealtimeMillis=" + this.fr + "]";
    }

    public void writeToParcel(Parcel parcel, int i) {
        ActivityRecognitionResultCreator.a(this, parcel, i);
    }
}
