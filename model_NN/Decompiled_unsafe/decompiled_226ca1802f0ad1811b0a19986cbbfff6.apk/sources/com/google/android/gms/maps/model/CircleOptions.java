package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.internal.q;

public final class CircleOptions implements SafeParcelable {
    public static final CircleOptionsCreator CREATOR = new CircleOptionsCreator();
    private final int ab;
    private LatLng gW;
    private double gX;
    private float gY;
    private int gZ;
    private int ha;
    private float hb;
    private boolean hc;

    public CircleOptions() {
        this.gW = null;
        this.gX = 0.0d;
        this.gY = 10.0f;
        this.gZ = -16777216;
        this.ha = 0;
        this.hb = 0.0f;
        this.hc = true;
        this.ab = 1;
    }

    CircleOptions(int i, LatLng latLng, double d, float f, int i2, int i3, float f2, boolean z) {
        this.gW = null;
        this.gX = 0.0d;
        this.gY = 10.0f;
        this.gZ = -16777216;
        this.ha = 0;
        this.hb = 0.0f;
        this.hc = true;
        this.ab = i;
        this.gW = latLng;
        this.gX = d;
        this.gY = f;
        this.gZ = i2;
        this.ha = i3;
        this.hb = f2;
        this.hc = z;
    }

    public int describeContents() {
        return 0;
    }

    public LatLng getCenter() {
        return this.gW;
    }

    public int getFillColor() {
        return this.ha;
    }

    public double getRadius() {
        return this.gX;
    }

    public int getStrokeColor() {
        return this.gZ;
    }

    public float getStrokeWidth() {
        return this.gY;
    }

    public float getZIndex() {
        return this.hb;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    public boolean isVisible() {
        return this.hc;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (q.bn()) {
            b.a(this, parcel, i);
        } else {
            CircleOptionsCreator.a(this, parcel, i);
        }
    }
}
