package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.r;
import com.google.android.gms.internal.s;
import com.google.android.gms.maps.internal.q;

public final class LatLngBounds implements SafeParcelable {
    public static final LatLngBoundsCreator CREATOR = new LatLngBoundsCreator();
    private final int ab;
    public final LatLng northeast;
    public final LatLng southwest;

    LatLngBounds(int i, LatLng latLng, LatLng latLng2) {
        s.b(latLng, "null southwest");
        s.b(latLng2, "null northeast");
        s.a(latLng2.latitude >= latLng.latitude, "southern latitude exceeds northern latitude (%s > %s)", Double.valueOf(latLng.latitude), Double.valueOf(latLng2.latitude));
        this.ab = i;
        this.southwest = latLng;
        this.northeast = latLng2;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LatLngBounds)) {
            return false;
        }
        LatLngBounds latLngBounds = (LatLngBounds) obj;
        return this.southwest.equals(latLngBounds.southwest) && this.northeast.equals(latLngBounds.northeast);
    }

    public int hashCode() {
        return r.hashCode(this.southwest, this.northeast);
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    public String toString() {
        return r.c(this).a("southwest", this.southwest).a("northeast", this.northeast).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (q.bn()) {
            d.a(this, parcel, i);
        } else {
            LatLngBoundsCreator.a(this, parcel, i);
        }
    }
}
