package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.cc;
import java.util.HashSet;
import java.util.Set;

public class cf implements Parcelable.Creator<cc.b> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.cc$b$a, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.cc$b$b, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(cc.b bVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        Set<Integer> bH = bVar.bH();
        if (bH.contains(1)) {
            b.c(parcel, 1, bVar.i());
        }
        if (bH.contains(2)) {
            b.a(parcel, 2, (Parcelable) bVar.cl(), i, true);
        }
        if (bH.contains(3)) {
            b.a(parcel, 3, (Parcelable) bVar.cm(), i, true);
        }
        if (bH.contains(4)) {
            b.c(parcel, 4, bVar.getLayout());
        }
        b.C(parcel, d);
    }

    /* renamed from: A */
    public cc.b createFromParcel(Parcel parcel) {
        cc.b.C0007b bVar = null;
        int i = 0;
        int c2 = a.c(parcel);
        HashSet hashSet = new HashSet();
        cc.b.a aVar = null;
        int i2 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i2 = a.f(parcel, b2);
                    hashSet.add(1);
                    break;
                case 2:
                    hashSet.add(2);
                    aVar = (cc.b.a) a.a(parcel, b2, cc.b.a.CREATOR);
                    break;
                case 3:
                    hashSet.add(3);
                    bVar = (cc.b.C0007b) a.a(parcel, b2, cc.b.C0007b.CREATOR);
                    break;
                case 4:
                    i = a.f(parcel, b2);
                    hashSet.add(4);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new cc.b(hashSet, i2, aVar, bVar, i);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: aa */
    public cc.b[] newArray(int i) {
        return new cc.b[i];
    }
}
