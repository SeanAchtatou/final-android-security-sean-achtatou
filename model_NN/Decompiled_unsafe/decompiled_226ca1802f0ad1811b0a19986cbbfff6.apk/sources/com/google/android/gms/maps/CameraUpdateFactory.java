package com.google.android.gms.maps;

import com.google.android.gms.internal.s;
import com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate;

public final class CameraUpdateFactory {
    private static ICameraUpdateFactoryDelegate fW;

    static void a(ICameraUpdateFactoryDelegate iCameraUpdateFactoryDelegate) {
        if (fW == null) {
            fW = (ICameraUpdateFactoryDelegate) s.d(iCameraUpdateFactoryDelegate);
        }
    }
}
