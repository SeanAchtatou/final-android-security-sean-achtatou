package com.google.android.gms.internal;

import android.os.Parcel;
import com.actionbarsherlock.R;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ae;
import com.google.android.gms.plus.model.people.Person;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class cc extends ae implements SafeParcelable, Person {
    public static final cd CREATOR = new cd();
    private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
    private final int ab;
    private String cl;
    private final Set<Integer> iD;
    private String ie;
    private String jE;
    private a jF;
    private String jG;
    private String jH;
    private int jI;
    private b jJ;
    private String jK;
    private int jL;
    private c jM;
    private boolean jN;
    private String jO;
    private d jP;
    private String jQ;
    private int jR;
    private List<f> jS;
    private List<g> jT;
    private int jU;
    private int jV;
    private String jW;
    private List<h> jX;
    private boolean jY;
    private String jh;

    public final class a extends ae implements SafeParcelable, Person.AgeRange {
        public static final ce CREATOR = new ce();
        private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
        private final int ab;
        private final Set<Integer> iD;
        private int jZ;
        private int ka;

        static {
            iC.put("max", ae.a.c("max", 2));
            iC.put("min", ae.a.c("min", 3));
        }

        public a() {
            this.ab = 1;
            this.iD = new HashSet();
        }

        a(Set<Integer> set, int i, int i2, int i3) {
            this.iD = set;
            this.ab = i;
            this.jZ = i2;
            this.ka = i3;
        }

        public HashMap<String, ae.a<?, ?>> T() {
            return iC;
        }

        /* access modifiers changed from: protected */
        public boolean a(ae.a aVar) {
            return this.iD.contains(Integer.valueOf(aVar.aa()));
        }

        /* access modifiers changed from: protected */
        public Object b(ae.a aVar) {
            switch (aVar.aa()) {
                case 2:
                    return Integer.valueOf(this.jZ);
                case 3:
                    return Integer.valueOf(this.ka);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> bH() {
            return this.iD;
        }

        /* renamed from: ck */
        public a freeze() {
            return this;
        }

        public int describeContents() {
            ce ceVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            a aVar = (a) obj;
            for (ae.a next : iC.values()) {
                if (a(next)) {
                    if (!aVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(aVar.b(next))) {
                        return false;
                    }
                } else if (aVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public int getMax() {
            return this.jZ;
        }

        public int getMin() {
            return this.ka;
        }

        public int hashCode() {
            int i = 0;
            Iterator<ae.a<?, ?>> it = iC.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                ae.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.aa();
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public int i() {
            return this.ab;
        }

        /* access modifiers changed from: protected */
        public Object m(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean n(String str) {
            return false;
        }

        public void writeToParcel(Parcel parcel, int i) {
            ce ceVar = CREATOR;
            ce.a(this, parcel, i);
        }
    }

    public final class b extends ae implements SafeParcelable, Person.Cover {
        public static final cf CREATOR = new cf();
        private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
        private final int ab;
        private final Set<Integer> iD;
        private a kb;
        private C0007b kc;
        private int kd;

        public final class a extends ae implements SafeParcelable, Person.Cover.CoverInfo {
            public static final cg CREATOR = new cg();
            private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
            private final int ab;
            private final Set<Integer> iD;
            private int ke;
            private int kf;

            static {
                iC.put("leftImageOffset", ae.a.c("leftImageOffset", 2));
                iC.put("topImageOffset", ae.a.c("topImageOffset", 3));
            }

            public a() {
                this.ab = 1;
                this.iD = new HashSet();
            }

            a(Set<Integer> set, int i, int i2, int i3) {
                this.iD = set;
                this.ab = i;
                this.ke = i2;
                this.kf = i3;
            }

            public HashMap<String, ae.a<?, ?>> T() {
                return iC;
            }

            /* access modifiers changed from: protected */
            public boolean a(ae.a aVar) {
                return this.iD.contains(Integer.valueOf(aVar.aa()));
            }

            /* access modifiers changed from: protected */
            public Object b(ae.a aVar) {
                switch (aVar.aa()) {
                    case 2:
                        return Integer.valueOf(this.ke);
                    case 3:
                        return Integer.valueOf(this.kf);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
                }
            }

            /* access modifiers changed from: package-private */
            public Set<Integer> bH() {
                return this.iD;
            }

            /* renamed from: co */
            public a freeze() {
                return this;
            }

            public int describeContents() {
                cg cgVar = CREATOR;
                return 0;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof a)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                a aVar = (a) obj;
                for (ae.a next : iC.values()) {
                    if (a(next)) {
                        if (!aVar.a(next)) {
                            return false;
                        }
                        if (!b(next).equals(aVar.b(next))) {
                            return false;
                        }
                    } else if (aVar.a(next)) {
                        return false;
                    }
                }
                return true;
            }

            public int getLeftImageOffset() {
                return this.ke;
            }

            public int getTopImageOffset() {
                return this.kf;
            }

            public int hashCode() {
                int i = 0;
                Iterator<ae.a<?, ?>> it = iC.values().iterator();
                while (true) {
                    int i2 = i;
                    if (!it.hasNext()) {
                        return i2;
                    }
                    ae.a next = it.next();
                    if (a(next)) {
                        i = b(next).hashCode() + i2 + next.aa();
                    } else {
                        i = i2;
                    }
                }
            }

            /* access modifiers changed from: package-private */
            public int i() {
                return this.ab;
            }

            /* access modifiers changed from: protected */
            public Object m(String str) {
                return null;
            }

            /* access modifiers changed from: protected */
            public boolean n(String str) {
                return false;
            }

            public void writeToParcel(Parcel parcel, int i) {
                cg cgVar = CREATOR;
                cg.a(this, parcel, i);
            }
        }

        /* renamed from: com.google.android.gms.internal.cc$b$b  reason: collision with other inner class name */
        public final class C0007b extends ae implements SafeParcelable, Person.Cover.CoverPhoto {
            public static final ch CREATOR = new ch();
            private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
            private final int ab;
            private int hL;
            private int hM;
            private final Set<Integer> iD;
            private String ie;

            static {
                iC.put("height", ae.a.c("height", 2));
                iC.put("url", ae.a.f("url", 3));
                iC.put("width", ae.a.c("width", 4));
            }

            public C0007b() {
                this.ab = 1;
                this.iD = new HashSet();
            }

            C0007b(Set<Integer> set, int i, int i2, String str, int i3) {
                this.iD = set;
                this.ab = i;
                this.hM = i2;
                this.ie = str;
                this.hL = i3;
            }

            public HashMap<String, ae.a<?, ?>> T() {
                return iC;
            }

            /* access modifiers changed from: protected */
            public boolean a(ae.a aVar) {
                return this.iD.contains(Integer.valueOf(aVar.aa()));
            }

            /* access modifiers changed from: protected */
            public Object b(ae.a aVar) {
                switch (aVar.aa()) {
                    case 2:
                        return Integer.valueOf(this.hM);
                    case 3:
                        return this.ie;
                    case 4:
                        return Integer.valueOf(this.hL);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
                }
            }

            /* access modifiers changed from: package-private */
            public Set<Integer> bH() {
                return this.iD;
            }

            /* renamed from: cp */
            public C0007b freeze() {
                return this;
            }

            public int describeContents() {
                ch chVar = CREATOR;
                return 0;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof C0007b)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                C0007b bVar = (C0007b) obj;
                for (ae.a next : iC.values()) {
                    if (a(next)) {
                        if (!bVar.a(next)) {
                            return false;
                        }
                        if (!b(next).equals(bVar.b(next))) {
                            return false;
                        }
                    } else if (bVar.a(next)) {
                        return false;
                    }
                }
                return true;
            }

            public int getHeight() {
                return this.hM;
            }

            public String getUrl() {
                return this.ie;
            }

            public int getWidth() {
                return this.hL;
            }

            public int hashCode() {
                int i = 0;
                Iterator<ae.a<?, ?>> it = iC.values().iterator();
                while (true) {
                    int i2 = i;
                    if (!it.hasNext()) {
                        return i2;
                    }
                    ae.a next = it.next();
                    if (a(next)) {
                        i = b(next).hashCode() + i2 + next.aa();
                    } else {
                        i = i2;
                    }
                }
            }

            /* access modifiers changed from: package-private */
            public int i() {
                return this.ab;
            }

            /* access modifiers changed from: protected */
            public Object m(String str) {
                return null;
            }

            /* access modifiers changed from: protected */
            public boolean n(String str) {
                return false;
            }

            public void writeToParcel(Parcel parcel, int i) {
                ch chVar = CREATOR;
                ch.a(this, parcel, i);
            }
        }

        static {
            iC.put("coverInfo", ae.a.a("coverInfo", 2, a.class));
            iC.put("coverPhoto", ae.a.a("coverPhoto", 3, C0007b.class));
            iC.put("layout", ae.a.a("layout", 4, new ab().b("banner", 0), false));
        }

        public b() {
            this.ab = 1;
            this.iD = new HashSet();
        }

        b(Set<Integer> set, int i, a aVar, C0007b bVar, int i2) {
            this.iD = set;
            this.ab = i;
            this.kb = aVar;
            this.kc = bVar;
            this.kd = i2;
        }

        public HashMap<String, ae.a<?, ?>> T() {
            return iC;
        }

        /* access modifiers changed from: protected */
        public boolean a(ae.a aVar) {
            return this.iD.contains(Integer.valueOf(aVar.aa()));
        }

        /* access modifiers changed from: protected */
        public Object b(ae.a aVar) {
            switch (aVar.aa()) {
                case 2:
                    return this.kb;
                case 3:
                    return this.kc;
                case 4:
                    return Integer.valueOf(this.kd);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> bH() {
            return this.iD;
        }

        /* access modifiers changed from: package-private */
        public a cl() {
            return this.kb;
        }

        /* access modifiers changed from: package-private */
        public C0007b cm() {
            return this.kc;
        }

        /* renamed from: cn */
        public b freeze() {
            return this;
        }

        public int describeContents() {
            cf cfVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            b bVar = (b) obj;
            for (ae.a next : iC.values()) {
                if (a(next)) {
                    if (!bVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(bVar.b(next))) {
                        return false;
                    }
                } else if (bVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public int getLayout() {
            return this.kd;
        }

        public int hashCode() {
            int i = 0;
            Iterator<ae.a<?, ?>> it = iC.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                ae.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.aa();
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public int i() {
            return this.ab;
        }

        /* access modifiers changed from: protected */
        public Object m(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean n(String str) {
            return false;
        }

        public void writeToParcel(Parcel parcel, int i) {
            cf cfVar = CREATOR;
            cf.a(this, parcel, i);
        }
    }

    public final class c extends ae implements SafeParcelable, Person.Image {
        public static final ci CREATOR = new ci();
        private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
        private final int ab;
        private final Set<Integer> iD;
        private String ie;

        static {
            iC.put("url", ae.a.f("url", 2));
        }

        public c() {
            this.ab = 1;
            this.iD = new HashSet();
        }

        c(Set<Integer> set, int i, String str) {
            this.iD = set;
            this.ab = i;
            this.ie = str;
        }

        public HashMap<String, ae.a<?, ?>> T() {
            return iC;
        }

        /* access modifiers changed from: protected */
        public boolean a(ae.a aVar) {
            return this.iD.contains(Integer.valueOf(aVar.aa()));
        }

        /* access modifiers changed from: protected */
        public Object b(ae.a aVar) {
            switch (aVar.aa()) {
                case 2:
                    break;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
            }
            return this.ie;
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> bH() {
            return this.iD;
        }

        /* renamed from: cq */
        public c freeze() {
            return this;
        }

        public int describeContents() {
            ci ciVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof c)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            c cVar = (c) obj;
            for (ae.a next : iC.values()) {
                if (a(next)) {
                    if (!cVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(cVar.b(next))) {
                        return false;
                    }
                } else if (cVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public String getUrl() {
            return this.ie;
        }

        public int hashCode() {
            int i = 0;
            Iterator<ae.a<?, ?>> it = iC.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                ae.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.aa();
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public int i() {
            return this.ab;
        }

        /* access modifiers changed from: protected */
        public Object m(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean n(String str) {
            return false;
        }

        public void writeToParcel(Parcel parcel, int i) {
            ci ciVar = CREATOR;
            ci.a(this, parcel, i);
        }
    }

    public final class d extends ae implements SafeParcelable, Person.Name {
        public static final cj CREATOR = new cj();
        private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
        private final int ab;
        private final Set<Integer> iD;
        private String jc;
        private String jf;
        private String kg;
        private String kh;
        private String ki;
        private String kj;

        static {
            iC.put("familyName", ae.a.f("familyName", 2));
            iC.put("formatted", ae.a.f("formatted", 3));
            iC.put("givenName", ae.a.f("givenName", 4));
            iC.put("honorificPrefix", ae.a.f("honorificPrefix", 5));
            iC.put("honorificSuffix", ae.a.f("honorificSuffix", 6));
            iC.put("middleName", ae.a.f("middleName", 7));
        }

        public d() {
            this.ab = 1;
            this.iD = new HashSet();
        }

        d(Set<Integer> set, int i, String str, String str2, String str3, String str4, String str5, String str6) {
            this.iD = set;
            this.ab = i;
            this.jc = str;
            this.kg = str2;
            this.jf = str3;
            this.kh = str4;
            this.ki = str5;
            this.kj = str6;
        }

        public HashMap<String, ae.a<?, ?>> T() {
            return iC;
        }

        /* access modifiers changed from: protected */
        public boolean a(ae.a aVar) {
            return this.iD.contains(Integer.valueOf(aVar.aa()));
        }

        /* access modifiers changed from: protected */
        public Object b(ae.a aVar) {
            switch (aVar.aa()) {
                case 2:
                    return this.jc;
                case 3:
                    return this.kg;
                case 4:
                    return this.jf;
                case 5:
                    return this.kh;
                case 6:
                    return this.ki;
                case 7:
                    return this.kj;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> bH() {
            return this.iD;
        }

        /* renamed from: cr */
        public d freeze() {
            return this;
        }

        public int describeContents() {
            cj cjVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof d)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            d dVar = (d) obj;
            for (ae.a next : iC.values()) {
                if (a(next)) {
                    if (!dVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(dVar.b(next))) {
                        return false;
                    }
                } else if (dVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public String getFamilyName() {
            return this.jc;
        }

        public String getFormatted() {
            return this.kg;
        }

        public String getGivenName() {
            return this.jf;
        }

        public String getHonorificPrefix() {
            return this.kh;
        }

        public String getHonorificSuffix() {
            return this.ki;
        }

        public String getMiddleName() {
            return this.kj;
        }

        public int hashCode() {
            int i = 0;
            Iterator<ae.a<?, ?>> it = iC.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                ae.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.aa();
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public int i() {
            return this.ab;
        }

        /* access modifiers changed from: protected */
        public Object m(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean n(String str) {
            return false;
        }

        public void writeToParcel(Parcel parcel, int i) {
            cj cjVar = CREATOR;
            cj.a(this, parcel, i);
        }
    }

    public final class f extends ae implements SafeParcelable, Person.Organizations {
        public static final ck CREATOR = new ck();
        private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
        private int aJ;
        private final int ab;
        private String di;
        private String hs;
        private final Set<Integer> iD;
        private String jb;
        private String js;
        private String kk;
        private String kl;
        private boolean km;
        private String mName;

        static {
            iC.put("department", ae.a.f("department", 2));
            iC.put("description", ae.a.f("description", 3));
            iC.put("endDate", ae.a.f("endDate", 4));
            iC.put("location", ae.a.f("location", 5));
            iC.put("name", ae.a.f("name", 6));
            iC.put("primary", ae.a.e("primary", 7));
            iC.put("startDate", ae.a.f("startDate", 8));
            iC.put("title", ae.a.f("title", 9));
            iC.put("type", ae.a.a("type", 10, new ab().b("work", 0).b("school", 1), false));
        }

        public f() {
            this.ab = 1;
            this.iD = new HashSet();
        }

        f(Set<Integer> set, int i, String str, String str2, String str3, String str4, String str5, boolean z, String str6, String str7, int i2) {
            this.iD = set;
            this.ab = i;
            this.kk = str;
            this.di = str2;
            this.jb = str3;
            this.kl = str4;
            this.mName = str5;
            this.km = z;
            this.js = str6;
            this.hs = str7;
            this.aJ = i2;
        }

        public HashMap<String, ae.a<?, ?>> T() {
            return iC;
        }

        /* access modifiers changed from: protected */
        public boolean a(ae.a aVar) {
            return this.iD.contains(Integer.valueOf(aVar.aa()));
        }

        /* access modifiers changed from: protected */
        public Object b(ae.a aVar) {
            switch (aVar.aa()) {
                case 2:
                    return this.kk;
                case 3:
                    return this.di;
                case 4:
                    return this.jb;
                case 5:
                    return this.kl;
                case 6:
                    return this.mName;
                case 7:
                    return Boolean.valueOf(this.km);
                case 8:
                    return this.js;
                case 9:
                    return this.hs;
                case 10:
                    return Integer.valueOf(this.aJ);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> bH() {
            return this.iD;
        }

        /* renamed from: cs */
        public f freeze() {
            return this;
        }

        public int describeContents() {
            ck ckVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof f)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            f fVar = (f) obj;
            for (ae.a next : iC.values()) {
                if (a(next)) {
                    if (!fVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(fVar.b(next))) {
                        return false;
                    }
                } else if (fVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public String getDepartment() {
            return this.kk;
        }

        public String getDescription() {
            return this.di;
        }

        public String getEndDate() {
            return this.jb;
        }

        public String getLocation() {
            return this.kl;
        }

        public String getName() {
            return this.mName;
        }

        public String getStartDate() {
            return this.js;
        }

        public String getTitle() {
            return this.hs;
        }

        public int getType() {
            return this.aJ;
        }

        public int hashCode() {
            int i = 0;
            Iterator<ae.a<?, ?>> it = iC.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                ae.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.aa();
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public int i() {
            return this.ab;
        }

        public boolean isPrimary() {
            return this.km;
        }

        /* access modifiers changed from: protected */
        public Object m(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean n(String str) {
            return false;
        }

        public void writeToParcel(Parcel parcel, int i) {
            ck ckVar = CREATOR;
            ck.a(this, parcel, i);
        }
    }

    public final class g extends ae implements SafeParcelable, Person.PlacesLived {
        public static final cl CREATOR = new cl();
        private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
        private final int ab;
        private final Set<Integer> iD;
        private boolean km;
        private String mValue;

        static {
            iC.put("primary", ae.a.e("primary", 2));
            iC.put("value", ae.a.f("value", 3));
        }

        public g() {
            this.ab = 1;
            this.iD = new HashSet();
        }

        g(Set<Integer> set, int i, boolean z, String str) {
            this.iD = set;
            this.ab = i;
            this.km = z;
            this.mValue = str;
        }

        public HashMap<String, ae.a<?, ?>> T() {
            return iC;
        }

        /* access modifiers changed from: protected */
        public boolean a(ae.a aVar) {
            return this.iD.contains(Integer.valueOf(aVar.aa()));
        }

        /* access modifiers changed from: protected */
        public Object b(ae.a aVar) {
            switch (aVar.aa()) {
                case 2:
                    return Boolean.valueOf(this.km);
                case 3:
                    return this.mValue;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> bH() {
            return this.iD;
        }

        /* renamed from: ct */
        public g freeze() {
            return this;
        }

        public int describeContents() {
            cl clVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof g)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            g gVar = (g) obj;
            for (ae.a next : iC.values()) {
                if (a(next)) {
                    if (!gVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(gVar.b(next))) {
                        return false;
                    }
                } else if (gVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public String getValue() {
            return this.mValue;
        }

        public int hashCode() {
            int i = 0;
            Iterator<ae.a<?, ?>> it = iC.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                ae.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.aa();
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public int i() {
            return this.ab;
        }

        public boolean isPrimary() {
            return this.km;
        }

        /* access modifiers changed from: protected */
        public Object m(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean n(String str) {
            return false;
        }

        public void writeToParcel(Parcel parcel, int i) {
            cl clVar = CREATOR;
            cl.a(this, parcel, i);
        }
    }

    public final class h extends ae implements SafeParcelable, Person.Urls {
        public static final cm CREATOR = new cm();
        private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
        private int aJ;
        private final int ab;
        private final Set<Integer> iD;
        private String kn;
        private final int ko;
        private String mValue;

        static {
            iC.put("label", ae.a.f("label", 5));
            iC.put("type", ae.a.a("type", 6, new ab().b("home", 0).b("work", 1).b("blog", 2).b("profile", 3).b("other", 4).b("otherProfile", 5).b("contributor", 6).b("website", 7), false));
            iC.put("value", ae.a.f("value", 4));
        }

        public h() {
            this.ko = 4;
            this.ab = 2;
            this.iD = new HashSet();
        }

        h(Set<Integer> set, int i, String str, int i2, String str2, int i3) {
            this.ko = 4;
            this.iD = set;
            this.ab = i;
            this.kn = str;
            this.aJ = i2;
            this.mValue = str2;
        }

        public HashMap<String, ae.a<?, ?>> T() {
            return iC;
        }

        /* access modifiers changed from: protected */
        public boolean a(ae.a aVar) {
            return this.iD.contains(Integer.valueOf(aVar.aa()));
        }

        /* access modifiers changed from: protected */
        public Object b(ae.a aVar) {
            switch (aVar.aa()) {
                case 4:
                    return this.mValue;
                case 5:
                    return this.kn;
                case 6:
                    return Integer.valueOf(this.aJ);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
            }
        }

        /* access modifiers changed from: package-private */
        public Set<Integer> bH() {
            return this.iD;
        }

        @Deprecated
        public int cu() {
            return 4;
        }

        /* renamed from: cv */
        public h freeze() {
            return this;
        }

        public int describeContents() {
            cm cmVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof h)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            h hVar = (h) obj;
            for (ae.a next : iC.values()) {
                if (a(next)) {
                    if (!hVar.a(next)) {
                        return false;
                    }
                    if (!b(next).equals(hVar.b(next))) {
                        return false;
                    }
                } else if (hVar.a(next)) {
                    return false;
                }
            }
            return true;
        }

        public String getLabel() {
            return this.kn;
        }

        public int getType() {
            return this.aJ;
        }

        public String getValue() {
            return this.mValue;
        }

        public int hashCode() {
            int i = 0;
            Iterator<ae.a<?, ?>> it = iC.values().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    return i2;
                }
                ae.a next = it.next();
                if (a(next)) {
                    i = b(next).hashCode() + i2 + next.aa();
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public int i() {
            return this.ab;
        }

        /* access modifiers changed from: protected */
        public Object m(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean n(String str) {
            return false;
        }

        public void writeToParcel(Parcel parcel, int i) {
            cm cmVar = CREATOR;
            cm.a(this, parcel, i);
        }
    }

    static {
        iC.put("aboutMe", ae.a.f("aboutMe", 2));
        iC.put("ageRange", ae.a.a("ageRange", 3, a.class));
        iC.put("birthday", ae.a.f("birthday", 4));
        iC.put("braggingRights", ae.a.f("braggingRights", 5));
        iC.put("circledByCount", ae.a.c("circledByCount", 6));
        iC.put("cover", ae.a.a("cover", 7, b.class));
        iC.put("currentLocation", ae.a.f("currentLocation", 8));
        iC.put("displayName", ae.a.f("displayName", 9));
        iC.put("gender", ae.a.a("gender", 12, new ab().b("male", 0).b("female", 1).b("other", 2), false));
        iC.put("id", ae.a.f("id", 14));
        iC.put("image", ae.a.a("image", 15, c.class));
        iC.put("isPlusUser", ae.a.e("isPlusUser", 16));
        iC.put("language", ae.a.f("language", 18));
        iC.put("name", ae.a.a("name", 19, d.class));
        iC.put("nickname", ae.a.f("nickname", 20));
        iC.put("objectType", ae.a.a("objectType", 21, new ab().b("person", 0).b("page", 1), false));
        iC.put("organizations", ae.a.b("organizations", 22, f.class));
        iC.put("placesLived", ae.a.b("placesLived", 23, g.class));
        iC.put("plusOneCount", ae.a.c("plusOneCount", 24));
        iC.put("relationshipStatus", ae.a.a("relationshipStatus", 25, new ab().b("single", 0).b("in_a_relationship", 1).b("engaged", 2).b("married", 3).b("its_complicated", 4).b("open_relationship", 5).b("widowed", 6).b("in_domestic_partnership", 7).b("in_civil_union", 8), false));
        iC.put("tagline", ae.a.f("tagline", 26));
        iC.put("url", ae.a.f("url", 27));
        iC.put("urls", ae.a.b("urls", 28, h.class));
        iC.put("verified", ae.a.e("verified", 29));
    }

    public cc() {
        this.ab = 2;
        this.iD = new HashSet();
    }

    cc(Set<Integer> set, int i, String str, a aVar, String str2, String str3, int i2, b bVar, String str4, String str5, int i3, String str6, c cVar, boolean z, String str7, d dVar, String str8, int i4, List<f> list, List<g> list2, int i5, int i6, String str9, String str10, List<h> list3, boolean z2) {
        this.iD = set;
        this.ab = i;
        this.jE = str;
        this.jF = aVar;
        this.jG = str2;
        this.jH = str3;
        this.jI = i2;
        this.jJ = bVar;
        this.jK = str4;
        this.cl = str5;
        this.jL = i3;
        this.jh = str6;
        this.jM = cVar;
        this.jN = z;
        this.jO = str7;
        this.jP = dVar;
        this.jQ = str8;
        this.jR = i4;
        this.jS = list;
        this.jT = list2;
        this.jU = i5;
        this.jV = i6;
        this.jW = str9;
        this.ie = str10;
        this.jX = list3;
        this.jY = z2;
    }

    public HashMap<String, ae.a<?, ?>> T() {
        return iC;
    }

    /* access modifiers changed from: protected */
    public boolean a(ae.a aVar) {
        return this.iD.contains(Integer.valueOf(aVar.aa()));
    }

    /* access modifiers changed from: protected */
    public Object b(ae.a aVar) {
        switch (aVar.aa()) {
            case 2:
                return this.jE;
            case 3:
                return this.jF;
            case 4:
                return this.jG;
            case 5:
                return this.jH;
            case 6:
                return Integer.valueOf(this.jI);
            case 7:
                return this.jJ;
            case 8:
                return this.jK;
            case 9:
                return this.cl;
            case 10:
            case 11:
            case 13:
            case 17:
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
            case 12:
                return Integer.valueOf(this.jL);
            case 14:
                return this.jh;
            case 15:
                return this.jM;
            case 16:
                return Boolean.valueOf(this.jN);
            case 18:
                return this.jO;
            case R.styleable.SherlockTheme_buttonStyleSmall:
                return this.jP;
            case R.styleable.SherlockTheme_selectableItemBackground:
                return this.jQ;
            case R.styleable.SherlockTheme_windowContentOverlay:
                return Integer.valueOf(this.jR);
            case R.styleable.SherlockTheme_textAppearanceLargePopupMenu:
                return this.jS;
            case R.styleable.SherlockTheme_textAppearanceSmallPopupMenu:
                return this.jT;
            case R.styleable.SherlockTheme_textAppearanceSmall:
                return Integer.valueOf(this.jU);
            case R.styleable.SherlockTheme_textColorPrimary:
                return Integer.valueOf(this.jV);
            case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                return this.jW;
            case R.styleable.SherlockTheme_textColorPrimaryInverse:
                return this.ie;
            case R.styleable.SherlockTheme_spinnerItemStyle:
                return this.jX;
            case R.styleable.SherlockTheme_spinnerDropDownItemStyle:
                return Boolean.valueOf(this.jY);
        }
    }

    /* access modifiers changed from: package-private */
    public Set<Integer> bH() {
        return this.iD;
    }

    /* access modifiers changed from: package-private */
    public a cc() {
        return this.jF;
    }

    /* access modifiers changed from: package-private */
    public b cd() {
        return this.jJ;
    }

    /* access modifiers changed from: package-private */
    public c ce() {
        return this.jM;
    }

    /* access modifiers changed from: package-private */
    public d cf() {
        return this.jP;
    }

    /* access modifiers changed from: package-private */
    public List<f> cg() {
        return this.jS;
    }

    /* access modifiers changed from: package-private */
    public List<g> ch() {
        return this.jT;
    }

    /* access modifiers changed from: package-private */
    public List<h> ci() {
        return this.jX;
    }

    /* renamed from: cj */
    public cc freeze() {
        return this;
    }

    public int describeContents() {
        cd cdVar = CREATOR;
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof cc)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        cc ccVar = (cc) obj;
        for (ae.a next : iC.values()) {
            if (a(next)) {
                if (!ccVar.a(next)) {
                    return false;
                }
                if (!b(next).equals(ccVar.b(next))) {
                    return false;
                }
            } else if (ccVar.a(next)) {
                return false;
            }
        }
        return true;
    }

    public String getAboutMe() {
        return this.jE;
    }

    public String getBirthday() {
        return this.jG;
    }

    public String getBraggingRights() {
        return this.jH;
    }

    public int getCircledByCount() {
        return this.jI;
    }

    public String getCurrentLocation() {
        return this.jK;
    }

    public String getDisplayName() {
        return this.cl;
    }

    public int getGender() {
        return this.jL;
    }

    public String getId() {
        return this.jh;
    }

    public String getLanguage() {
        return this.jO;
    }

    public String getNickname() {
        return this.jQ;
    }

    public int getObjectType() {
        return this.jR;
    }

    public int getPlusOneCount() {
        return this.jU;
    }

    public int getRelationshipStatus() {
        return this.jV;
    }

    public String getTagline() {
        return this.jW;
    }

    public String getUrl() {
        return this.ie;
    }

    public int hashCode() {
        int i = 0;
        Iterator<ae.a<?, ?>> it = iC.values().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            ae.a next = it.next();
            if (a(next)) {
                i = b(next).hashCode() + i2 + next.aa();
            } else {
                i = i2;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    public boolean isPlusUser() {
        return this.jN;
    }

    public boolean isVerified() {
        return this.jY;
    }

    /* access modifiers changed from: protected */
    public Object m(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean n(String str) {
        return false;
    }

    public void writeToParcel(Parcel parcel, int i) {
        cd cdVar = CREATOR;
        cd.a(this, parcel, i);
    }
}
