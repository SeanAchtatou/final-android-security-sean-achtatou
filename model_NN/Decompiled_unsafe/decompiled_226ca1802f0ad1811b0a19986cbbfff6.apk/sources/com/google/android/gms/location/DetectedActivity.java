package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class DetectedActivity implements SafeParcelable {
    public static final DetectedActivityCreator CREATOR = new DetectedActivityCreator();
    private final int ab;
    int fs;
    int ft;

    public DetectedActivity(int i, int i2, int i3) {
        this.ab = i;
        this.fs = i2;
        this.ft = i3;
    }

    private int L(int i) {
        if (i > 5) {
            return 4;
        }
        return i;
    }

    public int describeContents() {
        return 0;
    }

    public int getType() {
        return L(this.fs);
    }

    public int i() {
        return this.ab;
    }

    public String toString() {
        return "DetectedActivity [type=" + getType() + ", confidence=" + this.ft + "]";
    }

    public void writeToParcel(Parcel parcel, int i) {
        DetectedActivityCreator.a(this, parcel, i);
    }
}
