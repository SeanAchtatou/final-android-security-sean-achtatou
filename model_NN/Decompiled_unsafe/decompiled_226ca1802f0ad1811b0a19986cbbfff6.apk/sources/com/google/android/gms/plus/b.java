package com.google.android.gms.plus;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public class b implements Parcelable.Creator<a> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
     arg types: [android.os.Parcel, int, java.lang.String[], int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void */
    static void a(a aVar, Parcel parcel, int i) {
        int d = com.google.android.gms.common.internal.safeparcel.b.d(parcel);
        com.google.android.gms.common.internal.safeparcel.b.a(parcel, 1, aVar.getAccountName(), false);
        com.google.android.gms.common.internal.safeparcel.b.c(parcel, 1000, aVar.i());
        com.google.android.gms.common.internal.safeparcel.b.a(parcel, 2, aVar.by(), false);
        com.google.android.gms.common.internal.safeparcel.b.a(parcel, 3, aVar.bz(), false);
        com.google.android.gms.common.internal.safeparcel.b.a(parcel, 4, aVar.bA(), false);
        com.google.android.gms.common.internal.safeparcel.b.a(parcel, 5, aVar.bB(), false);
        com.google.android.gms.common.internal.safeparcel.b.a(parcel, 6, aVar.bC(), false);
        com.google.android.gms.common.internal.safeparcel.b.a(parcel, 7, aVar.bD(), false);
        com.google.android.gms.common.internal.safeparcel.b.C(parcel, d);
    }

    /* renamed from: U */
    public a[] newArray(int i) {
        return new a[i];
    }

    /* renamed from: u */
    public a createFromParcel(Parcel parcel) {
        String str = null;
        int c2 = a.c(parcel);
        int i = 0;
        String str2 = null;
        String str3 = null;
        String[] strArr = null;
        String[] strArr2 = null;
        String[] strArr3 = null;
        String str4 = null;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    str4 = a.l(parcel, b2);
                    break;
                case 2:
                    strArr3 = a.w(parcel, b2);
                    break;
                case 3:
                    strArr2 = a.w(parcel, b2);
                    break;
                case 4:
                    strArr = a.w(parcel, b2);
                    break;
                case 5:
                    str3 = a.l(parcel, b2);
                    break;
                case 6:
                    str2 = a.l(parcel, b2);
                    break;
                case 7:
                    str = a.l(parcel, b2);
                    break;
                case 1000:
                    i = a.f(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new a(i, str4, strArr3, strArr2, strArr, str3, str2, str);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }
}
