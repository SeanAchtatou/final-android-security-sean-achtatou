package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.dynamic.b;
import com.google.android.gms.maps.internal.q;

public final class GroundOverlayOptions implements SafeParcelable {
    public static final GroundOverlayOptionsCreator CREATOR = new GroundOverlayOptionsCreator();
    private final int ab;
    private float gU;
    private float hb;
    private boolean hc;
    private BitmapDescriptor he;
    private LatLng hf;
    private float hg;
    private float hh;
    private LatLngBounds hi;
    private float hj;
    private float hk;
    private float hl;

    public GroundOverlayOptions() {
        this.hc = true;
        this.hj = 0.0f;
        this.hk = 0.5f;
        this.hl = 0.5f;
        this.ab = 1;
    }

    GroundOverlayOptions(int i, IBinder iBinder, LatLng latLng, float f, float f2, LatLngBounds latLngBounds, float f3, float f4, boolean z, float f5, float f6, float f7) {
        this.hc = true;
        this.hj = 0.0f;
        this.hk = 0.5f;
        this.hl = 0.5f;
        this.ab = i;
        this.he = new BitmapDescriptor(b.a.l(iBinder));
        this.hf = latLng;
        this.hg = f;
        this.hh = f2;
        this.hi = latLngBounds;
        this.gU = f3;
        this.hb = f4;
        this.hc = z;
        this.hj = f5;
        this.hk = f6;
        this.hl = f7;
    }

    /* access modifiers changed from: package-private */
    public IBinder bp() {
        return this.he.aW().asBinder();
    }

    public int describeContents() {
        return 0;
    }

    public float getAnchorU() {
        return this.hk;
    }

    public float getAnchorV() {
        return this.hl;
    }

    public float getBearing() {
        return this.gU;
    }

    public LatLngBounds getBounds() {
        return this.hi;
    }

    public float getHeight() {
        return this.hh;
    }

    public LatLng getLocation() {
        return this.hf;
    }

    public float getTransparency() {
        return this.hj;
    }

    public float getWidth() {
        return this.hg;
    }

    public float getZIndex() {
        return this.hb;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    public boolean isVisible() {
        return this.hc;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (q.bn()) {
            c.a(this, parcel, i);
        } else {
            GroundOverlayOptionsCreator.a(this, parcel, i);
        }
    }
}
