package com.google.android.gms.internal;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;

public class cp implements Parcelable.Creator<co> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    static void a(co coVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.a(parcel, 1, coVar.getId(), false);
        b.c(parcel, 1000, coVar.i());
        b.b(parcel, 2, coVar.cB(), false);
        b.b(parcel, 3, coVar.cC(), false);
        b.a(parcel, 4, (Parcelable) coVar.cD(), i, false);
        b.a(parcel, 5, coVar.cE(), false);
        b.a(parcel, 6, coVar.cF(), false);
        b.a(parcel, 7, coVar.cG(), false);
        b.a(parcel, 8, coVar.cH(), false);
        b.a(parcel, 9, coVar.cI(), false);
        b.c(parcel, 10, coVar.cJ());
        b.C(parcel, d);
    }

    /* renamed from: I */
    public co createFromParcel(Parcel parcel) {
        int i = 0;
        Bundle bundle = null;
        int c2 = a.c(parcel);
        Bundle bundle2 = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        Uri uri = null;
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        String str4 = null;
        int i2 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    str4 = a.l(parcel, b2);
                    break;
                case 2:
                    arrayList2 = a.c(parcel, b2, x.CREATOR);
                    break;
                case 3:
                    arrayList = a.c(parcel, b2, Uri.CREATOR);
                    break;
                case 4:
                    uri = (Uri) a.a(parcel, b2, Uri.CREATOR);
                    break;
                case 5:
                    str3 = a.l(parcel, b2);
                    break;
                case 6:
                    str2 = a.l(parcel, b2);
                    break;
                case 7:
                    str = a.l(parcel, b2);
                    break;
                case 8:
                    bundle2 = a.n(parcel, b2);
                    break;
                case 9:
                    bundle = a.n(parcel, b2);
                    break;
                case 10:
                    i = a.f(parcel, b2);
                    break;
                case 1000:
                    i2 = a.f(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new co(i2, str4, arrayList2, arrayList, uri, str3, str2, str, bundle2, bundle, i);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: ai */
    public co[] newArray(int i) {
        return new co[i];
    }
}
