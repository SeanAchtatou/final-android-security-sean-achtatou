package com.google.android.gms.common.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.common.images.ImageManager;
import com.google.android.gms.internal.as;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.g;
import com.google.android.gms.internal.h;
import com.google.android.gms.internal.r;
import java.lang.ref.WeakReference;

public final class a {
    final C0000a aG;
    private int aI;
    int aJ;
    private int aK;
    private WeakReference<ImageManager.OnImageLoadedListener> aL;
    private WeakReference<ImageView> aM;
    private WeakReference<TextView> aN;
    private int aO;
    private boolean aP;
    private boolean aQ;

    /* renamed from: com.google.android.gms.common.images.a$a  reason: collision with other inner class name */
    public final class C0000a {
        public final Uri uri;

        public C0000a(Uri uri2) {
            this.uri = uri2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C0000a)) {
                return false;
            }
            return this == obj || ((C0000a) obj).hashCode() == hashCode();
        }

        public int hashCode() {
            return r.hashCode(this.uri);
        }
    }

    private f a(Drawable drawable, Drawable drawable2) {
        if (drawable == null) {
            drawable = null;
        } else if (drawable instanceof f) {
            drawable = ((f) drawable).r();
        }
        return new f(drawable, drawable2);
    }

    private void a(Drawable drawable, boolean z, boolean z2, boolean z3) {
        ImageManager.OnImageLoadedListener onImageLoadedListener;
        switch (this.aJ) {
            case 1:
                if (!z2 && (onImageLoadedListener = this.aL.get()) != null) {
                    onImageLoadedListener.onImageLoaded(this.aG.uri, drawable);
                    return;
                }
                return;
            case 2:
                ImageView imageView = this.aM.get();
                if (imageView != null) {
                    a(imageView, drawable, z, z2, z3);
                    return;
                }
                return;
            case 3:
                TextView textView = this.aN.get();
                if (textView != null) {
                    a(textView, this.aO, drawable, z, z2);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void a(ImageView imageView, Drawable drawable, boolean z, boolean z2, boolean z3) {
        boolean z4 = !z2 && !z3;
        if (z4 && (imageView instanceof g)) {
            int t = ((g) imageView).t();
            if (this.aI != 0 && t == this.aI) {
                return;
            }
        }
        boolean a2 = a(z, z2);
        Drawable a3 = a2 ? a(imageView.getDrawable(), drawable) : drawable;
        imageView.setImageDrawable(a3);
        if (imageView instanceof g) {
            g gVar = (g) imageView;
            gVar.a(z3 ? this.aG.uri : null);
            gVar.k(z4 ? this.aI : 0);
        }
        if (a2) {
            ((f) a3).startTransition(250);
        }
    }

    private void a(TextView textView, int i, Drawable drawable, boolean z, boolean z2) {
        boolean a2 = a(z, z2);
        Drawable[] compoundDrawablesRelative = as.as() ? textView.getCompoundDrawablesRelative() : textView.getCompoundDrawables();
        Drawable a3 = a2 ? a(compoundDrawablesRelative[i], drawable) : drawable;
        Drawable drawable2 = i == 0 ? a3 : compoundDrawablesRelative[0];
        Drawable drawable3 = i == 1 ? a3 : compoundDrawablesRelative[1];
        Drawable drawable4 = i == 2 ? a3 : compoundDrawablesRelative[2];
        Drawable drawable5 = i == 3 ? a3 : compoundDrawablesRelative[3];
        if (as.as()) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable2, drawable3, drawable4, drawable5);
        } else {
            textView.setCompoundDrawablesWithIntrinsicBounds(drawable2, drawable3, drawable4, drawable5);
        }
        if (a2) {
            ((f) a3).startTransition(250);
        }
    }

    private boolean a(boolean z, boolean z2) {
        return this.aP && !z2 && (!z || this.aQ);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Bitmap bitmap, boolean z) {
        h.b(bitmap);
        a(new BitmapDrawable(context.getResources(), bitmap), z, false, true);
    }

    /* access modifiers changed from: package-private */
    public void b(Context context, boolean z) {
        Drawable drawable = null;
        if (this.aI != 0) {
            drawable = context.getResources().getDrawable(this.aI);
        }
        a(drawable, z, false, false);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        return this == obj || ((a) obj).hashCode() == hashCode();
    }

    public int hashCode() {
        return this.aK;
    }
}
