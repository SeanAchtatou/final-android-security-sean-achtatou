package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.internal.q;
import java.util.ArrayList;
import java.util.List;

public final class PolylineOptions implements SafeParcelable {
    public static final PolylineOptionsCreator CREATOR = new PolylineOptionsCreator();
    private int P;
    private final int ab;
    private final List<LatLng> hB;
    private boolean hD;
    private float hb;
    private boolean hc;
    private float hg;

    public PolylineOptions() {
        this.hg = 10.0f;
        this.P = -16777216;
        this.hb = 0.0f;
        this.hc = true;
        this.hD = false;
        this.ab = 1;
        this.hB = new ArrayList();
    }

    PolylineOptions(int i, List list, float f, int i2, float f2, boolean z, boolean z2) {
        this.hg = 10.0f;
        this.P = -16777216;
        this.hb = 0.0f;
        this.hc = true;
        this.hD = false;
        this.ab = i;
        this.hB = list;
        this.hg = f;
        this.P = i2;
        this.hb = f2;
        this.hc = z;
        this.hD = z2;
    }

    public int describeContents() {
        return 0;
    }

    public int getColor() {
        return this.P;
    }

    public List<LatLng> getPoints() {
        return this.hB;
    }

    public float getWidth() {
        return this.hg;
    }

    public float getZIndex() {
        return this.hb;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    public boolean isGeodesic() {
        return this.hD;
    }

    public boolean isVisible() {
        return this.hc;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (q.bn()) {
            h.a(this, parcel, i);
        } else {
            PolylineOptionsCreator.a(this, parcel, i);
        }
    }
}
