package com.google.android.gms.internal;

import android.text.TextUtils;
import com.actionbarsherlock.R;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class aq {
    private static final Pattern cN = Pattern.compile("\\\\.");
    private static final Pattern cO = Pattern.compile("[\\\\\"/\b\f\n\r\t]");

    public static String r(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        Matcher matcher = cO.matcher(str);
        StringBuffer stringBuffer = null;
        while (matcher.find()) {
            if (stringBuffer == null) {
                stringBuffer = new StringBuffer();
            }
            switch (matcher.group().charAt(0)) {
                case 8:
                    matcher.appendReplacement(stringBuffer, "\\\\b");
                    break;
                case 9:
                    matcher.appendReplacement(stringBuffer, "\\\\t");
                    break;
                case 10:
                    matcher.appendReplacement(stringBuffer, "\\\\n");
                    break;
                case 12:
                    matcher.appendReplacement(stringBuffer, "\\\\f");
                    break;
                case 13:
                    matcher.appendReplacement(stringBuffer, "\\\\r");
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    matcher.appendReplacement(stringBuffer, "\\\\\\\"");
                    break;
                case R.styleable.SherlockTheme_textAppearanceListItemSmall:
                    matcher.appendReplacement(stringBuffer, "\\\\/");
                    break;
                case '\\':
                    matcher.appendReplacement(stringBuffer, "\\\\\\\\");
                    break;
            }
        }
        if (stringBuffer == null) {
            return str;
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }
}
