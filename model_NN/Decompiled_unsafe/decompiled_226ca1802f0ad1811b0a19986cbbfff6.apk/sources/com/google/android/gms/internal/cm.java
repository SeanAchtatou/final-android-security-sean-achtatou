package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.cc;
import java.util.HashSet;
import java.util.Set;

public class cm implements Parcelable.Creator<cc.h> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    static void a(cc.h hVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        Set<Integer> bH = hVar.bH();
        if (bH.contains(1)) {
            b.c(parcel, 1, hVar.i());
        }
        if (bH.contains(3)) {
            b.c(parcel, 3, hVar.cu());
        }
        if (bH.contains(4)) {
            b.a(parcel, 4, hVar.getValue(), true);
        }
        if (bH.contains(5)) {
            b.a(parcel, 5, hVar.getLabel(), true);
        }
        if (bH.contains(6)) {
            b.c(parcel, 6, hVar.getType());
        }
        b.C(parcel, d);
    }

    /* renamed from: H */
    public cc.h createFromParcel(Parcel parcel) {
        String str = null;
        int i = 0;
        int c2 = a.c(parcel);
        HashSet hashSet = new HashSet();
        int i2 = 0;
        String str2 = null;
        int i3 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i3 = a.f(parcel, b2);
                    hashSet.add(1);
                    break;
                case 2:
                default:
                    a.b(parcel, b2);
                    break;
                case 3:
                    i = a.f(parcel, b2);
                    hashSet.add(3);
                    break;
                case 4:
                    str = a.l(parcel, b2);
                    hashSet.add(4);
                    break;
                case 5:
                    str2 = a.l(parcel, b2);
                    hashSet.add(5);
                    break;
                case 6:
                    i2 = a.f(parcel, b2);
                    hashSet.add(6);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new cc.h(hashSet, i3, str2, i2, str, i);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    /* renamed from: ah */
    public cc.h[] newArray(int i) {
        return new cc.h[i];
    }
}
