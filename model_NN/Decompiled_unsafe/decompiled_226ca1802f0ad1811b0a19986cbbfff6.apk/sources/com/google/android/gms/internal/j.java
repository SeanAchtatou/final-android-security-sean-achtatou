package com.google.android.gms.internal;

import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public abstract class j implements SafeParcelable {
    private static final Object bo = new Object();
    private static ClassLoader bp = null;
    private static Integer bq = null;
    private boolean br = false;

    private static boolean a(Class<?> cls) {
        try {
            return "SAFE_PARCELABLE_NULL_STRING".equals(cls.getField("NULL").get(null));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public static boolean h(String str) {
        ClassLoader u = u();
        if (u == null) {
            return true;
        }
        try {
            return a(u.loadClass(str));
        } catch (Exception e) {
            return false;
        }
    }

    protected static ClassLoader u() {
        ClassLoader classLoader;
        synchronized (bo) {
            classLoader = bp;
        }
        return classLoader;
    }

    /* access modifiers changed from: protected */
    public static Integer v() {
        Integer num;
        synchronized (bo) {
            num = bq;
        }
        return num;
    }

    /* access modifiers changed from: protected */
    public boolean w() {
        return this.br;
    }
}
