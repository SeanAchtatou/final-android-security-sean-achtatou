package com.google.android.gms.common.data;

import android.database.CursorIndexOutOfBoundsException;
import android.database.CursorWindow;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.s;
import java.util.ArrayList;
import java.util.HashMap;

public final class d implements SafeParcelable {
    public static final e CREATOR = new e();
    private static final HashMap<CursorWindow, Throwable> Z = null;
    private static final Object aa = new Object();
    private static final a ai = new a(new String[0], null) {
    };
    private final int ab;
    private final String[] ac;
    Bundle ad;
    private final CursorWindow[] ae;
    private final Bundle af;
    int[] ag;
    int ah;
    boolean mClosed = false;
    private final int p;

    public class a {
        private final String[] ac;
        private final ArrayList<HashMap<String, Object>> aj;
        private final String ak;
        private final HashMap<Object, Integer> al;
        private boolean am;
        private String an;

        private a(String[] strArr, String str) {
            this.ac = (String[]) s.d(strArr);
            this.aj = new ArrayList<>();
            this.ak = str;
            this.al = new HashMap<>();
            this.am = false;
            this.an = null;
        }
    }

    d(int i, String[] strArr, CursorWindow[] cursorWindowArr, int i2, Bundle bundle) {
        this.ab = i;
        this.ac = strArr;
        this.ae = cursorWindowArr;
        this.p = i2;
        this.af = bundle;
    }

    private void a(String str, int i) {
        if (this.ad == null || !this.ad.containsKey(str)) {
            throw new IllegalArgumentException("No such column: " + str);
        } else if (isClosed()) {
            throw new IllegalArgumentException("Buffer is closed.");
        } else if (i < 0 || i >= this.ah) {
            throw new CursorIndexOutOfBoundsException(i, this.ah);
        }
    }

    public long a(String str, int i, int i2) {
        a(str, i);
        return this.ae[i2].getLong(i - this.ag[i2], this.ad.getInt(str));
    }

    public int b(String str, int i, int i2) {
        a(str, i);
        return this.ae[i2].getInt(i - this.ag[i2], this.ad.getInt(str));
    }

    public String c(String str, int i, int i2) {
        a(str, i);
        return this.ae[i2].getString(i - this.ag[i2], this.ad.getInt(str));
    }

    public boolean d(String str, int i, int i2) {
        a(str, i);
        return Long.valueOf(this.ae[i2].getLong(i - this.ag[i2], this.ad.getInt(str))).longValue() == 1;
    }

    public int describeContents() {
        return 0;
    }

    public int e(int i) {
        int i2 = 0;
        s.a(i >= 0 && i < this.ah);
        while (true) {
            if (i2 >= this.ag.length) {
                break;
            } else if (i < this.ag[i2]) {
                i2--;
                break;
            } else {
                i2++;
            }
        }
        return i2 == this.ag.length ? i2 - 1 : i2;
    }

    public Uri f(String str, int i, int i2) {
        String c2 = c(str, i, i2);
        if (c2 == null) {
            return null;
        }
        return Uri.parse(c2);
    }

    public boolean g(String str, int i, int i2) {
        a(str, i);
        return this.ae[i2].isNull(i - this.ag[i2], this.ad.getInt(str));
    }

    public int getCount() {
        return this.ah;
    }

    public int getStatusCode() {
        return this.p;
    }

    public void h() {
        this.ad = new Bundle();
        for (int i = 0; i < this.ac.length; i++) {
            this.ad.putInt(this.ac[i], i);
        }
        this.ag = new int[this.ae.length];
        int i2 = 0;
        for (int i3 = 0; i3 < this.ae.length; i3++) {
            this.ag[i3] = i2;
            i2 += this.ae[i3].getNumRows();
        }
        this.ah = i2;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    public boolean isClosed() {
        boolean z;
        synchronized (this) {
            z = this.mClosed;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public String[] j() {
        return this.ac;
    }

    /* access modifiers changed from: package-private */
    public CursorWindow[] k() {
        return this.ae;
    }

    public Bundle l() {
        return this.af;
    }

    public void writeToParcel(Parcel parcel, int i) {
        e.a(this, parcel, i);
    }
}
