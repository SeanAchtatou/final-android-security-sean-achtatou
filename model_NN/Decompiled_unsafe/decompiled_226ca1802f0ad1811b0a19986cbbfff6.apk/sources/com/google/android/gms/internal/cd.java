package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.actionbarsherlock.R;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.cc;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class cd implements Parcelable.Creator<cc> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.cc$a, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.cc$b, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.cc$c, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.cc$d, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(cc ccVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        Set<Integer> bH = ccVar.bH();
        if (bH.contains(1)) {
            b.c(parcel, 1, ccVar.i());
        }
        if (bH.contains(2)) {
            b.a(parcel, 2, ccVar.getAboutMe(), true);
        }
        if (bH.contains(3)) {
            b.a(parcel, 3, (Parcelable) ccVar.cc(), i, true);
        }
        if (bH.contains(4)) {
            b.a(parcel, 4, ccVar.getBirthday(), true);
        }
        if (bH.contains(5)) {
            b.a(parcel, 5, ccVar.getBraggingRights(), true);
        }
        if (bH.contains(6)) {
            b.c(parcel, 6, ccVar.getCircledByCount());
        }
        if (bH.contains(7)) {
            b.a(parcel, 7, (Parcelable) ccVar.cd(), i, true);
        }
        if (bH.contains(8)) {
            b.a(parcel, 8, ccVar.getCurrentLocation(), true);
        }
        if (bH.contains(9)) {
            b.a(parcel, 9, ccVar.getDisplayName(), true);
        }
        if (bH.contains(12)) {
            b.c(parcel, 12, ccVar.getGender());
        }
        if (bH.contains(14)) {
            b.a(parcel, 14, ccVar.getId(), true);
        }
        if (bH.contains(15)) {
            b.a(parcel, 15, (Parcelable) ccVar.ce(), i, true);
        }
        if (bH.contains(16)) {
            b.a(parcel, 16, ccVar.isPlusUser());
        }
        if (bH.contains(19)) {
            b.a(parcel, 19, (Parcelable) ccVar.cf(), i, true);
        }
        if (bH.contains(18)) {
            b.a(parcel, 18, ccVar.getLanguage(), true);
        }
        if (bH.contains(21)) {
            b.c(parcel, 21, ccVar.getObjectType());
        }
        if (bH.contains(20)) {
            b.a(parcel, 20, ccVar.getNickname(), true);
        }
        if (bH.contains(23)) {
            b.b(parcel, 23, ccVar.ch(), true);
        }
        if (bH.contains(22)) {
            b.b(parcel, 22, ccVar.cg(), true);
        }
        if (bH.contains(25)) {
            b.c(parcel, 25, ccVar.getRelationshipStatus());
        }
        if (bH.contains(24)) {
            b.c(parcel, 24, ccVar.getPlusOneCount());
        }
        if (bH.contains(27)) {
            b.a(parcel, 27, ccVar.getUrl(), true);
        }
        if (bH.contains(26)) {
            b.a(parcel, 26, ccVar.getTagline(), true);
        }
        if (bH.contains(29)) {
            b.a(parcel, 29, ccVar.isVerified());
        }
        if (bH.contains(28)) {
            b.b(parcel, 28, ccVar.ci(), true);
        }
        b.C(parcel, d);
    }

    /* renamed from: Y */
    public cc[] newArray(int i) {
        return new cc[i];
    }

    /* renamed from: y */
    public cc createFromParcel(Parcel parcel) {
        int c2 = a.c(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str = null;
        cc.a aVar = null;
        String str2 = null;
        String str3 = null;
        int i2 = 0;
        cc.b bVar = null;
        String str4 = null;
        String str5 = null;
        int i3 = 0;
        String str6 = null;
        cc.c cVar = null;
        boolean z = false;
        String str7 = null;
        cc.d dVar = null;
        String str8 = null;
        int i4 = 0;
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        int i5 = 0;
        int i6 = 0;
        String str9 = null;
        String str10 = null;
        ArrayList arrayList3 = null;
        boolean z2 = false;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i = a.f(parcel, b2);
                    hashSet.add(1);
                    break;
                case 2:
                    str = a.l(parcel, b2);
                    hashSet.add(2);
                    break;
                case 3:
                    hashSet.add(3);
                    aVar = (cc.a) a.a(parcel, b2, cc.a.CREATOR);
                    break;
                case 4:
                    str2 = a.l(parcel, b2);
                    hashSet.add(4);
                    break;
                case 5:
                    str3 = a.l(parcel, b2);
                    hashSet.add(5);
                    break;
                case 6:
                    i2 = a.f(parcel, b2);
                    hashSet.add(6);
                    break;
                case 7:
                    hashSet.add(7);
                    bVar = (cc.b) a.a(parcel, b2, cc.b.CREATOR);
                    break;
                case 8:
                    str4 = a.l(parcel, b2);
                    hashSet.add(8);
                    break;
                case 9:
                    str5 = a.l(parcel, b2);
                    hashSet.add(9);
                    break;
                case 10:
                case 11:
                case 13:
                case 17:
                default:
                    a.b(parcel, b2);
                    break;
                case 12:
                    i3 = a.f(parcel, b2);
                    hashSet.add(12);
                    break;
                case 14:
                    str6 = a.l(parcel, b2);
                    hashSet.add(14);
                    break;
                case 15:
                    hashSet.add(15);
                    cVar = (cc.c) a.a(parcel, b2, cc.c.CREATOR);
                    break;
                case 16:
                    z = a.c(parcel, b2);
                    hashSet.add(16);
                    break;
                case 18:
                    str7 = a.l(parcel, b2);
                    hashSet.add(18);
                    break;
                case R.styleable.SherlockTheme_buttonStyleSmall:
                    hashSet.add(19);
                    dVar = (cc.d) a.a(parcel, b2, cc.d.CREATOR);
                    break;
                case R.styleable.SherlockTheme_selectableItemBackground:
                    str8 = a.l(parcel, b2);
                    hashSet.add(20);
                    break;
                case R.styleable.SherlockTheme_windowContentOverlay:
                    i4 = a.f(parcel, b2);
                    hashSet.add(21);
                    break;
                case R.styleable.SherlockTheme_textAppearanceLargePopupMenu:
                    arrayList = a.c(parcel, b2, cc.f.CREATOR);
                    hashSet.add(22);
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmallPopupMenu:
                    arrayList2 = a.c(parcel, b2, cc.g.CREATOR);
                    hashSet.add(23);
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    i5 = a.f(parcel, b2);
                    hashSet.add(24);
                    break;
                case R.styleable.SherlockTheme_textColorPrimary:
                    i6 = a.f(parcel, b2);
                    hashSet.add(25);
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    str9 = a.l(parcel, b2);
                    hashSet.add(26);
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryInverse:
                    str10 = a.l(parcel, b2);
                    hashSet.add(27);
                    break;
                case R.styleable.SherlockTheme_spinnerItemStyle:
                    arrayList3 = a.c(parcel, b2, cc.h.CREATOR);
                    hashSet.add(28);
                    break;
                case R.styleable.SherlockTheme_spinnerDropDownItemStyle:
                    z2 = a.c(parcel, b2);
                    hashSet.add(29);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new cc(hashSet, i, str, aVar, str2, str3, i2, bVar, str4, str5, i3, str6, cVar, z, str7, dVar, str8, i4, arrayList, arrayList2, i5, i6, str9, str10, arrayList3, z2);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }
}
