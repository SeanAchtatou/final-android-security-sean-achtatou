package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;

public class PolylineOptionsCreator implements Parcelable.Creator<PolylineOptions> {
    static void a(PolylineOptions polylineOptions, Parcel parcel, int i) {
        int d = b.d(parcel);
        b.c(parcel, 1, polylineOptions.i());
        b.b(parcel, 2, polylineOptions.getPoints(), false);
        b.a(parcel, 3, polylineOptions.getWidth());
        b.c(parcel, 4, polylineOptions.getColor());
        b.a(parcel, 5, polylineOptions.getZIndex());
        b.a(parcel, 6, polylineOptions.isVisible());
        b.a(parcel, 7, polylineOptions.isGeodesic());
        b.C(parcel, d);
    }

    public PolylineOptions createFromParcel(Parcel parcel) {
        float f = 0.0f;
        boolean z = false;
        int c2 = a.c(parcel);
        ArrayList arrayList = null;
        boolean z2 = false;
        int i = 0;
        float f2 = 0.0f;
        int i2 = 0;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i2 = a.f(parcel, b2);
                    break;
                case 2:
                    arrayList = a.c(parcel, b2, LatLng.CREATOR);
                    break;
                case 3:
                    f2 = a.i(parcel, b2);
                    break;
                case 4:
                    i = a.f(parcel, b2);
                    break;
                case 5:
                    f = a.i(parcel, b2);
                    break;
                case 6:
                    z2 = a.c(parcel, b2);
                    break;
                case 7:
                    z = a.c(parcel, b2);
                    break;
                default:
                    a.b(parcel, b2);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new PolylineOptions(i2, arrayList, f2, i, f, z2, z);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }

    public PolylineOptions[] newArray(int i) {
        return new PolylineOptions[i];
    }
}
