package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ae;
import java.util.ArrayList;
import java.util.HashMap;

public class ah implements SafeParcelable {
    public static final ai CREATOR = new ai();
    private final int ab;
    private final HashMap<String, HashMap<String, ae.a<?, ?>>> cD;
    private final ArrayList<a> cE = null;
    private final String cF;

    public class a implements SafeParcelable {
        public static final aj CREATOR = new aj();
        final ArrayList<b> cG;
        final String className;
        final int versionCode;

        a(int i, String str, ArrayList<b> arrayList) {
            this.versionCode = i;
            this.className = str;
            this.cG = arrayList;
        }

        a(String str, HashMap<String, ae.a<?, ?>> hashMap) {
            this.versionCode = 1;
            this.className = str;
            this.cG = a(hashMap);
        }

        private static ArrayList<b> a(HashMap<String, ae.a<?, ?>> hashMap) {
            if (hashMap == null) {
                return null;
            }
            ArrayList<b> arrayList = new ArrayList<>();
            for (String next : hashMap.keySet()) {
                arrayList.add(new b(next, hashMap.get(next)));
            }
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        public HashMap<String, ae.a<?, ?>> ak() {
            HashMap<String, ae.a<?, ?>> hashMap = new HashMap<>();
            int size = this.cG.size();
            for (int i = 0; i < size; i++) {
                b bVar = this.cG.get(i);
                hashMap.put(bVar.cH, bVar.cI);
            }
            return hashMap;
        }

        public int describeContents() {
            aj ajVar = CREATOR;
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            aj ajVar = CREATOR;
            aj.a(this, parcel, i);
        }
    }

    public class b implements SafeParcelable {
        public static final ag CREATOR = new ag();
        final String cH;
        final ae.a<?, ?> cI;
        final int versionCode;

        b(int i, String str, ae.a<?, ?> aVar) {
            this.versionCode = i;
            this.cH = str;
            this.cI = aVar;
        }

        b(String str, ae.a<?, ?> aVar) {
            this.versionCode = 1;
            this.cH = str;
            this.cI = aVar;
        }

        public int describeContents() {
            ag agVar = CREATOR;
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            ag agVar = CREATOR;
            ag.a(this, parcel, i);
        }
    }

    ah(int i, ArrayList<a> arrayList, String str) {
        this.ab = i;
        this.cD = b(arrayList);
        this.cF = (String) s.d(str);
        ag();
    }

    private static HashMap<String, HashMap<String, ae.a<?, ?>>> b(ArrayList<a> arrayList) {
        HashMap<String, HashMap<String, ae.a<?, ?>>> hashMap = new HashMap<>();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            a aVar = arrayList.get(i);
            hashMap.put(aVar.className, aVar.ak());
        }
        return hashMap;
    }

    public void ag() {
        for (String str : this.cD.keySet()) {
            HashMap hashMap = this.cD.get(str);
            for (String str2 : hashMap.keySet()) {
                ((ae.a) hashMap.get(str2)).a(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<a> ai() {
        ArrayList<a> arrayList = new ArrayList<>();
        for (String next : this.cD.keySet()) {
            arrayList.add(new a(next, this.cD.get(next)));
        }
        return arrayList;
    }

    public String aj() {
        return this.cF;
    }

    public int describeContents() {
        ai aiVar = CREATOR;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    public HashMap<String, ae.a<?, ?>> q(String str) {
        return this.cD.get(str);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String next : this.cD.keySet()) {
            sb.append(next).append(":\n");
            HashMap hashMap = this.cD.get(next);
            for (String str : hashMap.keySet()) {
                sb.append("  ").append(str).append(": ");
                sb.append(hashMap.get(str));
            }
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        ai aiVar = CREATOR;
        ai.a(this, parcel, i);
    }
}
