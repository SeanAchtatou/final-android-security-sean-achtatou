package com.google.android.gms.internal;

import android.os.Parcel;
import com.actionbarsherlock.R;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ae;
import com.google.android.gms.plus.model.moments.ItemScope;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class bx extends ae implements SafeParcelable, ItemScope {
    public static final by CREATOR = new by();
    private static final HashMap<String, ae.a<?, ?>> iC = new HashMap<>();
    private final int ab;
    private String di;
    private double fy;
    private double fz;
    private final Set<Integer> iD;
    private bx iE;
    private List<String> iF;
    private bx iG;
    private String iH;
    private String iI;
    private String iJ;
    private List<bx> iK;
    private int iL;
    private List<bx> iM;
    private bx iN;
    private List<bx> iO;
    private String iP;
    private String iQ;
    private bx iR;
    private String iS;
    private String iT;
    private String iU;
    private List<bx> iV;
    private String iW;
    private String iX;
    private String iY;
    private String iZ;
    private String ie;
    private String jA;
    private String ja;
    private String jb;
    private String jc;
    private String jd;
    private bx je;
    private String jf;
    private String jg;
    private String jh;
    private String ji;
    private bx jj;
    private bx jk;
    private bx jl;
    private List<bx> jm;
    private String jn;
    private String jo;
    private String jp;
    private String jq;
    private bx jr;
    private String js;
    private String jt;
    private String ju;
    private bx jv;
    private String jw;
    private String jx;
    private String jy;
    private String jz;
    private String mName;

    static {
        iC.put("about", ae.a.a("about", 2, bx.class));
        iC.put("additionalName", ae.a.g("additionalName", 3));
        iC.put("address", ae.a.a("address", 4, bx.class));
        iC.put("addressCountry", ae.a.f("addressCountry", 5));
        iC.put("addressLocality", ae.a.f("addressLocality", 6));
        iC.put("addressRegion", ae.a.f("addressRegion", 7));
        iC.put("associated_media", ae.a.b("associated_media", 8, bx.class));
        iC.put("attendeeCount", ae.a.c("attendeeCount", 9));
        iC.put("attendees", ae.a.b("attendees", 10, bx.class));
        iC.put("audio", ae.a.a("audio", 11, bx.class));
        iC.put("author", ae.a.b("author", 12, bx.class));
        iC.put("bestRating", ae.a.f("bestRating", 13));
        iC.put("birthDate", ae.a.f("birthDate", 14));
        iC.put("byArtist", ae.a.a("byArtist", 15, bx.class));
        iC.put("caption", ae.a.f("caption", 16));
        iC.put("contentSize", ae.a.f("contentSize", 17));
        iC.put("contentUrl", ae.a.f("contentUrl", 18));
        iC.put("contributor", ae.a.b("contributor", 19, bx.class));
        iC.put("dateCreated", ae.a.f("dateCreated", 20));
        iC.put("dateModified", ae.a.f("dateModified", 21));
        iC.put("datePublished", ae.a.f("datePublished", 22));
        iC.put("description", ae.a.f("description", 23));
        iC.put("duration", ae.a.f("duration", 24));
        iC.put("embedUrl", ae.a.f("embedUrl", 25));
        iC.put("endDate", ae.a.f("endDate", 26));
        iC.put("familyName", ae.a.f("familyName", 27));
        iC.put("gender", ae.a.f("gender", 28));
        iC.put("geo", ae.a.a("geo", 29, bx.class));
        iC.put("givenName", ae.a.f("givenName", 30));
        iC.put("height", ae.a.f("height", 31));
        iC.put("id", ae.a.f("id", 32));
        iC.put("image", ae.a.f("image", 33));
        iC.put("inAlbum", ae.a.a("inAlbum", 34, bx.class));
        iC.put("latitude", ae.a.d("latitude", 36));
        iC.put("location", ae.a.a("location", 37, bx.class));
        iC.put("longitude", ae.a.d("longitude", 38));
        iC.put("name", ae.a.f("name", 39));
        iC.put("partOfTVSeries", ae.a.a("partOfTVSeries", 40, bx.class));
        iC.put("performers", ae.a.b("performers", 41, bx.class));
        iC.put("playerType", ae.a.f("playerType", 42));
        iC.put("postOfficeBoxNumber", ae.a.f("postOfficeBoxNumber", 43));
        iC.put("postalCode", ae.a.f("postalCode", 44));
        iC.put("ratingValue", ae.a.f("ratingValue", 45));
        iC.put("reviewRating", ae.a.a("reviewRating", 46, bx.class));
        iC.put("startDate", ae.a.f("startDate", 47));
        iC.put("streetAddress", ae.a.f("streetAddress", 48));
        iC.put("text", ae.a.f("text", 49));
        iC.put("thumbnail", ae.a.a("thumbnail", 50, bx.class));
        iC.put("thumbnailUrl", ae.a.f("thumbnailUrl", 51));
        iC.put("tickerSymbol", ae.a.f("tickerSymbol", 52));
        iC.put("type", ae.a.f("type", 53));
        iC.put("url", ae.a.f("url", 54));
        iC.put("width", ae.a.f("width", 55));
        iC.put("worstRating", ae.a.f("worstRating", 56));
    }

    public bx() {
        this.ab = 1;
        this.iD = new HashSet();
    }

    bx(Set<Integer> set, int i, bx bxVar, List<String> list, bx bxVar2, String str, String str2, String str3, List<bx> list2, int i2, List<bx> list3, bx bxVar3, List<bx> list4, String str4, String str5, bx bxVar4, String str6, String str7, String str8, List<bx> list5, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, String str17, bx bxVar5, String str18, String str19, String str20, String str21, bx bxVar6, double d, bx bxVar7, double d2, String str22, bx bxVar8, List<bx> list6, String str23, String str24, String str25, String str26, bx bxVar9, String str27, String str28, String str29, bx bxVar10, String str30, String str31, String str32, String str33, String str34, String str35) {
        this.iD = set;
        this.ab = i;
        this.iE = bxVar;
        this.iF = list;
        this.iG = bxVar2;
        this.iH = str;
        this.iI = str2;
        this.iJ = str3;
        this.iK = list2;
        this.iL = i2;
        this.iM = list3;
        this.iN = bxVar3;
        this.iO = list4;
        this.iP = str4;
        this.iQ = str5;
        this.iR = bxVar4;
        this.iS = str6;
        this.iT = str7;
        this.iU = str8;
        this.iV = list5;
        this.iW = str9;
        this.iX = str10;
        this.iY = str11;
        this.di = str12;
        this.iZ = str13;
        this.ja = str14;
        this.jb = str15;
        this.jc = str16;
        this.jd = str17;
        this.je = bxVar5;
        this.jf = str18;
        this.jg = str19;
        this.jh = str20;
        this.ji = str21;
        this.jj = bxVar6;
        this.fy = d;
        this.jk = bxVar7;
        this.fz = d2;
        this.mName = str22;
        this.jl = bxVar8;
        this.jm = list6;
        this.jn = str23;
        this.jo = str24;
        this.jp = str25;
        this.jq = str26;
        this.jr = bxVar9;
        this.js = str27;
        this.jt = str28;
        this.ju = str29;
        this.jv = bxVar10;
        this.jw = str30;
        this.jx = str31;
        this.jy = str32;
        this.ie = str33;
        this.jz = str34;
        this.jA = str35;
    }

    public HashMap<String, ae.a<?, ?>> T() {
        return iC;
    }

    /* access modifiers changed from: protected */
    public boolean a(ae.a aVar) {
        return this.iD.contains(Integer.valueOf(aVar.aa()));
    }

    /* access modifiers changed from: protected */
    public Object b(ae.a aVar) {
        switch (aVar.aa()) {
            case 2:
                return this.iE;
            case 3:
                return this.iF;
            case 4:
                return this.iG;
            case 5:
                return this.iH;
            case 6:
                return this.iI;
            case 7:
                return this.iJ;
            case 8:
                return this.iK;
            case 9:
                return Integer.valueOf(this.iL);
            case 10:
                return this.iM;
            case 11:
                return this.iN;
            case 12:
                return this.iO;
            case 13:
                return this.iP;
            case 14:
                return this.iQ;
            case 15:
                return this.iR;
            case 16:
                return this.iS;
            case 17:
                return this.iT;
            case 18:
                return this.iU;
            case R.styleable.SherlockTheme_buttonStyleSmall:
                return this.iV;
            case R.styleable.SherlockTheme_selectableItemBackground:
                return this.iW;
            case R.styleable.SherlockTheme_windowContentOverlay:
                return this.iX;
            case R.styleable.SherlockTheme_textAppearanceLargePopupMenu:
                return this.iY;
            case R.styleable.SherlockTheme_textAppearanceSmallPopupMenu:
                return this.di;
            case R.styleable.SherlockTheme_textAppearanceSmall:
                return this.iZ;
            case R.styleable.SherlockTheme_textColorPrimary:
                return this.ja;
            case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                return this.jb;
            case R.styleable.SherlockTheme_textColorPrimaryInverse:
                return this.jc;
            case R.styleable.SherlockTheme_spinnerItemStyle:
                return this.jd;
            case R.styleable.SherlockTheme_spinnerDropDownItemStyle:
                return this.je;
            case R.styleable.SherlockTheme_searchAutoCompleteTextView:
                return this.jf;
            case R.styleable.SherlockTheme_searchDropdownBackground:
                return this.jg;
            case R.styleable.SherlockTheme_searchViewCloseIcon:
                return this.jh;
            case R.styleable.SherlockTheme_searchViewGoIcon:
                return this.ji;
            case R.styleable.SherlockTheme_searchViewSearchIcon:
                return this.jj;
            case R.styleable.SherlockTheme_searchViewVoiceIcon:
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + aVar.aa());
            case R.styleable.SherlockTheme_searchViewEditQuery:
                return Double.valueOf(this.fy);
            case R.styleable.SherlockTheme_searchViewEditQueryBackground:
                return this.jk;
            case R.styleable.SherlockTheme_searchViewTextField:
                return Double.valueOf(this.fz);
            case R.styleable.SherlockTheme_searchViewTextFieldRight:
                return this.mName;
            case R.styleable.SherlockTheme_textColorSearchUrl:
                return this.jl;
            case R.styleable.SherlockTheme_searchResultListItemHeight:
                return this.jm;
            case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                return this.jn;
            case R.styleable.SherlockTheme_textAppearanceSearchResultSubtitle:
                return this.jo;
            case R.styleable.SherlockTheme_listPreferredItemHeightSmall:
                return this.jp;
            case R.styleable.SherlockTheme_listPreferredItemPaddingLeft:
                return this.jq;
            case R.styleable.SherlockTheme_listPreferredItemPaddingRight:
                return this.jr;
            case R.styleable.SherlockTheme_textAppearanceListItemSmall:
                return this.js;
            case R.styleable.SherlockTheme_windowMinWidthMajor:
                return this.jt;
            case R.styleable.SherlockTheme_windowMinWidthMinor:
                return this.ju;
            case 50:
                return this.jv;
            case R.styleable.SherlockTheme_actionDropDownStyle:
                return this.jw;
            case R.styleable.SherlockTheme_actionButtonStyle:
                return this.jx;
            case R.styleable.SherlockTheme_homeAsUpIndicator:
                return this.jy;
            case R.styleable.SherlockTheme_dropDownListViewStyle:
                return this.ie;
            case R.styleable.SherlockTheme_popupMenuStyle:
                return this.jz;
            case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                return this.jA;
        }
    }

    /* access modifiers changed from: package-private */
    public Set<Integer> bH() {
        return this.iD;
    }

    /* access modifiers changed from: package-private */
    public bx bI() {
        return this.iE;
    }

    /* access modifiers changed from: package-private */
    public bx bJ() {
        return this.iG;
    }

    /* access modifiers changed from: package-private */
    public List<bx> bK() {
        return this.iK;
    }

    /* access modifiers changed from: package-private */
    public List<bx> bL() {
        return this.iM;
    }

    /* access modifiers changed from: package-private */
    public bx bM() {
        return this.iN;
    }

    /* access modifiers changed from: package-private */
    public List<bx> bN() {
        return this.iO;
    }

    /* access modifiers changed from: package-private */
    public bx bO() {
        return this.iR;
    }

    /* access modifiers changed from: package-private */
    public List<bx> bP() {
        return this.iV;
    }

    /* access modifiers changed from: package-private */
    public bx bQ() {
        return this.je;
    }

    /* access modifiers changed from: package-private */
    public bx bR() {
        return this.jj;
    }

    /* access modifiers changed from: package-private */
    public bx bS() {
        return this.jk;
    }

    /* access modifiers changed from: package-private */
    public bx bT() {
        return this.jl;
    }

    /* access modifiers changed from: package-private */
    public List<bx> bU() {
        return this.jm;
    }

    /* access modifiers changed from: package-private */
    public bx bV() {
        return this.jr;
    }

    /* access modifiers changed from: package-private */
    public bx bW() {
        return this.jv;
    }

    /* renamed from: bX */
    public bx freeze() {
        return this;
    }

    public int describeContents() {
        by byVar = CREATOR;
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof bx)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        bx bxVar = (bx) obj;
        for (ae.a next : iC.values()) {
            if (a(next)) {
                if (!bxVar.a(next)) {
                    return false;
                }
                if (!b(next).equals(bxVar.b(next))) {
                    return false;
                }
            } else if (bxVar.a(next)) {
                return false;
            }
        }
        return true;
    }

    public List<String> getAdditionalName() {
        return this.iF;
    }

    public String getAddressCountry() {
        return this.iH;
    }

    public String getAddressLocality() {
        return this.iI;
    }

    public String getAddressRegion() {
        return this.iJ;
    }

    public int getAttendeeCount() {
        return this.iL;
    }

    public String getBestRating() {
        return this.iP;
    }

    public String getBirthDate() {
        return this.iQ;
    }

    public String getCaption() {
        return this.iS;
    }

    public String getContentSize() {
        return this.iT;
    }

    public String getContentUrl() {
        return this.iU;
    }

    public String getDateCreated() {
        return this.iW;
    }

    public String getDateModified() {
        return this.iX;
    }

    public String getDatePublished() {
        return this.iY;
    }

    public String getDescription() {
        return this.di;
    }

    public String getDuration() {
        return this.iZ;
    }

    public String getEmbedUrl() {
        return this.ja;
    }

    public String getEndDate() {
        return this.jb;
    }

    public String getFamilyName() {
        return this.jc;
    }

    public String getGender() {
        return this.jd;
    }

    public String getGivenName() {
        return this.jf;
    }

    public String getHeight() {
        return this.jg;
    }

    public String getId() {
        return this.jh;
    }

    public String getImage() {
        return this.ji;
    }

    public double getLatitude() {
        return this.fy;
    }

    public double getLongitude() {
        return this.fz;
    }

    public String getName() {
        return this.mName;
    }

    public String getPlayerType() {
        return this.jn;
    }

    public String getPostOfficeBoxNumber() {
        return this.jo;
    }

    public String getPostalCode() {
        return this.jp;
    }

    public String getRatingValue() {
        return this.jq;
    }

    public String getStartDate() {
        return this.js;
    }

    public String getStreetAddress() {
        return this.jt;
    }

    public String getText() {
        return this.ju;
    }

    public String getThumbnailUrl() {
        return this.jw;
    }

    public String getTickerSymbol() {
        return this.jx;
    }

    public String getType() {
        return this.jy;
    }

    public String getUrl() {
        return this.ie;
    }

    public String getWidth() {
        return this.jz;
    }

    public String getWorstRating() {
        return this.jA;
    }

    public int hashCode() {
        int i = 0;
        Iterator<ae.a<?, ?>> it = iC.values().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            ae.a next = it.next();
            if (a(next)) {
                i = b(next).hashCode() + i2 + next.aa();
            } else {
                i = i2;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.ab;
    }

    /* access modifiers changed from: protected */
    public Object m(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean n(String str) {
        return false;
    }

    public void writeToParcel(Parcel parcel, int i) {
        by byVar = CREATOR;
        by.a(this, parcel, i);
    }
}
