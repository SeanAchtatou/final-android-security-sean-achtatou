package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.HashSet;
import java.util.Set;

public class ca implements Parcelable.Creator<bz> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.internal.bx, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void a(bz bzVar, Parcel parcel, int i) {
        int d = b.d(parcel);
        Set<Integer> bH = bzVar.bH();
        if (bH.contains(1)) {
            b.c(parcel, 1, bzVar.i());
        }
        if (bH.contains(2)) {
            b.a(parcel, 2, bzVar.getId(), true);
        }
        if (bH.contains(4)) {
            b.a(parcel, 4, (Parcelable) bzVar.bY(), i, true);
        }
        if (bH.contains(5)) {
            b.a(parcel, 5, bzVar.getStartDate(), true);
        }
        if (bH.contains(6)) {
            b.a(parcel, 6, (Parcelable) bzVar.bZ(), i, true);
        }
        if (bH.contains(7)) {
            b.a(parcel, 7, bzVar.getType(), true);
        }
        b.C(parcel, d);
    }

    /* renamed from: X */
    public bz[] newArray(int i) {
        return new bz[i];
    }

    /* renamed from: x */
    public bz createFromParcel(Parcel parcel) {
        String str = null;
        int c2 = a.c(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        bx bxVar = null;
        String str2 = null;
        bx bxVar2 = null;
        String str3 = null;
        while (parcel.dataPosition() < c2) {
            int b2 = a.b(parcel);
            switch (a.m(b2)) {
                case 1:
                    i = a.f(parcel, b2);
                    hashSet.add(1);
                    break;
                case 2:
                    str3 = a.l(parcel, b2);
                    hashSet.add(2);
                    break;
                case 3:
                default:
                    a.b(parcel, b2);
                    break;
                case 4:
                    hashSet.add(4);
                    bxVar2 = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case 5:
                    str2 = a.l(parcel, b2);
                    hashSet.add(5);
                    break;
                case 6:
                    hashSet.add(6);
                    bxVar = (bx) a.a(parcel, b2, bx.CREATOR);
                    break;
                case 7:
                    str = a.l(parcel, b2);
                    hashSet.add(7);
                    break;
            }
        }
        if (parcel.dataPosition() == c2) {
            return new bz(hashSet, i, str3, bxVar2, str2, bxVar, str);
        }
        throw new a.C0001a("Overread allowed size end=" + c2, parcel);
    }
}
