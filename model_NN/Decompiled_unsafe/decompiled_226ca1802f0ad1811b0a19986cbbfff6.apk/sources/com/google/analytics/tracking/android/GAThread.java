package com.google.analytics.tracking.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.google.analytics.tracking.android.AnalyticsThread;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.android.gms.analytics.internal.Command;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

class GAThread extends Thread implements AnalyticsThread {
    private static GAThread sInstance;
    /* access modifiers changed from: private */
    public volatile boolean mAppOptOut;
    /* access modifiers changed from: private */
    public volatile String mClientId;
    private volatile boolean mClosed = false;
    /* access modifiers changed from: private */
    public volatile List<Command> mCommands;
    private final Context mContext;
    private volatile boolean mDisabled = false;
    /* access modifiers changed from: private */
    public volatile String mInstallCampaign;
    /* access modifiers changed from: private */
    public volatile MetaModel mMetaModel;
    /* access modifiers changed from: private */
    public volatile ServiceProxy mServiceProxy;
    private final LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();

    static GAThread getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new GAThread(context);
        }
        return sInstance;
    }

    private GAThread(Context context) {
        super("GAThread");
        if (context != null) {
            this.mContext = context.getApplicationContext();
        } else {
            this.mContext = context;
        }
        start();
    }

    /* access modifiers changed from: protected */
    public void init() {
        this.mServiceProxy.createService();
        this.mCommands = new ArrayList();
        this.mCommands.add(new Command("appendVersion", "_v", "ma1b6"));
        this.mCommands.add(new Command("appendQueueTime", "qt", null));
        this.mCommands.add(new Command("appendCacheBuster", "z", null));
        this.mMetaModel = new MetaModel();
        MetaModelInitializer.set(this.mMetaModel);
    }

    public void sendHit(Map<String, String> map) {
        final HashMap hashMap = new HashMap(map);
        final long currentTimeMillis = System.currentTimeMillis();
        hashMap.put("hitTime", Long.toString(currentTimeMillis));
        queueToThread(new Runnable() {
            public void run() {
                hashMap.put("clientId", GAThread.this.mClientId);
                if (!GAThread.this.mAppOptOut && !GAThread.this.isSampledOut(hashMap)) {
                    if (!TextUtils.isEmpty(GAThread.this.mInstallCampaign)) {
                        hashMap.put("campaign", GAThread.this.mInstallCampaign);
                        String unused = GAThread.this.mInstallCampaign = null;
                    }
                    GAThread.this.fillAppParameters(hashMap);
                    GAThread.this.fillCampaignParameters(hashMap);
                    GAThread.this.fillExceptionParameters(hashMap);
                    GAThread.this.mServiceProxy.putHit(HitBuilder.generateHitParams(GAThread.this.mMetaModel, hashMap), currentTimeMillis, GAThread.this.getHostUrl(hashMap), GAThread.this.mCommands);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public String getHostUrl(Map<String, String> map) {
        String str = map.get("internalHitUrl");
        if (str == null) {
            return (!map.containsKey("useSecure") || Utils.safeParseBoolean(map.get("useSecure"))) ? "https://ssl.google-analytics.com/collect" : "http://www.google-analytics.com/collect";
        }
        return str;
    }

    /* access modifiers changed from: private */
    public void fillExceptionParameters(Map<String, String> map) {
        String str = map.get("rawException");
        if (str != null) {
            map.remove("rawException");
            try {
                ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(Utils.hexDecode(str)));
                Object readObject = objectInputStream.readObject();
                objectInputStream.close();
                if (readObject instanceof Throwable) {
                    Throwable th = (Throwable) readObject;
                    map.put("exDescription", new StandardExceptionParser(this.mContext, new ArrayList()).getDescription(map.get("exceptionThreadName"), th));
                }
            } catch (IOException e) {
                Log.w("IOException reading exception");
            } catch (ClassNotFoundException e2) {
                Log.w("ClassNotFoundException reading exception");
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean isSampledOut(Map<String, String> map) {
        String str;
        if (map.get("sampleRate") != null) {
            double safeParseDouble = Utils.safeParseDouble(map.get("sampleRate"));
            if (safeParseDouble <= 0.0d) {
                return true;
            }
            if (safeParseDouble < 100.0d && (str = map.get("clientId")) != null && ((double) (Math.abs(str.hashCode()) % 10000)) >= safeParseDouble * 100.0d) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void fillAppParameters(Map<String, String> map) {
        String str;
        PackageManager packageManager = this.mContext.getPackageManager();
        String packageName = this.mContext.getPackageName();
        String installerPackageName = packageManager.getInstallerPackageName(packageName);
        String str2 = null;
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(this.mContext.getPackageName(), 0);
            if (packageInfo != null) {
                str = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
                try {
                    str2 = packageInfo.versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    Log.e("Error retrieving package info: appName set to " + str);
                    putIfAbsent(map, "appName", str);
                    putIfAbsent(map, "appVersion", str2);
                    putIfAbsent(map, "appId", packageName);
                    putIfAbsent(map, "appInstallerId", installerPackageName);
                    map.put("apiVersion", "1");
                }
            } else {
                str = packageName;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            str = packageName;
            Log.e("Error retrieving package info: appName set to " + str);
            putIfAbsent(map, "appName", str);
            putIfAbsent(map, "appVersion", str2);
            putIfAbsent(map, "appId", packageName);
            putIfAbsent(map, "appInstallerId", installerPackageName);
            map.put("apiVersion", "1");
        }
        putIfAbsent(map, "appName", str);
        putIfAbsent(map, "appVersion", str2);
        putIfAbsent(map, "appId", packageName);
        putIfAbsent(map, "appInstallerId", installerPackageName);
        map.put("apiVersion", "1");
    }

    private void putIfAbsent(Map<String, String> map, String str, String str2) {
        if (!map.containsKey(str)) {
            map.put(str, str2);
        }
    }

    /* access modifiers changed from: private */
    public void fillCampaignParameters(Map<String, String> map) {
        String filterCampaign = Utils.filterCampaign(map.get("campaign"));
        if (!TextUtils.isEmpty(filterCampaign)) {
            Map<String, String> parseURLParameters = Utils.parseURLParameters(filterCampaign);
            map.put("campaignContent", parseURLParameters.get("utm_content"));
            map.put("campaignMedium", parseURLParameters.get("utm_medium"));
            map.put("campaignName", parseURLParameters.get("utm_campaign"));
            map.put("campaignSource", parseURLParameters.get("utm_source"));
            map.put("campaignKeyword", parseURLParameters.get("utm_term"));
            map.put("campaignId", parseURLParameters.get("utm_id"));
            map.put("gclid", parseURLParameters.get("gclid"));
            map.put("dclid", parseURLParameters.get("dclid"));
            map.put("gmob_t", parseURLParameters.get("gmob_t"));
        }
    }

    public void dispatch() {
        queueToThread(new Runnable() {
            public void run() {
                GAThread.this.mServiceProxy.dispatch();
            }
        });
    }

    public void requestAppOptOut(final GoogleAnalytics.AppOptOutCallback appOptOutCallback) {
        queueToThread(new Runnable() {
            public void run() {
                appOptOutCallback.reportAppOptOut(GAThread.this.mAppOptOut);
            }
        });
    }

    public void requestClientId(final AnalyticsThread.ClientIdCallback clientIdCallback) {
        queueToThread(new Runnable() {
            public void run() {
                clientIdCallback.reportClientId(GAThread.this.mClientId);
            }
        });
    }

    private void queueToThread(Runnable runnable) {
        this.queue.add(runnable);
    }

    private boolean loadAppOptOut() {
        return this.mContext.getFileStreamPath("gaOptOut").exists();
    }

    private boolean storeClientId(String str) {
        try {
            FileOutputStream openFileOutput = this.mContext.openFileOutput("gaClientId", 0);
            openFileOutput.write(str.getBytes());
            openFileOutput.close();
            return true;
        } catch (FileNotFoundException e) {
            Log.e("Error creating clientId file.");
            return false;
        } catch (IOException e2) {
            Log.e("Error writing to clientId file.");
            return false;
        }
    }

    private String generateClientId() {
        String lowerCase = UUID.randomUUID().toString().toLowerCase();
        if (!storeClientId(lowerCase)) {
            return "0";
        }
        return lowerCase;
    }

    /* access modifiers changed from: package-private */
    public String initializeClientId() {
        String str = null;
        try {
            FileInputStream openFileInput = this.mContext.openFileInput("gaClientId");
            byte[] bArr = new byte[NotificationCompat.FLAG_HIGH_PRIORITY];
            int read = openFileInput.read(bArr, 0, NotificationCompat.FLAG_HIGH_PRIORITY);
            if (openFileInput.available() > 0) {
                Log.e("clientId file seems corrupted, deleting it.");
                openFileInput.close();
                this.mContext.deleteFile("gaInstallData");
            }
            if (read <= 0) {
                Log.e("clientId file seems empty, deleting it.");
                openFileInput.close();
                this.mContext.deleteFile("gaInstallData");
            } else {
                String str2 = new String(bArr, 0, read);
                try {
                    openFileInput.close();
                    str = str2;
                } catch (FileNotFoundException e) {
                    str = str2;
                } catch (IOException e2) {
                    str = str2;
                    Log.e("Error reading clientId file, deleting it.");
                    this.mContext.deleteFile("gaInstallData");
                } catch (NumberFormatException e3) {
                    str = str2;
                    Log.e("cliendId file doesn't have long value, deleting it.");
                    this.mContext.deleteFile("gaInstallData");
                }
            }
        } catch (FileNotFoundException e4) {
        } catch (IOException e5) {
            Log.e("Error reading clientId file, deleting it.");
            this.mContext.deleteFile("gaInstallData");
        } catch (NumberFormatException e6) {
            Log.e("cliendId file doesn't have long value, deleting it.");
            this.mContext.deleteFile("gaInstallData");
        }
        if (str == null) {
            return generateClientId();
        }
        return str;
    }

    static String getAndClearCampaign(Context context) {
        try {
            FileInputStream openFileInput = context.openFileInput("gaInstallData");
            byte[] bArr = new byte[FragmentTransaction.TRANSIT_EXIT_MASK];
            int read = openFileInput.read(bArr, 0, FragmentTransaction.TRANSIT_EXIT_MASK);
            if (openFileInput.available() > 0) {
                Log.e("Too much campaign data, ignoring it.");
                openFileInput.close();
                context.deleteFile("gaInstallData");
                return null;
            }
            openFileInput.close();
            context.deleteFile("gaInstallData");
            if (read <= 0) {
                Log.w("Campaign file is empty.");
                return null;
            }
            String str = new String(bArr, 0, read);
            Log.i("Campaign found: " + str);
            return str;
        } catch (FileNotFoundException e) {
            Log.i("No campaign data found.");
            return null;
        } catch (IOException e2) {
            Log.e("Error reading campaign data.");
            context.deleteFile("gaInstallData");
            return null;
        }
    }

    private String printStackTrace(Throwable th) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        th.printStackTrace(printStream);
        printStream.flush();
        return new String(byteArrayOutputStream.toByteArray());
    }

    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Log.w("sleep interrupted in GAThread initialize");
        }
        try {
            if (this.mServiceProxy == null) {
                this.mServiceProxy = new GAServiceProxy(this.mContext, this);
            }
            init();
            this.mAppOptOut = loadAppOptOut();
            this.mClientId = initializeClientId();
            this.mInstallCampaign = getAndClearCampaign(this.mContext);
        } catch (Throwable th) {
            Log.e("Error initializing the GAThread: " + printStackTrace(th));
            Log.e("Google Analytics will not start up.");
            this.mDisabled = true;
        }
        while (!this.mClosed) {
            try {
                Runnable take = this.queue.take();
                if (!this.mDisabled) {
                    take.run();
                }
            } catch (InterruptedException e2) {
                Log.i(e2.toString());
            } catch (Throwable th2) {
                Log.e("Error on GAThread: " + printStackTrace(th2));
                Log.e("Google Analytics is shutting down.");
                this.mDisabled = true;
            }
        }
    }

    public LinkedBlockingQueue<Runnable> getQueue() {
        return this.queue;
    }

    public Thread getThread() {
        return this;
    }
}
