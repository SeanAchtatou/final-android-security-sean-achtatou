package com.google.analytics.tracking.android;

import com.google.analytics.tracking.android.MetaModel;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

class HitBuilder {
    static Map<String, String> generateHitParams(MetaModel metaModel, Map<String, String> map) {
        String urlParam;
        HashMap hashMap = new HashMap();
        for (Map.Entry next : map.entrySet()) {
            MetaModel.MetaInfo metaInfo = metaModel.getMetaInfo((String) next.getKey());
            if (!(metaInfo == null || (urlParam = metaInfo.getUrlParam((String) next.getKey())) == null)) {
                String str = (String) next.getValue();
                if (metaInfo.getFormatter() != null) {
                    str = metaInfo.getFormatter().format(str);
                }
                if (str != null && !str.equals(metaInfo.getDefaultValue())) {
                    hashMap.put(urlParam, str);
                }
            }
        }
        return hashMap;
    }

    static String postProcessHit(Hit hit, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append(hit.getHitParams());
        if (hit.getHitTime() > 0) {
            long hitTime = j - hit.getHitTime();
            if (hitTime >= 0) {
                sb.append("&").append("qt").append("=").append(hitTime);
            }
        }
        sb.append("&").append("z").append("=").append(hit.getHitId());
        return sb.toString();
    }

    static String encode(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("URL encoding failed for: " + str);
        }
    }
}
