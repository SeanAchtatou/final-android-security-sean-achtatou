package com.google.analytics.tracking.android;

import android.content.Context;
import android.text.TextUtils;

class ParameterLoaderImpl implements ParameterLoader {
    private final Context mContext;

    public ParameterLoaderImpl(Context context) {
        if (context == null) {
            throw new NullPointerException("Context cannot be null");
        }
        this.mContext = context.getApplicationContext();
    }

    private int getResourceIdForType(String str, String str2) {
        if (this.mContext == null) {
            return 0;
        }
        return this.mContext.getResources().getIdentifier(str, str2, this.mContext.getPackageName());
    }

    public String getString(String str) {
        int resourceIdForType = getResourceIdForType(str, "string");
        if (resourceIdForType == 0) {
            return null;
        }
        return this.mContext.getString(resourceIdForType);
    }

    public boolean getBoolean(String str) {
        int resourceIdForType = getResourceIdForType(str, "bool");
        if (resourceIdForType == 0) {
            return false;
        }
        return "true".equalsIgnoreCase(this.mContext.getString(resourceIdForType));
    }

    public int getInt(String str, int i) {
        int resourceIdForType = getResourceIdForType(str, "integer");
        if (resourceIdForType == 0) {
            return i;
        }
        try {
            return Integer.parseInt(this.mContext.getString(resourceIdForType));
        } catch (NumberFormatException e) {
            Log.w("NumberFormatException parsing " + this.mContext.getString(resourceIdForType));
            return i;
        }
    }

    public Double getDoubleFromString(String str) {
        String string = getString(str);
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        try {
            return Double.valueOf(Double.parseDouble(string));
        } catch (NumberFormatException e) {
            Log.w("NumberFormatException parsing " + string);
            return null;
        }
    }
}
