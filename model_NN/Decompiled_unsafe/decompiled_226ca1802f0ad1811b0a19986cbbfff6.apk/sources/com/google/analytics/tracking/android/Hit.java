package com.google.analytics.tracking.android;

class Hit {
    private final long mHitId;
    private String mHitString;
    private final long mHitTime;
    private String mHitUrl;

    /* access modifiers changed from: package-private */
    public String getHitParams() {
        return this.mHitString;
    }

    /* access modifiers changed from: package-private */
    public void setHitString(String str) {
        this.mHitString = str;
    }

    /* access modifiers changed from: package-private */
    public long getHitId() {
        return this.mHitId;
    }

    /* access modifiers changed from: package-private */
    public long getHitTime() {
        return this.mHitTime;
    }

    Hit(String str, long j, long j2) {
        this.mHitString = str;
        this.mHitId = j;
        this.mHitTime = j2;
    }

    /* access modifiers changed from: package-private */
    public String getHitUrl() {
        return this.mHitUrl;
    }

    /* access modifiers changed from: package-private */
    public void setHitUrl(String str) {
        this.mHitUrl = str;
    }
}
