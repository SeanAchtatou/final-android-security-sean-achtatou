package com.google.analytics.tracking.android;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Transaction {
    private final String mAffiliation;
    private final String mCurrencyCode;
    private final Map<String, Item> mItems;
    private final long mShippingCostInMicros;
    private final long mTotalCostInMicros;
    private final long mTotalTaxInMicros;
    private final String mTransactionId;

    private Transaction(Builder builder) {
        this.mTransactionId = builder.mTransactionId;
        this.mTotalCostInMicros = builder.mTotalCostInMicros;
        this.mAffiliation = builder.mAffiliation;
        this.mTotalTaxInMicros = builder.mTotalTaxInMicros;
        this.mShippingCostInMicros = builder.mShippingCostInMicros;
        this.mCurrencyCode = builder.mCurrencyCode;
        this.mItems = new HashMap();
    }

    public String getTransactionId() {
        return this.mTransactionId;
    }

    public String getAffiliation() {
        return this.mAffiliation;
    }

    public long getTotalCostInMicros() {
        return this.mTotalCostInMicros;
    }

    public long getTotalTaxInMicros() {
        return this.mTotalTaxInMicros;
    }

    public long getShippingCostInMicros() {
        return this.mShippingCostInMicros;
    }

    public String getCurrencyCode() {
        return this.mCurrencyCode;
    }

    public void addItem(Item item) {
        this.mItems.put(item.getSKU(), item);
    }

    public List<Item> getItems() {
        return new ArrayList(this.mItems.values());
    }

    public final class Builder {
        /* access modifiers changed from: private */
        public String mAffiliation = null;
        /* access modifiers changed from: private */
        public String mCurrencyCode = null;
        /* access modifiers changed from: private */
        public long mShippingCostInMicros = 0;
        /* access modifiers changed from: private */
        public final long mTotalCostInMicros;
        /* access modifiers changed from: private */
        public long mTotalTaxInMicros = 0;
        /* access modifiers changed from: private */
        public final String mTransactionId;

        public Builder(String str, long j) {
            if (TextUtils.isEmpty(str)) {
                throw new IllegalArgumentException("orderId must not be empty or null");
            }
            this.mTransactionId = str;
            this.mTotalCostInMicros = j;
        }

        public Builder setAffiliation(String str) {
            this.mAffiliation = str;
            return this;
        }

        public Builder setTotalTaxInMicros(long j) {
            this.mTotalTaxInMicros = j;
            return this;
        }

        public Builder setShippingCostInMicros(long j) {
            this.mShippingCostInMicros = j;
            return this;
        }

        public Transaction build() {
            return new Transaction(this);
        }
    }

    public final class Item {
        private final String mCategory;
        private final String mName;
        private final long mPriceInMicros;
        private final long mQuantity;
        private final String mSKU;

        private Item(Builder builder) {
            this.mSKU = builder.mSKU;
            this.mPriceInMicros = builder.mPriceInMicros;
            this.mQuantity = builder.mQuantity;
            this.mName = builder.mName;
            this.mCategory = builder.mCategory;
        }

        public String getSKU() {
            return this.mSKU;
        }

        public String getName() {
            return this.mName;
        }

        public String getCategory() {
            return this.mCategory;
        }

        public long getPriceInMicros() {
            return this.mPriceInMicros;
        }

        public long getQuantity() {
            return this.mQuantity;
        }

        public final class Builder {
            /* access modifiers changed from: private */
            public String mCategory = null;
            /* access modifiers changed from: private */
            public final String mName;
            /* access modifiers changed from: private */
            public final long mPriceInMicros;
            /* access modifiers changed from: private */
            public final long mQuantity;
            /* access modifiers changed from: private */
            public final String mSKU;

            public Builder(String str, String str2, long j, long j2) {
                if (TextUtils.isEmpty(str)) {
                    throw new IllegalArgumentException("SKU must not be empty or null");
                } else if (TextUtils.isEmpty(str2)) {
                    throw new IllegalArgumentException("name must not be empty or null");
                } else {
                    this.mSKU = str;
                    this.mName = str2;
                    this.mPriceInMicros = j;
                    this.mQuantity = j2;
                }
            }

            public Builder setProductCategory(String str) {
                this.mCategory = str;
                return this;
            }

            public Item build() {
                return new Item(this);
            }
        }
    }
}
