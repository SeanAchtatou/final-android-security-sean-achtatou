package com.google.analytics.tracking.android;

import java.util.Random;

class AdMobInfo {
    private static final AdMobInfo INSTANCE = new AdMobInfo();
    private int mAdHitId;
    private Random mRandom = new Random();

    private AdMobInfo() {
    }

    static AdMobInfo getInstance() {
        return INSTANCE;
    }

    /* access modifiers changed from: package-private */
    public int generateAdHitId() {
        this.mAdHitId = this.mRandom.nextInt(2147483646) + 1;
        return this.mAdHitId;
    }
}
