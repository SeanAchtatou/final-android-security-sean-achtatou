package com.google.analytics.tracking.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;

class SimpleNetworkDispatcher implements Dispatcher {
    private final Context ctx;
    private final HttpClientFactory httpClientFactory;
    private final String userAgent;

    SimpleNetworkDispatcher(AnalyticsStore analyticsStore, HttpClientFactory httpClientFactory2, Context context) {
        this(httpClientFactory2, context);
    }

    SimpleNetworkDispatcher(HttpClientFactory httpClientFactory2, Context context) {
        this.ctx = context.getApplicationContext();
        this.userAgent = createUserAgentString("GoogleAnalytics", "2.0", Build.VERSION.RELEASE, Utils.getLanguage(Locale.getDefault()), Build.MODEL, Build.ID);
        this.httpClientFactory = httpClientFactory2;
    }

    public boolean okToDispatch() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.ctx.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        Log.vDebug("...no network connectivity");
        return false;
    }

    public int dispatchHits(List<Hit> list) {
        int min = Math.min(list.size(), 40);
        int i = 0;
        int i2 = 0;
        while (i < min) {
            HttpClient newInstance = this.httpClientFactory.newInstance();
            Hit hit = list.get(i);
            URL url = getUrl(hit);
            if (url != null) {
                HttpHost httpHost = new HttpHost(url.getHost(), url.getPort(), url.getProtocol());
                String path = url.getPath();
                String postProcessHit = TextUtils.isEmpty(hit.getHitParams()) ? "" : HitBuilder.postProcessHit(hit, System.currentTimeMillis());
                HttpEntityEnclosingRequest buildRequest = buildRequest(postProcessHit, path);
                if (buildRequest == null) {
                    continue;
                } else {
                    buildRequest.addHeader("Host", httpHost.toHostString());
                    logDebugInformation(Log.isDebugEnabled(), buildRequest);
                    if (postProcessHit.length() > 8192) {
                        Log.w("Hit too long (> 8192 bytes)--not sent");
                    } else {
                        try {
                            HttpResponse execute = newInstance.execute(httpHost, buildRequest);
                            if (execute.getStatusLine().getStatusCode() != 200) {
                                Log.w("Bad response: " + execute.getStatusLine().getStatusCode());
                                return i2;
                            }
                        } catch (ClientProtocolException e) {
                            Log.w("ClientProtocolException sending hit; discarding hit...");
                        } catch (IOException e2) {
                            Log.w("Exception sending hit: " + e2.getClass().getSimpleName());
                            Log.w(e2.getMessage());
                            return i2;
                        }
                    }
                }
            } else if (Log.isDebugEnabled()) {
                Log.w("No destination: discarding hit: " + hit.getHitParams());
            } else {
                Log.w("No destination: discarding hit.");
            }
            i++;
            i2++;
        }
        return i2;
    }

    private HttpEntityEnclosingRequest buildRequest(String str, String str2) {
        BasicHttpEntityEnclosingRequest basicHttpEntityEnclosingRequest;
        if (TextUtils.isEmpty(str)) {
            Log.w("Empty hit, discarding.");
            return null;
        }
        String str3 = str2 + "?" + str;
        if (str3.length() < 2036) {
            basicHttpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest("GET", str3);
        } else {
            basicHttpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest("POST", str2);
            try {
                basicHttpEntityEnclosingRequest.setEntity(new StringEntity(str));
            } catch (UnsupportedEncodingException e) {
                Log.w("Encoding error, discarding hit");
                return null;
            }
        }
        basicHttpEntityEnclosingRequest.addHeader("User-Agent", this.userAgent);
        return basicHttpEntityEnclosingRequest;
    }

    private void logDebugInformation(boolean z, HttpEntityEnclosingRequest httpEntityEnclosingRequest) {
        int available;
        if (z) {
            StringBuffer stringBuffer = new StringBuffer();
            for (Header obj : httpEntityEnclosingRequest.getAllHeaders()) {
                stringBuffer.append(obj.toString()).append("\n");
            }
            stringBuffer.append(httpEntityEnclosingRequest.getRequestLine().toString()).append("\n");
            if (httpEntityEnclosingRequest.getEntity() != null) {
                try {
                    InputStream content = httpEntityEnclosingRequest.getEntity().getContent();
                    if (content != null && (available = content.available()) > 0) {
                        byte[] bArr = new byte[available];
                        content.read(bArr);
                        stringBuffer.append("POST:\n");
                        stringBuffer.append(new String(bArr)).append("\n");
                    }
                } catch (IOException e) {
                    Log.w("Error Writing hit to log...");
                }
            }
            Log.i(stringBuffer.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public String createUserAgentString(String str, String str2, String str3, String str4, String str5, String str6) {
        return String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", str, str2, str3, str4, str5, str6);
    }

    private URL getUrl(Hit hit) {
        if (TextUtils.isEmpty(hit.getHitUrl())) {
            return null;
        }
        try {
            return new URL(hit.getHitUrl());
        } catch (MalformedURLException e) {
            try {
                return new URL("http://www.google-analytics.com/collect");
            } catch (MalformedURLException e2) {
                return null;
            }
        }
    }
}
