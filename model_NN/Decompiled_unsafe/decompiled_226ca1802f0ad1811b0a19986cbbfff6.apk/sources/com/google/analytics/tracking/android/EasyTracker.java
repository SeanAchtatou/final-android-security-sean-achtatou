package com.google.analytics.tracking.android;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import java.lang.Thread;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class EasyTracker {
    private static EasyTracker sInstance;
    private int mActivitiesActive = 0;
    private final Map<String, String> mActivityNameMap = new HashMap();
    private GoogleAnalytics mAnalyticsInstance;
    private String mAppName;
    private String mAppVersion;
    private Clock mClock = new Clock() {
        public long currentTimeMillis() {
            return System.currentTimeMillis();
        }
    };
    private Context mContext;
    private boolean mDebug;
    private int mDispatchPeriod = 1800;
    private Thread.UncaughtExceptionHandler mExceptionHandler;
    private boolean mIsAnonymizeIpEnabled;
    private boolean mIsAutoActivityTracking = false;
    private boolean mIsEnabled = false;
    /* access modifiers changed from: private */
    public boolean mIsInForeground = false;
    private boolean mIsReportUncaughtExceptionsEnabled;
    private long mLastOnStopTime;
    private ParameterLoader mParameterFetcher;
    private Double mSampleRate;
    private ServiceManager mServiceManager;
    private long mSessionTimeout;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private Tracker mTracker = null;
    private String mTrackingId;

    private EasyTracker() {
    }

    public static EasyTracker getInstance() {
        if (sInstance == null) {
            sInstance = new EasyTracker();
        }
        return sInstance;
    }

    public static Tracker getTracker() {
        if (getInstance().mContext != null) {
            return getInstance().mTracker;
        }
        throw new IllegalStateException("You must call EasyTracker.getInstance().setContext(context) or startActivity(activity) before calling getTracker()");
    }

    /* access modifiers changed from: package-private */
    public boolean checkForNewSession() {
        return this.mSessionTimeout == 0 || (this.mSessionTimeout > 0 && this.mClock.currentTimeMillis() > this.mLastOnStopTime + this.mSessionTimeout);
    }

    private void loadParameters() {
        boolean z = true;
        this.mTrackingId = this.mParameterFetcher.getString("ga_trackingId");
        if (TextUtils.isEmpty(this.mTrackingId)) {
            this.mTrackingId = this.mParameterFetcher.getString("ga_api_key");
            if (TextUtils.isEmpty(this.mTrackingId)) {
                Log.e("EasyTracker requested, but missing required ga_trackingId");
                this.mTracker = new NoopTracker();
                return;
            }
        }
        this.mIsEnabled = true;
        this.mAppName = this.mParameterFetcher.getString("ga_appName");
        this.mAppVersion = this.mParameterFetcher.getString("ga_appVersion");
        this.mDebug = this.mParameterFetcher.getBoolean("ga_debug");
        this.mSampleRate = this.mParameterFetcher.getDoubleFromString("ga_sampleFrequency");
        if (this.mSampleRate == null) {
            this.mSampleRate = new Double((double) this.mParameterFetcher.getInt("ga_sampleRate", 100));
        }
        this.mDispatchPeriod = this.mParameterFetcher.getInt("ga_dispatchPeriod", 1800);
        this.mSessionTimeout = (long) (this.mParameterFetcher.getInt("ga_sessionTimeout", 30) * 1000);
        if (!this.mParameterFetcher.getBoolean("ga_autoActivityTracking") && !this.mParameterFetcher.getBoolean("ga_auto_activity_tracking")) {
            z = false;
        }
        this.mIsAutoActivityTracking = z;
        this.mIsAnonymizeIpEnabled = this.mParameterFetcher.getBoolean("ga_anonymizeIp");
        this.mIsReportUncaughtExceptionsEnabled = this.mParameterFetcher.getBoolean("ga_reportUncaughtExceptions");
        this.mTracker = this.mAnalyticsInstance.getTracker(this.mTrackingId);
        if (!TextUtils.isEmpty(this.mAppName)) {
            Log.i("setting appName to " + this.mAppName);
            this.mTracker.setAppName(this.mAppName);
        }
        if (this.mAppVersion != null) {
            this.mTracker.setAppVersion(this.mAppVersion);
        }
        this.mTracker.setAnonymizeIp(this.mIsAnonymizeIpEnabled);
        this.mTracker.setSampleRate(this.mSampleRate.doubleValue());
        this.mAnalyticsInstance.setDebug(this.mDebug);
        this.mServiceManager.setDispatchPeriod(this.mDispatchPeriod);
        if (this.mIsReportUncaughtExceptionsEnabled) {
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.mExceptionHandler;
            if (uncaughtExceptionHandler == null) {
                uncaughtExceptionHandler = new ExceptionReporter(this.mTracker, this.mServiceManager, Thread.getDefaultUncaughtExceptionHandler(), this.mContext);
            }
            Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler);
        }
    }

    public void setContext(Context context) {
        if (context == null) {
            Log.e("Context cannot be null");
            return;
        }
        setContext(context, new ParameterLoaderImpl(context.getApplicationContext()), GoogleAnalytics.getInstance(context.getApplicationContext()), GAServiceManager.getInstance());
    }

    /* access modifiers changed from: package-private */
    public void setContext(Context context, ParameterLoader parameterLoader, GoogleAnalytics googleAnalytics, ServiceManager serviceManager) {
        if (context == null) {
            Log.e("Context cannot be null");
        }
        if (this.mContext == null) {
            this.mContext = context.getApplicationContext();
            this.mAnalyticsInstance = googleAnalytics;
            this.mServiceManager = serviceManager;
            this.mParameterFetcher = parameterLoader;
            loadParameters();
        }
    }

    public void activityStart(Activity activity) {
        setContext(activity);
        if (this.mIsEnabled) {
            clearExistingTimer();
            if (!this.mIsInForeground && this.mActivitiesActive == 0 && checkForNewSession()) {
                this.mTracker.setStartSession(true);
                if (!this.mIsAutoActivityTracking) {
                }
            }
            this.mIsInForeground = true;
            this.mActivitiesActive++;
            if (this.mIsAutoActivityTracking) {
                this.mTracker.sendView(getActivityName(activity));
            }
        }
    }

    public void activityStop(Activity activity) {
        setContext(activity);
        if (this.mIsEnabled) {
            this.mActivitiesActive--;
            this.mActivitiesActive = Math.max(0, this.mActivitiesActive);
            this.mLastOnStopTime = this.mClock.currentTimeMillis();
            if (this.mActivitiesActive == 0) {
                clearExistingTimer();
                this.mTimerTask = new NotInForegroundTimerTask();
                this.mTimer = new Timer("waitForActivityStart");
                this.mTimer.schedule(this.mTimerTask, 1000);
            }
        }
    }

    private synchronized void clearExistingTimer() {
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }
    }

    private String getActivityName(Activity activity) {
        String canonicalName = activity.getClass().getCanonicalName();
        if (this.mActivityNameMap.containsKey(canonicalName)) {
            return this.mActivityNameMap.get(canonicalName);
        }
        String string = this.mParameterFetcher.getString(canonicalName);
        if (string == null) {
            string = canonicalName;
        }
        this.mActivityNameMap.put(canonicalName, string);
        return string;
    }

    class NoopTracker extends Tracker {
        private boolean mIsAnonymizeIp;
        private double mSampleRate = 100.0d;

        NoopTracker() {
        }

        public void setStartSession(boolean z) {
        }

        public void setAppName(String str) {
        }

        public void setAppVersion(String str) {
        }

        public void sendView(String str) {
        }

        public void sendEvent(String str, String str2, String str3, Long l) {
        }

        public void sendTransaction(Transaction transaction) {
        }

        public void sendException(String str, boolean z) {
        }

        public void sendException(String str, Throwable th, boolean z) {
        }

        public void setAnonymizeIp(boolean z) {
            this.mIsAnonymizeIp = z;
        }

        public void setSampleRate(double d) {
            this.mSampleRate = d;
        }

        public Map<String, String> constructEvent(String str, String str2, String str3, Long l) {
            return new HashMap();
        }

        public Map<String, String> constructTransaction(Transaction transaction) {
            return new HashMap();
        }

        public Map<String, String> constructException(String str, boolean z) {
            return new HashMap();
        }

        public Map<String, String> constructRawException(String str, Throwable th, boolean z) {
            return new HashMap();
        }
    }

    class NotInForegroundTimerTask extends TimerTask {
        private NotInForegroundTimerTask() {
        }

        public void run() {
            boolean unused = EasyTracker.this.mIsInForeground = false;
        }
    }
}
