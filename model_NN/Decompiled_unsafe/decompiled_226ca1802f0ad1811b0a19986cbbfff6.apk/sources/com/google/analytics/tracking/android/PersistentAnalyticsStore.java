package com.google.analytics.tracking.android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.text.TextUtils;
import com.google.android.gms.analytics.internal.Command;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

class PersistentAnalyticsStore implements AnalyticsStore {
    /* access modifiers changed from: private */
    public static final String CREATE_HITS_TABLE = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER);", "hits2", "hit_id", "hit_time", "hit_url", "hit_string", "hit_app_id");
    /* access modifiers changed from: private */
    public Clock mClock;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final String mDatabaseName;
    private final AnalyticsDatabaseHelper mDbHelper;
    private volatile Dispatcher mDispatcher;
    private long mLastDeleteStaleHitsTime;
    private final AnalyticsStoreStateListener mListener;

    PersistentAnalyticsStore(AnalyticsStoreStateListener analyticsStoreStateListener, Context context) {
        this(analyticsStoreStateListener, context, "google_analytics_v2.db");
    }

    PersistentAnalyticsStore(AnalyticsStoreStateListener analyticsStoreStateListener, Context context, String str) {
        this.mContext = context.getApplicationContext();
        this.mDatabaseName = str;
        this.mListener = analyticsStoreStateListener;
        this.mClock = new Clock() {
            public long currentTimeMillis() {
                return System.currentTimeMillis();
            }
        };
        this.mDbHelper = new AnalyticsDatabaseHelper(this.mContext, this.mDatabaseName);
        this.mDispatcher = new SimpleNetworkDispatcher(this, createDefaultHttpClientFactory(), this.mContext);
        this.mLastDeleteStaleHitsTime = 0;
    }

    private HttpClientFactory createDefaultHttpClientFactory() {
        return new HttpClientFactory() {
            public HttpClient newInstance() {
                return new DefaultHttpClient();
            }
        };
    }

    public void clearHits(long j) {
        boolean z = true;
        SQLiteDatabase writableDatabase = getWritableDatabase("Error opening database for clearHits");
        if (writableDatabase != null) {
            if (j == 0) {
                writableDatabase.delete("hits2", null, null);
            } else {
                writableDatabase.delete("hits2", "hit_app_id = ?", new String[]{Long.valueOf(j).toString()});
            }
            AnalyticsStoreStateListener analyticsStoreStateListener = this.mListener;
            if (getNumStoredHits() != 0) {
                z = false;
            }
            analyticsStoreStateListener.reportStoreIsEmpty(z);
        }
    }

    public void putHit(Map<String, String> map, long j, String str, Collection<Command> collection) {
        deleteStaleHits();
        fillVersionParametersIfNecessary(map, collection);
        removeOldHitIfFull();
        writeHitToDatabase(map, j, str);
    }

    private void fillVersionParametersIfNecessary(Map<String, String> map, Collection<Command> collection) {
        for (Command next : collection) {
            if (next.getId().equals("appendVersion")) {
                storeVersion(map, next.getUrlParam(), next.getValue());
                return;
            }
        }
    }

    private void storeVersion(Map<String, String> map, String str, String str2) {
        String str3;
        if (str2 == null) {
            str3 = "";
        } else {
            str3 = str2 + "";
        }
        if (str != null) {
            map.put(str, str3);
        }
    }

    private void removeOldHitIfFull() {
        int numStoredHits = (getNumStoredHits() - 2000) + 1;
        if (numStoredHits > 0) {
            List<Hit> peekHits = peekHits(numStoredHits);
            Log.wDebug("Store full, deleting " + peekHits.size() + " hits to make room");
            deleteHits(peekHits);
        }
    }

    private void writeHitToDatabase(Map<String, String> map, long j, String str) {
        long j2;
        SQLiteDatabase writableDatabase = getWritableDatabase("Error opening database for putHit");
        if (writableDatabase != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_string", generateHitString(map));
            contentValues.put("hit_time", Long.valueOf(j));
            if (map.containsKey("AppUID")) {
                try {
                    j2 = Long.parseLong(map.get("AppUID"));
                } catch (NumberFormatException e) {
                    j2 = 0;
                }
            } else {
                j2 = 0;
            }
            contentValues.put("hit_app_id", Long.valueOf(j2));
            if (str == null) {
                str = "http://www.google-analytics.com/collect";
            }
            if (str.length() == 0) {
                Log.w("empty path: not sending hit");
                return;
            }
            contentValues.put("hit_url", str);
            try {
                writableDatabase.insert("hits2", null, contentValues);
                this.mListener.reportStoreIsEmpty(false);
            } catch (SQLiteException e2) {
                Log.w("Error storing hit");
            }
        }
    }

    public static String generateHitString(Map<String, String> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry next : map.entrySet()) {
            arrayList.add(((String) next.getKey()) + "=" + HitBuilder.encode((String) next.getValue()));
        }
        return TextUtils.join("&", arrayList);
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x0104  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.google.analytics.tracking.android.Hit> peekHits(int r16) {
        /*
            r15 = this;
            java.lang.String r1 = "Error opening database for peekHits"
            android.database.sqlite.SQLiteDatabase r1 = r15.getWritableDatabase(r1)
            if (r1 != 0) goto L_0x000e
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
        L_0x000d:
            return r1
        L_0x000e:
            r10 = 0
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.lang.String r2 = "hits2"
            r3 = 3
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x00d9, all -> 0x0101 }
            r4 = 0
            java.lang.String r5 = "hit_id"
            r3[r4] = r5     // Catch:{ SQLiteException -> 0x00d9, all -> 0x0101 }
            r4 = 1
            java.lang.String r5 = "hit_time"
            r3[r4] = r5     // Catch:{ SQLiteException -> 0x00d9, all -> 0x0101 }
            r4 = 2
            java.lang.String r5 = "hit_url"
            r3[r4] = r5     // Catch:{ SQLiteException -> 0x00d9, all -> 0x0101 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            java.lang.String r8 = "%s ASC, %s ASC"
            r9 = 2
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ SQLiteException -> 0x00d9, all -> 0x0101 }
            r11 = 0
            java.lang.String r12 = "hit_url"
            r9[r11] = r12     // Catch:{ SQLiteException -> 0x00d9, all -> 0x0101 }
            r11 = 1
            java.lang.String r12 = "hit_id"
            r9[r11] = r12     // Catch:{ SQLiteException -> 0x00d9, all -> 0x0101 }
            java.lang.String r8 = java.lang.String.format(r8, r9)     // Catch:{ SQLiteException -> 0x00d9, all -> 0x0101 }
            java.lang.String r9 = java.lang.Integer.toString(r16)     // Catch:{ SQLiteException -> 0x00d9, all -> 0x0101 }
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x00d9, all -> 0x0101 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x019e, all -> 0x0196 }
            r10.<init>()     // Catch:{ SQLiteException -> 0x019e, all -> 0x0196 }
            boolean r2 = r11.moveToFirst()     // Catch:{ SQLiteException -> 0x019e, all -> 0x0196 }
            if (r2 == 0) goto L_0x0073
        L_0x0052:
            com.google.analytics.tracking.android.Hit r2 = new com.google.analytics.tracking.android.Hit     // Catch:{ SQLiteException -> 0x019e, all -> 0x0196 }
            r3 = 0
            r4 = 0
            long r4 = r11.getLong(r4)     // Catch:{ SQLiteException -> 0x019e, all -> 0x0196 }
            r6 = 1
            long r6 = r11.getLong(r6)     // Catch:{ SQLiteException -> 0x019e, all -> 0x0196 }
            r2.<init>(r3, r4, r6)     // Catch:{ SQLiteException -> 0x019e, all -> 0x0196 }
            r3 = 2
            java.lang.String r3 = r11.getString(r3)     // Catch:{ SQLiteException -> 0x019e, all -> 0x0196 }
            r2.setHitUrl(r3)     // Catch:{ SQLiteException -> 0x019e, all -> 0x0196 }
            r10.add(r2)     // Catch:{ SQLiteException -> 0x019e, all -> 0x0196 }
            boolean r2 = r11.moveToNext()     // Catch:{ SQLiteException -> 0x019e, all -> 0x0196 }
            if (r2 != 0) goto L_0x0052
        L_0x0073:
            if (r11 == 0) goto L_0x0078
            r11.close()
        L_0x0078:
            r12 = 0
            java.lang.String r2 = "hits2"
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x0194 }
            r4 = 0
            java.lang.String r5 = "hit_id"
            r3[r4] = r5     // Catch:{ SQLiteException -> 0x0194 }
            r4 = 1
            java.lang.String r5 = "hit_string"
            r3[r4] = r5     // Catch:{ SQLiteException -> 0x0194 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            java.lang.String r8 = "%s ASC"
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ SQLiteException -> 0x0194 }
            r13 = 0
            java.lang.String r14 = "hit_id"
            r9[r13] = r14     // Catch:{ SQLiteException -> 0x0194 }
            java.lang.String r8 = java.lang.String.format(r8, r9)     // Catch:{ SQLiteException -> 0x0194 }
            java.lang.String r9 = java.lang.Integer.toString(r16)     // Catch:{ SQLiteException -> 0x0194 }
            android.database.Cursor r2 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x0194 }
            boolean r1 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            if (r1 == 0) goto L_0x00d1
            r3 = r12
        L_0x00a9:
            boolean r1 = r2 instanceof android.database.sqlite.SQLiteCursor     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            if (r1 == 0) goto L_0x0175
            r0 = r2
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            r1 = r0
            android.database.CursorWindow r1 = r1.getWindow()     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            int r1 = r1.getNumRows()     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            if (r1 <= 0) goto L_0x0108
            java.lang.Object r1 = r10.get(r3)     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            com.google.analytics.tracking.android.Hit r1 = (com.google.analytics.tracking.android.Hit) r1     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            r4 = 1
            java.lang.String r4 = r2.getString(r4)     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            r1.setHitString(r4)     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
        L_0x00c9:
            int r1 = r3 + 1
            boolean r3 = r2.moveToNext()     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            if (r3 != 0) goto L_0x01a2
        L_0x00d1:
            if (r2 == 0) goto L_0x00d6
            r2.close()
        L_0x00d6:
            r1 = r10
            goto L_0x000d
        L_0x00d9:
            r1 = move-exception
            r2 = r10
        L_0x00db:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x019a }
            r3.<init>()     // Catch:{ all -> 0x019a }
            java.lang.String r4 = "error in peekHits fetching hitIds: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x019a }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x019a }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x019a }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x019a }
            com.google.analytics.tracking.android.Log.w(r1)     // Catch:{ all -> 0x019a }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x019a }
            r1.<init>()     // Catch:{ all -> 0x019a }
            if (r2 == 0) goto L_0x000d
            r2.close()
            goto L_0x000d
        L_0x0101:
            r1 = move-exception
        L_0x0102:
            if (r10 == 0) goto L_0x0107
            r10.close()
        L_0x0107:
            throw r1
        L_0x0108:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            r1.<init>()     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            java.lang.String r4 = "hitString for hitId "
            java.lang.StringBuilder r4 = r1.append(r4)     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            java.lang.Object r1 = r10.get(r3)     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            com.google.analytics.tracking.android.Hit r1 = (com.google.analytics.tracking.android.Hit) r1     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            long r5 = r1.getHitId()     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            java.lang.StringBuilder r1 = r4.append(r5)     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            java.lang.String r4 = " too large.  Hit will be deleted."
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            com.google.analytics.tracking.android.Log.w(r1)     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            goto L_0x00c9
        L_0x012f:
            r1 = move-exception
            r11 = r2
        L_0x0131:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0192 }
            r2.<init>()     // Catch:{ all -> 0x0192 }
            java.lang.String r3 = "error in peekHits fetching hitString: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0192 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0192 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x0192 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0192 }
            com.google.analytics.tracking.android.Log.w(r1)     // Catch:{ all -> 0x0192 }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x0192 }
            r2.<init>()     // Catch:{ all -> 0x0192 }
            r3 = 0
            java.util.Iterator r4 = r10.iterator()     // Catch:{ all -> 0x0192 }
        L_0x0155:
            boolean r1 = r4.hasNext()     // Catch:{ all -> 0x0192 }
            if (r1 == 0) goto L_0x016d
            java.lang.Object r1 = r4.next()     // Catch:{ all -> 0x0192 }
            com.google.analytics.tracking.android.Hit r1 = (com.google.analytics.tracking.android.Hit) r1     // Catch:{ all -> 0x0192 }
            java.lang.String r5 = r1.getHitParams()     // Catch:{ all -> 0x0192 }
            boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x0192 }
            if (r5 == 0) goto L_0x018e
            if (r3 == 0) goto L_0x018d
        L_0x016d:
            if (r11 == 0) goto L_0x0172
            r11.close()
        L_0x0172:
            r1 = r2
            goto L_0x000d
        L_0x0175:
            java.lang.Object r1 = r10.get(r3)     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            com.google.analytics.tracking.android.Hit r1 = (com.google.analytics.tracking.android.Hit) r1     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            r4 = 1
            java.lang.String r4 = r2.getString(r4)     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            r1.setHitString(r4)     // Catch:{ SQLiteException -> 0x012f, all -> 0x0185 }
            goto L_0x00c9
        L_0x0185:
            r1 = move-exception
            r11 = r2
        L_0x0187:
            if (r11 == 0) goto L_0x018c
            r11.close()
        L_0x018c:
            throw r1
        L_0x018d:
            r3 = 1
        L_0x018e:
            r2.add(r1)     // Catch:{ all -> 0x0192 }
            goto L_0x0155
        L_0x0192:
            r1 = move-exception
            goto L_0x0187
        L_0x0194:
            r1 = move-exception
            goto L_0x0131
        L_0x0196:
            r1 = move-exception
            r10 = r11
            goto L_0x0102
        L_0x019a:
            r1 = move-exception
            r10 = r2
            goto L_0x0102
        L_0x019e:
            r1 = move-exception
            r2 = r11
            goto L_0x00db
        L_0x01a2:
            r3 = r1
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.analytics.tracking.android.PersistentAnalyticsStore.peekHits(int):java.util.List");
    }

    /* access modifiers changed from: package-private */
    public int deleteStaleHits() {
        boolean z = true;
        long currentTimeMillis = this.mClock.currentTimeMillis();
        if (currentTimeMillis <= this.mLastDeleteStaleHitsTime + 86400000) {
            return 0;
        }
        this.mLastDeleteStaleHitsTime = currentTimeMillis;
        SQLiteDatabase writableDatabase = getWritableDatabase("Error opening database for deleteStaleHits");
        if (writableDatabase == null) {
            return 0;
        }
        int delete = writableDatabase.delete("hits2", "HIT_TIME < ?", new String[]{Long.toString(this.mClock.currentTimeMillis() - 2592000000L)});
        AnalyticsStoreStateListener analyticsStoreStateListener = this.mListener;
        if (getNumStoredHits() != 0) {
            z = false;
        }
        analyticsStoreStateListener.reportStoreIsEmpty(z);
        return delete;
    }

    public void deleteHits(Collection<Hit> collection) {
        SQLiteDatabase writableDatabase;
        boolean z = false;
        if (collection == null) {
            throw new NullPointerException("hits cannot be null");
        } else if (!collection.isEmpty() && (writableDatabase = getWritableDatabase("Error opening database for deleteHit")) != null) {
            String[] strArr = new String[collection.size()];
            String format = String.format("HIT_ID in (%s)", TextUtils.join(",", Collections.nCopies(strArr.length, "?")));
            int i = 0;
            for (Hit hitId : collection) {
                strArr[i] = Long.toString(hitId.getHitId());
                i++;
            }
            try {
                writableDatabase.delete("hits2", format, strArr);
                AnalyticsStoreStateListener analyticsStoreStateListener = this.mListener;
                if (getNumStoredHits() == 0) {
                    z = true;
                }
                analyticsStoreStateListener.reportStoreIsEmpty(z);
            } catch (SQLiteException e) {
                Log.w("Error deleting hit " + collection);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int getNumStoredHits() {
        Cursor cursor = null;
        int i = 0;
        SQLiteDatabase writableDatabase = getWritableDatabase("Error opening database for requestNumHitsPending");
        if (writableDatabase != null) {
            try {
                Cursor rawQuery = writableDatabase.rawQuery("SELECT COUNT(*) from hits2", null);
                if (rawQuery.moveToFirst()) {
                    i = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e) {
                Log.w("Error getting numStoredHits");
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i;
    }

    public void dispatch() {
        Log.vDebug("dispatch running...");
        if (this.mDispatcher.okToDispatch()) {
            List<Hit> peekHits = peekHits(40);
            if (peekHits.isEmpty()) {
                Log.vDebug("...nothing to dispatch");
                this.mListener.reportStoreIsEmpty(true);
                return;
            }
            int dispatchHits = this.mDispatcher.dispatchHits(peekHits);
            Log.vDebug("sent " + dispatchHits + " of " + peekHits.size() + " hits");
            deleteHits(peekHits.subList(0, Math.min(dispatchHits, peekHits.size())));
            if (dispatchHits == peekHits.size() && getNumStoredHits() > 0) {
                GAServiceManager.getInstance().dispatch();
            }
        }
    }

    private SQLiteDatabase getWritableDatabase(String str) {
        try {
            return this.mDbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Log.w(str);
            return null;
        }
    }

    class AnalyticsDatabaseHelper extends SQLiteOpenHelper {
        private boolean mBadDatabase;
        private long mLastDatabaseCheckTime = 0;

        AnalyticsDatabaseHelper(Context context, String str) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0048  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean tablePresent(java.lang.String r11, android.database.sqlite.SQLiteDatabase r12) {
            /*
                r10 = this;
                r8 = 0
                r9 = 0
                java.lang.String r1 = "SQLITE_MASTER"
                r0 = 1
                java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0026, all -> 0x0045 }
                r0 = 0
                java.lang.String r3 = "name"
                r2[r0] = r3     // Catch:{ SQLiteException -> 0x0026, all -> 0x0045 }
                java.lang.String r3 = "name=?"
                r0 = 1
                java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0026, all -> 0x0045 }
                r0 = 0
                r4[r0] = r11     // Catch:{ SQLiteException -> 0x0026, all -> 0x0045 }
                r5 = 0
                r6 = 0
                r7 = 0
                r0 = r12
                android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0026, all -> 0x0045 }
                boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0053, all -> 0x004c }
                if (r1 == 0) goto L_0x0025
                r1.close()
            L_0x0025:
                return r0
            L_0x0026:
                r0 = move-exception
                r0 = r9
            L_0x0028:
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x004f }
                r1.<init>()     // Catch:{ all -> 0x004f }
                java.lang.String r2 = "error querying for table "
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x004f }
                java.lang.StringBuilder r1 = r1.append(r11)     // Catch:{ all -> 0x004f }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x004f }
                com.google.analytics.tracking.android.Log.w(r1)     // Catch:{ all -> 0x004f }
                if (r0 == 0) goto L_0x0043
                r0.close()
            L_0x0043:
                r0 = r8
                goto L_0x0025
            L_0x0045:
                r0 = move-exception
            L_0x0046:
                if (r9 == 0) goto L_0x004b
                r9.close()
            L_0x004b:
                throw r0
            L_0x004c:
                r0 = move-exception
                r9 = r1
                goto L_0x0046
            L_0x004f:
                r1 = move-exception
                r9 = r0
                r0 = r1
                goto L_0x0046
            L_0x0053:
                r0 = move-exception
                r0 = r1
                goto L_0x0028
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.analytics.tracking.android.PersistentAnalyticsStore.AnalyticsDatabaseHelper.tablePresent(java.lang.String, android.database.sqlite.SQLiteDatabase):boolean");
        }

        public SQLiteDatabase getWritableDatabase() {
            if (!this.mBadDatabase || this.mLastDatabaseCheckTime + 3600000 <= PersistentAnalyticsStore.this.mClock.currentTimeMillis()) {
                SQLiteDatabase sQLiteDatabase = null;
                this.mBadDatabase = true;
                this.mLastDatabaseCheckTime = PersistentAnalyticsStore.this.mClock.currentTimeMillis();
                try {
                    sQLiteDatabase = super.getWritableDatabase();
                } catch (SQLiteException e) {
                    PersistentAnalyticsStore.this.mContext.getDatabasePath(PersistentAnalyticsStore.this.mDatabaseName).delete();
                }
                if (sQLiteDatabase == null) {
                    sQLiteDatabase = super.getWritableDatabase();
                }
                this.mBadDatabase = false;
                return sQLiteDatabase;
            }
            throw new SQLiteException("Database creation failed");
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (Build.VERSION.SDK_INT < 15) {
                Cursor rawQuery = sQLiteDatabase.rawQuery("PRAGMA journal_mode=memory", null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            if (!tablePresent("hits2", sQLiteDatabase)) {
                sQLiteDatabase.execSQL(PersistentAnalyticsStore.CREATE_HITS_TABLE);
            } else {
                validateColumnsPresent(sQLiteDatabase);
            }
        }

        /* JADX INFO: finally extract failed */
        private void validateColumnsPresent(SQLiteDatabase sQLiteDatabase) {
            boolean z = false;
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM hits2 WHERE 0", null);
            HashSet hashSet = new HashSet();
            try {
                String[] columnNames = rawQuery.getColumnNames();
                for (String add : columnNames) {
                    hashSet.add(add);
                }
                rawQuery.close();
                if (!hashSet.remove("hit_id") || !hashSet.remove("hit_url") || !hashSet.remove("hit_string") || !hashSet.remove("hit_time")) {
                    throw new SQLiteException("Database column missing");
                }
                if (!hashSet.remove("hit_app_id")) {
                    z = true;
                }
                if (!hashSet.isEmpty()) {
                    throw new SQLiteException("Database has extra columns");
                } else if (z) {
                    sQLiteDatabase.execSQL("ALTER TABLE hits2 ADD COLUMN hit_app_id");
                }
            } catch (Throwable th) {
                rawQuery.close();
                throw th;
            }
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            FutureApis.setOwnerOnlyReadWrite(sQLiteDatabase.getPath());
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }
}
