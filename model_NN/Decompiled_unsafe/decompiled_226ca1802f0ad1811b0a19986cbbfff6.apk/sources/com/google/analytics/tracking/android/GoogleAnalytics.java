package com.google.analytics.tracking.android;

import android.content.Context;
import com.google.analytics.tracking.android.AnalyticsThread;
import com.google.analytics.tracking.android.GAUsage;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class GoogleAnalytics implements TrackerHandler {
    private static GoogleAnalytics sInstance;
    private AdHitIdGenerator mAdHitIdGenerator;
    /* access modifiers changed from: private */
    public volatile Boolean mAppOptOut;
    /* access modifiers changed from: private */
    public volatile String mClientId;
    private Context mContext;
    private boolean mDebug;
    private Tracker mDefaultTracker;
    private String mLastTrackingId;
    private AnalyticsThread mThread;
    private final Map<String, Tracker> mTrackers;

    public interface AppOptOutCallback {
        void reportAppOptOut(boolean z);
    }

    GoogleAnalytics() {
        this.mTrackers = new HashMap();
    }

    private GoogleAnalytics(Context context) {
        this(context, GAThread.getInstance(context));
    }

    private GoogleAnalytics(Context context, AnalyticsThread analyticsThread) {
        this.mTrackers = new HashMap();
        if (context == null) {
            throw new IllegalArgumentException("context cannot be null");
        }
        this.mContext = context.getApplicationContext();
        this.mThread = analyticsThread;
        this.mAdHitIdGenerator = new AdHitIdGenerator();
        this.mThread.requestAppOptOut(new AppOptOutCallback() {
            public void reportAppOptOut(boolean z) {
                Boolean unused = GoogleAnalytics.this.mAppOptOut = Boolean.valueOf(z);
            }
        });
        this.mThread.requestClientId(new AnalyticsThread.ClientIdCallback() {
            public void reportClientId(String str) {
                String unused = GoogleAnalytics.this.mClientId = str;
            }
        });
    }

    public static GoogleAnalytics getInstance(Context context) {
        GoogleAnalytics googleAnalytics;
        synchronized (GoogleAnalytics.class) {
            if (sInstance == null) {
                sInstance = new GoogleAnalytics(context);
            }
            googleAnalytics = sInstance;
        }
        return googleAnalytics;
    }

    public void setDebug(boolean z) {
        GAUsage.getInstance().setUsage(GAUsage.Field.SET_DEBUG);
        this.mDebug = z;
        Log.setDebug(z);
    }

    public Tracker getTracker(String str) {
        Tracker tracker;
        synchronized (this) {
            if (str == null) {
                throw new IllegalArgumentException("trackingId cannot be null");
            }
            tracker = this.mTrackers.get(str);
            if (tracker == null) {
                tracker = new Tracker(str, this);
                this.mTrackers.put(str, tracker);
                if (this.mDefaultTracker == null) {
                    this.mDefaultTracker = tracker;
                }
            }
            GAUsage.getInstance().setUsage(GAUsage.Field.GET_TRACKER);
        }
        return tracker;
    }

    public void sendHit(Map<String, String> map) {
        synchronized (this) {
            if (map == null) {
                throw new IllegalArgumentException("hit cannot be null");
            }
            map.put("language", Utils.getLanguage(Locale.getDefault()));
            map.put("adSenseAdMobHitId", Integer.toString(this.mAdHitIdGenerator.getAdHitId()));
            map.put("screenResolution", this.mContext.getResources().getDisplayMetrics().widthPixels + "x" + this.mContext.getResources().getDisplayMetrics().heightPixels);
            map.put("usage", GAUsage.getInstance().getAndClearSequence());
            GAUsage.getInstance().getAndClearUsage();
            this.mThread.sendHit(map);
            this.mLastTrackingId = map.get("trackingId");
        }
    }
}
