package com.google.analytics.tracking.android;

import java.util.Map;

interface TrackerHandler {
    void sendHit(Map<String, String> map);
}
