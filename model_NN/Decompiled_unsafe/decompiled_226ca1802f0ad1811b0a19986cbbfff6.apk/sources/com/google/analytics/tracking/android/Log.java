package com.google.analytics.tracking.android;

public class Log {
    private static boolean sDebug;

    public static void setDebug(boolean z) {
        sDebug = z;
    }

    public static boolean isDebugEnabled() {
        return sDebug;
    }

    public static int d(String str) {
        return android.util.Log.d("GAV2", formatMessage(str));
    }

    public static int dDebug(String str) {
        if (sDebug) {
            return d(str);
        }
        return 0;
    }

    public static int e(String str) {
        return android.util.Log.e("GAV2", formatMessage(str));
    }

    public static int i(String str) {
        return android.util.Log.i("GAV2", formatMessage(str));
    }

    public static int iDebug(String str) {
        if (sDebug) {
            return i(str);
        }
        return 0;
    }

    public static int v(String str) {
        return android.util.Log.v("GAV2", formatMessage(str));
    }

    public static int vDebug(String str) {
        if (sDebug) {
            return v(str);
        }
        return 0;
    }

    public static int w(String str) {
        return android.util.Log.w("GAV2", formatMessage(str));
    }

    public static int wDebug(String str) {
        if (sDebug) {
            return w(str);
        }
        return 0;
    }

    private static String formatMessage(String str) {
        return Thread.currentThread().toString() + ": " + str;
    }
}
