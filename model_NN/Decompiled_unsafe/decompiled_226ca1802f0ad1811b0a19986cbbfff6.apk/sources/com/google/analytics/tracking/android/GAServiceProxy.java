package com.google.analytics.tracking.android;

import android.content.Context;
import android.content.Intent;
import com.google.analytics.tracking.android.AnalyticsGmsCoreClient;
import com.google.android.gms.analytics.internal.Command;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

class GAServiceProxy implements AnalyticsGmsCoreClient.OnConnectedListener, AnalyticsGmsCoreClient.OnConnectionFailedListener, ServiceProxy {
    private volatile AnalyticsClient client;
    /* access modifiers changed from: private */
    public Clock clock;
    private volatile int connectTries;
    private final Context ctx;
    /* access modifiers changed from: private */
    public volatile Timer disconnectCheckTimer;
    private volatile Timer failedConnectTimer;
    /* access modifiers changed from: private */
    public long idleTimeout;
    /* access modifiers changed from: private */
    public volatile long lastRequestTime;
    private boolean pendingClearHits;
    private boolean pendingDispatch;
    /* access modifiers changed from: private */
    public final Queue<HitParams> queue;
    private volatile Timer reConnectTimer;
    /* access modifiers changed from: private */
    public volatile ConnectState state;
    private AnalyticsStore store;
    private AnalyticsStore testStore;
    private final AnalyticsThread thread;

    enum ConnectState {
        CONNECTING,
        CONNECTED_SERVICE,
        CONNECTED_LOCAL,
        BLOCKED,
        PENDING_CONNECTION,
        PENDING_DISCONNECT,
        DISCONNECTED
    }

    GAServiceProxy(Context context, AnalyticsThread analyticsThread, AnalyticsStore analyticsStore) {
        this.queue = new ConcurrentLinkedQueue();
        this.idleTimeout = 300000;
        this.testStore = analyticsStore;
        this.ctx = context;
        this.thread = analyticsThread;
        this.clock = new Clock() {
            public long currentTimeMillis() {
                return System.currentTimeMillis();
            }
        };
        this.connectTries = 0;
        this.state = ConnectState.DISCONNECTED;
    }

    GAServiceProxy(Context context, AnalyticsThread analyticsThread) {
        this(context, analyticsThread, null);
    }

    public void putHit(Map<String, String> map, long j, String str, List<Command> list) {
        Log.iDebug("putHit called");
        this.queue.add(new HitParams(map, j, str, list));
        sendQueue();
    }

    public void dispatch() {
        switch (this.state) {
            case CONNECTED_LOCAL:
                dispatchToStore();
                return;
            case CONNECTED_SERVICE:
                return;
            default:
                this.pendingDispatch = true;
                return;
        }
    }

    public void clearHits() {
        Log.iDebug("clearHits called");
        this.queue.clear();
        switch (this.state) {
            case CONNECTED_LOCAL:
                this.store.clearHits(0);
                this.pendingClearHits = false;
                return;
            case CONNECTED_SERVICE:
                this.client.clearHits();
                this.pendingClearHits = false;
                return;
            default:
                this.pendingClearHits = true;
                return;
        }
    }

    private Timer cancelTimer(Timer timer) {
        if (timer == null) {
            return null;
        }
        timer.cancel();
        return null;
    }

    private void clearAllTimers() {
        this.reConnectTimer = cancelTimer(this.reConnectTimer);
        this.failedConnectTimer = cancelTimer(this.failedConnectTimer);
        this.disconnectCheckTimer = cancelTimer(this.disconnectCheckTimer);
    }

    public void createService() {
        if (this.client == null) {
            this.client = new AnalyticsGmsCoreClient(this.ctx, this, this);
            connectToService();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public synchronized void sendQueue() {
        if (Thread.currentThread().equals(this.thread.getThread())) {
            if (this.pendingClearHits) {
                clearHits();
            }
            switch (this.state) {
                case CONNECTED_LOCAL:
                    while (!this.queue.isEmpty()) {
                        HitParams poll = this.queue.poll();
                        Log.iDebug("Sending hit to store");
                        this.store.putHit(poll.getWireFormatParams(), poll.getHitTimeInMilliseconds(), poll.getPath(), poll.getCommands());
                    }
                    if (this.pendingDispatch) {
                        dispatchToStore();
                        break;
                    }
                    break;
                case CONNECTED_SERVICE:
                    while (!this.queue.isEmpty()) {
                        HitParams peek = this.queue.peek();
                        Log.iDebug("Sending hit to service");
                        this.client.sendHit(peek.getWireFormatParams(), peek.getHitTimeInMilliseconds(), peek.getPath(), peek.getCommands());
                        this.queue.poll();
                    }
                    this.lastRequestTime = this.clock.currentTimeMillis();
                    break;
                case DISCONNECTED:
                    Log.iDebug("Need to reconnect");
                    if (!this.queue.isEmpty()) {
                        connectToService();
                        break;
                    }
                    break;
            }
        } else {
            this.thread.getQueue().add(new Runnable() {
                public void run() {
                    GAServiceProxy.this.sendQueue();
                }
            });
        }
    }

    private void dispatchToStore() {
        this.store.dispatch();
        this.pendingDispatch = false;
    }

    /* access modifiers changed from: private */
    public synchronized void useStore() {
        if (this.state != ConnectState.CONNECTED_LOCAL) {
            clearAllTimers();
            Log.iDebug("falling back to local store");
            if (this.testStore != null) {
                this.store = this.testStore;
            } else {
                GAServiceManager instance = GAServiceManager.getInstance();
                instance.initialize(this.ctx, this.thread);
                this.store = instance.getStore();
            }
            this.state = ConnectState.CONNECTED_LOCAL;
            sendQueue();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void connectToService() {
        if (this.client == null || this.state == ConnectState.CONNECTED_LOCAL) {
            Log.w("client not initialized.");
            useStore();
        } else {
            try {
                this.connectTries++;
                cancelTimer(this.failedConnectTimer);
                this.state = ConnectState.CONNECTING;
                this.failedConnectTimer = new Timer("Failed Connect");
                this.failedConnectTimer.schedule(new FailedConnectTask(), 3000);
                Log.iDebug("connecting to Analytics service");
                this.client.connect();
            } catch (SecurityException e) {
                Log.w("security exception on connectToService");
                useStore();
            }
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void disconnectFromService() {
        if (this.client != null && this.state == ConnectState.CONNECTED_SERVICE) {
            this.state = ConnectState.PENDING_DISCONNECT;
            this.client.disconnect();
        }
    }

    public synchronized void onConnected() {
        this.failedConnectTimer = cancelTimer(this.failedConnectTimer);
        this.connectTries = 0;
        Log.iDebug("Connected to service");
        this.state = ConnectState.CONNECTED_SERVICE;
        sendQueue();
        this.disconnectCheckTimer = cancelTimer(this.disconnectCheckTimer);
        this.disconnectCheckTimer = new Timer("disconnect check");
        this.disconnectCheckTimer.schedule(new DisconnectCheckTask(), this.idleTimeout);
    }

    public synchronized void onDisconnected() {
        if (this.state == ConnectState.PENDING_DISCONNECT) {
            Log.iDebug("Disconnected from service");
            clearAllTimers();
            this.state = ConnectState.DISCONNECTED;
        } else {
            Log.iDebug("Unexpected disconnect.");
            this.state = ConnectState.PENDING_CONNECTION;
            if (this.connectTries < 2) {
                fireReconnectAttempt();
            } else {
                useStore();
            }
        }
    }

    public synchronized void onConnectionFailed(int i, Intent intent) {
        this.state = ConnectState.PENDING_CONNECTION;
        if (this.connectTries < 2) {
            Log.w("Service unavailable (code=" + i + "), will retry.");
            fireReconnectAttempt();
        } else {
            Log.w("Service unavailable (code=" + i + "), using local store.");
            useStore();
        }
    }

    private void fireReconnectAttempt() {
        this.reConnectTimer = cancelTimer(this.reConnectTimer);
        this.reConnectTimer = new Timer("Service Reconnect");
        this.reConnectTimer.schedule(new ReconnectTask(), 5000);
    }

    class FailedConnectTask extends TimerTask {
        private FailedConnectTask() {
        }

        public void run() {
            if (GAServiceProxy.this.state == ConnectState.CONNECTING) {
                GAServiceProxy.this.useStore();
            }
        }
    }

    class ReconnectTask extends TimerTask {
        private ReconnectTask() {
        }

        public void run() {
            GAServiceProxy.this.connectToService();
        }
    }

    class DisconnectCheckTask extends TimerTask {
        private DisconnectCheckTask() {
        }

        public void run() {
            if (GAServiceProxy.this.state != ConnectState.CONNECTED_SERVICE || !GAServiceProxy.this.queue.isEmpty() || GAServiceProxy.this.lastRequestTime + GAServiceProxy.this.idleTimeout >= GAServiceProxy.this.clock.currentTimeMillis()) {
                GAServiceProxy.this.disconnectCheckTimer.schedule(new DisconnectCheckTask(), GAServiceProxy.this.idleTimeout);
                return;
            }
            Log.iDebug("Disconnecting due to inactivity");
            GAServiceProxy.this.disconnectFromService();
        }
    }

    class HitParams {
        private final List<Command> commands;
        private final long hitTimeInMilliseconds;
        private final String path;
        private final Map<String, String> wireFormatParams;

        public HitParams(Map<String, String> map, long j, String str, List<Command> list) {
            this.wireFormatParams = map;
            this.hitTimeInMilliseconds = j;
            this.path = str;
            this.commands = list;
        }

        public Map<String, String> getWireFormatParams() {
            return this.wireFormatParams;
        }

        public long getHitTimeInMilliseconds() {
            return this.hitTimeInMilliseconds;
        }

        public String getPath() {
            return this.path;
        }

        public List<Command> getCommands() {
            return this.commands;
        }
    }
}
