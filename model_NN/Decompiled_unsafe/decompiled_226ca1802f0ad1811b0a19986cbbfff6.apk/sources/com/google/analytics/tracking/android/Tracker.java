package com.google.analytics.tracking.android;

import android.text.TextUtils;
import com.google.analytics.tracking.android.GAUsage;
import com.google.analytics.tracking.android.Transaction;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Tracker {
    private static final DecimalFormat DF = new DecimalFormat("0.######", new DecimalFormatSymbols(Locale.US));
    private volatile ExceptionParser mExceptionParser;
    private final TrackerHandler mHandler;
    private boolean mIsThrottlingEnabled;
    private volatile boolean mIsTrackerClosed;
    private volatile boolean mIsTrackingStarted;
    private long mLastTrackTime;
    private final SimpleModel mModel;
    private long mTokens;

    Tracker() {
        this.mIsTrackerClosed = false;
        this.mIsTrackingStarted = false;
        this.mTokens = 120000;
        this.mIsThrottlingEnabled = true;
        this.mHandler = null;
        this.mModel = null;
    }

    Tracker(String str, TrackerHandler trackerHandler) {
        this.mIsTrackerClosed = false;
        this.mIsTrackingStarted = false;
        this.mTokens = 120000;
        this.mIsThrottlingEnabled = true;
        if (str == null) {
            throw new IllegalArgumentException("trackingId cannot be null");
        }
        this.mHandler = trackerHandler;
        this.mModel = new SimpleModel();
        this.mModel.set("trackingId", str);
        this.mModel.set("sampleRate", "100");
        this.mModel.setForNextHit("sessionControl", "start");
        this.mModel.set("useSecure", Boolean.toString(true));
    }

    private void assertTrackerOpen() {
        if (this.mIsTrackerClosed) {
            throw new IllegalStateException("Tracker closed");
        }
    }

    public void setStartSession(boolean z) {
        assertTrackerOpen();
        GAUsage.getInstance().setUsage(GAUsage.Field.SET_START_SESSION);
        this.mModel.setForNextHit("sessionControl", z ? "start" : null);
    }

    public void setAppName(String str) {
        if (this.mIsTrackingStarted) {
            Log.wDebug("Tracking already started, setAppName call ignored");
        } else if (TextUtils.isEmpty(str)) {
            Log.wDebug("setting appName to empty value not allowed, call ignored");
        } else {
            GAUsage.getInstance().setUsage(GAUsage.Field.SET_APP_NAME);
            this.mModel.set("appName", str);
        }
    }

    public void setAppVersion(String str) {
        if (this.mIsTrackingStarted) {
            Log.wDebug("Tracking already started, setAppVersion call ignored");
            return;
        }
        GAUsage.getInstance().setUsage(GAUsage.Field.SET_APP_VERSION);
        this.mModel.set("appVersion", str);
    }

    public void sendView(String str) {
        assertTrackerOpen();
        if (TextUtils.isEmpty(str)) {
            throw new IllegalStateException("trackView requires a appScreen to be set");
        }
        GAUsage.getInstance().setUsage(GAUsage.Field.TRACK_VIEW_WITH_APPSCREEN);
        this.mModel.set("description", str);
        internalSend("appview", null);
    }

    public void sendEvent(String str, String str2, String str3, Long l) {
        assertTrackerOpen();
        GAUsage.getInstance().setUsage(GAUsage.Field.TRACK_EVENT);
        GAUsage.getInstance().setDisableUsage(true);
        internalSend("event", constructEvent(str, str2, str3, l));
        GAUsage.getInstance().setDisableUsage(false);
    }

    public void sendTransaction(Transaction transaction) {
        assertTrackerOpen();
        GAUsage.getInstance().setUsage(GAUsage.Field.TRACK_TRANSACTION);
        GAUsage.getInstance().setDisableUsage(true);
        internalSend("tran", constructTransaction(transaction));
        for (Transaction.Item constructItem : transaction.getItems()) {
            internalSend("item", constructItem(constructItem, transaction));
        }
        GAUsage.getInstance().setDisableUsage(false);
    }

    public void sendException(String str, boolean z) {
        assertTrackerOpen();
        GAUsage.getInstance().setUsage(GAUsage.Field.TRACK_EXCEPTION_WITH_DESCRIPTION);
        GAUsage.getInstance().setDisableUsage(true);
        internalSend("exception", constructException(str, z));
        GAUsage.getInstance().setDisableUsage(false);
    }

    public void sendException(String str, Throwable th, boolean z) {
        String str2;
        assertTrackerOpen();
        GAUsage.getInstance().setUsage(GAUsage.Field.TRACK_EXCEPTION_WITH_THROWABLE);
        if (this.mExceptionParser != null) {
            str2 = this.mExceptionParser.getDescription(str, th);
        } else {
            try {
                GAUsage.getInstance().setDisableUsage(true);
                internalSend("exception", constructRawException(str, th, z));
                GAUsage.getInstance().setDisableUsage(false);
                return;
            } catch (IOException e) {
                Log.w("trackException: couldn't serialize, sending \"Unknown Exception\"");
                str2 = "Unknown Exception";
            }
        }
        GAUsage.getInstance().setDisableUsage(true);
        sendException(str2, z);
        GAUsage.getInstance().setDisableUsage(false);
    }

    private void internalSend(String str, Map<String, String> map) {
        this.mIsTrackingStarted = true;
        if (map == null) {
            map = new HashMap<>();
        }
        map.put("hitType", str);
        this.mModel.setAll(map, true);
        if (!tokensAvailable()) {
            Log.wDebug("Too many hits sent too quickly, throttling invoked.");
        } else {
            this.mHandler.sendHit(this.mModel.getKeysAndValues());
        }
        this.mModel.clearTemporaryValues();
    }

    public void setAnonymizeIp(boolean z) {
        GAUsage.getInstance().setUsage(GAUsage.Field.SET_ANONYMIZE_IP);
        this.mModel.set("anonymizeIp", Boolean.toString(z));
    }

    public void setSampleRate(double d) {
        GAUsage.getInstance().setUsage(GAUsage.Field.SET_SAMPLE_RATE);
        this.mModel.set("sampleRate", Double.toString(d));
    }

    public Map<String, String> constructEvent(String str, String str2, String str3, Long l) {
        HashMap hashMap = new HashMap();
        hashMap.put("eventCategory", str);
        hashMap.put("eventAction", str2);
        hashMap.put("eventLabel", str3);
        if (l != null) {
            hashMap.put("eventValue", Long.toString(l.longValue()));
        }
        GAUsage.getInstance().setUsage(GAUsage.Field.CONSTRUCT_EVENT);
        return hashMap;
    }

    private static String microsToCurrencyString(long j) {
        return DF.format(((double) j) / 1000000.0d);
    }

    public Map<String, String> constructTransaction(Transaction transaction) {
        HashMap hashMap = new HashMap();
        hashMap.put("transactionId", transaction.getTransactionId());
        hashMap.put("transactionAffiliation", transaction.getAffiliation());
        hashMap.put("transactionShipping", microsToCurrencyString(transaction.getShippingCostInMicros()));
        hashMap.put("transactionTax", microsToCurrencyString(transaction.getTotalTaxInMicros()));
        hashMap.put("transactionTotal", microsToCurrencyString(transaction.getTotalCostInMicros()));
        hashMap.put("currencyCode", transaction.getCurrencyCode());
        GAUsage.getInstance().setUsage(GAUsage.Field.CONSTRUCT_TRANSACTION);
        return hashMap;
    }

    private Map<String, String> constructItem(Transaction.Item item, Transaction transaction) {
        HashMap hashMap = new HashMap();
        hashMap.put("transactionId", transaction.getTransactionId());
        hashMap.put("currencyCode", transaction.getCurrencyCode());
        hashMap.put("itemCode", item.getSKU());
        hashMap.put("itemName", item.getName());
        hashMap.put("itemCategory", item.getCategory());
        hashMap.put("itemPrice", microsToCurrencyString(item.getPriceInMicros()));
        hashMap.put("itemQuantity", Long.toString(item.getQuantity()));
        GAUsage.getInstance().setUsage(GAUsage.Field.CONSTRUCT_ITEM);
        return hashMap;
    }

    public Map<String, String> constructException(String str, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("exDescription", str);
        hashMap.put("exFatal", Boolean.toString(z));
        GAUsage.getInstance().setUsage(GAUsage.Field.CONSTRUCT_EXCEPTION);
        return hashMap;
    }

    public Map<String, String> constructRawException(String str, Throwable th, boolean z) {
        HashMap hashMap = new HashMap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(th);
        objectOutputStream.close();
        hashMap.put("rawException", Utils.hexEncode(byteArrayOutputStream.toByteArray()));
        if (str != null) {
            hashMap.put("exceptionThreadName", str);
        }
        hashMap.put("exFatal", Boolean.toString(z));
        GAUsage.getInstance().setUsage(GAUsage.Field.CONSTRUCT_RAW_EXCEPTION);
        return hashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* access modifiers changed from: package-private */
    public synchronized boolean tokensAvailable() {
        boolean z = true;
        synchronized (this) {
            if (this.mIsThrottlingEnabled) {
                long currentTimeMillis = System.currentTimeMillis();
                if (this.mTokens < 120000) {
                    long j = currentTimeMillis - this.mLastTrackTime;
                    if (j > 0) {
                        this.mTokens = Math.min(120000L, j + this.mTokens);
                    }
                }
                this.mLastTrackTime = currentTimeMillis;
                if (this.mTokens >= 2000) {
                    this.mTokens -= 2000;
                } else {
                    Log.wDebug("Excessive tracking detected.  Tracking call ignored.");
                    z = false;
                }
            }
        }
        return z;
    }

    class SimpleModel {
        private Map<String, String> permanentMap;
        private Map<String, String> temporaryMap;

        private SimpleModel() {
            this.temporaryMap = new HashMap();
            this.permanentMap = new HashMap();
        }

        public synchronized void setForNextHit(String str, String str2) {
            this.temporaryMap.put(str, str2);
        }

        public synchronized void set(String str, String str2) {
            this.permanentMap.put(str, str2);
        }

        public synchronized void clearTemporaryValues() {
            this.temporaryMap.clear();
        }

        public synchronized void setAll(Map<String, String> map, Boolean bool) {
            if (bool.booleanValue()) {
                this.temporaryMap.putAll(map);
            } else {
                this.permanentMap.putAll(map);
            }
        }

        public synchronized Map<String, String> getKeysAndValues() {
            HashMap hashMap;
            hashMap = new HashMap(this.permanentMap);
            hashMap.putAll(this.temporaryMap);
            return hashMap;
        }
    }
}
