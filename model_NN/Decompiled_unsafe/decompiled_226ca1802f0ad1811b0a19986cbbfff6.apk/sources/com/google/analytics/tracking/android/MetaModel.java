package com.google.analytics.tracking.android;

import java.util.HashMap;
import java.util.Map;

class MetaModel {
    private Map<String, MetaInfo> mMetaInfos = new HashMap();

    public interface Formatter {
        String format(String str);
    }

    MetaModel() {
    }

    /* access modifiers changed from: package-private */
    public MetaInfo getMetaInfo(String str) {
        if (str.startsWith("&")) {
            return new MetaInfo(str.substring(1), null, null);
        }
        if (str.contains("*")) {
            str = str.substring(0, str.indexOf("*"));
        }
        return this.mMetaInfos.get(str);
    }

    public void addField(String str, String str2, String str3, Formatter formatter) {
        this.mMetaInfos.put(str, new MetaInfo(str2, str3, formatter));
    }

    public class MetaInfo {
        private final String mDefaultValue;
        private final Formatter mFormatter;
        private final String mUrlParam;

        public MetaInfo(String str, String str2, Formatter formatter) {
            this.mUrlParam = str;
            this.mDefaultValue = str2;
            this.mFormatter = formatter;
        }

        public String getUrlParam(String str) {
            if (!str.contains("*")) {
                return this.mUrlParam;
            }
            String str2 = this.mUrlParam;
            String[] split = str.split("\\*");
            if (split.length <= 1) {
                return null;
            }
            try {
                return str2 + Integer.parseInt(split[1]);
            } catch (NumberFormatException e) {
                Log.w("Unable to parse slot for url parameter " + str2);
                return null;
            }
        }

        public String getDefaultValue() {
            return this.mDefaultValue;
        }

        public Formatter getFormatter() {
            return this.mFormatter;
        }
    }
}
