package com.mixpanel.android.a;

import android.support.v4.app.NotificationCompat;

/* compiled from: Base64Coder */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static char[] f1999a = new char[64];

    /* renamed from: b  reason: collision with root package name */
    private static byte[] f2000b = new byte[NotificationCompat.FLAG_HIGH_PRIORITY];

    static {
        char c2 = 'A';
        int i = 0;
        while (c2 <= 'Z') {
            f1999a[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = 'a';
        while (c3 <= 'z') {
            f1999a[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        char c4 = '0';
        while (c4 <= '9') {
            f1999a[i] = c4;
            c4 = (char) (c4 + 1);
            i++;
        }
        int i2 = i + 1;
        f1999a[i] = '+';
        int i3 = i2 + 1;
        f1999a[i2] = '/';
        for (int i4 = 0; i4 < f2000b.length; i4++) {
            f2000b[i4] = -1;
        }
        for (int i5 = 0; i5 < 64; i5++) {
            f2000b[f1999a[i5]] = (byte) i5;
        }
    }

    public static String a(String str) {
        return new String(a(str.getBytes()));
    }

    public static char[] a(byte[] bArr) {
        return a(bArr, bArr.length);
    }

    public static char[] a(byte[] bArr, int i) {
        byte b2;
        byte b3;
        char c2;
        char c3;
        int i2 = ((i * 4) + 2) / 3;
        char[] cArr = new char[(((i + 2) / 3) * 4)];
        int i3 = 0;
        int i4 = 0;
        while (i4 < i) {
            int i5 = i4 + 1;
            byte b4 = bArr[i4] & 255;
            if (i5 < i) {
                b2 = bArr[i5] & 255;
                i5++;
            } else {
                b2 = 0;
            }
            if (i5 < i) {
                i4 = i5 + 1;
                b3 = bArr[i5] & 255;
            } else {
                i4 = i5;
                b3 = 0;
            }
            int i6 = b4 >>> 2;
            int i7 = ((b4 & 3) << 4) | (b2 >>> 4);
            int i8 = ((b2 & 15) << 2) | (b3 >>> 6);
            byte b5 = b3 & 63;
            int i9 = i3 + 1;
            cArr[i3] = f1999a[i6];
            int i10 = i9 + 1;
            cArr[i9] = f1999a[i7];
            if (i10 < i2) {
                c2 = f1999a[i8];
            } else {
                c2 = '=';
            }
            cArr[i10] = c2;
            int i11 = i10 + 1;
            if (i11 < i2) {
                c3 = f1999a[b5];
            } else {
                c3 = '=';
            }
            cArr[i11] = c3;
            i3 = i11 + 1;
        }
        return cArr;
    }
}
