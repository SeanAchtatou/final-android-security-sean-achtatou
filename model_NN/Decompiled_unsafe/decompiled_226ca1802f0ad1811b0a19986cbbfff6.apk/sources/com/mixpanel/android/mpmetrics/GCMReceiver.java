package com.mixpanel.android.mpmetrics;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

public class GCMReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    String f2001a = "MPGCMReceiver";

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
            a(intent);
        } else if ("com.google.android.c2dm.intent.RECEIVE".equals(action)) {
            a(context, intent);
        }
    }

    private void a(Intent intent) {
        String stringExtra = intent.getStringExtra("registration_id");
        if (intent.getStringExtra("error") != null) {
            Log.e(this.f2001a, "Error when registering for GCM: " + intent.getStringExtra("error"));
        } else if (stringExtra != null) {
            l.a(new e(this, stringExtra));
        } else if (intent.getStringExtra("unregistered") != null) {
            l.a(new f(this));
        }
    }

    private void a(Context context, Intent intent) {
        CharSequence charSequence;
        int i;
        String string = intent.getExtras().getString("mp_message");
        if (string != null) {
            PackageManager packageManager = context.getPackageManager();
            Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(context.getPackageName());
            CharSequence charSequence2 = "";
            try {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 0);
                charSequence2 = packageManager.getApplicationLabel(applicationInfo);
                charSequence = charSequence2;
                i = applicationInfo.icon;
            } catch (PackageManager.NameNotFoundException e) {
                charSequence = charSequence2;
                i = 17301651;
            }
            PendingIntent activity = PendingIntent.getActivity(context.getApplicationContext(), 0, launchIntentForPackage, 134217728);
            Notification notification = new Notification(i, string, System.currentTimeMillis());
            notification.flags |= 16;
            notification.setLatestEventInfo(context, charSequence, string, activity);
            ((NotificationManager) context.getSystemService("notification")).notify(0, notification);
        }
    }
}
