package com.mixpanel.android.mpmetrics;

/* compiled from: MPDbAdapter */
public enum k {
    EVENTS("events"),
    PEOPLE("people");
    

    /* renamed from: c  reason: collision with root package name */
    private final String f2027c;

    private k(String str) {
        this.f2027c = str;
    }

    public String a() {
        return this.f2027c;
    }
}
