package com.mixpanel.android.mpmetrics;

import android.os.Handler;

/* compiled from: AnalyticsMessages */
class d extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f2010a;

    /* renamed from: b  reason: collision with root package name */
    private String f2011b = "https://api.mixpanel.com";

    /* renamed from: c  reason: collision with root package name */
    private String f2012c = "http://api.mixpanel.com";
    private final i d;

    public d(b bVar) {
        this.f2010a = bVar;
        this.d = bVar.f2005a.b(bVar.f2005a.f2004c);
        this.d.a(System.currentTimeMillis() - 172800000, k.EVENTS);
        this.d.a(System.currentTimeMillis() - 172800000, k.PEOPLE);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r6) {
        /*
            r5 = this;
            r0 = 0
            r1 = -1
            int r2 = r6.what     // Catch:{ RuntimeException -> 0x0193 }
            int r3 = com.mixpanel.android.mpmetrics.a.g     // Catch:{ RuntimeException -> 0x0193 }
            if (r2 != r3) goto L_0x004f
            java.lang.Object r0 = r6.obj     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.b r2 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.a r2 = r2.f2005a     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0193 }
            r3.<init>()     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r4 = "Changing flush interval to "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r3 = r3.toString()     // Catch:{ RuntimeException -> 0x0193 }
            r2.a(r3)     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.b r2 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            long r3 = r0.longValue()     // Catch:{ RuntimeException -> 0x0193 }
            long unused = r2.d = r3     // Catch:{ RuntimeException -> 0x0193 }
            int r0 = com.mixpanel.android.mpmetrics.a.f     // Catch:{ RuntimeException -> 0x0193 }
            r5.removeMessages(r0)     // Catch:{ RuntimeException -> 0x0193 }
            r0 = r1
        L_0x0039:
            r1 = 40
            if (r0 < r1) goto L_0x01cc
            com.mixpanel.android.mpmetrics.b r0 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.a r0 = r0.f2005a     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r1 = "Flushing queue due to bulk upload limit"
            r0.a(r1)     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.b r0 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            r0.c()     // Catch:{ RuntimeException -> 0x0193 }
            r5.a()     // Catch:{ RuntimeException -> 0x0193 }
        L_0x004e:
            return
        L_0x004f:
            int r2 = r6.what     // Catch:{ RuntimeException -> 0x0193 }
            int r3 = com.mixpanel.android.mpmetrics.a.i     // Catch:{ RuntimeException -> 0x0193 }
            if (r2 != r3) goto L_0x0082
            com.mixpanel.android.mpmetrics.b r2 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.a r2 = r2.f2005a     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0193 }
            r3.<init>()     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r4 = "Setting endpoint API host to "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r4 = r5.f2011b     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r3 = r3.toString()     // Catch:{ RuntimeException -> 0x0193 }
            r2.a(r3)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.Object r2 = r6.obj     // Catch:{ RuntimeException -> 0x0193 }
            if (r2 != 0) goto L_0x007b
        L_0x0077:
            r5.f2011b = r0     // Catch:{ RuntimeException -> 0x0193 }
            r0 = r1
            goto L_0x0039
        L_0x007b:
            java.lang.Object r0 = r6.obj     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r0 = r0.toString()     // Catch:{ RuntimeException -> 0x0193 }
            goto L_0x0077
        L_0x0082:
            int r2 = r6.what     // Catch:{ RuntimeException -> 0x0193 }
            int r3 = com.mixpanel.android.mpmetrics.a.j     // Catch:{ RuntimeException -> 0x0193 }
            if (r2 != r3) goto L_0x00b5
            com.mixpanel.android.mpmetrics.b r2 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.a r2 = r2.f2005a     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0193 }
            r3.<init>()     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r4 = "Setting fallback API host to "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r4 = r5.f2012c     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r3 = r3.toString()     // Catch:{ RuntimeException -> 0x0193 }
            r2.a(r3)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.Object r2 = r6.obj     // Catch:{ RuntimeException -> 0x0193 }
            if (r2 != 0) goto L_0x00ae
        L_0x00aa:
            r5.f2012c = r0     // Catch:{ RuntimeException -> 0x0193 }
            r0 = r1
            goto L_0x0039
        L_0x00ae:
            java.lang.Object r0 = r6.obj     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r0 = r0.toString()     // Catch:{ RuntimeException -> 0x0193 }
            goto L_0x00aa
        L_0x00b5:
            int r0 = r6.what     // Catch:{ RuntimeException -> 0x0193 }
            int r2 = com.mixpanel.android.mpmetrics.a.d     // Catch:{ RuntimeException -> 0x0193 }
            if (r0 != r2) goto L_0x00f2
            java.lang.Object r0 = r6.obj     // Catch:{ RuntimeException -> 0x0193 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.b r1 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.a r1 = r1.f2005a     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r2 = "Queuing people record for sending later"
            r1.a(r2)     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.b r1 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.a r1 = r1.f2005a     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0193 }
            r2.<init>()     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r3 = "    "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r3 = r0.toString()     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r2 = r2.toString()     // Catch:{ RuntimeException -> 0x0193 }
            r1.a(r2)     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.i r1 = r5.d     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.k r2 = com.mixpanel.android.mpmetrics.k.PEOPLE     // Catch:{ RuntimeException -> 0x0193 }
            int r0 = r1.a(r0, r2)     // Catch:{ RuntimeException -> 0x0193 }
            goto L_0x0039
        L_0x00f2:
            int r0 = r6.what     // Catch:{ RuntimeException -> 0x0193 }
            int r2 = com.mixpanel.android.mpmetrics.a.e     // Catch:{ RuntimeException -> 0x0193 }
            if (r0 != r2) goto L_0x012f
            java.lang.Object r0 = r6.obj     // Catch:{ RuntimeException -> 0x0193 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.b r1 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.a r1 = r1.f2005a     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r2 = "Queuing event for sending later"
            r1.a(r2)     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.b r1 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.a r1 = r1.f2005a     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0193 }
            r2.<init>()     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r3 = "    "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r3 = r0.toString()     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r2 = r2.toString()     // Catch:{ RuntimeException -> 0x0193 }
            r1.a(r2)     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.i r1 = r5.d     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.k r2 = com.mixpanel.android.mpmetrics.k.EVENTS     // Catch:{ RuntimeException -> 0x0193 }
            int r0 = r1.a(r0, r2)     // Catch:{ RuntimeException -> 0x0193 }
            goto L_0x0039
        L_0x012f:
            int r0 = r6.what     // Catch:{ RuntimeException -> 0x0193 }
            int r2 = com.mixpanel.android.mpmetrics.a.f     // Catch:{ RuntimeException -> 0x0193 }
            if (r0 != r2) goto L_0x014b
            com.mixpanel.android.mpmetrics.b r0 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.a r0 = r0.f2005a     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r2 = "Flushing queue due to scheduled or forced flush"
            r0.a(r2)     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.b r0 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            r0.c()     // Catch:{ RuntimeException -> 0x0193 }
            r5.a()     // Catch:{ RuntimeException -> 0x0193 }
            r0 = r1
            goto L_0x0039
        L_0x014b:
            int r0 = r6.what     // Catch:{ RuntimeException -> 0x0193 }
            int r2 = com.mixpanel.android.mpmetrics.a.h     // Catch:{ RuntimeException -> 0x0193 }
            if (r0 != r2) goto L_0x01b1
            java.lang.String r0 = "MixpanelAPI"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0193 }
            r2.<init>()     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r3 = "Worker recieved a hard kill. Dumping all events and force-killing. Thread id "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.Thread r3 = java.lang.Thread.currentThread()     // Catch:{ RuntimeException -> 0x0193 }
            long r3 = r3.getId()     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r2 = r2.toString()     // Catch:{ RuntimeException -> 0x0193 }
            android.util.Log.w(r0, r2)     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.b r0 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.Object r2 = r0.f2006b     // Catch:{ RuntimeException -> 0x0193 }
            monitor-enter(r2)     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.i r0 = r5.d     // Catch:{ all -> 0x0190 }
            r0.a()     // Catch:{ all -> 0x0190 }
            com.mixpanel.android.mpmetrics.b r0 = r5.f2010a     // Catch:{ all -> 0x0190 }
            r3 = 0
            android.os.Handler unused = r0.f2007c = r3     // Catch:{ all -> 0x0190 }
            android.os.Looper r0 = android.os.Looper.myLooper()     // Catch:{ all -> 0x0190 }
            r0.quit()     // Catch:{ all -> 0x0190 }
            monitor-exit(r2)     // Catch:{ all -> 0x0190 }
            r0 = r1
            goto L_0x0039
        L_0x0190:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0190 }
            throw r0     // Catch:{ RuntimeException -> 0x0193 }
        L_0x0193:
            r0 = move-exception
            java.lang.String r1 = "MixpanelAPI"
            java.lang.String r2 = "Worker threw an unhandled exception- will not send any more mixpanel messages"
            android.util.Log.e(r1, r2, r0)
            com.mixpanel.android.mpmetrics.b r1 = r5.f2010a
            java.lang.Object r2 = r1.f2006b
            monitor-enter(r2)
            com.mixpanel.android.mpmetrics.b r1 = r5.f2010a     // Catch:{ all -> 0x021a }
            r3 = 0
            android.os.Handler unused = r1.f2007c = r3     // Catch:{ all -> 0x021a }
            android.os.Looper r1 = android.os.Looper.myLooper()     // Catch:{ Exception -> 0x0211 }
            r1.quit()     // Catch:{ Exception -> 0x0211 }
        L_0x01af:
            monitor-exit(r2)     // Catch:{ all -> 0x021a }
            throw r0
        L_0x01b1:
            java.lang.String r0 = "MixpanelAPI"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0193 }
            r2.<init>()     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r3 = "Unexpected message recieved by Mixpanel worker: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r2 = r2.toString()     // Catch:{ RuntimeException -> 0x0193 }
            android.util.Log.e(r0, r2)     // Catch:{ RuntimeException -> 0x0193 }
            r0 = r1
            goto L_0x0039
        L_0x01cc:
            if (r0 <= 0) goto L_0x004e
            int r1 = com.mixpanel.android.mpmetrics.a.f     // Catch:{ RuntimeException -> 0x0193 }
            boolean r1 = r5.hasMessages(r1)     // Catch:{ RuntimeException -> 0x0193 }
            if (r1 != 0) goto L_0x004e
            com.mixpanel.android.mpmetrics.b r1 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.a r1 = r1.f2005a     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0193 }
            r2.<init>()     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r3 = "Queue depth "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r2 = " - Adding flush in "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.b r2 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            long r2 = r2.d     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ RuntimeException -> 0x0193 }
            java.lang.String r0 = r0.toString()     // Catch:{ RuntimeException -> 0x0193 }
            r1.a(r0)     // Catch:{ RuntimeException -> 0x0193 }
            int r0 = com.mixpanel.android.mpmetrics.a.f     // Catch:{ RuntimeException -> 0x0193 }
            com.mixpanel.android.mpmetrics.b r1 = r5.f2010a     // Catch:{ RuntimeException -> 0x0193 }
            long r1 = r1.d     // Catch:{ RuntimeException -> 0x0193 }
            r5.sendEmptyMessageDelayed(r0, r1)     // Catch:{ RuntimeException -> 0x0193 }
            goto L_0x004e
        L_0x0211:
            r1 = move-exception
            java.lang.String r3 = "MixpanelAPI"
            java.lang.String r4 = "Could not halt looper"
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x021a }
            goto L_0x01af
        L_0x021a:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x021a }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mixpanel.android.mpmetrics.d.handleMessage(android.os.Message):void");
    }

    private void a() {
        this.f2010a.f2005a.a("Sending records to Mixpanel");
        a(k.EVENTS, "/track?ip=1");
        a(k.PEOPLE, "/engage");
    }

    private void a(k kVar, String str) {
        String[] a2 = this.d.a(kVar);
        if (a2 != null) {
            String str2 = a2[0];
            String str3 = a2[1];
            h a3 = this.f2010a.f2005a.a(this.f2011b, this.f2012c).a(str3, str);
            if (a3 == h.SUCCEEDED) {
                this.f2010a.f2005a.a("Posted to " + str);
                this.f2010a.f2005a.a("Sent Message\n" + str3);
                this.d.a(str2, kVar);
            } else if (a3 != h.FAILED_RECOVERABLE) {
                this.d.a(str2, kVar);
            } else if (!hasMessages(a.f)) {
                sendEmptyMessageDelayed(a.f, this.f2010a.d);
            }
        }
    }
}
