package com.mixpanel.android.mpmetrics;

import android.content.Context;
import android.os.Message;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

/* compiled from: AnalyticsMessages */
class a {
    /* access modifiers changed from: private */
    public static int d = 0;
    /* access modifiers changed from: private */
    public static int e = 1;
    /* access modifiers changed from: private */
    public static int f = 2;
    /* access modifiers changed from: private */
    public static int g = 4;
    /* access modifiers changed from: private */
    public static int h = 5;
    /* access modifiers changed from: private */
    public static int i = 6;
    /* access modifiers changed from: private */
    public static int j = 7;
    private static final Map<Context, a> k = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    private final AtomicBoolean f2002a = new AtomicBoolean(false);

    /* renamed from: b  reason: collision with root package name */
    private final b f2003b = new b(this);
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final Context f2004c;

    a(Context context) {
        this.f2004c = context;
    }

    public static a a(Context context) {
        a aVar;
        synchronized (k) {
            Context applicationContext = context.getApplicationContext();
            if (!k.containsKey(applicationContext)) {
                aVar = new a(applicationContext);
                k.put(applicationContext, aVar);
            } else {
                aVar = k.get(applicationContext);
            }
        }
        return aVar;
    }

    public void a(JSONObject jSONObject) {
        Message obtain = Message.obtain();
        obtain.what = e;
        obtain.obj = jSONObject;
        this.f2003b.a(obtain);
    }

    public void b(JSONObject jSONObject) {
        Message obtain = Message.obtain();
        obtain.what = d;
        obtain.obj = jSONObject;
        this.f2003b.a(obtain);
    }

    /* access modifiers changed from: protected */
    public i b(Context context) {
        return new i(context);
    }

    /* access modifiers changed from: protected */
    public g a(String str, String str2) {
        return new g(str, str2);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        if (this.f2002a.get()) {
            Log.i("MixpanelAPI", str + " (Thread " + Thread.currentThread().getId() + ")");
        }
    }
}
