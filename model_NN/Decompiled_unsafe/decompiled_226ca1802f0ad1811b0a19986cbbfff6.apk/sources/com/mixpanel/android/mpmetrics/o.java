package com.mixpanel.android.mpmetrics;

import java.util.Map;
import org.json.JSONObject;

/* compiled from: MixpanelAPI */
public interface o {
    void a();

    void a(String str);

    void a(String str, Object obj);

    void a(Map<String, ? extends Number> map);

    void a(JSONObject jSONObject);

    void b(String str);

    void b(String str, Object obj);
}
