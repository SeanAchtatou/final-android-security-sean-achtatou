package com.mixpanel.android.mpmetrics;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.File;

/* compiled from: MPDbAdapter */
class j extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private final File f2024a;

    j(Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 4);
        this.f2024a = context.getDatabasePath(str);
    }

    public void a() {
        close();
        this.f2024a.delete();
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL(i.f2021a);
        sQLiteDatabase.execSQL(i.f2022b);
        sQLiteDatabase.execSQL(i.f2023c);
        sQLiteDatabase.execSQL(i.d);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + k.EVENTS.a());
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + k.PEOPLE.a());
        sQLiteDatabase.execSQL(i.f2021a);
        sQLiteDatabase.execSQL(i.f2022b);
        sQLiteDatabase.execSQL(i.f2023c);
        sQLiteDatabase.execSQL(i.d);
    }
}
