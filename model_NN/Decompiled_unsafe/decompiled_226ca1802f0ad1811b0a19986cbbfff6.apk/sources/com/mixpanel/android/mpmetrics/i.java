package com.mixpanel.android.mpmetrics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import org.json.JSONObject;

/* compiled from: MPDbAdapter */
class i {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f2021a = ("CREATE TABLE " + k.EVENTS.a() + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " + "data" + " STRING NOT NULL, " + "created_at" + " INTEGER NOT NULL);");
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final String f2022b = ("CREATE TABLE " + k.PEOPLE.a() + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " + "data" + " STRING NOT NULL, " + "created_at" + " INTEGER NOT NULL);");
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public static final String f2023c = ("CREATE INDEX IF NOT EXISTS time_idx ON " + k.EVENTS.a() + " (" + "created_at" + ");");
    /* access modifiers changed from: private */
    public static final String d = ("CREATE INDEX IF NOT EXISTS time_idx ON " + k.PEOPLE.a() + " (" + "created_at" + ");");
    private final j e;

    public i(Context context) {
        this(context, "mixpanel");
    }

    public i(Context context, String str) {
        this.e = new j(context, str);
    }

    public int a(JSONObject jSONObject, k kVar) {
        Cursor cursor = null;
        String a2 = kVar.a();
        int i = -1;
        try {
            SQLiteDatabase writableDatabase = this.e.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("data", jSONObject.toString());
            contentValues.put("created_at", Long.valueOf(System.currentTimeMillis()));
            writableDatabase.insert(a2, null, contentValues);
            cursor = writableDatabase.rawQuery("SELECT COUNT(*) FROM " + a2, null);
            cursor.moveToFirst();
            i = cursor.getInt(0);
            this.e.close();
            if (cursor != null) {
                cursor.close();
            }
        } catch (SQLiteException e2) {
            Log.e("MixpanelAPI", "addJSON " + a2 + " FAILED. Deleting DB.", e2);
            this.e.a();
            this.e.close();
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            this.e.close();
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return i;
    }

    public void a(String str, k kVar) {
        String a2 = kVar.a();
        try {
            this.e.getWritableDatabase().delete(a2, "_id <= " + str, null);
        } catch (SQLiteException e2) {
            Log.e("MixpanelAPI", "cleanupEvents " + a2 + " by id FAILED. Deleting DB.", e2);
            this.e.a();
        } finally {
            this.e.close();
        }
    }

    public void a(long j, k kVar) {
        String a2 = kVar.a();
        try {
            this.e.getWritableDatabase().delete(a2, "created_at <= " + j, null);
        } catch (SQLiteException e2) {
            Log.e("MixpanelAPI", "cleanupEvents " + a2 + " by time FAILED. Deleting DB.", e2);
            this.e.a();
        } finally {
            this.e.close();
        }
    }

    public void a() {
        this.e.a();
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ba  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String[] a(com.mixpanel.android.mpmetrics.k r8) {
        /*
            r7 = this;
            r1 = 0
            java.lang.String r4 = r8.a()
            com.mixpanel.android.mpmetrics.j r0 = r7.e     // Catch:{ SQLiteException -> 0x008a, all -> 0x00b1 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x008a, all -> 0x00b1 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x008a, all -> 0x00b1 }
            r2.<init>()     // Catch:{ SQLiteException -> 0x008a, all -> 0x00b1 }
            java.lang.String r3 = "SELECT * FROM "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLiteException -> 0x008a, all -> 0x00b1 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ SQLiteException -> 0x008a, all -> 0x00b1 }
            java.lang.String r3 = " ORDER BY "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLiteException -> 0x008a, all -> 0x00b1 }
            java.lang.String r3 = "created_at"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLiteException -> 0x008a, all -> 0x00b1 }
            java.lang.String r3 = " ASC LIMIT 50"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLiteException -> 0x008a, all -> 0x00b1 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLiteException -> 0x008a, all -> 0x00b1 }
            r3 = 0
            android.database.Cursor r2 = r0.rawQuery(r2, r3)     // Catch:{ SQLiteException -> 0x008a, all -> 0x00b1 }
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ SQLiteException -> 0x00c0 }
            r0.<init>()     // Catch:{ SQLiteException -> 0x00c0 }
            r3 = r1
        L_0x003b:
            boolean r5 = r2.moveToNext()     // Catch:{ SQLiteException -> 0x00c0 }
            if (r5 == 0) goto L_0x0066
            boolean r5 = r2.isLast()     // Catch:{ SQLiteException -> 0x00c0 }
            if (r5 == 0) goto L_0x0051
            java.lang.String r3 = "_id"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x00c0 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ SQLiteException -> 0x00c0 }
        L_0x0051:
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r6 = "data"
            int r6 = r2.getColumnIndex(r6)     // Catch:{ JSONException -> 0x0064 }
            java.lang.String r6 = r2.getString(r6)     // Catch:{ JSONException -> 0x0064 }
            r5.<init>(r6)     // Catch:{ JSONException -> 0x0064 }
            r0.put(r5)     // Catch:{ JSONException -> 0x0064 }
            goto L_0x003b
        L_0x0064:
            r5 = move-exception
            goto L_0x003b
        L_0x0066:
            int r5 = r0.length()     // Catch:{ SQLiteException -> 0x00c0 }
            if (r5 <= 0) goto L_0x00c8
            java.lang.String r0 = r0.toString()     // Catch:{ SQLiteException -> 0x00c0 }
        L_0x0070:
            com.mixpanel.android.mpmetrics.j r4 = r7.e
            r4.close()
            if (r2 == 0) goto L_0x00c5
            r2.close()
            r2 = r0
            r0 = r3
        L_0x007c:
            if (r0 == 0) goto L_0x0089
            if (r2 == 0) goto L_0x0089
            r1 = 2
            java.lang.String[] r1 = new java.lang.String[r1]
            r3 = 0
            r1[r3] = r0
            r0 = 1
            r1[r0] = r2
        L_0x0089:
            return r1
        L_0x008a:
            r0 = move-exception
            r2 = r1
        L_0x008c:
            java.lang.String r3 = "MixpanelAPI"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00be }
            r5.<init>()     // Catch:{ all -> 0x00be }
            java.lang.String r6 = "generateDataString "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00be }
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ all -> 0x00be }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00be }
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x00be }
            com.mixpanel.android.mpmetrics.j r0 = r7.e
            r0.close()
            if (r2 == 0) goto L_0x00c2
            r2.close()
            r0 = r1
            r2 = r1
            goto L_0x007c
        L_0x00b1:
            r0 = move-exception
            r2 = r1
        L_0x00b3:
            com.mixpanel.android.mpmetrics.j r1 = r7.e
            r1.close()
            if (r2 == 0) goto L_0x00bd
            r2.close()
        L_0x00bd:
            throw r0
        L_0x00be:
            r0 = move-exception
            goto L_0x00b3
        L_0x00c0:
            r0 = move-exception
            goto L_0x008c
        L_0x00c2:
            r0 = r1
            r2 = r1
            goto L_0x007c
        L_0x00c5:
            r2 = r0
            r0 = r3
            goto L_0x007c
        L_0x00c8:
            r0 = r1
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mixpanel.android.mpmetrics.i.a(com.mixpanel.android.mpmetrics.k):java.lang.String[]");
    }
}
