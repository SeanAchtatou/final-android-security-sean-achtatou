package com.mixpanel.android.mpmetrics;

import android.util.Log;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: MixpanelAPI */
class p implements o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f2031a;

    private p(l lVar) {
        this.f2031a = lVar;
    }

    public void a(String str) {
        String unused = this.f2031a.k = str;
        this.f2031a.j();
        if (this.f2031a.l != null) {
            this.f2031a.f();
        }
    }

    public void a(JSONObject jSONObject) {
        try {
            if (this.f2031a.k == null) {
                if (this.f2031a.l == null) {
                    r unused = this.f2031a.l = new r();
                }
                this.f2031a.l.a(jSONObject);
                this.f2031a.j();
                return;
            }
            this.f2031a.e.b(a("$set", jSONObject));
        } catch (JSONException e) {
            Log.e("MixpanelAPI", "Exception setting people properties");
        }
    }

    public void a(String str, Object obj) {
        try {
            a(new JSONObject().put(str, obj));
        } catch (JSONException e) {
            Log.e("MixpanelAPI", "set", e);
        }
    }

    public void a(Map<String, ? extends Number> map) {
        JSONObject jSONObject = new JSONObject(map);
        try {
            if (this.f2031a.k == null) {
                if (this.f2031a.l == null) {
                    r unused = this.f2031a.l = new r();
                }
                this.f2031a.l.a(map);
                return;
            }
            this.f2031a.e.b(a("$add", jSONObject));
        } catch (JSONException e) {
            Log.e("MixpanelAPI", "Exception incrementing properties", e);
        }
    }

    public void b(String str, Object obj) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(str, obj);
            b(jSONObject);
        } catch (JSONException e) {
            Log.e("MixpanelAPI", "Exception appending a property", e);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(JSONObject jSONObject) {
        try {
            if (this.f2031a.k == null) {
                if (this.f2031a.l == null) {
                    r unused = this.f2031a.l = new r();
                }
                this.f2031a.l.b(jSONObject);
                return;
            }
            this.f2031a.e.b(a("$append", jSONObject));
        } catch (JSONException e) {
            Log.e("MixpanelAPI", "Can't create append message", e);
        }
    }

    public void b(String str) {
        if (this.f2031a.k != null) {
            this.f2031a.h.edit().putString("push_id", str).commit();
            try {
                this.f2031a.e.b(a("$union", new JSONObject().put("$android_devices", new JSONArray("[" + str + "]"))));
            } catch (JSONException e) {
                Log.e("MixpanelAPI", "set push registration id error", e);
            }
        }
    }

    public void a() {
        this.f2031a.h.edit().remove("push_id").commit();
        a("$android_devices", new JSONArray());
    }

    public JSONObject a(String str, JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put(str, jSONObject);
        jSONObject2.put("$token", this.f2031a.f);
        jSONObject2.put("$distinct_id", this.f2031a.k);
        jSONObject2.put("$time", System.currentTimeMillis());
        return jSONObject2;
    }
}
