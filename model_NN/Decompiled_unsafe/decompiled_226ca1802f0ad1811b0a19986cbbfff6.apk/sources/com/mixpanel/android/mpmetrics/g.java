package com.mixpanel.android.mpmetrics;

import android.util.Log;
import com.mixpanel.android.a.a;
import com.mixpanel.android.a.b;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

/* compiled from: HttpPoster */
class g {

    /* renamed from: a  reason: collision with root package name */
    private final String f2016a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2017b;

    public g(String str, String str2) {
        this.f2016a = str;
        this.f2017b = str2;
    }

    public h a(String str, String str2) {
        String a2 = a.a(str);
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new BasicNameValuePair("data", a2));
        h a3 = a(this.f2016a + str2, arrayList);
        if (a3 != h.FAILED_RECOVERABLE || this.f2017b == null) {
            return a3;
        }
        return a(this.f2017b + str2, arrayList);
    }

    private h a(String str, List<NameValuePair> list) {
        h hVar = h.FAILED_UNRECOVERABLE;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(str);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(list));
            HttpEntity entity = defaultHttpClient.execute(httpPost).getEntity();
            if (entity == null || !b.a(entity.getContent()).equals("1\n")) {
                return hVar;
            }
            return h.SUCCEEDED;
        } catch (IOException e) {
            Log.i("MixpanelAPI", "Cannot post message to Mixpanel Servers (May Retry)", e);
            return h.FAILED_RECOVERABLE;
        } catch (OutOfMemoryError e2) {
            Log.e("MixpanelAPI", "Cannot post message to Mixpanel Servers, will not retry.", e2);
            return h.FAILED_UNRECOVERABLE;
        }
    }
}
