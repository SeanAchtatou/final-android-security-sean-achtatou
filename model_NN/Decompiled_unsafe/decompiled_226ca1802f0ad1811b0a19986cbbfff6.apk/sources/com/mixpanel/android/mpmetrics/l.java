package com.mixpanel.android.mpmetrics;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: MixpanelAPI */
public class l {

    /* renamed from: a  reason: collision with root package name */
    private static final DateFormat f2028a = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    /* renamed from: b  reason: collision with root package name */
    private static Map<String, Map<Context, l>> f2029b = new HashMap();

    /* renamed from: c  reason: collision with root package name */
    private final Context f2030c;
    private final q d = d();
    /* access modifiers changed from: private */
    public final a e = c();
    /* access modifiers changed from: private */
    public final String f;
    private final p g = new p(this);
    /* access modifiers changed from: private */
    public final SharedPreferences h;
    private JSONObject i;
    private String j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public r l;

    l(Context context, String str) {
        this.f2030c = context;
        this.f = str;
        this.h = context.getSharedPreferences("com.mixpanel.android.mpmetrics.MixpanelAPI_" + str, 0);
        g();
        i();
    }

    public static l a(Context context, String str) {
        HashMap hashMap;
        l lVar;
        synchronized (f2029b) {
            Context applicationContext = context.getApplicationContext();
            Map map = f2029b.get(str);
            if (map == null) {
                HashMap hashMap2 = new HashMap();
                f2029b.put(str, hashMap2);
                hashMap = hashMap2;
            } else {
                hashMap = map;
            }
            lVar = (l) hashMap.get(applicationContext);
            if (lVar == null) {
                lVar = new l(applicationContext, str);
                hashMap.put(applicationContext, lVar);
            }
        }
        return lVar;
    }

    public void a(String str) {
        this.j = str;
        j();
    }

    public void a(String str, JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("event", str);
            JSONObject e2 = e();
            e2.put("token", this.f);
            e2.put("time", System.currentTimeMillis() / 1000);
            Iterator<String> keys = this.i.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                e2.put(next, this.i.get(next));
            }
            String a2 = a();
            if (a2 != null) {
                e2.put("distinct_id", a2);
            }
            if (jSONObject != null) {
                Iterator<String> keys2 = jSONObject.keys();
                while (keys2.hasNext()) {
                    String next2 = keys2.next();
                    e2.put(next2, jSONObject.get(next2));
                }
            }
            jSONObject2.put("properties", e2);
            this.e.a(jSONObject2);
        } catch (JSONException e3) {
            Log.e("MixpanelAPI", "Exception tracking event " + str, e3);
        }
    }

    public String a() {
        return this.j;
    }

    public void a(JSONObject jSONObject) {
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            try {
                this.i.put(next, jSONObject.get(next));
            } catch (JSONException e2) {
                Log.e("MixpanelAPI", "Exception registering super property.", e2);
            }
        }
        h();
    }

    public o b() {
        return this.g;
    }

    static void a(n nVar) {
        synchronized (f2029b) {
            for (Map<Context, l> values : f2029b.values()) {
                for (l a2 : values.values()) {
                    nVar.a(a2);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public a c() {
        return a.a(this.f2030c);
    }

    /* access modifiers changed from: package-private */
    public q d() {
        return new q(this.f2030c);
    }

    private JSONObject e() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("mp_lib", "android");
        jSONObject.put("$lib_version", "3.3.0");
        jSONObject.put("$os", "Android");
        jSONObject.put("$os_version", Build.VERSION.RELEASE == null ? "UNKNOWN" : Build.VERSION.RELEASE);
        jSONObject.put("$manufacturer", Build.MANUFACTURER == null ? "UNKNOWN" : Build.MANUFACTURER);
        jSONObject.put("$brand", Build.BRAND == null ? "UNKNOWN" : Build.BRAND);
        jSONObject.put("$model", Build.MODEL == null ? "UNKNOWN" : Build.MODEL);
        DisplayMetrics d2 = this.d.d();
        jSONObject.put("$screen_dpi", d2.densityDpi);
        jSONObject.put("$screen_height", d2.heightPixels);
        jSONObject.put("$screen_width", d2.widthPixels);
        String a2 = this.d.a();
        if (a2 != null) {
            jSONObject.put("$app_version", a2);
        }
        Boolean valueOf = Boolean.valueOf(this.d.b());
        if (valueOf != null) {
            jSONObject.put("$has_nfc", valueOf.booleanValue());
        }
        Boolean valueOf2 = Boolean.valueOf(this.d.c());
        if (valueOf2 != null) {
            jSONObject.put("$has_telephone", valueOf2.booleanValue());
        }
        String e2 = this.d.e();
        if (e2 != null) {
            jSONObject.put("$carrier", e2);
        }
        Boolean f2 = this.d.f();
        if (f2 != null) {
            jSONObject.put("$wifi", f2.booleanValue());
        }
        return jSONObject;
    }

    /* access modifiers changed from: private */
    public void f() {
        if (!(this.l == null || this.k == null)) {
            JSONObject b2 = this.l.b();
            Map<String, Double> c2 = this.l.c();
            List<JSONObject> d2 = this.l.d();
            b().a(b2);
            b().a(c2);
            for (JSONObject next : d2) {
                Iterator<String> keys = next.keys();
                while (keys.hasNext()) {
                    try {
                        String next2 = keys.next();
                        b().b(next2, next.get(next2));
                    } catch (JSONException e2) {
                        Log.e("MixpanelAPI", "Couldn't send stored append", e2);
                    }
                }
            }
        }
        this.l = null;
        j();
    }

    private void g() {
        try {
            this.i = new JSONObject(this.h.getString("super_properties", "{}"));
        } catch (JSONException e2) {
            Log.e("MixpanelAPI", "Cannot parse stored superProperties");
            this.i = new JSONObject();
            h();
        }
    }

    private void h() {
        String jSONObject = this.i.toString();
        SharedPreferences.Editor edit = this.h.edit();
        edit.putString("super_properties", jSONObject);
        edit.commit();
    }

    private void i() {
        this.j = this.h.getString("events_distinct_id", null);
        this.k = this.h.getString("people_distinct_id", null);
        this.l = null;
        String string = this.h.getString("waiting_people_record", null);
        if (string != null) {
            try {
                this.l = new r();
                this.l.a(string);
            } catch (JSONException e2) {
                Log.e("MixpanelAPI", "Could not interpret waiting people JSON record " + string);
            }
        }
        if (this.j == null) {
            this.j = UUID.randomUUID().toString();
            j();
        }
        if (this.l != null && this.k != null) {
            f();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        SharedPreferences.Editor edit = this.h.edit();
        edit.putString("events_distinct_id", this.j);
        edit.putString("people_distinct_id", this.k);
        if (this.l == null) {
            edit.remove("waiting_people_record");
        } else {
            edit.putString("waiting_people_record", this.l.a());
        }
        edit.commit();
    }

    static {
        f2028a.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
}
