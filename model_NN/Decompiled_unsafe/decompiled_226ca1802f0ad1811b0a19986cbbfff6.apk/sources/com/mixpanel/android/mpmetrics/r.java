package com.mixpanel.android.mpmetrics;

import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: WaitingPeopleRecord */
class r {

    /* renamed from: a  reason: collision with root package name */
    private JSONObject f2035a = new JSONObject();

    /* renamed from: b  reason: collision with root package name */
    private Map<String, Double> f2036b = new HashMap();

    /* renamed from: c  reason: collision with root package name */
    private List<JSONObject> f2037c = new ArrayList();

    public void a(JSONObject jSONObject) {
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            Object obj = jSONObject.get(next);
            this.f2036b.remove(next);
            ArrayList<JSONObject> arrayList = new ArrayList<>();
            for (JSONObject jSONObject2 : arrayList) {
                jSONObject2.remove(next);
                if (jSONObject2.length() > 0) {
                    arrayList.add(jSONObject2);
                }
            }
            this.f2037c = arrayList;
            this.f2035a.put(next, obj);
        }
    }

    public void a(Map<String, ? extends Number> map) {
        for (String next : map.keySet()) {
            Number number = this.f2036b.get(next);
            Number number2 = (Number) map.get(next);
            if (number == null && number2 != null) {
                this.f2036b.put(next, Double.valueOf(number2.doubleValue()));
            } else if (!(number == null || number2 == null)) {
                this.f2036b.put(next, Double.valueOf(number2.doubleValue() + number.doubleValue()));
            }
        }
    }

    public void b(JSONObject jSONObject) {
        this.f2037c.add(jSONObject);
    }

    public void a(String str) {
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject(str);
        JSONObject jSONObject3 = new JSONObject();
        if (jSONObject2.has("$set")) {
            jSONObject = jSONObject2.getJSONObject("$set");
        } else {
            jSONObject = jSONObject3;
        }
        HashMap hashMap = new HashMap();
        if (jSONObject2.has("$add")) {
            JSONObject jSONObject4 = jSONObject2.getJSONObject("$add");
            Iterator<String> keys = jSONObject4.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, Double.valueOf(jSONObject4.getDouble(next)));
            }
        }
        ArrayList arrayList = new ArrayList();
        if (jSONObject2.has("$append")) {
            JSONArray jSONArray = jSONObject2.getJSONArray("$append");
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(jSONArray.getJSONObject(i));
            }
        }
        this.f2035a = jSONObject;
        this.f2036b = hashMap;
        this.f2037c = arrayList;
    }

    public String a() {
        try {
            JSONObject jSONObject = new JSONObject();
            for (String next : this.f2036b.keySet()) {
                jSONObject.put(next, this.f2036b.get(next));
            }
            JSONArray jSONArray = new JSONArray();
            for (JSONObject put : this.f2037c) {
                jSONArray.put(put);
            }
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("$set", this.f2035a);
            jSONObject2.put("$add", jSONObject);
            jSONObject2.put("$append", jSONArray);
            return jSONObject2.toString();
        } catch (JSONException e) {
            Log.e("MixpanelAPI", "Could not write Waiting User Properties to JSON", e);
            return null;
        }
    }

    public JSONObject b() {
        return this.f2035a;
    }

    public Map<String, Double> c() {
        return this.f2036b;
    }

    public List<JSONObject> d() {
        return this.f2037c;
    }
}
