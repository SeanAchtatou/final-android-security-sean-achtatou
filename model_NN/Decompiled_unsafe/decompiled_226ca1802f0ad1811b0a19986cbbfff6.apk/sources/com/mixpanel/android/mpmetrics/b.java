package com.mixpanel.android.mpmetrics;

import android.os.Handler;
import android.os.Message;
import java.util.concurrent.SynchronousQueue;

/* compiled from: AnalyticsMessages */
class b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f2005a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final Object f2006b = new Object();
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Handler f2007c = b();
    /* access modifiers changed from: private */
    public long d = 60000;
    private long e = 0;
    private long f = 0;
    private long g = -1;

    public b(a aVar) {
        this.f2005a = aVar;
    }

    public boolean a() {
        boolean z;
        synchronized (this.f2006b) {
            z = this.f2007c == null;
        }
        return z;
    }

    public void a(Message message) {
        if (a()) {
            this.f2005a.a("Dead mixpanel worker dropping a message: " + message);
            return;
        }
        synchronized (this.f2006b) {
            if (this.f2007c != null) {
                this.f2007c.sendMessage(message);
            }
        }
    }

    private Handler b() {
        SynchronousQueue synchronousQueue = new SynchronousQueue();
        c cVar = new c(this, synchronousQueue);
        cVar.setPriority(1);
        cVar.start();
        try {
            return (Handler) synchronousQueue.take();
        } catch (InterruptedException e2) {
            throw new RuntimeException("Couldn't retrieve handler from worker thread");
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        long currentTimeMillis = System.currentTimeMillis();
        long j = this.e + 1;
        if (this.g > 0) {
            this.f = ((currentTimeMillis - this.g) + (this.f * this.e)) / j;
            this.f2005a.a("Average send frequency approximately " + (this.f / 1000) + " seconds.");
        }
        this.g = currentTimeMillis;
        this.e = j;
    }
}
