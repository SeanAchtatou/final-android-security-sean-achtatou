package com.mixpanel.android.mpmetrics;

import android.content.Context;
import android.net.ConnectivityManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

/* compiled from: SystemInformation */
class q {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2032a;

    /* renamed from: b  reason: collision with root package name */
    private final Boolean f2033b;

    /* renamed from: c  reason: collision with root package name */
    private final Boolean f2034c;
    private final DisplayMetrics d;
    private final String e;
    private final Integer f;

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x009e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public q(android.content.Context r8) {
        /*
            r7 = this;
            r2 = 0
            r7.<init>()
            r7.f2032a = r8
            android.content.Context r0 = r7.f2032a
            android.content.pm.PackageManager r3 = r0.getPackageManager()
            android.content.Context r0 = r7.f2032a     // Catch:{ NameNotFoundException -> 0x0074 }
            java.lang.String r0 = r0.getPackageName()     // Catch:{ NameNotFoundException -> 0x0074 }
            r1 = 0
            android.content.pm.PackageInfo r0 = r3.getPackageInfo(r0, r1)     // Catch:{ NameNotFoundException -> 0x0074 }
            java.lang.String r1 = r0.versionName     // Catch:{ NameNotFoundException -> 0x0074 }
            int r0 = r0.versionCode     // Catch:{ NameNotFoundException -> 0x009b }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ NameNotFoundException -> 0x009b }
        L_0x001f:
            r7.e = r1
            r7.f = r0
            java.lang.Class r0 = r3.getClass()
            java.lang.String r1 = "hasSystemFeature"
            r4 = 1
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ NoSuchMethodException -> 0x0080 }
            r5 = 0
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            r4[r5] = r6     // Catch:{ NoSuchMethodException -> 0x0080 }
            java.lang.reflect.Method r0 = r0.getMethod(r1, r4)     // Catch:{ NoSuchMethodException -> 0x0080 }
            r1 = r0
        L_0x0036:
            if (r1 == 0) goto L_0x009e
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ InvocationTargetException -> 0x0083, IllegalAccessException -> 0x008d }
            r4 = 0
            java.lang.String r5 = "android.hardware.nfc"
            r0[r4] = r5     // Catch:{ InvocationTargetException -> 0x0083, IllegalAccessException -> 0x008d }
            java.lang.Object r0 = r1.invoke(r3, r0)     // Catch:{ InvocationTargetException -> 0x0083, IllegalAccessException -> 0x008d }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ InvocationTargetException -> 0x0083, IllegalAccessException -> 0x008d }
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ InvocationTargetException -> 0x0099, IllegalAccessException -> 0x0097 }
            r5 = 0
            java.lang.String r6 = "android.hardware.telephony"
            r4[r5] = r6     // Catch:{ InvocationTargetException -> 0x0099, IllegalAccessException -> 0x0097 }
            java.lang.Object r1 = r1.invoke(r3, r4)     // Catch:{ InvocationTargetException -> 0x0099, IllegalAccessException -> 0x0097 }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ InvocationTargetException -> 0x0099, IllegalAccessException -> 0x0097 }
            r2 = r1
        L_0x0055:
            r7.f2033b = r0
            r7.f2034c = r2
            android.util.DisplayMetrics r0 = new android.util.DisplayMetrics
            r0.<init>()
            r7.d = r0
            android.content.Context r0 = r7.f2032a
            java.lang.String r1 = "window"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.view.WindowManager r0 = (android.view.WindowManager) r0
            android.view.Display r0 = r0.getDefaultDisplay()
            android.util.DisplayMetrics r1 = r7.d
            r0.getMetrics(r1)
            return
        L_0x0074:
            r0 = move-exception
            r0 = r2
        L_0x0076:
            java.lang.String r1 = "MixpanelAPI"
            java.lang.String r4 = "System information constructed with a context that apparently doesn't exist."
            android.util.Log.w(r1, r4)
            r1 = r0
            r0 = r2
            goto L_0x001f
        L_0x0080:
            r0 = move-exception
            r1 = r2
            goto L_0x0036
        L_0x0083:
            r0 = move-exception
            r0 = r2
        L_0x0085:
            java.lang.String r1 = "MixpanelAPI"
            java.lang.String r3 = "System version appeared to support PackageManager.hasSystemFeature, but we were unable to call it."
            android.util.Log.w(r1, r3)
            goto L_0x0055
        L_0x008d:
            r0 = move-exception
            r0 = r2
        L_0x008f:
            java.lang.String r1 = "MixpanelAPI"
            java.lang.String r3 = "System version appeared to support PackageManager.hasSystemFeature, but we were unable to call it."
            android.util.Log.w(r1, r3)
            goto L_0x0055
        L_0x0097:
            r1 = move-exception
            goto L_0x008f
        L_0x0099:
            r1 = move-exception
            goto L_0x0085
        L_0x009b:
            r0 = move-exception
            r0 = r1
            goto L_0x0076
        L_0x009e:
            r0 = r2
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mixpanel.android.mpmetrics.q.<init>(android.content.Context):void");
    }

    public String a() {
        return this.e;
    }

    public boolean b() {
        return this.f2033b.booleanValue();
    }

    public boolean c() {
        return this.f2034c.booleanValue();
    }

    public DisplayMetrics d() {
        return this.d;
    }

    public String e() {
        TelephonyManager telephonyManager = (TelephonyManager) this.f2032a.getSystemService("phone");
        if (telephonyManager != null) {
            return telephonyManager.getNetworkOperatorName();
        }
        return null;
    }

    public Boolean f() {
        if (this.f2032a.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0) {
            return Boolean.valueOf(((ConnectivityManager) this.f2032a.getSystemService("connectivity")).getNetworkInfo(1).isConnected());
        }
        return null;
    }
}
