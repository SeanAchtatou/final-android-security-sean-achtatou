package com.mixpanel.android.mpmetrics;

import android.os.Looper;
import android.util.Log;
import java.util.concurrent.SynchronousQueue;

/* compiled from: AnalyticsMessages */
class c extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SynchronousQueue f2008a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f2009b;

    c(b bVar, SynchronousQueue synchronousQueue) {
        this.f2009b = bVar;
        this.f2008a = synchronousQueue;
    }

    public void run() {
        Looper.prepare();
        try {
            this.f2008a.put(new d(this.f2009b));
            try {
                Looper.loop();
            } catch (RuntimeException e) {
                Log.e("MixpanelAPI", "Mixpanel Thread dying from RuntimeException", e);
            }
        } catch (InterruptedException e2) {
            throw new RuntimeException("Couldn't build worker thread for Analytics Messages", e2);
        }
    }
}
