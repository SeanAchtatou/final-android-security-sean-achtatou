package c.a.a.a.a;

/* compiled from: MinimalField */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private final String f196a;

    /* renamed from: b  reason: collision with root package name */
    private final String f197b;

    g(String str, String str2) {
        this.f196a = str;
        this.f197b = str2;
    }

    public String a() {
        return this.f196a;
    }

    public String b() {
        return this.f197b;
    }

    public String toString() {
        return this.f196a + ": " + this.f197b;
    }
}
