package c.a.a.a.a;

import c.a.a.a.a.a.c;

/* compiled from: FormBodyPart */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f175a;

    /* renamed from: b  reason: collision with root package name */
    private final b f176b;

    /* renamed from: c  reason: collision with root package name */
    private final c f177c;

    public a(String str, c cVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (cVar == null) {
            throw new IllegalArgumentException("Body may not be null");
        } else {
            this.f175a = str;
            this.f177c = cVar;
            this.f176b = new b();
            a(cVar);
            b(cVar);
            c(cVar);
        }
    }

    public String a() {
        return this.f175a;
    }

    public c b() {
        return this.f177c;
    }

    public b c() {
        return this.f176b;
    }

    public void a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Field name may not be null");
        }
        this.f176b.a(new g(str, str2));
    }

    /* access modifiers changed from: protected */
    public void a(c cVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"");
        sb.append(a());
        sb.append("\"");
        if (cVar.b() != null) {
            sb.append("; filename=\"");
            sb.append(cVar.b());
            sb.append("\"");
        }
        a("Content-Disposition", sb.toString());
    }

    /* access modifiers changed from: protected */
    public void b(c cVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(cVar.a());
        if (cVar.c() != null) {
            sb.append("; charset=");
            sb.append(cVar.c());
        }
        a("Content-Type", sb.toString());
    }

    /* access modifiers changed from: protected */
    public void c(c cVar) {
        a("Content-Transfer-Encoding", cVar.d());
    }
}
