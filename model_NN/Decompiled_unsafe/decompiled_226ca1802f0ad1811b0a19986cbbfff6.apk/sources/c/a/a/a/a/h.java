package c.a.a.a.a;

import c.a.a.a.a.a.c;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

/* compiled from: MultipartEntity */
public class h implements HttpEntity {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f198a = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    /* renamed from: b  reason: collision with root package name */
    private final c f199b;

    /* renamed from: c  reason: collision with root package name */
    private final Header f200c;
    private long d;
    private volatile boolean e;

    public h(e eVar, String str, Charset charset) {
        str = str == null ? a() : str;
        this.f199b = new c("form-data", charset, str, eVar == null ? e.STRICT : eVar);
        this.f200c = new BasicHeader("Content-Type", a(str, charset));
        this.e = true;
    }

    public h(e eVar) {
        this(eVar, null, null);
    }

    public h() {
        this(e.STRICT, null, null);
    }

    /* access modifiers changed from: protected */
    public String a(String str, Charset charset) {
        StringBuilder sb = new StringBuilder();
        sb.append("multipart/form-data; boundary=");
        sb.append(str);
        if (charset != null) {
            sb.append("; charset=");
            sb.append(charset.name());
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public String a() {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int nextInt = random.nextInt(11) + 30;
        for (int i = 0; i < nextInt; i++) {
            sb.append(f198a[random.nextInt(f198a.length)]);
        }
        return sb.toString();
    }

    public void a(a aVar) {
        this.f199b.a(aVar);
        this.e = true;
    }

    public void a(String str, c cVar) {
        a(new a(str, cVar));
    }

    public boolean isRepeatable() {
        for (a b2 : this.f199b.a()) {
            if (b2.b().e() < 0) {
                return false;
            }
        }
        return true;
    }

    public boolean isChunked() {
        return !isRepeatable();
    }

    public boolean isStreaming() {
        return !isRepeatable();
    }

    public long getContentLength() {
        if (this.e) {
            this.d = this.f199b.c();
            this.e = false;
        }
        return this.d;
    }

    public Header getContentType() {
        return this.f200c;
    }

    public Header getContentEncoding() {
        return null;
    }

    public void consumeContent() {
        if (isStreaming()) {
            throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
        }
    }

    public InputStream getContent() {
        throw new UnsupportedOperationException("Multipart form entity does not implement #getContent()");
    }

    public void writeTo(OutputStream outputStream) {
        this.f199b.a(outputStream);
    }
}
