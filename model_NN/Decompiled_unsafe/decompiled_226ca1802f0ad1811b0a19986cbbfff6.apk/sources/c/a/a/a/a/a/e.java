package c.a.a.a.a.a;

import android.support.v4.app.FragmentTransaction;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

/* compiled from: FileBody */
public class e extends a {

    /* renamed from: a  reason: collision with root package name */
    private final File f183a;

    /* renamed from: b  reason: collision with root package name */
    private final String f184b;

    /* renamed from: c  reason: collision with root package name */
    private final String f185c;

    public e(File file, String str, String str2, String str3) {
        super(str2);
        if (file == null) {
            throw new IllegalArgumentException("File may not be null");
        }
        this.f183a = file;
        if (str != null) {
            this.f184b = str;
        } else {
            this.f184b = file.getName();
        }
        this.f185c = str3;
    }

    public e(File file, String str, String str2) {
        this(file, null, str, str2);
    }

    public e(File file, String str) {
        this(file, str, null);
    }

    public void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        FileInputStream fileInputStream = new FileInputStream(this.f183a);
        try {
            byte[] bArr = new byte[FragmentTransaction.TRANSIT_ENTER_MASK];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    outputStream.flush();
                    return;
                }
            }
        } finally {
            fileInputStream.close();
        }
    }

    public String d() {
        return "binary";
    }

    public String c() {
        return this.f185c;
    }

    public long e() {
        return this.f183a.length();
    }

    public String b() {
        return this.f184b;
    }
}
