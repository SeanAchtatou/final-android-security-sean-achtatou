package c.a.a.a.a.a;

/* compiled from: AbstractContentBody */
public abstract class a implements c {

    /* renamed from: a  reason: collision with root package name */
    private final String f178a;

    /* renamed from: b  reason: collision with root package name */
    private final String f179b;

    /* renamed from: c  reason: collision with root package name */
    private final String f180c;

    public a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("MIME type may not be null");
        }
        this.f178a = str;
        int indexOf = str.indexOf(47);
        if (indexOf != -1) {
            this.f179b = str.substring(0, indexOf);
            this.f180c = str.substring(indexOf + 1);
            return;
        }
        this.f179b = str;
        this.f180c = null;
    }

    public String a() {
        return this.f178a;
    }
}
