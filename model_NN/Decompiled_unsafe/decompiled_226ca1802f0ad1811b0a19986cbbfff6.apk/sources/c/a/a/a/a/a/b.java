package c.a.a.a.a.a;

import java.io.OutputStream;

/* compiled from: ByteArrayBody */
public class b extends a {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f181a;

    /* renamed from: b  reason: collision with root package name */
    private final String f182b;

    public b(byte[] bArr, String str, String str2) {
        super(str);
        if (bArr == null) {
            throw new IllegalArgumentException("byte[] may not be null");
        }
        this.f181a = bArr;
        this.f182b = str2;
    }

    public b(byte[] bArr, String str) {
        this(bArr, "application/octet-stream", str);
    }

    public String b() {
        return this.f182b;
    }

    public void a(OutputStream outputStream) {
        outputStream.write(this.f181a);
    }

    public String c() {
        return null;
    }

    public String d() {
        return "binary";
    }

    public long e() {
        return (long) this.f181a.length;
    }
}
