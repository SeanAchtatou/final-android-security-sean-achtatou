package c.a.a.a.a;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;

/* compiled from: HttpMultipart */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final ByteArrayBuffer f188a = a(f.f195a, ": ");

    /* renamed from: b  reason: collision with root package name */
    private static final ByteArrayBuffer f189b = a(f.f195a, "\r\n");

    /* renamed from: c  reason: collision with root package name */
    private static final ByteArrayBuffer f190c = a(f.f195a, "--");
    private final String d;
    private final Charset e;
    private final String f;
    private final List<a> g;
    private final e h;

    private static ByteArrayBuffer a(Charset charset, String str) {
        ByteBuffer encode = charset.encode(CharBuffer.wrap(str));
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(encode.remaining());
        byteArrayBuffer.append(encode.array(), encode.position(), encode.remaining());
        return byteArrayBuffer;
    }

    private static void a(ByteArrayBuffer byteArrayBuffer, OutputStream outputStream) {
        outputStream.write(byteArrayBuffer.buffer(), 0, byteArrayBuffer.length());
    }

    private static void a(String str, Charset charset, OutputStream outputStream) {
        a(a(charset, str), outputStream);
    }

    private static void a(String str, OutputStream outputStream) {
        a(a(f.f195a, str), outputStream);
    }

    private static void a(g gVar, OutputStream outputStream) {
        a(gVar.a(), outputStream);
        a(f188a, outputStream);
        a(gVar.b(), outputStream);
        a(f189b, outputStream);
    }

    private static void a(g gVar, Charset charset, OutputStream outputStream) {
        a(gVar.a(), charset, outputStream);
        a(f188a, outputStream);
        a(gVar.b(), charset, outputStream);
        a(f189b, outputStream);
    }

    public c(String str, Charset charset, String str2, e eVar) {
        if (str == null) {
            throw new IllegalArgumentException("Multipart subtype may not be null");
        } else if (str2 == null) {
            throw new IllegalArgumentException("Multipart boundary may not be null");
        } else {
            this.d = str;
            this.e = charset == null ? f.f195a : charset;
            this.f = str2;
            this.g = new ArrayList();
            this.h = eVar;
        }
    }

    public List<a> a() {
        return this.g;
    }

    public void a(a aVar) {
        if (aVar != null) {
            this.g.add(aVar);
        }
    }

    public String b() {
        return this.f;
    }

    private void a(e eVar, OutputStream outputStream, boolean z) {
        ByteArrayBuffer a2 = a(this.e, b());
        for (a next : this.g) {
            a(f190c, outputStream);
            a(a2, outputStream);
            a(f189b, outputStream);
            b c2 = next.c();
            switch (eVar) {
                case STRICT:
                    Iterator<g> it = c2.iterator();
                    while (it.hasNext()) {
                        a(it.next(), outputStream);
                    }
                    break;
                case BROWSER_COMPATIBLE:
                    a(next.c().a("Content-Disposition"), this.e, outputStream);
                    if (next.b().b() != null) {
                        a(next.c().a("Content-Type"), this.e, outputStream);
                        break;
                    }
                    break;
            }
            a(f189b, outputStream);
            if (z) {
                next.b().a(outputStream);
            }
            a(f189b, outputStream);
        }
        a(f190c, outputStream);
        a(a2, outputStream);
        a(f190c, outputStream);
        a(f189b, outputStream);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a.a.a.a.c.a(c.a.a.a.a.e, java.io.OutputStream, boolean):void
     arg types: [c.a.a.a.a.e, java.io.OutputStream, int]
     candidates:
      c.a.a.a.a.c.a(c.a.a.a.a.g, java.nio.charset.Charset, java.io.OutputStream):void
      c.a.a.a.a.c.a(java.lang.String, java.nio.charset.Charset, java.io.OutputStream):void
      c.a.a.a.a.c.a(c.a.a.a.a.e, java.io.OutputStream, boolean):void */
    public void a(OutputStream outputStream) {
        a(this.h, outputStream, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a.a.a.a.c.a(c.a.a.a.a.e, java.io.OutputStream, boolean):void
     arg types: [c.a.a.a.a.e, java.io.ByteArrayOutputStream, int]
     candidates:
      c.a.a.a.a.c.a(c.a.a.a.a.g, java.nio.charset.Charset, java.io.OutputStream):void
      c.a.a.a.a.c.a(java.lang.String, java.nio.charset.Charset, java.io.OutputStream):void
      c.a.a.a.a.c.a(c.a.a.a.a.e, java.io.OutputStream, boolean):void */
    public long c() {
        long j = 0;
        for (a b2 : this.g) {
            long e2 = b2.b().e();
            if (e2 < 0) {
                return -1;
            }
            j += e2;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(this.h, (OutputStream) byteArrayOutputStream, false);
            return j + ((long) byteArrayOutputStream.toByteArray().length);
        } catch (IOException e3) {
            return -1;
        }
    }
}
