package c.a.a.a.a;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* compiled from: Header */
public class b implements Iterable<g> {

    /* renamed from: a  reason: collision with root package name */
    private final List<g> f186a = new LinkedList();

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, List<g>> f187b = new HashMap();

    public void a(g gVar) {
        if (gVar != null) {
            String lowerCase = gVar.a().toLowerCase(Locale.US);
            Object obj = this.f187b.get(lowerCase);
            if (obj == null) {
                obj = new LinkedList();
                this.f187b.put(lowerCase, obj);
            }
            obj.add(gVar);
            this.f186a.add(gVar);
        }
    }

    public g a(String str) {
        if (str == null) {
            return null;
        }
        List list = this.f187b.get(str.toLowerCase(Locale.US));
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (g) list.get(0);
    }

    public Iterator<g> iterator() {
        return Collections.unmodifiableList(this.f186a).iterator();
    }

    public String toString() {
        return this.f186a.toString();
    }
}
