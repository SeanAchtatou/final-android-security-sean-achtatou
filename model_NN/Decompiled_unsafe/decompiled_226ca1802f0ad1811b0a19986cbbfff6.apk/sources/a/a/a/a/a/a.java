package a.a.a.a.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.util.av;
import com.avast.android.generic.util.z;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"NewApi"})
/* compiled from: ErrorHandler */
public class a implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private static a f2a = new a();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Context f3b;

    /* renamed from: c  reason: collision with root package name */
    private Thread.UncaughtExceptionHandler f4c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e = null;
    private d f = null;
    private boolean g = true;

    private a() {
    }

    public static a a() {
        return f2a;
    }

    public a a(String str) {
        this.d = str;
        return this;
    }

    public a b(String str) {
        this.e = str;
        return this;
    }

    public a a(Context context) {
        this.f3b = context;
        if (this.g) {
            this.f4c = Thread.getDefaultUncaughtExceptionHandler();
        }
        Thread.setDefaultUncaughtExceptionHandler(this);
        return this;
    }

    public void a(String str, Throwable th) {
        try {
            JSONObject a2 = b().a();
            a2.put("exception", th.getClass().getName());
            a2.put("message", str != null ? str + " (" + th.getMessage() + ")" : th.getMessage());
            a2.put("stacktrace", a(th));
            c(a2.toString(1));
            z.a("AvastGeneric", "Handled uncaught exception (" + str + ")", th);
        } catch (JSONException e2) {
            e2.printStackTrace();
        } catch (Throwable th2) {
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        try {
            a("Crash report", th);
            new b(this, th).start();
        } catch (Throwable th2) {
        }
        this.f4c.uncaughtException(thread, th);
    }

    private void c(String str) {
        new Thread(new c(this, str)).start();
    }

    private String a(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        printWriter.close();
        return stringWriter.toString();
    }

    private synchronized d b() {
        if (this.f == null) {
            this.f = new d(null);
            PackageInfo d2 = d();
            try {
                ab abVar = (ab) aa.a(this.f3b, ab.class);
                if (abVar != null) {
                    this.f.f9a = abVar.r();
                    if (this.f.f9a == null || "".equals(this.f.f9a)) {
                        this.f.f9a = "N/A";
                    }
                } else {
                    this.f.f9a = "N/A";
                }
            } catch (Exception e2) {
                this.f.f9a = "N/A";
            }
            if (this.f3b.getPackageName().equals("com.avast.android.vps") || this.f3b.getPackageName().equals("com.avast.android.mobilesecurity")) {
                this.f.d = d2.versionName;
            } else {
                av a2 = av.a(this.f3b);
                this.f.d = a2.b() + "." + a2.c() + "." + a2.d();
            }
            this.f.f10b = d2.packageName;
            this.f.f11c = d2.versionCode;
            this.f.o = c();
            this.f.e = Build.VERSION.RELEASE;
            this.f.f = Build.VERSION.CODENAME;
            this.f.g = Build.MODEL;
            this.f.h = Build.ID;
            this.f.i = Build.TIME;
            this.f.j = Build.TYPE;
            this.f.k = Build.TAGS;
            this.f.l = Build.BOARD;
            this.f.m = Build.BRAND;
            this.f.n = Build.DEVICE;
        }
        return this.f;
    }

    private long c() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
    }

    private PackageInfo d() {
        try {
            return this.f3b.getPackageManager().getPackageInfo(this.f3b.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return new PackageInfo();
        }
    }
}
