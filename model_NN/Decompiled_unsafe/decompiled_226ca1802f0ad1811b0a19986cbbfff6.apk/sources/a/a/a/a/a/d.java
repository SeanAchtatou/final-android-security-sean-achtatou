package a.a.a.a.a;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ErrorHandler */
class d {

    /* renamed from: a  reason: collision with root package name */
    String f9a;

    /* renamed from: b  reason: collision with root package name */
    String f10b;

    /* renamed from: c  reason: collision with root package name */
    int f11c;
    String d;
    String e;
    String f;
    String g;
    String h;
    long i;
    String j;
    String k;
    String l;
    String m;
    String n;
    long o;

    private d() {
    }

    /* synthetic */ d(b bVar) {
        this();
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("hwid", this.f9a);
            jSONObject.put("packageName", this.f10b);
            jSONObject.put("versionCode", this.f11c);
            jSONObject.put("versionName", this.d);
            jSONObject.put("aRelease", this.e);
            jSONObject.put("aCodename", this.f);
            jSONObject.put("model", this.g);
            jSONObject.put("buildId", this.h);
            jSONObject.put("buildTime", this.i);
            jSONObject.put("buildType", this.j);
            jSONObject.put("buildTags", this.k);
            jSONObject.put("board", this.l);
            jSONObject.put("brand", this.m);
            jSONObject.put("device", this.n);
            jSONObject.put("avalaibleSpace", this.o);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }
}
