package a.a.a.a.a;

/* compiled from: ErrorHandler */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f7a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f8b;

    c(a aVar, String str) {
        this.f8b = aVar;
        this.f7a = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r6 = this;
            r2 = 0
            a.a.a.a.a.e r1 = new a.a.a.a.a.e     // Catch:{ Throwable -> 0x008e, all -> 0x009d }
            a.a.a.a.a.a r0 = r6.f8b     // Catch:{ Throwable -> 0x008e, all -> 0x009d }
            android.content.Context r0 = r0.f3b     // Catch:{ Throwable -> 0x008e, all -> 0x009d }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x008e, all -> 0x009d }
            android.a.a.a r0 = r1.a()     // Catch:{ Throwable -> 0x00a7 }
            a.a.a.a.a.a r2 = r6.f8b     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r2 = r2.e     // Catch:{ Throwable -> 0x00a7 }
            if (r2 == 0) goto L_0x0027
            org.apache.http.params.HttpParams r2 = r0.getParams()     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r3 = "http.useragent"
            a.a.a.a.a.a r4 = r6.f8b     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r4 = r4.e     // Catch:{ Throwable -> 0x00a7 }
            r2.setParameter(r3, r4)     // Catch:{ Throwable -> 0x00a7 }
        L_0x0027:
            org.apache.http.client.methods.HttpPost r2 = new org.apache.http.client.methods.HttpPost     // Catch:{ Throwable -> 0x00a7 }
            a.a.a.a.a.a r3 = r6.f8b     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r3 = r3.d     // Catch:{ Throwable -> 0x00a7 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x00a7 }
            org.apache.http.entity.StringEntity r3 = new org.apache.http.entity.StringEntity     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r4 = r6.f7a     // Catch:{ Throwable -> 0x00a7 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00a7 }
            r2.setEntity(r3)     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r3 = "Accept"
            java.lang.String r4 = "application/json"
            r2.setHeader(r3, r4)     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r3 = "Content-type"
            java.lang.String r4 = "application/json"
            r2.setHeader(r3, r4)     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r3 = "AvastGeneric"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a7 }
            r4.<init>()     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r5 = "ErrorHandler posting data... "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r5 = r6.f7a     // Catch:{ Throwable -> 0x00a7 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x00a7 }
            com.avast.android.generic.util.z.a(r3, r4)     // Catch:{ Throwable -> 0x00a7 }
            org.apache.http.HttpResponse r0 = r0.execute(r2)     // Catch:{ Throwable -> 0x00a7 }
            org.apache.http.StatusLine r0 = r0.getStatusLine()     // Catch:{ Throwable -> 0x00a7 }
            int r0 = r0.getStatusCode()     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r2 = "AvastGeneric"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a7 }
            r3.<init>()     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r4 = "ErrorHandler post status code: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00a7 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00a7 }
            com.avast.android.generic.util.z.a(r2, r0)     // Catch:{ Throwable -> 0x00a7 }
            if (r1 == 0) goto L_0x008d
            r1.b()
        L_0x008d:
            return
        L_0x008e:
            r0 = move-exception
            r1 = r2
        L_0x0090:
            java.lang.String r2 = "AvastGeneric"
            java.lang.String r3 = "ErrorHandler error"
            com.avast.android.generic.util.z.a(r2, r3, r0)     // Catch:{ all -> 0x00a5 }
            if (r1 == 0) goto L_0x008d
            r1.b()
            goto L_0x008d
        L_0x009d:
            r0 = move-exception
            r1 = r2
        L_0x009f:
            if (r1 == 0) goto L_0x00a4
            r1.b()
        L_0x00a4:
            throw r0
        L_0x00a5:
            r0 = move-exception
            goto L_0x009f
        L_0x00a7:
            r0 = move-exception
            goto L_0x0090
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.a.a.c.run():void");
    }
}
