package a.a.a.a.a;

import android.a.a.a;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import com.avast.android.generic.util.z;
import java.util.Locale;
import org.apache.http.client.params.HttpClientParams;

/* compiled from: ServerConnector */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private a f12a;

    /* renamed from: b  reason: collision with root package name */
    private Context f13b;

    public e(Context context) {
        this.f13b = context;
    }

    public a a() {
        d();
        return this.f12a;
    }

    public void b() {
        if (this.f12a != null) {
            this.f12a.a();
        }
        this.f12a = null;
    }

    private void d() {
        if (this.f12a == null) {
            this.f12a = a.a(c());
            HttpClientParams.setRedirecting(this.f12a.getParams(), true);
        }
    }

    public String c() {
        PackageInfo e = e();
        String string = Settings.Secure.getString(this.f13b.getContentResolver(), "android_id");
        String str = Build.VERSION.RELEASE;
        String str2 = Build.ID;
        String str3 = Build.MODEL;
        String format = String.format("avast/%s (%d;%s) ID/%s HW/%s Android/%s (%s)", e.versionName, Integer.valueOf(e.versionCode), Locale.getDefault().toString(), string, str3, str, str2);
        z.a("AvastGeneric", "HTTP UA: " + format);
        return format;
    }

    private PackageInfo e() {
        try {
            return this.f13b.getPackageManager().getPackageInfo(this.f13b.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return new PackageInfo();
        }
    }
}
