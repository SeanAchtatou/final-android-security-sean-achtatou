package android.a.a;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;

/* compiled from: AndroidHttpClient */
public final class a implements HttpClient {

    /* renamed from: a  reason: collision with root package name */
    public static long f14a = 256;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final HttpRequestInterceptor f15b = new b();

    /* renamed from: c  reason: collision with root package name */
    private final HttpClient f16c;
    private RuntimeException d = new IllegalStateException("AndroidHttpClient created and never closed");
    /* access modifiers changed from: private */
    public volatile e e;

    public static a a(String str, Context context) {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 20000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, FragmentTransaction.TRANSIT_EXIT_MASK);
        HttpClientParams.setRedirecting(basicHttpParams, false);
        HttpProtocolParams.setUserAgent(basicHttpParams, str);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        return new a(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
    }

    public static a a(String str) {
        return a(str, (Context) null);
    }

    private a(ClientConnectionManager clientConnectionManager, HttpParams httpParams) {
        this.f16c = new c(this, clientConnectionManager, httpParams);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        if (this.d != null) {
            Log.e("AndroidHttpClient", "Leak found", this.d);
            this.d = null;
        }
    }

    public void a() {
        if (this.d != null) {
            getConnectionManager().shutdown();
            this.d = null;
        }
    }

    public HttpParams getParams() {
        return this.f16c.getParams();
    }

    public ClientConnectionManager getConnectionManager() {
        return this.f16c.getConnectionManager();
    }

    public HttpResponse execute(HttpUriRequest httpUriRequest) {
        return this.f16c.execute(httpUriRequest);
    }

    public HttpResponse execute(HttpUriRequest httpUriRequest, HttpContext httpContext) {
        return this.f16c.execute(httpUriRequest, httpContext);
    }

    public HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest) {
        return this.f16c.execute(httpHost, httpRequest);
    }

    public HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest, HttpContext httpContext) {
        return this.f16c.execute(httpHost, httpRequest, httpContext);
    }

    public <T> T execute(HttpUriRequest httpUriRequest, ResponseHandler<? extends T> responseHandler) {
        return this.f16c.execute(httpUriRequest, responseHandler);
    }

    public <T> T execute(HttpUriRequest httpUriRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext) {
        return this.f16c.execute(httpUriRequest, responseHandler, httpContext);
    }

    public <T> T execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler) {
        return this.f16c.execute(httpHost, httpRequest, responseHandler);
    }

    public <T> T execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext) {
        return this.f16c.execute(httpHost, httpRequest, responseHandler, httpContext);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(org.apache.http.client.methods.HttpUriRequest r7, boolean r8) {
        /*
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r0 = "curl "
            r2.append(r0)
            org.apache.http.Header[] r1 = r7.getAllHeaders()
            int r3 = r1.length
            r0 = 0
        L_0x0010:
            if (r0 >= r3) goto L_0x0047
            r4 = r1[r0]
            if (r8 != 0) goto L_0x0031
            java.lang.String r5 = r4.getName()
            java.lang.String r6 = "Authorization"
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x002e
            java.lang.String r5 = r4.getName()
            java.lang.String r6 = "Cookie"
            boolean r5 = r5.equals(r6)
            if (r5 == 0) goto L_0x0031
        L_0x002e:
            int r0 = r0 + 1
            goto L_0x0010
        L_0x0031:
            java.lang.String r5 = "--header \""
            r2.append(r5)
            java.lang.String r4 = r4.toString()
            java.lang.String r4 = r4.trim()
            r2.append(r4)
            java.lang.String r4 = "\" "
            r2.append(r4)
            goto L_0x002e
        L_0x0047:
            java.net.URI r1 = r7.getURI()
            boolean r0 = r7 instanceof org.apache.http.impl.client.RequestWrapper
            if (r0 == 0) goto L_0x00af
            r0 = r7
            org.apache.http.impl.client.RequestWrapper r0 = (org.apache.http.impl.client.RequestWrapper) r0
            org.apache.http.HttpRequest r0 = r0.getOriginal()
            boolean r3 = r0 instanceof org.apache.http.client.methods.HttpUriRequest
            if (r3 == 0) goto L_0x00af
            org.apache.http.client.methods.HttpUriRequest r0 = (org.apache.http.client.methods.HttpUriRequest) r0
            java.net.URI r0 = r0.getURI()
        L_0x0060:
            java.lang.String r1 = "\""
            r2.append(r1)
            r2.append(r0)
            java.lang.String r0 = "\""
            r2.append(r0)
            boolean r0 = r7 instanceof org.apache.http.HttpEntityEnclosingRequest
            if (r0 == 0) goto L_0x00a4
            org.apache.http.HttpEntityEnclosingRequest r7 = (org.apache.http.HttpEntityEnclosingRequest) r7
            org.apache.http.HttpEntity r0 = r7.getEntity()
            if (r0 == 0) goto L_0x00a4
            boolean r1 = r0.isRepeatable()
            if (r1 == 0) goto L_0x00a4
            long r3 = r0.getContentLength()
            r5 = 1024(0x400, double:5.06E-321)
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 >= 0) goto L_0x00a9
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            r0.writeTo(r1)
            java.lang.String r0 = r1.toString()
            java.lang.String r1 = " --data-ascii \""
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = "\""
            r0.append(r1)
        L_0x00a4:
            java.lang.String r0 = r2.toString()
            return r0
        L_0x00a9:
            java.lang.String r0 = " [TOO MUCH DATA TO INCLUDE]"
            r2.append(r0)
            goto L_0x00a4
        L_0x00af:
            r0 = r1
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: android.a.a.a.b(org.apache.http.client.methods.HttpUriRequest, boolean):java.lang.String");
    }
}
