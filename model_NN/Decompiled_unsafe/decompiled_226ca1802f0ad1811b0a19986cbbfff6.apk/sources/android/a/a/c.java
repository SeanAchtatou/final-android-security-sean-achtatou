package android.a.a;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HttpContext;

/* compiled from: AndroidHttpClient */
class c extends DefaultHttpClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f17a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(a aVar, ClientConnectionManager clientConnectionManager, HttpParams httpParams) {
        super(clientConnectionManager, httpParams);
        this.f17a = aVar;
    }

    /* access modifiers changed from: protected */
    public BasicHttpProcessor createHttpProcessor() {
        BasicHttpProcessor createHttpProcessor = c.super.createHttpProcessor();
        createHttpProcessor.addRequestInterceptor(a.f15b);
        createHttpProcessor.addRequestInterceptor(new d(this.f17a, null));
        return createHttpProcessor;
    }

    /* access modifiers changed from: protected */
    public HttpContext createHttpContext() {
        BasicHttpContext basicHttpContext = new BasicHttpContext();
        basicHttpContext.setAttribute("http.authscheme-registry", getAuthSchemes());
        basicHttpContext.setAttribute("http.cookiespec-registry", getCookieSpecs());
        basicHttpContext.setAttribute("http.auth.credentials-provider", getCredentialsProvider());
        return basicHttpContext;
    }
}
