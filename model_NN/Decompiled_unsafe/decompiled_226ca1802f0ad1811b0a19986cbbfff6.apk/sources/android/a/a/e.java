package android.a.a;

import android.util.Log;

/* compiled from: AndroidHttpClient */
class e {

    /* renamed from: a  reason: collision with root package name */
    private final String f19a;

    /* renamed from: b  reason: collision with root package name */
    private final int f20b;

    /* access modifiers changed from: private */
    public boolean a() {
        return Log.isLoggable(this.f19a, this.f20b);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        Log.println(this.f20b, this.f19a, str);
    }
}
