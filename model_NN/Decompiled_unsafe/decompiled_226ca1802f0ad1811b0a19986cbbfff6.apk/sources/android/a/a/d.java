package android.a.a;

import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;

/* compiled from: AndroidHttpClient */
class d implements HttpRequestInterceptor {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f18a;

    private d(a aVar) {
        this.f18a = aVar;
    }

    /* synthetic */ d(a aVar, b bVar) {
        this(aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.a.a.a.a(org.apache.http.client.methods.HttpUriRequest, boolean):java.lang.String
     arg types: [org.apache.http.client.methods.HttpUriRequest, int]
     candidates:
      android.a.a.a.a(java.lang.String, android.content.Context):android.a.a.a
      android.a.a.a.a(org.apache.http.client.methods.HttpUriRequest, boolean):java.lang.String */
    public void process(HttpRequest httpRequest, HttpContext httpContext) {
        e a2 = this.f18a.e;
        if (a2 != null && a2.a() && (httpRequest instanceof HttpUriRequest)) {
            a2.a(a.b((HttpUriRequest) httpRequest, false));
        }
    }
}
