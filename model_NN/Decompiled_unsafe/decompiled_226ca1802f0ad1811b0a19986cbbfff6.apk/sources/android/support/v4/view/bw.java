package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.a.a;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ViewPager */
class bw extends a {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ViewPager f102b;

    bw(ViewPager viewPager) {
        this.f102b = viewPager;
    }

    public void d(View view, AccessibilityEvent accessibilityEvent) {
        super.d(view, accessibilityEvent);
        accessibilityEvent.setClassName(ViewPager.class.getName());
    }

    public void a(View view, a aVar) {
        boolean z = true;
        super.a(view, aVar);
        aVar.b(ViewPager.class.getName());
        if (this.f102b.h == null || this.f102b.h.getCount() <= 1) {
            z = false;
        }
        aVar.i(z);
        if (this.f102b.h != null && this.f102b.i >= 0 && this.f102b.i < this.f102b.h.getCount() - 1) {
            aVar.a((int) FragmentTransaction.TRANSIT_ENTER_MASK);
        }
        if (this.f102b.h != null && this.f102b.i > 0 && this.f102b.i < this.f102b.h.getCount()) {
            aVar.a((int) FragmentTransaction.TRANSIT_EXIT_MASK);
        }
    }

    public boolean a(View view, int i, Bundle bundle) {
        if (super.a(view, i, bundle)) {
            return true;
        }
        switch (i) {
            case FragmentTransaction.TRANSIT_ENTER_MASK:
                if (this.f102b.h == null || this.f102b.i < 0 || this.f102b.i >= this.f102b.h.getCount() - 1) {
                    return false;
                }
                this.f102b.a(this.f102b.i + 1);
                return true;
            case FragmentTransaction.TRANSIT_EXIT_MASK:
                if (this.f102b.h == null || this.f102b.i <= 0 || this.f102b.i >= this.f102b.h.getCount()) {
                    return false;
                }
                this.f102b.a(this.f102b.i - 1);
                return true;
            default:
                return false;
        }
    }
}
