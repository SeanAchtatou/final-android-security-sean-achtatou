package android.support.v4.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.b.a;
import android.support.v4.widget.l;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import com.actionbarsherlock.R;
import com.actionbarsherlock.view.Menu;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ViewPager extends ViewGroup {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final int[] f70a = {16842931};
    private static final cb ae = new cb();

    /* renamed from: c  reason: collision with root package name */
    private static final Comparator<bv> f71c = new br();
    private static final Interpolator d = new bs();
    private boolean A;
    private int B;
    private int C;
    private int D;
    private float E;
    private float F;
    private float G;
    private float H;
    private int I = -1;
    private VelocityTracker J;
    private int K;
    private int L;
    private int M;
    private int N;
    private boolean O;
    private long P;
    private l Q;
    private l R;
    private boolean S = true;
    private boolean T = false;
    private boolean U;
    private int V;
    private by W;
    private by Z;
    private bx aa;
    private bz ab;
    private int ac;
    private ArrayList<View> ad;
    private final Runnable af = new bt(this);
    private int ag = 0;

    /* renamed from: b  reason: collision with root package name */
    private int f72b;
    private final ArrayList<bv> e = new ArrayList<>();
    private final bv f = new bv();
    private final Rect g = new Rect();
    /* access modifiers changed from: private */
    public ae h;
    /* access modifiers changed from: private */
    public int i;
    private int j = -1;
    private Parcelable k = null;
    private ClassLoader l = null;
    private Scroller m;
    private int n;
    private Drawable o;
    private int p;
    private int q;
    private float r = -3.4028235E38f;
    private float s = Float.MAX_VALUE;
    private int t;
    private int u;
    private boolean v;
    private boolean w;
    private boolean x;
    private int y = 1;
    private boolean z;

    public ViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        setWillNotDraw(false);
        setDescendantFocusability(Menu.CATEGORY_ALTERNATIVE);
        setFocusable(true);
        Context context = getContext();
        this.m = new Scroller(context, d);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.D = bg.a(viewConfiguration);
        this.K = (int) (400.0f * f2);
        this.L = viewConfiguration.getScaledMaximumFlingVelocity();
        this.Q = new l(context);
        this.R = new l(context);
        this.M = (int) (25.0f * f2);
        this.N = (int) (2.0f * f2);
        this.B = (int) (16.0f * f2);
        at.a(this, new bw(this));
        if (at.c(this) == 0) {
            at.b(this, 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.af);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: private */
    public void e(int i2) {
        if (this.ag != i2) {
            this.ag = i2;
            if (this.ab != null) {
                b(i2 != 0);
            }
            if (this.W != null) {
                this.W.b(i2);
            }
        }
    }

    public ae b() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public void a(bx bxVar) {
        this.aa = bxVar;
    }

    private int k() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.bv, int, android.support.v4.view.bv):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void a(int i2) {
        boolean z2;
        this.x = false;
        if (!this.S) {
            z2 = true;
        } else {
            z2 = false;
        }
        a(i2, z2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.bv, int, android.support.v4.view.bv):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void a(int i2, boolean z2) {
        this.x = false;
        a(i2, z2, false);
    }

    public int c() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3) {
        a(i2, z2, z3, 0);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3, int i3) {
        boolean z4 = false;
        if (this.h == null || this.h.getCount() <= 0) {
            c(false);
        } else if (z3 || this.i != i2 || this.e.size() == 0) {
            if (i2 < 0) {
                i2 = 0;
            } else if (i2 >= this.h.getCount()) {
                i2 = this.h.getCount() - 1;
            }
            int i4 = this.y;
            if (i2 > this.i + i4 || i2 < this.i - i4) {
                for (int i5 = 0; i5 < this.e.size(); i5++) {
                    this.e.get(i5).f101c = true;
                }
            }
            if (this.i != i2) {
                z4 = true;
            }
            if (this.S) {
                this.i = i2;
                if (z4 && this.W != null) {
                    this.W.a(i2);
                }
                if (z4 && this.Z != null) {
                    this.Z.a(i2);
                }
                requestLayout();
                return;
            }
            b(i2);
            a(i2, z2, i3, z4);
        } else {
            c(false);
        }
    }

    private void a(int i2, boolean z2, int i3, boolean z3) {
        int i4;
        bv c2 = c(i2);
        if (c2 != null) {
            i4 = (int) (Math.max(this.r, Math.min(c2.e, this.s)) * ((float) k()));
        } else {
            i4 = 0;
        }
        if (z2) {
            a(i4, 0, i3);
            if (z3 && this.W != null) {
                this.W.a(i2);
            }
            if (z3 && this.Z != null) {
                this.Z.a(i2);
                return;
            }
            return;
        }
        if (z3 && this.W != null) {
            this.W.a(i2);
        }
        if (z3 && this.Z != null) {
            this.Z.a(i2);
        }
        a(false);
        scrollTo(i4, 0);
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        if (this.ac == 2) {
            i3 = (i2 - 1) - i3;
        }
        return ((LayoutParams) this.ad.get(i3).getLayoutParams()).f;
    }

    /* access modifiers changed from: package-private */
    public by a(by byVar) {
        by byVar2 = this.Z;
        this.Z = byVar;
        return byVar2;
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.o;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.o;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    public float a(float f2) {
        return (float) Math.sin((double) ((float) (((double) (f2 - 0.5f)) * 0.4712389167638204d)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4) {
        int abs;
        if (getChildCount() == 0) {
            c(false);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int i5 = i2 - scrollX;
        int i6 = i3 - scrollY;
        if (i5 == 0 && i6 == 0) {
            a(false);
            d();
            e(0);
            return;
        }
        c(true);
        e(2);
        int k2 = k();
        int i7 = k2 / 2;
        float a2 = (((float) i7) * a(Math.min(1.0f, (((float) Math.abs(i5)) * 1.0f) / ((float) k2)))) + ((float) i7);
        int abs2 = Math.abs(i4);
        if (abs2 > 0) {
            abs = Math.round(1000.0f * Math.abs(a2 / ((float) abs2))) * 4;
        } else {
            abs = (int) (((((float) Math.abs(i5)) / ((((float) k2) * this.h.getPageWidth(this.i)) + ((float) this.n))) + 1.0f) * 100.0f);
        }
        this.m.startScroll(scrollX, scrollY, i5, i6, Math.min(abs, 600));
        at.b(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ae.instantiateItem(android.view.ViewGroup, int):java.lang.Object
     arg types: [android.support.v4.view.ViewPager, int]
     candidates:
      android.support.v4.view.ae.instantiateItem(android.view.View, int):java.lang.Object
      android.support.v4.view.ae.instantiateItem(android.view.ViewGroup, int):java.lang.Object */
    /* access modifiers changed from: package-private */
    public bv a(int i2, int i3) {
        bv bvVar = new bv();
        bvVar.f100b = i2;
        bvVar.f99a = this.h.instantiateItem((ViewGroup) this, i2);
        bvVar.d = this.h.getPageWidth(i2);
        if (i3 < 0 || i3 >= this.e.size()) {
            this.e.add(bvVar);
        } else {
            this.e.add(i3, bvVar);
        }
        return bvVar;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        b(this.i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ae.setPrimaryItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.ae.setPrimaryItem(android.view.View, int, java.lang.Object):void
      android.support.v4.view.ae.setPrimaryItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ae.destroyItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.ae.destroyItem(android.view.View, int, java.lang.Object):void
      android.support.v4.view.ae.destroyItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ff, code lost:
        if (r2.f100b == r0.i) goto L_0x0101;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(int r18) {
        /*
            r17 = this;
            r3 = 0
            r2 = 2
            r0 = r17
            int r4 = r0.i
            r0 = r18
            if (r4 == r0) goto L_0x0328
            r0 = r17
            int r2 = r0.i
            r0 = r18
            if (r2 >= r0) goto L_0x0030
            r2 = 66
        L_0x0014:
            r0 = r17
            int r3 = r0.i
            r0 = r17
            android.support.v4.view.bv r3 = r0.c(r3)
            r0 = r18
            r1 = r17
            r1.i = r0
            r4 = r3
            r3 = r2
        L_0x0026:
            r0 = r17
            android.support.v4.view.ae r2 = r0.h
            if (r2 != 0) goto L_0x0033
            r17.l()
        L_0x002f:
            return
        L_0x0030:
            r2 = 17
            goto L_0x0014
        L_0x0033:
            r0 = r17
            boolean r2 = r0.x
            if (r2 == 0) goto L_0x003d
            r17.l()
            goto L_0x002f
        L_0x003d:
            android.os.IBinder r2 = r17.getWindowToken()
            if (r2 == 0) goto L_0x002f
            r0 = r17
            android.support.v4.view.ae r2 = r0.h
            r0 = r17
            r2.startUpdate(r0)
            r0 = r17
            int r2 = r0.y
            r5 = 0
            r0 = r17
            int r6 = r0.i
            int r6 = r6 - r2
            int r10 = java.lang.Math.max(r5, r6)
            r0 = r17
            android.support.v4.view.ae r5 = r0.h
            int r11 = r5.getCount()
            int r5 = r11 + -1
            r0 = r17
            int r6 = r0.i
            int r2 = r2 + r6
            int r12 = java.lang.Math.min(r5, r2)
            r0 = r17
            int r2 = r0.f72b
            if (r11 == r2) goto L_0x00da
            android.content.res.Resources r2 = r17.getResources()     // Catch:{ NotFoundException -> 0x00d0 }
            int r3 = r17.getId()     // Catch:{ NotFoundException -> 0x00d0 }
            java.lang.String r2 = r2.getResourceName(r3)     // Catch:{ NotFoundException -> 0x00d0 }
        L_0x007f:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r17
            int r5 = r0.f72b
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ", found: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r11)
            java.lang.String r5 = " Pager id: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " Pager class: "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.Class r4 = r17.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = " Problematic adapter: "
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r17
            android.support.v4.view.ae r4 = r0.h
            java.lang.Class r4 = r4.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r3.<init>(r2)
            throw r3
        L_0x00d0:
            r2 = move-exception
            int r2 = r17.getId()
            java.lang.String r2 = java.lang.Integer.toHexString(r2)
            goto L_0x007f
        L_0x00da:
            r6 = 0
            r2 = 0
            r5 = r2
        L_0x00dd:
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            int r2 = r2.size()
            if (r5 >= r2) goto L_0x0325
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.bv r2 = (android.support.v4.view.bv) r2
            int r7 = r2.f100b
            r0 = r17
            int r8 = r0.i
            if (r7 < r8) goto L_0x01de
            int r7 = r2.f100b
            r0 = r17
            int r8 = r0.i
            if (r7 != r8) goto L_0x0325
        L_0x0101:
            if (r2 != 0) goto L_0x0322
            if (r11 <= 0) goto L_0x0322
            r0 = r17
            int r2 = r0.i
            r0 = r17
            android.support.v4.view.bv r2 = r0.a(r2, r5)
            r9 = r2
        L_0x0110:
            if (r9 == 0) goto L_0x018f
            r8 = 0
            int r7 = r5 + -1
            if (r7 < 0) goto L_0x01e3
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.bv r2 = (android.support.v4.view.bv) r2
        L_0x0121:
            r6 = 1073741824(0x40000000, float:2.0)
            float r13 = r9.d
            float r6 = r6 - r13
            int r13 = r17.getPaddingLeft()
            float r13 = (float) r13
            int r14 = r17.k()
            float r14 = (float) r14
            float r13 = r13 / r14
            float r13 = r13 + r6
            r0 = r17
            int r6 = r0.i
            int r6 = r6 + -1
            r15 = r6
            r6 = r8
            r8 = r15
            r16 = r7
            r7 = r5
            r5 = r16
        L_0x0140:
            if (r8 < 0) goto L_0x014a
            int r14 = (r6 > r13 ? 1 : (r6 == r13 ? 0 : -1))
            if (r14 < 0) goto L_0x0216
            if (r8 >= r10) goto L_0x0216
            if (r2 != 0) goto L_0x01e6
        L_0x014a:
            float r6 = r9.d
            int r8 = r7 + 1
            r2 = 1073741824(0x40000000, float:2.0)
            int r2 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x018a
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            int r2 = r2.size()
            if (r8 >= r2) goto L_0x024c
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            java.lang.Object r2 = r2.get(r8)
            android.support.v4.view.bv r2 = (android.support.v4.view.bv) r2
        L_0x0168:
            int r5 = r17.getPaddingRight()
            float r5 = (float) r5
            int r10 = r17.k()
            float r10 = (float) r10
            float r5 = r5 / r10
            r10 = 1073741824(0x40000000, float:2.0)
            float r10 = r10 + r5
            r0 = r17
            int r5 = r0.i
            int r5 = r5 + 1
            r15 = r5
            r5 = r6
            r6 = r8
            r8 = r15
        L_0x0180:
            if (r8 >= r11) goto L_0x018a
            int r13 = (r5 > r10 ? 1 : (r5 == r10 ? 0 : -1))
            if (r13 < 0) goto L_0x0289
            if (r8 <= r12) goto L_0x0289
            if (r2 != 0) goto L_0x024f
        L_0x018a:
            r0 = r17
            r0.a(r9, r7, r4)
        L_0x018f:
            r0 = r17
            android.support.v4.view.ae r4 = r0.h
            r0 = r17
            int r5 = r0.i
            if (r9 == 0) goto L_0x02d3
            java.lang.Object r2 = r9.f99a
        L_0x019b:
            r0 = r17
            r4.setPrimaryItem(r0, r5, r2)
            r0 = r17
            android.support.v4.view.ae r2 = r0.h
            r0 = r17
            r2.finishUpdate(r0)
            int r5 = r17.getChildCount()
            r2 = 0
            r4 = r2
        L_0x01af:
            if (r4 >= r5) goto L_0x02d6
            r0 = r17
            android.view.View r6 = r0.getChildAt(r4)
            android.view.ViewGroup$LayoutParams r2 = r6.getLayoutParams()
            android.support.v4.view.ViewPager$LayoutParams r2 = (android.support.v4.view.ViewPager.LayoutParams) r2
            r2.f = r4
            boolean r7 = r2.f73a
            if (r7 != 0) goto L_0x01da
            float r7 = r2.f75c
            r8 = 0
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x01da
            r0 = r17
            android.support.v4.view.bv r6 = r0.a(r6)
            if (r6 == 0) goto L_0x01da
            float r7 = r6.d
            r2.f75c = r7
            int r6 = r6.f100b
            r2.e = r6
        L_0x01da:
            int r2 = r4 + 1
            r4 = r2
            goto L_0x01af
        L_0x01de:
            int r2 = r5 + 1
            r5 = r2
            goto L_0x00dd
        L_0x01e3:
            r2 = 0
            goto L_0x0121
        L_0x01e6:
            int r14 = r2.f100b
            if (r8 != r14) goto L_0x0210
            boolean r14 = r2.f101c
            if (r14 != 0) goto L_0x0210
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r14 = r0.e
            r14.remove(r5)
            r0 = r17
            android.support.v4.view.ae r14 = r0.h
            java.lang.Object r2 = r2.f99a
            r0 = r17
            r14.destroyItem(r0, r8, r2)
            int r5 = r5 + -1
            int r7 = r7 + -1
            if (r5 < 0) goto L_0x0214
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.bv r2 = (android.support.v4.view.bv) r2
        L_0x0210:
            int r8 = r8 + -1
            goto L_0x0140
        L_0x0214:
            r2 = 0
            goto L_0x0210
        L_0x0216:
            if (r2 == 0) goto L_0x0230
            int r14 = r2.f100b
            if (r8 != r14) goto L_0x0230
            float r2 = r2.d
            float r6 = r6 + r2
            int r5 = r5 + -1
            if (r5 < 0) goto L_0x022e
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.bv r2 = (android.support.v4.view.bv) r2
            goto L_0x0210
        L_0x022e:
            r2 = 0
            goto L_0x0210
        L_0x0230:
            int r2 = r5 + 1
            r0 = r17
            android.support.v4.view.bv r2 = r0.a(r8, r2)
            float r2 = r2.d
            float r6 = r6 + r2
            int r7 = r7 + 1
            if (r5 < 0) goto L_0x024a
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.bv r2 = (android.support.v4.view.bv) r2
            goto L_0x0210
        L_0x024a:
            r2 = 0
            goto L_0x0210
        L_0x024c:
            r2 = 0
            goto L_0x0168
        L_0x024f:
            int r13 = r2.f100b
            if (r8 != r13) goto L_0x031d
            boolean r13 = r2.f101c
            if (r13 != 0) goto L_0x031d
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r13 = r0.e
            r13.remove(r6)
            r0 = r17
            android.support.v4.view.ae r13 = r0.h
            java.lang.Object r2 = r2.f99a
            r0 = r17
            r13.destroyItem(r0, r8, r2)
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            int r2 = r2.size()
            if (r6 >= r2) goto L_0x0287
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            java.lang.Object r2 = r2.get(r6)
            android.support.v4.view.bv r2 = (android.support.v4.view.bv) r2
        L_0x027d:
            r15 = r5
            r5 = r2
            r2 = r15
        L_0x0280:
            int r8 = r8 + 1
            r15 = r2
            r2 = r5
            r5 = r15
            goto L_0x0180
        L_0x0287:
            r2 = 0
            goto L_0x027d
        L_0x0289:
            if (r2 == 0) goto L_0x02ae
            int r13 = r2.f100b
            if (r8 != r13) goto L_0x02ae
            float r2 = r2.d
            float r5 = r5 + r2
            int r6 = r6 + 1
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            int r2 = r2.size()
            if (r6 >= r2) goto L_0x02ac
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            java.lang.Object r2 = r2.get(r6)
            android.support.v4.view.bv r2 = (android.support.v4.view.bv) r2
        L_0x02a8:
            r15 = r5
            r5 = r2
            r2 = r15
            goto L_0x0280
        L_0x02ac:
            r2 = 0
            goto L_0x02a8
        L_0x02ae:
            r0 = r17
            android.support.v4.view.bv r2 = r0.a(r8, r6)
            int r6 = r6 + 1
            float r2 = r2.d
            float r5 = r5 + r2
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            int r2 = r2.size()
            if (r6 >= r2) goto L_0x02d1
            r0 = r17
            java.util.ArrayList<android.support.v4.view.bv> r2 = r0.e
            java.lang.Object r2 = r2.get(r6)
            android.support.v4.view.bv r2 = (android.support.v4.view.bv) r2
        L_0x02cd:
            r15 = r5
            r5 = r2
            r2 = r15
            goto L_0x0280
        L_0x02d1:
            r2 = 0
            goto L_0x02cd
        L_0x02d3:
            r2 = 0
            goto L_0x019b
        L_0x02d6:
            r17.l()
            boolean r2 = r17.hasFocus()
            if (r2 == 0) goto L_0x002f
            android.view.View r2 = r17.findFocus()
            if (r2 == 0) goto L_0x031b
            r0 = r17
            android.support.v4.view.bv r2 = r0.b(r2)
        L_0x02eb:
            if (r2 == 0) goto L_0x02f5
            int r2 = r2.f100b
            r0 = r17
            int r4 = r0.i
            if (r2 == r4) goto L_0x002f
        L_0x02f5:
            r2 = 0
        L_0x02f6:
            int r4 = r17.getChildCount()
            if (r2 >= r4) goto L_0x002f
            r0 = r17
            android.view.View r4 = r0.getChildAt(r2)
            r0 = r17
            android.support.v4.view.bv r5 = r0.a(r4)
            if (r5 == 0) goto L_0x0318
            int r5 = r5.f100b
            r0 = r17
            int r6 = r0.i
            if (r5 != r6) goto L_0x0318
            boolean r4 = r4.requestFocus(r3)
            if (r4 != 0) goto L_0x002f
        L_0x0318:
            int r2 = r2 + 1
            goto L_0x02f6
        L_0x031b:
            r2 = 0
            goto L_0x02eb
        L_0x031d:
            r15 = r5
            r5 = r2
            r2 = r15
            goto L_0x0280
        L_0x0322:
            r9 = r2
            goto L_0x0110
        L_0x0325:
            r2 = r6
            goto L_0x0101
        L_0x0328:
            r4 = r3
            r3 = r2
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.b(int):void");
    }

    private void l() {
        if (this.ac != 0) {
            if (this.ad == null) {
                this.ad = new ArrayList<>();
            } else {
                this.ad.clear();
            }
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                this.ad.add(getChildAt(i2));
            }
            Collections.sort(this.ad, ae);
        }
    }

    private void a(bv bvVar, int i2, bv bvVar2) {
        float f2;
        bv bvVar3;
        bv bvVar4;
        int count = this.h.getCount();
        int k2 = k();
        if (k2 > 0) {
            f2 = ((float) this.n) / ((float) k2);
        } else {
            f2 = 0.0f;
        }
        if (bvVar2 != null) {
            int i3 = bvVar2.f100b;
            if (i3 < bvVar.f100b) {
                float f3 = bvVar2.e + bvVar2.d + f2;
                int i4 = i3 + 1;
                int i5 = 0;
                while (i4 <= bvVar.f100b && i5 < this.e.size()) {
                    bv bvVar5 = this.e.get(i5);
                    while (true) {
                        bvVar4 = bvVar5;
                        if (i4 > bvVar4.f100b && i5 < this.e.size() - 1) {
                            i5++;
                            bvVar5 = this.e.get(i5);
                        }
                    }
                    while (i4 < bvVar4.f100b) {
                        f3 += this.h.getPageWidth(i4) + f2;
                        i4++;
                    }
                    bvVar4.e = f3;
                    f3 += bvVar4.d + f2;
                    i4++;
                }
            } else if (i3 > bvVar.f100b) {
                int size = this.e.size() - 1;
                float f4 = bvVar2.e;
                int i6 = i3 - 1;
                while (i6 >= bvVar.f100b && size >= 0) {
                    bv bvVar6 = this.e.get(size);
                    while (true) {
                        bvVar3 = bvVar6;
                        if (i6 < bvVar3.f100b && size > 0) {
                            size--;
                            bvVar6 = this.e.get(size);
                        }
                    }
                    while (i6 > bvVar3.f100b) {
                        f4 -= this.h.getPageWidth(i6) + f2;
                        i6--;
                    }
                    f4 -= bvVar3.d + f2;
                    bvVar3.e = f4;
                    i6--;
                }
            }
        }
        int size2 = this.e.size();
        float f5 = bvVar.e;
        int i7 = bvVar.f100b - 1;
        this.r = bvVar.f100b == 0 ? bvVar.e : -3.4028235E38f;
        this.s = bvVar.f100b == count + -1 ? (bvVar.e + bvVar.d) - 1.0f : Float.MAX_VALUE;
        for (int i8 = i2 - 1; i8 >= 0; i8--) {
            bv bvVar7 = this.e.get(i8);
            float f6 = f5;
            while (i7 > bvVar7.f100b) {
                f6 -= this.h.getPageWidth(i7) + f2;
                i7--;
            }
            f5 = f6 - (bvVar7.d + f2);
            bvVar7.e = f5;
            if (bvVar7.f100b == 0) {
                this.r = f5;
            }
            i7--;
        }
        float f7 = bvVar.e + bvVar.d + f2;
        int i9 = bvVar.f100b + 1;
        for (int i10 = i2 + 1; i10 < size2; i10++) {
            bv bvVar8 = this.e.get(i10);
            float f8 = f7;
            while (i9 < bvVar8.f100b) {
                f8 = this.h.getPageWidth(i9) + f2 + f8;
                i9++;
            }
            if (bvVar8.f100b == count - 1) {
                this.s = (bvVar8.d + f8) - 1.0f;
            }
            bvVar8.e = f8;
            f7 = f8 + bvVar8.d + f2;
            i9++;
        }
        this.T = false;
    }

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = a.a(new ca());

        /* renamed from: a  reason: collision with root package name */
        int f76a;

        /* renamed from: b  reason: collision with root package name */
        Parcelable f77b;

        /* renamed from: c  reason: collision with root package name */
        ClassLoader f78c;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f76a);
            parcel.writeParcelable(this.f77b, i);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.f76a + "}";
        }

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel);
            classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
            this.f76a = parcel.readInt();
            this.f77b = parcel.readParcelable(classLoader);
            this.f78c = classLoader;
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f76a = this.i;
        if (this.h != null) {
            savedState.f77b = this.h.saveState();
        }
        return savedState;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.bv, int, android.support.v4.view.bv):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (this.h != null) {
            this.h.restoreState(savedState.f77b, savedState.f78c);
            a(savedState.f76a, false, true);
            return;
        }
        this.j = savedState.f76a;
        this.k = savedState.f77b;
        this.l = savedState.f78c;
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2;
        if (!checkLayoutParams(layoutParams)) {
            layoutParams2 = generateLayoutParams(layoutParams);
        } else {
            layoutParams2 = layoutParams;
        }
        LayoutParams layoutParams3 = (LayoutParams) layoutParams2;
        layoutParams3.f73a |= view instanceof bu;
        if (!this.v) {
            super.addView(view, i2, layoutParams2);
        } else if (layoutParams3 == null || !layoutParams3.f73a) {
            layoutParams3.d = true;
            addViewInLayout(view, i2, layoutParams2);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    public void removeView(View view) {
        if (this.v) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    /* access modifiers changed from: package-private */
    public bv a(View view) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.e.size()) {
                return null;
            }
            bv bvVar = this.e.get(i3);
            if (this.h.isViewFromObject(view, bvVar.f99a)) {
                return bvVar;
            }
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public bv b(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return a(view);
            }
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            view = (View) parent;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public bv c(int i2) {
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= this.e.size()) {
                return null;
            }
            bv bvVar = this.e.get(i4);
            if (bvVar.f100b == i2) {
                return bvVar;
            }
            i3 = i4 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.S = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r0 = 0
            int r0 = getDefaultSize(r0, r14)
            r1 = 0
            int r1 = getDefaultSize(r1, r15)
            r13.setMeasuredDimension(r0, r1)
            int r0 = r13.getMeasuredWidth()
            int r1 = r0 / 10
            int r2 = r13.B
            int r1 = java.lang.Math.min(r1, r2)
            r13.C = r1
            int r1 = r13.getPaddingLeft()
            int r0 = r0 - r1
            int r1 = r13.getPaddingRight()
            int r3 = r0 - r1
            int r0 = r13.getMeasuredHeight()
            int r1 = r13.getPaddingTop()
            int r0 = r0 - r1
            int r1 = r13.getPaddingBottom()
            int r5 = r0 - r1
            int r9 = r13.getChildCount()
            r0 = 0
            r8 = r0
        L_0x003b:
            if (r8 >= r9) goto L_0x00bc
            android.view.View r10 = r13.getChildAt(r8)
            int r0 = r10.getVisibility()
            r1 = 8
            if (r0 == r1) goto L_0x00a5
            android.view.ViewGroup$LayoutParams r0 = r10.getLayoutParams()
            android.support.v4.view.ViewPager$LayoutParams r0 = (android.support.v4.view.ViewPager.LayoutParams) r0
            if (r0 == 0) goto L_0x00a5
            boolean r1 = r0.f73a
            if (r1 == 0) goto L_0x00a5
            int r1 = r0.f74b
            r6 = r1 & 7
            int r1 = r0.f74b
            r4 = r1 & 112(0x70, float:1.57E-43)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 48
            if (r4 == r7) goto L_0x0069
            r7 = 80
            if (r4 != r7) goto L_0x00a9
        L_0x0069:
            r4 = 1
            r7 = r4
        L_0x006b:
            r4 = 3
            if (r6 == r4) goto L_0x0071
            r4 = 5
            if (r6 != r4) goto L_0x00ac
        L_0x0071:
            r4 = 1
            r6 = r4
        L_0x0073:
            if (r7 == 0) goto L_0x00af
            r2 = 1073741824(0x40000000, float:2.0)
        L_0x0077:
            int r4 = r0.width
            r11 = -2
            if (r4 == r11) goto L_0x010f
            r4 = 1073741824(0x40000000, float:2.0)
            int r2 = r0.width
            r11 = -1
            if (r2 == r11) goto L_0x010c
            int r2 = r0.width
        L_0x0085:
            int r11 = r0.height
            r12 = -2
            if (r11 == r12) goto L_0x010a
            r1 = 1073741824(0x40000000, float:2.0)
            int r11 = r0.height
            r12 = -1
            if (r11 == r12) goto L_0x010a
            int r0 = r0.height
        L_0x0093:
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r4)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
            r10.measure(r2, r0)
            if (r7 == 0) goto L_0x00b4
            int r0 = r10.getMeasuredHeight()
            int r5 = r5 - r0
        L_0x00a5:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x003b
        L_0x00a9:
            r4 = 0
            r7 = r4
            goto L_0x006b
        L_0x00ac:
            r4 = 0
            r6 = r4
            goto L_0x0073
        L_0x00af:
            if (r6 == 0) goto L_0x0077
            r1 = 1073741824(0x40000000, float:2.0)
            goto L_0x0077
        L_0x00b4:
            if (r6 == 0) goto L_0x00a5
            int r0 = r10.getMeasuredWidth()
            int r3 = r3 - r0
            goto L_0x00a5
        L_0x00bc:
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r0)
            r13.t = r0
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r0)
            r13.u = r0
            r0 = 1
            r13.v = r0
            r13.d()
            r0 = 0
            r13.v = r0
            int r2 = r13.getChildCount()
            r0 = 0
            r1 = r0
        L_0x00db:
            if (r1 >= r2) goto L_0x0109
            android.view.View r4 = r13.getChildAt(r1)
            int r0 = r4.getVisibility()
            r5 = 8
            if (r0 == r5) goto L_0x0105
            android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
            android.support.v4.view.ViewPager$LayoutParams r0 = (android.support.v4.view.ViewPager.LayoutParams) r0
            if (r0 == 0) goto L_0x00f5
            boolean r5 = r0.f73a
            if (r5 != 0) goto L_0x0105
        L_0x00f5:
            float r5 = (float) r3
            float r0 = r0.f75c
            float r0 = r0 * r5
            int r0 = (int) r0
            r5 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r5)
            int r5 = r13.u
            r4.measure(r0, r5)
        L_0x0105:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00db
        L_0x0109:
            return
        L_0x010a:
            r0 = r5
            goto L_0x0093
        L_0x010c:
            r2 = r3
            goto L_0x0085
        L_0x010f:
            r4 = r2
            r2 = r3
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            a(i2, i4, this.n, this.n);
        }
    }

    private void a(int i2, int i3, int i4, int i5) {
        if (i3 <= 0 || this.e.isEmpty()) {
            bv c2 = c(this.i);
            int min = (int) ((c2 != null ? Math.min(c2.e, this.s) : 0.0f) * ((float) ((i2 - getPaddingLeft()) - getPaddingRight())));
            if (min != getScrollX()) {
                a(false);
                scrollTo(min, getScrollY());
                return;
            }
            return;
        }
        int paddingLeft = (int) (((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4)) * (((float) getScrollX()) / ((float) (((i3 - getPaddingLeft()) - getPaddingRight()) + i5))));
        scrollTo(paddingLeft, getScrollY());
        if (!this.m.isFinished()) {
            this.m.startScroll(paddingLeft, 0, (int) (c(this.i).e * ((float) i2)), 0, this.m.getDuration() - this.m.timePassed());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, float, int, int):int
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void */
    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        bv a2;
        int i6;
        int i7;
        int i8;
        int measuredHeight;
        int i9;
        int i10;
        int childCount = getChildCount();
        int i11 = i4 - i2;
        int i12 = i5 - i3;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i13 = 0;
        int i14 = 0;
        while (i14 < childCount) {
            View childAt = getChildAt(i14);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.f73a) {
                    int i15 = layoutParams.f74b & 7;
                    int i16 = layoutParams.f74b & 112;
                    switch (i15) {
                        case 1:
                            i8 = Math.max((i11 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 2:
                        case 4:
                        default:
                            i8 = paddingLeft;
                            break;
                        case 3:
                            i8 = paddingLeft;
                            paddingLeft = childAt.getMeasuredWidth() + paddingLeft;
                            break;
                        case 5:
                            int measuredWidth = (i11 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            i8 = measuredWidth;
                            break;
                    }
                    switch (i16) {
                        case 16:
                            measuredHeight = Math.max((i12 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            int i17 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i17;
                            break;
                        case R.styleable.SherlockTheme_windowMinWidthMajor /*48*/:
                            int measuredHeight2 = childAt.getMeasuredHeight() + paddingTop;
                            int i18 = paddingTop;
                            i10 = paddingBottom;
                            i9 = measuredHeight2;
                            measuredHeight = i18;
                            break;
                        case 80:
                            measuredHeight = (i12 - paddingBottom) - childAt.getMeasuredHeight();
                            int measuredHeight3 = paddingBottom + childAt.getMeasuredHeight();
                            i9 = paddingTop;
                            i10 = measuredHeight3;
                            break;
                        default:
                            measuredHeight = paddingTop;
                            int i19 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i19;
                            break;
                    }
                    int i20 = i8 + scrollX;
                    childAt.layout(i20, measuredHeight, childAt.getMeasuredWidth() + i20, childAt.getMeasuredHeight() + measuredHeight);
                    i6 = i13 + 1;
                    i7 = i9;
                    paddingBottom = i10;
                    i14++;
                    paddingLeft = paddingLeft;
                    paddingRight = paddingRight;
                    paddingTop = i7;
                    i13 = i6;
                }
            }
            i6 = i13;
            i7 = paddingTop;
            i14++;
            paddingLeft = paddingLeft;
            paddingRight = paddingRight;
            paddingTop = i7;
            i13 = i6;
        }
        int i21 = (i11 - paddingLeft) - paddingRight;
        for (int i22 = 0; i22 < childCount; i22++) {
            View childAt2 = getChildAt(i22);
            if (childAt2.getVisibility() != 8) {
                LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                if (!layoutParams2.f73a && (a2 = a(childAt2)) != null) {
                    int i23 = ((int) (a2.e * ((float) i21))) + paddingLeft;
                    if (layoutParams2.d) {
                        layoutParams2.d = false;
                        childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (layoutParams2.f75c * ((float) i21)), 1073741824), View.MeasureSpec.makeMeasureSpec((i12 - paddingTop) - paddingBottom, 1073741824));
                    }
                    childAt2.layout(i23, paddingTop, childAt2.getMeasuredWidth() + i23, childAt2.getMeasuredHeight() + paddingTop);
                }
            }
        }
        this.p = paddingTop;
        this.q = i12 - paddingBottom;
        this.V = i13;
        if (this.S) {
            a(this.i, false, 0, false);
        }
        this.S = false;
    }

    public void computeScroll() {
        if (this.m.isFinished() || !this.m.computeScrollOffset()) {
            a(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.m.getCurrX();
        int currY = this.m.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!f(currX)) {
                this.m.abortAnimation();
                scrollTo(0, currY);
            }
        }
        at.b(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, float, int):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.bv, int, android.support.v4.view.bv):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void
      android.support.v4.view.ViewPager.a(int, float, int):void */
    private boolean f(int i2) {
        if (this.e.size() == 0) {
            this.U = false;
            a(0, 0.0f, 0);
            if (this.U) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
        bv m2 = m();
        int k2 = k();
        int i3 = this.n + k2;
        float f2 = ((float) this.n) / ((float) k2);
        int i4 = m2.f100b;
        float f3 = ((((float) i2) / ((float) k2)) - m2.e) / (m2.d + f2);
        this.U = false;
        a(i4, f3, (int) (((float) i3) * f3));
        if (this.U) {
            return true;
        }
        throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }

    /* access modifiers changed from: protected */
    public void a(int i2, float f2, int i3) {
        int measuredWidth;
        int i4;
        int i5;
        if (this.V > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            int i6 = 0;
            while (i6 < childCount) {
                View childAt = getChildAt(i6);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (!layoutParams.f73a) {
                    int i7 = paddingRight;
                    i4 = paddingLeft;
                    i5 = i7;
                } else {
                    switch (layoutParams.f74b & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            int i8 = paddingRight;
                            i4 = paddingLeft;
                            i5 = i8;
                            break;
                        case 2:
                        case 4:
                        default:
                            measuredWidth = paddingLeft;
                            int i9 = paddingRight;
                            i4 = paddingLeft;
                            i5 = i9;
                            break;
                        case 3:
                            int width2 = childAt.getWidth() + paddingLeft;
                            int i10 = paddingLeft;
                            i5 = paddingRight;
                            i4 = width2;
                            measuredWidth = i10;
                            break;
                        case 5:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            int measuredWidth2 = paddingRight + childAt.getMeasuredWidth();
                            i4 = paddingLeft;
                            i5 = measuredWidth2;
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                }
                i6++;
                int i11 = i5;
                paddingLeft = i4;
                paddingRight = i11;
            }
        }
        if (this.W != null) {
            this.W.a(i2, f2, i3);
        }
        if (this.Z != null) {
            this.Z.a(i2, f2, i3);
        }
        if (this.ab != null) {
            int scrollX2 = getScrollX();
            int childCount2 = getChildCount();
            for (int i12 = 0; i12 < childCount2; i12++) {
                View childAt2 = getChildAt(i12);
                if (!((LayoutParams) childAt2.getLayoutParams()).f73a) {
                    this.ab.a(childAt2, ((float) (childAt2.getLeft() - scrollX2)) / ((float) k()));
                }
            }
        }
        this.U = true;
    }

    private void a(boolean z2) {
        boolean z3 = this.ag == 2;
        if (z3) {
            c(false);
            this.m.abortAnimation();
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.m.getCurrX();
            int currY = this.m.getCurrY();
            if (!(scrollX == currX && scrollY == currY)) {
                scrollTo(currX, currY);
            }
        }
        this.x = false;
        boolean z4 = z3;
        for (int i2 = 0; i2 < this.e.size(); i2++) {
            bv bvVar = this.e.get(i2);
            if (bvVar.f101c) {
                bvVar.f101c = false;
                z4 = true;
            }
        }
        if (!z4) {
            return;
        }
        if (z2) {
            at.a(this, this.af);
        } else {
            this.af.run();
        }
    }

    private boolean a(float f2, float f3) {
        return (f2 < ((float) this.C) && f3 > 0.0f) || (f2 > ((float) (getWidth() - this.C)) && f3 < 0.0f);
    }

    private void b(boolean z2) {
        int i2;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            if (z2) {
                i2 = 2;
            } else {
                i2 = 0;
            }
            at.a(getChildAt(i3), i2, null);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            this.z = false;
            this.A = false;
            this.I = -1;
            if (this.J == null) {
                return false;
            }
            this.J.recycle();
            this.J = null;
            return false;
        }
        if (action != 0) {
            if (this.z) {
                return true;
            }
            if (this.A) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x2 = motionEvent.getX();
                this.G = x2;
                this.E = x2;
                float y2 = motionEvent.getY();
                this.H = y2;
                this.F = y2;
                this.I = z.b(motionEvent, 0);
                this.A = false;
                this.m.computeScrollOffset();
                if (this.ag == 2 && Math.abs(this.m.getFinalX() - this.m.getCurrX()) > this.N) {
                    this.m.abortAnimation();
                    this.x = false;
                    d();
                    this.z = true;
                    e(1);
                    break;
                } else {
                    a(false);
                    this.z = false;
                    break;
                }
                break;
            case 2:
                int i2 = this.I;
                if (i2 != -1) {
                    int a2 = z.a(motionEvent, i2);
                    float c2 = z.c(motionEvent, a2);
                    float f2 = c2 - this.E;
                    float abs = Math.abs(f2);
                    float d2 = z.d(motionEvent, a2);
                    float abs2 = Math.abs(d2 - this.H);
                    if (f2 == 0.0f || a(this.E, f2) || !a(this, false, (int) f2, (int) c2, (int) d2)) {
                        if (abs > ((float) this.D) && 0.5f * abs > abs2) {
                            this.z = true;
                            e(1);
                            this.E = f2 > 0.0f ? this.G + ((float) this.D) : this.G - ((float) this.D);
                            this.F = d2;
                            c(true);
                        } else if (abs2 > ((float) this.D)) {
                            this.A = true;
                        }
                        if (this.z && c(c2)) {
                            at.b(this);
                            break;
                        }
                    } else {
                        this.E = c2;
                        this.F = d2;
                        this.A = true;
                        return false;
                    }
                }
                break;
            case 6:
                a(motionEvent);
                break;
        }
        if (this.J == null) {
            this.J = VelocityTracker.obtain();
        }
        this.J.addMovement(motionEvent);
        return this.z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, float, int, int):int
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, float, int, int):int
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f2;
        boolean z2 = false;
        if (this.O) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (this.h == null || this.h.getCount() == 0) {
            return false;
        }
        if (this.J == null) {
            this.J = VelocityTracker.obtain();
        }
        this.J.addMovement(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.m.abortAnimation();
                this.x = false;
                d();
                this.z = true;
                e(1);
                float x2 = motionEvent.getX();
                this.G = x2;
                this.E = x2;
                float y2 = motionEvent.getY();
                this.H = y2;
                this.F = y2;
                this.I = z.b(motionEvent, 0);
                break;
            case 1:
                if (this.z) {
                    VelocityTracker velocityTracker = this.J;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.L);
                    int a2 = (int) ao.a(velocityTracker, this.I);
                    this.x = true;
                    int k2 = k();
                    int scrollX = getScrollX();
                    bv m2 = m();
                    a(a(m2.f100b, ((((float) scrollX) / ((float) k2)) - m2.e) / m2.d, a2, (int) (z.c(motionEvent, z.a(motionEvent, this.I)) - this.G)), true, true, a2);
                    this.I = -1;
                    n();
                    z2 = this.R.c() | this.Q.c();
                    break;
                }
                break;
            case 2:
                if (!this.z) {
                    int a3 = z.a(motionEvent, this.I);
                    float c2 = z.c(motionEvent, a3);
                    float abs = Math.abs(c2 - this.E);
                    float d2 = z.d(motionEvent, a3);
                    float abs2 = Math.abs(d2 - this.F);
                    if (abs > ((float) this.D) && abs > abs2) {
                        this.z = true;
                        if (c2 - this.G > 0.0f) {
                            f2 = this.G + ((float) this.D);
                        } else {
                            f2 = this.G - ((float) this.D);
                        }
                        this.E = f2;
                        this.F = d2;
                        e(1);
                        c(true);
                    }
                }
                if (this.z) {
                    z2 = false | c(z.c(motionEvent, z.a(motionEvent, this.I)));
                    break;
                }
                break;
            case 3:
                if (this.z) {
                    a(this.i, true, 0, false);
                    this.I = -1;
                    n();
                    z2 = this.R.c() | this.Q.c();
                    break;
                }
                break;
            case 5:
                int b2 = z.b(motionEvent);
                this.E = z.c(motionEvent, b2);
                this.I = z.b(motionEvent, b2);
                break;
            case 6:
                a(motionEvent);
                this.E = z.c(motionEvent, z.a(motionEvent, this.I));
                break;
        }
        if (z2) {
            at.b(this);
        }
        return true;
    }

    private boolean c(float f2) {
        boolean z2;
        float f3;
        boolean z3 = true;
        boolean z4 = false;
        this.E = f2;
        float scrollX = ((float) getScrollX()) + (this.E - f2);
        int k2 = k();
        float f4 = ((float) k2) * this.r;
        float f5 = ((float) k2) * this.s;
        bv bvVar = this.e.get(0);
        bv bvVar2 = this.e.get(this.e.size() - 1);
        if (bvVar.f100b != 0) {
            f4 = bvVar.e * ((float) k2);
            z2 = false;
        } else {
            z2 = true;
        }
        if (bvVar2.f100b != this.h.getCount() - 1) {
            f3 = bvVar2.e * ((float) k2);
            z3 = false;
        } else {
            f3 = f5;
        }
        if (scrollX < f4) {
            if (z2) {
                z4 = this.Q.a(Math.abs(f4 - scrollX) / ((float) k2));
            }
        } else if (scrollX > f3) {
            if (z3) {
                z4 = this.R.a(Math.abs(scrollX - f3) / ((float) k2));
            }
            f4 = f3;
        } else {
            f4 = scrollX;
        }
        this.E += f4 - ((float) ((int) f4));
        scrollTo((int) f4, getScrollY());
        f((int) f4);
        return z4;
    }

    private bv m() {
        float f2;
        int i2;
        bv bvVar;
        int k2 = k();
        float scrollX = k2 > 0 ? ((float) getScrollX()) / ((float) k2) : 0.0f;
        if (k2 > 0) {
            f2 = ((float) this.n) / ((float) k2);
        } else {
            f2 = 0.0f;
        }
        float f3 = 0.0f;
        float f4 = 0.0f;
        int i3 = -1;
        int i4 = 0;
        boolean z2 = true;
        bv bvVar2 = null;
        while (i4 < this.e.size()) {
            bv bvVar3 = this.e.get(i4);
            if (z2 || bvVar3.f100b == i3 + 1) {
                bv bvVar4 = bvVar3;
                i2 = i4;
                bvVar = bvVar4;
            } else {
                bv bvVar5 = this.f;
                bvVar5.e = f3 + f4 + f2;
                bvVar5.f100b = i3 + 1;
                bvVar5.d = this.h.getPageWidth(bvVar5.f100b);
                bv bvVar6 = bvVar5;
                i2 = i4 - 1;
                bvVar = bvVar6;
            }
            float f5 = bvVar.e;
            float f6 = bvVar.d + f5 + f2;
            if (!z2 && scrollX < f5) {
                return bvVar2;
            }
            if (scrollX < f6 || i2 == this.e.size() - 1) {
                return bvVar;
            }
            f4 = f5;
            i3 = bvVar.f100b;
            z2 = false;
            f3 = bvVar.d;
            bvVar2 = bvVar;
            i4 = i2 + 1;
        }
        return bvVar2;
    }

    private int a(int i2, float f2, int i3, int i4) {
        if (Math.abs(i4) <= this.M || Math.abs(i3) <= this.K) {
            i2 = (int) ((i2 >= this.i ? 0.4f : 0.6f) + ((float) i2) + f2);
        } else if (i3 <= 0) {
            i2++;
        }
        if (this.e.size() > 0) {
            return Math.max(this.e.get(0).f100b, Math.min(i2, this.e.get(this.e.size() - 1).f100b));
        }
        return i2;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z2 = false;
        int a2 = at.a(this);
        if (a2 == 0 || (a2 == 1 && this.h != null && this.h.getCount() > 1)) {
            if (!this.Q.a()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.r * ((float) width));
                this.Q.a(height, width);
                z2 = false | this.Q.a(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.R.a()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.s + 1.0f)) * ((float) width2));
                this.R.a(height2, width2);
                z2 |= this.R.a(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.Q.b();
            this.R.b();
        }
        if (z2) {
            at.b(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f2;
        super.onDraw(canvas);
        if (this.n > 0 && this.o != null && this.e.size() > 0 && this.h != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f3 = ((float) this.n) / ((float) width);
            bv bvVar = this.e.get(0);
            float f4 = bvVar.e;
            int size = this.e.size();
            int i2 = bvVar.f100b;
            int i3 = this.e.get(size - 1).f100b;
            int i4 = 0;
            int i5 = i2;
            while (i5 < i3) {
                while (i5 > bvVar.f100b && i4 < size) {
                    i4++;
                    bvVar = this.e.get(i4);
                }
                if (i5 == bvVar.f100b) {
                    f2 = (bvVar.e + bvVar.d) * ((float) width);
                    f4 = bvVar.e + bvVar.d + f3;
                } else {
                    float pageWidth = this.h.getPageWidth(i5);
                    f2 = (f4 + pageWidth) * ((float) width);
                    f4 += pageWidth + f3;
                }
                if (((float) this.n) + f2 > ((float) scrollX)) {
                    this.o.setBounds((int) f2, this.p, (int) (((float) this.n) + f2 + 0.5f), this.q);
                    this.o.draw(canvas);
                }
                if (f2 <= ((float) (scrollX + width))) {
                    i5++;
                } else {
                    return;
                }
            }
        }
    }

    public boolean e() {
        if (this.z) {
            return false;
        }
        this.O = true;
        e(1);
        this.E = 0.0f;
        this.G = 0.0f;
        if (this.J == null) {
            this.J = VelocityTracker.obtain();
        } else {
            this.J.clear();
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 0, 0.0f, 0.0f, 0);
        this.J.addMovement(obtain);
        obtain.recycle();
        this.P = uptimeMillis;
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, float, int, int):int
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void */
    public void f() {
        if (!this.O) {
            throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
        }
        VelocityTracker velocityTracker = this.J;
        velocityTracker.computeCurrentVelocity(1000, (float) this.L);
        int a2 = (int) ao.a(velocityTracker, this.I);
        this.x = true;
        int k2 = k();
        int scrollX = getScrollX();
        bv m2 = m();
        a(a(m2.f100b, ((((float) scrollX) / ((float) k2)) - m2.e) / m2.d, a2, (int) (this.E - this.G)), true, true, a2);
        n();
        this.O = false;
    }

    public void b(float f2) {
        float f3;
        float f4;
        if (!this.O) {
            throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
        }
        this.E += f2;
        float scrollX = ((float) getScrollX()) - f2;
        int k2 = k();
        float f5 = ((float) k2) * this.r;
        float f6 = ((float) k2) * this.s;
        bv bvVar = this.e.get(0);
        bv bvVar2 = this.e.get(this.e.size() - 1);
        if (bvVar.f100b != 0) {
            f3 = bvVar.e * ((float) k2);
        } else {
            f3 = f5;
        }
        if (bvVar2.f100b != this.h.getCount() - 1) {
            f4 = bvVar2.e * ((float) k2);
        } else {
            f4 = f6;
        }
        if (scrollX >= f3) {
            if (scrollX > f4) {
                f3 = f4;
            } else {
                f3 = scrollX;
            }
        }
        this.E += f3 - ((float) ((int) f3));
        scrollTo((int) f3, getScrollY());
        f((int) f3);
        MotionEvent obtain = MotionEvent.obtain(this.P, SystemClock.uptimeMillis(), 2, this.E, 0.0f, 0);
        this.J.addMovement(obtain);
        obtain.recycle();
    }

    public boolean g() {
        return this.O;
    }

    private void a(MotionEvent motionEvent) {
        int b2 = z.b(motionEvent);
        if (z.b(motionEvent, b2) == this.I) {
            int i2 = b2 == 0 ? 1 : 0;
            this.E = z.c(motionEvent, i2);
            this.I = z.b(motionEvent, i2);
            if (this.J != null) {
                this.J.clear();
            }
        }
    }

    private void n() {
        this.z = false;
        this.A = false;
        if (this.J != null) {
            this.J.recycle();
            this.J = null;
        }
    }

    private void c(boolean z2) {
        if (this.w != z2) {
            this.w = z2;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(View view, boolean z2, int i2, int i3, int i4) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i3 + scrollX >= childAt.getLeft() && i3 + scrollX < childAt.getRight() && i4 + scrollY >= childAt.getTop() && i4 + scrollY < childAt.getBottom()) {
                    if (a(childAt, true, i2, (i3 + scrollX) - childAt.getLeft(), (i4 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        if (!z2 || !at.a(view, -i2)) {
            return false;
        }
        return true;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || a(keyEvent);
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (keyEvent.getKeyCode()) {
            case R.styleable.SherlockTheme_windowContentOverlay /*21*/:
                return d(17);
            case R.styleable.SherlockTheme_textAppearanceLargePopupMenu /*22*/:
                return d(66);
            case R.styleable.SherlockTheme_windowActionModeOverlay /*61*/:
                if (Build.VERSION.SDK_INT < 11) {
                    return false;
                }
                if (s.a(keyEvent)) {
                    return d(2);
                }
                if (s.a(keyEvent, 1)) {
                    return d(1);
                }
                return false;
            default:
                return false;
        }
    }

    public boolean d(int i2) {
        View view;
        boolean z2;
        boolean z3;
        View findFocus = findFocus();
        if (findFocus == this) {
            view = null;
        } else {
            if (findFocus != null) {
                ViewParent parent = findFocus.getParent();
                while (true) {
                    if (!(parent instanceof ViewGroup)) {
                        z2 = false;
                        break;
                    } else if (parent == this) {
                        z2 = true;
                        break;
                    } else {
                        parent = parent.getParent();
                    }
                }
                if (!z2) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(findFocus.getClass().getSimpleName());
                    for (ViewParent parent2 = findFocus.getParent(); parent2 instanceof ViewGroup; parent2 = parent2.getParent()) {
                        sb.append(" => ").append(parent2.getClass().getSimpleName());
                    }
                    Log.e("ViewPager", "arrowScroll tried to find focus based on non-child current focused view " + sb.toString());
                    view = null;
                }
            }
            view = findFocus;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, view, i2);
        if (findNextFocus == null || findNextFocus == view) {
            if (i2 == 17 || i2 == 1) {
                z3 = h();
            } else {
                if (i2 == 66 || i2 == 2) {
                    z3 = i();
                }
                z3 = false;
            }
        } else if (i2 == 17) {
            z3 = (view == null || a(this.g, findNextFocus).left < a(this.g, view).left) ? findNextFocus.requestFocus() : h();
        } else {
            if (i2 == 66) {
                z3 = (view == null || a(this.g, findNextFocus).left > a(this.g, view).left) ? findNextFocus.requestFocus() : i();
            }
            z3 = false;
        }
        if (z3) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i2));
        }
        return z3;
    }

    private Rect a(Rect rect, View view) {
        Rect rect2;
        if (rect == null) {
            rect2 = new Rect();
        } else {
            rect2 = rect;
        }
        if (view == null) {
            rect2.set(0, 0, 0, 0);
            return rect2;
        }
        rect2.left = view.getLeft();
        rect2.right = view.getRight();
        rect2.top = view.getTop();
        rect2.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect2.left += viewGroup.getLeft();
            rect2.right += viewGroup.getRight();
            rect2.top += viewGroup.getTop();
            rect2.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager, int):void
      android.support.v4.view.ViewPager.a(float, float):boolean
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.bv
      android.support.v4.view.ViewPager.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean h() {
        if (this.i <= 0) {
            return false;
        }
        a(this.i - 1, true);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager, int):void
      android.support.v4.view.ViewPager.a(float, float):boolean
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.bv
      android.support.v4.view.ViewPager.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean i() {
        if (this.h == null || this.i >= this.h.getCount() - 1) {
            return false;
        }
        a(this.i + 1, true);
        return true;
    }

    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        bv a2;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                View childAt = getChildAt(i4);
                if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.f100b == this.i) {
                    childAt.addFocusables(arrayList, i2, i3);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i3 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList<View> arrayList) {
        bv a2;
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.f100b == this.i) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        int i3;
        bv a2;
        int i4 = -1;
        int childCount = getChildCount();
        if ((i2 & 2) != 0) {
            i4 = 1;
            i3 = 0;
        } else {
            i3 = childCount - 1;
            childCount = -1;
        }
        while (i3 != childCount) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.f100b == this.i && childAt.requestFocus(i2, rect)) {
                return true;
            }
            i3 += i4;
        }
        return false;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        bv a2;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.f100b == this.i && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    public class LayoutParams extends ViewGroup.LayoutParams {

        /* renamed from: a  reason: collision with root package name */
        public boolean f73a;

        /* renamed from: b  reason: collision with root package name */
        public int f74b;

        /* renamed from: c  reason: collision with root package name */
        float f75c = 0.0f;
        boolean d;
        int e;
        int f;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, ViewPager.f70a);
            this.f74b = obtainStyledAttributes.getInteger(0, 48);
            obtainStyledAttributes.recycle();
        }
    }
}
