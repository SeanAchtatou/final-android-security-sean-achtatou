package android.support.v4.view;

import android.support.v4.view.ViewPager;
import android.view.View;
import java.util.Comparator;

/* compiled from: ViewPager */
class cb implements Comparator<View> {
    cb() {
    }

    /* renamed from: a */
    public int compare(View view, View view2) {
        ViewPager.LayoutParams layoutParams = (ViewPager.LayoutParams) view.getLayoutParams();
        ViewPager.LayoutParams layoutParams2 = (ViewPager.LayoutParams) view2.getLayoutParams();
        if (layoutParams.f73a != layoutParams2.f73a) {
            return layoutParams.f73a ? 1 : -1;
        }
        return layoutParams.e - layoutParams2.e;
    }
}
