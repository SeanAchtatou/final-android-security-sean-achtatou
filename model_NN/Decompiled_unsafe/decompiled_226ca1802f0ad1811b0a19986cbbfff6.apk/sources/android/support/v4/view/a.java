package android.support.v4.view;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.a.h;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: AccessibilityDelegateCompat */
public class a {

    /* renamed from: b  reason: collision with root package name */
    private static final d f79b;

    /* renamed from: c  reason: collision with root package name */
    private static final Object f80c = f79b.a();

    /* renamed from: a  reason: collision with root package name */
    final Object f81a = f79b.a(this);

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f79b = new e();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f79b = new b();
        } else {
            f79b = new g();
        }
    }

    /* access modifiers changed from: package-private */
    public Object a() {
        return this.f81a;
    }

    public void a(View view, int i) {
        f79b.a(f80c, view, i);
    }

    public void a(View view, AccessibilityEvent accessibilityEvent) {
        f79b.d(f80c, view, accessibilityEvent);
    }

    public boolean b(View view, AccessibilityEvent accessibilityEvent) {
        return f79b.a(f80c, view, accessibilityEvent);
    }

    public void c(View view, AccessibilityEvent accessibilityEvent) {
        f79b.c(f80c, view, accessibilityEvent);
    }

    public void d(View view, AccessibilityEvent accessibilityEvent) {
        f79b.b(f80c, view, accessibilityEvent);
    }

    public void a(View view, android.support.v4.view.a.a aVar) {
        f79b.a(f80c, view, aVar);
    }

    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return f79b.a(f80c, viewGroup, view, accessibilityEvent);
    }

    public h a(View view) {
        return f79b.a(f80c, view);
    }

    public boolean a(View view, int i, Bundle bundle) {
        return f79b.a(f80c, view, i, bundle);
    }
}
