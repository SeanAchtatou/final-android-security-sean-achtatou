package android.support.v4.view.a;

import android.graphics.Rect;
import android.view.View;

/* compiled from: AccessibilityNodeInfoCompat */
class b extends e {
    b() {
    }

    public Object a(Object obj) {
        return f.a(obj);
    }

    public void a(Object obj, int i) {
        f.a(obj, i);
    }

    public void a(Object obj, View view) {
        f.a(obj, view);
    }

    public int b(Object obj) {
        return f.b(obj);
    }

    public void a(Object obj, Rect rect) {
        f.a(obj, rect);
    }

    public void b(Object obj, Rect rect) {
        f.b(obj, rect);
    }

    public CharSequence c(Object obj) {
        return f.c(obj);
    }

    public CharSequence d(Object obj) {
        return f.d(obj);
    }

    public CharSequence e(Object obj) {
        return f.e(obj);
    }

    public boolean f(Object obj) {
        return f.f(obj);
    }

    public boolean g(Object obj) {
        return f.g(obj);
    }

    public boolean h(Object obj) {
        return f.h(obj);
    }

    public boolean i(Object obj) {
        return f.i(obj);
    }

    public boolean j(Object obj) {
        return f.j(obj);
    }

    public boolean k(Object obj) {
        return f.k(obj);
    }

    public void c(Object obj, Rect rect) {
        f.c(obj, rect);
    }

    public void d(Object obj, Rect rect) {
        f.d(obj, rect);
    }

    public void a(Object obj, CharSequence charSequence) {
        f.a(obj, charSequence);
    }

    public void a(Object obj, boolean z) {
        f.a(obj, z);
    }

    public void b(Object obj, CharSequence charSequence) {
        f.b(obj, charSequence);
    }

    public void b(Object obj, boolean z) {
        f.b(obj, z);
    }

    public void c(Object obj, boolean z) {
        f.c(obj, z);
    }

    public void d(Object obj, boolean z) {
        f.d(obj, z);
    }

    public void e(Object obj, boolean z) {
        f.e(obj, z);
    }

    public void c(Object obj, CharSequence charSequence) {
        f.c(obj, charSequence);
    }

    public void b(Object obj, View view) {
        f.b(obj, view);
    }

    public void f(Object obj, boolean z) {
        f.f(obj, z);
    }

    public void g(Object obj, boolean z) {
        f.g(obj, z);
    }

    public void c(Object obj, View view) {
        f.c(obj, view);
    }

    public void l(Object obj) {
        f.l(obj);
    }
}
