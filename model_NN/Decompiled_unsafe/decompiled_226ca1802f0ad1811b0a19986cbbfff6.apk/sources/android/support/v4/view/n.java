package android.support.v4.view;

import android.os.Build;

/* compiled from: GravityCompat */
public class n {

    /* renamed from: a  reason: collision with root package name */
    static final o f109a;

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            f109a = new q();
        } else {
            f109a = new p();
        }
    }

    public static int a(int i, int i2) {
        return f109a.a(i, i2);
    }
}
