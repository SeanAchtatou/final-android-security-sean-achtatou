package android.support.v4.view.a;

import android.graphics.Rect;
import android.view.View;

/* compiled from: AccessibilityNodeInfoCompat */
interface c {
    Object a(Object obj);

    void a(Object obj, int i);

    void a(Object obj, Rect rect);

    void a(Object obj, View view);

    void a(Object obj, CharSequence charSequence);

    void a(Object obj, boolean z);

    int b(Object obj);

    void b(Object obj, Rect rect);

    void b(Object obj, View view);

    void b(Object obj, CharSequence charSequence);

    void b(Object obj, boolean z);

    CharSequence c(Object obj);

    void c(Object obj, Rect rect);

    void c(Object obj, View view);

    void c(Object obj, CharSequence charSequence);

    void c(Object obj, boolean z);

    CharSequence d(Object obj);

    void d(Object obj, Rect rect);

    void d(Object obj, boolean z);

    CharSequence e(Object obj);

    void e(Object obj, boolean z);

    void f(Object obj, boolean z);

    boolean f(Object obj);

    void g(Object obj, boolean z);

    boolean g(Object obj);

    void h(Object obj, boolean z);

    boolean h(Object obj);

    void i(Object obj, boolean z);

    boolean i(Object obj);

    boolean j(Object obj);

    boolean k(Object obj);

    void l(Object obj);

    boolean m(Object obj);

    boolean n(Object obj);
}
