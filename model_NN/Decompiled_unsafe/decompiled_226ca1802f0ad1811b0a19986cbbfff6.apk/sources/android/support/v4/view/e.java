package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.h;
import android.view.View;

/* compiled from: AccessibilityDelegateCompat */
class e extends b {
    e() {
    }

    public Object a(a aVar) {
        return k.a(new f(this, aVar));
    }

    public h a(Object obj, View view) {
        Object a2 = k.a(obj, view);
        if (a2 != null) {
            return new h(a2);
        }
        return null;
    }

    public boolean a(Object obj, View view, int i, Bundle bundle) {
        return k.a(obj, view, i, bundle);
    }
}
