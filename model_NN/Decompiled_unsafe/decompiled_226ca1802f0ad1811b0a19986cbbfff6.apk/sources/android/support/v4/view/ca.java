package android.support.v4.view;

import android.os.Parcel;
import android.support.v4.b.c;
import android.support.v4.view.ViewPager;

/* compiled from: ViewPager */
final class ca implements c<ViewPager.SavedState> {
    ca() {
    }

    /* renamed from: b */
    public ViewPager.SavedState a(Parcel parcel, ClassLoader classLoader) {
        return new ViewPager.SavedState(parcel, classLoader);
    }

    /* renamed from: b */
    public ViewPager.SavedState[] a(int i) {
        return new ViewPager.SavedState[i];
    }
}
