package android.support.v4.view.a;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AccessibilityNodeProviderCompat */
class k implements o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f86a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ j f87b;

    k(j jVar, h hVar) {
        this.f87b = jVar;
        this.f86a = hVar;
    }

    public boolean a(int i, int i2, Bundle bundle) {
        return this.f86a.a(i, i2, bundle);
    }

    public List<Object> a(String str, int i) {
        List<a> a2 = this.f86a.a(str, i);
        ArrayList arrayList = new ArrayList();
        int size = a2.size();
        for (int i2 = 0; i2 < size; i2++) {
            arrayList.add(a2.get(i2).a());
        }
        return arrayList;
    }

    public Object a(int i) {
        a a2 = this.f86a.a(i);
        if (a2 == null) {
            return null;
        }
        return a2.a();
    }
}
