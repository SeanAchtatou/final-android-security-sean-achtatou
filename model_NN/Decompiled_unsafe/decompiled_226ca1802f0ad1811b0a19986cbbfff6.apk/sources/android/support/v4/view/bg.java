package android.support.v4.view;

import android.os.Build;
import android.view.ViewConfiguration;

/* compiled from: ViewConfigurationCompat */
public class bg {

    /* renamed from: a  reason: collision with root package name */
    static final bj f96a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f96a = new bi();
        } else {
            f96a = new bh();
        }
    }

    public static int a(ViewConfiguration viewConfiguration) {
        return f96a.a(viewConfiguration);
    }
}
