package android.support.v4.view.a;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;

/* compiled from: AccessibilityNodeInfoCompat */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final c f82a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f83b;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f82a = new d();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f82a = new b();
        } else {
            f82a = new e();
        }
    }

    static a a(Object obj) {
        if (obj != null) {
            return new a(obj);
        }
        return null;
    }

    public a(Object obj) {
        this.f83b = obj;
    }

    public Object a() {
        return this.f83b;
    }

    public static a a(a aVar) {
        return a(f82a.a(aVar.f83b));
    }

    public void a(View view) {
        f82a.c(this.f83b, view);
    }

    public void b(View view) {
        f82a.a(this.f83b, view);
    }

    public int b() {
        return f82a.b(this.f83b);
    }

    public void a(int i) {
        f82a.a(this.f83b, i);
    }

    public void c(View view) {
        f82a.b(this.f83b, view);
    }

    public void a(Rect rect) {
        f82a.a(this.f83b, rect);
    }

    public void b(Rect rect) {
        f82a.c(this.f83b, rect);
    }

    public void c(Rect rect) {
        f82a.b(this.f83b, rect);
    }

    public void d(Rect rect) {
        f82a.d(this.f83b, rect);
    }

    public boolean c() {
        return f82a.h(this.f83b);
    }

    public void a(boolean z) {
        f82a.c(this.f83b, z);
    }

    public boolean d() {
        return f82a.i(this.f83b);
    }

    public void b(boolean z) {
        f82a.d(this.f83b, z);
    }

    public boolean e() {
        return f82a.m(this.f83b);
    }

    public void c(boolean z) {
        f82a.h(this.f83b, z);
    }

    public boolean f() {
        return f82a.n(this.f83b);
    }

    public void d(boolean z) {
        f82a.i(this.f83b, z);
    }

    public boolean g() {
        return f82a.k(this.f83b);
    }

    public void e(boolean z) {
        f82a.g(this.f83b, z);
    }

    public boolean h() {
        return f82a.f(this.f83b);
    }

    public void f(boolean z) {
        f82a.a(this.f83b, z);
    }

    public boolean i() {
        return f82a.j(this.f83b);
    }

    public void g(boolean z) {
        f82a.e(this.f83b, z);
    }

    public boolean j() {
        return f82a.g(this.f83b);
    }

    public void h(boolean z) {
        f82a.b(this.f83b, z);
    }

    public void i(boolean z) {
        f82a.f(this.f83b, z);
    }

    public CharSequence k() {
        return f82a.e(this.f83b);
    }

    public void a(CharSequence charSequence) {
        f82a.c(this.f83b, charSequence);
    }

    public CharSequence l() {
        return f82a.c(this.f83b);
    }

    public void b(CharSequence charSequence) {
        f82a.a(this.f83b, charSequence);
    }

    public CharSequence m() {
        return f82a.d(this.f83b);
    }

    public void c(CharSequence charSequence) {
        f82a.b(this.f83b, charSequence);
    }

    public void n() {
        f82a.l(this.f83b);
    }

    public int hashCode() {
        if (this.f83b == null) {
            return 0;
        }
        return this.f83b.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        if (this.f83b == null) {
            if (aVar.f83b != null) {
                return false;
            }
            return true;
        } else if (!this.f83b.equals(aVar.f83b)) {
            return false;
        } else {
            return true;
        }
    }
}
