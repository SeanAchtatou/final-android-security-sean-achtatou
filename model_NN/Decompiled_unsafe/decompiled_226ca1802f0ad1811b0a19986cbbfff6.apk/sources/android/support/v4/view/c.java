package android.support.v4.view;

import android.support.v4.view.a.a;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: AccessibilityDelegateCompat */
class c implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f103a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f104b;

    c(b bVar, a aVar) {
        this.f104b = bVar;
        this.f103a = aVar;
    }

    public boolean a(View view, AccessibilityEvent accessibilityEvent) {
        return this.f103a.b(view, accessibilityEvent);
    }

    public void b(View view, AccessibilityEvent accessibilityEvent) {
        this.f103a.d(view, accessibilityEvent);
    }

    public void a(View view, Object obj) {
        this.f103a.a(view, new a(obj));
    }

    public void c(View view, AccessibilityEvent accessibilityEvent) {
        this.f103a.c(view, accessibilityEvent);
    }

    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.f103a.a(viewGroup, view, accessibilityEvent);
    }

    public void a(View view, int i) {
        this.f103a.a(view, i);
    }

    public void d(View view, AccessibilityEvent accessibilityEvent) {
        this.f103a.a(view, accessibilityEvent);
    }
}
