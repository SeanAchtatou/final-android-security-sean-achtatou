package android.support.v4.view;

import android.view.View;
import android.view.ViewParent;

/* compiled from: ViewCompat */
class ay extends ax {
    ay() {
    }

    public void b(View view) {
        be.a(view);
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        be.a(view, i, i2, i3, i4);
    }

    public void a(View view, Runnable runnable) {
        be.a(view, runnable);
    }

    public int c(View view) {
        return be.b(view);
    }

    public void b(View view, int i) {
        be.a(view, i);
    }

    public ViewParent f(View view) {
        return be.c(view);
    }
}
