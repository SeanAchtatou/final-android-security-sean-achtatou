package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.h;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: AccessibilityDelegateCompat */
class f implements m {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f105a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ e f106b;

    f(e eVar, a aVar) {
        this.f106b = eVar;
        this.f105a = aVar;
    }

    public boolean a(View view, AccessibilityEvent accessibilityEvent) {
        return this.f105a.b(view, accessibilityEvent);
    }

    public void b(View view, AccessibilityEvent accessibilityEvent) {
        this.f105a.d(view, accessibilityEvent);
    }

    public void a(View view, Object obj) {
        this.f105a.a(view, new a(obj));
    }

    public void c(View view, AccessibilityEvent accessibilityEvent) {
        this.f105a.c(view, accessibilityEvent);
    }

    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.f105a.a(viewGroup, view, accessibilityEvent);
    }

    public void a(View view, int i) {
        this.f105a.a(view, i);
    }

    public void d(View view, AccessibilityEvent accessibilityEvent) {
        this.f105a.a(view, accessibilityEvent);
    }

    public Object a(View view) {
        h a2 = this.f105a.a(view);
        if (a2 != null) {
            return a2.a();
        }
        return null;
    }

    public boolean a(View view, int i, Bundle bundle) {
        return this.f105a.a(view, i, bundle);
    }
}
