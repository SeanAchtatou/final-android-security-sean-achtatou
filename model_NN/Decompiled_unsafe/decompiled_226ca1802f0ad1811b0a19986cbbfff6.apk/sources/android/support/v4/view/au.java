package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewParent;

/* compiled from: ViewCompat */
class au implements ba {
    au() {
    }

    public boolean a(View view, int i) {
        return false;
    }

    public int a(View view) {
        return 2;
    }

    public void a(View view, a aVar) {
    }

    public void b(View view) {
        view.postInvalidateDelayed(a());
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        view.postInvalidateDelayed(a(), i, i2, i3, i4);
    }

    public void a(View view, Runnable runnable) {
        view.postDelayed(runnable, a());
    }

    /* access modifiers changed from: package-private */
    public long a() {
        return 10;
    }

    public int c(View view) {
        return 0;
    }

    public void b(View view, int i) {
    }

    public void a(View view, int i, Paint paint) {
    }

    public int d(View view) {
        return 0;
    }

    public void a(View view, Paint paint) {
    }

    public int e(View view) {
        return 0;
    }

    public ViewParent f(View view) {
        return view.getParent();
    }
}
