package android.support.v4.view.a;

import android.os.Build;
import android.os.Bundle;
import java.util.List;

/* compiled from: AccessibilityNodeProviderCompat */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private static final i f84a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f85b;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f84a = new j();
        } else {
            f84a = new l();
        }
    }

    public h() {
        this.f85b = f84a.a(this);
    }

    public h(Object obj) {
        this.f85b = obj;
    }

    public Object a() {
        return this.f85b;
    }

    public a a(int i) {
        return null;
    }

    public boolean a(int i, int i2, Bundle bundle) {
        return false;
    }

    public List<a> a(String str, int i) {
        return null;
    }
}
