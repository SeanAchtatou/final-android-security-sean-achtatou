package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

/* compiled from: ViewCompatJellybeanMr1 */
class bf {
    public static void a(View view, Paint paint) {
        view.setLayerPaint(paint);
    }

    public static int a(View view) {
        return view.getLayoutDirection();
    }
}
