package android.support.v4.view;

import android.os.Build;
import android.view.VelocityTracker;

/* compiled from: VelocityTrackerCompat */
public class ao {

    /* renamed from: a  reason: collision with root package name */
    static final ar f94a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f94a = new aq();
        } else {
            f94a = new ap();
        }
    }

    public static float a(VelocityTracker velocityTracker, int i) {
        return f94a.a(velocityTracker, i);
    }

    public static float b(VelocityTracker velocityTracker, int i) {
        return f94a.b(velocityTracker, i);
    }
}
