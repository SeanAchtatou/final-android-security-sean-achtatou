package android.support.v4.view;

import android.graphics.Paint;
import android.os.Build;
import android.view.View;
import android.view.ViewParent;

/* compiled from: ViewCompat */
public class at {

    /* renamed from: a  reason: collision with root package name */
    static final ba f95a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            f95a = new az();
        } else if (i >= 16) {
            f95a = new ay();
        } else if (i >= 14) {
            f95a = new ax();
        } else if (i >= 11) {
            f95a = new aw();
        } else if (i >= 9) {
            f95a = new av();
        } else {
            f95a = new au();
        }
    }

    public static boolean a(View view, int i) {
        return f95a.a(view, i);
    }

    public static int a(View view) {
        return f95a.a(view);
    }

    public static void a(View view, a aVar) {
        f95a.a(view, aVar);
    }

    public static void b(View view) {
        f95a.b(view);
    }

    public static void a(View view, int i, int i2, int i3, int i4) {
        f95a.a(view, i, i2, i3, i4);
    }

    public static void a(View view, Runnable runnable) {
        f95a.a(view, runnable);
    }

    public static int c(View view) {
        return f95a.c(view);
    }

    public static void b(View view, int i) {
        f95a.b(view, i);
    }

    public static void a(View view, int i, Paint paint) {
        f95a.a(view, i, paint);
    }

    public static int d(View view) {
        return f95a.d(view);
    }

    public static void a(View view, Paint paint) {
        f95a.a(view, paint);
    }

    public static int e(View view) {
        return f95a.e(view);
    }

    public static ViewParent f(View view) {
        return f95a.f(view);
    }
}
