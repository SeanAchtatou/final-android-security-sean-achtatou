package android.support.v4.view;

import android.database.DataSetObserver;

/* compiled from: PagerTitleStrip */
class ai extends DataSetObserver implements bx, by {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PagerTitleStrip f91a;

    /* renamed from: b  reason: collision with root package name */
    private int f92b;

    private ai(PagerTitleStrip pagerTitleStrip) {
        this.f91a = pagerTitleStrip;
    }

    public void a(int i, float f, int i2) {
        if (f > 0.5f) {
            i++;
        }
        this.f91a.a(i, f, false);
    }

    public void a(int i) {
        float f = 0.0f;
        if (this.f92b == 0) {
            this.f91a.a(this.f91a.f67a.c(), this.f91a.f67a.b());
            if (this.f91a.g >= 0.0f) {
                f = this.f91a.g;
            }
            this.f91a.a(this.f91a.f67a.c(), f, true);
        }
    }

    public void b(int i) {
        this.f92b = i;
    }

    public void onChanged() {
        float f = 0.0f;
        this.f91a.a(this.f91a.f67a.c(), this.f91a.f67a.b());
        if (this.f91a.g >= 0.0f) {
            f = this.f91a.g;
        }
        this.f91a.a(this.f91a.f67a.c(), f, true);
    }
}
