package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

/* compiled from: ViewCompat */
class aw extends av {
    aw() {
    }

    /* access modifiers changed from: package-private */
    public long a() {
        return bc.a();
    }

    public void a(View view, int i, Paint paint) {
        bc.a(view, i, paint);
    }

    public int d(View view) {
        return bc.a(view);
    }

    public void a(View view, Paint paint) {
        a(view, d(view), paint);
        view.invalidate();
    }
}
