package android.support.v4.view;

import android.os.Build;
import android.view.ViewGroup;

/* compiled from: ViewGroupCompat */
public class bl {

    /* renamed from: a  reason: collision with root package name */
    static final bo f97a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 14) {
            f97a = new bn();
        } else if (i >= 11) {
            f97a = new bm();
        } else {
            f97a = new bp();
        }
    }

    public static void a(ViewGroup viewGroup, boolean z) {
        f97a.a(viewGroup, z);
    }
}
