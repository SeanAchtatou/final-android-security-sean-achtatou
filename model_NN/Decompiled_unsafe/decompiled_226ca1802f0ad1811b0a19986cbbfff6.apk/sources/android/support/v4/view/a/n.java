package android.support.v4.view.a;

import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.List;

/* compiled from: AccessibilityNodeProviderCompatJellyBean */
final class n extends AccessibilityNodeProvider {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f88a;

    n(o oVar) {
        this.f88a = oVar;
    }

    public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
        return (AccessibilityNodeInfo) this.f88a.a(i);
    }

    public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
        return this.f88a.a(str, i);
    }

    public boolean performAction(int i, int i2, Bundle bundle) {
        return this.f88a.a(i, i2, bundle);
    }
}
