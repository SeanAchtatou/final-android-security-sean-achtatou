package android.support.v4.view;

import android.os.Build;
import android.view.KeyEvent;

/* compiled from: KeyEventCompat */
public class s {

    /* renamed from: a  reason: collision with root package name */
    static final w f110a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f110a = new v();
        } else {
            f110a = new t();
        }
    }

    public static boolean a(KeyEvent keyEvent, int i) {
        return f110a.a(keyEvent.getMetaState(), i);
    }

    public static boolean a(KeyEvent keyEvent) {
        return f110a.b(keyEvent.getMetaState());
    }

    public static void b(KeyEvent keyEvent) {
        f110a.a(keyEvent);
    }
}
