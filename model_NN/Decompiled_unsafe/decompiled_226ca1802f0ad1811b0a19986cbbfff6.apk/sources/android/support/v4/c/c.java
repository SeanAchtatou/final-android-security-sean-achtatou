package android.support.v4.c;

/* compiled from: SparseArrayCompat */
public class c<E> {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f27a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private boolean f28b;

    /* renamed from: c  reason: collision with root package name */
    private int[] f29c;
    private Object[] d;
    private int e;

    public c() {
        this(10);
    }

    public c(int i) {
        this.f28b = false;
        int i2 = i(i);
        this.f29c = new int[i2];
        this.d = new Object[i2];
        this.e = 0;
    }

    public E a(int i) {
        return a(i, null);
    }

    public E a(int i, E e2) {
        int a2 = a(this.f29c, 0, this.e, i);
        return (a2 < 0 || this.d[a2] == f27a) ? e2 : this.d[a2];
    }

    public void b(int i) {
        int a2 = a(this.f29c, 0, this.e, i);
        if (a2 >= 0 && this.d[a2] != f27a) {
            this.d[a2] = f27a;
            this.f28b = true;
        }
    }

    public void c(int i) {
        b(i);
    }

    public void d(int i) {
        if (this.d[i] != f27a) {
            this.d[i] = f27a;
            this.f28b = true;
        }
    }

    private void c() {
        int i = this.e;
        int[] iArr = this.f29c;
        Object[] objArr = this.d;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != f27a) {
                if (i3 != i2) {
                    iArr[i2] = iArr[i3];
                    objArr[i2] = obj;
                }
                i2++;
            }
        }
        this.f28b = false;
        this.e = i2;
    }

    public void b(int i, E e2) {
        int a2 = a(this.f29c, 0, this.e, i);
        if (a2 >= 0) {
            this.d[a2] = e2;
            return;
        }
        int i2 = a2 ^ -1;
        if (i2 >= this.e || this.d[i2] != f27a) {
            if (this.f28b && this.e >= this.f29c.length) {
                c();
                i2 = a(this.f29c, 0, this.e, i) ^ -1;
            }
            if (this.e >= this.f29c.length) {
                int i3 = i(this.e + 1);
                int[] iArr = new int[i3];
                Object[] objArr = new Object[i3];
                System.arraycopy(this.f29c, 0, iArr, 0, this.f29c.length);
                System.arraycopy(this.d, 0, objArr, 0, this.d.length);
                this.f29c = iArr;
                this.d = objArr;
            }
            if (this.e - i2 != 0) {
                System.arraycopy(this.f29c, i2, this.f29c, i2 + 1, this.e - i2);
                System.arraycopy(this.d, i2, this.d, i2 + 1, this.e - i2);
            }
            this.f29c[i2] = i;
            this.d[i2] = e2;
            this.e++;
            return;
        }
        this.f29c[i2] = i;
        this.d[i2] = e2;
    }

    public int a() {
        if (this.f28b) {
            c();
        }
        return this.e;
    }

    public int e(int i) {
        if (this.f28b) {
            c();
        }
        return this.f29c[i];
    }

    public E f(int i) {
        if (this.f28b) {
            c();
        }
        return this.d[i];
    }

    public int g(int i) {
        if (this.f28b) {
            c();
        }
        return a(this.f29c, 0, this.e, i);
    }

    public void b() {
        int i = this.e;
        Object[] objArr = this.d;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.e = 0;
        this.f28b = false;
    }

    private static int a(int[] iArr, int i, int i2, int i3) {
        int i4 = i - 1;
        int i5 = i + i2;
        while (i5 - i4 > 1) {
            int i6 = (i5 + i4) / 2;
            if (iArr[i6] < i3) {
                i4 = i6;
            } else {
                i5 = i6;
            }
        }
        if (i5 == i + i2) {
            return (i + i2) ^ -1;
        }
        return iArr[i5] != i3 ? i5 ^ -1 : i5;
    }

    static int h(int i) {
        for (int i2 = 4; i2 < 32; i2++) {
            if (i <= (1 << i2) - 12) {
                return (1 << i2) - 12;
            }
        }
        return i;
    }

    static int i(int i) {
        return h(i * 4) / 4;
    }
}
