package android.support.v4.c;

import android.support.v4.app.NotificationCompat;
import android.util.Log;
import java.io.Writer;

/* compiled from: LogWriter */
public class b extends Writer {

    /* renamed from: a  reason: collision with root package name */
    private final String f25a;

    /* renamed from: b  reason: collision with root package name */
    private StringBuilder f26b = new StringBuilder((int) NotificationCompat.FLAG_HIGH_PRIORITY);

    public b(String str) {
        this.f25a = str;
    }

    public void close() {
        a();
    }

    public void flush() {
        a();
    }

    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c2 = cArr[i + i3];
            if (c2 == 10) {
                a();
            } else {
                this.f26b.append(c2);
            }
        }
    }

    private void a() {
        if (this.f26b.length() > 0) {
            Log.d(this.f25a, this.f26b.toString());
            this.f26b.delete(0, this.f26b.length());
        }
    }
}
