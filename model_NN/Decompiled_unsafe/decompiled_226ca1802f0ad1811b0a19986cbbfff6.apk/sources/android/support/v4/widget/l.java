package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;

/* compiled from: EdgeEffectCompat */
public class l {

    /* renamed from: b  reason: collision with root package name */
    private static final o f144b;

    /* renamed from: a  reason: collision with root package name */
    private Object f145a;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            f144b = new n();
        } else {
            f144b = new m();
        }
    }

    public l(Context context) {
        this.f145a = f144b.a(context);
    }

    public void a(int i, int i2) {
        f144b.a(this.f145a, i, i2);
    }

    public boolean a() {
        return f144b.a(this.f145a);
    }

    public void b() {
        f144b.b(this.f145a);
    }

    public boolean a(float f) {
        return f144b.a(this.f145a, f);
    }

    public boolean c() {
        return f144b.c(this.f145a);
    }

    public boolean a(Canvas canvas) {
        return f144b.a(this.f145a, canvas);
    }
}
