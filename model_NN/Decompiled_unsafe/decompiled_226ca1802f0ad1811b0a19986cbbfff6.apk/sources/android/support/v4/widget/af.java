package android.support.v4.widget;

import android.util.Log;
import android.view.View;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* compiled from: SlidingPaneLayout */
class af extends ae {

    /* renamed from: a  reason: collision with root package name */
    private Method f129a;

    /* renamed from: b  reason: collision with root package name */
    private Field f130b;

    af() {
        try {
            this.f129a = View.class.getDeclaredMethod("getDisplayList", null);
        } catch (NoSuchMethodException e) {
            Log.e("SlidingPaneLayout", "Couldn't fetch getDisplayList method; dimming won't work right.", e);
        }
        try {
            this.f130b = View.class.getDeclaredField("mRecreateDisplayList");
            this.f130b.setAccessible(true);
        } catch (NoSuchFieldException e2) {
            Log.e("SlidingPaneLayout", "Couldn't fetch mRecreateDisplayList field; dimming will be slow.", e2);
        }
    }

    public void a(SlidingPaneLayout slidingPaneLayout, View view) {
        if (this.f129a == null || this.f130b == null) {
            view.invalidate();
            return;
        }
        try {
            this.f130b.setBoolean(view, true);
            this.f129a.invoke(view, null);
        } catch (Exception e) {
            Log.e("SlidingPaneLayout", "Error refreshing display list state", e);
        }
        super.a(slidingPaneLayout, view);
    }
}
