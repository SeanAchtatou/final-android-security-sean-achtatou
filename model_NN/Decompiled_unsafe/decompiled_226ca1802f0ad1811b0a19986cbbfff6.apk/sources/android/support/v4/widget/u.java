package android.support.v4.widget;

import android.content.Context;
import android.view.animation.Interpolator;

/* compiled from: ScrollerCompat */
class u implements s {
    u() {
    }

    public Object a(Context context, Interpolator interpolator) {
        return w.a(context, interpolator);
    }

    public boolean a(Object obj) {
        return w.a(obj);
    }

    public int b(Object obj) {
        return w.b(obj);
    }

    public int c(Object obj) {
        return w.c(obj);
    }

    public boolean d(Object obj) {
        return w.d(obj);
    }

    public void a(Object obj, int i, int i2, int i3, int i4, int i5) {
        w.a(obj, i, i2, i3, i4, i5);
    }

    public void e(Object obj) {
        w.e(obj);
    }

    public int f(Object obj) {
        return w.f(obj);
    }

    public int g(Object obj) {
        return w.g(obj);
    }
}
