package android.support.v4.widget;

import android.support.v4.widget.DrawerLayout;
import android.view.View;

/* compiled from: DrawerLayout */
class j extends ak {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DrawerLayout f140a;

    /* renamed from: b  reason: collision with root package name */
    private final int f141b;

    /* renamed from: c  reason: collision with root package name */
    private ah f142c;
    private final Runnable d = new k(this);

    public j(DrawerLayout drawerLayout, int i) {
        this.f140a = drawerLayout;
        this.f141b = i;
    }

    public void a(ah ahVar) {
        this.f142c = ahVar;
    }

    public void a() {
        this.f140a.removeCallbacks(this.d);
    }

    public boolean a(View view, int i) {
        return this.f140a.g(view) && this.f140a.a(view, this.f141b) && this.f140a.a(view) == 0;
    }

    public void a(int i) {
        this.f140a.a(this.f141b, i, this.f142c.c());
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        float width;
        int width2 = view.getWidth();
        if (this.f140a.a(view, 3)) {
            width = ((float) (width2 + i)) / ((float) width2);
        } else {
            width = ((float) (this.f140a.getWidth() - i)) / ((float) width2);
        }
        this.f140a.b(view, width);
        view.setVisibility(width == 0.0f ? 4 : 0);
        this.f140a.invalidate();
    }

    public void b(View view, int i) {
        ((DrawerLayout.LayoutParams) view.getLayoutParams()).f117c = false;
        b();
    }

    private void b() {
        int i = 3;
        if (this.f141b == 3) {
            i = 5;
        }
        View a2 = this.f140a.a(i);
        if (a2 != null) {
            this.f140a.i(a2);
        }
    }

    public void a(View view, float f, float f2) {
        int width;
        float d2 = this.f140a.d(view);
        int width2 = view.getWidth();
        if (this.f140a.a(view, 3)) {
            width = (f > 0.0f || (f == 0.0f && d2 > 0.5f)) ? 0 : -width2;
        } else {
            width = this.f140a.getWidth();
            if (f < 0.0f || (f == 0.0f && d2 < 0.5f)) {
                width -= width2;
            }
        }
        this.f142c.a(width, view.getTop());
        this.f140a.invalidate();
    }

    public void a(int i, int i2) {
        this.f140a.postDelayed(this.d, 160);
    }

    /* access modifiers changed from: private */
    public void c() {
        View view;
        int i;
        int i2 = 0;
        int b2 = this.f142c.b();
        boolean z = this.f141b == 3;
        if (z) {
            View a2 = this.f140a.a(3);
            if (a2 != null) {
                i2 = -a2.getWidth();
            }
            int i3 = i2 + b2;
            view = a2;
            i = i3;
        } else {
            View a3 = this.f140a.a(5);
            int width = this.f140a.getWidth() - b2;
            view = a3;
            i = width;
        }
        if (view == null) {
            return;
        }
        if (((z && view.getLeft() < i) || (!z && view.getLeft() > i)) && this.f140a.a(view) == 0) {
            this.f142c.a(view, i, view.getTop());
            ((DrawerLayout.LayoutParams) view.getLayoutParams()).f117c = true;
            this.f140a.invalidate();
            b();
            this.f140a.c();
        }
    }

    public boolean b(int i) {
        return false;
    }

    public void b(int i, int i2) {
        View a2;
        if ((i & 1) == 1) {
            a2 = this.f140a.a(3);
        } else {
            a2 = this.f140a.a(5);
        }
        if (a2 != null && this.f140a.a(a2) == 0) {
            this.f142c.a(a2, i2);
        }
    }

    public int a(View view) {
        return view.getWidth();
    }

    public int a(View view, int i, int i2) {
        if (this.f140a.a(view, 3)) {
            return Math.max(-view.getWidth(), Math.min(i, 0));
        }
        int width = this.f140a.getWidth();
        return Math.max(width - view.getWidth(), Math.min(i, width));
    }

    public int b(View view, int i, int i2) {
        return view.getTop();
    }
}
