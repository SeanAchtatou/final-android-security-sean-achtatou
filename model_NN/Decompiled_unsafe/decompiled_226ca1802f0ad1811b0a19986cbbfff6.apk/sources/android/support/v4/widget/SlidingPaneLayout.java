package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.at;
import android.support.v4.view.z;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import java.util.ArrayList;

public class SlidingPaneLayout extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    static final ad f121a;

    /* renamed from: b  reason: collision with root package name */
    private int f122b;

    /* renamed from: c  reason: collision with root package name */
    private int f123c;
    private Drawable d;
    private final int e;
    private boolean f;
    /* access modifiers changed from: private */
    public View g;
    /* access modifiers changed from: private */
    public float h;
    private float i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public boolean k;
    private int l;
    private float m;
    private float n;
    private ab o;
    /* access modifiers changed from: private */
    public final ah p;
    /* access modifiers changed from: private */
    public boolean q;
    private boolean r;
    private final Rect s;
    /* access modifiers changed from: private */
    public final ArrayList<z> t;

    static {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 17) {
            f121a = new ag();
        } else if (i2 >= 16) {
            f121a = new af();
        } else {
            f121a = new ae();
        }
    }

    public SlidingPaneLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SlidingPaneLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f122b = -858993460;
        this.r = true;
        this.s = new Rect();
        this.t = new ArrayList<>();
        float f2 = context.getResources().getDisplayMetrics().density;
        this.e = (int) ((32.0f * f2) + 0.5f);
        ViewConfiguration.get(context);
        setWillNotDraw(false);
        at.a(this, new y(this));
        at.b(this, 1);
        this.p = ah.a(this, 0.5f, new aa(this));
        this.p.a(1);
        this.p.a(f2 * 400.0f);
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        if (this.o != null) {
            this.o.a(view, this.h);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(View view) {
        if (this.o != null) {
            this.o.a(view);
        }
        sendAccessibilityEvent(32);
    }

    /* access modifiers changed from: package-private */
    public void c(View view) {
        if (this.o != null) {
            this.o.b(view);
        }
        sendAccessibilityEvent(32);
    }

    /* access modifiers changed from: package-private */
    public void d(View view) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int paddingLeft = getPaddingLeft();
        int width = getWidth() - getPaddingRight();
        int paddingTop = getPaddingTop();
        int height = getHeight() - getPaddingBottom();
        if (view == null || !f(view)) {
            i2 = 0;
            i3 = 0;
            i4 = 0;
            i5 = 0;
        } else {
            i5 = view.getLeft();
            i4 = view.getRight();
            i3 = view.getTop();
            i2 = view.getBottom();
        }
        int childCount = getChildCount();
        int i7 = 0;
        while (i7 < childCount) {
            View childAt = getChildAt(i7);
            if (childAt != view) {
                int max = Math.max(paddingLeft, childAt.getLeft());
                int max2 = Math.max(paddingTop, childAt.getTop());
                int min = Math.min(width, childAt.getRight());
                int min2 = Math.min(height, childAt.getBottom());
                if (max < i5 || max2 < i3 || min > i4 || min2 > i2) {
                    i6 = 0;
                } else {
                    i6 = 4;
                }
                childAt.setVisibility(i6);
                i7++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 4) {
                childAt.setVisibility(0);
            }
        }
    }

    private static boolean f(View view) {
        Drawable background = view.getBackground();
        if (background == null || background.getOpacity() != -1) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.r = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.r = true;
        int size = this.t.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.t.get(i2).run();
        }
        this.t.clear();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int paddingTop;
        int makeMeasureSpec;
        int makeMeasureSpec2;
        int makeMeasureSpec3;
        int makeMeasureSpec4;
        int i5;
        int i6;
        boolean z;
        float f2;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode != 1073741824) {
            throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
        } else if (mode2 == 0) {
            throw new IllegalStateException("Height must not be UNSPECIFIED");
        } else {
            switch (mode2) {
                case Integer.MIN_VALUE:
                    i4 = 0;
                    paddingTop = (size2 - getPaddingTop()) - getPaddingBottom();
                    break;
                case 1073741824:
                    i4 = (size2 - getPaddingTop()) - getPaddingBottom();
                    paddingTop = i4;
                    break;
                default:
                    i4 = 0;
                    paddingTop = -1;
                    break;
            }
            boolean z2 = false;
            int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
            int childCount = getChildCount();
            if (childCount > 2) {
                Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
            }
            this.g = null;
            int i7 = 0;
            int i8 = i4;
            float f3 = 0.0f;
            while (i7 < childCount) {
                View childAt = getChildAt(i7);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (childAt.getVisibility() == 8) {
                    layoutParams.f126c = false;
                    i5 = paddingLeft;
                    f2 = f3;
                    i6 = i8;
                    z = z2;
                } else {
                    if (layoutParams.f124a > 0.0f) {
                        f3 += layoutParams.f124a;
                        if (layoutParams.width == 0) {
                            i5 = paddingLeft;
                            f2 = f3;
                            i6 = i8;
                            z = z2;
                        }
                    }
                    int i9 = layoutParams.leftMargin + layoutParams.rightMargin;
                    if (layoutParams.width == -2) {
                        makeMeasureSpec3 = View.MeasureSpec.makeMeasureSpec(size - i9, Integer.MIN_VALUE);
                    } else if (layoutParams.width == -1) {
                        makeMeasureSpec3 = View.MeasureSpec.makeMeasureSpec(size - i9, 1073741824);
                    } else {
                        makeMeasureSpec3 = View.MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824);
                    }
                    if (layoutParams.height == -2) {
                        makeMeasureSpec4 = View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE);
                    } else if (layoutParams.height == -1) {
                        makeMeasureSpec4 = View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824);
                    } else {
                        makeMeasureSpec4 = View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824);
                    }
                    childAt.measure(makeMeasureSpec3, makeMeasureSpec4);
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (mode2 == Integer.MIN_VALUE && measuredHeight > i8) {
                        i8 = Math.min(measuredHeight, paddingTop);
                    }
                    int i10 = paddingLeft - measuredWidth;
                    boolean z3 = i10 < 0;
                    layoutParams.f125b = z3;
                    boolean z4 = z3 | z2;
                    if (layoutParams.f125b) {
                        this.g = childAt;
                    }
                    i5 = i10;
                    i6 = i8;
                    float f4 = f3;
                    z = z4;
                    f2 = f4;
                }
                i7++;
                z2 = z;
                i8 = i6;
                f3 = f2;
                paddingLeft = i5;
            }
            if (z2 || f3 > 0.0f) {
                int i11 = size - this.e;
                for (int i12 = 0; i12 < childCount; i12++) {
                    View childAt2 = getChildAt(i12);
                    if (childAt2.getVisibility() != 8) {
                        LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                        boolean z5 = layoutParams2.width == 0 && layoutParams2.f124a > 0.0f;
                        int measuredWidth2 = z5 ? 0 : childAt2.getMeasuredWidth();
                        if (!z2 || childAt2 == this.g) {
                            if (layoutParams2.f124a > 0.0f) {
                                if (layoutParams2.width != 0) {
                                    makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824);
                                } else if (layoutParams2.height == -2) {
                                    makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE);
                                } else if (layoutParams2.height == -1) {
                                    makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824);
                                } else {
                                    makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(layoutParams2.height, 1073741824);
                                }
                                if (z2) {
                                    int i13 = size - (layoutParams2.rightMargin + layoutParams2.leftMargin);
                                    int makeMeasureSpec5 = View.MeasureSpec.makeMeasureSpec(i13, 1073741824);
                                    if (measuredWidth2 != i13) {
                                        childAt2.measure(makeMeasureSpec5, makeMeasureSpec);
                                    }
                                } else {
                                    childAt2.measure(View.MeasureSpec.makeMeasureSpec(((int) ((layoutParams2.f124a * ((float) Math.max(0, paddingLeft))) / f3)) + measuredWidth2, 1073741824), makeMeasureSpec);
                                }
                            }
                        } else if (layoutParams2.width < 0 && (measuredWidth2 > i11 || layoutParams2.f124a > 0.0f)) {
                            if (!z5) {
                                makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824);
                            } else if (layoutParams2.height == -2) {
                                makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE);
                            } else if (layoutParams2.height == -1) {
                                makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824);
                            } else {
                                makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(layoutParams2.height, 1073741824);
                            }
                            childAt2.measure(View.MeasureSpec.makeMeasureSpec(i11, 1073741824), makeMeasureSpec2);
                        }
                    }
                }
            }
            setMeasuredDimension(size, i8);
            this.f = z2;
            if (this.p.a() != 0 && !z2) {
                this.p.f();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8 = i4 - i2;
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int childCount = getChildCount();
        if (this.r) {
            this.h = (!this.f || !this.q) ? 0.0f : 1.0f;
        }
        int i9 = 0;
        int i10 = paddingLeft;
        while (i9 < childCount) {
            View childAt = getChildAt(i9);
            if (childAt.getVisibility() == 8) {
                i7 = i10;
            } else {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                if (layoutParams.f125b) {
                    int min = (Math.min(paddingLeft, (i8 - paddingRight) - this.e) - i10) - (layoutParams.leftMargin + layoutParams.rightMargin);
                    this.j = min;
                    layoutParams.f126c = ((layoutParams.leftMargin + i10) + min) + (measuredWidth / 2) > i8 - paddingRight;
                    i7 = layoutParams.leftMargin + ((int) (((float) min) * this.h)) + i10;
                    i6 = 0;
                } else if (!this.f || this.l == 0) {
                    i6 = 0;
                    i7 = paddingLeft;
                } else {
                    i6 = (int) ((1.0f - this.h) * ((float) this.l));
                    i7 = paddingLeft;
                }
                int i11 = i7 - i6;
                childAt.layout(i11, paddingTop, i11 + measuredWidth, childAt.getMeasuredHeight() + paddingTop);
                paddingLeft += childAt.getWidth();
            }
            i9++;
            i10 = i7;
        }
        if (this.r) {
            if (this.f) {
                if (this.l != 0) {
                    a(this.h);
                }
                if (((LayoutParams) this.g.getLayoutParams()).f126c) {
                    a(this.g, this.h, this.f122b);
                }
            } else {
                for (int i12 = 0; i12 < childCount; i12++) {
                    a(getChildAt(i12), 0.0f, this.f122b);
                }
            }
            d(this.g);
        }
        this.r = false;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            this.r = true;
        }
    }

    public void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        if (!isInTouchMode() && !this.f) {
            this.q = view == this.g;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View childAt;
        int a2 = z.a(motionEvent);
        if (!this.f && a2 == 0 && getChildCount() > 1 && (childAt = getChildAt(1)) != null) {
            this.q = !this.p.b(childAt, (int) motionEvent.getX(), (int) motionEvent.getY());
        }
        if (!this.f || (this.k && a2 != 0)) {
            this.p.e();
            return super.onInterceptTouchEvent(motionEvent);
        } else if (a2 == 3 || a2 == 1) {
            this.p.e();
            return false;
        } else {
            switch (a2) {
                case 0:
                    this.k = false;
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    this.m = x;
                    this.n = y;
                    if (this.p.b(this.g, (int) x, (int) y) && e(this.g)) {
                        z = true;
                        break;
                    }
                    z = false;
                    break;
                case 1:
                default:
                    z = false;
                    break;
                case 2:
                    float x2 = motionEvent.getX();
                    float y2 = motionEvent.getY();
                    float abs = Math.abs(x2 - this.m);
                    float abs2 = Math.abs(y2 - this.n);
                    if (abs > ((float) this.p.d()) && abs2 > abs) {
                        this.p.e();
                        this.k = true;
                        return false;
                    }
                    z = false;
                    break;
            }
            if (this.p.a(motionEvent) || z) {
                return true;
            }
            return false;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.f) {
            return super.onTouchEvent(motionEvent);
        }
        this.p.b(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.m = x;
                this.n = y;
                return true;
            case 1:
                if (!e(this.g)) {
                    return true;
                }
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                float f2 = x2 - this.m;
                float f3 = y2 - this.n;
                int d2 = this.p.d();
                if ((f2 * f2) + (f3 * f3) >= ((float) (d2 * d2)) || !this.p.b(this.g, (int) x2, (int) y2)) {
                    return true;
                }
                a(this.g, 0);
                return true;
            default:
                return true;
        }
    }

    private boolean a(View view, int i2) {
        if (!this.r && !a(0.0f, i2)) {
            return false;
        }
        this.q = false;
        return true;
    }

    private boolean b(View view, int i2) {
        if (!this.r && !a(1.0f, i2)) {
            return false;
        }
        this.q = true;
        return true;
    }

    public boolean b() {
        return b(this.g, 0);
    }

    public boolean c() {
        return a(this.g, 0);
    }

    public boolean d() {
        return !this.f || this.h == 1.0f;
    }

    public boolean e() {
        return this.f;
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        LayoutParams layoutParams = (LayoutParams) this.g.getLayoutParams();
        this.h = ((float) (i2 - (getPaddingLeft() + layoutParams.leftMargin))) / ((float) this.j);
        if (this.l != 0) {
            a(this.h);
        }
        if (layoutParams.f126c) {
            a(this.g, this.h, this.f122b);
        }
        a(this.g);
    }

    private void a(View view, float f2, int i2) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (f2 > 0.0f && i2 != 0) {
            int i3 = (((int) (((float) ((-16777216 & i2) >>> 24)) * f2)) << 24) | (16777215 & i2);
            if (layoutParams.d == null) {
                layoutParams.d = new Paint();
            }
            layoutParams.d.setColorFilter(new PorterDuffColorFilter(i3, PorterDuff.Mode.SRC_OVER));
            if (at.d(view) != 2) {
                at.a(view, 2, layoutParams.d);
            }
            g(view);
        } else if (at.d(view) != 0) {
            if (layoutParams.d != null) {
                layoutParams.d.setColorFilter(null);
            }
            z zVar = new z(this, view);
            this.t.add(zVar);
            at.a(this, zVar);
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        boolean drawChild;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int save = canvas.save(2);
        if (this.f && !layoutParams.f125b && this.g != null) {
            canvas.getClipBounds(this.s);
            this.s.right = Math.min(this.s.right, this.g.getLeft());
            canvas.clipRect(this.s);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            drawChild = super.drawChild(canvas, view, j2);
        } else if (!layoutParams.f126c || this.h <= 0.0f) {
            if (view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(false);
            }
            drawChild = super.drawChild(canvas, view, j2);
        } else {
            if (!view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(true);
            }
            Bitmap drawingCache = view.getDrawingCache();
            if (drawingCache != null) {
                canvas.drawBitmap(drawingCache, (float) view.getLeft(), (float) view.getTop(), layoutParams.d);
                drawChild = false;
            } else {
                Log.e("SlidingPaneLayout", "drawChild: child view " + view + " returned null drawing cache");
                drawChild = super.drawChild(canvas, view, j2);
            }
        }
        canvas.restoreToCount(save);
        return drawChild;
    }

    /* access modifiers changed from: private */
    public void g(View view) {
        f121a.a(this, view);
    }

    /* access modifiers changed from: package-private */
    public boolean a(float f2, int i2) {
        if (!this.f) {
            return false;
        }
        int paddingLeft = getPaddingLeft();
        if (!this.p.a(this.g, (int) (((float) (((LayoutParams) this.g.getLayoutParams()).leftMargin + paddingLeft)) + (((float) this.j) * f2)), this.g.getTop())) {
            return false;
        }
        a();
        at.b(this);
        return true;
    }

    public void computeScroll() {
        if (!this.p.a(true)) {
            return;
        }
        if (!this.f) {
            this.p.f();
        } else {
            at.b(this);
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        View childAt = getChildCount() > 1 ? getChildAt(1) : null;
        if (childAt != null && this.d != null) {
            int intrinsicWidth = this.d.getIntrinsicWidth();
            int left = childAt.getLeft();
            this.d.setBounds(left - intrinsicWidth, childAt.getTop(), left, childAt.getBottom());
            this.d.draw(canvas);
        }
    }

    private void a(float f2) {
        boolean z;
        LayoutParams layoutParams = (LayoutParams) this.g.getLayoutParams();
        if (!layoutParams.f126c || layoutParams.leftMargin > 0) {
            z = false;
        } else {
            z = true;
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt != this.g) {
                this.i = f2;
                childAt.offsetLeftAndRight(((int) ((1.0f - this.i) * ((float) this.l))) - ((int) ((1.0f - f2) * ((float) this.l))));
                if (z) {
                    a(childAt, 1.0f - this.i, this.f123c);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean e(View view) {
        if (view == null) {
            return false;
        }
        return this.f && ((LayoutParams) view.getLayoutParams()).f126c && this.h > 0.0f;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f127a = e() ? d() : this.q;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.f127a) {
            b();
        } else {
            c();
        }
        this.q = savedState.f127a;
    }

    public class LayoutParams extends ViewGroup.MarginLayoutParams {
        private static final int[] e = {16843137};

        /* renamed from: a  reason: collision with root package name */
        public float f124a = 0.0f;

        /* renamed from: b  reason: collision with root package name */
        boolean f125b;

        /* renamed from: c  reason: collision with root package name */
        boolean f126c;
        Paint d;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, e);
            this.f124a = obtainStyledAttributes.getFloat(0, 0.0f);
            obtainStyledAttributes.recycle();
        }
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new ac();

        /* renamed from: a  reason: collision with root package name */
        boolean f127a;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f127a = parcel.readInt() != 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f127a ? 1 : 0);
        }
    }
}
