package android.support.v4.widget;

import android.content.Context;
import android.os.Build;
import android.view.animation.Interpolator;

/* compiled from: ScrollerCompat */
public class r {

    /* renamed from: b  reason: collision with root package name */
    static final s f146b;

    /* renamed from: a  reason: collision with root package name */
    Object f147a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 14) {
            f146b = new v();
        } else if (i >= 9) {
            f146b = new u();
        } else {
            f146b = new t();
        }
    }

    public static r a(Context context, Interpolator interpolator) {
        return new r(context, interpolator);
    }

    r(Context context, Interpolator interpolator) {
        this.f147a = f146b.a(context, interpolator);
    }

    public boolean a() {
        return f146b.a(this.f147a);
    }

    public int b() {
        return f146b.b(this.f147a);
    }

    public int c() {
        return f146b.c(this.f147a);
    }

    public int d() {
        return f146b.f(this.f147a);
    }

    public int e() {
        return f146b.g(this.f147a);
    }

    public boolean f() {
        return f146b.d(this.f147a);
    }

    public void a(int i, int i2, int i3, int i4, int i5) {
        f146b.a(this.f147a, i, i2, i3, i4, i5);
    }

    public void g() {
        f146b.e(this.f147a);
    }
}
