package android.support.v4.widget;

import android.database.Cursor;
import android.widget.Filter;

/* compiled from: CursorFilter */
class e extends Filter {

    /* renamed from: a  reason: collision with root package name */
    f f137a;

    e(f fVar) {
        this.f137a = fVar;
    }

    public CharSequence convertResultToString(Object obj) {
        return this.f137a.convertToString((Cursor) obj);
    }

    /* access modifiers changed from: protected */
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        Cursor runQueryOnBackgroundThread = this.f137a.runQueryOnBackgroundThread(charSequence);
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (runQueryOnBackgroundThread != null) {
            filterResults.count = runQueryOnBackgroundThread.getCount();
            filterResults.values = runQueryOnBackgroundThread;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    /* access modifiers changed from: protected */
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        Cursor cursor = this.f137a.getCursor();
        if (filterResults.values != null && filterResults.values != cursor) {
            this.f137a.changeCursor((Cursor) filterResults.values);
        }
    }
}
