package android.support.v4.widget;

import android.support.v4.view.at;
import android.view.View;

/* compiled from: SlidingPaneLayout */
class z implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final View f150a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SlidingPaneLayout f151b;

    z(SlidingPaneLayout slidingPaneLayout, View view) {
        this.f151b = slidingPaneLayout;
        this.f150a = view;
    }

    public void run() {
        if (this.f150a.getParent() == this.f151b) {
            at.a(this.f150a, 0, null);
            this.f151b.g(this.f150a);
        }
        this.f151b.t.remove(this);
    }
}
