package android.support.v4.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.widget.SlidingPaneLayout;

/* compiled from: SlidingPaneLayout */
final class ac implements Parcelable.Creator<SlidingPaneLayout.SavedState> {
    ac() {
    }

    /* renamed from: a */
    public SlidingPaneLayout.SavedState createFromParcel(Parcel parcel) {
        return new SlidingPaneLayout.SavedState(parcel);
    }

    /* renamed from: a */
    public SlidingPaneLayout.SavedState[] newArray(int i) {
        return new SlidingPaneLayout.SavedState[i];
    }
}
