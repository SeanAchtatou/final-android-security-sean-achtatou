package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.view.at;
import android.support.v4.view.bl;
import android.support.v4.view.n;
import android.support.v4.view.s;
import android.support.v4.view.z;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class DrawerLayout extends ViewGroup {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final int[] f112a = {16842931};

    /* renamed from: b  reason: collision with root package name */
    private int f113b;

    /* renamed from: c  reason: collision with root package name */
    private int f114c;
    private float d;
    private Paint e;
    private final ah f;
    private final ah g;
    private final j h;
    private final j i;
    private int j;
    private boolean k;
    private boolean l;
    private int m;
    private int n;
    private boolean o;
    private boolean p;
    private h q;
    private float r;
    private float s;
    private Drawable t;
    private Drawable u;

    public DrawerLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DrawerLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f114c = -1728053248;
        this.e = new Paint();
        this.l = true;
        float f2 = getResources().getDisplayMetrics().density;
        this.f113b = (int) ((64.0f * f2) + 0.5f);
        float f3 = f2 * 400.0f;
        this.h = new j(this, 3);
        this.i = new j(this, 5);
        this.f = ah.a(this, 0.5f, this.h);
        this.f.a(1);
        this.f.a(f3);
        this.h.a(this.f);
        this.g = ah.a(this, 0.5f, this.i);
        this.g.a(2);
        this.g.a(f3);
        this.i.a(this.g);
        setFocusableInTouchMode(true);
        at.a(this, new g(this));
        bl.a(this, false);
    }

    public void a(int i2, int i3) {
        int a2 = n.a(i3, at.e(this));
        if (a2 == 3) {
            this.m = i2;
        } else if (a2 == 5) {
            this.n = i2;
        }
        if (i2 != 0) {
            (a2 == 3 ? this.f : this.g).e();
        }
        switch (i2) {
            case 1:
                View a3 = a(a2);
                if (a3 != null) {
                    i(a3);
                    return;
                }
                return;
            case 2:
                View a4 = a(a2);
                if (a4 != null) {
                    h(a4);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public int a(View view) {
        int e2 = e(view);
        if (e2 == 3) {
            return this.m;
        }
        if (e2 == 5) {
            return this.n;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, View view) {
        int i4 = 1;
        int a2 = this.f.a();
        int a3 = this.g.a();
        if (!(a2 == 1 || a3 == 1)) {
            i4 = (a2 == 2 || a3 == 2) ? 2 : 0;
        }
        if (view != null && i3 == 0) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (layoutParams.f116b == 0.0f) {
                b(view);
            } else if (layoutParams.f116b == 1.0f) {
                c(view);
            }
        }
        if (i4 != this.j) {
            this.j = i4;
            if (this.q != null) {
                this.q.onDrawerStateChanged(i4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (layoutParams.d) {
            layoutParams.d = false;
            if (this.q != null) {
                this.q.onDrawerClosed(view);
            }
            sendAccessibilityEvent(32);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (!layoutParams.d) {
            layoutParams.d = true;
            if (this.q != null) {
                this.q.onDrawerOpened(view);
            }
            view.sendAccessibilityEvent(32);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view, float f2) {
        if (this.q != null) {
            this.q.onDrawerSlide(view, f2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(View view, float f2) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (f2 != layoutParams.f116b) {
            layoutParams.f116b = f2;
            a(view, f2);
        }
    }

    /* access modifiers changed from: package-private */
    public float d(View view) {
        return ((LayoutParams) view.getLayoutParams()).f116b;
    }

    /* access modifiers changed from: package-private */
    public int e(View view) {
        return n.a(((LayoutParams) view.getLayoutParams()).f115a, at.e(view));
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view, int i2) {
        return (e(view) & i2) == i2;
    }

    /* access modifiers changed from: package-private */
    public View a() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (((LayoutParams) childAt.getLayoutParams()).d) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public View a(int i2) {
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if ((e(childAt) & 7) == (i2 & 7)) {
                return childAt;
            }
        }
        return null;
    }

    static String b(int i2) {
        if ((i2 & 3) == 3) {
            return "LEFT";
        }
        if ((i2 & 5) == 5) {
            return "RIGHT";
        }
        return Integer.toHexString(i2);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.l = true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.l = true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode == 1073741824 && mode2 == 1073741824) {
            setMeasuredDimension(size, size2);
            int childCount = getChildCount();
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = getChildAt(i4);
                if (childAt.getVisibility() != 8) {
                    LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                    if (f(childAt)) {
                        childAt.measure(View.MeasureSpec.makeMeasureSpec((size - layoutParams.leftMargin) - layoutParams.rightMargin, 1073741824), View.MeasureSpec.makeMeasureSpec((size2 - layoutParams.topMargin) - layoutParams.bottomMargin, 1073741824));
                    } else if (g(childAt)) {
                        int e2 = e(childAt) & 7;
                        if ((0 & e2) != 0) {
                            throw new IllegalStateException("Child drawer has absolute gravity " + b(e2) + " but this " + "DrawerLayout" + " already has a " + "drawer view along that edge");
                        }
                        childAt.measure(getChildMeasureSpec(i2, this.f113b + layoutParams.leftMargin + layoutParams.rightMargin, layoutParams.width), getChildMeasureSpec(i3, layoutParams.topMargin + layoutParams.bottomMargin, layoutParams.height));
                    } else {
                        throw new IllegalStateException("Child " + childAt + " at index " + i4 + " does not have a valid layout_gravity - must be Gravity.LEFT, " + "Gravity.RIGHT or Gravity.NO_GRAVITY");
                    }
                }
            }
            return;
        }
        throw new IllegalArgumentException("DrawerLayout must be measured with MeasureSpec.EXACTLY.");
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        this.k = true;
        int childCount = getChildCount();
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (f(childAt)) {
                    childAt.layout(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.leftMargin + childAt.getMeasuredWidth(), layoutParams.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (a(childAt, 3)) {
                        i6 = (-measuredWidth) + ((int) (((float) measuredWidth) * layoutParams.f116b));
                    } else {
                        i6 = (i4 - i2) - ((int) (((float) measuredWidth) * layoutParams.f116b));
                    }
                    switch (layoutParams.f115a & 112) {
                        case 16:
                            int i8 = i5 - i3;
                            int i9 = (i8 - measuredHeight) / 2;
                            if (i9 < layoutParams.topMargin) {
                                i9 = layoutParams.topMargin;
                            } else if (i9 + measuredHeight > i8 - layoutParams.bottomMargin) {
                                i9 = (i8 - layoutParams.bottomMargin) - measuredHeight;
                            }
                            childAt.layout(i6, i9, measuredWidth + i6, measuredHeight + i9);
                            break;
                        case 80:
                            int i10 = i5 - i3;
                            childAt.layout(i6, (i10 - layoutParams.bottomMargin) - childAt.getMeasuredHeight(), measuredWidth + i6, i10 - layoutParams.bottomMargin);
                            break;
                        default:
                            childAt.layout(i6, layoutParams.topMargin, measuredWidth + i6, measuredHeight);
                            break;
                    }
                    if (layoutParams.f116b == 0.0f) {
                        childAt.setVisibility(4);
                    }
                }
            }
        }
        this.k = false;
        this.l = false;
    }

    public void requestLayout() {
        if (!this.k) {
            super.requestLayout();
        }
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float f2 = 0.0f;
        for (int i2 = 0; i2 < childCount; i2++) {
            f2 = Math.max(f2, ((LayoutParams) getChildAt(i2).getLayoutParams()).f116b);
        }
        this.d = f2;
        if (this.f.a(true) || this.g.a(true)) {
            at.b(this);
        }
    }

    private static boolean l(View view) {
        Drawable background = view.getBackground();
        if (background == null || background.getOpacity() != -1) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        int i2;
        int height = getHeight();
        boolean f2 = f(view);
        int i3 = 0;
        int width = getWidth();
        int save = canvas.save();
        if (f2) {
            int childCount = getChildCount();
            int i4 = 0;
            while (i4 < childCount) {
                View childAt = getChildAt(i4);
                if (childAt != view && childAt.getVisibility() == 0 && l(childAt) && g(childAt)) {
                    if (childAt.getHeight() < height) {
                        i2 = width;
                    } else if (a(childAt, 3)) {
                        int right = childAt.getRight();
                        if (right <= i3) {
                            right = i3;
                        }
                        i3 = right;
                        i2 = width;
                    } else {
                        i2 = childAt.getLeft();
                        if (i2 < width) {
                        }
                    }
                    i4++;
                    width = i2;
                }
                i2 = width;
                i4++;
                width = i2;
            }
            canvas.clipRect(i3, 0, width, getHeight());
        }
        int i5 = width;
        boolean drawChild = super.drawChild(canvas, view, j2);
        canvas.restoreToCount(save);
        if (this.d > 0.0f && f2) {
            this.e.setColor((((int) (((float) ((this.f114c & -16777216) >>> 24)) * this.d)) << 24) | (this.f114c & 16777215));
            canvas.drawRect((float) i3, 0.0f, (float) i5, (float) getHeight(), this.e);
        } else if (this.t != null && a(view, 3)) {
            int intrinsicWidth = this.t.getIntrinsicWidth();
            int right2 = view.getRight();
            float max = Math.max(0.0f, Math.min(((float) right2) / ((float) this.f.b()), 1.0f));
            this.t.setBounds(right2, view.getTop(), intrinsicWidth + right2, view.getBottom());
            this.t.setAlpha((int) (255.0f * max));
            this.t.draw(canvas);
        } else if (this.u != null && a(view, 5)) {
            int intrinsicWidth2 = this.u.getIntrinsicWidth();
            int left = view.getLeft();
            float max2 = Math.max(0.0f, Math.min(((float) (getWidth() - left)) / ((float) this.g.b()), 1.0f));
            this.u.setBounds(left - intrinsicWidth2, view.getTop(), left, view.getBottom());
            this.u.setAlpha((int) (255.0f * max2));
            this.u.draw(canvas);
        }
        return drawChild;
    }

    /* access modifiers changed from: package-private */
    public boolean f(View view) {
        return ((LayoutParams) view.getLayoutParams()).f115a == 0;
    }

    /* access modifiers changed from: package-private */
    public boolean g(View view) {
        return (n.a(((LayoutParams) view.getLayoutParams()).f115a, at.e(view)) & 7) != 0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        int a2 = z.a(motionEvent);
        boolean a3 = this.f.a(motionEvent) | this.g.a(motionEvent);
        switch (a2) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.r = x;
                this.s = y;
                if (this.d <= 0.0f || !f(this.f.e((int) x, (int) y))) {
                    z = false;
                } else {
                    z = true;
                }
                this.o = false;
                this.p = false;
                break;
            case 1:
            case 3:
                a(true);
                this.o = false;
                this.p = false;
                z = false;
                break;
            case 2:
                if (this.f.d(3)) {
                    this.h.a();
                    this.i.a();
                    z = false;
                    break;
                }
                z = false;
                break;
            default:
                z = false;
                break;
        }
        if (a3 || z || e() || this.p) {
            return true;
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View a2;
        this.f.b(motionEvent);
        this.g.b(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.r = x;
                this.s = y;
                this.o = false;
                this.p = false;
                break;
            case 1:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                View e2 = this.f.e((int) x2, (int) y2);
                if (e2 != null && f(e2)) {
                    float f2 = x2 - this.r;
                    float f3 = y2 - this.s;
                    int d2 = this.f.d();
                    if ((f2 * f2) + (f3 * f3) < ((float) (d2 * d2)) && (a2 = a()) != null) {
                        z = a(a2) == 2;
                        a(z);
                        this.o = false;
                        break;
                    }
                }
                z = true;
                a(z);
                this.o = false;
            case 3:
                a(true);
                this.o = false;
                this.p = false;
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        if (!this.f.e(1) && !this.g.e(2)) {
            super.requestDisallowInterceptTouchEvent(z);
        }
        this.o = z;
        if (z) {
            a(true);
        }
    }

    public void b() {
        a(false);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        int childCount = getChildCount();
        boolean z2 = false;
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (g(childAt) && (!z || layoutParams.f117c)) {
                int width = childAt.getWidth();
                if (a(childAt, 3)) {
                    z2 |= this.f.a(childAt, -width, childAt.getTop());
                } else {
                    z2 |= this.g.a(childAt, getWidth(), childAt.getTop());
                }
                layoutParams.f117c = false;
            }
        }
        this.h.a();
        this.i.a();
        if (z2) {
            invalidate();
        }
    }

    public void h(View view) {
        if (!g(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.l) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            layoutParams.f116b = 1.0f;
            layoutParams.d = true;
        } else if (a(view, 3)) {
            this.f.a(view, 0, view.getTop());
        } else {
            this.g.a(view, getWidth() - view.getWidth(), view.getTop());
        }
        invalidate();
    }

    public void c(int i2) {
        int a2 = n.a(i2, at.e(this));
        View a3 = a(a2);
        if (a3 == null) {
            throw new IllegalArgumentException("No drawer view found with absolute gravity " + b(a2));
        }
        h(a3);
    }

    public void i(View view) {
        if (!g(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.l) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            layoutParams.f116b = 0.0f;
            layoutParams.d = false;
        } else if (a(view, 3)) {
            this.f.a(view, -view.getWidth(), view.getTop());
        } else {
            this.g.a(view, getWidth(), view.getTop());
        }
        invalidate();
    }

    public void d(int i2) {
        int a2 = n.a(i2, at.e(this));
        View a3 = a(a2);
        if (a3 == null) {
            throw new IllegalArgumentException("No drawer view found with absolute gravity " + b(a2));
        }
        i(a3);
    }

    public boolean j(View view) {
        if (g(view)) {
            return ((LayoutParams) view.getLayoutParams()).d;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public boolean e(int i2) {
        View a2 = a(i2);
        if (a2 != null) {
            return j(a2);
        }
        return false;
    }

    public boolean k(View view) {
        if (g(view)) {
            return ((LayoutParams) view.getLayoutParams()).f116b > 0.0f;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public boolean f(int i2) {
        View a2 = a(i2);
        if (a2 != null) {
            return k(a2);
        }
        return false;
    }

    private boolean e() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (((LayoutParams) getChildAt(i2).getLayoutParams()).f117c) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -1);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof LayoutParams) {
            return new LayoutParams((LayoutParams) layoutParams);
        }
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    private boolean f() {
        return g() != null;
    }

    private View g() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (g(childAt) && k(childAt)) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (!this.p) {
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                getChildAt(i2).dispatchTouchEvent(obtain);
            }
            obtain.recycle();
            this.p = true;
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !f()) {
            return super.onKeyDown(i2, keyEvent);
        }
        s.b(keyEvent);
        return true;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyUp(i2, keyEvent);
        }
        View g2 = g();
        if (g2 != null && a(g2) == 0) {
            b();
        }
        return g2 != null;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        View a2;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!(savedState.f118a == 0 || (a2 = a(savedState.f118a)) == null)) {
            h(a2);
        }
        a(savedState.f119b, 3);
        a(savedState.f120c, 5);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        int childCount = getChildCount();
        int i2 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            if (g(childAt)) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.d) {
                    savedState.f118a = layoutParams.f115a;
                    break;
                }
            }
            i2++;
        }
        savedState.f119b = this.m;
        savedState.f120c = this.n;
        return savedState;
    }

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new i();

        /* renamed from: a  reason: collision with root package name */
        int f118a = 0;

        /* renamed from: b  reason: collision with root package name */
        int f119b = 0;

        /* renamed from: c  reason: collision with root package name */
        int f120c = 0;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.f118a = parcel.readInt();
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f118a);
        }
    }

    public class LayoutParams extends ViewGroup.MarginLayoutParams {

        /* renamed from: a  reason: collision with root package name */
        public int f115a = 0;

        /* renamed from: b  reason: collision with root package name */
        float f116b;

        /* renamed from: c  reason: collision with root package name */
        boolean f117c;
        boolean d;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.f112a);
            this.f115a = obtainStyledAttributes.getInt(0, 0);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.MarginLayoutParams) layoutParams);
            this.f115a = layoutParams.f115a;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }
    }
}
