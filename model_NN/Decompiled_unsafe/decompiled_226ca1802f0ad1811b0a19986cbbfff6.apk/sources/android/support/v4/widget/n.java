package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;

/* compiled from: EdgeEffectCompat */
class n implements o {
    n() {
    }

    public Object a(Context context) {
        return p.a(context);
    }

    public void a(Object obj, int i, int i2) {
        p.a(obj, i, i2);
    }

    public boolean a(Object obj) {
        return p.a(obj);
    }

    public void b(Object obj) {
        p.b(obj);
    }

    public boolean a(Object obj, float f) {
        return p.a(obj, f);
    }

    public boolean c(Object obj) {
        return p.c(obj);
    }

    public boolean a(Object obj, Canvas canvas) {
        return p.a(obj, canvas);
    }
}
