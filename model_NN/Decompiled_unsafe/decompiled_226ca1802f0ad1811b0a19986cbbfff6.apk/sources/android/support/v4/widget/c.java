package android.support.v4.widget;

import android.database.ContentObserver;
import android.os.Handler;

/* compiled from: CursorAdapter */
class c extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f135a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(a aVar) {
        super(new Handler());
        this.f135a = aVar;
    }

    public boolean deliverSelfNotifications() {
        return true;
    }

    public void onChange(boolean z) {
        this.f135a.onContentChanged();
    }
}
