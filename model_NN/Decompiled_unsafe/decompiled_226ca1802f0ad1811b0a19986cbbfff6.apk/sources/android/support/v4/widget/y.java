package android.support.v4.widget;

import android.graphics.Rect;
import android.support.v4.view.a;
import android.support.v4.view.at;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: SlidingPaneLayout */
class y extends a {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SlidingPaneLayout f148b;

    /* renamed from: c  reason: collision with root package name */
    private final Rect f149c = new Rect();

    y(SlidingPaneLayout slidingPaneLayout) {
        this.f148b = slidingPaneLayout;
    }

    public void a(View view, android.support.v4.view.a.a aVar) {
        android.support.v4.view.a.a a2 = android.support.v4.view.a.a.a(aVar);
        super.a(view, a2);
        aVar.a(view);
        ViewParent f = at.f(view);
        if (f instanceof View) {
            aVar.c((View) f);
        }
        a(aVar, a2);
        a2.n();
        int childCount = this.f148b.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.f148b.getChildAt(i);
            if (!b(childAt)) {
                aVar.b(childAt);
            }
        }
    }

    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        if (!b(view)) {
            return super.a(viewGroup, view, accessibilityEvent);
        }
        return false;
    }

    public boolean b(View view) {
        return this.f148b.e(view);
    }

    private void a(android.support.v4.view.a.a aVar, android.support.v4.view.a.a aVar2) {
        Rect rect = this.f149c;
        aVar2.a(rect);
        aVar.b(rect);
        aVar2.c(rect);
        aVar.d(rect);
        aVar.c(aVar2.e());
        aVar.a(aVar2.k());
        aVar.b(aVar2.l());
        aVar.c(aVar2.m());
        aVar.h(aVar2.j());
        aVar.f(aVar2.h());
        aVar.a(aVar2.c());
        aVar.b(aVar2.d());
        aVar.d(aVar2.f());
        aVar.e(aVar2.g());
        aVar.g(aVar2.i());
        aVar.a(aVar2.b());
    }
}
