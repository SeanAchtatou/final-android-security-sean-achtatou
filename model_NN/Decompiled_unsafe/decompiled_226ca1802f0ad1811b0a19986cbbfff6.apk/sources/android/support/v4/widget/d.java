package android.support.v4.widget;

import android.database.DataSetObserver;

/* compiled from: CursorAdapter */
class d extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f136a;

    private d(a aVar) {
        this.f136a = aVar;
    }

    public void onChanged() {
        this.f136a.mDataValid = true;
        this.f136a.notifyDataSetChanged();
    }

    public void onInvalidated() {
        this.f136a.mDataValid = false;
        this.f136a.notifyDataSetInvalidated();
    }
}
