package android.support.v4.widget;

import android.support.v4.widget.SlidingPaneLayout;
import android.view.View;

/* compiled from: SlidingPaneLayout */
class aa extends ak {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SlidingPaneLayout f128a;

    private aa(SlidingPaneLayout slidingPaneLayout) {
        this.f128a = slidingPaneLayout;
    }

    public boolean a(View view, int i) {
        if (this.f128a.k) {
            return false;
        }
        return ((SlidingPaneLayout.LayoutParams) view.getLayoutParams()).f125b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, boolean):boolean
     arg types: [android.support.v4.widget.SlidingPaneLayout, int]
     candidates:
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, int):void
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, android.view.View):void
      android.support.v4.widget.SlidingPaneLayout.a(android.view.View, int):boolean
      android.support.v4.widget.SlidingPaneLayout.a(float, int):boolean
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, boolean):boolean */
    public void a(int i) {
        if (this.f128a.p.a() != 0) {
            return;
        }
        if (this.f128a.h == 0.0f) {
            this.f128a.d(this.f128a.g);
            this.f128a.c(this.f128a.g);
            boolean unused = this.f128a.q = false;
            return;
        }
        this.f128a.b(this.f128a.g);
        boolean unused2 = this.f128a.q = true;
    }

    public void b(View view, int i) {
        this.f128a.a();
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        this.f128a.a(i);
        this.f128a.invalidate();
    }

    public void a(View view, float f, float f2) {
        int paddingLeft = ((SlidingPaneLayout.LayoutParams) view.getLayoutParams()).leftMargin + this.f128a.getPaddingLeft();
        if (f > 0.0f || (f == 0.0f && this.f128a.h > 0.5f)) {
            paddingLeft += this.f128a.j;
        }
        this.f128a.p.a(paddingLeft, view.getTop());
        this.f128a.invalidate();
    }

    public int a(View view) {
        return this.f128a.j;
    }

    public int a(View view, int i, int i2) {
        int paddingLeft = ((SlidingPaneLayout.LayoutParams) this.f128a.g.getLayoutParams()).leftMargin + this.f128a.getPaddingLeft();
        return Math.min(Math.max(i, paddingLeft), this.f128a.j + paddingLeft);
    }

    public void b(int i, int i2) {
        this.f128a.p.a(this.f128a.g, i2);
    }
}
