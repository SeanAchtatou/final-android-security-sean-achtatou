package android.support.v4.widget;

import android.view.View;

/* compiled from: DrawerLayout */
public interface h {
    void onDrawerClosed(View view);

    void onDrawerOpened(View view);

    void onDrawerSlide(View view, float f);

    void onDrawerStateChanged(int i);
}
