package android.support.v4.app;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.h;
import android.view.MenuItem;
import android.view.View;

public class ActionBarDrawerToggle implements h {
    private static final int ID_HOME = 16908332;
    private static final ActionBarDrawerToggleImpl IMPL;
    private final Activity mActivity;
    private final int mCloseDrawerContentDescRes;
    private Drawable mDrawerImage;
    private final int mDrawerImageResource;
    private boolean mDrawerIndicatorEnabled = true;
    private final DrawerLayout mDrawerLayout;
    private final int mOpenDrawerContentDescRes;
    private Object mSetIndicatorInfo;
    private SlideDrawable mSlider;
    private Drawable mThemeImage;

    interface ActionBarDrawerToggleImpl {
        Drawable getThemeUpIndicator(Activity activity);

        Object setActionBarDescription(Object obj, Activity activity, int i);

        Object setActionBarUpIndicator(Object obj, Activity activity, Drawable drawable, int i);
    }

    class ActionBarDrawerToggleImplBase implements ActionBarDrawerToggleImpl {
        private ActionBarDrawerToggleImplBase() {
        }

        public Drawable getThemeUpIndicator(Activity activity) {
            return null;
        }

        public Object setActionBarUpIndicator(Object obj, Activity activity, Drawable drawable, int i) {
            return obj;
        }

        public Object setActionBarDescription(Object obj, Activity activity, int i) {
            return obj;
        }
    }

    class ActionBarDrawerToggleImplHC implements ActionBarDrawerToggleImpl {
        private ActionBarDrawerToggleImplHC() {
        }

        public Drawable getThemeUpIndicator(Activity activity) {
            return ActionBarDrawerToggleHoneycomb.getThemeUpIndicator(activity);
        }

        public Object setActionBarUpIndicator(Object obj, Activity activity, Drawable drawable, int i) {
            return ActionBarDrawerToggleHoneycomb.setActionBarUpIndicator(obj, activity, drawable, i);
        }

        public Object setActionBarDescription(Object obj, Activity activity, int i) {
            return ActionBarDrawerToggleHoneycomb.setActionBarDescription(obj, activity, i);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            IMPL = new ActionBarDrawerToggleImplHC();
        } else {
            IMPL = new ActionBarDrawerToggleImplBase();
        }
    }

    public ActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, int i, int i2, int i3) {
        this.mActivity = activity;
        this.mDrawerLayout = drawerLayout;
        this.mDrawerImageResource = i;
        this.mOpenDrawerContentDescRes = i2;
        this.mCloseDrawerContentDescRes = i3;
        this.mThemeImage = IMPL.getThemeUpIndicator(activity);
        this.mDrawerImage = activity.getResources().getDrawable(i);
        this.mSlider = new SlideDrawable(this.mDrawerImage);
        this.mSlider.setOffsetBy(0.33333334f);
    }

    public void syncState() {
        if (this.mDrawerLayout.e(8388611)) {
            this.mSlider.setOffset(1.0f);
        } else {
            this.mSlider.setOffset(0.0f);
        }
        if (this.mDrawerIndicatorEnabled) {
            this.mSetIndicatorInfo = IMPL.setActionBarUpIndicator(this.mSetIndicatorInfo, this.mActivity, this.mSlider, this.mDrawerLayout.e(8388611) ? this.mOpenDrawerContentDescRes : this.mCloseDrawerContentDescRes);
        }
    }

    public void setDrawerIndicatorEnabled(boolean z) {
        if (z != this.mDrawerIndicatorEnabled) {
            if (z) {
                this.mSetIndicatorInfo = IMPL.setActionBarUpIndicator(this.mSetIndicatorInfo, this.mActivity, this.mSlider, this.mDrawerLayout.e(8388611) ? this.mOpenDrawerContentDescRes : this.mCloseDrawerContentDescRes);
            } else {
                this.mSetIndicatorInfo = IMPL.setActionBarUpIndicator(this.mSetIndicatorInfo, this.mActivity, this.mThemeImage, 0);
            }
            this.mDrawerIndicatorEnabled = z;
        }
    }

    public boolean isDrawerIndicatorEnabled() {
        return this.mDrawerIndicatorEnabled;
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.mThemeImage = IMPL.getThemeUpIndicator(this.mActivity);
        this.mDrawerImage = this.mActivity.getResources().getDrawable(this.mDrawerImageResource);
        syncState();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem == null || menuItem.getItemId() != ID_HOME || !this.mDrawerIndicatorEnabled) {
            return false;
        }
        if (this.mDrawerLayout.f(8388611)) {
            this.mDrawerLayout.d(8388611);
            return false;
        }
        this.mDrawerLayout.c(8388611);
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void onDrawerSlide(View view, float f) {
        float min;
        float offset = this.mSlider.getOffset();
        if (f > 0.5f) {
            min = Math.max(offset, Math.max(0.0f, f - 0.5f) * 2.0f);
        } else {
            min = Math.min(offset, f * 2.0f);
        }
        this.mSlider.setOffset(min);
    }

    public void onDrawerOpened(View view) {
        this.mSlider.setOffset(1.0f);
        if (this.mDrawerIndicatorEnabled) {
            this.mSetIndicatorInfo = IMPL.setActionBarDescription(this.mSetIndicatorInfo, this.mActivity, this.mOpenDrawerContentDescRes);
        }
    }

    public void onDrawerClosed(View view) {
        this.mSlider.setOffset(0.0f);
        if (this.mDrawerIndicatorEnabled) {
            this.mSetIndicatorInfo = IMPL.setActionBarDescription(this.mSetIndicatorInfo, this.mActivity, this.mCloseDrawerContentDescRes);
        }
    }

    public void onDrawerStateChanged(int i) {
    }

    class SlideDrawable extends Drawable implements Drawable.Callback {
        private float mOffset;
        private float mOffsetBy;
        private final Rect mTmpRect = new Rect();
        private Drawable mWrapped;

        public SlideDrawable(Drawable drawable) {
            this.mWrapped = drawable;
        }

        public void setOffset(float f) {
            this.mOffset = f;
            invalidateSelf();
        }

        public float getOffset() {
            return this.mOffset;
        }

        public void setOffsetBy(float f) {
            this.mOffsetBy = f;
            invalidateSelf();
        }

        public void draw(Canvas canvas) {
            this.mWrapped.copyBounds(this.mTmpRect);
            canvas.save();
            canvas.translate(this.mOffsetBy * ((float) this.mTmpRect.width()) * (-this.mOffset), 0.0f);
            this.mWrapped.draw(canvas);
            canvas.restore();
        }

        public void setChangingConfigurations(int i) {
            this.mWrapped.setChangingConfigurations(i);
        }

        public int getChangingConfigurations() {
            return this.mWrapped.getChangingConfigurations();
        }

        public void setDither(boolean z) {
            this.mWrapped.setDither(z);
        }

        public void setFilterBitmap(boolean z) {
            this.mWrapped.setFilterBitmap(z);
        }

        public void setAlpha(int i) {
            this.mWrapped.setAlpha(i);
        }

        public void setColorFilter(ColorFilter colorFilter) {
            this.mWrapped.setColorFilter(colorFilter);
        }

        public void setColorFilter(int i, PorterDuff.Mode mode) {
            this.mWrapped.setColorFilter(i, mode);
        }

        public void clearColorFilter() {
            this.mWrapped.clearColorFilter();
        }

        public boolean isStateful() {
            return this.mWrapped.isStateful();
        }

        public boolean setState(int[] iArr) {
            return this.mWrapped.setState(iArr);
        }

        public int[] getState() {
            return this.mWrapped.getState();
        }

        public Drawable getCurrent() {
            return this.mWrapped.getCurrent();
        }

        public boolean setVisible(boolean z, boolean z2) {
            return super.setVisible(z, z2);
        }

        public int getOpacity() {
            return this.mWrapped.getOpacity();
        }

        public Region getTransparentRegion() {
            return this.mWrapped.getTransparentRegion();
        }

        /* access modifiers changed from: protected */
        public boolean onStateChange(int[] iArr) {
            this.mWrapped.setState(iArr);
            return super.onStateChange(iArr);
        }

        /* access modifiers changed from: protected */
        public void onBoundsChange(Rect rect) {
            super.onBoundsChange(rect);
            this.mWrapped.setBounds(rect);
        }

        public int getIntrinsicWidth() {
            return this.mWrapped.getIntrinsicWidth();
        }

        public int getIntrinsicHeight() {
            return this.mWrapped.getIntrinsicHeight();
        }

        public int getMinimumWidth() {
            return this.mWrapped.getMinimumWidth();
        }

        public int getMinimumHeight() {
            return this.mWrapped.getMinimumHeight();
        }

        public boolean getPadding(Rect rect) {
            return this.mWrapped.getPadding(rect);
        }

        public Drawable.ConstantState getConstantState() {
            return super.getConstantState();
        }

        public void invalidateDrawable(Drawable drawable) {
            if (drawable == this.mWrapped) {
                invalidateSelf();
            }
        }

        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            if (drawable == this.mWrapped) {
                scheduleSelf(runnable, j);
            }
        }

        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            if (drawable == this.mWrapped) {
                unscheduleSelf(runnable);
            }
        }
    }
}
