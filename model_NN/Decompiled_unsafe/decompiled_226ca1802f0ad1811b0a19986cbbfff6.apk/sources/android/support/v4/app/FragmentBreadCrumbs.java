package android.support.v4.app;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.avast.android.generic.t;
import com.avast.android.generic.ui.rtl.c;
import com.avast.android.generic.y;

public class FragmentBreadCrumbs extends ViewGroup implements FragmentManager.OnBackStackChangedListener {
    private static final int MEASURED_HEIGHT_STATE_SHIFT = 16;
    private static final int MEASURED_STATE_MASK = -16777216;
    private static final int MEASURED_STATE_TOO_SMALL = 16777216;
    FragmentActivity mActivity;
    LinearLayout mContainer;
    LayoutInflater mInflater;
    BackStackRecord mLastEntry;
    View.OnClickListener mLastEntryListener;
    int mMaxVisible;
    /* access modifiers changed from: private */
    public OnBreadCrumbClickListener mOnBreadCrumbClickListener;
    private View.OnClickListener mOnClickListener;
    /* access modifiers changed from: private */
    public View.OnClickListener mParentClickListener;
    BackStackRecord mParentEntry;
    private int mParentWidth;
    private c mRtlHelper;
    BackStackRecord mTopEntry;

    public interface OnBreadCrumbClickListener {
        boolean onBreadCrumbClick(FragmentManager.BackStackEntry backStackEntry, int i);
    }

    private static final int getMeasuredState(View view) {
        return (view.getMeasuredWidth() & MEASURED_STATE_MASK) | ((view.getMeasuredHeight() >> 16) & -256);
    }

    public static int combineMeasuredStates(int i, int i2) {
        return i | i2;
    }

    public static int resolveSizeAndState(int i, int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (size < i) {
                    i = size | MEASURED_STATE_TOO_SMALL;
                    break;
                }
                break;
            case 1073741824:
                i = size;
                break;
        }
        return (MEASURED_STATE_MASK & i3) | i;
    }

    public FragmentBreadCrumbs(Context context) {
        this(context, null);
    }

    public FragmentBreadCrumbs(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, y.Widget_HanselAndGretel_FragmentBreadCrumb);
    }

    public FragmentBreadCrumbs(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mMaxVisible = -1;
        this.mParentWidth = -1;
        this.mOnClickListener = new View.OnClickListener() {
            public void onClick(View view) {
                if (view.getTag() instanceof FragmentManager.BackStackEntry) {
                    FragmentManager.BackStackEntry backStackEntry = (FragmentManager.BackStackEntry) view.getTag();
                    if (backStackEntry == FragmentBreadCrumbs.this.mParentEntry) {
                        if (FragmentBreadCrumbs.this.mParentClickListener != null) {
                            FragmentBreadCrumbs.this.mParentClickListener.onClick(view);
                        }
                    } else if (backStackEntry != FragmentBreadCrumbs.this.mLastEntry || FragmentBreadCrumbs.this.mLastEntryListener == null) {
                        if (FragmentBreadCrumbs.this.mOnBreadCrumbClickListener != null) {
                            if (FragmentBreadCrumbs.this.mOnBreadCrumbClickListener.onBreadCrumbClick(backStackEntry == FragmentBreadCrumbs.this.mTopEntry ? null : backStackEntry, 0)) {
                                return;
                            }
                        }
                        if (backStackEntry == FragmentBreadCrumbs.this.mTopEntry) {
                            FragmentBreadCrumbs.this.mActivity.getSupportFragmentManager().popBackStack();
                        } else {
                            FragmentBreadCrumbs.this.mActivity.getSupportFragmentManager().popBackStack(backStackEntry.getId(), 0);
                        }
                    } else {
                        FragmentBreadCrumbs.this.mLastEntryListener.onClick(view);
                    }
                }
            }
        };
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (getContext() instanceof FragmentActivity) {
            setActivity((FragmentActivity) getContext());
            return;
        }
        throw new IllegalStateException("FragmentBreadCrumbs can be used only in FragmentActivity");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v4.app.FragmentBreadCrumbs, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void setActivity(FragmentActivity fragmentActivity) {
        this.mActivity = fragmentActivity;
        this.mRtlHelper = new c(this.mActivity);
        if (this.mContainer == null) {
            this.mInflater = (LayoutInflater) fragmentActivity.getSystemService("layout_inflater");
            this.mContainer = (LinearLayout) this.mInflater.inflate(t.hag__fragment_bread_crumbs, (ViewGroup) this, false);
            addView(this.mContainer);
            fragmentActivity.getSupportFragmentManager().addOnBackStackChangedListener(this);
            updateCrumbs();
        }
    }

    public void setMaxVisible(int i) {
        if (i < 1) {
            throw new IllegalArgumentException("visibleCrumbs must be greater than zero");
        }
        this.mMaxVisible = i;
    }

    public void setParentTitle(CharSequence charSequence, CharSequence charSequence2, View.OnClickListener onClickListener) {
        this.mParentEntry = createBackStackEntry(charSequence, charSequence2);
        this.mParentClickListener = onClickListener;
        updateCrumbs();
    }

    public void setOnBreadCrumbClickListener(OnBreadCrumbClickListener onBreadCrumbClickListener) {
        this.mOnBreadCrumbClickListener = onBreadCrumbClickListener;
    }

    private BackStackRecord createBackStackEntry(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence == null) {
            return null;
        }
        BackStackRecord backStackRecord = new BackStackRecord((FragmentManagerImpl) this.mActivity.getSupportFragmentManager());
        backStackRecord.setBreadCrumbTitle(charSequence);
        backStackRecord.setBreadCrumbShortTitle(charSequence2);
        return backStackRecord;
    }

    public void setTitle(CharSequence charSequence, CharSequence charSequence2) {
        this.mTopEntry = createBackStackEntry(charSequence, charSequence2);
        updateCrumbs();
    }

    public void setLastTitle(CharSequence charSequence, CharSequence charSequence2, View.OnClickListener onClickListener) {
        this.mLastEntry = createBackStackEntry(charSequence, charSequence2);
        this.mLastEntryListener = onClickListener;
        updateCrumbs();
    }

    public void setLastTitle(CharSequence charSequence, CharSequence charSequence2) {
        setLastTitle(charSequence, charSequence2, null);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            childAt.layout(paddingLeft, paddingTop, (childAt.getMeasuredWidth() + paddingLeft) - paddingRight, (childAt.getMeasuredHeight() + paddingTop) - paddingBottom);
        }
        if (this.mParentWidth < getMeasuredWidth()) {
            this.mParentWidth = getMeasuredWidth();
        }
        if (this.mParentWidth == this.mContainer.getMeasuredWidth()) {
            com.avast.android.generic.util.t.d("TOO BIG");
            int childCount2 = this.mContainer.getChildCount();
            int i6 = 0;
            while (i6 < childCount2) {
                if (this.mContainer.getChildAt(i6).getVisibility() != 0 || this.mContainer.getChildAt(i6).findViewById(16908310).getTag() == this.mLastEntry) {
                    i6++;
                } else {
                    this.mContainer.getChildAt(i6).setVisibility(8);
                    if (!(this.mContainer.getChildAt(i6 + 1) == null || this.mContainer.getChildAt(i6 + 1).findViewById(16908294) == null)) {
                        this.mContainer.getChildAt(i6 + 1).findViewById(16908294).setVisibility(8);
                    }
                    this.mContainer.invalidate();
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = 0;
        int childCount = getChildCount();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i4 = 0;
        int i5 = 0;
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                measureChild(childAt, i, i2);
                i4 = Math.max(i4, childAt.getMeasuredWidth());
                i5 = Math.max(i5, childAt.getMeasuredHeight());
                i3 = combineMeasuredStates(i3, getMeasuredState(childAt));
            }
        }
        setMeasuredDimension(resolveSizeAndState(Math.max(i4 + paddingLeft + paddingRight, getSuggestedMinimumWidth()), i, i3), resolveSizeAndState(Math.max(i5 + paddingTop + paddingBottom, getSuggestedMinimumHeight()), i2, i3 << 16));
    }

    public void onBackStackChanged() {
        updateCrumbs();
    }

    private int getPreEntryCount() {
        int i = 1;
        int i2 = this.mTopEntry != null ? 1 : 0;
        if (this.mParentEntry == null) {
            i = 0;
        }
        return i2 + i;
    }

    private int getPostEntryCount() {
        return this.mLastEntry == null ? 0 : 1;
    }

    private FragmentManager.BackStackEntry getPreEntry(int i) {
        if (this.mParentEntry != null) {
            return i == 0 ? this.mParentEntry : this.mTopEntry;
        }
        return this.mTopEntry;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v4.app.FragmentBreadCrumbs, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: package-private */
    public void updateCrumbs() {
        int i;
        int i2;
        int i3;
        FragmentManager.BackStackEntry backStackEntry;
        int i4;
        FragmentManager supportFragmentManager = this.mActivity.getSupportFragmentManager();
        int backStackEntryCount = supportFragmentManager.getBackStackEntryCount();
        int preEntryCount = getPreEntryCount();
        int postEntryCount = getPostEntryCount();
        int childCount = this.mContainer.getChildCount();
        int i5 = 0;
        while (i5 < backStackEntryCount + preEntryCount + postEntryCount) {
            if (i5 < preEntryCount) {
                backStackEntry = getPreEntry(i5);
            } else if (i5 < preEntryCount + backStackEntryCount) {
                backStackEntry = supportFragmentManager.getBackStackEntryAt(i5 - preEntryCount);
            } else {
                backStackEntry = this.mLastEntry;
            }
            if (i5 >= childCount || this.mContainer.getChildAt(i5).getTag() == backStackEntry) {
                i4 = childCount;
            } else {
                for (int i6 = i5; i6 < childCount; i6++) {
                    this.mContainer.removeViewAt(i5);
                }
                i4 = i5;
            }
            if (i5 >= i4) {
                View inflate = this.mInflater.inflate(t.hag__fragment_bread_crumb_item, (ViewGroup) this, false);
                TextView textView = (TextView) inflate.findViewById(16908310);
                textView.setText(backStackEntry.getBreadCrumbTitle());
                textView.setTag(backStackEntry);
                if (i5 == 0) {
                    inflate.findViewById(16908294).setVisibility(8);
                }
                this.mContainer.addView(inflate);
                textView.setOnClickListener(this.mOnClickListener);
            }
            i5++;
            childCount = i4;
        }
        int i7 = backStackEntryCount + preEntryCount + postEntryCount;
        int childCount2 = this.mContainer.getChildCount();
        while (childCount2 > i7) {
            this.mContainer.removeViewAt(childCount2 - 1);
            childCount2--;
        }
        int i8 = 0;
        while (i8 < childCount2) {
            View childAt = this.mContainer.getChildAt(i8);
            View findViewById = childAt.findViewById(16908310);
            if (this.mLastEntryListener == null) {
                i = 1;
            } else {
                i = 0;
            }
            findViewById.setEnabled(i8 < childCount2 - i);
            if (this.mMaxVisible > 0) {
                if (i8 < childCount2 - this.mMaxVisible) {
                    i2 = 8;
                } else {
                    i2 = 0;
                }
                childAt.setVisibility(i2);
                View findViewById2 = childAt.findViewById(16908294);
                if (i8 <= childCount2 - this.mMaxVisible || i8 == 0) {
                    i3 = 8;
                } else {
                    i3 = 0;
                }
                findViewById2.setVisibility(i3);
            }
            i8++;
        }
        this.mRtlHelper.b(this.mContainer);
        this.mRtlHelper.a(this.mContainer);
    }
}
