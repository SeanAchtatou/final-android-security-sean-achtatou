package android.support.v4.app;

import android.view.View;
import com.actionbarsherlock.ActionBarSherlock;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import java.util.ArrayList;

public abstract class Watson extends FragmentActivity implements ActionBarSherlock.OnCreatePanelMenuListener, ActionBarSherlock.OnMenuItemSelectedListener, ActionBarSherlock.OnPreparePanelListener {
    private static final String TAG = "Watson";
    private ArrayList<Fragment> mCreatedMenus;

    public interface OnCreateOptionsMenuListener {
        void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater);
    }

    public interface OnOptionsItemSelectedListener {
        boolean onOptionsItemSelected(MenuItem menuItem);
    }

    public interface OnPrepareOptionsMenuListener {
        void onPrepareOptionsMenu(Menu menu);
    }

    public abstract MenuInflater getSupportMenuInflater();

    public abstract boolean onCreateOptionsMenu(Menu menu);

    public abstract boolean onOptionsItemSelected(MenuItem menuItem);

    public abstract boolean onPrepareOptionsMenu(Menu menu);

    public boolean onCreatePanelMenu(int i, Menu menu) {
        boolean z;
        boolean z2;
        ArrayList<Fragment> arrayList;
        if (i != 0) {
            return false;
        }
        boolean onCreateOptionsMenu = onCreateOptionsMenu(menu);
        MenuInflater supportMenuInflater = getSupportMenuInflater();
        ArrayList<Fragment> arrayList2 = null;
        if (this.mFragments.mAdded != null) {
            int i2 = 0;
            z = false;
            while (i2 < this.mFragments.mAdded.size()) {
                Fragment fragment = this.mFragments.mAdded.get(i2);
                if (fragment == null || fragment.mHidden || !fragment.mHasMenu || !fragment.mMenuVisible || !(fragment instanceof OnCreateOptionsMenuListener)) {
                    z2 = z;
                } else {
                    ((OnCreateOptionsMenuListener) fragment).onCreateOptionsMenu(menu, supportMenuInflater);
                    if (arrayList2 == null) {
                        arrayList = new ArrayList<>();
                    } else {
                        arrayList = arrayList2;
                    }
                    arrayList.add(fragment);
                    arrayList2 = arrayList;
                    z2 = true;
                }
                i2++;
                z = z2;
            }
        } else {
            z = false;
        }
        if (this.mCreatedMenus != null) {
            for (int i3 = 0; i3 < this.mCreatedMenus.size(); i3++) {
                Fragment fragment2 = this.mCreatedMenus.get(i3);
                if (arrayList2 == null || !arrayList2.contains(fragment2)) {
                    fragment2.onDestroyOptionsMenu();
                }
            }
        }
        this.mCreatedMenus = arrayList2;
        return onCreateOptionsMenu | z;
    }

    public boolean onPreparePanel(int i, View view, Menu menu) {
        boolean z;
        if (i != 0) {
            return false;
        }
        boolean onPrepareOptionsMenu = onPrepareOptionsMenu(menu);
        if (this.mFragments.mAdded != null) {
            z = false;
            for (int i2 = 0; i2 < this.mFragments.mAdded.size(); i2++) {
                Fragment fragment = this.mFragments.mAdded.get(i2);
                if (fragment != null && !fragment.mHidden && fragment.mHasMenu && fragment.mMenuVisible && (fragment instanceof OnPrepareOptionsMenuListener)) {
                    z = true;
                    ((OnPrepareOptionsMenuListener) fragment).onPrepareOptionsMenu(menu);
                }
            }
        } else {
            z = false;
        }
        return (onPrepareOptionsMenu | z) & menu.hasVisibleItems();
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (i != 0) {
            return false;
        }
        if (onOptionsItemSelected(menuItem)) {
            return true;
        }
        if (this.mFragments.mAdded == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.mFragments.mAdded.size(); i2++) {
            Fragment fragment = this.mFragments.mAdded.get(i2);
            if (fragment != null && !fragment.mHidden && fragment.mHasMenu && fragment.mMenuVisible && (fragment instanceof OnOptionsItemSelectedListener) && ((OnOptionsItemSelectedListener) fragment).onOptionsItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }
}
