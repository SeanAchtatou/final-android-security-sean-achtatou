package android.support.v4.b;

import android.os.Parcel;

/* compiled from: ParcelableCompatCreatorCallbacks */
public interface c<T> {
    T a(Parcel parcel, ClassLoader classLoader);

    T[] a(int i);
}
