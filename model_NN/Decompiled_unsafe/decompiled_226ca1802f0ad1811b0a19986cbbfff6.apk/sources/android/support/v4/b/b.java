package android.support.v4.b;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ParcelableCompat */
class b<T> implements Parcelable.Creator<T> {

    /* renamed from: a  reason: collision with root package name */
    final c<T> f23a;

    public b(c<T> cVar) {
        this.f23a = cVar;
    }

    public T createFromParcel(Parcel parcel) {
        return this.f23a.a(parcel, null);
    }

    public T[] newArray(int i) {
        return this.f23a.a(i);
    }
}
