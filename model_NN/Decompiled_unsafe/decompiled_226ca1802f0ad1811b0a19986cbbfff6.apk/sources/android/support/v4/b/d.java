package android.support.v4.b;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ParcelableCompatHoneycombMR2 */
class d<T> implements Parcelable.ClassLoaderCreator<T> {

    /* renamed from: a  reason: collision with root package name */
    private final c<T> f24a;

    public d(c<T> cVar) {
        this.f24a = cVar;
    }

    public T createFromParcel(Parcel parcel) {
        return this.f24a.a(parcel, null);
    }

    public T createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return this.f24a.a(parcel, classLoader);
    }

    public T[] newArray(int i) {
        return this.f24a.a(i);
    }
}
