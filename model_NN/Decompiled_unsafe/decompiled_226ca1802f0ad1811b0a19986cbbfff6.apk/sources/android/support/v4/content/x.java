package android.support.v4.content;

import android.os.Process;

/* compiled from: ModernAsyncTask */
class x extends ad<Params, Result> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f64a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    x(v vVar) {
        super(null);
        this.f64a = vVar;
    }

    public Result call() {
        this.f64a.i.set(true);
        Process.setThreadPriority(10);
        return this.f64a.d(this.f64a.a(this.f43b));
    }
}
