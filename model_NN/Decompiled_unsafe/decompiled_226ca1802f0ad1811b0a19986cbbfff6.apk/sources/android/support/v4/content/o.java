package android.support.v4.content;

import android.content.Context;
import android.support.v4.c.a;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: Loader */
public class o<D> {
    int m;
    q<D> n;
    Context o;
    boolean p = false;
    boolean q = false;
    boolean r = true;
    boolean s = false;

    public o(Context context) {
        this.o = context.getApplicationContext();
    }

    public void b(D d) {
        if (this.n != null) {
            this.n.onLoadComplete(this, d);
        }
    }

    public Context j() {
        return this.o;
    }

    public int k() {
        return this.m;
    }

    public void a(int i, q<D> qVar) {
        if (this.n != null) {
            throw new IllegalStateException("There is already a listener registered");
        }
        this.n = qVar;
        this.m = i;
    }

    public void a(q<D> qVar) {
        if (this.n == null) {
            throw new IllegalStateException("No listener register");
        } else if (this.n != qVar) {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        } else {
            this.n = null;
        }
    }

    public boolean l() {
        return this.p;
    }

    public boolean m() {
        return this.q;
    }

    public boolean n() {
        return this.r;
    }

    public final void o() {
        this.p = true;
        this.r = false;
        this.q = false;
        g();
    }

    /* access modifiers changed from: protected */
    public void g() {
    }

    public void p() {
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public void q() {
        this.p = false;
        h();
    }

    /* access modifiers changed from: protected */
    public void h() {
    }

    public void r() {
        this.q = true;
        s();
    }

    /* access modifiers changed from: protected */
    public void s() {
    }

    public void t() {
        i();
        this.r = true;
        this.p = false;
        this.q = false;
        this.s = false;
    }

    /* access modifiers changed from: protected */
    public void i() {
    }

    public boolean u() {
        boolean z = this.s;
        this.s = false;
        return z;
    }

    public void v() {
        if (this.p) {
            p();
        } else {
            this.s = true;
        }
    }

    public String c(D d) {
        StringBuilder sb = new StringBuilder(64);
        a.a(d, sb);
        sb.append("}");
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        a.a(this, sb);
        sb.append(" id=");
        sb.append(this.m);
        sb.append("}");
        return sb.toString();
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.m);
        printWriter.print(" mListener=");
        printWriter.println(this.n);
        printWriter.print(str);
        printWriter.print("mStarted=");
        printWriter.print(this.p);
        printWriter.print(" mContentChanged=");
        printWriter.print(this.s);
        printWriter.print(" mAbandoned=");
        printWriter.print(this.q);
        printWriter.print(" mReset=");
        printWriter.println(this.r);
    }
}
