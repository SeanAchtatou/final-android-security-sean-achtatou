package android.support.v4.content;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* compiled from: LocalBroadcastManager */
class s extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f54a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    s(r rVar, Looper looper) {
        super(looper);
        this.f54a = rVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.f54a.a();
                return;
            default:
                super.handleMessage(message);
                return;
        }
    }
}
