package android.support.v4.content;

import java.util.concurrent.CountDownLatch;

/* compiled from: AsyncTaskLoader */
final class b extends v<Void, Void, D> implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    D f44a;

    /* renamed from: b  reason: collision with root package name */
    boolean f45b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ a f46c;
    private CountDownLatch e = new CountDownLatch(1);

    b(a aVar) {
        this.f46c = aVar;
    }

    /* access modifiers changed from: protected */
    public D a(Void... voidArr) {
        this.f44a = this.f46c.e();
        return this.f44a;
    }

    /* access modifiers changed from: protected */
    public void a(D d) {
        try {
            this.f46c.b(this, d);
        } finally {
            this.e.countDown();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.f46c.a((a<D>.b) this, this.f44a);
        } finally {
            this.e.countDown();
        }
    }

    public void run() {
        this.f45b = false;
        this.f46c.c();
    }
}
