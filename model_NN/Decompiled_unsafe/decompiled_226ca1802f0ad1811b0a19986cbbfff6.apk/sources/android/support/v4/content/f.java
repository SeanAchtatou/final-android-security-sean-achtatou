package android.support.v4.content;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;

/* compiled from: CursorLoader */
public class f extends a<Cursor> {
    final o<Cursor>.p f = new p(this);
    Uri g;
    String[] h;
    String i;
    String[] j;
    String k;
    Cursor l;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.content.f.a(android.database.Cursor, android.database.ContentObserver):void
     arg types: [android.database.Cursor, android.support.v4.content.o<android.database.Cursor>$android.support.v4.content.p]
     candidates:
      android.support.v4.content.a.a(android.support.v4.content.a<D>$android.support.v4.content.b, java.lang.Object):void
      android.support.v4.content.o.a(int, android.support.v4.content.q):void
      android.support.v4.content.f.a(android.database.Cursor, android.database.ContentObserver):void */
    /* renamed from: f */
    public Cursor d() {
        Cursor query = j().getContentResolver().query(this.g, this.h, this.i, this.j, this.k);
        if (query != null) {
            query.getCount();
            a(query, (ContentObserver) this.f);
        }
        return query;
    }

    /* access modifiers changed from: package-private */
    public void a(Cursor cursor, ContentObserver contentObserver) {
        cursor.registerContentObserver(this.f);
    }

    /* renamed from: a */
    public void b(Cursor cursor) {
        if (!n()) {
            Cursor cursor2 = this.l;
            this.l = cursor;
            if (l()) {
                super.b((Object) cursor);
            }
            if (cursor2 != null && cursor2 != cursor && !cursor2.isClosed()) {
                cursor2.close();
            }
        } else if (cursor != null) {
            cursor.close();
        }
    }

    public f(Context context, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        super(context);
        this.g = uri;
        this.h = strArr;
        this.i = str;
        this.j = strArr2;
        this.k = str2;
    }

    /* access modifiers changed from: protected */
    public void g() {
        if (this.l != null) {
            b(this.l);
        }
        if (u() || this.l == null) {
            p();
        }
    }

    /* access modifiers changed from: protected */
    public void h() {
        b();
    }

    /* renamed from: b */
    public void a(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    /* access modifiers changed from: protected */
    public void i() {
        super.i();
        h();
        if (this.l != null && !this.l.isClosed()) {
            this.l.close();
        }
        this.l = null;
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.a(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.print("mUri=");
        printWriter.println(this.g);
        printWriter.print(str);
        printWriter.print("mProjection=");
        printWriter.println(Arrays.toString(this.h));
        printWriter.print(str);
        printWriter.print("mSelection=");
        printWriter.println(this.i);
        printWriter.print(str);
        printWriter.print("mSelectionArgs=");
        printWriter.println(Arrays.toString(this.j));
        printWriter.print(str);
        printWriter.print("mSortOrder=");
        printWriter.println(this.k);
        printWriter.print(str);
        printWriter.print("mCursor=");
        printWriter.println(this.l);
        printWriter.print(str);
        printWriter.print("mContentChanged=");
        printWriter.println(this.s);
    }
}
