package android.support.v4.content;

import android.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/* compiled from: ModernAsyncTask */
class y extends FutureTask<Result> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f65a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    y(v vVar, Callable callable) {
        super(callable);
        this.f65a = vVar;
    }

    /* access modifiers changed from: protected */
    public void done() {
        try {
            this.f65a.c(get());
        } catch (InterruptedException e) {
            Log.w("AsyncTask", e);
        } catch (ExecutionException e2) {
            throw new RuntimeException("An error occured while executing doInBackground()", e2.getCause());
        } catch (CancellationException e3) {
            this.f65a.c(null);
        } catch (Throwable th) {
            throw new RuntimeException("An error occured while executing doInBackground()", th);
        }
    }
}
