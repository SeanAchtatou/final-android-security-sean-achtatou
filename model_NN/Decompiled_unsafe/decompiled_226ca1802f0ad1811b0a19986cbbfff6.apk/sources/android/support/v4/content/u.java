package android.support.v4.content;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.support.v4.app.NotificationCompat;

/* compiled from: LocalBroadcastManager */
class u {

    /* renamed from: a  reason: collision with root package name */
    final IntentFilter f57a;

    /* renamed from: b  reason: collision with root package name */
    final BroadcastReceiver f58b;

    /* renamed from: c  reason: collision with root package name */
    boolean f59c;

    u(IntentFilter intentFilter, BroadcastReceiver broadcastReceiver) {
        this.f57a = intentFilter;
        this.f58b = broadcastReceiver;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((int) NotificationCompat.FLAG_HIGH_PRIORITY);
        sb.append("Receiver{");
        sb.append(this.f58b);
        sb.append(" filter=");
        sb.append(this.f57a);
        sb.append("}");
        return sb.toString();
    }
}
