package android.support.v4.content;

import android.database.ContentObserver;
import android.os.Handler;

/* compiled from: Loader */
public final class p extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f50a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(o oVar) {
        super(new Handler());
        this.f50a = oVar;
    }

    public boolean deliverSelfNotifications() {
        return true;
    }

    public void onChange(boolean z) {
        this.f50a.v();
    }
}
