package android.support.v4.content;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;

/* compiled from: IntentCompat */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private static final j f49a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 15) {
            f49a = new m();
        } else if (i >= 11) {
            f49a = new l();
        } else {
            f49a = new k();
        }
    }

    public static Intent a(ComponentName componentName) {
        return f49a.a(componentName);
    }
}
