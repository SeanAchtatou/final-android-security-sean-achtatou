package android.support.v4.content;

import java.util.concurrent.Callable;

/* compiled from: ModernAsyncTask */
abstract class ad<Params, Result> implements Callable<Result> {

    /* renamed from: b  reason: collision with root package name */
    Params[] f43b;

    private ad() {
    }

    /* synthetic */ ad(w wVar) {
        this();
    }
}
