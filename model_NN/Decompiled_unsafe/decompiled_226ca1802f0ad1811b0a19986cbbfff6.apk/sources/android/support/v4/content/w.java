package android.support.v4.content;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ModernAsyncTask */
final class w implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f63a = new AtomicInteger(1);

    w() {
    }

    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, "ModernAsyncTask #" + this.f63a.getAndIncrement());
    }
}
