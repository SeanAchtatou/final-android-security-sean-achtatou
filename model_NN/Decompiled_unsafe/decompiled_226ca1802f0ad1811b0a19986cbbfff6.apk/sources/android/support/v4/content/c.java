package android.support.v4.content;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

/* compiled from: ContextCompat */
public class c {
    public static boolean startActivities(Context context, Intent[] intentArr) {
        return startActivities(context, intentArr, null);
    }

    public static boolean startActivities(Context context, Intent[] intentArr, Bundle bundle) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 16) {
            e.a(context, intentArr, bundle);
            return true;
        } else if (i < 11) {
            return false;
        } else {
            d.a(context, intentArr);
            return true;
        }
    }
}
