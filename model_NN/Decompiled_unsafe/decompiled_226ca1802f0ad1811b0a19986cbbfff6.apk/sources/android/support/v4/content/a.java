package android.support.v4.content;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.c.d;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: AsyncTaskLoader */
public abstract class a<D> extends o<D> {

    /* renamed from: a  reason: collision with root package name */
    volatile a<D>.b f35a;

    /* renamed from: b  reason: collision with root package name */
    volatile a<D>.b f36b;

    /* renamed from: c  reason: collision with root package name */
    long f37c;
    long d = -10000;
    Handler e;

    public abstract D d();

    public a(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        b();
        this.f35a = new b(this);
        c();
    }

    public boolean b() {
        boolean z = false;
        if (this.f35a != null) {
            if (this.f36b != null) {
                if (this.f35a.f45b) {
                    this.f35a.f45b = false;
                    this.e.removeCallbacks(this.f35a);
                }
                this.f35a = null;
            } else if (this.f35a.f45b) {
                this.f35a.f45b = false;
                this.e.removeCallbacks(this.f35a);
                this.f35a = null;
            } else {
                z = this.f35a.a(false);
                if (z) {
                    this.f36b = this.f35a;
                }
                this.f35a = null;
            }
        }
        return z;
    }

    public void a(D d2) {
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.f36b == null && this.f35a != null) {
            if (this.f35a.f45b) {
                this.f35a.f45b = false;
                this.e.removeCallbacks(this.f35a);
            }
            if (this.f37c <= 0 || SystemClock.uptimeMillis() >= this.d + this.f37c) {
                this.f35a.a(v.d, null);
                return;
            }
            this.f35a.f45b = true;
            this.e.postAtTime(this.f35a, this.d + this.f37c);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a<D>.b bVar, D d2) {
        a((Object) d2);
        if (this.f36b == bVar) {
            this.d = SystemClock.uptimeMillis();
            this.f36b = null;
            c();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(a<D>.b bVar, D d2) {
        if (this.f35a != bVar) {
            a(bVar, d2);
        } else if (m()) {
            a((Object) d2);
        } else {
            this.d = SystemClock.uptimeMillis();
            this.f35a = null;
            b(d2);
        }
    }

    /* access modifiers changed from: protected */
    public D e() {
        return d();
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.a(str, fileDescriptor, printWriter, strArr);
        if (this.f35a != null) {
            printWriter.print(str);
            printWriter.print("mTask=");
            printWriter.print(this.f35a);
            printWriter.print(" waiting=");
            printWriter.println(this.f35a.f45b);
        }
        if (this.f36b != null) {
            printWriter.print(str);
            printWriter.print("mCancellingTask=");
            printWriter.print(this.f36b);
            printWriter.print(" waiting=");
            printWriter.println(this.f36b.f45b);
        }
        if (this.f37c != 0) {
            printWriter.print(str);
            printWriter.print("mUpdateThrottle=");
            d.a(this.f37c, printWriter);
            printWriter.print(" mLastLoadCompleteTime=");
            d.a(this.d, SystemClock.uptimeMillis(), printWriter);
            printWriter.println();
        }
    }
}
