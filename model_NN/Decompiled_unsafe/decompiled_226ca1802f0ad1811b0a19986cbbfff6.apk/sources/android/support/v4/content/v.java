package android.support.v4.content;

import android.support.v4.app.NotificationCompat;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: ModernAsyncTask */
abstract class v<Params, Progress, Result> {

    /* renamed from: a  reason: collision with root package name */
    private static final ThreadFactory f60a = new w();

    /* renamed from: b  reason: collision with root package name */
    private static final BlockingQueue<Runnable> f61b = new LinkedBlockingQueue(10);

    /* renamed from: c  reason: collision with root package name */
    private static final ab f62c = new ab(null);
    public static final Executor d = new ThreadPoolExecutor(5, (int) NotificationCompat.FLAG_HIGH_PRIORITY, 1, TimeUnit.SECONDS, f61b, f60a);
    private static volatile Executor e = d;
    private final ad<Params, Result> f = new x(this);
    private final FutureTask<Result> g = new y(this, this.f);
    private volatile ac h = ac.PENDING;
    /* access modifiers changed from: private */
    public final AtomicBoolean i = new AtomicBoolean();

    /* access modifiers changed from: protected */
    public abstract Result a(Params... paramsArr);

    /* access modifiers changed from: private */
    public void c(Result result) {
        if (!this.i.get()) {
            d(result);
        }
    }

    /* access modifiers changed from: private */
    public Result d(Result result) {
        f62c.obtainMessage(1, new aa(this, result)).sendToTarget();
        return result;
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    /* access modifiers changed from: protected */
    public void a(Result result) {
    }

    /* access modifiers changed from: protected */
    public void b(Progress... progressArr) {
    }

    /* access modifiers changed from: protected */
    public void b(Result result) {
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public final boolean c() {
        return this.g.isCancelled();
    }

    public final boolean a(boolean z) {
        return this.g.cancel(z);
    }

    public final v<Params, Progress, Result> a(Executor executor, Params... paramsArr) {
        if (this.h != ac.PENDING) {
            switch (z.f66a[this.h.ordinal()]) {
                case 1:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                case 2:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.h = ac.RUNNING;
        b();
        this.f.f43b = paramsArr;
        executor.execute(this.g);
        return this;
    }

    /* access modifiers changed from: private */
    public void e(Result result) {
        if (c()) {
            b(result);
        } else {
            a((Object) result);
        }
        this.h = ac.FINISHED;
    }
}
