package android.support.v4.a;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

/* compiled from: ConnectivityManagerCompat */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final c f21a;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f21a = new f();
        } else if (Build.VERSION.SDK_INT >= 13) {
            f21a = new e();
        } else if (Build.VERSION.SDK_INT >= 8) {
            f21a = new d();
        } else {
            f21a = new b();
        }
    }

    public static NetworkInfo a(ConnectivityManager connectivityManager, Intent intent) {
        return connectivityManager.getNetworkInfo(((NetworkInfo) intent.getParcelableExtra("networkInfo")).getType());
    }
}
