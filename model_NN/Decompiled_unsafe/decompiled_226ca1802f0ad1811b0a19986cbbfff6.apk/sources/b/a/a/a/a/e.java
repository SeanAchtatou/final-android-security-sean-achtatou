package b.a.a.a.a;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

/* compiled from: DefaultAnimationsBuilder */
final class e {

    /* renamed from: a  reason: collision with root package name */
    private static Animation f161a;

    /* renamed from: b  reason: collision with root package name */
    private static Animation f162b;

    /* renamed from: c  reason: collision with root package name */
    private static int f163c;
    private static int d;

    static Animation a(View view) {
        if (!c(view) || f161a == null) {
            f161a = new TranslateAnimation(0.0f, 0.0f, (float) (-view.getMeasuredHeight()), 0.0f);
            f161a.setDuration(400);
            a(view.getMeasuredHeight());
        }
        return f161a;
    }

    static Animation b(View view) {
        if (!d(view) || f162b == null) {
            f162b = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-view.getMeasuredHeight()));
            f162b.setDuration(400);
            b(view.getMeasuredHeight());
        }
        return f162b;
    }

    private static boolean c(View view) {
        return a(f163c, view);
    }

    private static boolean d(View view) {
        return a(d, view);
    }

    private static boolean a(int i, View view) {
        return i == view.getMeasuredHeight();
    }

    private static void a(int i) {
        f163c = i;
    }

    private static void b(int i) {
        d = i;
    }
}
