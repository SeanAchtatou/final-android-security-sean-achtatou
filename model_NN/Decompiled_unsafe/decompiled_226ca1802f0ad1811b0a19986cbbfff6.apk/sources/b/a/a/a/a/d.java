package b.a.a.a.a;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/* compiled from: Crouton */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final CharSequence f158a;

    /* renamed from: b  reason: collision with root package name */
    private final i f159b;

    /* renamed from: c  reason: collision with root package name */
    private a f160c = null;
    private final View d;
    private View.OnClickListener e;
    private Activity f;
    private ViewGroup g;
    private FrameLayout h;
    private Animation i;
    private Animation j;
    private f k = null;

    private d(Activity activity, CharSequence charSequence, i iVar) {
        if (activity == null || charSequence == null || iVar == null) {
            throw new IllegalArgumentException("Null parameters are not accepted");
        }
        this.f = activity;
        this.g = null;
        this.f158a = charSequence;
        this.f159b = iVar;
        this.d = null;
    }

    private d(Activity activity, CharSequence charSequence, i iVar, ViewGroup viewGroup) {
        if (activity == null || charSequence == null || iVar == null) {
            throw new IllegalArgumentException("Null parameters are not accepted");
        }
        this.f = activity;
        this.f158a = charSequence;
        this.f159b = iVar;
        this.g = viewGroup;
        this.d = null;
    }

    public static d a(Activity activity, CharSequence charSequence, i iVar) {
        return new d(activity, charSequence, iVar);
    }

    public static d a(Activity activity, CharSequence charSequence, i iVar, ViewGroup viewGroup) {
        return new d(activity, charSequence, iVar, viewGroup);
    }

    public static void a(Activity activity, CharSequence charSequence, i iVar, int i2, a aVar) {
        a(activity, charSequence, iVar, (ViewGroup) activity.findViewById(i2)).a(aVar).b();
    }

    public static void a() {
        g.a().b();
    }

    public void b() {
        g.a().a(this);
    }

    public Animation c() {
        if (this.i == null && this.f != null) {
            if (k().f154c > 0) {
                this.i = AnimationUtils.loadAnimation(l(), k().f154c);
            } else {
                r();
                this.i = e.a(o());
            }
        }
        return this.i;
    }

    public Animation d() {
        if (this.j == null && this.f != null) {
            if (k().d > 0) {
                this.j = AnimationUtils.loadAnimation(l(), k().d);
            } else {
                this.j = e.b(o());
            }
        }
        return this.j;
    }

    public d a(a aVar) {
        this.f160c = aVar;
        return this;
    }

    public String toString() {
        return "Crouton{text=" + ((Object) this.f158a) + ", style=" + this.f159b + ", configuration=" + this.f160c + ", customView=" + this.d + ", onClickListener=" + this.e + ", activity=" + this.f + ", viewGroup=" + this.g + ", croutonView=" + this.h + ", inAnimation=" + this.i + ", outAnimation=" + this.j + ", lifecycleCallback=" + this.k + '}';
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.f != null && (p() || q());
    }

    private boolean p() {
        return (this.h == null || this.h.getParent() == null) ? false : true;
    }

    private boolean q() {
        return (this.d == null || this.d.getParent() == null) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.f = null;
    }

    /* access modifiers changed from: package-private */
    public void g() {
        this.g = null;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        this.k = null;
    }

    /* access modifiers changed from: package-private */
    public f i() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public i j() {
        return this.f159b;
    }

    /* access modifiers changed from: package-private */
    public a k() {
        if (this.f160c == null) {
            this.f160c = j().d;
        }
        return this.f160c;
    }

    /* access modifiers changed from: package-private */
    public Activity l() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public ViewGroup m() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public CharSequence n() {
        return this.f158a;
    }

    /* access modifiers changed from: package-private */
    public View o() {
        if (this.d != null) {
            return this.d;
        }
        if (this.h == null) {
            s();
        }
        return this.h;
    }

    private void r() {
        int makeMeasureSpec;
        View o = o();
        if (this.g != null) {
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.g.getMeasuredWidth(), Integer.MIN_VALUE);
        } else {
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.f.getWindow().getDecorView().getMeasuredWidth(), Integer.MIN_VALUE);
        }
        o.measure(makeMeasureSpec, View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    private void s() {
        Resources resources = this.f.getResources();
        this.h = a(resources);
        this.h.addView(b(resources));
    }

    private FrameLayout a(Resources resources) {
        int i2;
        int i3;
        FrameLayout frameLayout = new FrameLayout(this.f);
        if (this.e != null) {
            frameLayout.setOnClickListener(this.e);
        }
        if (this.f159b.k > 0) {
            i2 = resources.getDimensionPixelSize(this.f159b.k);
        } else {
            i2 = this.f159b.j;
        }
        if (this.f159b.m > 0) {
            i3 = resources.getDimensionPixelSize(this.f159b.m);
        } else {
            i3 = this.f159b.l;
        }
        if (i3 == 0) {
            i3 = -1;
        }
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(i3, i2));
        if (this.f159b.g != -1) {
            frameLayout.setBackgroundColor(this.f159b.g);
        } else {
            frameLayout.setBackgroundColor(resources.getColor(this.f159b.e));
        }
        if (this.f159b.f != 0) {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(resources, BitmapFactory.decodeResource(resources, this.f159b.f));
            if (this.f159b.h) {
                bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            }
            frameLayout.setBackgroundDrawable(bitmapDrawable);
        }
        return frameLayout;
    }

    private RelativeLayout b(Resources resources) {
        RelativeLayout relativeLayout = new RelativeLayout(this.f);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        int i2 = this.f159b.x;
        if (this.f159b.y > 0) {
            i2 = resources.getDimensionPixelSize(this.f159b.y);
        }
        relativeLayout.setPadding(i2, i2, i2, i2);
        ImageView imageView = null;
        if (!(this.f159b.o == null && this.f159b.p == 0)) {
            imageView = t();
            relativeLayout.addView(imageView, imageView.getLayoutParams());
        }
        TextView c2 = c(resources);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        if (imageView != null) {
            layoutParams.addRule(1, imageView.getId());
        }
        relativeLayout.addView(c2, layoutParams);
        return relativeLayout;
    }

    private TextView c(Resources resources) {
        TextView textView = new TextView(this.f);
        textView.setId(257);
        textView.setText(this.f158a);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setGravity(this.f159b.n);
        if (this.f159b.i != 0) {
            textView.setTextColor(resources.getColor(this.f159b.i));
        }
        if (this.f159b.r != 0) {
            textView.setTextSize(2, (float) this.f159b.r);
        }
        if (this.f159b.s != 0) {
            a(resources, textView);
        }
        if (this.f159b.w != 0) {
            textView.setTextAppearance(this.f, this.f159b.w);
        }
        return textView;
    }

    private void a(Resources resources, TextView textView) {
        textView.setShadowLayer(this.f159b.t, this.f159b.v, this.f159b.u, resources.getColor(this.f159b.s));
    }

    private ImageView t() {
        ImageView imageView = new ImageView(this.f);
        imageView.setId(256);
        imageView.setAdjustViewBounds(true);
        imageView.setScaleType(this.f159b.q);
        if (this.f159b.o != null) {
            imageView.setImageDrawable(this.f159b.o);
        }
        if (this.f159b.p != 0) {
            imageView.setImageResource(this.f159b.p);
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(9, -1);
        layoutParams.addRule(15, -1);
        imageView.setLayoutParams(layoutParams);
        return imageView;
    }
}
