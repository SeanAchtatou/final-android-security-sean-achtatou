package b.a.a.a.a;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.FrameLayout;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/* compiled from: Manager */
final class g extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private static g f164a;

    /* renamed from: b  reason: collision with root package name */
    private Queue<d> f165b = new LinkedBlockingQueue();

    private g() {
    }

    static synchronized g a() {
        g gVar;
        synchronized (g.class) {
            if (f164a == null) {
                f164a = new g();
            }
            gVar = f164a;
        }
        return gVar;
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        this.f165b.add(dVar);
        c();
    }

    private void c() {
        if (!this.f165b.isEmpty()) {
            d peek = this.f165b.peek();
            if (peek.l() == null) {
                this.f165b.poll();
            }
            if (!peek.e()) {
                a(peek, -1040157475);
                if (peek.i() != null) {
                    peek.i().a();
                    return;
                }
                return;
            }
            a(peek, 794631, c(peek));
        }
    }

    private long c(d dVar) {
        return ((long) dVar.k().f153b) + dVar.c().getDuration() + dVar.d().getDuration();
    }

    private void a(d dVar, int i) {
        Message obtainMessage = obtainMessage(i);
        obtainMessage.obj = dVar;
        sendMessage(obtainMessage);
    }

    /* access modifiers changed from: private */
    public void a(d dVar, int i, long j) {
        Message obtainMessage = obtainMessage(i);
        obtainMessage.obj = dVar;
        sendMessageDelayed(obtainMessage, j);
    }

    public void handleMessage(Message message) {
        d dVar = (d) message.obj;
        switch (message.what) {
            case -1040157475:
                d(dVar);
                return;
            case -1040155167:
                b(dVar);
                if (dVar.i() != null) {
                    dVar.i().b();
                    return;
                }
                return;
            case 794631:
                c();
                return;
            default:
                super.handleMessage(message);
                return;
        }
    }

    private void d(d dVar) {
        if (!dVar.e()) {
            View o = dVar.o();
            if (o.getParent() == null) {
                ViewGroup.LayoutParams layoutParams = o.getLayoutParams();
                if (layoutParams == null) {
                    layoutParams = new ViewGroup.LayoutParams(-1, -2);
                }
                if (dVar.m() == null) {
                    Activity l = dVar.l();
                    if (l != null && !l.isFinishing()) {
                        l.addContentView(o, layoutParams);
                    } else {
                        return;
                    }
                } else if (dVar.m() instanceof FrameLayout) {
                    dVar.m().addView(o, layoutParams);
                } else {
                    dVar.m().addView(o, 0, layoutParams);
                }
            }
            o.requestLayout();
            o.getViewTreeObserver().addOnGlobalLayoutListener(new h(this, o, dVar));
        }
    }

    /* access modifiers changed from: protected */
    public void b(d dVar) {
        View o = dVar.o();
        ViewGroup viewGroup = (ViewGroup) o.getParent();
        if (viewGroup != null) {
            o.startAnimation(dVar.d());
            d poll = this.f165b.poll();
            viewGroup.removeView(o);
            if (poll != null) {
                poll.f();
                poll.g();
                if (poll.i() != null) {
                    poll.i().b();
                }
                poll.h();
            }
            a(dVar, 794631, dVar.d().getDuration());
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        d();
        if (this.f165b != null) {
            for (d next : this.f165b) {
                if (next.e()) {
                    ((ViewGroup) next.o().getParent()).removeView(next.o());
                }
            }
            this.f165b.clear();
        }
    }

    private void d() {
        removeMessages(-1040157475);
        removeMessages(794631);
        removeMessages(-1040155167);
    }

    public static void a(Context context, CharSequence charSequence) {
        int i;
        if (Build.VERSION.SDK_INT >= 4) {
            AccessibilityManager accessibilityManager = (AccessibilityManager) context.getSystemService("accessibility");
            if (accessibilityManager.isEnabled()) {
                if (Build.VERSION.SDK_INT < 16) {
                    i = 8;
                } else {
                    i = 16384;
                }
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
                obtain.getText().add(charSequence);
                obtain.setClassName(g.class.getName());
                obtain.setPackageName(context.getPackageName());
                accessibilityManager.sendAccessibilityEvent(obtain);
            }
        }
    }

    public String toString() {
        return "Manager{croutonQueue=" + this.f165b + '}';
    }
}
