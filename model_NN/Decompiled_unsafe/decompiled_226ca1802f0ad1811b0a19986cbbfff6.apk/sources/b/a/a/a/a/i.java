package b.a.a.a.a;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/* compiled from: Style */
public class i {

    /* renamed from: a  reason: collision with root package name */
    public static final i f169a = new k().b(-48060).a();

    /* renamed from: b  reason: collision with root package name */
    public static final i f170b = new k().b(-6697984).a();

    /* renamed from: c  reason: collision with root package name */
    public static final i f171c = new k().b(-13388315).a();
    final a d;
    final int e;
    final int f;
    final int g;
    final boolean h;
    final int i;
    final int j;
    final int k;
    final int l;
    final int m;
    final int n;
    final Drawable o;
    final int p;
    final ImageView.ScaleType q;
    final int r;
    final int s;
    final float t;
    final float u;
    final float v;
    final int w;
    final int x;
    final int y;

    private i(k kVar) {
        this.d = kVar.f172a;
        this.e = kVar.f174c;
        this.f = kVar.d;
        this.h = kVar.e;
        this.i = kVar.f;
        this.j = kVar.g;
        this.k = kVar.h;
        this.l = kVar.i;
        this.m = kVar.j;
        this.n = kVar.k;
        this.o = kVar.l;
        this.r = kVar.m;
        this.s = kVar.n;
        this.t = kVar.o;
        this.v = kVar.p;
        this.u = kVar.q;
        this.w = kVar.r;
        this.p = kVar.s;
        this.q = kVar.t;
        this.x = kVar.u;
        this.y = kVar.v;
        this.g = kVar.f173b;
    }

    public String toString() {
        return "Style{configuration=" + this.d + ", backgroundColorResourceId=" + this.e + ", backgroundDrawableResourceId=" + this.f + ", backgroundColorValue=" + this.g + ", isTileEnabled=" + this.h + ", textColorResourceId=" + this.i + ", heightInPixels=" + this.j + ", heightDimensionResId=" + this.k + ", widthInPixels=" + this.l + ", widthDimensionResId=" + this.m + ", gravity=" + this.n + ", imageDrawable=" + this.o + ", imageResId=" + this.p + ", imageScaleType=" + this.q + ", textSize=" + this.r + ", textShadowColorResId=" + this.s + ", textShadowRadius=" + this.t + ", textShadowDy=" + this.u + ", textShadowDx=" + this.v + ", textAppearanceResId=" + this.w + ", paddingInPixels=" + this.x + ", paddingDimensionResId=" + this.y + '}';
    }
}
