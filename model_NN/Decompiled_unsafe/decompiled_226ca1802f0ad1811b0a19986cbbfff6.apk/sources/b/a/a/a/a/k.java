package b.a.a.a.a;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/* compiled from: Style */
public class k {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public a f172a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f173b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public int f174c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public Drawable l;
    /* access modifiers changed from: private */
    public int m;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public float o;
    /* access modifiers changed from: private */
    public float p;
    /* access modifiers changed from: private */
    public float q;
    /* access modifiers changed from: private */
    public int r;
    /* access modifiers changed from: private */
    public int s;
    /* access modifiers changed from: private */
    public ImageView.ScaleType t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v;

    public k() {
        this.f172a = a.f152a;
        this.u = 10;
        this.f174c = 17170450;
        this.d = 0;
        this.f173b = -1;
        this.e = false;
        this.f = 17170443;
        this.g = -2;
        this.i = -1;
        this.k = 17;
        this.l = null;
        this.s = 0;
        this.t = ImageView.ScaleType.FIT_XY;
    }

    public k(i iVar) {
        this.f172a = iVar.d;
        this.f173b = iVar.g;
        this.f174c = iVar.e;
        this.d = iVar.f;
        this.e = iVar.h;
        this.f = iVar.i;
        this.g = iVar.j;
        this.h = iVar.k;
        this.i = iVar.l;
        this.j = iVar.m;
        this.k = iVar.n;
        this.l = iVar.o;
        this.m = iVar.r;
        this.n = iVar.s;
        this.o = iVar.t;
        this.p = iVar.v;
        this.q = iVar.u;
        this.r = iVar.w;
        this.s = iVar.p;
        this.t = iVar.q;
        this.u = iVar.x;
        this.v = iVar.y;
    }

    public k a(int i2) {
        this.f174c = i2;
        return this;
    }

    public k b(int i2) {
        this.f173b = i2;
        return this;
    }

    public i a() {
        return new i(this);
    }
}
