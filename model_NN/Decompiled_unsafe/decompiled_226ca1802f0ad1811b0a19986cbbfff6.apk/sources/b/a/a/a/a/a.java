package b.a.a.a.a;

/* compiled from: Configuration */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final a f152a = new c().a(3000).a();

    /* renamed from: b  reason: collision with root package name */
    final int f153b;

    /* renamed from: c  reason: collision with root package name */
    final int f154c;
    final int d;

    private a(c cVar) {
        this.f153b = cVar.f155a;
        this.f154c = cVar.f156b;
        this.d = cVar.f157c;
    }

    public String toString() {
        return "Configuration{durationInMilliseconds=" + this.f153b + ", inAnimationResId=" + this.f154c + ", outAnimationResId=" + this.d + '}';
    }
}
