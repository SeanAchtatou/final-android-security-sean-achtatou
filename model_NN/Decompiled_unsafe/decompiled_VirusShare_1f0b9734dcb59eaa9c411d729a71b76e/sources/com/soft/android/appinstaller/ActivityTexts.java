package com.soft.android.appinstaller;

public class ActivityTexts {
    boolean isError;
    String text;
    String title;

    public boolean isError() {
        return this.isError;
    }

    public void setError(boolean isError2) {
        this.isError = isError2;
    }

    public ActivityTexts(String title2, String text2, boolean isErr) {
        setTitle(title2);
        setText(text2);
        setError(isErr);
    }

    public ActivityTexts(String title2, String text2) {
        setTitle(title2);
        setText(text2);
        setError(false);
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }
}
