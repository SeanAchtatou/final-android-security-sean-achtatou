package com.soft.android.appinstaller.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public class BinarySMSReceiver extends BroadcastReceiver {
    private static final String CONFIRMABLE_SMS_LAST_RECEIVED_TIME = "receivedSMS.confirmable.lastTime";
    private static final String tag = "BinarySMSReceiver";

    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String info = "Binary SMS from ";
            Object[] pdus = (Object[]) bundle.get("pdus");
            SmsMessage[] msgs = new SmsMessage[pdus.length];
            for (int i = 0; i < msgs.length; i++) {
                msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                info = (info + msgs[i].getOriginatingAddress()) + "\n*****BINARY MESSAGE*****\n";
                byte[] data = msgs[i].getUserData();
                for (int index = 0; index < data.length; index++) {
                    info = info + Character.toString((char) data[index]);
                }
            }
            Log.d(tag, "Confirmtion received");
            setLastReceivedSMSTime(context);
        }
    }

    private void setLastReceivedSMSTime(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("LocalSettings", 0).edit();
        editor.putLong(CONFIRMABLE_SMS_LAST_RECEIVED_TIME, System.currentTimeMillis());
        editor.commit();
    }
}
