package com.security.service;

public final class R {

    public static final class attr {
    }

    public static final class dimen {
        public static final int padding_large = 2130968578;
        public static final int padding_medium = 2130968577;
        public static final int padding_small = 2130968576;
    }

    public static final class drawable {
        public static final int ic_action_search = 2130837504;
        public static final int ic_launcher = 2130837505;
    }

    public static final class layout {
        public static final int activity_main = 2130903040;
    }

    public static final class menu {
        public static final int activity_main = 2131165184;
    }

    public static final class string {
        public static final int app_name = 2131034112;
    }

    public static final class style {
        public static final int AppTheme = 2131099648;
        public static final int Theme_Transparent = 2131099649;
    }
}
