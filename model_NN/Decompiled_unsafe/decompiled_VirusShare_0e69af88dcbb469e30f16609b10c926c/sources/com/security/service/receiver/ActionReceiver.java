package com.security.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.security.service.MainActivity;

public class ActionReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction()) || "android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            MainActivity.showActivityAndSendInit(context);
        }
    }
}
