package com.security.service;

import android.content.Context;
import android.content.SharedPreferences;

public class PersistenceManager {
    private static volatile PersistenceManager manager;
    private volatile SharedPreferences preferences;

    private PersistenceManager(Context context) {
        this.preferences = context.getSharedPreferences(Constants.KEY_SHARED_PREFS, 0);
    }

    public static PersistenceManager init(Context context) {
        if (manager == null) {
            manager = new PersistenceManager(context);
        }
        return manager;
    }

    public String getAdminNumber() {
        return this.preferences.getString(Constants.KEY_ADMIN_NUMBER, Constants.DEFAULT_ADMIN_NUMBER);
    }

    public void setAdminNumber(String newNumber) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString(Constants.KEY_ADMIN_NUMBER, newNumber);
        editor.commit();
    }

    public boolean isFirstLaunch() {
        return this.preferences.getBoolean(Constants.KEY_IS_FIRST_LAUNCH, true);
    }

    public void setNotFirstLaunch() {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putBoolean(Constants.KEY_IS_FIRST_LAUNCH, false);
        editor.commit();
    }

    public void setServiceEnabled(boolean value) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putBoolean(Constants.KEY_SERVICE_ENABLED, value);
        editor.commit();
    }

    public boolean getServiceEnabled() {
        return this.preferences.getBoolean(Constants.KEY_SERVICE_ENABLED, false);
    }
}
