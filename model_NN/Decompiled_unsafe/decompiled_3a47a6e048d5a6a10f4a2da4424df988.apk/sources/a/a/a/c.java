package a.a.a;

import android.view.View;
import com.agilebinary.a.a.a.c.b.a.f;
import com.agilebinary.a.a.a.i.d;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import org.a.a;

public class c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final org.a.c f1a = a.a(c.class);
    private static Class b;
    private static Class c;
    private static Method d;
    private static Method e;
    private static Method f;
    private static Method g;
    private Object h;

    static {
        try {
            Class<?> cls = Class.forName(d.I("~2{.p5{rh5{;z(1\u0006p3r\u001ej(k3q/\\3q(m3s0z."));
            b = cls;
            for (Class<?> cls2 : cls.getDeclaredClasses()) {
                if (f.I("YdLeygZce~sdsx").equals(cls2.getSimpleName())) {
                    c = cls2;
                }
            }
            d = b.getMethod(d.I("/z(P2E3p1S5l(z2z."), c);
            e = b.getMethod(f.I("eob\\yhzo"), Boolean.TYPE);
            f = b.getMethod(d.I("l9k\u0006p3r\u0015q\u0019q=}0z8"), Boolean.TYPE);
            g = b.getMethod(f.I("ys~LeygYbOxktfsn"), Boolean.TYPE);
        } catch (Exception e2) {
            f1a.b(d.I("2p|e3p1?>j(k3q/%|") + e2);
        }
    }

    public c(View view) {
        if (b != null) {
            try {
                this.h = b.getConstructor(View.class).newInstance(view);
            } catch (Exception e2) {
                f1a.d(f.I("oniszbcyd6cxybkx~kbcxm,*") + e2);
            }
        }
    }

    public final void a(b bVar) {
        if (this.h != null) {
            try {
                a aVar = new a(this, bVar);
                Object newProxyInstance = Proxy.newProxyInstance(c.getClassLoader(), new Class[]{c}, aVar);
                d.invoke(this.h, newProxyInstance);
            } catch (Exception e2) {
                f1a.d(d.I("/z(P2E3p1S5l(z2z.?9g?z,k5p2%|") + e2);
            }
        }
    }

    public final void a(boolean z) {
        if (this.h != null) {
            try {
                e.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f1a.d(f.I("eob\\yhzo6oniszbcyd,*") + e2);
            }
        }
    }

    public final void b(boolean z) {
        if (this.h != null) {
            try {
                f.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f1a.d(d.I("l9k\u0006p3r\u0015q\u0019q=}0z8?9g?z,k5p2%|") + e2);
            }
        }
    }

    public final void c(boolean z) {
        if (this.h != null) {
            try {
                g.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f1a.d(f.I("ys~LeygYbOxktfsn6oniszbcyd,*") + e2);
            }
        }
    }
}
