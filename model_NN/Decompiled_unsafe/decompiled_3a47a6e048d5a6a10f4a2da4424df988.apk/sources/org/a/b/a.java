package org.a.b;

import android.util.Log;
import com.agilebinary.mobilemonitor.client.android.ui.a.e;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.a.b;

public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    private final Map f316a = new HashMap();

    /* access modifiers changed from: private */
    /* renamed from: b */
    public /* synthetic */ c a(String str) {
        String str2;
        c cVar;
        String str3;
        if (str == null || str.length() <= 23) {
            str2 = str;
        } else {
            StringTokenizer stringTokenizer = new StringTokenizer(str, e.I("#"));
            if (stringTokenizer.hasMoreTokens()) {
                StringBuilder sb = new StringBuilder();
                do {
                    String nextToken = stringTokenizer.nextToken();
                    if (nextToken.length() == 1) {
                        sb.append(nextToken);
                        sb.append('.');
                    } else if (stringTokenizer.hasMoreTokens()) {
                        sb.append(nextToken.charAt(0));
                        sb.append(org.osmdroid.b.b.a.I("Y6"));
                    } else {
                        sb.append(nextToken);
                    }
                } while (stringTokenizer.hasMoreTokens());
                str3 = sb.toString();
            } else {
                str3 = str;
            }
            str2 = str3.length() > 23 ? str3.substring(0, 22) + '*' : str3;
        }
        synchronized (this) {
            cVar = (c) this.f316a.get(str2);
            if (cVar == null) {
                if (!str2.equals(str)) {
                    Log.i(a.class.getSimpleName(), org.osmdroid.b.b.a.I("T\u001c\u0014}\u00018\u001dy\u001e}S?") + str + e.I("M-\u000fu\th\u000fi\u0019-\u0007l\u0012d\u0007x\u0007-\u0006h\u0004j\u001eeJb\f-") + 23 + org.osmdroid.b.b.a.I("S{\u001by\u0001y\u0010l\u0016j\u00004Sm\u0000q\u001dS?") + str2 + e.I("M-\u0003c\u0019y\u000fl\u000e#"));
                }
                cVar = new c(str2);
                this.f316a.put(str2, cVar);
            }
        }
        return cVar;
    }
}
