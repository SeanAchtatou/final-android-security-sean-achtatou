package org.a.a;

import com.agilebinary.mobilemonitor.client.android.c.e;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f313a = new b(null);
    private String b;
    private Throwable c;
    private Object[] d;

    public b(String str) {
        this(str, null, null);
    }

    public b(String str, Object[] objArr, Throwable th) {
        this.b = str;
        this.c = th;
        if (th == null) {
            this.d = objArr;
        } else if (objArr == null || objArr.length == 0) {
            throw new IllegalStateException(e.I("\u001dY\u001d\u001b\u0000S\u001dE\u001aU\u0012ZSS\u001eF\u0007OSY\u0001\u0016\u001dC\u001fZSW\u0001Q\u0006[\u0016X\u0007\u0016\u0012D\u0001W\n"));
        } else {
            int length = objArr.length - 1;
            Object[] objArr2 = new Object[length];
            System.arraycopy(objArr, 0, objArr2, 0, length);
            this.d = objArr2;
        }
    }

    public final String a() {
        return this.b;
    }
}
