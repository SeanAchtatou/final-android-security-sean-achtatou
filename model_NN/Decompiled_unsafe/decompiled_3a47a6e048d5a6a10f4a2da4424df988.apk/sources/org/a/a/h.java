package org.a.a;

import com.agilebinary.a.a.a.c.c.e;
import com.agilebinary.mobilemonitor.client.android.b.g;
import java.util.HashMap;
import java.util.Map;

public final class h {
    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ ',');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'w');
            i2 = i;
        }
        return new String(cArr);
    }

    public static final b a(String str, Object[] objArr) {
        Throwable th;
        if (objArr == null || objArr.length == 0) {
            th = null;
        } else {
            Object obj = objArr[objArr.length - 1];
            th = obj instanceof Throwable ? (Throwable) obj : null;
        }
        if (str == null) {
            return new b(null, objArr, th);
        }
        if (objArr == null) {
            return new b(str);
        }
        StringBuffer stringBuffer = new StringBuffer(str.length() + 50);
        int i = 0;
        int i2 = 0;
        while (i < objArr.length) {
            int indexOf = str.indexOf(e.I("Z\u0015"), i2);
            if (indexOf != -1) {
                if (indexOf != 0 && str.charAt(indexOf + -1) == '\\') {
                    if (!(indexOf >= 2 && str.charAt(indexOf + -2) == '\\')) {
                        i--;
                        stringBuffer.append(str.substring(i2, indexOf - 1));
                        stringBuffer.append('{');
                        i2 = indexOf + 1;
                    } else {
                        stringBuffer.append(str.substring(i2, indexOf - 1));
                        a(stringBuffer, objArr[i], new HashMap());
                        i2 = indexOf + 2;
                    }
                } else {
                    stringBuffer.append(str.substring(i2, indexOf));
                    a(stringBuffer, objArr[i], new HashMap());
                    i2 = indexOf + 2;
                }
                i++;
            } else if (i2 == 0) {
                return new b(str, objArr, th);
            } else {
                stringBuffer.append(str.substring(i2, str.length()));
                return new b(stringBuffer.toString(), objArr, th);
            }
        }
        stringBuffer.append(str.substring(i2, str.length()));
        return i < objArr.length + -1 ? new b(stringBuffer.toString(), objArr, th) : new b(stringBuffer.toString(), objArr, null);
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, Object obj, Map map) {
        if (obj == null) {
            stringBuffer.append(g.I("M\u0007O\u001e"));
        } else if (!obj.getClass().isArray()) {
            try {
                stringBuffer.append(obj.toString());
            } catch (Throwable th) {
                System.err.println(e.I("r$g\\kR\u0001.@\u0001M\rEHU\u0007r\u001cS\u0001O\u000f\tA\u0001\u0001O\u001eN\u000b@\u001cH\u0007OHN\u0006\u0001\tOHN\nK\rB\u001c\u0001\u0007GHU\u0011Q\r\u00013") + obj.getClass().getName() + g.I("/"));
                th.printStackTrace();
                stringBuffer.append(e.I("3g)h$d,\u0001\u001cN;U\u001aH\u0006F@\b5"));
            }
        } else if (obj instanceof boolean[]) {
            a(stringBuffer, (boolean[]) obj);
        } else if (obj instanceof byte[]) {
            a(stringBuffer, (byte[]) obj);
        } else if (obj instanceof char[]) {
            a(stringBuffer, (char[]) obj);
        } else if (obj instanceof short[]) {
            a(stringBuffer, (short[]) obj);
        } else if (obj instanceof int[]) {
            a(stringBuffer, (int[]) obj);
        } else if (obj instanceof long[]) {
            a(stringBuffer, (long[]) obj);
        } else if (obj instanceof float[]) {
            a(stringBuffer, (float[]) obj);
        } else if (obj instanceof double[]) {
            a(stringBuffer, (double[]) obj);
        } else {
            a(stringBuffer, (Object[]) obj, map);
        }
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, byte[] bArr) {
        stringBuffer.append('[');
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append((int) bArr[i]);
            if (i != length - 1) {
                stringBuffer.append(g.I("\u000fR"));
            }
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, char[] cArr) {
        stringBuffer.append('[');
        int length = cArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(cArr[i]);
            if (i != length - 1) {
                stringBuffer.append(e.I("\rH"));
            }
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, double[] dArr) {
        stringBuffer.append('[');
        int length = dArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(dArr[i]);
            if (i != length - 1) {
                stringBuffer.append(g.I("\u000fR"));
            }
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, float[] fArr) {
        stringBuffer.append('[');
        int length = fArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(fArr[i]);
            if (i != length - 1) {
                stringBuffer.append(e.I("\rH"));
            }
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, int[] iArr) {
        stringBuffer.append('[');
        int length = iArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(iArr[i]);
            if (i != length - 1) {
                stringBuffer.append(g.I("\u000fR"));
            }
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, long[] jArr) {
        stringBuffer.append('[');
        int length = jArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(jArr[i]);
            if (i != length - 1) {
                stringBuffer.append(e.I("\rH"));
            }
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, Object[] objArr, Map map) {
        stringBuffer.append('[');
        if (!map.containsKey(objArr)) {
            map.put(objArr, null);
            int length = objArr.length;
            for (int i = 0; i < length; i++) {
                a(stringBuffer, objArr[i], map);
                if (i != length - 1) {
                    stringBuffer.append(g.I("\u000fR"));
                }
            }
            map.remove(objArr);
        } else {
            stringBuffer.append(e.I("F\u000fF"));
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, short[] sArr) {
        stringBuffer.append('[');
        int length = sArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append((int) sArr[i]);
            if (i != length - 1) {
                stringBuffer.append(g.I("\u000fR"));
            }
        }
        stringBuffer.append(']');
    }

    private static /* synthetic */ void a(StringBuffer stringBuffer, boolean[] zArr) {
        stringBuffer.append('[');
        int length = zArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(zArr[i]);
            if (i != length - 1) {
                stringBuffer.append(e.I("\rH"));
            }
        }
        stringBuffer.append(']');
    }
}
