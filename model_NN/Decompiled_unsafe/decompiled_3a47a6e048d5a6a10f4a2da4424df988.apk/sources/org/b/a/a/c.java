package org.b.a.a;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private int f319a;
    private float[] b = new float[20];
    private float[] c = new float[20];
    private float[] d = new float[20];
    private int[] e = new int[20];
    private float f;
    private float g;
    private float h;
    private float i;
    private float j;
    private float k;
    private float l;
    private float m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private int s;
    /* access modifiers changed from: private */
    public long t;

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i2 = length - 1;
        int i3 = i2;
        while (i2 >= 0) {
            int i4 = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'O');
            if (i4 < 0) {
                break;
            }
            i2 = i4 - 1;
            cArr[i4] = (char) (str.charAt(i4) ^ ',');
            i3 = i2;
        }
        return new String(cArr);
    }

    static /* synthetic */ void a(c cVar, int i2, float[] fArr, float[] fArr2, float[] fArr3, int[] iArr, int i3, boolean z, long j2) {
        cVar.t = j2;
        cVar.s = i3;
        cVar.f319a = i2;
        for (int i4 = 0; i4 < i2; i4++) {
            cVar.b[i4] = fArr[i4];
            cVar.c[i4] = fArr2[i4];
            cVar.d[i4] = fArr3[i4];
            cVar.e[i4] = iArr[i4];
        }
        cVar.n = z;
        cVar.o = i2 >= 2;
        if (cVar.o) {
            cVar.f = (fArr[0] + fArr[1]) * 0.5f;
            cVar.g = (fArr2[0] + fArr2[1]) * 0.5f;
            cVar.h = (fArr3[0] + fArr3[1]) * 0.5f;
            cVar.i = Math.abs(fArr[1] - fArr[0]);
            cVar.j = Math.abs(fArr2[1] - fArr2[0]);
        } else {
            cVar.f = fArr[0];
            cVar.g = fArr2[0];
            cVar.h = fArr3[0];
            cVar.j = 0.0f;
            cVar.i = 0.0f;
        }
        cVar.r = false;
        cVar.q = false;
        cVar.p = false;
    }

    public final boolean a() {
        return this.o;
    }

    public final float b() {
        if (this.o) {
            return this.i;
        }
        return 0.0f;
    }

    public final float c() {
        if (this.o) {
            return this.j;
        }
        return 0.0f;
    }

    public final float d() {
        float f2 = 0.0f;
        if (!this.q) {
            if (!this.o) {
                this.k = 0.0f;
            } else {
                if (!this.p) {
                    this.l = this.o ? (this.i * this.i) + (this.j * this.j) : 0.0f;
                    this.p = true;
                }
                float f3 = this.l;
                if (f3 != 0.0f) {
                    int i2 = (int) (f3 * 256.0f);
                    int i3 = 32768;
                    int i4 = 0;
                    int i5 = 15;
                    while (true) {
                        int i6 = ((i4 << 1) + i3) << i5;
                        if (i2 >= i6) {
                            i2 -= i6;
                            i4 += i3;
                        }
                        i3 >>= 1;
                        if (i3 <= 0) {
                            break;
                        }
                        i5--;
                    }
                    f2 = ((float) i4) / 16.0f;
                }
                this.k = f2;
                if (this.k < this.i) {
                    this.k = this.i;
                }
                if (this.k < this.j) {
                    this.k = this.j;
                }
            }
            this.q = true;
        }
        return this.k;
    }

    public final float e() {
        if (!this.r) {
            if (!this.o) {
                this.m = 0.0f;
            } else {
                this.m = (float) Math.atan2((double) (this.c[1] - this.c[0]), (double) (this.b[1] - this.b[0]));
            }
            this.r = true;
        }
        return this.m;
    }

    public final float f() {
        return this.f;
    }

    public final float g() {
        return this.g;
    }

    public final boolean h() {
        return this.n;
    }

    public final long i() {
        return this.t;
    }
}
