package org.b.a.a;

import android.util.Log;
import android.view.MotionEvent;
import com.agilebinary.a.a.a.c.d.m;
import java.lang.reflect.Method;

public final class d {
    private static Method A;
    private static Method B;
    private static Method C;
    private static Method D;
    private static int E;
    private static int F;
    private static final float[] G = new float[20];
    private static final float[] H = new float[20];
    private static final float[] I = new float[20];
    private static final int[] J = new int[20];
    private static boolean v;
    private static Method w;
    private static Method x;
    private static Method y;
    private static Method z;

    /* renamed from: a  reason: collision with root package name */
    private b f320a;
    private c b;
    private c c;
    private float d;
    private float e;
    private float f;
    private float g;
    private float h;
    private float i;
    private boolean j;
    private Object k;
    private a l;
    private long m;
    private long n;
    private float o;
    private float p;
    private float q;
    private float r;
    private float s;
    private float t;
    private int u;

    static {
        boolean z2 = true;
        E = 6;
        F = 8;
        try {
            w = MotionEvent.class.getMethod(m.I("(T;a X!E*C\f^:_;"), new Class[0]);
            x = MotionEvent.class.getMethod(com.agilebinary.a.a.a.c.c.m.I("xzkOpvqkzmV{"), Integer.TYPE);
            y = MotionEvent.class.getMethod(m.I("(T;a=T<B:C*"), Integer.TYPE);
            z = MotionEvent.class.getMethod(com.agilebinary.a.a.a.c.c.m.I("xzkWvlkpmv|~sG"), Integer.TYPE, Integer.TYPE);
            A = MotionEvent.class.getMethod(m.I("V*E\u0007X<E C&R.]\u0016"), Integer.TYPE, Integer.TYPE);
            B = MotionEvent.class.getMethod(com.agilebinary.a.a.a.c.c.m.I("xzkWvlkpmv|~sOmzlljmz"), Integer.TYPE, Integer.TYPE);
            C = MotionEvent.class.getMethod(m.I("V*E\u0017"), Integer.TYPE);
            D = MotionEvent.class.getMethod(com.agilebinary.a.a.a.c.c.m.I("xzkF"), Integer.TYPE);
        } catch (Exception e2) {
            Log.e(m.I("|:];X\u001b^:R'r _;C ]#T="), com.agilebinary.a.a.a.c.c.m.I("lk~kv|?vqvkv~svezm?y~vsz{"), e2);
            z2 = false;
        }
        v = z2;
        if (z2) {
            try {
                E = MotionEvent.class.getField(m.I("\u000er\u001bx\u0000\u0010a\u0000x\u0001e\nc\u0010d\u001f")).getInt(null);
                F = MotionEvent.class.getField(com.agilebinary.a.a.a.c.c.m.I("^\\KVPQ@OPVQKZM@VQ[ZG@LWVYK")).getInt(null);
            } catch (Exception e3) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private /* synthetic */ void a() {
        float f2 = 0.0f;
        this.d = this.b.f();
        this.e = this.b.g();
        this.f = Math.max(21.3f, !this.l.g ? 0.0f : this.b.d());
        this.g = Math.max(30.0f, !this.l.h ? 0.0f : this.b.b());
        this.h = Math.max(30.0f, !this.l.h ? 0.0f : this.b.c());
        if (this.l.i) {
            f2 = this.b.e();
        }
        this.i = f2;
    }

    private /* synthetic */ void b() {
        if (this.k != null) {
            this.f320a.a(this.l);
            float d2 = 1.0f / (!this.l.g ? 1.0f : this.l.c == 0.0f ? 1.0f : this.l.c);
            a();
            this.o = (this.d - this.l.f318a) * d2;
            this.p = d2 * (this.e - this.l.b);
            this.q = this.l.c / this.f;
            this.s = this.l.d / this.g;
            this.t = this.l.e / this.h;
            this.r = this.l.f - this.i;
        }
    }

    private /* synthetic */ void c() {
        if (this.k != null) {
            float d2 = !this.l.g ? 1.0f : this.l.c == 0.0f ? 1.0f : this.l.c;
            a();
            float f2 = this.d;
            float f3 = this.o;
            float f4 = this.e;
            float f5 = this.p;
            float f6 = this.q;
            float f7 = this.f;
            float f8 = this.s;
            this.l.a(f2 - (f3 * d2), f4 - (d2 * f5), f7 * f6, this.g * f8, this.h * this.t, this.i + this.r);
            this.f320a.b(this.l);
        }
    }

    public final boolean a(MotionEvent motionEvent) {
        try {
            int intValue = v ? ((Integer) w.invoke(motionEvent, new Object[0])).intValue() : 1;
            if (this.u == 0 && !this.j && intValue == 1) {
                return false;
            }
            int action = motionEvent.getAction();
            int historySize = motionEvent.getHistorySize() / intValue;
            int i2 = 0;
            while (i2 <= historySize) {
                boolean z2 = i2 < historySize;
                if (!v || intValue == 1) {
                    G[0] = z2 ? motionEvent.getHistoricalX(i2) : motionEvent.getX();
                    H[0] = z2 ? motionEvent.getHistoricalY(i2) : motionEvent.getY();
                    I[0] = z2 ? motionEvent.getHistoricalPressure(i2) : motionEvent.getPressure();
                } else {
                    int min = Math.min(intValue, 20);
                    for (int i3 = 0; i3 < min; i3++) {
                        J[i3] = ((Integer) x.invoke(motionEvent, Integer.valueOf(i3))).intValue();
                        G[i3] = ((Float) (z2 ? z.invoke(motionEvent, Integer.valueOf(i3), Integer.valueOf(i2)) : C.invoke(motionEvent, Integer.valueOf(i3)))).floatValue();
                        H[i3] = ((Float) (z2 ? A.invoke(motionEvent, Integer.valueOf(i3), Integer.valueOf(i2)) : D.invoke(motionEvent, Integer.valueOf(i3)))).floatValue();
                        I[i3] = ((Float) (z2 ? B.invoke(motionEvent, Integer.valueOf(i3), Integer.valueOf(i2)) : y.invoke(motionEvent, Integer.valueOf(i3)))).floatValue();
                    }
                }
                float[] fArr = G;
                float[] fArr2 = H;
                float[] fArr3 = I;
                int[] iArr = J;
                int i4 = z2 ? 2 : action;
                boolean z3 = z2 ? true : (action == 1 || (((1 << F) + -1) & action) == E || action == 3) ? false : true;
                long historicalEventTime = z2 ? motionEvent.getHistoricalEventTime(i2) : motionEvent.getEventTime();
                c cVar = this.c;
                this.c = this.b;
                this.b = cVar;
                c.a(this.b, intValue, fArr, fArr2, fArr3, iArr, i4, z3, historicalEventTime);
                switch (this.u) {
                    case 0:
                        if (!this.b.h()) {
                            break;
                        } else {
                            this.k = this.f320a.a();
                            if (this.k == null) {
                                break;
                            } else {
                                this.u = 1;
                                this.f320a.a(this.k);
                                b();
                                long i5 = this.b.i();
                                this.n = i5;
                                this.m = i5;
                                break;
                            }
                        }
                    case 1:
                        if (this.b.h()) {
                            if (!this.b.a()) {
                                if (this.b.i() >= this.n) {
                                    c();
                                    break;
                                } else {
                                    b();
                                    break;
                                }
                            } else {
                                this.u = 2;
                                b();
                                this.m = this.b.i();
                                this.n = this.m + 20;
                                break;
                            }
                        } else {
                            this.u = 0;
                            b bVar = this.f320a;
                            this.k = null;
                            bVar.a((Object) null);
                            break;
                        }
                    case 2:
                        if (this.b.a() && this.b.h()) {
                            if (Math.abs(this.b.f() - this.c.f()) <= 30.0f && Math.abs(this.b.g() - this.c.g()) <= 30.0f && Math.abs(this.b.b() - this.c.b()) * 0.5f <= 40.0f && Math.abs(this.b.c() - this.c.c()) * 0.5f <= 40.0f) {
                                if (this.b.t >= this.n) {
                                    c();
                                    break;
                                } else {
                                    b();
                                    break;
                                }
                            } else {
                                b();
                                this.m = this.b.i();
                                this.n = this.m + 20;
                                break;
                            }
                        } else if (this.b.h()) {
                            this.u = 1;
                            b();
                            this.m = this.b.i();
                            this.n = this.m + 20;
                            break;
                        } else {
                            this.u = 0;
                            b bVar2 = this.f320a;
                            this.k = null;
                            bVar2.a((Object) null);
                            break;
                        }
                }
                i2++;
            }
            return true;
        } catch (Exception e2) {
            Log.e(m.I("|:];X\u001b^:R'r _;C ]#T="), com.agilebinary.a.a.a.c.c.m.I("pqKpj|wZizqk76?y~vsz{"), e2);
            return false;
        }
    }
}
