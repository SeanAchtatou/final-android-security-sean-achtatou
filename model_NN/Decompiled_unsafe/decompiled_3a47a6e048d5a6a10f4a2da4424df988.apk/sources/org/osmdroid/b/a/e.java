package org.osmdroid.b.a;

import android.graphics.Canvas;
import android.view.MotionEvent;
import org.osmdroid.b;
import org.osmdroid.b.f;

public abstract class e {

    /* renamed from: a  reason: collision with root package name */
    private b f348a;
    private boolean b = true;

    public e(b bVar) {
        this.f348a = bVar;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 20);
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'x');
            i2 = i;
        }
        return new String(cArr);
    }

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas);

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas, f fVar);

    public final void a(boolean z) {
        this.b = z;
    }

    public final boolean a() {
        return this.b;
    }

    public boolean a(MotionEvent motionEvent, f fVar) {
        return false;
    }

    public void b() {
    }

    public final void b(Canvas canvas, f fVar) {
        if (this.b) {
            a(canvas, fVar);
            a(canvas);
        }
    }

    public boolean b(MotionEvent motionEvent, f fVar) {
        return false;
    }
}
