package org.osmdroid.util;

public final class c {
    private /* synthetic */ c() {
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ '\'');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'f');
            i2 = i;
        }
        return new String(cArr);
    }

    public static int a(int i, int i2) {
        if (i > 0) {
            return i % i2;
        }
        int i3 = i;
        while (i3 < 0) {
            i3 += i2;
        }
        return i3;
    }
}
