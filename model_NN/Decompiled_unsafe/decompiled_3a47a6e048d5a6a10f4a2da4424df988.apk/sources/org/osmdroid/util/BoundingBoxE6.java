package org.osmdroid.util;

import android.os.Parcel;
import android.os.Parcelable;
import com.agilebinary.a.a.a.i.b;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import org.a.a.h;

public class BoundingBoxE6 implements Parcelable, Serializable {
    public static final Parcelable.Creator CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private int f364a;
    private int b;
    private int c;
    private int d;

    public BoundingBoxE6(double d2, double d3, double d4, double d5) {
        this.f364a = (int) (d2 * 1000000.0d);
        this.c = (int) (d3 * 1000000.0d);
        this.b = (int) (d4 * 1000000.0d);
        this.d = (int) (d5 * 1000000.0d);
    }

    private /* synthetic */ BoundingBoxE6(int i, int i2, int i3, int i4) {
        this.f364a = i;
        this.c = i2;
        this.b = i3;
        this.d = i4;
    }

    static /* synthetic */ BoundingBoxE6 a(Parcel parcel) {
        return new BoundingBoxE6(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
    }

    public static BoundingBoxE6 a(ArrayList arrayList) {
        int i = Integer.MAX_VALUE;
        Iterator it = arrayList.iterator();
        int i2 = Integer.MAX_VALUE;
        int i3 = Integer.MIN_VALUE;
        int i4 = Integer.MIN_VALUE;
        while (true) {
            int i5 = i;
            if (!it.hasNext()) {
                return new BoundingBoxE6(i5, i2, i3, i4);
            }
            GeoPoint geoPoint = (GeoPoint) it.next();
            int a2 = geoPoint.a();
            int b2 = geoPoint.b();
            i = Math.min(i5, a2);
            int min = Math.min(i2, b2);
            int max = Math.max(i3, a2);
            i4 = Math.max(i4, b2);
            i3 = max;
            i2 = min;
        }
    }

    public final GeoPoint a() {
        return new GeoPoint((this.f364a + this.b) / 2, (this.c + this.d) / 2);
    }

    public final GeoPoint a(float f, float f2) {
        int c2 = (int) (((float) this.f364a) - (((float) c()) * f2));
        int d2 = (int) (((float) this.d) + (((float) d()) * f));
        while (c2 > 90500000) {
            c2 -= 90500000;
        }
        while (c2 < -90500000) {
            c2 += 90500000;
        }
        while (d2 > 180000000) {
            d2 -= 180000000;
        }
        while (d2 < -180000000) {
            d2 += 180000000;
        }
        return new GeoPoint(c2, d2);
    }

    public final int b() {
        return new GeoPoint(this.f364a, this.d).a(new GeoPoint(this.b, this.c));
    }

    public final int c() {
        return Math.abs(this.f364a - this.b);
    }

    public final int d() {
        return Math.abs(this.c - this.d);
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return new StringBuffer().append(b.I("x\b")).append(this.f364a).append(h.I("L\f2\u0016")).append(this.c).append(b.I("\r\u0012e\b")).append(this.b).append(h.I("L\f \u0016")).append(this.d).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f364a);
        parcel.writeInt(this.c);
        parcel.writeInt(this.b);
        parcel.writeInt(this.d);
    }
}
