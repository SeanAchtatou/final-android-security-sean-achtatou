package org.osmdroid.a;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import org.osmdroid.a.b.v;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final Queue f341a = new LinkedList();
    private final f b;
    private final g c;
    private v d;

    public d(f fVar, v[] vVarArr, g gVar) {
        Collections.addAll(this.f341a, vVarArr);
        this.b = fVar;
        this.c = gVar;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ '\'');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '4');
            i2 = i;
        }
        return new String(cArr);
    }

    public final f a() {
        return this.b;
    }

    public final g b() {
        return this.c;
    }

    public final v c() {
        this.d = (v) this.f341a.poll();
        return this.d;
    }
}
