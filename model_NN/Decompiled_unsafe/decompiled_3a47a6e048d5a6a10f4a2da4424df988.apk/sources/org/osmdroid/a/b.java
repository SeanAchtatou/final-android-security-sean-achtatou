package org.osmdroid.a;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.agilebinary.a.a.a.i.f;
import java.util.LinkedHashMap;
import java.util.Map;
import org.a.a;
import org.a.c;

public class b extends LinkedHashMap {

    /* renamed from: a  reason: collision with root package name */
    private static final c f323a = a.a(b.class);
    private int b = 9;

    public b() {
        super(11, 0.1f, true);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public /* synthetic */ Drawable remove(Object obj) {
        Bitmap bitmap;
        Drawable drawable = (Drawable) super.remove(obj);
        if ((drawable instanceof BitmapDrawable) && (bitmap = ((BitmapDrawable) drawable).getBitmap()) != null) {
            bitmap.recycle();
        }
        return drawable;
    }

    public final void a(int i) {
        if (i > this.b) {
            f323a.b(com.agilebinary.mobilemonitor.client.a.a.a.I("\bg0k|m=m4k|g2m.k=}9j|h.a1.") + this.b + f.I("\u000fO@\u001b") + i);
            this.b = i;
        }
    }

    public void clear() {
        while (size() > 0) {
            remove(keySet().iterator().next());
        }
        super.clear();
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry entry) {
        if (size() <= this.b) {
            return false;
        }
        remove(entry.getKey());
        return false;
    }
}
