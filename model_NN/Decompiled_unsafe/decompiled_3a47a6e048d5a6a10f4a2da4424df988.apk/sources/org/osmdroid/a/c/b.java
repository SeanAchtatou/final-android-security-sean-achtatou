package org.osmdroid.a.c;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import org.osmdroid.a.a;

public final class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private final Context f338a;

    public b(Context context) {
        this.f338a = context;
    }

    public final Intent a(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        return this.f338a.registerReceiver(broadcastReceiver, intentFilter);
    }

    public final void a(BroadcastReceiver broadcastReceiver) {
        this.f338a.unregisterReceiver(broadcastReceiver);
    }
}
