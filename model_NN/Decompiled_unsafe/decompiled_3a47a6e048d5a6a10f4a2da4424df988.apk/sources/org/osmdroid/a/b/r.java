package org.osmdroid.a.b;

import com.agilebinary.a.a.a.k;
import org.a.a;
import org.a.c;
import org.osmdroid.a.a.d;
import org.osmdroid.a.a.f;

public class r extends v {
    /* access modifiers changed from: private */
    public static final c c = a.a(r.class);
    /* access modifiers changed from: private */
    public final c e;
    /* access modifiers changed from: private */
    public d f;
    /* access modifiers changed from: private */
    public final l g;

    public r(f fVar, c cVar, l lVar) {
        super(2);
        this.e = cVar;
        this.g = lVar;
        if (fVar instanceof d) {
            this.f = (d) fVar;
        } else {
            this.f = null;
        }
    }

    public final boolean e() {
        return true;
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return k.I("5\f&\r=\f0\u00074\u0011");
    }

    /* access modifiers changed from: protected */
    public final Runnable g() {
        return new d(this);
    }

    public final int h() {
        if (this.f != null) {
            return this.f.d();
        }
        return 23;
    }

    public final int i() {
        if (this.f != null) {
            return this.f.e();
        }
        return 0;
    }
}
