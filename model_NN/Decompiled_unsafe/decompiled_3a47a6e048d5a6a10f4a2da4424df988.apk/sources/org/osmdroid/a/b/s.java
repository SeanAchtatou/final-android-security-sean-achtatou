package org.osmdroid.a.b;

import android.database.sqlite.SQLiteException;
import com.agilebinary.a.a.a.c.c.m;
import java.io.File;
import java.io.IOException;
import org.a.a;
import org.a.c;
import org.osmdroid.b.a.e;

public class s {

    /* renamed from: a  reason: collision with root package name */
    private static final c f335a = a.a(s.class);

    public static m a(File file) {
        if (file.getName().endsWith(m.I("1evo"))) {
            try {
                return g.a(file);
            } catch (IOException e) {
                f335a.c(e.I("=f\n{\n4\u0017d\u001dz\u0011z\u001f4\"](4\u001e}\u0014q"), e);
            }
        }
        if (file.getName().endsWith(m.I("1lnsvkz"))) {
            try {
                return e.a(file);
            } catch (SQLiteException e2) {
                f335a.c(e.I("=f\n{\n4\u0017d\u001dz\u0011z\u001f4+E44\u001e}\u0014q"), e2);
            }
        }
        if (file.getName().endsWith(m.I("1xzry"))) {
            try {
                return f.a(file);
            } catch (IOException e3) {
                f335a.c(e.I("Q\nf\u0017fX{\bq\u0016}\u0016sXS=Y>4\u001e}\u0014q"), e3);
            }
        }
        return null;
    }
}
