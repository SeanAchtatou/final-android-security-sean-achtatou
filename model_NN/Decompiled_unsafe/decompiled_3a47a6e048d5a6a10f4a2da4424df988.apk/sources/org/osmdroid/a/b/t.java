package org.osmdroid.a.b;

import com.agilebinary.mobilemonitor.client.android.ui.a.e;
import org.a.a;
import org.a.c;
import org.osmdroid.a.a.b;
import org.osmdroid.a.a.f;

public class t extends h {
    private static final c c = a.a(t.class);
    /* access modifiers changed from: private */
    public final long e;
    /* access modifiers changed from: private */
    public f f;

    public t(org.osmdroid.a.a aVar) {
        this(aVar, b.f322a, (byte) 0);
    }

    private /* synthetic */ t(org.osmdroid.a.a aVar, f fVar) {
        super(aVar);
        this.f = fVar;
        this.e = 604800000;
    }

    private /* synthetic */ t(org.osmdroid.a.a aVar, f fVar, byte b) {
        this(aVar, fVar);
    }

    public final boolean e() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return e.I("\fd\u0006h\u0019t\u0019y\u000f`");
    }

    /* access modifiers changed from: protected */
    public final Runnable g() {
        return new u(this);
    }

    public final int h() {
        if (this.f != null) {
            return this.f.d();
        }
        return 23;
    }

    public final int i() {
        if (this.f != null) {
            return this.f.e();
        }
        return 0;
    }
}
