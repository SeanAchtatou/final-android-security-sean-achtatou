package org.osmdroid.a.b;

import android.content.IntentFilter;
import android.os.Environment;
import com.agilebinary.a.a.a.i.d;
import org.a.a;
import org.a.c;

public abstract class h extends v {
    private static final c c = a.a(h.class);
    private boolean e = true;
    private final org.osmdroid.a.a f;
    private n g;

    public h(org.osmdroid.a.a aVar) {
        super(8);
        k();
        this.f = aVar;
        this.g = new n(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(d.I("=q8m3v815q(z2kr~?k5p21\u0011Z\u0018V\u001d@\u0011P\tQ\bZ\u0018"));
        intentFilter.addAction(org.osmdroid.a.d.I("FZCFH]C\u001aNZSQI@\tUD@N[I\u001ajqc}fkrzj{rzsqc"));
        intentFilter.addDataScheme(d.I("y5s9"));
        aVar.a(this.g, intentFilter);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void k() {
        String externalStorageState = Environment.getExternalStorageState();
        c.b(org.osmdroid.a.d.I("GCWFFC\u0014T@F@B\u000e\u0007") + externalStorageState);
        this.e = d.I("1p)q(z8").equals(externalStorageState);
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        return this.e;
    }

    public final void b() {
        if (this.g != null) {
            this.f.a(this.g);
            this.g = null;
        }
        super.b();
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public void d() {
    }
}
