package org.osmdroid.a.b;

import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.android.c.e;
import java.io.File;
import java.io.InputStream;
import org.osmdroid.util.d;

public final class f implements m {

    /* renamed from: a  reason: collision with root package name */
    private final d f328a;

    private /* synthetic */ f(File file) {
        this.f328a = new d(file);
    }

    public static f a(File file) {
        return new f(file);
    }

    public final InputStream a(org.osmdroid.a.a.f fVar, org.osmdroid.a.f fVar2) {
        return this.f328a.a(fVar2.b(), fVar2.c(), fVar2.a());
    }

    public final String toString() {
        return c.I("\u0006/\f,\u0007\u0003-\u000f\u0000\u0018\"\u0002(\u001c$J\u001a\u0007\u0006/\f,\u0007\u0003-\u000f|") + this.f328a.a() + e.I(".");
    }
}
