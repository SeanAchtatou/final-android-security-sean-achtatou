package org.osmdroid.a.b;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.agilebinary.a.a.a.f.b;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import org.a.a;
import org.a.c;
import org.osmdroid.a.a.f;

public class e implements m {

    /* renamed from: a  reason: collision with root package name */
    private static final c f327a = a.a(e.class);
    private final SQLiteDatabase b;

    private /* synthetic */ e(SQLiteDatabase sQLiteDatabase) {
        this.b = sQLiteDatabase;
    }

    public static e a(File file) {
        return new e(SQLiteDatabase.openOrCreateDatabase(file, (SQLiteDatabase.CursorFactory) null));
    }

    public final InputStream a(f fVar, org.osmdroid.a.f fVar2) {
        ByteArrayInputStream byteArrayInputStream;
        try {
            long b2 = (long) fVar2.b();
            long c = (long) fVar2.c();
            long a2 = (long) fVar2.a();
            Cursor query = this.b.query(b.I("?c'o8"), new String[]{org.b.a.a.c.I("X&@*")}, org.b.a.a.c.I("G*Uo\u0011o") + (((b2 + (a2 << ((int) a2))) << ((int) a2)) + c) + b.I("kk%nkz9e=c/o9*v*l") + fVar.c() + org.b.a.a.c.I("h"), null, null, null, null);
            if (query.getCount() != 0) {
                query.moveToFirst();
                byteArrayInputStream = new ByteArrayInputStream(query.getBlob(0));
            } else {
                byteArrayInputStream = null;
            }
            query.close();
            if (byteArrayInputStream != null) {
                return byteArrayInputStream;
            }
        } catch (Throwable th) {
            f327a.b(b.I("\u000ex9e9*,o?~\"d,*/hky?x.k&0k") + fVar2, th);
        }
        return null;
    }

    public String toString() {
        return org.b.a.a.c.I("\u000bM;M-M<I\tE#I\u000e^,D&Z*\f\u0014A\u000bM;M-M<Ir") + this.b.getPath() + b.I("\u0016");
    }
}
