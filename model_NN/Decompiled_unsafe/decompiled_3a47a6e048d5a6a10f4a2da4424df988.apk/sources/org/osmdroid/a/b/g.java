package org.osmdroid.a.b;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.a.a;
import org.a.c;
import org.osmdroid.a.a.f;

public class g implements m {

    /* renamed from: a  reason: collision with root package name */
    private static final c f329a = a.a(g.class);
    private final ZipFile b;

    private /* synthetic */ g(ZipFile zipFile) {
        this.b = zipFile;
    }

    public static g a(File file) {
        return new g(new ZipFile(file));
    }

    public final InputStream a(f fVar, org.osmdroid.a.f fVar2) {
        try {
            ZipEntry entry = this.b.getEntry(fVar.b(fVar2));
            if (entry != null) {
                return this.b.getInputStream(entry);
            }
        } catch (IOException e) {
            f329a.b(com.jasonkostempski.android.calendar.a.I("eYRDR\u000bGNT_IEG\u000bZBP\u000bS_RNAF\u001a\u000b") + fVar2, e);
        }
        return null;
    }

    public String toString() {
        return org.osmdroid.util.c.I("}\u000fW N\nB'U\u0005O\u000fQ\u0003\u0007=J<N\u0016a\u000fK\u0003\u001a") + this.b.getName() + com.jasonkostempski.android.calendar.a.I("v");
    }
}
