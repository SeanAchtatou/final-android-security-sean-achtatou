package org.osmdroid.a.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.agilebinary.mobilemonitor.client.a.b.t;

public final class w implements l {

    /* renamed from: a  reason: collision with root package name */
    private final ConnectivityManager f337a;

    public w(Context context) {
        this.f337a = (ConnectivityManager) context.getSystemService(t.I("nPcQh\\yV{VyF"));
    }

    public final boolean a() {
        NetworkInfo activeNetworkInfo = this.f337a.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isAvailable();
    }
}
