package org.osmdroid.a.b;

import com.agilebinary.mobilemonitor.client.android.a.a.g;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.a.c;
import org.osmdroid.a.d;
import org.osmdroid.a.d.a;

public class k implements c, a {
    private static final c c = org.a.a.a(k.class);
    /* access modifiers changed from: private */
    public static long d;

    public k() {
        b bVar = new b(this);
        bVar.setPriority(1);
        bVar.start();
    }

    private static /* synthetic */ boolean a(File file) {
        if (file.mkdirs()) {
            return true;
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        return file.exists();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        synchronized (b) {
            if (d > 524288000) {
                c.b(d.I("sFNYJ]IS\u0007@NXB\u0014DUD\\B\u0014AFHY\u0007") + d + g.I("\u0010k_?") + 524288000L);
                File[] fileArr = (File[]) c(b).toArray(new File[0]);
                Arrays.sort(fileArr, new a(this));
                for (File file : fileArr) {
                    if (d <= 524288000) {
                        break;
                    }
                    long length = file.length();
                    if (file.delete()) {
                        d -= length;
                    }
                }
                c.b(d.I("rNZNGOQC\u0014SFNYJ]IS\u0007@NXB\u0014DUD\\B"));
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isFile()) {
                    d += file2.length();
                }
                if (file2.isDirectory()) {
                    b(file2);
                }
            }
        }
    }

    private /* synthetic */ List c(File file) {
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isFile()) {
                    arrayList.add(file2);
                }
                if (file2.isDirectory()) {
                    arrayList.addAll(c(file2));
                }
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(org.osmdroid.a.a.f r7, org.osmdroid.a.f r8, java.io.InputStream r9) {
        /*
            r6 = this;
            r2 = 0
            r0 = 0
            java.io.File r3 = new java.io.File
            java.io.File r1 = org.osmdroid.a.b.k.b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = r7.b(r8)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "1Dv\\z"
            java.lang.String r5 = com.agilebinary.mobilemonitor.client.android.a.a.g.I(r5)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.<init>(r1, r4)
            java.io.File r1 = r3.getParentFile()
            boolean r4 = r1.exists()
            if (r4 != 0) goto L_0x0035
            boolean r1 = a(r1)
            if (r1 != 0) goto L_0x0035
        L_0x0034:
            return r0
        L_0x0035:
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x005d, all -> 0x0065 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x005d, all -> 0x0065 }
            java.lang.String r3 = r3.getPath()     // Catch:{ IOException -> 0x005d, all -> 0x0065 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x005d, all -> 0x0065 }
            r3 = 8192(0x2000, float:1.14794E-41)
            r1.<init>(r4, r3)     // Catch:{ IOException -> 0x005d, all -> 0x0065 }
            long r2 = org.osmdroid.a.c.c.a(r9, r1)     // Catch:{ IOException -> 0x006d, all -> 0x006f }
            long r4 = org.osmdroid.a.b.k.d     // Catch:{ IOException -> 0x006d, all -> 0x006f }
            long r2 = r2 + r4
            org.osmdroid.a.b.k.d = r2     // Catch:{ IOException -> 0x006d, all -> 0x006f }
            r4 = 629145600(0x25800000, double:3.10839227E-315)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0058
            r6.b()     // Catch:{ IOException -> 0x006d, all -> 0x006f }
        L_0x0058:
            org.osmdroid.a.c.c.a(r1)
            r0 = 1
            goto L_0x0034
        L_0x005d:
            r1 = move-exception
            r1 = r2
        L_0x005f:
            if (r1 == 0) goto L_0x0034
            org.osmdroid.a.c.c.a(r1)
            goto L_0x0034
        L_0x0065:
            r0 = move-exception
            r1 = r2
        L_0x0067:
            if (r1 == 0) goto L_0x006c
            org.osmdroid.a.c.c.a(r1)
        L_0x006c:
            throw r0
        L_0x006d:
            r2 = move-exception
            goto L_0x005f
        L_0x006f:
            r0 = move-exception
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.a.b.k.a(org.osmdroid.a.a.f, org.osmdroid.a.f, java.io.InputStream):boolean");
    }
}
