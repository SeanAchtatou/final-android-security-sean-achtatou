package org.osmdroid.a.b;

import android.graphics.drawable.Drawable;
import com.agilebinary.a.a.a.k.a;
import com.agilebinary.mobilemonitor.client.android.h;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import org.osmdroid.a.d;
import org.osmdroid.a.f;

public abstract class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ v f330a;

    protected i(v vVar) {
        this.f330a = vVar;
    }

    private /* synthetic */ d a() {
        d dVar;
        synchronized (this.f330a.d) {
            Iterator it = this.f330a.d.keySet().iterator();
            f fVar = null;
            while (it.hasNext()) {
                try {
                    f fVar2 = (f) it.next();
                    if (this.f330a.g.containsKey(fVar2)) {
                        fVar2 = fVar;
                    }
                    fVar = fVar2;
                } catch (ConcurrentModificationException e) {
                    if (fVar != null) {
                        break;
                    }
                    it = this.f330a.d.keySet().iterator();
                }
            }
            if (fVar != null) {
                this.f330a.g.put(fVar, this.f330a.d.get(fVar));
            }
            dVar = fVar != null ? (d) this.f330a.d.get(fVar) : null;
        }
        return dVar;
    }

    /* access modifiers changed from: protected */
    public abstract Drawable a(d dVar);

    public final void run() {
        while (true) {
            d a2 = a();
            if (a2 != null) {
                Drawable drawable = null;
                try {
                    drawable = a(a2);
                } catch (p e) {
                    v.c.a(h.I("n1V=\u001a4U9^=HxY9TNxY7T,S6O="), e);
                    this.f330a.a();
                } catch (Throwable th) {
                    v.c.c(a.I("DAs\\s\u0013e\\v]m\\`Wh]f\u0013uZmV;\u0013") + a2, th);
                }
                if (drawable != null) {
                    v.a(this.f330a, a2.a());
                    a2.b().a(a2, drawable);
                } else {
                    v.a(this.f330a, a2.a());
                    a2.b().a(a2);
                }
            } else {
                return;
            }
        }
    }
}
