package org.osmdroid.a.b;

final class d extends i {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ r f326a;

    /* synthetic */ d(r rVar) {
        this(rVar, (byte) 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private /* synthetic */ d(r rVar, byte b) {
        super(rVar);
        this.f326a = rVar;
    }

    /* JADX WARN: Type inference failed for: r0v18 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:36:0x0104=Splitter:B:36:0x0104, B:41:0x013b=Splitter:B:41:0x013b, B:31:0x00cd=Splitter:B:31:0x00cd} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.drawable.Drawable a(org.osmdroid.a.d r10) {
        /*
            r9 = this;
            r1 = 0
            org.osmdroid.a.b.r r0 = r9.f326a
            org.osmdroid.a.a.d r0 = r0.f
            if (r0 != 0) goto L_0x000a
        L_0x0009:
            return r1
        L_0x000a:
            org.osmdroid.a.f r4 = r10.a()
            org.osmdroid.a.b.r r0 = r9.f326a     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            org.osmdroid.a.b.l r0 = r0.g     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            if (r0 == 0) goto L_0x0029
            org.osmdroid.a.b.r r0 = r9.f326a     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            org.osmdroid.a.b.l r0 = r0.g     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            boolean r0 = r0.a()     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            if (r0 != 0) goto L_0x0029
            org.osmdroid.a.c.c.a(r1)
            org.osmdroid.a.c.c.a(r1)
            goto L_0x0009
        L_0x0029:
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            java.net.URL r0 = new java.net.URL     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            org.osmdroid.a.b.r r3 = r9.f326a     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            org.osmdroid.a.a.d r3 = r3.f     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            java.lang.String r3 = r3.a(r4)     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            r0.<init>(r3)     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            java.io.InputStream r0 = r0.openStream()     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            r3 = 8192(0x2000, float:1.14794E-41)
            r2.<init>(r0, r3)     // Catch:{ UnknownHostException -> 0x0089, FileNotFoundException -> 0x00ca, IOException -> 0x0101, Throwable -> 0x0138, all -> 0x0161 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ UnknownHostException -> 0x018e, FileNotFoundException -> 0x017e, IOException -> 0x0176, Throwable -> 0x016e, all -> 0x0168 }
            r3.<init>()     // Catch:{ UnknownHostException -> 0x018e, FileNotFoundException -> 0x017e, IOException -> 0x0176, Throwable -> 0x016e, all -> 0x0168 }
            java.io.BufferedOutputStream r0 = new java.io.BufferedOutputStream     // Catch:{ UnknownHostException -> 0x018e, FileNotFoundException -> 0x017e, IOException -> 0x0176, Throwable -> 0x016e, all -> 0x0168 }
            r5 = 8192(0x2000, float:1.14794E-41)
            r0.<init>(r3, r5)     // Catch:{ UnknownHostException -> 0x018e, FileNotFoundException -> 0x017e, IOException -> 0x0176, Throwable -> 0x016e, all -> 0x0168 }
            org.osmdroid.a.c.c.a(r2, r0)     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            r0.flush()     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            java.io.ByteArrayInputStream r5 = new java.io.ByteArrayInputStream     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            byte[] r3 = r3.toByteArray()     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            r5.<init>(r3)     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            org.osmdroid.a.b.r r3 = r9.f326a     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            org.osmdroid.a.b.c r3 = r3.e     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            if (r3 == 0) goto L_0x0078
            org.osmdroid.a.b.r r3 = r9.f326a     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            org.osmdroid.a.b.c r3 = r3.e     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            org.osmdroid.a.b.r r6 = r9.f326a     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            org.osmdroid.a.a.d r6 = r6.f     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            r3.a(r6, r4, r5)     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            r5.reset()     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
        L_0x0078:
            org.osmdroid.a.b.r r3 = r9.f326a     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            org.osmdroid.a.a.d r3 = r3.f     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            android.graphics.drawable.Drawable r1 = r3.a(r5)     // Catch:{ UnknownHostException -> 0x0188, FileNotFoundException -> 0x0182, IOException -> 0x0179, Throwable -> 0x0171, all -> 0x0191 }
            org.osmdroid.a.c.c.a(r2)
            org.osmdroid.a.c.c.a(r0)
            goto L_0x0009
        L_0x0089:
            r0 = move-exception
            r2 = r1
        L_0x008b:
            org.a.c r3 = org.osmdroid.a.b.r.c     // Catch:{ all -> 0x00bf }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bf }
            r5.<init>()     // Catch:{ all -> 0x00bf }
            java.lang.String r6 = "-z\u0013z\u0017c\u0016\\\u0017g\fQ\u0000w\u001dd\f}\u0017zXp\u0017c\u0016x\u0017u\u001c}\u0016sXY\u0019d,}\u0014qB4"
            java.lang.String r6 = org.osmdroid.b.a.e.I(r6)     // Catch:{ all -> 0x00bf }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00bf }
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ all -> 0x00bf }
            java.lang.String r5 = "CkC"
            java.lang.String r5 = com.agilebinary.a.a.a.k.I(r5)     // Catch:{ all -> 0x00bf }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00bf }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ all -> 0x00bf }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00bf }
            r3.c(r4)     // Catch:{ all -> 0x00bf }
            org.osmdroid.a.b.p r3 = new org.osmdroid.a.b.p     // Catch:{ all -> 0x00bf }
            org.osmdroid.a.b.r r4 = r9.f326a     // Catch:{ all -> 0x00bf }
            r3.<init>(r4, r0)     // Catch:{ all -> 0x00bf }
            throw r3     // Catch:{ all -> 0x00bf }
        L_0x00bf:
            r0 = move-exception
            r8 = r0
            r0 = r1
            r1 = r8
        L_0x00c3:
            org.osmdroid.a.c.c.a(r2)
            org.osmdroid.a.c.c.a(r0)
            throw r1
        L_0x00ca:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x00cd:
            org.a.c r5 = org.osmdroid.a.b.r.c     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0194 }
            r6.<init>()     // Catch:{ all -> 0x0194 }
            java.lang.String r7 = ",}\u0014qXz\u0017`Xr\u0017a\u0016pB4"
            java.lang.String r7 = org.osmdroid.b.a.e.I(r7)     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ all -> 0x0194 }
            java.lang.String r6 = "CkC"
            java.lang.String r6 = com.agilebinary.a.a.a.k.I(r6)     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0194 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0194 }
            r5.c(r0)     // Catch:{ all -> 0x0194 }
            org.osmdroid.a.c.c.a(r2)
            org.osmdroid.a.c.c.a(r3)
            goto L_0x0009
        L_0x0101:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x0104:
            org.a.c r5 = org.osmdroid.a.b.r.c     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0194 }
            r6.<init>()     // Catch:{ all -> 0x0194 }
            java.lang.String r7 = "]7Q\u0000w\u001dd\f}\u0017zXp\u0017c\u0016x\u0017u\u001c}\u0016sXY\u0019d,}\u0014qB4"
            java.lang.String r7 = org.osmdroid.b.a.e.I(r7)     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ all -> 0x0194 }
            java.lang.String r6 = "CkC"
            java.lang.String r6 = com.agilebinary.a.a.a.k.I(r6)     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0194 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0194 }
            r5.c(r0)     // Catch:{ all -> 0x0194 }
            org.osmdroid.a.c.c.a(r2)
            org.osmdroid.a.c.c.a(r3)
            goto L_0x0009
        L_0x0138:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x013b:
            org.a.c r5 = org.osmdroid.a.b.r.c     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0194 }
            r6.<init>()     // Catch:{ all -> 0x0194 }
            java.lang.String r7 = "Q\nf\u0017fXp\u0017c\u0016x\u0017u\u001c}\u0016sXY\u0019d,}\u0014qB4"
            java.lang.String r7 = org.osmdroid.b.a.e.I(r7)     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0194 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ all -> 0x0194 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0194 }
            r5.c(r4, r0)     // Catch:{ all -> 0x0194 }
            org.osmdroid.a.c.c.a(r2)
            org.osmdroid.a.c.c.a(r3)
            goto L_0x0009
        L_0x0161:
            r0 = move-exception
            r2 = r1
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x00c3
        L_0x0168:
            r0 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c3
        L_0x016e:
            r0 = move-exception
            r3 = r1
            goto L_0x013b
        L_0x0171:
            r3 = move-exception
            r8 = r3
            r3 = r0
            r0 = r8
            goto L_0x013b
        L_0x0176:
            r0 = move-exception
            r3 = r1
            goto L_0x0104
        L_0x0179:
            r3 = move-exception
            r8 = r3
            r3 = r0
            r0 = r8
            goto L_0x0104
        L_0x017e:
            r0 = move-exception
            r3 = r1
            goto L_0x00cd
        L_0x0182:
            r3 = move-exception
            r8 = r3
            r3 = r0
            r0 = r8
            goto L_0x00cd
        L_0x0188:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x008b
        L_0x018e:
            r0 = move-exception
            goto L_0x008b
        L_0x0191:
            r1 = move-exception
            goto L_0x00c3
        L_0x0194:
            r1 = move-exception
            r0 = r3
            goto L_0x00c3
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.a.b.d.a(org.osmdroid.a.d):android.graphics.drawable.Drawable");
    }
}
