package org.osmdroid.a.b;

import com.agilebinary.mobilemonitor.client.android.b.g;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import org.a.a;
import org.a.c;
import org.osmdroid.a.a.f;

public class q extends h {
    /* access modifiers changed from: private */
    public static final c e = a.a(q.class);
    protected f c;
    private final ArrayList f = new ArrayList();

    public q(org.osmdroid.a.a aVar, f fVar) {
        super(aVar);
        this.c = fVar;
        k();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ InputStream a(org.osmdroid.a.f fVar) {
        InputStream inputStream;
        synchronized (this) {
            Iterator it = this.f.iterator();
            while (true) {
                if (!it.hasNext()) {
                    inputStream = null;
                    break;
                }
                inputStream = ((m) it.next()).a(this.c, fVar);
                if (inputStream != null) {
                    break;
                }
            }
        }
        return inputStream;
    }

    private /* synthetic */ void k() {
        File[] listFiles;
        this.f.clear();
        if (a() && (listFiles = f342a.listFiles()) != null) {
            for (File a2 : listFiles) {
                m a3 = s.a(a2);
                if (a3 != null) {
                    this.f.add(a3);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
        k();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        k();
    }

    public final boolean e() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return g.I("\u0014J\u001eF\u0013Q\u0011K\u001bU\u0017");
    }

    /* access modifiers changed from: protected */
    public final Runnable g() {
        return new j(this);
    }

    public final int h() {
        if (this.c != null) {
            return this.c.d();
        }
        return 23;
    }

    public final int i() {
        if (this.c != null) {
            return this.c.e();
        }
        return 0;
    }
}
