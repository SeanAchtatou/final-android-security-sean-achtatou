package org.osmdroid.a.b;

import java.util.LinkedHashMap;
import java.util.Map;

final class o extends LinkedHashMap {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ int f333a = 40;
    private /* synthetic */ v b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    o(v vVar, int i, int i2) {
        super(42, 0.1f, true);
        this.b = vVar;
    }

    /* access modifiers changed from: protected */
    public final boolean removeEldestEntry(Map.Entry entry) {
        return size() > this.f333a;
    }
}
