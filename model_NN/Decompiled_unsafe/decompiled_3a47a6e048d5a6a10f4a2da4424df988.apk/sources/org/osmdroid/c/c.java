package org.osmdroid.c;

import com.a.a.a.a;
import com.agilebinary.mobilemonitor.client.android.h;
import org.osmdroid.b.f;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private f f361a;
    private int b;
    private int c;

    public c(f fVar, int i, int i2) {
        this.f361a = fVar;
        this.b = i;
        this.c = i2;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'e');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '@');
            i2 = i;
        }
        return new String(cArr);
    }

    public final String toString() {
        return h.I("i;H7V4._6Nxa+U-H;_e") + this.f361a + a.I("\u0012bF") + this.b + h.I("\u0016xCe") + this.c + a.I("\u001f");
    }
}
