package org.osmdroid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import com.agilebinary.mobilemonitor.client.android.a.a.g;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import org.a.a;

public class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final org.a.c f359a = a.a(c.class);
    private DisplayMetrics b;

    public c(Context context) {
        if (context != null) {
            this.b = context.getResources().getDisplayMetrics();
        }
    }

    private /* synthetic */ BitmapFactory.Options a() {
        try {
            Field declaredField = DisplayMetrics.class.getDeclaredField(g.I("[uQcVdFo[uYqJ|K"));
            Field declaredField2 = BitmapFactory.Options.class.getDeclaredField(org.b.a.a.c.I("&B\u000bI!_&X6"));
            Field declaredField3 = BitmapFactory.Options.class.getDeclaredField(g.I("v^KQmWzD[UqCvDf"));
            Field declaredField4 = DisplayMetrics.class.getDeclaredField(org.b.a.a.c.I("H*B<E;U\u000b\\&"));
            BitmapFactory.Options options = new BitmapFactory.Options();
            declaredField2.setInt(options, declaredField.getInt(null));
            declaredField3.setInt(options, declaredField4.getInt(this.b));
            return options;
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return null;
        }
    }

    private /* synthetic */ Bitmap b(e eVar) {
        Throwable th;
        InputStream inputStream;
        BitmapFactory.Options options = null;
        try {
            String str = eVar.name() + g.I("\u001eo^x");
            InputStream resourceAsStream = getClass().getResourceAsStream(str);
            if (resourceAsStream == null) {
                try {
                    throw new IllegalArgumentException(org.b.a.a.c.I("~*_ Y=O*\f!C;\f)C:B+\u0016o") + str);
                } catch (Throwable th2) {
                    InputStream inputStream2 = resourceAsStream;
                    th = th2;
                    inputStream = inputStream2;
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                        }
                    }
                    throw th;
                }
            } else {
                if (this.b != null) {
                    options = a();
                }
                Bitmap decodeStream = BitmapFactory.decodeStream(resourceAsStream, null, options);
                if (resourceAsStream != null) {
                    try {
                        resourceAsStream.close();
                    } catch (IOException e2) {
                    }
                }
                return decodeStream;
            }
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
        }
    }

    public final Drawable a(e eVar) {
        return new BitmapDrawable(b(eVar));
    }

    public final String a(a aVar, Object... objArr) {
        String I;
        switch (d.f362a[aVar.ordinal()]) {
            case 1:
                I = g.I("l]~Bz^{Um");
                break;
            case 2:
                I = org.b.a.a.c.I("a.\\!E$");
                break;
            case 3:
                I = g.I("\\I|\\z\u0010RQo");
                break;
            case 4:
                I = org.b.a.a.c.I("|:N#E,\f;^.B<\\ ^;");
                break;
            case 5:
                I = g.I("L}?R~Cz\u0010sQfUm");
                break;
            case 6:
                I = org.b.a.a.c.I("\u001bC?C(^.\\'E,");
                break;
            case 7:
                I = g.I("WYs\\l");
                break;
            case 8:
                I = org.b.a.a.c.I("o#C:H\u0002M+Io\u0004\u001cX.B+M=HoX&@*_f");
                break;
            case 9:
                I = g.I("\\\\pE{}~Tz\u00107CrQs\\?Dv\\zC6");
                break;
            case 10:
                I = org.b.a.a.c.I("a.\\>Y*_;");
                break;
            case 11:
                I = g.I("oUqvvUkCTQ~Bk\u0010pFzBsQf");
                break;
            case 12:
                I = org.b.a.a.c.I("b*X'I=@.B+_oN._*\f Z*^#M6");
                break;
            case 13:
                I = g.I("QUkXzBsQqTl\u0010m_~Tl\u0010pFzBsQf");
                break;
            case 14:
                I = org.b.a.a.c.I("\u001aB$B [!");
                break;
            case 15:
                I = g.I("\u0015l\u0010r");
                break;
            case 16:
                I = org.b.a.a.c.I("j_oG\"");
                break;
            case 17:
                I = g.I(":C?]v");
                break;
            case 18:
                I = org.b.a.a.c.I("j_oB\"");
                break;
            case 19:
                I = g.I(":C?Vk");
                break;
            default:
                throw new IllegalArgumentException();
        }
        return String.format(I, objArr);
    }
}
