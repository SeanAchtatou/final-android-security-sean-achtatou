package com.jasonkostempski.android.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.b.g;
import com.biige.client.android.R;
import java.util.Calendar;
import java.util.Date;

public class CalendarView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private m f300a = new k(this);
    private View.OnClickListener b = new j(this);
    private View.OnClickListener c = new i(this);
    private View.OnClickListener d = new g(this);
    private View.OnClickListener e = new h(this);
    private View.OnClickListener f = new f(this);
    private final int g = 5;
    private final int h = 4;
    private final int i = 3;
    private final int j = 2;
    private final int k = 1;
    private final int l = 0;
    /* access modifiers changed from: private */
    public a m;
    private LinearLayout n;
    private TableLayout o;
    private TableLayout p;
    private Button q;
    private Button r;
    /* access modifiers changed from: private */
    public Button s;
    private b t;
    private o u;
    /* access modifiers changed from: private */
    public int v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public int x;

    public CalendarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    static /* synthetic */ int a(CalendarView calendarView, int i2) {
        int i3 = calendarView.w + i2;
        calendarView.w = i3;
        return i3;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(int i2) {
        int i3 = 8;
        boolean z = false;
        if (this.v != i2) {
            this.v = i2;
            this.p.setVisibility(this.v == 4 ? 0 : 8);
            this.o.setVisibility(this.v == 3 ? 0 : 8);
            LinearLayout linearLayout = this.n;
            if (this.v == 2) {
                i3 = 0;
            }
            linearLayout.setVisibility(i3);
            Button button = this.q;
            if (this.v != 3) {
                z = true;
            }
            button.setEnabled(z);
            c();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.jasonkostempski.android.calendar.CalendarView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private /* synthetic */ void a(Context context) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.calendar, (ViewGroup) this, true);
        this.m = new a();
        this.n = (LinearLayout) inflate.findViewById(R.id.calendarpicker_days);
        this.o = (TableLayout) inflate.findViewById(R.id.calendarpicker_months);
        this.p = (TableLayout) inflate.findViewById(R.id.calendarpicker_years);
        this.q = (Button) inflate.findViewById(R.id.calendarpicker_up);
        this.r = (Button) inflate.findViewById(R.id.calendarpicker_previous);
        this.s = (Button) inflate.findViewById(R.id.calendarpicker_next);
        d();
        String[] d2 = this.m.d();
        int i2 = 0;
        while (i2 < 7) {
            ViewGroup viewGroup = (ViewGroup) this.n.getChildAt(i2);
            for (int i3 = 0; i3 < 7; i3++) {
                if (Boolean.valueOf(i2 == 0).booleanValue()) {
                    ((TextView) viewGroup.getChildAt(i3)).setText(d2[i3]);
                } else {
                    ((Button) viewGroup.getChildAt(i3)).setOnClickListener(this.c);
                }
            }
            i2++;
        }
        b();
        String[] e2 = this.m.e();
        int i4 = 0;
        int i5 = 0;
        while (i4 < 3) {
            TableRow tableRow = (TableRow) this.o.getChildAt(i4);
            int i6 = i5;
            for (int i7 = 0; i7 < 4; i7++) {
                TextView textView = (TextView) tableRow.getChildAt(i7);
                textView.setOnClickListener(this.d);
                textView.setText(e2[i6]);
                textView.setTag(Integer.valueOf(i6));
                i6++;
            }
            i4++;
            i5 = i6;
        }
        for (int i8 = 0; i8 < 3; i8++) {
            TableRow tableRow2 = (TableRow) this.p.getChildAt(i8);
            for (int i9 = 0; i9 < 4; i9++) {
                ((TextView) tableRow2.getChildAt(i9)).setOnClickListener(this.e);
            }
        }
        this.m.a(this.f300a);
        this.q.setOnClickListener(this.f);
        this.r.setOnClickListener(this.b);
        this.s.setOnClickListener(this.b);
        a(2);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        int[] f2 = this.m.f();
        int i2 = -1;
        int i3 = 0;
        int i4 = 0;
        int i5 = 1;
        while (i3 < f2.length) {
            int i6 = f2[i3] == 1 ? i2 + 1 : i2;
            TextView textView = (TextView) ((ViewGroup) this.n.getChildAt(i5)).getChildAt(i4);
            textView.setText(f2[i3] + "");
            boolean c2 = this.m.c(i6, f2[i3]);
            textView.setEnabled(c2);
            if (!c2) {
                textView.setTextColor(getResources().getColor(R.color.text_invalid));
            } else if (i6 == 0) {
                textView.setTextColor(getResources().getColor(R.color.text));
            } else {
                textView.setTextColor(getResources().getColor(R.color.text_gray));
            }
            textView.setTag(new int[]{i6, f2[i3]});
            i4++;
            if (i4 == 7) {
                i5++;
                i4 = 0;
            }
            i3++;
            i2 = i6;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c() {
        switch (this.v) {
            case 0:
                this.q.setText(g.I(";w7n-u;f%"));
                return;
            case 1:
                this.q.setText(this.m.a(q.I("9i9iP\f1a1a\\H\u0018\u0000\\U\u0005U\u0005")));
                return;
            case 2:
                this.q.setText(this.m.a(g.I("?n?nRZ\u000bZ\u000b")));
                return;
            case 3:
                this.q.setText(this.w + "");
                return;
            case 4:
                this.q.setText(g.I("6f1b6f-u;f%"));
                return;
            case 5:
                this.q.setText(q.I("o9b(y.u#z5i+"));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d() {
        this.w = this.m.a();
        this.x = this.m.b();
        this.m.c();
    }

    static /* synthetic */ void i(CalendarView calendarView) {
        if (calendarView.u != null) {
            calendarView.u.a();
        }
    }

    public final Calendar a() {
        return this.m.g();
    }

    public final void a(b bVar) {
        this.t = bVar;
    }

    public final void a(o oVar) {
        this.u = oVar;
    }

    public final void a(Calendar calendar) {
        this.m.a(calendar);
    }

    public final void a(Date date) {
        this.m.a(date);
        b();
    }
}
