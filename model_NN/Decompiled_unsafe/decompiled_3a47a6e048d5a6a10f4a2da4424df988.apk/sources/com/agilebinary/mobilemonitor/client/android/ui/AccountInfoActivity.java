package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.a.c.c.e;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.d;
import com.biige.client.android.R;

public class AccountInfoActivity extends BaseLoggedInActivity {

    /* renamed from: a  reason: collision with root package name */
    private static final String f190a = b.a();
    private Button b;
    private Button c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TextView h;
    private TextView i;
    private LinearLayout k;
    private ImageView l;
    private Animation m;
    private View n;
    private d o;

    public static void a(Activity activity) {
        activity.startActivity(new Intent(activity, AccountInfoActivity.class));
    }

    public static void a(Context context, d dVar) {
        Intent intent = new Intent(context, AccountInfoActivity.class);
        intent.putExtra(e.I("-y<s)~)b+"), dVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    private /* synthetic */ void a(d dVar) {
        this.o = dVar;
        this.d.setText(dVar.b());
        this.e.setText((dVar.e() == null || dVar.e().equals("")) ? getString(R.string.label_account_notset) : dVar.e());
        this.f.setText(dVar.f());
        this.h.setText(dVar.g() ? R.string.label_account_status_activated : R.string.label_account_status_deactivated);
        this.i.setText(dVar.a(this));
        this.n.setVisibility(0);
        this.k.setVisibility(8);
        this.l.clearAnimation();
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.accountinfo;
    }

    public void onCreate(Bundle bundle) {
        boolean z = true;
        super.onCreate(bundle);
        this.n = findViewById(R.id.accountinfo_main);
        this.n.setVisibility(8);
        this.k = (LinearLayout) findViewById(R.id.accountinfo_progress);
        this.l = (ImageView) findViewById(R.id.accountinfo_progress_anim);
        this.m = AnimationUtils.loadAnimation(this, R.anim.spinner_black_76);
        this.d = (TextView) findViewById(R.id.account_key);
        this.e = (TextView) findViewById(R.id.account_email);
        this.f = (TextView) findViewById(R.id.account_product);
        this.h = (TextView) findViewById(R.id.account_status);
        this.i = (TextView) findViewById(R.id.account_target);
        this.b = (Button) findViewById(R.id.accountinfo_changepassword);
        this.b.setEnabled(!this.g.d());
        this.b.setOnClickListener(new bc(this));
        this.c = (Button) findViewById(R.id.accountinfo_changeemail);
        Button button = this.c;
        if (this.g.d()) {
            z = false;
        }
        button.setEnabled(z);
        this.c.setOnClickListener(new bd(this));
        if (bundle != null) {
            this.o = (d) bundle.getSerializable(a.I("8t)~<s<o>"));
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        d dVar = (d) intent.getSerializableExtra(e.I("-y<s)~)b+"));
        b(false);
        if (dVar != null) {
            a(dVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putSerializable(a.I("8t)~<s<o>"), this.o);
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.o != null) {
            a(this.o);
        } else if (com.agilebinary.mobilemonitor.client.android.ui.a.a.f208a == null) {
            this.n.setVisibility(8);
            this.k.setVisibility(0);
            this.l.startAnimation(this.m);
            com.agilebinary.mobilemonitor.client.android.ui.a.a aVar = new com.agilebinary.mobilemonitor.client.android.ui.a.a(this);
            com.agilebinary.mobilemonitor.client.android.ui.a.a.f208a = aVar;
            aVar.execute(new Void[0]);
        }
    }
}
