package com.agilebinary.mobilemonitor.client.android;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.client.a.b.h;
import com.agilebinary.mobilemonitor.client.a.b.r;
import com.agilebinary.mobilemonitor.client.a.c;
import com.agilebinary.mobilemonitor.client.android.a.a.d;
import com.agilebinary.mobilemonitor.client.android.a.a.e;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.k;
import com.agilebinary.mobilemonitor.client.android.a.p;
import com.agilebinary.mobilemonitor.client.android.c.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.osmdroid.b.b.a;

public final class i implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final String f188a = b.a();
    private boolean b;
    private p c;
    private k d;
    private MyApplication e;
    private SQLiteStatement f;
    private SQLiteStatement g;
    private SQLiteDatabase h;
    private com.agilebinary.a.a.a.d.c.b i;
    private Map j = new HashMap();
    private String[] k;

    public i(MyApplication myApplication) {
        this.e = myApplication;
        this.c = myApplication.b().d();
        this.d = myApplication.b().e();
    }

    private static /* synthetic */ String a(h hVar) {
        return hVar.d() + com.agilebinary.mobilemonitor.client.android.b.g.I("_") + hVar.e() + a.I("5") + hVar.b() + com.agilebinary.mobilemonitor.client.android.b.g.I("_") + hVar.c();
    }

    private static /* synthetic */ String a(r rVar) {
        return rVar.b() + a.I("5") + rVar.c() + com.agilebinary.mobilemonitor.client.android.b.g.I("_") + rVar.d();
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x012a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ com.agilebinary.mobilemonitor.client.android.a.a.b b(com.agilebinary.mobilemonitor.client.a.b.h r12) {
        /*
            r11 = this;
            r9 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            int r1 = r12.e()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "8\\8"
            java.lang.String r1 = org.osmdroid.b.b.a.I(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = r12.b()
            java.lang.StringBuilder r0 = r0.append(r1)
            r0.toString()
            java.lang.String r10 = a(r12)
            java.util.Map r0 = r11.j
            java.lang.Object r0 = r0.get(r10)
            com.agilebinary.mobilemonitor.client.android.a.a.b r0 = (com.agilebinary.mobilemonitor.client.android.a.a.b) r0
            if (r0 != 0) goto L_0x00f8
            android.database.sqlite.SQLiteDatabase r0 = r11.h     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.c.f178a     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.client.android.c.k     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            r3.<init>()     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.j     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r4 = "O\u001cRB\u001cGR"
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.g.I(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.c     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r4 = "%L8\u0012v\u00178"
            java.lang.String r4 = org.osmdroid.b.b.a.I(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.d     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r4 = "O\u001cRB\u001cGR"
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.g.I(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.e     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r4 = "%L8\u0012v\u00178"
            java.lang.String r4 = org.osmdroid.b.b.a.I(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.f     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r4 = "\u001eM"
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.g.I(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            com.agilebinary.mobilemonitor.client.android.MyApplication r4 = r11.e     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            boolean r4 = r4.d()     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            if (r4 == 0) goto L_0x0114
            java.lang.String r4 = ")"
            java.lang.String r4 = org.osmdroid.b.b.a.I(r4)
            r5 = r4
        L_0x009f:
            r4 = 5
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            r6 = 0
            r4[r6] = r5     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            r5 = 1
            int r6 = r12.d()     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            r5 = 2
            int r6 = r12.e()     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            r5 = 3
            int r6 = r12.b()     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            r5 = 4
            int r6 = r12.c()     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x011e, all -> 0x012e }
            boolean r0 = r8.moveToNext()     // Catch:{ SQLiteException -> 0x0131, all -> 0x0134 }
            if (r0 == 0) goto L_0x011c
            com.agilebinary.mobilemonitor.client.android.a.a.b r1 = new com.agilebinary.mobilemonitor.client.android.a.a.b     // Catch:{ SQLiteException -> 0x0131, all -> 0x0134 }
            r0 = 0
            double r2 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x0131, all -> 0x0134 }
            r0 = 1
            double r4 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x0131, all -> 0x0134 }
            r0 = 2
            double r6 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x0131, all -> 0x0134 }
            r1.<init>(r2, r4, r6)     // Catch:{ SQLiteException -> 0x0131, all -> 0x0134 }
            r0 = r1
        L_0x00f3:
            if (r8 == 0) goto L_0x00f8
            r8.close()
        L_0x00f8:
            if (r0 == 0) goto L_0x00ff
            java.util.Map r1 = r11.j
            r1.put(r10, r0)
        L_0x00ff:
            r11.i = r9
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = ""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            r1.toString()
            return r0
        L_0x0114:
            java.lang.String r4 = "B"
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.g.I(r4)
            r5 = r4
            goto L_0x009f
        L_0x011c:
            r0 = r9
            goto L_0x00f3
        L_0x011e:
            r0 = move-exception
            r1 = r9
        L_0x0120:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x0126 }
            r2.<init>(r0)     // Catch:{ all -> 0x0126 }
            throw r2     // Catch:{ all -> 0x0126 }
        L_0x0126:
            r0 = move-exception
            r8 = r1
        L_0x0128:
            if (r8 == 0) goto L_0x012d
            r8.close()
        L_0x012d:
            throw r0
        L_0x012e:
            r0 = move-exception
            r8 = r9
            goto L_0x0128
        L_0x0131:
            r0 = move-exception
            r1 = r8
            goto L_0x0120
        L_0x0134:
            r0 = move-exception
            goto L_0x0128
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.i.b(com.agilebinary.mobilemonitor.client.a.b.h):com.agilebinary.mobilemonitor.client.android.a.a.b");
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0132  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ com.agilebinary.mobilemonitor.client.android.a.a.b b(com.agilebinary.mobilemonitor.client.a.b.r r12) {
        /*
            r11 = this;
            r9 = 0
            java.lang.String r10 = a(r12)
            java.util.Map r0 = r11.j
            java.lang.Object r0 = r0.get(r10)
            com.agilebinary.mobilemonitor.client.android.a.a.b r0 = (com.agilebinary.mobilemonitor.client.android.a.a.b) r0
            if (r0 != 0) goto L_0x0112
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r11.e     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            boolean r0 = r0.d()     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            if (r0 == 0) goto L_0x011c
            java.lang.String r0 = ")"
            java.lang.String r0 = org.osmdroid.b.b.a.I(r0)
        L_0x001d:
            r1 = 4
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r2 = 0
            r1[r2] = r0     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r0 = 1
            int r2 = r12.b()     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r1[r0] = r2     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r0 = 2
            int r2 = r12.c()     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r1[r0] = r2     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r0 = 3
            int r2 = r12.d()     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r1[r0] = r2     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r11.k = r1     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r0.<init>()     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r1 = "R\u0007F\u0000Z\u001bM\u0015\u00031g?bRW\u0013A\u001eF^\u0003\u0016F\u001fLO"
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.b.g.I(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String[] r1 = r11.k     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r1 = "8\u0011k\u0000q\u0017%"
            java.lang.String r1 = org.osmdroid.b.b.a.I(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String[] r1 = r11.k     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r2 = 1
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r1 = "RM\u001bGO"
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.b.g.I(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String[] r1 = r11.k     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r2 = 2
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r1 = "8\u0000a\u0000q\u0017%"
            java.lang.String r1 = org.osmdroid.b.b.a.I(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String[] r1 = r11.k     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r2 = 3
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r0.toString()     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            android.database.sqlite.SQLiteDatabase r0 = r11.h     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.b.f165a     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.client.android.b.j     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.i     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r4 = "O\u001cRB\u001cGR"
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.g.I(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.c     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r4 = "%L8\u0012v\u00178"
            java.lang.String r4 = org.osmdroid.b.b.a.I(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.d     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r4 = "O\u001cRB\u001cGR"
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.g.I(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.e     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r4 = "N'"
            java.lang.String r4 = org.osmdroid.b.b.a.I(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            java.lang.String[] r4 = r11.k     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0136 }
            boolean r0 = r8.moveToNext()     // Catch:{ SQLiteException -> 0x0139, all -> 0x013c }
            if (r0 == 0) goto L_0x0124
            com.agilebinary.mobilemonitor.client.android.a.a.b r1 = new com.agilebinary.mobilemonitor.client.android.a.a.b     // Catch:{ SQLiteException -> 0x0139, all -> 0x013c }
            r0 = 0
            double r2 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x0139, all -> 0x013c }
            r0 = 1
            double r4 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x0139, all -> 0x013c }
            r0 = 2
            double r6 = r8.getDouble(r0)     // Catch:{ SQLiteException -> 0x0139, all -> 0x013c }
            r1.<init>(r2, r4, r6)     // Catch:{ SQLiteException -> 0x0139, all -> 0x013c }
            r0 = r1
        L_0x010d:
            if (r8 == 0) goto L_0x0112
            r8.close()
        L_0x0112:
            if (r0 == 0) goto L_0x0119
            java.util.Map r1 = r11.j
            r1.put(r10, r0)
        L_0x0119:
            r11.i = r9
            return r0
        L_0x011c:
            java.lang.String r0 = "B"
            java.lang.String r0 = com.agilebinary.mobilemonitor.client.android.b.g.I(r0)
            goto L_0x001d
        L_0x0124:
            r0 = r9
            goto L_0x010d
        L_0x0126:
            r0 = move-exception
            r1 = r9
        L_0x0128:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x012e }
            r2.<init>(r0)     // Catch:{ all -> 0x012e }
            throw r2     // Catch:{ all -> 0x012e }
        L_0x012e:
            r0 = move-exception
            r8 = r1
        L_0x0130:
            if (r8 == 0) goto L_0x0135
            r8.close()
        L_0x0135:
            throw r0
        L_0x0136:
            r0 = move-exception
            r8 = r9
            goto L_0x0130
        L_0x0139:
            r0 = move-exception
            r1 = r8
            goto L_0x0128
        L_0x013c:
            r0 = move-exception
            goto L_0x0130
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.i.b(com.agilebinary.mobilemonitor.client.a.b.r):com.agilebinary.mobilemonitor.client.android.a.a.b");
    }

    public final void a() {
        synchronized (this) {
            this.h = new a(this, this.e, a.I("\u001fw\u0010y\u0007q\u001cv,{\u0012{\u001b}")).getWritableDatabase();
            this.f = this.h.compileStatement(String.format(com.agilebinary.mobilemonitor.client.android.b.g.I("j<p7q&\u0003;m&lR\u0006C\u0007\u0001\u0003Z\u0006@\u0007\u0001\u000fW\u0010VP^\u0006F\u0007\u0001\u000fW\u0016VP^\u0006D\u0007\u0001\u000fW\u0014VP^\u0006J\u0007\u0001\u000fW\u001aVP^\u0006C\u0013VP[\u0003\u0004B\u001eV\u0017PR\u000bM\u000fM\u000fM\u000fM\u000fM\u000fM\u000fM\u000fM\u000fM\nR"), c.f178a, c.b, c.j, c.c, c.d, c.e, c.f, c.g, c.h, c.i));
            this.g = this.h.compileStatement(String.format(a.I(":V ]!LSQ=L<8V)WkS0V*Wk_=@<\u00004V,Wk_=F<\u00004V.Wk_=D<\u00004V Wk_=J<\u00001Sn\u0012t\u0006}\u00008['_'_'_'_'_'_'_'Z8"), b.f165a, b.b, b.i, b.d, b.c, b.e, b.f, b.g, b.h));
            SQLiteStatement compileStatement = this.h.compileStatement(String.format(com.agilebinary.mobilemonitor.client.android.b.g.I("g7o7w7\u00034q=nR\u0006C\u0007\u0001\u0003RT\u001aF\u0000FR\u0006@\u0007\u0001\u001fM"), c.f178a, c.b));
            SQLiteStatement compileStatement2 = this.h.compileStatement(String.format(a.I("7]?]']S^!W>8V)WkS8\u0004p\u0016j\u00168V*WkO'"), b.f165a, b.b));
            compileStatement.bindLong(1, System.currentTimeMillis() - 1209600000);
            compileStatement.execute();
            compileStatement.close();
            compileStatement2.bindLong(1, System.currentTimeMillis() - 1209600000);
            compileStatement2.execute();
            compileStatement2.close();
        }
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        this.i = bVar;
    }

    public final void a(List list) {
        synchronized (this) {
            this.c = this.e.b().d();
            this.b = false;
            try {
                ArrayList arrayList = new ArrayList();
                HashSet hashSet = new HashSet();
                HashSet<c> hashSet2 = new HashSet<>();
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    c cVar = (c) it.next();
                    if (cVar.a() && (cVar instanceof h)) {
                        h hVar = (h) cVar;
                        com.agilebinary.mobilemonitor.client.android.a.a.b b2 = b(hVar);
                        if (b2 == null) {
                            String a2 = a(hVar);
                            if (!hashSet.contains(a2)) {
                                arrayList.add(new com.agilebinary.mobilemonitor.client.android.a.a.c(hVar.d(), hVar.e(), hVar.b(), hVar.c()));
                                hashSet.add(a2);
                            }
                            hashSet2.add(cVar);
                        } else {
                            cVar.a(b2);
                        }
                    }
                }
                if (arrayList.size() > 0) {
                    List<com.agilebinary.mobilemonitor.client.android.a.a.a> a3 = this.c.a(this.e.a(), arrayList, this);
                    if (!this.b && a3 != null) {
                        for (com.agilebinary.mobilemonitor.client.android.a.a.a aVar : a3) {
                            com.agilebinary.mobilemonitor.client.android.a.a.b b3 = aVar.b();
                            "" + b3;
                            this.f.bindLong(1, System.currentTimeMillis());
                            this.f.bindLong(2, this.e.d() ? 1 : 0);
                            this.f.bindLong(3, (long) aVar.c());
                            this.f.bindLong(4, (long) aVar.d());
                            this.f.bindLong(5, (long) aVar.e());
                            this.f.bindLong(6, (long) aVar.f());
                            this.f.bindDouble(7, b3.a());
                            this.f.bindDouble(8, b3.b());
                            this.f.bindDouble(9, b3.c());
                            this.f.executeInsert();
                        }
                        for (c cVar2 : hashSet2) {
                            if (cVar2 instanceof h) {
                                cVar2.a(b((h) cVar2));
                            }
                        }
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                HashSet hashSet3 = new HashSet();
                HashSet<c> hashSet4 = new HashSet<>();
                Iterator it2 = list.iterator();
                while (it2.hasNext()) {
                    c cVar3 = (c) it2.next();
                    if (cVar3.a() && (cVar3 instanceof r)) {
                        r rVar = (r) cVar3;
                        com.agilebinary.mobilemonitor.client.android.a.a.b b4 = b(rVar);
                        "" + rVar.b();
                        if (b4 == null) {
                            String a4 = a(rVar);
                            if (!hashSet3.contains(a4)) {
                                arrayList2.add(new d(rVar.b(), rVar.c(), rVar.d()));
                                hashSet3.add(a4);
                            }
                            hashSet4.add(cVar3);
                        } else {
                            cVar3.a(b4);
                        }
                    }
                }
                if (arrayList2.size() > 0) {
                    List<e> a5 = this.d.a(this.e.a(), arrayList2, this);
                    if (!this.b && a5 != null) {
                        for (e eVar : a5) {
                            com.agilebinary.mobilemonitor.client.android.a.a.b e2 = eVar.e();
                            "" + e2;
                            this.g.bindLong(1, System.currentTimeMillis());
                            this.g.bindLong(2, this.e.d() ? 1 : 0);
                            this.g.bindLong(3, (long) eVar.c());
                            this.g.bindLong(4, (long) eVar.b());
                            this.g.bindLong(5, (long) eVar.d());
                            this.g.bindDouble(6, e2.a());
                            this.g.bindDouble(7, e2.b());
                            this.g.bindDouble(8, e2.c());
                            this.g.executeInsert();
                        }
                        for (c cVar4 : hashSet4) {
                            if (cVar4 instanceof r) {
                                cVar4.a(b((r) cVar4));
                            }
                        }
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return;
    }

    public final boolean a(c cVar) {
        com.agilebinary.mobilemonitor.client.android.a.a.b bVar = null;
        try {
            if (cVar instanceof r) {
                bVar = b((r) cVar);
            }
            if (cVar instanceof h) {
                bVar = b((h) cVar);
            }
            if (bVar == null) {
                return false;
            }
            cVar.a(bVar);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b() {
        /*
            r1 = this;
            monitor-enter(r1)
            android.database.sqlite.SQLiteStatement r0 = r1.f     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            android.database.sqlite.SQLiteStatement r0 = r1.g     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            android.database.sqlite.SQLiteDatabase r0 = r1.h     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
        L_0x0010:
            monitor-exit(r1)
            return
        L_0x0012:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0015:
            r0 = move-exception
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.i.b():void");
    }

    public final void c() {
        synchronized (this) {
            this.b = true;
            if (this.i != null) {
                try {
                    this.i.d();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        return;
    }
}
