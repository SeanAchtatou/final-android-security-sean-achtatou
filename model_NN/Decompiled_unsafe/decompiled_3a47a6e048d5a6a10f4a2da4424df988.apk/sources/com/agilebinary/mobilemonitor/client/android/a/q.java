package com.agilebinary.mobilemonitor.client.android.a;

import java.io.Serializable;

public final class q extends Exception implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private int f161a;
    private Exception b;

    public q() {
    }

    public q(int i) {
        this.f161a = i;
    }

    public q(Exception exc) {
        this.b = exc;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ '|');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ ',');
            i2 = i;
        }
        return new String(cArr);
    }

    public final boolean a() {
        return s.a(this.f161a);
    }

    public final boolean b() {
        return this.b != null;
    }

    public final boolean c() {
        return s.b(this.f161a);
    }

    public final boolean d() {
        return s.c(this.f161a);
    }
}
