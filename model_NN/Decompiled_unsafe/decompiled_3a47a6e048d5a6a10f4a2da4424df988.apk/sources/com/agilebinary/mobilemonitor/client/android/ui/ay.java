package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.biige.client.android.R;

final class ay extends ArrayAdapter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ LoginActivity f239a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ay(LoginActivity loginActivity, Context context) {
        super(context, (int) R.layout.locale_row, new String[LoginActivity.j.length]);
        this.f239a = loginActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private /* synthetic */ View a(int i, ViewGroup viewGroup) {
        View inflate = this.f239a.getLayoutInflater().inflate((int) R.layout.locale_row, viewGroup, false);
        ((TextView) inflate.findViewById(R.id.locale_row_label)).setText(LoginActivity.j[i].f241a.getDisplayLanguage());
        ((ImageView) inflate.findViewById(R.id.locale_row_image)).setImageResource(LoginActivity.j[i].b);
        return inflate;
    }

    public final View getDropDownView(int i, View view, ViewGroup viewGroup) {
        return a(i, viewGroup);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        return a(i, viewGroup);
    }
}
