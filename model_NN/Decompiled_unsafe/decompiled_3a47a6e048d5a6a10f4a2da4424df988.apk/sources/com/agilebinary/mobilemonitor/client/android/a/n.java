package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.a.a.a.j;
import com.agilebinary.mobilemonitor.a.a.a.a;
import com.agilebinary.mobilemonitor.client.android.c.b;
import java.io.IOException;
import java.util.TimerTask;

public final class n extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    private static String f160a = b.a();
    private com.agilebinary.a.a.a.d.c.b b;
    private long c;

    private /* synthetic */ n(long j) {
        this.c = j;
    }

    private /* synthetic */ j a(l lVar, com.agilebinary.a.a.a.d.c.b bVar) {
        this.b = bVar;
        lVar.c().schedule(this, this.c);
        try {
            j a2 = lVar.a(bVar);
            this.b = null;
            cancel();
            return a2;
        } catch (IOException e) {
            a.d(e);
            try {
                bVar.d();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            cancel();
            lVar.b();
            throw e;
        }
    }

    public static j a(l lVar, com.agilebinary.a.a.a.d.c.b bVar, long j) {
        return new n(300000).a(lVar, bVar);
    }

    public final void run() {
        if (this.b != null) {
            try {
                this.b.d();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
