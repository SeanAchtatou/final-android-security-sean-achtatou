package com.agilebinary.mobilemonitor.client.android.b;

import android.content.Context;
import android.content.SharedPreferences;

public abstract class g {

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f172a;

    public g(Context context, String str) {
        this.f172a = context.getSharedPreferences(str, 0);
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'r');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '#');
            i2 = i;
        }
        return new String(cArr);
    }

    public final boolean a(String str, boolean z) {
        return this.f172a.getBoolean(str, z);
    }

    public final void b(String str, boolean z) {
        SharedPreferences.Editor edit = this.f172a.edit();
        edit.putBoolean(str, z);
        edit.commit();
    }
}
