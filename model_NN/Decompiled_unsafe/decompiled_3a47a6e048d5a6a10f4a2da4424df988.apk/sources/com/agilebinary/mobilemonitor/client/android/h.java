package com.agilebinary.mobilemonitor.client.android;

import org.b.a.a.c;
import org.osmdroid.b.a;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    public byte f187a;
    public long b;
    public long c;
    private boolean d;

    public h(byte b2, long j, long j2) {
        this.f187a = b2;
        this.b = j;
        this.c = j2;
        this.d = j2 == 0;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'X');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ ':');
            i2 = i;
        }
        return new String(cArr);
    }

    public final String toString() {
        return a.I("\u001b:;\"*\u000f1\"*)08\f)/9;?*l\u0005)()08\n5.)c") + ((int) this.f187a) + c.I("c\f;E\"I\t^ Ar") + this.b + a.I("rl*%3)\n#c") + this.c + c.I("\u0000oC!@6b*[\nZ*B;_r") + this.d + a.I("\u0003");
    }
}
