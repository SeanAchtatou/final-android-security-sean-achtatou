package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.d;
import com.biige.client.android.R;
import java.util.Iterator;
import java.util.List;
import org.osmdroid.b.a;
import org.osmdroid.b.a.e;
import org.osmdroid.b.f;
import org.osmdroid.c;

public final class j extends e {

    /* renamed from: a  reason: collision with root package name */
    private Paint f285a = new Paint();
    private Path b;
    private List c;
    private int d = -1;
    private f e;
    private int f;
    private double g = 30.0d;
    private double h = 10.0d;
    private double i = 5.0d;

    public j(Context context, f fVar) {
        super(new c(context));
        this.e = fVar;
        this.f285a.setColor(context.getResources().getColor(R.color.directions_line));
        this.f285a.setStrokeWidth(Float.parseFloat(context.getString(R.string.directions_line_width)));
        this.f285a.setStyle(Paint.Style.STROKE);
        this.f285a.setAntiAlias(true);
        this.g = Double.parseDouble(context.getString(R.string.directions_line_arrow_spacing));
        this.h = Double.parseDouble(context.getString(R.string.directions_line_arrow_head_length));
        this.i = Double.parseDouble(context.getString(R.string.directions_line_min_length));
    }

    private /* synthetic */ void a(double d2, double d3, double d4, double d5) {
        this.f++;
        float f2 = (float) (0.7853981633974483d + d4);
        float f3 = ((double) f2) > 3.141592653589793d ? (float) (((double) f2) - 6.283185307179586d) : f2;
        float f4 = (float) (d4 - 0.7853981633974483d);
        if (((double) f4) <= -3.141592653589793d) {
            f4 = (float) (((double) f4) + 6.283185307179586d);
        }
        float sin = (float) ((Math.sin((double) f3) * d5) + d3);
        float cos = (float) (d2 - (Math.cos((double) f4) * d5));
        float sin2 = (float) ((Math.sin((double) f4) * d5) + d3);
        this.b.moveTo((float) (d2 - (Math.cos((double) f3) * d5)), sin);
        this.b.lineTo((float) d2, (float) d3);
        this.b.lineTo(cos, sin2);
    }

    private /* synthetic */ void a(f fVar) {
        double d2;
        a e2 = fVar.e();
        this.f = 0;
        Point point = new Point();
        Point point2 = new Point();
        e2.a(((d) this.c.get(0)).h(), point);
        int i2 = 1;
        while (true) {
            int i3 = i2;
            if (i3 < this.c.size()) {
                e2.a(((d) this.c.get(i3)).h(), point2);
                double d3 = (double) (point2.x - point.x);
                double d4 = (double) (point2.y - point.y);
                double sqrt = Math.sqrt((d3 * d3) + (d4 * d4));
                if (sqrt > this.i) {
                    double atan2 = Math.atan2(-d4, d3);
                    boolean z = this.f > 1500;
                    if (this.g == 0.0d) {
                        a((double) point2.x, (double) point2.y, atan2, this.h);
                        return;
                    } else if (this.g == 1.0d || z) {
                        a(((sqrt / 2.0d) * Math.cos(atan2)) + ((double) point.x), ((double) point.y) - ((sqrt / 2.0d) * Math.sin(atan2)), atan2, this.h);
                    } else {
                        double d5 = this.g;
                        while (true) {
                            if (d5 >= sqrt) {
                                d2 = d5;
                                break;
                            }
                            a((Math.cos(atan2) * d5) + ((double) point.x), ((double) point.y) - (Math.sin(atan2) * d5), atan2, this.h);
                            d2 = this.g + d5;
                            if (this.f > 1500) {
                                break;
                            }
                            d5 = d2;
                        }
                        if (d2 == this.g) {
                            a(((sqrt / 2.0d) * Math.cos(atan2)) + ((double) point.x), ((double) point.y) - ((sqrt / 2.0d) * Math.sin(atan2)), atan2, this.h);
                        }
                    }
                }
                point.set(point2.x, point2.y);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas) {
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas, f fVar) {
        boolean z;
        int i2 = 0;
        if (this.c.size() >= 2) {
            a e2 = fVar.e();
            if (this.d != e2.c()) {
                this.d = e2.c();
                z = true;
            } else {
                z = false;
            }
            if (z) {
                this.b = new Path();
                Point point = new Point();
                Iterator it = this.c.iterator();
                while (true) {
                    int i3 = i2;
                    if (!it.hasNext()) {
                        break;
                    }
                    e2.a(((d) ((s) it.next())).h(), point);
                    if (i3 == 0) {
                        this.b.moveTo((float) point.x, (float) point.y);
                    } else {
                        this.b.lineTo((float) point.x, (float) point.y);
                    }
                    i2 = i3 + 1;
                }
                a(fVar);
            }
            canvas.drawPath(this.b, this.f285a);
        }
    }

    public final void a(List list) {
        this.c = list;
    }
}
