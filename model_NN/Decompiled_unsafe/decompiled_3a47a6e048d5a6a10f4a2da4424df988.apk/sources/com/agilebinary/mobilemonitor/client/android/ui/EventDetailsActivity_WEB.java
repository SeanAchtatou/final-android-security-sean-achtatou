package com.agilebinary.mobilemonitor.client.android.ui;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;
import com.agilebinary.a.a.a.h.b.g;
import com.agilebinary.a.a.a.k;
import com.agilebinary.mobilemonitor.client.a.b.e;
import com.agilebinary.mobilemonitor.client.a.b.i;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.android.c.a;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.biige.client.android.R;

public class EventDetailsActivity_WEB extends EventDetailsActivity_base {

    /* renamed from: a  reason: collision with root package name */
    private TextView f199a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TextView h;
    private TextView i;
    private TableRow k;
    private TableRow l;
    private TableRow m;
    private TableRow n;
    private TableRow o;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService(g.I("\u0001\u0014\u0014\u001a\u0018\u00012\u001c\u0003\u0013\u0001\u0014\u0019\u0010\u001f"))).inflate((int) R.layout.eventdetails_web, viewGroup, true);
        this.f199a = (TextView) findViewById(R.id.eventdetails_web_kind);
        this.b = (TextView) findViewById(R.id.eventdetails_web_time);
        this.c = (TextView) findViewById(R.id.eventdetails_web_keywords);
        this.d = (TextView) findViewById(R.id.eventdetails_web_domain);
        this.e = (TextView) findViewById(R.id.eventdetails_web_title);
        this.f = (TextView) findViewById(R.id.eventdetails_web_numvisits);
        this.h = (TextView) findViewById(R.id.eventdetails_web_bookmarked);
        this.i = (TextView) findViewById(R.id.eventdetails_web_speed);
        this.k = (TableRow) findViewById(R.id.eventdetails_web_row_keywords);
        this.l = (TableRow) findViewById(R.id.eventdetails_web_row_address);
        this.m = (TableRow) findViewById(R.id.eventdetails_web_row_title);
        this.n = (TableRow) findViewById(R.id.eventdetails_web_row_numvisits);
        this.o = (TableRow) findViewById(R.id.eventdetails_web_row_bookmarked);
        this.d.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        super.a(oVar);
        if (oVar instanceof e) {
            this.f199a.setText((int) R.string.label_event_web_kind_search);
            this.k.setVisibility(0);
            this.l.setVisibility(8);
            this.m.setVisibility(8);
            this.n.setVisibility(8);
            this.o.setVisibility(8);
            e eVar = (e) oVar;
            this.c.setText(eVar.b());
            this.b.setText(c.a().c(eVar.a()));
        } else if (oVar instanceof com.agilebinary.mobilemonitor.client.a.b.g) {
            this.f199a.setText((int) R.string.label_event_web_kind_visit);
            this.k.setVisibility(8);
            this.l.setVisibility(0);
            this.m.setVisibility(0);
            this.n.setVisibility(0);
            this.o.setVisibility(0);
            com.agilebinary.mobilemonitor.client.a.b.g gVar = (com.agilebinary.mobilemonitor.client.a.b.g) oVar;
            this.d.setText(Html.fromHtml(k.I("_0C9\u00114\u0005lA") + gVar.b() + g.I("OUS") + gVar.b() + k.I("mL0]")));
            this.e.setText(gVar.c());
            this.f.setText(String.valueOf(gVar.e()));
            this.h.setText(getString(gVar.d() ? R.string.label_yes : R.string.label_no));
            this.b.setText(c.a().c(gVar.a()));
        }
        i iVar = (i) oVar;
        this.i.setText(a.a(this, iVar));
        if (a.a(iVar)) {
            this.i.setTextColor(getResources().getColor(R.color.warning));
        } else {
            this.i.setTextColor(getResources().getColor(R.color.text));
        }
    }
}
