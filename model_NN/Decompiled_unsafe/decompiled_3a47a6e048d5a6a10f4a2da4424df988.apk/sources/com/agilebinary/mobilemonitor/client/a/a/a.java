package com.agilebinary.mobilemonitor.client.a.a;

import com.agilebinary.a.a.a.h.f;
import com.agilebinary.a.a.a.i.d;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class a extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private volatile byte[] f123a;
    private int b;
    private int c;
    private int d = -1;
    private int e;
    private boolean f = false;

    public a(InputStream inputStream, byte[] bArr) {
        super(inputStream);
        this.f123a = bArr;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 14);
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '\\');
            i2 = i;
        }
        return new String(cArr);
    }

    private /* synthetic */ int a() {
        int i;
        int i2 = 0;
        if (this.d == -1 || this.e - this.d >= this.c) {
            i = this.in.read(this.f123a);
            if (i > 0) {
                this.d = -1;
                this.e = 0;
                if (i != -1) {
                    i2 = i;
                }
                this.b = i2;
            }
        } else {
            if (this.d == 0 && this.c > this.f123a.length) {
                int length = this.f123a.length * 2;
                if (length > this.c) {
                    length = this.c;
                }
                byte[] bArr = new byte[length];
                System.arraycopy(this.f123a, 0, bArr, 0, this.f123a.length);
                this.f123a = bArr;
            } else if (this.d > 0) {
                System.arraycopy(this.f123a, this.d, this.f123a, 0, this.f123a.length - this.d);
            }
            this.e -= this.d;
            this.d = 0;
            this.b = 0;
            i = this.in.read(this.f123a, this.e, this.f123a.length - this.e);
            this.b = i <= 0 ? this.e : this.e + i;
        }
        return i;
    }

    public final int available() {
        int i;
        int i2;
        int available;
        synchronized (this) {
            if (this.f123a == null) {
                throw new IOException(d.I("L(m9~1?5l||0p/z8"));
            }
            i = this.b;
            i2 = this.e;
            available = this.in.available();
        }
        return (i - i2) + available;
    }

    public final void close() {
        synchronized (this) {
            if (this.in != null) {
                super.close();
                this.in = null;
            }
            this.f123a = null;
            this.f = true;
        }
    }

    public final void mark(int i) {
        synchronized (this) {
            this.c = i;
            this.d = this.e;
        }
    }

    public final boolean markSupported() {
        return true;
    }

    public final int read() {
        byte b2 = -1;
        synchronized (this) {
            if (this.in == null) {
                throw new IOException(f.I("f\rG\u001cT\u0014\u0015\u0010FYV\u0015Z\nP\u001d"));
            } else if (this.e < this.b || a() != -1) {
                if (this.b - this.e > 0) {
                    byte[] bArr = this.f123a;
                    int i = this.e;
                    this.e = i + 1;
                    b2 = bArr[i] & 255;
                }
            }
        }
        return b2;
    }

    public final int read(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        synchronized (this) {
            if (this.f) {
                throw new IOException(d.I("L(m9~1?5l||0p/z8"));
            } else if (bArr == null) {
                throw new NullPointerException(f.I("2\u0005I\u0001N"));
            } else if ((i | i2) < 0 || i > bArr.length - i2) {
                throw new IndexOutOfBoundsException(d.I("\u0017/l-:"));
            } else if (i2 == 0) {
                i2 = 0;
            } else if (this.f123a == null) {
                throw new IOException(f.I("2\u0005I\u0000@"));
            } else {
                if (this.e < this.b) {
                    int i5 = this.b - this.e >= i2 ? i2 : this.b - this.e;
                    System.arraycopy(this.f123a, this.e, bArr, i, i5);
                    this.e += i5;
                    if (i5 == i2 || this.in.available() == 0) {
                        i2 = i5;
                    } else {
                        i += i5;
                        i3 = i2 - i5;
                    }
                } else {
                    i3 = i2;
                }
                while (true) {
                    if (this.d == -1 && i3 >= this.f123a.length) {
                        i4 = this.in.read(bArr, i, i3);
                        if (i4 == -1) {
                            i2 = i3 == i2 ? -1 : i2 - i3;
                        }
                    } else if (a() == -1) {
                        i2 = i3 == i2 ? -1 : i2 - i3;
                    } else {
                        i4 = this.b - this.e >= i3 ? i3 : this.b - this.e;
                        System.arraycopy(this.f123a, this.e, bArr, i, i4);
                        this.e += i4;
                    }
                    i3 -= i4;
                    if (i3 == 0) {
                        break;
                    } else if (this.in.available() == 0) {
                        i2 -= i3;
                        break;
                    } else {
                        i += i4;
                    }
                }
            }
        }
        return i2;
    }

    public final void reset() {
        synchronized (this) {
            if (this.f) {
                throw new IOException(d.I("L(m9~1?5l||0p/z8"));
            } else if (-1 == this.d) {
                throw new IOException(f.I("x\u0018G\u0012\u0015\u0011T\n\u0015\u001bP\u001c[Y\\\u0017C\u0018Y\u0010Q\u0018A\u001cQW"));
            } else {
                this.e = this.d;
            }
        }
    }

    public final long skip(long j) {
        synchronized (this) {
            if (this.in == null) {
                throw new IOException(d.I("\u0017/l*e"));
            } else if (j < 1) {
                j = 0;
            } else if (((long) (this.b - this.e)) >= j) {
                this.e = (int) (((long) this.e) + j);
            } else {
                long j2 = (long) (this.b - this.e);
                this.e = this.b;
                if (this.d != -1) {
                    if (j > ((long) this.c)) {
                        this.d = -1;
                    } else if (a() == -1) {
                        j = j2;
                    } else if (((long) (this.b - this.e)) >= j - j2) {
                        this.e = (int) ((j - j2) + ((long) this.e));
                    } else {
                        j = j2 + ((long) (this.b - this.e));
                        this.e = this.b;
                    }
                }
                j = j2 + this.in.skip(j - j2);
            }
        }
        return j;
    }
}
