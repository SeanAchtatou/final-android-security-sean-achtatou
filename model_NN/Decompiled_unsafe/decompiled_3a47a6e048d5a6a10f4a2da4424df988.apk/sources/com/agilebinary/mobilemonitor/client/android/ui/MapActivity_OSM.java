package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.ui.map.d;
import com.agilebinary.mobilemonitor.client.android.ui.map.g;
import com.agilebinary.mobilemonitor.client.android.ui.map.j;
import com.agilebinary.mobilemonitor.client.android.ui.map.l;
import com.biige.client.android.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.osmdroid.b.a.a;
import org.osmdroid.b.b;
import org.osmdroid.b.f;
import org.osmdroid.c;
import org.osmdroid.util.BoundingBoxE6;

public class MapActivity_OSM extends BaseLoggedInActivity implements a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public RelativeLayout f205a;
    /* access modifiers changed from: private */
    public f b;
    /* access modifiers changed from: private */
    public c c;
    /* access modifiers changed from: private */
    public b d;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.client.android.ui.map.b e;
    /* access modifiers changed from: private */
    public j f;
    /* access modifiers changed from: private */
    public l h;
    /* access modifiers changed from: private */
    public g i;
    /* access modifiers changed from: private */
    public List k;
    /* access modifiers changed from: private */
    public m l;
    private TextView m;
    private TextView n;
    private TextView o;
    private TextView p;
    private TextView q;
    private ImageButton r;
    private ImageButton s;
    private ImageButton t;
    private ImageButton u;
    private s v;
    private TextView w;
    /* access modifiers changed from: private */
    public l x;
    private com.agilebinary.mobilemonitor.client.android.ui.map.a y = new bs(this);
    /* access modifiers changed from: private */
    public d z;

    /* access modifiers changed from: private */
    public /* synthetic */ void a(s sVar) {
        boolean z2 = true;
        if (sVar != null) {
            this.v = sVar;
            this.i.a(this.k.indexOf(sVar));
            this.m.setText(com.agilebinary.mobilemonitor.client.android.c.c.a().d(this.v.u()));
            this.q.setText(getString(R.string.label_event_location_accuracy_short, new Object[]{sVar.f()}));
            CharSequence a2 = ((com.agilebinary.mobilemonitor.client.a.d) sVar).a(this);
            this.p.setText(a2);
            this.w.setVisibility(a2.length() == 0 ? 4 : 0);
            this.n.setText(sVar.b(this));
            this.o.setText(sVar.c(this));
            this.s.setEnabled(this.k.indexOf(this.v) < this.k.size() + -1);
            this.r.setEnabled(this.k.indexOf(this.v) > 0);
            this.t.setEnabled(!(this.k.indexOf(this.v) == 0));
            ImageButton imageButton = this.u;
            if (this.k.indexOf(this.v) == this.k.size() + -1) {
                z2 = false;
            }
            imageButton.setEnabled(z2);
            this.b.invalidate();
        }
    }

    static /* synthetic */ void l(MapActivity_OSM mapActivity_OSM) {
        ArrayList arrayList = new ArrayList();
        for (s sVar : mapActivity_OSM.k) {
            com.agilebinary.mobilemonitor.client.a.d dVar = (com.agilebinary.mobilemonitor.client.a.d) sVar;
            if (dVar.j()) {
                arrayList.add(dVar.h());
            }
        }
        BoundingBoxE6 a2 = BoundingBoxE6.a(arrayList);
        if (arrayList.size() > 0) {
            if (a2.b() > 10) {
                mapActivity_OSM.d.a(a2);
            } else {
                mapActivity_OSM.d.a(10000, 10000);
            }
            mapActivity_OSM.d.a(a2.a());
        }
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.map;
    }

    public final /* bridge */ /* synthetic */ boolean a(Object obj) {
        aw awVar = (aw) obj;
        if (this.z == null) {
            s sVar = awVar.c;
            ArrayList arrayList = new ArrayList();
            Point b2 = this.b.e().b(((com.agilebinary.mobilemonitor.client.a.d) sVar).h(), null);
            Point point = new Point();
            Iterator it = this.k.iterator();
            while (true) {
                Point point2 = point;
                if (!it.hasNext()) {
                    break;
                }
                s sVar2 = (s) it.next();
                com.agilebinary.mobilemonitor.client.a.d dVar = (com.agilebinary.mobilemonitor.client.a.d) sVar2;
                if (dVar != sVar && dVar.j()) {
                    point2 = this.b.e().b(dVar.h(), point2);
                    if (Math.sqrt(Math.pow((double) (b2.x - point2.x), 2.0d) + Math.pow((double) (b2.y - point2.y), 2.0d)) < 30.0d) {
                        arrayList.add(sVar2);
                    }
                }
                point = point2;
            }
            if (arrayList.size() == 0) {
                a(awVar.c);
            } else {
                arrayList.add(0, awVar.c);
                this.z = new d(this, this.y);
                this.z.setCancelable(true);
                this.z.a(arrayList);
                this.z.show();
                return false;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        a((s) this.k.get(this.k.indexOf(this.v) + 1));
    }

    /* access modifiers changed from: protected */
    public final void f() {
        a((s) this.k.get(this.k.indexOf(this.v) - 1));
    }

    /* access modifiers changed from: protected */
    public final void g() {
        a((s) this.k.get(0));
    }

    /* access modifiers changed from: protected */
    public final void h() {
        a((s) this.k.get(this.k.size() - 1));
    }

    /* access modifiers changed from: protected */
    public final boolean j() {
        return false;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.l = m.a(this);
        this.f205a = (RelativeLayout) findViewById(R.id.map_main);
        this.b = new f(this);
        this.c = new c(this);
        this.d = this.b.b();
        this.f205a.addView(this.b, new LinearLayout.LayoutParams(-1, -1));
        this.m = (TextView) findViewById(R.id.eventdetails_navigation_text);
        this.n = (TextView) findViewById(R.id.map_curevent_source);
        this.o = (TextView) findViewById(R.id.map_curevent_details);
        this.p = (TextView) findViewById(R.id.map_curevent_speed);
        this.w = (TextView) findViewById(R.id.map_curevent_label_speed);
        this.q = (TextView) findViewById(R.id.map_curevent_accuracy);
        this.r = (ImageButton) findViewById(R.id.eventdetails_navigation_prev);
        this.s = (ImageButton) findViewById(R.id.eventdetails_navigation_next);
        this.t = (ImageButton) findViewById(R.id.eventdetails_navigation_first);
        this.u = (ImageButton) findViewById(R.id.eventdetails_navigation_last);
        this.r.setEnabled(false);
        this.s.setEnabled(false);
        this.t.setEnabled(false);
        this.u.setEnabled(false);
        this.r.setOnClickListener(new bn(this));
        this.s.setOnClickListener(new bm(this));
        this.t.setOnClickListener(new bl(this));
        this.u.setOnClickListener(new bk(this));
        List list = (List) getIntent().getSerializableExtra(org.osmdroid.c.c.I(" \u00181\u0012$\u001f \u0016 \u000e1\u001f,\u00046"));
        Collections.sort(list);
        this.x = new l(this);
        this.x.execute(list);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        if (i2 != 0) {
            return null;
        }
        Dialog dialog = new Dialog(this);
        dialog.setContentView((int) R.layout.map_menu_dialog);
        dialog.setTitle((int) R.string.label_map_dialog_title);
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_satellite)).setVisibility(8);
        ((Button) dialog.findViewById(R.id.map_menu_zoomtomarkers)).setOnClickListener(new br(this, dialog));
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_directions)).setOnClickListener(new bq(this, dialog));
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_accuracy)).setOnClickListener(new bp(this, dialog));
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_scale)).setOnClickListener(new bo(this, dialog));
        return dialog;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        showDialog(0);
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_directions)).setChecked(this.f.a());
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_accuracy)).setChecked(this.h.a());
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_scale)).setChecked(this.e.a());
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.x != null) {
            this.x.cancel(false);
        }
        super.onStop();
    }
}
