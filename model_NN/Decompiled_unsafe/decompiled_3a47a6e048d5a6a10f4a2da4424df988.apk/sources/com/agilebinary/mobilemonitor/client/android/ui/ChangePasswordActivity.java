package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.agilebinary.a.a.a.i.d;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.c.e;
import com.agilebinary.mobilemonitor.client.android.ui.a.c;
import com.biige.client.android.R;
import java.io.UnsupportedEncodingException;

public class ChangePasswordActivity extends BaseLoggedInActivity {

    /* renamed from: a  reason: collision with root package name */
    private static final String f193a = b.a();
    private TextView b;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public EditText d;
    private Button e;
    private Button f;
    private Button h;
    private int i;
    private boolean k;

    public static void a(Activity activity, int i2, boolean z) {
        Intent intent = new Intent(activity, ChangePasswordActivity.class);
        intent.putExtra(d.I("\u0019G\bM\u001d@\bZ\u0004K\u0003V\u0018"), i2);
        intent.putExtra(com.agilebinary.a.a.a.i.b.I("sjb`wmeyb"), z);
        activity.startActivityForResult(intent, 0);
    }

    public static void a(Context context, c cVar) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(d.I("Z\u0004K\u000e^\u0003L\u0019M\nV\u001fZ\u0003M\u0019L\tS\b"), cVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    static /* synthetic */ void a(ChangePasswordActivity changePasswordActivity, String str) {
        String str2 = "";
        try {
            str2 = e.a(str);
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        changePasswordActivity.c.setText("");
        changePasswordActivity.d.setText("");
        changePasswordActivity.a((int) R.string.msg_progress_communicating);
        com.agilebinary.mobilemonitor.client.android.ui.a.d dVar = new com.agilebinary.mobilemonitor.client.android.ui.a.d(changePasswordActivity);
        com.agilebinary.mobilemonitor.client.android.ui.a.d.f211a = dVar;
        dVar.execute(str2);
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.change_password;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (com.agilebinary.mobilemonitor.client.android.ui.a.d.f211a != null) {
            try {
                com.agilebinary.mobilemonitor.client.android.ui.a.d.f211a.a();
            } catch (Exception e2) {
            }
        }
    }

    public void onCreate(Bundle bundle) {
        int i2 = 0;
        super.onCreate(bundle);
        this.b = (TextView) findViewById(R.id.changepassword_reason);
        this.i = getIntent().getIntExtra(com.agilebinary.a.a.a.i.b.I("wnfdsifsjbmv"), 0);
        this.k = getIntent().getBooleanExtra(d.I("Z\u0004K\u000e^\u0003L\u0017V\f"), false);
        this.b.setText(this.i);
        this.c = (EditText) findViewById(R.id.changepassword_password1);
        this.d = (EditText) findViewById(R.id.changepassword_password2);
        this.e = (Button) findViewById(R.id.changepassword_cancel);
        this.f = (Button) findViewById(R.id.changepassword_skip);
        this.e.setVisibility(!this.k ? 0 : 8);
        Button button = this.f;
        if (!this.k) {
            i2 = 8;
        }
        button.setVisibility(i2);
        this.h = (Button) findViewById(R.id.changepassword_ok);
        this.h.setOnClickListener(new x(this));
        this.e.setOnClickListener(new y(this));
        this.f.setOnClickListener(new z(this));
        if (bundle != null) {
            this.c.setText(bundle.getString(com.agilebinary.a.a.a.i.b.I("wnfdsiba\u0003")));
            this.d.setText(bundle.getString(d.I("\u0019G\bM\u001d@\fHn")));
            this.b.setText(bundle.getString(com.agilebinary.a.a.a.i.b.I("sjb`wmdwway|")));
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        c cVar = (c) intent.getSerializableExtra(d.I("Z\u0004K\u000e^\u0003L\u0019M\nV\u001fZ\u0003M\u0019L\tS\b"));
        b(false);
        if (cVar == null) {
            return;
        }
        if (cVar.b() != null) {
            if (cVar.b().c()) {
                b((int) R.string.error_service_account_general);
            } else if (cVar.b().b()) {
                b((int) R.string.error_service_account_general);
            } else {
                b((int) R.string.error_service_account_general);
            }
        } else if (cVar.a()) {
            setResult(-1);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString(com.agilebinary.a.a.a.i.b.I("wnfdsiba\u0003"), this.c.getText().toString());
        bundle.putString(d.I("\u0019G\bM\u001d@\fHn"), this.d.getText().toString());
        bundle.putInt(com.agilebinary.a.a.a.i.b.I("sjb`wmdwway|"), this.i);
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.f.getVisibility() == 0) {
            this.f.requestFocus();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
