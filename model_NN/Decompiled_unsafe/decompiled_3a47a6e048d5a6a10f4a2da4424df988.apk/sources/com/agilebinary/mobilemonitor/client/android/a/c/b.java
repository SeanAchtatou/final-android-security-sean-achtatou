package com.agilebinary.mobilemonitor.client.android.a.c;

import com.agilebinary.mobilemonitor.client.android.a.j;

public final class b extends k implements j {
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00cc, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00cd, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00d2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d3, code lost:
        r1 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00cc A[ExcHandler: all (th java.lang.Throwable), Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.mobilemonitor.client.android.a.s a(com.agilebinary.mobilemonitor.client.android.d r10, com.agilebinary.mobilemonitor.client.android.h r11, com.agilebinary.mobilemonitor.client.android.a.f r12, com.agilebinary.mobilemonitor.client.android.a.o r13, com.agilebinary.mobilemonitor.client.android.a.g r14) {
        /*
            r9 = this;
            long r0 = r11.b     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            long r2 = com.agilebinary.mobilemonitor.client.android.a.c.b.b     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            long r0 = r0 - r2
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
            long r0 = r0 / r2
            double r0 = (double) r0     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            double r0 = java.lang.Math.floor(r0)     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            int r0 = (int) r0     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            int r1 = r0 * -1
            r0 = 2
            if (r1 > r0) goto L_0x00ca
            if (r1 < 0) goto L_0x00ca
            javax.xml.parsers.SAXParserFactory r0 = javax.xml.parsers.SAXParserFactory.newInstance()     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            javax.xml.parsers.SAXParser r0 = r0.newSAXParser()     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            org.xml.sax.XMLReader r2 = r0.getXMLReader()     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            com.agilebinary.mobilemonitor.client.android.a.c.e r0 = new com.agilebinary.mobilemonitor.client.android.a.c.e     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            byte r3 = r11.f187a     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            r0.<init>(r9, r12, r13, r3)     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            r2.setContentHandler(r0)     // Catch:{ Exception -> 0x00cf, all -> 0x00cc }
            byte r0 = r11.f187a     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            r3 = -1
            long r4 = com.agilebinary.mobilemonitor.client.android.a.c.b.b     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            r6 = 172800000(0xa4cb800, double:8.53745436E-316)
            long r4 = r4 - r6
            r12.a(r0, r3, r4)     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            com.agilebinary.mobilemonitor.client.android.a.l r0 = com.agilebinary.mobilemonitor.client.android.a.l.a()     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            r9.c = r0     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            r0.<init>()     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            java.lang.String r3 = a()     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            java.lang.StringBuilder r3 = r0.append(r3)     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            byte r0 = r11.f187a     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            switch(r0) {
                case 1: goto L_0x00e5;
                case 2: goto L_0x00a8;
                case 3: goto L_0x00d5;
                case 4: goto L_0x00dd;
                case 5: goto L_0x0050;
                case 6: goto L_0x00af;
                case 7: goto L_0x0050;
                case 8: goto L_0x00ed;
                case 9: goto L_0x00f5;
                default: goto L_0x0050;
            }
        L_0x0050:
            java.lang.String r0 = ""
        L_0x0052:
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            java.lang.String r3 = "~"
            java.lang.String r3 = com.agilebinary.a.a.a.e.c.I(r3)     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            java.lang.String r1 = "\u000f\u0010L\u0004"
            java.lang.String r1 = com.agilebinary.a.a.a.c.c.e.I(r1)     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            com.agilebinary.mobilemonitor.client.android.a.l r1 = r9.c     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            com.agilebinary.mobilemonitor.client.android.a.t.a()     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            com.agilebinary.mobilemonitor.client.android.a.s r0 = r1.a(r0, r14)     // Catch:{ Exception -> 0x00d2, all -> 0x00cc }
            boolean r1 = r0.a()     // Catch:{ Exception -> 0x008b, all -> 0x00f9 }
            if (r1 != 0) goto L_0x00b6
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ Exception -> 0x008b, all -> 0x00f9 }
            int r2 = r0.b()     // Catch:{ Exception -> 0x008b, all -> 0x00f9 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x008b, all -> 0x00f9 }
            throw r1     // Catch:{ Exception -> 0x008b, all -> 0x00f9 }
        L_0x008b:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x008f:
            r2 = 1
            r12.a(r2)     // Catch:{ Exception -> 0x0099 }
            com.agilebinary.mobilemonitor.client.android.a.q r2 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ Exception -> 0x0099 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0099 }
            throw r2     // Catch:{ Exception -> 0x0099 }
        L_0x0099:
            r0 = move-exception
        L_0x009a:
            com.agilebinary.mobilemonitor.client.android.a.q r2 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x00a0 }
            r2.<init>(r0)     // Catch:{ all -> 0x00a0 }
            throw r2     // Catch:{ all -> 0x00a0 }
        L_0x00a0:
            r0 = move-exception
            r8 = r0
            r0 = r1
            r1 = r8
        L_0x00a4:
            a(r0)
            throw r1
        L_0x00a8:
            java.lang.String r0 = "k@dM"
            java.lang.String r0 = com.agilebinary.a.a.a.e.c.I(r0)
            goto L_0x0052
        L_0x00af:
            java.lang.String r0 = "\u001fD\n"
            java.lang.String r0 = com.agilebinary.a.a.a.c.c.e.I(r0)
            goto L_0x0052
        L_0x00b6:
            org.xml.sax.InputSource r1 = new org.xml.sax.InputSource     // Catch:{ Exception -> 0x008b, all -> 0x00f9 }
            java.io.InputStream r3 = r0.d()     // Catch:{ Exception -> 0x008b, all -> 0x00f9 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x008b, all -> 0x00f9 }
            r2.parse(r1)     // Catch:{ Exception -> 0x008b, all -> 0x00f9 }
            r1 = 0
            r12.a(r1)     // Catch:{ Exception -> 0x008b, all -> 0x00f9 }
        L_0x00c6:
            a(r0)
            return r0
        L_0x00ca:
            r0 = 0
            goto L_0x00c6
        L_0x00cc:
            r1 = move-exception
            r0 = 0
            goto L_0x00a4
        L_0x00cf:
            r0 = move-exception
            r1 = 0
            goto L_0x009a
        L_0x00d2:
            r0 = move-exception
            r1 = 0
            goto L_0x008f
        L_0x00d5:
            java.lang.String r0 = "ReR"
            java.lang.String r0 = com.agilebinary.a.a.a.e.c.I(r0)
            goto L_0x0052
        L_0x00dd:
            java.lang.String r0 = "\u0005L\u001b"
            java.lang.String r0 = com.agilebinary.a.a.a.c.c.e.I(r0)
            goto L_0x0052
        L_0x00e5:
            java.lang.String r0 = "dNk@|HgO"
            java.lang.String r0 = com.agilebinary.a.a.a.e.c.I(r0)
            goto L_0x0052
        L_0x00ed:
            java.lang.String r0 = "\u001bX\u001b"
            java.lang.String r0 = com.agilebinary.a.a.a.c.c.e.I(r0)
            goto L_0x0052
        L_0x00f5:
            java.lang.String r0 = "app"
            goto L_0x0052
        L_0x00f9:
            r1 = move-exception
            goto L_0x00a4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.c.b.a(com.agilebinary.mobilemonitor.client.android.d, com.agilebinary.mobilemonitor.client.android.h, com.agilebinary.mobilemonitor.client.android.a.f, com.agilebinary.mobilemonitor.client.android.a.o, com.agilebinary.mobilemonitor.client.android.a.g):com.agilebinary.mobilemonitor.client.android.a.s");
    }
}
