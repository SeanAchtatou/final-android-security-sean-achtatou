package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;

public abstract class i extends o {

    /* renamed from: a  reason: collision with root package name */
    private boolean f132a;
    private double b;
    private double c;
    private boolean d;
    private double e;
    private boolean f;
    private double g;
    private double h;
    private boolean i;
    private double j;
    private boolean k;
    private double l;
    private long m;

    public i(String str, long j2, long j3, c cVar, e eVar) {
        super(str, j2, j3, cVar);
        this.f132a = cVar.a();
        if (this.f132a) {
            this.b = cVar.e();
            this.c = cVar.e();
            this.d = cVar.a();
            if (this.d) {
                this.e = cVar.e();
            }
            this.f = cVar.a();
            if (this.f) {
                this.g = cVar.e();
                this.h = cVar.e();
            }
            this.i = cVar.a();
            if (this.i) {
                this.j = cVar.e();
            }
            this.k = cVar.a();
            if (this.k) {
                this.l = cVar.e();
            }
            this.m = eVar.a(cVar.d());
        }
    }

    public final boolean n() {
        return this.f132a;
    }

    public final double o() {
        return this.j;
    }

    public final long p() {
        return this.m;
    }

    public final boolean q() {
        return this.i;
    }
}
