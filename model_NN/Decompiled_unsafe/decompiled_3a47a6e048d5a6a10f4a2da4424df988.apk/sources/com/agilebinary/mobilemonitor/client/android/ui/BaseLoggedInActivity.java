package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.agilebinary.a.a.a.h.b.g;
import com.agilebinary.mobilemonitor.client.android.b.j;
import com.agilebinary.mobilemonitor.client.android.d;
import com.biige.client.android.R;

public abstract class BaseLoggedInActivity extends BaseActivity {
    protected j j;

    private /* synthetic */ void e() {
        getClass().getName();
        if (!this.g.c()) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(67108864);
            getClass().getName();
            startActivity(intent);
        }
        getClass().getName();
    }

    /* access modifiers changed from: protected */
    public final CharSequence d() {
        String str = null;
        d a2 = this.g.a();
        String d = a2 == null ? null : a2.d();
        if (a2 != null) {
            str = a2.b();
        }
        if (!(d == null || d.trim().length() == 0)) {
            str = d;
        }
        getClass().getName();
        g.I("VNVNVNVNVNVNVNVNVNVNVNVN") + str;
        return getString(R.string.titlebar_fmt, new Object[]{getString(R.string.app_title), str});
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.j = this.g.e();
        e();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.base_loggedin, menu);
        menu.findItem(R.id.menu_base_loggedin_accountinfo).setVisible(!this.g.d());
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.g.f();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_base_loggedin_logout /*2131296410*/:
                finish();
                this.g.a(this);
                return true;
            case R.id.menu_base_loggedin_accountinfo /*2131296411*/:
                c();
                AccountInfoActivity.a(this);
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        e();
        super.onStart();
    }
}
