package com.agilebinary.mobilemonitor.client.android;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.agilebinary.a.a.a.i.d;

final class a extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ i f142a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(i iVar, Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 6);
        this.f142a = iVar;
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        String format = String.format(d.I("\\\u000eZ\u001dK\u0019?\b^\u001eS\u0019?y.xl|7y-xl|V\u0012K\u0019X\u0019Mp?y,xl|V\u0012K\u0019X\u0019Mp?y+xl|V\u0012K\u0019X\u0019Mp?y*xl|V\u0012K\u0019X\u0019Mp?y)xl|[\u0013J\u001eS\u00193|:k;/?\u0018P\t]\u0010Zp:d;/?\u0018P\t]\u0010Zp:e;/?\u0015Q\bZ\u001bZ\u000e3|:m/xl|V\u0012K\u0019X\u0019Mp?\tQ\u0015N\tZ|7y.l;/3|:n;/3|:o;/3|:h;/3|:i;/6|P\u0012?\u001fP\u0012Y\u0010V\u001fK|M\u0019O\u0010^\u001fZ|6g"), c.f178a, c.c, c.d, c.e, c.f, c.g, c.h, c.i, c.b, c.j);
        String.format(com.agilebinary.mobilemonitor.client.a.a.a.I("K$k?{(g2i|]\rBf.y?x}"), format);
        sQLiteDatabase.execSQL(format);
        String format2 = String.format(d.I("\\\u000eZ\u001dK\u0019?\tQ\u0015N\tZ|V\u0012[\u0019G|V\u0018G\u0003:m;/?|P\u0012?y.xl|7y.l;/3|:n;/3|:o;/3|:h;/3|:i;/6g"), c.f178a, c.c, c.d, c.e, c.f, c.g, c.h, c.i, c.b, c.j);
        String.format(com.agilebinary.mobilemonitor.client.a.a.a.I("K$k?{(g2i|]\rBf.y?x}"), format2);
        sQLiteDatabase.execSQL(format2);
        String format3 = String.format(d.I("\\\u000eZ\u001dK\u0019?\b^\u001eS\u0019?y.xl|7y-xl|V\u0012K\u0019X\u0019Mp?y,xl|V\u0012K\u0019X\u0019Mp?y+xl|V\u0012K\u0019X\u0019Mp?y*xl|[\u0013J\u001eS\u00193|:j;/?\u0018P\t]\u0010Zp:k;/?\u0018P\t]\u0010Zp:d;/?\u0015Q\bZ\u001bZ\u000e3|:e;/?\u0015Q\bZ\u001bZ\u000e3|J\u0012V\rJ\u0019?t:e;/3|:n;/3|:o;/3|:h;/6|P\u0012?\u001fP\u0012Y\u0010V\u001fK|M\u0019O\u0010^\u001fZ|6g"), b.f165a, b.c, b.d, b.e, b.f, b.g, b.h, b.b, b.i);
        String.format(com.agilebinary.mobilemonitor.client.a.a.a.I("K$k?{(g2i|]\rBf.y?x}"), format3);
        sQLiteDatabase.execSQL(format3);
        String format4 = String.format(d.I("\u001fM\u0019^\bZ|J\u0012V\rJ\u0019?\u0015Q\u0018Z\u0004?\u0015[\u0004@y.xl|?\u0013Q|:m;/?t:e;/3|:n;/3|:o;/3|:h;/6g"), b.f165a, b.c, b.d, b.e, b.f, b.g, b.h, b.b, b.i);
        String.format(com.agilebinary.mobilemonitor.client.a.a.a.I("K$k?{(g2i|]\rBf.y?x}"), format4);
        sQLiteDatabase.execSQL(format4);
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        String.format(d.I("J,x.~8v2x|{=k=}=l9?:m3r|i9m/v3q|:m;/?(p|:n;/3|{9l(m3f5q;?=s0?9g5l(v2x|{=k="), Integer.valueOf(i), Integer.valueOf(i2));
        sQLiteDatabase.execSQL(String.format(com.agilebinary.mobilemonitor.client.a.a.a.I("J\u000eA\f.\bO\u001eB\u0019.\u0015H|K\u0004G\u000fZ\u000f.y?x}"), c.f178a));
        sQLiteDatabase.execSQL(String.format(d.I("\u0018M\u0013O|V\u0012[\u0019G|V\u001a?\u0019G\u0015L\bL|V\u0018G\u0003:m;/"), c.f178a));
        sQLiteDatabase.execSQL(String.format(com.agilebinary.mobilemonitor.client.a.a.a.I("J\u000eA\f.\bO\u001eB\u0019.\u0015H|K\u0004G\u000fZ\u000f.y?x}"), b.f165a));
        sQLiteDatabase.execSQL(String.format(d.I("\u0018M\u0013O|V\u0012[\u0019G|V\u001a?\u0019G\u0015L\bL|V\u0018G\u0003:m;/"), b.f165a));
        onCreate(sQLiteDatabase);
    }
}
