package com.agilebinary.mobilemonitor.client.android.ui.a;

import com.agilebinary.mobilemonitor.client.android.a.q;
import java.io.Serializable;

public final class e implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private q f212a;
    private boolean b;

    public e() {
        this.b = true;
    }

    public e(q qVar) {
        this.f212a = qVar;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 13);
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'j');
            i2 = i;
        }
        return new String(cArr);
    }

    public final q a() {
        return this.f212a;
    }
}
