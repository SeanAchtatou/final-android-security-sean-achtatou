package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.a.a.a.h.b.g;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.android.c.f;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashSet;
import javax.net.ssl.X509TrustManager;

public final class r implements X509TrustManager {

    /* renamed from: a  reason: collision with root package name */
    private PublicKey f162a;
    private boolean b;
    private HashSet c = new HashSet();
    private /* synthetic */ b d;

    r(b bVar) {
        this.d = bVar;
        if (a.I("o>d<l?n<o>ljl8l7nod8h6d8:9ljl?l?l?l;l>l=d<l?lhl>o>d<l?lol<d<l?l?l>=?n7m9j;:<dll68??9m<d>:od>n<9;k6lmkk=h?<d:8;j7j<m;kkn;>6o=>8j>jo87n<m;8o?;l=8<imm>=>e7i:nhh:i;o=l;9m>j?l:>nl:>dh:o==?h?kdon7lmo6jjm>==do8j99jkj?kj9;lkkkkjlm=mn<>ok<n7?ln6?=i6h7i6:ko=n6o8l<hk8<ijkk:?>7m8:8:lo7e?8=?8d:j7:=m8=hil=;i=j=nke<:j9<=6h7=>?;=ol9>?hmh9n<8=9kd>n8j9?;9>nknj9?:;n=8je:8o9?k>n<>h=:kml=ml8<e7=k:ke>j;e7mjo?jmmo?;j8m;ll>m>m>7?=l8l<><:=9k889o=lemh?89hm8h?=e9878?8>=lhmo=h>l<i:dmhlh<ml96j:j:okomlm:jm:io8;m:8h?=k:dh::e6h:hle7?oih=:l8ll:jo??jh>k8l9h8?l=9ojkm:;=9l?e7n>8>i>98ko:?o;h6n99mile=?mdmh<nll<l=l?l>l?").trim().length() > 0) {
            try {
                this.f162a = KeyFactory.getInstance(g.I("?&,")).generatePublic(new X509EncodedKeySpec(f.a(a.I("o>d<l?n<o>ljl8l7nod8h6d8:9ljl?l?l?l;l>l=d<l?lhl>o>d<l?lol<d<l?l?l>=?n7m9j;:<dll68??9m<d>:od>n<9;k6lmkk=h?<d:8;j7j<m;kkn;>6o=>8j>jo87n<m;8o?;l=8<imm>=>e7i:nhh:i;o=l;9m>j?l:>nl:>dh:o==?h?kdon7lmo6jjm>==do8j99jkj?kj9;lkkkkjlm=mn<>ok<n7?ln6?=i6h7i6:ko=n6o8l<hk8<ijkk:?>7m8:8:lo7e?8=?8d:j7:=m8=hil=;i=j=nke<:j9<=6h7=>?;=ol9>?hmh9n<8=9kd>n8j9?;9>nknj9?:;n=8je:8o9?k>n<>h=:kml=ml8<e7=k:ke>j;e7mjo?jmmo?;j8m;ll>m>m>7?=l8l<><:=9k889o=lemh?89hm8h?=e9878?8>=lhmo=h>l<i:dmhlh<ml96j:j:okomlm:jm:io8;m:8h?=k:dh::e6h:hle7?oih=:l8ll:jo??jh>k8l9h8?l=9ojkm:;=9l?e7n>8>i>98ko:?o;h6n99mile=?mdmh<nll<l=l?l>l?").toCharArray())));
            } catch (Exception e) {
                this.b = true;
            }
        }
    }

    public final void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
    }

    public final void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        if (x509CertificateArr.length == 0) {
            throw new CertificateException(g.I("\u0003\u001aM\u0016\b\u0007\u0019\u001c\u000b\u001c\u000e\u0014\u0019\u0010M\u001c\u0003U\u000e\u001d\f\u001c\u0003"));
        } else if (this.b) {
            throw new CertificateException(a.I("m9|(g:g?o(k|g2x=b5j|l9m={/k|m3{0j2)(.8k?a8k|m3`:g;{.k8.,{>b5m|e9w"));
        } else if (this.f162a != null) {
            X509Certificate x509Certificate = x509CertificateArr[0];
            if (!this.c.contains(x509Certificate)) {
                try {
                    x509Certificate.verify(this.f162a);
                    this.c.add(x509Certificate);
                } catch (InvalidKeyException e) {
                    com.agilebinary.mobilemonitor.a.a.a.a.e(e);
                    throw new CertificateException(e);
                } catch (NoSuchAlgorithmException e2) {
                    com.agilebinary.mobilemonitor.a.a.a.a.e(e2);
                    throw new CertificateException(e2);
                } catch (NoSuchProviderException e3) {
                    com.agilebinary.mobilemonitor.a.a.a.a.e(e3);
                    throw new CertificateException(e3);
                } catch (SignatureException e4) {
                    com.agilebinary.mobilemonitor.a.a.a.a.e(e4);
                    throw new CertificateException(e4);
                }
            }
        }
    }

    public final X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}
