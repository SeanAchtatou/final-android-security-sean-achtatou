package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.biige.client.android.R;

public final class l extends n {
    private int c = -1;

    public l(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.c = cVar.c();
    }

    public final String b(Context context) {
        return context.getString(R.string.label_event_location_type_cell_iden);
    }

    public final String c(Context context) {
        return this.f134a;
    }

    public final byte k() {
        return 12;
    }
}
