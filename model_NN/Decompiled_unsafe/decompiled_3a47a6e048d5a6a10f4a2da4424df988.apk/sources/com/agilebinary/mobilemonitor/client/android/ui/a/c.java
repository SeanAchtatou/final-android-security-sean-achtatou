package com.agilebinary.mobilemonitor.client.android.ui.a;

import com.agilebinary.mobilemonitor.client.android.a.q;
import java.io.Serializable;

public final class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private boolean f210a;
    private q b;

    public c(q qVar) {
        this.b = qVar;
    }

    public c(boolean z) {
        this.f210a = z;
    }

    public final boolean a() {
        return this.f210a;
    }

    public final q b() {
        return this.b;
    }
}
