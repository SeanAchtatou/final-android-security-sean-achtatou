package com.agilebinary.mobilemonitor.client.android.c;

import com.agilebinary.a.a.a.c.c.e;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.osmdroid.b.a;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static c f181a;
    private DateFormat b = SimpleDateFormat.getDateInstance(3);
    private DateFormat c = SimpleDateFormat.getDateInstance(2);
    private DateFormat d = SimpleDateFormat.getTimeInstance(3);
    private DateFormat e = SimpleDateFormat.getTimeInstance(2);
    private DateFormat f = SimpleDateFormat.getDateTimeInstance(3, 2);
    private DateFormat g = SimpleDateFormat.getDateTimeInstance(2, 2);
    private TimeZone h = TimeZone.getDefault();
    private DateFormat i = new SimpleDateFormat(a.I(":l\u0013\u0001\u0013"));

    public static c a() {
        c cVar;
        synchronized (c.class) {
            try {
                if (f181a == null) {
                    f181a = new c();
                }
                cVar = f181a;
            } catch (Throwable th) {
                Class<c> cls = c.class;
                throw th;
            }
        }
        return cVar;
    }

    public final String a(long j) {
        Date date = new Date(j);
        return String.format(e.I("MRb\u0004\u001b"), this.d.format(date), this.i.format(date));
    }

    public final String b(long j) {
        return this.b.format(new Date(j));
    }

    public final String c(long j) {
        if (j == 0) {
            return "";
        }
        Date date = new Date(j);
        if (this.h.getID().equals(TimeZone.getDefault().getID())) {
            return this.g.format(date);
        }
        return String.format(a.I("{?~\u0017{?\u0003"), this.g.format(date), this.h.getDisplayName());
    }

    public final String d(long j) {
        if (j == 0) {
            return "";
        }
        Date date = new Date(j);
        if (this.h.getID().equals(TimeZone.getDefault().getID())) {
            return this.f.format(date);
        }
        return String.format(e.I("MRHzMR5"), this.f.format(date), this.h.getDisplayName());
    }
}
