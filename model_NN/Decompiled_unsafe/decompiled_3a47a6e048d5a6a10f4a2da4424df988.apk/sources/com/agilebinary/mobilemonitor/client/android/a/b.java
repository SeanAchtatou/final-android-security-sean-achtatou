package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.a.a.a.e.c;
import com.agilebinary.a.a.a.h.b.j;
import com.agilebinary.a.a.a.h.d.e;
import com.agilebinary.mobilemonitor.client.android.a.a.g;
import com.agilebinary.mobilemonitor.client.android.h;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

public final class b implements j {

    /* renamed from: a  reason: collision with root package name */
    private static final String f150a = com.agilebinary.mobilemonitor.client.android.c.b.a();
    private static com.agilebinary.a.a.a.h.d.b b = new e();
    private static final b c = new b();
    private final SSLContext d;
    private final SSLSocketFactory e;
    private final com.agilebinary.a.a.a.h.b.b f;
    private volatile com.agilebinary.a.a.a.h.d.b g;

    private /* synthetic */ b() {
        this.g = b;
        this.d = null;
        this.e = HttpsURLConnection.getDefaultSSLSocketFactory();
        this.f = null;
    }

    public b(byte b2) {
        this.g = b;
        this.d = SSLContext.getInstance(h.I("\fv\u000b"));
        this.d.init(null, new TrustManager[]{new r(this)}, null);
        this.e = this.d.getSocketFactory();
        this.f = null;
    }

    public final Socket a() {
        return (SSLSocket) this.e.createSocket();
    }

    public final Socket a(Socket socket, String str, int i, InetAddress inetAddress, int i2, com.agilebinary.a.a.a.e.e eVar) {
        if (str == null) {
            throw new IllegalArgumentException(g.I("d~BxUk\u0010w_lD?]~I?^pD?Rz\u0010qEs\\1"));
        } else if (eVar == null) {
            throw new IllegalArgumentException(h.I("\b[*[5_,_*IxW9CxT7NxX=\u001a6O4Vv"));
        } else {
            SSLSocket sSLSocket = (SSLSocket) (socket != null ? socket : a());
            if (inetAddress != null || i2 > 0) {
                if (i2 < 0) {
                    i2 = 0;
                }
                sSLSocket.bind(new InetSocketAddress(inetAddress, i2));
            }
            int c2 = c.c(eVar);
            int a2 = c.a(eVar);
            InetSocketAddress inetSocketAddress = this.f != null ? new InetSocketAddress(this.f.a(), i) : new InetSocketAddress(str, i);
            try {
                sSLSocket.connect(inetSocketAddress, c2);
                sSLSocket.setSoTimeout(a2);
                try {
                    this.g.a(str, sSLSocket);
                    return sSLSocket;
                } catch (IOException e2) {
                    try {
                        sSLSocket.close();
                    } catch (Exception e3) {
                    }
                    throw e2;
                }
            } catch (SocketTimeoutException e4) {
                throw new com.agilebinary.a.a.a.h.h(g.I("\\_q^zSk\u0010k_?") + inetSocketAddress + h.I("\u001a,S5_<\u001a7O,"));
            }
        }
    }

    public final boolean a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException(g.I("L_|[zD?]~I?^pD?Rz\u0010qEs\\1"));
        } else if (!(socket instanceof SSLSocket)) {
            throw new IllegalArgumentException(h.I("\u000bU;Q=NxT7NxY*_9N=^xX!\u001a,R1Ix\\9Y,U*Cv"));
        } else if (!socket.isClosed()) {
            return true;
        } else {
            throw new IllegalArgumentException(g.I("L_|[zD?Yl\u0010|\\pCzT1"));
        }
    }

    public final Socket a_(Socket socket, String str, int i, boolean z) {
        SSLSocket sSLSocket = (SSLSocket) this.e.createSocket(socket, str, i, z);
        this.g.a(str, sSLSocket);
        return sSLSocket;
    }
}
