package com.agilebinary.mobilemonitor.client.a;

import com.agilebinary.mobilemonitor.client.android.c.b;
import java.util.zip.Inflater;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static String f140a = b.a();
    private static byte[] b = new byte[8192];
    private static byte[] c = new byte[2048];
    private static Inflater d = new Inflater(false);

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        r3 = com.agilebinary.mobilemonitor.client.a.f.class;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001b, code lost:
        throw r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.agilebinary.mobilemonitor.client.a.b.o a(java.lang.String r15, long r16, long r18, boolean r20, boolean r21, int r22, java.io.InputStream r23, com.agilebinary.mobilemonitor.client.a.e r24, com.agilebinary.mobilemonitor.client.a.b r25, android.content.Context r26, java.io.ByteArrayOutputStream r27) {
        /*
            java.lang.Class<com.agilebinary.mobilemonitor.client.a.f> r2 = com.agilebinary.mobilemonitor.client.a.f.class
            monitor-enter(r2)
            int r2 = r23.read()     // Catch:{ all -> 0x0015 }
            int r3 = r23.read()     // Catch:{ all -> 0x0015 }
            r4 = r2 | r3
            if (r4 >= 0) goto L_0x001c
            java.io.EOFException r2 = new java.io.EOFException     // Catch:{ all -> 0x0015 }
            r2.<init>()     // Catch:{ all -> 0x0015 }
            throw r2     // Catch:{ all -> 0x0015 }
        L_0x0015:
            r2 = move-exception
            throw r2     // Catch:{ all -> 0x0017 }
        L_0x0017:
            r2 = move-exception
            java.lang.Class<com.agilebinary.mobilemonitor.client.a.f> r3 = com.agilebinary.mobilemonitor.client.a.f.class
            monitor-exit(r3)
            throw r2
        L_0x001c:
            int r2 = r2 << 8
            int r3 = r3 << 0
            int r2 = r2 + r3
            short r2 = (short) r2
            if (r2 == 0) goto L_0x0041
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0015 }
            r4.<init>()     // Catch:{ all -> 0x0015 }
            java.lang.String r5 = ">X X$A%\u0016.X(Y/_%Qk@.D8_$Xq\u0016"
            java.lang.String r5 = com.agilebinary.a.a.a.k.f.I(r5)     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ all -> 0x0015 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0015 }
            r3.<init>(r2)     // Catch:{ all -> 0x0015 }
            throw r3     // Catch:{ all -> 0x0015 }
        L_0x0041:
            int r2 = r23.read()     // Catch:{ all -> 0x0015 }
            if (r2 >= 0) goto L_0x004d
            java.io.EOFException r2 = new java.io.EOFException     // Catch:{ all -> 0x0015 }
            r2.<init>()     // Catch:{ all -> 0x0015 }
            throw r2     // Catch:{ all -> 0x0015 }
        L_0x004d:
            byte r3 = (byte) r2     // Catch:{ all -> 0x0015 }
            if (r20 == 0) goto L_0x005c
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0015 }
            java.lang.String r3 = "\u0014\"\n#\r)\u0003]d\u0002*\u00046\u001e4\u0013-\b*G*\b0G=\u00020G-\n4\u000b!\n!\t0\u0002 "
            java.lang.String r3 = com.agilebinary.a.a.a.d.a.b.I(r3)     // Catch:{ all -> 0x0015 }
            r2.<init>(r3)     // Catch:{ all -> 0x0015 }
            throw r2     // Catch:{ all -> 0x0015 }
        L_0x005c:
            r2 = 0
            if (r21 == 0) goto L_0x009e
            java.util.zip.Inflater r2 = com.agilebinary.mobilemonitor.client.a.f.d     // Catch:{ all -> 0x0015 }
            r2.reset()     // Catch:{ all -> 0x0015 }
            com.agilebinary.mobilemonitor.client.a.a.b r2 = new com.agilebinary.mobilemonitor.client.a.a.b     // Catch:{ all -> 0x0015 }
            java.util.zip.Inflater r4 = com.agilebinary.mobilemonitor.client.a.f.d     // Catch:{ all -> 0x0015 }
            byte[] r5 = com.agilebinary.mobilemonitor.client.a.f.c     // Catch:{ all -> 0x0015 }
            int r6 = r22 + -3
            r0 = r23
            r2.<init>(r0, r4, r5, r6)     // Catch:{ all -> 0x0015 }
            com.agilebinary.mobilemonitor.client.a.a.c r8 = new com.agilebinary.mobilemonitor.client.a.a.c     // Catch:{ all -> 0x0015 }
            com.agilebinary.mobilemonitor.client.a.a.a r4 = new com.agilebinary.mobilemonitor.client.a.a.a     // Catch:{ all -> 0x0015 }
            byte[] r5 = com.agilebinary.mobilemonitor.client.a.f.b     // Catch:{ all -> 0x0015 }
            r4.<init>(r2, r5)     // Catch:{ all -> 0x0015 }
            r8.<init>(r4)     // Catch:{ all -> 0x0015 }
            r13 = r2
        L_0x007e:
            switch(r3) {
                case 1: goto L_0x00d5;
                case 2: goto L_0x00e2;
                case 3: goto L_0x00ef;
                case 4: goto L_0x0102;
                case 5: goto L_0x010f;
                case 6: goto L_0x0153;
                case 7: goto L_0x011c;
                case 8: goto L_0x0129;
                case 9: goto L_0x0145;
                case 10: goto L_0x0161;
                case 11: goto L_0x00a7;
                case 12: goto L_0x0137;
                case 13: goto L_0x016f;
                case 14: goto L_0x017d;
                default: goto L_0x0081;
            }     // Catch:{ all -> 0x0015 }
        L_0x0081:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0015 }
            r4.<init>()     // Catch:{ all -> 0x0015 }
            java.lang.String r5 = ">X X$A%\u0016(Y%B.X?\u0016?O;Sq\u0016"
            java.lang.String r5 = com.agilebinary.a.a.a.k.f.I(r5)     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ all -> 0x0015 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0015 }
            r2.<init>(r3)     // Catch:{ all -> 0x0015 }
            throw r2     // Catch:{ all -> 0x0015 }
        L_0x009e:
            com.agilebinary.mobilemonitor.client.a.a.c r8 = new com.agilebinary.mobilemonitor.client.a.a.c     // Catch:{ all -> 0x0015 }
            r0 = r23
            r8.<init>(r0)     // Catch:{ all -> 0x0015 }
            r13 = r2
            goto L_0x007e
        L_0x00a7:
            com.agilebinary.mobilemonitor.client.a.b.c r2 = new com.agilebinary.mobilemonitor.client.a.b.c     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
        L_0x00b3:
            if (r13 == 0) goto L_0x00d1
            int r3 = r13.a()     // Catch:{ all -> 0x0015 }
            if (r3 <= 0) goto L_0x00d1
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0015 }
            r3.<init>()     // Catch:{ all -> 0x0015 }
            java.lang.String r4 = "\t+\u0013d\u0006(\u000bd\u0005=\u0013!\u0014d\u0015!\u0006 G\"\u0015+\nd\u0004+\n4\u0015!\u00147\u0002 G7\u00136\u0002%\nd\u0010,\u000f!\td\u0003!\u0014!\u0015-\u0006(\u000e>\u000e*\u0000d"
            java.lang.String r4 = com.agilebinary.a.a.a.d.a.b.I(r4)     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ all -> 0x0015 }
            r3.toString()     // Catch:{ all -> 0x0015 }
        L_0x00d1:
            java.lang.Class<com.agilebinary.mobilemonitor.client.a.f> r3 = com.agilebinary.mobilemonitor.client.a.f.class
            monitor-exit(r3)
            return r2
        L_0x00d5:
            com.agilebinary.mobilemonitor.client.a.b.u r2 = new com.agilebinary.mobilemonitor.client.a.b.u     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x00e2:
            com.agilebinary.mobilemonitor.client.a.b.j r2 = new com.agilebinary.mobilemonitor.client.a.b.j     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x00ef:
            com.agilebinary.mobilemonitor.client.a.b.f r2 = new com.agilebinary.mobilemonitor.client.a.b.f     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r10 = r25
            r11 = r26
            r12 = r27
            r2.<init>(r3, r4, r6, r8, r9, r10, r11, r12)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x0102:
            com.agilebinary.mobilemonitor.client.a.b.k r2 = new com.agilebinary.mobilemonitor.client.a.b.k     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x010f:
            com.agilebinary.mobilemonitor.client.a.b.b r2 = new com.agilebinary.mobilemonitor.client.a.b.b     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x011c:
            com.agilebinary.mobilemonitor.client.a.b.h r2 = new com.agilebinary.mobilemonitor.client.a.b.h     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x0129:
            com.agilebinary.mobilemonitor.client.a.b.r r2 = new com.agilebinary.mobilemonitor.client.a.b.r     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x0137:
            com.agilebinary.mobilemonitor.client.a.b.l r2 = new com.agilebinary.mobilemonitor.client.a.b.l     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x0145:
            com.agilebinary.mobilemonitor.client.a.b.p r2 = new com.agilebinary.mobilemonitor.client.a.b.p     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x0153:
            com.agilebinary.mobilemonitor.client.a.b.m r2 = new com.agilebinary.mobilemonitor.client.a.b.m     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x0161:
            com.agilebinary.mobilemonitor.client.a.b.d r2 = new com.agilebinary.mobilemonitor.client.a.b.d     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x016f:
            com.agilebinary.mobilemonitor.client.a.b.e r2 = new com.agilebinary.mobilemonitor.client.a.b.e     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        L_0x017d:
            com.agilebinary.mobilemonitor.client.a.b.g r2 = new com.agilebinary.mobilemonitor.client.a.b.g     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            goto L_0x00b3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.a.f.a(java.lang.String, long, long, boolean, boolean, int, java.io.InputStream, com.agilebinary.mobilemonitor.client.a.e, com.agilebinary.mobilemonitor.client.a.b, android.content.Context, java.io.ByteArrayOutputStream):com.agilebinary.mobilemonitor.client.a.b.o");
    }
}
