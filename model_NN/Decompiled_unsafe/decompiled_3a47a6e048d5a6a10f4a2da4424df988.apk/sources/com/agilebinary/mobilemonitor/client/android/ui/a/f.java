package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.mobilemonitor.client.android.a.b.a;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.ui.BaseActivity;
import com.agilebinary.mobilemonitor.client.android.ui.ResetPasswordActivity;
import java.util.Locale;
import org.osmdroid.b.a.e;

public final class f extends AsyncTask implements g {

    /* renamed from: a  reason: collision with root package name */
    public static f f213a;
    private static String b = b.a();
    private BaseActivity c;
    private com.agilebinary.a.a.a.d.c.b d;

    public f(BaseActivity baseActivity) {
        this.c = baseActivity;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public /* synthetic */ c doInBackground(String... strArr) {
        c cVar;
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService(e.I("d\u0017c\u001df"))).newWakeLock(6, b);
        newWakeLock.acquire();
        try {
            new a().b(strArr[0], Locale.getDefault().getCountry(), this);
            cVar = new c(true);
            try {
                newWakeLock.release();
            } catch (Exception e) {
            }
            f213a = null;
        } catch (q e2) {
            cVar = new c(e2);
            try {
                newWakeLock.release();
            } catch (Exception e3) {
            }
            f213a = null;
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e4) {
            }
            f213a = null;
            throw th;
        }
        return cVar;
    }

    public final void a() {
        cancel(false);
        if (this.d != null) {
            try {
                this.d.d();
            } catch (Exception e) {
            }
        }
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        this.d = bVar;
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        ResetPasswordActivity.a(this.c, (c) null);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        ResetPasswordActivity.a(this.c, (c) obj);
    }
}
