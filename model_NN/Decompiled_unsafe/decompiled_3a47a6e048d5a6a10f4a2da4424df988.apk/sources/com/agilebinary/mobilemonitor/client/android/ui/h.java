package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.agilebinary.a.a.a.k;
import com.biige.client.android.R;

public final class h extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f272a = ((ImageView) findViewById(R.id.main_row_icon));
    private TextView b = ((TextView) findViewById(R.id.main_row_label));
    private TextView c = ((TextView) findViewById(R.id.main_row_cnt));
    private ImageView d = ((ImageView) findViewById(R.id.main_row_star));
    private /* synthetic */ MainActivity e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.h, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(MainActivity mainActivity, Context context) {
        super(context, null);
        this.e = mainActivity;
        ((LayoutInflater) context.getSystemService(k.I("\u000f0\u001a>\u0016%<8\r7\u000f0\u00174\u0011"))).inflate((int) R.layout.main_row, (ViewGroup) this, true);
    }

    public final void a(az azVar) {
        TextView textView;
        int i = 4;
        boolean z = false;
        this.f272a.setImageResource(azVar.f240a);
        this.b.setText(azVar.b);
        this.d.setVisibility(azVar.c < 0 ? 4 : 0);
        if (this.e.g.d()) {
            textView = this.c;
            i = 8;
        } else {
            textView = this.c;
            if (azVar.c >= 0 && this.e.g.a(azVar.c)) {
                i = 0;
            }
        }
        textView.setVisibility(i);
        this.c.setText(String.valueOf(this.e.g.b(azVar.c)));
        ImageView imageView = this.d;
        if (azVar.c >= 0) {
            z = this.e.g.a(azVar.c);
        }
        imageView.setEnabled(z);
        if (!this.e.g.c(azVar.c) || azVar.c < 0) {
            this.d.clearAnimation();
        } else {
            this.d.startAnimation(this.e.d);
        }
    }
}
