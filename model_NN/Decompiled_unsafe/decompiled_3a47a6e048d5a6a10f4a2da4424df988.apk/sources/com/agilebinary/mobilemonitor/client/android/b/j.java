package com.agilebinary.mobilemonitor.client.android.b;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.a.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.c;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.a.f;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.c.e;
import com.agilebinary.mobilemonitor.client.android.i;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class j implements f {

    /* renamed from: a  reason: collision with root package name */
    public static final byte[] f174a = {1, 2, 3, 4, 6, 8, 9};
    private static String b = b.a();
    private Context c;
    private SQLiteDatabase d;
    private SQLiteStatement e;
    private SQLiteStatement f;
    private SQLiteStatement g;
    private SQLiteStatement h;
    private SQLiteStatement i;
    private SQLiteStatement j;
    private SQLiteStatement k;
    private SQLiteStatement l;
    private SQLiteStatement m;
    private byte n = -1;
    private SQLiteStatement o;
    private SQLiteStatement p;
    private List q;
    private i r;
    private Calendar s;
    private ByteArrayOutputStream t = new ByteArrayOutputStream();
    private SQLiteStatement u;
    private SQLiteStatement v;
    private ByteArrayOutputStream w;

    public j(Context context, i iVar) {
        this.c = context;
        this.r = iVar;
        this.d = new e(this, this.c, com.agilebinary.a.a.a.h.f.I("P\u000fP\u0017A\n")).getWritableDatabase();
        this.e = this.d.compileStatement(String.format(e.I("=e6d'\u0016:x'yS\u0013B\u0012\u0000\u0016[\u0013A\u0012\u0000\u001aV\u0005WE_\u0013G\u0012\u0000\u001aV\u0003WE_\u0013E\u0012\u0000\u001aV\u0001WE_\u0013K\u0012\u0000\u001aV\u000fWE_\u0013B\u0006WE_\u0013B\u0007WE_\u0013B\u0004WEZ\u0016\u0005W\u001fC\u0016ES\u001eL\u001aL\u001aL\u001aL\u001aL\u001aL\u001aL\u001aL\u001aL\u001aL\u001aL\u001fS"), com.agilebinary.a.a.a.h.f.I("Y\u0016V\u0018A\u0010Z\u0017"), f.b, f.c, f.d, "line1", "line2", e.I("W\u0010U\u0006D\u0012U\n"), "lat", "lon", com.agilebinary.a.a.a.h.f.I("\u001aZ\u0017A\u001c[\rA\u0000E\u001c"), e.I("\u0003Y\u0004S\u0001E\u0012@\u0016"), com.agilebinary.a.a.a.h.f.I("C\u0018Y\u0015Z\u001a")));
        this.f = this.d.compileStatement(String.format(e.I(":x s!bS=b<\u0016V\u0007WES\u001eV\u0004WE_\u0013@\u0012\u0000\u001aV\u0002WE_\u0013F\u0012\u0000\u001aV\u0000WEZ\u0016\u0005W\u001fC\u0016ES\u001eL\u001aL\u001aL\u001aL\u001aL\u001fS"), com.agilebinary.a.a.a.h.f.I("V\u0018Y\u0015"), c.b, c.c, c.d, "line1", "line2"));
        this.g = this.d.compileStatement(String.format(e.I(":x s!bS=b<\u0016V\u0007WES\u001eV\u0004WE_\u0013@\u0012\u0000\u001aV\u0002WE_\u0013F\u0012\u0000\u001aV\u0000WEZ\u0016\u0005W\u001fC\u0016ES\u001eL\u001aL\u001aL\u001aL\u001aL\u001fS"), com.agilebinary.a.a.a.h.f.I("\nX\n"), a.b, a.c, a.d, e.I("\u0017_\u0001"), com.agilebinary.a.a.a.h.f.I("A\u001cM\r")));
        this.h = this.d.compileStatement(String.format(e.I("=e6d'\u0016:x'yS\u0013B\u0012\u0000\u0016[\u0013A\u0012\u0000\u001aV\u0005WE_\u0013G\u0012\u0000\u001aV\u0003WE_\u0013E\u0012\u0000\u001aV\u0001WEZ\u0016\u0005W\u001fC\u0016ES\u001eL\u001aL\u001aL\u001aL\u001aL\u001aL\u001fS"), com.agilebinary.a.a.a.h.f.I("\u0014X\n"), d.b, d.c, d.d, e.I("\u0017_\u0001"), com.agilebinary.a.a.a.h.f.I("A\u001cM\r"), e.I("\u0007^\u0006[\u0011X\u0012_\u001f")));
        this.i = this.d.compileStatement(String.format(com.agilebinary.a.a.a.h.f.I("0{*p+aY|7a6\u0015\\\u0004]FY\u001d\\\u0007]FU\u0010J\u0011\n\u0019\\\u0001]FU\u0010L\u0011\n\u0019\\\u0003]FP\u0015\u000fT\u0015@\u001cFY\u001dF\u0019F\u0019F\u0019F\u0019F\u001cY"), e.I("\u0004S\u0011"), k.b, k.c, k.d, "line1", "line2"));
        this.j = this.d.compileStatement(String.format(com.agilebinary.a.a.a.h.f.I("|7f<g-\u00150{-zY\u0010H\u0011\n\u0015Q\u0010K\u0011\n\u0019\\\u0006]FU\u0010M\u0011\n\u0019\\\u0000]FP\u0015\u000fT\u0015@\u001cFY\u001dF\u0019F\u0019F\u0019F\u001cY"), e.I("E\nE\u0007i\u001e"), l.b, l.c, l.d, "line1"));
        this.k = this.d.compileStatement(String.format(com.agilebinary.a.a.a.h.f.I("0{*p+aY|7a6\u0015\\\u0004]FY\u001d\\\u0007]FU\u0010J\u0011\n\u0019\\\u0001]FU\u0010L\u0011\n\u0019\\\u0003]FP\u0015\u000fT\u0015@\u001cFY\u001dF\u0019F\u0019F\u0019F\u0019F\u001cY"), i.f173a, i.b, i.c, i.d, i.f, i.g));
        this.l = this.d.compileStatement(String.format(e.I("=e6d'\u0016:x'yS\u0013B\u0012\u0000\u0016[\u0013A\u0012\u0000\u001aV\u0005WEZ\u0016\u0005W\u001fC\u0016ES\u001eL\u001aL\u001fS"), com.agilebinary.a.a.a.h.f.I("V\fG\u001dT\u0000"), e.I("\u0016@\u0016X\u0007B\nF\u0016"), com.agilebinary.a.a.a.h.f.I("\u001dT\u0000")));
        this.p = this.d.compileStatement(String.format(e.I("=e6d'\u0016:x'yS\u0013B\u0012\u0000\u0016[\u0013A\u0012\u0000\u001aV\u0005WEZ\u0016\u0005W\u001fC\u0016ES\u001eL\u001aL\u001fS"), com.agilebinary.a.a.a.h.f.I("\u0016Y\u001dP\nA\u001cC\u001c[\r"), e.I("\u0016@\u0016X\u0007B\nF\u0016"), com.agilebinary.a.a.a.h.f.I("Z\u0015Q\u001cF\r")));
        this.u = this.d.compileStatement(String.format(e.I("=e6d'\u0016:x'yS\u0013B\u0012\u0000\u0016[\u0013A\u0012\u0000\u001aV\u0005WEZ\u0016\u0005W\u001fC\u0016ES\u001eL\u001aL\u001fS"), com.agilebinary.a.a.a.h.f.I("Y\u0018F\rF\u001cP\u0017"), e.I("\u0016@\u0016X\u0007B\nF\u0016"), com.agilebinary.a.a.a.h.f.I("Y\u0018F\r\\\u001d")));
        this.m = this.d.compileStatement(String.format(e.I(" s?s0bS\u0013@\u0012\u0000\u00165d<{S\u0013B\u0012\u0000\u0016\u0004^\u0016D\u0016\u0016V\u0004WES\u000bS\tS"), com.agilebinary.a.a.a.h.f.I("V\fG\u001dT\u0000"), e.I("\u0016@\u0016X\u0007B\nF\u0016"), com.agilebinary.a.a.a.h.f.I("\u001dT\u0000")));
        this.o = this.d.compileStatement(String.format(e.I(" s?s0bS\u0013@\u0012\u0000\u00165d<{S\u0013B\u0012\u0000\u0016\u0004^\u0016D\u0016\u0016V\u0004WES\u000bS\tS"), com.agilebinary.a.a.a.h.f.I("\u0016Y\u001dP\nA\u001cC\u001c[\r"), e.I("\u0016@\u0016X\u0007B\nF\u0016"), com.agilebinary.a.a.a.h.f.I("Z\u0015Q\u001cF\r")));
        this.v = this.d.compileStatement(String.format(e.I(" s?s0bS\u0013@\u0012\u0000\u00165d<{S\u0013B\u0012\u0000\u0016\u0004^\u0016D\u0016\u0016V\u0004WES\u000bS\tS"), com.agilebinary.a.a.a.h.f.I("Y\u0018F\rF\u001cP\u0017"), e.I("\u0016@\u0016X\u0007B\nF\u0016"), com.agilebinary.a.a.a.h.f.I("Y\u0018F\r\\\u001d")));
    }

    private static /* synthetic */ int a(Calendar calendar) {
        return (calendar.get(1) * 1000) + calendar.get(6);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0052 A[Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x010b A[Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x018e A[Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01ad A[SYNTHETIC, Splitter:B:65:0x01ad] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0037 A[Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01d5 A[Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0206 A[Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0215 A[Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void a(byte r11, com.agilebinary.mobilemonitor.client.a.b.o r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            switch(r11) {
                case 1: goto L_0x003d;
                case 2: goto L_0x0040;
                case 3: goto L_0x0043;
                case 4: goto L_0x0046;
                case 5: goto L_0x0004;
                case 6: goto L_0x0049;
                case 7: goto L_0x0004;
                case 8: goto L_0x004c;
                case 9: goto L_0x004f;
                default: goto L_0x0004;
            }
        L_0x0004:
            r3 = 0
        L_0x0005:
            java.io.ByteArrayOutputStream r2 = r10.t     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2.reset()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.io.ByteArrayOutputStream r4 = r10.t     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2.writeObject(r12)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2.flush()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.clearBindings()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 1
            long r4 = r12.u()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindLong(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 2
            long r4 = r12.t()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindLong(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 3
            java.io.ByteArrayOutputStream r4 = r10.t     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            byte[] r4 = r4.toByteArray()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindBlob(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            switch(r11) {
                case 1: goto L_0x0052;
                case 2: goto L_0x010b;
                case 3: goto L_0x018e;
                case 4: goto L_0x01ad;
                case 5: goto L_0x0037;
                case 6: goto L_0x01d5;
                case 7: goto L_0x0037;
                case 8: goto L_0x0206;
                case 9: goto L_0x0215;
                default: goto L_0x0037;
            }     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
        L_0x0037:
            r2 = r3
        L_0x0038:
            r2.executeInsert()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
        L_0x003b:
            monitor-exit(r10)
            return
        L_0x003d:
            android.database.sqlite.SQLiteStatement r3 = r10.e     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0005
        L_0x0040:
            android.database.sqlite.SQLiteStatement r3 = r10.f     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0005
        L_0x0043:
            android.database.sqlite.SQLiteStatement r3 = r10.g     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0005
        L_0x0046:
            android.database.sqlite.SQLiteStatement r3 = r10.h     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0005
        L_0x0049:
            android.database.sqlite.SQLiteStatement r3 = r10.i     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0005
        L_0x004c:
            android.database.sqlite.SQLiteStatement r3 = r10.j     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0005
        L_0x004f:
            android.database.sqlite.SQLiteStatement r3 = r10.k     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0005
        L_0x0052:
            r0 = r12
            com.agilebinary.mobilemonitor.client.a.b.s r0 = (com.agilebinary.mobilemonitor.client.a.b.s) r0     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = r0
            r4 = 4
            android.content.Context r5 = r10.c     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.lang.String r5 = r2.b(r5)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r4, r5)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r4 = 5
            android.content.Context r5 = r10.c     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.lang.String r5 = r2.c(r5)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r4, r5)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.lang.Double r2 = r2.f()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            if (r2 != 0) goto L_0x00b3
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
        L_0x0072:
            r2 = 6
            r3.bindDouble(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 9
            byte r4 = r12.k()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            long r4 = (long) r4     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindLong(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            boolean r2 = r12 instanceof com.agilebinary.mobilemonitor.client.a.b.a     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            if (r2 == 0) goto L_0x00b8
            com.agilebinary.mobilemonitor.client.a.b.a r12 = (com.agilebinary.mobilemonitor.client.a.b.a) r12     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 7
            double r4 = r12.a()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindDouble(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 8
            double r4 = r12.b()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindDouble(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            boolean r2 = r12.e()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            if (r2 == 0) goto L_0x022b
            r4 = 1
        L_0x009f:
            r2 = 10
            r3.bindLong(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            boolean r2 = r12.d()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            if (r2 == 0) goto L_0x022f
            r4 = 1
        L_0x00ac:
            r2 = 11
            r3.bindLong(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = r3
            goto L_0x0038
        L_0x00b3:
            double r4 = r2.doubleValue()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0072
        L_0x00b8:
            boolean r2 = r12 instanceof com.agilebinary.mobilemonitor.client.a.b.n     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            if (r2 == 0) goto L_0x00f5
            com.agilebinary.mobilemonitor.client.a.b.n r12 = (com.agilebinary.mobilemonitor.client.a.b.n) r12     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            com.agilebinary.mobilemonitor.client.android.a.a.b r2 = r12.l()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            if (r2 == 0) goto L_0x00e2
            r4 = 7
            double r6 = r2.a()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindDouble(r4, r6)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r4 = 8
            double r6 = r2.b()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindDouble(r4, r6)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
        L_0x00d5:
            r2 = 10
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 11
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = r3
            goto L_0x0038
        L_0x00e2:
            r2 = 7
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 8
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x00d5
        L_0x00ec:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x00f2 }
            goto L_0x003b
        L_0x00f2:
            r2 = move-exception
            monitor-exit(r10)
            throw r2
        L_0x00f5:
            r2 = 7
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 8
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 10
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 11
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = r3
            goto L_0x0038
        L_0x010b:
            com.agilebinary.mobilemonitor.client.a.b.u r12 = (com.agilebinary.mobilemonitor.client.a.b.u) r12     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 4
            java.lang.String r4 = r12.e()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.lang.String r5 = r12.f()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.a.b(r4, r5)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            android.content.Context r5 = r10.c     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            long r6 = r12.c()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            long r8 = r12.b()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            long r6 = r6 - r8
            r8 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 / r8
            int r2 = (int) r6     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            byte r4 = r12.d()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            switch(r4) {
                case -1: goto L_0x017f;
                case 0: goto L_0x0133;
                case 1: goto L_0x0148;
                case 2: goto L_0x0157;
                case 3: goto L_0x0170;
                default: goto L_0x0133;
            }
        L_0x0133:
            java.lang.String r4 = ""
            java.lang.String r2 = ""
        L_0x0137:
            r5 = 5
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r7 = 0
            r6[r7] = r2     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.lang.String r2 = java.lang.String.format(r4, r6)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r5, r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = r3
            goto L_0x0038
        L_0x0148:
            r2 = 2131099859(0x7f0600d3, float:1.7812083E38)
            java.lang.String r2 = r5.getString(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r4 = 2131099864(0x7f0600d8, float:1.7812093E38)
            java.lang.String r4 = r5.getString(r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0137
        L_0x0157:
            r4 = 2131099865(0x7f0600d9, float:1.7812095E38)
            java.lang.String r4 = r5.getString(r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            if (r2 <= 0) goto L_0x0168
            r2 = 2131099860(0x7f0600d4, float:1.7812085E38)
            java.lang.String r2 = r5.getString(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0137
        L_0x0168:
            r2 = 2131099863(0x7f0600d7, float:1.7812091E38)
            java.lang.String r2 = r5.getString(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0137
        L_0x0170:
            r2 = 2131099862(0x7f0600d6, float:1.781209E38)
            java.lang.String r2 = r5.getString(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r4 = 2131099864(0x7f0600d8, float:1.7812093E38)
            java.lang.String r4 = r5.getString(r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0137
        L_0x017f:
            r2 = 2131099861(0x7f0600d5, float:1.7812087E38)
            java.lang.String r2 = r5.getString(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r4 = 2131099865(0x7f0600d9, float:1.7812095E38)
            java.lang.String r4 = r5.getString(r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0137
        L_0x018e:
            android.database.sqlite.SQLiteStatement r2 = r10.g     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            com.agilebinary.mobilemonitor.client.a.b.j r12 = (com.agilebinary.mobilemonitor.client.a.b.j) r12     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3 = 4
            android.content.Context r4 = r10.c     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.lang.String r4 = r12.a(r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2.bindString(r3, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3 = 5
            java.lang.String r4 = r12.d()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2.bindString(r3, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0038
        L_0x01a6:
            r2 = move-exception
            com.agilebinary.mobilemonitor.client.android.b.h r3 = new com.agilebinary.mobilemonitor.client.android.b.h     // Catch:{ all -> 0x00f2 }
            r3.<init>(r2)     // Catch:{ all -> 0x00f2 }
            throw r3     // Catch:{ all -> 0x00f2 }
        L_0x01ad:
            com.agilebinary.mobilemonitor.client.a.b.f r12 = (com.agilebinary.mobilemonitor.client.a.b.f) r12     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 4
            android.content.Context r4 = r10.c     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.lang.String r4 = r12.a(r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 5
            java.lang.String r4 = r12.l()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            byte[] r2 = r12.m()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            if (r2 != 0) goto L_0x01ce
            r2 = 6
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = r3
            goto L_0x0038
        L_0x01ce:
            r4 = 6
            r3.bindBlob(r4, r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = r3
            goto L_0x0038
        L_0x01d5:
            boolean r2 = r12 instanceof com.agilebinary.mobilemonitor.client.a.b.e     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            if (r2 == 0) goto L_0x01ed
            r4 = 4
            r0 = r12
            com.agilebinary.mobilemonitor.client.a.b.e r0 = (com.agilebinary.mobilemonitor.client.a.b.e) r0     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = r0
            android.content.Context r5 = r10.c     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.lang.String r2 = r2.a(r5)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r4, r2)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 5
            java.lang.String r4 = ""
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
        L_0x01ed:
            boolean r2 = r12 instanceof com.agilebinary.mobilemonitor.client.a.b.g     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            if (r2 == 0) goto L_0x0037
            com.agilebinary.mobilemonitor.client.a.b.g r12 = (com.agilebinary.mobilemonitor.client.a.b.g) r12     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 4
            java.lang.String r4 = r12.f()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 5
            java.lang.String r4 = r12.g()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = r3
            goto L_0x0038
        L_0x0206:
            r2 = 4
            com.agilebinary.mobilemonitor.client.a.b.d r12 = (com.agilebinary.mobilemonitor.client.a.b.d) r12     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            android.content.Context r4 = r10.c     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.lang.String r4 = r12.a(r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = r3
            goto L_0x0038
        L_0x0215:
            com.agilebinary.mobilemonitor.client.a.b.c r12 = (com.agilebinary.mobilemonitor.client.a.b.c) r12     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 4
            android.content.Context r4 = r10.c     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            java.lang.String r4 = r12.a(r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r2 = 5
            java.lang.String r4 = r12.c()     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00ec, OutOfMemoryError -> 0x01a6 }
            goto L_0x0037
        L_0x022b:
            r4 = 0
            goto L_0x009f
        L_0x022f:
            r4 = 0
            goto L_0x00ac
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.b.j.a(byte, com.agilebinary.mobilemonitor.client.a.b.o):void");
    }

    public static void e() {
        SQLiteDatabase.releaseMemory();
    }

    private static /* synthetic */ String h(byte b2) {
        switch (b2) {
            case 1:
                return com.agilebinary.a.a.a.h.f.I("Y\u0016V\u0018A\u0010Z\u0017");
            case 2:
                return e.I("U\u0012Z\u001f");
            case 3:
                return com.agilebinary.a.a.a.h.f.I("\nX\n");
            case 4:
                return e.I("\u001e[\u0000");
            case 5:
            case 7:
            default:
                return null;
            case 6:
                return com.agilebinary.a.a.a.h.f.I("\u000eP\u001b");
            case 8:
                return e.I("E\nE\u0007i\u001e");
            case 9:
                return i.f173a;
        }
    }

    private /* synthetic */ void h(byte b2, long j2) {
        synchronized (this) {
            "" + ((int) b2);
            com.agilebinary.a.a.a.h.f.I("\u0015\rFD") + j2;
            this.p.bindLong(1, (long) b2);
            this.p.bindLong(2, j2);
            this.p.executeInsert();
        }
    }

    public final long a(byte b2) {
        long j2;
        synchronized (this) {
            "" + ((int) b2);
            this.o.bindLong(1, (long) b2);
            j2 = 0;
            try {
                j2 = this.o.simpleQueryForLong();
            } catch (SQLiteDoneException e2) {
            }
            e.I("\u0016N") + j2;
        }
        return j2;
    }

    public final long a(byte b2, long j2) {
        "" + ((int) b2);
        com.agilebinary.a.a.a.h.f.I("\u0015\nA\u0018G\rz\u001fq\u0018LD") + j2;
        SQLiteStatement compileStatement = this.d.compileStatement(String.format(e.I("e6z6u'\u0016\u001eW\u000b\u001eV\u0004WEZ\u00165d<{S\u0013B\u0012\u0000\u0016S"), h(b2), b.b));
        long simpleQueryForLong = compileStatement.simpleQueryForLong();
        compileStatement.close();
        com.agilebinary.a.a.a.h.f.I("\u0015D") + simpleQueryForLong;
        return simpleQueryForLong == 0 ? j2 : simpleQueryForLong;
    }

    public final List a(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add((s) g((byte) 1, ((Long) it.next()).longValue()));
        }
        return arrayList;
    }

    public final void a() {
        this.d.close();
    }

    public final void a(byte b2, int i2, long j2) {
        synchronized (this) {
            e.I("S\u0019S") + ((int) b2);
            com.agilebinary.a.a.a.h.f.I("Y\u001aY") + i2;
            e.I("S\u0019S") + new Date(j2);
            if (this.n != -1) {
                throw new IllegalStateException();
            }
            this.n = b2;
            if (b2 == 1) {
                this.q = new ArrayList();
                this.r.a();
            }
            h(b2, j2);
            this.w = new ByteArrayOutputStream(32000);
        }
    }

    public final void a(String str, byte b2, long j2, long j3, boolean z, boolean z2, int i2, InputStream inputStream) {
        synchronized (this) {
            com.agilebinary.a.a.a.h.f.I("\u0015\rL\tPD") + ((int) b2);
            e.I("\u0016\u001aRN") + j2;
            if (this.n != -1) {
                try {
                    o a2 = com.agilebinary.mobilemonitor.client.a.f.a(str, j2, j3, z, z2, i2, inputStream, MyApplication.g(), MyApplication.f141a, this.c, this.w);
                    if (a2 != null) {
                        if (this.n != 1) {
                            a(b2, a2);
                        } else if (!(a2 instanceof c) || !((c) a2).a()) {
                            a(b2, a2);
                        } else if (this.r.a((c) a2)) {
                            a(b2, a2);
                        } else {
                            this.q.add((c) a2);
                        }
                    }
                } catch (IOException e2) {
                    e2.printStackTrace();
                } catch (OutOfMemoryError e3) {
                    throw new h(e3);
                }
            }
        }
        return;
    }

    public final void a(boolean z) {
        synchronized (this) {
            "" + z;
            this.w = null;
            if (this.n != -1) {
                if (!z) {
                    if (this.n == 1) {
                        this.r.a(this.q);
                        for (c cVar : this.q) {
                            try {
                                a(this.n, (s) cVar);
                            } catch (h e2) {
                                e2.printStackTrace();
                            }
                        }
                        this.q.clear();
                    }
                }
                this.n = -1;
                this.q = null;
                this.r.b();
            }
        }
    }

    public final boolean a(byte b2, Calendar calendar) {
        boolean z = true;
        synchronized (this) {
            "" + ((int) b2);
            com.agilebinary.a.a.a.h.f.I("YVD") + calendar;
            this.m.bindLong(1, (long) b2);
            try {
                if (this.m.simpleQueryForLong() != ((long) a(calendar))) {
                    z = false;
                }
            } catch (SQLiteDoneException e2) {
                z = false;
            }
            e.I("N") + z;
        }
        return z;
    }

    public final long b(byte b2) {
        long j2;
        synchronized (this) {
            "" + ((int) b2);
            this.v.bindLong(1, (long) b2);
            j2 = -1;
            try {
                j2 = this.v.simpleQueryForLong();
            } catch (SQLiteDoneException e2) {
            }
            com.agilebinary.a.a.a.h.f.I("\u0015D") + j2;
        }
        return j2;
    }

    public final void b() {
        this.d.execSQL(String.format(e.I("r6z6b6\u00165d<{S\u0013B\u0012\u0000"), e.I("Z\u001cU\u0012B\u001aY\u001d")));
        this.d.execSQL(String.format(e.I("r6z6b6\u00165d<{S\u0013B\u0012\u0000"), com.agilebinary.a.a.a.h.f.I("V\u0018Y\u0015")));
        this.d.execSQL(String.format(e.I("r6z6b6\u00165d<{S\u0013B\u0012\u0000"), e.I("\u0000[\u0000")));
        this.d.execSQL(String.format(e.I("r6z6b6\u00165d<{S\u0013B\u0012\u0000"), com.agilebinary.a.a.a.h.f.I("\u0014X\n")));
        this.d.execSQL(String.format(e.I("r6z6b6\u00165d<{S\u0013B\u0012\u0000"), e.I("\u0004S\u0011")));
        this.d.execSQL(String.format(e.I("r6z6b6\u00165d<{S\u0013B\u0012\u0000"), com.agilebinary.a.a.a.h.f.I("F\u0000F\rj\u0014")));
        this.d.execSQL(String.format(e.I("r6z6b6\u00165d<{S\u0013B\u0012\u0000"), i.f173a));
        this.d.execSQL(String.format(e.I("r6z6b6\u00165d<{S\u0013B\u0012\u0000"), e.I("Z\u0012E\u0007E\u0016S\u001d")));
        this.d.execSQL(String.format(e.I("r6z6b6\u00165d<{S\u0013B\u0012\u0000"), com.agilebinary.a.a.a.h.f.I("V\fG\u001dT\u0000")));
        this.d.execSQL(String.format(e.I("r6z6b6\u00165d<{S\u0013B\u0012\u0000"), e.I("\u001cZ\u0017S\u0000B\u0016@\u0016X\u0007")));
    }

    public final void b(byte b2, long j2) {
        synchronized (this) {
            "" + ((int) b2);
            com.agilebinary.a.a.a.h.f.I("\u0015\rFD") + j2;
            if (j2 > b(b2)) {
                e.I("C\u0003R\u0012B\u0016z\u0012E\u0007E\u0000S\u001ds\u0005S\u001dB'ES\u000bS") + j2;
                this.u.bindLong(1, (long) b2);
                this.u.bindLong(2, j2);
                this.u.executeInsert();
            }
        }
    }

    public final void b(byte b2, Calendar calendar) {
        synchronized (this) {
            "" + ((int) b2);
            com.agilebinary.a.a.a.h.f.I("YVD") + calendar;
            this.s = calendar;
            int a2 = a(calendar);
            this.l.bindLong(1, (long) b2);
            this.l.bindLong(2, (long) a2);
            this.l.executeInsert();
        }
    }

    public final void c() {
        this.r.c();
    }

    public final void c(byte b2) {
        this.d.execSQL(String.format(e.I("r6z6b6\u00165d<{S\u0013B\u0012\u0000"), h(b2)));
    }

    public final boolean c(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format(e.I(" s?s0bSU\u001cC\u001dB[\u001cZ\u00165d<{S\u0013B\u0012\u0000\u0016\u0004^\u0016D\u0016\u0016V\u0004WEO\tS"), h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return simpleQueryForLong > 0;
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return false;
        }
    }

    public final o d(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format(com.agilebinary.a.a.a.h.f.I("f<y<v-\u0015\u0014T\u0001\u001d\\\u0007]FP\u0015?g6xY\u0010H\u0011\n\u0015\u000e]\u001cG\u001c\u0015\\\u0007]FE\nY"), h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    public final Calendar d() {
        "" + this.s;
        return this.s;
    }

    public final void d(byte b2) {
        synchronized (this) {
            "" + ((int) b2);
            b(b2, a(b2, 0));
        }
    }

    public final Cursor e(byte b2) {
        Cursor query;
        String[] strArr = null;
        synchronized (this) {
            SQLiteDatabase sQLiteDatabase = this.d;
            String h2 = h(b2);
            switch (b2) {
                case 1:
                    strArr = f.f171a;
                    break;
                case 2:
                    strArr = c.f168a;
                    break;
                case 3:
                    strArr = a.f166a;
                    break;
                case 4:
                    strArr = d.f169a;
                    break;
                case 6:
                    strArr = k.f175a;
                    break;
                case 8:
                    strArr = l.f176a;
                    break;
                case 9:
                    strArr = i.h;
                    break;
            }
            query = sQLiteDatabase.query(h2, strArr, null, null, null, null, b.b + e.I("Sr6e0"));
        }
        return query;
    }

    public final boolean e(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format(com.agilebinary.a.a.a.h.f.I("*p5p:aYV\u0016@\u0017AQ\u001fP\u0015?g6xY\u0010H\u0011\n\u0015\u000e]\u001cG\u001c\u0015\\\u0007]FG\nY"), h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return simpleQueryForLong > 0;
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return false;
        }
    }

    public final o f(byte b2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format(e.I("e6z6u'\u0016\u001e_\u001d\u001eV\u0004WEZ\u00165d<{S\u0013B\u0012\u0000\u0016S"), h(b2), b.b));
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    public final o f(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format(com.agilebinary.a.a.a.h.f.I("f<y<v-\u0015\u0014\\\u0017\u001d\\\u0007]FP\u0015?g6xY\u0010H\u0011\n\u0015\u000e]\u001cG\u001c\u0015\\\u0007]FG\nY"), h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    public final o g(byte b2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format(e.I("e6z6u'\u0016\u001eW\u000b\u001eV\u0004WEZ\u00165d<{S\u0013B\u0012\u0000\u0016S"), h(b2), b.b));
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    public final o g(byte b2, long j2) {
        Cursor query = this.d.query(h(b2), b.e, b.b + com.agilebinary.a.a.a.h.f.I("\bF"), new String[]{String.valueOf(j2)}, null, null, null);
        if (!query.moveToFirst()) {
            query.close();
            return null;
        }
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(query.getBlob(0));
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            o oVar = (o) objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            query.close();
            return oVar;
        } catch (StreamCorruptedException e2) {
            a.e(e2);
            query.close();
            return null;
        } catch (OptionalDataException e3) {
            a.e(e3);
            query.close();
            return null;
        } catch (IOException e4) {
            a.e(e4);
            query.close();
            return null;
        } catch (ClassNotFoundException e5) {
            a.e(e5);
            query.close();
            return null;
        } catch (OutOfMemoryError e6) {
            a.e(e6);
            query.close();
            return null;
        }
    }
}
