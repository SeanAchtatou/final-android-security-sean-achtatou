package com.agilebinary.mobilemonitor.client.android.c;

import android.content.Context;
import com.agilebinary.a.a.a.c.a.k;
import com.agilebinary.a.a.a.c.b.a.j;
import com.agilebinary.mobilemonitor.client.a.b.i;
import com.biige.client.android.R;
import java.text.NumberFormat;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static NumberFormat f179a = NumberFormat.getIntegerInstance();

    private static /* synthetic */ double a(double d, String str) {
        return k.I("'\u0019\"").equals(str) ? (d * 3.6d) / 1.609d : d * 3.6d;
    }

    public static CharSequence a(Context context, i iVar) {
        if (iVar.n()) {
            String string = context.getString(R.string.speed_unit);
            String str = "";
            try {
                str = context.getResources().getString(com.a.a.a.a.class.getField(j.I("#J5_4e%T9N\u000f^9I V1C\u000f") + string).getInt(null));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Double valueOf = iVar.q() ? Double.valueOf(a(iVar.o(), string)) : Double.valueOf(0.0d);
            String c = c.a().c(iVar.p());
            if (iVar.q()) {
                return context.getString(R.string.msg_event_speed, f179a.format(valueOf.doubleValue()), str, c);
            }
        }
        return context.getString(R.string.label_event_not_available);
    }

    public static CharSequence a(Context context, Double d) {
        if (d == null) {
            return context.getString(R.string.label_event_not_available);
        }
        String string = context.getString(R.string.speed_unit);
        String str = "";
        try {
            str = context.getResources().getString(com.a.a.a.a.class.getField(k.I("9\u0019/\f.6?\u0007#\u001d\u0015\r#\u001a:\u0005+\u0010\u0015") + string).getInt(null));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return context.getString(R.string.msg_event_speed_short, f179a.format(Double.valueOf(a(d.doubleValue(), string)).doubleValue()), str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:264:0x07d0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence a(android.content.Context r8, java.lang.String r9) {
        /*
            r7 = 1
            r4 = 0
            r2 = 0
            java.lang.String r0 = "f,"
            java.lang.String r0 = com.agilebinary.a.a.a.c.b.a.j.I(r0)
            java.lang.String[] r1 = r9.split(r0)
            int r0 = r1.length
            if (r0 <= 0) goto L_0x07d3
            r0 = 0
            r0 = r1[r0]     // Catch:{ Exception -> 0x007f }
            java.util.TimeZone r0 = java.util.TimeZone.getTimeZone(r0)     // Catch:{ Exception -> 0x007f }
        L_0x0017:
            if (r0 == 0) goto L_0x07d3
            java.lang.String r0 = r0.getDisplayName()
        L_0x001d:
            if (r0 != 0) goto L_0x0021
            r0 = r1[r4]
        L_0x0021:
            int r3 = r1.length
            if (r3 <= r7) goto L_0x07d0
            r3 = r1[r7]
            int r1 = java.lang.Integer.parseInt(r3)     // Catch:{ NumberFormatException -> 0x0085 }
        L_0x002a:
            if (r1 != 0) goto L_0x0088
            java.lang.String r3 = r3.toUpperCase()
        L_0x0030:
            java.lang.Class<com.a.a.a.a> r1 = com.a.a.a.a.class
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07c8 }
            r5.<init>()     // Catch:{ Exception -> 0x07c8 }
            java.lang.String r6 = "\t*\u0015"
            java.lang.String r6 = com.agilebinary.a.a.a.c.a.k.I(r6)     // Catch:{ Exception -> 0x07c8 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x07c8 }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ Exception -> 0x07c8 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x07c8 }
            java.lang.reflect.Field r1 = r1.getField(r5)     // Catch:{ Exception -> 0x07c8 }
            r5 = 0
            int r1 = r1.getInt(r5)     // Catch:{ Exception -> 0x07c8 }
            android.content.res.Resources r5 = r8.getResources()     // Catch:{ Exception -> 0x07c8 }
            java.lang.String r1 = r5.getString(r1)     // Catch:{ Exception -> 0x07c8 }
            r2 = r3
        L_0x005b:
            if (r1 != 0) goto L_0x006c
            android.content.res.Resources r1 = r8.getResources()
            r3 = 2131099830(0x7f0600b6, float:1.7812024E38)
            java.lang.Object[] r5 = new java.lang.Object[r7]
            r5[r4] = r2
            java.lang.String r1 = r1.getString(r3, r5)
        L_0x006c:
            android.content.res.Resources r2 = r8.getResources()
            r3 = 2131099831(0x7f0600b7, float:1.7812026E38)
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r5[r4] = r1
            r5[r7] = r0
            java.lang.String r0 = r2.getString(r3, r5)
            return r0
        L_0x007f:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x0017
        L_0x0085:
            r1 = move-exception
            r1 = r4
            goto L_0x002a
        L_0x0088:
            switch(r1) {
                case 202: goto L_0x02f8;
                case 203: goto L_0x008b;
                case 204: goto L_0x0508;
                case 205: goto L_0x008b;
                case 206: goto L_0x0120;
                case 207: goto L_0x008b;
                case 208: goto L_0x02b0;
                case 209: goto L_0x008b;
                case 210: goto L_0x008b;
                case 211: goto L_0x008b;
                case 212: goto L_0x04b8;
                case 213: goto L_0x00a9;
                case 214: goto L_0x0668;
                case 215: goto L_0x008b;
                case 216: goto L_0x0358;
                case 217: goto L_0x008b;
                case 218: goto L_0x0150;
                case 219: goto L_0x0208;
                case 220: goto L_0x0620;
                case 221: goto L_0x008b;
                case 222: goto L_0x03a0;
                case 223: goto L_0x008b;
                case 224: goto L_0x008b;
                case 225: goto L_0x0790;
                case 226: goto L_0x05c0;
                case 227: goto L_0x008b;
                case 228: goto L_0x0698;
                case 229: goto L_0x008b;
                case 230: goto L_0x0220;
                case 231: goto L_0x0640;
                case 232: goto L_0x00e8;
                case 233: goto L_0x008b;
                case 234: goto L_0x0730;
                case 235: goto L_0x0728;
                case 236: goto L_0x008b;
                case 237: goto L_0x008b;
                case 238: goto L_0x0230;
                case 239: goto L_0x008b;
                case 240: goto L_0x0690;
                case 241: goto L_0x008b;
                case 242: goto L_0x0548;
                case 243: goto L_0x008b;
                case 244: goto L_0x02a8;
                case 245: goto L_0x008b;
                case 246: goto L_0x0438;
                case 247: goto L_0x0408;
                case 248: goto L_0x0280;
                case 249: goto L_0x008b;
                case 250: goto L_0x05c8;
                case 251: goto L_0x008b;
                case 252: goto L_0x008b;
                case 253: goto L_0x008b;
                case 254: goto L_0x008b;
                case 255: goto L_0x0708;
                case 256: goto L_0x008b;
                case 257: goto L_0x0118;
                case 258: goto L_0x008b;
                case 259: goto L_0x04b0;
                case 260: goto L_0x0598;
                case 261: goto L_0x008b;
                case 262: goto L_0x02e0;
                case 263: goto L_0x008b;
                case 264: goto L_0x008b;
                case 265: goto L_0x008b;
                case 266: goto L_0x02f0;
                case 267: goto L_0x008b;
                case 268: goto L_0x05a0;
                case 269: goto L_0x008b;
                case 270: goto L_0x0440;
                case 271: goto L_0x008b;
                case 272: goto L_0x0390;
                case 273: goto L_0x008b;
                case 274: goto L_0x0360;
                case 275: goto L_0x008b;
                case 276: goto L_0x0094;
                case 277: goto L_0x008b;
                case 278: goto L_0x0480;
                case 279: goto L_0x008b;
                case 280: goto L_0x0218;
                case 281: goto L_0x008b;
                case 282: goto L_0x02d8;
                case 283: goto L_0x00d0;
                case 284: goto L_0x0178;
                case 285: goto L_0x008b;
                case 286: goto L_0x06e8;
                case 287: goto L_0x008b;
                case 288: goto L_0x0298;
                case 289: goto L_0x008b;
                case 290: goto L_0x0300;
                case 291: goto L_0x008b;
                case 292: goto L_0x0600;
                case 293: goto L_0x0648;
                case 294: goto L_0x0450;
                case 295: goto L_0x0430;
                case 296: goto L_0x008b;
                case 297: goto L_0x04c8;
                case 298: goto L_0x008b;
                case 299: goto L_0x008b;
                case 300: goto L_0x008b;
                case 301: goto L_0x008b;
                case 302: goto L_0x01a0;
                case 303: goto L_0x008b;
                case 304: goto L_0x008b;
                case 305: goto L_0x008b;
                case 306: goto L_0x008b;
                case 307: goto L_0x008b;
                case 308: goto L_0x05e8;
                case 309: goto L_0x008b;
                case 310: goto L_0x0738;
                case 311: goto L_0x0740;
                case 312: goto L_0x0748;
                case 313: goto L_0x0750;
                case 314: goto L_0x0758;
                case 315: goto L_0x0760;
                case 316: goto L_0x0768;
                case 317: goto L_0x008b;
                case 318: goto L_0x008b;
                case 319: goto L_0x008b;
                case 320: goto L_0x008b;
                case 321: goto L_0x008b;
                case 322: goto L_0x008b;
                case 323: goto L_0x008b;
                case 324: goto L_0x008b;
                case 325: goto L_0x008b;
                case 326: goto L_0x008b;
                case 327: goto L_0x008b;
                case 328: goto L_0x008b;
                case 329: goto L_0x008b;
                case 330: goto L_0x05a8;
                case 331: goto L_0x008b;
                case 332: goto L_0x0770;
                case 333: goto L_0x008b;
                case 334: goto L_0x04a0;
                case 335: goto L_0x008b;
                case 336: goto L_0x008b;
                case 337: goto L_0x008b;
                case 338: goto L_0x03a8;
                case 339: goto L_0x008b;
                case 340: goto L_0x0310;
                case 341: goto L_0x008b;
                case 342: goto L_0x0110;
                case 343: goto L_0x008b;
                case 344: goto L_0x00c0;
                case 345: goto L_0x008b;
                case 346: goto L_0x01b0;
                case 347: goto L_0x008b;
                case 348: goto L_0x0168;
                case 349: goto L_0x008b;
                case 350: goto L_0x0138;
                case 351: goto L_0x008b;
                case 352: goto L_0x0308;
                case 353: goto L_0x008b;
                case 354: goto L_0x04d0;
                case 355: goto L_0x008b;
                case 356: goto L_0x05d8;
                case 357: goto L_0x008b;
                case 358: goto L_0x05e0;
                case 359: goto L_0x008b;
                case 360: goto L_0x05f0;
                case 361: goto L_0x008b;
                case 362: goto L_0x0510;
                case 363: goto L_0x00d8;
                case 364: goto L_0x00f8;
                case 365: goto L_0x00b8;
                case 366: goto L_0x0240;
                case 367: goto L_0x008b;
                case 368: goto L_0x0210;
                case 369: goto L_0x008b;
                case 370: goto L_0x0248;
                case 371: goto L_0x008b;
                case 372: goto L_0x0340;
                case 373: goto L_0x008b;
                case 374: goto L_0x06d8;
                case 375: goto L_0x008b;
                case 376: goto L_0x06f8;
                case 377: goto L_0x008b;
                case 378: goto L_0x008b;
                case 379: goto L_0x008b;
                case 380: goto L_0x008b;
                case 381: goto L_0x008b;
                case 382: goto L_0x008b;
                case 383: goto L_0x008b;
                case 384: goto L_0x008b;
                case 385: goto L_0x008b;
                case 386: goto L_0x008b;
                case 387: goto L_0x008b;
                case 388: goto L_0x008b;
                case 389: goto L_0x008b;
                case 390: goto L_0x008b;
                case 391: goto L_0x008b;
                case 392: goto L_0x008b;
                case 393: goto L_0x008b;
                case 394: goto L_0x008b;
                case 395: goto L_0x008b;
                case 396: goto L_0x008b;
                case 397: goto L_0x008b;
                case 398: goto L_0x008b;
                case 399: goto L_0x008b;
                case 400: goto L_0x00f0;
                case 401: goto L_0x03c8;
                case 402: goto L_0x0140;
                case 403: goto L_0x008b;
                case 404: goto L_0x0368;
                case 405: goto L_0x0370;
                case 406: goto L_0x008b;
                case 407: goto L_0x008b;
                case 408: goto L_0x008b;
                case 409: goto L_0x008b;
                case 410: goto L_0x0558;
                case 411: goto L_0x008b;
                case 412: goto L_0x008d;
                case 413: goto L_0x0670;
                case 414: goto L_0x04e8;
                case 415: goto L_0x0410;
                case 416: goto L_0x03c0;
                case 417: goto L_0x06a0;
                case 418: goto L_0x0388;
                case 419: goto L_0x03f0;
                case 420: goto L_0x0610;
                case 421: goto L_0x07b0;
                case 422: goto L_0x0550;
                case 423: goto L_0x0568;
                case 424: goto L_0x0710;
                case 425: goto L_0x0398;
                case 426: goto L_0x0100;
                case 427: goto L_0x05b0;
                case 428: goto L_0x04c0;
                case 429: goto L_0x0500;
                case 430: goto L_0x0718;
                case 431: goto L_0x0720;
                case 432: goto L_0x0380;
                case 433: goto L_0x008b;
                case 434: goto L_0x0780;
                case 435: goto L_0x008b;
                case 436: goto L_0x06b0;
                case 437: goto L_0x03f8;
                case 438: goto L_0x06f0;
                case 439: goto L_0x008b;
                case 440: goto L_0x03b8;
                case 441: goto L_0x03b0;
                case 442: goto L_0x008b;
                case 443: goto L_0x008b;
                case 444: goto L_0x008b;
                case 445: goto L_0x008b;
                case 446: goto L_0x008b;
                case 447: goto L_0x008b;
                case 448: goto L_0x008b;
                case 449: goto L_0x008b;
                case 450: goto L_0x03e8;
                case 451: goto L_0x008b;
                case 452: goto L_0x07a0;
                case 453: goto L_0x008b;
                case 454: goto L_0x0350;
                case 455: goto L_0x0448;
                case 456: goto L_0x0190;
                case 457: goto L_0x0400;
                case 458: goto L_0x008b;
                case 459: goto L_0x008b;
                case 460: goto L_0x01d0;
                case 461: goto L_0x008b;
                case 462: goto L_0x008b;
                case 463: goto L_0x008b;
                case 464: goto L_0x008b;
                case 465: goto L_0x008b;
                case 466: goto L_0x06a8;
                case 467: goto L_0x03e0;
                case 468: goto L_0x008b;
                case 469: goto L_0x008b;
                case 470: goto L_0x0108;
                case 471: goto L_0x008b;
                case 472: goto L_0x0470;
                case 473: goto L_0x008b;
                case 474: goto L_0x008b;
                case 475: goto L_0x008b;
                case 476: goto L_0x008b;
                case 477: goto L_0x008b;
                case 478: goto L_0x008b;
                case 479: goto L_0x008b;
                case 480: goto L_0x008b;
                case 481: goto L_0x008b;
                case 482: goto L_0x008b;
                case 483: goto L_0x008b;
                case 484: goto L_0x008b;
                case 485: goto L_0x008b;
                case 486: goto L_0x008b;
                case 487: goto L_0x008b;
                case 488: goto L_0x008b;
                case 489: goto L_0x008b;
                case 490: goto L_0x008b;
                case 491: goto L_0x008b;
                case 492: goto L_0x008b;
                case 493: goto L_0x008b;
                case 494: goto L_0x008b;
                case 495: goto L_0x008b;
                case 496: goto L_0x008b;
                case 497: goto L_0x008b;
                case 498: goto L_0x008b;
                case 499: goto L_0x008b;
                case 500: goto L_0x008b;
                case 501: goto L_0x008b;
                case 502: goto L_0x0468;
                case 503: goto L_0x008b;
                case 504: goto L_0x008b;
                case 505: goto L_0x00e0;
                case 506: goto L_0x008b;
                case 507: goto L_0x008b;
                case 508: goto L_0x008b;
                case 509: goto L_0x008b;
                case 510: goto L_0x0378;
                case 511: goto L_0x008b;
                case 512: goto L_0x008b;
                case 513: goto L_0x008b;
                case 514: goto L_0x0250;
                case 515: goto L_0x0590;
                case 516: goto L_0x008b;
                case 517: goto L_0x008b;
                case 518: goto L_0x008b;
                case 519: goto L_0x008b;
                case 520: goto L_0x06c0;
                case 521: goto L_0x008b;
                case 522: goto L_0x008b;
                case 523: goto L_0x008b;
                case 524: goto L_0x008b;
                case 525: goto L_0x0638;
                case 526: goto L_0x008b;
                case 527: goto L_0x008b;
                case 528: goto L_0x0170;
                case 529: goto L_0x008b;
                case 530: goto L_0x0520;
                case 531: goto L_0x008b;
                case 532: goto L_0x008b;
                case 533: goto L_0x008b;
                case 534: goto L_0x0540;
                case 535: goto L_0x0318;
                case 536: goto L_0x04f8;
                case 537: goto L_0x0578;
                case 538: goto L_0x008b;
                case 539: goto L_0x06d0;
                case 540: goto L_0x0650;
                case 541: goto L_0x0788;
                case 542: goto L_0x02a0;
                case 543: goto L_0x07a8;
                case 544: goto L_0x00a2;
                case 545: goto L_0x03d8;
                case 546: goto L_0x0518;
                case 547: goto L_0x02c0;
                case 548: goto L_0x01f0;
                case 549: goto L_0x05f8;
                case 550: goto L_0x04a8;
                case 551: goto L_0x0488;
                case 552: goto L_0x0560;
                case 553: goto L_0x008b;
                case 554: goto L_0x008b;
                case 555: goto L_0x008b;
                case 556: goto L_0x008b;
                case 557: goto L_0x008b;
                case 558: goto L_0x008b;
                case 559: goto L_0x008b;
                case 560: goto L_0x008b;
                case 561: goto L_0x008b;
                case 562: goto L_0x008b;
                case 563: goto L_0x008b;
                case 564: goto L_0x008b;
                case 565: goto L_0x008b;
                case 566: goto L_0x008b;
                case 567: goto L_0x008b;
                case 568: goto L_0x008b;
                case 569: goto L_0x008b;
                case 570: goto L_0x008b;
                case 571: goto L_0x008b;
                case 572: goto L_0x008b;
                case 573: goto L_0x008b;
                case 574: goto L_0x008b;
                case 575: goto L_0x008b;
                case 576: goto L_0x008b;
                case 577: goto L_0x008b;
                case 578: goto L_0x008b;
                case 579: goto L_0x008b;
                case 580: goto L_0x008b;
                case 581: goto L_0x008b;
                case 582: goto L_0x008b;
                case 583: goto L_0x008b;
                case 584: goto L_0x008b;
                case 585: goto L_0x008b;
                case 586: goto L_0x008b;
                case 587: goto L_0x008b;
                case 588: goto L_0x008b;
                case 589: goto L_0x008b;
                case 590: goto L_0x008b;
                case 591: goto L_0x008b;
                case 592: goto L_0x008b;
                case 593: goto L_0x008b;
                case 594: goto L_0x008b;
                case 595: goto L_0x008b;
                case 596: goto L_0x008b;
                case 597: goto L_0x008b;
                case 598: goto L_0x008b;
                case 599: goto L_0x008b;
                case 600: goto L_0x008b;
                case 601: goto L_0x008b;
                case 602: goto L_0x0260;
                case 603: goto L_0x009b;
                case 604: goto L_0x04d8;
                case 605: goto L_0x06e0;
                case 606: goto L_0x0428;
                case 607: goto L_0x02d0;
                case 608: goto L_0x0618;
                case 609: goto L_0x0490;
                case 610: goto L_0x0478;
                case 611: goto L_0x0328;
                case 612: goto L_0x0200;
                case 613: goto L_0x0180;
                case 614: goto L_0x0530;
                case 615: goto L_0x06c8;
                case 616: goto L_0x0130;
                case 617: goto L_0x0498;
                case 618: goto L_0x0420;
                case 619: goto L_0x0630;
                case 620: goto L_0x02e8;
                case 621: goto L_0x0538;
                case 622: goto L_0x01c0;
                case 623: goto L_0x01b8;
                case 624: goto L_0x0198;
                case 625: goto L_0x01a8;
                case 626: goto L_0x0608;
                case 627: goto L_0x0270;
                case 628: goto L_0x02c8;
                case 629: goto L_0x01e8;
                case 630: goto L_0x0228;
                case 631: goto L_0x00b0;
                case 632: goto L_0x0330;
                case 633: goto L_0x0628;
                case 634: goto L_0x0678;
                case 635: goto L_0x05d0;
                case 636: goto L_0x0288;
                case 637: goto L_0x0658;
                case 638: goto L_0x0238;
                case 639: goto L_0x03d0;
                case 640: goto L_0x06b8;
                case 641: goto L_0x0700;
                case 642: goto L_0x0188;
                case 643: goto L_0x04e0;
                case 644: goto L_0x008b;
                case 645: goto L_0x07b8;
                case 646: goto L_0x0458;
                case 647: goto L_0x05b8;
                case 648: goto L_0x07c0;
                case 649: goto L_0x04f0;
                case 650: goto L_0x0460;
                case 651: goto L_0x0418;
                case 652: goto L_0x0158;
                case 653: goto L_0x0688;
                case 654: goto L_0x01e0;
                case 655: goto L_0x0660;
                case 656: goto L_0x008b;
                case 657: goto L_0x0278;
                case 658: goto L_0x008b;
                case 659: goto L_0x008b;
                case 660: goto L_0x008b;
                case 661: goto L_0x008b;
                case 662: goto L_0x008b;
                case 663: goto L_0x008b;
                case 664: goto L_0x008b;
                case 665: goto L_0x008b;
                case 666: goto L_0x008b;
                case 667: goto L_0x008b;
                case 668: goto L_0x008b;
                case 669: goto L_0x008b;
                case 670: goto L_0x008b;
                case 671: goto L_0x008b;
                case 672: goto L_0x008b;
                case 673: goto L_0x008b;
                case 674: goto L_0x008b;
                case 675: goto L_0x008b;
                case 676: goto L_0x008b;
                case 677: goto L_0x008b;
                case 678: goto L_0x008b;
                case 679: goto L_0x008b;
                case 680: goto L_0x008b;
                case 681: goto L_0x008b;
                case 682: goto L_0x008b;
                case 683: goto L_0x008b;
                case 684: goto L_0x008b;
                case 685: goto L_0x008b;
                case 686: goto L_0x008b;
                case 687: goto L_0x008b;
                case 688: goto L_0x008b;
                case 689: goto L_0x008b;
                case 690: goto L_0x008b;
                case 691: goto L_0x008b;
                case 692: goto L_0x008b;
                case 693: goto L_0x008b;
                case 694: goto L_0x008b;
                case 695: goto L_0x008b;
                case 696: goto L_0x008b;
                case 697: goto L_0x008b;
                case 698: goto L_0x008b;
                case 699: goto L_0x008b;
                case 700: goto L_0x008b;
                case 701: goto L_0x008b;
                case 702: goto L_0x0128;
                case 703: goto L_0x008b;
                case 704: goto L_0x0320;
                case 705: goto L_0x008b;
                case 706: goto L_0x0268;
                case 707: goto L_0x008b;
                case 708: goto L_0x0348;
                case 709: goto L_0x008b;
                case 710: goto L_0x0528;
                case 711: goto L_0x008b;
                case 712: goto L_0x01f8;
                case 713: goto L_0x008b;
                case 714: goto L_0x0570;
                case 715: goto L_0x008b;
                case 716: goto L_0x0588;
                case 717: goto L_0x008b;
                case 718: goto L_0x008b;
                case 719: goto L_0x008b;
                case 720: goto L_0x008b;
                case 721: goto L_0x008b;
                case 722: goto L_0x00c8;
                case 723: goto L_0x008b;
                case 724: goto L_0x0160;
                case 725: goto L_0x008b;
                case 726: goto L_0x008b;
                case 727: goto L_0x008b;
                case 728: goto L_0x008b;
                case 729: goto L_0x008b;
                case 730: goto L_0x01c8;
                case 731: goto L_0x008b;
                case 732: goto L_0x01d8;
                case 733: goto L_0x008b;
                case 734: goto L_0x0798;
                case 735: goto L_0x008b;
                case 736: goto L_0x0148;
                case 737: goto L_0x008b;
                case 738: goto L_0x0338;
                case 739: goto L_0x008b;
                case 740: goto L_0x0258;
                case 741: goto L_0x008b;
                case 742: goto L_0x02b8;
                case 743: goto L_0x008b;
                case 744: goto L_0x0580;
                case 745: goto L_0x008b;
                case 746: goto L_0x0680;
                case 747: goto L_0x008b;
                case 748: goto L_0x0778;
                case 749: goto L_0x008b;
                case 750: goto L_0x0290;
                default: goto L_0x008b;
            }
        L_0x008b:
            r3 = r2
            goto L_0x0030
        L_0x008d:
            java.lang.String r1 = "{\u0016"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0094:
            java.lang.String r1 = "(\u0006"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x009b:
            java.lang.String r1 = "~\n"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x00a2:
            java.lang.String r1 = "(\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x00a9:
            java.lang.String r1 = "{\u0014"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x00b0:
            java.lang.String r1 = "(\u0005"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x00b8:
            java.lang.String r1 = "{\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x00c0:
            java.lang.String r1 = "(\r"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x00c8:
            java.lang.String r1 = "{\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x00d0:
            java.lang.String r1 = "(\u0007"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x00d8:
            java.lang.String r1 = "{\u0007"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x00e0:
            java.lang.String r1 = "(\u001f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x00e8:
            java.lang.String r1 = "{\u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x00f0:
            java.lang.String r1 = "(\u0010"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x00f8:
            java.lang.String r1 = "x\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0100:
            java.lang.String r1 = "+\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0108:
            java.lang.String r1 = "x\u0014"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0110:
            java.lang.String r1 = "+\b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0118:
            java.lang.String r1 = "x\t"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0120:
            java.lang.String r1 = "+\u000f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0128:
            java.lang.String r1 = "x\n"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0130:
            java.lang.String r1 = "+\u0000"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0138:
            java.lang.String r1 = "x\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0140:
            java.lang.String r1 = "+\u001e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0148:
            java.lang.String r1 = "x\u001f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0150:
            java.lang.String r1 = "+\u000b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0158:
            java.lang.String r1 = "x\u0007"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0160:
            java.lang.String r1 = "+\u0018"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0168:
            java.lang.String r1 = "l\u0017"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0170:
            java.lang.String r1 = "+\u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0178:
            java.lang.String r1 = "x\u0017"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0180:
            java.lang.String r1 = "+\f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0188:
            java.lang.String r1 = "x\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0190:
            java.lang.String r1 = "\"\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0198:
            java.lang.String r1 = "y\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x01a0:
            java.lang.String r1 = "*\u000b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x01a8:
            java.lang.String r1 = "y\u0006"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x01b0:
            java.lang.String r1 = "\"\u0013"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x01b8:
            java.lang.String r1 = "y\u0016"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x01c0:
            java.lang.String r1 = "=\u000e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x01c8:
            java.lang.String r1 = "y\u001c"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x01d0:
            java.lang.String r1 = "*\u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x01d8:
            java.lang.String r1 = "y\u001f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x01e0:
            java.lang.String r1 = "\"\u0007"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x01e8:
            java.lang.String r1 = "y\u0017"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x01f0:
            java.lang.String r1 = "*\u0001"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x01f8:
            java.lang.String r1 = "y\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0200:
            java.lang.String r1 = "*\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0208:
            java.lang.String r1 = "r\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0210:
            java.lang.String r1 = "*\u001f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0218:
            java.lang.String r1 = "y\t"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0220:
            java.lang.String r1 = "*\u0010"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0228:
            java.lang.String r1 = "y\u0014"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0230:
            java.lang.String r1 = "-\u0001"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0238:
            java.lang.String r1 = "~\u001a"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0240:
            java.lang.String r1 = "-\u0007"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0248:
            java.lang.String r1 = "~\u001f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0250:
            java.lang.String r1 = "=\u0006"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0258:
            java.lang.String r1 = "\u0013"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0260:
            java.lang.String r1 = ",\r"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0268:
            java.lang.String r1 = "i\u0006"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0270:
            java.lang.String r1 = ".\u001b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0278:
            java.lang.String r1 = "\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0280:
            java.lang.String r1 = ",\u000f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0288:
            java.lang.String r1 = "\u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0290:
            java.lang.String r1 = "/\u0001"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0298:
            java.lang.String r1 = "|\u001f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x02a0:
            java.lang.String r1 = "/\u0000"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x02a8:
            java.lang.String r1 = "|\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x02b0:
            java.lang.String r1 = "/\u0018"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x02b8:
            java.lang.String r1 = "}\u0016"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x02c0:
            java.lang.String r1 = "9\f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x02c8:
            java.lang.String r1 = "}\u0011"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x02d0:
            java.lang.String r1 = ".\u0007"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x02d8:
            java.lang.String r1 = "}\u0015"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x02e0:
            java.lang.String r1 = "-\u000f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x02e8:
            java.lang.String r1 = "}\u0018"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x02f0:
            java.lang.String r1 = ".\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x02f8:
            java.lang.String r1 = "}\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0300:
            java.lang.String r1 = ".\u0006"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0308:
            java.lang.String r1 = "}\u0014"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0310:
            java.lang.String r1 = ".\u001a"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0318:
            java.lang.String r1 = "}\u0005"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0320:
            java.lang.String r1 = ".\u001e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0328:
            java.lang.String r1 = "}\u001e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0330:
            java.lang.String r1 = ".\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0338:
            java.lang.String r1 = "}\t"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0340:
            java.lang.String r1 = "!\u001e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0348:
            java.lang.String r1 = "r\u001e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0350:
            java.lang.String r1 = "!\u0001"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0358:
            java.lang.String r1 = "r\u0005"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0360:
            java.lang.String r1 = " \u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0368:
            java.lang.String r1 = "s\u001e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0370:
            java.lang.String r1 = " \u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0378:
            java.lang.String r1 = "s\u0014"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0380:
            java.lang.String r1 = " \u0018"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0388:
            java.lang.String r1 = "s\u0001"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0390:
            java.lang.String r1 = " \u000f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0398:
            java.lang.String r1 = "s\u001c"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x03a0:
            java.lang.String r1 = " \u001e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x03a8:
            java.lang.String r1 = "p\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x03b0:
            java.lang.String r1 = "#\u001a"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x03b8:
            java.lang.String r1 = "p\u0000"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x03c0:
            java.lang.String r1 = "#\u0005"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x03c8:
            java.lang.String r1 = "q\n"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x03d0:
            java.lang.String r1 = "\"\u000f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x03d8:
            java.lang.String r1 = "q\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x03e0:
            java.lang.String r1 = "\"\u001a"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x03e8:
            java.lang.String r1 = "q\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x03f0:
            java.lang.String r1 = "\"\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x03f8:
            java.lang.String r1 = "q\u0017"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0400:
            java.lang.String r1 = "%\u000b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0408:
            java.lang.String r1 = "v\u0006"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0410:
            java.lang.String r1 = "%\b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0418:
            java.lang.String r1 = "v\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0420:
            java.lang.String r1 = "%\u0018"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0428:
            java.lang.String r1 = "v\t"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0430:
            java.lang.String r1 = "%\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0438:
            java.lang.String r1 = "v\u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0440:
            java.lang.String r1 = "%\u001f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0448:
            java.lang.String r1 = "w\u001f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0450:
            java.lang.String r1 = "$\u0001"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0458:
            java.lang.String r1 = "w\u0017"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0460:
            java.lang.String r1 = "$\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0468:
            java.lang.String r1 = "w\t"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0470:
            java.lang.String r1 = "$\u001c"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0478:
            java.lang.String r1 = "w\u001c"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0480:
            java.lang.String r1 = "$\u001e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0488:
            java.lang.String r1 = "w\u0018"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0490:
            java.lang.String r1 = "$\u0018"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0498:
            java.lang.String r1 = "w\u0005"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x04a0:
            java.lang.String r1 = "$\u0012"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x04a8:
            java.lang.String r1 = "|\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x04b0:
            java.lang.String r1 = "$\u000e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x04b8:
            java.lang.String r1 = "w\u0013"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x04c0:
            java.lang.String r1 = "$\u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x04c8:
            java.lang.String r1 = "w\u0015"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x04d0:
            java.lang.String r1 = "$\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x04d8:
            java.lang.String r1 = "w\u0011"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x04e0:
            java.lang.String r1 = "$\u0010"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x04e8:
            java.lang.String r1 = "w\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x04f0:
            java.lang.String r1 = "'\u000b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x04f8:
            java.lang.String r1 = "t\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0500:
            java.lang.String r1 = "'\u001a"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0508:
            java.lang.String r1 = "t\u001c"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0510:
            java.lang.String r1 = "(\u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0518:
            java.lang.String r1 = "t\u0013"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0520:
            java.lang.String r1 = "'\u0010"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0528:
            java.lang.String r1 = "t\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0530:
            java.lang.String r1 = "'\u000f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0538:
            java.lang.String r1 = "t\u0017"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0540:
            java.lang.String r1 = "$\u001a"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0548:
            java.lang.String r1 = "t\u001f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0550:
            java.lang.String r1 = "&\u0007"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0558:
            java.lang.String r1 = "j\u001b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0560:
            java.lang.String r1 = "9\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0568:
            java.lang.String r1 = "j\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0570:
            java.lang.String r1 = "9\u000b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0578:
            java.lang.String r1 = "j\u0017"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0580:
            java.lang.String r1 = "9\u0013"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0588:
            java.lang.String r1 = "j\u0015"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0590:
            java.lang.String r1 = "9\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0598:
            java.lang.String r1 = "j\u001c"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x05a0:
            java.lang.String r1 = "9\u001e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x05a8:
            java.lang.String r1 = "j\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x05b0:
            java.lang.String r1 = "8\u000b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x05b8:
            java.lang.String r1 = "h\u0015"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x05c0:
            java.lang.String r1 = ";\u0005"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x05c8:
            java.lang.String r1 = "h\u0005"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x05d0:
            java.lang.String r1 = ";\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x05d8:
            java.lang.String r1 = "q\u001e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x05e0:
            java.lang.String r1 = "%\t"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x05e8:
            java.lang.String r1 = "j\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x05f0:
            java.lang.String r1 = "?\t"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x05f8:
            java.lang.String r1 = "m\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0600:
            java.lang.String r1 = ":\u0007"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0608:
            java.lang.String r1 = "i\u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0610:
            java.lang.String r1 = ":\u000b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0618:
            java.lang.String r1 = "i\u001e"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0620:
            java.lang.String r1 = ";\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0628:
            java.lang.String r1 = "i\u0013"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0630:
            java.lang.String r1 = ":\u0006"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0638:
            java.lang.String r1 = "i\u0017"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0640:
            java.lang.String r1 = ":\u0001"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0648:
            java.lang.String r1 = "i\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0650:
            java.lang.String r1 = ":\b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0658:
            java.lang.String r1 = "i\u001f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0660:
            java.lang.String r1 = "3\u000b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0668:
            java.lang.String r1 = "\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0670:
            java.lang.String r1 = "%\u0001"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0678:
            java.lang.String r1 = "i\u0014"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0680:
            java.lang.String r1 = ":\u0018"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0688:
            java.lang.String r1 = "i\n"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0690:
            java.lang.String r1 = ":\u000f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0698:
            java.lang.String r1 = "y\u0018"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x06a0:
            java.lang.String r1 = ":\u0013"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x06a8:
            java.lang.String r1 = "n\u0007"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x06b0:
            java.lang.String r1 = "=\u0000"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x06b8:
            java.lang.String r1 = "n\n"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x06c0:
            java.lang.String r1 = "=\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x06c8:
            java.lang.String r1 = "n\u0017"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x06d0:
            java.lang.String r1 = "=\u0005"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x06d8:
            java.lang.String r1 = "n\u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x06e0:
            java.lang.String r1 = "=\u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x06e8:
            java.lang.String r1 = "n\u0002"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x06f0:
            java.lang.String r1 = "=\u0007"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x06f8:
            java.lang.String r1 = "n\u0013"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0700:
            java.lang.String r1 = "<\r"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0708:
            java.lang.String r1 = "o\u0011"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0710:
            java.lang.String r1 = "(\u000f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0718:
            java.lang.String r1 = "{\u0015"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0720:
            java.lang.String r1 = "(\u000f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0728:
            java.lang.String r1 = "}\u0012"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0730:
            java.lang.String r1 = ".\b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0738:
            java.lang.String r1 = "o\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0740:
            java.lang.String r1 = "<\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0748:
            java.lang.String r1 = "o\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0750:
            java.lang.String r1 = "<\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0758:
            java.lang.String r1 = "o\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0760:
            java.lang.String r1 = "<\u0019"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0768:
            java.lang.String r1 = "o\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0770:
            java.lang.String r1 = "?\u0003"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0778:
            java.lang.String r1 = "o\t"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0780:
            java.lang.String r1 = "<\u0010"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0788:
            java.lang.String r1 = "l\u0005"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x0790:
            java.lang.String r1 = "?\u000b"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x0798:
            java.lang.String r1 = "l\u0015"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x07a0:
            java.lang.String r1 = "?\u0004"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x07a8:
            java.lang.String r1 = "m\u0016"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x07b0:
            java.lang.String r1 = "0\u000f"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x07b8:
            java.lang.String r1 = "`\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.b.a.j.I(r1)
            goto L_0x0030
        L_0x07c0:
            java.lang.String r1 = "3\u001d"
            java.lang.String r3 = com.agilebinary.a.a.a.c.a.k.I(r1)
            goto L_0x0030
        L_0x07c8:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r2
            r2 = r3
            goto L_0x005b
        L_0x07d0:
            r1 = r2
            goto L_0x005b
        L_0x07d3:
            r0 = r2
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.c.a.a(android.content.Context, java.lang.String):java.lang.CharSequence");
    }

    public static String a(Context context, String[] strArr, String[] strArr2) {
        if (strArr.length <= 0) {
            return context.getString(R.string.label_event_mms_address_not_provided);
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < strArr2.length; i++) {
            if (i > 0) {
                stringBuffer.append(j.I("\u0016p"));
            }
            stringBuffer.append(b(strArr[i], strArr2[i]));
        }
        return stringBuffer.toString();
    }

    public static String a(String str, String str2) {
        return (str2 == null || str2.trim().length() == 0) ? str : str2 + k.I("Iv") + str + j.I("n");
    }

    public static boolean a(i iVar) {
        return iVar.n() && iVar.q() && iVar.o() > 1.7d;
    }

    public static boolean a(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static String b(String str, String str2) {
        return a(str2) ? str : str2;
    }
}
