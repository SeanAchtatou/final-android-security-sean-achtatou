package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.os.Build;
import com.agilebinary.a.a.a.c.c.j;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;
import org.osmdroid.a;
import org.osmdroid.b;
import org.osmdroid.b.a.e;
import org.osmdroid.c;

public final class i extends Overlay {

    /* renamed from: a  reason: collision with root package name */
    private float f284a = 10.0f;
    private float b = 10.0f;
    private float c = 2.0f;
    private int d = 12;
    private int e = 0;
    private boolean f = false;
    private boolean g = false;
    private boolean h = true;
    private boolean i = false;
    private final Activity j;
    private Picture k = new Picture();
    private int l = -1;
    private float m = 0.0f;
    private float n;
    private float o;
    private int p;
    private int q;
    private Matrix r;
    private final Paint s;
    private final Paint t;
    private Projection u;
    private boolean v;
    private b w;

    public i(Activity activity) {
        String str;
        this.w = new c(activity);
        this.j = activity;
        this.s = new Paint();
        this.s.setColor(-16777216);
        this.s.setAntiAlias(true);
        this.s.setStyle(Paint.Style.FILL);
        this.s.setAlpha(255);
        this.t = new Paint();
        this.t.setColor(-16777216);
        this.t.setAntiAlias(true);
        this.t.setStyle(Paint.Style.FILL);
        this.t.setAlpha(255);
        this.t.setTextSize((float) this.d);
        this.n = this.j.getResources().getDisplayMetrics().xdpi;
        this.o = this.j.getResources().getDisplayMetrics().ydpi;
        this.p = this.j.getResources().getDisplayMetrics().widthPixels;
        this.q = this.j.getResources().getDisplayMetrics().heightPixels;
        try {
            str = (String) Build.class.getField(j.I("6:5.=:8/.)>)")).get(null);
        } catch (Exception e2) {
            str = null;
        }
        if (!e.I("\u0015{\f{\n{\u0014u").equals(str) || !j.I("?)42?#").equals(Build.MODEL)) {
            if (e.I("\u0015{\f{\n{\u0014u").equals(str) && j.I("?\t\u0014\u0012\u001f").equals(Build.MODEL)) {
                this.n = 264.0f;
                this.o = 264.0f;
            }
        } else if (activity.getWindowManager().getDefaultDisplay().getOrientation() > 0) {
            this.n = (float) (((double) this.p) / 3.75d);
            this.o = (float) (((double) this.q) / 2.1d);
        } else {
            this.n = (float) (((double) this.p) / 2.1d);
            this.o = (float) (((double) this.q) / 3.75d);
        }
    }

    private /* synthetic */ String a(int i2) {
        if (this.f) {
            if (((double) i2) >= 8046.72d) {
                return this.w.a(a.q, Integer.valueOf((int) (((double) i2) / 1609.344d)));
            } else if (((double) i2) >= 321.8688d) {
                return this.w.a(a.q, Double.valueOf(((double) ((int) (((double) i2) / 160.9344d))) / 10.0d));
            } else {
                return this.w.a(a.s, Integer.valueOf((int) (((double) i2) * 3.2808399d)));
            }
        } else if (this.g) {
            if (((double) i2) >= 9260.0d) {
                return this.w.a(a.r, Integer.valueOf((int) (((double) i2) / 1852.0d)));
            } else if (((double) i2) >= 370.4d) {
                return this.w.a(a.r, Double.valueOf(((double) ((int) (((double) i2) / 185.2d))) / 10.0d));
            } else {
                return this.w.a(a.s, Integer.valueOf((int) (((double) i2) * 3.2808399d)));
            }
        } else if (i2 >= 5000) {
            return this.w.a(a.p, Integer.valueOf(i2 / 1000));
        } else if (i2 >= 200) {
            return this.w.a(a.p, Double.valueOf(((double) ((int) (((double) i2) / 100.0d))) / 10.0d));
        } else {
            return this.w.a(a.o, Integer.valueOf(i2));
        }
    }

    public final void a(float f2) {
        this.f284a = f2;
        this.b = 10.0f;
    }

    public final void a(boolean z) {
        this.v = z;
    }

    public final boolean a() {
        return this.v;
    }

    public final void draw(Canvas canvas, MapView mapView, boolean z) {
        Projection projection;
        i.super.draw(canvas, mapView, z);
        int zoomLevel = mapView.getZoomLevel();
        if (this.v && zoomLevel >= this.e && (projection = mapView.getProjection()) != null) {
            GeoPoint fromPixels = projection.fromPixels(this.p / 2, this.q / 2);
            if (!(zoomLevel == this.l && ((int) (((double) fromPixels.getLatitudeE6()) / 1000000.0d)) == ((int) (((double) this.m) / 1000000.0d)))) {
                this.l = zoomLevel;
                this.m = (float) fromPixels.getLatitudeE6();
                this.u = mapView.getProjection();
                if (this.u != null) {
                    GeoPoint fromPixels2 = this.u.fromPixels((int) (((float) (this.p / 2)) - (this.n / 2.0f)), this.q / 2);
                    GeoPoint fromPixels3 = this.u.fromPixels((int) (((float) (this.p / 2)) + (this.n / 2.0f)), this.q / 2);
                    int a2 = new org.osmdroid.util.GeoPoint(fromPixels2.getLatitudeE6(), fromPixels2.getLongitudeE6()).a(new org.osmdroid.util.GeoPoint(fromPixels3.getLatitudeE6(), fromPixels3.getLongitudeE6()));
                    GeoPoint fromPixels4 = this.u.fromPixels(this.p / 2, (int) (((float) (this.q / 2)) - (this.o / 2.0f)));
                    GeoPoint fromPixels5 = this.u.fromPixels(this.p / 2, (int) (((float) (this.q / 2)) + (this.o / 2.0f)));
                    int a3 = new org.osmdroid.util.GeoPoint(fromPixels4.getLatitudeE6(), fromPixels4.getLongitudeE6()).a(new org.osmdroid.util.GeoPoint(fromPixels5.getLatitudeE6(), fromPixels5.getLongitudeE6()));
                    Canvas beginRecording = this.k.beginRecording((int) this.n, (int) this.o);
                    if (this.h) {
                        String a4 = a(a2);
                        Rect rect = new Rect();
                        this.t.getTextBounds(a4, 0, a4.length(), rect);
                        int height = (int) (((double) rect.height()) / 5.0d);
                        beginRecording.drawRect(0.0f, 0.0f, this.n, this.c, this.s);
                        beginRecording.drawRect(this.n, 0.0f, this.n + this.c, ((float) rect.height()) + this.c + ((float) height), this.s);
                        if (!this.i) {
                            beginRecording.drawRect(0.0f, 0.0f, this.c, ((float) rect.height()) + this.c + ((float) height), this.s);
                        }
                        beginRecording.drawText(a4, (this.n / 2.0f) - ((float) (rect.width() / 2)), ((float) rect.height()) + this.c + ((float) height), this.t);
                    }
                    if (this.i) {
                        String a5 = a(a3);
                        Rect rect2 = new Rect();
                        this.t.getTextBounds(a5, 0, a5.length(), rect2);
                        int height2 = (int) (((double) rect2.height()) / 5.0d);
                        beginRecording.drawRect(0.0f, 0.0f, this.c, this.o, this.s);
                        beginRecording.drawRect(0.0f, this.o, ((float) rect2.height()) + this.c + ((float) height2), this.o + this.c, this.s);
                        if (!this.h) {
                            beginRecording.drawRect(0.0f, 0.0f, ((float) rect2.height()) + this.c + ((float) height2), this.c, this.s);
                        }
                        float height3 = ((float) rect2.height()) + this.c + ((float) height2);
                        float width = (this.o / 2.0f) + ((float) (rect2.width() / 2));
                        beginRecording.rotate(-90.0f, height3, width);
                        beginRecording.drawText(a5, height3, width + ((float) height2), this.t);
                    }
                    this.k.endRecording();
                }
            }
            this.r = canvas.getMatrix();
            canvas.restore();
            canvas.save();
            canvas.translate(this.f284a, this.b);
            canvas.drawPicture(this.k);
            canvas.setMatrix(this.r);
        }
    }
}
