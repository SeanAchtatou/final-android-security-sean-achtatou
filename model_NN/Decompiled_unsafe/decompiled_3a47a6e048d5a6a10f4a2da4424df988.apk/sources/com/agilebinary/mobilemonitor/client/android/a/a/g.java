package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;

public final class g implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f149a;
    private String b;
    private String c;
    private boolean d;
    private boolean e;
    private long f;
    private String g;
    private String h;
    private String i;
    private long j;
    private long k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i2 = length - 1;
        int i3 = i2;
        while (i2 >= 0) {
            int i4 = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 31);
            if (i4 < 0) {
                break;
            }
            i2 = i4 - 1;
            cArr[i4] = (char) (str.charAt(i4) ^ '0');
            i3 = i2;
        }
        return new String(cArr);
    }

    public final String a() {
        return this.f149a;
    }

    public final String b() {
        return this.b;
    }

    public final long c() {
        return this.f;
    }

    public final String d() {
        return this.g;
    }

    public final String e() {
        return this.h;
    }

    public final String f() {
        return this.i;
    }

    public final String g() {
        return this.c;
    }

    public final boolean h() {
        return this.d;
    }

    public final long i() {
        return this.j;
    }

    public final boolean j() {
        return this.e;
    }

    public final long k() {
        return this.k;
    }

    public final String l() {
        return this.l;
    }

    public final String m() {
        return this.m;
    }

    public final String n() {
        return this.n;
    }

    public final String o() {
        return this.o;
    }

    public final String p() {
        return this.p;
    }
}
