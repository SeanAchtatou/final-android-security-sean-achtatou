package com.agilebinary.mobilemonitor.client.android.a.a;

import com.agilebinary.a.a.a.c.b.q;
import com.agilebinary.a.a.a.c.c.j;
import java.io.Serializable;

public final class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    double f144a;
    double b;
    double c;
    String d;
    String e;
    String f;
    String g;
    String h;

    public b() {
    }

    public b(double d2, double d3, double d4) {
        this.f144a = d2;
        this.b = d3;
        this.c = d4;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
    }

    public final double a() {
        return this.f144a;
    }

    public final void a(double d2) {
        this.f144a = d2;
    }

    public final double b() {
        return this.b;
    }

    public final void b(double d2) {
        this.b = d2;
    }

    public final double c() {
        return this.c;
    }

    public final void c(double d2) {
        this.c = d2;
    }

    public final String toString() {
        return j.I("7\u0014\u0018\u001a\u000f\u0012\u0014\u0015A[") + (q.I("Z\u001eM]UUUIEY\u0003\u0006\u0003") + this.f144a + j.I("YW[Y") + q.I("MSO[HHTXD") + j.I("YAY") + this.b + q.I("\u001e\r\u001c\u0003") + j.I("\u001a\u0018\u0018\u000e\t\u001a\u0018\u0002") + q.I("\u0003\u0006\u0003") + this.c + j.I("YW[Y") + q.I("BSTRUNX") + j.I("YAY") + this.d + q.I("\u001e\r\u001c\u0003") + j.I("\u0018\u0014\u000e\u0015\u000f\t\u0002$\u0018\u0014\u001f\u001e") + q.I("\u0003\u0006\u0003") + this.e + j.I("YW[Y") + q.I("ND[HSO") + j.I("YAY") + this.f + q.I("\u001e\r\u001c\u0003") + j.I("\u0018\u0012\u000f\u0002") + q.I("\u0003\u0006\u0003") + this.g + j.I("YW[Y") + q.I("OUNDYU") + j.I("YAY") + this.h + q.I("\u001e\\"));
    }
}
