package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.agilebinary.a.a.a.b;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.biige.client.android.R;

public class MapMarkerSelectDialogRowView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private TextView f277a = ((TextView) findViewById(R.id.location_select_dialog_row_line1));
    private TextView b = ((TextView) findViewById(R.id.location_select_dialog_row_line2));
    private TextView c = ((TextView) findViewById(R.id.location_select_dialog_row_accuracy));
    private TextView d = ((TextView) findViewById(R.id.location_select_dialog_row_time));

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.map.MapMarkerSelectDialogRowView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public MapMarkerSelectDialogRowView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ((LayoutInflater) context.getSystemService(b.I("$V1X=C\u0017^&Q$V<R:"))).inflate((int) R.layout.location_select_dialog_row, (ViewGroup) this, true);
    }

    public final void a(s sVar) {
        TextView textView = this.d;
        getContext();
        textView.setText(c.a().a(sVar.u()));
        this.f277a.setText(sVar.b(getContext()));
        this.b.setText(sVar.c(getContext()));
        Double f = sVar.f();
        if (f != null) {
            this.c.setText(getContext().getString(R.string.label_event_location_accuracy, f));
            return;
        }
        this.c.setText("");
    }
}
