package com.agilebinary.mobilemonitor.client.android.ui;

import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agilebinary.a.a.a.c.b.q;
import com.agilebinary.mobilemonitor.client.android.b.i;
import com.biige.client.android.R;

public final class v extends k {
    private TextView c;
    private TextView d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.v, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public v(EventListActivity_base eventListActivity_base) {
        super(eventListActivity_base);
        ((LayoutInflater) eventListActivity_base.getSystemService(q.I("M]XSTH~UOZM]UYS"))).inflate((int) R.layout.eventlist_rowview_app, (ViewGroup) this, true);
        a();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.c = (TextView) findViewById(R.id.eventlist_rowview_app_line1);
        this.d = (TextView) findViewById(R.id.eventlist_rowview_app_line2);
    }

    public final void a(Cursor cursor) {
        super.a(cursor);
        this.d.setText(cursor.getString(cursor.getColumnIndex(i.g)));
        this.c.setText(cursor.getString(cursor.getColumnIndex(i.f)));
    }
}
