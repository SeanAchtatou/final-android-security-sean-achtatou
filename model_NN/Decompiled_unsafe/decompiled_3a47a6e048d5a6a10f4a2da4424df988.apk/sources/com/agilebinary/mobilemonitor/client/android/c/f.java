package com.agilebinary.mobilemonitor.client.android.c;

import com.agilebinary.a.a.a.k.a;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static char[] f183a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static byte[] a(char[] cArr) {
        int i = 0;
        int length = cArr.length;
        if ((length & 1) != 0) {
            throw new Exception(a.I("|eW!]t^cVs\u0013nU!PiRsRbGdAr\u001d"));
        }
        byte[] bArr = new byte[(length >> 1)];
        int i2 = 0;
        while (i2 < length) {
            int i3 = i2 + 1;
            bArr[i] = (byte) (((Character.digit(cArr[i2], 16) << 4) | Character.digit(cArr[i3], 16)) & 255);
            i++;
            i2 = i3 + 1;
        }
        return bArr;
    }

    public static char[] a(byte[] bArr) {
        int i = 0;
        int length = bArr.length;
        char[] cArr = new char[(length << 1)];
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = i + 1;
            cArr[i] = f183a[(bArr[i2] & 240) >>> 4];
            i = i3 + 1;
            cArr[i3] = f183a[bArr[i2] & 15];
        }
        return cArr;
    }
}
