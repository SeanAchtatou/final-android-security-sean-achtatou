package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.c.b.q;
import com.agilebinary.a.a.a.i.f;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import com.agilebinary.a.a.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;

public final class s implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final String f163a = b.a();
    private j b;
    private c c;
    private int d;
    private boolean e;
    private InputStream f;
    private int g;

    public s(j jVar) {
        this.b = jVar;
        this.d = jVar.a().b();
        this.c = jVar.b();
        try {
            if (this.c.g()) {
                this.f = this.c.f();
            }
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        String a2 = a(f.I("|r\u0002h"));
        this.g = -2;
        if (a2 != null) {
            try {
                this.g = Integer.parseInt(a2);
            } catch (NumberFormatException e4) {
                q.I("Z@UMYE\u001cUS\u0001L@NRY\u0001TD]EYS\u0011W]MID\u001cNZ\u0001") + a2;
            }
        }
    }

    private /* synthetic */ String a(String str) {
        t[] b2 = this.b.b(str);
        if (b2.length == 0) {
            return null;
        }
        return b2[0].b();
    }

    public static boolean a(int i) {
        return i == 8 || i == 9 || i == 5 || i == 7 || i == 11;
    }

    public static boolean b(int i) {
        return i == -3;
    }

    public static boolean c(int i) {
        return i == 304;
    }

    public final boolean a() {
        if (this.e) {
            return false;
        }
        int i = this.g;
        if (this.d == 200) {
            return i == 0 || i == -2 || a(f.I("|r\u0002h\u0002o")) != null;
        }
        return false;
    }

    public final int b() {
        return this.g;
    }

    public final boolean c() {
        return a(this.g);
    }

    public final InputStream d() {
        return this.f;
    }

    public final void e() {
        if (this.f != null) {
            try {
                this.f.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public final boolean f() {
        return b(this.g);
    }

    /* JADX INFO: finally extract failed */
    public final String g() {
        o a2;
        String str = null;
        if (this.f == null) {
            return null;
        }
        if (this.c.c() > 2147483647L) {
            throw new IllegalArgumentException(q.I("tuhq\u001cDRUUUE\u0001HNS\u0001P@NFY\u0001HN\u001cCY\u0001^TZGYSYE\u001cHR\u0001QDQNNX"));
        }
        int c2 = (int) this.c.c();
        if (c2 < 0) {
            c2 = 4096;
        }
        c cVar = this.c;
        if (cVar == null) {
            throw new IllegalArgumentException(f.I("s{o\u001bJU[R[B\u000fVNB\u000fU@O\u000fYJ\u001bANCW"));
        }
        if (cVar.d() != null) {
            m[] c3 = cVar.d().c();
            if (c3.length > 0 && (a2 = c3[0].a(q.I("BT@NRYU"))) != null) {
                str = a2.b();
            }
        }
        if (str == null) {
            str = f.I("fh`\u0016\u0017\u0003\u001a\u0002\u0002\n");
        }
        InputStreamReader inputStreamReader = new InputStreamReader(this.f, str);
        com.agilebinary.a.a.a.i.c cVar2 = new com.agilebinary.a.a.a.i.c(c2);
        try {
            char[] cArr = new char[1024];
            while (true) {
                int read = inputStreamReader.read(cArr);
                if (read != -1) {
                    cVar2.a(cArr, 0, read);
                } else {
                    inputStreamReader.close();
                    return cVar2.toString();
                }
            }
        } catch (Throwable th) {
            inputStreamReader.close();
            throw th;
        }
    }
}
