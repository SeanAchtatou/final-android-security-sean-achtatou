package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.ui.BaseActivity;
import com.agilebinary.mobilemonitor.client.android.ui.MainActivity;

public final class g extends AsyncTask implements com.agilebinary.mobilemonitor.client.android.a.g {

    /* renamed from: a  reason: collision with root package name */
    public static g f214a;
    private static String b = b.a();
    private final BaseActivity c;
    private final MyApplication d;
    private com.agilebinary.a.a.a.d.c.b e;

    public g(BaseActivity baseActivity) {
        this.c = baseActivity;
        this.d = baseActivity.g;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0108 A[Catch:{ q -> 0x0121, all -> 0x0126 }, LOOP:0: B:4:0x000c->B:31:0x0108, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0096 A[EDGE_INSN: B:46:0x0096->B:16:0x0096 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ java.lang.Void b() {
        /*
            r14 = this;
            r12 = 0
            r7 = 0
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r14.d
            com.agilebinary.mobilemonitor.client.android.b.j r9 = r0.e()
            byte[] r10 = com.agilebinary.mobilemonitor.client.android.b.j.f174a     // Catch:{ all -> 0x0126 }
            int r11 = r10.length     // Catch:{ all -> 0x0126 }
            r8 = r7
        L_0x000c:
            if (r8 >= r11) goto L_0x0096
            byte r3 = r10[r8]
            boolean r0 = r14.isCancelled()     // Catch:{ all -> 0x0126 }
            if (r0 != 0) goto L_0x0096
            long r4 = r9.b(r3)     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0126 }
            r0.<init>()     // Catch:{ all -> 0x0126 }
            java.lang.String r1 = "\u0004S=_\u001f\\\u001c[#N#I5T\u0015L5T$\u0014~\u00146U\"\u001a5L5T$\u001a$C _p"
            java.lang.String r1 = com.agilebinary.a.a.a.c.b.a.j.I(r1)     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x0126 }
            java.lang.String r1 = "4E4"
            java.lang.String r1 = org.osmdroid.b.a.e.I(r1)     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0126 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x0126 }
            r0.toString()     // Catch:{ all -> 0x0126 }
            boolean r0 = r14.isCancelled()     // Catch:{ all -> 0x0126 }
            if (r0 != 0) goto L_0x0096
            r0 = 0
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x00a7
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ q -> 0x0121 }
            r0.<init>()     // Catch:{ q -> 0x0121 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ q -> 0x0121 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ q -> 0x0121 }
            r0.toString()     // Catch:{ q -> 0x0121 }
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r14.d     // Catch:{ q -> 0x0121 }
            com.agilebinary.mobilemonitor.client.android.a.m r0 = r0.b()     // Catch:{ q -> 0x0121 }
            com.agilebinary.mobilemonitor.client.android.a.a r0 = r0.f()     // Catch:{ q -> 0x0121 }
            com.agilebinary.mobilemonitor.client.android.MyApplication r1 = r14.d     // Catch:{ q -> 0x0121 }
            com.agilebinary.mobilemonitor.client.android.d r1 = r1.a()     // Catch:{ q -> 0x0121 }
            long r4 = r0.a(r1, r3, r14)     // Catch:{ q -> 0x0121 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ q -> 0x0121 }
            r0.<init>()     // Catch:{ q -> 0x0121 }
            java.lang.String r1 = "t?M|\u001a\u0004S=_\u001f\\\u001c[#N#I5T\u0015L5T$\u0014~\u00146U\"\u001a5L5T$\u001a$C _p"
            java.lang.String r1 = com.agilebinary.a.a.a.c.b.a.j.I(r1)     // Catch:{ q -> 0x0121 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ q -> 0x0121 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ q -> 0x0121 }
            java.lang.String r1 = "4E4Sf\u001dr"
            java.lang.String r1 = org.osmdroid.b.a.e.I(r1)     // Catch:{ q -> 0x0121 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ q -> 0x0121 }
            r0.toString()     // Catch:{ q -> 0x0121 }
            boolean r0 = r14.isCancelled()     // Catch:{ q -> 0x0121 }
            if (r0 == 0) goto L_0x009e
        L_0x0096:
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r14.d
            r0.f()
            com.agilebinary.mobilemonitor.client.android.ui.a.g.f214a = r12
            return r12
        L_0x009e:
            r9.b(r3, r4)     // Catch:{ q -> 0x0121 }
            boolean r0 = r14.isCancelled()     // Catch:{ q -> 0x0121 }
            if (r0 != 0) goto L_0x0096
        L_0x00a7:
            boolean r0 = r14.isCancelled()     // Catch:{ q -> 0x012f }
            if (r0 != 0) goto L_0x0096
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ q -> 0x012f }
            r0.<init>()     // Catch:{ q -> 0x012f }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ q -> 0x012f }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ q -> 0x012f }
            r0.toString()     // Catch:{ q -> 0x012f }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ q -> 0x012f }
            r0.<init>()     // Catch:{ q -> 0x012f }
            java.lang.String r1 = "pI9T3_p"
            java.lang.String r1 = com.agilebinary.a.a.a.c.b.a.j.I(r1)     // Catch:{ q -> 0x012f }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ q -> 0x012f }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ q -> 0x012f }
            r0.toString()     // Catch:{ q -> 0x012f }
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r14.d     // Catch:{ q -> 0x012f }
            com.agilebinary.mobilemonitor.client.android.a.m r0 = r0.b()     // Catch:{ q -> 0x012f }
            com.agilebinary.mobilemonitor.client.android.a.c r1 = r0.c()     // Catch:{ q -> 0x012f }
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r14.d     // Catch:{ q -> 0x012f }
            com.agilebinary.mobilemonitor.client.android.d r2 = r0.a()     // Catch:{ q -> 0x012f }
            r6 = r14
            int r1 = r1.a(r2, r3, r4, r6)     // Catch:{ q -> 0x012f }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ q -> 0x0137 }
            r0.<init>()     // Catch:{ q -> 0x0137 }
            java.lang.String r2 = ""
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ q -> 0x0137 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ q -> 0x0137 }
            r0.toString()     // Catch:{ q -> 0x0137 }
            boolean r0 = r14.isCancelled()     // Catch:{ q -> 0x0137 }
            if (r0 != 0) goto L_0x0096
        L_0x0102:
            boolean r0 = r14.isCancelled()     // Catch:{ all -> 0x0126 }
            if (r0 != 0) goto L_0x0096
            r0 = 2
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ all -> 0x0126 }
            r2 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0126 }
            r0[r2] = r3     // Catch:{ all -> 0x0126 }
            r2 = 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x0126 }
            r0[r2] = r1     // Catch:{ all -> 0x0126 }
            r14.publishProgress(r0)     // Catch:{ all -> 0x0126 }
            int r0 = r8 + 1
            r8 = r0
            goto L_0x000c
        L_0x0121:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0126 }
            goto L_0x00a7
        L_0x0126:
            r0 = move-exception
            com.agilebinary.mobilemonitor.client.android.MyApplication r1 = r14.d
            r1.f()
            com.agilebinary.mobilemonitor.client.android.ui.a.g.f214a = r12
            throw r0
        L_0x012f:
            r0 = move-exception
            r1 = r0
            r0 = r7
        L_0x0132:
            r1.printStackTrace()     // Catch:{ all -> 0x0126 }
            r1 = r0
            goto L_0x0102
        L_0x0137:
            r0 = move-exception
            r13 = r1
            r1 = r0
            r0 = r13
            goto L_0x0132
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.ui.a.g.b():java.lang.Void");
    }

    public final void a() {
        cancel(false);
        if (this.e != null) {
            try {
                this.e.d();
            } catch (Exception e2) {
            }
        }
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        this.e = bVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return b();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        Integer[] numArr = (Integer[]) objArr;
        MainActivity.a(this.c, (byte) numArr[0].intValue(), numArr[1].intValue());
    }
}
