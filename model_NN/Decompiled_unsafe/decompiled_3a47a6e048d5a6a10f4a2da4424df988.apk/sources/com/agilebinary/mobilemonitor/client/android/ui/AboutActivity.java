package com.agilebinary.mobilemonitor.client.android.ui;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.e;
import com.biige.client.android.R;
import org.osmdroid.b.a;

public class AboutActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private TextView f189a;
    private TextView b;
    private TextView c;

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.about;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = (TextView) findViewById(R.id.about_version);
        TextView textView = this.c;
        t.a();
        textView.setText(e.I("\u0007]\u0004A"));
        this.f189a = (TextView) findViewById(R.id.about_url);
        this.f189a.setMovementMethod(LinkMovementMethod.getInstance());
        t.a();
        this.f189a.setText(Html.fromHtml(getString(R.string.label_about_url, new Object[]{a.I("p?l6>;*ck68*<dcq;);p.7%9)p/1!yr);)b<%7+;b=#3pq-`")})));
        this.b = (TextView) findViewById(R.id.about_copyright);
        TextView textView2 = this.b;
        t.a();
        textView2.setText(getString(R.string.label_about_copyright, new Object[]{e.I("\u0004C\u0007B\u0016\u0011_\u001aQ\u0016\u0018\u0010Y\u001e")}));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
