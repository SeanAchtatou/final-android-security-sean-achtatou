package com.agilebinary.mobilemonitor.client.android.c;

import com.agilebinary.a.a.a.c.b.e;
import com.agilebinary.a.a.a.h.f;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f182a = System.getProperty(e.I("\u0015\b\u0017\u0004W\u0012\u001c\u0011\u0018\u0013\u0018\u0015\u0016\u0013"));
    private static char[] b = new char[64];
    private static byte[] c = new byte[128];

    static {
        char c2 = 'A';
        int i = 0;
        while (c2 <= 'Z') {
            b[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = 'a';
        while (c3 <= 'z') {
            b[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        char c4 = '0';
        while (c4 <= '9') {
            b[i] = c4;
            c4 = (char) (c4 + 1);
            i++;
        }
        b[i] = '+';
        b[i + 1] = '/';
        for (int i2 = 0; i2 < c.length; i2++) {
            c[i2] = -1;
        }
        for (int i3 = 0; i3 < 64; i3++) {
            c[b[i3]] = (byte) i3;
        }
    }

    private /* synthetic */ d() {
    }

    public static byte[] a(char[] cArr, int i) {
        char c2;
        int i2;
        char c3;
        int i3;
        int i4 = 0;
        if (i % 4 != 0) {
            throw new IllegalArgumentException(f.I("5P\u0017R\r]YZ\u001f\u0015;T\nPO\u0001YP\u0017V\u0016Q\u001cQY\\\u0017E\fAYF\rG\u0010[\u001e\u0015\u0010FY[\u0016AYTYX\fY\r\\\tY\u001c\u0015\u0016SY\u0001W"));
        }
        while (i > 0 && cArr[(i + 0) - 1] == '=') {
            i--;
        }
        int i5 = (i * 3) / 4;
        byte[] bArr = new byte[i5];
        int i6 = i + 0;
        int i7 = 0;
        while (i4 < i6) {
            int i8 = i4 + 1;
            char c4 = cArr[i4];
            int i9 = i8 + 1;
            char c5 = cArr[i8];
            if (i9 < i6) {
                c2 = cArr[i9];
                i9++;
            } else {
                c2 = 'A';
            }
            if (i9 < i6) {
                int i10 = i9 + 1;
                c3 = cArr[i9];
                i2 = i10;
            } else {
                i2 = i9;
                c3 = 'A';
            }
            if (c4 > 127 || c5 > 127 || c2 > 127 || c3 > 127) {
                throw new IllegalArgumentException(e.I("(\u0015\r\u001c\u0006\u0018\rY\u0002\u0011\u0000\u000b\u0000\u001a\u0015\u001c\u0013Y\b\u0017A;\u0000\n\u0004OUY\u0004\u0017\u0002\u0016\u0005\u001c\u0005Y\u0005\u0018\u0015\u0018O"));
            }
            byte b2 = c[c4];
            byte b3 = c[c5];
            byte b4 = c[c2];
            byte b5 = c[c3];
            if (b2 < 0 || b3 < 0 || b4 < 0 || b5 < 0) {
                throw new IllegalArgumentException(f.I("0Y\u0015P\u001eT\u0015\u0015\u001a]\u0018G\u0018V\rP\u000b\u0015\u0010[Yw\u0018F\u001c\u0003M\u0015\u001c[\u001aZ\u001dP\u001d\u0015\u001dT\rTW"));
            }
            int i11 = i7 + 1;
            bArr[i7] = (byte) ((b2 << 2) | (b3 >>> 4));
            if (i11 < i5) {
                i3 = i11 + 1;
                bArr[i11] = (byte) (((b3 & 15) << 4) | (b4 >>> 2));
            } else {
                i3 = i11;
            }
            if (i3 < i5) {
                bArr[i3] = (byte) (((b4 & 3) << 6) | b5);
                i3++;
            }
            i7 = i3;
            i4 = i2;
        }
        return bArr;
    }
}
