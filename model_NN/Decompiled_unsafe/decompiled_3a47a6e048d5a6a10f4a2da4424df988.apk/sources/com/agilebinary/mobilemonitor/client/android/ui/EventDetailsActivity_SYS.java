package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agilebinary.a.a.a.i.d;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.biige.client.android.R;

public class EventDetailsActivity_SYS extends EventDetailsActivity_base {

    /* renamed from: a  reason: collision with root package name */
    private TextView f198a;
    private TextView b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService(d.I("0~%p)k\u0003v2y0~(z."))).inflate((int) R.layout.eventdetails_sys, viewGroup, true);
        this.b = (TextView) findViewById(R.id.eventdetails_sys_time);
        this.f198a = (TextView) findViewById(R.id.eventdetails_sys_kind);
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        super.a(oVar);
        com.agilebinary.mobilemonitor.client.a.b.d dVar = (com.agilebinary.mobilemonitor.client.a.b.d) oVar;
        this.f198a.setText(dVar.a(this));
        this.b.setText(c.a().c(dVar.a()));
    }
}
