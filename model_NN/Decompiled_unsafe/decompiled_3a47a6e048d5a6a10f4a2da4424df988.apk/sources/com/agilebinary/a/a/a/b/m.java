package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.h;
import com.agilebinary.mobilemonitor.client.android.c.e;
import java.io.Serializable;

public final class m implements h, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final a f16a;
    private final int b;
    private final String c;

    public m(a aVar, int i, String str) {
        if (aVar == null) {
            throw new IllegalArgumentException(e.I("#D\u001cB\u001cU\u001cZS@\u0016D\u0000_\u001cXS[\u0012OSX\u001cBST\u0016\u0016\u001dC\u001fZ]"));
        } else if (i < 0) {
            throw new IllegalArgumentException(com.a.a.a.a.I("m6_6K1\u001e!Q&[bS#GbP-Jb\\'\u001e,[%_6W4[l"));
        } else {
            this.f16a = aVar;
            this.b = i;
            this.c = str;
        }
    }

    public final a a() {
        return this.f16a;
    }

    public final int b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final Object clone() {
        return super.clone();
    }

    public final String toString() {
        return r.a(this).toString();
    }
}
