package com.agilebinary.a.a.a.g;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.i.d;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class e extends b {
    private final byte[] b;

    public e(c cVar) {
        super(cVar);
        if (!cVar.a() || cVar.c() < 0) {
            this.b = b.b(cVar);
        } else {
            this.b = null;
        }
    }

    public final void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException(d.I("\u0013j(o)k|l(m9~1?1~%?2p(?>z|q)s0"));
        } else if (this.b != null) {
            outputStream.write(this.b);
        } else {
            this.f94a.a(outputStream);
        }
    }

    public final boolean a() {
        return true;
    }

    public final long c() {
        return this.b != null ? (long) this.b.length : this.f94a.c();
    }

    public final InputStream f() {
        return this.b != null ? new ByteArrayInputStream(this.b) : this.f94a.f();
    }

    public final boolean g() {
        return this.b == null && this.f94a.g();
    }

    public final boolean k_() {
        return this.b == null && this.f94a.k_();
    }
}
