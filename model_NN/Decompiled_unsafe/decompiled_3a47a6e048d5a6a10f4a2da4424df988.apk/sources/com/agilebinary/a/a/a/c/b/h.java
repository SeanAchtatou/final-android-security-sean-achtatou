package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.c.f;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.j.d;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;

public final class h extends f implements k, g {

    /* renamed from: a  reason: collision with root package name */
    private final Log f47a = a.a(getClass());
    private final Log b = a.a(org.a.a.h.I("C\u0005KYM\u0007M\u0014D\u0012\u0002\u001fX\u0003\\YD\u0012M\u0013I\u0005_"));
    private final Log c = a.a(com.agilebinary.a.a.a.k.f.I("$D,\u0018*F*U#Se^?B;\u0018<_9S"));
    private volatile Socket d;
    private b e;
    private boolean f;
    private volatile boolean g;
    private final Map h = new HashMap();

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.j.a a(Socket socket, int i, e eVar) {
        if (i == -1) {
            i = 8192;
        }
        com.agilebinary.a.a.a.j.a a2 = super.a(socket, i, eVar);
        return this.c.isDebugEnabled() ? new k(a2, new q(this.c), com.agilebinary.a.a.a.e.b.a(eVar)) : a2;
    }

    /* access modifiers changed from: protected */
    public final d a(com.agilebinary.a.a.a.j.a aVar, i iVar, e eVar) {
        return new p(aVar, iVar, eVar);
    }

    public final Object a(String str) {
        return this.h.get(str);
    }

    public final void a(com.agilebinary.a.a.a.f fVar) {
        if (this.f47a.isDebugEnabled()) {
            this.f47a.debug(org.a.a.h.I("\u0012B\u0013E\u0019KW^\u0012]\u0002I\u0004XM\f") + fVar.a());
        }
        super.a(fVar);
        if (this.b.isDebugEnabled()) {
            this.b.debug(com.agilebinary.a.a.a.k.f.I("\bu\u0016") + fVar.a().toString());
            t[] e2 = fVar.e();
            int length = e2.length;
            for (int i = 0; i < length; i++) {
                this.b.debug(org.a.a.h.I("\u0012I\f") + e2[i].toString());
            }
        }
    }

    public final void a(String str, Object obj) {
        this.h.put(str, obj);
    }

    public final void a(Socket socket, b bVar) {
        f();
        this.d = socket;
        this.e = bVar;
        if (this.g) {
            socket.close();
            throw new IOException(com.agilebinary.a.a.a.k.f.I("u$X%S(B\"Y%\u0016*Z9S*R2\u00168^>B/Y<X"));
        }
    }

    public final void a(Socket socket, b bVar, boolean z, e eVar) {
        a();
        if (bVar == null) {
            throw new IllegalArgumentException(org.a.a.h.I("x\u0016^\u0010I\u0003\f\u001fC\u0004XWA\u0002_\u0003\f\u0019C\u0003\f\u0015IWB\u0002@\u001b\u0002"));
        } else if (eVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.k.f.I("\u001bW9W&S?S9Ek[>E?\u0016%Y?\u0016)SkX>Z'\u0018"));
        } else {
            if (socket != null) {
                this.d = socket;
                a(socket, eVar);
            }
            this.e = bVar;
            this.f = z;
        }
    }

    public final void a(boolean z, e eVar) {
        f();
        if (eVar == null) {
            throw new IllegalArgumentException(org.a.a.h.I("'M\u0005M\u001aI\u0003I\u0005_WA\u0002_\u0003\f\u0019C\u0003\f\u0015IWB\u0002@\u001b\u0002"));
        }
        this.f = z;
        a(this.d, eVar);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.j.e b(Socket socket, int i, e eVar) {
        if (i == -1) {
            i = 8192;
        }
        com.agilebinary.a.a.a.j.e b2 = super.b(socket, i, eVar);
        return this.c.isDebugEnabled() ? new j(b2, new q(this.c), com.agilebinary.a.a.a.e.b.a(eVar)) : b2;
    }

    public final j d() {
        j d2 = super.d();
        if (this.f47a.isDebugEnabled()) {
            this.f47a.debug(com.agilebinary.a.a.a.k.f.I("\u0019S(S\"@\"X,\u00169S8F$X8Sq\u0016") + d2.a());
        }
        if (this.b.isDebugEnabled()) {
            this.b.debug(org.a.a.h.I("\u0010K\f") + d2.a().toString());
            t[] e2 = d2.e();
            int length = e2.length;
            for (int i = 0; i < length; i++) {
                this.b.debug(com.agilebinary.a.a.a.k.f.I("\nw\u0016") + e2[i].toString());
            }
        }
        return d2;
    }

    public final boolean h_() {
        return this.f;
    }

    public final Socket i_() {
        return this.d;
    }

    public final void k() {
        try {
            super.k();
            this.f47a.debug(org.a.a.h.I("o\u0018B\u0019I\u0014X\u001eC\u0019\f\u0014@\u0018_\u0012H"));
        } catch (IOException e2) {
            this.f47a.debug(com.agilebinary.a.a.a.k.f.I("\u0002\u0019\u0004\u0016.D9Y9\u0016(Z$E\"X,\u0016(Y%X.U?_$X"), e2);
        }
    }

    public final void m() {
        this.g = true;
        try {
            super.m();
            this.f47a.debug(org.a.a.h.I("4C\u0019B\u0012O\u0003E\u0018BW_\u001fY\u0003\f\u0013C\u0000B"));
            Socket socket = this.d;
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e2) {
            this.f47a.debug(com.agilebinary.a.a.a.k.f.I("\u0002\u0019\u0004\u0016.D9Y9\u00168^>B?_%QkR$A%\u0016(Y%X.U?_$X"), e2);
        }
    }
}
