package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.h;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.a.k.m;
import com.agilebinary.a.a.b.a.a;
import java.util.StringTokenizer;
import org.osmdroid.util.c;

public final class g implements k {
    private static /* synthetic */ boolean a(int i, int[] iArr) {
        for (int i2 : iArr) {
            if (i == i2) {
                return true;
            }
        }
        return false;
    }

    private static /* synthetic */ int[] a(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, a.I("\u0004"));
        int[] iArr = new int[stringTokenizer.countTokens()];
        int i = 0;
        while (stringTokenizer.hasMoreTokens()) {
            try {
                iArr[i] = Integer.parseInt(stringTokenizer.nextToken().trim());
                if (iArr[i] < 0) {
                    throw new e(c.I("n\bQ\u0007K\u000fCFw\tU\u0012\u0007\u0007S\u0012U\u000fE\u0013S\u0003\t"));
                }
                i++;
            } catch (NumberFormatException e) {
                throw new e(a.I("NFqIkAc\bWGu\\'Is\\uAe]sM=\b") + e.getMessage());
            }
        }
        return iArr;
    }

    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException(c.I("%H\tL\u000fBFJ\u0007^FI\tSFE\u0003\u0007\bR\nK"));
        } else if (bVar instanceof h) {
            h hVar = (h) bVar;
            if (str != null && str.trim().length() > 0) {
                hVar.a(a(str));
            }
        }
    }

    public final void a(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(a.I("DGhCnM'EfQ'Fh\\'Jb\bi]kD"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(c.I("d\tH\rN\u0003\u0007\tU\u000f@\u000fIFJ\u0007^FI\tSFE\u0003\u0007\bR\nK"));
        } else {
            int c = fVar.c();
            if ((cVar instanceof m) && ((m) cVar).e(a.I("wGu\\")) && !a(c, cVar.g())) {
                throw new i(c.I("w\tU\u0012\u0007\u0007S\u0012U\u000fE\u0013S\u0003\u0007\u0010N\tK\u0007S\u0003TFu dF\u0015_\u0011S\u001dFu\u0003V\u0013B\u0015SFW\tU\u0012\u0007\bH\u0012\u0007\u0000H\u0013I\u0002\u0007\u000fIFD\tH\rN\u0003\u0000\u0015\u0007\u0016H\u0014SFK\u000fT\u0012\t"));
            }
        }
    }

    public final boolean b(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(a.I("DGhCnM'EfQ'Fh\\'Jb\bi]kD"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(c.I("d\tH\rN\u0003\u0007\tU\u000f@\u000fIFJ\u0007^FI\tSFE\u0003\u0007\bR\nK"));
        } else {
            int c = fVar.c();
            if ((cVar instanceof m) && ((m) cVar).e(a.I("wGu\\"))) {
                if (cVar.g() == null) {
                    return false;
                }
                if (!a(c, cVar.g())) {
                    return false;
                }
            }
            return true;
        }
    }
}
