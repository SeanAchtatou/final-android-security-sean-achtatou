package com.agilebinary.a.a.a.h;

import com.agilebinary.a.a.a.c.b.a.j;
import com.agilebinary.a.a.a.h.b.g;
import java.io.IOException;
import java.io.InputStream;

public final class c extends InputStream implements d {

    /* renamed from: a  reason: collision with root package name */
    private InputStream f105a;
    private boolean b;
    private final e c;

    public c(InputStream inputStream, e eVar) {
        if (inputStream == null) {
            throw new IllegalArgumentException(g.I(":\u0007\f\u0005\u001d\u0010\tU\u001e\u0001\u001f\u0010\f\u0018M\u0018\f\fM\u001b\u0002\u0001M\u0017\bU\u0003\u0000\u0001\u0019C"));
        }
        this.f105a = inputStream;
        this.b = false;
        this.c = eVar;
    }

    private /* synthetic */ void a(int i) {
        if (this.f105a != null && i < 0) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.a(this.f105a);
                }
                if (z) {
                    this.f105a.close();
                }
            } finally {
                this.f105a = null;
            }
        }
    }

    private /* synthetic */ boolean c() {
        if (!this.b) {
            return this.f105a != null;
        }
        throw new IOException(j.I("{$N5W N5^pH5[4\u001a?TpY<U#_4\u001a#N\"_1W~"));
    }

    private /* synthetic */ void d() {
        if (this.f105a != null) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.j_();
                }
                if (z) {
                    this.f105a.close();
                }
            } finally {
                this.f105a = null;
            }
        }
    }

    public final int available() {
        if (!c()) {
            return 0;
        }
        try {
            return this.f105a.available();
        } catch (IOException e) {
            d();
            throw e;
        }
    }

    public final void b() {
        this.b = true;
        d();
    }

    public final void close() {
        boolean z = true;
        this.b = true;
        if (this.f105a != null) {
            try {
                if (this.c != null) {
                    z = this.c.b(this.f105a);
                }
                if (z) {
                    this.f105a.close();
                }
            } finally {
                this.f105a = null;
            }
        }
    }

    public final void d_() {
        close();
    }

    public final int read() {
        if (!c()) {
            return -1;
        }
        try {
            int read = this.f105a.read();
            a(read);
            return read;
        } catch (IOException e) {
            d();
            throw e;
        }
    }

    public final int read(byte[] bArr) {
        if (!c()) {
            return -1;
        }
        try {
            int read = this.f105a.read(bArr);
            a(read);
            return read;
        } catch (IOException e) {
            d();
            throw e;
        }
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (!c()) {
            return -1;
        }
        try {
            int read = this.f105a.read(bArr, i, i2);
            a(read);
            return read;
        } catch (IOException e) {
            d();
            throw e;
        }
    }
}
