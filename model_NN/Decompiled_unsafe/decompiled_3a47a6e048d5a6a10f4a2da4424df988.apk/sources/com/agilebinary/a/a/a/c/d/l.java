package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.b.q;
import com.agilebinary.a.a.a.d;
import com.agilebinary.a.a.a.d.c.b;
import com.agilebinary.a.a.a.f;
import java.net.URI;
import java.net.URISyntaxException;
import org.osmdroid.b.a.e;

public class l extends com.agilebinary.a.a.a.b.l implements b {
    private final f c;
    private URI d;
    private String e;
    private a f;
    private int g;

    public l(f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException(e.I("0@,DXf\u001de\rq\u000b`Xy\u0019mXz\u0017`Xv\u001d4\u0016a\u0014x"));
        }
        this.c = fVar;
        a(fVar.g());
        a(fVar.e());
        if (fVar instanceof b) {
            this.d = ((b) fVar).e_();
            this.e = ((b) fVar).b();
            this.f = null;
        } else {
            d a2 = fVar.a();
            try {
                this.d = new URI(a2.c());
                this.e = a2.a();
                this.f = fVar.c();
            } catch (URISyntaxException e2) {
                throw new com.agilebinary.a.a.a.l(com.agilebinary.mobilemonitor.client.android.c.e.I(":X\u0005W\u001f_\u0017\u0016\u0001S\u0002C\u0016E\u0007\u0016&d:\fS") + a2.c(), e2);
            }
        }
        this.g = 0;
    }

    public final d a() {
        String str = this.e;
        a c2 = c();
        String str2 = null;
        if (this.d != null) {
            str2 = this.d.toASCIIString();
        }
        if (str2 == null || str2.length() == 0) {
            str2 = "/";
        }
        return new q(str, str2, c2);
    }

    public final void a(URI uri) {
        this.d = uri;
    }

    public final String b() {
        return this.e;
    }

    public final a c() {
        if (this.f == null) {
            this.f = com.agilebinary.a.a.a.e.b.b(g());
        }
        return this.f;
    }

    public final void d() {
        throw new UnsupportedOperationException();
    }

    public final URI e_() {
        return this.d;
    }

    public boolean i() {
        return true;
    }

    public final void j() {
        this.f15a.a();
        a(this.c.e());
    }

    public final f k() {
        return this.c;
    }

    public final int l() {
        return this.g;
    }

    public final void m() {
        this.g++;
    }
}
