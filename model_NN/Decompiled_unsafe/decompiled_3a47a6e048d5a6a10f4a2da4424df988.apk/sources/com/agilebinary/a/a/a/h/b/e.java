package com.agilebinary.a.a.a.h.b;

import com.agilebinary.a.a.a.e.c;
import com.agilebinary.a.a.a.h.h;
import com.jasonkostempski.android.calendar.a;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import org.osmdroid.a.d;

public final class e implements a, i {

    /* renamed from: a  reason: collision with root package name */
    private static final e f102a = new e();
    private final b b = null;

    public static e b() {
        return f102a;
    }

    public final Socket a() {
        return new Socket();
    }

    public final Socket a(Socket socket, String str, int i, InetAddress inetAddress, int i2, com.agilebinary.a.a.a.e.e eVar) {
        InetSocketAddress inetSocketAddress = null;
        if (inetAddress != null || i2 > 0) {
            if (i2 < 0) {
                i2 = 0;
            }
            inetSocketAddress = new InetSocketAddress(inetAddress, i2);
        }
        return a(socket, new InetSocketAddress(this.b != null ? this.b.a() : InetAddress.getByName(str), i), inetSocketAddress, eVar);
    }

    public final Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, com.agilebinary.a.a.a.e.e eVar) {
        if (inetSocketAddress == null) {
            throw new IllegalArgumentException(d.I("fBYH@B\u0014FPCFBGT\u0014JU^\u0014I[S\u0014EQ\u0007ZRXK"));
        } else if (eVar == null) {
            throw new IllegalArgumentException(a.I("ctp\u000bPJRJMNTNRX\u0000FAR\u0000EO_\u0000IE\u000bN^LG"));
        } else {
            if (socket == null) {
                socket = new Socket();
            }
            if (inetSocketAddress2 != null) {
                socket.setReuseAddress(c.b(eVar));
                socket.bind(inetSocketAddress2);
            }
            try {
                socket.connect(inetSocketAddress, c.c(eVar));
                return socket;
            } catch (SocketTimeoutException e) {
                throw new h(d.I("d[IZBWS\u0014S[\u0007") + inetSocketAddress.getHostName() + "/" + inetSocketAddress.getAddress() + a.I("\u0000_IFEO\u0000DU_"));
            }
        }
    }

    public final boolean a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException(d.I("t[D_B@\u0007YFM\u0007ZH@\u0007VB\u0014IAKX\t"));
        } else if (!socket.isClosed()) {
            return false;
        } else {
            throw new IllegalArgumentException(a.I("xOHKNT\u000bIX\u0000HLDSND\u0005"));
        }
    }

    public final Socket f_() {
        return new Socket();
    }
}
