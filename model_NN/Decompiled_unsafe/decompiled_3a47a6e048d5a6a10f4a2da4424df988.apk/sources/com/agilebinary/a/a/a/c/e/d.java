package com.agilebinary.a.a.a.c.e;

import com.agilebinary.a.a.a.c.a.k;
import com.agilebinary.a.a.a.g.a;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.x;
import org.osmdroid.c.c;

public final class d implements a {
    public final long a(x xVar) {
        long j;
        if (xVar == null) {
            throw new IllegalArgumentException(c.I("\b1\u00145`\b%\u00163\u0004'\u0000`\b!\u001c`\u000b/\u0011`\u0007%E.\u0010,\t"));
        }
        boolean c = xVar.g().c(k.I("\u0001>\u001d:G:\u001b%\u001d%\n%\u0005d\u001a>\u001b#\n>D>\u001b+\u00079\u000f/\u001bg\f$\n%\r#\u0007-"));
        t c2 = xVar.c(c.I("12\u0004.\u0016&\u00002H\u0005\u000b#\n$\f.\u0002"));
        t c3 = xVar.c(k.I("*%\u0007>\f$\u001dg%/\u0007-\u001d\""));
        if (c2 != null) {
            try {
                m[] c4 = c2.c();
                if (c) {
                    int i = 0;
                    while (i < c4.length) {
                        String a2 = c4[i].a();
                        if (a2 == null || a2.length() <= 0 || a2.equalsIgnoreCase(c.I("\u0006(\u0010.\u000e%\u0001")) || a2.equalsIgnoreCase(k.I("\u0000.\f$\u001d#\u001d3"))) {
                            i++;
                        } else {
                            throw new l(c.I("0.\u00165\u00150\n2\u0011%\u0001`\u00112\u0004.\u0016&\u00002E%\u000b#\n$\f.\u0002zE") + a2);
                        }
                    }
                }
                int length = c4.length;
                if (c.I(")\u0001%\u000b4\f4\u001c").equalsIgnoreCase(c2.b())) {
                    j = -1;
                } else if (length > 0 && k.I(")\u0001?\u0007!\f.").equalsIgnoreCase(c4[length - 1].a())) {
                    return -2;
                } else {
                    if (!c) {
                        return -1;
                    }
                    throw new l(c.I("&(\u0010.\u000em\u0000.\u0006/\u0001)\u000b'E-\u00103\u0011`\u0007%E4\r%E,\u00043\u0011`\n.\u0000`\u00040\u0015,\f%\u0001"));
                }
            } catch (u e) {
                throw new l(k.I(" $\u001f+\u0005#\rj=8\b$\u001a,\f8D\u000f\u0007)\u0006.\u0000$\u000ej\u0001/\b.\f8I<\b&\u001c/Sj") + c2, e);
            }
        } else if (c3 == null) {
            return -1;
        } else {
            t[] b = xVar.b(k.I("*%\u0007>\f$\u001dg%/\u0007-\u001d\""));
            if (!c || b.length <= 1) {
                int length2 = b.length - 1;
                while (true) {
                    if (length2 < 0) {
                        j = -1;
                        break;
                    }
                    t tVar = b[length2];
                    try {
                        j = Long.parseLong(tVar.b());
                        break;
                    } catch (NumberFormatException e2) {
                        if (c) {
                            throw new l(k.I(" $\u001f+\u0005#\rj\n%\u0007>\f$\u001dj\u0005/\u0007-\u001d\"Sj") + tVar.b());
                        }
                        length2--;
                    }
                }
                if (j < 0) {
                    return -1;
                }
            } else {
                throw new l(c.I("(5\t4\f0\t%E#\n.\u0011%\u000b4E,\u0000.\u00024\r`\r%\u0004$\u00002\u0016"));
            }
        }
        return j;
    }
}
