package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.e;
import com.agilebinary.a.a.a.a.g;
import com.agilebinary.a.a.a.a.h;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.h.a;
import java.security.Principal;
import javax.net.ssl.SSLSession;

public final class f implements com.agilebinary.a.a.a.d.f {
    private static /* synthetic */ Principal a(e eVar) {
        g d;
        h c = eVar.c();
        if (c == null || !c.e() || !c.d() || (d = eVar.d()) == null) {
            return null;
        }
        return d.a();
    }

    public final Object a(k kVar) {
        SSLSession f;
        Principal principal = null;
        e eVar = (e) kVar.a(com.agilebinary.a.a.a.k.f.I("#B?FeW>B#\u0018?W9Q.BfE(Y;S"));
        if (eVar != null && (principal = a(eVar)) == null) {
            principal = a((e) kVar.a(com.agilebinary.a.a.a.h.f.I("\u0011A\rEWT\fA\u0011\u001b\tG\u0016M\u0000\u0018\nV\u0016E\u001c")));
        }
        if (principal == null) {
            a aVar = (a) kVar.a(com.agilebinary.a.a.a.k.f.I("^?B;\u0018(Y%X.U?_$X"));
            if (aVar.l() && (f = aVar.f()) != null) {
                return f.getLocalPrincipal();
            }
        }
        return principal;
    }
}
