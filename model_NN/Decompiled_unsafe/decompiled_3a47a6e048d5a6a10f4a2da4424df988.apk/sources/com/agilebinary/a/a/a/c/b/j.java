package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.f.b;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.j.f;
import com.agilebinary.a.a.a.k.a;

public final class j implements e {

    /* renamed from: a  reason: collision with root package name */
    private final e f49a;
    private final q b;
    private final String c;

    public j(e eVar, q qVar, String str) {
        this.f49a = eVar;
        this.b = qVar;
        this.c = str == null ? b.I("\nY\bC\u0002") : str;
    }

    public final void a(int i) {
        this.f49a.a(i);
        if (this.b.a()) {
            this.b.a(new byte[]{(byte) i});
        }
    }

    public final void a(c cVar) {
        this.f49a.a(cVar);
        if (this.b.a()) {
            this.b.a((new String(cVar.b(), 0, cVar.c()) + a.I("\f9")).getBytes(this.c));
        }
    }

    public final void a(String str) {
        this.f49a.a(str);
        if (this.b.a()) {
            this.b.a((str + b.I("\u0007A")).getBytes(this.c));
        }
    }

    public final void a(byte[] bArr, int i, int i2) {
        this.f49a.a(bArr, i, i2);
        if (this.b.a()) {
            this.b.a(bArr, i, i2);
        }
    }

    public final void b() {
        this.f49a.b();
    }

    public final f c() {
        return this.f49a.c();
    }
}
