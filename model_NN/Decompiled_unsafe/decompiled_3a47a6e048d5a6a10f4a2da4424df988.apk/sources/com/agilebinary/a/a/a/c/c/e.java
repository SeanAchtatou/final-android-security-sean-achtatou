package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.b.s;
import com.agilebinary.a.a.a.c.a.k;
import com.agilebinary.a.a.a.g;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.y;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public final class e extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final a f60a;
    private final c b;
    private int c;
    private int d;
    private int e;
    private boolean f = false;
    private boolean g = false;
    private t[] h = new t[0];

    public e(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException(k.I(":/\u001a9\u0000%\u0007j\u0000$\u0019?\u001dj\u000b?\u000f,\f8I'\b3I$\u0006>I(\fj\u0007?\u0005&"));
        }
        this.f60a = aVar;
        this.e = 0;
        this.b = new c(16);
        this.c = 1;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'h');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '!');
            i2 = i;
        }
        return new String(cArr);
    }

    private /* synthetic */ void a() {
        this.d = b();
        if (this.d < 0) {
            throw new g(u.I("\u001a@3D L\"@tF<P:NtV=_1"));
        }
        this.c = 2;
        this.e = 0;
        if (this.d == 0) {
            this.f = true;
            try {
                this.h = f.a(this.f60a, -1, -1, s.f21a, new ArrayList());
            } catch (com.agilebinary.a.a.a.k e2) {
                g gVar = new g(k.I(" $\u001f+\u0005#\rj\u000f%\u0006>\f8Sj") + e2.getMessage());
                com.agilebinary.a.a.a.i.a.a(gVar, e2);
                throw gVar;
            }
        }
    }

    private /* synthetic */ int b() {
        switch (this.c) {
            case 1:
                break;
            case 2:
            default:
                throw new IllegalStateException(u.I("l:F;K'L'Q1K \u00057J0@7\u0005'Q5Q1"));
            case 3:
                this.b.a();
                if (this.f60a.a(this.b) != -1) {
                    if (this.b.d()) {
                        this.c = 1;
                        break;
                    } else {
                        throw new g(k.I("<$\f2\u0019/\n>\f.I)\u0006$\u001d/\u0007>I+\u001dj\u001d\"\fj\f$\rj\u0006,I)\u0001?\u0007!"));
                    }
                } else {
                    return 0;
                }
        }
        this.b.a();
        if (this.f60a.a(this.b) == -1) {
            return 0;
        }
        int c2 = this.b.c(59);
        if (c2 < 0) {
            c2 = this.b.c();
        }
        try {
            return Integer.parseInt(this.b.b(0, c2), 16);
        } catch (NumberFormatException e2) {
            throw new g(u.I("g5AtF<P:NtM1D0@&"));
        }
    }

    public final int available() {
        if (this.f60a instanceof com.agilebinary.a.a.a.j.g) {
            return Math.min(((com.agilebinary.a.a.a.j.g) this.f60a).a(), this.d - this.e);
        }
        return 0;
    }

    public final void close() {
        if (!this.g) {
            try {
                if (!this.f) {
                    do {
                    } while (read(new byte[2048]) >= 0);
                }
            } finally {
                this.f = true;
                this.g = true;
            }
        }
    }

    public final int read() {
        if (this.g) {
            throw new IOException(k.I("(>\u001d/\u0004:\u001d/\rj\u001b/\b.I,\u001b%\u0004j\n&\u00069\f.I9\u001d8\f+\u0004d"));
        } else if (this.f) {
            return -1;
        } else {
            if (this.c != 2) {
                a();
                if (this.f) {
                    return -1;
                }
            }
            int d2 = this.f60a.d();
            if (d2 != -1) {
                this.e++;
                if (this.e >= this.d) {
                    this.c = 3;
                    return d2;
                }
            }
            return d2;
        }
    }

    public final int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (this.g) {
            throw new IOException(u.I("d Q1H$Q1AtW1D0\u00052W;HtF8J'@0\u0005'Q&@5Hz"));
        } else if (this.f) {
            return -1;
        } else {
            if (this.c != 2) {
                a();
                if (this.f) {
                    return -1;
                }
            }
            int a2 = this.f60a.a(bArr, i, Math.min(i2, this.d - this.e));
            if (a2 != -1) {
                this.e += a2;
                if (this.e < this.d) {
                    return a2;
                }
                this.c = 3;
                return a2;
            }
            this.f = true;
            throw new y(k.I("\u001e\u001b?\u0007)\b>\f.I)\u0001?\u0007!IbI/\u0011:\f)\u001d/\rj\u001a#\u0013/Sj") + this.d + u.I("o\u00055F P5ItV=_1\u001ft") + this.e + k.I("c"));
        }
    }
}
