package com.agilebinary.a.a.a;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.i.f;
import com.agilebinary.a.a.b.a.a;
import java.io.Serializable;
import java.util.Locale;
import org.osmdroid.a.d;

public final class b implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private String f6a;
    private String b;
    private int c;
    private String d;

    public b(String str, int i) {
        this(str, i, null);
    }

    public b(String str, int i, String str2) {
        if (str == null) {
            throw new IllegalArgumentException(a.I("`h[s\biIjM'EfQ'Fh\\'Jb\bi]kD"));
        }
        this.f6a = str;
        this.b = str.toLowerCase(Locale.ENGLISH);
        if (str2 != null) {
            this.d = str2.toLowerCase(Locale.ENGLISH);
        } else {
            this.d = "http";
        }
        this.c = i;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'H');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '7');
            i2 = i;
        }
        return new String(cArr);
    }

    public final String a() {
        return this.f6a;
    }

    public final int b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    public final Object clone() {
        return super.clone();
    }

    public final String d() {
        if (this.c == -1) {
            return this.f6a;
        }
        c cVar = new c(this.f6a.length() + 6);
        cVar.a(this.f6a);
        cVar.a(d.I("\u001d"));
        cVar.a(Integer.toString(this.c));
        return cVar.toString();
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return this.b.equals(bVar.b) && this.c == bVar.c && this.d.equals(bVar.d);
    }

    public final int hashCode() {
        return f.a((f.a(17, this.b) * 37) + this.c, this.d);
    }

    public final String toString() {
        c cVar = new c(32);
        cVar.a(this.d);
        cVar.a(a.I("\u0012(\u0007"));
        cVar.a(this.f6a);
        if (this.c != -1) {
            cVar.a(':');
            cVar.a(Integer.toString(this.c));
        }
        return cVar.toString();
    }
}
