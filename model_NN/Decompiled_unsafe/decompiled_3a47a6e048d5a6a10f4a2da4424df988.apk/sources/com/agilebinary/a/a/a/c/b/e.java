package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.e.c;
import com.agilebinary.a.a.a.q;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final Log f45a = a.a(getClass());
    private final Map b = new HashMap();

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'a');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'y');
            i2 = i;
        }
        return new String(cArr);
    }

    public final void a() {
        this.b.clear();
    }

    public final void a(long j) {
        long currentTimeMillis = System.currentTimeMillis() - j;
        if (this.f45a.isDebugEnabled()) {
            this.f45a.debug(c.I("KImBcHfF(GgS(BgOfDkUaNfR$\u0001aEdD(UaLmN}U2\u0001") + currentTimeMillis);
        }
        for (Map.Entry entry : this.b.entrySet()) {
            q qVar = (q) entry.getKey();
            long a2 = ((o) entry.getValue()).f53a;
            if (a2 <= currentTimeMillis) {
                if (this.f45a.isDebugEnabled()) {
                    this.f45a.debug(org.osmdroid.util.c.I("%K\tT\u000fI\u0001\u0007\u000fC\nBFD\tI\bB\u0005S\u000fH\b\u000bFD\tI\bB\u0005S\u000fH\b\u0007\u0012N\u000bB\\\u0007") + a2);
                }
                try {
                    qVar.k();
                } catch (IOException e) {
                    this.f45a.debug(c.I("A\u000eG\u0001mSzNz\u0001kMgRaOo\u0001kNfOmB|HgO"), e);
                }
            }
        }
    }
}
