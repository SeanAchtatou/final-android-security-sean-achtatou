package com.agilebinary.a.a.a.b;

import com.a.a.a.a;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import com.agilebinary.mobilemonitor.client.android.a.q;
import java.util.List;
import java.util.NoSuchElementException;

public final class g implements w {

    /* renamed from: a  reason: collision with root package name */
    private List f12a;
    private int b;
    private int c;
    private String d;

    public g(List list, String str) {
        if (list == null) {
            throw new IllegalArgumentException(a.I("\n[#Z'LbR+M6\u001e/K1JbP-Jb\\'\u001e,K.Rl"));
        }
        this.f12a = list;
        this.d = str;
        this.b = a(-1);
        this.c = -1;
    }

    private /* synthetic */ int a(int i) {
        if (i < -1) {
            return -1;
        }
        int size = this.f12a.size();
        boolean z = false;
        int i2 = i;
        while (!z && i2 < size - 1) {
            int i3 = i2 + 1;
            if (this.d == null) {
                z = true;
                i2 = i3;
            } else {
                z = this.d.equalsIgnoreCase(((t) this.f12a.get(i3)).a());
                i2 = i3;
            }
        }
        if (!z) {
            return -1;
        }
        return i2;
    }

    public final t a() {
        int i = this.b;
        if (i < 0) {
            throw new NoSuchElementException(q.I("5X\u0019^\u001dX\u0015C\u0012\f\u001d@\u000eI\u001dH\u0005\f\u001aE\u0012E\u000fD\u0019HR"));
        }
        this.c = i;
        this.b = a(i);
        return (t) this.f12a.get(i);
    }

    public final boolean hasNext() {
        return this.b >= 0;
    }

    public final Object next() {
        return a();
    }

    public final void remove() {
        if (this.c < 0) {
            throw new IllegalStateException(a.I("p-\u001e*[#Z'LbJ-\u001e0[/Q4[l"));
        }
        this.f12a.remove(this.c);
        this.c = -1;
        this.b--;
    }
}
