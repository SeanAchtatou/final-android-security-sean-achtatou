package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.b.b;
import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.p;
import com.agilebinary.a.a.a.t;
import org.c.a.a.a.a;

public final class k extends l implements p {
    private c c;
    /* access modifiers changed from: private */
    public boolean d;

    public k(p pVar) {
        super(pVar);
        c h = pVar.h();
        this.c = h != null ? new h(this, h) : null;
        this.d = false;
    }

    public final boolean g_() {
        t c2 = c(b.I("*\u000e\u001f\u0013\f\u0002"));
        return c2 != null && a.I(">\u001d?\u0000lBaYfCzH").equalsIgnoreCase(c2.b());
    }

    public final c h() {
        return this.c;
    }

    public final boolean i() {
        return this.c == null || this.c.a() || !this.d;
    }
}
