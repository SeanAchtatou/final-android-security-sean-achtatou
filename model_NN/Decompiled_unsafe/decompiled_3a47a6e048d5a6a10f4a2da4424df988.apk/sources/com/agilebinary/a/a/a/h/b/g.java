package com.agilebinary.a.a.a.h.b;

import com.agilebinary.a.a.a.i.f;
import com.agilebinary.a.a.a.k;
import com.agilebinary.a.a.b.a.a;
import java.util.Locale;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private final String f103a;
    private final i b;
    private final int c;
    private final boolean d;
    private String e;

    public g(String str, int i, i iVar) {
        if (str == null) {
            throw new IllegalArgumentException(k.I("02\u000b4\u000e4C?\u0002<\u0006q\u000e0\u001aq\r>\u0017q\u00014C?\u0016=\u000f"));
        } else if (i <= 0 || i > 65535) {
            throw new IllegalArgumentException(a.I("xhZs\bn['Ai^fDnL=\b") + i);
        } else if (iVar == null) {
            throw new IllegalArgumentException(k.I("\u0002\f2\b4\u0017q\u00050\u0000%\f#\u001aq\u000e0\u001aq\r>\u0017q\u00014C?\u0016=\u000f"));
        } else {
            this.f103a = str.toLowerCase(Locale.ENGLISH);
            this.b = iVar;
            this.c = i;
            this.d = iVar instanceof f;
        }
    }

    public g(String str, a aVar, int i) {
        if (str == null) {
            throw new IllegalArgumentException(a.I("{d@bEb\biIjM'EfQ'Fh\\'Jb\bi]kD"));
        } else if (aVar == null) {
            throw new IllegalArgumentException(k.I("\u0002\f2\b4\u0017q\u00050\u0000%\f#\u001aq\u000e0\u001aq\r>\u0017q\u00014C?\u0016=\u000f"));
        } else if (i <= 0 || i > 65535) {
            throw new IllegalArgumentException(a.I("xhZs\bn['Ai^fDnL=\b") + i);
        } else {
            this.f103a = str.toLowerCase(Locale.ENGLISH);
            if (aVar instanceof j) {
                this.b = new c((j) aVar);
                this.d = true;
            } else {
                this.b = new d(aVar);
                this.d = false;
            }
            this.c = i;
        }
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'm');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'u');
            i2 = i;
        }
        return new String(cArr);
    }

    public final int a() {
        return this.c;
    }

    public final int a(int i) {
        return i <= 0 ? this.c : i;
    }

    public final i b() {
        return this.b;
    }

    public final String c() {
        return this.f103a;
    }

    public final boolean d() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        return this.f103a.equals(gVar.f103a) && this.c == gVar.c && this.d == gVar.d && this.b.equals(gVar.b);
    }

    public final int hashCode() {
        return f.a((this.d ? 1 : 0) + (f.a(this.c + 629, this.f103a) * 37), this.b);
    }

    public final String toString() {
        if (this.e == null) {
            this.e = this.f103a + ':' + Integer.toString(this.c);
        }
        return this.e;
    }
}
