package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.b;
import com.agilebinary.a.a.a.j.f;

public final class k implements a, b {

    /* renamed from: a  reason: collision with root package name */
    private final a f50a;
    private final b b;
    private final q c;
    private final String d;

    public k(a aVar, q qVar, String str) {
        this.f50a = aVar;
        this.b = aVar instanceof b ? (b) aVar : null;
        this.c = qVar;
        this.d = str == null ? com.agilebinary.a.a.a.b.b.I("7<5&?") : str;
    }

    public final int a(c cVar) {
        int a2 = this.f50a.a(cVar);
        if (this.c.a() && a2 >= 0) {
            this.c.b((new String(cVar.b(), cVar.c() - a2, a2) + com.agilebinary.a.a.a.k.I("\\i")).getBytes(this.d));
        }
        return a2;
    }

    public final int a(byte[] bArr, int i, int i2) {
        int a2 = this.f50a.a(bArr, i, i2);
        if (this.c.a() && a2 > 0) {
            this.c.b(bArr, i, a2);
        }
        return a2;
    }

    public final boolean a(int i) {
        return this.f50a.a(i);
    }

    public final int d() {
        int d2 = this.f50a.d();
        if (this.c.a() && d2 != -1) {
            this.c.b(new byte[]{(byte) d2});
        }
        return d2;
    }

    public final f e() {
        return this.f50a.e();
    }

    public final boolean f() {
        if (this.b != null) {
            return this.b.f();
        }
        return false;
    }
}
