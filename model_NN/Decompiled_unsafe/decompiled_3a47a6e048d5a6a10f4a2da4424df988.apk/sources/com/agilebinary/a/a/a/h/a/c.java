package com.agilebinary.a.a.a.h.a;

import com.agilebinary.a.a.a.c.b.a.f;
import java.util.concurrent.ConcurrentHashMap;

public final class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f99a;
    private volatile int b;

    public c() {
        this((byte) 0);
    }

    private /* synthetic */ c(byte b2) {
        this.f99a = new ConcurrentHashMap();
        a(2);
    }

    public final int a(com.agilebinary.a.a.a.h.c.c cVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(f.I("BB^F*dec~s*{ko*xeb*to6dcfz$"));
        }
        Integer num = (Integer) this.f99a.get(cVar);
        return num != null ? num.intValue() : this.b;
    }

    public final void a(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.k.f.I("b#Sk[*N\"[>[k[>E?\u0016)SkQ9S*B.DkB#W%\u0016{\u0018"));
        }
        this.b = i;
    }

    public final String toString() {
        return this.f99a.toString();
    }
}
