package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.i.f;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.g;
import java.io.IOException;
import java.io.InputStream;

public final class m extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private long f66a;
    private long b = 0;
    private boolean c = false;
    private a d = null;

    public m(a aVar, long j) {
        if (aVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.k.a.I("H]qFu\u0013rGsV`^!^`J!]nG!Qd\u0013oFm_"));
        } else if (j < 0) {
            throw new IllegalArgumentException(f.I("lTAOJU[\u001bC^A\\[S\u000fVNB\u000fU@O\u000fYJ\u001bA^HZ[RY^"));
        } else {
            this.d = aVar;
            this.f66a = j;
        }
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 31);
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 31);
            i2 = i;
        }
        return new String(cArr);
    }

    public final int available() {
        if (this.d instanceof g) {
            return Math.min(((g) this.d).a(), (int) (this.f66a - this.b));
        }
        return 0;
    }

    public final void close() {
        if (!this.c) {
            try {
                do {
                } while (read(new byte[2048]) >= 0);
            } finally {
                this.c = true;
            }
        }
    }

    public final int read() {
        if (this.c) {
            throw new IOException(com.agilebinary.a.a.a.k.a.I("@GuVlCuVe\u0013sV`W!Us\\l\u0013b_n@dW!@uAdRl\u001d"));
        } else if (this.b >= this.f66a) {
            return -1;
        } else {
            this.b++;
            return this.d.d();
        }
    }

    public final int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (this.c) {
            throw new IOException(f.I("nO[^BK[^K\u001b]^N_\u000f]]TB\u001bLW@HJ_\u000fH[IJZB\u0015"));
        } else if (this.b >= this.f66a) {
            return -1;
        } else {
            if (this.b + ((long) i2) > this.f66a) {
                i2 = (int) (this.f66a - this.b);
            }
            int a2 = this.d.a(bArr, i, i2);
            this.b += (long) a2;
            return a2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public final long skip(long j) {
        int read;
        if (j <= 0) {
            return 0;
        }
        byte[] bArr = new byte[2048];
        long min = Math.min(j, this.f66a - this.b);
        long j2 = 0;
        while (min > 0 && (read = read(bArr, 0, (int) Math.min(2048L, min))) != -1) {
            j2 += (long) read;
            min -= (long) read;
        }
        return j2;
    }
}
