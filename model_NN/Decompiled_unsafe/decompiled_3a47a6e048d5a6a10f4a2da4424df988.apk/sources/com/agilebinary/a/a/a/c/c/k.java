package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.j.e;
import java.io.IOException;
import java.io.OutputStream;

public final class k extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final e f65a;
    private boolean b = false;

    public k(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException(e.I(";D\u001bR\u0001N\u0006\u0001\u0007T\u001cQ\u001dUHC\u001dG\u000eD\u001a\u0001\u0005@\u0011\u0001\u0006N\u001c\u0001\nDHO\u001dM\u0004"));
        }
        this.f65a = eVar;
    }

    public final void close() {
        if (!this.b) {
            this.b = true;
            this.f65a.b();
        }
    }

    public final void flush() {
        this.f65a.b();
    }

    public final void write(int i) {
        if (this.b) {
            throw new IOException(com.agilebinary.mobilemonitor.client.android.c.e.I("2B\u0007S\u001eF\u0007S\u0017\u0016\u0004D\u001aB\u0016\u0016\u0007YSU\u001fY\u0000S\u0017\u0016\u0000B\u0001S\u0012[]"));
        }
        this.f65a.a(i);
    }

    public final void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public final void write(byte[] bArr, int i, int i2) {
        if (this.b) {
            throw new IOException(e.I(")U\u001cD\u0005Q\u001cD\f\u0001\u001fS\u0001U\r\u0001\u001cNHB\u0004N\u001bD\f\u0001\u001bU\u001aD\tLF"));
        }
        this.f65a.a(bArr, i, i2);
    }
}
