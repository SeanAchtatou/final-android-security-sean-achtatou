package com.agilebinary.a.a.a.h;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.g.b;
import com.agilebinary.a.a.a.k.a;
import java.io.InputStream;
import java.io.OutputStream;

public final class j extends b implements d, e {
    private a b;
    private boolean c;

    public j(c cVar, a aVar, boolean z) {
        super(cVar);
        if (aVar == null) {
            throw new IllegalArgumentException(a.I("pn]oVbGh\\o\u0013lRx\u0013o\\u\u0013cV!]t_m\u001d"));
        }
        this.b = aVar;
        this.c = z;
    }

    private /* synthetic */ void h() {
        if (this.b != null) {
            try {
                if (this.c) {
                    com.agilebinary.a.a.a.i.b.a(this.f94a);
                    this.b.g();
                }
            } finally {
                i();
            }
        }
    }

    private /* synthetic */ void i() {
        if (this.b != null) {
            try {
                this.b.d_();
            } finally {
                this.b = null;
            }
        }
    }

    public final void a(OutputStream outputStream) {
        super.a(outputStream);
        h();
    }

    public final boolean a() {
        return false;
    }

    /* JADX INFO: finally extract failed */
    public final boolean a(InputStream inputStream) {
        try {
            if (this.c && this.b != null) {
                inputStream.close();
                this.b.g();
            }
            i();
            return false;
        } catch (Throwable th) {
            i();
            throw th;
        }
    }

    public final void b() {
        if (this.b != null) {
            try {
                this.b.b();
            } finally {
                this.b = null;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public final boolean b(InputStream inputStream) {
        try {
            if (this.c && this.b != null) {
                inputStream.close();
                this.b.g();
            }
            i();
            return false;
        } catch (Throwable th) {
            i();
            throw th;
        }
    }

    public final void d_() {
        h();
    }

    public final InputStream f() {
        return new c(this.f94a.f(), this);
    }

    public final boolean j_() {
        if (this.b == null) {
            return false;
        }
        this.b.b();
        return false;
    }
}
