package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.h;
import com.agilebinary.a.a.a.k.j;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.t;
import java.util.Iterator;
import java.util.List;
import org.b.a.a.c;
import org.osmdroid.b.a;

public final class p implements j {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f30a;
    private final boolean b;
    private ae c;
    private ag d;
    private w e;
    private q f;

    public p() {
        this(null, false);
    }

    public p(String[] strArr, boolean z) {
        this.f30a = strArr == null ? null : (String[]) strArr.clone();
        this.b = z;
    }

    private /* synthetic */ ae c() {
        if (this.c == null) {
            this.c = new ae(this.f30a, this.b);
        }
        return this.c;
    }

    private /* synthetic */ ag d() {
        if (this.d == null) {
            this.d = new ag(this.f30a, this.b);
        }
        return this.d;
    }

    private /* synthetic */ w e() {
        if (this.e == null) {
            this.e = new w(this.f30a);
        }
        return this.e;
    }

    public final int a() {
        return c().a();
    }

    public final List a(t tVar, f fVar) {
        boolean z = false;
        if (tVar == null) {
            throw new IllegalArgumentException(c.I("d*M+I=\f\"M6\f!C;\f-IoB:@#"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(a.I("\u001d#1'7)~#,%9%0l3-'l0#*l<)~\"+ 2"));
        } else {
            m[] c2 = tVar.c();
            boolean z2 = false;
            for (m mVar : c2) {
                if (mVar.a(c.I("9I=_&C!")) != null) {
                    z = true;
                }
                if (mVar.a(a.I(";4.%,)-")) != null) {
                    z2 = true;
                }
            }
            if (z) {
                return c.I("\u001cI;\u0001\fC G&I}").equals(tVar.a()) ? c().a(c2, fVar) : d().a(c2, fVar);
            }
            if (!z2) {
                return e().a(c2, fVar);
            }
            if (this.f == null) {
                this.f = new q(this.f30a);
            }
            return this.f.a(tVar, fVar);
        }
    }

    public final List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException(a.I("\u00007?*l1*~/1#5%;l3-'l0#*l<)~\"+ 2"));
        }
        Iterator it = list.iterator();
        int i = Integer.MAX_VALUE;
        boolean z = true;
        while (it.hasNext()) {
            com.agilebinary.a.a.a.k.c cVar = (com.agilebinary.a.a.a.k.c) it.next();
            if (!(cVar instanceof h)) {
                z = false;
            }
            i = cVar.h() < i ? cVar.h() : i;
        }
        return i > 0 ? z ? c().a(list) : d().a(list) : e().a(list);
    }

    public final void a(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(c.I("o C$E*\f\"M6\f!C;\f-IoB:@#"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(a.I("\u001d#1'7)~#,%9%0l3-'l0#*l<)~\"+ 2"));
        } else if (cVar.h() <= 0) {
            e().a(cVar, fVar);
        } else if (cVar instanceof h) {
            c().a(cVar, fVar);
        } else {
            d().a(cVar, fVar);
        }
    }

    public final t b() {
        return c().b();
    }

    public final boolean b(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(c.I("o C$E*\f\"M6\f!C;\f-IoB:@#"));
        } else if (fVar != null) {
            return cVar.h() > 0 ? cVar instanceof h ? c().b(cVar, fVar) : d().b(cVar, fVar) : e().b(cVar, fVar);
        } else {
            throw new IllegalArgumentException(a.I("\u001d#1'7)~#,%9%0l3-'l0#*l<)~\"+ 2"));
        }
    }

    public final String toString() {
        return c.I("N*_;\u0001\"M;O'");
    }
}
