package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.j;
import com.agilebinary.a.a.a.k.k;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class u implements j {

    /* renamed from: a  reason: collision with root package name */
    private final Map f32a = new HashMap(10);

    /* access modifiers changed from: protected */
    public final k a(String str) {
        return (k) this.f32a.get(str);
    }

    public final void a(String str, k kVar) {
        if (str == null) {
            throw new IllegalArgumentException(f.I("\nB?D\"T>B.\u0016%W&Sk[*OkX$BkT.\u0016%C'Z"));
        } else if (kVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.h.f.I("8A\rG\u0010W\fA\u001c\u0015\u0011T\u0017Q\u0015P\u000b\u0015\u0014T\u0000\u0015\u0017Z\r\u0015\u001bPY[\fY\u0015"));
        } else {
            this.f32a.put(str, kVar);
        }
    }

    /* access modifiers changed from: protected */
    public final Collection c() {
        return this.f32a.values();
    }
}
