package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.d.a.b;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.mobilemonitor.client.android.h;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private final int f14a;
    private final int b;
    private int c;

    public i(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException(h.I("v7M=HxX7O6^xY9T6U,\u001a:_xT=]9N1L="));
        } else if (i > i2) {
            throw new IndexOutOfBoundsException(b.I("++\u0010!\u0015d\u0005+\u0012*\u0003d\u0004%\t*\b0G&\u0002d\u00006\u0002%\u0013!\u0015d\u0013,\u0002*G1\u00174\u00026G&\b1\t "));
        } else {
            this.f14a = i;
            this.b = i2;
            this.c = i;
        }
    }

    public final int a() {
        return this.b;
    }

    public final void a(int i) {
        if (i < this.f14a) {
            throw new IndexOutOfBoundsException(h.I("(U+\u0000x") + i + b.I("d[d\u000b+\u0010!\u0015\u0006\b1\t ]d") + this.f14a);
        } else if (i > this.b) {
            throw new IndexOutOfBoundsException(h.I("(U+\u0000x") + i + b.I("dYd\u00124\u0017!\u0015\u0006\b1\t ]d") + this.b);
        } else {
            this.c = i;
        }
    }

    public final int b() {
        return this.c;
    }

    public final boolean c() {
        return this.c >= this.b;
    }

    public final String toString() {
        c cVar = new c(16);
        cVar.a('[');
        cVar.a(Integer.toString(this.f14a));
        cVar.a('>');
        cVar.a(Integer.toString(this.c));
        cVar.a('>');
        cVar.a(Integer.toString(this.b));
        cVar.a(']');
        return cVar.toString();
    }
}
