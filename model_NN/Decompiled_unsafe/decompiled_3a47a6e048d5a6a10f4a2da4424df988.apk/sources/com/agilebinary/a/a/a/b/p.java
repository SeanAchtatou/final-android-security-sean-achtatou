package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.c.c.e;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.v;
import com.agilebinary.a.a.a.w;
import java.util.NoSuchElementException;

public final class p implements v {

    /* renamed from: a  reason: collision with root package name */
    private final w f18a;
    private final j b;
    private m c;
    private c d;
    private i e;

    public p(w wVar) {
        this(wVar, f.f11a);
    }

    private /* synthetic */ p(w wVar, j jVar) {
        this.c = null;
        this.d = null;
        this.e = null;
        if (wVar == null) {
            throw new IllegalArgumentException(org.osmdroid.util.c.I("o\u0003F\u0002B\u0014\u0007\u000fS\u0003U\u0007S\tUFJ\u0007^FI\tSFE\u0003\u0007\bR\nK"));
        } else if (jVar == null) {
            throw new IllegalArgumentException(e.I("q\tS\u001bD\u001a\u0001\u0005@\u0011\u0001\u0006N\u001c\u0001\nDHO\u001dM\u0004"));
        } else {
            this.f18a = wVar;
            this.b = jVar;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x005d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void b() {
        /*
            r5 = this;
            r4 = 0
            r3 = 0
        L_0x0002:
            com.agilebinary.a.a.a.w r0 = r5.f18a
            boolean r0 = r0.hasNext()
            if (r0 != 0) goto L_0x000e
            com.agilebinary.a.a.a.b.i r0 = r5.e
            if (r0 == 0) goto L_0x0079
        L_0x000e:
            com.agilebinary.a.a.a.b.i r0 = r5.e
            if (r0 == 0) goto L_0x001a
            com.agilebinary.a.a.a.b.i r0 = r5.e
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x0051
        L_0x001a:
            r5.e = r3
            r5.d = r3
        L_0x001e:
            com.agilebinary.a.a.a.w r0 = r5.f18a
            boolean r0 = r0.hasNext()
            if (r0 == 0) goto L_0x0051
            com.agilebinary.a.a.a.w r0 = r5.f18a
            com.agilebinary.a.a.a.t r1 = r0.a()
            boolean r0 = r1 instanceof com.agilebinary.a.a.a.r
            if (r0 == 0) goto L_0x007a
            r0 = r1
            com.agilebinary.a.a.a.r r0 = (com.agilebinary.a.a.a.r) r0
            com.agilebinary.a.a.a.i.c r0 = r0.e()
            r5.d = r0
            com.agilebinary.a.a.a.b.i r0 = new com.agilebinary.a.a.a.b.i
            com.agilebinary.a.a.a.i.c r2 = r5.d
            int r2 = r2.c()
            r0.<init>(r4, r2)
            r5.e = r0
            com.agilebinary.a.a.a.b.i r0 = r5.e
            com.agilebinary.a.a.a.r r1 = (com.agilebinary.a.a.a.r) r1
            int r1 = r1.d()
            r0.a(r1)
        L_0x0051:
            com.agilebinary.a.a.a.b.i r0 = r5.e
            if (r0 == 0) goto L_0x0002
        L_0x0055:
            com.agilebinary.a.a.a.b.i r0 = r5.e
            boolean r0 = r0.c()
            if (r0 != 0) goto L_0x009e
            com.agilebinary.a.a.a.b.j r0 = r5.b
            com.agilebinary.a.a.a.i.c r1 = r5.d
            com.agilebinary.a.a.a.b.i r2 = r5.e
            com.agilebinary.a.a.a.m r0 = r0.b(r1, r2)
            java.lang.String r1 = r0.a()
            int r1 = r1.length()
            if (r1 != 0) goto L_0x0077
            java.lang.String r1 = r0.b()
            if (r1 == 0) goto L_0x0055
        L_0x0077:
            r5.c = r0
        L_0x0079:
            return
        L_0x007a:
            java.lang.String r0 = r1.b()
            if (r0 == 0) goto L_0x001e
            com.agilebinary.a.a.a.i.c r1 = new com.agilebinary.a.a.a.i.c
            int r2 = r0.length()
            r1.<init>(r2)
            r5.d = r1
            com.agilebinary.a.a.a.i.c r1 = r5.d
            r1.a(r0)
            com.agilebinary.a.a.a.b.i r0 = new com.agilebinary.a.a.a.b.i
            com.agilebinary.a.a.a.i.c r1 = r5.d
            int r1 = r1.c()
            r0.<init>(r4, r1)
            r5.e = r0
            goto L_0x0051
        L_0x009e:
            com.agilebinary.a.a.a.b.i r0 = r5.e
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x0002
            r5.e = r3
            r5.d = r3
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.b.p.b():void");
    }

    public final m a() {
        if (this.c == null) {
            b();
        }
        if (this.c == null) {
            throw new NoSuchElementException(org.osmdroid.util.c.I("i\t\u0007\u000bH\u0014BFO\u0003F\u0002B\u0014\u0007\u0003K\u0003J\u0003I\u0012TFF\u0010F\u000fK\u0007E\nB"));
        }
        m mVar = this.c;
        this.c = null;
        return mVar;
    }

    public final boolean hasNext() {
        if (this.c == null) {
            b();
        }
        return this.c != null;
    }

    public final Object next() {
        return a();
    }

    public final void remove() {
        throw new UnsupportedOperationException(e.I("s\rL\u0007W\r\u0001\u0006N\u001c\u0001\u001bT\u0018Q\u0007S\u001cD\f"));
    }
}
