package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.e;
import com.agilebinary.a.a.a.i.f;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.w;
import java.util.NoSuchElementException;

public final class d implements e {

    /* renamed from: a  reason: collision with root package name */
    private w f9a;
    private String b;
    private String c;
    private int d;

    public d(w wVar) {
        if (wVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I(" D\tE\rSHH\u001cD\u001a@\u001cN\u001a\u0001\u0005T\u001bUHO\u0007UHC\r\u0001\u0006T\u0004MF"));
        }
        this.f9a = wVar;
        this.d = a(-1);
    }

    private /* synthetic */ int a(int i) {
        int i2 = 0;
        if (i < 0) {
            if (!this.f9a.hasNext()) {
                return -1;
            }
            this.b = this.f9a.a().b();
        } else if (i < 0) {
            throw new IllegalArgumentException(f.I("|^NILS\u000fK@HFOFTA\u001bBN\\O\u000fU@O\u000fYJ\u001bA^HZ[RY^\u0015\u001b") + i);
        } else {
            int length = this.b.length();
            boolean z = false;
            i2 = i;
            while (!z && i2 < length) {
                char charAt = this.b.charAt(i2);
                if (a(charAt)) {
                    z = true;
                } else if (b(charAt)) {
                    i2++;
                } else if (c(charAt)) {
                    throw new u(com.agilebinary.a.a.a.c.c.e.I("u\u0007J\rO\u001b\u0001\u001fH\u001cI\u0007T\u001c\u0001\u001bD\u0018@\u001a@\u001cN\u001a\u0001@Q\u0007RH") + i2 + f.I("\u0012\u0015\u001b") + this.b);
                } else {
                    throw new u(com.agilebinary.a.a.a.c.c.e.I("!O\u001e@\u0004H\f\u0001\u000bI\tS\tB\u001cD\u001a\u0001\tG\u001cD\u001a\u0001\u001cN\u0003D\u0006\u0001@Q\u0007RH") + i2 + f.I("\u0012\u0015\u001b") + this.b);
                }
            }
        }
        int b2 = b(i2);
        if (b2 < 0) {
            this.c = null;
            return -1;
        }
        int c2 = c(b2);
        this.c = this.b.substring(b2, c2);
        return c2;
    }

    private static /* synthetic */ boolean a(char c2) {
        return c2 == ',';
    }

    private /* synthetic */ int b(int i) {
        if (i < 0) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I("r\r@\u001aB\u0000\u0001\u0018N\u001bH\u001cH\u0007OHL\u001dR\u001c\u0001\u0006N\u001c\u0001\nDHO\rF\tU\u0001W\r\u001bH") + i);
        }
        boolean z = false;
        int i2 = i;
        while (!z && this.b != null) {
            int length = this.b.length();
            while (!z && i2 < length) {
                char charAt = this.b.charAt(i2);
                if (a(charAt) || b(charAt)) {
                    i2++;
                } else if (c(this.b.charAt(i2))) {
                    z = true;
                } else {
                    throw new u(f.I("fUYZCRK\u001bLSNINX[^]\u001bM^IT]^\u000fO@PJU\u000f\u0013_T\\\u001b") + i2 + com.agilebinary.a.a.a.c.c.e.I("A\u001bH") + this.b);
                }
            }
            if (!z) {
                if (this.f9a.hasNext()) {
                    this.b = this.f9a.a().b();
                    i2 = 0;
                } else {
                    this.b = null;
                }
            }
        }
        if (z) {
            return i2;
        }
        return -1;
    }

    private static /* synthetic */ boolean b(char c2) {
        return c2 == 9 || Character.isSpaceChar(c2);
    }

    private /* synthetic */ int c(int i) {
        if (i < 0) {
            throw new IllegalArgumentException(f.I("o@PJU\u000fH[Z]O\u000fK@HFOFTA\u001bBN\\O\u000fU@O\u000fYJ\u001bA^HZ[RY^\u0015\u001b") + i);
        }
        int length = this.b.length();
        int i2 = i + 1;
        while (i2 < length && c(this.b.charAt(i2))) {
            i2++;
        }
        return i2;
    }

    private static /* synthetic */ boolean c(char c2) {
        if (Character.isLetterOrDigit(c2)) {
            return true;
        }
        if (Character.isISOControl(c2)) {
            return false;
        }
        return !(com.agilebinary.a.a.a.c.c.e.I("H\rS\u001c@\bT\u001f(\u001b4\u0003Gz5\u001e\u0013\\a").indexOf(c2) >= 0);
    }

    public final String a() {
        if (this.c == null) {
            throw new NoSuchElementException(f.I("r[^]Z[R@U\u000fZCIJZKB\u000f]FUFHG^K\u0015"));
        }
        String str = this.c;
        this.d = a(this.d);
        return str;
    }

    public final boolean hasNext() {
        return this.c != null;
    }

    public final Object next() {
        return a();
    }

    public final void remove() {
        throw new UnsupportedOperationException(com.agilebinary.a.a.a.c.c.e.I(":D\u0005N\u001eH\u0006FHU\u0007J\rO\u001b\u0001\u0001RHO\u0007UHR\u001dQ\u0018N\u001aU\rEF"));
    }
}
