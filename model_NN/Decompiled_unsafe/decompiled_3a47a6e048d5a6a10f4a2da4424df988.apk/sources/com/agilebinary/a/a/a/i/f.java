package com.agilebinary.a.a.a.i;

public final class f {
    private /* synthetic */ f() {
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ ';');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '/');
            i2 = i;
        }
        return new String(cArr);
    }

    public static int a(int i, Object obj) {
        return (obj != null ? obj.hashCode() : 0) + (i * 37);
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }
}
