package com.agilebinary.a.a.a.f;

import com.agilebinary.mobilemonitor.client.android.ui.a.e;
import java.util.HashMap;
import java.util.Map;

public final class g implements k {

    /* renamed from: a  reason: collision with root package name */
    private final k f92a;
    private Map b;

    private /* synthetic */ g() {
        this.b = null;
        this.f92a = null;
    }

    public g(byte b2) {
        this();
    }

    public final Object a(String str) {
        if (str == null) {
            throw new IllegalArgumentException(e.I("#iJ`\u000btJc\u0005yJo\u000f-\u0004x\u0006a"));
        }
        Object obj = null;
        if (this.b != null) {
            obj = this.b.get(str);
        }
        return (obj != null || this.f92a == null) ? obj : this.f92a.a(str);
    }

    public final void a(String str, Object obj) {
        if (str == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I("h\f\u0001\u0005@\u0011\u0001\u0006N\u001c\u0001\nDHO\u001dM\u0004"));
        }
        if (this.b == null) {
            this.b = new HashMap();
        }
        this.b.put(str, obj);
    }
}
