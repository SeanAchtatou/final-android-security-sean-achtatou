package com.agilebinary.a.a.a.d.a;

import com.agilebinary.a.a.a.c.b.e;
import java.lang.reflect.InvocationTargetException;

public final class a {
    private /* synthetic */ a() {
    }

    public static Object a(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Cloneable) {
            try {
                try {
                    return obj.getClass().getMethod(e.I("\u0002\u0015\u000e\u0017\u0004"), null).invoke(obj, null);
                } catch (InvocationTargetException e) {
                    Throwable cause = e.getCause();
                    if (cause instanceof CloneNotSupportedException) {
                        throw ((CloneNotSupportedException) cause);
                    }
                    throw new Error(com.jasonkostempski.android.calendar.a.I("uEESPNC_EO\u0000NXHE[TBOE"), cause);
                } catch (IllegalAccessException e2) {
                    throw new IllegalAccessError(e2.getMessage());
                }
            } catch (NoSuchMethodException e3) {
                throw new NoSuchMethodError(e3.getMessage());
            }
        } else {
            throw new CloneNotSupportedException();
        }
    }
}
