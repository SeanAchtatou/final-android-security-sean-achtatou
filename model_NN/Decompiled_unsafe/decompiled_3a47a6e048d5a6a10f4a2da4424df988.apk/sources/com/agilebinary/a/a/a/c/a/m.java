package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.c.e.c;
import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.b.a.a;

public final class m implements k {
    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException(c.I("su_qY\u0010wQc\u0010t_n\u0010xU:^o\\v"));
        }
        if (str == null || str.trim().length() == 0) {
            str = "/";
        }
        bVar.c(str);
    }

    public final void a(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (!b(cVar, fVar)) {
            throw new i(a.I("NDkM`Ik\bwIs@'Is\\uAe]sM'\n") + cVar.d() + c.I("\u00124\u0010JQnX:_|\u0010uBsWs^ \u00108") + fVar.b() + a.I("\n"));
        }
    }

    public final boolean b(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(c.I("su_qY\u0010wQc\u0010t_n\u0010xU:^o\\v"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(a.I("khGlAb\bhZnOnF'EfQ'Fh\\'Jb\bi]kD"));
        } else {
            String b = fVar.b();
            String d = cVar.d();
            if (d == null) {
                d = "/";
            }
            if (d.length() > 1 && d.endsWith("/")) {
                d = d.substring(0, d.length() - 1);
            }
            boolean startsWith = b.startsWith(d);
            return (!startsWith || b.length() == d.length() || d.endsWith("/")) ? startsWith : b.charAt(d.length()) == '/';
        }
    }
}
