package com.agilebinary.a.a.a.h.c;

import com.agilebinary.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.c.e;
import java.net.InetAddress;

public final class c implements b, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private static final b[] f106a = new b[0];
    private final b b;
    private final InetAddress c;
    private final b[] d;
    private final g e;
    private final e f;
    private final boolean g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.h.c.c.<init>(java.net.InetAddress, com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void
     arg types: [?[OBJECT, ARRAY], com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], int, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e]
     candidates:
      com.agilebinary.a.a.a.h.c.c.<init>(com.agilebinary.a.a.a.b, java.net.InetAddress, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void
      com.agilebinary.a.a.a.h.c.c.<init>(java.net.InetAddress, com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void */
    public c(b bVar) {
        this((InetAddress) null, bVar, f106a, false, g.f109a, e.f107a);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public c(b bVar, InetAddress inetAddress, b bVar2, boolean z) {
        this(inetAddress, bVar, bVar2 == null ? f106a : new b[]{bVar2}, z, z ? g.b : g.f109a, z ? e.b : e.f107a);
        if (bVar2 == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.f.b.I("\u001bx$r2*#e8~kg*skd$~kh.*%'fe"));
        }
    }

    public c(b bVar, InetAddress inetAddress, boolean z) {
        this(inetAddress, bVar, f106a, z, g.f109a, e.f107a);
    }

    public c(b bVar, InetAddress inetAddress, b[] bVarArr, boolean z, g gVar, e eVar) {
        this(inetAddress, bVar, a(bVarArr), z, gVar, eVar);
    }

    private /* synthetic */ c(InetAddress inetAddress, b bVar, b[] bVarArr, boolean z, g gVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException(e.I("b\u0012D\u0014S\u0007\u0016\u001bY\u0000BS[\u0012OSX\u001cBST\u0016\u0016\u001dC\u001fZ]"));
        } else if (bVarArr == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.f.b.I("Z9e3c.ykg*skd$~kh.*%'fe"));
        } else if (gVar == g.b && bVarArr.length == 0) {
            throw new IllegalArgumentException(e.I("f\u0001Y\u000bOSD\u0016G\u0006_\u0001S\u0017\u0016\u001aPSB\u0006X\u001dS\u001fZ\u0016R]"));
        } else {
            gVar = gVar == null ? g.f109a : gVar;
            eVar = eVar == null ? e.f107a : eVar;
            this.b = bVar;
            this.c = inetAddress;
            this.d = bVarArr;
            this.g = z;
            this.e = gVar;
            this.f = eVar;
        }
    }

    private static /* synthetic */ b[] a(b[] bVarArr) {
        if (bVarArr == null || bVarArr.length <= 0) {
            return f106a;
        }
        for (b bVar : bVarArr) {
            if (bVar == null) {
                throw new IllegalArgumentException(com.agilebinary.a.a.a.f.b.I("Z9e3ski#k\"dkg*skd$~ki$d?k\"dkd>f'*.f.g.d?ye"));
            }
        }
        b[] bVarArr2 = new b[bVarArr.length];
        System.arraycopy(bVarArr, 0, bVarArr2, 0, bVarArr.length);
        return bVarArr2;
    }

    public final b a() {
        return this.b;
    }

    public final b a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException(e.I("~\u001cFS_\u001dR\u0016NS[\u0006E\u0007\u0016\u001dY\u0007\u0016\u0011SSX\u0016Q\u0012B\u001a@\u0016\fS") + i);
        }
        int length = this.d.length + 1;
        if (i < length) {
            return i < length + -1 ? this.d[i] : this.b;
        }
        throw new IllegalArgumentException(com.agilebinary.a.a.a.f.b.I("B$zkc%n.rk") + i + e.I("\u0016\u0016N\u0010S\u0016R\u0000\u0016\u0001Y\u0006B\u0016\u0016\u001fS\u001dQ\u0007^S") + length);
    }

    public final InetAddress b() {
        return this.c;
    }

    public final int c() {
        return this.d.length + 1;
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean d() {
        return this.e == g.b;
    }

    public final boolean e() {
        return this.f == e.b;
    }

    public final boolean equals(Object obj) {
        boolean z = true;
        int i = 0;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        boolean equals = this.b.equals(cVar.b);
        boolean z2 = this.c == cVar.c || (this.c != null && this.c.equals(cVar.c));
        boolean z3 = this.d == cVar.d || this.d.length == cVar.d.length;
        if (!(this.g == cVar.g && this.e == cVar.e && this.f == cVar.f)) {
            z = false;
        }
        boolean z4 = z & z2 & equals & z3;
        if (!z4 || this.d == null) {
            return z4;
        }
        while (z4 && i < this.d.length) {
            z4 = this.d[i].equals(cVar.d[i]);
            i++;
        }
        return z4;
    }

    public final boolean f() {
        return this.g;
    }

    public final b g() {
        if (this.d.length == 0) {
            return null;
        }
        return this.d[0];
    }

    public final int hashCode() {
        int hashCode = this.b.hashCode();
        if (this.c != null) {
            hashCode ^= this.c.hashCode();
        }
        int length = this.d.length;
        b[] bVarArr = this.d;
        int length2 = bVarArr.length;
        int i = length ^ hashCode;
        int i2 = 0;
        while (i2 < length2) {
            int hashCode2 = bVarArr[i2].hashCode() ^ i;
            i2++;
            i = hashCode2;
        }
        if (this.g) {
            i ^= 286331153;
        }
        return (i ^ this.e.hashCode()) ^ this.f.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(((this.d.length + 1) * 30) + 50);
        sb.append(com.agilebinary.a.a.a.f.b.I("B?~;X$?o\u0010"));
        if (this.c != null) {
            sb.append(this.c);
            sb.append(e.I("\u001bM"));
        }
        sb.append('{');
        if (this.e == g.b) {
            sb.append('t');
        }
        if (this.f == e.b) {
            sb.append('l');
        }
        if (this.g) {
            sb.append('s');
        }
        sb.append(com.agilebinary.a.a.a.f.b.I("6'u"));
        for (b append : this.d) {
            sb.append(append);
            sb.append(e.I("\u001bM"));
        }
        sb.append(this.b);
        sb.append(']');
        return sb.toString();
    }
}
