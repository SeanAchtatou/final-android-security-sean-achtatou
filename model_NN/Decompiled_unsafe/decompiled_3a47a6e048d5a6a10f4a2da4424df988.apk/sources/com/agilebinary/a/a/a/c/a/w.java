package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.List;
import org.osmdroid.b.a.e;

public final class w extends ah {

    /* renamed from: a  reason: collision with root package name */
    private static String[] f33a = {e.I("Q=QT4\u001cpXY5YXm\u0001m\u000140\\By\u0015.\u000bgXn\u0002n"), com.agilebinary.a.a.a.c.b.e.I("<$<$UA\u001d\u0005T,4,T\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b\u0003\u001b"), e.I("Q=QXY5YXpX\\0.\u0015yBg\u000b4\u0001m\u0001m"), com.agilebinary.a.a.a.c.b.e.I("$<$UA\u001d\u0005T,4,T\u0018\u0000\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b"), e.I("Q=QT4\u001cpUY5YUm\u0001m\u000140\\Uy\u00159\u000bgXn"), com.agilebinary.a.a.a.c.b.e.I("$<$UA\u001d\u0005Y,4,Y\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b"), e.I("=Q=4\u001cpUY5YUm\u0001m\u000140\\By\u0015.\u000bgXn"), com.agilebinary.a.a.a.c.b.e.I("<$<A\u001d\u0005Y,4,Y\u0018\u0000\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b"), e.I("=Q=4\u001cpUY5YUm\u0001m\u000140\\Uy\u00159\u000bgXn"), com.agilebinary.a.a.a.c.b.e.I("<$<A\u001d\u0005T,4,T\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b"), e.I("=Q=4\u001cpXY5YXm\u000140\\By\u0015.\u000bgXn"), com.agilebinary.a.a.a.c.b.e.I("<$<M\u001d\u0005T,4,T\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b"), e.I("=Q=8\u001cpUY5YUm\u0001m\u000140\\By\u0015.\u000bgXn"), com.agilebinary.a.a.a.c.b.e.I("<$<MY\u0005\u001dL4,T\u0018\u0000\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b")};
    private static final String[] b = {e.I("Q=QT4\u001cpXY5YXm\u0001m\u000140\\By\u0015.\u000bgXn\u0002n"), com.agilebinary.a.a.a.c.b.e.I("<$<$UA\u001d\u0005T,4,T\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b\u0003\u001b"), e.I("Q=QXY5YXpX\\0.\u0015yBg\u000b4\u0001m\u0001m"), com.agilebinary.a.a.a.c.b.e.I("$<$UA\u001d\u0005T,4,T\u0018\u0000\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b"), e.I("Q=QT4\u001cpUY5YUm\u0001m\u000140\\Uy\u00159\u000bgXn"), com.agilebinary.a.a.a.c.b.e.I("$<$UA\u001d\u0005Y,4,Y\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b"), e.I("=Q=4\u001cpUY5YUm\u0001m\u000140\\By\u0015.\u000bgXn"), com.agilebinary.a.a.a.c.b.e.I("<$<A\u001d\u0005Y,4,Y\u0018\u0000\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b"), e.I("=Q=4\u001cpUY5YUm\u0001m\u000140\\Uy\u00159\u000bgXn"), com.agilebinary.a.a.a.c.b.e.I("<$<A\u001d\u0005T,4,T\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b"), e.I("=Q=4\u001cpXY5YXm\u000140\\By\u0015.\u000bgXn"), com.agilebinary.a.a.a.c.b.e.I("<$<M\u001d\u0005T,4,T\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b"), e.I("=Q=8\u001cpUY5YUm\u0001m\u000140\\By\u0015.\u000bgXn"), com.agilebinary.a.a.a.c.b.e.I("<$<MY\u0005\u001dL4,T\u0018\u0000\u0018\u0000A1)C\f\u0014[\n\u0012Y\u001b")};
    private final String[] c;

    public w() {
        this(null);
    }

    public w(String[] strArr) {
        if (strArr != null) {
            this.c = (String[]) strArr.clone();
        } else {
            this.c = b;
        }
        a(e.I("\bu\f|"), new m());
        a(com.agilebinary.a.a.a.c.b.e.I("\u001d\u000e\u0014\u0000\u0010\u000f"), new r());
        a(e.I("y\u0019lUu\u001fq"), new ac());
        a(com.agilebinary.a.a.a.c.b.e.I("\n\u0004\u001a\u0014\u000b\u0004"), new h());
        a(e.I("w\u0017y\u0015q\u0016`"), new o());
        a(com.agilebinary.a.a.a.c.b.e.I("\u0004\u0001\u0011\u0010\u0013\u001c\u0012"), new j(this.c));
    }

    public final int a() {
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List a(com.agilebinary.a.a.a.t r7, com.agilebinary.a.a.a.k.f r8) {
        /*
            r6 = this;
            r2 = 1
            r5 = -1
            r3 = 0
            if (r7 != 0) goto L_0x0011
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "0q\u0019p\u001dfXy\u0019mXz\u0017`Xv\u001d4\u0016a\u0014x"
            java.lang.String r1 = org.osmdroid.b.a.e.I(r1)
            r0.<init>(r1)
            throw r0
        L_0x0011:
            if (r8 != 0) goto L_0x001f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "\"\u0016\u000e\u0012\b\u001cA\u0016\u0013\u0010\u0006\u0010\u000fY\f\u0018\u0018Y\u000f\u0016\u0015Y\u0003\u001cA\u0017\u0014\u0015\r"
            java.lang.String r1 = com.agilebinary.a.a.a.c.b.e.I(r1)
            r0.<init>(r1)
            throw r0
        L_0x001f:
            java.lang.String r0 = r7.a()
            java.lang.String r1 = r7.b()
            java.lang.String r4 = "+q\f9;{\u0017\u0011q"
            java.lang.String r4 = org.osmdroid.b.a.e.I(r4)
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x005e
            com.agilebinary.a.a.a.k.e r0 = new com.agilebinary.a.a.a.k.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = ",\u000f\u000b\u0004\u001a\u000e\u001e\u000f\u0010\u001b\u001c\u0005Y\u0002\u0016\u000e\u0012\b\u001cA\u0011\u0004\u0018\u0005\u001c\u0013YF"
            java.lang.String r2 = com.agilebinary.a.a.a.c.b.e.I(r2)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r7.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "3"
            java.lang.String r2 = org.osmdroid.b.a.e.I(r2)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x005e:
            java.util.Locale r0 = java.util.Locale.ENGLISH
            java.lang.String r0 = r1.toLowerCase(r0)
            java.lang.String r4 = "\u001c\u0019\t\b\u000b\u0004\n\\"
            java.lang.String r4 = com.agilebinary.a.a.a.c.b.e.I(r4)
            int r0 = r0.indexOf(r4)
            if (r0 == r5) goto L_0x00bc
            java.lang.String r4 = "\u001dl\b}\nq\u000b)"
            java.lang.String r4 = org.osmdroid.b.a.e.I(r4)
            int r4 = r4.length()
            int r4 = r4 + r0
            r0 = 59
            int r0 = r1.indexOf(r0, r4)
            if (r0 != r5) goto L_0x0087
            int r0 = r1.length()
        L_0x0087:
            java.lang.String r0 = r1.substring(r4, r0)     // Catch:{ k -> 0x00bb }
            java.lang.String[] r1 = r6.c     // Catch:{ k -> 0x00bb }
            com.agilebinary.a.a.a.c.a.l.a(r0, r1)     // Catch:{ k -> 0x00bb }
            r0 = r2
        L_0x0091:
            if (r0 == 0) goto L_0x00e6
            boolean r0 = r7 instanceof com.agilebinary.a.a.a.r
            if (r0 == 0) goto L_0x00be
            r0 = r7
            com.agilebinary.a.a.a.r r0 = (com.agilebinary.a.a.a.r) r0
            com.agilebinary.a.a.a.i.c r1 = r0.e()
            com.agilebinary.a.a.a.b.i r0 = new com.agilebinary.a.a.a.b.i
            com.agilebinary.a.a.a.r r7 = (com.agilebinary.a.a.a.r) r7
            int r4 = r7.d()
            int r5 = r1.c()
            r0.<init>(r4, r5)
        L_0x00ad:
            com.agilebinary.a.a.a.m[] r2 = new com.agilebinary.a.a.a.m[r2]
            com.agilebinary.a.a.a.m r0 = com.agilebinary.a.a.a.c.a.aa.a(r1, r0)
            r2[r3] = r0
            r0 = r2
        L_0x00b6:
            java.util.List r0 = r6.a(r0, r8)
            return r0
        L_0x00bb:
            r0 = move-exception
        L_0x00bc:
            r0 = r3
            goto L_0x0091
        L_0x00be:
            java.lang.String r0 = r7.b()
            if (r0 != 0) goto L_0x00d0
            com.agilebinary.a.a.a.k.e r0 = new com.agilebinary.a.a.a.k.e
            java.lang.String r1 = "1\u0004\u0018\u0005\u001c\u0013Y\u0017\u0018\r\f\u0004Y\b\nA\u0017\u0014\u0015\r"
            java.lang.String r1 = com.agilebinary.a.a.a.c.b.e.I(r1)
            r0.<init>(r1)
            throw r0
        L_0x00d0:
            com.agilebinary.a.a.a.i.c r1 = new com.agilebinary.a.a.a.i.c
            int r4 = r0.length()
            r1.<init>(r4)
            r1.a(r0)
            com.agilebinary.a.a.a.b.i r0 = new com.agilebinary.a.a.a.b.i
            int r4 = r1.c()
            r0.<init>(r3, r4)
            goto L_0x00ad
        L_0x00e6:
            com.agilebinary.a.a.a.m[] r0 = r7.c()
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.a.w.a(com.agilebinary.a.a.a.t, com.agilebinary.a.a.a.k.f):java.util.List");
    }

    public final List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException(e.I("X\u0011g\f4\u0017rXw\u0017{\u0013}\u001dgXy\u0019mXz\u0017`Xv\u001d4\u0016a\u0014x"));
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.b.e.I("5\b\n\u0015Y\u000e\u001fA\u001a\u000e\u0016\n\u0010\u0004\nA\u0014\u0000\u0000A\u0017\u000e\rA\u001b\u0004Y\u0004\u0014\u0011\r\u0018"));
        } else {
            c cVar = new c(list.size() * 20);
            cVar.a(e.I(";{\u0017\u0011q"));
            cVar.a(com.agilebinary.a.a.a.c.b.e.I("CA"));
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    com.agilebinary.a.a.a.k.c cVar2 = (com.agilebinary.a.a.a.k.c) list.get(i2);
                    if (i2 > 0) {
                        cVar.a(e.I("C4"));
                    }
                    cVar.a(cVar2.a());
                    cVar.a(com.agilebinary.a.a.a.c.b.e.I("\\"));
                    String b2 = cVar2.b();
                    if (b2 != null) {
                        cVar.a(b2);
                    }
                    i = i2 + 1;
                } else {
                    ArrayList arrayList = new ArrayList(1);
                    arrayList.add(new com.agilebinary.a.a.a.b.e(cVar));
                    return arrayList;
                }
            }
        }
    }

    public final t b() {
        return null;
    }

    public final String toString() {
        return e.I("w\u0017y\bu\f}\u001a}\u0014}\fm");
    }
}
