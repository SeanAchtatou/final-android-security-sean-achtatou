package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.c.c.j;
import com.agilebinary.a.a.a.h.b;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.c.f;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.m;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public final class i implements k {

    /* renamed from: a  reason: collision with root package name */
    protected final m f48a;
    private final Log b = a.a(getClass());
    private h c;
    private boolean d;
    private f e;
    private c f;
    private long g;
    private long h;
    private volatile boolean i;

    public i(h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException(j.I("(\u0018\u0013\u001e\u0016\u001e[\t\u001e\u001c\u0012\b\u000f\t\u0002[\u0016\u000e\b\u000f[\u0015\u0014\u000f[\u0019\u001e[\u0015\u000e\u0017\u0017U"));
        }
        this.c = hVar;
        this.f48a = new b(hVar);
        this.e = new f(this);
        this.f = null;
        this.g = -1;
        this.d = false;
        this.i = false;
    }

    private /* synthetic */ void b() {
        if (this.i) {
            throw new IllegalStateException(com.agilebinary.a.a.a.k.a.I("~`]`TdA!Zr\u0013r[tG!WnDo\u001d"));
        }
    }

    private /* synthetic */ void c() {
        synchronized (this) {
            if (System.currentTimeMillis() >= this.h) {
                a(0, TimeUnit.MILLISECONDS);
            }
        }
    }

    private /* synthetic */ void d() {
        synchronized (this) {
            this.i = true;
            if (this.f != null) {
                this.f.j();
            }
            try {
                if (this.e != null) {
                    this.e.d();
                }
                this.e = null;
            } catch (IOException e2) {
                this.b.debug(j.I("+\t\u0014\u0019\u0017\u001e\u0016[\f\u0013\u0012\u0017\u001e[\b\u0013\u000e\u000f\u000f\u0012\u0015\u001c[\u001f\u0014\f\u0015[\u0016\u001a\u0015\u001a\u001c\u001e\tU"), e2);
                this.e = null;
            } catch (Throwable th) {
                this.e = null;
                throw th;
            }
        }
        return;
    }

    public final com.agilebinary.a.a.a.h.a a(c cVar) {
        boolean z;
        c cVar2;
        boolean z2 = false;
        boolean z3 = true;
        synchronized (this) {
            if (cVar == null) {
                throw new IllegalArgumentException(com.agilebinary.a.a.a.k.a.I("S\\tGd\u0013lRx\u0013o\\u\u0013cV!]t_m\u001d"));
            }
            b();
            if (this.b.isDebugEnabled()) {
                this.b.debug(j.I("<\u001e\u000f[\u0018\u0014\u0015\u0015\u001e\u0018\u000f\u0012\u0014\u0015[\u001d\u0014\t[\t\u0014\u000e\u000f\u001e[") + cVar);
            }
            if (this.f != null) {
                throw new IllegalStateException(com.agilebinary.a.a.a.k.a.I("H]wRmZe\u0013t@d\u0013nU!`h]f_dpmZd]upn]o~`]`TdA;\u0013b\\o]dPuZn]!@uZm_!Rm_nP`GdW/9LRjV!@tAd\u0013u\\!Ad_dRrV!GiV!Pn]oVbGh\\o\u0013cVg\\sV!Rm_nP`Gh]f\u0013`]nGiVs\u0013n]d\u001d"));
            }
            c();
            if (this.e.f35a.l()) {
                f fVar = this.e.c;
                z2 = fVar == null || !fVar.h().equals(cVar);
                z = false;
            } else {
                z = true;
            }
            if (z2) {
                try {
                    this.e.d();
                } catch (IOException e2) {
                    this.b.debug(j.I("+\t\u0014\u0019\u0017\u001e\u0016[\b\u0013\u000e\u000f\u000f\u0012\u0015\u001c[\u001f\u0014\f\u0015[\u0018\u0014\u0015\u0015\u001e\u0018\u000f\u0012\u0014\u0015U"), e2);
                }
            } else {
                z3 = z;
            }
            if (z3) {
                this.e = new f(this);
            }
            this.f = new c(this, this.e, cVar);
            cVar2 = this.f;
        }
        return cVar2;
    }

    public final h a() {
        return this.c;
    }

    public final b a(c cVar, Object obj) {
        return new d(this, cVar, obj);
    }

    public final void a(long j, TimeUnit timeUnit) {
        synchronized (this) {
            b();
            if (timeUnit == null) {
                throw new IllegalArgumentException(com.agilebinary.a.a.a.k.a.I("gh^d\u0013t]hG!^t@u\u0013o\\u\u0013cV!]t_m\u001d"));
            } else if (this.f == null && this.e.f35a.l()) {
                if (this.g <= System.currentTimeMillis() - timeUnit.toMillis(j)) {
                    try {
                        this.e.c();
                    } catch (IOException e2) {
                        this.b.debug(j.I("+\t\u0014\u0019\u0017\u001e\u0016[\u0018\u0017\u0014\b\u0012\u0015\u001c[\u0012\u001f\u0017\u001e[\u0018\u0014\u0015\u0015\u001e\u0018\u000f\u0012\u0014\u0015U"), e2);
                    }
                }
            }
        }
        return;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:45:0x00b5=Splitter:B:45:0x00b5, B:35:0x007f=Splitter:B:35:0x007f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.h.a r7, long r8, java.util.concurrent.TimeUnit r10) {
        /*
            r6 = this;
            r4 = 0
            monitor-enter(r6)
            r6.b()     // Catch:{ all -> 0x0016 }
            boolean r0 = r7 instanceof com.agilebinary.a.a.a.c.b.c     // Catch:{ all -> 0x0016 }
            if (r0 != 0) goto L_0x0019
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0016 }
            java.lang.String r1 = "pn]oVbGh\\o\u0013b_`@r\u0013lZr^`Gb[-\u0013b\\o]dPuZn]!]nG!\\cG`ZoVe\u0013gAn^!GiZr\u0013lRoRfVs\u001d"
            java.lang.String r1 = com.agilebinary.a.a.a.k.a.I(r1)     // Catch:{ all -> 0x0016 }
            r0.<init>(r1)     // Catch:{ all -> 0x0016 }
            throw r0     // Catch:{ all -> 0x0016 }
        L_0x0016:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0019:
            org.apache.commons.logging.Log r0 = r6.b     // Catch:{ all -> 0x0016 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0016 }
            if (r0 == 0) goto L_0x003d
            org.apache.commons.logging.Log r0 = r6.b     // Catch:{ all -> 0x0016 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0016 }
            r1.<init>()     // Catch:{ all -> 0x0016 }
            java.lang.String r2 = ")\u001e\u0017\u001e\u001a\b\u0012\u0015\u001c[\u0018\u0014\u0015\u0015\u001e\u0018\u000f\u0012\u0014\u0015["
            java.lang.String r2 = com.agilebinary.a.a.a.c.c.j.I(r2)     // Catch:{ all -> 0x0016 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0016 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0016 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0016 }
            r0.debug(r1)     // Catch:{ all -> 0x0016 }
        L_0x003d:
            com.agilebinary.a.a.a.c.b.c r7 = (com.agilebinary.a.a.a.c.b.c) r7     // Catch:{ all -> 0x0016 }
            com.agilebinary.a.a.a.c.b.a r0 = r7.f52a     // Catch:{ all -> 0x0016 }
            if (r0 != 0) goto L_0x0045
        L_0x0043:
            monitor-exit(r6)
            return
        L_0x0045:
            com.agilebinary.a.a.a.h.k r0 = r7.h()     // Catch:{ all -> 0x0016 }
            if (r0 == 0) goto L_0x0059
            if (r0 == r6) goto L_0x0059
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0016 }
            java.lang.String r1 = "B\\o]dPuZn]!]nG!\\cG`ZoVe\u0013gAn^!GiZr\u0013lRoRfVs\u001d"
            java.lang.String r1 = com.agilebinary.a.a.a.k.a.I(r1)     // Catch:{ all -> 0x0016 }
            r0.<init>(r1)     // Catch:{ all -> 0x0016 }
            throw r0     // Catch:{ all -> 0x0016 }
        L_0x0059:
            boolean r0 = r7.l()     // Catch:{ IOException -> 0x00a1 }
            if (r0 == 0) goto L_0x007f
            boolean r0 = r6.d     // Catch:{ IOException -> 0x00a1 }
            if (r0 != 0) goto L_0x0069
            boolean r0 = r7.r()     // Catch:{ IOException -> 0x00a1 }
            if (r0 != 0) goto L_0x007f
        L_0x0069:
            org.apache.commons.logging.Log r0 = r6.b     // Catch:{ IOException -> 0x00a1 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ IOException -> 0x00a1 }
            if (r0 == 0) goto L_0x007c
            org.apache.commons.logging.Log r0 = r6.b     // Catch:{ IOException -> 0x00a1 }
            java.lang.String r1 = ")\u001e\u0017\u001e\u001a\b\u001e\u001f[\u0018\u0014\u0015\u0015\u001e\u0018\u000f\u0012\u0014\u0015[\u0014\u000b\u001e\u0015[\u0019\u000e\u000f[\u0015\u0014\u000f[\t\u001e\u000e\b\u001a\u0019\u0017\u001eU"
            java.lang.String r1 = com.agilebinary.a.a.a.c.c.j.I(r1)     // Catch:{ IOException -> 0x00a1 }
            r0.debug(r1)     // Catch:{ IOException -> 0x00a1 }
        L_0x007c:
            r7.m()     // Catch:{ IOException -> 0x00a1 }
        L_0x007f:
            r7.j()     // Catch:{ all -> 0x0016 }
            r0 = 0
            r6.f = r0     // Catch:{ all -> 0x0016 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0016 }
            r6.g = r0     // Catch:{ all -> 0x0016 }
            int r0 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0099
            long r0 = r10.toMillis(r8)     // Catch:{ all -> 0x0016 }
            long r2 = r6.g     // Catch:{ all -> 0x0016 }
            long r0 = r0 + r2
            r6.h = r0     // Catch:{ all -> 0x0016 }
            goto L_0x0043
        L_0x0099:
            r0 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r6.h = r0     // Catch:{ all -> 0x0016 }
            goto L_0x0043
        L_0x00a1:
            r0 = move-exception
            org.apache.commons.logging.Log r1 = r6.b     // Catch:{ all -> 0x00d9 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x00d9 }
            if (r1 == 0) goto L_0x00b5
            org.apache.commons.logging.Log r1 = r6.b     // Catch:{ all -> 0x00d9 }
            java.lang.String r2 = "DKbVqGh\\o\u0013r[tGuZoT!WnDo\u0013sVmV`@dW!Pn]oVbGh\\o\u001d"
            java.lang.String r2 = com.agilebinary.a.a.a.k.a.I(r2)     // Catch:{ all -> 0x00d9 }
            r1.debug(r2, r0)     // Catch:{ all -> 0x00d9 }
        L_0x00b5:
            r7.j()     // Catch:{ all -> 0x0016 }
            r0 = 0
            r6.f = r0     // Catch:{ all -> 0x0016 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0016 }
            r6.g = r0     // Catch:{ all -> 0x0016 }
            int r0 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x00d0
            long r0 = r10.toMillis(r8)     // Catch:{ all -> 0x0016 }
            long r2 = r6.g     // Catch:{ all -> 0x0016 }
            long r0 = r0 + r2
            r6.h = r0     // Catch:{ all -> 0x0016 }
            goto L_0x0043
        L_0x00d0:
            r0 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r6.h = r0     // Catch:{ all -> 0x0016 }
            goto L_0x0043
        L_0x00d9:
            r0 = move-exception
            r7.j()     // Catch:{ all -> 0x0016 }
            r1 = 0
            r6.f = r1     // Catch:{ all -> 0x0016 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0016 }
            r6.g = r2     // Catch:{ all -> 0x0016 }
            int r1 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r1 <= 0) goto L_0x00f4
            long r2 = r10.toMillis(r8)     // Catch:{ all -> 0x0016 }
            long r4 = r6.g     // Catch:{ all -> 0x0016 }
            long r2 = r2 + r4
            r6.h = r2     // Catch:{ all -> 0x0016 }
        L_0x00f3:
            throw r0     // Catch:{ all -> 0x0016 }
        L_0x00f4:
            r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r6.h = r2     // Catch:{ all -> 0x0016 }
            goto L_0x00f3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.i.a(com.agilebinary.a.a.a.h.a, long, java.util.concurrent.TimeUnit):void");
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        try {
            d();
        } finally {
            super.finalize();
        }
    }
}
