package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.d.c;
import com.agilebinary.a.a.a.d.i;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f.b;
import com.agilebinary.a.a.a.f.f;
import com.agilebinary.a.a.a.f.j;
import com.agilebinary.a.a.a.h.c.h;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.n;
import com.agilebinary.a.a.a.s;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.b.a.a;
import com.agilebinary.mobilemonitor.client.a.b.t;
import java.net.URI;
import org.apache.commons.logging.Log;

public abstract class d {

    /* renamed from: a  reason: collision with root package name */
    private final Log f69a = a.a(getClass());
    private e b;
    private b c;
    private k d;
    private s e;
    private n f;
    private com.agilebinary.a.a.a.k.d g;
    private com.agilebinary.a.a.a.a.a h;
    private j i;
    private f j;
    private com.agilebinary.a.a.a.d.k k;
    private c l;
    private com.agilebinary.a.a.a.d.b m;
    private com.agilebinary.a.a.a.d.b n;
    private com.agilebinary.a.a.a.d.d o;
    private com.agilebinary.a.a.a.d.e p;
    private h q;
    private com.agilebinary.a.a.a.d.f r;

    protected d(k kVar, e eVar) {
        this.b = eVar;
        this.d = kVar;
    }

    private /* synthetic */ c A() {
        c cVar;
        synchronized (this) {
            if (this.l == null) {
                this.l = new p();
            }
            cVar = this.l;
        }
        return cVar;
    }

    private /* synthetic */ com.agilebinary.a.a.a.d.b B() {
        com.agilebinary.a.a.a.d.b bVar;
        synchronized (this) {
            if (this.m == null) {
                this.m = k();
            }
            bVar = this.m;
        }
        return bVar;
    }

    private /* synthetic */ com.agilebinary.a.a.a.d.b C() {
        com.agilebinary.a.a.a.d.b bVar;
        synchronized (this) {
            if (this.n == null) {
                this.n = l();
            }
            bVar = this.n;
        }
        return bVar;
    }

    private /* synthetic */ h D() {
        h hVar;
        synchronized (this) {
            if (this.q == null) {
                this.q = o();
            }
            hVar = this.q;
        }
        return hVar;
    }

    private /* synthetic */ com.agilebinary.a.a.a.d.f E() {
        com.agilebinary.a.a.a.d.f fVar;
        synchronized (this) {
            if (this.r == null) {
                this.r = p();
            }
            fVar = this.r;
        }
        return fVar;
    }

    private /* synthetic */ j F() {
        j jVar;
        synchronized (this) {
            if (this.i == null) {
                this.i = i();
            }
            jVar = this.i;
        }
        return jVar;
    }

    private final /* synthetic */ com.agilebinary.a.a.a.f.c G() {
        f fVar;
        synchronized (this) {
            if (this.j == null) {
                j F = F();
                int a2 = F.a();
                ab[] abVarArr = new ab[a2];
                for (int i2 = 0; i2 < a2; i2++) {
                    abVarArr[i2] = F.a(i2);
                }
                int b2 = F.b();
                ad[] adVarArr = new ad[b2];
                for (int i3 = 0; i3 < b2; i3++) {
                    adVarArr[i3] = F.b(i3);
                }
                this.j = new f(abVarArr, adVarArr);
            }
            fVar = this.j;
        }
        return fVar;
    }

    private /* synthetic */ com.agilebinary.a.a.a.j a(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar) {
        com.agilebinary.a.a.a.f.k b2;
        s sVar;
        if (fVar == null) {
            throw new IllegalArgumentException(u.I("\u0006@%P1V \u00059P'QtK;QtG1\u0005:P8Iz"));
        }
        synchronized (this) {
            b2 = b();
            sVar = new s(this.f69a, w(), r(), x(), y(), D(), G(), z(), A(), B(), C(), E(), new q(q(), fVar.g()));
        }
        try {
            return sVar.a(bVar, fVar, b2);
        } catch (com.agilebinary.a.a.a.k e2) {
            throw new i(e2);
        }
    }

    private /* synthetic */ b w() {
        b bVar;
        synchronized (this) {
            if (this.c == null) {
                this.c = c();
            }
            bVar = this.c;
        }
        return bVar;
    }

    private /* synthetic */ s x() {
        s sVar;
        synchronized (this) {
            if (this.e == null) {
                this.e = g();
            }
            sVar = this.e;
        }
        return sVar;
    }

    private /* synthetic */ n y() {
        n nVar;
        synchronized (this) {
            if (this.f == null) {
                this.f = h();
            }
            nVar = this.f;
        }
        return nVar;
    }

    private /* synthetic */ com.agilebinary.a.a.a.d.k z() {
        com.agilebinary.a.a.a.d.k kVar;
        synchronized (this) {
            if (this.k == null) {
                this.k = j();
            }
            kVar = this.k;
        }
        return kVar;
    }

    /* access modifiers changed from: protected */
    public abstract e a();

    public final com.agilebinary.a.a.a.j a(com.agilebinary.a.a.a.d.c.b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException(t.I("mhNxZ~K-RxLy\u001fcPy\u001foZ-QxSa\u0011"));
        }
        com.agilebinary.a.a.a.b bVar2 = null;
        URI e_ = bVar.e_();
        if (e_.isAbsolute()) {
            String schemeSpecificPart = e_.getSchemeSpecificPart();
            String substring = schemeSpecificPart.substring(2, schemeSpecificPart.length());
            String substring2 = substring.substring(0, substring.indexOf(58) > 0 ? substring.indexOf(58) : substring.indexOf(47) > 0 ? substring.indexOf(47) : substring.indexOf(63) > 0 ? substring.indexOf(63) : substring.length());
            int port = e_.getPort();
            String scheme = e_.getScheme();
            if (substring2 == null || "".equals(substring2)) {
                throw new i(u.I("p\u0006ltA;@'\u0005:J \u0005'U1F=C-\u00055\u0005\"D8L0\u0005<J'QtK5H1\u001ft") + e_);
            }
            bVar2 = new com.agilebinary.a.a.a.b(substring2, port, scheme);
        }
        return a(bVar2, bVar);
    }

    public final void a(ab abVar) {
        synchronized (this) {
            F().a(abVar);
            this.j = null;
        }
    }

    public final void a(ad adVar) {
        synchronized (this) {
            F().a(adVar);
            this.j = null;
        }
    }

    public final void a(c cVar) {
        synchronized (this) {
            this.l = cVar;
        }
    }

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.f.k b();

    /* access modifiers changed from: protected */
    public abstract b c();

    /* access modifiers changed from: protected */
    public abstract k d();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.a.a e();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.k.d f();

    /* access modifiers changed from: protected */
    public abstract s g();

    /* access modifiers changed from: protected */
    public abstract n h();

    /* access modifiers changed from: protected */
    public abstract j i();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.k j();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.b k();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.b l();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.d m();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.e n();

    /* access modifiers changed from: protected */
    public abstract h o();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.f p();

    public final e q() {
        e eVar;
        synchronized (this) {
            if (this.b == null) {
                this.b = a();
            }
            eVar = this.b;
        }
        return eVar;
    }

    public final k r() {
        k kVar;
        synchronized (this) {
            if (this.d == null) {
                this.d = d();
            }
            kVar = this.d;
        }
        return kVar;
    }

    public final com.agilebinary.a.a.a.a.a s() {
        com.agilebinary.a.a.a.a.a aVar;
        synchronized (this) {
            if (this.h == null) {
                this.h = e();
            }
            aVar = this.h;
        }
        return aVar;
    }

    public final com.agilebinary.a.a.a.k.d t() {
        com.agilebinary.a.a.a.k.d dVar;
        synchronized (this) {
            if (this.g == null) {
                this.g = f();
            }
            dVar = this.g;
        }
        return dVar;
    }

    public final com.agilebinary.a.a.a.d.d u() {
        com.agilebinary.a.a.a.d.d dVar;
        synchronized (this) {
            if (this.o == null) {
                this.o = m();
            }
            dVar = this.o;
        }
        return dVar;
    }

    public final com.agilebinary.a.a.a.d.e v() {
        com.agilebinary.a.a.a.d.e eVar;
        synchronized (this) {
            if (this.p == null) {
                this.p = n();
            }
            eVar = this.p;
        }
        return eVar;
    }
}
