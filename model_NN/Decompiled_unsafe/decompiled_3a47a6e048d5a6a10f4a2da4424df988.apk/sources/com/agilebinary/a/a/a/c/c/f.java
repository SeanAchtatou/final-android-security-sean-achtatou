package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.b.s;
import com.agilebinary.a.a.a.b.t;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.d;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.x;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.osmdroid.util.c;

public abstract class f implements d {

    /* renamed from: a  reason: collision with root package name */
    protected final t f61a;
    private final a b;
    private final int c;
    private final int d;
    private final List e;
    private int f;
    private x g;

    public f(a aVar, e eVar) {
        if (aVar == null) {
            throw new IllegalArgumentException(c.I("5B\u0015T\u000fH\b\u0007\u000fI\u0016R\u0012\u0007\u0004R\u0000A\u0003UFJ\u0007^FI\tSFE\u0003\u0007\bR\nK"));
        } else if (eVar == null) {
            throw new IllegalArgumentException(org.b.a.a.c.I("\u0007x\u001b|o\\.^.A*X*^<\f\"M6\f!C;\f-IoB:@#"));
        } else {
            this.b = aVar;
            this.c = eVar.a(c.I("\u000eS\u0012WHD\tI\bB\u0005S\u000fH\b\t\u000bF\u001e\n\u000eB\u0007C\u0003UKD\tR\bS"), -1);
            this.d = eVar.a(org.b.a.a.c.I("'X;\\aO B!I,X&C!\u0002\"M7\u0001#E!Ib@*B(X'"), -1);
            this.f61a = s.f21a;
            this.e = new ArrayList();
            this.f = 0;
        }
    }

    public static com.agilebinary.a.a.a.t[] a(a aVar, int i, int i2, t tVar, List list) {
        com.agilebinary.a.a.a.i.c cVar;
        int i3 = 0;
        if (aVar == null) {
            throw new IllegalArgumentException(c.I("5B\u0015T\u000fH\b\u0007\u000fI\u0016R\u0012\u0007\u0004R\u0000A\u0003UFJ\u0007^FI\tSFE\u0003\u0007\bR\nK"));
        } else if (tVar == null) {
            throw new IllegalArgumentException(org.b.a.a.c.I("\u0003E!Io\\.^<I=\f\"M6\f!C;\f-IoB:@#"));
        } else if (list == null) {
            throw new IllegalArgumentException(c.I(".B\u0007C\u0003UFK\u000fI\u0003\u0007\nN\u0015SFJ\u0007^FI\tSFE\u0003\u0007\bR\nK"));
        } else {
            com.agilebinary.a.a.a.i.c cVar2 = null;
            com.agilebinary.a.a.a.i.c cVar3 = null;
            while (true) {
                if (cVar3 == null) {
                    cVar3 = new com.agilebinary.a.a.a.i.c(64);
                } else {
                    cVar3.a();
                }
                if (aVar.a(cVar3) == -1 || cVar3.c() <= 0) {
                    com.agilebinary.a.a.a.t[] tVarArr = new com.agilebinary.a.a.a.t[list.size()];
                } else {
                    if ((cVar3.a(0) == ' ' || cVar3.a(0) == 9) && cVar2 != null) {
                        int i4 = 0;
                        while (i4 < cVar3.c() && ((r5 = cVar3.a(i4)) == ' ' || r5 == 9)) {
                            i4++;
                        }
                        if (i2 <= 0 || ((cVar2.c() + 1) + cVar3.c()) - i4 <= i2) {
                            cVar2.a(' ');
                            cVar2.a(cVar3, i4, cVar3.c() - i4);
                            cVar = cVar3;
                            cVar3 = cVar2;
                        } else {
                            throw new IOException(org.b.a.a.c.I("a.T&A:Ao@&B*\f#I!K;Do@&A&XoI7O*I+I+"));
                        }
                    } else {
                        list.add(cVar3);
                        cVar = null;
                    }
                    if (i <= 0 || list.size() < i) {
                        cVar2 = cVar3;
                        cVar3 = cVar;
                    } else {
                        throw new IOException(c.I("j\u0007_\u000fJ\u0013JFO\u0003F\u0002B\u0014\u0007\u0005H\u0013I\u0012\u0007\u0003_\u0005B\u0003C\u0003C"));
                    }
                }
            }
            com.agilebinary.a.a.a.t[] tVarArr2 = new com.agilebinary.a.a.a.t[list.size()];
            while (i3 < list.size()) {
                try {
                    tVarArr2[i3] = tVar.a((com.agilebinary.a.a.a.i.c) list.get(i3));
                    i3++;
                } catch (u e2) {
                    throw new l(e2.getMessage());
                }
            }
            return tVarArr2;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final x a() {
        switch (this.f) {
            case 0:
                try {
                    this.g = a(this.b);
                    this.f = 1;
                    break;
                } catch (u e2) {
                    throw new l(e2.getMessage(), e2);
                }
            case 1:
                break;
            default:
                throw new IllegalStateException(org.b.a.a.c.I("\u0006B,C!_&_;I!Xo\\.^<I=\f<X.X*"));
        }
        this.g.a(a(this.b, this.c, this.d, this.f61a, this.e));
        x xVar = this.g;
        this.g = null;
        this.e.clear();
        this.f = 0;
        return xVar;
    }

    /* access modifiers changed from: protected */
    public abstract x a(a aVar);
}
