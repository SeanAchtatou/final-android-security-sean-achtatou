package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.k;

public class r implements k {
    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.b.b.I(",\u0019\u0000\u001d\u0006\u0013O\u001b\u000e\u000fO\u0018\u0000\u0002O\u0014\nV\u0001\u0003\u0003\u001a"));
        } else if (str == null) {
            throw new e(com.agilebinary.a.a.a.c.b.e.I("4\b\n\u0012\u0010\u000f\u001eA\u000f\u0000\u0015\u0014\u001cA\u001f\u000e\u000bA\u001d\u000e\u0014\u0000\u0010\u000fY\u0000\r\u0015\u000b\b\u001b\u0014\r\u0004"));
        } else if (str.trim().length() == 0) {
            throw new e(com.agilebinary.a.a.a.b.b.I("-\u001a\u000e\u0018\u0004V\u0019\u0017\u0003\u0003\nV\t\u0019\u001dV\u000b\u0019\u0002\u0017\u0006\u0018O\u0017\u001b\u0002\u001d\u001f\r\u0003\u001b\u0013"));
        } else {
            bVar.b(str);
        }
    }

    public void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.b.e.I(":\u000e\u0016\n\u0010\u0004Y\f\u0018\u0018Y\u000f\u0016\u0015Y\u0003\u001cA\u0017\u0014\u0015\r"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.b.b.I("5\u0000\u0019\u0004\u001f\nV\u0000\u0004\u0006\u0011\u0006\u0018O\u001b\u000e\u000fO\u0018\u0000\u0002O\u0014\nV\u0001\u0003\u0003\u001a"));
        } else {
            String a2 = fVar.a();
            String c = cVar.c();
            if (c == null) {
                throw new i(com.agilebinary.a.a.a.c.b.e.I("\"\u0016\u000e\u0012\b\u001cA\u001d\u000e\u0014\u0000\u0010\u000fY\f\u0018\u0018Y\u000f\u0016\u0015Y\u0003\u001cA\u0017\u0014\u0015\r"));
            } else if (a2.contains(com.agilebinary.a.a.a.b.b.I("X"))) {
                if (!a2.endsWith(c)) {
                    if (c.startsWith(com.agilebinary.a.a.a.c.b.e.I("O"))) {
                        c = c.substring(1, c.length());
                    }
                    if (!a2.equals(c)) {
                        throw new i(com.agilebinary.a.a.a.b.b.I("&\u001a\u0003\u0013\b\u0017\u0003V\u000b\u0019\u0002\u0017\u0006\u0018O\u0017\u001b\u0002\u001d\u001f\r\u0003\u001b\u0013OT") + c + com.agilebinary.a.a.a.c.b.e.I("[OY%\u0016\f\u0018\b\u0017A\u0016\u0007Y\u000e\u000b\b\u001e\b\u0017[YC") + a2 + com.agilebinary.a.a.a.b.b.I("T"));
                    }
                }
            } else if (!a2.equals(c)) {
                throw new i(com.agilebinary.a.a.a.c.b.e.I("0\r\u0015\u0004\u001e\u0000\u0015A\u001d\u000e\u0014\u0000\u0010\u000fY\u0000\r\u0015\u000b\b\u001b\u0014\r\u0004YC") + c + com.agilebinary.a.a.a.b.b.I("MXO2\u0000\u001b\u000e\u001f\u0001V\u0000\u0010O\u0019\u001d\u001f\b\u001f\u0001LOT") + a2 + com.agilebinary.a.a.a.c.b.e.I("C"));
            }
        }
    }

    public boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.b.b.I(",\u0019\u0000\u001d\u0006\u0013O\u001b\u000e\u000fO\u0018\u0000\u0002O\u0014\nV\u0001\u0003\u0003\u001a"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.b.e.I("\"\u0016\u000e\u0012\b\u001cA\u0016\u0013\u0010\u0006\u0010\u000fY\f\u0018\u0018Y\u000f\u0016\u0015Y\u0003\u001cA\u0017\u0014\u0015\r"));
        } else {
            String a2 = fVar.a();
            String c = cVar.c();
            if (c == null) {
                return false;
            }
            if (a2.equals(c)) {
                return true;
            }
            if (!c.startsWith(com.agilebinary.a.a.a.b.b.I("X"))) {
                c = '.' + c;
            }
            return a2.endsWith(c) || a2.equals(c.substring(1));
        }
    }
}
