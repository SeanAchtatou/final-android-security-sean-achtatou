package com.agilebinary.a.a.a.c.e;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.c.c.e;
import com.agilebinary.a.a.a.c.c.m;
import com.agilebinary.a.a.a.c.c.o;
import com.agilebinary.a.a.a.x;
import com.agilebinary.mobilemonitor.client.a.b.t;
import com.agilebinary.mobilemonitor.client.android.a.a.g;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final com.agilebinary.a.a.a.g.a f81a;

    public a(com.agilebinary.a.a.a.g.a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException(g.I("\\_qDz^k\u0010sUqWkX?CkB~DzWf\u0010rQf\u0010q_k\u0010}U?^j\\s"));
        }
        this.f81a = aVar;
    }

    public final c a(com.agilebinary.a.a.a.j.a aVar, x xVar) {
        if (aVar == null) {
            throw new IllegalArgumentException(t.I("^Z~LdPc\u001fdQ}Jy\u001foJkYhM-RlF-QbK-]h\u001fcJaS"));
        } else if (xVar == null) {
            throw new IllegalArgumentException(g.I("xKdO\u0010rUlC~Wz\u0010rQf\u0010q_k\u0010}U?^j\\s"));
        } else {
            com.agilebinary.a.a.a.g.c cVar = new com.agilebinary.a.a.a.g.c();
            long a2 = this.f81a.a(xVar);
            if (a2 == -2) {
                cVar.a(true);
                cVar.a(-1);
                cVar.a(new e(aVar));
            } else if (a2 == -1) {
                cVar.a(false);
                cVar.a(-1);
                cVar.a(new o(aVar));
            } else {
                cVar.a(false);
                cVar.a(a2);
                cVar.a(new m(aVar, a2));
            }
            com.agilebinary.a.a.a.t c = xVar.c(t.I("NPcKhQy\u0012YF}Z"));
            if (c != null) {
                cVar.a(c);
            }
            com.agilebinary.a.a.a.t c2 = xVar.c(g.I("sp^kUqD2uqSpTv^x"));
            if (c2 != null) {
                cVar.b(c2);
            }
            return cVar;
        }
    }
}
