package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.d;
import com.agilebinary.a.a.a.e.c;
import java.io.Serializable;

public final class q implements d, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final a f19a;
    private final String b;
    private final String c;

    public q(String str, String str2, a aVar) {
        if (str == null) {
            throw new IllegalArgumentException(c.I("ED|IgE(L}R|\u0001fN|\u0001jD(O}Md\u000f"));
        } else if (str2 == null) {
            throw new IllegalArgumentException(org.osmdroid.c.c.I("0\u0012,`\b5\u00164E.\n4E\"\u0000`\u000b5\t,K"));
        } else if (aVar == null) {
            throw new IllegalArgumentException(c.I("XSgUgBgM(WmS{HgO(L}R|\u0001fN|\u0001jD(O}Md\u000f"));
        } else {
            this.b = str;
            this.c = str2;
            this.f19a = aVar;
        }
    }

    public final String a() {
        return this.b;
    }

    public final a b() {
        return this.f19a;
    }

    public final String c() {
        return this.c;
    }

    public final Object clone() {
        return super.clone();
    }

    public final String toString() {
        return r.f20a.a((com.agilebinary.a.a.a.i.c) null, this).toString();
    }
}
