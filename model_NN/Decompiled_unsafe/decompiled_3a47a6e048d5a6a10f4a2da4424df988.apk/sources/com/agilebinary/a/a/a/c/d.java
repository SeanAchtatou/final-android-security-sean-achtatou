package com.agilebinary.a.a.a.c;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.d.a.b;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.q;
import com.agilebinary.a.a.a.s;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.w;
import org.osmdroid.b.a.e;

public final class d implements s {
    public final boolean a(j jVar, k kVar) {
        if (jVar == null) {
            throw new IllegalArgumentException(b.I("/\u00103\u0014G6\u00027\u0017+\t7\u0002d\n%\u001ed\t+\u0013d\u0005!G*\u0012(\u000bj"));
        } else if (kVar == null) {
            throw new IllegalArgumentException(e.I("\\,@(4\u001b{\u0016`\u001dl\f4\u0015u\u00014\u0016{\f4\u001aqXz\rx\u0014:"));
        } else {
            q qVar = (q) kVar.a(b.I(",\u00130\u0017j\u0004+\t*\u0002'\u0013-\b*"));
            if (qVar != null && !qVar.l()) {
                return false;
            }
            c b = jVar.b();
            a a2 = jVar.a().a();
            if (b != null && b.c() < 0 && (!b.k_() || a2.a(aa.c))) {
                return false;
            }
            w d = jVar.d(e.I(";{\u0016z\u001dw\f}\u0017z"));
            if (!d.hasNext()) {
                d = jVar.d(b.I("76\b<\u001ei$+\t*\u0002'\u0013-\b*"));
            }
            if (d.hasNext()) {
                try {
                    com.agilebinary.a.a.a.b.d dVar = new com.agilebinary.a.a.a.b.d(d);
                    boolean z = false;
                    while (dVar.hasNext()) {
                        String a3 = dVar.a();
                        if (e.I("W\u0014{\u000bq").equalsIgnoreCase(a3)) {
                            return false;
                        }
                        if (b.I(",!\u00024J\u0005\u000b-\u0011!").equalsIgnoreCase(a3)) {
                            z = true;
                        }
                    }
                    if (z) {
                        return true;
                    }
                } catch (u e) {
                    return false;
                }
            }
            return !a2.a(aa.c);
        }
    }
}
