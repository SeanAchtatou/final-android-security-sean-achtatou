package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.mobilemonitor.client.android.ui.a.e;
import org.c.a.a.a.a;

public final class f extends x {
    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException(e.I(")b\u0005f\u0003hJ`\u000btJc\u0005yJo\u000f-\u0004x\u0006a"));
        } else if (str == null) {
            throw new com.agilebinary.a.a.a.k.e(a.I("`f^|DaJ/[nAzH/K`_/[j_|D`C/L{Y}DmX{H"));
        } else if (str.trim().length() == 0) {
            throw new com.agilebinary.a.a.a.k.e(e.I("O\u0006l\u0004fJ{\u000ba\u001fhJk\u0005J{\u000f\u0019d\u0005cJl\u001ey\u0018d\bx\u001eh"));
        } else {
            try {
                bVar.a(Integer.parseInt(str));
            } catch (NumberFormatException e) {
                throw new com.agilebinary.a.a.a.k.e(a.I("da[nAfI/[j_|D`C5\r") + e.getMessage());
            }
        }
    }

    public final void a(c cVar, com.agilebinary.a.a.a.k.f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(e.I(")b\u0005f\u0003hJ`\u000btJc\u0005yJo\u000f-\u0004x\u0006a"));
        } else if (cVar.h() < 0) {
            throw new i(a.I("LB`FfH/[j_|D`C/@nT/C`Y/Oj\raHhL{DyH"));
        }
    }
}
