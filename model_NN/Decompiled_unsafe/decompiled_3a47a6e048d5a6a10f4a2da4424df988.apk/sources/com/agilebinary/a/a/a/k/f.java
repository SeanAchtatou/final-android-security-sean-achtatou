package com.agilebinary.a.a.a.k;

import com.agilebinary.a.a.a.d.a.b;
import java.util.Locale;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private final String f119a;
    private final int b;
    private final String c;
    private final boolean d;

    public f(String str, int i, String str2, boolean z) {
        if (str == null) {
            throw new IllegalArgumentException(b.I("/+\u00140G+\u0001d\b6\u000e#\u000e*G)\u0006=G*\b0G&\u0002d\t1\u000b("));
        } else if (str.trim().length() == 0) {
            throw new IllegalArgumentException(I("~$E?\u0016$PkY9_,_%\u0016&W2\u0016%Y?\u0016)SkT'W%]"));
        } else if (i < 0) {
            throw new IllegalArgumentException(b.I(".*\u0011%\u000b-\u0003d\u0017+\u00150]d") + i);
        } else if (str2 == null) {
            throw new IllegalArgumentException(I("f*B#\u0016$PkY9_,_%\u0016&W2\u0016%Y?\u0016)SkX>Z'\u0018"));
        } else {
            this.f119a = str.toLowerCase(Locale.ENGLISH);
            this.b = i;
            if (str2.trim().length() != 0) {
                this.c = str2;
            } else {
                this.c = "/";
            }
            this.d = z;
        }
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ '6');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'K');
            i2 = i;
        }
        return new String(cArr);
    }

    public final String a() {
        return this.f119a;
    }

    public final String b() {
        return this.c;
    }

    public final int c() {
        return this.b;
    }

    public final boolean d() {
        return this.d;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        if (this.d) {
            sb.append(b.I("O7\u0002'\u00126\u0002m"));
        }
        sb.append(this.f119a);
        sb.append(':');
        sb.append(Integer.toString(this.b));
        sb.append(this.c);
        sb.append(']');
        return sb.toString();
    }
}
