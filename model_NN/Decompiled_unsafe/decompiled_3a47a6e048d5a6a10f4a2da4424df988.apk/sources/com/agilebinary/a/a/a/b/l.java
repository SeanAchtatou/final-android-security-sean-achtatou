package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.e.a;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import com.agilebinary.a.a.a.x;

public abstract class l implements x {

    /* renamed from: a  reason: collision with root package name */
    protected b f15a;
    protected e b;

    protected l() {
        this((byte) 0);
    }

    private /* synthetic */ l(byte b2) {
        this.f15a = new b();
        this.b = null;
    }

    public final void a(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException(b.I(">;\"?V\u001f\u0017\u001d\u0017\u0002\u0013\u001b\u0013\u001d\u0005O\u001b\u000e\u000fO\u0018\u0000\u0002O\u0014\nV\u0001\u0003\u0003\u001a"));
        }
        this.b = eVar;
    }

    public final void a(t tVar) {
        this.f15a.a(tVar);
    }

    public final void a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException(com.agilebinary.mobilemonitor.client.android.ui.a.e.I("E\u000fl\u000eh\u0018-\u0004l\u0007hJ`\u000btJc\u0005yJo\u000f-\u0004x\u0006a"));
        }
        this.f15a.a(new h(str, str2));
    }

    public final void a(t[] tVarArr) {
        this.f15a.a(tVarArr);
    }

    public final boolean a(String str) {
        return this.f15a.c(str);
    }

    public final void b(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException(b.I(">\n\u0017\u000b\u0013\u001dV\u0001\u0017\u0002\u0013O\u001b\u000e\u000fO\u0018\u0000\u0002O\u0014\nV\u0001\u0003\u0003\u001a"));
        }
        this.f15a.b(new h(str, str2));
    }

    public final t[] b(String str) {
        return this.f15a.a(str);
    }

    public final t c(String str) {
        return this.f15a.b(str);
    }

    public final w d(String str) {
        return this.f15a.d(str);
    }

    public final t[] e() {
        return this.f15a.b();
    }

    public final w f() {
        return this.f15a.c();
    }

    public final e g() {
        if (this.b == null) {
            this.b = new a();
        }
        return this.b;
    }
}
