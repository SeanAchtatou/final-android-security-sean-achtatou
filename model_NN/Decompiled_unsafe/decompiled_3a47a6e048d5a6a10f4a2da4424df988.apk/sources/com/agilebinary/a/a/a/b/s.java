package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.f.d;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.k;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.u;

public final class s implements t {

    /* renamed from: a  reason: collision with root package name */
    public static final s f21a = new s();
    private a b;

    public s() {
        this((byte) 0);
    }

    private /* synthetic */ s(byte b2) {
        this.b = aa.d;
    }

    private /* synthetic */ a c(c cVar, i iVar) {
        boolean z = true;
        if (cVar == null) {
            throw new IllegalArgumentException(k.I(" 9\u0002#C0\u0011#\u0002(C3\u00167\u00054\u0011q\u000e0\u001aq\r>\u0017q\u00014C?\u0016=\u000f"));
        } else if (iVar == null) {
            throw new IllegalArgumentException(com.jasonkostempski.android.calendar.a.I("{AYSNR\u000bC^RXOY\u0000FAR\u0000EO_\u0000IE\u000bN^LG"));
        } else {
            String a2 = this.b.a();
            int length = a2.length();
            int b2 = iVar.b();
            int a3 = iVar.a();
            d(cVar, iVar);
            int b3 = iVar.b();
            if (b3 + length + 4 > a3) {
                throw new u(k.I("\u001f\f%C0C'\u0002=\n5C!\u0011>\u0017>\u0000>\u000fq\u00154\u0011\"\n>\rkC") + cVar.a(b2, a3));
            }
            int i = 0;
            boolean z2 = true;
            while (z2 && i < length) {
                z2 = cVar.a(b3 + i) == a2.charAt(i);
                i++;
            }
            if (!z2) {
                z = z2;
            } else if (cVar.a(b3 + length) != '/') {
                z = false;
            }
            if (!z) {
                throw new u(com.jasonkostempski.android.calendar.a.I("nDT\u000bA\u000bVJLBD\u000bPYO_OHOG\u0000]EYSBOE\u001a\u000b") + cVar.a(b2, a3));
            }
            int i2 = length + 1 + b3;
            int a4 = cVar.a(46, i2, a3);
            if (a4 == -1) {
                throw new u(k.I("*?\u00150\u000f8\u0007q\u0013#\f%\f2\f=C'\u0006#\u00108\f?C?\u0016<\u00014\u0011kC") + cVar.a(b2, a3));
            }
            try {
                int parseInt = Integer.parseInt(cVar.b(i2, a4));
                int i3 = a4 + 1;
                int a5 = cVar.a(32, i3, a3);
                if (a5 == -1) {
                    a5 = a3;
                }
                try {
                    int parseInt2 = Integer.parseInt(cVar.b(i3, a5));
                    iVar.a(a5);
                    return this.b.a(parseInt, parseInt2);
                } catch (NumberFormatException e) {
                    throw new u(k.I("*?\u00150\u000f8\u0007q\u0013#\f%\f2\f=C<\n?\f#C'\u0006#\u00108\f?C?\u0016<\u00014\u0011kC") + cVar.a(b2, a3));
                }
            } catch (NumberFormatException e2) {
                throw new u(com.jasonkostempski.android.calendar.a.I("bN]AGIO\u0000[RDTDCDL\u000bMJJDR\u000bVNRXIDN\u000bN^MIEY\u001a\u000b") + cVar.a(b2, a3));
            }
        }
    }

    private static /* synthetic */ void d(c cVar, i iVar) {
        int b2 = iVar.b();
        int a2 = iVar.a();
        while (b2 < a2 && d.a(cVar.a(b2))) {
            b2++;
        }
        iVar.a(b2);
    }

    public final t a(c cVar) {
        return new e(cVar);
    }

    public final boolean a(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(com.jasonkostempski.android.calendar.a.I("hHJR\u000bAYRJY\u000bB^FMEY\u0000FAR\u0000EO_\u0000IE\u000bN^LG"));
        } else if (iVar == null) {
            throw new IllegalArgumentException(k.I("30\u0011\"\u0006#C2\u0016#\u0010>\u0011q\u000e0\u001aq\r>\u0017q\u00014C?\u0016=\u000f"));
        } else {
            int b2 = iVar.b();
            String a2 = this.b.a();
            int length = a2.length();
            if (cVar.c() < length + 4) {
                return false;
            }
            if (b2 < 0) {
                b2 = (cVar.c() - 4) - length;
            } else if (b2 == 0) {
                while (b2 < cVar.c() && d.a(cVar.a(b2))) {
                    b2++;
                }
            }
            if (b2 + length + 4 > cVar.c()) {
                return false;
            }
            boolean z = true;
            int i = 0;
            while (z && i < length) {
                z = cVar.a(b2 + i) == a2.charAt(i);
                i++;
            }
            return z ? cVar.a(b2 + length) == '/' : z;
        }
    }

    public final h b(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(com.jasonkostempski.android.calendar.a.I("hHJR\u000bAYRJY\u000bB^FMEY\u0000FAR\u0000EO_\u0000IE\u000bN^LG"));
        } else if (iVar == null) {
            throw new IllegalArgumentException(k.I("30\u0011\"\u0006#C2\u0016#\u0010>\u0011q\u000e0\u001aq\r>\u0017q\u00014C?\u0016=\u000f"));
        } else {
            int b2 = iVar.b();
            int a2 = iVar.a();
            try {
                a c = c(cVar, iVar);
                d(cVar, iVar);
                int b3 = iVar.b();
                int a3 = cVar.a(32, b3, a2);
                int i = a3 < 0 ? a2 : a3;
                String b4 = cVar.b(b3, i);
                for (int i2 = 0; i2 < b4.length(); i2++) {
                    if (!Character.isDigit(b4.charAt(i2))) {
                        throw new u(com.jasonkostempski.android.calendar.a.I("s_A_UX\u0000GIEE\u000bCDN_ABNX\u0000BN]AGIO\u0000XTJT^S\u000bCDDN\u001a\u000b") + cVar.a(b2, a2));
                    }
                }
                return new m(c, Integer.parseInt(b4), i < a2 ? cVar.b(i, a2) : "");
            } catch (NumberFormatException e) {
                throw new u(com.jasonkostempski.android.calendar.a.I("s_A_UX\u0000GIEE\u000bCDN_ABNX\u0000BN]AGIO\u0000XTJT^S\u000bCDDN\u001a\u000b") + cVar.a(b2, a2));
            } catch (IndexOutOfBoundsException e2) {
                throw new u(k.I("*?\u00150\u000f8\u0007q\u0010%\u0002%\u0016\"C=\n?\u0006kC") + cVar.a(b2, a2));
            }
        }
    }
}
