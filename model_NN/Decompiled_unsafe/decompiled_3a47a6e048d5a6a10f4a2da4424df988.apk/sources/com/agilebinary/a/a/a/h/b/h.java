package com.agilebinary.a.a.a.h.b;

import com.agilebinary.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.ui.a.e;
import java.util.concurrent.ConcurrentHashMap;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f104a = new ConcurrentHashMap();

    public final g a(b bVar) {
        if (bVar != null) {
            return a(bVar.c());
        }
        throw new IllegalArgumentException(e.I("\"b\u0019yJ`\u001f~\u001e-\u0004b\u001e-\bhJc\u001fa\u0006#"));
    }

    public final g a(g gVar) {
        if (gVar != null) {
            return (g) this.f104a.put(gVar.c(), gVar);
        }
        throw new IllegalArgumentException(com.agilebinary.a.a.a.c.b.e.I("*\u0002\u0011\u0004\u0014\u0004Y\f\f\u0012\rA\u0017\u000e\rA\u001b\u0004Y\u000f\f\r\u0015O"));
    }

    public final g a(String str) {
        g b = b(str);
        if (b != null) {
            return b;
        }
        throw new IllegalStateException(e.I("9n\u0002h\u0007hJ*") + str + com.agilebinary.a.a.a.c.b.e.I("FY\u000f\u0016\u0015Y\u0013\u001c\u0006\u0010\u0012\r\u0004\u000b\u0004\u001dO"));
    }

    public final g b(String str) {
        if (str != null) {
            return (g) this.f104a.get(str);
        }
        throw new IllegalArgumentException(e.I("$l\u0007hJ`\u001f~\u001e-\u0004b\u001e-\bhJc\u001fa\u0006#"));
    }
}
