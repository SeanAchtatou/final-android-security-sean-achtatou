package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.h.c.c;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    private l f72a;
    private c b;

    public m(l lVar, c cVar) {
        this.f72a = lVar;
        this.b = cVar;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'O');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '1');
            i2 = i;
        }
        return new String(cArr);
    }

    public final l a() {
        return this.f72a;
    }

    public final c b() {
        return this.b;
    }
}
