package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.a;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class ah extends u {
    protected static String a(f fVar) {
        String b = fVar.b();
        int lastIndexOf = b.lastIndexOf(47);
        if (lastIndexOf < 0) {
            return b;
        }
        if (lastIndexOf == 0) {
            lastIndexOf = 1;
        }
        return b.substring(0, lastIndexOf);
    }

    /* access modifiers changed from: protected */
    public List a(m[] mVarArr, f fVar) {
        ArrayList arrayList = new ArrayList(mVarArr.length);
        for (m mVar : mVarArr) {
            String a2 = mVar.a();
            String b = mVar.b();
            if (a2 == null || a2.length() == 0) {
                throw new e(com.agilebinary.a.a.a.c.b.e.I(":\u000e\u0016\n\u0010\u0004Y\u000f\u0018\f\u001cA\u0014\u0000\u0000A\u0017\u000e\rA\u001b\u0004Y\u0004\u0014\u0011\r\u0018"));
            }
            d dVar = new d(a2, b);
            dVar.c(a(fVar));
            dVar.b(fVar.a());
            o[] c = mVar.c();
            for (int length = c.length - 1; length >= 0; length--) {
                o oVar = c[length];
                String lowerCase = oVar.a().toLowerCase(Locale.ENGLISH);
                dVar.a(lowerCase, oVar.b());
                k a3 = a(lowerCase);
                if (a3 != null) {
                    a3.a(dVar, oVar.b());
                }
            }
            arrayList.add(dVar);
        }
        return arrayList;
    }

    public void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(a.I("B\\nXhV!^`J!]nG!Qd\u0013oFm_"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.b.e.I("\"\u0016\u000e\u0012\b\u001cA\u0016\u0013\u0010\u0006\u0010\u000fY\f\u0018\u0018Y\u000f\u0016\u0015Y\u0003\u001cA\u0017\u0014\u0015\r"));
        } else {
            for (k a2 : c()) {
                a2.a(cVar, fVar);
            }
        }
    }

    public boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(a.I("B\\nXhV!^`J!]nG!Qd\u0013oFm_"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.b.e.I("\"\u0016\u000e\u0012\b\u001cA\u0016\u0013\u0010\u0006\u0010\u000fY\f\u0018\u0018Y\u000f\u0016\u0015Y\u0003\u001cA\u0017\u0014\u0015\r"));
        } else {
            for (k b : c()) {
                if (!b.b(cVar, fVar)) {
                    return false;
                }
            }
            return true;
        }
    }
}
