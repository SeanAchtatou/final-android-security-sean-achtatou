package com.agilebinary.a.a.a.c.e;

import com.agilebinary.a.a.a.c.b.a.j;
import com.agilebinary.a.a.a.c.c.g;
import com.agilebinary.a.a.a.c.c.k;
import com.agilebinary.a.a.a.g.a;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.x;
import java.io.OutputStream;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final a f82a;

    public c(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException(org.osmdroid.b.b.a.I("[\u001cv\u0007}\u001dlSt\u0016v\u0014l\u001b8\u0000l\u0001y\u0007}\u0014aSu\u0012aSv\u001clSz\u00168\u001dm\u001ft"));
        }
        this.f82a = aVar;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 26);
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '0');
            i2 = i;
        }
        return new String(cArr);
    }

    public final void a(e eVar, x xVar, com.agilebinary.a.a.a.c cVar) {
        if (eVar == null) {
            throw new IllegalArgumentException(j.I("\u0003_#I9U>\u001a?O$J%NpX%\\6_\"\u001a=[)\u001a>U$\u001a2_pT%V<"));
        } else if (xVar == null) {
            throw new IllegalArgumentException(org.osmdroid.b.b.a.I(";L'HSu\u0016k\u0000y\u0014}Su\u0012aSv\u001clSz\u00168\u001dm\u001ft"));
        } else if (cVar == null) {
            throw new IllegalArgumentException(j.I("\u0018n\u0004jp_>N9N)\u001a=[)\u001a>U$\u001a2_pT%V<"));
        } else {
            long a2 = this.f82a.a(xVar);
            OutputStream gVar = a2 == -2 ? new g(eVar) : a2 == -1 ? new k(eVar) : new com.agilebinary.a.a.a.c.c.j(eVar, a2);
            cVar.a(gVar);
            gVar.close();
        }
    }
}
