package com.agilebinary.a.a.a.i;

import com.agilebinary.mobilemonitor.client.android.a.a.g;
import org.osmdroid.util.c;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final String f116a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;

    private /* synthetic */ d(String str, String str2, String str3, String str4, String str5) {
        if (str == null) {
            throw new IllegalArgumentException(c.I("6F\u0005L\u0007@\u0003\u0007\u000fC\u0003I\u0012N\u0000N\u0003UFJ\u0013T\u0012\u0007\bH\u0012\u0007\u0004BFI\u0013K\n\t"));
        }
        this.f116a = str;
        this.b = str2 == null ? g.I("J~^f^ySq]|Z") : str2;
        this.c = str3 == null ? c.I("r(f0f/k'e*b") : str3;
        this.d = str4 == null ? g.I("J~^f^ySq]|Z") : str4;
        this.e = str5 == null ? c.I("r(f0f/k'e*b") : str5;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ '\\');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 31);
            i2 = i;
        }
        return new String(cArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00db  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.agilebinary.a.a.a.i.d a(java.lang.String r6, java.lang.ClassLoader r7) {
        /*
            r1 = 0
            if (r6 != 0) goto L_0x000f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "`~StQxU?Y{UqDvVvUm\u0010rElD?^pD?Rz\u0010qEs\\1"
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.a.a.g.I(r1)
            r0.<init>(r1)
            throw r0
        L_0x000f:
            if (r7 != 0) goto L_0x0019
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            java.lang.ClassLoader r7 = r0.getContextClassLoader()
        L_0x0019:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0065 }
            r0.<init>()     // Catch:{ IOException -> 0x0065 }
            r2 = 46
            r3 = 47
            java.lang.String r2 = r6.replace(r2, r3)     // Catch:{ IOException -> 0x0065 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x0065 }
            java.lang.String r2 = "/"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x0065 }
            java.lang.String r2 = "\u0010B\u0014T\u000fH\b\t\u0016U\tW\u0003U\u0012N\u0003T"
            java.lang.String r2 = org.osmdroid.util.c.I(r2)     // Catch:{ IOException -> 0x0065 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x0065 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0065 }
            java.io.InputStream r2 = r7.getResourceAsStream(r0)     // Catch:{ IOException -> 0x0065 }
            if (r2 == 0) goto L_0x0069
            java.util.Properties r0 = new java.util.Properties     // Catch:{ all -> 0x0060 }
            r0.<init>()     // Catch:{ all -> 0x0060 }
            r0.load(r2)     // Catch:{ all -> 0x0060 }
            r2.close()     // Catch:{ IOException -> 0x00dd }
            r4 = r0
        L_0x0050:
            if (r4 == 0) goto L_0x00db
            if (r6 != 0) goto L_0x006b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "`~StQxU?Y{UqDvVvUm\u0010rElD?^pD?Rz\u0010qEs\\1"
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.a.a.g.I(r1)
            r0.<init>(r1)
            throw r0
        L_0x0060:
            r0 = move-exception
            r2.close()     // Catch:{ IOException -> 0x0065 }
            throw r0     // Catch:{ IOException -> 0x0065 }
        L_0x0065:
            r0 = move-exception
            r0 = r1
        L_0x0067:
            r4 = r0
            goto L_0x0050
        L_0x0069:
            r4 = r1
            goto L_0x0050
        L_0x006b:
            if (r4 == 0) goto L_0x00d7
            java.lang.String r0 = "N\bA\t\t\u000bH\u0002R\nB"
            java.lang.String r0 = org.osmdroid.util.c.I(r0)
            java.lang.Object r0 = r4.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00d5
            int r2 = r0.length()
            if (r2 > 0) goto L_0x00d5
            r2 = r1
        L_0x0082:
            java.lang.String r0 = "YqVp\u001emUsU~Cz"
            java.lang.String r0 = com.agilebinary.mobilemonitor.client.android.a.a.g.I(r0)
            java.lang.Object r0 = r4.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00d3
            int r3 = r0.length()
            if (r3 <= 0) goto L_0x00a2
            java.lang.String r3 = "B\\\u0016H\u000b\t\u0010B\u0014T\u000fH\bZ"
            java.lang.String r3 = org.osmdroid.util.c.I(r3)
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x00d3
        L_0x00a2:
            r3 = r1
        L_0x00a3:
            java.lang.String r0 = "YqVp\u001ekYrUlD~]o"
            java.lang.String r0 = com.agilebinary.mobilemonitor.client.android.a.a.g.I(r0)
            java.lang.Object r0 = r4.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00d1
            int r4 = r0.length()
            if (r4 <= 0) goto L_0x00c3
            java.lang.String r4 = "B\\\u000bQ\b\t\u0012N\u000bB\u0015S\u0007J\u0016Z"
            java.lang.String r4 = org.osmdroid.util.c.I(r4)
            boolean r4 = r0.equals(r4)
            if (r4 == 0) goto L_0x00d1
        L_0x00c3:
            r4 = r1
        L_0x00c4:
            if (r7 == 0) goto L_0x00df
            java.lang.String r5 = r7.toString()
        L_0x00ca:
            com.agilebinary.a.a.a.i.d r0 = new com.agilebinary.a.a.a.i.d
            r1 = r6
            r0.<init>(r1, r2, r3, r4, r5)
        L_0x00d0:
            return r0
        L_0x00d1:
            r4 = r0
            goto L_0x00c4
        L_0x00d3:
            r3 = r0
            goto L_0x00a3
        L_0x00d5:
            r2 = r0
            goto L_0x0082
        L_0x00d7:
            r4 = r1
            r3 = r1
            r2 = r1
            goto L_0x00c4
        L_0x00db:
            r0 = r1
            goto L_0x00d0
        L_0x00dd:
            r2 = move-exception
            goto L_0x0067
        L_0x00df:
            r5 = r1
            goto L_0x00ca
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.i.d.a(java.lang.String, java.lang.ClassLoader):com.agilebinary.a.a.a.i.d");
    }

    public final String a() {
        return this.c;
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer(this.f116a.length() + 20 + this.b.length() + this.c.length() + this.d.length() + this.e.length());
        stringBuffer.append(g.I("fzBlYp^V^y_7")).append(this.f116a).append(':').append(this.b);
        if (!c.I("r(f0f/k'e*b").equals(this.c)) {
            stringBuffer.append(':').append(this.c);
        }
        if (!g.I("J~^f^ySq]|Z").equals(this.d)) {
            stringBuffer.append(':').append(this.d);
        }
        stringBuffer.append(')');
        if (!c.I("r(f0f/k'e*b").equals(this.e)) {
            stringBuffer.append('@').append(this.e);
        }
        return stringBuffer.toString();
    }
}
