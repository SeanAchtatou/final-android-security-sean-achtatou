package com.agilebinary.a.a.a.c.b;

import com.jasonkostempski.android.calendar.a;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.apache.commons.logging.Log;

public final class q {

    /* renamed from: a  reason: collision with root package name */
    private final Log f54a;

    public q(Log log) {
        this.f54a = log;
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ '!');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '<');
            i2 = i;
        }
        return new String(cArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.CharSequence):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.Object):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, float):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.String):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, long):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char[]):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, int):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, boolean):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, double):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder} */
    private /* synthetic */ void a(String str, InputStream inputStream) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                break;
            } else if (read == 13) {
                sb.append(a.I("{wRv"));
            } else if (read == 10) {
                sb.append(com.agilebinary.a.a.a.a.a.I("&p\u0013q_"));
                sb.insert(0, a.I("\t"));
                sb.insert(0, str);
                this.f54a.debug(sb.toString());
                sb.setLength(0);
            } else if (read < 32 || read > 127) {
                sb.append(com.agilebinary.a.a.a.a.a.I("&\u001c\u0005"));
                sb.append(Integer.toHexString(read));
                sb.append(a.I("v"));
            } else {
                sb.append((char) read);
            }
        }
        if (sb.length() > 0) {
            sb.append('\"');
            sb.insert(0, '\"');
            sb.insert(0, str);
            this.f54a.debug(sb.toString());
        }
    }

    public final void a(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.a.a.I("c\bX\rY\t\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"));
        }
        a(a.I("\u0015\u001e\u000b"), new ByteArrayInputStream(bArr));
    }

    public final void a(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.a.a.I("c\bX\rY\t\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"));
        }
        a(a.I("\u0015\u001e\u000b"), new ByteArrayInputStream(bArr, i, i2));
    }

    public final boolean a() {
        return this.f54a.isDebugEnabled();
    }

    public final void b(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.a.a.I("4B\rY\t\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"));
        }
        a(a.I("\u0017\u001c\u000b"), new ByteArrayInputStream(bArr));
    }

    public final void b(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.a.a.I("4B\rY\t\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"));
        }
        a(a.I("\u0017\u001c\u000b"), new ByteArrayInputStream(bArr, i, i2));
    }
}
