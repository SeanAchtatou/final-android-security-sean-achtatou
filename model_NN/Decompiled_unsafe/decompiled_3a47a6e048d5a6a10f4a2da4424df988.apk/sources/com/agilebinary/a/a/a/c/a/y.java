package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.a.k.m;
import com.agilebinary.mobilemonitor.client.android.a.q;
import java.util.Locale;
import org.osmdroid.b.b.a;

public final class y implements k {
    private static /* synthetic */ boolean a(String str, String str2) {
        return str.equals(str2) || (str2.startsWith(a.I("6")) && str.endsWith(str2));
    }

    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException(q.I("o\u0013C\u0017E\u0019\f\u0011M\u0005\f\u0012C\b\f\u001eI\\B\t@\u0010"));
        } else if (str == null) {
            throw new e(a.I(">q\u0000k\u001av\u00148\u0005y\u001fm\u00168\u0015w\u00018\u0017w\u001ey\u001avSy\u0007l\u0001q\u0011m\u0007}"));
        } else if (str.trim().length() == 0) {
            throw new e(q.I("n\u0010M\u0012G\\Z\u001d@\tI\\J\u0013^\\H\u0013A\u001dE\u0012\f\u001dX\b^\u0015N\tX\u0019"));
        } else {
            String lowerCase = str.toLowerCase(Locale.ENGLISH);
            if (!lowerCase.startsWith(a.I("6"))) {
                lowerCase = '.' + lowerCase;
            }
            bVar.b(lowerCase);
        }
    }

    public final void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(q.I("o\u0013C\u0017E\u0019\f\u0011M\u0005\f\u0012C\b\f\u001eI\\B\t@\u0010"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(a.I("[\u001cw\u0018q\u00168\u001cj\u001a\u001avSu\u0012aSv\u001clSz\u00168\u001dm\u001ft"));
        } else {
            String lowerCase = fVar.a().toLowerCase(Locale.ENGLISH);
            if (cVar.c() == null) {
                throw new i(q.I("e\u0012Z\u001d@\u0015H\\O\u0013C\u0017E\u0019\f\u000fX\u001dX\u0019\u0016\\H\u0013A\u001dE\u0012\f\u0012C\b\f\u000f\\\u0019O\u0015J\u0015I\u0018"));
            }
            String lowerCase2 = cVar.c().toLowerCase(Locale.ENGLISH);
            if (!(cVar instanceof m) || !((m) cVar).e(a.I("\u0017w\u001ey\u001av"))) {
                if (!cVar.c().equals(lowerCase)) {
                    throw new i(a.I("Q\u001ft\u0016\u0012tS|\u001cu\u0012q\u001d8\u0012l\u0007j\u001az\u0006l\u0016\"S:") + cVar.c() + q.I("\u000eR") + a.I("\\\u001cu\u0012q\u001d8\u001c~Sw\u0001q\u0014q\u001d\"S:") + lowerCase + q.I("^"));
                }
            } else if (!lowerCase2.startsWith(q.I("R"))) {
                throw new i(a.I("7w\u001ey\u001avSy\u0007l\u0001q\u0011m\u0007}S:") + cVar.c() + q.I("^\f\nE\u0013@\u001dX\u0019_\\~:o\\\u001eM\u001cE\u0016\\H\u0013A\u001dE\u0012\f\u0011Y\u000fX\\_\bM\u000eX\\[\u0015X\u0014\f\u001d\f\u0018C\b"));
            } else {
                int indexOf = lowerCase2.indexOf(46, 1);
                if ((indexOf < 0 || indexOf == lowerCase2.length() - 1) && !lowerCase2.equals(a.I("]t\u001c{\u0012t"))) {
                    throw new i(q.I("h\u0013A\u001dE\u0012\f\u001dX\b^\u0015N\tX\u0019\f^") + cVar.c() + a.I(":Sn\u001aw\u001fy\u0007}\u00008!^08A!E-I8\u0007p\u00168\u0005y\u001fm\u00168\u0010w\u001dl\u0012q\u001dkSv\u001c8\u0016u\u0011}\u0017|\u0016|S|\u001cl\u00008") + q.I("\u001dB\u0018\f\bD\u0019\f\nM\u0010Y\u0019\f\u0015_\\B\u0013X\\\u0002\u0010C\u001fM\u0010"));
                } else if (!a(lowerCase, lowerCase2)) {
                    throw new i(a.I("7w\u001ey\u001avSy\u0007l\u0001q\u0011m\u0007}S:") + cVar.c() + q.I("\u000e\\Z\u0015C\u0010M\bI\u000f\f.j?\fN\u0015J\u0019F\f\u0019J\u001aI\u001fX\u0015Z\u0019\f\u0014C\u000fX\\B\u001dA\u0019\f\u0018C\u0019_\\B\u0013X\\") + a.I("\u0017w\u001ey\u001av^u\u0012l\u0010pS|\u001cu\u0012q\u001d8\u0012l\u0007j\u001az\u0006l\u00166"));
                } else if (lowerCase.substring(0, lowerCase.length() - lowerCase2.length()).indexOf(46) != -1) {
                    throw new i(q.I("h\u0013A\u001dE\u0012\f\u001dX\b^\u0015N\tX\u0019\f^") + cVar.c() + a.I(":Sn\u001aw\u001fy\u0007}\u00008!^08A!E-I8") + q.I("I\u001aJ\u0019O\bE\nI\\D\u0013_\b\f\u0011E\u0012Y\u000f\f\u0018C\u0011M\u0015B\\A\u001dU\\B\u0013X\\O\u0013B\bM\u0015B\\M\u0012U\\H\u0013X\u000f"));
                }
            }
        }
    }

    public final boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(a.I("0w\u001cs\u001a}Su\u0012aSv\u001clSz\u00168\u001dm\u001ft"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(q.I("?C\u0013G\u0015I\\C\u000eE\u001bE\u0012\f\u0011M\u0005\f\u0012C\b\f\u001eI\\B\t@\u0010"));
        } else {
            String lowerCase = fVar.a().toLowerCase(Locale.ENGLISH);
            String c = cVar.c();
            return a(lowerCase, c) && lowerCase.substring(0, lowerCase.length() - c.length()).indexOf(46) == -1;
        }
    }
}
