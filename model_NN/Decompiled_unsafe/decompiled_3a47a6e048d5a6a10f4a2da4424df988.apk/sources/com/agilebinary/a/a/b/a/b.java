package com.agilebinary.a.a.b.a;

import com.agilebinary.a.a.a.k;
import com.agilebinary.mobilemonitor.a.a.a.a;
import com.agilebinary.mobilemonitor.client.android.ui.a.e;
import org.apache.commons.logging.Log;

public final class b implements Log {

    /* renamed from: a  reason: collision with root package name */
    private String f120a;

    public b(String str) {
        this.f120a = str;
    }

    public final void debug(Object obj) {
        obj.toString();
    }

    public final void debug(Object obj, Throwable th) {
        obj.toString();
        a.b(th);
    }

    public final void error(Object obj) {
        obj.toString();
    }

    public final void error(Object obj, Throwable th) {
        obj.toString();
        a.e(th);
    }

    public final void fatal(Object obj) {
        obj.toString();
    }

    public final void fatal(Object obj, Throwable th) {
        obj.toString();
        a.e(th);
    }

    public final void info(Object obj) {
        obj.toString();
    }

    public final void info(Object obj, Throwable th) {
        obj.toString();
        a.c(th);
    }

    public final boolean isDebugEnabled() {
        if (!e.I("\u0005\r#\u000b}\u000bn\u0002hDe\u001ey\u001a#\u001dd\u0018h").equals(this.f120a) && k.I("\f#\u0004\u0002!\u00022\u000b4M9\u0017%\u0013\u000b4\u00025\u0006#\u0010").equals(this.f120a)) {
        }
        return false;
    }

    public final boolean isErrorEnabled() {
        return false;
    }

    public final boolean isFatalEnabled() {
        return false;
    }

    public final boolean isInfoEnabled() {
        return false;
    }

    public final boolean isTraceEnabled() {
        return false;
    }

    public final boolean isWarnEnabled() {
        return false;
    }

    public final void trace(Object obj) {
        obj.toString();
    }

    public final void trace(Object obj, Throwable th) {
        obj.toString();
        a.a(th);
    }

    public final void warn(Object obj) {
        obj.toString();
    }

    public final void warn(Object obj, Throwable th) {
        obj.toString();
        a.d(th);
    }
}
