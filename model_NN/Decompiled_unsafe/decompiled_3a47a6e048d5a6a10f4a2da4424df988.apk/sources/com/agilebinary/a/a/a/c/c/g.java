package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.c.b.q;
import com.agilebinary.a.a.a.j.e;
import java.io.IOException;
import java.io.OutputStream;

public final class g extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final e f62a;
    private byte[] b;
    private int c;
    private boolean d;
    private boolean e;

    public g(e eVar) {
        this(eVar, (byte) 0);
    }

    private /* synthetic */ g(e eVar, byte b2) {
        this.c = 0;
        this.d = false;
        this.e = false;
        this.b = new byte[2048];
        this.f62a = eVar;
    }

    private /* synthetic */ void a() {
        if (this.c > 0) {
            this.f62a.a(Integer.toHexString(this.c));
            this.f62a.a(this.b, 0, this.c);
            this.f62a.a("");
            this.c = 0;
        }
    }

    public final void close() {
        if (!this.e) {
            this.e = true;
            if (!this.d) {
                a();
                this.f62a.a(q.I("\u0011"));
                this.f62a.a("");
                this.d = true;
            }
            this.f62a.b();
        }
    }

    public final void flush() {
        a();
        this.f62a.b();
    }

    public final void write(int i) {
        if (this.e) {
            throw new IOException(m.I("^kkzrokz{?hmvkz?kp?|splz{?lkmz~r1"));
        }
        this.b[this.c] = (byte) i;
        this.c++;
        if (this.c == this.b.length) {
            a();
        }
    }

    public final void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public final void write(byte[] bArr, int i, int i2) {
        if (this.e) {
            throw new IOException(q.I("`HUYLLUYE\u001cVNHHD\u001cUS\u0001_MSRYE\u001cRHSY@Q\u000f"));
        } else if (i2 >= this.b.length - this.c) {
            this.f62a.a(Integer.toHexString(this.c + i2));
            this.f62a.a(this.b, 0, this.c);
            this.f62a.a(bArr, i, i2);
            this.f62a.a("");
            this.c = 0;
        } else {
            System.arraycopy(bArr, i, this.b, this.c, i2);
            this.c += i2;
        }
    }
}
