package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.c.b.a.j;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.b.a.a;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import org.apache.commons.logging.Log;

public final class d implements ab {

    /* renamed from: a  reason: collision with root package name */
    private final Log f87a = a.a(getClass());

    public final void a(f fVar, k kVar) {
        URI uri;
        int i;
        t b;
        if (fVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.a.a.I("d)x-\f\u000fI\fY\u0018_\t\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"));
        } else if (kVar == null) {
            throw new IllegalArgumentException(j.I("r\u0004n\u0000\u001a3U>N5B$\u001a=[)\u001a>U$\u001a2_pT%V<"));
        } else if (!fVar.a().a().equalsIgnoreCase(com.agilebinary.a.a.a.a.a.I(">c3b8o)"))) {
            com.agilebinary.a.a.a.d.d dVar = (com.agilebinary.a.a.a.d.d) kVar.a(j.I("8N$J~Y?U;S5\u0017#N?H5"));
            if (dVar == null) {
                this.f87a.info(com.agilebinary.a.a.a.a.a.I("o\u0012C\u0016E\u0018\f\u000eX\u0012^\u0018\f\u0013C\t\f\u001cZ\u001cE\u0011M\u001f@\u0018\f\u0014B]d)x-\f\u001eC\u0013X\u0018T\t"));
                return;
            }
            com.agilebinary.a.a.a.k.d dVar2 = (com.agilebinary.a.a.a.k.d) kVar.a(j.I("R$N \u00143U?Q9_#J5Y}H5]9I$H)"));
            if (dVar2 == null) {
                this.f87a.info(com.agilebinary.a.a.a.a.a.I(">C\u0012G\u0014I.\\\u0018O]^\u0018K\u0014_\t^\u0004\f\u0013C\t\f\u001cZ\u001cE\u0011M\u001f@\u0018\f\u0014B]d)x-\f\u001eC\u0013X\u0018T\t"));
                return;
            }
            b bVar = (b) kVar.a(j.I("R$N \u0014$[\"]5N\u000fR?I$"));
            if (bVar == null) {
                throw new IllegalStateException(com.agilebinary.a.a.a.a.a.I(")M\u000fK\u0018X]D\u0012_\t\f\u0013C\t\f\u000e\\\u0018O\u0014J\u0014I\u0019\f\u0014B]d)x-\f\u001eC\u0013X\u0018T\t"));
            }
            com.agilebinary.a.a.a.h.a aVar = (com.agilebinary.a.a.a.h.a) kVar.a(j.I("8N$J~Y?T>_3N9U>"));
            if (aVar == null) {
                throw new IllegalStateException(com.agilebinary.a.a.a.a.a.I(">@\u0014I\u0013X]O\u0012B\u0013I\u001eX\u0014C\u0013\f\u0013C\t\f\u000e\\\u0018O\u0014J\u0014I\u0019\f\u0014B]d)x-\f\u001eC\u0013X\u0018T\t"));
            }
            e g = fVar.g();
            if (g == null) {
                throw new IllegalArgumentException(j.I("\u0018n\u0004jpJ1H1W5N5H#\u001a=[)\u001a>U$\u001a2_pT%V<"));
            }
            String str = (String) g.a(com.agilebinary.a.a.a.a.a.I("\u0015X\t\\S\\\u000fC\tC\u001eC\u0011\u0002\u001eC\u0012G\u0014IP\\\u0012@\u0014O\u0004"));
            String I = str == null ? j.I("X5I$\u0017=[$Y8") : str;
            if (this.f87a.isDebugEnabled()) {
                this.f87a.debug(com.agilebinary.a.a.a.a.a.I(">C\u0012G\u0014I.\\\u0018O]_\u0018@\u0018O\tI\u0019\u0016]") + I);
            }
            if (fVar instanceof com.agilebinary.a.a.a.d.c.b) {
                uri = ((com.agilebinary.a.a.a.d.c.b) fVar).e_();
            } else {
                try {
                    uri = new URI(fVar.a().c());
                } catch (URISyntaxException e) {
                    throw new l(com.agilebinary.a.a.a.a.a.I("4B\u000bM\u0011E\u0019\f\u000fI\fY\u0018_\t\f(~4\u0016]") + fVar.a().c(), e);
                }
            }
            String a2 = bVar.a();
            int b2 = bVar.b();
            if (b2 < 0) {
                h hVar = (h) kVar.a(j.I("R$N \u0014#Y8_=_}H5]9I$H)"));
                i = hVar != null ? hVar.b(bVar.c()).a(b2) : aVar.q();
            } else {
                i = b2;
            }
            com.agilebinary.a.a.a.k.f fVar2 = new com.agilebinary.a.a.a.k.f(a2, i, uri.getPath(), aVar.a());
            com.agilebinary.a.a.a.k.j a3 = dVar2.a(I, fVar.g());
            ArrayList<c> arrayList = new ArrayList<>(dVar.a());
            ArrayList<c> arrayList2 = new ArrayList<>();
            Date date = new Date();
            for (c cVar : arrayList) {
                if (!cVar.b(date)) {
                    if (a3.b(cVar, fVar2)) {
                        if (this.f87a.isDebugEnabled()) {
                            this.f87a.debug(com.agilebinary.a.a.a.a.a.I(">C\u0012G\u0014I]") + cVar + j.I("pW1N3Rp") + fVar2);
                        }
                        arrayList2.add(cVar);
                    }
                } else if (this.f87a.isDebugEnabled()) {
                    this.f87a.debug(j.I("\u0013U?Q9_p") + cVar + com.agilebinary.a.a.a.a.a.I("\f\u0018T\rE\u000fI\u0019"));
                }
            }
            if (!arrayList2.isEmpty()) {
                for (t a4 : a3.a(arrayList2)) {
                    fVar.a(a4);
                }
            }
            int a5 = a3.a();
            if (a5 > 0) {
                boolean z = false;
                for (c cVar2 : arrayList2) {
                    if (a5 != cVar2.h() || !(cVar2 instanceof com.agilebinary.a.a.a.k.h)) {
                        z = true;
                    }
                }
                if (z && (b = a3.b()) != null) {
                    fVar.a(b);
                }
            }
            kVar.a(j.I("R$N \u00143U?Q9_}I _3"), a3);
            kVar.a(com.agilebinary.a.a.a.a.a.I("D\tX\r\u0002\u001eC\u0012G\u0014IPC\u000fE\u001aE\u0013"), fVar2);
        }
    }
}
