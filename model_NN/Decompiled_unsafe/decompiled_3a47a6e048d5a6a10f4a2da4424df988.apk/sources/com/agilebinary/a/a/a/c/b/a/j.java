package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.c.b.e;
import com.agilebinary.a.a.a.c.c.m;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.logging.Log;

public abstract class j {

    /* renamed from: a  reason: collision with root package name */
    protected final Lock f41a = new ReentrantLock();
    protected Set b = new HashSet();
    private final Log c = a.a(getClass());
    private volatile boolean d;
    private e e = new e();

    protected j() {
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'P');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ ':');
            i2 = i;
        }
        return new String(cArr);
    }

    public void a() {
        this.f41a.lock();
        try {
            if (this.d) {
                this.f41a.unlock();
                return;
            }
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                it.remove();
                g c2 = ((g) it.next()).c();
                if (c2 != null) {
                    c2.k();
                }
            }
            this.e.a();
            this.d = true;
            this.f41a.unlock();
        } catch (IOException e2) {
            this.c.debug(m.I("V0P?zmmpm?|splvqx?|pqqz|kvpq"), e2);
        } catch (Throwable th) {
            this.f41a.unlock();
            throw th;
        }
    }

    public void a(long j, TimeUnit timeUnit) {
        if (timeUnit == null) {
            throw new IllegalArgumentException(com.agilebinary.mobilemonitor.client.a.a.a.I("Z5c9.)`5z|c)}(.2a(.>k|`)b0 "));
        }
        this.f41a.lock();
        try {
            this.e.a(timeUnit.toMillis(j));
        } finally {
            this.f41a.unlock();
        }
    }
}
