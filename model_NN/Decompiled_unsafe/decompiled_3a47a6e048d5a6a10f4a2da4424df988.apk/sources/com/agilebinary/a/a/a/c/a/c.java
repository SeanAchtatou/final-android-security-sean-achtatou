package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.c.b.a.j;
import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.k;
import com.jasonkostempski.android.calendar.a;
import java.util.Locale;

public final class c implements k {
    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException(j.I("y?U;S5\u001a=[)\u001a>U$\u001a2_pT%V<"));
        } else if (str == null) {
            throw new e(a.I("mBSXIEG\u000bVJL^E\u000bFDR\u000bDDMJIE\u0000JT_RBB^TN"));
        } else if (str.trim().length() == 0) {
            throw new e(j.I("x<[>QpL1V%_p\\?Hp^?W1S>\u001a1N$H9X%N5"));
        } else {
            bVar.b(str);
        }
    }

    public final void a(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(a.I("cDO@IN\u0000FAR\u0000EO_\u0000IE\u000bN^LG"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(j.I("\u0013U?Q9_pU\"S7S>\u001a=[)\u001a>U$\u001a2_pT%V<"));
        } else {
            String a2 = fVar.a();
            String c = cVar.c();
            if (c == null) {
                throw new i(a.I("hODKBE\u000bDDMJIE\u0000FAR\u0000EO_\u0000IE\u000bN^LG"));
            } else if (c.equals(a2)) {
            } else {
                if (c.indexOf(46) == -1) {
                    throw new i(j.I("~?W1S>\u001a1N$H9X%N5\u001ar") + c + a.I("\t\u0000OONS\u000bNDT\u000bMJTHH\u000bTCE\u000bHDS_\u0000\t") + a2 + j.I("r"));
                } else if (!c.startsWith(a.I("\u0005"))) {
                    throw new i(j.I("~?W1S>\u001a1N$H9X%N5\u001ar") + c + a.I("\t\u0000]IDLJTNS\u000brmc\u000b\u0012\u001a\u0010\u0012\u001a\u000bDDMJIE\u0000FUXT\u000bS_AYT\u000bWBTC\u0000J\u0000OO_"));
                } else {
                    int indexOf = c.indexOf(46, 1);
                    if (indexOf < 0 || indexOf == c.length() - 1) {
                        throw new i(j.I("~?W1S>\u001a1N$H9X%N5\u001ar") + c + a.I("\u0002\u000bVBOGA_EX\u0000yfh\u0000\u0019\u0011\u001b\u0019\u0011\u0000OOFABN\u000bM^S_\u0000HOETJIE\u0000JN\u000bEFBNDOEO\u0000OO_"));
                    }
                    String lowerCase = a2.toLowerCase(Locale.ENGLISH);
                    if (!lowerCase.endsWith(c)) {
                        throw new i(j.I("s<V5]1Vp^?W1S>\u001a1N$H9X%N5\u001ar") + c + a.I("\u0002\u0005\u0000oOFABN\u000bOM\u0000DRBGBN\u0011\u0000\t") + lowerCase + j.I("r"));
                    } else if (lowerCase.substring(0, lowerCase.length() - c.length()).indexOf(46) != -1) {
                        throw new i(a.I("dDMJIE\u0000JT_RBB^TN\u0000\t") + c + j.I("r\u001a&S?V1N5Iph\u0016yp\ba\ni\u0000pR?I$\u001a=S>O#\u001a4U=[9TpW1CpT?NpY?T$[9Tp[>Cp^?N#"));
                    }
                }
            }
        }
    }

    public final boolean b(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(a.I("cDO@IN\u0000FAR\u0000EO_\u0000IE\u000bN^LG"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(j.I("\u0013U?Q9_pU\"S7S>\u001a=[)\u001a>U$\u001a2_pT%V<"));
        } else {
            String a2 = fVar.a();
            String c = cVar.c();
            if (c == null) {
                return false;
            }
            return a2.equals(c) || (c.startsWith(a.I("\u0005")) && a2.endsWith(c));
        }
    }
}
