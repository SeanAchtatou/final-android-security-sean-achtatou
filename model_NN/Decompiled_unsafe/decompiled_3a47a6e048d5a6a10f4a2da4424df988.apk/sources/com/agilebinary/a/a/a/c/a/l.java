package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.b;
import com.agilebinary.a.a.a.c.c.m;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f29a = {m.I("ZZZZ3?{{2RRR2ff?WW%rr%ll?eee"), b.I("3*3CV\u000b\u0012O;\";O\u000f\u0016\u000f\u0016V'>U\u001b\u0002L\u001c\u0005O\f\u0015\f"), m.I("ZZZ?RRR?{?WW%rr%ll?ffff")};
    private static final Date b;
    private static TimeZone c = TimeZone.getTimeZone(b.I("1\"\""));

    static {
        Calendar instance = Calendar.getInstance();
        instance.setTimeZone(c);
        instance.set(2000, 0, 1, 0, 0, 0);
        instance.set(14, 0);
        b = instance.getTime();
    }

    private /* synthetic */ l() {
    }

    public static Date a(String str, String[] strArr) {
        return b(str, strArr);
    }

    private static /* synthetic */ Date b(String str, String[] strArr) {
        if (str == null) {
            throw new IllegalArgumentException(m.I("{~kzI~sjz?vl?qjss"));
        }
        if (strArr == null) {
            strArr = f29a;
        }
        Date date = b;
        if (str.length() > 1 && str.startsWith(b.I("Q")) && str.endsWith(m.I("8"))) {
            str = str.substring(1, str.length() - 1);
        }
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            SimpleDateFormat a2 = ad.a(strArr[i]);
            a2.set2DigitYearStart(date);
            try {
                return a2.parse(str);
            } catch (ParseException e) {
                i++;
            }
        }
        throw new k(b.I("#\u0001\u0017\r\u001a\nV\u001b\u0019O\u0006\u000e\u0004\u001c\u0013O\u0002\u0007\u0013O\u0012\u000e\u0002\nV") + str);
    }
}
