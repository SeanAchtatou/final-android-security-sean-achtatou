package com.agilebinary.a.a.a.a;

import com.agilebinary.a.a.a.c.c.j;
import com.jasonkostempski.android.calendar.a;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private h f5a;
    private c b;
    private g c;

    public final void a() {
        this.f5a = null;
        this.b = null;
        this.c = null;
    }

    public final void a(c cVar) {
        this.b = cVar;
    }

    public final void a(g gVar) {
        this.c = gVar;
    }

    public final void a(h hVar) {
        if (hVar == null) {
            a();
        } else {
            this.f5a = hVar;
        }
    }

    public final boolean b() {
        return this.f5a != null;
    }

    public final h c() {
        return this.f5a;
    }

    public final g d() {
        return this.c;
    }

    public final c e() {
        return this.b;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a.I("A^TC\u0000XCDPN\u0000p"));
        sb.append(this.b);
        sb.append(j.I("&@[\u0018\t\u001e\u001f\u001e\u0015\u000f\u0012\u001a\u0017\b[\b\u001e\u000f[ "));
        sb.append(this.c != null ? a.I("TYUN") : a.I("MAGSN"));
        sb.append(j.I("&"));
        return sb.toString();
    }
}
