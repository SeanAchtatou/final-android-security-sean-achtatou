package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.c.c.j;
import com.agilebinary.a.a.a.e.d;
import com.agilebinary.a.a.a.e.e;

public final class q extends d {

    /* renamed from: a  reason: collision with root package name */
    private e f76a = null;
    private e b;
    private e c;
    private e d;

    public q(e eVar, e eVar2) {
        this.b = eVar;
        this.c = eVar2;
        this.d = null;
    }

    public final e a(String str, Object obj) {
        throw new UnsupportedOperationException(com.agilebinary.a.a.a.c.b.q.I("rYUHHRF\u001cQ]S]LYUYSO\u0001UO\u001c@\u001cRH@_J\u001cHO\u0001RNH\u0001OTLQSSHDX\u000f"));
    }

    public final Object a(String str) {
        if (str == null) {
            throw new IllegalArgumentException(j.I("+\u001a\t\u001a\u0016\u001e\u000f\u001e\t[\u0015\u001a\u0016\u001e[\u0016\u000e\b\u000f[\u0015\u0014\u000f[\u0019\u001e[\u0015\u000e\u0017\u0017U"));
        }
        Object obj = null;
        if (this.d != null) {
            obj = this.d.a(str);
        }
        if (obj == null && this.c != null) {
            obj = this.c.a(str);
        }
        if (obj == null && this.b != null) {
            obj = this.b.a(str);
        }
        return (obj != null || this.f76a == null) ? obj : this.f76a.a(str);
    }
}
