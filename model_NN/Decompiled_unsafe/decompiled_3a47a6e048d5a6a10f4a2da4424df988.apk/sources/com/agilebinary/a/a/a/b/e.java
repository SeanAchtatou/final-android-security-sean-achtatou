package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.a.u;
import java.io.Serializable;
import org.c.a.a.a.a;

public final class e implements r, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f10a;
    private final c b;
    private final int c;

    public e(c cVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(a.I("ngL}\rn_}Lv\rmXiKj_/@nT/C`Y/Oj\raXcA"));
        }
        int c2 = cVar.c(58);
        if (c2 == -1) {
            throw new u(com.agilebinary.mobilemonitor.client.android.ui.a.e.I("#c\u001cl\u0006d\u000e-\u0002h\u000bi\u000fP-") + cVar.toString());
        }
        String b2 = cVar.b(0, c2);
        if (b2.length() == 0) {
            throw new u(a.I("FCyLcDk\rgHnIj_5\r") + cVar.toString());
        }
        this.b = cVar;
        this.f10a = b2;
        this.c = c2 + 1;
    }

    public final String a() {
        return this.f10a;
    }

    public final String b() {
        return this.b.b(this.c, this.b.c());
    }

    public final m[] c() {
        i iVar = new i(0, this.b.c());
        iVar.a(this.c);
        return f.f11a.a(this.b, iVar);
    }

    public final Object clone() {
        return super.clone();
    }

    public final int d() {
        return this.c;
    }

    public final c e() {
        return this.b;
    }

    public final String toString() {
        return this.b.toString();
    }
}
