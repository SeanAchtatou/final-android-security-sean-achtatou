package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.i.e;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.f;
import com.agilebinary.a.a.a.j.g;
import java.io.IOException;
import java.io.InputStream;

public abstract class b implements a, g {

    /* renamed from: a  reason: collision with root package name */
    private InputStream f57a;
    private byte[] b;
    private int c;
    private int d;
    private e e = null;
    private String f = "US-ASCII";
    private boolean g = true;
    private int h = -1;
    private int i = 512;
    private i j;

    private /* synthetic */ int f() {
        for (int i2 = this.c; i2 < this.d; i2++) {
            if (this.b[i2] == 10) {
                return i2;
            }
        }
        return -1;
    }

    public final int a() {
        return this.d - this.c;
    }

    public final int a(c cVar) {
        int b2;
        boolean z;
        if (cVar == null) {
            throw new IllegalArgumentException(org.osmdroid.util.c.I("d\u000eF\u0014\u0007\u0007U\u0014F\u001f\u0007\u0004R\u0000A\u0003UFJ\u0007^FI\tSFE\u0003\u0007\bR\nK"));
        }
        boolean z2 = true;
        int i2 = 0;
        while (z2) {
            int f2 = f();
            if (f2 == -1) {
                if (c()) {
                    this.e.a(this.b, this.c, this.d - this.c);
                    this.c = this.d;
                }
                b2 = b();
                z = b2 == -1 ? false : z2;
            } else if (this.e.f()) {
                int i3 = this.c;
                this.c = f2 + 1;
                if (f2 > 0 && this.b[f2 - 1] == 13) {
                    f2--;
                }
                int i4 = f2 - i3;
                if (this.g) {
                    cVar.a(this.b, i3, i4);
                    return i4;
                }
                String str = new String(this.b, i3, i4, this.f);
                cVar.a(str);
                return str.length();
            } else {
                this.e.a(this.b, this.c, (f2 + 1) - this.c);
                this.c = f2 + 1;
                b2 = i2;
                z = false;
            }
            if (this.h <= 0 || this.e.d() < this.h) {
                z2 = z;
                i2 = b2;
            } else {
                throw new IOException(j.I("6\u001a\u0003\u0012\u0016\u000e\u0016[\u0017\u0012\u0015\u001e[\u0017\u001e\u0015\u001c\u000f\u0013[\u0017\u0012\u0016\u0012\u000f[\u001e\u0003\u0018\u001e\u001e\u001f\u001e\u001f"));
            }
        }
        if (i2 == -1 && this.e.f()) {
            return -1;
        }
        int d2 = this.e.d();
        if (d2 > 0) {
            if (this.e.b(d2 - 1) == 10) {
                d2--;
                this.e.c(d2);
            }
            if (d2 > 0 && this.e.b(d2 - 1) == 13) {
                this.e.c(d2 - 1);
            }
        }
        int d3 = this.e.d();
        if (this.g) {
            e eVar = this.e;
            if (eVar != null) {
                cVar.a(eVar.e(), 0, d3);
            }
        } else {
            String str2 = new String(this.e.e(), 0, d3, this.f);
            d3 = str2.length();
            cVar.a(str2);
        }
        this.e.a();
        return d3;
    }

    public final int a(byte[] bArr, int i2, int i3) {
        if (bArr == null) {
            return 0;
        }
        if (c()) {
            int min = Math.min(i3, this.d - this.c);
            System.arraycopy(this.b, this.c, bArr, i2, min);
            this.c += min;
            return min;
        } else if (i3 > this.i) {
            return this.f57a.read(bArr, i2, i3);
        } else {
            while (!c()) {
                if (b() == -1) {
                    return -1;
                }
            }
            int min2 = Math.min(i3, this.d - this.c);
            System.arraycopy(this.b, this.c, bArr, i2, min2);
            this.c += min2;
            return min2;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(InputStream inputStream, int i2, com.agilebinary.a.a.a.e.e eVar) {
        boolean z = false;
        if (inputStream == null) {
            throw new IllegalArgumentException(org.osmdroid.util.c.I("/I\u0016R\u0012\u0007\u0015S\u0014B\u0007JFJ\u0007^FI\tSFE\u0003\u0007\bR\nK"));
        } else if (i2 <= 0) {
            throw new IllegalArgumentException(j.I("9\u000e\u001d\u001d\u001e\t[\b\u0012\u0001\u001e[\u0016\u001a\u0002[\u0015\u0014\u000f[\u0019\u001e[\u0015\u001e\u001c\u001a\u000f\u0012\r\u001e[\u0014\t[\u0001\u001e\t\u0014"));
        } else if (eVar == null) {
            throw new IllegalArgumentException(org.osmdroid.util.c.I("o2s6\u0007\u0016F\u0014F\u000bB\u0012B\u0014TFJ\u0007^FI\tSFE\u0003\u0007\bR\nK"));
        } else {
            this.f57a = inputStream;
            this.b = new byte[i2];
            this.c = 0;
            this.d = 0;
            this.e = new e(i2);
            this.f = com.agilebinary.a.a.a.e.b.a(eVar);
            if (this.f.equalsIgnoreCase("US-ASCII") || this.f.equalsIgnoreCase(j.I(":(822"))) {
                z = true;
            }
            this.g = z;
            this.h = eVar.a(org.osmdroid.util.c.I("O\u0012S\u0016\t\u0005H\bI\u0003D\u0012N\tIHJ\u0007_KK\u000fI\u0003\n\nB\b@\u0012O"), -1);
            this.i = eVar.a(j.I("\u0013\u000f\u000f\u000bU\u0018\u0014\u0015\u0015\u001e\u0018\u000f\u0012\u0014\u0015U\u0016\u0012\u0015V\u0018\u0013\u000e\u0015\u0010V\u0017\u0012\u0016\u0012\u000f"), 512);
            this.j = new i();
        }
    }

    /* access modifiers changed from: protected */
    public int b() {
        if (this.c > 0) {
            int i2 = this.d - this.c;
            if (i2 > 0) {
                System.arraycopy(this.b, this.c, this.b, 0, i2);
            }
            this.c = 0;
            this.d = i2;
        }
        int i3 = this.d;
        int read = this.f57a.read(this.b, i3, this.b.length - i3);
        if (read == -1) {
            return -1;
        }
        this.d = i3 + read;
        this.j.a((long) read);
        return read;
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        return this.c < this.d;
    }

    public final int d() {
        while (!c()) {
            if (b() == -1) {
                return -1;
            }
        }
        byte[] bArr = this.b;
        int i2 = this.c;
        this.c = i2 + 1;
        return bArr[i2] & 255;
    }

    public final f e() {
        return this.j;
    }
}
