package com.agilebinary.a.a.a.g;

import com.a.a.a.a;
import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.t;
import java.io.InputStream;
import java.io.OutputStream;

public class b implements c {

    /* renamed from: a  reason: collision with root package name */
    protected c f94a;

    public b(c cVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(a.I("5L#N2[&\u001e'P6W6GbS7M6\u001e,Q6\u001e [bP7R."));
        }
        this.f94a = cVar;
    }

    public void a(OutputStream outputStream) {
        this.f94a.a(outputStream);
    }

    public boolean a() {
        return this.f94a.a();
    }

    public long c() {
        return this.f94a.c();
    }

    public final t d() {
        return this.f94a.d();
    }

    public final t e() {
        return this.f94a.e();
    }

    public InputStream f() {
        return this.f94a.f();
    }

    public boolean g() {
        return this.f94a.g();
    }

    public boolean k_() {
        return this.f94a.k_();
    }
}
