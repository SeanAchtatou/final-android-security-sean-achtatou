package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.c.b.a.j;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import com.agilebinary.a.a.a.t;
import com.agilebinary.mobilemonitor.client.android.a.q;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class ae extends ag {
    public ae() {
        this(null, false);
    }

    public ae(String[] strArr, boolean z) {
        super(strArr, z);
        a(q.I("H\u0013A\u001dE\u0012"), new y());
        a(j.I("J?H$"), new g());
        a(q.I("O\u0013A\u0011I\u0012X\t^\u0010"), new z());
        a(j.I("4S#Y1H4"), new b());
        a(q.I("\nI\u000e_\u0015C\u0012"), new n());
    }

    private static /* synthetic */ f b(f fVar) {
        boolean z = false;
        String a2 = fVar.a();
        int i = 0;
        while (true) {
            if (i >= a2.length()) {
                z = true;
                break;
            }
            char charAt = a2.charAt(i);
            if (charAt == '.' || charAt == ':') {
                break;
            }
            i++;
        }
        return z ? new f(a2 + j.I("\u0014<U3[<"), fVar.c(), fVar.b(), fVar.d()) : fVar;
    }

    private /* synthetic */ List b(m[] mVarArr, f fVar) {
        ArrayList arrayList = new ArrayList(mVarArr.length);
        for (m mVar : mVarArr) {
            String a2 = mVar.a();
            String b = mVar.b();
            if (a2 == null || a2.length() == 0) {
                throw new e(q.I("o\u0013C\u0017E\u0019\f\u0012M\u0011I\\A\u001dU\\B\u0013X\\N\u0019\f\u0019A\fX\u0005"));
            }
            af afVar = new af(a2, b);
            afVar.c(a(fVar));
            afVar.b(fVar.a());
            afVar.a(new int[]{fVar.c()});
            o[] c = mVar.c();
            HashMap hashMap = new HashMap(c.length);
            for (int length = c.length - 1; length >= 0; length--) {
                o oVar = c[length];
                hashMap.put(oVar.a().toLowerCase(Locale.ENGLISH), oVar);
            }
            for (Map.Entry value : hashMap.entrySet()) {
                o oVar2 = (o) value.getValue();
                String lowerCase = oVar2.a().toLowerCase(Locale.ENGLISH);
                afVar.a(lowerCase, oVar2.b());
                k a3 = a(lowerCase);
                if (a3 != null) {
                    a3.a(afVar, oVar2.b());
                }
            }
            arrayList.add(afVar);
        }
        return arrayList;
    }

    public final int a() {
        return 1;
    }

    public final List a(t tVar, f fVar) {
        if (tVar == null) {
            throw new IllegalArgumentException(j.I("r5[4_\"\u001a=[)\u001a>U$\u001a2_pT%V<"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(q.I("?C\u0013G\u0015I\\C\u000eE\u001bE\u0012\f\u0011M\u0005\f\u0012C\b\f\u001eI\\B\t@\u0010"));
        } else if (!tVar.a().equalsIgnoreCase(j.I("\u0003_$\u0017\u0013U?Q9_b"))) {
            throw new e(q.I("y\u0012^\u0019O\u0013K\u0012E\u0006I\u0018\f\u001fC\u0013G\u0015I\\D\u0019M\u0018I\u000e\f[") + tVar.toString() + j.I("w"));
        } else {
            return b(tVar.c(), b(fVar));
        }
    }

    /* access modifiers changed from: protected */
    public final List a(m[] mVarArr, f fVar) {
        return b(mVarArr, b(fVar));
    }

    /* access modifiers changed from: protected */
    public final void a(c cVar, com.agilebinary.a.a.a.k.c cVar2, int i) {
        String d;
        int[] g;
        super.a(cVar, cVar2, i);
        if ((cVar2 instanceof com.agilebinary.a.a.a.k.m) && (d = ((com.agilebinary.a.a.a.k.m) cVar2).d(q.I("\\\u0013^\b"))) != null) {
            cVar.a(j.I("k\u001atj?H$"));
            cVar.a(q.I("\u0011^"));
            if (d.trim().length() > 0 && (g = cVar2.g()) != null) {
                int length = g.length;
                for (int i2 = 0; i2 < length; i2++) {
                    if (i2 > 0) {
                        cVar.a(j.I("|"));
                    }
                    cVar.a(Integer.toString(g[i2]));
                }
            }
            cVar.a(q.I("^"));
        }
    }

    public final void a(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(j.I("y?U;S5\u001a=[)\u001a>U$\u001a2_pT%V<"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(q.I("?C\u0013G\u0015I\\C\u000eE\u001bE\u0012\f\u0011M\u0005\f\u0012C\b\f\u001eI\\B\t@\u0010"));
        } else {
            super.a(cVar, b(fVar));
        }
    }

    public final t b() {
        c cVar = new c(40);
        cVar.a(j.I("\u0013U?Q9_b"));
        cVar.a(q.I("\u0016\\"));
        cVar.a(j.I("tl5H#S?Tm"));
        cVar.a(Integer.toString(1));
        return new com.agilebinary.a.a.a.b.e(cVar);
    }

    public final boolean b(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(q.I("o\u0013C\u0017E\u0019\f\u0011M\u0005\f\u0012C\b\f\u001eI\\B\t@\u0010"));
        } else if (fVar != null) {
            return super.b(cVar, b(fVar));
        } else {
            throw new IllegalArgumentException(j.I("\u0013U?Q9_pU\"S7S>\u001a=[)\u001a>U$\u001a2_pT%V<"));
        }
    }

    public final String toString() {
        return q.I("\u000eJ\u001f\u001eE\u001aI");
    }
}
