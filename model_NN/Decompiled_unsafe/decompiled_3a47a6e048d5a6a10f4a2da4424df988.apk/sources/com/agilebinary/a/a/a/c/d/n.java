package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.c;
import com.agilebinary.a.a.a.a.g;
import com.agilebinary.a.a.a.d.e;
import com.agilebinary.a.a.a.i.b;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class n implements e {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f73a = new ConcurrentHashMap();

    private static /* synthetic */ g a(Map map, c cVar) {
        int i;
        c cVar2;
        g gVar = (g) map.get(cVar);
        if (gVar != null) {
            return gVar;
        }
        c cVar3 = null;
        int i2 = -1;
        for (c cVar4 : map.keySet()) {
            int a2 = cVar.a(cVar4);
            if (a2 > i2) {
                int i3 = a2;
                cVar2 = cVar4;
                i = i3;
            } else {
                i = i2;
                cVar2 = cVar3;
            }
            i2 = i;
            cVar3 = cVar2;
        }
        return cVar3 != null ? (g) map.get(cVar3) : gVar;
    }

    public final g a(c cVar) {
        if (cVar != null) {
            return a(this.f73a, cVar);
        }
        throw new IllegalArgumentException(b.I("wGBZS\\B[USB[Y\\\u0016AU]FW\u0016_WK\u0016\\YF\u0016PS\u0012XGZ^"));
    }

    public final String toString() {
        return this.f73a.toString();
    }
}
