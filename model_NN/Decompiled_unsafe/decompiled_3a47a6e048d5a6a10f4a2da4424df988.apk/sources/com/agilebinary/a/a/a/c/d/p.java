package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.c.c.e;
import com.agilebinary.a.a.a.d.c;
import com.agilebinary.a.a.a.d.c.d;
import com.agilebinary.a.a.a.d.m;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.b.a.a;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.commons.logging.Log;

public final class p implements c {

    /* renamed from: a  reason: collision with root package name */
    private final Log f75a = a.a(getClass());

    private static /* synthetic */ URI a(String str) {
        try {
            return new URI(str);
        } catch (URISyntaxException e) {
            throw new l(e.I("h\u0006W\tM\u0001EHS\rE\u0001S\rB\u001c\u0001=s!\u001bH") + str, e);
        }
    }

    private /* synthetic */ URI b(f fVar, j jVar, k kVar) {
        URI uri;
        if (jVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.b.e.I(")-5)A\u000b\u0004\n\u0011\u0016\u000f\n\u0004Y\f\u0018\u0018Y\u000f\u0016\u0015Y\u0003\u001cA\u0017\u0014\u0015\r"));
        }
        t c = jVar.c(e.I("M\u0007B\tU\u0001N\u0006"));
        if (c == null) {
            throw new l(com.agilebinary.a.a.a.c.b.e.I("3\u001c\u0002\u001c\b\u000f\u0004\u001dA\u000b\u0004\u001d\b\u000b\u0004\u001a\u0015Y\u0013\u001c\u0012\t\u000e\u0017\u0012\u001cA") + jVar.a() + e.I("HC\u001dUHO\u0007\u0001\u0004N\u000b@\u001cH\u0007OHI\r@\fD\u001a"));
        }
        String b = c.b();
        if (this.f75a.isDebugEnabled()) {
            this.f75a.debug(com.agilebinary.a.a.a.c.b.e.I("+\u0004\u001d\b\u000b\u0004\u001a\u0015Y\u0013\u001c\u0010\f\u0004\n\u0015\u001c\u0005Y\u0015\u0016A\u0015\u000e\u001a\u0000\r\b\u0016\u000fYF") + b + e.I("O"));
        }
        URI a2 = a(b);
        com.agilebinary.a.a.a.e.e g = jVar.g();
        if (!a2.isAbsolute()) {
            if (g.c(com.agilebinary.a.a.a.c.b.e.I("\u0011\u0015\r\u0011W\u0011\u000b\u000e\r\u000e\u001a\u000e\u0015O\u000b\u0004\u0013\u0004\u001a\u0015T\u0013\u001c\r\u0018\u0015\u0010\u0017\u001cL\u000b\u0004\u001d\b\u000b\u0004\u001a\u0015"))) {
                throw new l(e.I("s\rM\tU\u0001W\r\u0001\u001aD\fH\u001aD\u000bUHM\u0007B\tU\u0001N\u0006\u0001O") + a2 + com.agilebinary.a.a.a.c.b.e.I("FY\u000f\u0016\u0015Y\u0000\u0015\r\u0016\u0016\u001c\u0005"));
            }
            b bVar = (b) kVar.a(e.I("I\u001cU\u0018\u000f\u001c@\u001aF\rU7I\u0007R\u001c"));
            if (bVar == null) {
                throw new IllegalStateException(com.agilebinary.a.a.a.c.b.e.I("5\u0018\u0013\u001e\u0004\rA\u0011\u000e\n\u0015Y\u000f\u0016\u0015Y\u0000\u000f\u0000\u0010\r\u0018\u0003\u0015\u0004Y\b\u0017A\r\t\u001cA15-1Y\u0002\u0016\u000f\r\u0004\u0001\u0015"));
            }
            try {
                a2 = com.agilebinary.a.a.a.d.a.b.a(com.agilebinary.a.a.a.d.a.b.a(new URI(fVar.a().c()), bVar, true), a2);
            } catch (URISyntaxException e) {
                throw new l(e.getMessage(), e);
            }
        }
        if (g.d(e.I("I\u001cU\u0018\u000f\u0018S\u0007U\u0007B\u0007MF@\u0004M\u0007VEB\u0001S\u000bT\u0004@\u001a\f\u001aD\fH\u001aD\u000bU\u001b"))) {
            r rVar = (r) kVar.a(com.agilebinary.a.a.a.c.b.e.I("\u0011\u0015\r\u0011W\u0011\u000b\u000e\r\u000e\u001a\u000e\u0015O\u000b\u0004\u001d\b\u000b\u0004\u001a\u0015T\r\u0016\u0002\u0018\u0015\u0010\u000e\u0017\u0012"));
            if (rVar == null) {
                rVar = new r();
                kVar.a(e.I("I\u001cU\u0018\u000f\u0018S\u0007U\u0007B\u0007MFS\rE\u0001S\rB\u001c\f\u0004N\u000b@\u001cH\u0007O\u001b"), rVar);
            }
            if (a2.getFragment() != null) {
                try {
                    uri = com.agilebinary.a.a.a.d.a.b.a(a2, new b(a2.getHost(), a2.getPort(), a2.getScheme()), true);
                } catch (URISyntaxException e2) {
                    throw new l(e2.getMessage(), e2);
                }
            } else {
                uri = a2;
            }
            if (rVar.a(uri)) {
                throw new m(com.agilebinary.a.a.a.c.b.e.I(":\b\u000b\u0002\f\r\u0018\u0013Y\u0013\u001c\u0005\u0010\u0013\u001c\u0002\rA\r\u000eYF") + uri + e.I("O"));
            }
            rVar.b(uri);
        }
        return a2;
    }

    public final com.agilebinary.a.a.a.d.c.b a(f fVar, j jVar, k kVar) {
        URI b = b(fVar, jVar, kVar);
        return fVar.a().a().equalsIgnoreCase(com.agilebinary.a.a.a.c.b.e.I("1$8%")) ? new d(b) : new com.agilebinary.a.a.a.d.c.a(b);
    }

    public final boolean a(f fVar, j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException(e.I(" u<qHS\rR\u0018N\u0006R\r\u0001\u0005@\u0011\u0001\u0006N\u001c\u0001\nDHO\u001dM\u0004"));
        }
        int b = jVar.a().b();
        String a2 = fVar.a().a();
        t c = jVar.c(com.agilebinary.a.a.a.c.b.e.I("\u0015\u000e\u001a\u0000\r\b\u0016\u000f"));
        switch (b) {
            case 301:
            case 307:
                return a2.equalsIgnoreCase(e.I("/d<")) || a2.equalsIgnoreCase(com.agilebinary.a.a.a.c.b.e.I("1$8%"));
            case 302:
                return (a2.equalsIgnoreCase(e.I("/d<")) || a2.equalsIgnoreCase(com.agilebinary.a.a.a.c.b.e.I("1$8%"))) && c != null;
            case 303:
                return true;
            case 304:
            case 305:
            case 306:
            default:
                return false;
        }
    }
}
