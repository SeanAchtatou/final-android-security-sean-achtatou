package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.a.b;
import com.agilebinary.a.a.b.a.a;
import java.util.LinkedList;
import java.util.Queue;
import org.apache.commons.logging.Log;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final Log f37a = a.a(getClass());
    private com.agilebinary.a.a.a.h.c.c b;
    private int c;
    private b d;
    private LinkedList e;
    private Queue f;
    private int g;

    public c(com.agilebinary.a.a.a.h.c.c cVar, b bVar) {
        this.b = cVar;
        this.d = bVar;
        this.c = bVar.a(cVar);
        this.e = new LinkedList();
        this.f = new LinkedList();
        this.g = 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.a.a.a.c.b.a.g a(java.lang.Object r5) {
        /*
            r4 = this;
            java.util.LinkedList r0 = r4.e
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0034
            java.util.LinkedList r0 = r4.e
            java.util.LinkedList r1 = r4.e
            int r1 = r1.size()
            java.util.ListIterator r1 = r0.listIterator(r1)
        L_0x0014:
            boolean r0 = r1.hasPrevious()
            if (r0 == 0) goto L_0x0034
            java.lang.Object r0 = r1.previous()
            com.agilebinary.a.a.a.c.b.a.g r0 = (com.agilebinary.a.a.a.c.b.a.g) r0
            java.lang.Object r2 = r0.a()
            if (r2 == 0) goto L_0x0030
            java.lang.Object r2 = r0.a()
            boolean r2 = com.agilebinary.a.a.a.i.f.a(r5, r2)
            if (r2 == 0) goto L_0x0014
        L_0x0030:
            r1.remove()
        L_0x0033:
            return r0
        L_0x0034:
            int r0 = r4.d()
            if (r0 != 0) goto L_0x0062
            java.util.LinkedList r0 = r4.e
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0062
            java.util.LinkedList r0 = r4.e
            java.lang.Object r0 = r0.remove()
            com.agilebinary.a.a.a.c.b.a.g r0 = (com.agilebinary.a.a.a.c.b.a.g) r0
            r0.b()
            com.agilebinary.a.a.a.h.g r1 = r0.c()
            r1.k()     // Catch:{ IOException -> 0x0055 }
            goto L_0x0033
        L_0x0055:
            r1 = move-exception
            org.apache.commons.logging.Log r2 = r4.f37a
            java.lang.String r3 = "e`coI=^ ^oO#C<E!KoO B!I,X&C!"
            java.lang.String r3 = org.b.a.a.c.I(r3)
            r2.debug(r3, r1)
            goto L_0x0033
        L_0x0062:
            r0 = 0
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.a.c.a(java.lang.Object):com.agilebinary.a.a.a.c.b.a.g");
    }

    public final com.agilebinary.a.a.a.h.c.c a() {
        return this.b;
    }

    public final void a(g gVar) {
        if (this.g <= 0) {
            throw new IllegalStateException(com.agilebinary.a.a.a.i.b.I("x]\u0016WXFDK\u0016QDWWFSV\u0016TY@\u0016F^[E\u0012F]Y^\u0018\u0012") + this.b);
        } else if (this.g <= this.e.size()) {
            throw new IllegalStateException(org.b.a.a.c.I("\u0001CoI!X=UoM#@ O.X*HoJ=C\"\f;D&_o\\ C#\u0002o") + this.b);
        } else {
            this.e.add(gVar);
        }
    }

    public final void a(k kVar) {
        if (kVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.i.b.I("aS_F_\\Q\u0012BZDWWV\u0016_CAB\u0012X]B\u0012TW\u0016\\C^Z\u001c"));
        }
        this.f.add(kVar);
    }

    public final int b() {
        return this.c;
    }

    public final void b(g gVar) {
        if (!this.b.equals(gVar.d())) {
            throw new IllegalArgumentException(org.b.a.a.c.I("\nB;^6\f!C;\f?@.B!I+\f)C=\f;D&_o\\ C#\u0002E\\ C#\u0016o") + this.b + com.agilebinary.a.a.a.i.b.I("8F^W\\\f\u0012") + gVar.d());
        }
        this.g++;
    }

    public final void b(k kVar) {
        if (kVar != null) {
            this.f.remove(kVar);
        }
    }

    public final boolean c() {
        return this.g <= 0 && this.f.isEmpty();
    }

    public final boolean c(g gVar) {
        boolean remove = this.e.remove(gVar);
        if (remove) {
            this.g--;
        }
        return remove;
    }

    public final int d() {
        return this.d.a(this.b) - this.g;
    }

    public final void e() {
        if (this.g <= 0) {
            throw new IllegalStateException(org.b.a.a.c.I("x'I=IoE<\f!CoI!X=UoX'M;\f,C:@+\f-IoH=C?\\*Ha"));
        }
        this.g--;
    }

    public final boolean f() {
        return !this.f.isEmpty();
    }

    public final k g() {
        return (k) this.f.peek();
    }
}
