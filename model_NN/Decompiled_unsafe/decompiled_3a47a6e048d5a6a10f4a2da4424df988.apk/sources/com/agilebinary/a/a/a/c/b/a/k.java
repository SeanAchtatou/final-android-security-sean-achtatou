package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.c.c.m;
import com.agilebinary.mobilemonitor.client.a.a.c;
import java.util.Date;
import java.util.concurrent.locks.Condition;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    private final Condition f42a;
    private final c b;
    private Thread c;
    private boolean d;

    public k(Condition condition, c cVar) {
        if (condition == null) {
            throw new IllegalArgumentException(m.I("\\pq{vkvpq?rjlk?qpk?}z?qjss1"));
        }
        this.f42a = condition;
        this.b = cVar;
    }

    public final void a() {
        if (this.c == null) {
            throw new IllegalStateException(c.I("$.\b.\u000e8J6\u000b(\u001e(\u0004&J.\u0004a\u001e)\u00032J.\b+\u000f\"\u001eo"));
        }
        this.f42a.signalAll();
    }

    public final boolean a(Date date) {
        boolean z;
        if (this.c != null) {
            throw new IllegalStateException(m.I("^?kwmz~{?vl?~smz~{f?h~vkvqx?pq?kwvl?p}uz|k1\u0015|~sszm%?") + Thread.currentThread() + c.I("K\u001d \u00035\u000f3Pa") + this.c);
        } else if (this.d) {
            throw new InterruptedException(m.I("Pozm~kvpq?vqkzmmjokz{"));
        } else {
            this.c = Thread.currentThread();
            if (date != null) {
                try {
                    z = this.f42a.awaitUntil(date);
                } catch (Throwable th) {
                    this.c = null;
                    throw th;
                }
            } else {
                this.f42a.await();
                z = true;
            }
            if (this.d) {
                throw new InterruptedException(c.I("\u000e\u001a$\u0018 \u001e(\u0005/J(\u00045\u000f3\u00184\u001a5\u000f%"));
            }
            this.c = null;
            return z;
        }
    }

    public final void b() {
        this.d = true;
        this.f42a.signalAll();
    }
}
