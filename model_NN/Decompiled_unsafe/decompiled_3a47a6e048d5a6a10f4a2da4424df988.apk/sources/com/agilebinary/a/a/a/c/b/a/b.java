package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.a.c;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.m;
import com.agilebinary.a.a.b.a.a;
import com.agilebinary.mobilemonitor.client.android.c.e;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public final class b implements k {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Log f36a;
    private h b;
    private j c;
    private a d;
    private m e;
    private c f;

    public b(h hVar, TimeUnit timeUnit) {
        if (hVar == null) {
            throw new IllegalArgumentException(e.I(" U\u001bS\u001eSSD\u0016Q\u001aE\u0007D\n\u0016\u001eW\n\u0016\u001dY\u0007\u0016\u0011SSX\u0006Z\u001f"));
        }
        this.f36a = a.a(getClass());
        this.b = hVar;
        this.f = new c();
        this.e = new com.agilebinary.a.a.a.c.b.b(hVar);
        this.d = new a(this.e, this.f, timeUnit);
        this.c = this.d;
    }

    public final h a() {
        return this.b;
    }

    public final com.agilebinary.a.a.a.h.b a(com.agilebinary.a.a.a.h.c.c cVar, Object obj) {
        return new h(this, new i(this.d, new f(), cVar, obj), cVar);
    }

    public final void a(long j, TimeUnit timeUnit) {
        if (this.f36a.isDebugEnabled()) {
            this.f36a.debug(org.osmdroid.b.a.I("\u001d 1?7\"9l=#0\";/*%1\"-l7(2)~ 1\"9),l*$?\"~") + j + e.I("S") + timeUnit);
        }
        this.d.a(j, timeUnit);
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x0044=Splitter:B:21:0x0044, B:39:0x008c=Splitter:B:39:0x008c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.h.a r9, long r10, java.util.concurrent.TimeUnit r12) {
        /*
            r8 = this;
            boolean r0 = r9 instanceof com.agilebinary.a.a.a.c.b.a.d
            if (r0 != 0) goto L_0x0010
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "\u001d#0\";/*%1\"~/2--?~!7?3-*/6`~/1\"0)=87#0l0#*l1.*-7\";(~*,#3l*$7?~!?\"?+;>p"
            java.lang.String r1 = org.osmdroid.b.a.I(r1)
            r0.<init>(r1)
            throw r0
        L_0x0010:
            com.agilebinary.a.a.a.c.b.a.d r9 = (com.agilebinary.a.a.a.c.b.a.d) r9
            com.agilebinary.a.a.a.c.b.a r0 = r9.i()
            if (r0 == 0) goto L_0x002a
            com.agilebinary.a.a.a.h.k r0 = r9.h()
            if (r0 == r8) goto L_0x002a
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "u\u001cX\u001dS\u0010B\u001aY\u001d\u0016\u001dY\u0007\u0016\u001cT\u0007W\u001aX\u0016RSP\u0001Y\u001e\u0016\u0007^\u001aES[\u0012X\u0012Q\u0016D]"
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.c.e.I(r1)
            r0.<init>(r1)
            throw r0
        L_0x002a:
            monitor-enter(r9)
            com.agilebinary.a.a.a.c.b.a r2 = r9.i()     // Catch:{ all -> 0x0069 }
            com.agilebinary.a.a.a.c.b.a.g r2 = (com.agilebinary.a.a.a.c.b.a.g) r2     // Catch:{ all -> 0x0069 }
            if (r2 != 0) goto L_0x0035
            monitor-exit(r9)     // Catch:{ all -> 0x0069 }
        L_0x0034:
            return
        L_0x0035:
            boolean r0 = r9.l()     // Catch:{ IOException -> 0x0078 }
            if (r0 == 0) goto L_0x0044
            boolean r0 = r9.r()     // Catch:{ IOException -> 0x0078 }
            if (r0 != 0) goto L_0x0044
            r9.m()     // Catch:{ IOException -> 0x0078 }
        L_0x0044:
            boolean r3 = r9.r()     // Catch:{ all -> 0x0069 }
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x0069 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0069 }
            if (r0 == 0) goto L_0x005d
            if (r3 == 0) goto L_0x006c
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x0069 }
            java.lang.String r1 = "\u001e; ;--):l=#0\";/*%1\"~%-l,)+??.2)p"
            java.lang.String r1 = org.osmdroid.b.a.I(r1)     // Catch:{ all -> 0x0069 }
            r0.debug(r1)     // Catch:{ all -> 0x0069 }
        L_0x005d:
            r9.j()     // Catch:{ all -> 0x0069 }
            com.agilebinary.a.a.a.c.b.a.a r1 = r8.d     // Catch:{ all -> 0x0069 }
            r4 = r10
            r6 = r12
            r1.a(r2, r3, r4, r6)     // Catch:{ all -> 0x0069 }
        L_0x0067:
            monitor-exit(r9)     // Catch:{ all -> 0x0069 }
            goto L_0x0034
        L_0x0069:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0069 }
            throw r0
        L_0x006c:
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x0069 }
            java.lang.String r1 = "d\u0016Z\u0016W\u0000S\u0017\u0016\u0010Y\u001dX\u0016U\u0007_\u001cXS_\u0000\u0016\u001dY\u0007\u0016\u0001S\u0006E\u0012T\u001fS]"
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.c.e.I(r1)     // Catch:{ all -> 0x0069 }
            r0.debug(r1)     // Catch:{ all -> 0x0069 }
            goto L_0x005d
        L_0x0078:
            r0 = move-exception
            org.apache.commons.logging.Log r1 = r8.f36a     // Catch:{ all -> 0x00bc }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x00bc }
            if (r1 == 0) goto L_0x008c
            org.apache.commons.logging.Log r1 = r8.f36a     // Catch:{ all -> 0x00bc }
            java.lang.String r3 = "\t&/;<*%1\"~?69*87\"9l:#)\"~>; ;--):l=#0\";/*%1\"p"
            java.lang.String r3 = org.osmdroid.b.a.I(r3)     // Catch:{ all -> 0x00bc }
            r1.debug(r3, r0)     // Catch:{ all -> 0x00bc }
        L_0x008c:
            boolean r3 = r9.r()     // Catch:{ all -> 0x0069 }
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x0069 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0069 }
            if (r0 == 0) goto L_0x00a5
            if (r3 == 0) goto L_0x00b0
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x0069 }
            java.lang.String r1 = "d\u0016Z\u0016W\u0000S\u0017\u0016\u0010Y\u001dX\u0016U\u0007_\u001cXS_\u0000\u0016\u0001S\u0006E\u0012T\u001fS]"
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.c.e.I(r1)     // Catch:{ all -> 0x0069 }
            r0.debug(r1)     // Catch:{ all -> 0x0069 }
        L_0x00a5:
            r9.j()     // Catch:{ all -> 0x0069 }
            com.agilebinary.a.a.a.c.b.a.a r1 = r8.d     // Catch:{ all -> 0x0069 }
            r4 = r10
            r6 = r12
            r1.a(r2, r3, r4, r6)     // Catch:{ all -> 0x0069 }
            goto L_0x0067
        L_0x00b0:
            org.apache.commons.logging.Log r0 = r8.f36a     // Catch:{ all -> 0x0069 }
            java.lang.String r1 = "\u001e; ;--):l=#0\";/*%1\"~%-l0#*l,)+??.2)p"
            java.lang.String r1 = org.osmdroid.b.a.I(r1)     // Catch:{ all -> 0x0069 }
            r0.debug(r1)     // Catch:{ all -> 0x0069 }
            goto L_0x00a5
        L_0x00bc:
            r0 = move-exception
            boolean r3 = r9.r()     // Catch:{ all -> 0x0069 }
            org.apache.commons.logging.Log r1 = r8.f36a     // Catch:{ all -> 0x0069 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x0069 }
            if (r1 == 0) goto L_0x00d6
            if (r3 == 0) goto L_0x00e1
            org.apache.commons.logging.Log r1 = r8.f36a     // Catch:{ all -> 0x0069 }
            java.lang.String r4 = "d\u0016Z\u0016W\u0000S\u0017\u0016\u0010Y\u001dX\u0016U\u0007_\u001cXS_\u0000\u0016\u0001S\u0006E\u0012T\u001fS]"
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.e.I(r4)     // Catch:{ all -> 0x0069 }
            r1.debug(r4)     // Catch:{ all -> 0x0069 }
        L_0x00d6:
            r9.j()     // Catch:{ all -> 0x0069 }
            com.agilebinary.a.a.a.c.b.a.a r1 = r8.d     // Catch:{ all -> 0x0069 }
            r4 = r10
            r6 = r12
            r1.a(r2, r3, r4, r6)     // Catch:{ all -> 0x0069 }
            throw r0     // Catch:{ all -> 0x0069 }
        L_0x00e1:
            org.apache.commons.logging.Log r1 = r8.f36a     // Catch:{ all -> 0x0069 }
            java.lang.String r4 = "\u001e; ;--):l=#0\";/*%1\"~%-l0#*l,)+??.2)p"
            java.lang.String r4 = org.osmdroid.b.a.I(r4)     // Catch:{ all -> 0x0069 }
            r1.debug(r4)     // Catch:{ all -> 0x0069 }
            goto L_0x00d6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.a.b.a(com.agilebinary.a.a.a.h.a, long, java.util.concurrent.TimeUnit):void");
    }

    public final void b() {
        this.d.b();
    }

    public final void c() {
        this.f.a(3);
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        try {
            this.f36a.debug(e.I(" ^\u0006B\u0007_\u001dQSR\u001cA\u001d"));
            this.d.a();
        } finally {
            super.finalize();
        }
    }
}
