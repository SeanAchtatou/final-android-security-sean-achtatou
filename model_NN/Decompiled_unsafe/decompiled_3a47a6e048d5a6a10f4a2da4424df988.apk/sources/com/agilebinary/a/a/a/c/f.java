package com.agilebinary.a.a.a.c;

import com.agilebinary.a.a.a.ae;
import com.agilebinary.a.a.a.c.c.c;
import com.agilebinary.a.a.a.c.c.h;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.j.a;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

public class f extends a implements ae {

    /* renamed from: a  reason: collision with root package name */
    private volatile boolean f83a;
    private volatile Socket b = null;

    /* access modifiers changed from: protected */
    public a a(Socket socket, int i, e eVar) {
        return new c(socket, i, eVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (!this.f83a) {
            throw new IllegalStateException(com.agilebinary.a.a.a.c.c.e.I("b\u0007O\u0006D\u000bU\u0001N\u0006\u0001\u0001RHO\u0007UHN\u0018D\u0006"));
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Socket socket, e eVar) {
        if (socket == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.e.c.I("[NkJmU(LiX(OgU(Cm\u0001fTdM"));
        } else if (eVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I(" u<qHQ\tS\tL\rU\rS\u001b\u0001\u0005@\u0011\u0001\u0006N\u001c\u0001\nDHO\u001dM\u0004"));
        } else {
            this.b = socket;
            if (eVar == null) {
                throw new IllegalArgumentException(com.agilebinary.a.a.a.e.c.I("i\\uX\u0001x@z@eD|DzR(LiX(OgU(Cm\u0001fTdM"));
            }
            int a2 = eVar.a(com.agilebinary.a.a.a.c.c.e.I("\u0000U\u001cQFR\u0007B\u0003D\u001c\u000f\nT\u000eG\rSER\u0001[\r"), -1);
            a(a(socket, a2, eVar), b(socket, a2, eVar), eVar);
            this.f83a = true;
        }
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.a.j.e b(Socket socket, int i, e eVar) {
        return new h(socket, i, eVar);
    }

    public final void b(int i) {
        a();
        if (this.b != null) {
            try {
                this.b.setSoTimeout(i);
            } catch (SocketException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (this.f83a) {
            throw new IllegalStateException(com.agilebinary.a.a.a.e.c.I("KNfOmB|HgO(H{\u0001iMzDiEq\u0001gQmO"));
        }
    }

    /* access modifiers changed from: protected */
    public Socket i_() {
        return this.b;
    }

    public void k() {
        if (this.f83a) {
            this.f83a = false;
            Socket socket = this.b;
            try {
                b();
                try {
                    socket.shutdownOutput();
                } catch (IOException e) {
                }
                try {
                    socket.shutdownInput();
                } catch (IOException | UnsupportedOperationException e2) {
                }
            } finally {
                socket.close();
            }
        }
    }

    public final boolean l() {
        return this.f83a;
    }

    public void m() {
        this.f83a = false;
        Socket socket = this.b;
        if (socket != null) {
            socket.close();
        }
    }

    public final InetAddress p() {
        if (this.b != null) {
            return this.b.getInetAddress();
        }
        return null;
    }

    public final int q() {
        if (this.b != null) {
            return this.b.getPort();
        }
        return -1;
    }
}
