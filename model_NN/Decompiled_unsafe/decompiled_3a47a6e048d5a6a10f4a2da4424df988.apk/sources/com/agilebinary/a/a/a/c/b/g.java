package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.c.h;
import java.net.InetAddress;
import org.osmdroid.b.a;

public final class g implements h {

    /* renamed from: a  reason: collision with root package name */
    private com.agilebinary.a.a.a.h.b.h f46a;

    public g(com.agilebinary.a.a.a.h.b.h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException(a.I("\u001f=$;!;\u001e;+7?*>'l39-8~\"18~.;l092 p"));
        }
        this.f46a = hVar;
    }

    public final c a(b bVar, f fVar) {
        if (fVar == null) {
            throw new IllegalStateException(org.b.a.a.c.I("\u001dI>Y*_;\f\"Y<XoB XoN*\f!Y#@a"));
        }
        c b = com.agilebinary.a.a.a.h.a.a.b(fVar.g());
        if (b != null) {
            return b;
        }
        if (bVar == null) {
            throw new IllegalStateException(a.I("\n-,+;8~$1?*l39-8~\"18~.;l092 p"));
        }
        InetAddress c = com.agilebinary.a.a.a.h.a.a.c(fVar.g());
        b a2 = com.agilebinary.a.a.a.h.a.a.a(fVar.g());
        boolean d = this.f46a.a(bVar.c()).d();
        return a2 == null ? new c(bVar, c, d) : new c(bVar, c, a2, d);
    }
}
