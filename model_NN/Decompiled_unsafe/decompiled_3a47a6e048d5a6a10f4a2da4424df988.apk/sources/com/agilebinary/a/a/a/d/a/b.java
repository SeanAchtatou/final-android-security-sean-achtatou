package com.agilebinary.a.a.a.d.a;

import com.agilebinary.a.a.a.c.b.a.f;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Stack;

public final class b {
    private /* synthetic */ b() {
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'D');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'g');
            i2 = i;
        }
        return new String(cArr);
    }

    private static /* synthetic */ String a(String str) {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(str.length());
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt != '/') {
                sb.append(charAt);
                z = false;
            } else if (!z) {
                sb.append(charAt);
                z = true;
            }
        }
        return sb.toString();
    }

    private static /* synthetic */ URI a(String str, String str2, int i, String str3, String str4, String str5) {
        StringBuilder sb = new StringBuilder();
        if (str2 != null) {
            if (str != null) {
                sb.append(str);
                sb.append(f.I("09%"));
            }
            sb.append(str2);
            if (i > 0) {
                sb.append(':');
                sb.append(i);
            }
        }
        if (str3 == null || !str3.startsWith("/")) {
            sb.append('/');
        }
        if (str3 != null) {
            sb.append(str3);
        }
        if (str4 != null) {
            sb.append('?');
            sb.append(str4);
        }
        if (str5 != null) {
            sb.append('#');
            sb.append(str5);
        }
        return new URI(sb.toString());
    }

    private static /* synthetic */ URI a(URI uri) {
        String path = uri.getPath();
        if (path == null || path.indexOf(com.agilebinary.a.a.a.i.b.I("\u0019\u001c")) == -1) {
            return uri;
        }
        String[] split = path.split("/");
        Stack stack = new Stack();
        for (int i = 0; i < split.length; i++) {
            if (split[i].length() != 0 && !f.I("$").equals(split[i])) {
                if (!com.agilebinary.a.a.a.i.b.I("\u0018\u001c").equals(split[i])) {
                    stack.push(split[i]);
                } else if (!stack.isEmpty()) {
                    stack.pop();
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = stack.iterator();
        while (it.hasNext()) {
            sb.append('/').append((String) it.next());
        }
        try {
            return new URI(uri.getScheme(), uri.getAuthority(), sb.toString(), uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static URI a(URI uri, com.agilebinary.a.a.a.b bVar) {
        return a(uri, bVar, false);
    }

    public static URI a(URI uri, com.agilebinary.a.a.a.b bVar, boolean z) {
        if (uri == null) {
            throw new IllegalArgumentException(f.I("_DC6gws6dyx6hs*xzf"));
        } else if (bVar != null) {
            return a(bVar.c(), bVar.a(), bVar.b(), a(uri.getRawPath()), uri.getRawQuery(), z ? null : uri.getRawFragment());
        } else {
            return a(null, null, -1, a(uri.getRawPath()), uri.getRawQuery(), z ? null : uri.getRawFragment());
        }
    }

    public static URI a(URI uri, URI uri2) {
        if (uri == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.i.b.I("tSEW\u0016gd{\u0016_WK\u0016\\Y@\u0016PS\u0012XGZ^"));
        } else if (uri2 == null) {
            throw new IllegalArgumentException(f.I("Xslsxsduo6_DC6gws6dyx6hs*xzf"));
        } else {
            String uri3 = uri2.toString();
            if (uri3.startsWith(com.agilebinary.a.a.a.i.b.I("\r"))) {
                String uri4 = uri.toString();
                if (uri4.indexOf(63) >= 0) {
                    uri4 = uri4.substring(0, uri4.indexOf(63));
                }
                return URI.create(uri4 + uri2.toString());
            }
            boolean z = uri3.length() == 0;
            if (z) {
                uri2 = URI.create(f.I(")"));
            }
            URI resolve = uri.resolve(uri2);
            if (z) {
                String uri5 = resolve.toString();
                resolve = URI.create(uri5.substring(0, uri5.indexOf(35)));
            }
            return a(resolve);
        }
    }
}
