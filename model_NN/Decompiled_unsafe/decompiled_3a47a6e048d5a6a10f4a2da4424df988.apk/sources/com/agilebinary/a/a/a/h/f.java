package com.agilebinary.a.a.a.h;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.c.a.k;
import com.agilebinary.mobilemonitor.client.android.b.g;
import java.net.ConnectException;

public final class f extends ConnectException {

    /* renamed from: a  reason: collision with root package name */
    private final b f113a;

    public f(b bVar, ConnectException connectException) {
        super(g.I("`\u001dM\u001cF\u0011W\u001bL\u001c\u0003\u0006LR") + bVar + k.I("I8\f,\u001c9\f."));
        this.f113a = bVar;
        initCause(connectException);
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'y');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '5');
            i2 = i;
        }
        return new String(cArr);
    }
}
