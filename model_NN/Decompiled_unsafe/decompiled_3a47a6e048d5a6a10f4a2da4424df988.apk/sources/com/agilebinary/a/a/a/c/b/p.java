package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.c.c.f;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.n;
import com.agilebinary.a.a.a.x;
import com.agilebinary.a.a.b.a.a;
import org.apache.commons.logging.Log;

public final class p extends f {
    private final Log b = a.a(getClass());
    private final i c;
    private final c d;
    private final int e;

    public p(com.agilebinary.a.a.a.j.a aVar, i iVar, e eVar) {
        super(aVar, eVar);
        if (iVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.h.f.I("g\u001cF\tZ\u0017F\u001c\u0015\u001fT\u001aA\u0016G\u0000\u0015\u0014T\u0000\u0015\u0017Z\r\u0015\u001bPY[\fY\u0015"));
        }
        this.c = iVar;
        this.d = new c(128);
        this.e = eVar.a(com.agilebinary.a.a.a.c.e.c.I("rDn@4Su^tUyDs_t\u001ewQb\u001diD{DoC7\\s^\u001d}QhR{W"), Integer.MAX_VALUE);
    }

    /* access modifiers changed from: protected */
    public final x a(com.agilebinary.a.a.a.j.a aVar) {
        int i = 0;
        while (true) {
            this.d.a();
            int a2 = aVar.a(this.d);
            if (a2 == -1 && i == 0) {
                throw new n(com.agilebinary.a.a.a.h.f.I("-]\u001c\u0015\rT\u000bR\u001cAYF\u001cG\u000fP\u000b\u0015\u001fT\u0010Y\u001cQYA\u0016\u0015\u000bP\nE\u0016[\u001d"));
            }
            com.agilebinary.a.a.a.b.i iVar = new com.agilebinary.a.a.a.b.i(0, this.d.c());
            if (this.f61a.a(this.d, iVar)) {
                return this.c.a(this.f61a.b(this.d, iVar));
            } else if (a2 != -1 && i < this.e) {
                if (this.b.isDebugEnabled()) {
                    this.b.debug(com.agilebinary.a.a.a.h.f.I(">T\u000bW\u0018R\u001c\u0015\u0010[YG\u001cF\tZ\u0017F\u001c\u000fY") + this.d.toString());
                }
                i++;
            }
        }
        throw new l(com.agilebinary.a.a.a.c.e.c.I("NX\u0010iUhFB:V{YvU~\u0010n_:BCj_tT:GsDr\u0010{\u0010lQvY~\u0010RdN`:BCj_tC"));
    }
}
