package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.f.d;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import com.agilebinary.mobilemonitor.client.android.c.e;
import java.util.ArrayList;

public final class f implements j {

    /* renamed from: a  reason: collision with root package name */
    public static final f f11a = new f();
    private static final char[] b = {';', ','};

    public static o a(c cVar, i iVar, char[] cArr) {
        boolean z;
        String str;
        boolean z2;
        int i;
        if (cVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.e.c.I("b`@z\u0001iSz@q\u0001jTnGmS(LiX(OgU(Cm\u0001fTdM"));
        } else if (iVar == null) {
            throw new IllegalArgumentException(e.I("#W\u0001E\u0016DSU\u0006D\u0000Y\u0001\u0016\u001eW\n\u0016\u001dY\u0007\u0016\u0011SSX\u0006Z\u001f"));
        } else {
            int b2 = iVar.b();
            int b3 = iVar.b();
            int a2 = iVar.a();
            while (true) {
                if (b2 < a2) {
                    char a3 = cVar.a(b2);
                    if (a3 == '=') {
                        break;
                    } else if (a(a3, cArr)) {
                        z = true;
                        break;
                    } else {
                        b2++;
                    }
                } else {
                    break;
                }
            }
            z = false;
            if (b2 == a2) {
                z = true;
                str = cVar.b(b3, a2);
            } else {
                String b4 = cVar.b(b3, b2);
                b2++;
                str = b4;
            }
            if (z) {
                iVar.a(b2);
                return a(str, (String) null);
            }
            boolean z3 = false;
            boolean z4 = false;
            int i2 = b2;
            while (true) {
                if (i2 < a2) {
                    char a4 = cVar.a(i2);
                    boolean z5 = (a4 != '\"' || z4) ? z3 : !z3;
                    if (!z5 && !z4 && a(a4, cArr)) {
                        z2 = true;
                        i = b2;
                        break;
                    }
                    i2++;
                    z4 = !z4 && z5 && a4 == '\\';
                    z3 = z5;
                } else {
                    z2 = z;
                    i = b2;
                    break;
                }
            }
            while (i < i2 && d.a(cVar.a(i))) {
                i++;
            }
            int i3 = i2;
            while (i3 > i && d.a(cVar.a(i3 - 1))) {
                i3--;
            }
            if (i3 - i >= 2 && cVar.a(i) == '\"' && cVar.a(i3 - 1) == '\"') {
                i++;
                i3--;
            }
            String a5 = cVar.a(i, i3);
            iVar.a(z2 ? i2 + 1 : i2);
            return a(str, a5);
        }
    }

    private static /* synthetic */ o a(String str, String str2) {
        return new o(str, str2);
    }

    private static /* synthetic */ boolean a(char c, char[] cArr) {
        if (cArr == null) {
            return false;
        }
        for (char c2 : cArr) {
            if (c == c2) {
                return true;
            }
        }
        return false;
    }

    public static final m[] a(String str) {
        if (str == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.e.c.I("^@dTm\u0001|N(QiS{D(LiX(OgU(Cm\u0001fTdM"));
        }
        f fVar = f11a;
        c cVar = new c(str.length());
        cVar.a(str);
        return fVar.a(cVar, new i(0, str.length()));
    }

    private /* synthetic */ o[] c(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(e.I("0^\u0012DSW\u0001D\u0012OST\u0006P\u0015S\u0001\u0016\u001eW\n\u0016\u001dY\u0007\u0016\u0011SSX\u0006Z\u001f"));
        } else if (iVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.e.c.I("qiS{Dz\u0001kTzRgS(LiX(OgU(Cm\u0001fTdM"));
        } else {
            int b2 = iVar.b();
            int a2 = iVar.a();
            while (b2 < a2 && d.a(cVar.a(b2))) {
                b2++;
            }
            iVar.a(b2);
            if (iVar.c()) {
                return new o[0];
            }
            ArrayList arrayList = new ArrayList();
            while (!iVar.c()) {
                arrayList.add(a(cVar, iVar, b));
                if (cVar.a(iVar.b() - 1) == ',') {
                    break;
                }
            }
            return (o[]) arrayList.toArray(new o[arrayList.size()]);
        }
    }

    public final m[] a(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(e.I("0^\u0012DSW\u0001D\u0012OST\u0006P\u0015S\u0001\u0016\u001eW\n\u0016\u001dY\u0007\u0016\u0011SSX\u0006Z\u001f"));
        } else if (iVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.e.c.I("qiS{Dz\u0001kTzRgS(LiX(OgU(Cm\u0001fTdM"));
        } else {
            ArrayList arrayList = new ArrayList();
            while (!iVar.c()) {
                m b2 = b(cVar, iVar);
                if (b2.a().length() != 0 || b2.b() != null) {
                    arrayList.add(b2);
                }
            }
            return (m[]) arrayList.toArray(new m[arrayList.size()]);
        }
    }

    public final m b(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(e.I("0^\u0012DSW\u0001D\u0012OST\u0006P\u0015S\u0001\u0016\u001eW\n\u0016\u001dY\u0007\u0016\u0011SSX\u0006Z\u001f"));
        } else if (iVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.e.c.I("qiS{Dz\u0001kTzRgS(LiX(OgU(Cm\u0001fTdM"));
        } else {
            o d = a(cVar, iVar, b);
            o[] oVarArr = null;
            if (!iVar.c() && cVar.a(iVar.b() - 1) != ',') {
                oVarArr = c(cVar, iVar);
            }
            return new a(d.a(), d.b(), oVarArr);
        }
    }
}
