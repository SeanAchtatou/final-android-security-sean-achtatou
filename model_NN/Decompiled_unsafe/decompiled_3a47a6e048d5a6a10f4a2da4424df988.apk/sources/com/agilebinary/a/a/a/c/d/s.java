package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.g;
import com.agilebinary.a.a.a.d.a;
import com.agilebinary.a.a.a.d.f;
import com.agilebinary.a.a.a.d.l;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f.b;
import com.agilebinary.a.a.a.f.c;
import com.agilebinary.a.a.a.h.c.h;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.n;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.p;
import com.agilebinary.a.a.a.t;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public final class s implements a {

    /* renamed from: a  reason: collision with root package name */
    private final Log f78a;
    private k b;
    private h c;
    private com.agilebinary.a.a.a.s d;
    private n e;
    private b f;
    private c g;
    private com.agilebinary.a.a.a.d.k h;
    private com.agilebinary.a.a.a.d.h i = null;
    private com.agilebinary.a.a.a.d.c j;
    private com.agilebinary.a.a.a.d.b k;
    private com.agilebinary.a.a.a.d.b l;
    private f m;
    private e n;
    private com.agilebinary.a.a.a.h.a o;
    private com.agilebinary.a.a.a.a.e p;
    private com.agilebinary.a.a.a.a.e q;
    private int r;
    private int s;
    private int t;
    private com.agilebinary.a.a.a.b u;

    public s(Log log, b bVar, k kVar, com.agilebinary.a.a.a.s sVar, n nVar, h hVar, c cVar, com.agilebinary.a.a.a.d.k kVar2, com.agilebinary.a.a.a.d.c cVar2, com.agilebinary.a.a.a.d.b bVar2, com.agilebinary.a.a.a.d.b bVar3, f fVar, e eVar) {
        if (log == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I("m\u0007FHL\tXHO\u0007UHC\r\u0001\u0006T\u0004MF"));
        } else if (bVar == null) {
            throw new IllegalArgumentException(org.c.a.a.a.a.I("j\\zH|Y/HwHlX{B}\rbLv\raB{\rmH/CzAc\u0003"));
        } else if (kVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I("b\u0004H\rO\u001c\u0001\u000bN\u0006O\rB\u001cH\u0007OHL\tO\tF\rSHL\tXHO\u0007UHC\r\u0001\u0006T\u0004MF"));
        } else if (sVar == null) {
            throw new IllegalArgumentException(org.c.a.a.a.a.I("LBaCjN{D`C/_jX|H/^{_nYjJv\rbLv\raB{\rmH/CzAc\u0003"));
        } else if (nVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I("+N\u0006O\rB\u001cH\u0007OHJ\rD\u0018\u0001\tM\u0001W\r\u0001\u001bU\u001a@\u001cD\u000fXHL\tXHO\u0007UHC\r\u0001\u0006T\u0004MF"));
        } else if (hVar == null) {
            throw new IllegalArgumentException(org.c.a.a.a.a.I("]BzYj\rAnCaH}\rbLv\raB{\rmH/CzAc\u0003"));
        } else if (cVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I("i<u8\u0001\u0018S\u0007U\u0007B\u0007MHQ\u001aN\u000bD\u001bR\u0007SHL\tXHO\u0007UHC\r\u0001\u0006T\u0004MF"));
        } else if (kVar2 == null) {
            throw new IllegalArgumentException(org.c.a.a.a.a.I("e[y_\r}H~Xj^{\r}H{_v\rgLaIcH}\rbLv\raB{\rmH/CzAc\u0003"));
        } else if (cVar2 == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I("s\rE\u0001S\rB\u001c\u0001\u001bU\u001a@\u001cD\u000fXHL\tXHO\u0007UHC\r\u0001\u0006T\u0004MF"));
        } else if (bVar2 == null) {
            throw new IllegalArgumentException(org.c.a.a.a.a.I("[L}JjY/LzYgHaYfNnYfBa\rgLaIcH}\rbLv\raB{\rmH/CzAc\u0003"));
        } else if (bVar3 == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I("8S\u0007Y\u0011\u0001\tT\u001cI\rO\u001cH\u000b@\u001cH\u0007OHI\tO\fM\rSHL\tXHO\u0007UHC\r\u0001\u0006T\u0004MF"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(org.c.a.a.a.a.I("x|H}\r{BdHa\rgLaIcH}\rbLv\raB{\rmH/CzAc\u0003"));
        } else if (eVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I(" u<qHQ\tS\tL\rU\rS\u001b\u0001\u0005@\u0011\u0001\u0006N\u001c\u0001\nDHO\u001dM\u0004"));
        } else {
            this.f78a = log;
            this.f = bVar;
            this.b = kVar;
            this.d = sVar;
            this.e = nVar;
            this.c = hVar;
            this.g = cVar;
            this.h = kVar2;
            this.j = cVar2;
            this.k = bVar2;
            this.l = bVar3;
            this.m = fVar;
            this.n = eVar;
            this.o = null;
            this.r = 0;
            this.s = 0;
            this.t = this.n.a(org.c.a.a.a.a.I("E{Y\u0003_`Y`N`A!@nU\"_jIf_jN{^"), 100);
            this.p = new com.agilebinary.a.a.a.a.e();
            this.q = new com.agilebinary.a.a.a.a.e();
        }
    }

    private static /* synthetic */ l a(com.agilebinary.a.a.a.f fVar) {
        return fVar instanceof p ? new k((p) fVar) : new l(fVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean */
    private /* synthetic */ m a(m mVar, j jVar, com.agilebinary.a.a.a.f.k kVar) {
        com.agilebinary.a.a.a.h.c.c b2 = mVar.b();
        l a2 = mVar.a();
        e g2 = a2.g();
        if (g2 == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I(" u<qHQ\tS\tL\rU\rS\u001b\u0001\u0005@\u0011\u0001\u0006N\u001c\u0001\nDHO\u001dM\u0004"));
        } else if (!g2.a(org.c.a.a.a.a.I("gY{]!]}B{BlBc\u0003gLaIcH\"_jIf_jN{^"), true) || !this.j.a(a2, jVar)) {
            com.agilebinary.a.a.a.d.e eVar = (com.agilebinary.a.a.a.d.e) kVar.a(org.c.a.a.a.a.I("gY{]!LzYg\u0003l_jIjC{DnA|\u0000_`[fIj_"));
            if (eVar != null && com.agilebinary.a.a.a.d.d.a.a(g2)) {
                if (this.k.a(jVar)) {
                    com.agilebinary.a.a.a.b bVar = (com.agilebinary.a.a.a.b) kVar.a(com.agilebinary.a.a.a.c.c.e.I("I\u001cU\u0018\u000f\u001c@\u001aF\rU7I\u0007R\u001c"));
                    com.agilebinary.a.a.a.b a3 = bVar == null ? b2.a() : bVar;
                    this.f78a.debug(org.c.a.a.a.a.I("yn_hH{\r}H~Xj^{Hk\rnX{EjC{DlL{D`C"));
                    try {
                        a(this.k.b(jVar), this.p, this.k, jVar, kVar);
                    } catch (com.agilebinary.a.a.a.a.b e2) {
                        if (this.f78a.isWarnEnabled()) {
                            this.f78a.warn(com.agilebinary.a.a.a.c.c.e.I("`\u001dU\u0000D\u0006U\u0001B\tU\u0001N\u0006\u0001\rS\u001aN\u001a\u001bH") + e2.getMessage());
                            return null;
                        }
                    }
                    a(this.p, a3, eVar);
                    if (this.p.d() == null) {
                        return null;
                    }
                    return mVar;
                }
                this.p.a((com.agilebinary.a.a.a.a.c) null);
                if (this.l.a(jVar)) {
                    com.agilebinary.a.a.a.b g3 = b2.g();
                    this.f78a.debug(org.c.a.a.a.a.I("__`Uv\r}H~Xj^{Hk\rnX{EjC{DlL{D`C"));
                    try {
                        a(this.l.b(jVar), this.q, this.l, jVar, kVar);
                    } catch (com.agilebinary.a.a.a.a.b e3) {
                        if (this.f78a.isWarnEnabled()) {
                            this.f78a.warn(com.agilebinary.a.a.a.c.c.e.I("`\u001dU\u0000D\u0006U\u0001B\tU\u0001N\u0006\u0001\rS\u001aN\u001a\u001bH") + e3.getMessage());
                            return null;
                        }
                    }
                    a(this.q, g3, eVar);
                    if (this.q.d() == null) {
                        return null;
                    }
                    return mVar;
                }
                this.q.a((com.agilebinary.a.a.a.a.c) null);
            }
            return null;
        } else if (this.s >= this.t) {
            throw new l(com.agilebinary.a.a.a.c.c.e.I("%@\u0010H\u0005T\u0005\u0001\u001aD\fH\u001aD\u000bU\u001b\u0001@") + this.t + org.c.a.a.a.a.I("&\rjUlHjIjI"));
        } else {
            this.s++;
            this.u = null;
            com.agilebinary.a.a.a.d.c.b a4 = this.j.a(a2, jVar, kVar);
            a4.a(a2.k().e());
            URI e_ = a4.e_();
            if (e_.getHost() == null) {
                throw new com.agilebinary.a.a.a.l(com.agilebinary.a.a.a.c.c.e.I(":D\fH\u001aD\u000bUHt:hHE\u0007D\u001b\u0001\u0006N\u001c\u0001\u001bQ\rB\u0001G\u0011\u0001\t\u0001\u001e@\u0004H\f\u0001\u0000N\u001bUHO\tL\r\u001bH") + e_);
            }
            com.agilebinary.a.a.a.b bVar2 = new com.agilebinary.a.a.a.b(e_.getHost(), e_.getPort(), e_.getScheme());
            this.p.a((com.agilebinary.a.a.a.a.c) null);
            this.q.a((com.agilebinary.a.a.a.a.c) null);
            if (!b2.a().equals(bVar2)) {
                this.p.a();
                com.agilebinary.a.a.a.a.h c2 = this.q.c();
                if (c2 != null && c2.d()) {
                    this.q.a();
                }
            }
            l a5 = a(a4);
            a5.a(g2);
            com.agilebinary.a.a.a.h.c.c a6 = a(bVar2, a5);
            m mVar2 = new m(a5, a6);
            if (!this.f78a.isDebugEnabled()) {
                return mVar2;
            }
            this.f78a.debug(org.c.a.a.a.a.I("]HkD}HlYfCh\r{B/\n") + e_ + com.agilebinary.a.a.a.c.c.e.I("\u0006HW\u0001@H") + a6);
            return mVar2;
        }
    }

    private /* synthetic */ com.agilebinary.a.a.a.h.c.c a(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar) {
        com.agilebinary.a.a.a.b bVar2 = bVar == null ? (com.agilebinary.a.a.a.b) fVar.g().a(org.c.a.a.a.a.I("E{Y\u0003kHiLzA{\u0000gB|Y")) : bVar;
        if (bVar2 != null) {
            return this.c.a(bVar2, fVar);
        }
        throw new IllegalStateException(com.agilebinary.a.a.a.c.c.e.I("<@\u001aF\rUHI\u0007R\u001c\u0001\u0005T\u001bUHO\u0007UHC\r\u0001\u0006T\u0004MD\u0001\u0007SHR\rUHH\u0006\u0001\u0018@\u001a@\u0005D\u001cD\u001aRF"));
    }

    private /* synthetic */ void a() {
        try {
            this.o.d_();
        } catch (IOException e2) {
            this.f78a.debug(org.c.a.a.a.a.I("FbJUlHYfBa\r}HcHn^fCh\rlBaCjN{D`C"), e2);
        }
        this.o = null;
    }

    private /* synthetic */ void a(com.agilebinary.a.a.a.a.e eVar, com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.d.e eVar2) {
        if (eVar.b()) {
            String a2 = bVar.a();
            int b2 = bVar.b();
            if (b2 < 0) {
                b2 = this.b.a().a(bVar).a();
            }
            com.agilebinary.a.a.a.a.h c2 = eVar.c();
            com.agilebinary.a.a.a.a.c cVar = new com.agilebinary.a.a.a.a.c(a2, b2, c2.c(), c2.b());
            if (this.f78a.isDebugEnabled()) {
                this.f78a.debug(com.agilebinary.a.a.a.c.c.e.I("`\u001dU\u0000D\u0006U\u0001B\tU\u0001N\u0006\u0001\u001bB\u0007Q\r\u001bH") + cVar);
            }
            g d2 = eVar.d();
            if (d2 == null) {
                d2 = eVar2.a(cVar);
                if (this.f78a.isDebugEnabled()) {
                    if (d2 != null) {
                        this.f78a.debug(org.c.a.a.a.a.I("k`XaI/N}HkHaYfLc^"));
                    } else {
                        this.f78a.debug(com.agilebinary.a.a.a.c.c.e.I("+S\rE\rO\u001cH\tM\u001b\u0001\u0006N\u001c\u0001\u000eN\u001dO\f"));
                    }
                }
            } else if (c2.e()) {
                this.f78a.debug(org.c.a.a.a.a.I("lzYgHaYfNnYfBa\riLfAjI"));
                d2 = null;
            }
            eVar.a(cVar);
            eVar.a(d2);
        }
    }

    private /* synthetic */ void a(m mVar, com.agilebinary.a.a.a.f.k kVar) {
        int a2;
        com.agilebinary.a.a.a.h.c.c b2 = mVar.b();
        boolean z = true;
        int i2 = 0;
        while (z) {
            int i3 = i2 + 1;
            try {
                if (!this.o.l()) {
                    this.o.a(b2, kVar, this.n);
                } else {
                    this.o.b(com.agilebinary.a.a.a.e.c.a(this.n));
                }
                com.agilebinary.a.a.a.h.c.a aVar = new com.agilebinary.a.a.a.h.c.a();
                do {
                    com.agilebinary.a.a.a.h.c.c c_ = this.o.c_();
                    a2 = aVar.a(b2, c_);
                    switch (a2) {
                        case -1:
                            throw new IllegalStateException(org.c.a.a.a.a.I("xaLmAj\r{B/H|YnOcD|E/_`X{H!'AnCaHk\r2\r") + b2 + com.agilebinary.a.a.a.c.c.e.I("bB\u001dS\u001aD\u0006UH\u001cH") + c_);
                        case 0:
                            break;
                        case 1:
                        case 2:
                            this.o.a(b2, kVar, this.n);
                            continue;
                        case 3:
                            a(b2, kVar);
                            this.f78a.debug(org.c.a.a.a.a.I("yzCaHc\r{B/Yn_hH{\rl_jL{Hk\u0003"));
                            this.o.a(this.n);
                            continue;
                        case 4:
                            throw new UnsupportedOperationException(com.agilebinary.a.a.a.c.c.e.I("8S\u0007Y\u0011\u0001\u000bI\tH\u0006RH@\u001aDHO\u0007UHR\u001dQ\u0018N\u001aU\rEF"));
                        case 5:
                            this.o.a(kVar, this.n);
                            continue;
                        default:
                            throw new IllegalStateException(com.agilebinary.a.a.a.c.c.e.I("=O\u0003O\u0007V\u0006\u0001\u001bU\rQHH\u0006E\u0001B\tU\u0007SH") + a2 + org.c.a.a.a.a.I("/K}Bb\r]BzYjif_jN{B}\u0003"));
                    }
                } while (a2 > 0);
                i2 = i3;
                z = false;
            } catch (IOException e2) {
                try {
                    this.o.k();
                } catch (IOException e3) {
                }
                if (this.h.a(e2, i3, kVar)) {
                    if (this.f78a.isInfoEnabled()) {
                        this.f78a.info(com.agilebinary.a.a.a.c.c.e.I("!\u000e'\u0001\rY\u000bD\u0018U\u0001N\u0006\u0001@") + e2.getClass().getName() + org.c.a.a.a.a.I("\u0004/NnXhE{\rxEjC/N`CaHlYfCh\r{B/YgH/Yn_hH{\rgB|Y5\r") + e2.getMessage());
                    }
                    if (this.f78a.isDebugEnabled()) {
                        this.f78a.debug(e2.getMessage(), e2);
                    }
                    this.f78a.info(com.agilebinary.a.a.a.c.c.e.I("s\rU\u001aX\u0001O\u000f\u0001\u000bN\u0006O\rB\u001c"));
                    i2 = i3;
                } else {
                    throw e2;
                }
            }
        }
    }

    private /* synthetic */ void a(Map map, com.agilebinary.a.a.a.a.e eVar, com.agilebinary.a.a.a.d.b bVar, j jVar, com.agilebinary.a.a.a.f.k kVar) {
        com.agilebinary.a.a.a.a.h c2 = eVar.c();
        if (c2 == null) {
            c2 = bVar.a(map, jVar, kVar);
            eVar.a(c2);
        }
        String b2 = c2.b();
        if (((t) map.get(b2.toLowerCase(Locale.ENGLISH))) == null) {
            throw new com.agilebinary.a.a.a.a.b(b2 + org.c.a.a.a.a.I("/LzYgB}DuL{D`C/NgLcAjChH/Hw]jN{Hk\u0001/OzY/C`Y/K`XaI"));
        }
        this.f78a.debug(com.agilebinary.a.a.a.c.c.e.I(")T\u001cI\u0007S\u0001[\tU\u0001N\u0006\u0001\u000bI\tM\u0004D\u0006F\r\u0001\u0018S\u0007B\rR\u001bD\f"));
    }

    private /* synthetic */ boolean a(com.agilebinary.a.a.a.h.c.c cVar, com.agilebinary.a.a.a.f.k kVar) {
        com.agilebinary.a.a.a.b g2 = cVar.g();
        com.agilebinary.a.a.a.b a2 = cVar.a();
        boolean z = false;
        j jVar = null;
        while (true) {
            if (z) {
                break;
            }
            if (!this.o.l()) {
                this.o.a(cVar, kVar, this.n);
            }
            com.agilebinary.a.a.a.b a3 = cVar.a();
            String a4 = a3.a();
            int b2 = a3.b();
            if (b2 < 0) {
                b2 = this.b.a().a(a3.c()).a();
            }
            StringBuilder sb = new StringBuilder(a4.length() + 6);
            sb.append(a4);
            sb.append(':');
            sb.append(Integer.toString(b2));
            com.agilebinary.a.a.a.b.n nVar = new com.agilebinary.a.a.a.b.n(org.c.a.a.a.a.I("n@cAhLy"), sb.toString(), com.agilebinary.a.a.a.e.b.b(this.n));
            nVar.a(this.n);
            kVar.a(com.agilebinary.a.a.a.c.c.e.I("I\u001cU\u0018\u000f\u001c@\u001aF\rU7I\u0007R\u001c"), a2);
            kVar.a(org.c.a.a.a.a.I("E{Y\u0003_`UvrgB|Y"), g2);
            kVar.a(com.agilebinary.a.a.a.c.c.e.I("\u0000U\u001cQFB\u0007O\u0006D\u000bU\u0001N\u0006"), this.o);
            kVar.a(org.c.a.a.a.a.I("gY{]!LzYg\u0003{L}JjY\"^lBH"), this.p);
            kVar.a(com.agilebinary.a.a.a.c.c.e.I("\u0000U\u001cQF@\u001dU\u0000\u000f\u0018S\u0007Y\u0011\f\u001bB\u0007Q\r"), this.q);
            kVar.a(org.c.a.a.a.a.I("gY{]!_j\\zH|Y"), nVar);
            b.a(nVar, this.g, kVar);
            jVar = b.a(nVar, this.o, kVar);
            jVar.a(this.n);
            b.a(jVar, this.g, kVar);
            if (jVar.a().b() < 200) {
                throw new com.agilebinary.a.a.a.k(com.agilebinary.a.a.a.c.c.e.I("t\u0006D\u0010Q\rB\u001cD\f\u0001\u001aD\u001bQ\u0007O\u001bDHU\u0007\u0001+n&o-b<\u0001\u001aD\u0019T\rR\u001c\u001bH") + jVar.a());
            }
            com.agilebinary.a.a.a.d.e eVar = (com.agilebinary.a.a.a.d.e) kVar.a(org.c.a.a.a.a.I("gY{]!LzYg\u0003l_jIjC{DnA|\u0000_`[fIj_"));
            if (eVar != null && com.agilebinary.a.a.a.d.d.a.a(this.n)) {
                if (this.l.a(jVar)) {
                    this.f78a.debug(com.agilebinary.a.a.a.c.c.e.I("q\u001aN\u0010XHS\rP\u001dD\u001bU\rEH@\u001dU\u0000D\u0006U\u0001B\tU\u0001N\u0006"));
                    try {
                        a(this.l.b(jVar), this.q, this.l, jVar, kVar);
                    } catch (com.agilebinary.a.a.a.a.b e2) {
                        if (this.f78a.isWarnEnabled()) {
                            this.f78a.warn(com.agilebinary.a.a.a.c.c.e.I("`\u001dU\u0000D\u0006U\u0001B\tU\u0001N\u0006\u0001\rS\u001aN\u001a\u001bH") + e2.getMessage());
                            break;
                        }
                    }
                    a(this.q, g2, eVar);
                    if (this.q.d() == null) {
                        z = true;
                    } else if (this.d.a(jVar, kVar)) {
                        this.f78a.debug(org.c.a.a.a.a.I("n`CaHlYfBa\rdHY/LcDyH"));
                        com.agilebinary.a.a.a.i.b.a(jVar.b());
                        z = false;
                    } else {
                        this.o.k();
                        z = false;
                    }
                } else {
                    this.q.a((com.agilebinary.a.a.a.a.c) null);
                }
            }
            z = true;
        }
        if (jVar.a().b() > 299) {
            com.agilebinary.a.a.a.c b3 = jVar.b();
            if (b3 != null) {
                jVar.a(new com.agilebinary.a.a.a.g.e(b3));
            }
            this.o.k();
            throw new g(org.c.a.a.a.a.I("LbAcJn[\r}HiX|Hk\rmT/]}BwT5\r") + jVar.a(), jVar);
        }
        this.o.g();
        return false;
    }

    private /* synthetic */ j b(m mVar, com.agilebinary.a.a.a.f.k kVar) {
        l a2 = mVar.a();
        com.agilebinary.a.a.a.h.c.c b2 = mVar.b();
        j jVar = null;
        boolean z = true;
        IOException e2 = null;
        while (z) {
            this.r++;
            a2.m();
            if (!a2.i()) {
                this.f78a.debug(com.agilebinary.a.a.a.c.c.e.I("+@\u0006O\u0007UHS\rU\u001aXHO\u0007OES\rQ\r@\u001c@\nM\r\u0001\u001aD\u0019T\rR\u001c"));
                if (e2 != null) {
                    throw new com.agilebinary.a.a.a.d.j(org.c.a.a.a.a.I("nnCaB{\r}H{_v\r}H~Xj^{\rxD{E/L/C`C\"_j]jL{LmAj\r}H~Xj^{\rjC{D{T!\r/ygH/NnX|H/Af^{^/YgH/_jL|Ba\r{Ej\r`_fJfCnA/_j\\zH|Y/KnDcHk\u0003"), e2);
                }
                throw new com.agilebinary.a.a.a.d.j(com.agilebinary.a.a.a.c.c.e.I("b\tO\u0006N\u001c\u0001\u001aD\u001cS\u0011\u0001\u001aD\u0019T\rR\u001c\u0001\u001fH\u001cIH@HO\u0007OES\rQ\r@\u001c@\nM\r\u0001\u001aD\u0019T\rR\u001c\u0001\rO\u001cH\u001cXF"));
            }
            try {
                if (this.f78a.isDebugEnabled()) {
                    this.f78a.debug(org.c.a.a.a.a.I("NY{Hb]{\r") + this.r + com.agilebinary.a.a.a.c.c.e.I("HU\u0007\u0001\rY\rB\u001dU\r\u0001\u001aD\u0019T\rR\u001c"));
                }
                jVar = b.a(a2, this.o, kVar);
                z = false;
            } catch (IOException e3) {
                e2 = e3;
                this.f78a.debug(org.c.a.a.a.a.I("ncB|DaJ/YgH/N`CaHlYfBa\u0003"));
                try {
                    this.o.k();
                } catch (IOException e4) {
                }
                if (this.h.a(e2, a2.l(), kVar)) {
                    if (this.f78a.isInfoEnabled()) {
                        this.f78a.info(com.agilebinary.a.a.a.c.c.e.I("!\u000e'\u0001\rY\u000bD\u0018U\u0001N\u0006\u0001@") + e2.getClass().getName() + org.c.a.a.a.a.I("&\rlLzJgY/ZgHa\r_`Nj^|DaJ/_j\\zH|Y5\r") + e2.getMessage());
                    }
                    if (this.f78a.isDebugEnabled()) {
                        this.f78a.debug(e2.getMessage(), e2);
                    }
                    this.f78a.info(com.agilebinary.a.a.a.c.c.e.I("s\rU\u001aX\u0001O\u000f\u0001\u001aD\u0019T\rR\u001c"));
                    if (!b2.d()) {
                        this.f78a.debug(org.c.a.a.a.a.I("]H`]jCfCh\r{Ej\rkD}HlY/N`CaHlYfBa\u0003"));
                        this.o.a(b2, kVar, this.n);
                    } else {
                        this.f78a.debug(com.agilebinary.a.a.a.c.c.e.I("8S\u0007Y\u0001D\f\u0001\u000bN\u0006O\rB\u001cH\u0007OF\u0001&D\rEHU\u0007\u0001\u001bU\tS\u001c\u0001\u0007W\rSF"));
                        z = false;
                    }
                } else {
                    throw e2;
                }
            }
        }
        return jVar;
    }

    private /* synthetic */ void b() {
        com.agilebinary.a.a.a.h.a aVar = this.o;
        if (aVar != null) {
            this.o = null;
            try {
                aVar.b();
            } catch (IOException e2) {
                if (this.f78a.isDebugEnabled()) {
                    this.f78a.debug(e2.getMessage(), e2);
                }
            }
            try {
                aVar.d_();
            } catch (IOException e3) {
                this.f78a.debug(org.c.a.a.a.a.I("J_}B}\r}HcHn^fCh\rlBaCjN{D`C"), e3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean */
    public final j a(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar, com.agilebinary.a.a.a.f.k kVar) {
        l a2;
        m mVar;
        boolean z;
        l a3 = a(fVar);
        a3.a(this.n);
        com.agilebinary.a.a.a.h.c.c a4 = a(bVar, a3);
        this.u = (com.agilebinary.a.a.a.b) fVar.g().a(com.agilebinary.a.a.a.c.c.e.I("\u0000U\u001cQFW\u0001S\u001cT\tMEI\u0007R\u001c"));
        m mVar2 = new m(a3, a4);
        long c2 = (long) com.agilebinary.a.a.a.e.c.c(this.n);
        boolean z2 = false;
        j jVar = null;
        boolean z3 = false;
        while (!z2) {
            try {
                a2 = mVar2.a();
                com.agilebinary.a.a.a.h.c.c b2 = mVar2.b();
                Object a5 = kVar.a(org.c.a.a.a.a.I("E{Y\u0003z^j_\"Y`FjC"));
                if (this.o == null) {
                    com.agilebinary.a.a.a.h.b a6 = this.b.a(b2, a5);
                    if (fVar instanceof com.agilebinary.a.a.a.d.c.g) {
                        ((com.agilebinary.a.a.a.d.c.g) fVar).a(a6);
                    }
                    this.o = a6.a(c2, TimeUnit.MILLISECONDS);
                    e eVar = this.n;
                    if (eVar == null) {
                        throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.e.I(" u<qHQ\tS\tL\rU\rS\u001b\u0001\u0005@\u0011\u0001\u0006N\u001c\u0001\nDHO\u001dM\u0004"));
                    } else if (eVar.a(com.agilebinary.a.a.a.c.c.e.I("I\u001cU\u0018\u000f\u000bN\u0006O\rB\u001cH\u0007OFR\u001c@\u0004D\u000bI\rB\u0003"), true) && this.o.l()) {
                        this.f78a.debug(org.c.a.a.a.a.I("\\YnAj\rlBaCjN{D`C/NgHlF"));
                        if (this.o.e()) {
                            this.f78a.debug(com.agilebinary.a.a.a.c.c.e.I(";U\tM\r\u0001\u000bN\u0006O\rB\u001cH\u0007OHE\rU\rB\u001cD\f"));
                            this.o.k();
                        }
                    }
                }
                if (fVar instanceof com.agilebinary.a.a.a.d.c.g) {
                    ((com.agilebinary.a.a.a.d.c.g) fVar).a(this.o);
                }
                try {
                    a(mVar2, kVar);
                    a2.j();
                    URI e_ = a2.e_();
                    if (b2.g() == null || b2.d()) {
                        if (e_.isAbsolute()) {
                            a2.a(com.agilebinary.a.a.a.d.a.b.a(e_, (com.agilebinary.a.a.a.b) null));
                        }
                    } else if (!e_.isAbsolute()) {
                        a2.a(com.agilebinary.a.a.a.d.a.b.a(e_, b2.a()));
                    }
                    com.agilebinary.a.a.a.b bVar2 = this.u;
                    if (bVar2 == null) {
                        bVar2 = b2.a();
                    }
                    com.agilebinary.a.a.a.b g2 = b2.g();
                    kVar.a(org.c.a.a.a.a.I("gY{]!Yn_hH{rgB|Y"), bVar2);
                    kVar.a(com.agilebinary.a.a.a.c.c.e.I("\u0000U\u001cQFQ\u001aN\u0010X7I\u0007R\u001c"), g2);
                    kVar.a(org.c.a.a.a.a.I("E{Y\u0003lBaCjN{D`C"), this.o);
                    kVar.a(com.agilebinary.a.a.a.c.c.e.I("I\u001cU\u0018\u000f\tT\u001cIFU\tS\u000fD\u001c\f\u001bB\u0007Q\r"), this.p);
                    kVar.a(org.c.a.a.a.a.I("E{Y\u0003nX{E!]}BwT\"^lBH"), this.q);
                    b.a(a2, this.g, kVar);
                    j b3 = b(mVar2, kVar);
                    if (b3 != null) {
                        b3.a(this.n);
                        b.a(b3, this.g, kVar);
                        z3 = this.d.a(b3, kVar);
                        if (z3) {
                            long a7 = this.e.a(b3);
                            if (this.f78a.isDebugEnabled()) {
                                this.f78a.debug(org.c.a.a.a.a.I("n`CaHlYfBa\rlLa\rmH/Fj]{\rnAf[j\riB}\r") + (a7 >= 0 ? a7 + com.agilebinary.a.a.a.c.c.e.I("H") + TimeUnit.MILLISECONDS : com.agilebinary.a.a.a.c.c.e.I("D\u001eD\u001a")));
                            }
                            this.o.a(a7, TimeUnit.MILLISECONDS);
                        }
                        m a8 = a(mVar2, b3, kVar);
                        if (a8 == null) {
                            z = true;
                            mVar = mVar2;
                        } else {
                            if (z3) {
                                com.agilebinary.a.a.a.i.b.a(b3.b());
                                this.o.g();
                            } else {
                                this.o.k();
                            }
                            if (!a8.b().equals(mVar2.b())) {
                                a();
                            }
                            boolean z4 = z2;
                            mVar = a8;
                            z = z4;
                        }
                        if (this.o != null && a5 == null) {
                            Object a9 = this.m.a(kVar);
                            kVar.a(com.agilebinary.a.a.a.c.c.e.I("\u0000U\u001cQFT\u001bD\u001a\f\u001cN\u0003D\u0006"), a9);
                            if (a9 != null) {
                                this.o.a(a9);
                                mVar2 = mVar;
                                z2 = z;
                                jVar = b3;
                            }
                        }
                        mVar2 = mVar;
                        z2 = z;
                        jVar = b3;
                    } else {
                        jVar = b3;
                    }
                } catch (g e2) {
                    if (this.f78a.isDebugEnabled()) {
                        this.f78a.debug(e2.getMessage());
                    }
                    jVar = e2.a();
                }
            } catch (URISyntaxException e3) {
                throw new com.agilebinary.a.a.a.l(org.c.a.a.a.a.I("da[nAfI/x]d5\r") + a2.a().c(), e3);
            } catch (InterruptedException e4) {
                InterruptedIOException interruptedIOException = new InterruptedIOException();
                interruptedIOException.initCause(e4);
                throw interruptedIOException;
            } catch (com.agilebinary.a.a.a.c.b.n e5) {
                InterruptedIOException interruptedIOException2 = new InterruptedIOException(org.c.a.a.a.a.I("n`CaHlYfBa\rgL|\rmHjC/^gX{\rkBxC"));
                interruptedIOException2.initCause(e5);
                throw interruptedIOException2;
            } catch (com.agilebinary.a.a.a.k e6) {
                b();
                throw e6;
            } catch (IOException e7) {
                b();
                throw e7;
            } catch (RuntimeException e8) {
                b();
                throw e8;
            }
        }
        if (jVar == null || jVar.b() == null || !jVar.b().g()) {
            if (z3) {
                this.o.g();
            }
            a();
        } else {
            jVar.a(new com.agilebinary.a.a.a.h.j(jVar.b(), this.o, z3));
        }
        return jVar;
    }
}
