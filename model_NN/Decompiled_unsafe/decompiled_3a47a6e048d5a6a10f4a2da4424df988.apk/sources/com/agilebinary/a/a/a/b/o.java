package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.f;
import java.io.Serializable;
import org.osmdroid.c.c;

public final class o implements com.agilebinary.a.a.a.o, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f17a;
    private final String b;

    public o(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException(c.I("\u000e\u0004-\u0000`\b!\u001c`\u000b/\u0011`\u0007%E.\u0010,\t"));
        }
        this.f17a = str;
        this.b = str2;
    }

    public final String a() {
        return this.f17a;
    }

    public final String b() {
        return this.b;
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof com.agilebinary.a.a.a.o)) {
            return false;
        }
        o oVar = (o) obj;
        return this.f17a.equals(oVar.f17a) && f.a(this.b, oVar.b);
    }

    public final int hashCode() {
        return f.a(f.a(17, this.f17a), this.b);
    }

    public final String toString() {
        if (this.b == null) {
            return this.f17a;
        }
        com.agilebinary.a.a.a.i.c cVar = new com.agilebinary.a.a.a.i.c(this.f17a.length() + 1 + this.b.length());
        cVar.a(this.f17a);
        cVar.a(com.agilebinary.a.a.a.k.f.I("\u000b"));
        cVar.a(this.b);
        return cVar.toString();
    }
}
