package com.agilebinary.a.a.a.g;

import com.agilebinary.a.a.a.c.c.j;
import com.agilebinary.mobilemonitor.client.android.a.a.g;
import java.io.InputStream;
import java.io.OutputStream;

public final class c extends f {

    /* renamed from: a  reason: collision with root package name */
    private InputStream f95a;
    private long b = -1;

    public final void a(long j) {
        this.b = j;
    }

    public final void a(InputStream inputStream) {
        this.f95a = inputStream;
    }

    public final void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException(j.I("4\u000e\u000f\u000b\u000e\u000f[\b\u000f\t\u001e\u001a\u0016[\u0016\u001a\u0002[\u0015\u0014\u000f[\u0019\u001e[\u0015\u000e\u0017\u0017"));
        }
        InputStream f = f();
        try {
            byte[] bArr = new byte[2048];
            while (true) {
                int read = f.read(bArr);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        } finally {
            f.close();
        }
    }

    public final boolean a() {
        return false;
    }

    public final long c() {
        return this.b;
    }

    public final InputStream f() {
        if (this.f95a != null) {
            return this.f95a;
        }
        throw new IllegalStateException(g.I("\\_qDz^k\u0010wQl\u0010q_k\u0010}Uz^?@m_iY{U{"));
    }

    public final boolean g() {
        return this.f95a != null;
    }
}
