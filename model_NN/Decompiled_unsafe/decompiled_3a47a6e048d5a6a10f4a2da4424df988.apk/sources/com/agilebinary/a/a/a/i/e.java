package com.agilebinary.a.a.a.i;

import com.agilebinary.a.a.a.k.f;
import java.io.Serializable;

public final class e implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f117a;
    private int b;

    public e(int i) {
        if (i < 0) {
            throw new IllegalArgumentException(b.I("pCTPWD\u0012USFSU[BK\u0016_WK\u0016\\YF\u0016PS\u0012XWQSB[@W"));
        }
        this.f117a = new byte[i];
    }

    private /* synthetic */ void d(int i) {
        byte[] bArr = new byte[Math.max(this.f117a.length << 1, i)];
        System.arraycopy(this.f117a, 0, bArr, 0, this.b);
        this.f117a = bArr;
    }

    public final void a() {
        this.b = 0;
    }

    public final void a(int i) {
        int i2 = this.b + 1;
        if (i2 > this.f117a.length) {
            d(i2);
        }
        this.f117a[this.b] = (byte) i;
        this.b = i2;
    }

    public final void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i < 0 || i > bArr.length || i2 < 0 || i + i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException(f.I("Y-Pq\u0016") + i + b.I("\u0016^S\\\f\u0012") + i2 + f.I("\u0016)\u0018'S%Q?^q\u0016") + bArr.length);
            } else if (i2 != 0) {
                int i3 = this.b + i2;
                if (i3 > this.f117a.length) {
                    d(i3);
                }
                System.arraycopy(bArr, i, this.f117a, this.b, i2);
                this.b = i3;
            }
        }
    }

    public final void a(char[] cArr, int i, int i2) {
        if (cArr != null) {
            if (i < 0 || i > cArr.length || i2 < 0 || i + i2 < 0 || i + i2 > cArr.length) {
                throw new IndexOutOfBoundsException(b.I("]PT\f\u0012") + i + f.I("kZ.Xq\u0016") + i2 + b.I("\u0012T\u001cZWXUBZ\f\u0012") + cArr.length);
            } else if (i2 != 0) {
                int i3 = this.b;
                int i4 = i3 + i2;
                if (i4 > this.f117a.length) {
                    d(i4);
                }
                while (i3 < i4) {
                    this.f117a[i3] = (byte) cArr[i];
                    i++;
                    i3++;
                }
                this.b = i4;
            }
        }
    }

    public final int b(int i) {
        return this.f117a[i];
    }

    public final byte[] b() {
        byte[] bArr = new byte[this.b];
        if (this.b > 0) {
            System.arraycopy(this.f117a, 0, bArr, 0, this.b);
        }
        return bArr;
    }

    public final int c() {
        return this.f117a.length;
    }

    public final void c(int i) {
        if (i < 0 || i > this.f117a.length) {
            throw new IndexOutOfBoundsException(f.I("Z.Xq\u0016") + i + b.I("\u0016\u000e\u0016\u0002\u0016]D\u0012\b\u0012TGPTS@\u0016^S\\\f\u0012") + this.f117a.length);
        }
        this.b = i;
    }

    public final int d() {
        return this.b;
    }

    public final byte[] e() {
        return this.f117a;
    }

    public final boolean f() {
        return this.b == 0;
    }

    public final boolean g() {
        return this.b == this.f117a.length;
    }
}
