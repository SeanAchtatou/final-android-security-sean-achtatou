package com.agilebinary.a.a.a.a;

import com.agilebinary.a.a.a.d.a.b;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f3a = new ConcurrentHashMap();

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ '}');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ ',');
            i2 = i;
        }
        return new String(cArr);
    }

    public final h a(String str) {
        if (str == null) {
            throw new IllegalArgumentException(b.I(")%\n!G)\u0006=G*\b0G&\u0002d\t1\u000b("));
        }
        f fVar = (f) this.f3a.get(str.toLowerCase(Locale.ENGLISH));
        if (fVar != null) {
            return fVar.a();
        }
        throw new IllegalStateException(com.agilebinary.mobilemonitor.client.a.a.a.I("[2})~,a.z9j|o)z4k2z5m=z5a2./m4k1kf.") + str);
    }
}
