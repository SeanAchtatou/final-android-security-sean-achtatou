package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.e.c;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.b.f;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.m;
import com.agilebinary.a.a.b.a.a;
import java.net.ConnectException;
import java.net.Socket;
import org.apache.commons.logging.Log;

public final class b implements m {

    /* renamed from: a  reason: collision with root package name */
    private final Log f43a = a.a(getClass());
    private h b;

    public b(h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException(c.I("rkImLm\u0001zDoH{UzX(@eX(OgU(Cm\u0001fTdM"));
        }
        this.b = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean */
    private static /* synthetic */ void a(Socket socket, e eVar) {
        boolean z = true;
        if (eVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.a.a.I("5x)|]\\\u001c^\u001cA\u0018X\u0018^\u000e\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"));
        }
        socket.setTcpNoDelay(eVar.a(c.I("`U|Q&UkQ&OgEmMiX"), true));
        socket.setSoTimeout(c.a(eVar));
        if (eVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.a.a.I("5x)|]\\\u001c^\u001cA\u0018X\u0018^\u000e\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"));
        }
        int a2 = eVar.a(c.I("`U|Q&RgBcD|\u000fdHfFmS"), -1);
        if (a2 >= 0) {
            if (a2 <= 0) {
                z = false;
            }
            socket.setSoLinger(z, a2);
        }
    }

    public final g a() {
        return new h();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.h.b.f.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      com.agilebinary.a.a.a.h.b.i.a(java.net.Socket, java.net.InetSocketAddress, java.net.InetSocketAddress, com.agilebinary.a.a.a.e.e):java.net.Socket
      com.agilebinary.a.a.a.h.b.f.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket */
    public final void a(g gVar, com.agilebinary.a.a.a.b bVar, e eVar) {
        if (gVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.a.a.I("o\u0012B\u0013I\u001eX\u0014C\u0013\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"));
        } else if (bVar == null) {
            throw new IllegalArgumentException(c.I("uiSoD|\u0001`N{U(LiX(OgU(Cm\u0001fTdM"));
        } else if (eVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.a.a.I("|\u001c^\u001cA\u0018X\u0018^\u000e\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"));
        } else if (!gVar.l()) {
            throw new IllegalStateException(c.I("bgOfDkUaNf\u0001eT{U(Cm\u0001gQmO"));
        } else {
            com.agilebinary.a.a.a.h.b.g a2 = this.b.a(bVar.c());
            if (!(a2.b() instanceof f)) {
                throw new IllegalArgumentException(com.agilebinary.a.a.a.a.a.I(")M\u000fK\u0018X]_\u001eD\u0018A\u0018\fU") + a2.c() + c.I("\b(L}R|\u0001`@~D(MiXmSmE(RgBcD|\u0001n@kUgSq\u000f"));
            }
            f fVar = (f) a2.b();
            try {
                Socket a3 = fVar.a(gVar.i_(), bVar.a(), bVar.b(), true);
                a(a3, eVar);
                gVar.a(a3, bVar, fVar.a(a3), eVar);
            } catch (ConnectException e) {
                throw new com.agilebinary.a.a.a.h.f(bVar, e);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00fb A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.h.g r12, com.agilebinary.a.a.a.b r13, java.net.InetAddress r14, com.agilebinary.a.a.a.e.e r15) {
        /*
            r11 = this;
            if (r12 != 0) goto L_0x000e
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "o\u0012B\u0013I\u001eX\u0014C\u0013\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"
            java.lang.String r1 = com.agilebinary.a.a.a.a.a.I(r1)
            r0.<init>(r1)
            throw r0
        L_0x000e:
            if (r13 != 0) goto L_0x001c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "uiSoD|\u0001`N{U(LiX(OgU(Cm\u0001fTdM"
            java.lang.String r1 = com.agilebinary.a.a.a.e.c.I(r1)
            r0.<init>(r1)
            throw r0
        L_0x001c:
            if (r15 != 0) goto L_0x002a
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "|\u001c^\u001cA\u0018X\u0018^\u000e\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"
            java.lang.String r1 = com.agilebinary.a.a.a.a.a.I(r1)
            r0.<init>(r1)
            throw r0
        L_0x002a:
            boolean r0 = r12.l()
            if (r0 == 0) goto L_0x003c
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "bgOfDkUaNf\u0001eT{U(OgU(Cm\u0001gQmO"
            java.lang.String r1 = com.agilebinary.a.a.a.e.c.I(r1)
            r0.<init>(r1)
            throw r0
        L_0x003c:
            com.agilebinary.a.a.a.h.b.h r0 = r11.b
            java.lang.String r1 = r13.c()
            com.agilebinary.a.a.a.h.b.g r0 = r0.a(r1)
            com.agilebinary.a.a.a.h.b.i r4 = r0.b()
            java.lang.String r1 = r13.a()
            java.net.InetAddress[] r5 = java.net.InetAddress.getAllByName(r1)
            int r1 = r13.b()
            int r6 = r0.a(r1)
            r0 = 0
            r2 = r0
        L_0x005c:
            int r0 = r5.length
            if (r2 >= r0) goto L_0x00b3
            r1 = r5[r2]
            int r0 = r5.length
            int r0 = r0 + -1
            if (r2 != r0) goto L_0x00b4
            r0 = 1
        L_0x0067:
            java.net.Socket r3 = r4.f_()
            r12.a(r3, r13)
            java.net.InetSocketAddress r7 = new java.net.InetSocketAddress
            r7.<init>(r1, r6)
            r1 = 0
            if (r14 == 0) goto L_0x007c
            java.net.InetSocketAddress r1 = new java.net.InetSocketAddress
            r8 = 0
            r1.<init>(r14, r8)
        L_0x007c:
            org.apache.commons.logging.Log r8 = r11.f43a
            boolean r8 = r8.isDebugEnabled()
            if (r8 == 0) goto L_0x00a0
            org.apache.commons.logging.Log r8 = r11.f43a
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "o\u0012B\u0013I\u001eX\u0014B\u001a\f\tC]"
            java.lang.String r10 = com.agilebinary.a.a.a.a.a.I(r10)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r7)
            java.lang.String r9 = r9.toString()
            r8.debug(r9)
        L_0x00a0:
            java.net.Socket r1 = r4.a(r3, r7, r1, r15)     // Catch:{ ConnectException -> 0x00b6, h -> 0x00bf }
            if (r3 == r1) goto L_0x0100
            r12.a(r1, r13)     // Catch:{ ConnectException -> 0x00b6, h -> 0x00bf }
        L_0x00a9:
            a(r1, r15)     // Catch:{ ConnectException -> 0x00b6, h -> 0x00bf }
            boolean r1 = r4.a(r1)     // Catch:{ ConnectException -> 0x00b6, h -> 0x00bf }
            r12.a(r1, r15)     // Catch:{ ConnectException -> 0x00b6, h -> 0x00bf }
        L_0x00b3:
            return
        L_0x00b4:
            r0 = 0
            goto L_0x0067
        L_0x00b6:
            r1 = move-exception
            if (r0 == 0) goto L_0x00c3
            com.agilebinary.a.a.a.h.f r0 = new com.agilebinary.a.a.a.h.f
            r0.<init>(r13, r1)
            throw r0
        L_0x00bf:
            r1 = move-exception
            if (r0 == 0) goto L_0x00c3
            throw r1
        L_0x00c3:
            org.apache.commons.logging.Log r0 = r11.f43a
            boolean r0 = r0.isDebugEnabled()
            if (r0 == 0) goto L_0x00fb
            org.apache.commons.logging.Log r0 = r11.f43a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "bgOfDkU(Ug\u0001"
            java.lang.String r3 = com.agilebinary.a.a.a.e.c.I(r3)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r3 = "\f\tE\u0010I\u0019\f\u0012Y\t\u0002]"
            java.lang.String r3 = com.agilebinary.a.a.a.a.a.I(r3)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "bgOfDkUaNf\u0001HdM(Cm\u0001zD|SaDl\u0001}RaOo\u0001iOgU`Dz\u0001Aq(@lEzD{R"
            java.lang.String r3 = com.agilebinary.a.a.a.e.c.I(r3)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r0.debug(r1)
        L_0x00fb:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x005c
        L_0x0100:
            r1 = r3
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.b.a(com.agilebinary.a.a.a.h.g, com.agilebinary.a.a.a.b, java.net.InetAddress, com.agilebinary.a.a.a.e.e):void");
    }
}
