package com.agilebinary.a.a.a.h.c;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.c.b.q;
import com.agilebinary.a.a.a.c.c.j;
import java.net.InetAddress;

public final class f implements b, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final b f108a;
    private final InetAddress b;
    private boolean c;
    private b[] d;
    private g e;
    private e f;
    private boolean g;

    private /* synthetic */ f(b bVar, InetAddress inetAddress) {
        if (bVar == null) {
            throw new IllegalArgumentException(q.I("h@NFYU\u001cISRH\u0001Q@E\u0001RNH\u0001^D\u001cOIMP\u000f"));
        }
        this.f108a = bVar;
        this.b = inetAddress;
        this.e = g.f109a;
        this.f = e.f107a;
    }

    public f(c cVar) {
        this(cVar.a(), cVar.b());
    }

    public final b a() {
        return this.f108a;
    }

    public final b a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException(j.I("3\u0014\u000b[\u0012\u0015\u001f\u001e\u0003[\u0016\u000e\b\u000f[\u0015\u0014\u000f[\u0019\u001e[\u0015\u001e\u001c\u001a\u000f\u0012\r\u001eA[") + i);
        }
        int c2 = c();
        if (i < c2) {
            return i < c2 + -1 ? this.d[i] : this.f108a;
        }
        throw new IllegalArgumentException(q.I("tNL\u0001UOXDD\u0001") + i + j.I("[\u001e\u0003\u0018\u001e\u001e\u001f\b[\u000f\t\u001a\u0018\u0010\u001e\u001f[\t\u0014\u000e\u000f\u001e[\u0017\u001e\u0015\u001c\u000f\u0013[") + c2 + q.I("\u000f"));
    }

    public final void a(b bVar, boolean z) {
        if (bVar == null) {
            throw new IllegalArgumentException(j.I("+\t\u0014\u0003\u0002[\u0013\u0014\b\u000f[\u0016\u001a\u0002[\u0015\u0014\u000f[\u0019\u001e[\u0015\u000e\u0017\u0017U"));
        } else if (this.c) {
            throw new IllegalStateException(q.I("}MND]EE\u0001_NROYBHDX\u000f"));
        } else {
            this.c = true;
            this.d = new b[]{bVar};
            this.g = z;
        }
    }

    public final void a(boolean z) {
        if (this.c) {
            throw new IllegalStateException(j.I(":\u0017\t\u001e\u001a\u001f\u0002[\u0018\u0014\u0015\u0015\u001e\u0018\u000f\u001e\u001fU"));
        }
        this.c = true;
        this.g = z;
    }

    public final InetAddress b() {
        return this.b;
    }

    public final void b(boolean z) {
        if (!this.c) {
            throw new IllegalStateException(q.I("oS\u0001HTROYM\u001cTRMYRO\u0001_NROYBHDX\u000f"));
        } else if (this.d == null) {
            throw new IllegalStateException(j.I("5\u0014[\u000f\u000e\u0015\u0015\u001e\u0017[\f\u0012\u000f\u0013\u0014\u000e\u000f[\u000b\t\u0014\u0003\u0002U"));
        } else {
            this.e = g.b;
            this.g = z;
        }
    }

    public final int c() {
        if (!this.c) {
            return 0;
        }
        if (this.d == null) {
            return 1;
        }
        return this.d.length + 1;
    }

    public final void c(boolean z) {
        if (!this.c) {
            throw new IllegalStateException(q.I("oS\u0001P@EDNDX\u0001LSSUSBSM\u001cTRMYRO\u0001_NROYBHDX\u000f"));
        }
        this.f = e.b;
        this.g = z;
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean d() {
        return this.e == g.b;
    }

    public final boolean e() {
        return this.f == e.b;
    }

    public final boolean equals(Object obj) {
        boolean z = true;
        int i = 0;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        boolean equals = this.f108a.equals(fVar.f108a);
        boolean z2 = this.b == fVar.b || (this.b != null && this.b.equals(fVar.b));
        boolean z3 = this.d == fVar.d || !(this.d == null || fVar.d == null || this.d.length != fVar.d.length);
        if (!(this.c == fVar.c && this.g == fVar.g && this.e == fVar.e && this.f == fVar.f)) {
            z = false;
        }
        boolean z4 = z & z2 & equals & z3;
        if (!z4 || this.d == null) {
            return z4;
        }
        while (z4 && i < this.d.length) {
            z4 = this.d[i].equals(fVar.d[i]);
            i++;
        }
        return z4;
    }

    public final boolean f() {
        return this.g;
    }

    public final boolean g() {
        return this.c;
    }

    public final c h() {
        if (!this.c) {
            return null;
        }
        return new c(this.f108a, this.b, this.d, this.g, this.e, this.f);
    }

    public final int hashCode() {
        int i;
        int hashCode = this.f108a.hashCode();
        if (this.b != null) {
            hashCode ^= this.b.hashCode();
        }
        if (this.d != null) {
            i = this.d.length ^ hashCode;
            for (b hashCode2 : this.d) {
                i ^= hashCode2.hashCode();
            }
        } else {
            i = hashCode;
        }
        if (this.c) {
            i ^= 286331153;
        }
        if (this.g) {
            i ^= 572662306;
        }
        return (this.e.hashCode() ^ i) ^ this.f.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((c() * 30) + 50);
        sb.append(j.I(")\u0014\u000e\u000f\u001e/\t\u001a\u0018\u0010\u001e\t "));
        if (this.b != null) {
            sb.append(this.b);
            sb.append(q.I("\u0011\u001f"));
        }
        sb.append('{');
        if (this.c) {
            sb.append('c');
        }
        if (this.e == g.b) {
            sb.append('t');
        }
        if (this.f == e.b) {
            sb.append('l');
        }
        if (this.g) {
            sb.append('s');
        }
        sb.append(j.I("\u0006VE"));
        if (this.d != null) {
            for (b append : this.d) {
                sb.append(append);
                sb.append(q.I("\u0011\u001f"));
            }
        }
        sb.append(this.f108a);
        sb.append(']');
        return sb.toString();
    }
}
