package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.i;
import com.agilebinary.a.a.a.c.c.e;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.List;

public final class q extends ah {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f31a;

    public q() {
        this(null);
    }

    public q(String[] strArr) {
        if (strArr != null) {
            this.f31a = (String[]) strArr.clone();
        } else {
            this.f31a = new String[]{e.I("-d-\rHE\f\f%l%\f\u0011XHi \u001b\u0005LRR\u001b\u0001\u0012")};
        }
        a(e.I("Q\tU\u0000"), new m());
        a(b.I("R][S_\\"), new ab());
        a(e.I("\u0005@\u0010\f\tF\r"), new ac());
        a(b.I("EWUGDW"), new h());
        a(e.I("\u000bN\u0005L\rO\u001c"), new o());
        a(b.I("WNB_@SA"), new j(this.f31a));
    }

    public final int a() {
        return 0;
    }

    public final List a(t tVar, f fVar) {
        c cVar;
        i iVar;
        if (tVar == null) {
            throw new IllegalArgumentException(b.I("~WWVS@\u0016_WK\u0016\\YF\u0016PS\u0012XGZ^"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(e.I("+N\u0007J\u0001DHN\u001aH\u000fH\u0006\u0001\u0005@\u0011\u0001\u0006N\u001c\u0001\nDHO\u001dM\u0004"));
        } else if (!tVar.a().equalsIgnoreCase(b.I("eWB\u001fu]YY_W"))) {
            throw new com.agilebinary.a.a.a.k.e(e.I("t\u0006S\rB\u0007F\u0006H\u0012D\f\u0001\u000bN\u0007J\u0001DHI\r@\fD\u001a\u0001O") + tVar.toString() + b.I("\u0015"));
        } else {
            if (tVar instanceof r) {
                cVar = ((r) tVar).e();
                iVar = new i(((r) tVar).d(), cVar.c());
            } else {
                String b = tVar.b();
                if (b == null) {
                    throw new com.agilebinary.a.a.a.k.e(e.I("i\r@\fD\u001a\u0001\u001e@\u0004T\r\u0001\u0001RHO\u001dM\u0004"));
                }
                cVar = new c(b.length());
                cVar.a(b);
                iVar = new i(0, cVar.c());
            }
            return a(new m[]{aa.a(cVar, iVar)}, fVar);
        }
    }

    public final List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException(b.I("~_AB\u0012YT\u0016QY]][SA\u0016_WK\u0016\\YF\u0016PS\u0012XGZ^"));
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException(e.I("m\u0001R\u001c\u0001\u0007GHB\u0007N\u0003H\rRHL\tXHO\u0007UHC\r\u0001\rL\u0018U\u0011"));
        } else {
            c cVar = new c(list.size() * 20);
            cVar.a(b.I("u]YY_W"));
            cVar.a(e.I("\u001bH"));
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    com.agilebinary.a.a.a.k.c cVar2 = (com.agilebinary.a.a.a.k.c) list.get(i2);
                    if (i2 > 0) {
                        cVar.a(b.I("\r\u0012"));
                    }
                    cVar.a(cVar2.a());
                    String b = cVar2.b();
                    if (b != null) {
                        cVar.a(e.I("U"));
                        cVar.a(b);
                    }
                    i = i2 + 1;
                } else {
                    ArrayList arrayList = new ArrayList(1);
                    arrayList.add(new com.agilebinary.a.a.a.b.e(cVar));
                    return arrayList;
                }
            }
        }
    }

    public final t b() {
        return null;
    }

    public final String toString() {
        return b.I("XWBAUSFW");
    }
}
