package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.h;
import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.d.g;
import com.agilebinary.a.a.a.u;
import java.util.HashMap;
import org.c.a.a.a.a;

public final class j implements g {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap f71a = new HashMap();

    public final h a(b bVar) {
        if (bVar != null) {
            return (h) this.f71a.get(bVar);
        }
        throw new IllegalArgumentException(a.I("e[y_\rgB|Y/@nT/C`Y/Oj\raXcA"));
    }

    public final void a(b bVar, h hVar) {
        if (bVar == null) {
            throw new IllegalArgumentException(u.I("\u001cq\u0000utM;V \u00059D-\u0005:J \u00056@tK!I8"));
        }
        this.f71a.put(bVar, hVar);
    }

    public final void b(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException(a.I("e[y_\rgB|Y/@nT/C`Y/Oj\raXcA"));
        }
        this.f71a.remove(bVar);
    }

    public final String toString() {
        return this.f71a.toString();
    }
}
