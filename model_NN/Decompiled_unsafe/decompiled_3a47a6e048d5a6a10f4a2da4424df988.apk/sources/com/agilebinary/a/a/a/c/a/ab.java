package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.f.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import java.util.Locale;
import java.util.StringTokenizer;

public final class ab extends r {
    public final void a(c cVar, f fVar) {
        super.a(cVar, fVar);
        String a2 = fVar.a();
        String c = cVar.c();
        if (a2.contains(com.agilebinary.a.a.a.i.f.I("\u0015"))) {
            int countTokens = new StringTokenizer(c, b.I("e")).countTokens();
            String upperCase = c.toUpperCase(Locale.ENGLISH);
            if (upperCase.endsWith(com.agilebinary.a.a.a.i.f.I("\u0001x`v")) || upperCase.endsWith(b.I("$\u000eN\u001e")) || upperCase.endsWith(com.agilebinary.a.a.a.i.f.I("\u0001ujo")) || upperCase.endsWith(b.I("$\fE\u001d")) || upperCase.endsWith(com.agilebinary.a.a.a.i.f.I("\u0001vfw")) || upperCase.endsWith(b.I("$\u0004X\f")) || upperCase.endsWith(com.agilebinary.a.a.a.i.f.I("\u0001rao"))) {
                if (countTokens < 2) {
                    throw new i(b.I("N$g*c%**~?x\"h>~.*i") + c + com.agilebinary.a.a.a.i.f.I("\u0019\u000fMFTCZ[^\\\u001b[SJ\u001ba^[HLZ_^\u000fX@TDRJ\u001b\\KJXF]FXNOFTA\u001bIT]\u001b") + b.I("8z.i\"k'*/e&k\"d8"));
                }
            } else if (countTokens < 3) {
                throw new i(com.agilebinary.a.a.a.i.f.I("kTBZFU\u000fZ[O]RMN[^\u000f\u0019") + c + b.I("(k|\"e'k?o8*?b.*\u0005o?y(k;oki$e c.*8z.i\"l\"i*~\"e%"));
            }
        }
    }

    public final boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.i.f.I("lT@PF^\u000fVNB\u000fU@O\u000fYJ\u001bANCW"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(b.I("\be$a\"oke9c,c%*&k2*%e?*)okd>f'"));
        } else {
            String a2 = fVar.a();
            String c = cVar.c();
            if (c == null) {
                return false;
            }
            return a2.endsWith(c);
        }
    }
}
