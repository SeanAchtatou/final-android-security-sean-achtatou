package com.agilebinary.a.a.a.i;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.c.c.m;
import java.io.InputStream;

public final class b {
    private /* synthetic */ b() {
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ '2');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '6');
            i2 = i;
        }
        return new String(cArr);
    }

    public static void a(c cVar) {
        InputStream f;
        if (cVar != null && cVar.g() && (f = cVar.f()) != null) {
            f.close();
        }
    }

    /* JADX INFO: finally extract failed */
    public static byte[] b(c cVar) {
        int i = 4096;
        if (cVar == null) {
            throw new IllegalArgumentException(I("zbff\u0012S\\B[BK\u0016_WK\u0016\\YF\u0016PS\u0012XGZ^"));
        }
        InputStream f = cVar.f();
        if (f == null) {
            return null;
        }
        if (cVar.c() > 2147483647L) {
            throw new IllegalArgumentException(m.I("WKKO?zqkvkf?kpp?s~mxz?kp?}z?}jyyzmz{?vq?rzrpmf"));
        }
        int c = (int) cVar.c();
        if (c >= 0) {
            i = c;
        }
        e eVar = new e(i);
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = f.read(bArr);
                if (read != -1) {
                    eVar.a(bArr, 0, read);
                } else {
                    f.close();
                    return eVar.b();
                }
            }
        } catch (Throwable th) {
            f.close();
            throw th;
        }
    }
}
