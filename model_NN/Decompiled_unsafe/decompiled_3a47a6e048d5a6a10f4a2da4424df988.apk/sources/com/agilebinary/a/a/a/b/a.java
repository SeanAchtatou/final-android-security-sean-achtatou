package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.i.f;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import com.agilebinary.mobilemonitor.client.android.a.q;

public final class a implements m, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f7a;
    private final String b;
    private final o[] c;

    public a(String str, String str2, o[] oVarArr) {
        if (str == null) {
            throw new IllegalArgumentException(q.I("b\u001dA\u0019\f\u0011M\u0005\f\u0012C\b\f\u001eI\\B\t@\u0010"));
        }
        this.f7a = str;
        this.b = str2;
        if (oVarArr != null) {
            this.c = oVarArr;
        } else {
            this.c = new o[0];
        }
    }

    public final o a(String str) {
        if (str == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.d.m.I(".\\*\u0011\"P6\u0011!^;\u0011-To_:]#"));
        }
        for (o oVar : this.c) {
            if (oVar.a().equalsIgnoreCase(str)) {
                return oVar;
            }
        }
        return null;
    }

    public final String a() {
        return this.f7a;
    }

    public final String b() {
        return this.b;
    }

    public final o[] c() {
        return (o[]) this.c.clone();
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean equals(Object obj) {
        boolean z;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof m)) {
            return false;
        }
        a aVar = (a) obj;
        if (!this.f7a.equals(aVar.f7a) || !f.a(this.b, aVar.b)) {
            return false;
        }
        o[] oVarArr = this.c;
        o[] oVarArr2 = aVar.c;
        if (oVarArr == null) {
            z = oVarArr2 == null;
        } else {
            if (oVarArr2 != null && oVarArr.length == oVarArr2.length) {
                int i = 0;
                while (true) {
                    if (i < oVarArr.length) {
                        if (!f.a(oVarArr[i], oVarArr2[i])) {
                            break;
                        }
                        i++;
                    } else {
                        z = true;
                        break;
                    }
                }
            }
            z = false;
        }
        return z;
    }

    public final int hashCode() {
        int a2 = f.a(f.a(17, this.f7a), this.b);
        for (o a3 : this.c) {
            a2 = f.a(a2, a3);
        }
        return a2;
    }

    public final String toString() {
        c cVar = new c(64);
        cVar.a(this.f7a);
        if (this.b != null) {
            cVar.a(q.I("A"));
            cVar.a(this.b);
        }
        for (o valueOf : this.c) {
            cVar.a(com.agilebinary.a.a.a.c.d.m.I("\no"));
            cVar.a(String.valueOf(valueOf));
        }
        return cVar.toString();
    }
}
