package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.h;
import com.agilebinary.a.a.a.a.i;
import com.agilebinary.a.a.a.c.c.e;
import com.agilebinary.a.a.a.d.b;
import com.agilebinary.a.a.a.f.d;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.b.a.a;
import com.agilebinary.mobilemonitor.client.android.a.q;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.logging.Log;

public abstract class t implements b {
    private static final List b = Collections.unmodifiableList(Arrays.asList(q.I("\u0012I\u001bC\bE\u001dX\u0019"), e.I("o<m%"), q.I("h\u0015K\u0019_\b"), e.I("*@\u001bH\u000b")));

    /* renamed from: a  reason: collision with root package name */
    private final Log f79a = a.a(getClass());

    protected static Map a(com.agilebinary.a.a.a.t[] tVarArr) {
        c cVar;
        int i;
        HashMap hashMap = new HashMap(tVarArr.length);
        for (com.agilebinary.a.a.a.t tVar : tVarArr) {
            if (tVar instanceof r) {
                cVar = ((r) tVar).e();
                i = ((r) tVar).d();
            } else {
                String b2 = tVar.b();
                if (b2 == null) {
                    throw new i(q.I("d\u0019M\u0018I\u000e\f\nM\u0010Y\u0019\f\u0015_\\B\t@\u0010"));
                }
                c cVar2 = new c(b2.length());
                cVar2.a(b2);
                cVar = cVar2;
                i = 0;
            }
            while (i < cVar.c() && d.a(cVar.a(i))) {
                i++;
            }
            int i2 = i;
            while (i2 < cVar.c() && !d.a(cVar.a(i2))) {
                i2++;
            }
            hashMap.put(cVar.a(i, i2).toLowerCase(Locale.ENGLISH), tVar);
        }
        return hashMap;
    }

    public final h a(Map map, j jVar, k kVar) {
        h hVar;
        com.agilebinary.a.a.a.a.a aVar = (com.agilebinary.a.a.a.a.a) kVar.a(e.I("I\u001cU\u0018\u000f\tT\u001cI\u001bB\u0000D\u0005DES\rF\u0001R\u001cS\u0011"));
        if (aVar == null) {
            throw new IllegalStateException(q.I("=Y\bD/O\u0014I\u0011I\\^\u0019K\u0015_\b^\u0005\f\u0012C\b\f\u000fI\b\f\u0015B\\d(x,\f\u001fC\u0012X\u0019T\b"));
        }
        List a2 = a(jVar, kVar);
        if (a2 == null) {
            a2 = b;
        }
        if (this.f79a.isDebugEnabled()) {
            this.f79a.debug(e.I(")T\u001cI\rO\u001cH\u000b@\u001cH\u0007OHR\u000bI\rL\rRHH\u0006\u0001\u001cI\r\u0001\u0007S\fD\u001a\u0001\u0007GHQ\u001aD\u000eD\u001aD\u0006B\r\u001bH") + a2);
        }
        Iterator it = a2.iterator();
        while (true) {
            if (!it.hasNext()) {
                hVar = null;
                break;
            }
            String str = (String) it.next();
            if (((com.agilebinary.a.a.a.t) map.get(str.toLowerCase(Locale.ENGLISH))) != null) {
                if (this.f79a.isDebugEnabled()) {
                    this.f79a.debug(str + q.I("\\M\tX\u0014I\u0012X\u0015O\u001dX\u0015C\u0012\f\u000fO\u0014I\u0011I\\_\u0019@\u0019O\bI\u0018"));
                }
                try {
                    jVar.g();
                    hVar = aVar.a(str);
                    break;
                } catch (IllegalStateException e) {
                    if (this.f79a.isWarnEnabled()) {
                        this.f79a.warn(q.I("m\tX\u0014I\u0012X\u0015O\u001dX\u0015C\u0012\f\u000fO\u0014I\u0011I\\") + str + e.I("\u0001\u0006N\u001c\u0001\u001bT\u0018Q\u0007S\u001cD\f"));
                    }
                }
            } else if (this.f79a.isDebugEnabled()) {
                this.f79a.debug(q.I("o\u0014M\u0010@\u0019B\u001bI\\J\u0013^\\") + str + e.I("\u0001\tT\u001cI\rO\u001cH\u000b@\u001cH\u0007OHR\u000bI\rL\r\u0001\u0006N\u001c\u0001\tW\tH\u0004@\nM\r"));
            }
        }
        if (hVar != null) {
            return hVar;
        }
        throw new com.agilebinary.a.a.a.a.b(e.I("t\u0006@\nM\r\u0001\u001cNHS\rR\u0018N\u0006EHU\u0007\u0001\tO\u0011\u0001\u0007GHU\u0000D\u001bDHB\u0000@\u0004M\rO\u000fD\u001b\u001bH") + map);
    }

    /* access modifiers changed from: protected */
    public List a(j jVar, k kVar) {
        return b;
    }
}
