package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.d.a.b;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.g;
import java.io.InputStream;

public final class o extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final a f67a;
    private boolean b = false;

    public o(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException(b.I("4!\u00147\u000e+\td\u000e*\u00171\u0013d\u00051\u0001\"\u00026G)\u0006=G*\b0G&\u0002d\t1\u000b("));
        }
        this.f67a = aVar;
    }

    public final int available() {
        if (this.f67a instanceof g) {
            return ((g) this.f67a).a();
        }
        return 0;
    }

    public final void close() {
        this.b = true;
    }

    public final int read() {
        if (this.b) {
            return -1;
        }
        return this.f67a.d();
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (this.b) {
            return -1;
        }
        return this.f67a.a(bArr, i, i2);
    }
}
