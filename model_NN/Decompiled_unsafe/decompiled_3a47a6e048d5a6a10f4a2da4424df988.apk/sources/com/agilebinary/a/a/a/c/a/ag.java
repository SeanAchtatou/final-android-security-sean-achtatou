package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.c.b.a.j;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.l;
import com.agilebinary.a.a.a.k.m;
import com.agilebinary.a.a.a.t;
import com.agilebinary.mobilemonitor.client.android.ui.a.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ag extends ah {

    /* renamed from: a  reason: collision with root package name */
    private static final l f26a = new l();
    private static final String[] b = {j.I("\u0015\u0015\u0016p^4\u001a\u001dw\u001d\u001a)C)Cpr\u0018\u0000=WjI#\u001a*@*"), e.I("/H/HF-\u000eiG@'@Gt\u0013-\"EP`\u00077\u0019~Jw\u0010w"), j.I("\u0015\u0015\u001a\u001dw\u001d\u001a4\u001a\u0018rjW=\u0000#IpC)C)")};
    private final String[] c;
    private final boolean d;

    public ag() {
        this(null, false);
    }

    public ag(String[] strArr, boolean z) {
        if (strArr != null) {
            this.c = (String[]) strArr.clone();
        } else {
            this.c = b;
        }
        this.d = z;
        a(e.I("{\u000f\u0019d\u0005c"), new f());
        a(j.I("J1N8"), new m());
        a(e.I("\u000eb\u0007l\u0003c"), new c());
        a(j.I("=[(\u00171]5"), new ac());
        a(e.I("\u0019h\tx\u0018h"), new h());
        a(j.I("3U=W5T$"), new o());
        a(e.I("h\u0012}\u0003\u000f~"), new j(this.c));
    }

    private static /* synthetic */ void a(c cVar, String str, String str2, int i) {
        cVar.a(str);
        cVar.a(j.I("m"));
        if (str2 == null) {
            return;
        }
        if (i > 0) {
            cVar.a('\"');
            cVar.a(str2);
            cVar.a('\"');
            return;
        }
        cVar.a(str2);
    }

    private /* synthetic */ List b(List list) {
        int i;
        Iterator it = list.iterator();
        int i2 = Integer.MAX_VALUE;
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                break;
            }
            com.agilebinary.a.a.a.k.c cVar = (com.agilebinary.a.a.a.k.c) it.next();
            i2 = cVar.h() < i ? cVar.h() : i;
        }
        c cVar2 = new c(list.size() * 40);
        cVar2.a(e.I(")b\u0005f\u0003h"));
        cVar2.a(j.I("\u0000p"));
        cVar2.a(e.I(")<h\u0018~\u0003b\u00040"));
        cVar2.a(Integer.toString(i));
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            cVar2.a(j.I("\u0001p"));
            a(cVar2, (com.agilebinary.a.a.a.k.c) it2.next(), i);
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new com.agilebinary.a.a.a.b.e(cVar2));
        return arrayList;
    }

    private /* synthetic */ List c(List list) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            com.agilebinary.a.a.a.k.c cVar = (com.agilebinary.a.a.a.k.c) it.next();
            int h = cVar.h();
            c cVar2 = new c(40);
            cVar2.a(e.I(")b\u0005f\u0003hP-"));
            cVar2.a(j.I("tl5H#S?Tm"));
            cVar2.a(Integer.toString(h));
            cVar2.a(e.I("Q-"));
            a(cVar2, cVar, h);
            arrayList.add(new com.agilebinary.a.a.a.b.e(cVar2));
        }
        return arrayList;
    }

    public int a() {
        return 1;
    }

    public List a(t tVar, f fVar) {
        if (tVar == null) {
            throw new IllegalArgumentException(j.I("r5[4_\"\u001a=[)\u001a>U$\u001a2_pT%V<"));
        } else if (fVar == null) {
            throw new IllegalArgumentException(e.I("N\u0005b\u0001d\u000f-\u0005\u0003j\u0003cJ`\u000btJc\u0005yJo\u000f-\u0004x\u0006a"));
        } else if (tVar.a().equalsIgnoreCase(j.I("i5N}y?U;S5"))) {
            return a(tVar.c(), fVar);
        } else {
            throw new com.agilebinary.a.a.a.k.e(e.I("?c\u0018h\tb\rc\u0003w\u000fiJn\u0005b\u0001d\u000f-\u0002h\u000bi\u000fJ*") + tVar.toString() + j.I("w"));
        }
    }

    public final List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException(e.I("A\u0003~\u001e-\u0005kJn\u0005b\u0001d\u000f~J`\u000btJc\u0005yJo\u000f-\u0004x\u0006a"));
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException(j.I("v9I$\u001a?\\pY?U;S5IpW1CpT?NpX5\u001a5W N)"));
        } else {
            if (list.size() > 1) {
                ArrayList arrayList = new ArrayList(list);
                Collections.sort(arrayList, f26a);
                list = arrayList;
            }
            return this.d ? b(list) : c(list);
        }
    }

    /* access modifiers changed from: protected */
    public void a(c cVar, com.agilebinary.a.a.a.k.c cVar2, int i) {
        a(cVar, cVar2.a(), cVar2.b(), i);
        if (cVar2.d() != null && (cVar2 instanceof m) && ((m) cVar2).e(e.I("\u001al\u001ee"))) {
            cVar.a(j.I("\u0001p"));
            a(cVar, e.I("):l\u001ee"), cVar2.d(), i);
        }
        if (cVar2.c() != null && (cVar2 instanceof m) && ((m) cVar2).e(j.I("^?W1S>"))) {
            cVar.a(e.I("Q-"));
            a(cVar, j.I("t~?W1S>"), cVar2.c(), i);
        }
    }

    public void a(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(e.I(")b\u0005f\u0003hJ`\u000btJc\u0005yJo\u000f-\u0004x\u0006a"));
        }
        String a2 = cVar.a();
        if (a2.indexOf(32) != -1) {
            throw new i(j.I("y?U;S5\u001a>[=_pW1CpT?NpY?T$[9TpX<[>Q#"));
        } else if (a2.startsWith(e.I(")"))) {
            throw new i(j.I("y?U;S5\u001a>[=_pW1CpT?NpI$[\"NpM9N8\u001at"));
        } else {
            super.a(cVar, fVar);
        }
    }

    public t b() {
        return null;
    }

    public String toString() {
        return e.I("\fnX<Z4");
    }
}
