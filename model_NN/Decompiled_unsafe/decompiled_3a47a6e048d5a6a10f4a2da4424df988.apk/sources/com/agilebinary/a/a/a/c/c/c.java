package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.j.b;
import java.io.InterruptedIOException;
import java.net.Socket;

public final class c extends b implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final Class f58a = g();
    private final Socket b;
    private boolean c;

    public c(Socket socket, int i, e eVar) {
        int i2 = 1024;
        if (socket == null) {
            throw new IllegalArgumentException(org.b.a.a.c.I(" O$I;\f\"M6\f!C;\f-IoB:@#"));
        }
        this.b = socket;
        this.c = false;
        int receiveBufferSize = i < 0 ? socket.getReceiveBufferSize() : i;
        a(socket.getInputStream(), receiveBufferSize >= 1024 ? receiveBufferSize : i2, eVar);
    }

    private static /* synthetic */ Class g() {
        try {
            return Class.forName(com.agilebinary.mobilemonitor.client.a.a.c.I("+\u000b7\u000bo\u0004$\u001eo9.\t*\u000f5>(\u0007$\u00054\u001e\u0004\u0012\"\u000f1\u001e(\u0005/"));
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public final boolean a(int i) {
        boolean c2 = c();
        if (!c2) {
            int soTimeout = this.b.getSoTimeout();
            try {
                this.b.setSoTimeout(i);
                b();
                c2 = c();
            } catch (InterruptedIOException e) {
                if (!(f58a != null ? f58a.isInstance(e) : true)) {
                    throw e;
                }
            } finally {
                this.b.setSoTimeout(soTimeout);
            }
        }
        return c2;
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        this.c = b2 == -1;
        return b2;
    }

    public final boolean f() {
        return this.c;
    }
}
