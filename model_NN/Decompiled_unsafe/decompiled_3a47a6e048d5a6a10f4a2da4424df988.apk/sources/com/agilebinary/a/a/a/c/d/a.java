package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.d.k;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.p;
import com.agilebinary.mobilemonitor.client.a.b.t;
import com.agilebinary.mobilemonitor.client.android.b.g;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import javax.net.ssl.SSLException;

public final class a implements k {

    /* renamed from: a  reason: collision with root package name */
    private final int f68a;
    private final boolean b;

    public a() {
        this((byte) 0);
    }

    private /* synthetic */ a(byte b2) {
        this.f68a = 3;
        this.b = false;
    }

    public final boolean a(IOException iOException, int i, com.agilebinary.a.a.a.f.k kVar) {
        if (iOException == null) {
            throw new IllegalArgumentException(t.I("zu\\hOyVbQ-OlMlRhKhM-RlF-QbK-]h\u001fcJaS"));
        } else if (kVar == null) {
            throw new IllegalArgumentException(g.I("k&w\"\u0003\u0011L\u001cW\u0017[\u0006\u0003\u001fB\u000b\u0003\u001cL\u0006\u0003\u0010FRM\u0007O\u001e"));
        } else if (i > this.f68a) {
            return false;
        } else {
            if (iOException instanceof InterruptedIOException) {
                return false;
            }
            if (iOException instanceof UnknownHostException) {
                return false;
            }
            if (iOException instanceof ConnectException) {
                return false;
            }
            if (iOException instanceof SSLException) {
                return false;
            }
            if (!(((f) kVar.a(t.I("eKyO#MhNxZ~K"))) instanceof p)) {
                return true;
            }
            Boolean bool = (Boolean) kVar.a(g.I("\u001aW\u0006S\\Q\u0017R\u0007F\u0001W-P\u0017M\u0006"));
            return !(bool != null && bool.booleanValue()) || this.b;
        }
    }
}
