package com.agilebinary.a.a.a.g;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.t;

public abstract class f implements c {

    /* renamed from: a  reason: collision with root package name */
    private t f97a;
    private t b;
    private boolean c;

    protected f() {
    }

    public final void a(t tVar) {
        this.f97a = tVar;
    }

    public final void a(boolean z) {
        this.c = z;
    }

    public final void b(t tVar) {
        this.b = tVar;
    }

    public final t d() {
        return this.f97a;
    }

    public final t e() {
        return this.b;
    }

    public final boolean k_() {
        return this.c;
    }
}
