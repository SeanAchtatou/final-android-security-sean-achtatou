package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.h.b.g;
import com.agilebinary.a.a.a.j.e;
import com.jasonkostempski.android.calendar.a;
import java.io.IOException;
import java.io.OutputStream;

public final class j extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final e f64a;
    private final long b;
    private long c = 0;
    private boolean d = false;

    public j(e eVar, long j) {
        if (eVar == null) {
            throw new IllegalArgumentException(g.I(">\u0010\u001e\u0006\u0004\u001a\u0003U\u0002\u0000\u0019\u0005\u0018\u0001M\u0017\u0018\u0013\u000b\u0010\u001fU\u0000\u0014\u0014U\u0003\u001a\u0019U\u000f\u0010M\u001b\u0018\u0019\u0001"));
        } else if (j < 0) {
            throw new IllegalArgumentException(a.I("cDN_EET\u000bLNNLTC\u0000FAR\u0000EO_\u0000IE\u000bNNGJTBVN"));
        } else {
            this.f64a = eVar;
            this.b = j;
        }
    }

    public static String I(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ '{');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ '{');
            i2 = i;
        }
        return new String(cArr);
    }

    public final void close() {
        if (!this.d) {
            this.d = true;
            this.f64a.b();
        }
    }

    public final void flush() {
        this.f64a.b();
    }

    public final void write(int i) {
        if (this.d) {
            throw new IOException(g.I(",\u0001\u0019\u0010\u0000\u0005\u0019\u0010\tU\u001a\u0007\u0004\u0001\bU\u0019\u001aM\u0016\u0001\u001a\u001e\u0010\tU\u001e\u0001\u001f\u0010\f\u0018C"));
        } else if (this.c < this.b) {
            this.f64a.a(i);
            this.c++;
        }
    }

    public final void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public final void write(byte[] bArr, int i, int i2) {
        if (this.d) {
            throw new IOException(a.I("jT_EFP_EO\u0000\\RBTN\u0000_O\u000bCGOXEO\u0000XTYEJM\u0005"));
        } else if (this.c < this.b) {
            long j = this.b - this.c;
            if (((long) i2) > j) {
                i2 = (int) j;
            }
            this.f64a.a(bArr, i, i2);
            this.c += (long) i2;
        }
    }
}
