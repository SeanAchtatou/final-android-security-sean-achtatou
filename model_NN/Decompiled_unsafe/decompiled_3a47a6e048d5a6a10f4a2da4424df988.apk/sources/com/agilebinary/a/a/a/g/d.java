package com.agilebinary.a.a.a.g;

import com.agilebinary.a.a.a.f.b;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.osmdroid.b.a;

public final class d extends f implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f96a;

    public d(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException(a.I("\r#+>=)~.'8;l?>,-'l3-'l0#*l<)~\"+ 2"));
        }
        this.f96a = bArr;
    }

    public final void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException(b.I("\u0004?z>~ky?x.k&*&k2*%e?*)okd>f'"));
        }
        outputStream.write(this.f96a);
        outputStream.flush();
    }

    public final boolean a() {
        return true;
    }

    public final long c() {
        return (long) this.f96a.length;
    }

    public final Object clone() {
        return super.clone();
    }

    public final InputStream f() {
        return new ByteArrayInputStream(this.f96a);
    }

    public final boolean g() {
        return false;
    }
}
