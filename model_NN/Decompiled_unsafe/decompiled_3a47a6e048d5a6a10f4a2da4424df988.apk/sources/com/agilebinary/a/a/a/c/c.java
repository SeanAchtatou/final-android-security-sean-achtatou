package com.agilebinary.a.a.a.c;

import com.agilebinary.a.a.a.ac;
import com.agilebinary.a.a.a.b.k;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.i.f;
import com.agilebinary.a.a.a.j;
import java.util.Locale;

public final class c implements i {

    /* renamed from: a  reason: collision with root package name */
    private ac f55a;

    public c() {
        this(e.f80a);
    }

    private /* synthetic */ c(ac acVar) {
        if (acVar == null) {
            throw new IllegalArgumentException(f.I("iJZ\\TA\u001b_S]Z\\^\u000fXNONW@\\\u000fVZH[\u001bAT[\u001bM^\u000fUZWC\u0015"));
        }
        this.f55a = acVar;
    }

    public final j a(h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException(org.a.a.h.I("\u0003M\u0003Y\u0004\f\u001bE\u0019IWA\u0016UWB\u0018XWN\u0012\f\u0019Y\u001b@"));
        }
        return new k(hVar, this.f55a, Locale.getDefault());
    }
}
