package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.d.d;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.j;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import com.agilebinary.a.a.b.a.a;
import org.apache.commons.logging.Log;
import org.osmdroid.b.a.e;

public final class c implements ad {

    /* renamed from: a  reason: collision with root package name */
    private final Log f86a = a.a(getClass());

    private /* synthetic */ void a(w wVar, j jVar, f fVar, d dVar) {
        while (wVar.hasNext()) {
            t a2 = wVar.a();
            try {
                for (com.agilebinary.a.a.a.k.c cVar : jVar.a(a2, fVar)) {
                    try {
                        jVar.a(cVar, fVar);
                        dVar.a(cVar);
                        if (this.f86a.isDebugEnabled()) {
                            this.f86a.debug(e.I(";{\u0017\u0011qXu\u001bw\u001dd\fq\u001c.X6") + cVar + com.agilebinary.a.a.a.a.a.I("_\u0002]"));
                        }
                    } catch (com.agilebinary.a.a.a.k.e e) {
                        if (this.f86a.isWarnEnabled()) {
                            this.f86a.warn(e.I(";{\u0017\u0011qXf\u001d~\u001dw\fq\u001c.X6") + cVar + com.agilebinary.a.a.a.a.a.I("_\u0002]") + e.getMessage());
                        }
                    }
                }
            } catch (com.agilebinary.a.a.a.k.e e2) {
                if (this.f86a.isWarnEnabled()) {
                    this.f86a.warn(e.I("1z\u000eu\u0014}\u001c4\u001b{\u0017\u0011qX|\u001du\u001cq\n.X6") + a2 + com.agilebinary.a.a.a.a.a.I("_\u0002]") + e2.getMessage());
                }
            }
        }
    }

    public final void a(com.agilebinary.a.a.a.j jVar, k kVar) {
        if (jVar == null) {
            throw new IllegalArgumentException(e.I("0@,DXf\u001de\rq\u000b`Xy\u0019mXz\u0017`Xv\u001d4\u0016a\u0014x"));
        } else if (kVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.a.a.I("d)x-\f\u001eC\u0013X\u0018T\t\f\u0010M\u0004\f\u0013C\t\f\u001fI]B\b@\u0011"));
        } else {
            j jVar2 = (j) kVar.a(e.I("\u0010`\fdVw\u0017{\u0013}\u001d9\u000bd\u001dw"));
            if (jVar2 != null) {
                d dVar = (d) kVar.a(com.agilebinary.a.a.a.a.a.I("\u0015X\t\\SO\u0012C\u0016E\u0018\u0001\u000eX\u0012^\u0018"));
                if (dVar == null) {
                    this.f86a.info(e.I("W\u0017{\u0013}\u001dG\f{\nqXz\u0017`Xu\u000eu\u0011x\u0019v\u0014qX}\u001640@,DXw\u0017z\fq\u0000`"));
                    return;
                }
                f fVar = (f) kVar.a(com.agilebinary.a.a.a.a.a.I("D\tX\r\u0002\u001eC\u0012G\u0014IPC\u000fE\u001aE\u0013"));
                if (fVar == null) {
                    this.f86a.info(e.I(";{\u0017\u0011q7f\u0011s\u0011zXz\u0017`Xu\u000eu\u0011x\u0019v\u0014qX}\u001640@,DXw\u0017z\fq\u0000`"));
                    return;
                }
                a(jVar.d(com.agilebinary.a.a.a.a.a.I("\u0018XPo\u0012C\u0016E\u0018")), jVar2, fVar, dVar);
                if (jVar2.a() > 0) {
                    a(jVar.d(e.I("G\u001d`UW\u0017{\u0013}\u001d&")), jVar2, fVar, dVar);
                }
            }
        }
    }
}
