package com.agilebinary.a.a.a.k;

import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.i.b;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f118a = new ConcurrentHashMap();

    public final j a(String str, e eVar) {
        if (str == null) {
            throw new IllegalArgumentException(b.I("xS[W\u0016_WK\u0016\\YF\u0016PS\u0012XGZ^"));
        }
        g gVar = (g) this.f118a.get(str.toLowerCase(Locale.ENGLISH));
        if (gVar != null) {
            return gVar.a(eVar);
        }
        throw new IllegalStateException(com.agilebinary.a.a.a.b.I("\u001dY;B8G'E<R,\u0017+X'\\!RhD8R+\rh") + str);
    }

    public final void a(String str, g gVar) {
        if (str == null) {
            throw new IllegalArgumentException(b.I("xS[W\u0016_WK\u0016\\YF\u0016PS\u0012XGZ^"));
        } else if (gVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.b.I("\u000bX'\\!RhD8R+\u0017.V+C'E1\u0017%V1\u0017&X<\u0017*RhY=[$"));
        } else {
            this.f118a.put(str.toLowerCase(Locale.ENGLISH), gVar);
        }
    }
}
