package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.a.c;
import com.agilebinary.a.a.a.a.e;
import com.agilebinary.a.a.a.a.h;
import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.c.d.j;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.b.a.a;
import org.apache.commons.logging.Log;

public final class g implements ab {

    /* renamed from: a  reason: collision with root package name */
    private final Log f89a = a.a(getClass());

    private /* synthetic */ void a(b bVar, h hVar, e eVar, com.agilebinary.a.a.a.d.e eVar2) {
        String b = hVar.b();
        if (this.f89a.isDebugEnabled()) {
            this.f89a.debug(a.I("zb\u0005r[nF`\bdId@bL'\u000f") + b + f.I("l\u0016*C?^kE(^.[.\u0016-Y9\u0016") + bVar);
        }
        com.agilebinary.a.a.a.a.g a2 = eVar2.a(new c(bVar.a(), bVar.b(), b));
        if (a2 != null) {
            eVar.a(hVar);
            eVar.a(a2);
            return;
        }
        this.f89a.debug(a.I("IG'KuMcMi\\nIk['NhZ'XuMbEw\\n^b\bf]s@bFsAdIsAhF"));
    }

    public final void a(com.agilebinary.a.a.a.f fVar, k kVar) {
        h a2;
        h a3;
        if (fVar == null) {
            throw new IllegalArgumentException(f.I("\u0003b\u001ffkD.G>S8Bk[*OkX$BkT.\u0016%C'Z"));
        } else if (kVar == null) {
            throw new IllegalArgumentException(a.I("O|Sx'KhFsM\\'EfQ'Fh\\'Jb\bi]kD"));
        } else {
            com.agilebinary.a.a.a.d.g gVar = (com.agilebinary.a.a.a.d.g) kVar.a(f.I("#B?FeW>B#\u0018*C?^fU*U#S"));
            if (gVar == null) {
                kVar.a(a.I("o\\sX)Ir\\o\u0006f]s@*KfKoM"), new j());
                return;
            }
            com.agilebinary.a.a.a.d.e eVar = (com.agilebinary.a.a.a.d.e) kVar.a(f.I("#B?FeW>B#\u0018(D.R.X?_*Z8\u001b;D$@\"R.D"));
            b bVar = (b) kVar.a(a.I("o\\sX)\\fZ`MswoGt\\"));
            if (!(bVar == null || (a3 = gVar.a(bVar)) == null)) {
                a(bVar, a3, (e) kVar.a(f.I("#B?FeW>B#\u0018?W9Q.BfE(Y;S")), eVar);
            }
            b bVar2 = (b) kVar.a(a.I("@s\\w\u0006wZhP~woGt\\"));
            if (bVar2 != null && (a2 = gVar.a(bVar2)) != null) {
                a(bVar2, a2, (e) kVar.a(f.I("^?B;\u0018*C?^eF9Y3OfE(Y;S")), eVar);
            }
        }
    }
}
