package com.agilebinary.a.a.a;

import java.io.Serializable;
import org.osmdroid.c.c;

public class a implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    protected final int f2a;
    protected final int b;
    private String c;

    public a(String str, int i, int i2) {
        if (str == null) {
            throw new IllegalArgumentException(c.I("52\n4\n#\n,E.\u0004-\u0000`\b5\u00164E.\n4E\"\u0000`\u000b5\t,K"));
        } else if (i < 0) {
            throw new IllegalArgumentException(com.jasonkostempski.android.calendar.a.I("{RDTDCDL\u000bMJJDR\u000bVNRXIDN\u000bN^MIEY\u0000FUXT\u000bNDT\u000bBN\u0000EELA_I]E\u0005"));
        } else if (i2 < 0) {
            throw new IllegalArgumentException(c.I("52\n4\n#\n,E-\f.\n2E6\u00002\u0016)\n.E.\u0010-\u0007%\u0017`\b!\u001c`\u000b/\u0011`\u0007%E.\u0000'\u00044\f6\u0000"));
        } else {
            this.c = str;
            this.f2a = i;
            this.b = i2;
        }
    }

    public a a(int i, int i2) {
        return (i == this.f2a && i2 == this.b) ? this : new a(this.c, i, i2);
    }

    public final String a() {
        return this.c;
    }

    public final boolean a(a aVar) {
        if (aVar != null && this.c.equals(aVar.c)) {
            if (aVar == null) {
                throw new IllegalArgumentException(com.jasonkostempski.android.calendar.a.I("pYO_OHOG\u0000]EYSBOE\u0000FUXT\u000bNDT\u000bBN\u0000EUGL\u0005"));
            } else if (!this.c.equals(aVar.c)) {
                throw new IllegalArgumentException(c.I("3%\u00173\f/\u000b3E&\n2E$\f&\u0003%\u0017%\u000b4E0\u0017/\u0011/\u0006/\t3E#\u0004.\u000b/\u0011`\u0007%E#\n-\u0015!\u0017%\u0001nE") + this + com.jasonkostempski.android.calendar.a.I("\u000b") + aVar);
            } else {
                int i = this.f2a - aVar.f2a;
                if (i == 0) {
                    i = this.b - aVar.b;
                }
                if (i <= 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public final int b() {
        return this.f2a;
    }

    public final int c() {
        return this.b;
    }

    public Object clone() {
        return super.clone();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return this.c.equals(aVar.c) && this.f2a == aVar.f2a && this.b == aVar.b;
    }

    public final int hashCode() {
        return (this.c.hashCode() ^ (this.f2a * 100000)) ^ this.b;
    }

    public String toString() {
        com.agilebinary.a.a.a.i.c cVar = new com.agilebinary.a.a.a.i.c(16);
        cVar.a(this.c);
        cVar.a('/');
        cVar.a(Integer.toString(this.f2a));
        cVar.a('.');
        cVar.a(Integer.toString(this.b));
        return cVar.toString();
    }
}
