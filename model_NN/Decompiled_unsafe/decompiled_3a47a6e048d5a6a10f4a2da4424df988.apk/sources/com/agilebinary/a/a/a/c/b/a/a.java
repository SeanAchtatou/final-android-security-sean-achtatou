package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.a.b;
import com.agilebinary.a.a.a.h.f;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.l;
import com.agilebinary.a.a.a.h.m;
import com.agilebinary.mobilemonitor.client.a.a.c;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import org.apache.commons.logging.Log;

public final class a extends j {
    private final Log c = com.agilebinary.a.a.b.a.a.a(getClass());
    /* access modifiers changed from: private */
    public final Lock d;
    private m e;
    private b f;
    private Set g;
    private Queue h;
    private Queue i;
    private Map j;
    private final long k;
    private final TimeUnit l;
    private volatile boolean m;
    private volatile int n;
    private volatile int o;

    public a(m mVar, b bVar, TimeUnit timeUnit) {
        if (mVar == null) {
            throw new IllegalArgumentException(c.I("\u0002\u0005/\u0004$\t5\u0003.\u0004a\u00051\u000f3\u000b5\u00053J,\u000b8J/\u00055J#\u000fa\u00044\u0006-"));
        } else if (bVar == null) {
            throw new IllegalArgumentException(f.I(":Z\u0017[\u001cV\r\\\u0016[\n\u0015\tP\u000b\u0015\u000bZ\fA\u001c\u0015\u0014T\u0000\u0015\u0017Z\r\u0015\u001bPY[\fY\u0015"));
        } else {
            this.d = this.f41a;
            this.g = this.b;
            this.e = mVar;
            this.f = bVar;
            this.n = 20;
            this.h = new LinkedList();
            this.i = new LinkedList();
            this.j = new HashMap();
            this.k = 30;
            this.l = timeUnit;
        }
    }

    private /* synthetic */ c a(com.agilebinary.a.a.a.h.c.c cVar) {
        this.d.lock();
        try {
            c cVar2 = (c) this.j.get(cVar);
            if (cVar2 == null) {
                cVar2 = new c(cVar, this.f);
                this.j.put(cVar, cVar2);
            }
            return cVar2;
        } finally {
            this.d.unlock();
        }
    }

    private /* synthetic */ g a(c cVar, m mVar) {
        if (this.c.isDebugEnabled()) {
            this.c.debug(c.I("\u0002\u0018$\u000b5\u0003/\ra\u0004$\u001da\t.\u0004/\u000f\"\u001e(\u0005/J\u001a") + cVar.a() + f.I("$"));
        }
        g gVar = new g(mVar, cVar.a(), this.k, this.l);
        this.d.lock();
        try {
            cVar.b(gVar);
            this.o++;
            this.g.add(gVar);
            return gVar;
        } finally {
            this.d.unlock();
        }
    }

    private /* synthetic */ g a(c cVar, Object obj) {
        this.d.lock();
        g gVar = null;
        boolean z = false;
        while (!z) {
            try {
                gVar = cVar.a(obj);
                if (gVar != null) {
                    if (this.c.isDebugEnabled()) {
                        this.c.debug(c.I("\u0006\u000f5\u001e(\u0004&J'\u0018$\u000fa\t.\u0004/\u000f\"\u001e(\u0005/J\u001a") + cVar.a() + f.I("h\"") + obj + c.I("\u001c"));
                    }
                    this.h.remove(gVar);
                    if (gVar.a(System.currentTimeMillis())) {
                        if (this.c.isDebugEnabled()) {
                            this.c.debug(f.I(":Y\u0016F\u0010[\u001e\u0015\u001cM\t\\\u000bP\u001d\u0015\u001fG\u001cPYV\u0016[\u0017P\u001aA\u0010Z\u0017\u0015\"") + cVar.a() + c.I("7\u001a") + obj + f.I("$"));
                        }
                        a(gVar);
                        cVar.e();
                        this.o--;
                    } else {
                        this.g.add(gVar);
                        z = true;
                    }
                } else if (this.c.isDebugEnabled()) {
                    this.c.debug(c.I("\u000f\u0005a\f3\u000f$J\"\u0005/\u0004$\t5\u0003.\u00042J\u001a") + cVar.a() + f.I("h\"") + obj + c.I("\u001c"));
                    z = true;
                } else {
                    z = true;
                }
            } catch (Throwable th) {
                this.d.unlock();
                throw th;
            }
        }
        this.d.unlock();
        return gVar;
    }

    private /* synthetic */ void a(g gVar) {
        g c2 = gVar.c();
        if (c2 != null) {
            try {
                c2.k();
            } catch (IOException e2) {
                this.c.debug(f.I("|VzYP\u000bG\u0016GYV\u0015Z\n\\\u0017RYV\u0016[\u0017P\u001aA\u0010Z\u0017"), e2);
            }
        }
    }

    private /* synthetic */ void b(g gVar) {
        com.agilebinary.a.a.a.h.c.c d2 = gVar.d();
        if (this.c.isDebugEnabled()) {
            this.c.debug(c.I("\u0005\u000f-\u000f5\u0003/\ra\t.\u0004/\u000f\"\u001e(\u0005/J\u001a") + d2 + f.I("h\"") + gVar.a() + c.I("\u001c"));
        }
        this.d.lock();
        try {
            a(gVar);
            c a2 = a(d2);
            a2.c(gVar);
            this.o--;
            if (a2.c()) {
                this.j.remove(d2);
            }
        } finally {
            this.d.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public final g a(com.agilebinary.a.a.a.h.c.c cVar, Object obj, long j2, TimeUnit timeUnit, f fVar) {
        Date date = j2 > 0 ? new Date(System.currentTimeMillis() + timeUnit.toMillis(j2)) : null;
        this.d.lock();
        k kVar = null;
        c a2 = a(cVar);
        g gVar = null;
        while (gVar == null) {
            if (!this.m) {
                if (this.c.isDebugEnabled()) {
                    this.c.debug(c.I("\u001a") + cVar + f.I("hYA\u0016A\u0018YY^\u001cE\r\u0015\u0018Y\u0010C\u001c\u000fY") + this.h.size() + c.I("Fa\u001e.\u001e \u0006a\u00032\u00194\u000f%Pa") + this.g.size() + f.I("U\u0015\rZ\rT\u0015\u0015\u0018Y\u0015Z\u001aT\rP\u001d\u000fY") + this.o + c.I("J.\u001f5J.\fa") + this.n);
                }
                gVar = a(a2, obj);
                if (gVar != null) {
                    break;
                }
                boolean z = a2.d() > 0;
                if (this.c.isDebugEnabled()) {
                    this.c.debug(f.I("t\u000fT\u0010Y\u0018W\u0015PYV\u0018E\u0018V\u0010A\u0000\u000fY") + a2.d() + c.I("J.\u001f5J.\fa") + a2.b() + f.I("\u0015\"") + cVar + c.I("7\u001a") + obj + f.I("$"));
                }
                if (z && this.o < this.n) {
                    gVar = a(a2, this.e);
                } else if (!z || this.h.isEmpty()) {
                    if (this.c.isDebugEnabled()) {
                        this.c.debug(f.I("7P\u001cQYA\u0016\u0015\u000eT\u0010AYS\u0016GYV\u0016[\u0017P\u001aA\u0010Z\u0017\u0015\"") + cVar + c.I("7\u001a") + obj + f.I("$"));
                    }
                    if (kVar == null) {
                        kVar = new k(this.d.newCondition(), a2);
                        fVar.a(kVar);
                    }
                    a2.a(kVar);
                    this.i.add(kVar);
                    boolean a3 = kVar.a(date);
                    a2.b(kVar);
                    this.i.remove(kVar);
                    if (!a3 && date != null && date.getTime() <= System.currentTimeMillis()) {
                        throw new l(c.I(">(\u0007$\u00054\u001ea\u001d \u00035\u0003/\ra\f.\u0018a\t.\u0004/\u000f\"\u001e(\u0005/"));
                    }
                } else {
                    this.d.lock();
                    try {
                        g gVar2 = (g) this.h.remove();
                        if (gVar2 != null) {
                            b(gVar2);
                        } else if (this.c.isDebugEnabled()) {
                            this.c.debug(c.I("$.J'\u0018$\u000fa\t.\u0004/\u000f\"\u001e(\u0005/J5\u0005a\u000e$\u0006$\u001e$"));
                        }
                        this.d.unlock();
                        c a4 = a(cVar);
                        a2 = a4;
                        gVar = a(a4, this.e);
                    } catch (Throwable th) {
                        this.d.unlock();
                        throw th;
                    }
                }
            } else {
                throw new IllegalStateException(f.I(":Z\u0017[\u001cV\r\\\u0016[YE\u0016Z\u0015\u0015\n]\fAYQ\u0016B\u0017"));
            }
        }
        this.d.unlock();
        return gVar;
    }

    public final void a() {
        this.d.lock();
        try {
            if (!this.m) {
                this.m = true;
                Iterator it = this.g.iterator();
                while (it.hasNext()) {
                    it.remove();
                    a((g) it.next());
                }
                Iterator it2 = this.h.iterator();
                while (it2.hasNext()) {
                    g gVar = (g) it2.next();
                    it2.remove();
                    if (this.c.isDebugEnabled()) {
                        this.c.debug(f.I("v\u0015Z\n\\\u0017RYV\u0016[\u0017P\u001aA\u0010Z\u0017\u0015\"") + gVar.d() + c.I("7\u001a") + gVar.a() + f.I("$"));
                    }
                    a(gVar);
                }
                Iterator it3 = this.i.iterator();
                while (it3.hasNext()) {
                    it3.remove();
                    ((k) it3.next()).a();
                }
                this.j.clear();
                this.d.unlock();
            }
        } finally {
            this.d.unlock();
        }
    }

    public final void a(long j2, TimeUnit timeUnit) {
        if (timeUnit == null) {
            throw new IllegalArgumentException(c.I("\u0015\u0003,\u000fa\u001f/\u00035J,\u001f2\u001ea\u0004.\u001ea\b$J/\u001f-\u0006o"));
        }
        if (j2 < 0) {
            j2 = 0;
        }
        if (this.c.isDebugEnabled()) {
            this.c.debug(f.I(":Y\u0016F\u0010[\u001e\u0015\u001aZ\u0017[\u001cV\r\\\u0016[\n\u0015\u0010Q\u0015PYY\u0016[\u001eP\u000b\u0015\r]\u0018[Y") + j2 + c.I("a") + timeUnit);
        }
        long currentTimeMillis = System.currentTimeMillis();
        long millis = timeUnit.toMillis(j2);
        this.d.lock();
        try {
            Iterator it = this.h.iterator();
            while (it.hasNext()) {
                g gVar = (g) it.next();
                if (gVar.e() <= currentTimeMillis - millis) {
                    if (this.c.isDebugEnabled()) {
                        this.c.debug(f.I(":Y\u0016F\u0010[\u001e\u0015\u001aZ\u0017[\u001cV\r\\\u0016[YY\u0018F\r\u0015\fF\u001cQYuY") + new Date(gVar.e()));
                    }
                    it.remove();
                    b(gVar);
                }
            }
        } finally {
            this.d.unlock();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0117 A[Catch:{ all -> 0x0177, all -> 0x0138 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.c.b.a.g r8, boolean r9, long r10, java.util.concurrent.TimeUnit r12) {
        /*
            r7 = this;
            com.agilebinary.a.a.a.h.c.c r1 = r8.d()
            org.apache.commons.logging.Log r0 = r7.c
            boolean r0 = r0.isDebugEnabled()
            if (r0 == 0) goto L_0x0044
            org.apache.commons.logging.Log r0 = r7.c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "8$\u0006$\u000b2\u0003/\ra\t.\u0004/\u000f\"\u001e(\u0005/J\u001a"
            java.lang.String r3 = com.agilebinary.mobilemonitor.client.a.a.c.I(r3)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r3 = "h\""
            java.lang.String r3 = com.agilebinary.a.a.a.h.f.I(r3)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.Object r3 = r8.a()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "\u001c"
            java.lang.String r3 = com.agilebinary.mobilemonitor.client.a.a.c.I(r3)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.debug(r2)
        L_0x0044:
            java.util.concurrent.locks.Lock r0 = r7.d
            r0.lock()
            boolean r0 = r7.m     // Catch:{ all -> 0x0138 }
            if (r0 == 0) goto L_0x0056
            r7.a(r8)     // Catch:{ all -> 0x0138 }
            java.util.concurrent.locks.Lock r0 = r7.d
            r0.unlock()
        L_0x0055:
            return
        L_0x0056:
            java.util.Set r0 = r7.g     // Catch:{ all -> 0x0138 }
            r0.remove(r8)     // Catch:{ all -> 0x0138 }
            com.agilebinary.a.a.a.c.b.a.c r2 = r7.a(r1)     // Catch:{ all -> 0x0138 }
            if (r9 == 0) goto L_0x012e
            org.apache.commons.logging.Log r0 = r7.c     // Catch:{ all -> 0x0138 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0138 }
            if (r0 == 0) goto L_0x00c6
            r4 = 0
            int r0 = (r10 > r4 ? 1 : (r10 == r4 ? 0 : -1))
            if (r0 < 0) goto L_0x0126
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0138 }
            r0.<init>()     // Catch:{ all -> 0x0138 }
            java.lang.StringBuilder r0 = r0.append(r10)     // Catch:{ all -> 0x0138 }
            java.lang.String r3 = "Y"
            java.lang.String r3 = com.agilebinary.a.a.a.h.f.I(r3)     // Catch:{ all -> 0x0138 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x0138 }
            java.lang.StringBuilder r0 = r0.append(r12)     // Catch:{ all -> 0x0138 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0138 }
        L_0x008a:
            org.apache.commons.logging.Log r3 = r7.c     // Catch:{ all -> 0x0138 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0138 }
            r4.<init>()     // Catch:{ all -> 0x0138 }
            java.lang.String r5 = ":.\u0005-\u0003/\ra\t.\u0004/\u000f\"\u001e(\u0005/J\u001a"
            java.lang.String r5 = com.agilebinary.mobilemonitor.client.a.a.c.I(r5)     // Catch:{ all -> 0x0138 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0138 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x0138 }
            java.lang.String r4 = "h\""
            java.lang.String r4 = com.agilebinary.a.a.a.h.f.I(r4)     // Catch:{ all -> 0x0138 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x0138 }
            java.lang.Object r4 = r8.a()     // Catch:{ all -> 0x0138 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x0138 }
            java.lang.String r4 = "7zJ*\u000f$\u001aa\u000b-\u00037\u000fa\f.\u0018a"
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.a.a.c.I(r4)     // Catch:{ all -> 0x0138 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x0138 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0138 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0138 }
            r3.debug(r0)     // Catch:{ all -> 0x0138 }
        L_0x00c6:
            r2.a(r8)     // Catch:{ all -> 0x0138 }
            r8.a(r10, r12)     // Catch:{ all -> 0x0138 }
            java.util.Queue r0 = r7.h     // Catch:{ all -> 0x0138 }
            r0.add(r8)     // Catch:{ all -> 0x0138 }
        L_0x00d1:
            r0 = 0
            java.util.concurrent.locks.Lock r1 = r7.d     // Catch:{ all -> 0x0138 }
            r1.lock()     // Catch:{ all -> 0x0138 }
            if (r2 == 0) goto L_0x013f
            boolean r1 = r2.f()     // Catch:{ all -> 0x0177 }
            if (r1 == 0) goto L_0x013f
            org.apache.commons.logging.Log r0 = r7.c     // Catch:{ all -> 0x0177 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0177 }
            if (r0 == 0) goto L_0x0111
            org.apache.commons.logging.Log r0 = r7.c     // Catch:{ all -> 0x0177 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0177 }
            r1.<init>()     // Catch:{ all -> 0x0177 }
            java.lang.String r3 = "{\u0016A\u0010S\u0000\\\u0017RYA\u0011G\u001cT\u001d\u0015\u000eT\u0010A\u0010[\u001e\u0015\u0016[YE\u0016Z\u0015\u0015\""
            java.lang.String r3 = com.agilebinary.a.a.a.h.f.I(r3)     // Catch:{ all -> 0x0177 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0177 }
            com.agilebinary.a.a.a.h.c.c r3 = r2.a()     // Catch:{ all -> 0x0177 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0177 }
            java.lang.String r3 = "\u001c"
            java.lang.String r3 = com.agilebinary.mobilemonitor.client.a.a.c.I(r3)     // Catch:{ all -> 0x0177 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0177 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0177 }
            r0.debug(r1)     // Catch:{ all -> 0x0177 }
        L_0x0111:
            com.agilebinary.a.a.a.c.b.a.k r0 = r2.g()     // Catch:{ all -> 0x0177 }
        L_0x0115:
            if (r0 == 0) goto L_0x011a
            r0.a()     // Catch:{ all -> 0x0177 }
        L_0x011a:
            java.util.concurrent.locks.Lock r0 = r7.d     // Catch:{ all -> 0x0138 }
            r0.unlock()     // Catch:{ all -> 0x0138 }
            java.util.concurrent.locks.Lock r0 = r7.d
            r0.unlock()
            goto L_0x0055
        L_0x0126:
            java.lang.String r0 = "P\u000fP\u000b"
            java.lang.String r0 = com.agilebinary.a.a.a.h.f.I(r0)
            goto L_0x008a
        L_0x012e:
            r2.e()     // Catch:{ all -> 0x0138 }
            int r0 = r7.o     // Catch:{ all -> 0x0138 }
            int r0 = r0 + -1
            r7.o = r0     // Catch:{ all -> 0x0138 }
            goto L_0x00d1
        L_0x0138:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r7.d
            r1.unlock()
            throw r0
        L_0x013f:
            java.util.Queue r1 = r7.i     // Catch:{ all -> 0x0177 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0177 }
            if (r1 != 0) goto L_0x0163
            org.apache.commons.logging.Log r0 = r7.c     // Catch:{ all -> 0x0177 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0177 }
            if (r0 == 0) goto L_0x015a
            org.apache.commons.logging.Log r0 = r7.c     // Catch:{ all -> 0x0177 }
            java.lang.String r1 = "$.\u001e(\f8\u0003/\ra\u001e)\u0018$\u000b%J6\u000b(\u001e(\u0004&J.\u0004a\u000b/\u0013a\u001a.\u0005-"
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.a.a.c.I(r1)     // Catch:{ all -> 0x0177 }
            r0.debug(r1)     // Catch:{ all -> 0x0177 }
        L_0x015a:
            java.util.Queue r0 = r7.i     // Catch:{ all -> 0x0177 }
            java.lang.Object r0 = r0.remove()     // Catch:{ all -> 0x0177 }
            com.agilebinary.a.a.a.c.b.a.k r0 = (com.agilebinary.a.a.a.c.b.a.k) r0     // Catch:{ all -> 0x0177 }
            goto L_0x0115
        L_0x0163:
            org.apache.commons.logging.Log r1 = r7.c     // Catch:{ all -> 0x0177 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x0177 }
            if (r1 == 0) goto L_0x0115
            org.apache.commons.logging.Log r1 = r7.c     // Catch:{ all -> 0x0177 }
            java.lang.String r2 = "{\u0016A\u0010S\u0000\\\u0017RY[\u0016\u0018\u0016[\u001c\u0019YA\u0011P\u000bPYT\u000bPY[\u0016\u0015\u000eT\u0010A\u0010[\u001e\u0015\r]\u000bP\u0018Q\n"
            java.lang.String r2 = com.agilebinary.a.a.a.h.f.I(r2)     // Catch:{ all -> 0x0177 }
            r1.debug(r2)     // Catch:{ all -> 0x0177 }
            goto L_0x0115
        L_0x0177:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r7.d     // Catch:{ all -> 0x0138 }
            r1.unlock()     // Catch:{ all -> 0x0138 }
            throw r0     // Catch:{ all -> 0x0138 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.c.b.a.g, boolean, long, java.util.concurrent.TimeUnit):void");
    }

    public final void b() {
        this.d.lock();
        try {
            this.n = 3;
        } finally {
            this.d.unlock();
        }
    }
}
