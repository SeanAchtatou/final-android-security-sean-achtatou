package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.j.f;
import com.agilebinary.a.a.a.j.g;
import java.io.OutputStream;
import org.osmdroid.b.b.a;

public abstract class d implements e, g {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f59a = {13, 10};
    private OutputStream b;
    private com.agilebinary.a.a.a.i.e c;
    private String d = "US-ASCII";
    private boolean e = true;
    private int f = 512;
    private i g;

    private /* synthetic */ void a(byte[] bArr) {
        if (bArr != null) {
            a(bArr, 0, bArr.length);
        }
    }

    private /* synthetic */ void d() {
        int d2 = this.c.d();
        if (d2 > 0) {
            this.b.write(this.c.e(), 0, d2);
            this.c.a();
            this.g.a((long) d2);
        }
    }

    public final int a() {
        return this.c.d();
    }

    public final void a(int i) {
        if (this.c.g()) {
            d();
        }
        this.c.a(i);
    }

    public final void a(c cVar) {
        if (cVar != null) {
            if (this.e) {
                int c2 = cVar.c();
                int i = 0;
                while (c2 > 0) {
                    int min = Math.min(this.c.c() - this.c.d(), c2);
                    if (min > 0) {
                        com.agilebinary.a.a.a.i.e eVar = this.c;
                        if (cVar != null) {
                            eVar.a(cVar.b(), i, min);
                        }
                    }
                    if (this.c.g()) {
                        d();
                    }
                    i += min;
                    c2 -= min;
                }
            } else {
                a(cVar.toString().getBytes(this.d));
            }
            a(f59a);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(OutputStream outputStream, int i, com.agilebinary.a.a.a.e.e eVar) {
        if (outputStream == null) {
            throw new IllegalArgumentException(a.I(":v\u0003m\u00078\u0000l\u0001}\u0012uSu\u0012aSv\u001clSz\u00168\u001dm\u001ft"));
        } else if (i <= 0) {
            throw new IllegalArgumentException(org.b.a.a.c.I("\rY)J*^o_&V*\f\"M6\f!C;\f-IoB*K.X&Z*\f ^oV*^ "));
        } else if (eVar == null) {
            throw new IllegalArgumentException(a.I("P'L#8\u0003y\u0001y\u001e}\u0007}\u0001kSu\u0012aSv\u001clSz\u00168\u001dm\u001ft"));
        } else {
            this.b = outputStream;
            this.c = new com.agilebinary.a.a.a.i.e(i);
            this.d = b.a(eVar);
            this.e = this.d.equalsIgnoreCase("US-ASCII") || this.d.equalsIgnoreCase(org.b.a.a.c.I("\u000e\fe\u0006"));
            this.f = eVar.a(a.I("p\u0007l\u00036\u0010w\u001dv\u0016{\u0007q\u001cv]u\u001av^{\u001bm\u001ds^t\u001au\u001al"), 512);
            this.g = new i();
        }
    }

    public final void a(String str) {
        if (str != null) {
            if (str.length() > 0) {
                a(str.getBytes(this.d));
            }
            a(f59a);
        }
    }

    public final void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i2 > this.f || i2 > this.c.c()) {
                d();
                this.b.write(bArr, i, i2);
                this.g.a((long) i2);
                return;
            }
            if (i2 > this.c.c() - this.c.d()) {
                d();
            }
            this.c.a(bArr, i, i2);
        }
    }

    public final void b() {
        d();
        this.b.flush();
    }

    public final f c() {
        return this.g;
    }
}
