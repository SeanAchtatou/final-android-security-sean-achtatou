package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.c.f;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.m;
import java.io.IOException;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected final g f35a;
    protected volatile c b;
    protected volatile f c;
    private m d;
    private volatile Object e;

    protected a(m mVar, c cVar) {
        if (mVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.m.I("\\pqqz|kvpq?pozm~kpm?r~f?qpk?}z?qjss"));
        }
        this.d = mVar;
        this.f35a = mVar.a();
        this.b = cVar;
        this.c = null;
    }

    public final Object a() {
        return this.e;
    }

    public final void a(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.i.f.I("Z]ZB^[^]H\u000fVZH[\u001bAT[\u001bM^\u000fUZWC\u0015"));
        } else if (this.c == null || !this.c.g()) {
            throw new IllegalStateException(com.agilebinary.a.a.a.c.c.m.I("\\pqqz|kvpq?qpk?pozq1"));
        } else if (!this.c.d()) {
            throw new IllegalStateException(com.agilebinary.a.a.a.i.f.I("k]T[TLTC\u001bCZV^]RA\\\u000fLFOGTZO\u000fZ\u000fOZUA^C\u001bAT[\u001b\\N_K@I[^K\u0015"));
        } else if (this.c.e()) {
            throw new IllegalStateException(com.agilebinary.a.a.a.c.c.m.I("Rjskvosz?ompkp|ps?s~fzmvqx?qpk?ljoopmkz{1"));
        } else {
            this.d.a(this.f35a, this.c.a(), eVar);
            this.c.c(this.f35a.h_());
        }
    }

    public final void a(c cVar, e eVar) {
        if (cVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.i.f.I("i@N[^\u000fVZH[\u001bAT[\u001bM^\u000fUZWC\u0015"));
        } else if (eVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.c.c.m.I("O~m~rzkzml?rjlk?qpk?}z?qjss1"));
        } else if (this.c == null || !this.c.g()) {
            this.c = new f(cVar);
            b g = cVar.g();
            this.d.a(this.f35a, g != null ? g : cVar.a(), cVar.b(), eVar);
            f fVar = this.c;
            if (fVar == null) {
                throw new IOException(com.agilebinary.a.a.a.c.c.m.I("Mznjzlk?~}pmkz{"));
            } else if (g == null) {
                fVar.a(this.f35a.h_());
            } else {
                fVar.a(g, this.f35a.h_());
            }
        } else {
            throw new IllegalStateException(com.agilebinary.a.a.a.i.f.I("lTAUJX[R@U\u000fZCIJZKB\u000fT_^A\u0015"));
        }
    }

    public final void a(Object obj) {
        this.e = obj;
    }

    public final void a(boolean z, e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException(com.agilebinary.a.a.a.i.f.I("Z]ZB^[^]H\u000fVZH[\u001bAT[\u001bM^\u000fUZWC\u0015"));
        } else if (this.c == null || !this.c.g()) {
            throw new IllegalStateException(com.agilebinary.a.a.a.c.c.m.I("\\pqqz|kvpq?qpk?pozq1"));
        } else if (this.c.d()) {
            throw new IllegalStateException(com.agilebinary.a.a.a.i.f.I("lTAUJX[R@U\u000fR\\\u001bNW]^N_V\u001b[NAUJWC^K\u0015"));
        } else {
            this.f35a.a(null, this.c.a(), false, eVar);
            this.c.b(false);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.c = null;
        this.e = null;
    }
}
