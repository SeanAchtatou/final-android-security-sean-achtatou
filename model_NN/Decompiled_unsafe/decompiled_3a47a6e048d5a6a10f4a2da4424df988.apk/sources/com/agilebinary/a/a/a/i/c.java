package com.agilebinary.a.a.a.i;

import com.agilebinary.a.a.a.c.b.e;
import com.agilebinary.a.a.a.f.d;
import java.io.Serializable;

public final class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private char[] f115a;
    private int b;

    public c(int i) {
        if (i < 0) {
            throw new IllegalArgumentException(e.I("#\f\u0007\u001f\u0004\u000bA\u001a\u0000\t\u0000\u001a\b\r\u0018Y\f\u0018\u0018Y\u000f\u0016\u0015Y\u0003\u001cA\u0017\u0004\u001e\u0000\r\b\u000f\u0004"));
        }
        this.f115a = new char[i];
    }

    private /* synthetic */ void d(int i) {
        char[] cArr = new char[Math.max(this.f115a.length << 1, i)];
        System.arraycopy(this.f115a, 0, cArr, 0, this.b);
        this.f115a = cArr;
    }

    public final char a(int i) {
        return this.f115a[i];
    }

    public final int a(int i, int i2, int i3) {
        if (i2 < 0) {
            i2 = 0;
        }
        if (i3 > this.b) {
            i3 = this.b;
        }
        if (i2 > i3) {
            return -1;
        }
        for (int i4 = i2; i4 < i3; i4++) {
            if (this.f115a[i4] == i) {
                return i4;
            }
        }
        return -1;
    }

    public final String a(int i, int i2) {
        return new String(this.f115a, i, i2 - i);
    }

    public final void a() {
        this.b = 0;
    }

    public final void a(char c) {
        int i = this.b + 1;
        if (i > this.f115a.length) {
            d(i);
        }
        this.f115a[this.b] = c;
        this.b = i;
    }

    public final void a(c cVar, int i, int i2) {
        if (cVar != null) {
            a(cVar.f115a, i, i2);
        }
    }

    public final void a(String str) {
        if (str == null) {
            str = b.I("XGZ^");
        }
        int length = str.length();
        int i = this.b + length;
        if (i > this.f115a.length) {
            d(i);
        }
        str.getChars(0, length, this.f115a, this.b);
        this.b = i;
    }

    public final void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i < 0 || i > bArr.length || i2 < 0 || i + i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException(e.I("\u000e\u001f\u0007CA") + i + b.I("\u0016^S\\\f\u0012") + i2 + e.I("A\u001bO\u0015\u0004\u0017\u0006\r\tCA") + bArr.length);
            } else if (i2 != 0) {
                int i3 = this.b;
                int i4 = i3 + i2;
                if (i4 > this.f115a.length) {
                    d(i4);
                }
                while (i3 < i4) {
                    this.f115a[i3] = (char) (bArr[i] & 255);
                    i++;
                    i3++;
                }
                this.b = i4;
            }
        }
    }

    public final void a(char[] cArr, int i, int i2) {
        if (cArr != null) {
            if (i < 0 || i > cArr.length || i2 < 0 || i + i2 < 0 || i + i2 > cArr.length) {
                throw new IndexOutOfBoundsException(b.I("]PT\f\u0012") + i + e.I("Y\r\u001c\u000fCA") + i2 + b.I("\u0012T\u001cZWXUBZ\f\u0012") + cArr.length);
            } else if (i2 != 0) {
                int i3 = this.b + i2;
                if (i3 > this.f115a.length) {
                    d(i3);
                }
                System.arraycopy(cArr, i, this.f115a, this.b, i2);
                this.b = i3;
            }
        }
    }

    public final String b(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException(e.I("/\u001c\u0006\u0018\u0015\u0010\u0017\u001cA\u001b\u0004\u001e\b\u0017(\u0017\u0005\u001c\u0019CA") + i);
        } else if (i2 > this.b) {
            throw new IndexOutOfBoundsException(b.I("S\\R{XVSJ\f\u0012") + i2 + e.I("AGA\u0015\u0004\u0017\u0006\r\tCA") + this.b);
        } else if (i > i2) {
            throw new IndexOutOfBoundsException(b.I("TWQ[X{XVSJ\f\u0012") + i + e.I("AGA\u001c\u000f\u001d(\u0017\u0005\u001c\u0019CA") + i2);
        } else {
            while (i < i2 && d.a(this.f115a[i])) {
                i++;
            }
            while (i2 > i && d.a(this.f115a[i2 - 1])) {
                i2--;
            }
            return new String(this.f115a, i, i2 - i);
        }
    }

    public final void b(int i) {
        if (i > 0 && i > this.f115a.length - this.b) {
            d(this.b + i);
        }
    }

    public final char[] b() {
        return this.f115a;
    }

    public final int c() {
        return this.b;
    }

    public final int c(int i) {
        return a(i, 0, this.b);
    }

    public final boolean d() {
        return this.b == 0;
    }

    public final String toString() {
        return new String(this.f115a, 0, this.b);
    }
}
