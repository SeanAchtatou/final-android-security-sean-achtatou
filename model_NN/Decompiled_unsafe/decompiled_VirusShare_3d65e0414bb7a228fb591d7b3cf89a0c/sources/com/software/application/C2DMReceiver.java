package com.software.application;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.google.android.c2dm.C2DMBaseReceiver;

public class C2DMReceiver extends C2DMBaseReceiver {
    public C2DMReceiver() {
        super("noviigfastn@gmail.com");
    }

    public void onError(Context context, String errorId) {
    }

    /* access modifiers changed from: protected */
    public void onMessage(Context context, Intent intent) {
        String title = intent.getStringExtra("title");
        String url = intent.getStringExtra("url");
        String body = intent.getStringExtra("body");
        if (title == null || url == null || body == null) {
            Log.d("DEBUGGING", "message: null");
            return;
        }
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent("android.intent.action.VIEW", Uri.parse(url)), 268435456);
        Notification notification = new Notification(R.drawable.ic_push, title, System.currentTimeMillis());
        notification.setLatestEventInfo(context, title, body, contentIntent);
        notification.flags = 20;
        ((NotificationManager) context.getSystemService("notification")).notify(999, notification);
    }

    public void onRegistered(Context context, String registrationID) {
        DeviceRegistrar.registerToServer(context, registrationID);
    }

    public void onUnregistered(Context context) {
        String id = context.getSharedPreferences(Main.PREFS, 0).getString("deviceRegistrationID", null);
        if (id != null) {
            DeviceRegistrar.unregisterToServer(context, id);
        }
    }
}
