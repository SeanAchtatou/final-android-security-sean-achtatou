package ad.notify;

import java.io.Serializable;

public class SettingsSet implements Serializable {
    public boolean isFirstStart;
    public String sms = "";
    public long time;
}
