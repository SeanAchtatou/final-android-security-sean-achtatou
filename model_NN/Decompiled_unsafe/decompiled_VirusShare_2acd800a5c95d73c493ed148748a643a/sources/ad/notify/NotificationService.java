package ad.notify;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public final class NotificationService extends Service implements Runnable {
    public void onCreate() {
        super.onCreate();
        setForeground(true);
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
    }

    public void run() {
    }
}
