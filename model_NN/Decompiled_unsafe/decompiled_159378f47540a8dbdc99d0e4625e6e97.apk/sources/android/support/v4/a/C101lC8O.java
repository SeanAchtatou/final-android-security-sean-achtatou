package android.support.v4.a;

import android.support.v4.c.C11013l3;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class C101lC8O {
    int C01O0C;
    C11013l3 C0I1O3C3lI8;
    boolean C101lC8O;
    boolean C11013l3;
    boolean C11ll3;
    boolean C18Cl1C;
    boolean C1l00I1;

    public String C01O0C(Object obj) {
        StringBuilder sb = new StringBuilder(64);
        C11013l3.C01O0C(obj, sb);
        sb.append("}");
        return sb.toString();
    }

    public final void C01O0C() {
        this.C101lC8O = true;
        this.C11ll3 = false;
        this.C11013l3 = false;
        C0I1O3C3lI8();
    }

    public void C01O0C(int i, C11013l3 c11013l3) {
        if (this.C0I1O3C3lI8 != null) {
            throw new IllegalStateException("There is already a listener registered");
        }
        this.C0I1O3C3lI8 = c11013l3;
        this.C01O0C = i;
    }

    public void C01O0C(C11013l3 c11013l3) {
        if (this.C0I1O3C3lI8 == null) {
            throw new IllegalStateException("No listener register");
        } else if (this.C0I1O3C3lI8 != c11013l3) {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        } else {
            this.C0I1O3C3lI8 = null;
        }
    }

    public void C01O0C(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.C01O0C);
        printWriter.print(" mListener=");
        printWriter.println(this.C0I1O3C3lI8);
        if (this.C101lC8O || this.C18Cl1C || this.C1l00I1) {
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.C101lC8O);
            printWriter.print(" mContentChanged=");
            printWriter.print(this.C18Cl1C);
            printWriter.print(" mProcessingChange=");
            printWriter.println(this.C1l00I1);
        }
        if (this.C11013l3 || this.C11ll3) {
            printWriter.print(str);
            printWriter.print("mAbandoned=");
            printWriter.print(this.C11013l3);
            printWriter.print(" mReset=");
            printWriter.println(this.C11ll3);
        }
    }

    /* access modifiers changed from: protected */
    public void C0I1O3C3lI8() {
    }

    public void C101lC8O() {
        this.C101lC8O = false;
        C11013l3();
    }

    /* access modifiers changed from: protected */
    public void C11013l3() {
    }

    public void C11ll3() {
        C18Cl1C();
        this.C11ll3 = true;
        this.C101lC8O = false;
        this.C11013l3 = false;
        this.C18Cl1C = false;
        this.C1l00I1 = false;
    }

    /* access modifiers changed from: protected */
    public void C18Cl1C() {
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        C11013l3.C01O0C(this, sb);
        sb.append(" id=");
        sb.append(this.C01O0C);
        sb.append("}");
        return sb.toString();
    }
}
