package android.support.v4.a;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;

public class C01O0C {
    public static final Drawable C01O0C(Context context, int i) {
        return Build.VERSION.SDK_INT >= 21 ? C0I1O3C3lI8.C01O0C(context, i) : context.getResources().getDrawable(i);
    }
}
