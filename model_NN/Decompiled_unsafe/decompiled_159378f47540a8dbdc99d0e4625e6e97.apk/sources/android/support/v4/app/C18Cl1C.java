package android.support.v4.app;

import android.support.v4.c.C01O0C;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;

class C18Cl1C implements ViewTreeObserver.OnPreDrawListener {
    final /* synthetic */ View C01O0C;
    final /* synthetic */ Object C0I1O3C3lI8;
    final /* synthetic */ ArrayList C101lC8O;
    final /* synthetic */ C1OC33O0lO81 C11013l3;
    final /* synthetic */ boolean C11ll3;
    final /* synthetic */ Fragment C18Cl1C;
    final /* synthetic */ C11013l3 C1O10Cl038;
    final /* synthetic */ Fragment C1l00I1;

    C18Cl1C(C11013l3 c11013l3, View view, Object obj, ArrayList arrayList, C1OC33O0lO81 c1OC33O0lO81, boolean z, Fragment fragment, Fragment fragment2) {
        this.C1O10Cl038 = c11013l3;
        this.C01O0C = view;
        this.C0I1O3C3lI8 = obj;
        this.C101lC8O = arrayList;
        this.C11013l3 = c1OC33O0lO81;
        this.C11ll3 = z;
        this.C18Cl1C = fragment;
        this.C1l00I1 = fragment2;
    }

    public boolean onPreDraw() {
        this.C01O0C.getViewTreeObserver().removeOnPreDrawListener(this);
        if (this.C0I1O3C3lI8 == null) {
            return true;
        }
        CO30CC1l0313.C01O0C(this.C0I1O3C3lI8, this.C101lC8O);
        this.C101lC8O.clear();
        C01O0C C01O0C2 = this.C1O10Cl038.C01O0C(this.C11013l3, this.C11ll3, this.C18Cl1C);
        this.C101lC8O.add(this.C11013l3.C11013l3);
        this.C101lC8O.addAll(C01O0C2.values());
        CO30CC1l0313.C0I1O3C3lI8(this.C0I1O3C3lI8, this.C101lC8O);
        this.C1O10Cl038.C01O0C(C01O0C2, this.C11013l3);
        this.C1O10Cl038.C01O0C(this.C11013l3, this.C18Cl1C, this.C1l00I1, this.C11ll3, C01O0C2);
        return true;
    }
}
