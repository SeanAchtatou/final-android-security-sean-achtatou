package android.support.v4.app;

import android.support.v4.c.C11013l3;
import android.support.v4.c.C3l3O8lIOIO8;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;

class I0IC1O01888 extends I08O3C {
    static boolean C01O0C = false;
    final C3l3O8lIOIO8 C0I1O3C3lI8 = new C3l3O8lIOIO8();
    final C3l3O8lIOIO8 C101lC8O = new C3l3O8lIOIO8();
    final String C11013l3;
    C3llC38O1 C11ll3;
    boolean C18Cl1C;
    boolean C1l00I1;

    I0IC1O01888(String str, C3llC38O1 c3llC38O1, boolean z) {
        this.C11013l3 = str;
        this.C11ll3 = c3llC38O1;
        this.C18Cl1C = z;
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(C3llC38O1 c3llC38O1) {
        this.C11ll3 = c3llC38O1;
    }

    public void C01O0C(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (this.C0I1O3C3lI8.C0I1O3C3lI8() > 0) {
            printWriter.print(str);
            printWriter.println("Active Loaders:");
            String str2 = str + "    ";
            for (int i = 0; i < this.C0I1O3C3lI8.C0I1O3C3lI8(); i++) {
                I0l3Oll3 i0l3Oll3 = (I0l3Oll3) this.C0I1O3C3lI8.C0I1O3C3lI8(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.C0I1O3C3lI8.C01O0C(i));
                printWriter.print(": ");
                printWriter.println(i0l3Oll3.toString());
                i0l3Oll3.C01O0C(str2, fileDescriptor, printWriter, strArr);
            }
        }
        if (this.C101lC8O.C0I1O3C3lI8() > 0) {
            printWriter.print(str);
            printWriter.println("Inactive Loaders:");
            String str3 = str + "    ";
            for (int i2 = 0; i2 < this.C101lC8O.C0I1O3C3lI8(); i2++) {
                I0l3Oll3 i0l3Oll32 = (I0l3Oll3) this.C101lC8O.C0I1O3C3lI8(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.C101lC8O.C01O0C(i2));
                printWriter.print(": ");
                printWriter.println(i0l3Oll32.toString());
                i0l3Oll32.C01O0C(str3, fileDescriptor, printWriter, strArr);
            }
        }
    }

    public boolean C01O0C() {
        int C0I1O3C3lI82 = this.C0I1O3C3lI8.C0I1O3C3lI8();
        boolean z = false;
        for (int i = 0; i < C0I1O3C3lI82; i++) {
            I0l3Oll3 i0l3Oll3 = (I0l3Oll3) this.C0I1O3C3lI8.C0I1O3C3lI8(i);
            z |= i0l3Oll3.C1O10Cl038 && !i0l3Oll3.C18Cl1C;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void C0I1O3C3lI8() {
        if (C01O0C) {
            Log.v("LoaderManager", "Starting in " + this);
        }
        if (this.C18Cl1C) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStart when already started: " + this, runtimeException);
            return;
        }
        this.C18Cl1C = true;
        for (int C0I1O3C3lI82 = this.C0I1O3C3lI8.C0I1O3C3lI8() - 1; C0I1O3C3lI82 >= 0; C0I1O3C3lI82--) {
            ((I0l3Oll3) this.C0I1O3C3lI8.C0I1O3C3lI8(C0I1O3C3lI82)).C01O0C();
        }
    }

    /* access modifiers changed from: package-private */
    public void C101lC8O() {
        if (C01O0C) {
            Log.v("LoaderManager", "Stopping in " + this);
        }
        if (!this.C18Cl1C) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStop when not started: " + this, runtimeException);
            return;
        }
        for (int C0I1O3C3lI82 = this.C0I1O3C3lI8.C0I1O3C3lI8() - 1; C0I1O3C3lI82 >= 0; C0I1O3C3lI82--) {
            ((I0l3Oll3) this.C0I1O3C3lI8.C0I1O3C3lI8(C0I1O3C3lI82)).C11ll3();
        }
        this.C18Cl1C = false;
    }

    /* access modifiers changed from: package-private */
    public void C11013l3() {
        if (C01O0C) {
            Log.v("LoaderManager", "Retaining in " + this);
        }
        if (!this.C18Cl1C) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doRetain when not started: " + this, runtimeException);
            return;
        }
        this.C1l00I1 = true;
        this.C18Cl1C = false;
        for (int C0I1O3C3lI82 = this.C0I1O3C3lI8.C0I1O3C3lI8() - 1; C0I1O3C3lI82 >= 0; C0I1O3C3lI82--) {
            ((I0l3Oll3) this.C0I1O3C3lI8.C0I1O3C3lI8(C0I1O3C3lI82)).C0I1O3C3lI8();
        }
    }

    /* access modifiers changed from: package-private */
    public void C11ll3() {
        if (this.C1l00I1) {
            if (C01O0C) {
                Log.v("LoaderManager", "Finished Retaining in " + this);
            }
            this.C1l00I1 = false;
            for (int C0I1O3C3lI82 = this.C0I1O3C3lI8.C0I1O3C3lI8() - 1; C0I1O3C3lI82 >= 0; C0I1O3C3lI82--) {
                ((I0l3Oll3) this.C0I1O3C3lI8.C0I1O3C3lI8(C0I1O3C3lI82)).C101lC8O();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void C18Cl1C() {
        for (int C0I1O3C3lI82 = this.C0I1O3C3lI8.C0I1O3C3lI8() - 1; C0I1O3C3lI82 >= 0; C0I1O3C3lI82--) {
            ((I0l3Oll3) this.C0I1O3C3lI8.C0I1O3C3lI8(C0I1O3C3lI82)).C3CIO118 = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void C1O10Cl038() {
        if (!this.C1l00I1) {
            if (C01O0C) {
                Log.v("LoaderManager", "Destroying Active in " + this);
            }
            for (int C0I1O3C3lI82 = this.C0I1O3C3lI8.C0I1O3C3lI8() - 1; C0I1O3C3lI82 >= 0; C0I1O3C3lI82--) {
                ((I0l3Oll3) this.C0I1O3C3lI8.C0I1O3C3lI8(C0I1O3C3lI82)).C18Cl1C();
            }
            this.C0I1O3C3lI8.C101lC8O();
        }
        if (C01O0C) {
            Log.v("LoaderManager", "Destroying Inactive in " + this);
        }
        for (int C0I1O3C3lI83 = this.C101lC8O.C0I1O3C3lI8() - 1; C0I1O3C3lI83 >= 0; C0I1O3C3lI83--) {
            ((I0l3Oll3) this.C101lC8O.C0I1O3C3lI8(C0I1O3C3lI83)).C18Cl1C();
        }
        this.C101lC8O.C101lC8O();
    }

    /* access modifiers changed from: package-private */
    public void C1l00I1() {
        for (int C0I1O3C3lI82 = this.C0I1O3C3lI8.C0I1O3C3lI8() - 1; C0I1O3C3lI82 >= 0; C0I1O3C3lI82--) {
            ((I0l3Oll3) this.C0I1O3C3lI8.C0I1O3C3lI8(C0I1O3C3lI82)).C11013l3();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        C11013l3.C01O0C(this.C11ll3, sb);
        sb.append("}}");
        return sb.toString();
    }
}
