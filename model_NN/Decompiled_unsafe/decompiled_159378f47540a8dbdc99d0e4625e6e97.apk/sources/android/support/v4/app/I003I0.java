package android.support.v4.app;

import android.transition.Transition;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.Map;

final class I003I0 implements ViewTreeObserver.OnPreDrawListener {
    final /* synthetic */ View C01O0C;
    final /* synthetic */ I088l3088 C0I1O3C3lI8;
    final /* synthetic */ Map C101lC8O;
    final /* synthetic */ Map C11013l3;
    final /* synthetic */ Transition C11ll3;
    final /* synthetic */ ArrayList C18Cl1C;
    final /* synthetic */ View C1l00I1;

    I003I0(View view, I088l3088 i088l3088, Map map, Map map2, Transition transition, ArrayList arrayList, View view2) {
        this.C01O0C = view;
        this.C0I1O3C3lI8 = i088l3088;
        this.C101lC8O = map;
        this.C11013l3 = map2;
        this.C11ll3 = transition;
        this.C18Cl1C = arrayList;
        this.C1l00I1 = view2;
    }

    public boolean onPreDraw() {
        this.C01O0C.getViewTreeObserver().removeOnPreDrawListener(this);
        View C01O0C2 = this.C0I1O3C3lI8.C01O0C();
        if (C01O0C2 == null) {
            return true;
        }
        if (!this.C101lC8O.isEmpty()) {
            CO30CC1l0313.C01O0C(this.C11013l3, C01O0C2);
            this.C11013l3.keySet().retainAll(this.C101lC8O.values());
            for (Map.Entry entry : this.C101lC8O.entrySet()) {
                View view = (View) this.C11013l3.get((String) entry.getValue());
                if (view != null) {
                    view.setTransitionName((String) entry.getKey());
                }
            }
        }
        if (this.C11ll3 == null) {
            return true;
        }
        CO30CC1l0313.C0I1O3C3lI8(this.C18Cl1C, C01O0C2);
        this.C18Cl1C.removeAll(this.C11013l3.values());
        this.C18Cl1C.add(this.C1l00I1);
        this.C11ll3.removeTarget(this.C1l00I1);
        CO30CC1l0313.C0I1O3C3lI8(this.C11ll3, this.C18Cl1C);
        return true;
    }
}
