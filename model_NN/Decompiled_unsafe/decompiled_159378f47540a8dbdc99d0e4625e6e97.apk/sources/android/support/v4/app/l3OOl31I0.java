package android.support.v4.app;

import android.os.Bundle;

class l3OOl31I0 {
    static Bundle C01O0C(l3O003I1OOl0 l3o003i1ool0) {
        Bundle bundle = new Bundle();
        bundle.putString("resultKey", l3o003i1ool0.C01O0C());
        bundle.putCharSequence("label", l3o003i1ool0.C0I1O3C3lI8());
        bundle.putCharSequenceArray("choices", l3o003i1ool0.C101lC8O());
        bundle.putBoolean("allowFreeFormInput", l3o003i1ool0.C11013l3());
        bundle.putBundle("extras", l3o003i1ool0.C11ll3());
        return bundle;
    }

    static Bundle[] C01O0C(l3O003I1OOl0[] l3o003i1ool0Arr) {
        if (l3o003i1ool0Arr == null) {
            return null;
        }
        Bundle[] bundleArr = new Bundle[l3o003i1ool0Arr.length];
        for (int i = 0; i < l3o003i1ool0Arr.length; i++) {
            bundleArr[i] = C01O0C(l3o003i1ool0Arr[i]);
        }
        return bundleArr;
    }
}
