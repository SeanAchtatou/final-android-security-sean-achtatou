package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.c.C11013l3;
import android.support.v4.c.C3ICl0OOl;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Fragment implements ComponentCallbacks, View.OnCreateContextMenuListener {
    static final Object C01O0C = new Object();
    private static final C3ICl0OOl II1lC1O = new C3ICl0OOl();
    int C0I1O3C3lI8 = 0;
    View C101lC8O;
    int C11013l3;
    Bundle C11ll3;
    SparseArray C18Cl1C;
    String C1O10Cl038;
    Bundle C1OC33O0lO81;
    int C1l00I1 = -1;
    Fragment C3C1C0I8l3;
    int C3CIO118 = -1;
    int C3ICl0OOl;
    boolean C3l3O8lIOIO8;
    boolean C3llC38O1;
    boolean C831O13C118;
    boolean C8CI00;
    boolean CC38COI1l3I;
    boolean CC8IOI1II0;
    int CCC3CC0l;
    CCC3CC0l CI0I8l333131;
    C3llC38O1 CI3C103l01O;
    CCC3CC0l CII3C813OIC8;
    Fragment CIOC8C;
    boolean CO081lO0OC0;
    boolean CO1830lI8C03;
    boolean CO30CC1l0313;
    boolean CO88CO1Cl383;
    int Cl80C0l838l;
    int ClC13lIl;
    String ClO80C3lOO8;
    boolean I003I0;
    boolean I003OlCCOlC = true;
    boolean I008018O;
    int I03lII1;
    ViewGroup I088l3088;
    View I08O3C;
    View I0CIIIC;
    boolean I0IC1O01888;
    I0IC1O01888 I0OlCO0CI13;
    boolean I0l3Oll3 = true;
    boolean I1CO03;
    boolean I1I11O81II;
    Object I30OCIOO = null;
    Object I3ClO1C31 = C01O0C;
    Object I80183lOl = null;
    Object I801IO8CII = C01O0C;
    Object I8C3388l1301 = null;
    Object IC11OO80I3 = C01O0C;
    Boolean ICI3C3O;
    Boolean ICOI8lC3;
    l83C0OCC3 II083CII = null;
    l83C0OCC3 II0ll1CC13l = null;

    public class SavedState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new C3l3O8lIOIO8();
        final Bundle C01O0C;

        SavedState(Parcel parcel, ClassLoader classLoader) {
            this.C01O0C = parcel.readBundle();
            if (classLoader != null && this.C01O0C != null) {
                this.C01O0C.setClassLoader(classLoader);
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeBundle(this.C01O0C);
        }
    }

    public static Fragment C01O0C(Context context, String str) {
        return C01O0C(context, str, (Bundle) null);
    }

    public static Fragment C01O0C(Context context, String str, Bundle bundle) {
        try {
            Class<?> cls = (Class) II1lC1O.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                II1lC1O.put(str, cls);
            }
            Fragment fragment = (Fragment) cls.newInstance();
            if (bundle != null) {
                bundle.setClassLoader(fragment.getClass().getClassLoader());
                fragment.C1OC33O0lO81 = bundle;
            }
            return fragment;
        } catch (ClassNotFoundException e) {
            throw new C3ICl0OOl("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e);
        } catch (InstantiationException e2) {
            throw new C3ICl0OOl("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e2);
        } catch (IllegalAccessException e3) {
            throw new C3ICl0OOl("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e3);
        }
    }

    static boolean C0I1O3C3lI8(Context context, String str) {
        try {
            Class<?> cls = (Class) II1lC1O.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                II1lC1O.put(str, cls);
            }
            return Fragment.class.isAssignableFrom(cls);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public View C01O0C(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return null;
    }

    public Animation C01O0C(int i, boolean z, int i2) {
        return null;
    }

    public void C01O0C(int i, int i2, Intent intent) {
    }

    /* access modifiers changed from: package-private */
    public final void C01O0C(int i, Fragment fragment) {
        this.C1l00I1 = i;
        if (fragment != null) {
            this.C1O10Cl038 = fragment.C1O10Cl038 + ":" + this.C1l00I1;
        } else {
            this.C1O10Cl038 = "android:fragment:" + this.C1l00I1;
        }
    }

    public void C01O0C(Activity activity) {
        this.I008018O = true;
    }

    public void C01O0C(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        this.I008018O = true;
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(Configuration configuration) {
        onConfigurationChanged(configuration);
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C01O0C(configuration);
        }
    }

    /* access modifiers changed from: package-private */
    public final void C01O0C(Bundle bundle) {
        if (this.C18Cl1C != null) {
            this.I0CIIIC.restoreHierarchyState(this.C18Cl1C);
            this.C18Cl1C = null;
        }
        this.I008018O = false;
        C11ll3(bundle);
        if (!this.I008018O) {
            throw new l888l8lCIl0I("Fragment " + this + " did not call through to super.onViewStateRestored()");
        }
    }

    public void C01O0C(Menu menu) {
    }

    public void C01O0C(Menu menu, MenuInflater menuInflater) {
    }

    public void C01O0C(View view, Bundle bundle) {
    }

    public void C01O0C(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mFragmentId=#");
        printWriter.print(Integer.toHexString(this.Cl80C0l838l));
        printWriter.print(" mContainerId=#");
        printWriter.print(Integer.toHexString(this.ClC13lIl));
        printWriter.print(" mTag=");
        printWriter.println(this.ClO80C3lOO8);
        printWriter.print(str);
        printWriter.print("mState=");
        printWriter.print(this.C0I1O3C3lI8);
        printWriter.print(" mIndex=");
        printWriter.print(this.C1l00I1);
        printWriter.print(" mWho=");
        printWriter.print(this.C1O10Cl038);
        printWriter.print(" mBackStackNesting=");
        printWriter.println(this.CCC3CC0l);
        printWriter.print(str);
        printWriter.print("mAdded=");
        printWriter.print(this.C3l3O8lIOIO8);
        printWriter.print(" mRemoving=");
        printWriter.print(this.C3llC38O1);
        printWriter.print(" mResumed=");
        printWriter.print(this.C831O13C118);
        printWriter.print(" mFromLayout=");
        printWriter.print(this.C8CI00);
        printWriter.print(" mInLayout=");
        printWriter.println(this.CC38COI1l3I);
        printWriter.print(str);
        printWriter.print("mHidden=");
        printWriter.print(this.CO081lO0OC0);
        printWriter.print(" mDetached=");
        printWriter.print(this.CO1830lI8C03);
        printWriter.print(" mMenuVisible=");
        printWriter.print(this.I003OlCCOlC);
        printWriter.print(" mHasMenu=");
        printWriter.println(this.I003I0);
        printWriter.print(str);
        printWriter.print("mRetainInstance=");
        printWriter.print(this.CO30CC1l0313);
        printWriter.print(" mRetaining=");
        printWriter.print(this.CO88CO1Cl383);
        printWriter.print(" mUserVisibleHint=");
        printWriter.println(this.I0l3Oll3);
        if (this.CI0I8l333131 != null) {
            printWriter.print(str);
            printWriter.print("mFragmentManager=");
            printWriter.println(this.CI0I8l333131);
        }
        if (this.CI3C103l01O != null) {
            printWriter.print(str);
            printWriter.print("mActivity=");
            printWriter.println(this.CI3C103l01O);
        }
        if (this.CIOC8C != null) {
            printWriter.print(str);
            printWriter.print("mParentFragment=");
            printWriter.println(this.CIOC8C);
        }
        if (this.C1OC33O0lO81 != null) {
            printWriter.print(str);
            printWriter.print("mArguments=");
            printWriter.println(this.C1OC33O0lO81);
        }
        if (this.C11ll3 != null) {
            printWriter.print(str);
            printWriter.print("mSavedFragmentState=");
            printWriter.println(this.C11ll3);
        }
        if (this.C18Cl1C != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewState=");
            printWriter.println(this.C18Cl1C);
        }
        if (this.C3C1C0I8l3 != null) {
            printWriter.print(str);
            printWriter.print("mTarget=");
            printWriter.print(this.C3C1C0I8l3);
            printWriter.print(" mTargetRequestCode=");
            printWriter.println(this.C3ICl0OOl);
        }
        if (this.I03lII1 != 0) {
            printWriter.print(str);
            printWriter.print("mNextAnim=");
            printWriter.println(this.I03lII1);
        }
        if (this.I088l3088 != null) {
            printWriter.print(str);
            printWriter.print("mContainer=");
            printWriter.println(this.I088l3088);
        }
        if (this.I08O3C != null) {
            printWriter.print(str);
            printWriter.print("mView=");
            printWriter.println(this.I08O3C);
        }
        if (this.I0CIIIC != null) {
            printWriter.print(str);
            printWriter.print("mInnerView=");
            printWriter.println(this.I08O3C);
        }
        if (this.C101lC8O != null) {
            printWriter.print(str);
            printWriter.print("mAnimatingAway=");
            printWriter.println(this.C101lC8O);
            printWriter.print(str);
            printWriter.print("mStateAfterAnimating=");
            printWriter.println(this.C11013l3);
        }
        if (this.I0OlCO0CI13 != null) {
            printWriter.print(str);
            printWriter.println("Loader Manager:");
            this.I0OlCO0CI13.C01O0C(str + "  ", fileDescriptor, printWriter, strArr);
        }
        if (this.CII3C813OIC8 != null) {
            printWriter.print(str);
            printWriter.println("Child " + this.CII3C813OIC8 + ":");
            this.CII3C813OIC8.C01O0C(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }

    public void C01O0C(boolean z) {
    }

    /* access modifiers changed from: package-private */
    public final boolean C01O0C() {
        return this.CCC3CC0l > 0;
    }

    public boolean C01O0C(MenuItem menuItem) {
        return false;
    }

    public final C3llC38O1 C0I1O3C3lI8() {
        return this.CI3C103l01O;
    }

    public LayoutInflater C0I1O3C3lI8(Bundle bundle) {
        LayoutInflater cloneInContext = this.CI3C103l01O.getLayoutInflater().cloneInContext(this.CI3C103l01O);
        C11013l3();
        cloneInContext.setFactory(this.CII3C813OIC8.CI0I8l333131());
        return cloneInContext;
    }

    /* access modifiers changed from: package-private */
    public View C0I1O3C3lI8(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C1OC33O0lO81();
        }
        return C01O0C(layoutInflater, viewGroup, bundle);
    }

    public void C0I1O3C3lI8(Menu menu) {
    }

    /* access modifiers changed from: package-private */
    public boolean C0I1O3C3lI8(Menu menu, MenuInflater menuInflater) {
        boolean z = false;
        if (this.CO081lO0OC0) {
            return false;
        }
        if (this.I003I0 && this.I003OlCCOlC) {
            z = true;
            C01O0C(menu, menuInflater);
        }
        return this.CII3C813OIC8 != null ? z | this.CII3C813OIC8.C01O0C(menu, menuInflater) : z;
    }

    public boolean C0I1O3C3lI8(MenuItem menuItem) {
        return false;
    }

    public final Resources C101lC8O() {
        if (this.CI3C103l01O != null) {
            return this.CI3C103l01O.getResources();
        }
        throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }

    public void C101lC8O(Bundle bundle) {
        this.I008018O = true;
    }

    /* access modifiers changed from: package-private */
    public boolean C101lC8O(Menu menu) {
        boolean z = false;
        if (this.CO081lO0OC0) {
            return false;
        }
        if (this.I003I0 && this.I003OlCCOlC) {
            z = true;
            C01O0C(menu);
        }
        return this.CII3C813OIC8 != null ? z | this.CII3C813OIC8.C01O0C(menu) : z;
    }

    /* access modifiers changed from: package-private */
    public boolean C101lC8O(MenuItem menuItem) {
        if (!this.CO081lO0OC0) {
            if (!this.I003I0 || !this.I003OlCCOlC || !C01O0C(menuItem)) {
                return this.CII3C813OIC8 != null && this.CII3C813OIC8.C01O0C(menuItem);
            }
            return true;
        }
    }

    public final CC38COI1l3I C11013l3() {
        if (this.CII3C813OIC8 == null) {
            ClO80C3lOO8();
            if (this.C0I1O3C3lI8 >= 5) {
                this.CII3C813OIC8.C3l3O8lIOIO8();
            } else if (this.C0I1O3C3lI8 >= 4) {
                this.CII3C813OIC8.C3ICl0OOl();
            } else if (this.C0I1O3C3lI8 >= 2) {
                this.CII3C813OIC8.C3CIO118();
            } else if (this.C0I1O3C3lI8 >= 1) {
                this.CII3C813OIC8.C3C1C0I8l3();
            }
        }
        return this.CII3C813OIC8;
    }

    public void C11013l3(Bundle bundle) {
        this.I008018O = true;
    }

    /* access modifiers changed from: package-private */
    public void C11013l3(Menu menu) {
        if (!this.CO081lO0OC0) {
            if (this.I003I0 && this.I003OlCCOlC) {
                C0I1O3C3lI8(menu);
            }
            if (this.CII3C813OIC8 != null) {
                this.CII3C813OIC8.C0I1O3C3lI8(menu);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean C11013l3(MenuItem menuItem) {
        if (!this.CO081lO0OC0) {
            if (C0I1O3C3lI8(menuItem)) {
                return true;
            }
            return this.CII3C813OIC8 != null && this.CII3C813OIC8.C0I1O3C3lI8(menuItem);
        }
    }

    public void C11ll3(Bundle bundle) {
        this.I008018O = true;
    }

    public final boolean C11ll3() {
        return this.CI3C103l01O != null && this.C3l3O8lIOIO8;
    }

    public void C18Cl1C(Bundle bundle) {
    }

    public final boolean C18Cl1C() {
        return this.CO1830lI8C03;
    }

    public View C1O10Cl038() {
        return this.I08O3C;
    }

    /* access modifiers changed from: package-private */
    public void C1O10Cl038(Bundle bundle) {
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C1OC33O0lO81();
        }
        this.I008018O = false;
        C11013l3(bundle);
        if (!this.I008018O) {
            throw new l888l8lCIl0I("Fragment " + this + " did not call through to super.onActivityCreated()");
        } else if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C3CIO118();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, boolean, boolean):android.support.v4.app.I0IC1O01888
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, boolean, boolean):android.support.v4.app.I0IC1O01888 */
    public void C1OC33O0lO81() {
        this.I008018O = true;
        if (!this.I1CO03) {
            this.I1CO03 = true;
            if (!this.I1I11O81II) {
                this.I1I11O81II = true;
                this.I0OlCO0CI13 = this.CI3C103l01O.C01O0C(this.C1O10Cl038, this.I1CO03, false);
            }
            if (this.I0OlCO0CI13 != null) {
                this.I0OlCO0CI13.C0I1O3C3lI8();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void C1OC33O0lO81(Bundle bundle) {
        Parcelable C1O10Cl0382;
        C18Cl1C(bundle);
        if (this.CII3C813OIC8 != null && (C1O10Cl0382 = this.CII3C813OIC8.C1O10Cl038()) != null) {
            bundle.putParcelable("android:support:fragments", C1O10Cl0382);
        }
    }

    /* access modifiers changed from: package-private */
    public void C1l00I1(Bundle bundle) {
        Parcelable parcelable;
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C1OC33O0lO81();
        }
        this.I008018O = false;
        C101lC8O(bundle);
        if (!this.I008018O) {
            throw new l888l8lCIl0I("Fragment " + this + " did not call through to super.onCreate()");
        } else if (bundle != null && (parcelable = bundle.getParcelable("android:support:fragments")) != null) {
            if (this.CII3C813OIC8 == null) {
                ClO80C3lOO8();
            }
            this.CII3C813OIC8.C01O0C(parcelable, (ArrayList) null);
            this.CII3C813OIC8.C3C1C0I8l3();
        }
    }

    public final boolean C1l00I1() {
        return this.CO081lO0OC0;
    }

    public void C3C1C0I8l3() {
        this.I008018O = true;
    }

    public void C3CIO118() {
        this.I008018O = true;
    }

    public void C3ICl0OOl() {
        this.I008018O = true;
    }

    public void C3l3O8lIOIO8() {
        this.I008018O = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, boolean, boolean):android.support.v4.app.I0IC1O01888
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, boolean, boolean):android.support.v4.app.I0IC1O01888 */
    public void C3llC38O1() {
        this.I008018O = true;
        if (!this.I1I11O81II) {
            this.I1I11O81II = true;
            this.I0OlCO0CI13 = this.CI3C103l01O.C01O0C(this.C1O10Cl038, this.I1CO03, false);
        }
        if (this.I0OlCO0CI13 != null) {
            this.I0OlCO0CI13.C1O10Cl038();
        }
    }

    /* access modifiers changed from: package-private */
    public void C831O13C118() {
        this.C1l00I1 = -1;
        this.C1O10Cl038 = null;
        this.C3l3O8lIOIO8 = false;
        this.C3llC38O1 = false;
        this.C831O13C118 = false;
        this.C8CI00 = false;
        this.CC38COI1l3I = false;
        this.CC8IOI1II0 = false;
        this.CCC3CC0l = 0;
        this.CI0I8l333131 = null;
        this.CII3C813OIC8 = null;
        this.CI3C103l01O = null;
        this.Cl80C0l838l = 0;
        this.ClC13lIl = 0;
        this.ClO80C3lOO8 = null;
        this.CO081lO0OC0 = false;
        this.CO1830lI8C03 = false;
        this.CO88CO1Cl383 = false;
        this.I0OlCO0CI13 = null;
        this.I1CO03 = false;
        this.I1I11O81II = false;
    }

    public void C8CI00() {
        this.I008018O = true;
    }

    public void CC38COI1l3I() {
    }

    public Object CC8IOI1II0() {
        return this.I30OCIOO;
    }

    public Object CCC3CC0l() {
        return this.I3ClO1C31 == C01O0C ? CC8IOI1II0() : this.I3ClO1C31;
    }

    public Object CI0I8l333131() {
        return this.I80183lOl;
    }

    public Object CI3C103l01O() {
        return this.I801IO8CII == C01O0C ? CI0I8l333131() : this.I801IO8CII;
    }

    public Object CII3C813OIC8() {
        return this.I8C3388l1301;
    }

    public Object CIOC8C() {
        return this.IC11OO80I3 == C01O0C ? CII3C813OIC8() : this.IC11OO80I3;
    }

    /* access modifiers changed from: package-private */
    public void CO081lO0OC0() {
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C1OC33O0lO81();
            this.CII3C813OIC8.C11ll3();
        }
        this.I008018O = false;
        C1OC33O0lO81();
        if (!this.I008018O) {
            throw new l888l8lCIl0I("Fragment " + this + " did not call through to super.onStart()");
        }
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C3ICl0OOl();
        }
        if (this.I0OlCO0CI13 != null) {
            this.I0OlCO0CI13.C1l00I1();
        }
    }

    /* access modifiers changed from: package-private */
    public void CO1830lI8C03() {
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C1OC33O0lO81();
            this.CII3C813OIC8.C11ll3();
        }
        this.I008018O = false;
        C3C1C0I8l3();
        if (!this.I008018O) {
            throw new l888l8lCIl0I("Fragment " + this + " did not call through to super.onResume()");
        } else if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C3l3O8lIOIO8();
            this.CII3C813OIC8.C11ll3();
        }
    }

    /* access modifiers changed from: package-private */
    public void CO30CC1l0313() {
        onLowMemory();
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.CCC3CC0l();
        }
    }

    /* access modifiers changed from: package-private */
    public void CO88CO1Cl383() {
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C3llC38O1();
        }
        this.I008018O = false;
        C3CIO118();
        if (!this.I008018O) {
            throw new l888l8lCIl0I("Fragment " + this + " did not call through to super.onPause()");
        }
    }

    public boolean Cl80C0l838l() {
        if (this.ICOI8lC3 == null) {
            return true;
        }
        return this.ICOI8lC3.booleanValue();
    }

    public boolean ClC13lIl() {
        if (this.ICI3C3O == null) {
            return true;
        }
        return this.ICI3C3O.booleanValue();
    }

    /* access modifiers changed from: package-private */
    public void ClO80C3lOO8() {
        this.CII3C813OIC8 = new CCC3CC0l();
        this.CII3C813OIC8.C01O0C(this.CI3C103l01O, new C3CIO118(this), this);
    }

    /* access modifiers changed from: package-private */
    public void I003I0() {
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C831O13C118();
        }
        this.I008018O = false;
        C3ICl0OOl();
        if (!this.I008018O) {
            throw new l888l8lCIl0I("Fragment " + this + " did not call through to super.onStop()");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, boolean, boolean):android.support.v4.app.I0IC1O01888
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, boolean, boolean):android.support.v4.app.I0IC1O01888 */
    /* access modifiers changed from: package-private */
    public void I003OlCCOlC() {
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.C8CI00();
        }
        if (this.I1CO03) {
            this.I1CO03 = false;
            if (!this.I1I11O81II) {
                this.I1I11O81II = true;
                this.I0OlCO0CI13 = this.CI3C103l01O.C01O0C(this.C1O10Cl038, this.I1CO03, false);
            }
            if (this.I0OlCO0CI13 == null) {
                return;
            }
            if (!this.CI3C103l01O.C1O10Cl038) {
                this.I0OlCO0CI13.C101lC8O();
            } else {
                this.I0OlCO0CI13.C11013l3();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void I008018O() {
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.CC38COI1l3I();
        }
        this.I008018O = false;
        C3l3O8lIOIO8();
        if (!this.I008018O) {
            throw new l888l8lCIl0I("Fragment " + this + " did not call through to super.onDestroyView()");
        } else if (this.I0OlCO0CI13 != null) {
            this.I0OlCO0CI13.C18Cl1C();
        }
    }

    /* access modifiers changed from: package-private */
    public void I03lII1() {
        if (this.CII3C813OIC8 != null) {
            this.CII3C813OIC8.CC8IOI1II0();
        }
        this.I008018O = false;
        C3llC38O1();
        if (!this.I008018O) {
            throw new l888l8lCIl0I("Fragment " + this + " did not call through to super.onDestroy()");
        }
    }

    public final boolean equals(Object obj) {
        return super.equals(obj);
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.I008018O = true;
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        C0I1O3C3lI8().onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    public void onLowMemory() {
        this.I008018O = true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        C11013l3.C01O0C(this, sb);
        if (this.C1l00I1 >= 0) {
            sb.append(" #");
            sb.append(this.C1l00I1);
        }
        if (this.Cl80C0l838l != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.Cl80C0l838l));
        }
        if (this.ClO80C3lOO8 != null) {
            sb.append(" ");
            sb.append(this.ClO80C3lOO8);
        }
        sb.append('}');
        return sb.toString();
    }
}
