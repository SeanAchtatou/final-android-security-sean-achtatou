package android.support.v4.app;

import android.transition.Transition;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.Map;

final class I008018O implements ViewTreeObserver.OnPreDrawListener {
    final /* synthetic */ View C01O0C;
    final /* synthetic */ Transition C0I1O3C3lI8;
    final /* synthetic */ View C101lC8O;
    final /* synthetic */ ArrayList C11013l3;
    final /* synthetic */ Transition C11ll3;
    final /* synthetic */ ArrayList C18Cl1C;
    final /* synthetic */ ArrayList C1O10Cl038;
    final /* synthetic */ Map C1OC33O0lO81;
    final /* synthetic */ Transition C1l00I1;
    final /* synthetic */ ArrayList C3C1C0I8l3;
    final /* synthetic */ Transition C3CIO118;

    I008018O(View view, Transition transition, View view2, ArrayList arrayList, Transition transition2, ArrayList arrayList2, Transition transition3, ArrayList arrayList3, Map map, ArrayList arrayList4, Transition transition4) {
        this.C01O0C = view;
        this.C0I1O3C3lI8 = transition;
        this.C101lC8O = view2;
        this.C11013l3 = arrayList;
        this.C11ll3 = transition2;
        this.C18Cl1C = arrayList2;
        this.C1l00I1 = transition3;
        this.C1O10Cl038 = arrayList3;
        this.C1OC33O0lO81 = map;
        this.C3C1C0I8l3 = arrayList4;
        this.C3CIO118 = transition4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.transition.Transition.excludeTarget(android.view.View, boolean):android.transition.Transition}
     arg types: [android.view.View, int]
     candidates:
      ClspMth{android.transition.Transition.excludeTarget(int, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(java.lang.Class, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(java.lang.String, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(android.view.View, boolean):android.transition.Transition} */
    public boolean onPreDraw() {
        this.C01O0C.getViewTreeObserver().removeOnPreDrawListener(this);
        if (this.C0I1O3C3lI8 != null) {
            this.C0I1O3C3lI8.removeTarget(this.C101lC8O);
            CO30CC1l0313.C01O0C(this.C0I1O3C3lI8, this.C11013l3);
        }
        if (this.C11ll3 != null) {
            CO30CC1l0313.C01O0C(this.C11ll3, this.C18Cl1C);
        }
        if (this.C1l00I1 != null) {
            CO30CC1l0313.C01O0C(this.C1l00I1, this.C1O10Cl038);
        }
        for (Map.Entry entry : this.C1OC33O0lO81.entrySet()) {
            ((View) entry.getValue()).setTransitionName((String) entry.getKey());
        }
        int size = this.C3C1C0I8l3.size();
        for (int i = 0; i < size; i++) {
            this.C3CIO118.excludeTarget((View) this.C3C1C0I8l3.get(i), false);
        }
        this.C3CIO118.excludeTarget(this.C101lC8O, false);
        return true;
    }
}
