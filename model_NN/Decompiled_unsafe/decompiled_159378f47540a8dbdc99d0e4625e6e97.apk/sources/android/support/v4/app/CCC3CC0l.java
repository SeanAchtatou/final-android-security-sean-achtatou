package android.support.v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.c.C11013l3;
import android.support.v4.c.C11ll3;
import android.support.v4.view.CO88CO1Cl383;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

final class CCC3CC0l extends CC38COI1l3I implements LayoutInflater.Factory {
    static boolean C01O0C = false;
    static final boolean C0I1O3C3lI8;
    static final Interpolator CO081lO0OC0 = new DecelerateInterpolator(1.5f);
    static final Interpolator CO1830lI8C03 = new AccelerateInterpolator(2.5f);
    static final Interpolator CO30CC1l0313 = new AccelerateInterpolator(1.5f);
    static final Interpolator ClO80C3lOO8 = new DecelerateInterpolator(2.5f);
    ArrayList C101lC8O;
    Runnable[] C11013l3;
    boolean C11ll3;
    ArrayList C18Cl1C;
    ArrayList C1O10Cl038;
    ArrayList C1OC33O0lO81;
    ArrayList C1l00I1;
    ArrayList C3C1C0I8l3;
    ArrayList C3CIO118;
    ArrayList C3ICl0OOl;
    ArrayList C3l3O8lIOIO8;
    int C3llC38O1 = 0;
    C3llC38O1 C831O13C118;
    C8CI00 C8CI00;
    Fragment CC38COI1l3I;
    boolean CC8IOI1II0;
    boolean CCC3CC0l;
    boolean CI0I8l333131;
    String CI3C103l01O;
    boolean CII3C813OIC8;
    Bundle CIOC8C = null;
    SparseArray Cl80C0l838l = null;
    Runnable ClC13lIl = new CI0I8l333131(this);

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 11) {
            z = true;
        }
        C0I1O3C3lI8 = z;
    }

    CCC3CC0l() {
    }

    static Animation C01O0C(Context context, float f, float f2) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f, f2);
        alphaAnimation.setInterpolator(CO081lO0OC0);
        alphaAnimation.setDuration(220);
        return alphaAnimation;
    }

    static Animation C01O0C(Context context, float f, float f2, float f3, float f4) {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f, f2, f, f2, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(ClO80C3lOO8);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f3, f4);
        alphaAnimation.setInterpolator(CO081lO0OC0);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    private void C01O0C(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new C11ll3("FragmentManager"));
        if (this.C831O13C118 != null) {
            try {
                this.C831O13C118.dump("  ", null, printWriter, new String[0]);
            } catch (Exception e) {
                Log.e("FragmentManager", "Failed dumping state", e);
            }
        } else {
            try {
                C01O0C("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        }
        throw runtimeException;
    }

    public static int C0I1O3C3lI8(int i, boolean z) {
        switch (i) {
            case 4097:
                return z ? 1 : 2;
            case 4099:
                return z ? 5 : 6;
            case 8194:
                return z ? 3 : 4;
            default:
                return -1;
        }
    }

    public static int C101lC8O(int i) {
        switch (i) {
            case 4097:
                return 8194;
            case 4099:
                return 4099;
            case 8194:
                return 4097;
            default:
                return 0;
        }
    }

    private void CI3C103l01O() {
        if (this.CCC3CC0l) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        } else if (this.CI3C103l01O != null) {
            throw new IllegalStateException("Can not perform this action inside of " + this.CI3C103l01O);
        }
    }

    public int C01O0C(C11013l3 c11013l3) {
        int i;
        synchronized (this) {
            if (this.C3ICl0OOl == null || this.C3ICl0OOl.size() <= 0) {
                if (this.C3CIO118 == null) {
                    this.C3CIO118 = new ArrayList();
                }
                i = this.C3CIO118.size();
                if (C01O0C) {
                    Log.v("FragmentManager", "Setting back stack index " + i + " to " + c11013l3);
                }
                this.C3CIO118.add(c11013l3);
            } else {
                i = ((Integer) this.C3ICl0OOl.remove(this.C3ICl0OOl.size() - 1)).intValue();
                if (C01O0C) {
                    Log.v("FragmentManager", "Adding back stack index " + i + " with " + c11013l3);
                }
                this.C3CIO118.set(i, c11013l3);
            }
        }
        return i;
    }

    public CO1830lI8C03 C01O0C() {
        return new C11013l3(this);
    }

    public Fragment C01O0C(int i) {
        if (this.C1l00I1 != null) {
            for (int size = this.C1l00I1.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.C1l00I1.get(size);
                if (fragment != null && fragment.Cl80C0l838l == i) {
                    return fragment;
                }
            }
        }
        if (this.C18Cl1C != null) {
            for (int size2 = this.C18Cl1C.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = (Fragment) this.C18Cl1C.get(size2);
                if (fragment2 != null && fragment2.Cl80C0l838l == i) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public Fragment C01O0C(Bundle bundle, String str) {
        int i = bundle.getInt(str, -1);
        if (i == -1) {
            return null;
        }
        if (i >= this.C18Cl1C.size()) {
            C01O0C(new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i));
        }
        Fragment fragment = (Fragment) this.C18Cl1C.get(i);
        if (fragment != null) {
            return fragment;
        }
        C01O0C(new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i));
        return fragment;
    }

    public Fragment C01O0C(String str) {
        if (!(this.C1l00I1 == null || str == null)) {
            for (int size = this.C1l00I1.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.C1l00I1.get(size);
                if (fragment != null && str.equals(fragment.ClO80C3lOO8)) {
                    return fragment;
                }
            }
        }
        if (!(this.C18Cl1C == null || str == null)) {
            for (int size2 = this.C18Cl1C.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = (Fragment) this.C18Cl1C.get(size2);
                if (fragment2 != null && str.equals(fragment2.ClO80C3lOO8)) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public Animation C01O0C(Fragment fragment, int i, boolean z, int i2) {
        Animation loadAnimation;
        Animation C01O0C2 = fragment.C01O0C(i, z, fragment.I03lII1);
        if (C01O0C2 != null) {
            return C01O0C2;
        }
        if (fragment.I03lII1 != 0 && (loadAnimation = AnimationUtils.loadAnimation(this.C831O13C118, fragment.I03lII1)) != null) {
            return loadAnimation;
        }
        if (i == 0) {
            return null;
        }
        int C0I1O3C3lI82 = C0I1O3C3lI8(i, z);
        if (C0I1O3C3lI82 < 0) {
            return null;
        }
        switch (C0I1O3C3lI82) {
            case 1:
                return C01O0C(this.C831O13C118, 1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                return C01O0C(this.C831O13C118, 1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                return C01O0C(this.C831O13C118, 0.975f, 1.0f, 0.0f, 1.0f);
            case 4:
                return C01O0C(this.C831O13C118, 1.0f, 1.075f, 1.0f, 0.0f);
            case 5:
                return C01O0C(this.C831O13C118, 0.0f, 1.0f);
            case 6:
                return C01O0C(this.C831O13C118, 1.0f, 0.0f);
            default:
                if (i2 == 0 && this.C831O13C118.getWindow() != null) {
                    i2 = this.C831O13C118.getWindow().getAttributes().windowAnimations;
                }
                return i2 == 0 ? null : null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public void C01O0C(int i, int i2, int i3, boolean z) {
        if (this.C831O13C118 == null && i != 0) {
            throw new IllegalStateException("No activity");
        } else if (z || this.C3llC38O1 != i) {
            this.C3llC38O1 = i;
            if (this.C18Cl1C != null) {
                int i4 = 0;
                boolean z2 = false;
                while (i4 < this.C18Cl1C.size()) {
                    Fragment fragment = (Fragment) this.C18Cl1C.get(i4);
                    if (fragment != null) {
                        C01O0C(fragment, i, i2, i3, false);
                        if (fragment.I0OlCO0CI13 != null) {
                            z2 |= fragment.I0OlCO0CI13.C01O0C();
                        }
                    }
                    i4++;
                    z2 = z2;
                }
                if (!z2) {
                    C11013l3();
                }
                if (this.CC8IOI1II0 && this.C831O13C118 != null && this.C3llC38O1 == 5) {
                    this.C831O13C118.C11013l3();
                    this.CC8IOI1II0 = false;
                }
            }
        }
    }

    public void C01O0C(int i, C11013l3 c11013l3) {
        synchronized (this) {
            if (this.C3CIO118 == null) {
                this.C3CIO118 = new ArrayList();
            }
            int size = this.C3CIO118.size();
            if (i < size) {
                if (C01O0C) {
                    Log.v("FragmentManager", "Setting back stack index " + i + " to " + c11013l3);
                }
                this.C3CIO118.set(i, c11013l3);
            } else {
                while (size < i) {
                    this.C3CIO118.add(null);
                    if (this.C3ICl0OOl == null) {
                        this.C3ICl0OOl = new ArrayList();
                    }
                    if (C01O0C) {
                        Log.v("FragmentManager", "Adding available back stack index " + size);
                    }
                    this.C3ICl0OOl.add(Integer.valueOf(size));
                    size++;
                }
                if (C01O0C) {
                    Log.v("FragmentManager", "Adding back stack index " + i + " with " + c11013l3);
                }
                this.C3CIO118.add(c11013l3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(int i, boolean z) {
        C01O0C(i, 0, 0, z);
    }

    public void C01O0C(Configuration configuration) {
        if (this.C1l00I1 != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.C1l00I1.size()) {
                    Fragment fragment = (Fragment) this.C1l00I1.get(i2);
                    if (fragment != null) {
                        fragment.C01O0C(configuration);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void C01O0C(Bundle bundle, String str, Fragment fragment) {
        if (fragment.C1l00I1 < 0) {
            C01O0C(new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        bundle.putInt(str, fragment.C1l00I1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C11013l3.C01O0C(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.app.C11013l3.C01O0C(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.C1OC33O0lO81
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.app.Fragment, boolean):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, boolean, android.support.v4.app.Fragment):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(java.util.ArrayList, java.util.ArrayList, android.support.v4.c.C01O0C):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C11013l3, android.support.v4.c.C01O0C, android.support.v4.app.C1OC33O0lO81):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, int, java.lang.Object):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.c.C01O0C, java.lang.String, java.lang.String):void
      android.support.v4.app.C11013l3.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.CO1830lI8C03.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.C11013l3.C01O0C(java.lang.String, java.io.PrintWriter, boolean):void */
    /* access modifiers changed from: package-private */
    public void C01O0C(Parcelable parcelable, ArrayList arrayList) {
        if (parcelable != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
            if (fragmentManagerState.C01O0C != null) {
                if (arrayList != null) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        Fragment fragment = (Fragment) arrayList.get(i);
                        if (C01O0C) {
                            Log.v("FragmentManager", "restoreAllState: re-attaching retained " + fragment);
                        }
                        FragmentState fragmentState = fragmentManagerState.C01O0C[fragment.C1l00I1];
                        fragmentState.C3CIO118 = fragment;
                        fragment.C18Cl1C = null;
                        fragment.CCC3CC0l = 0;
                        fragment.CC38COI1l3I = false;
                        fragment.C3l3O8lIOIO8 = false;
                        fragment.C3C1C0I8l3 = null;
                        if (fragmentState.C3C1C0I8l3 != null) {
                            fragmentState.C3C1C0I8l3.setClassLoader(this.C831O13C118.getClassLoader());
                            fragment.C18Cl1C = fragmentState.C3C1C0I8l3.getSparseParcelableArray("android:view_state");
                            fragment.C11ll3 = fragmentState.C3C1C0I8l3;
                        }
                    }
                }
                this.C18Cl1C = new ArrayList(fragmentManagerState.C01O0C.length);
                if (this.C1O10Cl038 != null) {
                    this.C1O10Cl038.clear();
                }
                for (int i2 = 0; i2 < fragmentManagerState.C01O0C.length; i2++) {
                    FragmentState fragmentState2 = fragmentManagerState.C01O0C[i2];
                    if (fragmentState2 != null) {
                        Fragment C01O0C2 = fragmentState2.C01O0C(this.C831O13C118, this.CC38COI1l3I);
                        if (C01O0C) {
                            Log.v("FragmentManager", "restoreAllState: active #" + i2 + ": " + C01O0C2);
                        }
                        this.C18Cl1C.add(C01O0C2);
                        fragmentState2.C3CIO118 = null;
                    } else {
                        this.C18Cl1C.add(null);
                        if (this.C1O10Cl038 == null) {
                            this.C1O10Cl038 = new ArrayList();
                        }
                        if (C01O0C) {
                            Log.v("FragmentManager", "restoreAllState: avail #" + i2);
                        }
                        this.C1O10Cl038.add(Integer.valueOf(i2));
                    }
                }
                if (arrayList != null) {
                    for (int i3 = 0; i3 < arrayList.size(); i3++) {
                        Fragment fragment2 = (Fragment) arrayList.get(i3);
                        if (fragment2.C3CIO118 >= 0) {
                            if (fragment2.C3CIO118 < this.C18Cl1C.size()) {
                                fragment2.C3C1C0I8l3 = (Fragment) this.C18Cl1C.get(fragment2.C3CIO118);
                            } else {
                                Log.w("FragmentManager", "Re-attaching retained fragment " + fragment2 + " target no longer exists: " + fragment2.C3CIO118);
                                fragment2.C3C1C0I8l3 = null;
                            }
                        }
                    }
                }
                if (fragmentManagerState.C0I1O3C3lI8 != null) {
                    this.C1l00I1 = new ArrayList(fragmentManagerState.C0I1O3C3lI8.length);
                    for (int i4 = 0; i4 < fragmentManagerState.C0I1O3C3lI8.length; i4++) {
                        Fragment fragment3 = (Fragment) this.C18Cl1C.get(fragmentManagerState.C0I1O3C3lI8[i4]);
                        if (fragment3 == null) {
                            C01O0C(new IllegalStateException("No instantiated fragment for index #" + fragmentManagerState.C0I1O3C3lI8[i4]));
                        }
                        fragment3.C3l3O8lIOIO8 = true;
                        if (C01O0C) {
                            Log.v("FragmentManager", "restoreAllState: added #" + i4 + ": " + fragment3);
                        }
                        if (this.C1l00I1.contains(fragment3)) {
                            throw new IllegalStateException("Already added!");
                        }
                        this.C1l00I1.add(fragment3);
                    }
                } else {
                    this.C1l00I1 = null;
                }
                if (fragmentManagerState.C101lC8O != null) {
                    this.C1OC33O0lO81 = new ArrayList(fragmentManagerState.C101lC8O.length);
                    for (int i5 = 0; i5 < fragmentManagerState.C101lC8O.length; i5++) {
                        C11013l3 C01O0C3 = fragmentManagerState.C101lC8O[i5].C01O0C(this);
                        if (C01O0C) {
                            Log.v("FragmentManager", "restoreAllState: back stack #" + i5 + " (index " + C01O0C3.C831O13C118 + "): " + C01O0C3);
                            C01O0C3.C01O0C("  ", new PrintWriter(new C11ll3("FragmentManager")), false);
                        }
                        this.C1OC33O0lO81.add(C01O0C3);
                        if (C01O0C3.C831O13C118 >= 0) {
                            C01O0C(C01O0C3.C831O13C118, C01O0C3);
                        }
                    }
                    return;
                }
                this.C1OC33O0lO81 = null;
            }
        }
    }

    public void C01O0C(C3llC38O1 c3llC38O1, C8CI00 c8ci00, Fragment fragment) {
        if (this.C831O13C118 != null) {
            throw new IllegalStateException("Already attached");
        }
        this.C831O13C118 = c3llC38O1;
        this.C8CI00 = c8ci00;
        this.CC38COI1l3I = fragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void C01O0C(Fragment fragment) {
        if (!fragment.I0IC1O01888) {
            return;
        }
        if (this.C11ll3) {
            this.CII3C813OIC8 = true;
            return;
        }
        fragment.I0IC1O01888 = false;
        C01O0C(fragment, this.C3llC38O1, 0, 0, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void C01O0C(Fragment fragment, int i, int i2) {
        if (C01O0C) {
            Log.v("FragmentManager", "remove: " + fragment + " nesting=" + fragment.CCC3CC0l);
        }
        boolean z = !fragment.C01O0C();
        if (!fragment.CO1830lI8C03 || z) {
            if (this.C1l00I1 != null) {
                this.C1l00I1.remove(fragment);
            }
            if (fragment.I003I0 && fragment.I003OlCCOlC) {
                this.CC8IOI1II0 = true;
            }
            fragment.C3l3O8lIOIO8 = false;
            fragment.C3llC38O1 = true;
            C01O0C(fragment, z ? 0 : 1, i, i2, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.CO88CO1Cl383.C01O0C(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.CO88CO1Cl383.C01O0C(android.view.View, android.graphics.Paint):void
      android.support.v4.view.CO88CO1Cl383.C01O0C(android.view.View, android.support.v4.view.C01O0C):void
      android.support.v4.view.CO88CO1Cl383.C01O0C(android.view.View, java.lang.Runnable):void
      android.support.v4.view.CO88CO1Cl383.C01O0C(android.view.View, int):boolean
      android.support.v4.view.CO88CO1Cl383.C01O0C(android.view.View, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(int, int, int, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    /* access modifiers changed from: package-private */
    public void C01O0C(Fragment fragment, int i, int i2, int i3, boolean z) {
        ViewGroup viewGroup;
        if ((!fragment.C3l3O8lIOIO8 || fragment.CO1830lI8C03) && i > 1) {
            i = 1;
        }
        if (fragment.C3llC38O1 && i > fragment.C0I1O3C3lI8) {
            i = fragment.C0I1O3C3lI8;
        }
        if (fragment.I0IC1O01888 && fragment.C0I1O3C3lI8 < 4 && i > 3) {
            i = 3;
        }
        if (fragment.C0I1O3C3lI8 >= i) {
            if (fragment.C0I1O3C3lI8 > i) {
                switch (fragment.C0I1O3C3lI8) {
                    case 5:
                        if (i < 5) {
                            if (C01O0C) {
                                Log.v("FragmentManager", "movefrom RESUMED: " + fragment);
                            }
                            fragment.CO88CO1Cl383();
                            fragment.C831O13C118 = false;
                        }
                    case 4:
                        if (i < 4) {
                            if (C01O0C) {
                                Log.v("FragmentManager", "movefrom STARTED: " + fragment);
                            }
                            fragment.I003I0();
                        }
                    case 3:
                        if (i < 3) {
                            if (C01O0C) {
                                Log.v("FragmentManager", "movefrom STOPPED: " + fragment);
                            }
                            fragment.I003OlCCOlC();
                        }
                    case 2:
                        if (i < 2) {
                            if (C01O0C) {
                                Log.v("FragmentManager", "movefrom ACTIVITY_CREATED: " + fragment);
                            }
                            if (fragment.I08O3C != null && !this.C831O13C118.isFinishing() && fragment.C18Cl1C == null) {
                                C11ll3(fragment);
                            }
                            fragment.I008018O();
                            if (!(fragment.I08O3C == null || fragment.I088l3088 == null)) {
                                Animation C01O0C2 = (this.C3llC38O1 <= 0 || this.CI0I8l333131) ? null : C01O0C(fragment, i2, false, i3);
                                if (C01O0C2 != null) {
                                    fragment.C101lC8O = fragment.I08O3C;
                                    fragment.C11013l3 = i;
                                    C01O0C2.setAnimationListener(new CI3C103l01O(this, fragment));
                                    fragment.I08O3C.startAnimation(C01O0C2);
                                }
                                fragment.I088l3088.removeView(fragment.I08O3C);
                            }
                            fragment.I088l3088 = null;
                            fragment.I08O3C = null;
                            fragment.I0CIIIC = null;
                        }
                        break;
                    case 1:
                        if (i < 1) {
                            if (this.CI0I8l333131 && fragment.C101lC8O != null) {
                                View view = fragment.C101lC8O;
                                fragment.C101lC8O = null;
                                view.clearAnimation();
                            }
                            if (fragment.C101lC8O == null) {
                                if (C01O0C) {
                                    Log.v("FragmentManager", "movefrom CREATED: " + fragment);
                                }
                                if (!fragment.CO88CO1Cl383) {
                                    fragment.I03lII1();
                                }
                                fragment.I008018O = false;
                                fragment.C8CI00();
                                if (fragment.I008018O) {
                                    if (!z) {
                                        if (fragment.CO88CO1Cl383) {
                                            fragment.CI3C103l01O = null;
                                            fragment.CIOC8C = null;
                                            fragment.CI0I8l333131 = null;
                                            fragment.CII3C813OIC8 = null;
                                            break;
                                        } else {
                                            C11013l3(fragment);
                                            break;
                                        }
                                    }
                                } else {
                                    throw new l888l8lCIl0I("Fragment " + fragment + " did not call through to super.onDetach()");
                                }
                            } else {
                                fragment.C11013l3 = i;
                                i = 1;
                                break;
                            }
                        }
                        break;
                }
            }
        } else if (!fragment.C8CI00 || fragment.CC38COI1l3I) {
            if (fragment.C101lC8O != null) {
                fragment.C101lC8O = null;
                C01O0C(fragment, fragment.C11013l3, 0, 0, true);
            }
            switch (fragment.C0I1O3C3lI8) {
                case 0:
                    if (C01O0C) {
                        Log.v("FragmentManager", "moveto CREATED: " + fragment);
                    }
                    if (fragment.C11ll3 != null) {
                        fragment.C11ll3.setClassLoader(this.C831O13C118.getClassLoader());
                        fragment.C18Cl1C = fragment.C11ll3.getSparseParcelableArray("android:view_state");
                        fragment.C3C1C0I8l3 = C01O0C(fragment.C11ll3, "android:target_state");
                        if (fragment.C3C1C0I8l3 != null) {
                            fragment.C3ICl0OOl = fragment.C11ll3.getInt("android:target_req_state", 0);
                        }
                        fragment.I0l3Oll3 = fragment.C11ll3.getBoolean("android:user_visible_hint", true);
                        if (!fragment.I0l3Oll3) {
                            fragment.I0IC1O01888 = true;
                            if (i > 3) {
                                i = 3;
                            }
                        }
                    }
                    fragment.CI3C103l01O = this.C831O13C118;
                    fragment.CIOC8C = this.CC38COI1l3I;
                    fragment.CI0I8l333131 = this.CC38COI1l3I != null ? this.CC38COI1l3I.CII3C813OIC8 : this.C831O13C118.C0I1O3C3lI8;
                    fragment.I008018O = false;
                    fragment.C01O0C(this.C831O13C118);
                    if (!fragment.I008018O) {
                        throw new l888l8lCIl0I("Fragment " + fragment + " did not call through to super.onAttach()");
                    }
                    if (fragment.CIOC8C == null) {
                        this.C831O13C118.C01O0C(fragment);
                    }
                    if (!fragment.CO88CO1Cl383) {
                        fragment.C1l00I1(fragment.C11ll3);
                    }
                    fragment.CO88CO1Cl383 = false;
                    if (fragment.C8CI00) {
                        fragment.I08O3C = fragment.C0I1O3C3lI8(fragment.C0I1O3C3lI8(fragment.C11ll3), null, fragment.C11ll3);
                        if (fragment.I08O3C != null) {
                            fragment.I0CIIIC = fragment.I08O3C;
                            if (Build.VERSION.SDK_INT >= 11) {
                                CO88CO1Cl383.C01O0C(fragment.I08O3C, false);
                            } else {
                                fragment.I08O3C = I0OlCO0CI13.C01O0C(fragment.I08O3C);
                            }
                            if (fragment.CO081lO0OC0) {
                                fragment.I08O3C.setVisibility(8);
                            }
                            fragment.C01O0C(fragment.I08O3C, fragment.C11ll3);
                        } else {
                            fragment.I0CIIIC = null;
                        }
                    }
                case 1:
                    if (i > 1) {
                        if (C01O0C) {
                            Log.v("FragmentManager", "moveto ACTIVITY_CREATED: " + fragment);
                        }
                        if (!fragment.C8CI00) {
                            if (fragment.ClC13lIl != 0) {
                                viewGroup = (ViewGroup) this.C8CI00.C01O0C(fragment.ClC13lIl);
                                if (viewGroup == null && !fragment.CC8IOI1II0) {
                                    C01O0C(new IllegalArgumentException("No view found for id 0x" + Integer.toHexString(fragment.ClC13lIl) + " (" + fragment.C101lC8O().getResourceName(fragment.ClC13lIl) + ") for fragment " + fragment));
                                }
                            } else {
                                viewGroup = null;
                            }
                            fragment.I088l3088 = viewGroup;
                            fragment.I08O3C = fragment.C0I1O3C3lI8(fragment.C0I1O3C3lI8(fragment.C11ll3), viewGroup, fragment.C11ll3);
                            if (fragment.I08O3C != null) {
                                fragment.I0CIIIC = fragment.I08O3C;
                                if (Build.VERSION.SDK_INT >= 11) {
                                    CO88CO1Cl383.C01O0C(fragment.I08O3C, false);
                                } else {
                                    fragment.I08O3C = I0OlCO0CI13.C01O0C(fragment.I08O3C);
                                }
                                if (viewGroup != null) {
                                    Animation C01O0C3 = C01O0C(fragment, i2, true, i3);
                                    if (C01O0C3 != null) {
                                        fragment.I08O3C.startAnimation(C01O0C3);
                                    }
                                    viewGroup.addView(fragment.I08O3C);
                                }
                                if (fragment.CO081lO0OC0) {
                                    fragment.I08O3C.setVisibility(8);
                                }
                                fragment.C01O0C(fragment.I08O3C, fragment.C11ll3);
                            } else {
                                fragment.I0CIIIC = null;
                            }
                        }
                        fragment.C1O10Cl038(fragment.C11ll3);
                        if (fragment.I08O3C != null) {
                            fragment.C01O0C(fragment.C11ll3);
                        }
                        fragment.C11ll3 = null;
                    }
                case 2:
                case 3:
                    if (i > 3) {
                        if (C01O0C) {
                            Log.v("FragmentManager", "moveto STARTED: " + fragment);
                        }
                        fragment.CO081lO0OC0();
                    }
                case 4:
                    if (i > 4) {
                        if (C01O0C) {
                            Log.v("FragmentManager", "moveto RESUMED: " + fragment);
                        }
                        fragment.C831O13C118 = true;
                        fragment.CO1830lI8C03();
                        fragment.C11ll3 = null;
                        fragment.C18Cl1C = null;
                        break;
                    }
                    break;
            }
        } else {
            return;
        }
        fragment.C0I1O3C3lI8 = i;
    }

    public void C01O0C(Fragment fragment, boolean z) {
        if (this.C1l00I1 == null) {
            this.C1l00I1 = new ArrayList();
        }
        if (C01O0C) {
            Log.v("FragmentManager", "add: " + fragment);
        }
        C101lC8O(fragment);
        if (fragment.CO1830lI8C03) {
            return;
        }
        if (this.C1l00I1.contains(fragment)) {
            throw new IllegalStateException("Fragment already added: " + fragment);
        }
        this.C1l00I1.add(fragment);
        fragment.C3l3O8lIOIO8 = true;
        fragment.C3llC38O1 = false;
        if (fragment.I003I0 && fragment.I003OlCCOlC) {
            this.CC8IOI1II0 = true;
        }
        if (z) {
            C0I1O3C3lI8(fragment);
        }
    }

    public void C01O0C(Runnable runnable, boolean z) {
        if (!z) {
            CI3C103l01O();
        }
        synchronized (this) {
            if (this.CI0I8l333131 || this.C831O13C118 == null) {
                throw new IllegalStateException("Activity has been destroyed");
            }
            if (this.C101lC8O == null) {
                this.C101lC8O = new ArrayList();
            }
            this.C101lC8O.add(runnable);
            if (this.C101lC8O.size() == 1) {
                this.C831O13C118.C01O0C.removeCallbacks(this.ClC13lIl);
                this.C831O13C118.C01O0C.post(this.ClC13lIl);
            }
        }
    }

    public void C01O0C(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        int size3;
        int size4;
        int size5;
        int size6;
        String str2 = str + "    ";
        if (this.C18Cl1C != null && (size6 = this.C18Cl1C.size()) > 0) {
            printWriter.print(str);
            printWriter.print("Active Fragments in ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this)));
            printWriter.println(":");
            for (int i = 0; i < size6; i++) {
                Fragment fragment = (Fragment) this.C18Cl1C.get(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.println(fragment);
                if (fragment != null) {
                    fragment.C01O0C(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
        if (this.C1l00I1 != null && (size5 = this.C1l00I1.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i2 = 0; i2 < size5; i2++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(((Fragment) this.C1l00I1.get(i2)).toString());
            }
        }
        if (this.C3C1C0I8l3 != null && (size4 = this.C3C1C0I8l3.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i3 = 0; i3 < size4; i3++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(((Fragment) this.C3C1C0I8l3.get(i3)).toString());
            }
        }
        if (this.C1OC33O0lO81 != null && (size3 = this.C1OC33O0lO81.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i4 = 0; i4 < size3; i4++) {
                C11013l3 c11013l3 = (C11013l3) this.C1OC33O0lO81.get(i4);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i4);
                printWriter.print(": ");
                printWriter.println(c11013l3.toString());
                c11013l3.C01O0C(str2, fileDescriptor, printWriter, strArr);
            }
        }
        synchronized (this) {
            if (this.C3CIO118 != null && (size2 = this.C3CIO118.size()) > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack Indices:");
                for (int i5 = 0; i5 < size2; i5++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i5);
                    printWriter.print(": ");
                    printWriter.println((C11013l3) this.C3CIO118.get(i5));
                }
            }
            if (this.C3ICl0OOl != null && this.C3ICl0OOl.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.C3ICl0OOl.toArray()));
            }
        }
        if (this.C101lC8O != null && (size = this.C101lC8O.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Pending Actions:");
            for (int i6 = 0; i6 < size; i6++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i6);
                printWriter.print(": ");
                printWriter.println((Runnable) this.C101lC8O.get(i6));
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mActivity=");
        printWriter.println(this.C831O13C118);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.C8CI00);
        if (this.CC38COI1l3I != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.CC38COI1l3I);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.C3llC38O1);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.CCC3CC0l);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.CI0I8l333131);
        if (this.CC8IOI1II0) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.CC8IOI1II0);
        }
        if (this.CI3C103l01O != null) {
            printWriter.print(str);
            printWriter.print("  mNoTransactionsBecause=");
            printWriter.println(this.CI3C103l01O);
        }
        if (this.C1O10Cl038 != null && this.C1O10Cl038.size() > 0) {
            printWriter.print(str);
            printWriter.print("  mAvailIndices: ");
            printWriter.println(Arrays.toString(this.C1O10Cl038.toArray()));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C11013l3.C01O0C(boolean, android.support.v4.app.C1OC33O0lO81, android.util.SparseArray, android.util.SparseArray):android.support.v4.app.C1OC33O0lO81
     arg types: [int, ?[OBJECT, ARRAY], android.util.SparseArray, android.util.SparseArray]
     candidates:
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C11013l3, android.support.v4.app.C1OC33O0lO81, boolean, android.support.v4.app.Fragment):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(int, android.support.v4.app.Fragment, java.lang.String, int):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C11013l3, android.support.v4.app.C1OC33O0lO81, int, java.lang.Object):void
      android.support.v4.app.C11013l3.C01O0C(android.view.View, android.support.v4.app.C1OC33O0lO81, int, java.lang.Object):void
      android.support.v4.app.C11013l3.C01O0C(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.C11013l3.C01O0C(boolean, android.support.v4.app.C1OC33O0lO81, android.util.SparseArray, android.util.SparseArray):android.support.v4.app.C1OC33O0lO81 */
    /* access modifiers changed from: package-private */
    public boolean C01O0C(Handler handler, String str, int i, int i2) {
        int i3;
        if (this.C1OC33O0lO81 == null) {
            return false;
        }
        if (str == null && i < 0 && (i2 & 1) == 0) {
            int size = this.C1OC33O0lO81.size() - 1;
            if (size < 0) {
                return false;
            }
            C11013l3 c11013l3 = (C11013l3) this.C1OC33O0lO81.remove(size);
            SparseArray sparseArray = new SparseArray();
            SparseArray sparseArray2 = new SparseArray();
            c11013l3.C01O0C(sparseArray, sparseArray2);
            c11013l3.C01O0C(true, (C1OC33O0lO81) null, sparseArray, sparseArray2);
            C18Cl1C();
        } else {
            int i4 = -1;
            if (str != null || i >= 0) {
                int size2 = this.C1OC33O0lO81.size() - 1;
                while (i3 >= 0) {
                    C11013l3 c11013l32 = (C11013l3) this.C1OC33O0lO81.get(i3);
                    if ((str != null && str.equals(c11013l32.C0I1O3C3lI8())) || (i >= 0 && i == c11013l32.C831O13C118)) {
                        break;
                    }
                    size2 = i3 - 1;
                }
                if (i3 < 0) {
                    return false;
                }
                if ((i2 & 1) != 0) {
                    i3--;
                    while (i3 >= 0) {
                        C11013l3 c11013l33 = (C11013l3) this.C1OC33O0lO81.get(i3);
                        if ((str == null || !str.equals(c11013l33.C0I1O3C3lI8())) && (i < 0 || i != c11013l33.C831O13C118)) {
                            break;
                        }
                        i3--;
                    }
                }
                i4 = i3;
            }
            if (i4 == this.C1OC33O0lO81.size() - 1) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int size3 = this.C1OC33O0lO81.size() - 1; size3 > i4; size3--) {
                arrayList.add(this.C1OC33O0lO81.remove(size3));
            }
            int size4 = arrayList.size() - 1;
            SparseArray sparseArray3 = new SparseArray();
            SparseArray sparseArray4 = new SparseArray();
            for (int i5 = 0; i5 <= size4; i5++) {
                ((C11013l3) arrayList.get(i5)).C01O0C(sparseArray3, sparseArray4);
            }
            C1OC33O0lO81 c1OC33O0lO81 = null;
            int i6 = 0;
            while (i6 <= size4) {
                if (C01O0C) {
                    Log.v("FragmentManager", "Popping back stack state: " + arrayList.get(i6));
                }
                i6++;
                c1OC33O0lO81 = ((C11013l3) arrayList.get(i6)).C01O0C(i6 == size4, c1OC33O0lO81, sparseArray3, sparseArray4);
            }
            C18Cl1C();
        }
        return true;
    }

    public boolean C01O0C(Menu menu) {
        if (this.C1l00I1 == null) {
            return false;
        }
        boolean z = false;
        for (int i = 0; i < this.C1l00I1.size(); i++) {
            Fragment fragment = (Fragment) this.C1l00I1.get(i);
            if (fragment != null && fragment.C101lC8O(menu)) {
                z = true;
            }
        }
        return z;
    }

    public boolean C01O0C(Menu menu, MenuInflater menuInflater) {
        boolean z;
        ArrayList arrayList = null;
        if (this.C1l00I1 != null) {
            int i = 0;
            z = false;
            while (i < this.C1l00I1.size()) {
                Fragment fragment = (Fragment) this.C1l00I1.get(i);
                if (fragment != null && fragment.C0I1O3C3lI8(menu, menuInflater)) {
                    z = true;
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(fragment);
                }
                i++;
                z = z;
            }
        } else {
            z = false;
        }
        if (this.C3C1C0I8l3 != null) {
            for (int i2 = 0; i2 < this.C3C1C0I8l3.size(); i2++) {
                Fragment fragment2 = (Fragment) this.C3C1C0I8l3.get(i2);
                if (arrayList == null || !arrayList.contains(fragment2)) {
                    fragment2.CC38COI1l3I();
                }
            }
        }
        this.C3C1C0I8l3 = arrayList;
        return z;
    }

    public boolean C01O0C(MenuItem menuItem) {
        if (this.C1l00I1 == null) {
            return false;
        }
        for (int i = 0; i < this.C1l00I1.size(); i++) {
            Fragment fragment = (Fragment) this.C1l00I1.get(i);
            if (fragment != null && fragment.C101lC8O(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public void C0I1O3C3lI8(int i) {
        synchronized (this) {
            this.C3CIO118.set(i, null);
            if (this.C3ICl0OOl == null) {
                this.C3ICl0OOl = new ArrayList();
            }
            if (C01O0C) {
                Log.v("FragmentManager", "Freeing back stack index " + i);
            }
            this.C3ICl0OOl.add(Integer.valueOf(i));
        }
    }

    /* access modifiers changed from: package-private */
    public void C0I1O3C3lI8(C11013l3 c11013l3) {
        if (this.C1OC33O0lO81 == null) {
            this.C1OC33O0lO81 = new ArrayList();
        }
        this.C1OC33O0lO81.add(c11013l3);
        C18Cl1C();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public void C0I1O3C3lI8(Fragment fragment) {
        C01O0C(fragment, this.C3llC38O1, 0, 0, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(int, int, int, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    public void C0I1O3C3lI8(Fragment fragment, int i, int i2) {
        if (C01O0C) {
            Log.v("FragmentManager", "hide: " + fragment);
        }
        if (!fragment.CO081lO0OC0) {
            fragment.CO081lO0OC0 = true;
            if (fragment.I08O3C != null) {
                Animation C01O0C2 = C01O0C(fragment, i, false, i2);
                if (C01O0C2 != null) {
                    fragment.I08O3C.startAnimation(C01O0C2);
                }
                fragment.I08O3C.setVisibility(8);
            }
            if (fragment.C3l3O8lIOIO8 && fragment.I003I0 && fragment.I003OlCCOlC) {
                this.CC8IOI1II0 = true;
            }
            fragment.C01O0C(true);
        }
    }

    public void C0I1O3C3lI8(Menu menu) {
        if (this.C1l00I1 != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.C1l00I1.size()) {
                    Fragment fragment = (Fragment) this.C1l00I1.get(i2);
                    if (fragment != null) {
                        fragment.C11013l3(menu);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public boolean C0I1O3C3lI8() {
        return C11ll3();
    }

    public boolean C0I1O3C3lI8(MenuItem menuItem) {
        if (this.C1l00I1 == null) {
            return false;
        }
        for (int i = 0; i < this.C1l00I1.size(); i++) {
            Fragment fragment = (Fragment) this.C1l00I1.get(i);
            if (fragment != null && fragment.C11013l3(menuItem)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void C101lC8O(Fragment fragment) {
        if (fragment.C1l00I1 < 0) {
            if (this.C1O10Cl038 == null || this.C1O10Cl038.size() <= 0) {
                if (this.C18Cl1C == null) {
                    this.C18Cl1C = new ArrayList();
                }
                fragment.C01O0C(this.C18Cl1C.size(), this.CC38COI1l3I);
                this.C18Cl1C.add(fragment);
            } else {
                fragment.C01O0C(((Integer) this.C1O10Cl038.remove(this.C1O10Cl038.size() - 1)).intValue(), this.CC38COI1l3I);
                this.C18Cl1C.set(fragment.C1l00I1, fragment);
            }
            if (C01O0C) {
                Log.v("FragmentManager", "Allocated fragment index " + fragment);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(int, int, int, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    public void C101lC8O(Fragment fragment, int i, int i2) {
        if (C01O0C) {
            Log.v("FragmentManager", "show: " + fragment);
        }
        if (fragment.CO081lO0OC0) {
            fragment.CO081lO0OC0 = false;
            if (fragment.I08O3C != null) {
                Animation C01O0C2 = C01O0C(fragment, i, true, i2);
                if (C01O0C2 != null) {
                    fragment.I08O3C.startAnimation(C01O0C2);
                }
                fragment.I08O3C.setVisibility(0);
            }
            if (fragment.C3l3O8lIOIO8 && fragment.I003I0 && fragment.I003OlCCOlC) {
                this.CC8IOI1II0 = true;
            }
            fragment.C01O0C(false);
        }
    }

    public boolean C101lC8O() {
        CI3C103l01O();
        C0I1O3C3lI8();
        return C01O0C(this.C831O13C118.C01O0C, (String) null, -1, 0);
    }

    /* access modifiers changed from: package-private */
    public void C11013l3() {
        if (this.C18Cl1C != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.C18Cl1C.size()) {
                    Fragment fragment = (Fragment) this.C18Cl1C.get(i2);
                    if (fragment != null) {
                        C01O0C(fragment);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void C11013l3(Fragment fragment) {
        if (fragment.C1l00I1 >= 0) {
            if (C01O0C) {
                Log.v("FragmentManager", "Freeing fragment index " + fragment);
            }
            this.C18Cl1C.set(fragment.C1l00I1, null);
            if (this.C1O10Cl038 == null) {
                this.C1O10Cl038 = new ArrayList();
            }
            this.C1O10Cl038.add(Integer.valueOf(fragment.C1l00I1));
            this.C831O13C118.C01O0C(fragment.C1O10Cl038);
            fragment.C831O13C118();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void C11013l3(Fragment fragment, int i, int i2) {
        if (C01O0C) {
            Log.v("FragmentManager", "detach: " + fragment);
        }
        if (!fragment.CO1830lI8C03) {
            fragment.CO1830lI8C03 = true;
            if (fragment.C3l3O8lIOIO8) {
                if (this.C1l00I1 != null) {
                    if (C01O0C) {
                        Log.v("FragmentManager", "remove from detach: " + fragment);
                    }
                    this.C1l00I1.remove(fragment);
                }
                if (fragment.I003I0 && fragment.I003OlCCOlC) {
                    this.CC8IOI1II0 = true;
                }
                fragment.C3l3O8lIOIO8 = false;
                C01O0C(fragment, 1, i, i2, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void C11ll3(Fragment fragment) {
        if (fragment.I0CIIIC != null) {
            if (this.Cl80C0l838l == null) {
                this.Cl80C0l838l = new SparseArray();
            } else {
                this.Cl80C0l838l.clear();
            }
            fragment.I0CIIIC.saveHierarchyState(this.Cl80C0l838l);
            if (this.Cl80C0l838l.size() > 0) {
                fragment.C18Cl1C = this.Cl80C0l838l;
                this.Cl80C0l838l = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void C11ll3(Fragment fragment, int i, int i2) {
        if (C01O0C) {
            Log.v("FragmentManager", "attach: " + fragment);
        }
        if (fragment.CO1830lI8C03) {
            fragment.CO1830lI8C03 = false;
            if (!fragment.C3l3O8lIOIO8) {
                if (this.C1l00I1 == null) {
                    this.C1l00I1 = new ArrayList();
                }
                if (this.C1l00I1.contains(fragment)) {
                    throw new IllegalStateException("Fragment already added: " + fragment);
                }
                if (C01O0C) {
                    Log.v("FragmentManager", "add from attach: " + fragment);
                }
                this.C1l00I1.add(fragment);
                fragment.C3l3O8lIOIO8 = true;
                if (fragment.I003I0 && fragment.I003OlCCOlC) {
                    this.CC8IOI1II0 = true;
                }
                C01O0C(fragment, this.C3llC38O1, i, i2, false);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0083, code lost:
        r6.C11ll3 = true;
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0086, code lost:
        if (r1 >= r3) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0088, code lost:
        r6.C11013l3[r1].run();
        r6.C11013l3[r1] = null;
        r1 = r1 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean C11ll3() {
        /*
            r6 = this;
            r0 = 1
            r2 = 0
            boolean r1 = r6.C11ll3
            if (r1 == 0) goto L_0x000e
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Recursive entry to executePendingTransactions"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            android.os.Looper r1 = android.os.Looper.myLooper()
            android.support.v4.app.C3llC38O1 r3 = r6.C831O13C118
            android.os.Handler r3 = r3.C01O0C
            android.os.Looper r3 = r3.getLooper()
            if (r1 == r3) goto L_0x0024
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Must be called from main thread of process"
            r0.<init>(r1)
            throw r0
        L_0x0024:
            r1 = r2
        L_0x0025:
            monitor-enter(r6)
            java.util.ArrayList r3 = r6.C101lC8O     // Catch:{ all -> 0x0097 }
            if (r3 == 0) goto L_0x0032
            java.util.ArrayList r3 = r6.C101lC8O     // Catch:{ all -> 0x0097 }
            int r3 = r3.size()     // Catch:{ all -> 0x0097 }
            if (r3 != 0) goto L_0x005a
        L_0x0032:
            monitor-exit(r6)     // Catch:{ all -> 0x0097 }
            boolean r0 = r6.CII3C813OIC8
            if (r0 == 0) goto L_0x00a5
            r3 = r2
            r4 = r2
        L_0x0039:
            java.util.ArrayList r0 = r6.C18Cl1C
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x009e
            java.util.ArrayList r0 = r6.C18Cl1C
            java.lang.Object r0 = r0.get(r3)
            android.support.v4.app.Fragment r0 = (android.support.v4.app.Fragment) r0
            if (r0 == 0) goto L_0x0056
            android.support.v4.app.I0IC1O01888 r5 = r0.I0OlCO0CI13
            if (r5 == 0) goto L_0x0056
            android.support.v4.app.I0IC1O01888 r0 = r0.I0OlCO0CI13
            boolean r0 = r0.C01O0C()
            r4 = r4 | r0
        L_0x0056:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0039
        L_0x005a:
            java.util.ArrayList r1 = r6.C101lC8O     // Catch:{ all -> 0x0097 }
            int r3 = r1.size()     // Catch:{ all -> 0x0097 }
            java.lang.Runnable[] r1 = r6.C11013l3     // Catch:{ all -> 0x0097 }
            if (r1 == 0) goto L_0x0069
            java.lang.Runnable[] r1 = r6.C11013l3     // Catch:{ all -> 0x0097 }
            int r1 = r1.length     // Catch:{ all -> 0x0097 }
            if (r1 >= r3) goto L_0x006d
        L_0x0069:
            java.lang.Runnable[] r1 = new java.lang.Runnable[r3]     // Catch:{ all -> 0x0097 }
            r6.C11013l3 = r1     // Catch:{ all -> 0x0097 }
        L_0x006d:
            java.util.ArrayList r1 = r6.C101lC8O     // Catch:{ all -> 0x0097 }
            java.lang.Runnable[] r4 = r6.C11013l3     // Catch:{ all -> 0x0097 }
            r1.toArray(r4)     // Catch:{ all -> 0x0097 }
            java.util.ArrayList r1 = r6.C101lC8O     // Catch:{ all -> 0x0097 }
            r1.clear()     // Catch:{ all -> 0x0097 }
            android.support.v4.app.C3llC38O1 r1 = r6.C831O13C118     // Catch:{ all -> 0x0097 }
            android.os.Handler r1 = r1.C01O0C     // Catch:{ all -> 0x0097 }
            java.lang.Runnable r4 = r6.ClC13lIl     // Catch:{ all -> 0x0097 }
            r1.removeCallbacks(r4)     // Catch:{ all -> 0x0097 }
            monitor-exit(r6)     // Catch:{ all -> 0x0097 }
            r6.C11ll3 = r0
            r1 = r2
        L_0x0086:
            if (r1 >= r3) goto L_0x009a
            java.lang.Runnable[] r4 = r6.C11013l3
            r4 = r4[r1]
            r4.run()
            java.lang.Runnable[] r4 = r6.C11013l3
            r5 = 0
            r4[r1] = r5
            int r1 = r1 + 1
            goto L_0x0086
        L_0x0097:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0097 }
            throw r0
        L_0x009a:
            r6.C11ll3 = r2
            r1 = r0
            goto L_0x0025
        L_0x009e:
            if (r4 != 0) goto L_0x00a5
            r6.CII3C813OIC8 = r2
            r6.C11013l3()
        L_0x00a5:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.CCC3CC0l.C11ll3():boolean");
    }

    /* access modifiers changed from: package-private */
    public Bundle C18Cl1C(Fragment fragment) {
        Bundle bundle;
        if (this.CIOC8C == null) {
            this.CIOC8C = new Bundle();
        }
        fragment.C1OC33O0lO81(this.CIOC8C);
        if (!this.CIOC8C.isEmpty()) {
            bundle = this.CIOC8C;
            this.CIOC8C = null;
        } else {
            bundle = null;
        }
        if (fragment.I08O3C != null) {
            C11ll3(fragment);
        }
        if (fragment.C18Cl1C != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", fragment.C18Cl1C);
        }
        if (!fragment.I0l3Oll3) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", fragment.I0l3Oll3);
        }
        return bundle;
    }

    /* access modifiers changed from: package-private */
    public void C18Cl1C() {
        if (this.C3l3O8lIOIO8 != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.C3l3O8lIOIO8.size()) {
                    ((CC8IOI1II0) this.C3l3O8lIOIO8.get(i2)).C01O0C();
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Parcelable C1O10Cl038() {
        int[] iArr;
        int size;
        int size2;
        boolean z;
        BackStackState[] backStackStateArr = null;
        C11ll3();
        if (C0I1O3C3lI8) {
            this.CCC3CC0l = true;
        }
        if (this.C18Cl1C == null || this.C18Cl1C.size() <= 0) {
            return null;
        }
        int size3 = this.C18Cl1C.size();
        FragmentState[] fragmentStateArr = new FragmentState[size3];
        int i = 0;
        boolean z2 = false;
        while (i < size3) {
            Fragment fragment = (Fragment) this.C18Cl1C.get(i);
            if (fragment != null) {
                if (fragment.C1l00I1 < 0) {
                    C01O0C(new IllegalStateException("Failure saving state: active " + fragment + " has cleared index: " + fragment.C1l00I1));
                }
                FragmentState fragmentState = new FragmentState(fragment);
                fragmentStateArr[i] = fragmentState;
                if (fragment.C0I1O3C3lI8 <= 0 || fragmentState.C3C1C0I8l3 != null) {
                    fragmentState.C3C1C0I8l3 = fragment.C11ll3;
                } else {
                    fragmentState.C3C1C0I8l3 = C18Cl1C(fragment);
                    if (fragment.C3C1C0I8l3 != null) {
                        if (fragment.C3C1C0I8l3.C1l00I1 < 0) {
                            C01O0C(new IllegalStateException("Failure saving state: " + fragment + " has target not in fragment manager: " + fragment.C3C1C0I8l3));
                        }
                        if (fragmentState.C3C1C0I8l3 == null) {
                            fragmentState.C3C1C0I8l3 = new Bundle();
                        }
                        C01O0C(fragmentState.C3C1C0I8l3, "android:target_state", fragment.C3C1C0I8l3);
                        if (fragment.C3ICl0OOl != 0) {
                            fragmentState.C3C1C0I8l3.putInt("android:target_req_state", fragment.C3ICl0OOl);
                        }
                    }
                }
                if (C01O0C) {
                    Log.v("FragmentManager", "Saved state of " + fragment + ": " + fragmentState.C3C1C0I8l3);
                }
                z = true;
            } else {
                z = z2;
            }
            i++;
            z2 = z;
        }
        if (z2) {
            if (this.C1l00I1 == null || (size2 = this.C1l00I1.size()) <= 0) {
                iArr = null;
            } else {
                iArr = new int[size2];
                for (int i2 = 0; i2 < size2; i2++) {
                    iArr[i2] = ((Fragment) this.C1l00I1.get(i2)).C1l00I1;
                    if (iArr[i2] < 0) {
                        C01O0C(new IllegalStateException("Failure saving state: active " + this.C1l00I1.get(i2) + " has cleared index: " + iArr[i2]));
                    }
                    if (C01O0C) {
                        Log.v("FragmentManager", "saveAllState: adding fragment #" + i2 + ": " + this.C1l00I1.get(i2));
                    }
                }
            }
            if (this.C1OC33O0lO81 != null && (size = this.C1OC33O0lO81.size()) > 0) {
                backStackStateArr = new BackStackState[size];
                for (int i3 = 0; i3 < size; i3++) {
                    backStackStateArr[i3] = new BackStackState(this, (C11013l3) this.C1OC33O0lO81.get(i3));
                    if (C01O0C) {
                        Log.v("FragmentManager", "saveAllState: adding back stack #" + i3 + ": " + this.C1OC33O0lO81.get(i3));
                    }
                }
            }
            FragmentManagerState fragmentManagerState = new FragmentManagerState();
            fragmentManagerState.C01O0C = fragmentStateArr;
            fragmentManagerState.C0I1O3C3lI8 = iArr;
            fragmentManagerState.C101lC8O = backStackStateArr;
            return fragmentManagerState;
        } else if (!C01O0C) {
            return null;
        } else {
            Log.v("FragmentManager", "saveAllState: no fragments!");
            return null;
        }
    }

    public void C1OC33O0lO81() {
        this.CCC3CC0l = false;
    }

    /* access modifiers changed from: package-private */
    public ArrayList C1l00I1() {
        ArrayList arrayList = null;
        if (this.C18Cl1C != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.C18Cl1C.size()) {
                    break;
                }
                Fragment fragment = (Fragment) this.C18Cl1C.get(i2);
                if (fragment != null && fragment.CO30CC1l0313) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(fragment);
                    fragment.CO88CO1Cl383 = true;
                    fragment.C3CIO118 = fragment.C3C1C0I8l3 != null ? fragment.C3C1C0I8l3.C1l00I1 : -1;
                    if (C01O0C) {
                        Log.v("FragmentManager", "retainNonConfig: keeping retained " + fragment);
                    }
                }
                i = i2 + 1;
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void */
    public void C3C1C0I8l3() {
        this.CCC3CC0l = false;
        C01O0C(1, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void */
    public void C3CIO118() {
        this.CCC3CC0l = false;
        C01O0C(2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void */
    public void C3ICl0OOl() {
        this.CCC3CC0l = false;
        C01O0C(4, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void */
    public void C3l3O8lIOIO8() {
        this.CCC3CC0l = false;
        C01O0C(5, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void */
    public void C3llC38O1() {
        C01O0C(4, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void */
    public void C831O13C118() {
        this.CCC3CC0l = true;
        C01O0C(3, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void */
    public void C8CI00() {
        C01O0C(2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void */
    public void CC38COI1l3I() {
        C01O0C(1, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void */
    public void CC8IOI1II0() {
        this.CI0I8l333131 = true;
        C11ll3();
        C01O0C(0, false);
        this.C831O13C118 = null;
        this.C8CI00 = null;
        this.CC38COI1l3I = null;
    }

    public void CCC3CC0l() {
        if (this.C1l00I1 != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.C1l00I1.size()) {
                    Fragment fragment = (Fragment) this.C1l00I1.get(i2);
                    if (fragment != null) {
                        fragment.CO30CC1l0313();
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public LayoutInflater.Factory CI0I8l333131() {
        return this;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [android.view.View, java.lang.String] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        Fragment fragment;
        ? r4 = 0;
        if (!"fragment".equals(str)) {
            return r4;
        }
        String attributeValue = attributeSet.getAttributeValue(r4, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, CII3C813OIC8.C01O0C);
        String string = attributeValue == null ? obtainStyledAttributes.getString(0) : attributeValue;
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string2 = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (!Fragment.C0I1O3C3lI8(this.C831O13C118, string)) {
            return r4;
        }
        int id = r4 != 0 ? r4.getId() : 0;
        if (id == -1 && resourceId == -1 && string2 == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + string);
        }
        Fragment C01O0C2 = resourceId != -1 ? C01O0C(resourceId) : r4;
        if (C01O0C2 == null && string2 != null) {
            C01O0C2 = C01O0C(string2);
        }
        if (C01O0C2 == null && id != -1) {
            C01O0C2 = C01O0C(id);
        }
        if (C01O0C) {
            Log.v("FragmentManager", "onCreateView: id=0x" + Integer.toHexString(resourceId) + " fname=" + string + " existing=" + C01O0C2);
        }
        if (C01O0C2 == null) {
            Fragment C01O0C3 = Fragment.C01O0C(context, string);
            C01O0C3.C8CI00 = true;
            C01O0C3.Cl80C0l838l = resourceId != 0 ? resourceId : id;
            C01O0C3.ClC13lIl = id;
            C01O0C3.ClO80C3lOO8 = string2;
            C01O0C3.CC38COI1l3I = true;
            C01O0C3.CI0I8l333131 = this;
            C01O0C3.C01O0C(this.C831O13C118, attributeSet, C01O0C3.C11ll3);
            C01O0C(C01O0C3, true);
            fragment = C01O0C3;
        } else if (C01O0C2.CC38COI1l3I) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string2 + ", or parent id 0x" + Integer.toHexString(id) + " with another fragment for " + string);
        } else {
            C01O0C2.CC38COI1l3I = true;
            if (!C01O0C2.CO88CO1Cl383) {
                C01O0C2.C01O0C(this.C831O13C118, attributeSet, C01O0C2.C11ll3);
            }
            fragment = C01O0C2;
        }
        if (this.C3llC38O1 >= 1 || !fragment.C8CI00) {
            C0I1O3C3lI8(fragment);
        } else {
            C01O0C(fragment, 1, 0, 0, false);
        }
        if (fragment.I08O3C == null) {
            throw new IllegalStateException("Fragment " + string + " did not create a view.");
        }
        if (resourceId != 0) {
            fragment.I08O3C.setId(resourceId);
        }
        if (fragment.I08O3C.getTag() == null) {
            fragment.I08O3C.setTag(string2);
        }
        return fragment.I08O3C;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        if (this.CC38COI1l3I != null) {
            C11013l3.C01O0C(this.CC38COI1l3I, sb);
        } else {
            C11013l3.C01O0C(this.C831O13C118, sb);
        }
        sb.append("}}");
        return sb.toString();
    }
}
