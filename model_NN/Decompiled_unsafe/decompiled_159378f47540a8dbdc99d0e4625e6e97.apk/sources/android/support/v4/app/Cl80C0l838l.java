package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;

final class Cl80C0l838l implements Parcelable.Creator {
    Cl80C0l838l() {
    }

    /* renamed from: C01O0C */
    public FragmentState createFromParcel(Parcel parcel) {
        return new FragmentState(parcel);
    }

    /* renamed from: C01O0C */
    public FragmentState[] newArray(int i) {
        return new FragmentState[i];
    }
}
