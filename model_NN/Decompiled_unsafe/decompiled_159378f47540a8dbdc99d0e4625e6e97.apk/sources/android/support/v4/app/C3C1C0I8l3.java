package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;

final class C3C1C0I8l3 implements Parcelable.Creator {
    C3C1C0I8l3() {
    }

    /* renamed from: C01O0C */
    public BackStackState createFromParcel(Parcel parcel) {
        return new BackStackState(parcel);
    }

    /* renamed from: C01O0C */
    public BackStackState[] newArray(int i) {
        return new BackStackState[i];
    }
}
