package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.RemoteViews;
import java.util.ArrayList;

public class IC11OO80I3 {
    Context C01O0C;
    CharSequence C0I1O3C3lI8;
    CharSequence C101lC8O;
    PendingIntent C11013l3;
    PendingIntent C11ll3;
    RemoteViews C18Cl1C;
    CharSequence C1O10Cl038;
    int C1OC33O0lO81;
    Bitmap C1l00I1;
    int C3C1C0I8l3;
    boolean C3CIO118 = true;
    boolean C3ICl0OOl;
    Il08CO0Oll C3l3O8lIOIO8;
    CharSequence C3llC38O1;
    int C831O13C118;
    int C8CI00;
    boolean CC38COI1l3I;
    String CC8IOI1II0;
    boolean CCC3CC0l;
    String CI0I8l333131;
    ArrayList CI3C103l01O = new ArrayList();
    boolean CII3C813OIC8 = false;
    String CIOC8C;
    Notification CO081lO0OC0;
    Notification CO1830lI8C03 = new Notification();
    public ArrayList CO30CC1l0313;
    Bundle Cl80C0l838l;
    int ClC13lIl = 0;
    int ClO80C3lOO8 = 0;

    public IC11OO80I3(Context context) {
        this.C01O0C = context;
        this.CO1830lI8C03.when = System.currentTimeMillis();
        this.CO1830lI8C03.audioStreamType = -1;
        this.C3C1C0I8l3 = 0;
        this.CO30CC1l0313 = new ArrayList();
    }

    private void C01O0C(int i, boolean z) {
        if (z) {
            this.CO1830lI8C03.flags |= i;
            return;
        }
        this.CO1830lI8C03.flags &= i ^ -1;
    }

    protected static CharSequence C11013l3(CharSequence charSequence) {
        return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
    }

    public Notification C01O0C() {
        return I30OCIOO.C01O0C.C01O0C(this);
    }

    public IC11OO80I3 C01O0C(int i) {
        this.CO1830lI8C03.icon = i;
        return this;
    }

    public IC11OO80I3 C01O0C(long j) {
        this.CO1830lI8C03.when = j;
        return this;
    }

    public IC11OO80I3 C01O0C(PendingIntent pendingIntent) {
        this.C11013l3 = pendingIntent;
        return this;
    }

    public IC11OO80I3 C01O0C(Bitmap bitmap) {
        this.C1l00I1 = bitmap;
        return this;
    }

    public IC11OO80I3 C01O0C(CharSequence charSequence) {
        this.C0I1O3C3lI8 = C11013l3(charSequence);
        return this;
    }

    public IC11OO80I3 C01O0C(boolean z) {
        C01O0C(16, z);
        return this;
    }

    public IC11OO80I3 C0I1O3C3lI8(int i) {
        this.CO1830lI8C03.defaults = i;
        if ((i & 4) != 0) {
            this.CO1830lI8C03.flags |= 1;
        }
        return this;
    }

    public IC11OO80I3 C0I1O3C3lI8(CharSequence charSequence) {
        this.C101lC8O = C11013l3(charSequence);
        return this;
    }

    public IC11OO80I3 C101lC8O(CharSequence charSequence) {
        this.CO1830lI8C03.tickerText = C11013l3(charSequence);
        return this;
    }
}
