package android.support.v4.app;

import android.view.View;

class C3CIO118 implements C8CI00 {
    final /* synthetic */ Fragment C01O0C;

    C3CIO118(Fragment fragment) {
        this.C01O0C = fragment;
    }

    public View C01O0C(int i) {
        if (this.C01O0C.I08O3C != null) {
            return this.C01O0C.I08O3C.findViewById(i);
        }
        throw new IllegalStateException("Fragment does not have a view");
    }

    public boolean C01O0C() {
        return this.C01O0C.I08O3C != null;
    }
}
