package android.support.v4.app;

import android.os.Bundle;
import android.support.v4.a.C101lC8O;
import android.support.v4.a.C11013l3;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

final class I0l3Oll3 implements C11013l3 {
    final int C01O0C;
    final Bundle C0I1O3C3lI8;
    I0CIIIC C101lC8O;
    C101lC8O C11013l3;
    boolean C11ll3;
    boolean C18Cl1C;
    boolean C1O10Cl038;
    boolean C1OC33O0lO81;
    Object C1l00I1;
    boolean C3C1C0I8l3;
    boolean C3CIO118;
    boolean C3ICl0OOl;
    boolean C3l3O8lIOIO8;
    I0l3Oll3 C3llC38O1;
    final /* synthetic */ I0IC1O01888 C831O13C118;

    /* access modifiers changed from: package-private */
    public void C01O0C() {
        if (this.C1OC33O0lO81 && this.C3C1C0I8l3) {
            this.C1O10Cl038 = true;
        } else if (!this.C1O10Cl038) {
            this.C1O10Cl038 = true;
            if (I0IC1O01888.C01O0C) {
                Log.v("LoaderManager", "  Starting: " + this);
            }
            if (this.C11013l3 == null && this.C101lC8O != null) {
                this.C11013l3 = this.C101lC8O.C01O0C(this.C01O0C, this.C0I1O3C3lI8);
            }
            if (this.C11013l3 == null) {
                return;
            }
            if (!this.C11013l3.getClass().isMemberClass() || Modifier.isStatic(this.C11013l3.getClass().getModifiers())) {
                if (!this.C3l3O8lIOIO8) {
                    this.C11013l3.C01O0C(this.C01O0C, this);
                    this.C3l3O8lIOIO8 = true;
                }
                this.C11013l3.C01O0C();
                return;
            }
            throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + this.C11013l3);
        }
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(C101lC8O c101lC8O, Object obj) {
        String str;
        if (this.C101lC8O != null) {
            if (this.C831O13C118.C11ll3 != null) {
                String str2 = this.C831O13C118.C11ll3.C0I1O3C3lI8.CI3C103l01O;
                this.C831O13C118.C11ll3.C0I1O3C3lI8.CI3C103l01O = "onLoadFinished";
                str = str2;
            } else {
                str = null;
            }
            try {
                if (I0IC1O01888.C01O0C) {
                    Log.v("LoaderManager", "  onLoadFinished in " + c101lC8O + ": " + c101lC8O.C01O0C(obj));
                }
                this.C101lC8O.C01O0C(c101lC8O, obj);
                this.C18Cl1C = true;
            } finally {
                if (this.C831O13C118.C11ll3 != null) {
                    this.C831O13C118.C11ll3.C0I1O3C3lI8.CI3C103l01O = str;
                }
            }
        }
    }

    public void C01O0C(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.C01O0C);
        printWriter.print(" mArgs=");
        printWriter.println(this.C0I1O3C3lI8);
        printWriter.print(str);
        printWriter.print("mCallbacks=");
        printWriter.println(this.C101lC8O);
        printWriter.print(str);
        printWriter.print("mLoader=");
        printWriter.println(this.C11013l3);
        if (this.C11013l3 != null) {
            this.C11013l3.C01O0C(str + "  ", fileDescriptor, printWriter, strArr);
        }
        if (this.C11ll3 || this.C18Cl1C) {
            printWriter.print(str);
            printWriter.print("mHaveData=");
            printWriter.print(this.C11ll3);
            printWriter.print("  mDeliveredData=");
            printWriter.println(this.C18Cl1C);
            printWriter.print(str);
            printWriter.print("mData=");
            printWriter.println(this.C1l00I1);
        }
        printWriter.print(str);
        printWriter.print("mStarted=");
        printWriter.print(this.C1O10Cl038);
        printWriter.print(" mReportNextStart=");
        printWriter.print(this.C3CIO118);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.C3ICl0OOl);
        printWriter.print(str);
        printWriter.print("mRetaining=");
        printWriter.print(this.C1OC33O0lO81);
        printWriter.print(" mRetainingStarted=");
        printWriter.print(this.C3C1C0I8l3);
        printWriter.print(" mListenerRegistered=");
        printWriter.println(this.C3l3O8lIOIO8);
        if (this.C3llC38O1 != null) {
            printWriter.print(str);
            printWriter.println("Pending Loader ");
            printWriter.print(this.C3llC38O1);
            printWriter.println(":");
            this.C3llC38O1.C01O0C(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }

    /* access modifiers changed from: package-private */
    public void C0I1O3C3lI8() {
        if (I0IC1O01888.C01O0C) {
            Log.v("LoaderManager", "  Retaining: " + this);
        }
        this.C1OC33O0lO81 = true;
        this.C3C1C0I8l3 = this.C1O10Cl038;
        this.C1O10Cl038 = false;
        this.C101lC8O = null;
    }

    /* access modifiers changed from: package-private */
    public void C101lC8O() {
        if (this.C1OC33O0lO81) {
            if (I0IC1O01888.C01O0C) {
                Log.v("LoaderManager", "  Finished Retaining: " + this);
            }
            this.C1OC33O0lO81 = false;
            if (this.C1O10Cl038 != this.C3C1C0I8l3 && !this.C1O10Cl038) {
                C11ll3();
            }
        }
        if (this.C1O10Cl038 && this.C11ll3 && !this.C3CIO118) {
            C01O0C(this.C11013l3, this.C1l00I1);
        }
    }

    /* access modifiers changed from: package-private */
    public void C11013l3() {
        if (this.C1O10Cl038 && this.C3CIO118) {
            this.C3CIO118 = false;
            if (this.C11ll3) {
                C01O0C(this.C11013l3, this.C1l00I1);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void C11ll3() {
        if (I0IC1O01888.C01O0C) {
            Log.v("LoaderManager", "  Stopping: " + this);
        }
        this.C1O10Cl038 = false;
        if (!this.C1OC33O0lO81 && this.C11013l3 != null && this.C3l3O8lIOIO8) {
            this.C3l3O8lIOIO8 = false;
            this.C11013l3.C01O0C((C11013l3) this);
            this.C11013l3.C101lC8O();
        }
    }

    /* access modifiers changed from: package-private */
    public void C18Cl1C() {
        String str;
        if (I0IC1O01888.C01O0C) {
            Log.v("LoaderManager", "  Destroying: " + this);
        }
        this.C3ICl0OOl = true;
        boolean z = this.C18Cl1C;
        this.C18Cl1C = false;
        if (this.C101lC8O != null && this.C11013l3 != null && this.C11ll3 && z) {
            if (I0IC1O01888.C01O0C) {
                Log.v("LoaderManager", "  Reseting: " + this);
            }
            if (this.C831O13C118.C11ll3 != null) {
                String str2 = this.C831O13C118.C11ll3.C0I1O3C3lI8.CI3C103l01O;
                this.C831O13C118.C11ll3.C0I1O3C3lI8.CI3C103l01O = "onLoaderReset";
                str = str2;
            } else {
                str = null;
            }
            try {
                this.C101lC8O.C01O0C(this.C11013l3);
            } finally {
                if (this.C831O13C118.C11ll3 != null) {
                    this.C831O13C118.C11ll3.C0I1O3C3lI8.CI3C103l01O = str;
                }
            }
        }
        this.C101lC8O = null;
        this.C1l00I1 = null;
        this.C11ll3 = false;
        if (this.C11013l3 != null) {
            if (this.C3l3O8lIOIO8) {
                this.C3l3O8lIOIO8 = false;
                this.C11013l3.C01O0C((C11013l3) this);
            }
            this.C11013l3.C11ll3();
        }
        if (this.C3llC38O1 != null) {
            this.C3llC38O1.C18Cl1C();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        sb.append("LoaderInfo{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" #");
        sb.append(this.C01O0C);
        sb.append(" : ");
        android.support.v4.c.C11013l3.C01O0C(this.C11013l3, sb);
        sb.append("}}");
        return sb.toString();
    }
}
