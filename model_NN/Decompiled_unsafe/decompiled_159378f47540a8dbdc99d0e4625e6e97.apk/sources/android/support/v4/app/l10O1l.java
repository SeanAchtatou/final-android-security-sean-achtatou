package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.SparseArray;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.List;

public class l10O1l implements I1CO03, I1I11O81II {
    private Notification.Builder C01O0C;
    private Bundle C0I1O3C3lI8;
    private List C101lC8O = new ArrayList();

    public l10O1l(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, boolean z3, int i4, CharSequence charSequence4, boolean z4, ArrayList arrayList, Bundle bundle, String str, boolean z5, String str2) {
        this.C01O0C = new Notification.Builder(context).setWhen(notification.when).setShowWhen(z2).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, remoteViews).setSound(notification.sound, notification.audioStreamType).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(pendingIntent2, (notification.flags & 128) != 0).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z3).setPriority(i4).setProgress(i2, i3, z);
        this.C0I1O3C3lI8 = new Bundle();
        if (bundle != null) {
            this.C0I1O3C3lI8.putAll(bundle);
        }
        if (arrayList != null && !arrayList.isEmpty()) {
            this.C0I1O3C3lI8.putStringArray("android.people", (String[]) arrayList.toArray(new String[arrayList.size()]));
        }
        if (z4) {
            this.C0I1O3C3lI8.putBoolean("android.support.localOnly", true);
        }
        if (str != null) {
            this.C0I1O3C3lI8.putString("android.support.groupKey", str);
            if (z5) {
                this.C0I1O3C3lI8.putBoolean("android.support.isGroupSummary", true);
            } else {
                this.C0I1O3C3lI8.putBoolean("android.support.useSideChannel", true);
            }
        }
        if (str2 != null) {
            this.C0I1O3C3lI8.putString("android.support.sortKey", str2);
        }
    }

    public Notification.Builder C01O0C() {
        return this.C01O0C;
    }

    public void C01O0C(IO1C1CO13l iO1C1CO13l) {
        this.C101lC8O.add(l03l300018l.C01O0C(this.C01O0C, iO1C1CO13l));
    }

    public Notification C0I1O3C3lI8() {
        SparseArray C01O0C2 = l03l300018l.C01O0C(this.C101lC8O);
        if (C01O0C2 != null) {
            this.C0I1O3C3lI8.putSparseParcelableArray("android.support.actionExtras", C01O0C2);
        }
        this.C01O0C.setExtras(this.C0I1O3C3lI8);
        return this.C01O0C.build();
    }
}
