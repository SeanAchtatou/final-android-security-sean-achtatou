package android.support.v4.app;

import android.view.animation.Animation;

class CI3C103l01O implements Animation.AnimationListener {
    final /* synthetic */ Fragment C01O0C;
    final /* synthetic */ CCC3CC0l C0I1O3C3lI8;

    CI3C103l01O(CCC3CC0l cCC3CC0l, Fragment fragment) {
        this.C0I1O3C3lI8 = cCC3CC0l;
        this.C01O0C = fragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void onAnimationEnd(Animation animation) {
        if (this.C01O0C.C101lC8O != null) {
            this.C01O0C.C101lC8O = null;
            this.C0I1O3C3lI8.C01O0C(this.C01O0C, this.C01O0C.C11013l3, 0, 0, false);
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
