package android.support.v4.app;

import android.app.Notification;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class l03l300018l {
    private static final Object C01O0C = new Object();
    private static Field C0I1O3C3lI8;
    private static boolean C101lC8O;
    private static final Object C11013l3 = new Object();

    public static Bundle C01O0C(Notification.Builder builder, IO1C1CO13l iO1C1CO13l) {
        builder.addAction(iO1C1CO13l.C01O0C(), iO1C1CO13l.C0I1O3C3lI8(), iO1C1CO13l.C101lC8O());
        Bundle bundle = new Bundle(iO1C1CO13l.C11013l3());
        if (iO1C1CO13l.C18Cl1C() != null) {
            bundle.putParcelableArray("android.support.remoteInputs", l3OOl31I0.C01O0C(iO1C1CO13l.C18Cl1C()));
        }
        return bundle;
    }

    public static Bundle C01O0C(Notification notification) {
        synchronized (C01O0C) {
            if (C101lC8O) {
                return null;
            }
            try {
                if (C0I1O3C3lI8 == null) {
                    Field declaredField = Notification.class.getDeclaredField("extras");
                    if (!Bundle.class.isAssignableFrom(declaredField.getType())) {
                        Log.e("NotificationCompat", "Notification.extras field is not of type Bundle");
                        C101lC8O = true;
                        return null;
                    }
                    declaredField.setAccessible(true);
                    C0I1O3C3lI8 = declaredField;
                }
                Bundle bundle = (Bundle) C0I1O3C3lI8.get(notification);
                if (bundle == null) {
                    bundle = new Bundle();
                    C0I1O3C3lI8.set(notification, bundle);
                }
                return bundle;
            } catch (IllegalAccessException e) {
                Log.e("NotificationCompat", "Unable to access notification extras", e);
                C101lC8O = true;
                return null;
            } catch (NoSuchFieldException e2) {
                Log.e("NotificationCompat", "Unable to access notification extras", e2);
                C101lC8O = true;
                return null;
            }
        }
    }

    public static SparseArray C01O0C(List list) {
        SparseArray sparseArray = null;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Bundle bundle = (Bundle) list.get(i);
            if (bundle != null) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray();
                }
                sparseArray.put(i, bundle);
            }
        }
        return sparseArray;
    }

    public static void C01O0C(I1I11O81II i1i11o81ii, CharSequence charSequence, boolean z, CharSequence charSequence2, Bitmap bitmap, Bitmap bitmap2, boolean z2) {
        Notification.BigPictureStyle bigPicture = new Notification.BigPictureStyle(i1i11o81ii.C01O0C()).setBigContentTitle(charSequence).bigPicture(bitmap);
        if (z2) {
            bigPicture.bigLargeIcon(bitmap2);
        }
        if (z) {
            bigPicture.setSummaryText(charSequence2);
        }
    }

    public static void C01O0C(I1I11O81II i1i11o81ii, CharSequence charSequence, boolean z, CharSequence charSequence2, CharSequence charSequence3) {
        Notification.BigTextStyle bigText = new Notification.BigTextStyle(i1i11o81ii.C01O0C()).setBigContentTitle(charSequence).bigText(charSequence3);
        if (z) {
            bigText.setSummaryText(charSequence2);
        }
    }

    public static void C01O0C(I1I11O81II i1i11o81ii, CharSequence charSequence, boolean z, CharSequence charSequence2, ArrayList arrayList) {
        Notification.InboxStyle bigContentTitle = new Notification.InboxStyle(i1i11o81ii.C01O0C()).setBigContentTitle(charSequence);
        if (z) {
            bigContentTitle.setSummaryText(charSequence2);
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            bigContentTitle.addLine((CharSequence) it.next());
        }
    }
}
