package android.support.v4.app;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

final class FragmentState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Cl80C0l838l();
    final String C01O0C;
    final int C0I1O3C3lI8;
    final boolean C101lC8O;
    final int C11013l3;
    final int C11ll3;
    final String C18Cl1C;
    final boolean C1O10Cl038;
    final Bundle C1OC33O0lO81;
    final boolean C1l00I1;
    Bundle C3C1C0I8l3;
    Fragment C3CIO118;

    public FragmentState(Parcel parcel) {
        boolean z = true;
        this.C01O0C = parcel.readString();
        this.C0I1O3C3lI8 = parcel.readInt();
        this.C101lC8O = parcel.readInt() != 0;
        this.C11013l3 = parcel.readInt();
        this.C11ll3 = parcel.readInt();
        this.C18Cl1C = parcel.readString();
        this.C1l00I1 = parcel.readInt() != 0;
        this.C1O10Cl038 = parcel.readInt() == 0 ? false : z;
        this.C1OC33O0lO81 = parcel.readBundle();
        this.C3C1C0I8l3 = parcel.readBundle();
    }

    public FragmentState(Fragment fragment) {
        this.C01O0C = fragment.getClass().getName();
        this.C0I1O3C3lI8 = fragment.C1l00I1;
        this.C101lC8O = fragment.C8CI00;
        this.C11013l3 = fragment.Cl80C0l838l;
        this.C11ll3 = fragment.ClC13lIl;
        this.C18Cl1C = fragment.ClO80C3lOO8;
        this.C1l00I1 = fragment.CO30CC1l0313;
        this.C1O10Cl038 = fragment.CO1830lI8C03;
        this.C1OC33O0lO81 = fragment.C1OC33O0lO81;
    }

    public Fragment C01O0C(C3llC38O1 c3llC38O1, Fragment fragment) {
        if (this.C3CIO118 != null) {
            return this.C3CIO118;
        }
        if (this.C1OC33O0lO81 != null) {
            this.C1OC33O0lO81.setClassLoader(c3llC38O1.getClassLoader());
        }
        this.C3CIO118 = Fragment.C01O0C(c3llC38O1, this.C01O0C, this.C1OC33O0lO81);
        if (this.C3C1C0I8l3 != null) {
            this.C3C1C0I8l3.setClassLoader(c3llC38O1.getClassLoader());
            this.C3CIO118.C11ll3 = this.C3C1C0I8l3;
        }
        this.C3CIO118.C01O0C(this.C0I1O3C3lI8, fragment);
        this.C3CIO118.C8CI00 = this.C101lC8O;
        this.C3CIO118.CC8IOI1II0 = true;
        this.C3CIO118.Cl80C0l838l = this.C11013l3;
        this.C3CIO118.ClC13lIl = this.C11ll3;
        this.C3CIO118.ClO80C3lOO8 = this.C18Cl1C;
        this.C3CIO118.CO30CC1l0313 = this.C1l00I1;
        this.C3CIO118.CO1830lI8C03 = this.C1O10Cl038;
        this.C3CIO118.CI0I8l333131 = c3llC38O1.C0I1O3C3lI8;
        if (CCC3CC0l.C01O0C) {
            Log.v("FragmentManager", "Instantiated fragment " + this.C3CIO118);
        }
        return this.C3CIO118;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        parcel.writeString(this.C01O0C);
        parcel.writeInt(this.C0I1O3C3lI8);
        parcel.writeInt(this.C101lC8O ? 1 : 0);
        parcel.writeInt(this.C11013l3);
        parcel.writeInt(this.C11ll3);
        parcel.writeString(this.C18Cl1C);
        parcel.writeInt(this.C1l00I1 ? 1 : 0);
        if (!this.C1O10Cl038) {
            i2 = 0;
        }
        parcel.writeInt(i2);
        parcel.writeBundle(this.C1OC33O0lO81);
        parcel.writeBundle(this.C3C1C0I8l3);
    }
}
