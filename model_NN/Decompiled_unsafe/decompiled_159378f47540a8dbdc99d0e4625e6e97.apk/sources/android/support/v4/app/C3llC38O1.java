package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.c.C3ICl0OOl;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

public class C3llC38O1 extends Activity {
    final Handler C01O0C;
    final CCC3CC0l C0I1O3C3lI8;
    final C8CI00 C101lC8O;
    boolean C11013l3;
    boolean C11ll3;
    boolean C18Cl1C;
    boolean C1O10Cl038;
    boolean C1OC33O0lO81;
    boolean C1l00I1;
    boolean C3C1C0I8l3;
    boolean C3CIO118;
    C3ICl0OOl C3ICl0OOl;
    I0IC1O01888 C3l3O8lIOIO8;

    private static String C01O0C(View view) {
        String str;
        char c = 'F';
        char c2 = '.';
        StringBuilder sb = new StringBuilder(128);
        sb.append(view.getClass().getName());
        sb.append('{');
        sb.append(Integer.toHexString(System.identityHashCode(view)));
        sb.append(' ');
        switch (view.getVisibility()) {
            case 0:
                sb.append('V');
                break;
            case 4:
                sb.append('I');
                break;
            case 8:
                sb.append('G');
                break;
            default:
                sb.append('.');
                break;
        }
        sb.append(view.isFocusable() ? 'F' : '.');
        sb.append(view.isEnabled() ? 'E' : '.');
        sb.append(view.willNotDraw() ? '.' : 'D');
        sb.append(view.isHorizontalScrollBarEnabled() ? 'H' : '.');
        sb.append(view.isVerticalScrollBarEnabled() ? 'V' : '.');
        sb.append(view.isClickable() ? 'C' : '.');
        sb.append(view.isLongClickable() ? 'L' : '.');
        sb.append(' ');
        if (!view.isFocused()) {
            c = '.';
        }
        sb.append(c);
        sb.append(view.isSelected() ? 'S' : '.');
        if (view.isPressed()) {
            c2 = 'P';
        }
        sb.append(c2);
        sb.append(' ');
        sb.append(view.getLeft());
        sb.append(',');
        sb.append(view.getTop());
        sb.append('-');
        sb.append(view.getRight());
        sb.append(',');
        sb.append(view.getBottom());
        int id = view.getId();
        if (id != -1) {
            sb.append(" #");
            sb.append(Integer.toHexString(id));
            Resources resources = view.getResources();
            if (!(id == 0 || resources == null)) {
                switch (-16777216 & id) {
                    case 16777216:
                        str = "android";
                        String resourceTypeName = resources.getResourceTypeName(id);
                        String resourceEntryName = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName);
                        sb.append("/");
                        sb.append(resourceEntryName);
                        break;
                    case 2130706432:
                        str = "app";
                        String resourceTypeName2 = resources.getResourceTypeName(id);
                        String resourceEntryName2 = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName2);
                        sb.append("/");
                        sb.append(resourceEntryName2);
                        break;
                    default:
                        try {
                            str = resources.getResourcePackageName(id);
                            String resourceTypeName22 = resources.getResourceTypeName(id);
                            String resourceEntryName22 = resources.getResourceEntryName(id);
                            sb.append(" ");
                            sb.append(str);
                            sb.append(":");
                            sb.append(resourceTypeName22);
                            sb.append("/");
                            sb.append(resourceEntryName22);
                            break;
                        } catch (Resources.NotFoundException e) {
                            break;
                        }
                }
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private void C01O0C(String str, PrintWriter printWriter, View view) {
        ViewGroup viewGroup;
        int childCount;
        printWriter.print(str);
        if (view == null) {
            printWriter.println("null");
            return;
        }
        printWriter.println(C01O0C(view));
        if ((view instanceof ViewGroup) && (childCount = (viewGroup = (ViewGroup) view).getChildCount()) > 0) {
            String str2 = str + "  ";
            for (int i = 0; i < childCount; i++) {
                C01O0C(str2, printWriter, viewGroup.getChildAt(i));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public I0IC1O01888 C01O0C(String str, boolean z, boolean z2) {
        if (this.C3ICl0OOl == null) {
            this.C3ICl0OOl = new C3ICl0OOl();
        }
        I0IC1O01888 i0ic1o01888 = (I0IC1O01888) this.C3ICl0OOl.get(str);
        if (i0ic1o01888 != null) {
            i0ic1o01888.C01O0C(this);
            return i0ic1o01888;
        } else if (!z2) {
            return i0ic1o01888;
        } else {
            I0IC1O01888 i0ic1o018882 = new I0IC1O01888(str, this, z);
            this.C3ICl0OOl.put(str, i0ic1o018882);
            return i0ic1o018882;
        }
    }

    public void C01O0C() {
        C01O0C.C01O0C(this);
    }

    public void C01O0C(Fragment fragment) {
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(String str) {
        I0IC1O01888 i0ic1o01888;
        if (this.C3ICl0OOl != null && (i0ic1o01888 = (I0IC1O01888) this.C3ICl0OOl.get(str)) != null && !i0ic1o01888.C1l00I1) {
            i0ic1o01888.C1O10Cl038();
            this.C3ICl0OOl.remove(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(boolean z) {
        if (!this.C1l00I1) {
            this.C1l00I1 = true;
            this.C1O10Cl038 = z;
            this.C01O0C.removeMessages(1);
            C11ll3();
        }
    }

    /* access modifiers changed from: protected */
    public boolean C01O0C(View view, Menu menu) {
        return super.onPreparePanel(0, view, menu);
    }

    /* access modifiers changed from: protected */
    public void C0I1O3C3lI8() {
        this.C0I1O3C3lI8.C3l3O8lIOIO8();
    }

    public Object C101lC8O() {
        return null;
    }

    public void C11013l3() {
        if (Build.VERSION.SDK_INT >= 11) {
            C101lC8O.C01O0C(this);
        } else {
            this.C1OC33O0lO81 = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void C11ll3() {
        if (this.C3CIO118) {
            this.C3CIO118 = false;
            if (this.C3l3O8lIOIO8 != null) {
                if (!this.C1O10Cl038) {
                    this.C3l3O8lIOIO8.C101lC8O();
                } else {
                    this.C3l3O8lIOIO8.C11013l3();
                }
            }
        }
        this.C0I1O3C3lI8.C8CI00();
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (Build.VERSION.SDK_INT >= 11) {
        }
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        String str2 = str + "  ";
        printWriter.print(str2);
        printWriter.print("mCreated=");
        printWriter.print(this.C11013l3);
        printWriter.print("mResumed=");
        printWriter.print(this.C11ll3);
        printWriter.print(" mStopped=");
        printWriter.print(this.C18Cl1C);
        printWriter.print(" mReallyStopped=");
        printWriter.println(this.C1l00I1);
        printWriter.print(str2);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.C3CIO118);
        if (this.C3l3O8lIOIO8 != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.C3l3O8lIOIO8)));
            printWriter.println(":");
            this.C3l3O8lIOIO8.C01O0C(str + "  ", fileDescriptor, printWriter, strArr);
        }
        this.C0I1O3C3lI8.C01O0C(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.println("View Hierarchy:");
        C01O0C(str + "  ", printWriter, getWindow().getDecorView());
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        this.C0I1O3C3lI8.C1OC33O0lO81();
        int i3 = i >> 16;
        if (i3 != 0) {
            int i4 = i3 - 1;
            if (this.C0I1O3C3lI8.C18Cl1C == null || i4 < 0 || i4 >= this.C0I1O3C3lI8.C18Cl1C.size()) {
                Log.w("FragmentActivity", "Activity result fragment index out of range: 0x" + Integer.toHexString(i));
                return;
            }
            Fragment fragment = (Fragment) this.C0I1O3C3lI8.C18Cl1C.get(i4);
            if (fragment == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for index: 0x" + Integer.toHexString(i));
            } else {
                fragment.C01O0C(65535 & i, i2, intent);
            }
        } else {
            super.onActivityResult(i, i2, intent);
        }
    }

    public void onBackPressed() {
        if (!this.C0I1O3C3lI8.C101lC8O()) {
            C01O0C();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.C0I1O3C3lI8.C01O0C(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        this.C0I1O3C3lI8.C01O0C(this, this.C101lC8O, (Fragment) null);
        if (getLayoutInflater().getFactory() == null) {
            getLayoutInflater().setFactory(this);
        }
        super.onCreate(bundle);
        C831O13C118 c831o13c118 = (C831O13C118) getLastNonConfigurationInstance();
        if (c831o13c118 != null) {
            this.C3ICl0OOl = c831o13c118.C11ll3;
        }
        if (bundle != null) {
            this.C0I1O3C3lI8.C01O0C(bundle.getParcelable("android:support:fragments"), c831o13c118 != null ? c831o13c118.C11013l3 : null);
        }
        this.C0I1O3C3lI8.C3C1C0I8l3();
    }

    public boolean onCreatePanelMenu(int i, Menu menu) {
        if (i != 0) {
            return super.onCreatePanelMenu(i, menu);
        }
        boolean onCreatePanelMenu = super.onCreatePanelMenu(i, menu) | this.C0I1O3C3lI8.C01O0C(menu, getMenuInflater());
        if (Build.VERSION.SDK_INT >= 11) {
            return onCreatePanelMenu;
        }
        return true;
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        if (!"fragment".equals(str)) {
            return super.onCreateView(str, context, attributeSet);
        }
        View onCreateView = this.C0I1O3C3lI8.onCreateView(str, context, attributeSet);
        return onCreateView == null ? super.onCreateView(str, context, attributeSet) : onCreateView;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        C01O0C(false);
        this.C0I1O3C3lI8.CC8IOI1II0();
        if (this.C3l3O8lIOIO8 != null) {
            this.C3l3O8lIOIO8.C1O10Cl038();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 5 || i != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i, keyEvent);
        }
        onBackPressed();
        return true;
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.C0I1O3C3lI8.CCC3CC0l();
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (super.onMenuItemSelected(i, menuItem)) {
            return true;
        }
        switch (i) {
            case 0:
                return this.C0I1O3C3lI8.C01O0C(menuItem);
            case 6:
                return this.C0I1O3C3lI8.C0I1O3C3lI8(menuItem);
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.C0I1O3C3lI8.C1OC33O0lO81();
    }

    public void onPanelClosed(int i, Menu menu) {
        switch (i) {
            case 0:
                this.C0I1O3C3lI8.C0I1O3C3lI8(menu);
                break;
        }
        super.onPanelClosed(i, menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.C11ll3 = false;
        if (this.C01O0C.hasMessages(2)) {
            this.C01O0C.removeMessages(2);
            C0I1O3C3lI8();
        }
        this.C0I1O3C3lI8.C3llC38O1();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.C01O0C.removeMessages(2);
        C0I1O3C3lI8();
        this.C0I1O3C3lI8.C11ll3();
    }

    public boolean onPreparePanel(int i, View view, Menu menu) {
        if (i != 0 || menu == null) {
            return super.onPreparePanel(i, view, menu);
        }
        if (this.C1OC33O0lO81) {
            this.C1OC33O0lO81 = false;
            menu.clear();
            onCreatePanelMenu(i, menu);
        }
        return C01O0C(view, menu) | this.C0I1O3C3lI8.C01O0C(menu);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.C01O0C.sendEmptyMessage(2);
        this.C11ll3 = true;
        this.C0I1O3C3lI8.C11ll3();
    }

    public final Object onRetainNonConfigurationInstance() {
        boolean z;
        if (this.C18Cl1C) {
            C01O0C(true);
        }
        Object C101lC8O2 = C101lC8O();
        ArrayList C1l00I12 = this.C0I1O3C3lI8.C1l00I1();
        if (this.C3ICl0OOl != null) {
            int size = this.C3ICl0OOl.size();
            I0IC1O01888[] i0ic1o01888Arr = new I0IC1O01888[size];
            for (int i = size - 1; i >= 0; i--) {
                i0ic1o01888Arr[i] = (I0IC1O01888) this.C3ICl0OOl.C101lC8O(i);
            }
            z = false;
            for (int i2 = 0; i2 < size; i2++) {
                I0IC1O01888 i0ic1o01888 = i0ic1o01888Arr[i2];
                if (i0ic1o01888.C1l00I1) {
                    z = true;
                } else {
                    i0ic1o01888.C1O10Cl038();
                    this.C3ICl0OOl.remove(i0ic1o01888.C11013l3);
                }
            }
        } else {
            z = false;
        }
        if (C1l00I12 == null && !z && C101lC8O2 == null) {
            return null;
        }
        C831O13C118 c831o13c118 = new C831O13C118();
        c831o13c118.C01O0C = null;
        c831o13c118.C0I1O3C3lI8 = C101lC8O2;
        c831o13c118.C101lC8O = null;
        c831o13c118.C11013l3 = C1l00I12;
        c831o13c118.C11ll3 = this.C3ICl0OOl;
        return c831o13c118;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Parcelable C1O10Cl0382 = this.C0I1O3C3lI8.C1O10Cl038();
        if (C1O10Cl0382 != null) {
            bundle.putParcelable("android:support:fragments", C1O10Cl0382);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, boolean, boolean):android.support.v4.app.I0IC1O01888
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.C3llC38O1.C01O0C(java.lang.String, boolean, boolean):android.support.v4.app.I0IC1O01888 */
    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.C18Cl1C = false;
        this.C1l00I1 = false;
        this.C01O0C.removeMessages(1);
        if (!this.C11013l3) {
            this.C11013l3 = true;
            this.C0I1O3C3lI8.C3CIO118();
        }
        this.C0I1O3C3lI8.C1OC33O0lO81();
        this.C0I1O3C3lI8.C11ll3();
        if (!this.C3CIO118) {
            this.C3CIO118 = true;
            if (this.C3l3O8lIOIO8 != null) {
                this.C3l3O8lIOIO8.C0I1O3C3lI8();
            } else if (!this.C3C1C0I8l3) {
                this.C3l3O8lIOIO8 = C01O0C("(root)", this.C3CIO118, false);
                if (this.C3l3O8lIOIO8 != null && !this.C3l3O8lIOIO8.C18Cl1C) {
                    this.C3l3O8lIOIO8.C0I1O3C3lI8();
                }
            }
            this.C3C1C0I8l3 = true;
        }
        this.C0I1O3C3lI8.C3ICl0OOl();
        if (this.C3ICl0OOl != null) {
            int size = this.C3ICl0OOl.size();
            I0IC1O01888[] i0ic1o01888Arr = new I0IC1O01888[size];
            for (int i = size - 1; i >= 0; i--) {
                i0ic1o01888Arr[i] = (I0IC1O01888) this.C3ICl0OOl.C101lC8O(i);
            }
            for (int i2 = 0; i2 < size; i2++) {
                I0IC1O01888 i0ic1o01888 = i0ic1o01888Arr[i2];
                i0ic1o01888.C11ll3();
                i0ic1o01888.C1l00I1();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.C18Cl1C = true;
        this.C01O0C.sendEmptyMessage(1);
        this.C0I1O3C3lI8.C831O13C118();
    }

    public void startActivityForResult(Intent intent, int i) {
        if (i == -1 || (-65536 & i) == 0) {
            super.startActivityForResult(intent, i);
            return;
        }
        throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
    }
}
