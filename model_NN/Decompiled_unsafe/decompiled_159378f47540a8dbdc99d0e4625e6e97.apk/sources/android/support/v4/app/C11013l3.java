package android.support.v4.app;

import android.os.Build;
import android.support.v4.c.C01O0C;
import android.support.v4.c.C11ll3;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

final class C11013l3 extends CO1830lI8C03 implements Runnable {
    final CCC3CC0l C01O0C;
    C1O10Cl038 C0I1O3C3lI8;
    C1O10Cl038 C101lC8O;
    int C11013l3;
    int C11ll3;
    int C18Cl1C;
    int C1O10Cl038;
    int C1OC33O0lO81;
    int C1l00I1;
    int C3C1C0I8l3;
    boolean C3CIO118;
    boolean C3ICl0OOl = true;
    String C3l3O8lIOIO8;
    boolean C3llC38O1;
    int C831O13C118 = -1;
    int C8CI00;
    CharSequence CC38COI1l3I;
    int CC8IOI1II0;
    CharSequence CCC3CC0l;
    ArrayList CI0I8l333131;
    ArrayList CI3C103l01O;

    public C11013l3(CCC3CC0l cCC3CC0l) {
        this.C01O0C = cCC3CC0l;
    }

    private C1OC33O0lO81 C01O0C(SparseArray sparseArray, SparseArray sparseArray2, boolean z) {
        C1OC33O0lO81 c1OC33O0lO81 = new C1OC33O0lO81(this);
        c1OC33O0lO81.C11013l3 = new View(this.C01O0C.C831O13C118);
        int i = 0;
        boolean z2 = false;
        while (i < sparseArray.size()) {
            boolean z3 = C01O0C(sparseArray.keyAt(i), c1OC33O0lO81, z, sparseArray, sparseArray2) ? true : z2;
            i++;
            z2 = z3;
        }
        for (int i2 = 0; i2 < sparseArray2.size(); i2++) {
            int keyAt = sparseArray2.keyAt(i2);
            if (sparseArray.get(keyAt) == null && C01O0C(keyAt, c1OC33O0lO81, z, sparseArray, sparseArray2)) {
                z2 = true;
            }
        }
        if (!z2) {
            return null;
        }
        return c1OC33O0lO81;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CO30CC1l0313.C01O0C(java.util.Map, android.view.View):void
     arg types: [android.support.v4.c.C01O0C, android.view.View]
     candidates:
      android.support.v4.app.CO30CC1l0313.C01O0C(android.transition.Transition, android.support.v4.app.I03lII1):void
      android.support.v4.app.CO30CC1l0313.C01O0C(android.view.ViewGroup, java.lang.Object):void
      android.support.v4.app.CO30CC1l0313.C01O0C(java.lang.Object, android.view.View):void
      android.support.v4.app.CO30CC1l0313.C01O0C(java.lang.Object, java.util.ArrayList):void
      android.support.v4.app.CO30CC1l0313.C01O0C(java.util.ArrayList, android.view.View):void
      android.support.v4.app.CO30CC1l0313.C01O0C(java.util.Map, android.view.View):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void
     arg types: [android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, int]
     candidates:
      android.support.v4.app.C11013l3.C01O0C(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.C1OC33O0lO81
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.app.Fragment, boolean):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, boolean, android.support.v4.app.Fragment):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(java.util.ArrayList, java.util.ArrayList, android.support.v4.c.C01O0C):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C11013l3, android.support.v4.c.C01O0C, android.support.v4.app.C1OC33O0lO81):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, int, java.lang.Object):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.c.C01O0C, java.lang.String, java.lang.String):void
      android.support.v4.app.C11013l3.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.C11013l3.C01O0C(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.CO1830lI8C03.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C11013l3.C0I1O3C3lI8(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void
     arg types: [android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, int]
     candidates:
      android.support.v4.app.C11013l3.C0I1O3C3lI8(android.support.v4.app.C1OC33O0lO81, android.support.v4.app.Fragment, boolean):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C0I1O3C3lI8(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void */
    private C01O0C C01O0C(C1OC33O0lO81 c1OC33O0lO81, Fragment fragment, boolean z) {
        C01O0C c01o0c = new C01O0C();
        if (this.CI0I8l333131 != null) {
            CO30CC1l0313.C01O0C((Map) c01o0c, fragment.C1O10Cl038());
            if (z) {
                c01o0c.C01O0C((Collection) this.CI3C103l01O);
            } else {
                c01o0c = C01O0C(this.CI0I8l333131, this.CI3C103l01O, c01o0c);
            }
        }
        if (z) {
            if (fragment.II083CII != null) {
                fragment.II083CII.C01O0C(this.CI3C103l01O, c01o0c);
            }
            C01O0C(c1OC33O0lO81, c01o0c, false);
        } else {
            if (fragment.II0ll1CC13l != null) {
                fragment.II0ll1CC13l.C01O0C(this.CI3C103l01O, c01o0c);
            }
            C0I1O3C3lI8(c1OC33O0lO81, c01o0c, false);
        }
        return c01o0c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void
     arg types: [android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, int]
     candidates:
      android.support.v4.app.C11013l3.C01O0C(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.C1OC33O0lO81
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.app.Fragment, boolean):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, boolean, android.support.v4.app.Fragment):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(java.util.ArrayList, java.util.ArrayList, android.support.v4.c.C01O0C):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C11013l3, android.support.v4.c.C01O0C, android.support.v4.app.C1OC33O0lO81):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, int, java.lang.Object):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.c.C01O0C, java.lang.String, java.lang.String):void
      android.support.v4.app.C11013l3.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.C11013l3.C01O0C(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.CO1830lI8C03.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C11013l3.C0I1O3C3lI8(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void
     arg types: [android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, int]
     candidates:
      android.support.v4.app.C11013l3.C0I1O3C3lI8(android.support.v4.app.C1OC33O0lO81, android.support.v4.app.Fragment, boolean):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C0I1O3C3lI8(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void */
    /* access modifiers changed from: private */
    public C01O0C C01O0C(C1OC33O0lO81 c1OC33O0lO81, boolean z, Fragment fragment) {
        C01O0C C0I1O3C3lI82 = C0I1O3C3lI8(c1OC33O0lO81, fragment, z);
        if (z) {
            if (fragment.II0ll1CC13l != null) {
                fragment.II0ll1CC13l.C01O0C(this.CI3C103l01O, C0I1O3C3lI82);
            }
            C01O0C(c1OC33O0lO81, C0I1O3C3lI82, true);
        } else {
            if (fragment.II083CII != null) {
                fragment.II083CII.C01O0C(this.CI3C103l01O, C0I1O3C3lI82);
            }
            C0I1O3C3lI8(c1OC33O0lO81, C0I1O3C3lI82, true);
        }
        return C0I1O3C3lI82;
    }

    private static C01O0C C01O0C(ArrayList arrayList, ArrayList arrayList2, C01O0C c01o0c) {
        if (c01o0c.isEmpty()) {
            return c01o0c;
        }
        C01O0C c01o0c2 = new C01O0C();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            View view = (View) c01o0c.get(arrayList.get(i));
            if (view != null) {
                c01o0c2.put(arrayList2.get(i), view);
            }
        }
        return c01o0c2;
    }

    private static Object C01O0C(Fragment fragment, Fragment fragment2, boolean z) {
        if (fragment == null || fragment2 == null) {
            return null;
        }
        return CO30CC1l0313.C01O0C(z ? fragment2.CIOC8C() : fragment.CII3C813OIC8());
    }

    private static Object C01O0C(Fragment fragment, boolean z) {
        if (fragment == null) {
            return null;
        }
        return CO30CC1l0313.C01O0C(z ? fragment.CI3C103l01O() : fragment.CC8IOI1II0());
    }

    private static Object C01O0C(Object obj, Fragment fragment, ArrayList arrayList, C01O0C c01o0c, View view) {
        return obj != null ? CO30CC1l0313.C01O0C(obj, fragment.C1O10Cl038(), arrayList, c01o0c, view) : obj;
    }

    private void C01O0C(int i, Fragment fragment, String str, int i2) {
        fragment.CI0I8l333131 = this.C01O0C;
        if (str != null) {
            if (fragment.ClO80C3lOO8 == null || str.equals(fragment.ClO80C3lOO8)) {
                fragment.ClO80C3lOO8 = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.ClO80C3lOO8 + " now " + str);
            }
        }
        if (i != 0) {
            if (fragment.Cl80C0l838l == 0 || fragment.Cl80C0l838l == i) {
                fragment.Cl80C0l838l = i;
                fragment.ClC13lIl = i;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.Cl80C0l838l + " now " + i);
            }
        }
        C1O10Cl038 c1O10Cl038 = new C1O10Cl038();
        c1O10Cl038.C101lC8O = i2;
        c1O10Cl038.C11013l3 = fragment;
        C01O0C(c1O10Cl038);
    }

    /* access modifiers changed from: private */
    public void C01O0C(C1OC33O0lO81 c1OC33O0lO81, int i, Object obj) {
        if (this.C01O0C.C1l00I1 != null) {
            for (int i2 = 0; i2 < this.C01O0C.C1l00I1.size(); i2++) {
                Fragment fragment = (Fragment) this.C01O0C.C1l00I1.get(i2);
                if (!(fragment.I08O3C == null || fragment.I088l3088 == null || fragment.ClC13lIl != i)) {
                    if (!fragment.CO081lO0OC0) {
                        CO30CC1l0313.C01O0C(obj, fragment.I08O3C, false);
                        c1OC33O0lO81.C0I1O3C3lI8.remove(fragment.I08O3C);
                    } else if (!c1OC33O0lO81.C0I1O3C3lI8.contains(fragment.I08O3C)) {
                        CO30CC1l0313.C01O0C(obj, fragment.I08O3C, true);
                        c1OC33O0lO81.C0I1O3C3lI8.add(fragment.I08O3C);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void C01O0C(C1OC33O0lO81 c1OC33O0lO81, Fragment fragment, Fragment fragment2, boolean z, C01O0C c01o0c) {
        l83C0OCC3 l83c0occ3 = z ? fragment2.II083CII : fragment.II083CII;
        if (l83c0occ3 != null) {
            l83c0occ3.C0I1O3C3lI8(new ArrayList(c01o0c.keySet()), new ArrayList(c01o0c.values()), null);
        }
    }

    private void C01O0C(C1OC33O0lO81 c1OC33O0lO81, C01O0C c01o0c, boolean z) {
        int size = this.CI3C103l01O == null ? 0 : this.CI3C103l01O.size();
        for (int i = 0; i < size; i++) {
            String str = (String) this.CI0I8l333131.get(i);
            View view = (View) c01o0c.get((String) this.CI3C103l01O.get(i));
            if (view != null) {
                String C01O0C2 = CO30CC1l0313.C01O0C(view);
                if (z) {
                    C01O0C(c1OC33O0lO81.C01O0C, str, C01O0C2);
                } else {
                    C01O0C(c1OC33O0lO81.C01O0C, C01O0C2, str);
                }
            }
        }
    }

    private void C01O0C(C1OC33O0lO81 c1OC33O0lO81, View view, Object obj, Fragment fragment, Fragment fragment2, boolean z, ArrayList arrayList) {
        view.getViewTreeObserver().addOnPreDrawListener(new C18Cl1C(this, view, obj, arrayList, c1OC33O0lO81, z, fragment, fragment2));
    }

    private static void C01O0C(C1OC33O0lO81 c1OC33O0lO81, ArrayList arrayList, ArrayList arrayList2) {
        if (arrayList != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < arrayList.size()) {
                    C01O0C(c1OC33O0lO81.C01O0C, (String) arrayList.get(i2), (String) arrayList2.get(i2));
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void C01O0C(C01O0C c01o0c, C1OC33O0lO81 c1OC33O0lO81) {
        View view;
        if (this.CI3C103l01O != null && !c01o0c.isEmpty() && (view = (View) c01o0c.get(this.CI3C103l01O.get(0))) != null) {
            c1OC33O0lO81.C101lC8O.C01O0C = view;
        }
    }

    private static void C01O0C(C01O0C c01o0c, String str, String str2) {
        if (str != null && str2 != null && !str.equals(str2)) {
            for (int i = 0; i < c01o0c.size(); i++) {
                if (str.equals(c01o0c.C101lC8O(i))) {
                    c01o0c.C01O0C(i, str2);
                    return;
                }
            }
            c01o0c.put(str, str2);
        }
    }

    private static void C01O0C(SparseArray sparseArray, Fragment fragment) {
        int i;
        if (fragment != null && (i = fragment.ClC13lIl) != 0 && !fragment.C1l00I1() && fragment.C11ll3() && fragment.C1O10Cl038() != null && sparseArray.get(i) == null) {
            sparseArray.put(i, fragment);
        }
    }

    private void C01O0C(View view, C1OC33O0lO81 c1OC33O0lO81, int i, Object obj) {
        view.getViewTreeObserver().addOnPreDrawListener(new C1l00I1(this, view, c1OC33O0lO81, i, obj));
    }

    private boolean C01O0C(int i, C1OC33O0lO81 c1OC33O0lO81, boolean z, SparseArray sparseArray, SparseArray sparseArray2) {
        View view;
        ViewGroup viewGroup = (ViewGroup) this.C01O0C.C8CI00.C01O0C(i);
        if (viewGroup == null) {
            return false;
        }
        Fragment fragment = (Fragment) sparseArray2.get(i);
        Fragment fragment2 = (Fragment) sparseArray.get(i);
        Object C01O0C2 = C01O0C(fragment, z);
        Object C01O0C3 = C01O0C(fragment, fragment2, z);
        Object C0I1O3C3lI82 = C0I1O3C3lI8(fragment2, z);
        if (C01O0C2 == null && C01O0C3 == null && C0I1O3C3lI82 == null) {
            return false;
        }
        C01O0C c01o0c = null;
        ArrayList arrayList = new ArrayList();
        if (C01O0C3 != null) {
            c01o0c = C01O0C(c1OC33O0lO81, fragment2, z);
            arrayList.add(c1OC33O0lO81.C11013l3);
            arrayList.addAll(c01o0c.values());
            l83C0OCC3 l83c0occ3 = z ? fragment2.II083CII : fragment.II083CII;
            if (l83c0occ3 != null) {
                l83c0occ3.C01O0C(new ArrayList(c01o0c.keySet()), new ArrayList(c01o0c.values()), null);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        Object C01O0C4 = C01O0C(C0I1O3C3lI82, fragment2, arrayList2, c01o0c, c1OC33O0lO81.C11013l3);
        if (!(this.CI3C103l01O == null || c01o0c == null || (view = (View) c01o0c.get(this.CI3C103l01O.get(0))) == null)) {
            if (C01O0C4 != null) {
                CO30CC1l0313.C01O0C(C01O0C4, view);
            }
            if (C01O0C3 != null) {
                CO30CC1l0313.C01O0C(C01O0C3, view);
            }
        }
        C11ll3 c11ll3 = new C11ll3(this, fragment);
        if (C01O0C3 != null) {
            C01O0C(c1OC33O0lO81, viewGroup, C01O0C3, fragment, fragment2, z, arrayList);
        }
        ArrayList arrayList3 = new ArrayList();
        C01O0C c01o0c2 = new C01O0C();
        Object C01O0C5 = CO30CC1l0313.C01O0C(C01O0C2, C01O0C4, C01O0C3, z ? fragment.ClC13lIl() : fragment.Cl80C0l838l());
        if (C01O0C5 != null) {
            CO30CC1l0313.C01O0C(C01O0C2, C01O0C3, viewGroup, c11ll3, c1OC33O0lO81.C11013l3, c1OC33O0lO81.C101lC8O, c1OC33O0lO81.C01O0C, arrayList3, c01o0c2, arrayList);
            C01O0C(viewGroup, c1OC33O0lO81, i, C01O0C5);
            CO30CC1l0313.C01O0C(C01O0C5, c1OC33O0lO81.C11013l3, true);
            C01O0C(c1OC33O0lO81, i, C01O0C5);
            CO30CC1l0313.C01O0C(viewGroup, C01O0C5);
            CO30CC1l0313.C01O0C(viewGroup, c1OC33O0lO81.C11013l3, C01O0C2, arrayList3, C01O0C4, arrayList2, C01O0C3, arrayList, C01O0C5, c1OC33O0lO81.C0I1O3C3lI8, c01o0c2);
        }
        return C01O0C5 != null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CO30CC1l0313.C01O0C(java.util.Map, android.view.View):void
     arg types: [android.support.v4.c.C01O0C, android.view.View]
     candidates:
      android.support.v4.app.CO30CC1l0313.C01O0C(android.transition.Transition, android.support.v4.app.I03lII1):void
      android.support.v4.app.CO30CC1l0313.C01O0C(android.view.ViewGroup, java.lang.Object):void
      android.support.v4.app.CO30CC1l0313.C01O0C(java.lang.Object, android.view.View):void
      android.support.v4.app.CO30CC1l0313.C01O0C(java.lang.Object, java.util.ArrayList):void
      android.support.v4.app.CO30CC1l0313.C01O0C(java.util.ArrayList, android.view.View):void
      android.support.v4.app.CO30CC1l0313.C01O0C(java.util.Map, android.view.View):void */
    private C01O0C C0I1O3C3lI8(C1OC33O0lO81 c1OC33O0lO81, Fragment fragment, boolean z) {
        C01O0C c01o0c = new C01O0C();
        View C1O10Cl0382 = fragment.C1O10Cl038();
        if (C1O10Cl0382 == null || this.CI0I8l333131 == null) {
            return c01o0c;
        }
        CO30CC1l0313.C01O0C((Map) c01o0c, C1O10Cl0382);
        if (z) {
            return C01O0C(this.CI0I8l333131, this.CI3C103l01O, c01o0c);
        }
        c01o0c.C01O0C((Collection) this.CI3C103l01O);
        return c01o0c;
    }

    private static Object C0I1O3C3lI8(Fragment fragment, boolean z) {
        if (fragment == null) {
            return null;
        }
        return CO30CC1l0313.C01O0C(z ? fragment.CCC3CC0l() : fragment.CI0I8l333131());
    }

    private void C0I1O3C3lI8(C1OC33O0lO81 c1OC33O0lO81, C01O0C c01o0c, boolean z) {
        int size = c01o0c.size();
        for (int i = 0; i < size; i++) {
            String str = (String) c01o0c.C0I1O3C3lI8(i);
            String C01O0C2 = CO30CC1l0313.C01O0C((View) c01o0c.C101lC8O(i));
            if (z) {
                C01O0C(c1OC33O0lO81.C01O0C, str, C01O0C2);
            } else {
                C01O0C(c1OC33O0lO81.C01O0C, C01O0C2, str);
            }
        }
    }

    private void C0I1O3C3lI8(SparseArray sparseArray, Fragment fragment) {
        int i;
        if (fragment != null && (i = fragment.ClC13lIl) != 0) {
            sparseArray.put(i, fragment);
        }
    }

    private void C0I1O3C3lI8(SparseArray sparseArray, SparseArray sparseArray2) {
        Fragment fragment;
        if (this.C01O0C.C8CI00.C01O0C()) {
            for (C1O10Cl038 c1O10Cl038 = this.C0I1O3C3lI8; c1O10Cl038 != null; c1O10Cl038 = c1O10Cl038.C01O0C) {
                switch (c1O10Cl038.C101lC8O) {
                    case 1:
                        C0I1O3C3lI8(sparseArray2, c1O10Cl038.C11013l3);
                        break;
                    case 2:
                        Fragment fragment2 = c1O10Cl038.C11013l3;
                        if (this.C01O0C.C1l00I1 != null) {
                            int i = 0;
                            fragment = fragment2;
                            while (true) {
                                int i2 = i;
                                if (i2 < this.C01O0C.C1l00I1.size()) {
                                    Fragment fragment3 = (Fragment) this.C01O0C.C1l00I1.get(i2);
                                    if (fragment == null || fragment3.ClC13lIl == fragment.ClC13lIl) {
                                        if (fragment3 == fragment) {
                                            fragment = null;
                                        } else {
                                            C01O0C(sparseArray, fragment3);
                                        }
                                    }
                                    i = i2 + 1;
                                }
                            }
                        } else {
                            fragment = fragment2;
                        }
                        C0I1O3C3lI8(sparseArray2, fragment);
                        break;
                    case 3:
                        C01O0C(sparseArray, c1O10Cl038.C11013l3);
                        break;
                    case 4:
                        C01O0C(sparseArray, c1O10Cl038.C11013l3);
                        break;
                    case 5:
                        C0I1O3C3lI8(sparseArray2, c1O10Cl038.C11013l3);
                        break;
                    case 6:
                        C01O0C(sparseArray, c1O10Cl038.C11013l3);
                        break;
                    case 7:
                        C0I1O3C3lI8(sparseArray2, c1O10Cl038.C11013l3);
                        break;
                }
            }
        }
    }

    public int C01O0C() {
        return C01O0C(false);
    }

    /* access modifiers changed from: package-private */
    public int C01O0C(boolean z) {
        if (this.C3llC38O1) {
            throw new IllegalStateException("commit already called");
        }
        if (CCC3CC0l.C01O0C) {
            Log.v("FragmentManager", "Commit: " + this);
            C01O0C("  ", (FileDescriptor) null, new PrintWriter(new C11ll3("FragmentManager")), (String[]) null);
        }
        this.C3llC38O1 = true;
        if (this.C3CIO118) {
            this.C831O13C118 = this.C01O0C.C01O0C(this);
        } else {
            this.C831O13C118 = -1;
        }
        this.C01O0C.C01O0C(this, z);
        return this.C831O13C118;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C11013l3.C01O0C(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.C1OC33O0lO81
     arg types: [android.util.SparseArray, android.util.SparseArray, int]
     candidates:
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.app.Fragment, boolean):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, boolean, android.support.v4.app.Fragment):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(java.util.ArrayList, java.util.ArrayList, android.support.v4.c.C01O0C):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C11013l3, android.support.v4.c.C01O0C, android.support.v4.app.C1OC33O0lO81):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, int, java.lang.Object):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.c.C01O0C, java.lang.String, java.lang.String):void
      android.support.v4.app.C11013l3.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.C11013l3.C01O0C(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.CO1830lI8C03.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.C11013l3.C01O0C(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.C1OC33O0lO81 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(int, int, int, boolean):void */
    public C1OC33O0lO81 C01O0C(boolean z, C1OC33O0lO81 c1OC33O0lO81, SparseArray sparseArray, SparseArray sparseArray2) {
        if (CCC3CC0l.C01O0C) {
            Log.v("FragmentManager", "popFromBackStack: " + this);
            C01O0C("  ", (FileDescriptor) null, new PrintWriter(new C11ll3("FragmentManager")), (String[]) null);
        }
        if (c1OC33O0lO81 == null) {
            if (!(sparseArray.size() == 0 && sparseArray2.size() == 0)) {
                c1OC33O0lO81 = C01O0C(sparseArray, sparseArray2, true);
            }
        } else if (!z) {
            C01O0C(c1OC33O0lO81, this.CI3C103l01O, this.CI0I8l333131);
        }
        C01O0C(-1);
        int i = c1OC33O0lO81 != null ? 0 : this.C3C1C0I8l3;
        int i2 = c1OC33O0lO81 != null ? 0 : this.C1OC33O0lO81;
        for (C1O10Cl038 c1O10Cl038 = this.C101lC8O; c1O10Cl038 != null; c1O10Cl038 = c1O10Cl038.C0I1O3C3lI8) {
            int i3 = c1OC33O0lO81 != null ? 0 : c1O10Cl038.C1l00I1;
            int i4 = c1OC33O0lO81 != null ? 0 : c1O10Cl038.C1O10Cl038;
            switch (c1O10Cl038.C101lC8O) {
                case 1:
                    Fragment fragment = c1O10Cl038.C11013l3;
                    fragment.I03lII1 = i4;
                    this.C01O0C.C01O0C(fragment, CCC3CC0l.C101lC8O(i2), i);
                    break;
                case 2:
                    Fragment fragment2 = c1O10Cl038.C11013l3;
                    if (fragment2 != null) {
                        fragment2.I03lII1 = i4;
                        this.C01O0C.C01O0C(fragment2, CCC3CC0l.C101lC8O(i2), i);
                    }
                    if (c1O10Cl038.C1OC33O0lO81 == null) {
                        break;
                    } else {
                        for (int i5 = 0; i5 < c1O10Cl038.C1OC33O0lO81.size(); i5++) {
                            Fragment fragment3 = (Fragment) c1O10Cl038.C1OC33O0lO81.get(i5);
                            fragment3.I03lII1 = i3;
                            this.C01O0C.C01O0C(fragment3, false);
                        }
                        break;
                    }
                case 3:
                    Fragment fragment4 = c1O10Cl038.C11013l3;
                    fragment4.I03lII1 = i3;
                    this.C01O0C.C01O0C(fragment4, false);
                    break;
                case 4:
                    Fragment fragment5 = c1O10Cl038.C11013l3;
                    fragment5.I03lII1 = i3;
                    this.C01O0C.C101lC8O(fragment5, CCC3CC0l.C101lC8O(i2), i);
                    break;
                case 5:
                    Fragment fragment6 = c1O10Cl038.C11013l3;
                    fragment6.I03lII1 = i4;
                    this.C01O0C.C0I1O3C3lI8(fragment6, CCC3CC0l.C101lC8O(i2), i);
                    break;
                case 6:
                    Fragment fragment7 = c1O10Cl038.C11013l3;
                    fragment7.I03lII1 = i3;
                    this.C01O0C.C11ll3(fragment7, CCC3CC0l.C101lC8O(i2), i);
                    break;
                case 7:
                    Fragment fragment8 = c1O10Cl038.C11013l3;
                    fragment8.I03lII1 = i3;
                    this.C01O0C.C11013l3(fragment8, CCC3CC0l.C101lC8O(i2), i);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + c1O10Cl038.C101lC8O);
            }
        }
        if (z) {
            this.C01O0C.C01O0C(this.C01O0C.C3llC38O1, CCC3CC0l.C101lC8O(i2), i, true);
            c1OC33O0lO81 = null;
        }
        if (this.C831O13C118 >= 0) {
            this.C01O0C.C0I1O3C3lI8(this.C831O13C118);
            this.C831O13C118 = -1;
        }
        return c1OC33O0lO81;
    }

    public CO1830lI8C03 C01O0C(int i, Fragment fragment, String str) {
        C01O0C(i, fragment, str, 1);
        return this;
    }

    public CO1830lI8C03 C01O0C(Fragment fragment) {
        C1O10Cl038 c1O10Cl038 = new C1O10Cl038();
        c1O10Cl038.C101lC8O = 6;
        c1O10Cl038.C11013l3 = fragment;
        C01O0C(c1O10Cl038);
        return this;
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(int i) {
        if (this.C3CIO118) {
            if (CCC3CC0l.C01O0C) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i);
            }
            for (C1O10Cl038 c1O10Cl038 = this.C0I1O3C3lI8; c1O10Cl038 != null; c1O10Cl038 = c1O10Cl038.C01O0C) {
                if (c1O10Cl038.C11013l3 != null) {
                    c1O10Cl038.C11013l3.CCC3CC0l += i;
                    if (CCC3CC0l.C01O0C) {
                        Log.v("FragmentManager", "Bump nesting of " + c1O10Cl038.C11013l3 + " to " + c1O10Cl038.C11013l3.CCC3CC0l);
                    }
                }
                if (c1O10Cl038.C1OC33O0lO81 != null) {
                    for (int size = c1O10Cl038.C1OC33O0lO81.size() - 1; size >= 0; size--) {
                        Fragment fragment = (Fragment) c1O10Cl038.C1OC33O0lO81.get(size);
                        fragment.CCC3CC0l += i;
                        if (CCC3CC0l.C01O0C) {
                            Log.v("FragmentManager", "Bump nesting of " + fragment + " to " + fragment.CCC3CC0l);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(C1O10Cl038 c1O10Cl038) {
        if (this.C0I1O3C3lI8 == null) {
            this.C101lC8O = c1O10Cl038;
            this.C0I1O3C3lI8 = c1O10Cl038;
        } else {
            c1O10Cl038.C0I1O3C3lI8 = this.C101lC8O;
            this.C101lC8O.C01O0C = c1O10Cl038;
            this.C101lC8O = c1O10Cl038;
        }
        c1O10Cl038.C11ll3 = this.C11ll3;
        c1O10Cl038.C18Cl1C = this.C18Cl1C;
        c1O10Cl038.C1l00I1 = this.C1l00I1;
        c1O10Cl038.C1O10Cl038 = this.C1O10Cl038;
        this.C11013l3++;
    }

    public void C01O0C(SparseArray sparseArray, SparseArray sparseArray2) {
        if (this.C01O0C.C8CI00.C01O0C()) {
            for (C1O10Cl038 c1O10Cl038 = this.C0I1O3C3lI8; c1O10Cl038 != null; c1O10Cl038 = c1O10Cl038.C01O0C) {
                switch (c1O10Cl038.C101lC8O) {
                    case 1:
                        C01O0C(sparseArray, c1O10Cl038.C11013l3);
                        break;
                    case 2:
                        if (c1O10Cl038.C1OC33O0lO81 != null) {
                            for (int size = c1O10Cl038.C1OC33O0lO81.size() - 1; size >= 0; size--) {
                                C0I1O3C3lI8(sparseArray2, (Fragment) c1O10Cl038.C1OC33O0lO81.get(size));
                            }
                        }
                        C01O0C(sparseArray, c1O10Cl038.C11013l3);
                        break;
                    case 3:
                        C0I1O3C3lI8(sparseArray2, c1O10Cl038.C11013l3);
                        break;
                    case 4:
                        C0I1O3C3lI8(sparseArray2, c1O10Cl038.C11013l3);
                        break;
                    case 5:
                        C01O0C(sparseArray, c1O10Cl038.C11013l3);
                        break;
                    case 6:
                        C0I1O3C3lI8(sparseArray2, c1O10Cl038.C11013l3);
                        break;
                    case 7:
                        C01O0C(sparseArray, c1O10Cl038.C11013l3);
                        break;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C11013l3.C01O0C(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.app.C11013l3.C01O0C(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.C1OC33O0lO81
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.app.Fragment, boolean):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, boolean, android.support.v4.app.Fragment):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(java.util.ArrayList, java.util.ArrayList, android.support.v4.c.C01O0C):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C11013l3, android.support.v4.c.C01O0C, android.support.v4.app.C1OC33O0lO81):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, int, java.lang.Object):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.c.C01O0C, java.lang.String, java.lang.String):void
      android.support.v4.app.C11013l3.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.CO1830lI8C03.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.C11013l3.C01O0C(java.lang.String, java.io.PrintWriter, boolean):void */
    public void C01O0C(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        C01O0C(str, printWriter, true);
    }

    public void C01O0C(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.C3l3O8lIOIO8);
            printWriter.print(" mIndex=");
            printWriter.print(this.C831O13C118);
            printWriter.print(" mCommitted=");
            printWriter.println(this.C3llC38O1);
            if (this.C1OC33O0lO81 != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.C1OC33O0lO81));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.C3C1C0I8l3));
            }
            if (!(this.C11ll3 == 0 && this.C18Cl1C == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.C11ll3));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.C18Cl1C));
            }
            if (!(this.C1l00I1 == 0 && this.C1O10Cl038 == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.C1l00I1));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.C1O10Cl038));
            }
            if (!(this.C8CI00 == 0 && this.CC38COI1l3I == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.C8CI00));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.CC38COI1l3I);
            }
            if (!(this.CC8IOI1II0 == 0 && this.CCC3CC0l == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.CC8IOI1II0));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.CCC3CC0l);
            }
        }
        if (this.C0I1O3C3lI8 != null) {
            printWriter.print(str);
            printWriter.println("Operations:");
            String str3 = str + "    ";
            int i = 0;
            C1O10Cl038 c1O10Cl038 = this.C0I1O3C3lI8;
            while (c1O10Cl038 != null) {
                switch (c1O10Cl038.C101lC8O) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    default:
                        str2 = "cmd=" + c1O10Cl038.C101lC8O;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(c1O10Cl038.C11013l3);
                if (z) {
                    if (!(c1O10Cl038.C11ll3 == 0 && c1O10Cl038.C18Cl1C == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(c1O10Cl038.C11ll3));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(c1O10Cl038.C18Cl1C));
                    }
                    if (!(c1O10Cl038.C1l00I1 == 0 && c1O10Cl038.C1O10Cl038 == 0)) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(c1O10Cl038.C1l00I1));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(c1O10Cl038.C1O10Cl038));
                    }
                }
                if (c1O10Cl038.C1OC33O0lO81 != null && c1O10Cl038.C1OC33O0lO81.size() > 0) {
                    for (int i2 = 0; i2 < c1O10Cl038.C1OC33O0lO81.size(); i2++) {
                        printWriter.print(str3);
                        if (c1O10Cl038.C1OC33O0lO81.size() == 1) {
                            printWriter.print("Removed: ");
                        } else {
                            if (i2 == 0) {
                                printWriter.println("Removed:");
                            }
                            printWriter.print(str3);
                            printWriter.print("  #");
                            printWriter.print(i2);
                            printWriter.print(": ");
                        }
                        printWriter.println(c1O10Cl038.C1OC33O0lO81.get(i2));
                    }
                }
                c1O10Cl038 = c1O10Cl038.C01O0C;
                i++;
            }
        }
    }

    public CO1830lI8C03 C0I1O3C3lI8(Fragment fragment) {
        C1O10Cl038 c1O10Cl038 = new C1O10Cl038();
        c1O10Cl038.C101lC8O = 7;
        c1O10Cl038.C11013l3 = fragment;
        C01O0C(c1O10Cl038);
        return this;
    }

    public String C0I1O3C3lI8() {
        return this.C3l3O8lIOIO8;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.C11013l3.C01O0C(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.C1OC33O0lO81
     arg types: [android.util.SparseArray, android.util.SparseArray, int]
     candidates:
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.app.Fragment, boolean):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, boolean, android.support.v4.app.Fragment):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(java.util.ArrayList, java.util.ArrayList, android.support.v4.c.C01O0C):android.support.v4.c.C01O0C
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C11013l3, android.support.v4.c.C01O0C, android.support.v4.app.C1OC33O0lO81):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, int, java.lang.Object):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, android.support.v4.c.C01O0C, boolean):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.app.C1OC33O0lO81, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.C11013l3.C01O0C(android.support.v4.c.C01O0C, java.lang.String, java.lang.String):void
      android.support.v4.app.C11013l3.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.C11013l3.C01O0C(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.CO1830lI8C03.C01O0C(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.CO1830lI8C03
      android.support.v4.app.C11013l3.C01O0C(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.C1OC33O0lO81 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.CCC3CC0l.C01O0C(int, android.support.v4.app.C11013l3):void
      android.support.v4.app.CCC3CC0l.C01O0C(int, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.Runnable, boolean):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.CCC3CC0l.C01O0C(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.CCC3CC0l.C01O0C(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.CCC3CC0l.C01O0C(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.CCC3CC0l.C01O0C(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.CCC3CC0l.C01O0C(int, int, int, boolean):void */
    public void run() {
        C1OC33O0lO81 c1OC33O0lO81;
        Fragment fragment;
        if (CCC3CC0l.C01O0C) {
            Log.v("FragmentManager", "Run: " + this);
        }
        if (!this.C3CIO118 || this.C831O13C118 >= 0) {
            C01O0C(1);
            if (Build.VERSION.SDK_INT >= 21) {
                SparseArray sparseArray = new SparseArray();
                SparseArray sparseArray2 = new SparseArray();
                C0I1O3C3lI8(sparseArray, sparseArray2);
                c1OC33O0lO81 = C01O0C(sparseArray, sparseArray2, false);
            } else {
                c1OC33O0lO81 = null;
            }
            int i = c1OC33O0lO81 != null ? 0 : this.C3C1C0I8l3;
            int i2 = c1OC33O0lO81 != null ? 0 : this.C1OC33O0lO81;
            for (C1O10Cl038 c1O10Cl038 = this.C0I1O3C3lI8; c1O10Cl038 != null; c1O10Cl038 = c1O10Cl038.C01O0C) {
                int i3 = c1OC33O0lO81 != null ? 0 : c1O10Cl038.C11ll3;
                int i4 = c1OC33O0lO81 != null ? 0 : c1O10Cl038.C18Cl1C;
                switch (c1O10Cl038.C101lC8O) {
                    case 1:
                        Fragment fragment2 = c1O10Cl038.C11013l3;
                        fragment2.I03lII1 = i3;
                        this.C01O0C.C01O0C(fragment2, false);
                        break;
                    case 2:
                        Fragment fragment3 = c1O10Cl038.C11013l3;
                        if (this.C01O0C.C1l00I1 != null) {
                            fragment = fragment3;
                            for (int i5 = 0; i5 < this.C01O0C.C1l00I1.size(); i5++) {
                                Fragment fragment4 = (Fragment) this.C01O0C.C1l00I1.get(i5);
                                if (CCC3CC0l.C01O0C) {
                                    Log.v("FragmentManager", "OP_REPLACE: adding=" + fragment + " old=" + fragment4);
                                }
                                if (fragment == null || fragment4.ClC13lIl == fragment.ClC13lIl) {
                                    if (fragment4 == fragment) {
                                        c1O10Cl038.C11013l3 = null;
                                        fragment = null;
                                    } else {
                                        if (c1O10Cl038.C1OC33O0lO81 == null) {
                                            c1O10Cl038.C1OC33O0lO81 = new ArrayList();
                                        }
                                        c1O10Cl038.C1OC33O0lO81.add(fragment4);
                                        fragment4.I03lII1 = i4;
                                        if (this.C3CIO118) {
                                            fragment4.CCC3CC0l++;
                                            if (CCC3CC0l.C01O0C) {
                                                Log.v("FragmentManager", "Bump nesting of " + fragment4 + " to " + fragment4.CCC3CC0l);
                                            }
                                        }
                                        this.C01O0C.C01O0C(fragment4, i2, i);
                                    }
                                }
                            }
                        } else {
                            fragment = fragment3;
                        }
                        if (fragment == null) {
                            break;
                        } else {
                            fragment.I03lII1 = i3;
                            this.C01O0C.C01O0C(fragment, false);
                            break;
                        }
                    case 3:
                        Fragment fragment5 = c1O10Cl038.C11013l3;
                        fragment5.I03lII1 = i4;
                        this.C01O0C.C01O0C(fragment5, i2, i);
                        break;
                    case 4:
                        Fragment fragment6 = c1O10Cl038.C11013l3;
                        fragment6.I03lII1 = i4;
                        this.C01O0C.C0I1O3C3lI8(fragment6, i2, i);
                        break;
                    case 5:
                        Fragment fragment7 = c1O10Cl038.C11013l3;
                        fragment7.I03lII1 = i3;
                        this.C01O0C.C101lC8O(fragment7, i2, i);
                        break;
                    case 6:
                        Fragment fragment8 = c1O10Cl038.C11013l3;
                        fragment8.I03lII1 = i4;
                        this.C01O0C.C11013l3(fragment8, i2, i);
                        break;
                    case 7:
                        Fragment fragment9 = c1O10Cl038.C11013l3;
                        fragment9.I03lII1 = i3;
                        this.C01O0C.C11ll3(fragment9, i2, i);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown cmd: " + c1O10Cl038.C101lC8O);
                }
            }
            this.C01O0C.C01O0C(this.C01O0C.C3llC38O1, i2, i, true);
            if (this.C3CIO118) {
                this.C01O0C.C0I1O3C3lI8(this);
                return;
            }
            return;
        }
        throw new IllegalStateException("addToBackStack() called after commit()");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.C831O13C118 >= 0) {
            sb.append(" #");
            sb.append(this.C831O13C118);
        }
        if (this.C3l3O8lIOIO8 != null) {
            sb.append(" ");
            sb.append(this.C3l3O8lIOIO8);
        }
        sb.append("}");
        return sb.toString();
    }
}
