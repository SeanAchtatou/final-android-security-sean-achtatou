package android.support.v4.app;

import android.view.View;
import android.view.ViewTreeObserver;

class C1l00I1 implements ViewTreeObserver.OnPreDrawListener {
    final /* synthetic */ View C01O0C;
    final /* synthetic */ C1OC33O0lO81 C0I1O3C3lI8;
    final /* synthetic */ int C101lC8O;
    final /* synthetic */ Object C11013l3;
    final /* synthetic */ C11013l3 C11ll3;

    C1l00I1(C11013l3 c11013l3, View view, C1OC33O0lO81 c1OC33O0lO81, int i, Object obj) {
        this.C11ll3 = c11013l3;
        this.C01O0C = view;
        this.C0I1O3C3lI8 = c1OC33O0lO81;
        this.C101lC8O = i;
        this.C11013l3 = obj;
    }

    public boolean onPreDraw() {
        this.C01O0C.getViewTreeObserver().removeOnPreDrawListener(this);
        this.C11ll3.C01O0C(this.C0I1O3C3lI8, this.C101lC8O, this.C11013l3);
        return true;
    }
}
