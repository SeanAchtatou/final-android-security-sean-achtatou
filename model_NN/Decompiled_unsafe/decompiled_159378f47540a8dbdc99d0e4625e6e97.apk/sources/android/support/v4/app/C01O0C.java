package android.support.v4.app;

import android.app.Activity;
import android.os.Build;

public class C01O0C extends android.support.v4.a.C01O0C {
    public static void C01O0C(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            C0I1O3C3lI8.C01O0C(activity);
        } else {
            activity.finish();
        }
    }
}
