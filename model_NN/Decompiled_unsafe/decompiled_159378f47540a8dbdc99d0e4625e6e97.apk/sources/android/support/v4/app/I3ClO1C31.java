package android.support.v4.app;

import android.app.PendingIntent;
import android.os.Bundle;

public class I3ClO1C31 extends IO1C1CO13l {
    public static final IOC301l31l8I C11013l3 = new I80183lOl();
    public int C01O0C;
    public CharSequence C0I1O3C3lI8;
    public PendingIntent C101lC8O;
    private final Bundle C11ll3;
    private final l10O8Cl01l1[] C18Cl1C;

    /* access modifiers changed from: protected */
    public int C01O0C() {
        return this.C01O0C;
    }

    /* access modifiers changed from: protected */
    public CharSequence C0I1O3C3lI8() {
        return this.C0I1O3C3lI8;
    }

    /* access modifiers changed from: protected */
    public PendingIntent C101lC8O() {
        return this.C101lC8O;
    }

    public Bundle C11013l3() {
        return this.C11ll3;
    }

    /* renamed from: C11ll3 */
    public l10O8Cl01l1[] C18Cl1C() {
        return this.C18Cl1C;
    }
}
