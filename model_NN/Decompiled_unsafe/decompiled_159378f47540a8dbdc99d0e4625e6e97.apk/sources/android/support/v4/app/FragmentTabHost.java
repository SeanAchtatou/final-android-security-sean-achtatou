package android.support.v4.app;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.TabHost;
import java.util.ArrayList;

public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {
    private final ArrayList C01O0C;
    private Context C0I1O3C3lI8;
    private CC38COI1l3I C101lC8O;
    private int C11013l3;
    private TabHost.OnTabChangeListener C11ll3;
    private CO081lO0OC0 C18Cl1C;
    private boolean C1l00I1;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new ClO80C3lOO8();
        String C01O0C;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.C01O0C = parcel.readString();
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.C01O0C + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.C01O0C);
        }
    }

    private CO1830lI8C03 C01O0C(String str, CO1830lI8C03 cO1830lI8C03) {
        CO081lO0OC0 cO081lO0OC0 = null;
        int i = 0;
        while (i < this.C01O0C.size()) {
            CO081lO0OC0 cO081lO0OC02 = (CO081lO0OC0) this.C01O0C.get(i);
            if (!cO081lO0OC02.C01O0C.equals(str)) {
                cO081lO0OC02 = cO081lO0OC0;
            }
            i++;
            cO081lO0OC0 = cO081lO0OC02;
        }
        if (cO081lO0OC0 == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.C18Cl1C != cO081lO0OC0) {
            if (cO1830lI8C03 == null) {
                cO1830lI8C03 = this.C101lC8O.C01O0C();
            }
            if (!(this.C18Cl1C == null || this.C18Cl1C.C11013l3 == null)) {
                cO1830lI8C03.C01O0C(this.C18Cl1C.C11013l3);
            }
            if (cO081lO0OC0 != null) {
                if (cO081lO0OC0.C11013l3 == null) {
                    Fragment unused = cO081lO0OC0.C11013l3 = Fragment.C01O0C(this.C0I1O3C3lI8, cO081lO0OC0.C0I1O3C3lI8.getName(), cO081lO0OC0.C101lC8O);
                    cO1830lI8C03.C01O0C(this.C11013l3, cO081lO0OC0.C11013l3, cO081lO0OC0.C01O0C);
                } else {
                    cO1830lI8C03.C0I1O3C3lI8(cO081lO0OC0.C11013l3);
                }
            }
            this.C18Cl1C = cO081lO0OC0;
        }
        return cO1830lI8C03;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        CO1830lI8C03 cO1830lI8C03 = null;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.C01O0C.size()) {
                break;
            }
            CO081lO0OC0 cO081lO0OC0 = (CO081lO0OC0) this.C01O0C.get(i2);
            Fragment unused = cO081lO0OC0.C11013l3 = this.C101lC8O.C01O0C(cO081lO0OC0.C01O0C);
            if (cO081lO0OC0.C11013l3 != null && !cO081lO0OC0.C11013l3.C18Cl1C()) {
                if (cO081lO0OC0.C01O0C.equals(currentTabTag)) {
                    this.C18Cl1C = cO081lO0OC0;
                } else {
                    if (cO1830lI8C03 == null) {
                        cO1830lI8C03 = this.C101lC8O.C01O0C();
                    }
                    cO1830lI8C03.C01O0C(cO081lO0OC0.C11013l3);
                }
            }
            i = i2 + 1;
        }
        this.C1l00I1 = true;
        CO1830lI8C03 C01O0C2 = C01O0C(currentTabTag, cO1830lI8C03);
        if (C01O0C2 != null) {
            C01O0C2.C01O0C();
            this.C101lC8O.C0I1O3C3lI8();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.C1l00I1 = false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.C01O0C);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.C01O0C = getCurrentTabTag();
        return savedState;
    }

    public void onTabChanged(String str) {
        CO1830lI8C03 C01O0C2;
        if (this.C1l00I1 && (C01O0C2 = C01O0C(str, null)) != null) {
            C01O0C2.C01O0C();
        }
        if (this.C11ll3 != null) {
            this.C11ll3.onTabChanged(str);
        }
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.C11ll3 = onTabChangeListener;
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }
}
