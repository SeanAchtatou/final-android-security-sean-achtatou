package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;

final class C3l3O8lIOIO8 implements Parcelable.Creator {
    C3l3O8lIOIO8() {
    }

    /* renamed from: C01O0C */
    public Fragment.SavedState createFromParcel(Parcel parcel) {
        return new Fragment.SavedState(parcel, null);
    }

    /* renamed from: C01O0C */
    public Fragment.SavedState[] newArray(int i) {
        return new Fragment.SavedState[i];
    }
}
