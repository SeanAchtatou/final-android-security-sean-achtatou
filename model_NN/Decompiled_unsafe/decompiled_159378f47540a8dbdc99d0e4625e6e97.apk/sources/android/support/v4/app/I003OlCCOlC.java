package android.support.v4.app;

import android.graphics.Rect;
import android.transition.Transition;

final class I003OlCCOlC extends Transition.EpicenterCallback {
    final /* synthetic */ I03lII1 C01O0C;
    private Rect C0I1O3C3lI8;

    I003OlCCOlC(I03lII1 i03lII1) {
        this.C01O0C = i03lII1;
    }

    public Rect onGetEpicenter(Transition transition) {
        if (this.C0I1O3C3lI8 == null && this.C01O0C.C01O0C != null) {
            this.C0I1O3C3lI8 = CO30CC1l0313.C101lC8O(this.C01O0C.C01O0C);
        }
        return this.C0I1O3C3lI8;
    }
}
