package android.support.v4.app;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

class I0OlCO0CI13 extends FrameLayout {
    public I0OlCO0CI13(Context context) {
        super(context);
    }

    static ViewGroup C01O0C(View view) {
        I0OlCO0CI13 i0OlCO0CI13 = new I0OlCO0CI13(view.getContext());
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            i0OlCO0CI13.setLayoutParams(layoutParams);
        }
        view.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        i0OlCO0CI13.addView(view);
        return i0OlCO0CI13;
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(SparseArray sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }
}
