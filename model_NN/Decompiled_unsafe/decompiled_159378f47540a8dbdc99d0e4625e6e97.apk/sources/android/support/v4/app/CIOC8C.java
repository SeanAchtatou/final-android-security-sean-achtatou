package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;

final class CIOC8C implements Parcelable.Creator {
    CIOC8C() {
    }

    /* renamed from: C01O0C */
    public FragmentManagerState createFromParcel(Parcel parcel) {
        return new FragmentManagerState(parcel);
    }

    /* renamed from: C01O0C */
    public FragmentManagerState[] newArray(int i) {
        return new FragmentManagerState[i];
    }
}
