package android.support.v4.app;

import android.os.Build;
import android.os.Bundle;

public class l10O8Cl01l1 extends l3O003I1OOl0 {
    public static final l3O1CO C01O0C = new l1Cl81308();
    private static final l1I1C038OCC8 C1l00I1;
    private final String C0I1O3C3lI8;
    private final CharSequence C101lC8O;
    private final CharSequence[] C11013l3;
    private final boolean C11ll3;
    private final Bundle C18Cl1C;

    static {
        if (Build.VERSION.SDK_INT >= 20) {
            C1l00I1 = new l30lClCIOI();
        } else if (Build.VERSION.SDK_INT >= 16) {
            C1l00I1 = new l3I88388();
        } else {
            C1l00I1 = new l31lIC11OlIl();
        }
    }

    public String C01O0C() {
        return this.C0I1O3C3lI8;
    }

    public CharSequence C0I1O3C3lI8() {
        return this.C101lC8O;
    }

    public CharSequence[] C101lC8O() {
        return this.C11013l3;
    }

    public boolean C11013l3() {
        return this.C11ll3;
    }

    public Bundle C11ll3() {
        return this.C18Cl1C;
    }
}
