package android.support.v4.app;

import android.os.Build;
import java.util.ArrayList;
import java.util.Iterator;

public class I30OCIOO {
    /* access modifiers changed from: private */
    public static final ICOI8lC3 C01O0C;

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            C01O0C = new II0ll1CC13l();
        } else if (Build.VERSION.SDK_INT >= 20) {
            C01O0C = new II083CII();
        } else if (Build.VERSION.SDK_INT >= 19) {
            C01O0C = new IIOII113C033();
        } else if (Build.VERSION.SDK_INT >= 16) {
            C01O0C = new IIOC18I8();
        } else if (Build.VERSION.SDK_INT >= 14) {
            C01O0C = new IIO3O0CI();
        } else if (Build.VERSION.SDK_INT >= 11) {
            C01O0C = new IICICOC38();
        } else if (Build.VERSION.SDK_INT >= 9) {
            C01O0C = new II3OlOI8II0C();
        } else {
            C01O0C = new II1lC1O();
        }
    }

    /* access modifiers changed from: private */
    public static void C0I1O3C3lI8(I1CO03 i1co03, ArrayList arrayList) {
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            i1co03.C01O0C((I3ClO1C31) it.next());
        }
    }

    /* access modifiers changed from: private */
    public static void C0I1O3C3lI8(I1I11O81II i1i11o81ii, Il08CO0Oll il08CO0Oll) {
        if (il08CO0Oll == null) {
            return;
        }
        if (il08CO0Oll instanceof I8C3388l1301) {
            I8C3388l1301 i8C3388l1301 = (I8C3388l1301) il08CO0Oll;
            l03l300018l.C01O0C(i1i11o81ii, i8C3388l1301.C11013l3, i8C3388l1301.C18Cl1C, i8C3388l1301.C11ll3, i8C3388l1301.C01O0C);
        } else if (il08CO0Oll instanceof ICI3C3O) {
            ICI3C3O ici3c3o = (ICI3C3O) il08CO0Oll;
            l03l300018l.C01O0C(i1i11o81ii, ici3c3o.C11013l3, ici3c3o.C18Cl1C, ici3c3o.C11ll3, ici3c3o.C01O0C);
        } else if (il08CO0Oll instanceof I801IO8CII) {
            I801IO8CII i801io8cii = (I801IO8CII) il08CO0Oll;
            l03l300018l.C01O0C(i1i11o81ii, i801io8cii.C11013l3, i801io8cii.C18Cl1C, i801io8cii.C11ll3, i801io8cii.C01O0C, i801io8cii.C0I1O3C3lI8, i801io8cii.C101lC8O);
        }
    }
}
