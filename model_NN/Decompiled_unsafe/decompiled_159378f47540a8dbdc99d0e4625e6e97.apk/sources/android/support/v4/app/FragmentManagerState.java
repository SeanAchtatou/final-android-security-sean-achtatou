package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;

final class FragmentManagerState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new CIOC8C();
    FragmentState[] C01O0C;
    int[] C0I1O3C3lI8;
    BackStackState[] C101lC8O;

    public FragmentManagerState() {
    }

    public FragmentManagerState(Parcel parcel) {
        this.C01O0C = (FragmentState[]) parcel.createTypedArray(FragmentState.CREATOR);
        this.C0I1O3C3lI8 = parcel.createIntArray();
        this.C101lC8O = (BackStackState[]) parcel.createTypedArray(BackStackState.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.C01O0C, i);
        parcel.writeIntArray(this.C0I1O3C3lI8);
        parcel.writeTypedArray(this.C101lC8O, i);
    }
}
