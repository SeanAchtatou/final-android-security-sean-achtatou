package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

final class BackStackState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C3C1C0I8l3();
    final int[] C01O0C;
    final int C0I1O3C3lI8;
    final int C101lC8O;
    final String C11013l3;
    final int C11ll3;
    final int C18Cl1C;
    final int C1O10Cl038;
    final CharSequence C1OC33O0lO81;
    final CharSequence C1l00I1;
    final ArrayList C3C1C0I8l3;
    final ArrayList C3CIO118;

    public BackStackState(Parcel parcel) {
        this.C01O0C = parcel.createIntArray();
        this.C0I1O3C3lI8 = parcel.readInt();
        this.C101lC8O = parcel.readInt();
        this.C11013l3 = parcel.readString();
        this.C11ll3 = parcel.readInt();
        this.C18Cl1C = parcel.readInt();
        this.C1l00I1 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.C1O10Cl038 = parcel.readInt();
        this.C1OC33O0lO81 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.C3C1C0I8l3 = parcel.createStringArrayList();
        this.C3CIO118 = parcel.createStringArrayList();
    }

    public BackStackState(CCC3CC0l cCC3CC0l, C11013l3 c11013l3) {
        int i = 0;
        for (C1O10Cl038 c1O10Cl038 = c11013l3.C0I1O3C3lI8; c1O10Cl038 != null; c1O10Cl038 = c1O10Cl038.C01O0C) {
            if (c1O10Cl038.C1OC33O0lO81 != null) {
                i += c1O10Cl038.C1OC33O0lO81.size();
            }
        }
        this.C01O0C = new int[(i + (c11013l3.C11013l3 * 7))];
        if (!c11013l3.C3CIO118) {
            throw new IllegalStateException("Not on back stack");
        }
        int i2 = 0;
        for (C1O10Cl038 c1O10Cl0382 = c11013l3.C0I1O3C3lI8; c1O10Cl0382 != null; c1O10Cl0382 = c1O10Cl0382.C01O0C) {
            int i3 = i2 + 1;
            this.C01O0C[i2] = c1O10Cl0382.C101lC8O;
            int i4 = i3 + 1;
            this.C01O0C[i3] = c1O10Cl0382.C11013l3 != null ? c1O10Cl0382.C11013l3.C1l00I1 : -1;
            int i5 = i4 + 1;
            this.C01O0C[i4] = c1O10Cl0382.C11ll3;
            int i6 = i5 + 1;
            this.C01O0C[i5] = c1O10Cl0382.C18Cl1C;
            int i7 = i6 + 1;
            this.C01O0C[i6] = c1O10Cl0382.C1l00I1;
            int i8 = i7 + 1;
            this.C01O0C[i7] = c1O10Cl0382.C1O10Cl038;
            if (c1O10Cl0382.C1OC33O0lO81 != null) {
                int size = c1O10Cl0382.C1OC33O0lO81.size();
                int i9 = i8 + 1;
                this.C01O0C[i8] = size;
                int i10 = 0;
                while (i10 < size) {
                    this.C01O0C[i9] = ((Fragment) c1O10Cl0382.C1OC33O0lO81.get(i10)).C1l00I1;
                    i10++;
                    i9++;
                }
                i2 = i9;
            } else {
                i2 = i8 + 1;
                this.C01O0C[i8] = 0;
            }
        }
        this.C0I1O3C3lI8 = c11013l3.C1OC33O0lO81;
        this.C101lC8O = c11013l3.C3C1C0I8l3;
        this.C11013l3 = c11013l3.C3l3O8lIOIO8;
        this.C11ll3 = c11013l3.C831O13C118;
        this.C18Cl1C = c11013l3.C8CI00;
        this.C1l00I1 = c11013l3.CC38COI1l3I;
        this.C1O10Cl038 = c11013l3.CC8IOI1II0;
        this.C1OC33O0lO81 = c11013l3.CCC3CC0l;
        this.C3C1C0I8l3 = c11013l3.CI0I8l333131;
        this.C3CIO118 = c11013l3.CI3C103l01O;
    }

    public C11013l3 C01O0C(CCC3CC0l cCC3CC0l) {
        C11013l3 c11013l3 = new C11013l3(cCC3CC0l);
        int i = 0;
        int i2 = 0;
        while (i2 < this.C01O0C.length) {
            C1O10Cl038 c1O10Cl038 = new C1O10Cl038();
            int i3 = i2 + 1;
            c1O10Cl038.C101lC8O = this.C01O0C[i2];
            if (CCC3CC0l.C01O0C) {
                Log.v("FragmentManager", "Instantiate " + c11013l3 + " op #" + i + " base fragment #" + this.C01O0C[i3]);
            }
            int i4 = i3 + 1;
            int i5 = this.C01O0C[i3];
            if (i5 >= 0) {
                c1O10Cl038.C11013l3 = (Fragment) cCC3CC0l.C18Cl1C.get(i5);
            } else {
                c1O10Cl038.C11013l3 = null;
            }
            int i6 = i4 + 1;
            c1O10Cl038.C11ll3 = this.C01O0C[i4];
            int i7 = i6 + 1;
            c1O10Cl038.C18Cl1C = this.C01O0C[i6];
            int i8 = i7 + 1;
            c1O10Cl038.C1l00I1 = this.C01O0C[i7];
            int i9 = i8 + 1;
            c1O10Cl038.C1O10Cl038 = this.C01O0C[i8];
            int i10 = i9 + 1;
            int i11 = this.C01O0C[i9];
            if (i11 > 0) {
                c1O10Cl038.C1OC33O0lO81 = new ArrayList(i11);
                int i12 = 0;
                while (i12 < i11) {
                    if (CCC3CC0l.C01O0C) {
                        Log.v("FragmentManager", "Instantiate " + c11013l3 + " set remove fragment #" + this.C01O0C[i10]);
                    }
                    c1O10Cl038.C1OC33O0lO81.add((Fragment) cCC3CC0l.C18Cl1C.get(this.C01O0C[i10]));
                    i12++;
                    i10++;
                }
            }
            c11013l3.C01O0C(c1O10Cl038);
            i++;
            i2 = i10;
        }
        c11013l3.C1OC33O0lO81 = this.C0I1O3C3lI8;
        c11013l3.C3C1C0I8l3 = this.C101lC8O;
        c11013l3.C3l3O8lIOIO8 = this.C11013l3;
        c11013l3.C831O13C118 = this.C11ll3;
        c11013l3.C3CIO118 = true;
        c11013l3.C8CI00 = this.C18Cl1C;
        c11013l3.CC38COI1l3I = this.C1l00I1;
        c11013l3.CC8IOI1II0 = this.C1O10Cl038;
        c11013l3.CCC3CC0l = this.C1OC33O0lO81;
        c11013l3.CI0I8l333131 = this.C3C1C0I8l3;
        c11013l3.CI3C103l01O = this.C3CIO118;
        c11013l3.C01O0C(1);
        return c11013l3;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeIntArray(this.C01O0C);
        parcel.writeInt(this.C0I1O3C3lI8);
        parcel.writeInt(this.C101lC8O);
        parcel.writeString(this.C11013l3);
        parcel.writeInt(this.C11ll3);
        parcel.writeInt(this.C18Cl1C);
        TextUtils.writeToParcel(this.C1l00I1, parcel, 0);
        parcel.writeInt(this.C1O10Cl038);
        TextUtils.writeToParcel(this.C1OC33O0lO81, parcel, 0);
        parcel.writeStringList(this.C3C1C0I8l3);
        parcel.writeStringList(this.C3CIO118);
    }
}
