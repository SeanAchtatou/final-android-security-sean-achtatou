package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class C18Cl1C extends ViewGroup.MarginLayoutParams {
    public int C01O0C = 0;
    float C0I1O3C3lI8;
    boolean C101lC8O;
    boolean C11013l3;

    public C18Cl1C(int i, int i2) {
        super(i, i2);
    }

    public C18Cl1C(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.C0I1O3C3lI8);
        this.C01O0C = obtainStyledAttributes.getInt(0, 0);
        obtainStyledAttributes.recycle();
    }

    public C18Cl1C(C18Cl1C c18Cl1C) {
        super((ViewGroup.MarginLayoutParams) c18Cl1C);
        this.C01O0C = c18Cl1C.C01O0C;
    }

    public C18Cl1C(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public C18Cl1C(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }
}
