package android.support.v4.widget;

import android.view.View;

class C1O10Cl038 extends CO1830lI8C03 {
    final /* synthetic */ DrawerLayout C01O0C;
    private final int C0I1O3C3lI8;
    private ClO80C3lOO8 C101lC8O;
    private final Runnable C11013l3;

    private void C0I1O3C3lI8() {
        int i = 3;
        if (this.C0I1O3C3lI8 == 3) {
            i = 5;
        }
        View C01O0C2 = this.C01O0C.C01O0C(i);
        if (C01O0C2 != null) {
            this.C01O0C.C1OC33O0lO81(C01O0C2);
        }
    }

    public int C01O0C(View view) {
        if (this.C01O0C.C1l00I1(view)) {
            return view.getWidth();
        }
        return 0;
    }

    public int C01O0C(View view, int i, int i2) {
        if (this.C01O0C.C01O0C(view, 3)) {
            return Math.max(-view.getWidth(), Math.min(i, 0));
        }
        int width = this.C01O0C.getWidth();
        return Math.max(width - view.getWidth(), Math.min(i, width));
    }

    public void C01O0C() {
        this.C01O0C.removeCallbacks(this.C11013l3);
    }

    public void C01O0C(int i) {
        this.C01O0C.C01O0C(this.C0I1O3C3lI8, i, this.C101lC8O.C101lC8O());
    }

    public void C01O0C(int i, int i2) {
        this.C01O0C.postDelayed(this.C11013l3, 160);
    }

    public void C01O0C(View view, float f, float f2) {
        int width;
        float C11013l32 = this.C01O0C.C11013l3(view);
        int width2 = view.getWidth();
        if (this.C01O0C.C01O0C(view, 3)) {
            width = (f > 0.0f || (f == 0.0f && C11013l32 > 0.5f)) ? 0 : -width2;
        } else {
            width = this.C01O0C.getWidth();
            if (f < 0.0f || (f == 0.0f && C11013l32 > 0.5f)) {
                width -= width2;
            }
        }
        this.C101lC8O.C01O0C(width, view.getTop());
        this.C01O0C.invalidate();
    }

    public void C01O0C(View view, int i, int i2, int i3, int i4) {
        int width = view.getWidth();
        float width2 = this.C01O0C.C01O0C(view, 3) ? ((float) (width + i)) / ((float) width) : ((float) (this.C01O0C.getWidth() - i)) / ((float) width);
        this.C01O0C.C0I1O3C3lI8(view, width2);
        view.setVisibility(width2 == 0.0f ? 4 : 0);
        this.C01O0C.invalidate();
    }

    public boolean C01O0C(View view, int i) {
        return this.C01O0C.C1l00I1(view) && this.C01O0C.C01O0C(view, this.C0I1O3C3lI8) && this.C01O0C.C01O0C(view) == 0;
    }

    public int C0I1O3C3lI8(View view, int i, int i2) {
        return view.getTop();
    }

    public void C0I1O3C3lI8(int i, int i2) {
        View C01O0C2 = (i & 1) == 1 ? this.C01O0C.C01O0C(3) : this.C01O0C.C01O0C(5);
        if (C01O0C2 != null && this.C01O0C.C01O0C(C01O0C2) == 0) {
            this.C101lC8O.C01O0C(C01O0C2, i2);
        }
    }

    public void C0I1O3C3lI8(View view, int i) {
        ((C18Cl1C) view.getLayoutParams()).C101lC8O = false;
        C0I1O3C3lI8();
    }

    public boolean C0I1O3C3lI8(int i) {
        return false;
    }
}
