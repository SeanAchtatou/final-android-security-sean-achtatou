package android.support.v4.widget;

import android.graphics.Canvas;
import android.os.Build;

public class C3C1C0I8l3 {
    private static final C3l3O8lIOIO8 C0I1O3C3lI8;
    private Object C01O0C;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            C0I1O3C3lI8 = new C3ICl0OOl();
        } else {
            C0I1O3C3lI8 = new C3CIO118();
        }
    }

    public void C01O0C(int i, int i2) {
        C0I1O3C3lI8.C01O0C(this.C01O0C, i, i2);
    }

    public boolean C01O0C() {
        return C0I1O3C3lI8.C01O0C(this.C01O0C);
    }

    public boolean C01O0C(float f) {
        return C0I1O3C3lI8.C01O0C(this.C01O0C, f);
    }

    public boolean C01O0C(Canvas canvas) {
        return C0I1O3C3lI8.C01O0C(this.C01O0C, canvas);
    }

    public void C0I1O3C3lI8() {
        C0I1O3C3lI8.C0I1O3C3lI8(this.C01O0C);
    }

    public boolean C101lC8O() {
        return C0I1O3C3lI8.C101lC8O(this.C01O0C);
    }
}
