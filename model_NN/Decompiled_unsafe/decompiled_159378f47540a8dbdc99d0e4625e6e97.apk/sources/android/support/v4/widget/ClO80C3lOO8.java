package android.support.v4.widget;

import android.support.v4.view.CCC3CC0l;
import android.support.v4.view.ClC13lIl;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import java.util.Arrays;

public class ClO80C3lOO8 {
    private static final Interpolator CII3C813OIC8 = new CO081lO0OC0();
    private int C01O0C;
    private int C0I1O3C3lI8;
    private int C101lC8O;
    private float[] C11013l3;
    private float[] C11ll3;
    private float[] C18Cl1C;
    private int[] C1O10Cl038;
    private int[] C1OC33O0lO81;
    private float[] C1l00I1;
    private int[] C3C1C0I8l3;
    private int C3CIO118;
    private VelocityTracker C3ICl0OOl;
    private float C3l3O8lIOIO8;
    private float C3llC38O1;
    private int C831O13C118;
    private int C8CI00;
    private C831O13C118 CC38COI1l3I;
    private final CO1830lI8C03 CC8IOI1II0;
    private View CCC3CC0l;
    private boolean CI0I8l333131;
    private final ViewGroup CI3C103l01O;
    private final Runnable CIOC8C;

    private float C01O0C(float f) {
        return (float) Math.sin((double) ((float) (((double) (f - 0.5f)) * 0.4712389167638204d)));
    }

    private float C01O0C(float f, float f2, float f3) {
        float abs = Math.abs(f);
        if (abs < f2) {
            return 0.0f;
        }
        return abs > f3 ? f <= 0.0f ? -f3 : f3 : f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private int C01O0C(int i, int i2, int i3) {
        if (i == 0) {
            return 0;
        }
        int width = this.CI3C103l01O.getWidth();
        int i4 = width / 2;
        float C01O0C2 = (C01O0C(Math.min(1.0f, ((float) Math.abs(i)) / ((float) width))) * ((float) i4)) + ((float) i4);
        int abs = Math.abs(i2);
        return Math.min(abs > 0 ? Math.round(Math.abs(C01O0C2 / ((float) abs)) * 1000.0f) * 4 : (int) (((((float) Math.abs(i)) / ((float) i3)) + 1.0f) * 256.0f), 600);
    }

    private int C01O0C(View view, int i, int i2, int i3, int i4) {
        int C0I1O3C3lI82 = C0I1O3C3lI8(i3, (int) this.C3llC38O1, (int) this.C3l3O8lIOIO8);
        int C0I1O3C3lI83 = C0I1O3C3lI8(i4, (int) this.C3llC38O1, (int) this.C3l3O8lIOIO8);
        int abs = Math.abs(i);
        int abs2 = Math.abs(i2);
        int abs3 = Math.abs(C0I1O3C3lI82);
        int abs4 = Math.abs(C0I1O3C3lI83);
        int i5 = abs3 + abs4;
        int i6 = abs + abs2;
        return (int) (((C0I1O3C3lI83 != 0 ? ((float) abs4) / ((float) i5) : ((float) abs2) / ((float) i6)) * ((float) C01O0C(i2, C0I1O3C3lI83, this.CC8IOI1II0.C0I1O3C3lI8(view)))) + ((C0I1O3C3lI82 != 0 ? ((float) abs3) / ((float) i5) : ((float) abs) / ((float) i6)) * ((float) C01O0C(i, C0I1O3C3lI82, this.CC8IOI1II0.C01O0C(view)))));
    }

    private void C01O0C(float f, float f2) {
        this.CI0I8l333131 = true;
        this.CC8IOI1II0.C01O0C(this.CCC3CC0l, f, f2);
        this.CI0I8l333131 = false;
        if (this.C01O0C == 1) {
            C101lC8O(0);
        }
    }

    private void C01O0C(float f, float f2, int i) {
        C18Cl1C(i);
        float[] fArr = this.C11013l3;
        this.C18Cl1C[i] = f;
        fArr[i] = f;
        float[] fArr2 = this.C11ll3;
        this.C1l00I1[i] = f2;
        fArr2[i] = f2;
        this.C1O10Cl038[i] = C11ll3((int) f, (int) f2);
        this.C3CIO118 |= 1 << i;
    }

    private boolean C01O0C(float f, float f2, int i, int i2) {
        float abs = Math.abs(f);
        float abs2 = Math.abs(f2);
        if ((this.C1O10Cl038[i] & i2) != i2 || (this.C8CI00 & i2) == 0 || (this.C3C1C0I8l3[i] & i2) == i2 || (this.C1OC33O0lO81[i] & i2) == i2) {
            return false;
        }
        if (abs <= ((float) this.C0I1O3C3lI8) && abs2 <= ((float) this.C0I1O3C3lI8)) {
            return false;
        }
        if (abs >= abs2 * 0.5f || !this.CC8IOI1II0.C0I1O3C3lI8(i2)) {
            return (this.C1OC33O0lO81[i] & i2) == 0 && abs > ((float) this.C0I1O3C3lI8);
        }
        int[] iArr = this.C3C1C0I8l3;
        iArr[i] = iArr[i] | i2;
        return false;
    }

    private boolean C01O0C(int i, int i2, int i3, int i4) {
        int left = this.CCC3CC0l.getLeft();
        int top = this.CCC3CC0l.getTop();
        int i5 = i - left;
        int i6 = i2 - top;
        if (i5 == 0 && i6 == 0) {
            this.CC38COI1l3I.C18Cl1C();
            C101lC8O(0);
            return false;
        }
        this.CC38COI1l3I.C01O0C(left, top, i5, i6, C01O0C(this.CCC3CC0l, i5, i6, i3, i4));
        C101lC8O(2);
        return true;
    }

    private boolean C01O0C(View view, float f, float f2) {
        if (view == null) {
            return false;
        }
        boolean z = this.CC8IOI1II0.C01O0C(view) > 0;
        boolean z2 = this.CC8IOI1II0.C0I1O3C3lI8(view) > 0;
        if (z && z2) {
            return (f * f) + (f2 * f2) > ((float) (this.C0I1O3C3lI8 * this.C0I1O3C3lI8));
        }
        if (z) {
            return Math.abs(f) > ((float) this.C0I1O3C3lI8);
        }
        if (z2) {
            return Math.abs(f2) > ((float) this.C0I1O3C3lI8);
        }
        return false;
    }

    private int C0I1O3C3lI8(int i, int i2, int i3) {
        int abs = Math.abs(i);
        if (abs < i2) {
            return 0;
        }
        return abs > i3 ? i <= 0 ? -i3 : i3 : i;
    }

    private void C0I1O3C3lI8(float f, float f2, int i) {
        int i2 = 1;
        if (!C01O0C(f, f2, i, 1)) {
            i2 = 0;
        }
        if (C01O0C(f2, f, i, 4)) {
            i2 |= 4;
        }
        if (C01O0C(f, f2, i, 2)) {
            i2 |= 2;
        }
        if (C01O0C(f2, f, i, 8)) {
            i2 |= 8;
        }
        if (i2 != 0) {
            int[] iArr = this.C1OC33O0lO81;
            iArr[i] = iArr[i] | i2;
            this.CC8IOI1II0.C0I1O3C3lI8(i2, i);
        }
    }

    private void C0I1O3C3lI8(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int left = this.CCC3CC0l.getLeft();
        int top = this.CCC3CC0l.getTop();
        if (i3 != 0) {
            i5 = this.CC8IOI1II0.C01O0C(this.CCC3CC0l, i, i3);
            this.CCC3CC0l.offsetLeftAndRight(i5 - left);
        } else {
            i5 = i;
        }
        if (i4 != 0) {
            i6 = this.CC8IOI1II0.C0I1O3C3lI8(this.CCC3CC0l, i2, i4);
            this.CCC3CC0l.offsetTopAndBottom(i6 - top);
        } else {
            i6 = i2;
        }
        if (i3 != 0 || i4 != 0) {
            this.CC8IOI1II0.C01O0C(this.CCC3CC0l, i5, i6, i5 - left, i6 - top);
        }
    }

    private void C101lC8O(MotionEvent motionEvent) {
        int C101lC8O2 = CCC3CC0l.C101lC8O(motionEvent);
        for (int i = 0; i < C101lC8O2; i++) {
            int C0I1O3C3lI82 = CCC3CC0l.C0I1O3C3lI8(motionEvent, i);
            float C101lC8O3 = CCC3CC0l.C101lC8O(motionEvent, i);
            float C11013l32 = CCC3CC0l.C11013l3(motionEvent, i);
            this.C18Cl1C[C0I1O3C3lI82] = C101lC8O3;
            this.C1l00I1[C0I1O3C3lI82] = C11013l32;
        }
    }

    private int C11ll3(int i, int i2) {
        int i3 = 0;
        if (i < this.CI3C103l01O.getLeft() + this.C831O13C118) {
            i3 = 1;
        }
        if (i2 < this.CI3C103l01O.getTop() + this.C831O13C118) {
            i3 |= 4;
        }
        if (i > this.CI3C103l01O.getRight() - this.C831O13C118) {
            i3 |= 2;
        }
        return i2 > this.CI3C103l01O.getBottom() - this.C831O13C118 ? i3 | 8 : i3;
    }

    private void C11ll3(int i) {
        if (this.C11013l3 != null) {
            this.C11013l3[i] = 0.0f;
            this.C11ll3[i] = 0.0f;
            this.C18Cl1C[i] = 0.0f;
            this.C1l00I1[i] = 0.0f;
            this.C1O10Cl038[i] = 0;
            this.C1OC33O0lO81[i] = 0;
            this.C3C1C0I8l3[i] = 0;
            this.C3CIO118 &= (1 << i) ^ -1;
        }
    }

    private void C18Cl1C(int i) {
        if (this.C11013l3 == null || this.C11013l3.length <= i) {
            float[] fArr = new float[(i + 1)];
            float[] fArr2 = new float[(i + 1)];
            float[] fArr3 = new float[(i + 1)];
            float[] fArr4 = new float[(i + 1)];
            int[] iArr = new int[(i + 1)];
            int[] iArr2 = new int[(i + 1)];
            int[] iArr3 = new int[(i + 1)];
            if (this.C11013l3 != null) {
                System.arraycopy(this.C11013l3, 0, fArr, 0, this.C11013l3.length);
                System.arraycopy(this.C11ll3, 0, fArr2, 0, this.C11ll3.length);
                System.arraycopy(this.C18Cl1C, 0, fArr3, 0, this.C18Cl1C.length);
                System.arraycopy(this.C1l00I1, 0, fArr4, 0, this.C1l00I1.length);
                System.arraycopy(this.C1O10Cl038, 0, iArr, 0, this.C1O10Cl038.length);
                System.arraycopy(this.C1OC33O0lO81, 0, iArr2, 0, this.C1OC33O0lO81.length);
                System.arraycopy(this.C3C1C0I8l3, 0, iArr3, 0, this.C3C1C0I8l3.length);
            }
            this.C11013l3 = fArr;
            this.C11ll3 = fArr2;
            this.C18Cl1C = fArr3;
            this.C1l00I1 = fArr4;
            this.C1O10Cl038 = iArr;
            this.C1OC33O0lO81 = iArr2;
            this.C3C1C0I8l3 = iArr3;
        }
    }

    private void C1O10Cl038() {
        this.C3ICl0OOl.computeCurrentVelocity(1000, this.C3l3O8lIOIO8);
        C01O0C(C01O0C(ClC13lIl.C01O0C(this.C3ICl0OOl, this.C101lC8O), this.C3llC38O1, this.C3l3O8lIOIO8), C01O0C(ClC13lIl.C0I1O3C3lI8(this.C3ICl0OOl, this.C101lC8O), this.C3llC38O1, this.C3l3O8lIOIO8));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(float[], float):void}
     arg types: [float[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void} */
    private void C1l00I1() {
        if (this.C11013l3 != null) {
            Arrays.fill(this.C11013l3, 0.0f);
            Arrays.fill(this.C11ll3, 0.0f);
            Arrays.fill(this.C18Cl1C, 0.0f);
            Arrays.fill(this.C1l00I1, 0.0f);
            Arrays.fill(this.C1O10Cl038, 0);
            Arrays.fill(this.C1OC33O0lO81, 0);
            Arrays.fill(this.C3C1C0I8l3, 0);
            this.C3CIO118 = 0;
        }
    }

    public int C01O0C() {
        return this.C01O0C;
    }

    public void C01O0C(int i) {
        this.C8CI00 = i;
    }

    public void C01O0C(View view, int i) {
        if (view.getParent() != this.CI3C103l01O) {
            throw new IllegalArgumentException("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (" + this.CI3C103l01O + ")");
        }
        this.CCC3CC0l = view;
        this.C101lC8O = i;
        this.CC8IOI1II0.C0I1O3C3lI8(view, i);
        C101lC8O(1);
    }

    public boolean C01O0C(int i, int i2) {
        if (this.CI0I8l333131) {
            return C01O0C(i, i2, (int) ClC13lIl.C01O0C(this.C3ICl0OOl, this.C101lC8O), (int) ClC13lIl.C0I1O3C3lI8(this.C3ICl0OOl, this.C101lC8O));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ed, code lost:
        if (r8 != r7) goto L_0x00fc;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean C01O0C(android.view.MotionEvent r14) {
        /*
            r13 = this;
            int r0 = android.support.v4.view.CCC3CC0l.C01O0C(r14)
            int r1 = android.support.v4.view.CCC3CC0l.C0I1O3C3lI8(r14)
            if (r0 != 0) goto L_0x000d
            r13.C11ll3()
        L_0x000d:
            android.view.VelocityTracker r2 = r13.C3ICl0OOl
            if (r2 != 0) goto L_0x0017
            android.view.VelocityTracker r2 = android.view.VelocityTracker.obtain()
            r13.C3ICl0OOl = r2
        L_0x0017:
            android.view.VelocityTracker r2 = r13.C3ICl0OOl
            r2.addMovement(r14)
            switch(r0) {
                case 0: goto L_0x0026;
                case 1: goto L_0x0119;
                case 2: goto L_0x0092;
                case 3: goto L_0x0119;
                case 4: goto L_0x001f;
                case 5: goto L_0x005a;
                case 6: goto L_0x0110;
                default: goto L_0x001f;
            }
        L_0x001f:
            int r0 = r13.C01O0C
            r1 = 1
            if (r0 != r1) goto L_0x011e
            r0 = 1
        L_0x0025:
            return r0
        L_0x0026:
            float r0 = r14.getX()
            float r1 = r14.getY()
            r2 = 0
            int r2 = android.support.v4.view.CCC3CC0l.C0I1O3C3lI8(r14, r2)
            r13.C01O0C(r0, r1, r2)
            int r0 = (int) r0
            int r1 = (int) r1
            android.view.View r0 = r13.C11013l3(r0, r1)
            android.view.View r1 = r13.CCC3CC0l
            if (r0 != r1) goto L_0x0048
            int r1 = r13.C01O0C
            r3 = 2
            if (r1 != r3) goto L_0x0048
            r13.C0I1O3C3lI8(r0, r2)
        L_0x0048:
            int[] r0 = r13.C1O10Cl038
            r0 = r0[r2]
            int r1 = r13.C8CI00
            r1 = r1 & r0
            if (r1 == 0) goto L_0x001f
            android.support.v4.widget.CO1830lI8C03 r1 = r13.CC8IOI1II0
            int r3 = r13.C8CI00
            r0 = r0 & r3
            r1.C01O0C(r0, r2)
            goto L_0x001f
        L_0x005a:
            int r0 = android.support.v4.view.CCC3CC0l.C0I1O3C3lI8(r14, r1)
            float r2 = android.support.v4.view.CCC3CC0l.C101lC8O(r14, r1)
            float r1 = android.support.v4.view.CCC3CC0l.C11013l3(r14, r1)
            r13.C01O0C(r2, r1, r0)
            int r3 = r13.C01O0C
            if (r3 != 0) goto L_0x007f
            int[] r1 = r13.C1O10Cl038
            r1 = r1[r0]
            int r2 = r13.C8CI00
            r2 = r2 & r1
            if (r2 == 0) goto L_0x001f
            android.support.v4.widget.CO1830lI8C03 r2 = r13.CC8IOI1II0
            int r3 = r13.C8CI00
            r1 = r1 & r3
            r2.C01O0C(r1, r0)
            goto L_0x001f
        L_0x007f:
            int r3 = r13.C01O0C
            r4 = 2
            if (r3 != r4) goto L_0x001f
            int r2 = (int) r2
            int r1 = (int) r1
            android.view.View r1 = r13.C11013l3(r2, r1)
            android.view.View r2 = r13.CCC3CC0l
            if (r1 != r2) goto L_0x001f
            r13.C0I1O3C3lI8(r1, r0)
            goto L_0x001f
        L_0x0092:
            int r2 = android.support.v4.view.CCC3CC0l.C101lC8O(r14)
            r0 = 0
            r1 = r0
        L_0x0098:
            if (r1 >= r2) goto L_0x00f5
            int r3 = android.support.v4.view.CCC3CC0l.C0I1O3C3lI8(r14, r1)
            float r0 = android.support.v4.view.CCC3CC0l.C101lC8O(r14, r1)
            float r4 = android.support.v4.view.CCC3CC0l.C11013l3(r14, r1)
            float[] r5 = r13.C11013l3
            r5 = r5[r3]
            float r5 = r0 - r5
            float[] r6 = r13.C11ll3
            r6 = r6[r3]
            float r6 = r4 - r6
            int r0 = (int) r0
            int r4 = (int) r4
            android.view.View r4 = r13.C11013l3(r0, r4)
            if (r4 == 0) goto L_0x00fa
            boolean r0 = r13.C01O0C(r4, r5, r6)
            if (r0 == 0) goto L_0x00fa
            r0 = 1
        L_0x00c1:
            if (r0 == 0) goto L_0x00fc
            int r7 = r4.getLeft()
            int r8 = (int) r5
            int r8 = r8 + r7
            android.support.v4.widget.CO1830lI8C03 r9 = r13.CC8IOI1II0
            int r10 = (int) r5
            int r8 = r9.C01O0C(r4, r8, r10)
            int r9 = r4.getTop()
            int r10 = (int) r6
            int r10 = r10 + r9
            android.support.v4.widget.CO1830lI8C03 r11 = r13.CC8IOI1II0
            int r12 = (int) r6
            int r10 = r11.C0I1O3C3lI8(r4, r10, r12)
            android.support.v4.widget.CO1830lI8C03 r11 = r13.CC8IOI1II0
            int r11 = r11.C01O0C(r4)
            android.support.v4.widget.CO1830lI8C03 r12 = r13.CC8IOI1II0
            int r12 = r12.C0I1O3C3lI8(r4)
            if (r11 == 0) goto L_0x00ef
            if (r11 <= 0) goto L_0x00fc
            if (r8 != r7) goto L_0x00fc
        L_0x00ef:
            if (r12 == 0) goto L_0x00f5
            if (r12 <= 0) goto L_0x00fc
            if (r10 != r9) goto L_0x00fc
        L_0x00f5:
            r13.C101lC8O(r14)
            goto L_0x001f
        L_0x00fa:
            r0 = 0
            goto L_0x00c1
        L_0x00fc:
            r13.C0I1O3C3lI8(r5, r6, r3)
            int r5 = r13.C01O0C
            r6 = 1
            if (r5 == r6) goto L_0x00f5
            if (r0 == 0) goto L_0x010c
            boolean r0 = r13.C0I1O3C3lI8(r4, r3)
            if (r0 != 0) goto L_0x00f5
        L_0x010c:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0098
        L_0x0110:
            int r0 = android.support.v4.view.CCC3CC0l.C0I1O3C3lI8(r14, r1)
            r13.C11ll3(r0)
            goto L_0x001f
        L_0x0119:
            r13.C11ll3()
            goto L_0x001f
        L_0x011e:
            r0 = 0
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.ClO80C3lOO8.C01O0C(android.view.MotionEvent):boolean");
    }

    public boolean C01O0C(View view, int i, int i2) {
        this.CCC3CC0l = view;
        this.C101lC8O = -1;
        boolean C01O0C2 = C01O0C(i, i2, 0, 0);
        if (!C01O0C2 && this.C01O0C == 0 && this.CCC3CC0l != null) {
            this.CCC3CC0l = null;
        }
        return C01O0C2;
    }

    public boolean C01O0C(boolean z) {
        boolean z2;
        if (this.C01O0C == 2) {
            boolean C11ll32 = this.CC38COI1l3I.C11ll3();
            int C01O0C2 = this.CC38COI1l3I.C01O0C();
            int C0I1O3C3lI82 = this.CC38COI1l3I.C0I1O3C3lI8();
            int left = C01O0C2 - this.CCC3CC0l.getLeft();
            int top = C0I1O3C3lI82 - this.CCC3CC0l.getTop();
            if (left != 0) {
                this.CCC3CC0l.offsetLeftAndRight(left);
            }
            if (top != 0) {
                this.CCC3CC0l.offsetTopAndBottom(top);
            }
            if (!(left == 0 && top == 0)) {
                this.CC8IOI1II0.C01O0C(this.CCC3CC0l, C01O0C2, C0I1O3C3lI82, left, top);
            }
            if (C11ll32 && C01O0C2 == this.CC38COI1l3I.C101lC8O() && C0I1O3C3lI82 == this.CC38COI1l3I.C11013l3()) {
                this.CC38COI1l3I.C18Cl1C();
                z2 = false;
            } else {
                z2 = C11ll32;
            }
            if (!z2) {
                if (z) {
                    this.CI3C103l01O.post(this.CIOC8C);
                } else {
                    C101lC8O(0);
                }
            }
        }
        return this.C01O0C == 2;
    }

    public int C0I1O3C3lI8() {
        return this.C831O13C118;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.ClO80C3lOO8.C01O0C(float, float):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.ClO80C3lOO8.C01O0C(android.view.View, int):void
      android.support.v4.widget.ClO80C3lOO8.C01O0C(int, int):boolean
      android.support.v4.widget.ClO80C3lOO8.C01O0C(float, float):void */
    public void C0I1O3C3lI8(MotionEvent motionEvent) {
        int i;
        int i2 = 0;
        int C01O0C2 = CCC3CC0l.C01O0C(motionEvent);
        int C0I1O3C3lI82 = CCC3CC0l.C0I1O3C3lI8(motionEvent);
        if (C01O0C2 == 0) {
            C11ll3();
        }
        if (this.C3ICl0OOl == null) {
            this.C3ICl0OOl = VelocityTracker.obtain();
        }
        this.C3ICl0OOl.addMovement(motionEvent);
        switch (C01O0C2) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                int C0I1O3C3lI83 = CCC3CC0l.C0I1O3C3lI8(motionEvent, 0);
                View C11013l32 = C11013l3((int) x, (int) y);
                C01O0C(x, y, C0I1O3C3lI83);
                C0I1O3C3lI8(C11013l32, C0I1O3C3lI83);
                int i3 = this.C1O10Cl038[C0I1O3C3lI83];
                if ((this.C8CI00 & i3) != 0) {
                    this.CC8IOI1II0.C01O0C(i3 & this.C8CI00, C0I1O3C3lI83);
                    return;
                }
                return;
            case 1:
                if (this.C01O0C == 1) {
                    C1O10Cl038();
                }
                C11ll3();
                return;
            case 2:
                if (this.C01O0C == 1) {
                    int C01O0C3 = CCC3CC0l.C01O0C(motionEvent, this.C101lC8O);
                    float C101lC8O2 = CCC3CC0l.C101lC8O(motionEvent, C01O0C3);
                    float C11013l33 = CCC3CC0l.C11013l3(motionEvent, C01O0C3);
                    int i4 = (int) (C101lC8O2 - this.C18Cl1C[this.C101lC8O]);
                    int i5 = (int) (C11013l33 - this.C1l00I1[this.C101lC8O]);
                    C0I1O3C3lI8(this.CCC3CC0l.getLeft() + i4, this.CCC3CC0l.getTop() + i5, i4, i5);
                    C101lC8O(motionEvent);
                    return;
                }
                int C101lC8O3 = CCC3CC0l.C101lC8O(motionEvent);
                while (i2 < C101lC8O3) {
                    int C0I1O3C3lI84 = CCC3CC0l.C0I1O3C3lI8(motionEvent, i2);
                    float C101lC8O4 = CCC3CC0l.C101lC8O(motionEvent, i2);
                    float C11013l34 = CCC3CC0l.C11013l3(motionEvent, i2);
                    float f = C101lC8O4 - this.C11013l3[C0I1O3C3lI84];
                    float f2 = C11013l34 - this.C11ll3[C0I1O3C3lI84];
                    C0I1O3C3lI8(f, f2, C0I1O3C3lI84);
                    if (this.C01O0C != 1) {
                        View C11013l35 = C11013l3((int) C101lC8O4, (int) C11013l34);
                        if (!C01O0C(C11013l35, f, f2) || !C0I1O3C3lI8(C11013l35, C0I1O3C3lI84)) {
                            i2++;
                        }
                    }
                    C101lC8O(motionEvent);
                    return;
                }
                C101lC8O(motionEvent);
                return;
            case 3:
                if (this.C01O0C == 1) {
                    C01O0C(0.0f, 0.0f);
                }
                C11ll3();
                return;
            case 4:
            default:
                return;
            case 5:
                int C0I1O3C3lI85 = CCC3CC0l.C0I1O3C3lI8(motionEvent, C0I1O3C3lI82);
                float C101lC8O5 = CCC3CC0l.C101lC8O(motionEvent, C0I1O3C3lI82);
                float C11013l36 = CCC3CC0l.C11013l3(motionEvent, C0I1O3C3lI82);
                C01O0C(C101lC8O5, C11013l36, C0I1O3C3lI85);
                if (this.C01O0C == 0) {
                    C0I1O3C3lI8(C11013l3((int) C101lC8O5, (int) C11013l36), C0I1O3C3lI85);
                    int i6 = this.C1O10Cl038[C0I1O3C3lI85];
                    if ((this.C8CI00 & i6) != 0) {
                        this.CC8IOI1II0.C01O0C(i6 & this.C8CI00, C0I1O3C3lI85);
                        return;
                    }
                    return;
                } else if (C101lC8O((int) C101lC8O5, (int) C11013l36)) {
                    C0I1O3C3lI8(this.CCC3CC0l, C0I1O3C3lI85);
                    return;
                } else {
                    return;
                }
            case 6:
                int C0I1O3C3lI86 = CCC3CC0l.C0I1O3C3lI8(motionEvent, C0I1O3C3lI82);
                if (this.C01O0C == 1 && C0I1O3C3lI86 == this.C101lC8O) {
                    int C101lC8O6 = CCC3CC0l.C101lC8O(motionEvent);
                    while (true) {
                        if (i2 >= C101lC8O6) {
                            i = -1;
                        } else {
                            int C0I1O3C3lI87 = CCC3CC0l.C0I1O3C3lI8(motionEvent, i2);
                            if (C0I1O3C3lI87 != this.C101lC8O) {
                                if (C11013l3((int) CCC3CC0l.C101lC8O(motionEvent, i2), (int) CCC3CC0l.C11013l3(motionEvent, i2)) == this.CCC3CC0l && C0I1O3C3lI8(this.CCC3CC0l, C0I1O3C3lI87)) {
                                    i = this.C101lC8O;
                                }
                            }
                            i2++;
                        }
                    }
                    if (i == -1) {
                        C1O10Cl038();
                    }
                }
                C11ll3(C0I1O3C3lI86);
                return;
        }
    }

    public boolean C0I1O3C3lI8(int i) {
        return (this.C3CIO118 & (1 << i)) != 0;
    }

    public boolean C0I1O3C3lI8(int i, int i2) {
        if (!C0I1O3C3lI8(i2)) {
            return false;
        }
        boolean z = (i & 1) == 1;
        boolean z2 = (i & 2) == 2;
        float f = this.C18Cl1C[i2] - this.C11013l3[i2];
        float f2 = this.C1l00I1[i2] - this.C11ll3[i2];
        if (z && z2) {
            return (f * f) + (f2 * f2) > ((float) (this.C0I1O3C3lI8 * this.C0I1O3C3lI8));
        }
        if (z) {
            return Math.abs(f) > ((float) this.C0I1O3C3lI8);
        }
        if (z2) {
            return Math.abs(f2) > ((float) this.C0I1O3C3lI8);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean C0I1O3C3lI8(View view, int i) {
        if (view == this.CCC3CC0l && this.C101lC8O == i) {
            return true;
        }
        if (view == null || !this.CC8IOI1II0.C01O0C(view, i)) {
            return false;
        }
        this.C101lC8O = i;
        C01O0C(view, i);
        return true;
    }

    public boolean C0I1O3C3lI8(View view, int i, int i2) {
        return view != null && i >= view.getLeft() && i < view.getRight() && i2 >= view.getTop() && i2 < view.getBottom();
    }

    public View C101lC8O() {
        return this.CCC3CC0l;
    }

    /* access modifiers changed from: package-private */
    public void C101lC8O(int i) {
        this.CI3C103l01O.removeCallbacks(this.CIOC8C);
        if (this.C01O0C != i) {
            this.C01O0C = i;
            this.CC8IOI1II0.C01O0C(i);
            if (this.C01O0C == 0) {
                this.CCC3CC0l = null;
            }
        }
    }

    public boolean C101lC8O(int i, int i2) {
        return C0I1O3C3lI8(this.CCC3CC0l, i, i2);
    }

    public int C11013l3() {
        return this.C0I1O3C3lI8;
    }

    public View C11013l3(int i, int i2) {
        for (int childCount = this.CI3C103l01O.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = this.CI3C103l01O.getChildAt(this.CC8IOI1II0.C101lC8O(childCount));
            if (i >= childAt.getLeft() && i < childAt.getRight() && i2 >= childAt.getTop() && i2 < childAt.getBottom()) {
                return childAt;
            }
        }
        return null;
    }

    public boolean C11013l3(int i) {
        int length = this.C11013l3.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (C0I1O3C3lI8(i, i2)) {
                return true;
            }
        }
        return false;
    }

    public void C11ll3() {
        this.C101lC8O = -1;
        C1l00I1();
        if (this.C3ICl0OOl != null) {
            this.C3ICl0OOl.recycle();
            this.C3ICl0OOl = null;
        }
    }

    public void C18Cl1C() {
        C11ll3();
        if (this.C01O0C == 2) {
            int C01O0C2 = this.CC38COI1l3I.C01O0C();
            int C0I1O3C3lI82 = this.CC38COI1l3I.C0I1O3C3lI8();
            this.CC38COI1l3I.C18Cl1C();
            int C01O0C3 = this.CC38COI1l3I.C01O0C();
            int C0I1O3C3lI83 = this.CC38COI1l3I.C0I1O3C3lI8();
            this.CC8IOI1II0.C01O0C(this.CCC3CC0l, C01O0C3, C0I1O3C3lI83, C01O0C3 - C01O0C2, C0I1O3C3lI83 - C0I1O3C3lI82);
        }
        C101lC8O(0);
    }
}
