package android.support.v4.widget;

import android.graphics.Canvas;
import android.widget.EdgeEffect;

class C3llC38O1 {
    public static void C01O0C(Object obj, int i, int i2) {
        ((EdgeEffect) obj).setSize(i, i2);
    }

    public static boolean C01O0C(Object obj) {
        return ((EdgeEffect) obj).isFinished();
    }

    public static boolean C01O0C(Object obj, float f) {
        ((EdgeEffect) obj).onPull(f);
        return true;
    }

    public static boolean C01O0C(Object obj, Canvas canvas) {
        return ((EdgeEffect) obj).draw(canvas);
    }

    public static void C0I1O3C3lI8(Object obj) {
        ((EdgeEffect) obj).finish();
    }

    public static boolean C101lC8O(Object obj) {
        EdgeEffect edgeEffect = (EdgeEffect) obj;
        edgeEffect.onRelease();
        return edgeEffect.isFinished();
    }
}
