package android.support.v4.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.CCC3CC0l;
import android.support.v4.view.CO88CO1Cl383;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class SlidingPaneLayout extends ViewGroup {
    static final CII3C813OIC8 C01O0C;
    private int C0I1O3C3lI8;
    private int C101lC8O;
    private Drawable C11013l3;
    private Drawable C11ll3;
    private final int C18Cl1C;
    private View C1O10Cl038;
    private float C1OC33O0lO81;
    private boolean C1l00I1;
    private float C3C1C0I8l3;
    private int C3CIO118;
    private boolean C3ICl0OOl;
    private int C3l3O8lIOIO8;
    private float C3llC38O1;
    private float C831O13C118;
    private CI0I8l333131 C8CI00;
    private final ClO80C3lOO8 CC38COI1l3I;
    private boolean CC8IOI1II0;
    private boolean CCC3CC0l;
    private final Rect CI0I8l333131;
    /* access modifiers changed from: private */
    public final ArrayList CI3C103l01O;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new CI3C103l01O();
        boolean C01O0C;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.C01O0C = parcel.readInt() != 0;
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.C01O0C ? 1 : 0);
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            C01O0C = new ClC13lIl();
        } else if (i >= 16) {
            C01O0C = new Cl80C0l838l();
        } else {
            C01O0C = new CIOC8C();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void C01O0C(float r10) {
        /*
            r9 = this;
            r1 = 0
            r8 = 1065353216(0x3f800000, float:1.0)
            boolean r3 = r9.C18Cl1C()
            android.view.View r0 = r9.C1O10Cl038
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            android.support.v4.widget.CCC3CC0l r0 = (android.support.v4.widget.CCC3CC0l) r0
            boolean r2 = r0.C101lC8O
            if (r2 == 0) goto L_0x0030
            if (r3 == 0) goto L_0x002d
            int r0 = r0.rightMargin
        L_0x0017:
            if (r0 > 0) goto L_0x0030
            r0 = 1
        L_0x001a:
            int r4 = r9.getChildCount()
            r2 = r1
        L_0x001f:
            if (r2 >= r4) goto L_0x005d
            android.view.View r5 = r9.getChildAt(r2)
            android.view.View r1 = r9.C1O10Cl038
            if (r5 != r1) goto L_0x0032
        L_0x0029:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x001f
        L_0x002d:
            int r0 = r0.leftMargin
            goto L_0x0017
        L_0x0030:
            r0 = r1
            goto L_0x001a
        L_0x0032:
            float r1 = r9.C3C1C0I8l3
            float r1 = r8 - r1
            int r6 = r9.C3l3O8lIOIO8
            float r6 = (float) r6
            float r1 = r1 * r6
            int r1 = (int) r1
            r9.C3C1C0I8l3 = r10
            float r6 = r8 - r10
            int r7 = r9.C3l3O8lIOIO8
            float r7 = (float) r7
            float r6 = r6 * r7
            int r6 = (int) r6
            int r1 = r1 - r6
            if (r3 == 0) goto L_0x0048
            int r1 = -r1
        L_0x0048:
            r5.offsetLeftAndRight(r1)
            if (r0 == 0) goto L_0x0029
            if (r3 == 0) goto L_0x0058
            float r1 = r9.C3C1C0I8l3
            float r1 = r1 - r8
        L_0x0052:
            int r6 = r9.C101lC8O
            r9.C01O0C(r5, r1, r6)
            goto L_0x0029
        L_0x0058:
            float r1 = r9.C3C1C0I8l3
            float r1 = r8 - r1
            goto L_0x0052
        L_0x005d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.SlidingPaneLayout.C01O0C(float):void");
    }

    private void C01O0C(View view, float f, int i) {
        CCC3CC0l cCC3CC0l = (CCC3CC0l) view.getLayoutParams();
        if (f > 0.0f && i != 0) {
            int i2 = (((int) (((float) ((-16777216 & i) >>> 24)) * f)) << 24) | (16777215 & i);
            if (cCC3CC0l.C11013l3 == null) {
                cCC3CC0l.C11013l3 = new Paint();
            }
            cCC3CC0l.C11013l3.setColorFilter(new PorterDuffColorFilter(i2, PorterDuff.Mode.SRC_OVER));
            if (CO88CO1Cl383.C101lC8O(view) != 2) {
                CO88CO1Cl383.C01O0C(view, 2, cCC3CC0l.C11013l3);
            }
            C11013l3(view);
        } else if (CO88CO1Cl383.C101lC8O(view) != 0) {
            if (cCC3CC0l.C11013l3 != null) {
                cCC3CC0l.C11013l3.setColorFilter(null);
            }
            CC8IOI1II0 cc8ioi1ii0 = new CC8IOI1II0(this, view);
            this.CI3C103l01O.add(cc8ioi1ii0);
            CO88CO1Cl383.C01O0C(this, cc8ioi1ii0);
        }
    }

    private boolean C01O0C(View view, int i) {
        if (!this.CCC3CC0l && !C01O0C(0.0f, i)) {
            return false;
        }
        this.CC8IOI1II0 = false;
        return true;
    }

    private boolean C0I1O3C3lI8(View view, int i) {
        if (!this.CCC3CC0l && !C01O0C(1.0f, i)) {
            return false;
        }
        this.CC8IOI1II0 = true;
        return true;
    }

    private static boolean C101lC8O(View view) {
        if (CO88CO1Cl383.C11ll3(view)) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            return false;
        }
        Drawable background = view.getBackground();
        if (background != null) {
            return background.getOpacity() == -1;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void C11013l3(View view) {
        C01O0C.C01O0C(this, view);
    }

    private boolean C18Cl1C() {
        return CO88CO1Cl383.C11013l3(this) == 1;
    }

    /* access modifiers changed from: package-private */
    public void C01O0C() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 4) {
                childAt.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(View view) {
        int i;
        int i2;
        int i3;
        int i4;
        boolean C18Cl1C2 = C18Cl1C();
        int width = C18Cl1C2 ? getWidth() - getPaddingRight() : getPaddingLeft();
        int paddingLeft = C18Cl1C2 ? getPaddingLeft() : getWidth() - getPaddingRight();
        int paddingTop = getPaddingTop();
        int height = getHeight() - getPaddingBottom();
        if (view == null || !C101lC8O(view)) {
            i = 0;
            i2 = 0;
            i3 = 0;
            i4 = 0;
        } else {
            i4 = view.getLeft();
            i3 = view.getRight();
            i2 = view.getTop();
            i = view.getBottom();
        }
        int childCount = getChildCount();
        int i5 = 0;
        while (i5 < childCount) {
            View childAt = getChildAt(i5);
            if (childAt != view) {
                childAt.setVisibility((Math.max(C18Cl1C2 ? paddingLeft : width, childAt.getLeft()) < i4 || Math.max(paddingTop, childAt.getTop()) < i2 || Math.min(C18Cl1C2 ? width : paddingLeft, childAt.getRight()) > i3 || Math.min(height, childAt.getBottom()) > i) ? 0 : 4);
                i5++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean C01O0C(float f, int i) {
        int paddingLeft;
        if (!this.C1l00I1) {
            return false;
        }
        boolean C18Cl1C2 = C18Cl1C();
        CCC3CC0l cCC3CC0l = (CCC3CC0l) this.C1O10Cl038.getLayoutParams();
        if (C18Cl1C2) {
            paddingLeft = (int) (((float) getWidth()) - ((((float) (cCC3CC0l.rightMargin + getPaddingRight())) + (((float) this.C3CIO118) * f)) + ((float) this.C1O10Cl038.getWidth())));
        } else {
            paddingLeft = (int) (((float) (cCC3CC0l.leftMargin + getPaddingLeft())) + (((float) this.C3CIO118) * f));
        }
        if (!this.CC38COI1l3I.C01O0C(this.C1O10Cl038, paddingLeft, this.C1O10Cl038.getTop())) {
            return false;
        }
        C01O0C();
        CO88CO1Cl383.C0I1O3C3lI8(this);
        return true;
    }

    public boolean C0I1O3C3lI8() {
        return C0I1O3C3lI8(this.C1O10Cl038, 0);
    }

    /* access modifiers changed from: package-private */
    public boolean C0I1O3C3lI8(View view) {
        if (view == null) {
            return false;
        }
        return this.C1l00I1 && ((CCC3CC0l) view.getLayoutParams()).C101lC8O && this.C1OC33O0lO81 > 0.0f;
    }

    public boolean C101lC8O() {
        return C01O0C(this.C1O10Cl038, 0);
    }

    public boolean C11013l3() {
        return !this.C1l00I1 || this.C1OC33O0lO81 == 1.0f;
    }

    public boolean C11ll3() {
        return this.C1l00I1;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof CCC3CC0l) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        if (!this.CC38COI1l3I.C01O0C(true)) {
            return;
        }
        if (!this.C1l00I1) {
            this.CC38COI1l3I.C18Cl1C();
        } else {
            CO88CO1Cl383.C0I1O3C3lI8(this);
        }
    }

    public void draw(Canvas canvas) {
        int left;
        int i;
        super.draw(canvas);
        Drawable drawable = C18Cl1C() ? this.C11ll3 : this.C11013l3;
        View childAt = getChildCount() > 1 ? getChildAt(1) : null;
        if (childAt != null && drawable != null) {
            int top = childAt.getTop();
            int bottom = childAt.getBottom();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            if (C18Cl1C()) {
                i = childAt.getRight();
                left = i + intrinsicWidth;
            } else {
                left = childAt.getLeft();
                i = left - intrinsicWidth;
            }
            drawable.setBounds(i, top, left, bottom);
            drawable.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        boolean drawChild;
        CCC3CC0l cCC3CC0l = (CCC3CC0l) view.getLayoutParams();
        int save = canvas.save(2);
        if (this.C1l00I1 && !cCC3CC0l.C0I1O3C3lI8 && this.C1O10Cl038 != null) {
            canvas.getClipBounds(this.CI0I8l333131);
            if (C18Cl1C()) {
                this.CI0I8l333131.left = Math.max(this.CI0I8l333131.left, this.C1O10Cl038.getRight());
            } else {
                this.CI0I8l333131.right = Math.min(this.CI0I8l333131.right, this.C1O10Cl038.getLeft());
            }
            canvas.clipRect(this.CI0I8l333131);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            drawChild = super.drawChild(canvas, view, j);
        } else if (!cCC3CC0l.C101lC8O || this.C1OC33O0lO81 <= 0.0f) {
            if (view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(false);
            }
            drawChild = super.drawChild(canvas, view, j);
        } else {
            if (!view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(true);
            }
            Bitmap drawingCache = view.getDrawingCache();
            if (drawingCache != null) {
                canvas.drawBitmap(drawingCache, (float) view.getLeft(), (float) view.getTop(), cCC3CC0l.C11013l3);
                drawChild = false;
            } else {
                Log.e("SlidingPaneLayout", "drawChild: child view " + view + " returned null drawing cache");
                drawChild = super.drawChild(canvas, view, j);
            }
        }
        canvas.restoreToCount(save);
        return drawChild;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new CCC3CC0l();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new CCC3CC0l(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new CCC3CC0l((ViewGroup.MarginLayoutParams) layoutParams) : new CCC3CC0l(layoutParams);
    }

    public int getCoveredFadeColor() {
        return this.C101lC8O;
    }

    public int getParallaxDistance() {
        return this.C3l3O8lIOIO8;
    }

    public int getSliderFadeColor() {
        return this.C0I1O3C3lI8;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.CCC3CC0l = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.CCC3CC0l = true;
        int size = this.CI3C103l01O.size();
        for (int i = 0; i < size; i++) {
            ((CC8IOI1II0) this.CI3C103l01O.get(i)).run();
        }
        this.CI3C103l01O.clear();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View childAt;
        int C01O0C2 = CCC3CC0l.C01O0C(motionEvent);
        if (!this.C1l00I1 && C01O0C2 == 0 && getChildCount() > 1 && (childAt = getChildAt(1)) != null) {
            this.CC8IOI1II0 = !this.CC38COI1l3I.C0I1O3C3lI8(childAt, (int) motionEvent.getX(), (int) motionEvent.getY());
        }
        if (!this.C1l00I1 || (this.C3ICl0OOl && C01O0C2 != 0)) {
            this.CC38COI1l3I.C11ll3();
            return super.onInterceptTouchEvent(motionEvent);
        } else if (C01O0C2 == 3 || C01O0C2 == 1) {
            this.CC38COI1l3I.C11ll3();
            return false;
        } else {
            switch (C01O0C2) {
                case 0:
                    this.C3ICl0OOl = false;
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    this.C3llC38O1 = x;
                    this.C831O13C118 = y;
                    if (this.CC38COI1l3I.C0I1O3C3lI8(this.C1O10Cl038, (int) x, (int) y) && C0I1O3C3lI8(this.C1O10Cl038)) {
                        z = true;
                        break;
                    }
                    z = false;
                    break;
                case 1:
                default:
                    z = false;
                    break;
                case 2:
                    float x2 = motionEvent.getX();
                    float y2 = motionEvent.getY();
                    float abs = Math.abs(x2 - this.C3llC38O1);
                    float abs2 = Math.abs(y2 - this.C831O13C118);
                    if (abs > ((float) this.CC38COI1l3I.C11013l3()) && abs2 > abs) {
                        this.CC38COI1l3I.C11ll3();
                        this.C3ICl0OOl = true;
                        return false;
                    }
                    z = false;
                    break;
            }
            return this.CC38COI1l3I.C01O0C(motionEvent) || z;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        int width;
        int i9;
        boolean C18Cl1C2 = C18Cl1C();
        if (C18Cl1C2) {
            this.CC38COI1l3I.C01O0C(2);
        } else {
            this.CC38COI1l3I.C01O0C(1);
        }
        int i10 = i3 - i;
        int paddingRight = C18Cl1C2 ? getPaddingRight() : getPaddingLeft();
        int paddingLeft = C18Cl1C2 ? getPaddingLeft() : getPaddingRight();
        int paddingTop = getPaddingTop();
        int childCount = getChildCount();
        if (this.CCC3CC0l) {
            this.C1OC33O0lO81 = (!this.C1l00I1 || !this.CC8IOI1II0) ? 0.0f : 1.0f;
        }
        int i11 = 0;
        int i12 = paddingRight;
        while (i11 < childCount) {
            View childAt = getChildAt(i11);
            if (childAt.getVisibility() == 8) {
                width = paddingRight;
                i9 = i12;
            } else {
                CCC3CC0l cCC3CC0l = (CCC3CC0l) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                if (cCC3CC0l.C0I1O3C3lI8) {
                    int min = (Math.min(paddingRight, (i10 - paddingLeft) - this.C18Cl1C) - i12) - (cCC3CC0l.leftMargin + cCC3CC0l.rightMargin);
                    this.C3CIO118 = min;
                    int i13 = C18Cl1C2 ? cCC3CC0l.rightMargin : cCC3CC0l.leftMargin;
                    cCC3CC0l.C101lC8O = ((i12 + i13) + min) + (measuredWidth / 2) > i10 - paddingLeft;
                    int i14 = (int) (((float) min) * this.C1OC33O0lO81);
                    i6 = i12 + i13 + i14;
                    this.C1OC33O0lO81 = ((float) i14) / ((float) this.C3CIO118);
                    i5 = 0;
                } else if (!this.C1l00I1 || this.C3l3O8lIOIO8 == 0) {
                    i5 = 0;
                    i6 = paddingRight;
                } else {
                    i5 = (int) ((1.0f - this.C1OC33O0lO81) * ((float) this.C3l3O8lIOIO8));
                    i6 = paddingRight;
                }
                if (C18Cl1C2) {
                    i8 = (i10 - i6) + i5;
                    i7 = i8 - measuredWidth;
                } else {
                    i7 = i6 - i5;
                    i8 = i7 + measuredWidth;
                }
                childAt.layout(i7, paddingTop, i8, childAt.getMeasuredHeight() + paddingTop);
                width = childAt.getWidth() + paddingRight;
                i9 = i6;
            }
            i11++;
            paddingRight = width;
            i12 = i9;
        }
        if (this.CCC3CC0l) {
            if (this.C1l00I1) {
                if (this.C3l3O8lIOIO8 != 0) {
                    C01O0C(this.C1OC33O0lO81);
                }
                if (((CCC3CC0l) this.C1O10Cl038.getLayoutParams()).C101lC8O) {
                    C01O0C(this.C1O10Cl038, this.C1OC33O0lO81, this.C0I1O3C3lI8);
                }
            } else {
                for (int i15 = 0; i15 < childCount; i15++) {
                    C01O0C(getChildAt(i15), 0.0f, this.C0I1O3C3lI8);
                }
            }
            C01O0C(this.C1O10Cl038);
        }
        this.CCC3CC0l = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int paddingTop;
        int i7;
        int i8;
        boolean z;
        float f;
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            if (mode2 == 0) {
                if (!isInEditMode()) {
                    throw new IllegalStateException("Height must not be UNSPECIFIED");
                } else if (mode2 == 0) {
                    i3 = Integer.MIN_VALUE;
                    i4 = size;
                    i5 = 300;
                }
            }
            i3 = mode2;
            i4 = size;
            i5 = size2;
        } else if (!isInEditMode()) {
            throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
        } else if (mode == Integer.MIN_VALUE) {
            i3 = mode2;
            i4 = size;
            i5 = size2;
        } else {
            if (mode == 0) {
                i3 = mode2;
                i4 = 300;
                i5 = size2;
            }
            i3 = mode2;
            i4 = size;
            i5 = size2;
        }
        switch (i3) {
            case Integer.MIN_VALUE:
                i6 = 0;
                paddingTop = (i5 - getPaddingTop()) - getPaddingBottom();
                break;
            case 1073741824:
                i6 = (i5 - getPaddingTop()) - getPaddingBottom();
                paddingTop = i6;
                break;
            default:
                i6 = 0;
                paddingTop = -1;
                break;
        }
        boolean z2 = false;
        int paddingLeft = (i4 - getPaddingLeft()) - getPaddingRight();
        int childCount = getChildCount();
        if (childCount > 2) {
            Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
        }
        this.C1O10Cl038 = null;
        int i9 = 0;
        int i10 = paddingLeft;
        int i11 = i6;
        float f2 = 0.0f;
        while (i9 < childCount) {
            View childAt = getChildAt(i9);
            CCC3CC0l cCC3CC0l = (CCC3CC0l) childAt.getLayoutParams();
            if (childAt.getVisibility() == 8) {
                cCC3CC0l.C101lC8O = false;
                i7 = i10;
                f = f2;
                i8 = i11;
                z = z2;
            } else {
                if (cCC3CC0l.C01O0C > 0.0f) {
                    f2 += cCC3CC0l.C01O0C;
                    if (cCC3CC0l.width == 0) {
                        i7 = i10;
                        f = f2;
                        i8 = i11;
                        z = z2;
                    }
                }
                int i12 = cCC3CC0l.leftMargin + cCC3CC0l.rightMargin;
                childAt.measure(cCC3CC0l.width == -2 ? View.MeasureSpec.makeMeasureSpec(paddingLeft - i12, Integer.MIN_VALUE) : cCC3CC0l.width == -1 ? View.MeasureSpec.makeMeasureSpec(paddingLeft - i12, 1073741824) : View.MeasureSpec.makeMeasureSpec(cCC3CC0l.width, 1073741824), cCC3CC0l.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : cCC3CC0l.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(cCC3CC0l.height, 1073741824));
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                if (i3 == Integer.MIN_VALUE && measuredHeight > i11) {
                    i11 = Math.min(measuredHeight, paddingTop);
                }
                int i13 = i10 - measuredWidth;
                boolean z3 = i13 < 0;
                cCC3CC0l.C0I1O3C3lI8 = z3;
                boolean z4 = z3 | z2;
                if (cCC3CC0l.C0I1O3C3lI8) {
                    this.C1O10Cl038 = childAt;
                }
                i7 = i13;
                i8 = i11;
                float f3 = f2;
                z = z4;
                f = f3;
            }
            i9++;
            z2 = z;
            i11 = i8;
            f2 = f;
            i10 = i7;
        }
        if (z2 || f2 > 0.0f) {
            int i14 = paddingLeft - this.C18Cl1C;
            for (int i15 = 0; i15 < childCount; i15++) {
                View childAt2 = getChildAt(i15);
                if (childAt2.getVisibility() != 8) {
                    CCC3CC0l cCC3CC0l2 = (CCC3CC0l) childAt2.getLayoutParams();
                    if (childAt2.getVisibility() != 8) {
                        boolean z5 = cCC3CC0l2.width == 0 && cCC3CC0l2.C01O0C > 0.0f;
                        int measuredWidth2 = z5 ? 0 : childAt2.getMeasuredWidth();
                        if (!z2 || childAt2 == this.C1O10Cl038) {
                            if (cCC3CC0l2.C01O0C > 0.0f) {
                                int makeMeasureSpec = cCC3CC0l2.width == 0 ? cCC3CC0l2.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : cCC3CC0l2.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(cCC3CC0l2.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824);
                                if (z2) {
                                    int i16 = paddingLeft - (cCC3CC0l2.rightMargin + cCC3CC0l2.leftMargin);
                                    int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i16, 1073741824);
                                    if (measuredWidth2 != i16) {
                                        childAt2.measure(makeMeasureSpec2, makeMeasureSpec);
                                    }
                                } else {
                                    childAt2.measure(View.MeasureSpec.makeMeasureSpec(((int) ((cCC3CC0l2.C01O0C * ((float) Math.max(0, i10))) / f2)) + measuredWidth2, 1073741824), makeMeasureSpec);
                                }
                            }
                        } else if (cCC3CC0l2.width < 0 && (measuredWidth2 > i14 || cCC3CC0l2.C01O0C > 0.0f)) {
                            childAt2.measure(View.MeasureSpec.makeMeasureSpec(i14, 1073741824), z5 ? cCC3CC0l2.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : cCC3CC0l2.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(cCC3CC0l2.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824));
                        }
                    }
                }
            }
        }
        setMeasuredDimension(i4, getPaddingTop() + i11 + getPaddingBottom());
        this.C1l00I1 = z2;
        if (this.CC38COI1l3I.C01O0C() != 0 && !z2) {
            this.CC38COI1l3I.C18Cl1C();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.C01O0C) {
            C0I1O3C3lI8();
        } else {
            C101lC8O();
        }
        this.CC8IOI1II0 = savedState.C01O0C;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.C01O0C = C11ll3() ? C11013l3() : this.CC8IOI1II0;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3) {
            this.CCC3CC0l = true;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.C1l00I1) {
            return super.onTouchEvent(motionEvent);
        }
        this.CC38COI1l3I.C0I1O3C3lI8(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.C3llC38O1 = x;
                this.C831O13C118 = y;
                return true;
            case 1:
                if (!C0I1O3C3lI8(this.C1O10Cl038)) {
                    return true;
                }
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                float f = x2 - this.C3llC38O1;
                float f2 = y2 - this.C831O13C118;
                int C11013l32 = this.CC38COI1l3I.C11013l3();
                if ((f * f) + (f2 * f2) >= ((float) (C11013l32 * C11013l32)) || !this.CC38COI1l3I.C0I1O3C3lI8(this.C1O10Cl038, (int) x2, (int) y2)) {
                    return true;
                }
                C01O0C(this.C1O10Cl038, 0);
                return true;
            default:
                return true;
        }
    }

    public void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        if (!isInTouchMode() && !this.C1l00I1) {
            this.CC8IOI1II0 = view == this.C1O10Cl038;
        }
    }

    public void setCoveredFadeColor(int i) {
        this.C101lC8O = i;
    }

    public void setPanelSlideListener(CI0I8l333131 cI0I8l333131) {
        this.C8CI00 = cI0I8l333131;
    }

    public void setParallaxDistance(int i) {
        this.C3l3O8lIOIO8 = i;
        requestLayout();
    }

    @Deprecated
    public void setShadowDrawable(Drawable drawable) {
        setShadowDrawableLeft(drawable);
    }

    public void setShadowDrawableLeft(Drawable drawable) {
        this.C11013l3 = drawable;
    }

    public void setShadowDrawableRight(Drawable drawable) {
        this.C11ll3 = drawable;
    }

    @Deprecated
    public void setShadowResource(int i) {
        setShadowDrawable(getResources().getDrawable(i));
    }

    public void setShadowResourceLeft(int i) {
        setShadowDrawableLeft(getResources().getDrawable(i));
    }

    public void setShadowResourceRight(int i) {
        setShadowDrawableRight(getResources().getDrawable(i));
    }

    public void setSliderFadeColor(int i) {
        this.C0I1O3C3lI8 = i;
    }
}
