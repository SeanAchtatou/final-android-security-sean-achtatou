package android.support.v4.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.a.C01O0C;
import android.support.v4.view.C1l00I1;
import android.support.v4.view.C3ICl0OOl;
import android.support.v4.view.CCC3CC0l;
import android.support.v4.view.CO88CO1Cl383;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class DrawerLayout extends ViewGroup {
    static final C0I1O3C3lI8 C01O0C;
    /* access modifiers changed from: private */
    public static final int[] C0I1O3C3lI8 = {16842931};
    private static final boolean C101lC8O;
    private final C01O0C C11013l3;
    private int C11ll3;
    private int C18Cl1C;
    private Paint C1O10Cl038;
    private final ClO80C3lOO8 C1OC33O0lO81;
    private float C1l00I1;
    private final ClO80C3lOO8 C3C1C0I8l3;
    private final C1O10Cl038 C3CIO118;
    private final C1O10Cl038 C3ICl0OOl;
    private int C3l3O8lIOIO8;
    private boolean C3llC38O1;
    private boolean C831O13C118;
    private int C8CI00;
    private int CC38COI1l3I;
    private boolean CC8IOI1II0;
    private boolean CCC3CC0l;
    private C11ll3 CI0I8l333131;
    private float CI3C103l01O;
    private float CII3C813OIC8;
    private Drawable CIOC8C;
    private boolean CO081lO0OC0;
    private Drawable Cl80C0l838l;
    private Drawable ClC13lIl;
    private Object ClO80C3lOO8;

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new C1l00I1();
        int C01O0C = 0;
        int C0I1O3C3lI8 = 0;
        int C101lC8O = 0;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.C01O0C = parcel.readInt();
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.C01O0C);
        }
    }

    static {
        boolean z = true;
        if (Build.VERSION.SDK_INT < 19) {
            z = false;
        }
        C101lC8O = z;
        if (Build.VERSION.SDK_INT >= 21) {
            C01O0C = new C101lC8O();
        } else {
            C01O0C = new C11013l3();
        }
    }

    private void C01O0C(View view, boolean z) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((z || C1l00I1(childAt)) && (!z || childAt != view)) {
                CO88CO1Cl383.C0I1O3C3lI8(childAt, 4);
            } else {
                CO88CO1Cl383.C0I1O3C3lI8(childAt, 1);
            }
        }
    }

    static String C0I1O3C3lI8(int i) {
        return (i & 3) == 3 ? "LEFT" : (i & 5) == 5 ? "RIGHT" : Integer.toHexString(i);
    }

    private boolean C11013l3() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (((C18Cl1C) getChildAt(i).getLayoutParams()).C101lC8O) {
                return true;
            }
        }
        return false;
    }

    private boolean C11ll3() {
        return C18Cl1C() != null;
    }

    private View C18Cl1C() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (C1l00I1(childAt) && C3C1C0I8l3(childAt)) {
                return childAt;
            }
        }
        return null;
    }

    private static boolean C3CIO118(View view) {
        Drawable background = view.getBackground();
        return background != null && background.getOpacity() == -1;
    }

    public int C01O0C(View view) {
        int C11ll32 = C11ll3(view);
        if (C11ll32 == 3) {
            return this.C8CI00;
        }
        if (C11ll32 == 5) {
            return this.CC38COI1l3I;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public View C01O0C() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (((C18Cl1C) childAt.getLayoutParams()).C11013l3) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public View C01O0C(int i) {
        int C01O0C2 = C1l00I1.C01O0C(i, CO88CO1Cl383.C11013l3(this)) & 7;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if ((C11ll3(childAt) & 7) == C01O0C2) {
                return childAt;
            }
        }
        return null;
    }

    public void C01O0C(int i, int i2) {
        int C01O0C2 = C1l00I1.C01O0C(i2, CO88CO1Cl383.C11013l3(this));
        if (C01O0C2 == 3) {
            this.C8CI00 = i;
        } else if (C01O0C2 == 5) {
            this.CC38COI1l3I = i;
        }
        if (i != 0) {
            (C01O0C2 == 3 ? this.C1OC33O0lO81 : this.C3C1C0I8l3).C11ll3();
        }
        switch (i) {
            case 1:
                View C01O0C3 = C01O0C(C01O0C2);
                if (C01O0C3 != null) {
                    C1OC33O0lO81(C01O0C3);
                    return;
                }
                return;
            case 2:
                View C01O0C4 = C01O0C(C01O0C2);
                if (C01O0C4 != null) {
                    C1O10Cl038(C01O0C4);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(int i, int i2, View view) {
        int i3 = 1;
        int C01O0C2 = this.C1OC33O0lO81.C01O0C();
        int C01O0C3 = this.C3C1C0I8l3.C01O0C();
        if (!(C01O0C2 == 1 || C01O0C3 == 1)) {
            i3 = (C01O0C2 == 2 || C01O0C3 == 2) ? 2 : 0;
        }
        if (view != null && i2 == 0) {
            C18Cl1C c18Cl1C = (C18Cl1C) view.getLayoutParams();
            if (c18Cl1C.C0I1O3C3lI8 == 0.0f) {
                C0I1O3C3lI8(view);
            } else if (c18Cl1C.C0I1O3C3lI8 == 1.0f) {
                C101lC8O(view);
            }
        }
        if (i3 != this.C3l3O8lIOIO8) {
            this.C3l3O8lIOIO8 = i3;
            if (this.CI0I8l333131 != null) {
                this.CI0I8l333131.C01O0C(i3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(View view, float f) {
        if (this.CI0I8l333131 != null) {
            this.CI0I8l333131.C01O0C(view, f);
        }
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(boolean z) {
        int childCount = getChildCount();
        boolean z2 = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            C18Cl1C c18Cl1C = (C18Cl1C) childAt.getLayoutParams();
            if (C1l00I1(childAt) && (!z || c18Cl1C.C101lC8O)) {
                z2 = C01O0C(childAt, 3) ? z2 | this.C1OC33O0lO81.C01O0C(childAt, -childAt.getWidth(), childAt.getTop()) : z2 | this.C3C1C0I8l3.C01O0C(childAt, getWidth(), childAt.getTop());
                c18Cl1C.C101lC8O = false;
            }
        }
        this.C3CIO118.C01O0C();
        this.C3ICl0OOl.C01O0C();
        if (z2) {
            invalidate();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean C01O0C(View view, int i) {
        return (C11ll3(view) & i) == i;
    }

    public void C0I1O3C3lI8() {
        C01O0C(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.C01O0C(int, int):void
      android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, int):boolean
      android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void C0I1O3C3lI8(View view) {
        View rootView;
        C18Cl1C c18Cl1C = (C18Cl1C) view.getLayoutParams();
        if (c18Cl1C.C11013l3) {
            c18Cl1C.C11013l3 = false;
            if (this.CI0I8l333131 != null) {
                this.CI0I8l333131.C0I1O3C3lI8(view);
            }
            C01O0C(view, false);
            if (hasWindowFocus() && (rootView = getRootView()) != null) {
                rootView.sendAccessibilityEvent(32);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void C0I1O3C3lI8(View view, float f) {
        C18Cl1C c18Cl1C = (C18Cl1C) view.getLayoutParams();
        if (f != c18Cl1C.C0I1O3C3lI8) {
            c18Cl1C.C0I1O3C3lI8 = f;
            C01O0C(view, f);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.C01O0C(int, int):void
      android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, int):boolean
      android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void C101lC8O(View view) {
        C18Cl1C c18Cl1C = (C18Cl1C) view.getLayoutParams();
        if (!c18Cl1C.C11013l3) {
            c18Cl1C.C11013l3 = true;
            if (this.CI0I8l333131 != null) {
                this.CI0I8l333131.C01O0C(view);
            }
            C01O0C(view, true);
            if (hasWindowFocus()) {
                sendAccessibilityEvent(32);
            }
            view.requestFocus();
        }
    }

    /* access modifiers changed from: package-private */
    public float C11013l3(View view) {
        return ((C18Cl1C) view.getLayoutParams()).C0I1O3C3lI8;
    }

    /* access modifiers changed from: package-private */
    public int C11ll3(View view) {
        return C1l00I1.C01O0C(((C18Cl1C) view.getLayoutParams()).C01O0C, CO88CO1Cl383.C11013l3(this));
    }

    /* access modifiers changed from: package-private */
    public boolean C18Cl1C(View view) {
        return ((C18Cl1C) view.getLayoutParams()).C01O0C == 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.C01O0C(int, int):void
      android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, int):boolean
      android.support.v4.widget.DrawerLayout.C01O0C(android.view.View, boolean):void */
    public void C1O10Cl038(View view) {
        if (!C1l00I1(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.C831O13C118) {
            C18Cl1C c18Cl1C = (C18Cl1C) view.getLayoutParams();
            c18Cl1C.C0I1O3C3lI8 = 1.0f;
            c18Cl1C.C11013l3 = true;
            C01O0C(view, true);
        } else if (C01O0C(view, 3)) {
            this.C1OC33O0lO81.C01O0C(view, 0, view.getTop());
        } else {
            this.C3C1C0I8l3.C01O0C(view, getWidth() - view.getWidth(), view.getTop());
        }
        invalidate();
    }

    public void C1OC33O0lO81(View view) {
        if (!C1l00I1(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.C831O13C118) {
            C18Cl1C c18Cl1C = (C18Cl1C) view.getLayoutParams();
            c18Cl1C.C0I1O3C3lI8 = 0.0f;
            c18Cl1C.C11013l3 = false;
        } else if (C01O0C(view, 3)) {
            this.C1OC33O0lO81.C01O0C(view, -view.getWidth(), view.getTop());
        } else {
            this.C3C1C0I8l3.C01O0C(view, getWidth(), view.getTop());
        }
        invalidate();
    }

    /* access modifiers changed from: package-private */
    public boolean C1l00I1(View view) {
        return (C1l00I1.C01O0C(((C18Cl1C) view.getLayoutParams()).C01O0C, CO88CO1Cl383.C11013l3(view)) & 7) != 0;
    }

    public boolean C3C1C0I8l3(View view) {
        if (C1l00I1(view)) {
            return ((C18Cl1C) view.getLayoutParams()).C0I1O3C3lI8 > 0.0f;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        if (C01O0C() != null || C1l00I1(view)) {
            CO88CO1Cl383.C0I1O3C3lI8(view, 4);
        } else {
            CO88CO1Cl383.C0I1O3C3lI8(view, 1);
        }
        if (!C101lC8O) {
            CO88CO1Cl383.C01O0C(view, this.C11013l3);
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof C18Cl1C) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float f = 0.0f;
        for (int i = 0; i < childCount; i++) {
            f = Math.max(f, ((C18Cl1C) getChildAt(i).getLayoutParams()).C0I1O3C3lI8);
        }
        this.C1l00I1 = f;
        if (this.C1OC33O0lO81.C01O0C(true) || this.C3C1C0I8l3.C01O0C(true)) {
            CO88CO1Cl383.C0I1O3C3lI8(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        int i;
        int height = getHeight();
        boolean C18Cl1C2 = C18Cl1C(view);
        int i2 = 0;
        int width = getWidth();
        int save = canvas.save();
        if (C18Cl1C2) {
            int childCount = getChildCount();
            int i3 = 0;
            while (i3 < childCount) {
                View childAt = getChildAt(i3);
                if (childAt != view && childAt.getVisibility() == 0 && C3CIO118(childAt) && C1l00I1(childAt)) {
                    if (childAt.getHeight() < height) {
                        i = width;
                    } else if (C01O0C(childAt, 3)) {
                        int right = childAt.getRight();
                        if (right <= i2) {
                            right = i2;
                        }
                        i2 = right;
                        i = width;
                    } else {
                        i = childAt.getLeft();
                        if (i < width) {
                        }
                    }
                    i3++;
                    width = i;
                }
                i = width;
                i3++;
                width = i;
            }
            canvas.clipRect(i2, 0, width, getHeight());
        }
        int i4 = width;
        boolean drawChild = super.drawChild(canvas, view, j);
        canvas.restoreToCount(save);
        if (this.C1l00I1 > 0.0f && C18Cl1C2) {
            this.C1O10Cl038.setColor((((int) (((float) ((this.C18Cl1C & -16777216) >>> 24)) * this.C1l00I1)) << 24) | (this.C18Cl1C & 16777215));
            canvas.drawRect((float) i2, 0.0f, (float) i4, (float) getHeight(), this.C1O10Cl038);
        } else if (this.CIOC8C != null && C01O0C(view, 3)) {
            int intrinsicWidth = this.CIOC8C.getIntrinsicWidth();
            int right2 = view.getRight();
            float max = Math.max(0.0f, Math.min(((float) right2) / ((float) this.C1OC33O0lO81.C0I1O3C3lI8()), 1.0f));
            this.CIOC8C.setBounds(right2, view.getTop(), intrinsicWidth + right2, view.getBottom());
            this.CIOC8C.setAlpha((int) (255.0f * max));
            this.CIOC8C.draw(canvas);
        } else if (this.Cl80C0l838l != null && C01O0C(view, 5)) {
            int intrinsicWidth2 = this.Cl80C0l838l.getIntrinsicWidth();
            int left = view.getLeft();
            float max2 = Math.max(0.0f, Math.min(((float) (getWidth() - left)) / ((float) this.C3C1C0I8l3.C0I1O3C3lI8()), 1.0f));
            this.Cl80C0l838l.setBounds(left - intrinsicWidth2, view.getTop(), left, view.getBottom());
            this.Cl80C0l838l.setAlpha((int) (255.0f * max2));
            this.Cl80C0l838l.draw(canvas);
        }
        return drawChild;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new C18Cl1C(-1, -1);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new C18Cl1C(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof C18Cl1C ? new C18Cl1C((C18Cl1C) layoutParams) : layoutParams instanceof ViewGroup.MarginLayoutParams ? new C18Cl1C((ViewGroup.MarginLayoutParams) layoutParams) : new C18Cl1C(layoutParams);
    }

    public Drawable getStatusBarBackgroundDrawable() {
        return this.ClC13lIl;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.C831O13C118 = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.C831O13C118 = true;
    }

    public void onDraw(Canvas canvas) {
        int C01O0C2;
        super.onDraw(canvas);
        if (this.CO081lO0OC0 && this.ClC13lIl != null && (C01O0C2 = C01O0C.C01O0C(this.ClO80C3lOO8)) > 0) {
            this.ClC13lIl.setBounds(0, 0, getWidth(), C01O0C2);
            this.ClC13lIl.draw(canvas);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View C11013l32;
        int C01O0C2 = CCC3CC0l.C01O0C(motionEvent);
        boolean C01O0C3 = this.C1OC33O0lO81.C01O0C(motionEvent) | this.C3C1C0I8l3.C01O0C(motionEvent);
        switch (C01O0C2) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.CI3C103l01O = x;
                this.CII3C813OIC8 = y;
                z = this.C1l00I1 > 0.0f && (C11013l32 = this.C1OC33O0lO81.C11013l3((int) x, (int) y)) != null && C18Cl1C(C11013l32);
                this.CC8IOI1II0 = false;
                this.CCC3CC0l = false;
                break;
            case 1:
            case 3:
                C01O0C(true);
                this.CC8IOI1II0 = false;
                this.CCC3CC0l = false;
                z = false;
                break;
            case 2:
                if (this.C1OC33O0lO81.C11013l3(3)) {
                    this.C3CIO118.C01O0C();
                    this.C3ICl0OOl.C01O0C();
                    z = false;
                    break;
                }
                z = false;
                break;
            default:
                z = false;
                break;
        }
        return C01O0C3 || z || C11013l3() || this.CCC3CC0l;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !C11ll3()) {
            return super.onKeyDown(i, keyEvent);
        }
        C3ICl0OOl.C0I1O3C3lI8(keyEvent);
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyUp(i, keyEvent);
        }
        View C18Cl1C2 = C18Cl1C();
        if (C18Cl1C2 != null && C01O0C(C18Cl1C2) == 0) {
            C0I1O3C3lI8();
        }
        return C18Cl1C2 != null;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        float f;
        this.C3llC38O1 = true;
        int i6 = i3 - i;
        int childCount = getChildCount();
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                C18Cl1C c18Cl1C = (C18Cl1C) childAt.getLayoutParams();
                if (C18Cl1C(childAt)) {
                    childAt.layout(c18Cl1C.leftMargin, c18Cl1C.topMargin, c18Cl1C.leftMargin + childAt.getMeasuredWidth(), c18Cl1C.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (C01O0C(childAt, 3)) {
                        i5 = ((int) (((float) measuredWidth) * c18Cl1C.C0I1O3C3lI8)) + (-measuredWidth);
                        f = ((float) (measuredWidth + i5)) / ((float) measuredWidth);
                    } else {
                        i5 = i6 - ((int) (((float) measuredWidth) * c18Cl1C.C0I1O3C3lI8));
                        f = ((float) (i6 - i5)) / ((float) measuredWidth);
                    }
                    boolean z2 = f != c18Cl1C.C0I1O3C3lI8;
                    switch (c18Cl1C.C01O0C & 112) {
                        case 16:
                            int i8 = i4 - i2;
                            int i9 = (i8 - measuredHeight) / 2;
                            if (i9 < c18Cl1C.topMargin) {
                                i9 = c18Cl1C.topMargin;
                            } else if (i9 + measuredHeight > i8 - c18Cl1C.bottomMargin) {
                                i9 = (i8 - c18Cl1C.bottomMargin) - measuredHeight;
                            }
                            childAt.layout(i5, i9, measuredWidth + i5, measuredHeight + i9);
                            break;
                        case 80:
                            int i10 = i4 - i2;
                            childAt.layout(i5, (i10 - c18Cl1C.bottomMargin) - childAt.getMeasuredHeight(), measuredWidth + i5, i10 - c18Cl1C.bottomMargin);
                            break;
                        default:
                            childAt.layout(i5, c18Cl1C.topMargin, measuredWidth + i5, measuredHeight + c18Cl1C.topMargin);
                            break;
                    }
                    if (z2) {
                        C0I1O3C3lI8(childAt, f);
                    }
                    int i11 = c18Cl1C.C0I1O3C3lI8 > 0.0f ? 0 : 4;
                    if (childAt.getVisibility() != i11) {
                        childAt.setVisibility(i11);
                    }
                }
            }
        }
        this.C3llC38O1 = false;
        this.C831O13C118 = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        if (r5 != 0) goto L_0x0056;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r1 = 300(0x12c, float:4.2E-43)
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            r12 = 1073741824(0x40000000, float:2.0)
            int r3 = android.view.View.MeasureSpec.getMode(r14)
            int r5 = android.view.View.MeasureSpec.getMode(r15)
            int r2 = android.view.View.MeasureSpec.getSize(r14)
            int r0 = android.view.View.MeasureSpec.getSize(r15)
            if (r3 != r12) goto L_0x001b
            if (r5 == r12) goto L_0x0056
        L_0x001b:
            boolean r6 = r13.isInEditMode()
            if (r6 == 0) goto L_0x0058
            if (r3 != r7) goto L_0x0050
        L_0x0023:
            if (r5 != r7) goto L_0x0054
            r1 = r0
        L_0x0026:
            r13.setMeasuredDimension(r2, r1)
            java.lang.Object r0 = r13.ClO80C3lOO8
            if (r0 == 0) goto L_0x0060
            boolean r0 = android.support.v4.view.CO88CO1Cl383.C18Cl1C(r13)
            if (r0 == 0) goto L_0x0060
            r0 = 1
            r3 = r0
        L_0x0035:
            int r6 = android.support.v4.view.CO88CO1Cl383.C11013l3(r13)
            int r7 = r13.getChildCount()
            r5 = r4
        L_0x003e:
            if (r5 >= r7) goto L_0x0138
            android.view.View r8 = r13.getChildAt(r5)
            int r0 = r8.getVisibility()
            r9 = 8
            if (r0 != r9) goto L_0x0062
        L_0x004c:
            int r0 = r5 + 1
            r5 = r0
            goto L_0x003e
        L_0x0050:
            if (r3 != 0) goto L_0x0023
            r2 = r1
            goto L_0x0023
        L_0x0054:
            if (r5 == 0) goto L_0x0026
        L_0x0056:
            r1 = r0
            goto L_0x0026
        L_0x0058:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "DrawerLayout must be measured with MeasureSpec.EXACTLY."
            r0.<init>(r1)
            throw r0
        L_0x0060:
            r3 = r4
            goto L_0x0035
        L_0x0062:
            android.view.ViewGroup$LayoutParams r0 = r8.getLayoutParams()
            android.support.v4.widget.C18Cl1C r0 = (android.support.v4.widget.C18Cl1C) r0
            if (r3 == 0) goto L_0x007d
            int r9 = r0.C01O0C
            int r9 = android.support.v4.view.C1l00I1.C01O0C(r9, r6)
            boolean r10 = android.support.v4.view.CO88CO1Cl383.C18Cl1C(r8)
            if (r10 == 0) goto L_0x009e
            android.support.v4.widget.C0I1O3C3lI8 r10 = android.support.v4.widget.DrawerLayout.C01O0C
            java.lang.Object r11 = r13.ClO80C3lOO8
            r10.C01O0C(r8, r11, r9)
        L_0x007d:
            boolean r9 = r13.C18Cl1C(r8)
            if (r9 == 0) goto L_0x00a6
            int r9 = r0.leftMargin
            int r9 = r2 - r9
            int r10 = r0.rightMargin
            int r9 = r9 - r10
            int r9 = android.view.View.MeasureSpec.makeMeasureSpec(r9, r12)
            int r10 = r0.topMargin
            int r10 = r1 - r10
            int r0 = r0.bottomMargin
            int r0 = r10 - r0
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r12)
            r8.measure(r9, r0)
            goto L_0x004c
        L_0x009e:
            android.support.v4.widget.C0I1O3C3lI8 r10 = android.support.v4.widget.DrawerLayout.C01O0C
            java.lang.Object r11 = r13.ClO80C3lOO8
            r10.C01O0C(r0, r11, r9)
            goto L_0x007d
        L_0x00a6:
            boolean r9 = r13.C1l00I1(r8)
            if (r9 == 0) goto L_0x0109
            int r9 = r13.C11ll3(r8)
            r9 = r9 & 7
            r10 = r4 & r9
            if (r10 == 0) goto L_0x00eb
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Child drawer has absolute gravity "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = C0I1O3C3lI8(r9)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " but this "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "DrawerLayout"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " already has a "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "drawer view along that edge"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00eb:
            int r9 = r13.C11ll3
            int r10 = r0.leftMargin
            int r9 = r9 + r10
            int r10 = r0.rightMargin
            int r9 = r9 + r10
            int r10 = r0.width
            int r9 = getChildMeasureSpec(r14, r9, r10)
            int r10 = r0.topMargin
            int r11 = r0.bottomMargin
            int r10 = r10 + r11
            int r0 = r0.height
            int r0 = getChildMeasureSpec(r15, r10, r0)
            r8.measure(r9, r0)
            goto L_0x004c
        L_0x0109:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Child "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r2 = " at index "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = " does not have a valid layout_gravity - must be Gravity.LEFT, "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "Gravity.RIGHT or Gravity.NO_GRAVITY"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0138:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        View C01O0C2;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!(savedState.C01O0C == 0 || (C01O0C2 = C01O0C(savedState.C01O0C)) == null)) {
            C1O10Cl038(C01O0C2);
        }
        C01O0C(savedState.C0I1O3C3lI8, 3);
        C01O0C(savedState.C101lC8O, 5);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        View C01O0C2 = C01O0C();
        if (C01O0C2 != null) {
            savedState.C01O0C = ((C18Cl1C) C01O0C2.getLayoutParams()).C01O0C;
        }
        savedState.C0I1O3C3lI8 = this.C8CI00;
        savedState.C101lC8O = this.CC38COI1l3I;
        return savedState;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View C01O0C2;
        this.C1OC33O0lO81.C0I1O3C3lI8(motionEvent);
        this.C3C1C0I8l3.C0I1O3C3lI8(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.CI3C103l01O = x;
                this.CII3C813OIC8 = y;
                this.CC8IOI1II0 = false;
                this.CCC3CC0l = false;
                break;
            case 1:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                View C11013l32 = this.C1OC33O0lO81.C11013l3((int) x2, (int) y2);
                if (C11013l32 != null && C18Cl1C(C11013l32)) {
                    float f = x2 - this.CI3C103l01O;
                    float f2 = y2 - this.CII3C813OIC8;
                    int C11013l33 = this.C1OC33O0lO81.C11013l3();
                    if ((f * f) + (f2 * f2) < ((float) (C11013l33 * C11013l33)) && (C01O0C2 = C01O0C()) != null) {
                        z = C01O0C(C01O0C2) == 2;
                        C01O0C(z);
                        this.CC8IOI1II0 = false;
                        break;
                    }
                }
                z = true;
                C01O0C(z);
                this.CC8IOI1II0 = false;
            case 3:
                C01O0C(true);
                this.CC8IOI1II0 = false;
                this.CCC3CC0l = false;
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        this.CC8IOI1II0 = z;
        if (z) {
            C01O0C(true);
        }
    }

    public void requestLayout() {
        if (!this.C3llC38O1) {
            super.requestLayout();
        }
    }

    public void setDrawerListener(C11ll3 c11ll3) {
        this.CI0I8l333131 = c11ll3;
    }

    public void setDrawerLockMode(int i) {
        C01O0C(i, 3);
        C01O0C(i, 5);
    }

    public void setScrimColor(int i) {
        this.C18Cl1C = i;
        invalidate();
    }

    public void setStatusBarBackground(int i) {
        this.ClC13lIl = i != 0 ? C01O0C.C01O0C(getContext(), i) : null;
    }

    public void setStatusBarBackground(Drawable drawable) {
        this.ClC13lIl = drawable;
    }

    public void setStatusBarBackgroundColor(int i) {
        this.ClC13lIl = new ColorDrawable(i);
    }
}
