package android.support.v4.widget;

import android.util.Log;
import android.view.View;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

class Cl80C0l838l extends CIOC8C {
    private Method C01O0C;
    private Field C0I1O3C3lI8;

    Cl80C0l838l() {
        try {
            this.C01O0C = View.class.getDeclaredMethod("getDisplayList", null);
        } catch (NoSuchMethodException e) {
            Log.e("SlidingPaneLayout", "Couldn't fetch getDisplayList method; dimming won't work right.", e);
        }
        try {
            this.C0I1O3C3lI8 = View.class.getDeclaredField("mRecreateDisplayList");
            this.C0I1O3C3lI8.setAccessible(true);
        } catch (NoSuchFieldException e2) {
            Log.e("SlidingPaneLayout", "Couldn't fetch mRecreateDisplayList field; dimming will be slow.", e2);
        }
    }

    public void C01O0C(SlidingPaneLayout slidingPaneLayout, View view) {
        if (this.C01O0C == null || this.C0I1O3C3lI8 == null) {
            view.invalidate();
            return;
        }
        try {
            this.C0I1O3C3lI8.setBoolean(view, true);
            this.C01O0C.invoke(view, null);
        } catch (Exception e) {
            Log.e("SlidingPaneLayout", "Error refreshing display list state", e);
        }
        super.C01O0C(slidingPaneLayout, view);
    }
}
