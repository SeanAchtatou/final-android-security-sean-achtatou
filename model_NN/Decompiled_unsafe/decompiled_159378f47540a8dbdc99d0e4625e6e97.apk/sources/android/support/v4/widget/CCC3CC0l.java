package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class CCC3CC0l extends ViewGroup.MarginLayoutParams {
    private static final int[] C11ll3 = {16843137};
    public float C01O0C = 0.0f;
    boolean C0I1O3C3lI8;
    boolean C101lC8O;
    Paint C11013l3;

    public CCC3CC0l() {
        super(-1, -1);
    }

    public CCC3CC0l(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C11ll3);
        this.C01O0C = obtainStyledAttributes.getFloat(0, 0.0f);
        obtainStyledAttributes.recycle();
    }

    public CCC3CC0l(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public CCC3CC0l(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }
}
