package android.support.v4.c;

import java.util.Collection;
import java.util.Iterator;

final class C3CIO118 implements Collection {
    final /* synthetic */ C18Cl1C C01O0C;

    C3CIO118(C18Cl1C c18Cl1C) {
        this.C01O0C = c18Cl1C;
    }

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.C01O0C.C101lC8O();
    }

    public boolean contains(Object obj) {
        return this.C01O0C.C0I1O3C3lI8(obj) >= 0;
    }

    public boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean isEmpty() {
        return this.C01O0C.C01O0C() == 0;
    }

    public Iterator iterator() {
        return new C1l00I1(this.C01O0C, 1);
    }

    public boolean remove(Object obj) {
        int C0I1O3C3lI8 = this.C01O0C.C0I1O3C3lI8(obj);
        if (C0I1O3C3lI8 < 0) {
            return false;
        }
        this.C01O0C.C01O0C(C0I1O3C3lI8);
        return true;
    }

    public boolean removeAll(Collection collection) {
        int i = 0;
        int C01O0C2 = this.C01O0C.C01O0C();
        boolean z = false;
        while (i < C01O0C2) {
            if (collection.contains(this.C01O0C.C01O0C(i, 1))) {
                this.C01O0C.C01O0C(i);
                i--;
                C01O0C2--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public boolean retainAll(Collection collection) {
        int i = 0;
        int C01O0C2 = this.C01O0C.C01O0C();
        boolean z = false;
        while (i < C01O0C2) {
            if (!collection.contains(this.C01O0C.C01O0C(i, 1))) {
                this.C01O0C.C01O0C(i);
                i--;
                C01O0C2--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public int size() {
        return this.C01O0C.C01O0C();
    }

    public Object[] toArray() {
        return this.C01O0C.C0I1O3C3lI8(1);
    }

    public Object[] toArray(Object[] objArr) {
        return this.C01O0C.C01O0C(objArr, 1);
    }
}
