package android.support.v4.c;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

abstract class C18Cl1C {
    C1O10Cl038 C0I1O3C3lI8;
    C1OC33O0lO81 C101lC8O;
    C3CIO118 C11013l3;

    C18Cl1C() {
    }

    public static boolean C01O0C(Map map, Collection collection) {
        for (Object containsKey : collection) {
            if (!map.containsKey(containsKey)) {
                return false;
            }
        }
        return true;
    }

    public static boolean C01O0C(Set set, Object obj) {
        boolean z = true;
        if (set == obj) {
            return true;
        }
        if (!(obj instanceof Set)) {
            return false;
        }
        Set set2 = (Set) obj;
        try {
            if (set.size() != set2.size() || !set.containsAll(set2)) {
                z = false;
            }
            return z;
        } catch (ClassCastException | NullPointerException e) {
            return false;
        }
    }

    public static boolean C0I1O3C3lI8(Map map, Collection collection) {
        int size = map.size();
        for (Object remove : collection) {
            map.remove(remove);
        }
        return size != map.size();
    }

    public static boolean C101lC8O(Map map, Collection collection) {
        int size = map.size();
        Iterator it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        return size != map.size();
    }

    /* access modifiers changed from: protected */
    public abstract int C01O0C();

    /* access modifiers changed from: protected */
    public abstract int C01O0C(Object obj);

    /* access modifiers changed from: protected */
    public abstract Object C01O0C(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract Object C01O0C(int i, Object obj);

    /* access modifiers changed from: protected */
    public abstract void C01O0C(int i);

    /* access modifiers changed from: protected */
    public abstract void C01O0C(Object obj, Object obj2);

    public Object[] C01O0C(Object[] objArr, int i) {
        int C01O0C = C01O0C();
        Object[] objArr2 = objArr.length < C01O0C ? (Object[]) Array.newInstance(objArr.getClass().getComponentType(), C01O0C) : objArr;
        for (int i2 = 0; i2 < C01O0C; i2++) {
            objArr2[i2] = C01O0C(i2, i);
        }
        if (objArr2.length > C01O0C) {
            objArr2[C01O0C] = null;
        }
        return objArr2;
    }

    /* access modifiers changed from: protected */
    public abstract int C0I1O3C3lI8(Object obj);

    /* access modifiers changed from: protected */
    public abstract Map C0I1O3C3lI8();

    public Object[] C0I1O3C3lI8(int i) {
        int C01O0C = C01O0C();
        Object[] objArr = new Object[C01O0C];
        for (int i2 = 0; i2 < C01O0C; i2++) {
            objArr[i2] = C01O0C(i2, i);
        }
        return objArr;
    }

    /* access modifiers changed from: protected */
    public abstract void C101lC8O();

    public Set C11013l3() {
        if (this.C0I1O3C3lI8 == null) {
            this.C0I1O3C3lI8 = new C1O10Cl038(this);
        }
        return this.C0I1O3C3lI8;
    }

    public Set C11ll3() {
        if (this.C101lC8O == null) {
            this.C101lC8O = new C1OC33O0lO81(this);
        }
        return this.C101lC8O;
    }

    public Collection C18Cl1C() {
        if (this.C11013l3 == null) {
            this.C11013l3 = new C3CIO118(this);
        }
        return this.C11013l3;
    }
}
