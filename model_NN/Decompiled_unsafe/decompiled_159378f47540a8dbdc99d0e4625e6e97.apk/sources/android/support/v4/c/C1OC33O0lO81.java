package android.support.v4.c;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

final class C1OC33O0lO81 implements Set {
    final /* synthetic */ C18Cl1C C01O0C;

    C1OC33O0lO81(C18Cl1C c18Cl1C) {
        this.C01O0C = c18Cl1C;
    }

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.C01O0C.C101lC8O();
    }

    public boolean contains(Object obj) {
        return this.C01O0C.C01O0C(obj) >= 0;
    }

    public boolean containsAll(Collection collection) {
        return C18Cl1C.C01O0C(this.C01O0C.C0I1O3C3lI8(), collection);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.c.C18Cl1C.C01O0C(java.util.Set, java.lang.Object):boolean
     arg types: [android.support.v4.c.C1OC33O0lO81, java.lang.Object]
     candidates:
      android.support.v4.c.C18Cl1C.C01O0C(java.util.Map, java.util.Collection):boolean
      android.support.v4.c.C18Cl1C.C01O0C(int, int):java.lang.Object
      android.support.v4.c.C18Cl1C.C01O0C(int, java.lang.Object):java.lang.Object
      android.support.v4.c.C18Cl1C.C01O0C(java.lang.Object, java.lang.Object):void
      android.support.v4.c.C18Cl1C.C01O0C(java.lang.Object[], int):java.lang.Object[]
      android.support.v4.c.C18Cl1C.C01O0C(java.util.Set, java.lang.Object):boolean */
    public boolean equals(Object obj) {
        return C18Cl1C.C01O0C((Set) this, obj);
    }

    public int hashCode() {
        int i = 0;
        for (int C01O0C2 = this.C01O0C.C01O0C() - 1; C01O0C2 >= 0; C01O0C2--) {
            Object C01O0C3 = this.C01O0C.C01O0C(C01O0C2, 0);
            i += C01O0C3 == null ? 0 : C01O0C3.hashCode();
        }
        return i;
    }

    public boolean isEmpty() {
        return this.C01O0C.C01O0C() == 0;
    }

    public Iterator iterator() {
        return new C1l00I1(this.C01O0C, 0);
    }

    public boolean remove(Object obj) {
        int C01O0C2 = this.C01O0C.C01O0C(obj);
        if (C01O0C2 < 0) {
            return false;
        }
        this.C01O0C.C01O0C(C01O0C2);
        return true;
    }

    public boolean removeAll(Collection collection) {
        return C18Cl1C.C0I1O3C3lI8(this.C01O0C.C0I1O3C3lI8(), collection);
    }

    public boolean retainAll(Collection collection) {
        return C18Cl1C.C101lC8O(this.C01O0C.C0I1O3C3lI8(), collection);
    }

    public int size() {
        return this.C01O0C.C01O0C();
    }

    public Object[] toArray() {
        return this.C01O0C.C0I1O3C3lI8(0);
    }

    public Object[] toArray(Object[] objArr) {
        return this.C01O0C.C01O0C(objArr, 0);
    }
}
