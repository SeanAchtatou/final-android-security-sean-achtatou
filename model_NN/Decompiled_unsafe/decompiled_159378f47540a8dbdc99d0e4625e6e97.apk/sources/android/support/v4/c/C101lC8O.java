package android.support.v4.c;

class C101lC8O {
    static final int[] C01O0C = new int[0];
    static final long[] C0I1O3C3lI8 = new long[0];
    static final Object[] C101lC8O = new Object[0];

    public static int C01O0C(int i) {
        return C0I1O3C3lI8(i * 4) / 4;
    }

    static int C01O0C(int[] iArr, int i, int i2) {
        int i3 = 0;
        int i4 = i - 1;
        while (i3 <= i4) {
            int i5 = (i3 + i4) >>> 1;
            int i6 = iArr[i5];
            if (i6 < i2) {
                i3 = i5 + 1;
            } else if (i6 <= i2) {
                return i5;
            } else {
                i4 = i5 - 1;
            }
        }
        return i3 ^ -1;
    }

    public static boolean C01O0C(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static int C0I1O3C3lI8(int i) {
        for (int i2 = 4; i2 < 32; i2++) {
            if (i <= (1 << i2) - 12) {
                return (1 << i2) - 12;
            }
        }
        return i;
    }
}
