package android.support.v4.c;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class C1O10Cl038 implements Set {
    final /* synthetic */ C18Cl1C C01O0C;

    C1O10Cl038(C18Cl1C c18Cl1C) {
        this.C01O0C = c18Cl1C;
    }

    /* renamed from: C01O0C */
    public boolean add(Map.Entry entry) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        int C01O0C2 = this.C01O0C.C01O0C();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            this.C01O0C.C01O0C(entry.getKey(), entry.getValue());
        }
        return C01O0C2 != this.C01O0C.C01O0C();
    }

    public void clear() {
        this.C01O0C.C101lC8O();
    }

    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        int C01O0C2 = this.C01O0C.C01O0C(entry.getKey());
        if (C01O0C2 >= 0) {
            return C101lC8O.C01O0C(this.C01O0C.C01O0C(C01O0C2, 1), entry.getValue());
        }
        return false;
    }

    public boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.c.C18Cl1C.C01O0C(java.util.Set, java.lang.Object):boolean
     arg types: [android.support.v4.c.C1O10Cl038, java.lang.Object]
     candidates:
      android.support.v4.c.C18Cl1C.C01O0C(java.util.Map, java.util.Collection):boolean
      android.support.v4.c.C18Cl1C.C01O0C(int, int):java.lang.Object
      android.support.v4.c.C18Cl1C.C01O0C(int, java.lang.Object):java.lang.Object
      android.support.v4.c.C18Cl1C.C01O0C(java.lang.Object, java.lang.Object):void
      android.support.v4.c.C18Cl1C.C01O0C(java.lang.Object[], int):java.lang.Object[]
      android.support.v4.c.C18Cl1C.C01O0C(java.util.Set, java.lang.Object):boolean */
    public boolean equals(Object obj) {
        return C18Cl1C.C01O0C((Set) this, obj);
    }

    public int hashCode() {
        int C01O0C2 = this.C01O0C.C01O0C() - 1;
        int i = 0;
        while (C01O0C2 >= 0) {
            Object C01O0C3 = this.C01O0C.C01O0C(C01O0C2, 0);
            Object C01O0C4 = this.C01O0C.C01O0C(C01O0C2, 1);
            C01O0C2--;
            i += (C01O0C4 == null ? 0 : C01O0C4.hashCode()) ^ (C01O0C3 == null ? 0 : C01O0C3.hashCode());
        }
        return i;
    }

    public boolean isEmpty() {
        return this.C01O0C.C01O0C() == 0;
    }

    public Iterator iterator() {
        return new C3C1C0I8l3(this.C01O0C);
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public int size() {
        return this.C01O0C.C01O0C();
    }

    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    public Object[] toArray(Object[] objArr) {
        throw new UnsupportedOperationException();
    }
}
