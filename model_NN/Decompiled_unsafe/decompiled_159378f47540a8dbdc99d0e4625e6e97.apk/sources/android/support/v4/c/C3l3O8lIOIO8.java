package android.support.v4.c;

public class C3l3O8lIOIO8 implements Cloneable {
    private static final Object C01O0C = new Object();
    private boolean C0I1O3C3lI8;
    private int[] C101lC8O;
    private Object[] C11013l3;
    private int C11ll3;

    public C3l3O8lIOIO8() {
        this(10);
    }

    public C3l3O8lIOIO8(int i) {
        this.C0I1O3C3lI8 = false;
        if (i == 0) {
            this.C101lC8O = C101lC8O.C01O0C;
            this.C11013l3 = C101lC8O.C101lC8O;
        } else {
            int C01O0C2 = C101lC8O.C01O0C(i);
            this.C101lC8O = new int[C01O0C2];
            this.C11013l3 = new Object[C01O0C2];
        }
        this.C11ll3 = 0;
    }

    private void C11013l3() {
        int i = this.C11ll3;
        int[] iArr = this.C101lC8O;
        Object[] objArr = this.C11013l3;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != C01O0C) {
                if (i3 != i2) {
                    iArr[i2] = iArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.C0I1O3C3lI8 = false;
        this.C11ll3 = i2;
    }

    public int C01O0C(int i) {
        if (this.C0I1O3C3lI8) {
            C11013l3();
        }
        return this.C101lC8O[i];
    }

    /* renamed from: C01O0C */
    public C3l3O8lIOIO8 clone() {
        try {
            C3l3O8lIOIO8 c3l3O8lIOIO8 = (C3l3O8lIOIO8) super.clone();
            try {
                c3l3O8lIOIO8.C101lC8O = (int[]) this.C101lC8O.clone();
                c3l3O8lIOIO8.C11013l3 = (Object[]) this.C11013l3.clone();
                return c3l3O8lIOIO8;
            } catch (CloneNotSupportedException e) {
                return c3l3O8lIOIO8;
            }
        } catch (CloneNotSupportedException e2) {
            return null;
        }
    }

    public int C0I1O3C3lI8() {
        if (this.C0I1O3C3lI8) {
            C11013l3();
        }
        return this.C11ll3;
    }

    public Object C0I1O3C3lI8(int i) {
        if (this.C0I1O3C3lI8) {
            C11013l3();
        }
        return this.C11013l3[i];
    }

    public void C101lC8O() {
        int i = this.C11ll3;
        Object[] objArr = this.C11013l3;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.C11ll3 = 0;
        this.C0I1O3C3lI8 = false;
    }

    public String toString() {
        if (C0I1O3C3lI8() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.C11ll3 * 28);
        sb.append('{');
        for (int i = 0; i < this.C11ll3; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(C01O0C(i));
            sb.append('=');
            Object C0I1O3C3lI82 = C0I1O3C3lI8(i);
            if (C0I1O3C3lI82 != this) {
                sb.append(C0I1O3C3lI82);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
