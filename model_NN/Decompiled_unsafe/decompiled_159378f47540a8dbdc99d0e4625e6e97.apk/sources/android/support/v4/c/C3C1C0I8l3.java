package android.support.v4.c;

import java.util.Iterator;
import java.util.Map;

final class C3C1C0I8l3 implements Iterator, Map.Entry {
    int C01O0C;
    int C0I1O3C3lI8;
    boolean C101lC8O = false;
    final /* synthetic */ C18Cl1C C11013l3;

    C3C1C0I8l3(C18Cl1C c18Cl1C) {
        this.C11013l3 = c18Cl1C;
        this.C01O0C = c18Cl1C.C01O0C() - 1;
        this.C0I1O3C3lI8 = -1;
    }

    /* renamed from: C01O0C */
    public Map.Entry next() {
        this.C0I1O3C3lI8++;
        this.C101lC8O = true;
        return this;
    }

    public final boolean equals(Object obj) {
        boolean z = true;
        if (!this.C101lC8O) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        } else if (!(obj instanceof Map.Entry)) {
            return false;
        } else {
            Map.Entry entry = (Map.Entry) obj;
            if (!C101lC8O.C01O0C(entry.getKey(), this.C11013l3.C01O0C(this.C0I1O3C3lI8, 0)) || !C101lC8O.C01O0C(entry.getValue(), this.C11013l3.C01O0C(this.C0I1O3C3lI8, 1))) {
                z = false;
            }
            return z;
        }
    }

    public Object getKey() {
        if (this.C101lC8O) {
            return this.C11013l3.C01O0C(this.C0I1O3C3lI8, 0);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public Object getValue() {
        if (this.C101lC8O) {
            return this.C11013l3.C01O0C(this.C0I1O3C3lI8, 1);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public boolean hasNext() {
        return this.C0I1O3C3lI8 < this.C01O0C;
    }

    public final int hashCode() {
        int i = 0;
        if (!this.C101lC8O) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        Object C01O0C2 = this.C11013l3.C01O0C(this.C0I1O3C3lI8, 0);
        Object C01O0C3 = this.C11013l3.C01O0C(this.C0I1O3C3lI8, 1);
        int hashCode = C01O0C2 == null ? 0 : C01O0C2.hashCode();
        if (C01O0C3 != null) {
            i = C01O0C3.hashCode();
        }
        return i ^ hashCode;
    }

    public void remove() {
        if (!this.C101lC8O) {
            throw new IllegalStateException();
        }
        this.C11013l3.C01O0C(this.C0I1O3C3lI8);
        this.C0I1O3C3lI8--;
        this.C01O0C--;
        this.C101lC8O = false;
    }

    public Object setValue(Object obj) {
        if (this.C101lC8O) {
            return this.C11013l3.C01O0C(this.C0I1O3C3lI8, obj);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public final String toString() {
        return getKey() + "=" + getValue();
    }
}
