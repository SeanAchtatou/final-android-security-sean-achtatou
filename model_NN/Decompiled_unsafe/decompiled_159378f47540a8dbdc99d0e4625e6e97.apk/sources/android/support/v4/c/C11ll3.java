package android.support.v4.c;

import android.util.Log;
import java.io.Writer;

public class C11ll3 extends Writer {
    private final String C01O0C;
    private StringBuilder C0I1O3C3lI8 = new StringBuilder(128);

    public C11ll3(String str) {
        this.C01O0C = str;
    }

    private void C01O0C() {
        if (this.C0I1O3C3lI8.length() > 0) {
            Log.d(this.C01O0C, this.C0I1O3C3lI8.toString());
            this.C0I1O3C3lI8.delete(0, this.C0I1O3C3lI8.length());
        }
    }

    public void close() {
        C01O0C();
    }

    public void flush() {
        C01O0C();
    }

    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c = cArr[i + i3];
            if (c == 10) {
                C01O0C();
            } else {
                this.C0I1O3C3lI8.append(c);
            }
        }
    }
}
