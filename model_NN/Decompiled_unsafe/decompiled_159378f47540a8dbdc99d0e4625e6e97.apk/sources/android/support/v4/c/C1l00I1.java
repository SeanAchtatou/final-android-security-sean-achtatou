package android.support.v4.c;

import java.util.Iterator;

final class C1l00I1 implements Iterator {
    final int C01O0C;
    int C0I1O3C3lI8;
    int C101lC8O;
    boolean C11013l3 = false;
    final /* synthetic */ C18Cl1C C11ll3;

    C1l00I1(C18Cl1C c18Cl1C, int i) {
        this.C11ll3 = c18Cl1C;
        this.C01O0C = i;
        this.C0I1O3C3lI8 = c18Cl1C.C01O0C();
    }

    public boolean hasNext() {
        return this.C101lC8O < this.C0I1O3C3lI8;
    }

    public Object next() {
        Object C01O0C2 = this.C11ll3.C01O0C(this.C101lC8O, this.C01O0C);
        this.C101lC8O++;
        this.C11013l3 = true;
        return C01O0C2;
    }

    public void remove() {
        if (!this.C11013l3) {
            throw new IllegalStateException();
        }
        this.C101lC8O--;
        this.C0I1O3C3lI8--;
        this.C11013l3 = false;
        this.C11ll3.C01O0C(this.C101lC8O);
    }
}
