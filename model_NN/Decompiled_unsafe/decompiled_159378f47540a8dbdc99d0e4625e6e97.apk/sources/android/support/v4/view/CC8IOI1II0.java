package android.support.v4.view;

import android.view.KeyEvent;

class CC8IOI1II0 {
    public static int C01O0C(int i) {
        return KeyEvent.normalizeMetaState(i);
    }

    public static boolean C01O0C(int i, int i2) {
        return KeyEvent.metaStateHasModifiers(i, i2);
    }

    public static boolean C0I1O3C3lI8(int i) {
        return KeyEvent.metaStateHasNoModifiers(i);
    }
}
