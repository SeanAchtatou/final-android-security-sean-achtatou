package android.support.v4.view;

import android.graphics.Paint;
import android.os.Build;
import android.view.View;

public class CO88CO1Cl383 {
    static final I0OlCO0CI13 C01O0C;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            C01O0C = new I003I0();
        } else if (i >= 19) {
            C01O0C = new I0l3Oll3();
        } else if (i >= 17) {
            C01O0C = new I0IC1O01888();
        } else if (i >= 16) {
            C01O0C = new I0CIIIC();
        } else if (i >= 14) {
            C01O0C = new I08O3C();
        } else if (i >= 11) {
            C01O0C = new I088l3088();
        } else if (i >= 9) {
            C01O0C = new I03lII1();
        } else if (i >= 7) {
            C01O0C = new I008018O();
        } else {
            C01O0C = new I003OlCCOlC();
        }
    }

    public static int C01O0C(View view) {
        return C01O0C.C01O0C(view);
    }

    public static void C01O0C(View view, int i, int i2, int i3, int i4) {
        C01O0C.C01O0C(view, i, i2, i3, i4);
    }

    public static void C01O0C(View view, int i, Paint paint) {
        C01O0C.C01O0C(view, i, paint);
    }

    public static void C01O0C(View view, Paint paint) {
        C01O0C.C01O0C(view, paint);
    }

    public static void C01O0C(View view, C01O0C c01o0c) {
        C01O0C.C01O0C(view, c01o0c);
    }

    public static void C01O0C(View view, Runnable runnable) {
        C01O0C.C01O0C(view, runnable);
    }

    public static void C01O0C(View view, boolean z) {
        C01O0C.C01O0C(view, z);
    }

    public static boolean C01O0C(View view, int i) {
        return C01O0C.C01O0C(view, i);
    }

    public static void C0I1O3C3lI8(View view) {
        C01O0C.C0I1O3C3lI8(view);
    }

    public static void C0I1O3C3lI8(View view, int i) {
        C01O0C.C0I1O3C3lI8(view, i);
    }

    public static int C101lC8O(View view) {
        return C01O0C.C101lC8O(view);
    }

    public static int C11013l3(View view) {
        return C01O0C.C11013l3(view);
    }

    public static boolean C11ll3(View view) {
        return C01O0C.C11ll3(view);
    }

    public static boolean C18Cl1C(View view) {
        return C01O0C.C18Cl1C(view);
    }
}
