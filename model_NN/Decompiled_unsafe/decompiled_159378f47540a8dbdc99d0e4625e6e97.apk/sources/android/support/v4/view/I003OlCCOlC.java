package android.support.v4.view;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;
import java.util.WeakHashMap;

class I003OlCCOlC implements I0OlCO0CI13 {
    WeakHashMap C01O0C = null;

    I003OlCCOlC() {
    }

    public int C01O0C(View view) {
        return 2;
    }

    /* access modifiers changed from: package-private */
    public long C01O0C() {
        return 10;
    }

    public void C01O0C(View view, int i, int i2, int i3, int i4) {
        view.invalidate(i, i2, i3, i4);
    }

    public void C01O0C(View view, int i, Paint paint) {
    }

    public void C01O0C(View view, Paint paint) {
    }

    public void C01O0C(View view, C01O0C c01o0c) {
    }

    public void C01O0C(View view, Runnable runnable) {
        view.postDelayed(runnable, C01O0C());
    }

    public void C01O0C(View view, boolean z) {
    }

    public boolean C01O0C(View view, int i) {
        return false;
    }

    public void C0I1O3C3lI8(View view) {
        view.invalidate();
    }

    public void C0I1O3C3lI8(View view, int i) {
    }

    public int C101lC8O(View view) {
        return 0;
    }

    public int C11013l3(View view) {
        return 0;
    }

    public boolean C11ll3(View view) {
        Drawable background = view.getBackground();
        return background != null && background.getOpacity() == -1;
    }

    public boolean C18Cl1C(View view) {
        return false;
    }
}
