package android.support.v4.view;

import android.view.View;
import java.util.Comparator;

class IIOC18I8 implements Comparator {
    IIOC18I8() {
    }

    /* renamed from: C01O0C */
    public int compare(View view, View view2) {
        II083CII ii083cii = (II083CII) view.getLayoutParams();
        II083CII ii083cii2 = (II083CII) view2.getLayoutParams();
        return ii083cii.C01O0C != ii083cii2.C01O0C ? ii083cii.C01O0C ? 1 : -1 : ii083cii.C11ll3 - ii083cii2.C11ll3;
    }
}
