package android.support.v4.view;

import android.os.Parcel;
import android.support.v4.b.C101lC8O;
import android.support.v4.view.ViewPager;

final class IIO3O0CI implements C101lC8O {
    IIO3O0CI() {
    }

    /* renamed from: C0I1O3C3lI8 */
    public ViewPager.SavedState C01O0C(Parcel parcel, ClassLoader classLoader) {
        return new ViewPager.SavedState(parcel, classLoader);
    }

    /* renamed from: C0I1O3C3lI8 */
    public ViewPager.SavedState[] C01O0C(int i) {
        return new ViewPager.SavedState[i];
    }
}
