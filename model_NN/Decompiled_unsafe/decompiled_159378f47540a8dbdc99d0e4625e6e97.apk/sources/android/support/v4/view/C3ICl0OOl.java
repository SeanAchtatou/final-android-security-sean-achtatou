package android.support.v4.view;

import android.os.Build;
import android.view.KeyEvent;

public class C3ICl0OOl {
    static final C8CI00 C01O0C;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            C01O0C = new C831O13C118();
        } else {
            C01O0C = new C3l3O8lIOIO8();
        }
    }

    public static boolean C01O0C(KeyEvent keyEvent) {
        return C01O0C.C0I1O3C3lI8(keyEvent.getMetaState());
    }

    public static boolean C01O0C(KeyEvent keyEvent, int i) {
        return C01O0C.C01O0C(keyEvent.getMetaState(), i);
    }

    public static void C0I1O3C3lI8(KeyEvent keyEvent) {
        C01O0C.C01O0C(keyEvent);
    }
}
