package android.support.v4.view;

import android.os.Build;
import android.view.MotionEvent;

public class CCC3CC0l {
    static final CII3C813OIC8 C01O0C;

    static {
        if (Build.VERSION.SDK_INT >= 5) {
            C01O0C = new CI3C103l01O();
        } else {
            C01O0C = new CI0I8l333131();
        }
    }

    public static int C01O0C(MotionEvent motionEvent) {
        return motionEvent.getAction() & 255;
    }

    public static int C01O0C(MotionEvent motionEvent, int i) {
        return C01O0C.C01O0C(motionEvent, i);
    }

    public static int C0I1O3C3lI8(MotionEvent motionEvent) {
        return (motionEvent.getAction() & 65280) >> 8;
    }

    public static int C0I1O3C3lI8(MotionEvent motionEvent, int i) {
        return C01O0C.C0I1O3C3lI8(motionEvent, i);
    }

    public static float C101lC8O(MotionEvent motionEvent, int i) {
        return C01O0C.C101lC8O(motionEvent, i);
    }

    public static int C101lC8O(MotionEvent motionEvent) {
        return C01O0C.C01O0C(motionEvent);
    }

    public static float C11013l3(MotionEvent motionEvent, int i) {
        return C01O0C.C11013l3(motionEvent, i);
    }
}
