package android.support.v4.view;

import android.os.Build;
import android.view.VelocityTracker;

public class ClC13lIl {
    static final CO1830lI8C03 C01O0C;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            C01O0C = new CO081lO0OC0();
        } else {
            C01O0C = new ClO80C3lOO8();
        }
    }

    public static float C01O0C(VelocityTracker velocityTracker, int i) {
        return C01O0C.C01O0C(velocityTracker, i);
    }

    public static float C0I1O3C3lI8(VelocityTracker velocityTracker, int i) {
        return C01O0C.C0I1O3C3lI8(velocityTracker, i);
    }
}
