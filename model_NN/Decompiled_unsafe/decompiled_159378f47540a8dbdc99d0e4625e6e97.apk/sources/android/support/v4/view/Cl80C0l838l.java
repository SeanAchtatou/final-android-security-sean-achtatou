package android.support.v4.view;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

public abstract class Cl80C0l838l {
    private DataSetObservable C01O0C;

    public float C01O0C(int i) {
        return 1.0f;
    }

    public abstract int C01O0C();

    public int C01O0C(Object obj) {
        return -1;
    }

    public Object C01O0C(View view, int i) {
        throw new UnsupportedOperationException("Required method instantiateItem was not overridden");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.Cl80C0l838l.C01O0C(android.view.View, int):java.lang.Object
     arg types: [android.view.ViewGroup, int]
     candidates:
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.ViewGroup, int):java.lang.Object
      android.support.v4.view.Cl80C0l838l.C01O0C(android.os.Parcelable, java.lang.ClassLoader):void
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.View, java.lang.Object):boolean
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.View, int):java.lang.Object */
    public Object C01O0C(ViewGroup viewGroup, int i) {
        return C01O0C((View) viewGroup, i);
    }

    public void C01O0C(DataSetObserver dataSetObserver) {
        this.C01O0C.registerObserver(dataSetObserver);
    }

    public void C01O0C(Parcelable parcelable, ClassLoader classLoader) {
    }

    public void C01O0C(View view) {
    }

    public void C01O0C(View view, int i, Object obj) {
        throw new UnsupportedOperationException("Required method destroyItem was not overridden");
    }

    public void C01O0C(ViewGroup viewGroup) {
        C01O0C((View) viewGroup);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.Cl80C0l838l.C01O0C(android.view.View, int, java.lang.Object):void
     arg types: [android.view.ViewGroup, int, java.lang.Object]
     candidates:
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.ViewGroup, int, java.lang.Object):void
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.View, int, java.lang.Object):void */
    public void C01O0C(ViewGroup viewGroup, int i, Object obj) {
        C01O0C((View) viewGroup, i, obj);
    }

    public abstract boolean C01O0C(View view, Object obj);

    public Parcelable C0I1O3C3lI8() {
        return null;
    }

    public void C0I1O3C3lI8(DataSetObserver dataSetObserver) {
        this.C01O0C.unregisterObserver(dataSetObserver);
    }

    public void C0I1O3C3lI8(View view) {
    }

    public void C0I1O3C3lI8(View view, int i, Object obj) {
    }

    public void C0I1O3C3lI8(ViewGroup viewGroup) {
        C0I1O3C3lI8((View) viewGroup);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.Cl80C0l838l.C0I1O3C3lI8(android.view.View, int, java.lang.Object):void
     arg types: [android.view.ViewGroup, int, java.lang.Object]
     candidates:
      android.support.v4.view.Cl80C0l838l.C0I1O3C3lI8(android.view.ViewGroup, int, java.lang.Object):void
      android.support.v4.view.Cl80C0l838l.C0I1O3C3lI8(android.view.View, int, java.lang.Object):void */
    public void C0I1O3C3lI8(ViewGroup viewGroup, int i, Object obj) {
        C0I1O3C3lI8((View) viewGroup, i, obj);
    }
}
