package android.support.v4.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class II083CII extends ViewGroup.LayoutParams {
    public boolean C01O0C;
    public int C0I1O3C3lI8;
    float C101lC8O = 0.0f;
    boolean C11013l3;
    int C11ll3;
    int C18Cl1C;

    public II083CII() {
        super(-1, -1);
    }

    public II083CII(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, ViewPager.C01O0C);
        this.C0I1O3C3lI8 = obtainStyledAttributes.getInteger(0, 48);
        obtainStyledAttributes.recycle();
    }
}
