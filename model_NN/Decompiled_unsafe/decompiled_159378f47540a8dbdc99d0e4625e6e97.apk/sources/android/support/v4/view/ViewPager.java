package android.support.v4.view;

import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.b.C01O0C;
import android.support.v4.widget.C3C1C0I8l3;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ViewPager extends ViewGroup {
    /* access modifiers changed from: private */
    public static final int[] C01O0C = {16842931};
    private static final Comparator C101lC8O = new I8C3388l1301();
    private static final Interpolator C11013l3 = new IC11OO80I3();
    private static final IIOC18I8 IIO3O0CI = new IIOC18I8();
    private int C0I1O3C3lI8;
    private final ArrayList C11ll3;
    private final ICOI8lC3 C18Cl1C;
    private Cl80C0l838l C1O10Cl038;
    private int C1OC33O0lO81;
    private final Rect C1l00I1;
    private int C3C1C0I8l3;
    private Parcelable C3CIO118;
    private ClassLoader C3ICl0OOl;
    private Scroller C3l3O8lIOIO8;
    private IICICOC38 C3llC38O1;
    private int C831O13C118;
    private Drawable C8CI00;
    private int CC38COI1l3I;
    private int CC8IOI1II0;
    private float CCC3CC0l;
    private float CI0I8l333131;
    private int CI3C103l01O;
    private int CII3C813OIC8;
    private boolean CIOC8C;
    private boolean CO081lO0OC0;
    private boolean CO1830lI8C03;
    private int CO30CC1l0313;
    private int CO88CO1Cl383;
    private boolean Cl80C0l838l;
    private boolean ClC13lIl;
    private int ClO80C3lOO8;
    private int I003I0;
    private float I003OlCCOlC;
    private float I008018O;
    private float I03lII1;
    private float I088l3088;
    private int I08O3C;
    private VelocityTracker I0CIIIC;
    private int I0IC1O01888;
    private int I0OlCO0CI13;
    private int I0l3Oll3;
    private int I1CO03;
    private boolean I1I11O81II;
    private C3C1C0I8l3 I30OCIOO;
    private C3C1C0I8l3 I3ClO1C31;
    private boolean I80183lOl;
    private boolean I801IO8CII;
    private boolean I8C3388l1301;
    private int IC11OO80I3;
    private II1lC1O ICI3C3O;
    private II1lC1O ICOI8lC3;
    private II0ll1CC13l II083CII;
    private II3OlOI8II0C II0ll1CC13l;
    private Method II1lC1O;
    private int II3OlOI8II0C;
    private ArrayList IICICOC38;
    private final Runnable IIOC18I8;
    private int IIOII113C033;

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = C01O0C.C01O0C(new IIO3O0CI());
        int C01O0C;
        Parcelable C0I1O3C3lI8;
        ClassLoader C101lC8O;

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel);
            classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
            this.C01O0C = parcel.readInt();
            this.C0I1O3C3lI8 = parcel.readParcelable(classLoader);
            this.C101lC8O = classLoader;
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.C01O0C + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.C01O0C);
            parcel.writeParcelable(this.C0I1O3C3lI8, i);
        }
    }

    private int C01O0C(int i, float f, int i2, int i3) {
        if (Math.abs(i3) <= this.I0OlCO0CI13 || Math.abs(i2) <= this.I0IC1O01888) {
            i = (int) ((i >= this.C1OC33O0lO81 ? 0.4f : 0.6f) + ((float) i) + f);
        } else if (i2 <= 0) {
            i++;
        }
        return this.C11ll3.size() > 0 ? Math.max(((ICOI8lC3) this.C11ll3.get(0)).C0I1O3C3lI8, Math.min(i, ((ICOI8lC3) this.C11ll3.get(this.C11ll3.size() - 1)).C0I1O3C3lI8)) : i;
    }

    private Rect C01O0C(Rect rect, View view) {
        Rect rect2 = rect == null ? new Rect() : rect;
        if (view == null) {
            rect2.set(0, 0, 0, 0);
            return rect2;
        }
        rect2.left = view.getLeft();
        rect2.right = view.getRight();
        rect2.top = view.getTop();
        rect2.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect2.left += viewGroup.getLeft();
            rect2.right += viewGroup.getRight();
            rect2.top += viewGroup.getTop();
            rect2.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect2;
    }

    private void C01O0C(int i, int i2, int i3, int i4) {
        if (i2 <= 0 || this.C11ll3.isEmpty()) {
            ICOI8lC3 C0I1O3C3lI82 = C0I1O3C3lI8(this.C1OC33O0lO81);
            int min = (int) ((C0I1O3C3lI82 != null ? Math.min(C0I1O3C3lI82.C11ll3, this.CI0I8l333131) : 0.0f) * ((float) ((i - getPaddingLeft()) - getPaddingRight())));
            if (min != getScrollX()) {
                C01O0C(false);
                scrollTo(min, getScrollY());
                return;
            }
            return;
        }
        int paddingLeft = (int) (((float) (((i - getPaddingLeft()) - getPaddingRight()) + i3)) * (((float) getScrollX()) / ((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4))));
        scrollTo(paddingLeft, getScrollY());
        if (!this.C3l3O8lIOIO8.isFinished()) {
            this.C3l3O8lIOIO8.startScroll(paddingLeft, 0, (int) (C0I1O3C3lI8(this.C1OC33O0lO81).C11ll3 * ((float) i)), 0, this.C3l3O8lIOIO8.getDuration() - this.C3l3O8lIOIO8.timePassed());
        }
    }

    private void C01O0C(int i, boolean z, int i2, boolean z2) {
        int i3;
        ICOI8lC3 C0I1O3C3lI82 = C0I1O3C3lI8(i);
        if (C0I1O3C3lI82 != null) {
            i3 = (int) (Math.max(this.CCC3CC0l, Math.min(C0I1O3C3lI82.C11ll3, this.CI0I8l333131)) * ((float) getClientWidth()));
        } else {
            i3 = 0;
        }
        if (z) {
            C01O0C(i3, 0, i2);
            if (z2 && this.ICI3C3O != null) {
                this.ICI3C3O.C01O0C(i);
            }
            if (z2 && this.ICOI8lC3 != null) {
                this.ICOI8lC3.C01O0C(i);
                return;
            }
            return;
        }
        if (z2 && this.ICI3C3O != null) {
            this.ICI3C3O.C01O0C(i);
        }
        if (z2 && this.ICOI8lC3 != null) {
            this.ICOI8lC3.C01O0C(i);
        }
        C01O0C(false);
        scrollTo(i3, 0);
        C11013l3(i3);
    }

    private void C01O0C(ICOI8lC3 iCOI8lC3, int i, ICOI8lC3 iCOI8lC32) {
        ICOI8lC3 iCOI8lC33;
        ICOI8lC3 iCOI8lC34;
        int C01O0C2 = this.C1O10Cl038.C01O0C();
        int clientWidth = getClientWidth();
        float f = clientWidth > 0 ? ((float) this.C831O13C118) / ((float) clientWidth) : 0.0f;
        if (iCOI8lC32 != null) {
            int i2 = iCOI8lC32.C0I1O3C3lI8;
            if (i2 < iCOI8lC3.C0I1O3C3lI8) {
                float f2 = iCOI8lC32.C11ll3 + iCOI8lC32.C11013l3 + f;
                int i3 = i2 + 1;
                int i4 = 0;
                while (i3 <= iCOI8lC3.C0I1O3C3lI8 && i4 < this.C11ll3.size()) {
                    Object obj = this.C11ll3.get(i4);
                    while (true) {
                        iCOI8lC34 = (ICOI8lC3) obj;
                        if (i3 > iCOI8lC34.C0I1O3C3lI8 && i4 < this.C11ll3.size() - 1) {
                            i4++;
                            obj = this.C11ll3.get(i4);
                        }
                    }
                    while (i3 < iCOI8lC34.C0I1O3C3lI8) {
                        f2 += this.C1O10Cl038.C01O0C(i3) + f;
                        i3++;
                    }
                    iCOI8lC34.C11ll3 = f2;
                    f2 += iCOI8lC34.C11013l3 + f;
                    i3++;
                }
            } else if (i2 > iCOI8lC3.C0I1O3C3lI8) {
                int size = this.C11ll3.size() - 1;
                float f3 = iCOI8lC32.C11ll3;
                int i5 = i2 - 1;
                while (i5 >= iCOI8lC3.C0I1O3C3lI8 && size >= 0) {
                    Object obj2 = this.C11ll3.get(size);
                    while (true) {
                        iCOI8lC33 = (ICOI8lC3) obj2;
                        if (i5 < iCOI8lC33.C0I1O3C3lI8 && size > 0) {
                            size--;
                            obj2 = this.C11ll3.get(size);
                        }
                    }
                    while (i5 > iCOI8lC33.C0I1O3C3lI8) {
                        f3 -= this.C1O10Cl038.C01O0C(i5) + f;
                        i5--;
                    }
                    f3 -= iCOI8lC33.C11013l3 + f;
                    iCOI8lC33.C11ll3 = f3;
                    i5--;
                }
            }
        }
        int size2 = this.C11ll3.size();
        float f4 = iCOI8lC3.C11ll3;
        int i6 = iCOI8lC3.C0I1O3C3lI8 - 1;
        this.CCC3CC0l = iCOI8lC3.C0I1O3C3lI8 == 0 ? iCOI8lC3.C11ll3 : -3.4028235E38f;
        this.CI0I8l333131 = iCOI8lC3.C0I1O3C3lI8 == C01O0C2 + -1 ? (iCOI8lC3.C11ll3 + iCOI8lC3.C11013l3) - 1.0f : Float.MAX_VALUE;
        for (int i7 = i - 1; i7 >= 0; i7--) {
            ICOI8lC3 iCOI8lC35 = (ICOI8lC3) this.C11ll3.get(i7);
            float f5 = f4;
            while (i6 > iCOI8lC35.C0I1O3C3lI8) {
                f5 -= this.C1O10Cl038.C01O0C(i6) + f;
                i6--;
            }
            f4 = f5 - (iCOI8lC35.C11013l3 + f);
            iCOI8lC35.C11ll3 = f4;
            if (iCOI8lC35.C0I1O3C3lI8 == 0) {
                this.CCC3CC0l = f4;
            }
            i6--;
        }
        float f6 = iCOI8lC3.C11ll3 + iCOI8lC3.C11013l3 + f;
        int i8 = iCOI8lC3.C0I1O3C3lI8 + 1;
        for (int i9 = i + 1; i9 < size2; i9++) {
            ICOI8lC3 iCOI8lC36 = (ICOI8lC3) this.C11ll3.get(i9);
            float f7 = f6;
            while (i8 < iCOI8lC36.C0I1O3C3lI8) {
                f7 = this.C1O10Cl038.C01O0C(i8) + f + f7;
                i8++;
            }
            if (iCOI8lC36.C0I1O3C3lI8 == C01O0C2 - 1) {
                this.CI0I8l333131 = (iCOI8lC36.C11013l3 + f7) - 1.0f;
            }
            iCOI8lC36.C11ll3 = f7;
            f6 = f7 + iCOI8lC36.C11013l3 + f;
            i8++;
        }
        this.I801IO8CII = false;
    }

    private void C01O0C(MotionEvent motionEvent) {
        int C0I1O3C3lI82 = CCC3CC0l.C0I1O3C3lI8(motionEvent);
        if (CCC3CC0l.C0I1O3C3lI8(motionEvent, C0I1O3C3lI82) == this.I08O3C) {
            int i = C0I1O3C3lI82 == 0 ? 1 : 0;
            this.I003OlCCOlC = CCC3CC0l.C101lC8O(motionEvent, i);
            this.I08O3C = CCC3CC0l.C0I1O3C3lI8(motionEvent, i);
            if (this.I0CIIIC != null) {
                this.I0CIIIC.clear();
            }
        }
    }

    private void C01O0C(boolean z) {
        boolean z2 = this.IIOII113C033 == 2;
        if (z2) {
            setScrollingCacheEnabled(false);
            this.C3l3O8lIOIO8.abortAnimation();
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.C3l3O8lIOIO8.getCurrX();
            int currY = this.C3l3O8lIOIO8.getCurrY();
            if (!(scrollX == currX && scrollY == currY)) {
                scrollTo(currX, currY);
            }
        }
        this.ClC13lIl = false;
        boolean z3 = z2;
        for (int i = 0; i < this.C11ll3.size(); i++) {
            ICOI8lC3 iCOI8lC3 = (ICOI8lC3) this.C11ll3.get(i);
            if (iCOI8lC3.C101lC8O) {
                iCOI8lC3.C101lC8O = false;
                z3 = true;
            }
        }
        if (!z3) {
            return;
        }
        if (z) {
            CO88CO1Cl383.C01O0C(this, this.IIOC18I8);
        } else {
            this.IIOC18I8.run();
        }
    }

    private boolean C01O0C(float f, float f2) {
        return (f < ((float) this.CO88CO1Cl383) && f2 > 0.0f) || (f > ((float) (getWidth() - this.CO88CO1Cl383)) && f2 < 0.0f);
    }

    private void C0I1O3C3lI8(boolean z) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            CO88CO1Cl383.C01O0C(getChildAt(i), z ? 2 : 0, null);
        }
    }

    private boolean C0I1O3C3lI8(float f) {
        boolean z;
        float f2;
        boolean z2 = true;
        boolean z3 = false;
        this.I003OlCCOlC = f;
        float scrollX = ((float) getScrollX()) + (this.I003OlCCOlC - f);
        int clientWidth = getClientWidth();
        float f3 = ((float) clientWidth) * this.CCC3CC0l;
        float f4 = ((float) clientWidth) * this.CI0I8l333131;
        ICOI8lC3 iCOI8lC3 = (ICOI8lC3) this.C11ll3.get(0);
        ICOI8lC3 iCOI8lC32 = (ICOI8lC3) this.C11ll3.get(this.C11ll3.size() - 1);
        if (iCOI8lC3.C0I1O3C3lI8 != 0) {
            f3 = iCOI8lC3.C11ll3 * ((float) clientWidth);
            z = false;
        } else {
            z = true;
        }
        if (iCOI8lC32.C0I1O3C3lI8 != this.C1O10Cl038.C01O0C() - 1) {
            f2 = iCOI8lC32.C11ll3 * ((float) clientWidth);
            z2 = false;
        } else {
            f2 = f4;
        }
        if (scrollX < f3) {
            if (z) {
                z3 = this.I30OCIOO.C01O0C(Math.abs(f3 - scrollX) / ((float) clientWidth));
            }
        } else if (scrollX > f2) {
            if (z2) {
                z3 = this.I3ClO1C31.C01O0C(Math.abs(scrollX - f2) / ((float) clientWidth));
            }
            f3 = f2;
        } else {
            f3 = scrollX;
        }
        this.I003OlCCOlC += f3 - ((float) ((int) f3));
        scrollTo((int) f3, getScrollY());
        C11013l3((int) f3);
        return z3;
    }

    private void C101lC8O(boolean z) {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.C01O0C(int, float, int):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.C01O0C(android.support.v4.view.ICOI8lC3, int, android.support.v4.view.ICOI8lC3):void
      android.support.v4.view.ViewPager.C01O0C(int, int, int):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean):void
      android.support.v4.view.ViewPager.C01O0C(int, float, int):void */
    private boolean C11013l3(int i) {
        if (this.C11ll3.size() == 0) {
            this.I8C3388l1301 = false;
            C01O0C(0, 0.0f, 0);
            if (this.I8C3388l1301) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
        ICOI8lC3 C1O10Cl0382 = C1O10Cl038();
        int clientWidth = getClientWidth();
        int i2 = this.C831O13C118 + clientWidth;
        float f = ((float) this.C831O13C118) / ((float) clientWidth);
        int i3 = C1O10Cl0382.C0I1O3C3lI8;
        float f2 = ((((float) i) / ((float) clientWidth)) - C1O10Cl0382.C11ll3) / (C1O10Cl0382.C11013l3 + f);
        this.I8C3388l1301 = false;
        C01O0C(i3, f2, (int) (((float) i2) * f2));
        if (this.I8C3388l1301) {
            return true;
        }
        throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }

    private void C18Cl1C() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < getChildCount()) {
                if (!((II083CII) getChildAt(i2).getLayoutParams()).C01O0C) {
                    removeViewAt(i2);
                    i2--;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private ICOI8lC3 C1O10Cl038() {
        int i;
        ICOI8lC3 iCOI8lC3;
        int clientWidth = getClientWidth();
        float scrollX = clientWidth > 0 ? ((float) getScrollX()) / ((float) clientWidth) : 0.0f;
        float f = clientWidth > 0 ? ((float) this.C831O13C118) / ((float) clientWidth) : 0.0f;
        float f2 = 0.0f;
        float f3 = 0.0f;
        int i2 = -1;
        int i3 = 0;
        boolean z = true;
        ICOI8lC3 iCOI8lC32 = null;
        while (i3 < this.C11ll3.size()) {
            ICOI8lC3 iCOI8lC33 = (ICOI8lC3) this.C11ll3.get(i3);
            if (z || iCOI8lC33.C0I1O3C3lI8 == i2 + 1) {
                ICOI8lC3 iCOI8lC34 = iCOI8lC33;
                i = i3;
                iCOI8lC3 = iCOI8lC34;
            } else {
                ICOI8lC3 iCOI8lC35 = this.C18Cl1C;
                iCOI8lC35.C11ll3 = f2 + f3 + f;
                iCOI8lC35.C0I1O3C3lI8 = i2 + 1;
                iCOI8lC35.C11013l3 = this.C1O10Cl038.C01O0C(iCOI8lC35.C0I1O3C3lI8);
                ICOI8lC3 iCOI8lC36 = iCOI8lC35;
                i = i3 - 1;
                iCOI8lC3 = iCOI8lC36;
            }
            float f4 = iCOI8lC3.C11ll3;
            float f5 = iCOI8lC3.C11013l3 + f4 + f;
            if (!z && scrollX < f4) {
                return iCOI8lC32;
            }
            if (scrollX < f5 || i == this.C11ll3.size() - 1) {
                return iCOI8lC3;
            }
            f3 = f4;
            i2 = iCOI8lC3.C0I1O3C3lI8;
            z = false;
            f2 = iCOI8lC3.C11013l3;
            iCOI8lC32 = iCOI8lC3;
            i3 = i + 1;
        }
        return iCOI8lC32;
    }

    private void C1OC33O0lO81() {
        this.CO081lO0OC0 = false;
        this.CO1830lI8C03 = false;
        if (this.I0CIIIC != null) {
            this.I0CIIIC.recycle();
            this.I0CIIIC = null;
        }
    }

    private void C1l00I1() {
        if (this.II3OlOI8II0C != 0) {
            if (this.IICICOC38 == null) {
                this.IICICOC38 = new ArrayList();
            } else {
                this.IICICOC38.clear();
            }
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                this.IICICOC38.add(getChildAt(i));
            }
            Collections.sort(this.IICICOC38, IIO3O0CI);
        }
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    private void setScrollState(int i) {
        if (this.IIOII113C033 != i) {
            this.IIOII113C033 = i;
            if (this.II0ll1CC13l != null) {
                C0I1O3C3lI8(i != 0);
            }
            if (this.ICI3C3O != null) {
                this.ICI3C3O.C0I1O3C3lI8(i);
            }
        }
    }

    private void setScrollingCacheEnabled(boolean z) {
        if (this.Cl80C0l838l != z) {
            this.Cl80C0l838l = z;
        }
    }

    /* access modifiers changed from: package-private */
    public float C01O0C(float f) {
        return (float) Math.sin((double) ((float) (((double) (f - 0.5f)) * 0.4712389167638204d)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.Cl80C0l838l.C01O0C(android.view.ViewGroup, int):java.lang.Object
     arg types: [android.support.v4.view.ViewPager, int]
     candidates:
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.View, int):java.lang.Object
      android.support.v4.view.Cl80C0l838l.C01O0C(android.os.Parcelable, java.lang.ClassLoader):void
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.View, java.lang.Object):boolean
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.ViewGroup, int):java.lang.Object */
    /* access modifiers changed from: package-private */
    public ICOI8lC3 C01O0C(int i, int i2) {
        ICOI8lC3 iCOI8lC3 = new ICOI8lC3();
        iCOI8lC3.C0I1O3C3lI8 = i;
        iCOI8lC3.C01O0C = this.C1O10Cl038.C01O0C((ViewGroup) this, i);
        iCOI8lC3.C11013l3 = this.C1O10Cl038.C01O0C(i);
        if (i2 < 0 || i2 >= this.C11ll3.size()) {
            this.C11ll3.add(iCOI8lC3);
        } else {
            this.C11ll3.add(i2, iCOI8lC3);
        }
        return iCOI8lC3;
    }

    /* access modifiers changed from: package-private */
    public ICOI8lC3 C01O0C(View view) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.C11ll3.size()) {
                return null;
            }
            ICOI8lC3 iCOI8lC3 = (ICOI8lC3) this.C11ll3.get(i2);
            if (this.C1O10Cl038.C01O0C(view, iCOI8lC3.C01O0C)) {
                return iCOI8lC3;
            }
            i = i2 + 1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.Cl80C0l838l.C01O0C(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.View, int, java.lang.Object):void
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.C01O0C(android.support.v4.view.ICOI8lC3, int, android.support.v4.view.ICOI8lC3):void
      android.support.v4.view.ViewPager.C01O0C(int, float, int):void
      android.support.v4.view.ViewPager.C01O0C(int, int, int):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public void C01O0C() {
        int i;
        boolean z;
        int i2;
        boolean z2;
        int C01O0C2 = this.C1O10Cl038.C01O0C();
        this.C0I1O3C3lI8 = C01O0C2;
        boolean z3 = this.C11ll3.size() < (this.ClO80C3lOO8 * 2) + 1 && this.C11ll3.size() < C01O0C2;
        boolean z4 = false;
        int i3 = this.C1OC33O0lO81;
        boolean z5 = z3;
        int i4 = 0;
        while (i4 < this.C11ll3.size()) {
            ICOI8lC3 iCOI8lC3 = (ICOI8lC3) this.C11ll3.get(i4);
            int C01O0C3 = this.C1O10Cl038.C01O0C(iCOI8lC3.C01O0C);
            if (C01O0C3 == -1) {
                i = i4;
                z = z4;
                i2 = i3;
                z2 = z5;
            } else if (C01O0C3 == -2) {
                this.C11ll3.remove(i4);
                int i5 = i4 - 1;
                if (!z4) {
                    this.C1O10Cl038.C01O0C((ViewGroup) this);
                    z4 = true;
                }
                this.C1O10Cl038.C01O0C((ViewGroup) this, iCOI8lC3.C0I1O3C3lI8, iCOI8lC3.C01O0C);
                if (this.C1OC33O0lO81 == iCOI8lC3.C0I1O3C3lI8) {
                    i = i5;
                    z = z4;
                    i2 = Math.max(0, Math.min(this.C1OC33O0lO81, C01O0C2 - 1));
                    z2 = true;
                } else {
                    i = i5;
                    z = z4;
                    i2 = i3;
                    z2 = true;
                }
            } else if (iCOI8lC3.C0I1O3C3lI8 != C01O0C3) {
                if (iCOI8lC3.C0I1O3C3lI8 == this.C1OC33O0lO81) {
                    i3 = C01O0C3;
                }
                iCOI8lC3.C0I1O3C3lI8 = C01O0C3;
                i = i4;
                z = z4;
                i2 = i3;
                z2 = true;
            } else {
                i = i4;
                z = z4;
                i2 = i3;
                z2 = z5;
            }
            z5 = z2;
            i3 = i2;
            z4 = z;
            i4 = i + 1;
        }
        if (z4) {
            this.C1O10Cl038.C0I1O3C3lI8((ViewGroup) this);
        }
        Collections.sort(this.C11ll3, C101lC8O);
        if (z5) {
            int childCount = getChildCount();
            for (int i6 = 0; i6 < childCount; i6++) {
                II083CII ii083cii = (II083CII) getChildAt(i6).getLayoutParams();
                if (!ii083cii.C01O0C) {
                    ii083cii.C101lC8O = 0.0f;
                }
            }
            C01O0C(i3, false, true);
            requestLayout();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.Cl80C0l838l.C0I1O3C3lI8(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.Cl80C0l838l.C0I1O3C3lI8(android.view.View, int, java.lang.Object):void
      android.support.v4.view.Cl80C0l838l.C0I1O3C3lI8(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.Cl80C0l838l.C01O0C(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.View, int, java.lang.Object):void
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.ViewGroup, int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ff, code lost:
        if (r2.C0I1O3C3lI8 == r0.C1OC33O0lO81) goto L_0x0101;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void C01O0C(int r19) {
        /*
            r18 = this;
            r3 = 0
            r2 = 2
            r0 = r18
            int r4 = r0.C1OC33O0lO81
            r0 = r19
            if (r4 == r0) goto L_0x033f
            r0 = r18
            int r2 = r0.C1OC33O0lO81
            r0 = r19
            if (r2 >= r0) goto L_0x0030
            r2 = 66
        L_0x0014:
            r0 = r18
            int r3 = r0.C1OC33O0lO81
            r0 = r18
            android.support.v4.view.ICOI8lC3 r3 = r0.C0I1O3C3lI8(r3)
            r0 = r19
            r1 = r18
            r1.C1OC33O0lO81 = r0
            r4 = r3
            r3 = r2
        L_0x0026:
            r0 = r18
            android.support.v4.view.Cl80C0l838l r2 = r0.C1O10Cl038
            if (r2 != 0) goto L_0x0033
            r18.C1l00I1()
        L_0x002f:
            return
        L_0x0030:
            r2 = 17
            goto L_0x0014
        L_0x0033:
            r0 = r18
            boolean r2 = r0.ClC13lIl
            if (r2 == 0) goto L_0x003d
            r18.C1l00I1()
            goto L_0x002f
        L_0x003d:
            android.os.IBinder r2 = r18.getWindowToken()
            if (r2 == 0) goto L_0x002f
            r0 = r18
            android.support.v4.view.Cl80C0l838l r2 = r0.C1O10Cl038
            r0 = r18
            r2.C01O0C(r0)
            r0 = r18
            int r2 = r0.ClO80C3lOO8
            r5 = 0
            r0 = r18
            int r6 = r0.C1OC33O0lO81
            int r6 = r6 - r2
            int r11 = java.lang.Math.max(r5, r6)
            r0 = r18
            android.support.v4.view.Cl80C0l838l r5 = r0.C1O10Cl038
            int r12 = r5.C01O0C()
            int r5 = r12 + -1
            r0 = r18
            int r6 = r0.C1OC33O0lO81
            int r2 = r2 + r6
            int r13 = java.lang.Math.min(r5, r2)
            r0 = r18
            int r2 = r0.C0I1O3C3lI8
            if (r12 == r2) goto L_0x00da
            android.content.res.Resources r2 = r18.getResources()     // Catch:{ NotFoundException -> 0x00d0 }
            int r3 = r18.getId()     // Catch:{ NotFoundException -> 0x00d0 }
            java.lang.String r2 = r2.getResourceName(r3)     // Catch:{ NotFoundException -> 0x00d0 }
        L_0x007f:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r18
            int r5 = r0.C0I1O3C3lI8
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ", found: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r12)
            java.lang.String r5 = " Pager id: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " Pager class: "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.Class r4 = r18.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = " Problematic adapter: "
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r18
            android.support.v4.view.Cl80C0l838l r4 = r0.C1O10Cl038
            java.lang.Class r4 = r4.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r3.<init>(r2)
            throw r3
        L_0x00d0:
            r2 = move-exception
            int r2 = r18.getId()
            java.lang.String r2 = java.lang.Integer.toHexString(r2)
            goto L_0x007f
        L_0x00da:
            r6 = 0
            r2 = 0
            r5 = r2
        L_0x00dd:
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            int r2 = r2.size()
            if (r5 >= r2) goto L_0x033c
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.ICOI8lC3 r2 = (android.support.v4.view.ICOI8lC3) r2
            int r7 = r2.C0I1O3C3lI8
            r0 = r18
            int r8 = r0.C1OC33O0lO81
            if (r7 < r8) goto L_0x01cf
            int r7 = r2.C0I1O3C3lI8
            r0 = r18
            int r8 = r0.C1OC33O0lO81
            if (r7 != r8) goto L_0x033c
        L_0x0101:
            if (r2 != 0) goto L_0x0339
            if (r12 <= 0) goto L_0x0339
            r0 = r18
            int r2 = r0.C1OC33O0lO81
            r0 = r18
            android.support.v4.view.ICOI8lC3 r2 = r0.C01O0C(r2, r5)
            r10 = r2
        L_0x0110:
            if (r10 == 0) goto L_0x0180
            r9 = 0
            int r8 = r5 + -1
            if (r8 < 0) goto L_0x01d4
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            java.lang.Object r2 = r2.get(r8)
            android.support.v4.view.ICOI8lC3 r2 = (android.support.v4.view.ICOI8lC3) r2
        L_0x0121:
            int r14 = r18.getClientWidth()
            if (r14 > 0) goto L_0x01d7
            r6 = 0
        L_0x0128:
            r0 = r18
            int r7 = r0.C1OC33O0lO81
            int r7 = r7 + -1
            r16 = r7
            r7 = r9
            r9 = r16
            r17 = r8
            r8 = r5
            r5 = r17
        L_0x0138:
            if (r9 < 0) goto L_0x0142
            int r15 = (r7 > r6 ? 1 : (r7 == r6 ? 0 : -1))
            if (r15 < 0) goto L_0x0216
            if (r9 >= r11) goto L_0x0216
            if (r2 != 0) goto L_0x01e6
        L_0x0142:
            float r6 = r10.C11013l3
            int r9 = r8 + 1
            r2 = 1073741824(0x40000000, float:2.0)
            int r2 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x017b
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            int r2 = r2.size()
            if (r9 >= r2) goto L_0x024c
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            java.lang.Object r2 = r2.get(r9)
            android.support.v4.view.ICOI8lC3 r2 = (android.support.v4.view.ICOI8lC3) r2
            r7 = r2
        L_0x0161:
            if (r14 > 0) goto L_0x024f
            r2 = 0
            r5 = r2
        L_0x0165:
            r0 = r18
            int r2 = r0.C1OC33O0lO81
            int r2 = r2 + 1
            r16 = r2
            r2 = r7
            r7 = r9
            r9 = r16
        L_0x0171:
            if (r9 >= r12) goto L_0x017b
            int r11 = (r6 > r5 ? 1 : (r6 == r5 ? 0 : -1))
            if (r11 < 0) goto L_0x029a
            if (r9 <= r13) goto L_0x029a
            if (r2 != 0) goto L_0x025c
        L_0x017b:
            r0 = r18
            r0.C01O0C(r10, r8, r4)
        L_0x0180:
            r0 = r18
            android.support.v4.view.Cl80C0l838l r4 = r0.C1O10Cl038
            r0 = r18
            int r5 = r0.C1OC33O0lO81
            if (r10 == 0) goto L_0x02e8
            java.lang.Object r2 = r10.C01O0C
        L_0x018c:
            r0 = r18
            r4.C0I1O3C3lI8(r0, r5, r2)
            r0 = r18
            android.support.v4.view.Cl80C0l838l r2 = r0.C1O10Cl038
            r0 = r18
            r2.C0I1O3C3lI8(r0)
            int r5 = r18.getChildCount()
            r2 = 0
            r4 = r2
        L_0x01a0:
            if (r4 >= r5) goto L_0x02eb
            r0 = r18
            android.view.View r6 = r0.getChildAt(r4)
            android.view.ViewGroup$LayoutParams r2 = r6.getLayoutParams()
            android.support.v4.view.II083CII r2 = (android.support.v4.view.II083CII) r2
            r2.C18Cl1C = r4
            boolean r7 = r2.C01O0C
            if (r7 != 0) goto L_0x01cb
            float r7 = r2.C101lC8O
            r8 = 0
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x01cb
            r0 = r18
            android.support.v4.view.ICOI8lC3 r6 = r0.C01O0C(r6)
            if (r6 == 0) goto L_0x01cb
            float r7 = r6.C11013l3
            r2.C101lC8O = r7
            int r6 = r6.C0I1O3C3lI8
            r2.C11ll3 = r6
        L_0x01cb:
            int r2 = r4 + 1
            r4 = r2
            goto L_0x01a0
        L_0x01cf:
            int r2 = r5 + 1
            r5 = r2
            goto L_0x00dd
        L_0x01d4:
            r2 = 0
            goto L_0x0121
        L_0x01d7:
            r6 = 1073741824(0x40000000, float:2.0)
            float r7 = r10.C11013l3
            float r6 = r6 - r7
            int r7 = r18.getPaddingLeft()
            float r7 = (float) r7
            float r15 = (float) r14
            float r7 = r7 / r15
            float r6 = r6 + r7
            goto L_0x0128
        L_0x01e6:
            int r15 = r2.C0I1O3C3lI8
            if (r9 != r15) goto L_0x0210
            boolean r15 = r2.C101lC8O
            if (r15 != 0) goto L_0x0210
            r0 = r18
            java.util.ArrayList r15 = r0.C11ll3
            r15.remove(r5)
            r0 = r18
            android.support.v4.view.Cl80C0l838l r15 = r0.C1O10Cl038
            java.lang.Object r2 = r2.C01O0C
            r0 = r18
            r15.C01O0C(r0, r9, r2)
            int r5 = r5 + -1
            int r8 = r8 + -1
            if (r5 < 0) goto L_0x0214
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.ICOI8lC3 r2 = (android.support.v4.view.ICOI8lC3) r2
        L_0x0210:
            int r9 = r9 + -1
            goto L_0x0138
        L_0x0214:
            r2 = 0
            goto L_0x0210
        L_0x0216:
            if (r2 == 0) goto L_0x0230
            int r15 = r2.C0I1O3C3lI8
            if (r9 != r15) goto L_0x0230
            float r2 = r2.C11013l3
            float r7 = r7 + r2
            int r5 = r5 + -1
            if (r5 < 0) goto L_0x022e
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.ICOI8lC3 r2 = (android.support.v4.view.ICOI8lC3) r2
            goto L_0x0210
        L_0x022e:
            r2 = 0
            goto L_0x0210
        L_0x0230:
            int r2 = r5 + 1
            r0 = r18
            android.support.v4.view.ICOI8lC3 r2 = r0.C01O0C(r9, r2)
            float r2 = r2.C11013l3
            float r7 = r7 + r2
            int r8 = r8 + 1
            if (r5 < 0) goto L_0x024a
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.ICOI8lC3 r2 = (android.support.v4.view.ICOI8lC3) r2
            goto L_0x0210
        L_0x024a:
            r2 = 0
            goto L_0x0210
        L_0x024c:
            r7 = 0
            goto L_0x0161
        L_0x024f:
            int r2 = r18.getPaddingRight()
            float r2 = (float) r2
            float r5 = (float) r14
            float r2 = r2 / r5
            r5 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 + r5
            r5 = r2
            goto L_0x0165
        L_0x025c:
            int r11 = r2.C0I1O3C3lI8
            if (r9 != r11) goto L_0x0332
            boolean r11 = r2.C101lC8O
            if (r11 != 0) goto L_0x0332
            r0 = r18
            java.util.ArrayList r11 = r0.C11ll3
            r11.remove(r7)
            r0 = r18
            android.support.v4.view.Cl80C0l838l r11 = r0.C1O10Cl038
            java.lang.Object r2 = r2.C01O0C
            r0 = r18
            r11.C01O0C(r0, r9, r2)
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x0298
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.ICOI8lC3 r2 = (android.support.v4.view.ICOI8lC3) r2
        L_0x028a:
            r16 = r6
            r6 = r2
            r2 = r16
        L_0x028f:
            int r9 = r9 + 1
            r16 = r2
            r2 = r6
            r6 = r16
            goto L_0x0171
        L_0x0298:
            r2 = 0
            goto L_0x028a
        L_0x029a:
            if (r2 == 0) goto L_0x02c1
            int r11 = r2.C0I1O3C3lI8
            if (r9 != r11) goto L_0x02c1
            float r2 = r2.C11013l3
            float r6 = r6 + r2
            int r7 = r7 + 1
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x02bf
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.ICOI8lC3 r2 = (android.support.v4.view.ICOI8lC3) r2
        L_0x02b9:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x028f
        L_0x02bf:
            r2 = 0
            goto L_0x02b9
        L_0x02c1:
            r0 = r18
            android.support.v4.view.ICOI8lC3 r2 = r0.C01O0C(r9, r7)
            int r7 = r7 + 1
            float r2 = r2.C11013l3
            float r6 = r6 + r2
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x02e6
            r0 = r18
            java.util.ArrayList r2 = r0.C11ll3
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.ICOI8lC3 r2 = (android.support.v4.view.ICOI8lC3) r2
        L_0x02e0:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x028f
        L_0x02e6:
            r2 = 0
            goto L_0x02e0
        L_0x02e8:
            r2 = 0
            goto L_0x018c
        L_0x02eb:
            r18.C1l00I1()
            boolean r2 = r18.hasFocus()
            if (r2 == 0) goto L_0x002f
            android.view.View r2 = r18.findFocus()
            if (r2 == 0) goto L_0x0330
            r0 = r18
            android.support.v4.view.ICOI8lC3 r2 = r0.C0I1O3C3lI8(r2)
        L_0x0300:
            if (r2 == 0) goto L_0x030a
            int r2 = r2.C0I1O3C3lI8
            r0 = r18
            int r4 = r0.C1OC33O0lO81
            if (r2 == r4) goto L_0x002f
        L_0x030a:
            r2 = 0
        L_0x030b:
            int r4 = r18.getChildCount()
            if (r2 >= r4) goto L_0x002f
            r0 = r18
            android.view.View r4 = r0.getChildAt(r2)
            r0 = r18
            android.support.v4.view.ICOI8lC3 r5 = r0.C01O0C(r4)
            if (r5 == 0) goto L_0x032d
            int r5 = r5.C0I1O3C3lI8
            r0 = r18
            int r6 = r0.C1OC33O0lO81
            if (r5 != r6) goto L_0x032d
            boolean r4 = r4.requestFocus(r3)
            if (r4 != 0) goto L_0x002f
        L_0x032d:
            int r2 = r2 + 1
            goto L_0x030b
        L_0x0330:
            r2 = 0
            goto L_0x0300
        L_0x0332:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x028f
        L_0x0339:
            r10 = r2
            goto L_0x0110
        L_0x033c:
            r2 = r6
            goto L_0x0101
        L_0x033f:
            r4 = r3
            r3 = r2
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.C01O0C(int):void");
    }

    /* access modifiers changed from: protected */
    public void C01O0C(int i, float f, int i2) {
        int measuredWidth;
        int i3;
        int i4;
        if (this.IC11OO80I3 > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            int i5 = 0;
            while (i5 < childCount) {
                View childAt = getChildAt(i5);
                II083CII ii083cii = (II083CII) childAt.getLayoutParams();
                if (!ii083cii.C01O0C) {
                    int i6 = paddingRight;
                    i3 = paddingLeft;
                    i4 = i6;
                } else {
                    switch (ii083cii.C0I1O3C3lI8 & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            int i7 = paddingRight;
                            i3 = paddingLeft;
                            i4 = i7;
                            break;
                        case 2:
                        case 4:
                        default:
                            measuredWidth = paddingLeft;
                            int i8 = paddingRight;
                            i3 = paddingLeft;
                            i4 = i8;
                            break;
                        case 3:
                            int width2 = childAt.getWidth() + paddingLeft;
                            int i9 = paddingLeft;
                            i4 = paddingRight;
                            i3 = width2;
                            measuredWidth = i9;
                            break;
                        case 5:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            int measuredWidth2 = paddingRight + childAt.getMeasuredWidth();
                            i3 = paddingLeft;
                            i4 = measuredWidth2;
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                }
                i5++;
                int i10 = i4;
                paddingLeft = i3;
                paddingRight = i10;
            }
        }
        if (this.ICI3C3O != null) {
            this.ICI3C3O.C01O0C(i, f, i2);
        }
        if (this.ICOI8lC3 != null) {
            this.ICOI8lC3.C01O0C(i, f, i2);
        }
        if (this.II0ll1CC13l != null) {
            int scrollX2 = getScrollX();
            int childCount2 = getChildCount();
            for (int i11 = 0; i11 < childCount2; i11++) {
                View childAt2 = getChildAt(i11);
                if (!((II083CII) childAt2.getLayoutParams()).C01O0C) {
                    this.II0ll1CC13l.C01O0C(childAt2, ((float) (childAt2.getLeft() - scrollX2)) / ((float) getClientWidth()));
                }
            }
        }
        this.I8C3388l1301 = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void C01O0C(int i, int i2, int i3) {
        int abs;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int i4 = i - scrollX;
        int i5 = i2 - scrollY;
        if (i4 == 0 && i5 == 0) {
            C01O0C(false);
            C0I1O3C3lI8();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int clientWidth = getClientWidth();
        int i6 = clientWidth / 2;
        float C01O0C2 = (((float) i6) * C01O0C(Math.min(1.0f, (((float) Math.abs(i4)) * 1.0f) / ((float) clientWidth)))) + ((float) i6);
        int abs2 = Math.abs(i3);
        if (abs2 > 0) {
            abs = Math.round(1000.0f * Math.abs(C01O0C2 / ((float) abs2))) * 4;
        } else {
            abs = (int) (((((float) Math.abs(i4)) / ((((float) clientWidth) * this.C1O10Cl038.C01O0C(this.C1OC33O0lO81)) + ((float) this.C831O13C118))) + 1.0f) * 100.0f);
        }
        this.C3l3O8lIOIO8.startScroll(scrollX, scrollY, i4, i5, Math.min(abs, 600));
        CO88CO1Cl383.C0I1O3C3lI8(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.C01O0C(android.support.v4.view.ICOI8lC3, int, android.support.v4.view.ICOI8lC3):void
      android.support.v4.view.ViewPager.C01O0C(int, float, int):void
      android.support.v4.view.ViewPager.C01O0C(int, int, int):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean):void */
    public void C01O0C(int i, boolean z) {
        this.ClC13lIl = false;
        C01O0C(i, z, false);
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(int i, boolean z, boolean z2) {
        C01O0C(i, z, z2, 0);
    }

    /* access modifiers changed from: package-private */
    public void C01O0C(int i, boolean z, boolean z2, int i2) {
        boolean z3 = false;
        if (this.C1O10Cl038 == null || this.C1O10Cl038.C01O0C() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z2 || this.C1OC33O0lO81 != i || this.C11ll3.size() == 0) {
            if (i < 0) {
                i = 0;
            } else if (i >= this.C1O10Cl038.C01O0C()) {
                i = this.C1O10Cl038.C01O0C() - 1;
            }
            int i3 = this.ClO80C3lOO8;
            if (i > this.C1OC33O0lO81 + i3 || i < this.C1OC33O0lO81 - i3) {
                for (int i4 = 0; i4 < this.C11ll3.size(); i4++) {
                    ((ICOI8lC3) this.C11ll3.get(i4)).C101lC8O = true;
                }
            }
            if (this.C1OC33O0lO81 != i) {
                z3 = true;
            }
            if (this.I80183lOl) {
                this.C1OC33O0lO81 = i;
                if (z3 && this.ICI3C3O != null) {
                    this.ICI3C3O.C01O0C(i);
                }
                if (z3 && this.ICOI8lC3 != null) {
                    this.ICOI8lC3.C01O0C(i);
                }
                requestLayout();
                return;
            }
            C01O0C(i);
            C01O0C(i, z, i2, z3);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    public boolean C01O0C(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (keyEvent.getKeyCode()) {
            case 21:
                return C101lC8O(17);
            case 22:
                return C101lC8O(66);
            case 61:
                if (Build.VERSION.SDK_INT < 11) {
                    return false;
                }
                if (C3ICl0OOl.C01O0C(keyEvent)) {
                    return C101lC8O(2);
                }
                if (C3ICl0OOl.C01O0C(keyEvent, 1)) {
                    return C101lC8O(1);
                }
                return false;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean C01O0C(View view, boolean z, int i, int i2, int i3) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i2 + scrollX >= childAt.getLeft() && i2 + scrollX < childAt.getRight() && i3 + scrollY >= childAt.getTop() && i3 + scrollY < childAt.getBottom()) {
                    if (C01O0C(childAt, true, i, (i2 + scrollX) - childAt.getLeft(), (i3 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        return z && CO88CO1Cl383.C01O0C(view, -i);
    }

    /* access modifiers changed from: package-private */
    public ICOI8lC3 C0I1O3C3lI8(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.C11ll3.size()) {
                return null;
            }
            ICOI8lC3 iCOI8lC3 = (ICOI8lC3) this.C11ll3.get(i3);
            if (iCOI8lC3.C0I1O3C3lI8 == i) {
                return iCOI8lC3;
            }
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public ICOI8lC3 C0I1O3C3lI8(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return C01O0C(view);
            }
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            view = (View) parent;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void C0I1O3C3lI8() {
        C01O0C(this.C1OC33O0lO81);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.C01O0C(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.C01O0C(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.C01O0C(float, float):boolean
      android.support.v4.view.ViewPager.C01O0C(int, int):android.support.v4.view.ICOI8lC3
      android.support.v4.view.ViewPager.C01O0C(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean C101lC8O() {
        if (this.C1OC33O0lO81 <= 0) {
            return false;
        }
        C01O0C(this.C1OC33O0lO81 - 1, true);
        return true;
    }

    public boolean C101lC8O(int i) {
        View view;
        boolean z;
        boolean z2;
        View findFocus = findFocus();
        if (findFocus == this) {
            view = null;
        } else {
            if (findFocus != null) {
                ViewParent parent = findFocus.getParent();
                while (true) {
                    if (!(parent instanceof ViewGroup)) {
                        z = false;
                        break;
                    } else if (parent == this) {
                        z = true;
                        break;
                    } else {
                        parent = parent.getParent();
                    }
                }
                if (!z) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(findFocus.getClass().getSimpleName());
                    for (ViewParent parent2 = findFocus.getParent(); parent2 instanceof ViewGroup; parent2 = parent2.getParent()) {
                        sb.append(" => ").append(parent2.getClass().getSimpleName());
                    }
                    Log.e("ViewPager", "arrowScroll tried to find focus based on non-child current focused view " + sb.toString());
                    view = null;
                }
            }
            view = findFocus;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, view, i);
        if (findNextFocus == null || findNextFocus == view) {
            if (i == 17 || i == 1) {
                z2 = C101lC8O();
            } else {
                if (i == 66 || i == 2) {
                    z2 = C11013l3();
                }
                z2 = false;
            }
        } else if (i == 17) {
            z2 = (view == null || C01O0C(this.C1l00I1, findNextFocus).left < C01O0C(this.C1l00I1, view).left) ? findNextFocus.requestFocus() : C101lC8O();
        } else {
            if (i == 66) {
                z2 = (view == null || C01O0C(this.C1l00I1, findNextFocus).left > C01O0C(this.C1l00I1, view).left) ? findNextFocus.requestFocus() : C11013l3();
            }
            z2 = false;
        }
        if (z2) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i));
        }
        return z2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.C01O0C(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.C01O0C(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.C01O0C(float, float):boolean
      android.support.v4.view.ViewPager.C01O0C(int, int):android.support.v4.view.ICOI8lC3
      android.support.v4.view.ViewPager.C01O0C(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean C11013l3() {
        if (this.C1O10Cl038 == null || this.C1OC33O0lO81 >= this.C1O10Cl038.C01O0C() - 1) {
            return false;
        }
        C01O0C(this.C1OC33O0lO81 + 1, true);
        return true;
    }

    public void addFocusables(ArrayList arrayList, int i, int i2) {
        ICOI8lC3 C01O0C2;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                View childAt = getChildAt(i3);
                if (childAt.getVisibility() == 0 && (C01O0C2 = C01O0C(childAt)) != null && C01O0C2.C0I1O3C3lI8 == this.C1OC33O0lO81) {
                    childAt.addFocusables(arrayList, i, i2);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i2 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList arrayList) {
        ICOI8lC3 C01O0C2;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (C01O0C2 = C01O0C(childAt)) != null && C01O0C2.C0I1O3C3lI8 == this.C1OC33O0lO81) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams generateLayoutParams = !checkLayoutParams(layoutParams) ? generateLayoutParams(layoutParams) : layoutParams;
        II083CII ii083cii = (II083CII) generateLayoutParams;
        ii083cii.C01O0C |= view instanceof ICI3C3O;
        if (!this.CIOC8C) {
            super.addView(view, i, generateLayoutParams);
        } else if (ii083cii == null || !ii083cii.C01O0C) {
            ii083cii.C11013l3 = true;
            addViewInLayout(view, i, generateLayoutParams);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    public boolean canScrollHorizontally(int i) {
        boolean z = true;
        if (this.C1O10Cl038 == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        if (i < 0) {
            if (scrollX <= ((int) (((float) clientWidth) * this.CCC3CC0l))) {
                z = false;
            }
            return z;
        } else if (i <= 0) {
            return false;
        } else {
            if (scrollX >= ((int) (((float) clientWidth) * this.CI0I8l333131))) {
                z = false;
            }
            return z;
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof II083CII) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        if (this.C3l3O8lIOIO8.isFinished() || !this.C3l3O8lIOIO8.computeScrollOffset()) {
            C01O0C(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.C3l3O8lIOIO8.getCurrX();
        int currY = this.C3l3O8lIOIO8.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!C11013l3(currX)) {
                this.C3l3O8lIOIO8.abortAnimation();
                scrollTo(0, currY);
            }
        }
        CO88CO1Cl383.C0I1O3C3lI8(this);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || C01O0C(keyEvent);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        ICOI8lC3 C01O0C2;
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (C01O0C2 = C01O0C(childAt)) != null && C01O0C2.C0I1O3C3lI8 == this.C1OC33O0lO81 && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z = false;
        int C01O0C2 = CO88CO1Cl383.C01O0C(this);
        if (C01O0C2 == 0 || (C01O0C2 == 1 && this.C1O10Cl038 != null && this.C1O10Cl038.C01O0C() > 1)) {
            if (!this.I30OCIOO.C01O0C()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.CCC3CC0l * ((float) width));
                this.I30OCIOO.C01O0C(height, width);
                z = false | this.I30OCIOO.C01O0C(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.I3ClO1C31.C01O0C()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.CI0I8l333131 + 1.0f)) * ((float) width2));
                this.I3ClO1C31.C01O0C(height2, width2);
                z |= this.I3ClO1C31.C01O0C(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.I30OCIOO.C0I1O3C3lI8();
            this.I3ClO1C31.C0I1O3C3lI8();
        }
        if (z) {
            CO88CO1Cl383.C0I1O3C3lI8(this);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.C8CI00;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new II083CII();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new II083CII(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    public Cl80C0l838l getAdapter() {
        return this.C1O10Cl038;
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i, int i2) {
        if (this.II3OlOI8II0C == 2) {
            i2 = (i - 1) - i2;
        }
        return ((II083CII) ((View) this.IICICOC38.get(i2)).getLayoutParams()).C18Cl1C;
    }

    public int getCurrentItem() {
        return this.C1OC33O0lO81;
    }

    public int getOffscreenPageLimit() {
        return this.ClO80C3lOO8;
    }

    public int getPageMargin() {
        return this.C831O13C118;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.I80183lOl = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.IIOC18I8);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f;
        super.onDraw(canvas);
        if (this.C831O13C118 > 0 && this.C8CI00 != null && this.C11ll3.size() > 0 && this.C1O10Cl038 != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f2 = ((float) this.C831O13C118) / ((float) width);
            ICOI8lC3 iCOI8lC3 = (ICOI8lC3) this.C11ll3.get(0);
            float f3 = iCOI8lC3.C11ll3;
            int size = this.C11ll3.size();
            int i = iCOI8lC3.C0I1O3C3lI8;
            int i2 = ((ICOI8lC3) this.C11ll3.get(size - 1)).C0I1O3C3lI8;
            int i3 = 0;
            int i4 = i;
            while (i4 < i2) {
                while (i4 > iCOI8lC3.C0I1O3C3lI8 && i3 < size) {
                    i3++;
                    iCOI8lC3 = (ICOI8lC3) this.C11ll3.get(i3);
                }
                if (i4 == iCOI8lC3.C0I1O3C3lI8) {
                    f = (iCOI8lC3.C11ll3 + iCOI8lC3.C11013l3) * ((float) width);
                    f3 = iCOI8lC3.C11ll3 + iCOI8lC3.C11013l3 + f2;
                } else {
                    float C01O0C2 = this.C1O10Cl038.C01O0C(i4);
                    f = (f3 + C01O0C2) * ((float) width);
                    f3 += C01O0C2 + f2;
                }
                if (((float) this.C831O13C118) + f > ((float) scrollX)) {
                    this.C8CI00.setBounds((int) f, this.CC38COI1l3I, (int) (((float) this.C831O13C118) + f + 0.5f), this.CC8IOI1II0);
                    this.C8CI00.draw(canvas);
                }
                if (f <= ((float) (scrollX + width))) {
                    i4++;
                } else {
                    return;
                }
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            this.CO081lO0OC0 = false;
            this.CO1830lI8C03 = false;
            this.I08O3C = -1;
            if (this.I0CIIIC == null) {
                return false;
            }
            this.I0CIIIC.recycle();
            this.I0CIIIC = null;
            return false;
        }
        if (action != 0) {
            if (this.CO081lO0OC0) {
                return true;
            }
            if (this.CO1830lI8C03) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x = motionEvent.getX();
                this.I03lII1 = x;
                this.I003OlCCOlC = x;
                float y = motionEvent.getY();
                this.I088l3088 = y;
                this.I008018O = y;
                this.I08O3C = CCC3CC0l.C0I1O3C3lI8(motionEvent, 0);
                this.CO1830lI8C03 = false;
                this.C3l3O8lIOIO8.computeScrollOffset();
                if (this.IIOII113C033 == 2 && Math.abs(this.C3l3O8lIOIO8.getFinalX() - this.C3l3O8lIOIO8.getCurrX()) > this.I1CO03) {
                    this.C3l3O8lIOIO8.abortAnimation();
                    this.ClC13lIl = false;
                    C0I1O3C3lI8();
                    this.CO081lO0OC0 = true;
                    C101lC8O(true);
                    setScrollState(1);
                    break;
                } else {
                    C01O0C(false);
                    this.CO081lO0OC0 = false;
                    break;
                }
                break;
            case 2:
                int i = this.I08O3C;
                if (i != -1) {
                    int C01O0C2 = CCC3CC0l.C01O0C(motionEvent, i);
                    float C101lC8O2 = CCC3CC0l.C101lC8O(motionEvent, C01O0C2);
                    float f = C101lC8O2 - this.I003OlCCOlC;
                    float abs = Math.abs(f);
                    float C11013l32 = CCC3CC0l.C11013l3(motionEvent, C01O0C2);
                    float abs2 = Math.abs(C11013l32 - this.I088l3088);
                    if (f == 0.0f || C01O0C(this.I003OlCCOlC, f) || !C01O0C(this, false, (int) f, (int) C101lC8O2, (int) C11013l32)) {
                        if (abs > ((float) this.I003I0) && 0.5f * abs > abs2) {
                            this.CO081lO0OC0 = true;
                            C101lC8O(true);
                            setScrollState(1);
                            this.I003OlCCOlC = f > 0.0f ? this.I03lII1 + ((float) this.I003I0) : this.I03lII1 - ((float) this.I003I0);
                            this.I008018O = C11013l32;
                            setScrollingCacheEnabled(true);
                        } else if (abs2 > ((float) this.I003I0)) {
                            this.CO1830lI8C03 = true;
                        }
                        if (this.CO081lO0OC0 && C0I1O3C3lI8(C101lC8O2)) {
                            CO88CO1Cl383.C0I1O3C3lI8(this);
                            break;
                        }
                    } else {
                        this.I003OlCCOlC = C101lC8O2;
                        this.I008018O = C11013l32;
                        this.CO1830lI8C03 = true;
                        return false;
                    }
                }
                break;
            case 6:
                C01O0C(motionEvent);
                break;
        }
        if (this.I0CIIIC == null) {
            this.I0CIIIC = VelocityTracker.obtain();
        }
        this.I0CIIIC.addMovement(motionEvent);
        return this.CO081lO0OC0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.C01O0C(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.C01O0C(int, float, int, int):int
      android.support.v4.view.ViewPager.C01O0C(int, int, int, int):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, int, boolean):void */
    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        ICOI8lC3 C01O0C2;
        int i5;
        int i6;
        int i7;
        int measuredHeight;
        int i8;
        int i9;
        int childCount = getChildCount();
        int i10 = i3 - i;
        int i11 = i4 - i2;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i12 = 0;
        int i13 = 0;
        while (i13 < childCount) {
            View childAt = getChildAt(i13);
            if (childAt.getVisibility() != 8) {
                II083CII ii083cii = (II083CII) childAt.getLayoutParams();
                if (ii083cii.C01O0C) {
                    int i14 = ii083cii.C0I1O3C3lI8 & 7;
                    int i15 = ii083cii.C0I1O3C3lI8 & 112;
                    switch (i14) {
                        case 1:
                            i7 = Math.max((i10 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 2:
                        case 4:
                        default:
                            i7 = paddingLeft;
                            break;
                        case 3:
                            i7 = paddingLeft;
                            paddingLeft = childAt.getMeasuredWidth() + paddingLeft;
                            break;
                        case 5:
                            int measuredWidth = (i10 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            i7 = measuredWidth;
                            break;
                    }
                    switch (i15) {
                        case 16:
                            measuredHeight = Math.max((i11 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            int i16 = paddingBottom;
                            i8 = paddingTop;
                            i9 = i16;
                            break;
                        case 48:
                            int measuredHeight2 = childAt.getMeasuredHeight() + paddingTop;
                            int i17 = paddingTop;
                            i9 = paddingBottom;
                            i8 = measuredHeight2;
                            measuredHeight = i17;
                            break;
                        case 80:
                            measuredHeight = (i11 - paddingBottom) - childAt.getMeasuredHeight();
                            int measuredHeight3 = paddingBottom + childAt.getMeasuredHeight();
                            i8 = paddingTop;
                            i9 = measuredHeight3;
                            break;
                        default:
                            measuredHeight = paddingTop;
                            int i18 = paddingBottom;
                            i8 = paddingTop;
                            i9 = i18;
                            break;
                    }
                    int i19 = i7 + scrollX;
                    childAt.layout(i19, measuredHeight, childAt.getMeasuredWidth() + i19, childAt.getMeasuredHeight() + measuredHeight);
                    i5 = i12 + 1;
                    i6 = i8;
                    paddingBottom = i9;
                    i13++;
                    paddingLeft = paddingLeft;
                    paddingRight = paddingRight;
                    paddingTop = i6;
                    i12 = i5;
                }
            }
            i5 = i12;
            i6 = paddingTop;
            i13++;
            paddingLeft = paddingLeft;
            paddingRight = paddingRight;
            paddingTop = i6;
            i12 = i5;
        }
        int i20 = (i10 - paddingLeft) - paddingRight;
        for (int i21 = 0; i21 < childCount; i21++) {
            View childAt2 = getChildAt(i21);
            if (childAt2.getVisibility() != 8) {
                II083CII ii083cii2 = (II083CII) childAt2.getLayoutParams();
                if (!ii083cii2.C01O0C && (C01O0C2 = C01O0C(childAt2)) != null) {
                    int i22 = ((int) (C01O0C2.C11ll3 * ((float) i20))) + paddingLeft;
                    if (ii083cii2.C11013l3) {
                        ii083cii2.C11013l3 = false;
                        childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (ii083cii2.C101lC8O * ((float) i20)), 1073741824), View.MeasureSpec.makeMeasureSpec((i11 - paddingTop) - paddingBottom, 1073741824));
                    }
                    childAt2.layout(i22, paddingTop, childAt2.getMeasuredWidth() + i22, childAt2.getMeasuredHeight() + paddingTop);
                }
            }
        }
        this.CC38COI1l3I = paddingTop;
        this.CC8IOI1II0 = i11 - paddingBottom;
        this.IC11OO80I3 = i12;
        if (this.I80183lOl) {
            C01O0C(this.C1OC33O0lO81, false, 0, false);
        }
        this.I80183lOl = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r0 = 0
            int r0 = getDefaultSize(r0, r14)
            r1 = 0
            int r1 = getDefaultSize(r1, r15)
            r13.setMeasuredDimension(r0, r1)
            int r0 = r13.getMeasuredWidth()
            int r1 = r0 / 10
            int r2 = r13.CO30CC1l0313
            int r1 = java.lang.Math.min(r1, r2)
            r13.CO88CO1Cl383 = r1
            int r1 = r13.getPaddingLeft()
            int r0 = r0 - r1
            int r1 = r13.getPaddingRight()
            int r3 = r0 - r1
            int r0 = r13.getMeasuredHeight()
            int r1 = r13.getPaddingTop()
            int r0 = r0 - r1
            int r1 = r13.getPaddingBottom()
            int r5 = r0 - r1
            int r9 = r13.getChildCount()
            r0 = 0
            r8 = r0
        L_0x003b:
            if (r8 >= r9) goto L_0x00bc
            android.view.View r10 = r13.getChildAt(r8)
            int r0 = r10.getVisibility()
            r1 = 8
            if (r0 == r1) goto L_0x00a5
            android.view.ViewGroup$LayoutParams r0 = r10.getLayoutParams()
            android.support.v4.view.II083CII r0 = (android.support.v4.view.II083CII) r0
            if (r0 == 0) goto L_0x00a5
            boolean r1 = r0.C01O0C
            if (r1 == 0) goto L_0x00a5
            int r1 = r0.C0I1O3C3lI8
            r6 = r1 & 7
            int r1 = r0.C0I1O3C3lI8
            r4 = r1 & 112(0x70, float:1.57E-43)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 48
            if (r4 == r7) goto L_0x0069
            r7 = 80
            if (r4 != r7) goto L_0x00a9
        L_0x0069:
            r4 = 1
            r7 = r4
        L_0x006b:
            r4 = 3
            if (r6 == r4) goto L_0x0071
            r4 = 5
            if (r6 != r4) goto L_0x00ac
        L_0x0071:
            r4 = 1
            r6 = r4
        L_0x0073:
            if (r7 == 0) goto L_0x00af
            r2 = 1073741824(0x40000000, float:2.0)
        L_0x0077:
            int r4 = r0.width
            r11 = -2
            if (r4 == r11) goto L_0x010f
            r4 = 1073741824(0x40000000, float:2.0)
            int r2 = r0.width
            r11 = -1
            if (r2 == r11) goto L_0x010c
            int r2 = r0.width
        L_0x0085:
            int r11 = r0.height
            r12 = -2
            if (r11 == r12) goto L_0x010a
            r1 = 1073741824(0x40000000, float:2.0)
            int r11 = r0.height
            r12 = -1
            if (r11 == r12) goto L_0x010a
            int r0 = r0.height
        L_0x0093:
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r4)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
            r10.measure(r2, r0)
            if (r7 == 0) goto L_0x00b4
            int r0 = r10.getMeasuredHeight()
            int r5 = r5 - r0
        L_0x00a5:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x003b
        L_0x00a9:
            r4 = 0
            r7 = r4
            goto L_0x006b
        L_0x00ac:
            r4 = 0
            r6 = r4
            goto L_0x0073
        L_0x00af:
            if (r6 == 0) goto L_0x0077
            r1 = 1073741824(0x40000000, float:2.0)
            goto L_0x0077
        L_0x00b4:
            if (r6 == 0) goto L_0x00a5
            int r0 = r10.getMeasuredWidth()
            int r3 = r3 - r0
            goto L_0x00a5
        L_0x00bc:
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r0)
            r13.CI3C103l01O = r0
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r0)
            r13.CII3C813OIC8 = r0
            r0 = 1
            r13.CIOC8C = r0
            r13.C0I1O3C3lI8()
            r0 = 0
            r13.CIOC8C = r0
            int r2 = r13.getChildCount()
            r0 = 0
            r1 = r0
        L_0x00db:
            if (r1 >= r2) goto L_0x0109
            android.view.View r4 = r13.getChildAt(r1)
            int r0 = r4.getVisibility()
            r5 = 8
            if (r0 == r5) goto L_0x0105
            android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
            android.support.v4.view.II083CII r0 = (android.support.v4.view.II083CII) r0
            if (r0 == 0) goto L_0x00f5
            boolean r5 = r0.C01O0C
            if (r5 != 0) goto L_0x0105
        L_0x00f5:
            float r5 = (float) r3
            float r0 = r0.C101lC8O
            float r0 = r0 * r5
            int r0 = (int) r0
            r5 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r5)
            int r5 = r13.CII3C813OIC8
            r4.measure(r0, r5)
        L_0x0105:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00db
        L_0x0109:
            return
        L_0x010a:
            r0 = r5
            goto L_0x0093
        L_0x010c:
            r2 = r3
            goto L_0x0085
        L_0x010f:
            r4 = r2
            r2 = r3
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        int i2;
        ICOI8lC3 C01O0C2;
        int i3 = -1;
        int childCount = getChildCount();
        if ((i & 2) != 0) {
            i3 = 1;
            i2 = 0;
        } else {
            i2 = childCount - 1;
            childCount = -1;
        }
        while (i2 != childCount) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (C01O0C2 = C01O0C(childAt)) != null && C01O0C2.C0I1O3C3lI8 == this.C1OC33O0lO81 && childAt.requestFocus(i, rect)) {
                return true;
            }
            i2 += i3;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.C01O0C(android.support.v4.view.ICOI8lC3, int, android.support.v4.view.ICOI8lC3):void
      android.support.v4.view.ViewPager.C01O0C(int, float, int):void
      android.support.v4.view.ViewPager.C01O0C(int, int, int):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean):void */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (this.C1O10Cl038 != null) {
            this.C1O10Cl038.C01O0C(savedState.C0I1O3C3lI8, savedState.C101lC8O);
            C01O0C(savedState.C01O0C, false, true);
            return;
        }
        this.C3C1C0I8l3 = savedState.C01O0C;
        this.C3CIO118 = savedState.C0I1O3C3lI8;
        this.C3ICl0OOl = savedState.C101lC8O;
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.C01O0C = this.C1OC33O0lO81;
        if (this.C1O10Cl038 != null) {
            savedState.C0I1O3C3lI8 = this.C1O10Cl038.C0I1O3C3lI8();
        }
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3) {
            C01O0C(i, i3, this.C831O13C118, this.C831O13C118);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.C01O0C(int, float, int, int):int
      android.support.v4.view.ViewPager.C01O0C(int, int, int, int):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, int, boolean):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.C01O0C(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.C01O0C(int, float, int, int):int
      android.support.v4.view.ViewPager.C01O0C(int, int, int, int):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, int, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        if (this.I1I11O81II) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (this.C1O10Cl038 == null || this.C1O10Cl038.C01O0C() == 0) {
            return false;
        }
        if (this.I0CIIIC == null) {
            this.I0CIIIC = VelocityTracker.obtain();
        }
        this.I0CIIIC.addMovement(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.C3l3O8lIOIO8.abortAnimation();
                this.ClC13lIl = false;
                C0I1O3C3lI8();
                float x = motionEvent.getX();
                this.I03lII1 = x;
                this.I003OlCCOlC = x;
                float y = motionEvent.getY();
                this.I088l3088 = y;
                this.I008018O = y;
                this.I08O3C = CCC3CC0l.C0I1O3C3lI8(motionEvent, 0);
                break;
            case 1:
                if (this.CO081lO0OC0) {
                    VelocityTracker velocityTracker = this.I0CIIIC;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.I0l3Oll3);
                    int C01O0C2 = (int) ClC13lIl.C01O0C(velocityTracker, this.I08O3C);
                    this.ClC13lIl = true;
                    int clientWidth = getClientWidth();
                    int scrollX = getScrollX();
                    ICOI8lC3 C1O10Cl0382 = C1O10Cl038();
                    C01O0C(C01O0C(C1O10Cl0382.C0I1O3C3lI8, ((((float) scrollX) / ((float) clientWidth)) - C1O10Cl0382.C11ll3) / C1O10Cl0382.C11013l3, C01O0C2, (int) (CCC3CC0l.C101lC8O(motionEvent, CCC3CC0l.C01O0C(motionEvent, this.I08O3C)) - this.I03lII1)), true, true, C01O0C2);
                    this.I08O3C = -1;
                    C1OC33O0lO81();
                    z = this.I3ClO1C31.C101lC8O() | this.I30OCIOO.C101lC8O();
                    break;
                }
                break;
            case 2:
                if (!this.CO081lO0OC0) {
                    int C01O0C3 = CCC3CC0l.C01O0C(motionEvent, this.I08O3C);
                    float C101lC8O2 = CCC3CC0l.C101lC8O(motionEvent, C01O0C3);
                    float abs = Math.abs(C101lC8O2 - this.I003OlCCOlC);
                    float C11013l32 = CCC3CC0l.C11013l3(motionEvent, C01O0C3);
                    float abs2 = Math.abs(C11013l32 - this.I008018O);
                    if (abs > ((float) this.I003I0) && abs > abs2) {
                        this.CO081lO0OC0 = true;
                        C101lC8O(true);
                        this.I003OlCCOlC = C101lC8O2 - this.I03lII1 > 0.0f ? this.I03lII1 + ((float) this.I003I0) : this.I03lII1 - ((float) this.I003I0);
                        this.I008018O = C11013l32;
                        setScrollState(1);
                        setScrollingCacheEnabled(true);
                        ViewParent parent = getParent();
                        if (parent != null) {
                            parent.requestDisallowInterceptTouchEvent(true);
                        }
                    }
                }
                if (this.CO081lO0OC0) {
                    z = false | C0I1O3C3lI8(CCC3CC0l.C101lC8O(motionEvent, CCC3CC0l.C01O0C(motionEvent, this.I08O3C)));
                    break;
                }
                break;
            case 3:
                if (this.CO081lO0OC0) {
                    C01O0C(this.C1OC33O0lO81, true, 0, false);
                    this.I08O3C = -1;
                    C1OC33O0lO81();
                    z = this.I3ClO1C31.C101lC8O() | this.I30OCIOO.C101lC8O();
                    break;
                }
                break;
            case 5:
                int C0I1O3C3lI82 = CCC3CC0l.C0I1O3C3lI8(motionEvent);
                this.I003OlCCOlC = CCC3CC0l.C101lC8O(motionEvent, C0I1O3C3lI82);
                this.I08O3C = CCC3CC0l.C0I1O3C3lI8(motionEvent, C0I1O3C3lI82);
                break;
            case 6:
                C01O0C(motionEvent);
                this.I003OlCCOlC = CCC3CC0l.C101lC8O(motionEvent, CCC3CC0l.C01O0C(motionEvent, this.I08O3C));
                break;
        }
        if (z) {
            CO88CO1Cl383.C0I1O3C3lI8(this);
        }
        return true;
    }

    public void removeView(View view) {
        if (this.CIOC8C) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.Cl80C0l838l.C01O0C(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.View, int, java.lang.Object):void
      android.support.v4.view.Cl80C0l838l.C01O0C(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.C01O0C(android.support.v4.view.ICOI8lC3, int, android.support.v4.view.ICOI8lC3):void
      android.support.v4.view.ViewPager.C01O0C(int, float, int):void
      android.support.v4.view.ViewPager.C01O0C(int, int, int):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean):void */
    public void setAdapter(Cl80C0l838l cl80C0l838l) {
        if (this.C1O10Cl038 != null) {
            this.C1O10Cl038.C0I1O3C3lI8(this.C3llC38O1);
            this.C1O10Cl038.C01O0C((ViewGroup) this);
            for (int i = 0; i < this.C11ll3.size(); i++) {
                ICOI8lC3 iCOI8lC3 = (ICOI8lC3) this.C11ll3.get(i);
                this.C1O10Cl038.C01O0C((ViewGroup) this, iCOI8lC3.C0I1O3C3lI8, iCOI8lC3.C01O0C);
            }
            this.C1O10Cl038.C0I1O3C3lI8((ViewGroup) this);
            this.C11ll3.clear();
            C18Cl1C();
            this.C1OC33O0lO81 = 0;
            scrollTo(0, 0);
        }
        Cl80C0l838l cl80C0l838l2 = this.C1O10Cl038;
        this.C1O10Cl038 = cl80C0l838l;
        this.C0I1O3C3lI8 = 0;
        if (this.C1O10Cl038 != null) {
            if (this.C3llC38O1 == null) {
                this.C3llC38O1 = new IICICOC38(this, null);
            }
            this.C1O10Cl038.C01O0C((DataSetObserver) this.C3llC38O1);
            this.ClC13lIl = false;
            boolean z = this.I80183lOl;
            this.I80183lOl = true;
            this.C0I1O3C3lI8 = this.C1O10Cl038.C01O0C();
            if (this.C3C1C0I8l3 >= 0) {
                this.C1O10Cl038.C01O0C(this.C3CIO118, this.C3ICl0OOl);
                C01O0C(this.C3C1C0I8l3, false, true);
                this.C3C1C0I8l3 = -1;
                this.C3CIO118 = null;
                this.C3ICl0OOl = null;
            } else if (!z) {
                C0I1O3C3lI8();
            } else {
                requestLayout();
            }
        }
        if (this.II083CII != null && cl80C0l838l2 != cl80C0l838l) {
            this.II083CII.C01O0C(cl80C0l838l2, cl80C0l838l);
        }
    }

    /* access modifiers changed from: package-private */
    public void setChildrenDrawingOrderEnabledCompat(boolean z) {
        if (Build.VERSION.SDK_INT >= 7) {
            if (this.II1lC1O == null) {
                Class<ViewGroup> cls = ViewGroup.class;
                try {
                    this.II1lC1O = cls.getDeclaredMethod("setChildrenDrawingOrderEnabled", Boolean.TYPE);
                } catch (NoSuchMethodException e) {
                    Log.e("ViewPager", "Can't find setChildrenDrawingOrderEnabled", e);
                }
            }
            try {
                this.II1lC1O.invoke(this, Boolean.valueOf(z));
            } catch (Exception e2) {
                Log.e("ViewPager", "Error changing children drawing order", e2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.C01O0C(android.support.v4.view.ICOI8lC3, int, android.support.v4.view.ICOI8lC3):void
      android.support.v4.view.ViewPager.C01O0C(int, float, int):void
      android.support.v4.view.ViewPager.C01O0C(int, int, int):void
      android.support.v4.view.ViewPager.C01O0C(int, boolean, boolean):void */
    public void setCurrentItem(int i) {
        this.ClC13lIl = false;
        C01O0C(i, !this.I80183lOl, false);
    }

    public void setOffscreenPageLimit(int i) {
        if (i < 1) {
            Log.w("ViewPager", "Requested offscreen page limit " + i + " too small; defaulting to " + 1);
            i = 1;
        }
        if (i != this.ClO80C3lOO8) {
            this.ClO80C3lOO8 = i;
            C0I1O3C3lI8();
        }
    }

    /* access modifiers changed from: package-private */
    public void setOnAdapterChangeListener(II0ll1CC13l iI0ll1CC13l) {
        this.II083CII = iI0ll1CC13l;
    }

    public void setOnPageChangeListener(II1lC1O iI1lC1O) {
        this.ICI3C3O = iI1lC1O;
    }

    public void setPageMargin(int i) {
        int i2 = this.C831O13C118;
        this.C831O13C118 = i;
        int width = getWidth();
        C01O0C(width, width, i, i2);
        requestLayout();
    }

    public void setPageMarginDrawable(int i) {
        setPageMarginDrawable(getContext().getResources().getDrawable(i));
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.C8CI00 = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.C8CI00;
    }
}
