package android.support.v4.view;

import android.view.View;

class I80183lOl {
    public static void C01O0C(View view) {
        view.postInvalidateOnAnimation();
    }

    public static void C01O0C(View view, int i) {
        view.setImportantForAccessibility(i);
    }

    public static void C01O0C(View view, int i, int i2, int i3, int i4) {
        view.postInvalidate(i, i2, i3, i4);
    }

    public static void C01O0C(View view, Runnable runnable) {
        view.postOnAnimation(runnable);
    }

    public static boolean C0I1O3C3lI8(View view) {
        return view.getFitsSystemWindows();
    }
}
