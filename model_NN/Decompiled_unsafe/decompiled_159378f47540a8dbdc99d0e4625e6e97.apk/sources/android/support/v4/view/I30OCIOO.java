package android.support.v4.view;

import android.animation.ValueAnimator;
import android.graphics.Paint;
import android.view.View;

class I30OCIOO {
    public static int C01O0C(View view) {
        return view.getLayerType();
    }

    static long C01O0C() {
        return ValueAnimator.getFrameDelay();
    }

    public static void C01O0C(View view, int i, Paint paint) {
        view.setLayerType(i, paint);
    }

    public static void C01O0C(View view, boolean z) {
        view.setSaveFromParentEnabled(z);
    }
}
