package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

class I088l3088 extends I03lII1 {
    I088l3088() {
    }

    /* access modifiers changed from: package-private */
    public long C01O0C() {
        return I30OCIOO.C01O0C();
    }

    public void C01O0C(View view, int i, Paint paint) {
        I30OCIOO.C01O0C(view, i, paint);
    }

    public void C01O0C(View view, Paint paint) {
        C01O0C(view, C101lC8O(view), paint);
        view.invalidate();
    }

    public void C01O0C(View view, boolean z) {
        I30OCIOO.C01O0C(view, z);
    }

    public int C101lC8O(View view) {
        return I30OCIOO.C01O0C(view);
    }
}
