package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

final class C11ll3 implements Parcelable.Creator {
    C11ll3() {
    }

    /* renamed from: C01O0C */
    public MediaMetadataCompat createFromParcel(Parcel parcel) {
        return new MediaMetadataCompat(parcel, null);
    }

    /* renamed from: C01O0C */
    public MediaMetadataCompat[] newArray(int i) {
        return new MediaMetadataCompat[i];
    }
}
