package android.support.v4.media.session;

import android.os.Build;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.v4.media.MediaDescriptionCompat;

public class MediaSessionCompat {

    public final class QueueItem implements Parcelable {
        public static final Parcelable.Creator CREATOR = new C0I1O3C3lI8();
        private final MediaDescriptionCompat C01O0C;
        private final long C0I1O3C3lI8;

        private QueueItem(Parcel parcel) {
            this.C01O0C = (MediaDescriptionCompat) MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
            this.C0I1O3C3lI8 = parcel.readLong();
        }

        public int describeContents() {
            return 0;
        }

        public String toString() {
            return "MediaSession.QueueItem {Description=" + this.C01O0C + ", Id=" + this.C0I1O3C3lI8 + " }";
        }

        public void writeToParcel(Parcel parcel, int i) {
            this.C01O0C.writeToParcel(parcel, i);
            parcel.writeLong(this.C0I1O3C3lI8);
        }
    }

    final class ResultReceiverWrapper implements Parcelable {
        public static final Parcelable.Creator CREATOR = new C101lC8O();
        private ResultReceiver C01O0C;

        ResultReceiverWrapper(Parcel parcel) {
            this.C01O0C = (ResultReceiver) ResultReceiver.CREATOR.createFromParcel(parcel);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            this.C01O0C.writeToParcel(parcel, i);
        }
    }

    public final class Token implements Parcelable {
        public static final Parcelable.Creator CREATOR = new C11013l3();
        private final Object C01O0C;

        Token(Object obj) {
            this.C01O0C = obj;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            if (Build.VERSION.SDK_INT >= 21) {
                parcel.writeParcelable((Parcelable) this.C01O0C, i);
            } else {
                parcel.writeStrongBinder((IBinder) this.C01O0C);
            }
        }
    }
}
