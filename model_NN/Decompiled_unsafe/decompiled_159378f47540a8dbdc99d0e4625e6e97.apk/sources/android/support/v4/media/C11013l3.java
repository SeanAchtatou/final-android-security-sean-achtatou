package android.support.v4.media;

import android.graphics.Bitmap;
import android.media.MediaDescription;
import android.net.Uri;
import android.os.Bundle;

public class C11013l3 {
    public static Object C01O0C() {
        return new MediaDescription.Builder();
    }

    public static Object C01O0C(Object obj) {
        return ((MediaDescription.Builder) obj).build();
    }

    public static void C01O0C(Object obj, Bitmap bitmap) {
        ((MediaDescription.Builder) obj).setIconBitmap(bitmap);
    }

    public static void C01O0C(Object obj, Uri uri) {
        ((MediaDescription.Builder) obj).setIconUri(uri);
    }

    public static void C01O0C(Object obj, Bundle bundle) {
        ((MediaDescription.Builder) obj).setExtras(bundle);
    }

    public static void C01O0C(Object obj, CharSequence charSequence) {
        ((MediaDescription.Builder) obj).setTitle(charSequence);
    }

    public static void C01O0C(Object obj, String str) {
        ((MediaDescription.Builder) obj).setMediaId(str);
    }

    public static void C0I1O3C3lI8(Object obj, CharSequence charSequence) {
        ((MediaDescription.Builder) obj).setSubtitle(charSequence);
    }

    public static void C101lC8O(Object obj, CharSequence charSequence) {
        ((MediaDescription.Builder) obj).setDescription(charSequence);
    }
}
