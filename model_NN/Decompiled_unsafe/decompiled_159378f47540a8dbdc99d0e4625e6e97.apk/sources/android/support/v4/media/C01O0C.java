package android.support.v4.media;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

final class C01O0C implements Parcelable.Creator {
    C01O0C() {
    }

    /* renamed from: C01O0C */
    public MediaDescriptionCompat createFromParcel(Parcel parcel) {
        return Build.VERSION.SDK_INT < 21 ? new MediaDescriptionCompat(parcel, null) : MediaDescriptionCompat.C01O0C(C101lC8O.C01O0C(parcel));
    }

    /* renamed from: C01O0C */
    public MediaDescriptionCompat[] newArray(int i) {
        return new MediaDescriptionCompat[i];
    }
}
