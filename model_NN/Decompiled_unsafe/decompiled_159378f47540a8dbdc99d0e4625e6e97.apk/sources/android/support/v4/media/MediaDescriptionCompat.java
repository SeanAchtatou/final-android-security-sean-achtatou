package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public final class MediaDescriptionCompat implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C01O0C();
    private final String C01O0C;
    private final CharSequence C0I1O3C3lI8;
    private final CharSequence C101lC8O;
    private final CharSequence C11013l3;
    private final Bitmap C11ll3;
    private final Uri C18Cl1C;
    private Object C1O10Cl038;
    private final Bundle C1l00I1;

    private MediaDescriptionCompat(Parcel parcel) {
        this.C01O0C = parcel.readString();
        this.C0I1O3C3lI8 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.C101lC8O = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.C11013l3 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.C11ll3 = (Bitmap) parcel.readParcelable(null);
        this.C18Cl1C = (Uri) parcel.readParcelable(null);
        this.C1l00I1 = parcel.readBundle();
    }

    /* synthetic */ MediaDescriptionCompat(Parcel parcel, C01O0C c01o0c) {
        this(parcel);
    }

    private MediaDescriptionCompat(String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Bitmap bitmap, Uri uri, Bundle bundle) {
        this.C01O0C = str;
        this.C0I1O3C3lI8 = charSequence;
        this.C101lC8O = charSequence2;
        this.C11013l3 = charSequence3;
        this.C11ll3 = bitmap;
        this.C18Cl1C = uri;
        this.C1l00I1 = bundle;
    }

    /* synthetic */ MediaDescriptionCompat(String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Bitmap bitmap, Uri uri, Bundle bundle, C01O0C c01o0c) {
        this(str, charSequence, charSequence2, charSequence3, bitmap, uri, bundle);
    }

    public static MediaDescriptionCompat C01O0C(Object obj) {
        if (obj == null || Build.VERSION.SDK_INT < 21) {
            return null;
        }
        C0I1O3C3lI8 c0I1O3C3lI8 = new C0I1O3C3lI8();
        c0I1O3C3lI8.C01O0C(C101lC8O.C01O0C(obj));
        c0I1O3C3lI8.C01O0C(C101lC8O.C0I1O3C3lI8(obj));
        c0I1O3C3lI8.C0I1O3C3lI8(C101lC8O.C101lC8O(obj));
        c0I1O3C3lI8.C101lC8O(C101lC8O.C11013l3(obj));
        c0I1O3C3lI8.C01O0C(C101lC8O.C11ll3(obj));
        c0I1O3C3lI8.C01O0C(C101lC8O.C18Cl1C(obj));
        c0I1O3C3lI8.C01O0C(C101lC8O.C1l00I1(obj));
        MediaDescriptionCompat C01O0C2 = c0I1O3C3lI8.C01O0C();
        C01O0C2.C1O10Cl038 = obj;
        return C01O0C2;
    }

    public Object C01O0C() {
        if (this.C1O10Cl038 != null || Build.VERSION.SDK_INT < 21) {
            return this.C1O10Cl038;
        }
        Object C01O0C2 = C11013l3.C01O0C();
        C11013l3.C01O0C(C01O0C2, this.C01O0C);
        C11013l3.C01O0C(C01O0C2, this.C0I1O3C3lI8);
        C11013l3.C0I1O3C3lI8(C01O0C2, this.C101lC8O);
        C11013l3.C101lC8O(C01O0C2, this.C11013l3);
        C11013l3.C01O0C(C01O0C2, this.C11ll3);
        C11013l3.C01O0C(C01O0C2, this.C18Cl1C);
        C11013l3.C01O0C(C01O0C2, this.C1l00I1);
        this.C1O10Cl038 = C11013l3.C01O0C(C01O0C2);
        return this.C1O10Cl038;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return ((Object) this.C0I1O3C3lI8) + ", " + ((Object) this.C101lC8O) + ", " + ((Object) this.C11013l3);
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (Build.VERSION.SDK_INT < 21) {
            parcel.writeString(this.C01O0C);
            TextUtils.writeToParcel(this.C0I1O3C3lI8, parcel, i);
            TextUtils.writeToParcel(this.C101lC8O, parcel, i);
            TextUtils.writeToParcel(this.C11013l3, parcel, i);
            parcel.writeParcelable(this.C11ll3, i);
            parcel.writeParcelable(this.C18Cl1C, i);
            parcel.writeBundle(this.C1l00I1);
            return;
        }
        C101lC8O.C01O0C(C01O0C(), parcel, i);
    }
}
