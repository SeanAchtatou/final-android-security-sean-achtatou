package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable;

public class ParcelableVolumeInfo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C11ll3();
    public int C01O0C;
    public int C0I1O3C3lI8;
    public int C101lC8O;
    public int C11013l3;
    public int C11ll3;

    public ParcelableVolumeInfo(Parcel parcel) {
        this.C01O0C = parcel.readInt();
        this.C101lC8O = parcel.readInt();
        this.C11013l3 = parcel.readInt();
        this.C11ll3 = parcel.readInt();
        this.C0I1O3C3lI8 = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.C01O0C);
        parcel.writeInt(this.C101lC8O);
        parcel.writeInt(this.C11013l3);
        parcel.writeInt(this.C11ll3);
        parcel.writeInt(this.C0I1O3C3lI8);
    }
}
