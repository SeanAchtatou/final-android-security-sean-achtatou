package android.support.v4.media.session;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public final class PlaybackStateCompat implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C18Cl1C();
    private final int C01O0C;
    private final long C0I1O3C3lI8;
    private final long C101lC8O;
    private final float C11013l3;
    private final long C11ll3;
    private final CharSequence C18Cl1C;
    private final long C1l00I1;

    public final class CustomAction implements Parcelable {
        public static final Parcelable.Creator CREATOR = new C1l00I1();
        private final String C01O0C;
        private final CharSequence C0I1O3C3lI8;
        private final int C101lC8O;
        private final Bundle C11013l3;

        private CustomAction(Parcel parcel) {
            this.C01O0C = parcel.readString();
            this.C0I1O3C3lI8 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
            this.C101lC8O = parcel.readInt();
            this.C11013l3 = parcel.readBundle();
        }

        /* synthetic */ CustomAction(Parcel parcel, C18Cl1C c18Cl1C) {
            this(parcel);
        }

        public int describeContents() {
            return 0;
        }

        public String toString() {
            return "Action:mName='" + ((Object) this.C0I1O3C3lI8) + ", mIcon=" + this.C101lC8O + ", mExtras=" + this.C11013l3;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.C01O0C);
            TextUtils.writeToParcel(this.C0I1O3C3lI8, parcel, i);
            parcel.writeInt(this.C101lC8O);
            parcel.writeBundle(this.C11013l3);
        }
    }

    private PlaybackStateCompat(Parcel parcel) {
        this.C01O0C = parcel.readInt();
        this.C0I1O3C3lI8 = parcel.readLong();
        this.C11013l3 = parcel.readFloat();
        this.C1l00I1 = parcel.readLong();
        this.C101lC8O = parcel.readLong();
        this.C11ll3 = parcel.readLong();
        this.C18Cl1C = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
    }

    /* synthetic */ PlaybackStateCompat(Parcel parcel, C18Cl1C c18Cl1C) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PlaybackState {");
        sb.append("state=").append(this.C01O0C);
        sb.append(", position=").append(this.C0I1O3C3lI8);
        sb.append(", buffered position=").append(this.C101lC8O);
        sb.append(", speed=").append(this.C11013l3);
        sb.append(", updated=").append(this.C1l00I1);
        sb.append(", actions=").append(this.C11ll3);
        sb.append(", error=").append(this.C18Cl1C);
        sb.append("}");
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.C01O0C);
        parcel.writeLong(this.C0I1O3C3lI8);
        parcel.writeFloat(this.C11013l3);
        parcel.writeLong(this.C1l00I1);
        parcel.writeLong(this.C101lC8O);
        parcel.writeLong(this.C11ll3);
        TextUtils.writeToParcel(this.C18Cl1C, parcel, i);
    }
}
