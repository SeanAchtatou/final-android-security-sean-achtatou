package android.support.v4.media;

import android.graphics.Bitmap;
import android.media.MediaDescription;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;

public class C101lC8O {
    public static Object C01O0C(Parcel parcel) {
        return MediaDescription.CREATOR.createFromParcel(parcel);
    }

    public static String C01O0C(Object obj) {
        return ((MediaDescription) obj).getMediaId();
    }

    public static void C01O0C(Object obj, Parcel parcel, int i) {
        ((MediaDescription) obj).writeToParcel(parcel, i);
    }

    public static CharSequence C0I1O3C3lI8(Object obj) {
        return ((MediaDescription) obj).getTitle();
    }

    public static CharSequence C101lC8O(Object obj) {
        return ((MediaDescription) obj).getSubtitle();
    }

    public static CharSequence C11013l3(Object obj) {
        return ((MediaDescription) obj).getDescription();
    }

    public static Bitmap C11ll3(Object obj) {
        return ((MediaDescription) obj).getIconBitmap();
    }

    public static Uri C18Cl1C(Object obj) {
        return ((MediaDescription) obj).getIconUri();
    }

    public static Bundle C1l00I1(Object obj) {
        return ((MediaDescription) obj).getExtras();
    }
}
