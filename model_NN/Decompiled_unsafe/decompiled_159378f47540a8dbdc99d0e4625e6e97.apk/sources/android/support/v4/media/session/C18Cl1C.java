package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable;

final class C18Cl1C implements Parcelable.Creator {
    C18Cl1C() {
    }

    /* renamed from: C01O0C */
    public PlaybackStateCompat createFromParcel(Parcel parcel) {
        return new PlaybackStateCompat(parcel, null);
    }

    /* renamed from: C01O0C */
    public PlaybackStateCompat[] newArray(int i) {
        return new PlaybackStateCompat[i];
    }
}
