package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

public final class RatingCompat implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C18Cl1C();
    private final int C01O0C;
    private final float C0I1O3C3lI8;

    private RatingCompat(int i, float f) {
        this.C01O0C = i;
        this.C0I1O3C3lI8 = f;
    }

    /* synthetic */ RatingCompat(int i, float f, C18Cl1C c18Cl1C) {
        this(i, f);
    }

    public int describeContents() {
        return this.C01O0C;
    }

    public String toString() {
        return "Rating:style=" + this.C01O0C + " rating=" + (this.C0I1O3C3lI8 < 0.0f ? "unrated" : String.valueOf(this.C0I1O3C3lI8));
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.C01O0C);
        parcel.writeFloat(this.C0I1O3C3lI8);
    }
}
