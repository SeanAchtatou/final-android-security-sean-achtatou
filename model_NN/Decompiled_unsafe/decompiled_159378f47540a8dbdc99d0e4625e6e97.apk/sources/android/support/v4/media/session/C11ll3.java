package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable;

final class C11ll3 implements Parcelable.Creator {
    C11ll3() {
    }

    /* renamed from: C01O0C */
    public ParcelableVolumeInfo createFromParcel(Parcel parcel) {
        return new ParcelableVolumeInfo(parcel);
    }

    /* renamed from: C01O0C */
    public ParcelableVolumeInfo[] newArray(int i) {
        return new ParcelableVolumeInfo[i];
    }
}
