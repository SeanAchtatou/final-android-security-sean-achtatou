package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

final class C18Cl1C implements Parcelable.Creator {
    C18Cl1C() {
    }

    /* renamed from: C01O0C */
    public RatingCompat createFromParcel(Parcel parcel) {
        return new RatingCompat(parcel.readInt(), parcel.readFloat(), null);
    }

    /* renamed from: C01O0C */
    public RatingCompat[] newArray(int i) {
        return new RatingCompat[i];
    }
}
