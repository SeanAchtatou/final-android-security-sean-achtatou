package android.support.v4.media;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.c.C01O0C;

public final class MediaMetadataCompat implements Parcelable {
    private static final C01O0C C01O0C = new C01O0C();
    private static final String[] C0I1O3C3lI8 = {"android.media.metadata.TITLE", "android.media.metadata.ARTIST", "android.media.metadata.ALBUM", "android.media.metadata.ALBUM_ARTIST", "android.media.metadata.WRITER", "android.media.metadata.AUTHOR", "android.media.metadata.COMPOSER"};
    private static final String[] C101lC8O = {"android.media.metadata.DISPLAY_ICON", "android.media.metadata.ART", "android.media.metadata.ALBUM_ART"};
    private static final String[] C11013l3 = {"android.media.metadata.DISPLAY_ICON_URI", "android.media.metadata.ART_URI", "android.media.metadata.ALBUM_ART_URI"};
    public static final Parcelable.Creator CREATOR = new C11ll3();
    private final Bundle C11ll3;

    static {
        C01O0C.put("android.media.metadata.TITLE", 1);
        C01O0C.put("android.media.metadata.ARTIST", 1);
        C01O0C.put("android.media.metadata.DURATION", 0);
        C01O0C.put("android.media.metadata.ALBUM", 1);
        C01O0C.put("android.media.metadata.AUTHOR", 1);
        C01O0C.put("android.media.metadata.WRITER", 1);
        C01O0C.put("android.media.metadata.COMPOSER", 1);
        C01O0C.put("android.media.metadata.COMPILATION", 1);
        C01O0C.put("android.media.metadata.DATE", 1);
        C01O0C.put("android.media.metadata.YEAR", 0);
        C01O0C.put("android.media.metadata.GENRE", 1);
        C01O0C.put("android.media.metadata.TRACK_NUMBER", 0);
        C01O0C.put("android.media.metadata.NUM_TRACKS", 0);
        C01O0C.put("android.media.metadata.DISC_NUMBER", 0);
        C01O0C.put("android.media.metadata.ALBUM_ARTIST", 1);
        C01O0C.put("android.media.metadata.ART", 2);
        C01O0C.put("android.media.metadata.ART_URI", 1);
        C01O0C.put("android.media.metadata.ALBUM_ART", 2);
        C01O0C.put("android.media.metadata.ALBUM_ART_URI", 1);
        C01O0C.put("android.media.metadata.USER_RATING", 3);
        C01O0C.put("android.media.metadata.RATING", 3);
        C01O0C.put("android.media.metadata.DISPLAY_TITLE", 1);
        C01O0C.put("android.media.metadata.DISPLAY_SUBTITLE", 1);
        C01O0C.put("android.media.metadata.DISPLAY_DESCRIPTION", 1);
        C01O0C.put("android.media.metadata.DISPLAY_ICON", 2);
        C01O0C.put("android.media.metadata.DISPLAY_ICON_URI", 1);
        C01O0C.put("android.media.metadata.MEDIA_ID", 1);
    }

    private MediaMetadataCompat(Parcel parcel) {
        this.C11ll3 = parcel.readBundle();
    }

    /* synthetic */ MediaMetadataCompat(Parcel parcel, C11ll3 c11ll3) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.C11ll3);
    }
}
