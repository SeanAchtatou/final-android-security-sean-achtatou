package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

public final class C0I1O3C3lI8 {
    private String C01O0C;
    private CharSequence C0I1O3C3lI8;
    private CharSequence C101lC8O;
    private CharSequence C11013l3;
    private Bitmap C11ll3;
    private Uri C18Cl1C;
    private Bundle C1l00I1;

    public C0I1O3C3lI8 C01O0C(Bitmap bitmap) {
        this.C11ll3 = bitmap;
        return this;
    }

    public C0I1O3C3lI8 C01O0C(Uri uri) {
        this.C18Cl1C = uri;
        return this;
    }

    public C0I1O3C3lI8 C01O0C(Bundle bundle) {
        this.C1l00I1 = bundle;
        return this;
    }

    public C0I1O3C3lI8 C01O0C(CharSequence charSequence) {
        this.C0I1O3C3lI8 = charSequence;
        return this;
    }

    public C0I1O3C3lI8 C01O0C(String str) {
        this.C01O0C = str;
        return this;
    }

    public MediaDescriptionCompat C01O0C() {
        return new MediaDescriptionCompat(this.C01O0C, this.C0I1O3C3lI8, this.C101lC8O, this.C11013l3, this.C11ll3, this.C18Cl1C, this.C1l00I1, null);
    }

    public C0I1O3C3lI8 C0I1O3C3lI8(CharSequence charSequence) {
        this.C101lC8O = charSequence;
        return this;
    }

    public C0I1O3C3lI8 C101lC8O(CharSequence charSequence) {
        this.C11013l3 = charSequence;
        return this;
    }
}
