package android.support.v4.b;

import android.os.Parcel;
import android.os.Parcelable;

class C0I1O3C3lI8 implements Parcelable.Creator {
    final C101lC8O C01O0C;

    public C0I1O3C3lI8(C101lC8O c101lC8O) {
        this.C01O0C = c101lC8O;
    }

    public Object createFromParcel(Parcel parcel) {
        return this.C01O0C.C01O0C(parcel, null);
    }

    public Object[] newArray(int i) {
        return this.C01O0C.C01O0C(i);
    }
}
