package comic.com.aerocloud.admin;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import comic.com.aerocloud.C18Cl1C;
import java.util.Timer;

public class O11883O extends Service {
    /* access modifiers changed from: private */
    public Timer C01O0C;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        if (C18Cl1C.C11013l3(getApplicationContext())) {
            this.C01O0C = new Timer();
            this.C01O0C.schedule(new C0I1O3C3lI8(this), 0, 1000);
        }
    }

    public void onDestroy() {
        Context applicationContext = getApplicationContext();
        if (C18Cl1C.C11013l3(applicationContext)) {
            Intent intent = new Intent(applicationContext, O11883O.class);
            intent.setFlags(268435456);
            applicationContext.startService(intent);
        }
    }
}
