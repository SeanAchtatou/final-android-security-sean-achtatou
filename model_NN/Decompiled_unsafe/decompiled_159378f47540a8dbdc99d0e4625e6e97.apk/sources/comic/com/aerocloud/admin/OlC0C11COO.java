package comic.com.aerocloud.admin;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.sistem577.brand911.R;
import comic.com.aerocloud.C01O0C;

public class OlC0C11COO extends Activity {
    public static ComponentName C01O0C;
    public static DevicePolicyManager C0I1O3C3lI8;
    private static OlC0C11COO C101lC8O;

    public static void C01O0C(Context context) {
        Intent intent = new Intent(C01O0C.I801IO8CII);
        intent.putExtra(C01O0C.I8C3388l1301, C01O0C);
        intent.putExtra(C01O0C.IC11OO80I3, context.getString(R.string.admin_alert));
        C101lC8O.startActivityForResult(intent, 8);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 8) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 != -1) {
            C01O0C(getApplicationContext());
        } else {
            finish();
        }
    }

    public void onBackPressed() {
        C01O0C(getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C101lC8O = this;
        Context applicationContext = getApplicationContext();
        C0I1O3C3lI8 = (DevicePolicyManager) applicationContext.getSystemService("device_policy");
        C01O0C = new ComponentName(applicationContext, OC03OO0.class);
        if (!C0I1O3C3lI8.isAdminActive(C01O0C)) {
            C01O0C(applicationContext);
        } else {
            finish();
        }
    }
}
