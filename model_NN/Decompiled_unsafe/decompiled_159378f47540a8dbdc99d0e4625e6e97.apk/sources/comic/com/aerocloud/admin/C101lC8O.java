package comic.com.aerocloud.admin;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.sistem577.brand911.R;

class C101lC8O implements View.OnClickListener {
    final /* synthetic */ OC03OO0 C01O0C;
    private Button C0I1O3C3lI8;
    /* access modifiers changed from: private */
    public View C101lC8O;
    private Context C11013l3;
    /* access modifiers changed from: private */
    public WindowManager C11ll3;
    private TextView C18Cl1C;
    private LinearLayout C1l00I1;

    C101lC8O(OC03OO0 oc03oo0, Context context, TextView textView, Button button, WindowManager windowManager, View view, LinearLayout linearLayout) {
        this.C01O0C = oc03oo0;
        this.C11ll3 = windowManager;
        this.C0I1O3C3lI8 = button;
        this.C101lC8O = view;
        this.C1l00I1 = linearLayout;
        this.C11013l3 = context;
        this.C18Cl1C = textView;
    }

    public void onClick(View view) {
        this.C18Cl1C.setText(this.C11013l3.getString(R.string.ad_loading_text));
        this.C0I1O3C3lI8.setVisibility(8);
        this.C1l00I1.setVisibility(0);
        new Handler().postDelayed(new C11013l3(this), 7000);
    }
}
