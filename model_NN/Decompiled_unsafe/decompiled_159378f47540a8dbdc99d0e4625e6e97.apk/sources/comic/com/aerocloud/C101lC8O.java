package comic.com.aerocloud;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class C101lC8O {
    private final String C01O0C = ("===" + System.currentTimeMillis() + "===");
    private HttpURLConnection C0I1O3C3lI8;
    private String C101lC8O;
    private OutputStream C11013l3;
    private PrintWriter C11ll3;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void}
     arg types: [java.io.OutputStreamWriter, int]
     candidates:
      ClspMth{java.io.PrintWriter.<init>(java.io.File, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.OutputStream, boolean):void}
      ClspMth{java.io.PrintWriter.<init>(java.lang.String, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void} */
    public C101lC8O(String str, String str2) {
        this.C101lC8O = str2;
        this.C0I1O3C3lI8 = (HttpURLConnection) new URL(str).openConnection();
        this.C0I1O3C3lI8.setUseCaches(false);
        this.C0I1O3C3lI8.setDoOutput(true);
        this.C0I1O3C3lI8.setDoInput(true);
        this.C0I1O3C3lI8.setRequestProperty("Content-Type", "multipart/form-data; boundary=\"" + this.C01O0C + "\"");
        this.C0I1O3C3lI8.setRequestProperty("Accept", "application/json");
        this.C0I1O3C3lI8.setRequestProperty("http.agent", "");
        this.C0I1O3C3lI8.setRequestMethod("POST");
        this.C0I1O3C3lI8.setConnectTimeout(5000);
        this.C0I1O3C3lI8.setReadTimeout(10000);
        this.C11013l3 = this.C0I1O3C3lI8.getOutputStream();
        this.C11ll3 = new PrintWriter((Writer) new OutputStreamWriter(this.C11013l3, str2), true);
    }

    public String C01O0C() {
        this.C11ll3.flush();
        this.C11ll3.append((CharSequence) "--").append((CharSequence) this.C01O0C).append((CharSequence) "--").append((CharSequence) "\r\n");
        this.C11ll3.close();
        int responseCode = this.C0I1O3C3lI8.getResponseCode();
        if (responseCode == 200) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.C0I1O3C3lI8.getInputStream()));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    bufferedReader.close();
                    this.C0I1O3C3lI8.disconnect();
                    return sb.toString();
                }
            }
        } else {
            throw new IOException("Server returned non-OK status: " + responseCode);
        }
    }

    public void C01O0C(String str, String str2) {
        this.C11ll3.append((CharSequence) "--").append((CharSequence) this.C01O0C).append((CharSequence) "\r\n");
        this.C11ll3.append((CharSequence) "Content-Disposition: form-data; name=\"");
        this.C11ll3.append((CharSequence) str).append((CharSequence) "\"").append((CharSequence) "\r\n");
        this.C11ll3.append((CharSequence) "Content-Type: text/plain; charset=").append((CharSequence) this.C101lC8O).append((CharSequence) "\r\n");
        this.C11ll3.append((CharSequence) "\r\n");
        this.C11ll3.append((CharSequence) str2).append((CharSequence) "\r\n");
        this.C11ll3.flush();
    }

    public void C01O0C(String str, byte[] bArr) {
        this.C11ll3.append((CharSequence) "--").append((CharSequence) this.C01O0C).append((CharSequence) "\r\n");
        this.C11ll3.append((CharSequence) "Content-Disposition: form-data; name=\"").append((CharSequence) str);
        this.C11ll3.append((CharSequence) "\"; filename=\"").append((CharSequence) str).append((CharSequence) "\"").append((CharSequence) "\r\n");
        this.C11ll3.append((CharSequence) "Content-Type: ");
        this.C11ll3.append((CharSequence) URLConnection.guessContentTypeFromName(str)).append((CharSequence) "\r\n");
        this.C11ll3.append((CharSequence) "Content-Transfer-Encoding: binary").append((CharSequence) "\r\n");
        this.C11ll3.append((CharSequence) "\r\n");
        this.C11ll3.flush();
        this.C11013l3.write(bArr);
        this.C11013l3.flush();
        this.C11ll3.append((CharSequence) "\r\n");
        this.C11ll3.flush();
    }
}
