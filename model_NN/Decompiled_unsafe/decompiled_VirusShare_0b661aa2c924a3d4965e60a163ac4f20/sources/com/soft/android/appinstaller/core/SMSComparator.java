package com.soft.android.appinstaller.core;

import com.soft.android.appinstaller.core.SmsInfo;
import java.util.Comparator;

/* compiled from: SmsInfo */
class SMSComparator implements Comparator<SmsInfo.SMS> {
    SMSComparator() {
    }

    public int compare(SmsInfo.SMS o1, SmsInfo.SMS o2) {
        if (o1.getCost() > o2.getCost()) {
            return -1;
        }
        if (o1.getCost() < o2.getCost()) {
            return 1;
        }
        return 0;
    }
}
