package defpackage;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/* renamed from: LogCatBroadcaster  reason: default package */
public class LogCatBroadcaster implements Runnable {
    private static boolean started = false;
    private Context context;

    private LogCatBroadcaster(Context context2) {
        this.context = context2;
    }

    public static synchronized void start(Context context2) {
        Thread thread;
        Runnable runnable;
        Context context3 = context2;
        synchronized (LogCatBroadcaster.class) {
            if (!started) {
                started = true;
                if (Build.VERSION.SDK_INT >= 16) {
                    if (0 != (context3.getApplicationInfo().flags & 2)) {
                        try {
                            PackageInfo packageInfo = context3.getPackageManager().getPackageInfo("com.aide.ui", 128);
                            new LogCatBroadcaster(context3);
                            new Thread(runnable);
                            thread.start();
                        } catch (PackageManager.NameNotFoundException e) {
                        }
                    }
                }
            }
        }
    }

    public void run() {
        BufferedReader bufferedReader;
        Reader reader;
        Intent intent;
        try {
            new InputStreamReader(Runtime.getRuntime().exec("logcat -v threadtime").getInputStream());
            new BufferedReader(reader, 20);
            BufferedReader bufferedReader2 = bufferedReader;
            Object obj = "";
            while (true) {
                String readLine = bufferedReader2.readLine();
                String str = readLine;
                if (readLine != null) {
                    new Intent();
                    Intent intent2 = intent;
                    Intent intent3 = intent2.setPackage("com.aide.ui");
                    Intent action = intent2.setAction("com.aide.runtime.VIEW_LOGCAT_ENTRY");
                    Intent putExtra = intent2.putExtra("lines", new String[]{str});
                    this.context.sendBroadcast(intent2);
                } else {
                    return;
                }
            }
        } catch (IOException e) {
        }
    }
}
