package com.h;

public final class R {

    public static final class anim {

        /* renamed from: show1 */
        public static final int MT_Bin = 2130968576;

        /* renamed from: show2  reason: collision with other field name */
        public static final int f0show2 = 2130968577;

        /* renamed from: show3  reason: collision with other field name */
        public static final int f1show3 = 2130968578;

        /* renamed from: show4  reason: collision with other field name */
        public static final int f2show4 = 2130968579;

        /* renamed from: tvanim  reason: collision with other field name */
        public static final int f3tvanim = 2130968580;
        /* added by JADX */

        /* renamed from: MT_Bin  reason: collision with other field name */
        public static final int f4MT_Bin = 2130968576;
    }

    public static final class attr {
    }

    public static final class color {

        /* renamed from: down */
        public static final int MT_Bin = 2131165184;

        /* renamed from: stroke  reason: collision with other field name */
        public static final int f5stroke = 2131165186;

        /* renamed from: up  reason: collision with other field name */
        public static final int f6up = 2131165185;
        /* added by JADX */

        /* renamed from: MT_Bin  reason: collision with other field name */
        public static final int f7MT_Bin = 2131165184;
    }

    public static final class drawable {

        /* renamed from: authorization_manager_image */
        public static final int MT_Bin = 2130837504;

        /* renamed from: btn_green_normal  reason: collision with other field name */
        public static final int f8btn_green_normal = 2130837505;

        /* renamed from: buttonyun  reason: collision with other field name */
        public static final int f9buttonyun = 2130837506;

        /* renamed from: ed  reason: collision with other field name */
        public static final int f10ed = 2130837507;

        /* renamed from: ic_launcher  reason: collision with other field name */
        public static final int f11ic_launcher = 2130837508;

        /* renamed from: ic_mess_receiver_btn_pressed  reason: collision with other field name */
        public static final int f12ic_mess_receiver_btn_pressed = 2130837509;

        /* renamed from: icon  reason: collision with other field name */
        public static final int f13icon = 2130837510;

        /* renamed from: image_3  reason: collision with other field name */
        public static final int f14image_3 = 2130837511;

        /* renamed from: image_7  reason: collision with other field name */
        public static final int f15image_7 = 2130837512;

        /* renamed from: ok01  reason: collision with other field name */
        public static final int f16ok01 = 2130837513;

        /* renamed from: tv  reason: collision with other field name */
        public static final int f17tv = 2130837514;
        /* added by JADX */

        /* renamed from: MT_Bin  reason: collision with other field name */
        public static final int f18MT_Bin = 2130837504;
    }

    public static final class id {

        /* renamed from: bt */
        public static final int MT_Bin = 2131361794;

        /* renamed from: ed  reason: collision with other field name */
        public static final int f19ed = 2131361793;

        /* renamed from: nr  reason: collision with other field name */
        public static final int f20nr = 2131361795;

        /* renamed from: tv  reason: collision with other field name */
        public static final int f21tv = 2131361792;
        /* added by JADX */

        /* renamed from: MT_Bin  reason: collision with other field name */
        public static final int f22MT_Bin = 2131361792;
    }

    public static final class layout {

        /* renamed from: main */
        public static final int MT_Bin = 2130903040;

        /* renamed from: newone  reason: collision with other field name */
        public static final int f23newone = 2130903041;
        /* added by JADX */

        /* renamed from: MT_Bin  reason: collision with other field name */
        public static final int f24MT_Bin = 2130903040;
    }

    public static final class raw {

        /* renamed from: mm */
        public static final int MT_Bin = 2131099648;

        /* renamed from: nr  reason: collision with other field name */
        public static final int f25nr = 2131099649;

        /* renamed from: pin  reason: collision with other field name */
        public static final int f26pin = 2131099650;
        /* added by JADX */

        /* renamed from: MT_Bin  reason: collision with other field name */
        public static final int f27MT_Bin = 2131099648;
    }

    public static final class string {

        /* renamed from: app_name */
        public static final int MT_Bin = 2131230721;

        /* renamed from: hello  reason: collision with other field name */
        public static final int f28hello = 2131230720;
        /* added by JADX */

        /* renamed from: MT_Bin  reason: collision with other field name */
        public static final int f29MT_Bin = 2131230720;
    }

    public static final class style {

        /* renamed from: AppTheme */
        public static final int MT_Bin = 2131296256;
        /* added by JADX */

        /* renamed from: MT_Bin  reason: collision with other field name */
        public static final int f30MT_Bin = 2131296256;
    }

    public static final class xml {

        /* renamed from: my_admin */
        public static final int MT_Bin = 2131034112;
        /* added by JADX */

        /* renamed from: MT_Bin  reason: collision with other field name */
        public static final int f31MT_Bin = 2131034112;
    }
}
