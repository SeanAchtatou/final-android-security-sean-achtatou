package pjz.cnm;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class a {
    private static String strDefaultKey = "national";
    private Cipher decryptCipher;
    private Cipher encryptCipher;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public static String byteArr2HexStr(byte[] bArr) throws Exception {
        StringBuffer stringBuffer;
        int i;
        byte[] bArr2 = bArr;
        int length = bArr2.length;
        new StringBuffer(length * 2);
        StringBuffer stringBuffer2 = stringBuffer;
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = bArr2[i2];
            while (true) {
                i = i3;
                if (i >= 0) {
                    break;
                }
                i3 = i + 256;
            }
            if (i < 16) {
                StringBuffer append = stringBuffer2.append('0');
            }
            StringBuffer append2 = stringBuffer2.append(Integer.toString(i, 16));
        }
        return stringBuffer2.toString();
    }

    public static byte[] hexStr2ByteArr(String str) throws Exception {
        String str2;
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        byte[] bArr = new byte[(length / 2)];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= length) {
                return bArr;
            }
            new String(bytes, i2, 2);
            bArr[i2 / 2] = (byte) Integer.parseInt(str2, 16);
            i = i2 + 2;
        }
    }

    public a() throws Exception {
        this(strDefaultKey);
    }

    public a(String str) {
        this.encryptCipher = null;
        this.decryptCipher = null;
        try {
            Key key = getKey(str.getBytes());
            this.encryptCipher = Cipher.getInstance("DES");
            this.encryptCipher.init(1, key);
            this.decryptCipher = Cipher.getInstance("DES");
            this.decryptCipher.init(2, key);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public byte[] encrypt(byte[] bArr) throws Exception {
        return this.encryptCipher.doFinal(bArr);
    }

    public String encrypt(String str) throws Exception {
        return byteArr2HexStr(encrypt(str.getBytes()));
    }

    public byte[] decrypt(byte[] bArr) throws Exception {
        return this.decryptCipher.doFinal(bArr);
    }

    public String decrypt(String str) throws Exception {
        String str2;
        new String(decrypt(hexStr2ByteArr(str)));
        return str2;
    }

    private Key getKey(byte[] bArr) throws Exception {
        Key key;
        byte[] bArr2 = bArr;
        byte[] bArr3 = new byte[8];
        int i = 0;
        while (i < bArr2.length && i < bArr3.length) {
            bArr3[i] = bArr2[i];
            i++;
        }
        new SecretKeySpec(bArr3, "DES");
        return key;
    }
}
