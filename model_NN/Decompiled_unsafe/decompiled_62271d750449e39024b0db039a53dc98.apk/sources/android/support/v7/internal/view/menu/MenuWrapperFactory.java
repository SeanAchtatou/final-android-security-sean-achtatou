package android.support.v7.internal.view.menu;

import android.os.Build;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.internal.view.SupportSubMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

public final class MenuWrapperFactory {
    private MenuWrapperFactory() {
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [android.view.Menu] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.view.Menu createMenuWrapper(android.view.Menu r2) {
        /*
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 14
            if (r0 < r1) goto L_0x000c
            android.support.v7.internal.view.menu.MenuWrapperICS r0 = new android.support.v7.internal.view.menu.MenuWrapperICS
            r0.<init>(r2)
            r2 = r0
        L_0x000c:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.view.menu.MenuWrapperFactory.createMenuWrapper(android.view.Menu):android.view.Menu");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: android.support.v7.internal.view.menu.MenuItemWrapperICS} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: android.view.MenuItem} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: android.support.v7.internal.view.menu.MenuItemWrapperJB} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: android.view.MenuItem} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.view.MenuItem createMenuItemWrapper(android.view.MenuItem r2) {
        /*
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 16
            if (r0 < r1) goto L_0x000d
            android.support.v7.internal.view.menu.MenuItemWrapperJB r0 = new android.support.v7.internal.view.menu.MenuItemWrapperJB
            r0.<init>(r2)
            r2 = r0
        L_0x000c:
            return r2
        L_0x000d:
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 14
            if (r0 < r1) goto L_0x000c
            android.support.v7.internal.view.menu.MenuItemWrapperICS r0 = new android.support.v7.internal.view.menu.MenuItemWrapperICS
            r0.<init>(r2)
            r2 = r0
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.view.menu.MenuWrapperFactory.createMenuItemWrapper(android.view.MenuItem):android.view.MenuItem");
    }

    public static SupportMenu createSupportMenuWrapper(Menu frameworkMenu) {
        if (Build.VERSION.SDK_INT >= 14) {
            return new MenuWrapperICS(frameworkMenu);
        }
        throw new UnsupportedOperationException();
    }

    public static SupportSubMenu createSupportSubMenuWrapper(SubMenu frameworkSubMenu) {
        if (Build.VERSION.SDK_INT >= 14) {
            return new SubMenuWrapperICS(frameworkSubMenu);
        }
        throw new UnsupportedOperationException();
    }

    public static SupportMenuItem createSupportMenuItemWrapper(MenuItem frameworkMenuItem) {
        if (Build.VERSION.SDK_INT >= 16) {
            return new MenuItemWrapperJB(frameworkMenuItem);
        }
        if (Build.VERSION.SDK_INT >= 14) {
            return new MenuItemWrapperICS(frameworkMenuItem);
        }
        throw new UnsupportedOperationException();
    }
}
