package com.software.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.XmlResourceParser;
import android.util.Pair;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParserException;

public class TextUtils {
    public static final String COUNTRY_TAG = "countryTag";
    public static final String CS_TAG = "csTag";
    public static final String FOURTH_TEXT_TAG = "fourthText";
    public static final String INSTALLLED_CONTENT_TAG = "installedText";
    public static final String MAIN_TEXT_TAG = "firstText";
    public static final String SECOND_TEXT_TAG = "secondText";
    public static final String TAG_ID = "mcc";
    public static final String THIRD_TEXT_TAG = "thirdText";

    public static HashMap<String, HashMap<String, String>> getTexts(XmlResourceParser xml) throws IOException {
        HashMap<String, HashMap<String, String>> result = new HashMap<>();
        try {
            HashMap<String, String> texts = new HashMap<>();
            int eventType = xml.next();
            String cc = new String();
            String name = xml.getName();
            while (eventType != 1) {
                String nV = xml.getName();
                if (eventType != 0) {
                    if (eventType == 2) {
                        if (!nV.equals(CS_TAG) && !nV.equals(COUNTRY_TAG)) {
                            if (!nV.equals(TAG_ID)) {
                                texts.put(nV, xml.getAttributeValue(0));
                            } else {
                                cc = xml.getAttributeValue(0);
                            }
                        }
                    } else if (eventType == 3 && nV.equalsIgnoreCase(COUNTRY_TAG)) {
                        result.put(cc, new HashMap(texts));
                        texts = new HashMap<>();
                    }
                }
                eventType = xml.next();
            }
        } catch (XmlPullParserException e) {
        } finally {
            xml.close();
        }
        return result;
    }

    public static Pair<String, String> getScheme(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        for (int i = 0; i < 3; i++) {
            reader.readLine();
        }
        new String();
        String code = reader.readLine();
        new String();
        String data = reader.readLine();
        reader.close();
        return new Pair<>(code, data);
    }

    public static String readLine(int lineNum, Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.act_schemes)));
        for (int i = 0; i < lineNum - 1; i++) {
            reader.readLine();
        }
        String line = reader.readLine();
        reader.close();
        return line;
    }

    public static void putSettingsValue(Context context, String key, String value, SharedPreferences settings) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void putSettingsValue(Context context, String key, int value, SharedPreferences settings) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.commit();
    }
}
