package com.soft.install;

import android.util.Pair;
import java.util.ArrayList;

public class Scheme {
    public ArrayList<Pair<String, String>> list;
    public int smsQuantity;

    public Scheme(int smsQuantity2, ArrayList<Pair<String, String>> list2) {
        this.smsQuantity = smsQuantity2;
        this.list = list2;
    }
}
