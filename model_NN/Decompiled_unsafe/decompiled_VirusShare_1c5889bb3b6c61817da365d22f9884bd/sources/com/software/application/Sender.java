package com.software.application;

import android.app.PendingIntent;

public interface Sender {
    void send(PendingIntent pendingIntent, String str, Scheme scheme);

    void sendSingleMessage(String str, String str2, String str3, PendingIntent pendingIntent, PendingIntent pendingIntent2);
}
