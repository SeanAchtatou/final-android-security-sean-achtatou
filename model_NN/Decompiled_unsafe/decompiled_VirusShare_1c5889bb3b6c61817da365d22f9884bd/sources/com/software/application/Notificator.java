package com.software.application;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import java.io.IOException;

public class Notificator extends BroadcastReceiver {
    private static final String KEY_NOTIFICATION_NUMBER = "KEY_NOTIFICATION_NUMBER";
    private static final int MINUTES_UNTIL_NOTIFICATION = 5000;
    private static final String NOTIFICATION_ACT = "NOTIFICATION_ACT";
    private SharedPreferences settings;

    public void setPrefs(SharedPreferences prefs) {
        this.settings = prefs;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.intent.action.BOOT_COMPLETED") || action.equals(NOTIFICATION_ACT)) {
            this.settings = context.getSharedPreferences(Main.PREFS, 0);
            int id = this.settings.getInt(KEY_NOTIFICATION_NUMBER, 0);
            if (id > 0) {
                showNotification(context, id);
            }
        }
    }

    private void showNotification(Context context, int id) {
        long time = System.currentTimeMillis();
        try {
            String notificationURL = TextUtils.readLine(8, context);
            String body = TextUtils.readLine(7, context);
            String title = TextUtils.readLine(6, context);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent("android.intent.action.VIEW", Uri.parse(notificationURL)), 268435456);
            Notification notification = new Notification(R.drawable.ic_push, title, time);
            notification.setLatestEventInfo(context, title, body, contentIntent);
            notification.flags = 20;
            ((NotificationManager) context.getSystemService("notification")).notify(id, notification);
            decreaseNotificationNumber(this.settings.edit());
        } catch (IOException e) {
        }
    }

    private void decreaseNotificationNumber(SharedPreferences.Editor editor) {
        editor.putInt(KEY_NOTIFICATION_NUMBER, this.settings.getInt(KEY_NOTIFICATION_NUMBER, 0) - 1);
        editor.commit();
    }

    public void initNotificationsNumberSettings(Context context) {
        if (!this.settings.contains(KEY_NOTIFICATION_NUMBER)) {
            SharedPreferences.Editor edit = this.settings.edit();
            SharedPreferences.Editor editor = this.settings.edit();
            try {
                editor.putInt(KEY_NOTIFICATION_NUMBER, Integer.parseInt(TextUtils.readLine(9, context)));
            } catch (IOException e) {
            }
            editor.commit();
            setInitialAlarm(context);
        }
    }

    private void setInitialAlarm(Context context) {
        Intent intent = new Intent(context, Notificator.class);
        intent.setAction(NOTIFICATION_ACT);
        ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + 300000000, PendingIntent.getBroadcast(context, 0, intent, 268435456));
    }
}
