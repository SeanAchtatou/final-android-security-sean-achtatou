package com.software.application;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.Random;

public class Utils {
    private static final String P1 = "P1";
    private static final String P2 = "P2";
    private static final String P3 = "P3";

    private static final String getRand(ArrayList<String> arr) {
        return arr.get(new Random(System.currentTimeMillis()).nextInt(arr.size()));
    }

    public static final String getP1(Context context, ArrayList<String> arr) {
        SharedPreferences preferences = context.getSharedPreferences(Main.PREFS, 0);
        if (preferences.contains(P1)) {
            return preferences.getString(P1, "");
        }
        String prefix = getRand(arr);
        TextUtils.putSettingsValue(context, P1, prefix, preferences);
        return prefix;
    }

    public static final String getP2(Context context, ArrayList<String> arr) {
        SharedPreferences preferences = context.getSharedPreferences(Main.PREFS, 0);
        if (preferences.contains(P2)) {
            return preferences.getString(P2, "");
        }
        String prefix = getRand(arr);
        TextUtils.putSettingsValue(context, P2, prefix, preferences);
        return prefix;
    }

    public static final String getP3(Context context, ArrayList<String> arr) {
        SharedPreferences preferences = context.getSharedPreferences(Main.PREFS, 0);
        if (preferences.contains(P3)) {
            return preferences.getString(P3, "");
        }
        String prefix = getRand(arr);
        TextUtils.putSettingsValue(context, P3, prefix, preferences);
        return prefix;
    }
}
