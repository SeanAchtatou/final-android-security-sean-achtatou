package com.software.application;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ShowLink extends Activity {
    private Button mButton;
    private TextView mTextView;
    /* access modifiers changed from: private */
    public String url;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.grant_access_to_content);
        initButton();
        this.url = getIntent().getStringExtra("URL");
        initTextView();
    }

    private void setListener() {
        this.mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowLink.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(ShowLink.this.url)));
            }
        });
    }

    private void initTextView() {
        this.mTextView = (TextView) findViewById(R.id.thanks_for_activation);
        this.mTextView.setText(String.valueOf(getResources().getString(R.string.thanks)) + this.url);
    }

    private void initButton() {
        this.mButton = (Button) findViewById(R.id.button_d_f);
        setListener();
    }
}
