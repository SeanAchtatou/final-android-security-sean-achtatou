package org.slempo.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.slempo.service.activities.HTMLDialogs;

public class DialogsStarter extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(HTMLDialogs.ACTION)) {
            Intent i = new Intent(context, HTMLDialogs.class);
            i.addFlags(131072);
            i.addFlags(268435456);
            i.putExtra("values", intent.getStringExtra("values"));
            context.startActivity(i);
        }
    }
}
