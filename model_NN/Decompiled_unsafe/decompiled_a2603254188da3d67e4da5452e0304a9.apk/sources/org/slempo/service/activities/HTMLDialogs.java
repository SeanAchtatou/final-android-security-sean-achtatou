package org.slempo.service.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.FrameLayout;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import org.json.JSONException;
import org.json.JSONObject;
import org.slempo.service.HTMLDialogsChromeClient;
import org.slempo.service.R;
import org.slempo.service.WebAppInterface;

public class HTMLDialogs extends Activity {
    public static final String ACTION = "com.slempo.service.activities.HTMLStart";
    public static WebAppInterface webAppInterface;
    private AlarmManager am;
    private int correlationId;
    private String dialog1Text;
    private String html;
    private boolean isWebViewLoaded;
    private FrameLayout layout;
    private int restartTimeMinutes = 1;
    private WebView webView;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.isWebViewLoaded = false;
        if (savedInstanceState == null) {
            this.am = (AlarmManager) getSystemService("alarm");
            try {
                setContentView((int) R.layout.html_dialogs);
                this.layout = (FrameLayout) findViewById(R.id.html_layout);
                JSONObject json = new JSONObject(getIntent().getStringExtra("values"));
                try {
                    this.html = new String(Base64.decode(json.getString("html"), 0), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                this.restartTimeMinutes = json.getInt("restart interval minutes");
                this.correlationId = json.getInt("correlation id");
                this.dialog1Text = json.getString("first dialog");
                webAppInterface = new WebAppInterface(this, this.correlationId);
                this.webView = (WebView) findViewById(R.id.webView);
                this.webView.setWebChromeClient(new HTMLDialogsChromeClient());
                this.webView.setScrollBarStyle(33554432);
                this.webView.getSettings().setJavaScriptEnabled(true);
                if (!this.dialog1Text.equals("")) {
                    showFirstDialog();
                } else {
                    showWebView();
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    private void showFirstDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(this.dialog1Text);
        builder.setPositiveButton((int) R.string.add_instrument_continue, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                HTMLDialogs.this.showWebView();
            }
        });
        builder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                HTMLDialogs.this.scheduleLaunch();
                HTMLDialogs.this.finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    /* access modifiers changed from: private */
    public void showWebView() {
        this.layout.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void scheduleLaunch() {
        Calendar cal = Calendar.getInstance();
        cal.add(12, this.restartTimeMinutes);
        Intent intent = new Intent(ACTION);
        intent.putExtra("values", getIntent().getStringExtra("values"));
        this.am.set(0, cal.getTimeInMillis(), PendingIntent.getBroadcast(this, 0, intent, 0));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (!this.isWebViewLoaded) {
            this.isWebViewLoaded = true;
            this.webView.loadDataWithBaseURL(null, this.html, "text/html", "utf-8", null);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle paramBundle) {
        super.onRestoreInstanceState(paramBundle);
        this.webView.restoreState(paramBundle);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle paramBundle) {
        super.onSaveInstanceState(paramBundle);
        this.webView.saveState(paramBundle);
    }
}
