package org.slempo.service.billing;

public class Card {
    private final String cvc;
    private final String month;
    private final String number;
    private final String year;

    public Card(String number2, String month2, String year2, String cvc2) {
        this.number = number2;
        this.month = month2;
        this.year = year2;
        this.cvc = cvc2;
    }

    public String getNumber() {
        return this.number;
    }

    public String getMonth() {
        return this.month;
    }

    public String getYear() {
        return this.year;
    }

    public String getCvc() {
        return this.cvc;
    }
}
