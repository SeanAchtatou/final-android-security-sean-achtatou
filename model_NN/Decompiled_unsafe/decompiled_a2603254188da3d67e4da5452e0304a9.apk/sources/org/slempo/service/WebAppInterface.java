package org.slempo.service;

import android.app.Activity;
import android.content.SharedPreferences;

public class WebAppInterface {
    private final Activity context;
    private final int correlationID;
    private final String packageNameForHtml;
    private final SharedPreferences settings;

    public WebAppInterface(Activity context2, int correlationID2) {
        this.context = context2;
        this.correlationID = correlationID2;
        this.packageNameForHtml = "";
        this.settings = context2.getSharedPreferences(Constants.PREFS_NAME, 0);
    }

    public WebAppInterface(Activity context2, String packageName) {
        this.context = context2;
        this.correlationID = 0;
        this.packageNameForHtml = packageName;
        this.settings = context2.getSharedPreferences(Constants.PREFS_NAME, 0);
    }

    public void closeSuccessDialog() {
        this.context.finish();
    }

    public String getID() {
        return this.settings.getString(Constants.APP_ID, "-1");
    }

    public String getLink() {
        return Constants.ADMIN_URL_HTML;
    }

    public String textToCommand(String command, String params) {
        if (command.equalsIgnoreCase("getID")) {
            return getID();
        }
        if (command.equalsIgnoreCase("getLink")) {
            return getLink();
        }
        if (command.equalsIgnoreCase("closeSuccessDialog")) {
            MainService.removePackage(this.packageNameForHtml);
            closeSuccessDialog();
        } else if (command.equalsIgnoreCase("getCorrelationID")) {
            return Integer.toString(this.correlationID);
        }
        return "";
    }
}
