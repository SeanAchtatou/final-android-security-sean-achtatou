package android.support.v7.app;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class ActionBarActivity extends FragmentActivity implements ActionBar.Callback, TaskStackBuilder.SupportParentable, ActionBarDrawerToggle.DelegateProvider {
    ActionBarActivityDelegate mImpl;

    public ActionBar getSupportActionBar() {
        return this.mImpl.getSupportActionBar();
    }

    public MenuInflater getMenuInflater() {
        return this.mImpl.getMenuInflater();
    }

    public void setContentView(int layoutResID) {
        this.mImpl.setContentView(layoutResID);
    }

    public void setContentView(View view) {
        this.mImpl.setContentView(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams params) {
        this.mImpl.setContentView(view, params);
    }

    public void addContentView(View view, ViewGroup.LayoutParams params) {
        this.mImpl.addContentView(view, params);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        this.mImpl = ActionBarActivityDelegate.createDelegate(this);
        ActionBarActivity.super.onCreate(savedInstanceState);
        this.mImpl.onCreate(savedInstanceState);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        ActionBarActivity.super.onConfigurationChanged(newConfig);
        this.mImpl.onConfigurationChanged(newConfig);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        ActionBarActivity.super.onStop();
        this.mImpl.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        ActionBarActivity.super.onPostResume();
        this.mImpl.onPostResume();
    }

    public View onCreatePanelView(int featureId) {
        if (featureId == 0) {
            return this.mImpl.onCreatePanelView(featureId);
        }
        return ActionBarActivity.super.onCreatePanelView(featureId);
    }

    public final boolean onMenuItemSelected(int featureId, MenuItem item) {
        if (this.mImpl.onMenuItemSelected(featureId, item)) {
            return true;
        }
        ActionBar ab = getSupportActionBar();
        if (item.getItemId() != 16908332 || ab == null || (ab.getDisplayOptions() & 4) == 0) {
            return false;
        }
        return onSupportNavigateUp();
    }

    /* access modifiers changed from: protected */
    public void onTitleChanged(CharSequence title, int color) {
        ActionBarActivity.super.onTitleChanged(title, color);
        this.mImpl.onTitleChanged(title);
    }

    public boolean supportRequestWindowFeature(int featureId) {
        return this.mImpl.supportRequestWindowFeature(featureId);
    }

    public void supportInvalidateOptionsMenu() {
        if (Build.VERSION.SDK_INT >= 14) {
            ActionBarActivity.super.supportInvalidateOptionsMenu();
        }
        this.mImpl.supportInvalidateOptionsMenu();
    }

    public void onSupportActionModeStarted(ActionMode mode) {
    }

    public void onSupportActionModeFinished(ActionMode mode) {
    }

    public ActionMode startSupportActionMode(ActionMode.Callback callback) {
        return this.mImpl.startSupportActionMode(callback);
    }

    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        return this.mImpl.onCreatePanelMenu(featureId, menu);
    }

    public boolean onPreparePanel(int featureId, View view, Menu menu) {
        return this.mImpl.onPreparePanel(featureId, view, menu);
    }

    /* access modifiers changed from: protected */
    public boolean onPrepareOptionsPanel(View view, Menu menu) {
        return this.mImpl.onPrepareOptionsPanel(view, menu);
    }

    /* access modifiers changed from: package-private */
    public void superSetContentView(int resId) {
        ActionBarActivity.super.setContentView(resId);
    }

    /* access modifiers changed from: package-private */
    public void superSetContentView(View v) {
        ActionBarActivity.super.setContentView(v);
    }

    /* access modifiers changed from: package-private */
    public void superSetContentView(View v, ViewGroup.LayoutParams lp) {
        ActionBarActivity.super.setContentView(v, lp);
    }

    /* access modifiers changed from: package-private */
    public void superAddContentView(View v, ViewGroup.LayoutParams lp) {
        ActionBarActivity.super.addContentView(v, lp);
    }

    /* access modifiers changed from: package-private */
    public boolean superOnCreatePanelMenu(int featureId, Menu frameworkMenu) {
        return ActionBarActivity.super.onCreatePanelMenu(featureId, frameworkMenu);
    }

    /* access modifiers changed from: package-private */
    public boolean superOnPreparePanel(int featureId, View view, Menu menu) {
        return ActionBarActivity.super.onPreparePanel(featureId, view, menu);
    }

    /* access modifiers changed from: package-private */
    public boolean superOnPrepareOptionsPanel(View view, Menu menu) {
        return ActionBarActivity.super.onPrepareOptionsPanel(view, menu);
    }

    /* access modifiers changed from: package-private */
    public boolean superOnMenuItemSelected(int featureId, MenuItem menuItem) {
        return ActionBarActivity.super.onMenuItemSelected(featureId, menuItem);
    }

    public void onBackPressed() {
        if (!this.mImpl.onBackPressed()) {
            ActionBarActivity.super.onBackPressed();
        }
    }

    public void setSupportProgressBarVisibility(boolean visible) {
        this.mImpl.setSupportProgressBarVisibility(visible);
    }

    public void setSupportProgressBarIndeterminateVisibility(boolean visible) {
        this.mImpl.setSupportProgressBarIndeterminateVisibility(visible);
    }

    public void setSupportProgressBarIndeterminate(boolean indeterminate) {
        this.mImpl.setSupportProgressBarIndeterminate(indeterminate);
    }

    public void setSupportProgress(int progress) {
        this.mImpl.setSupportProgress(progress);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [android.support.v7.app.ActionBarActivity, android.app.Activity] */
    public void onCreateSupportNavigateUpTaskStack(TaskStackBuilder builder) {
        builder.addParentStack((Activity) this);
    }

    public void onPrepareSupportNavigateUpTaskStack(TaskStackBuilder builder) {
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [android.content.Context, android.support.v7.app.ActionBarActivity, android.app.Activity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean onSupportNavigateUp() {
        /*
            r4 = this;
            android.content.Intent r2 = r4.getSupportParentActivityIntent()
            if (r2 == 0) goto L_0x0027
            boolean r3 = r4.supportShouldUpRecreateTask(r2)
            if (r3 == 0) goto L_0x0023
            android.support.v4.app.TaskStackBuilder r0 = android.support.v4.app.TaskStackBuilder.create(r4)
            r4.onCreateSupportNavigateUpTaskStack(r0)
            r4.onPrepareSupportNavigateUpTaskStack(r0)
            r0.startActivities()
            android.support.v4.app.ActivityCompat.finishAffinity(r4)     // Catch:{ IllegalStateException -> 0x001e }
        L_0x001c:
            r3 = 1
        L_0x001d:
            return r3
        L_0x001e:
            r1 = move-exception
            r4.finish()
            goto L_0x001c
        L_0x0023:
            r4.supportNavigateUpTo(r2)
            goto L_0x001c
        L_0x0027:
            r3 = 0
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.ActionBarActivity.onSupportNavigateUp():boolean");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.app.ActionBarActivity, android.app.Activity] */
    public Intent getSupportParentActivityIntent() {
        return NavUtils.getParentActivityIntent((Activity) this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.app.ActionBarActivity, android.app.Activity] */
    public boolean supportShouldUpRecreateTask(Intent targetIntent) {
        return NavUtils.shouldUpRecreateTask((Activity) this, targetIntent);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [android.support.v7.app.ActionBarActivity, android.app.Activity] */
    public void supportNavigateUpTo(Intent upIntent) {
        NavUtils.navigateUpTo((Activity) this, upIntent);
    }

    public final ActionBarDrawerToggle.Delegate getDrawerToggleDelegate() {
        return this.mImpl.getDrawerToggleDelegate();
    }

    public final void onContentChanged() {
        this.mImpl.onContentChanged();
    }

    public void onSupportContentChanged() {
    }
}
