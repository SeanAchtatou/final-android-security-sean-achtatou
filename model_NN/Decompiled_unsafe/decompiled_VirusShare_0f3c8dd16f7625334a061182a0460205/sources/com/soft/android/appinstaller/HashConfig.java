package com.soft.android.appinstaller;

import java.util.Hashtable;

public class HashConfig {
    private Hashtable config = null;

    private void parseConfigLine(String s) {
        int div;
        if (!s.startsWith("#") && (div = s.indexOf(32)) > 0) {
            this.config.put(s.substring(0, div), s.substring(div + 1));
        }
    }

    public void init(String s) {
        if (this.config == null) {
            this.config = new Hashtable();
        } else {
            this.config.clear();
        }
        int div = 0;
        int dip = s.indexOf(10);
        while (dip != -1 && div < s.length()) {
            dip = s.indexOf(10, div);
            String p = s.substring(div, dip);
            if (p != null) {
                parseConfigLine(p);
            }
            div = dip + 1;
        }
    }

    public Object get(Object key) {
        if (this.config == null) {
            return null;
        }
        return this.config.get(key);
    }

    public void put(String key, String value) {
        if (this.config != null) {
            this.config.put(key, value);
        }
    }

    public void remove(Object key) {
        this.config.remove(key);
    }

    public String getParam(String paramName, String defaultValue) {
        String value = (String) get(paramName);
        if (value == null) {
            return defaultValue;
        }
        return value;
    }
}
