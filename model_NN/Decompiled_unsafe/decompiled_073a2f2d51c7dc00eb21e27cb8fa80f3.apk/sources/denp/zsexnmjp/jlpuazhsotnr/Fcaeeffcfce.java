package denp.zsexnmjp.jlpuazhsotnr;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import java.lang.reflect.Method;

public final class Fcaeeffcfce extends Activity {
    private static boolean a = false;
    private static Fcaeeffcfce b;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a = true;
        b = this;
        d();
    }

    private void d() {
        if (Build.VERSION.SDK_INT >= 23) {
            String[] e = e();
            if (e.length > 0) {
                requestPermissions(e, 1);
            }
        }
    }

    private static String[] e() {
        int i;
        int i2;
        int length = k.l.length;
        String[] strArr = new String[length];
        try {
            Method method = Beabefaf.class.getMethod(k.a(1048845), g.b(839141));
            k.p.invoke(method, true);
            int i3 = 0;
            int i4 = 0;
            while (i3 < length) {
                if (((Integer) method.invoke(Beabefaf.a(), k.l[i3])).intValue() != 0) {
                    strArr[i3] = k.l[i3];
                    i2 = i4 + 1;
                } else {
                    i2 = i4;
                }
                i3++;
                i4 = i2;
            }
            if (i4 > 0) {
                String[] strArr2 = new String[i4];
                int i5 = 0;
                int i6 = 0;
                while (i6 < strArr.length) {
                    if (strArr[i6] != null) {
                        i = i5 + 1;
                        strArr2[i5] = strArr[i6];
                    } else {
                        i = i5;
                    }
                    i6++;
                    i5 = i;
                }
                return strArr2;
            }
        } catch (Throwable th) {
        }
        return new String[0];
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0012  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onRequestPermissionsResult(int r5, java.lang.String[] r6, int[] r7) {
        /*
            r4 = this;
            r2 = 1
            r1 = 0
            if (r5 != r2) goto L_0x001b
            r0 = r1
        L_0x0005:
            int r3 = r6.length
            if (r0 >= r3) goto L_0x001b
            r3 = r7[r0]
            if (r3 == 0) goto L_0x0018
            r4.d()
            r0 = r1
        L_0x0010:
            if (r0 == 0) goto L_0x0017
            denp.zsexnmjp.jlpuazhsotnr.Fcaeeffcfce.a = r1
            r4.finish()
        L_0x0017:
            return
        L_0x0018:
            int r0 = r0 + 1
            goto L_0x0005
        L_0x001b:
            r0 = r2
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: denp.zsexnmjp.jlpuazhsotnr.Fcaeeffcfce.onRequestPermissionsResult(int, java.lang.String[], int[]):void");
    }

    public void onDestroy() {
        super.onDestroy();
        a = false;
        if (b != null) {
            b = null;
        }
    }

    static boolean a() {
        return a;
    }

    static void b() {
        try {
            if (b != null) {
                b.finish();
                b = null;
            }
        } catch (Throwable th) {
        }
    }

    static boolean c() {
        return e().length == 0;
    }
}
