package javax.microedition.enhance;

import android.graphics.Paint;
import android.graphics.Rect;
import com.a.a.e.c;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.meteoroid.core.k;

public final class MIDPHelper {
    public static final int HORIZONTAL = 1;
    private static final String LOG_TAG = "MIDPHelper";
    public static final int MOVEDOWN = 2;
    public static final int MOVELEFT = 3;
    public static final int MOVERIGHT = 4;
    public static final int MOVEUP = 0;
    public static final int NOMOVE = -1;
    public static final int VERTICAL = 0;
    private static final String[] bS = {".png", ".mp3", ".jpg", ".jpeg", ".mpeg", ".bmp"};
    public static Paint bT = new Paint();
    public static Rect rect = new Rect();

    public static InputStream j(String str) {
        String str2;
        if (str == null) {
            throw new IOException("Can't load resource noname.");
        }
        "Load assert " + str + " .";
        while (str.startsWith(File.separator)) {
            str = str.substring(1);
        }
        String[] split = str.split("\\.");
        String K = c.K(split[0]);
        String L = c.L(split[0]);
        if (split.length == 1) {
            str2 = "a_b";
        } else if (split.length == 2) {
            str2 = split[1];
        } else {
            str2 = "";
            for (int i = 1; i < split.length; i++) {
                str2 = str2 + split[i];
            }
        }
        String str3 = (L.length() == 0 || L.charAt(0) == '_') ? "b_a" + L : L;
        int i2 = 0;
        while (true) {
            if (i2 >= bS.length) {
                break;
            } else if (str2.equalsIgnoreCase(bS[i2])) {
                str2 = str2.toLowerCase();
                break;
            } else {
                i2++;
            }
        }
        String str4 = K + str3 + "." + str2;
        "Load assert " + str4 + " .";
        return k.t(c.E(str4));
    }
}
