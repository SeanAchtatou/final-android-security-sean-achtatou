package javax.microedition.media;

import java.io.InputStream;
import org.meteoroid.core.g;
import org.meteoroid.plugin.device.MIDPDevice;

public class Manager {
    public static final String TONE_DEVICE_LOCATOR = "device://tone";
    public static final String[] cA = {"audio/x-wav", "audio/basic", "audio/mpeg", "audio/midi", "audio/x-tone-seq", "audio/amr"};
    public static final String[] cB = {"http://"};

    public static Player a(InputStream inputStream, String str) {
        return new MIDPDevice.e(g.a(null, inputStream, str));
    }
}
