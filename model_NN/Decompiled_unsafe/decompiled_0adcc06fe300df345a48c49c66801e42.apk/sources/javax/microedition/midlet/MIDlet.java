package javax.microedition.midlet;

import android.os.Message;
import org.meteoroid.core.a;
import org.meteoroid.core.h;
import org.meteoroid.plugin.device.MIDPDevice;

public abstract class MIDlet implements a.C0003a, h.a {
    public static final int MIDLET_PLATFORM_REQUEST = -2023686143;
    public static final int MIDLET_PLATFORM_REQUEST_FINISH = -2023686142;
    private boolean cC;
    private int cD;

    protected MIDlet() {
    }

    public final void C() {
        getClass().getSimpleName();
        h.o(MIDPDevice.MSG_MIDP_MIDLET_NOTIFYDESTROYED);
    }

    public final void D() {
        this.cD = 0;
        h.a(this);
        a.start();
    }

    public final boolean a(Message message) {
        if (message.what == -2023686142) {
            if (message.obj != null) {
                this.cC = ((Boolean) message.obj).booleanValue();
            }
            return true;
        } else if (message.what == 47623) {
            this.cD = 2;
            return true;
        } else if (message.what != 47622) {
            return false;
        } else {
            onStart();
            return true;
        }
    }

    public final int getState() {
        return this.cD;
    }

    public final void onDestroy() {
        this.cD = 3;
    }

    public final void onPause() {
        this.cD = 2;
    }

    public final void onResume() {
        this.cD = 1;
    }

    public final void onStart() {
        this.cD = 1;
    }
}
