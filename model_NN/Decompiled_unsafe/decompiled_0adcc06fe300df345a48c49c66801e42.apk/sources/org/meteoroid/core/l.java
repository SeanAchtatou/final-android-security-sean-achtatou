package org.meteoroid.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import org.meteoroid.core.h;

public final class l {
    public static final String LOG_TAG = "ViewManager";
    public static final int MSG_VIEW_CHANGED = 23041;
    private static final int ROOT_VIEW_ID = 792998026;
    /* access modifiers changed from: private */
    public static ProgressDialog cS;
    /* access modifiers changed from: private */
    public static a ev;
    /* access modifiers changed from: private */
    public static a ew;
    /* access modifiers changed from: private */
    public static FrameLayout ex;
    public static boolean ey = false;
    /* access modifiers changed from: private */
    public static AlertDialog ez;

    public interface a {
        View getView();

        void n();

        boolean o();

        void v();
    }

    public static void a(long j) {
        k.getHandler().postDelayed(new Runnable() {
            public final void run() {
                if (l.ez != null && l.ez.isShowing()) {
                    l.ez.dismiss();
                }
            }
        }, j);
    }

    protected static void a(Activity activity) {
        h.b((int) MSG_VIEW_CHANGED, "MSG_VIEW_CHANGED");
        FrameLayout frameLayout = new FrameLayout(activity);
        ex = frameLayout;
        frameLayout.setBackgroundColor(-16777216);
        ex.setId(ROOT_VIEW_ID);
        ex.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        ex.requestFocus();
        h.a(new h.a() {
            public final boolean a(Message message) {
                if (message.what == 47873) {
                    if (l.ev == null) {
                        return false;
                    }
                    l.ev.v();
                    return false;
                } else if (message.what != 47874 || l.ev == null) {
                    return false;
                } else {
                    l.ev.n();
                    return false;
                }
            }
        });
    }

    public static void a(String str, String str2, View view, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, null, view, null, null, null, null, null, null, null, null, false, null);
    }

    private static void a(String str, String str2, View view, String[] strArr, DialogInterface.OnClickListener onClickListener, String str3, DialogInterface.OnClickListener onClickListener2, String str4, DialogInterface.OnClickListener onClickListener3, String str5, DialogInterface.OnClickListener onClickListener4, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(k.getActivity());
        if (str != null) {
            builder.setTitle(str);
        }
        if (str2 != null) {
            builder.setMessage(str2);
        }
        if (view != null) {
            builder.setView(view);
        }
        if (str3 != null) {
            builder.setPositiveButton(str3, onClickListener2);
        }
        if (str4 != null) {
            builder.setNegativeButton(str4, onClickListener3);
        }
        if (str5 != null) {
            builder.setNeutralButton(str5, onClickListener4);
        }
        builder.setCancelable(z);
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        if (builder != null) {
            k.getHandler().post(new Runnable() {
                public final void run() {
                    AlertDialog unused = l.ez = builder.create();
                    l.ez.show();
                }
            });
        }
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener) {
        a(str, str2, str3, onClickListener, null, null, null, null, false, null);
    }

    private static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, String str5, DialogInterface.OnClickListener onClickListener3, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, str2, null, null, null, str3, onClickListener, str4, onClickListener2, null, null, z, onCancelListener);
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, str2, str3, onClickListener, str4, onClickListener2, null, null, true, onCancelListener);
    }

    public static void a(String str, final String str2, boolean z, boolean z2) {
        k.getHandler().post(new Runnable(null, false, true) {
            public final void run() {
                if (l.cS == null) {
                    ProgressDialog unused = l.cS = new ProgressDialog(k.getActivity());
                }
                if (null != null) {
                    l.cS.setTitle(null);
                }
                if (str2 != null) {
                    l.cS.setMessage(str2);
                }
                l.cS.setCancelable(false);
                l.cS.setIndeterminate(true);
                if (!l.cS.isShowing()) {
                    l.cS.show();
                }
            }
        });
    }

    public static void a(final a aVar) {
        if (aVar != null && aVar != ev && aVar.getView() != null) {
            k.getHandler().post(new Runnable() {
                public final void run() {
                    if (l.ex.getParent() == null) {
                        k.getActivity().setContentView(l.ex);
                    }
                    if (!(l.ev == null || l.ev.getView() == null)) {
                        l.ex.removeView(l.ev.getView());
                    }
                    if (l.ev != null) {
                        l.ev.v();
                        a unused = l.ew = l.ev;
                    }
                    l.ex.addView(aVar.getView());
                    if (aVar != null) {
                        aVar.n();
                    }
                    a unused2 = l.ev = aVar;
                    l.ex.invalidate();
                    l.ev.getView().requestFocus();
                    h.b((int) l.MSG_VIEW_CHANGED, l.ev);
                }
            });
        }
    }

    public static void aA() {
        k.getHandler().post(new Runnable() {
            public final void run() {
                if (l.ez != null && l.ez.isShowing()) {
                    l.ez.dismiss();
                }
            }
        });
    }

    public static void az() {
        k.getHandler().post(new Runnable() {
            public final void run() {
                if (l.cS != null && l.cS.isShowing()) {
                    l.cS.dismiss();
                }
            }
        });
    }

    public static void b(a aVar) {
        ew = ev;
        ev = aVar;
    }

    protected static boolean o() {
        if (ev != null) {
            return ev.o() || ey;
        }
        return false;
    }

    protected static void onDestroy() {
        ew = null;
        ev = null;
    }
}
