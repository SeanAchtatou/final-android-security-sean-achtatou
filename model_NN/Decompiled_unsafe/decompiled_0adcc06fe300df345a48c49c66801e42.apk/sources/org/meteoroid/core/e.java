package org.meteoroid.core;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class e implements SurfaceHolder.Callback {
    public static final String LOG_TAG = "GraphicsManager";
    private static final ConcurrentLinkedQueue<a> dA = new ConcurrentLinkedQueue<>();
    public static SurfaceView du;
    public static final e dv = new e();
    private static boolean dw = false;
    private static SurfaceHolder dx;
    private static final Rect dy = new Rect(-1, -1, -1, -1);
    private boolean dz;

    public interface a {
        void onDraw(Canvas canvas);
    }

    protected static final void V() {
        if (!dv.dz) {
            synchronized (du) {
                try {
                    du.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        if (dx == null) {
            dx = du.getHolder();
        }
        synchronized (dx) {
            Canvas lockCanvas = dx.lockCanvas();
            if (lockCanvas != null) {
                lockCanvas.drawColor(-16777216);
                Iterator<a> it = dA.iterator();
                while (it.hasNext()) {
                    it.next().onDraw(lockCanvas);
                }
                dx.unlockCanvasAndPost(lockCanvas);
            }
        }
        return;
    }

    protected static void a(Activity activity) {
        SurfaceView surfaceView = new SurfaceView(activity);
        du = surfaceView;
        surfaceView.setId(268049792);
        du.setFocusable(true);
        du.setFocusableInTouchMode(true);
        du.setLongClickable(true);
        du.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        dx = du.getHolder();
        f.a(du);
        dx.addCallback(dv);
        try {
            dx.setType(1);
        } catch (Exception e) {
            try {
                dx.setType(2);
            } catch (Exception e2) {
                dx.setType(0);
            }
        }
    }

    public static final void a(a aVar) {
        dA.add(aVar);
    }

    public static Bitmap b(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        try {
            return BitmapFactory.decodeStream(inputStream);
        } catch (Exception e) {
            Log.w(LOG_TAG, "createBitmap with InputStream error." + e);
            throw new IOException();
        }
    }

    public static final void b(a aVar) {
        dA.remove(aVar);
    }

    protected static void onDestroy() {
        dv.dz = false;
        dx.removeCallback(dv);
        f.b(du);
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        dv.dz = true;
        synchronized (du) {
            du.notifyAll();
        }
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        dv.dz = true;
        synchronized (du) {
            du.notifyAll();
        }
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        dv.dz = false;
    }
}
