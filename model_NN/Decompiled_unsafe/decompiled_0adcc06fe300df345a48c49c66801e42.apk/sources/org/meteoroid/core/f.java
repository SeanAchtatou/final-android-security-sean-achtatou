package org.meteoroid.core;

import android.app.Activity;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.util.Iterator;
import java.util.LinkedHashSet;
import org.meteoroid.core.InputManager;

public final class f extends GestureDetector.SimpleOnGestureListener implements View.OnKeyListener, View.OnTouchListener {
    public static final String LOG_TAG = "InputManager";
    private static final f dB = new f();
    private static final LinkedHashSet<a> dE = new LinkedHashSet<>();
    private static final LinkedHashSet<b> dF = new LinkedHashSet<>();
    private static final LinkedHashSet<e> dG = new LinkedHashSet<>();
    /* access modifiers changed from: private */
    public static final LinkedHashSet<d> dH = new LinkedHashSet<>();
    /* access modifiers changed from: private */
    public static final LinkedHashSet<InputManager.RawMotionEventListener> dI = new LinkedHashSet<>();
    /* access modifiers changed from: private */
    public GestureDetector dC;
    private SensorManager dD;

    public interface a {
        boolean a(KeyEvent keyEvent);
    }

    public interface b {
        boolean d(int i, int i2, int i3, int i4);
    }

    private final class c implements View.OnTouchListener {
        private c() {
        }

        /* synthetic */ c(f fVar, byte b) {
            this();
        }

        public final boolean onTouch(View view, MotionEvent motionEvent) {
            boolean z = false;
            SystemClock.sleep(34);
            if (!f.dH.isEmpty()) {
                f.this.dC.onTouchEvent(motionEvent);
            }
            if (!f.dI.isEmpty()) {
                Iterator it = f.dI.iterator();
                while (it.hasNext()) {
                    it.next();
                }
            }
            int action = motionEvent.getAction();
            int i = action & 255;
            int i2 = action >> 8;
            if (i == 6) {
                return f.d(1, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
            }
            if (i == 5) {
                return f.d(0, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
            }
            int pointerCount = motionEvent.getPointerCount();
            for (int i3 = 0; i3 < pointerCount; i3++) {
                if (f.d(action, (int) motionEvent.getX(i3), (int) motionEvent.getY(i3), i3)) {
                    z = true;
                }
            }
            return z;
        }
    }

    public interface d {
        public static final int GESTURE_SLIDE_DOWN = 2;
        public static final int GESTURE_SLIDE_LEFT = 3;
        public static final int GESTURE_SLIDE_RIGHT = 4;
        public static final int GESTURE_SLIDE_UP = 1;

        boolean Y();
    }

    public interface e {
    }

    protected static void a(Activity activity) {
        dB.dC = new GestureDetector(activity, dB);
        dB.dD = (SensorManager) activity.getSystemService("sensor");
    }

    public static final void a(SensorEventListener sensorEventListener) {
        dB.dD.registerListener(sensorEventListener, dB.dD.getDefaultSensor(1), 3);
    }

    protected static void a(View view) {
        View.OnTouchListener onTouchListener;
        if (!l.ey) {
            view.setOnKeyListener(dB);
        }
        if (k.at() >= 5) {
            f fVar = dB;
            fVar.getClass();
            onTouchListener = new c(fVar, (byte) 0);
        } else {
            onTouchListener = dB;
        }
        view.setOnTouchListener(onTouchListener);
    }

    public static final void a(a aVar) {
        dE.add(aVar);
    }

    public static final void a(b bVar) {
        dF.add(bVar);
    }

    public static final void a(e eVar) {
        dG.add(eVar);
    }

    public static final void b(SensorEventListener sensorEventListener) {
        dB.dD.unregisterListener(sensorEventListener);
    }

    protected static void b(View view) {
        view.setOnKeyListener(null);
        view.setOnTouchListener(null);
    }

    public static final void b(b bVar) {
        dF.remove(bVar);
    }

    public static final boolean d(int i, int i2, int i3, int i4) {
        Iterator<b> it = dF.iterator();
        while (it.hasNext()) {
            if (it.next().d(i, i2, i3, i4)) {
                return true;
            }
        }
        return false;
    }

    protected static void onDestroy() {
        dH.clear();
        dF.clear();
        dG.clear();
        dE.clear();
    }

    public static boolean onTrackballEvent(MotionEvent motionEvent) {
        if (!dG.isEmpty()) {
            Iterator<e> it = dG.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
        return false;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        Iterator<d> it = dH.iterator();
        while (it.hasNext()) {
            if (it.next().Y()) {
                return true;
            }
        }
        return false;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (!dE.isEmpty()) {
            Iterator<a> it = dE.iterator();
            while (it.hasNext()) {
                it.next().a(keyEvent);
            }
        }
        return false;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        SystemClock.sleep(34);
        if (!dH.isEmpty()) {
            this.dC.onTouchEvent(motionEvent);
        }
        if (!dI.isEmpty()) {
            Iterator<InputManager.RawMotionEventListener> it = dI.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
        return d(motionEvent.getAction(), (int) motionEvent.getX(), (int) motionEvent.getY(), 0);
    }
}
