package org.meteoroid.core;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import com.a.a.b.a;

public class MeteoroidActivity extends Activity {
    public static final String LOG_TAG = "Meteoroid";
    public static final int MSG_ACTIVITY_CONF_CHANGE = 40965;
    public static final int MSG_ACTIVITY_CREATE = 40967;
    public static final int MSG_ACTIVITY_DESTROY = 40968;
    public static final int MSG_ACTIVITY_INTENT = 40966;
    public static final int MSG_ACTIVITY_PAUSE = 40960;
    public static final int MSG_ACTIVITY_RESTART = 40963;
    public static final int MSG_ACTIVITY_RESUME = 40961;
    public static final int MSG_ACTIVITY_START = 40962;
    public static final int MSG_ACTIVITY_STOP = 40964;
    private boolean eh = true;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        "onActivityResult:" + i + "|" + i2;
        h.b(h.a(k.MSG_SYSTEM_ACTIVITY_RESULT, new Object[]{new int[]{i, i2}, intent}));
        super.onActivityResult(i, i2, intent);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        h.b(h.a(MSG_ACTIVITY_CONF_CHANGE, this));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        getWindow().setBackgroundDrawable(null);
        getWindow().requestFeature(1);
        getWindow().setFlags(a.GAME_B_PRESSED, a.GAME_B_PRESSED);
        h.b((int) MSG_ACTIVITY_PAUSE, "MSG_ACTIVITY_PAUSE");
        h.b((int) MSG_ACTIVITY_RESUME, "MSG_ACTIVITY_RESUME");
        h.b((int) MSG_ACTIVITY_START, "MSG_ACTIVITY_START");
        h.b((int) MSG_ACTIVITY_RESTART, "MSG_ACTIVITY_RESTART");
        h.b((int) MSG_ACTIVITY_STOP, "MSG_ACTIVITY_STOP");
        h.b((int) MSG_ACTIVITY_CONF_CHANGE, "MSG_ACTIVITY_CONF_CHANGE");
        h.b((int) MSG_ACTIVITY_INTENT, "MSG_ACTIVITY_INTENT");
        h.b((int) MSG_ACTIVITY_CREATE, "MSG_ACTIVITY_CREATE");
        h.b((int) MSG_ACTIVITY_DESTROY, "MSG_ACTIVITY_DESTROY");
        k.a(this);
        super.onCreate(bundle);
        h.b((int) MSG_ACTIVITY_INTENT, getIntent());
        h.b((int) k.MSG_SYSTEM_INIT_COMPLETE, bundle);
        h.o(MSG_ACTIVITY_CREATE);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        h.o(MSG_ACTIVITY_DESTROY);
        SystemClock.sleep(200);
        k.ag();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (!l.o()) {
            k.al();
        }
        return true;
    }

    public void onLowMemory() {
        k.ao();
        super.onLowMemory();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        i.a(menuItem);
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        h.b(h.a(MSG_ACTIVITY_PAUSE, this));
        k.pause();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        i.onPrepareOptionsMenu(menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        h.b(h.a(MSG_ACTIVITY_RESTART, this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        h.b(h.a(MSG_ACTIVITY_RESUME, this));
        if (this.eh) {
            this.eh = false;
        } else {
            k.resume();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        h.b(h.a(MSG_ACTIVITY_START, this));
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        h.b(h.a(MSG_ACTIVITY_STOP, this));
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        return f.onTrackballEvent(motionEvent);
    }
}
