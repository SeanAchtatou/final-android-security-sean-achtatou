package org.meteoroid.core;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Message;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.meteoroid.core.h;

public final class g {
    public static final String LOG_TAG = "MediaManager";
    public static final String MEDIA_TEMP_FILE = "temp";
    public static final int VOLUME_CONTROL_BY_CLIP = -1;
    public static final int VOLUME_CONTROL_BY_DEVICE = -2;
    private static final g dK = new g();
    private static int dL = -1;
    private static String dM;
    private static int dN = 15;
    /* access modifiers changed from: private */
    public static int dO;
    private static float dP;
    private static AudioManager dQ;
    /* access modifiers changed from: private */
    public static final ConcurrentLinkedQueue<a> dR = new ConcurrentLinkedQueue<>();
    private static int dS = 0;
    private static int dT;
    private static int dU = 0;
    /* access modifiers changed from: private */
    public static int dV = 0;

    public class a {
        public String dW;
        public boolean dX;
        public int dY;
        public MediaPlayer dZ;
        public String name;
        public String type;

        public a() {
        }
    }

    public static int Z() {
        "Current volume is:" + dQ.getStreamVolume(3);
        return (int) (((float) dQ.getStreamVolume(3)) / dP);
    }

    public static a a(String str, InputStream inputStream, String str2) {
        if (inputStream == null) {
            return null;
        }
        dS++;
        g gVar = dK;
        gVar.getClass();
        a aVar = new a();
        String str3 = "";
        if (str2.indexOf("mid") != -1) {
            str3 = ".mid";
        } else if (str2.indexOf("mpeg") != -1) {
            str3 = ".mp3";
        } else if (str2.indexOf("amr") != -1) {
            str3 = ".amr";
        }
        aVar.name = null;
        aVar.type = str2;
        aVar.dW = dS + str3;
        FileOutputStream openFileOutput = k.getActivity().openFileOutput(aVar.dW, 1);
        byte[] bArr = new byte[com.a.a.b.a.FIRE_PRESSED];
        while (true) {
            int read = inputStream.read(bArr);
            if (read > 0) {
                openFileOutput.write(bArr, 0, read);
            } else {
                openFileOutput.flush();
                openFileOutput.close();
                aVar.dZ = new MediaPlayer();
                aVar.dW = dM + aVar.dW;
                dR.add(aVar);
                "Create a media clip " + ((String) null) + " [" + str2 + "].";
                return aVar;
            }
        }
    }

    protected static void a(Activity activity) {
        dM = activity.getFilesDir().getAbsolutePath() + File.separator;
        AudioManager audioManager = (AudioManager) activity.getSystemService("audio");
        dQ = audioManager;
        dN = audioManager.getStreamMaxVolume(3);
        "Max volume is" + dN;
        dP = ((float) dN) / 100.0f;
        "VOLUME_TRANS_RATIO is" + dP;
        dO = Z();
        "Init volume is" + dO;
        h.a(new h.a() {
            public final boolean a(Message message) {
                if (message.what == 47873) {
                    Iterator it = g.dR.iterator();
                    while (it.hasNext()) {
                        a aVar = (a) it.next();
                        try {
                            if (aVar.dZ != null && aVar.dZ.isPlaying() && aVar.dX) {
                                aVar.dZ.pause();
                                "force to pause:" + aVar.name;
                            }
                        } catch (IllegalStateException e) {
                            Log.w(g.LOG_TAG, "error to pause:" + aVar.name);
                        }
                    }
                    int unused = g.dV = g.dQ.getStreamVolume(3);
                    "Pause volume is" + g.dV;
                    g.k(g.dO);
                    return false;
                } else if (message.what != 47874) {
                    return false;
                } else {
                    g.dQ.setStreamVolume(3, g.dV, 16);
                    Iterator it2 = g.dR.iterator();
                    while (it2.hasNext()) {
                        a aVar2 = (a) it2.next();
                        try {
                            if (aVar2.dZ != null && aVar2.dX && !aVar2.dZ.isPlaying()) {
                                aVar2.dZ.start();
                                "force to resume:" + aVar2.name;
                            }
                        } catch (IllegalStateException e2) {
                            Log.w(g.LOG_TAG, "error to resume:" + aVar2.name);
                        }
                    }
                    return false;
                }
            }
        });
        if (dL == -2) {
            k(70);
        }
    }

    public static void b(boolean z) {
        "Mute works:" + z;
        if (z) {
            dU = Z();
            j(0);
        } else if (dU != 0) {
            j(dU);
        }
    }

    public static void i(int i) {
        dL = i;
    }

    private static void j(int i) {
        if (dL == -1) {
            k(i);
        } else if (dL == -2) {
            if (i == 0) {
                dT = Z();
                k(0);
            } else if (dT != 0) {
                k(dT);
                dT = 0;
            }
            "Failed to set volume because the globe volume mode is control by device." + dT;
        }
    }

    /* access modifiers changed from: private */
    public static void k(int i) {
        "Set device volume to " + i;
        dQ.setStreamVolume(3, (int) (((float) i) * dP), 16);
    }

    protected static void onDestroy() {
        dR.clear();
        k(dO);
    }
}
