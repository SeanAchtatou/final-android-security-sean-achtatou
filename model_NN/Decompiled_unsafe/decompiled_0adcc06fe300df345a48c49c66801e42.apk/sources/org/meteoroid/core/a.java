package org.meteoroid.core;

import android.os.Message;
import android.util.Log;
import java.util.HashMap;
import org.meteoroid.core.h;

public final class a {
    public static final String LOG_TAG = "ApplicationManager";
    public static final int MSG_APPLICATION_LAUNCH = 47617;
    public static final int MSG_APPLICATION_NEED_PAUSE = 47623;
    public static final int MSG_APPLICATION_NEED_START = 47622;
    public static final int MSG_APPLICATION_PAUSE = 47619;
    public static final int MSG_APPLICATION_QUIT = 47621;
    public static final int MSG_APPLICATION_RESUME = 47620;
    public static final int MSG_APPLICATION_START = 47618;
    public static C0003a dq;
    private static final HashMap<String, String> dr = new HashMap<>();

    /* renamed from: org.meteoroid.core.a$a  reason: collision with other inner class name */
    public interface C0003a {
        public static final int EXIT = 3;
        public static final int INIT = 0;
        public static final int PAUSE = 2;
        public static final int RUN = 1;

        void D();

        int getState();

        void onDestroy();

        void onPause();

        void onResume();

        void onStart();
    }

    protected static void T() {
        h.b((int) MSG_APPLICATION_LAUNCH, "MSG_APPLICATION_LAUNCH");
        h.b((int) MSG_APPLICATION_START, "MSG_APPLICATION_START");
        h.b((int) MSG_APPLICATION_PAUSE, "MSG_APPLICATION_PAUSE");
        h.b((int) MSG_APPLICATION_RESUME, "MSG_APPLICATION_RESUME");
        h.b((int) MSG_APPLICATION_QUIT, "MSG_APPLICATION_QUIT");
        h.b((int) MSG_APPLICATION_NEED_START, "MSG_APPLICATION_NEED_START");
        h.b((int) MSG_APPLICATION_NEED_PAUSE, "MSG_APPLICATION_NEED_PAUSE");
        h.a(new h.a() {
            public final boolean a(Message message) {
                if (message.what == 47873) {
                    a.pause();
                    return false;
                } else if (message.what != 47874) {
                    return false;
                } else {
                    a.resume();
                    return false;
                }
            }
        });
    }

    public static boolean U() {
        return dq != null && dq.getState() == 1;
    }

    public static void f(String str, String str2) {
        dr.put(str, str2);
    }

    public static void n(String str) {
        if (str != null) {
            try {
                dq = (C0003a) Class.forName(str).newInstance();
                h.b(h.a(MSG_APPLICATION_LAUNCH, dq));
            } catch (Exception e) {
                e.printStackTrace();
                Log.w(LOG_TAG, e.toString());
            }
            if (dq != null) {
                dq.D();
            } else {
                Log.w(LOG_TAG, "The application failed to launch.");
            }
        } else {
            Log.w(LOG_TAG, "No available application could launch.");
        }
    }

    protected static void onDestroy() {
        if (dq != null && dq.getState() != 3) {
            dq.onDestroy();
            h.b(h.a(MSG_APPLICATION_QUIT, dq));
        }
    }

    public static void pause() {
        if (dq == null) {
            return;
        }
        if (dq.getState() == 1) {
            dq.onPause();
            h.b(h.a(MSG_APPLICATION_PAUSE, dq));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + dq.getState());
    }

    public static void resume() {
        if (dq == null) {
            return;
        }
        if (dq.getState() == 2) {
            dq.onResume();
            h.b(h.a(MSG_APPLICATION_RESUME, dq));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + dq.getState());
    }

    public static void start() {
        if (dq == null) {
            return;
        }
        if (dq.getState() == 0) {
            dq.onStart();
            h.b(h.a(MSG_APPLICATION_START, dq));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + dq.getState());
    }
}
