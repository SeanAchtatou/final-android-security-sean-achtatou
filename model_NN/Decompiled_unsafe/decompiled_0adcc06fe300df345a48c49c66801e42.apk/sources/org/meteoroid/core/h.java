package org.meteoroid.core;

import android.os.Message;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingQueue;
import javax.microedition.midlet.MIDlet;

public final class h implements Runnable {
    public static final String LOG_TAG = "MessageDispatchManager";
    public static final int MSG_ARG2_DONT_RECYCLE_ME = -13295;
    public static final int MSG_ARG_NONE = 0;
    private static final LinkedBlockingQueue<Message> eb = new LinkedBlockingQueue<>();
    private static final CopyOnWriteArraySet<a> ec = new CopyOnWriteArraySet<>();
    private static final h ed = new h();
    private static final SparseArray<String> ee = new SparseArray<>();
    private static final ArrayList<a> eg = new ArrayList<>();
    private boolean ef;

    public interface a {
        boolean a(Message message);
    }

    protected static void T() {
        new Thread(ed).start();
    }

    public static Message a(int i, Object obj) {
        return a(i, obj, 0, 0);
    }

    public static Message a(int i, Object obj, int i2, int i3) {
        Message obtain = Message.obtain();
        obtain.what = i;
        obtain.obj = obj;
        obtain.arg1 = 0;
        obtain.arg2 = i3;
        return obtain;
    }

    public static void a(a aVar) {
        if (!ec.contains(aVar)) {
            ec.add(aVar);
        }
    }

    public static final void ae() {
        eb.clear();
    }

    public static void b(int i, Object obj) {
        b(a(i, obj, 0, 0));
    }

    public static void b(int i, String str) {
        ee.append(i, str);
    }

    public static void b(Message message) {
        eb.add(message);
    }

    public static synchronized void c(int i, Object obj) {
        synchronized (h.class) {
            c(a(MIDlet.MIDLET_PLATFORM_REQUEST_FINISH, obj, 0, 0));
        }
    }

    private static final void c(Message message) {
        boolean z;
        Iterator<a> it = ec.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            a next = it.next();
            if (next.a(message)) {
                "The message [" + ee.get(message.what) + "] has been consumed by [" + next.getClass().getName() + "]. And " + eb.size() + " messages are waiting.";
                z = true;
                break;
            }
        }
        if (!z) {
            "There is a message [" + ee.get(message.what, Integer.toHexString(message.what)) + "] which no consumer could deal with it. " + eb.size() + " messages left.";
        }
        if (message.arg2 != -13295) {
            message.recycle();
        }
    }

    public static void o(int i) {
        b(i, (Object) null);
    }

    protected static void onDestroy() {
        ed.ef = true;
        eb.clear();
    }

    public final void run() {
        while (!this.ef) {
            while (true) {
                Message poll = eb.poll();
                if (poll == null) {
                    break;
                }
                c(poll);
            }
            if (!eg.isEmpty()) {
                Iterator<a> it = eg.iterator();
                while (it.hasNext()) {
                    ec.remove(it.next());
                }
                eg.clear();
            }
            Thread.yield();
        }
    }
}
