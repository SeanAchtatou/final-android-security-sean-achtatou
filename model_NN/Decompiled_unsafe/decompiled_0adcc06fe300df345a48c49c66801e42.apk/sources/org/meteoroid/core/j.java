package org.meteoroid.core;

import android.content.SharedPreferences;

public final class j {

    public static final class a {
        private SharedPreferences el;

        private a(int i) {
            this.el = k.getActivity().getPreferences(i);
        }

        /* synthetic */ a(int i, byte b) {
            this(i);
        }

        public final SharedPreferences.Editor getEditor() {
            return this.el.edit();
        }

        public final SharedPreferences getSharedPreferences() {
            return this.el;
        }
    }

    public static a p(int i) {
        return new a(0, (byte) 0);
    }
}
