package org.meteoroid.core;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import com.androidbox.g5msnjhckhdeoe.R;
import java.util.ArrayList;
import java.util.Iterator;
import org.meteoroid.core.h;

public final class i {
    public static final int MSG_OPTIONMENU_ABOUT = 60080;
    public static final int OPTION_MENU_ITEM_ABOUT = 64160;
    public static final int OPTION_MENU_ITEM_EXIT = 64161;
    private static final ArrayList<c> ei = new ArrayList<>();
    private static int[] ej;

    private static final class a implements h.a, c {
        private a() {
        }

        /* synthetic */ a(byte b) {
            this();
        }

        public final boolean a(Message message) {
            if (message.what != 60080) {
                return false;
            }
            k.getHandler().post(new Runnable() {
                public final void run() {
                    a.this.r();
                }
            });
            return true;
        }

        public final int getId() {
            return i.OPTION_MENU_ITEM_ABOUT;
        }

        public final String p() {
            return k.getString(R.string.about);
        }

        public final void r() {
            AlertDialog.Builder builder = new AlertDialog.Builder(k.getActivity());
            builder.setTitle((int) R.string.about);
            builder.setMessage((int) R.string.about_dialog);
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public final void onCancel(DialogInterface dialogInterface) {
                    dialogInterface.dismiss();
                    k.resume();
                }
            });
            builder.setNegativeButton((int) R.string.close, new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    k.resume();
                }
            });
            builder.create().show();
            k.pause();
        }
    }

    private static final class b implements c {
        private b() {
        }

        /* synthetic */ b(byte b) {
            this();
        }

        public final int getId() {
            return i.OPTION_MENU_ITEM_EXIT;
        }

        public final String p() {
            return k.getString(R.string.exit);
        }

        public final void r() {
            k.al();
        }
    }

    public interface c {
        int getId();

        String p();

        void r();
    }

    protected static void T() {
        String string = k.getString(R.string.about_dialog);
        if (string != null && string.length() > 0) {
            a aVar = new a((byte) 0);
            a(aVar);
            h.a(aVar);
        }
        a(new b((byte) 0));
    }

    public static void a(MenuItem menuItem) {
        ei.get(menuItem.getItemId()).r();
    }

    public static void a(c cVar) {
        if (!ei.contains(cVar)) {
            ei.add(0, cVar);
        }
    }

    public static void b(c cVar) {
        ei.remove(cVar);
    }

    protected static void onDestroy() {
        ei.clear();
    }

    public static void onPrepareOptionsMenu(Menu menu) {
        boolean z;
        int i;
        menu.clear();
        Iterator<c> it = ei.iterator();
        int i2 = 0;
        int i3 = 0;
        while (it.hasNext()) {
            c next = it.next();
            if (ej != null) {
                z = false;
                for (int i4 : ej) {
                    if (i4 == next.getId()) {
                        z = true;
                    }
                }
            } else {
                z = false;
            }
            if (!z) {
                menu.add(131072, i3, i2, next.p());
                i = i2 + 1;
            } else {
                i = i2;
            }
            i3++;
            i2 = i;
        }
    }
}
