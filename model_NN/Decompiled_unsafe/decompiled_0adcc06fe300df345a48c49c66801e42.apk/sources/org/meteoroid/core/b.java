package org.meteoroid.core;

import android.util.Log;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public final class b {
    private static final String DATA_STORE_FILE = ".dat";
    private static final String LOG_TAG = "DataManager";

    private static String o(String str) {
        return str.replace(File.separator, "_");
    }

    public static boolean p(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String o = o(str);
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String o2 = o(o);
        String[] fileList = k.getActivity().fileList();
        ArrayList<String> arrayList = new ArrayList<>();
        if (fileList != null && fileList.length > 0) {
            for (int i = 0; i < fileList.length; i++) {
                if (fileList[i].endsWith(DATA_STORE_FILE) && (!(o2 == null || fileList[i].indexOf(o2) == -1) || o2 == null)) {
                    arrayList.add(fileList[i].substring(0, fileList[i].length() - 4));
                    "Find data file:" + fileList[i];
                }
            }
        }
        for (String equals : arrayList) {
            if (equals.equals(o)) {
                "Find data:" + o;
                return true;
            }
        }
        "Find no data:" + o;
        return false;
    }

    public static OutputStream q(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String o = o(str);
        "Update data:" + o;
        return k.getActivity().openFileOutput(o + DATA_STORE_FILE, 1);
    }

    public static InputStream r(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String o = o(str);
        "Read data:" + o;
        return k.getActivity().openFileInput(o + DATA_STORE_FILE);
    }
}
