package org.meteoroid.core;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import com.a.a.e.c;
import com.androidbox.g5msnjhckhdeoe.R;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import org.meteoroid.core.h;

public final class k {
    public static final String LOG_TAG = "SystemManager";
    public static final int MSG_SYSTEM_ACTIVITY_RESULT = 47880;
    public static final int MSG_SYSTEM_DEVICE_INIT_COMPLETE = 47878;
    public static final int MSG_SYSTEM_EXIT = 47875;
    public static final int MSG_SYSTEM_FEATURE_ADDED_COMPLETE = 47876;
    public static final int MSG_SYSTEM_FUNCTION_REQUEST = 47882;
    public static final int MSG_SYSTEM_GRAPHICS_INIT_COMPLETE = 47877;
    public static final int MSG_SYSTEM_INIT_COMPLETE = 47872;
    public static final int MSG_SYSTEM_LOG_APPENDER = 47886;
    public static final int MSG_SYSTEM_LOG_EVENT = 47887;
    public static final int MSG_SYSTEM_LOG_EVENT_BY_APPENDER = 47902;
    public static final int MSG_SYSTEM_NOTIFY_EXIT = 47881;
    public static final int MSG_SYSTEM_ONLINE_PARAM = 47885;
    public static final int MSG_SYSTEM_ON_PAUSE = 47873;
    public static final int MSG_SYSTEM_ON_RESUME = 47874;
    public static final int MSG_SYSTEM_VD_INIT_COMPLETE = 47879;
    /* access modifiers changed from: private */
    public static Activity em;
    private static int en = 0;
    public static boolean eo = false;
    /* access modifiers changed from: private */
    public static boolean ep = false;
    private static final Timer eq = new Timer();
    private static Handler handler;

    protected static void a(Activity activity) {
        em = activity;
        h.b((int) MSG_SYSTEM_INIT_COMPLETE, "MSG_SYSTEM_INIT_COMPLETE");
        h.b((int) MSG_SYSTEM_ON_PAUSE, "MSG_SYSTEM_ON_PAUSE");
        h.b((int) MSG_SYSTEM_FEATURE_ADDED_COMPLETE, "MSG_SYSTEM_FEATURE_ADDED_COMPLETE");
        h.b((int) MSG_SYSTEM_GRAPHICS_INIT_COMPLETE, "MSG_SYSTEM_GRAPHICS_INIT_COMPLETE");
        h.b((int) MSG_SYSTEM_DEVICE_INIT_COMPLETE, "MSG_SYSTEM_DEVICE_INIT_COMPLETE");
        h.b((int) MSG_SYSTEM_VD_INIT_COMPLETE, "MSG_SYSTEM_VD_INIT_COMPLETE");
        h.b((int) MSG_SYSTEM_ON_PAUSE, "MSG_SYSTEM_ON_PAUSE");
        h.b((int) MSG_SYSTEM_ON_RESUME, "MSG_SYSTEM_ON_RESUME");
        h.b((int) MSG_SYSTEM_FUNCTION_REQUEST, "MSG_SYSTEM_FUNCTION_REQUEST");
        h.b((int) MSG_SYSTEM_EXIT, "MSG_SYSTEM_EXIT");
        h.b((int) MSG_SYSTEM_ACTIVITY_RESULT, "MSG_SYSTEM_ACTIVITY_RESULT");
        h.b((int) MSG_SYSTEM_LOG_EVENT, "MSG_SYSTEM_LOG_EVENT");
        h.b((int) MSG_SYSTEM_NOTIFY_EXIT, "MSG_SYSTEM_NOTIFY_EXIT");
        Properties properties = new Properties();
        try {
            properties.load(activity.getResources().openRawResource(c.G("globe")));
            handler = new Handler();
            ao();
            if (properties.containsKey("ThrowIOExceptions")) {
                eo = Boolean.parseBoolean(properties.getProperty("ThrowIOExceptions"));
            }
            if (properties.containsKey("DontQuit")) {
                l.ey = Boolean.parseBoolean(properties.getProperty("DontQuit"));
            }
            if (!properties.containsKey("DisableWakeLock")) {
                activity.getWindow().setFlags(128, 128);
            }
            h.T();
            e.a(activity);
            d.T();
            if (properties.containsKey("feature")) {
                String[] split = properties.getProperty("feature").split("\\}");
                for (int i = 0; i < split.length; i++) {
                    int indexOf = split[i].indexOf("{");
                    if (indexOf != -1) {
                        d.g(split[i].trim().substring(0, indexOf), split[i].trim().substring(indexOf + 1));
                        split[i] + " has been added.";
                    } else {
                        Log.w(LOG_TAG, "Failed to create feature:" + split[i]);
                    }
                }
            }
            h.o(MSG_SYSTEM_FEATURE_ADDED_COMPLETE);
            a.T();
            f.a(activity);
            if (properties.containsKey("VolumeMode")) {
                g.i(Integer.parseInt(properties.getProperty("VolumeMode")));
            }
            g.a(activity);
            l.a(activity);
            i.T();
            c.s(properties.getProperty("device"));
            h.o(MSG_SYSTEM_DEVICE_INIT_COMPLETE);
            h.o(MSG_SYSTEM_VD_INIT_COMPLETE);
            if (properties.containsKey("AdaptiveVirtualDevice")) {
                m.eH = Boolean.parseBoolean(properties.getProperty("AdaptiveVirtualDevice"));
            }
            m.y(properties.getProperty("virtualdevice"));
            properties.clear();
            System.gc();
            h.a(new h.a() {
                public final boolean a(Message message) {
                    if (message.what == 47875) {
                        k.af();
                        return true;
                    } else if (message.what == 47881) {
                        if (k.ep) {
                            return true;
                        }
                        l.a(k.getString(R.string.alert), k.getString(R.string.exit_dialog), k.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public final void onClick(DialogInterface dialogInterface, int i) {
                                k.ag();
                                boolean unused = k.ep = false;
                            }
                        }, k.getString(R.string.no), new DialogInterface.OnClickListener() {
                            public final void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                k.resume();
                                boolean unused = k.ep = false;
                            }
                        }, true, new DialogInterface.OnCancelListener() {
                            public final void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                                k.resume();
                                boolean unused = k.ep = false;
                            }
                        });
                        boolean unused = k.ep = true;
                        k.pause();
                        return true;
                    } else if (message.what == 47882) {
                        return k.v((String) message.obj);
                    } else {
                        return false;
                    }
                }
            });
            h.b(h.a(MSG_SYSTEM_LOG_EVENT, new String[]{"Launch", em.getString(R.string.app_name)}));
        } catch (Exception e) {
            Log.e(LOG_TAG, "Load globe.properties error." + e);
        }
    }

    public static void a(final String str, final int i) {
        handler.post(new Runnable() {
            public final void run() {
                Toast.makeText(k.em, str, i).show();
            }
        });
    }

    protected static void af() {
        h.ae();
        eq.cancel();
        eq.purge();
        a.onDestroy();
        i.onDestroy();
        e.onDestroy();
        f.onDestroy();
        g.onDestroy();
        l.onDestroy();
        if (c.ds != null) {
            c.ds.onDestroy();
        }
        m.onDestroy();
        d.onDestroy();
        h.onDestroy();
        em.finish();
        System.gc();
        System.exit(0);
        Process.killProcess(Process.myPid());
    }

    public static void ag() {
        h.b(h.a(MSG_SYSTEM_EXIT, null));
    }

    public static final boolean ah() {
        return ai() || aj();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (an().getNetworkInfo(1).isConnected() == true) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean ai() {
        /*
            r0 = 1
            android.app.Activity r1 = org.meteoroid.core.k.em
            java.lang.String r2 = "android.permission.ACCESS_NETWORK_STATE"
            int r1 = r1.checkCallingOrSelfPermission(r2)
            if (r1 == 0) goto L_0x000c
        L_0x000b:
            return r0
        L_0x000c:
            r1 = 0
            android.net.ConnectivityManager r2 = an()     // Catch:{ Exception -> 0x002b }
            r3 = 1
            android.net.NetworkInfo r2 = r2.getNetworkInfo(r3)     // Catch:{ Exception -> 0x002b }
            boolean r2 = r2.isConnected()     // Catch:{ Exception -> 0x002b }
            if (r2 != r0) goto L_0x0031
        L_0x001c:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Wifi state: "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            r1.toString()
            goto L_0x000b
        L_0x002b:
            r0 = move-exception
            java.lang.String r2 = "SystemManager"
            android.util.Log.w(r2, r0)
        L_0x0031:
            r0 = r1
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.core.k.ai():boolean");
    }

    private static boolean aj() {
        boolean z = true;
        if (em.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0) {
            try {
                if (!an().getNetworkInfo(0).isConnectedOrConnecting()) {
                    if (am().getDataState() != 2) {
                        z = false;
                    }
                }
            } catch (Exception e) {
                Log.w(LOG_TAG, e);
                z = false;
            }
            "DataConnect state: " + z;
        }
        return z;
    }

    public static String ak() {
        return em.getString(R.string.app_name);
    }

    public static void al() {
        h.o(MSG_SYSTEM_NOTIFY_EXIT);
    }

    public static TelephonyManager am() {
        return (TelephonyManager) em.getSystemService("phone");
    }

    private static ConnectivityManager an() {
        return (ConnectivityManager) em.getSystemService("connectivity");
    }

    public static void ao() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) em.getSystemService("activity")).getRunningAppProcesses();
        int myPid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.pid != myPid && next.importance > 300) {
                Process.killProcess(next.pid);
                "Kill background process:" + next.processName + " pid:" + next.pid;
            }
        }
    }

    public static int ap() {
        return em.getWindowManager().getDefaultDisplay().getWidth();
    }

    public static int aq() {
        return em.getWindowManager().getDefaultDisplay().getHeight();
    }

    public static int ar() {
        return em.getResources().getConfiguration().orientation;
    }

    public static Timer as() {
        return eq;
    }

    public static int at() {
        try {
            return Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (NumberFormatException e) {
            return 4;
        }
    }

    public static String au() {
        return Build.MODEL;
    }

    public static String av() {
        return Build.MANUFACTURER;
    }

    public static String aw() {
        String str;
        String string = Settings.Secure.getString(em.getContentResolver(), "android_id");
        if (!"9774d56d682e549c".equals(string)) {
            try {
                Class<?> cls = Class.forName("android.os.SystemProperties");
                str = (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
            } catch (Exception e) {
            }
            "androidId=" + str;
            return str;
        }
        str = string;
        "androidId=" + str;
        return str;
    }

    public static Activity getActivity() {
        return em;
    }

    public static Handler getHandler() {
        return handler;
    }

    public static String getString(int i) {
        return em.getString(i);
    }

    public static void pause() {
        int i = en + 1;
        en = i;
        if (i == 1) {
            h.b(h.a(MSG_SYSTEM_ON_PAUSE, null));
        } else {
            Log.w(LOG_TAG, "The system has already paused." + en);
        }
    }

    public static void q(int i) {
        em.setRequestedOrientation(i);
    }

    public static void resume() {
        int i = en - 1;
        en = i;
        if (i == 0) {
            h.b(h.a(MSG_SYSTEM_ON_RESUME, null));
        } else {
            Log.w(LOG_TAG, "The system do not need resumed." + en);
        }
        if (en <= 0) {
            en = 0;
        }
    }

    public static final InputStream t(String str) {
        InputStream inputStream = null;
        try {
            inputStream = em.getAssets().open(str);
        } catch (Exception e) {
            Log.w(LOG_TAG, "Can't load resource:" + str + " is not exist.");
            if (eo) {
                throw new IOException();
            }
        }
        "Load assert " + str + (inputStream != null ? " success." : " failed.");
        return inputStream;
    }

    public static boolean u(String str) {
        h.b(h.a(MSG_SYSTEM_FUNCTION_REQUEST, str));
        return true;
    }

    public static boolean v(final String str) {
        if (str.startsWith("http://") || str.startsWith("market://")) {
            handler.post(new Runnable() {
                public final void run() {
                    k.em.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                }
            });
            return true;
        } else if (str.startsWith("tel:")) {
            handler.post(new Runnable() {
                public final void run() {
                    k.em.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(str)));
                }
            });
            return true;
        } else {
            Log.w(LOG_TAG, "Not supported " + str);
            return false;
        }
    }

    public static boolean w(String str) {
        try {
            return em.getPackageManager().getApplicationInfo(str, 0) != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean x(String str) {
        PackageManager packageManager = em.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(str, 0);
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setPackage(packageInfo.packageName);
            ResolveInfo next = packageManager.queryIntentActivities(intent, 0).iterator().next();
            if (next != null) {
                String str2 = next.activityInfo.name;
                Intent intent2 = new Intent("android.intent.action.MAIN");
                intent2.addCategory("android.intent.category.LAUNCHER");
                intent2.setComponent(new ComponentName(str, str2));
                em.startActivity(intent2);
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }
}
