package org.meteoroid.core;

import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import com.a.a.d.c;
import org.meteoroid.core.h;

public final class m {
    public static final String LOG_TAG = "VDManager";
    public static final int MSG_SYSTEM_VD_RELOAD = 6913;
    public static final int MSG_SYSTEM_VD_RELOAD_COMPLETE = 6914;
    public static c eG;
    public static boolean eH = false;
    public static boolean eI;
    private static int eJ;
    private static int eK;
    /* access modifiers changed from: private */
    public static boolean eL;
    /* access modifiers changed from: private */
    public static boolean eM;
    private static final Thread eN = new Thread() {
        public final void run() {
            while (m.eL) {
                m.aI();
                SystemClock.sleep(200);
            }
        }
    };
    private static int orientation;

    private static final void aF() {
        eJ = k.ap();
        eK = k.aq();
        orientation = k.ar();
    }

    public static final boolean aG() {
        return (eJ == k.ap() && eK == k.aq() && orientation == k.ar()) ? false : true;
    }

    public static void aH() {
        if (!eL && !eN.isAlive()) {
            eL = true;
            eN.start();
        }
    }

    public static void aI() {
        if (!eI) {
            eM = true;
            e.V();
            eM = false;
        }
    }

    protected static void onDestroy() {
        if (eG != null) {
            eG.onDestroy();
        }
    }

    protected static c y(String str) {
        try {
            c cVar = (c) Class.forName(str).newInstance();
            eG = cVar;
            cVar.onCreate();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to create virtual device. " + e);
            e.printStackTrace();
        }
        h.b((int) MSG_SYSTEM_VD_RELOAD, "MSG_SYSTEM_VD_RELOAD");
        h.b((int) MSG_SYSTEM_VD_RELOAD_COMPLETE, "MSG_SYSTEM_VD_RELOAD_COMPLETE");
        h.a(new h.a() {
            public final boolean a(Message message) {
                if (message.what == 40965 && m.eH && m.aG()) {
                    k.pause();
                    h.o(m.MSG_SYSTEM_VD_RELOAD);
                } else if (message.what == 6913) {
                    m.eI = true;
                    SystemClock.sleep(100);
                    while (m.eM) {
                        Thread.yield();
                    }
                    m.z((String) message.obj);
                    h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"ReloadSkin", k.ak() + "=" + ((String) message.obj)});
                    h.o(m.MSG_SYSTEM_VD_RELOAD_COMPLETE);
                    return true;
                } else if (message.what == 6914) {
                    SystemClock.sleep(100);
                    k.resume();
                    m.eI = false;
                }
                return false;
            }
        });
        aF();
        return eG;
    }

    protected static void z(String str) {
        eG.z(str);
        aF();
    }
}
