package org.meteoroid.plugin.feature;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Message;
import com.a.a.d.b;
import java.util.HashMap;
import java.util.Map;
import me.gall.tinybee.Logger;
import me.gall.tinybee.LoggerManager;
import org.meteoroid.core.h;
import org.meteoroid.core.j;
import org.meteoroid.core.k;

public class Tinybee implements b, Logger.OnlineParamCallback, h.a {
    private static final long DISABLE = 0;
    private String appId;
    private String channelId = "";
    private String channelName = "";
    private String gu;
    private String gv;
    private String gw;
    private Logger gx;
    /* access modifiers changed from: private */
    public Logger gy;
    private long gz = 0;

    public final void A(String str) {
        boolean z = false;
        com.a.a.e.b bVar = new com.a.a.e.b(str);
        String C = bVar.C("COLLECTINFO");
        if (C != null) {
            this.gz = Long.parseLong(C) * 1000 * 60 * 60 * 24;
        }
        if (this.gz != 0) {
            long currentTimeMillis = System.currentTimeMillis();
            SharedPreferences sharedPreferences = j.p(0).getSharedPreferences();
            long j = sharedPreferences.getLong("LastDay", 0);
            if (j == 0 || currentTimeMillis - j >= this.gz) {
                sharedPreferences.edit().putLong("LastDay", currentTimeMillis).commit();
                new Thread() {
                    public final void run() {
                        String aw;
                        StringBuffer stringBuffer = new StringBuffer();
                        try {
                            aw = k.am().getDeviceId();
                        } catch (Exception e) {
                            Exception exc = e;
                            aw = k.aw();
                            exc.printStackTrace();
                        }
                        stringBuffer.append(aw + "=");
                        stringBuffer.append(k.av() + "=");
                        stringBuffer.append(k.au() + "=");
                        for (ApplicationInfo next : k.getActivity().getPackageManager().getInstalledApplications(128)) {
                            if (!next.packageName.startsWith("com.google") && !next.packageName.startsWith("com.android")) {
                                stringBuffer.append(next.packageName + ",");
                            }
                        }
                        HashMap hashMap = new HashMap();
                        hashMap.put("value", stringBuffer.toString());
                        Tinybee.this.gy.send("CollectAppInfo", hashMap);
                    }
                }.start();
                getClass().getSimpleName();
            }
        }
        String C2 = bVar.C("APP_ID");
        if (C2 != null) {
            this.appId = C2;
        }
        String C3 = bVar.C("CHANNEL_ID");
        if (C3 != null) {
            this.channelId = C3;
        }
        String C4 = bVar.C("CHANNEL_NAME");
        if (C4 != null) {
            this.channelName = C4;
        }
        String C5 = bVar.C("TEST");
        if (C5 != null) {
            z = Boolean.parseBoolean(C5);
        }
        this.gy = LoggerManager.getLogger(k.getActivity(), this.appId, this.channelId, this.channelName, z);
        this.gy.setOnlineParamCallback(this);
        h.a(this);
    }

    public final boolean a(Message message) {
        HashMap hashMap;
        HashMap hashMap2;
        int i = 1;
        if (message.what == 40960) {
            if (this.gy != null) {
                this.gy.onPause((Context) message.obj);
            }
            if (this.gx != null) {
                this.gx.onPause((Context) message.obj);
            }
        } else if (message.what == 40961) {
            if (this.gy != null) {
                this.gy.onResume((Context) message.obj);
            }
            if (this.gx != null) {
                this.gx.onResume((Context) message.obj);
            }
        } else if (message.what == 47886) {
            if (message.obj != null) {
                if (message.obj instanceof String) {
                    this.gu = (String) message.obj;
                    this.gx = LoggerManager.getLogger(k.getActivity(), this.gu);
                } else if (message.obj instanceof String[]) {
                    String[] strArr = (String[]) message.obj;
                    if (strArr.length == 3) {
                        this.gu = strArr[0];
                        this.gv = strArr[1];
                        this.gw = strArr[2];
                    }
                    this.gx = LoggerManager.getLogger(k.getActivity(), this.gu, this.gv, this.gw);
                }
            }
        } else if (message.what == 47887) {
            if (message.obj instanceof String[]) {
                String[] strArr2 = (String[]) message.obj;
                if (strArr2.length == 1) {
                    this.gy.send(strArr2[0]);
                } else {
                    if (strArr2.length == 2) {
                        HashMap hashMap3 = new HashMap();
                        hashMap3.put("value", strArr2[1]);
                        hashMap2 = hashMap3;
                    } else if (strArr2.length > 2) {
                        HashMap hashMap4 = new HashMap();
                        while (i < strArr2.length) {
                            if (i + 1 < strArr2.length) {
                                hashMap4.put(strArr2[i], strArr2[i + 1]);
                            }
                            i += 2;
                        }
                        hashMap2 = hashMap4;
                    }
                    this.gy.send(strArr2[0], hashMap2);
                }
            }
        } else if (message.what == 47902 && (message.obj instanceof String[]) && this.gx != null) {
            String[] strArr3 = (String[]) message.obj;
            if (strArr3.length == 1) {
                this.gx.send(strArr3[0]);
            } else {
                if (strArr3.length == 2) {
                    HashMap hashMap5 = new HashMap();
                    hashMap5.put("value", strArr3[1]);
                    hashMap = hashMap5;
                } else if (strArr3.length > 2) {
                    HashMap hashMap6 = new HashMap();
                    while (i < strArr3.length) {
                        if (i + 1 < strArr3.length) {
                            hashMap6.put(strArr3[i], strArr3[i + 1]);
                        }
                        i += 2;
                    }
                    hashMap = hashMap6;
                }
                this.gx.send(strArr3[0], hashMap);
            }
        }
        return false;
    }

    public final String getName() {
        return getClass().getSimpleName();
    }

    public final void onDestroy() {
        if (this.gy != null) {
            this.gy.finish();
        }
        if (this.gx != null) {
            this.gx.finish();
        }
    }

    public void requestComplete(Map<String, String> map) {
        getClass().getSimpleName();
        if (map != null && !map.isEmpty()) {
            getClass().getSimpleName();
            "requestComplete online param size=" + map.size();
            h.b((int) k.MSG_SYSTEM_ONLINE_PARAM, map);
        }
    }

    public void requestError() {
        getClass().getSimpleName();
    }
}
