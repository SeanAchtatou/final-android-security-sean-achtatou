package org.meteoroid.plugin.feature;

import android.content.DialogInterface;
import com.a.a.d.b;
import java.util.LinkedList;
import org.meteoroid.core.h;

public abstract class AbstractPaymentManager implements DialogInterface.OnClickListener, b, h.a {
    private boolean gl = false;
    private LinkedList<Payment> gm;

    public interface Payment extends b {
        public static final int MSG_PAYMENT_FAIL = 61699;
        public static final int MSG_PAYMENT_NO_MORE = 61700;
        public static final int MSG_PAYMENT_QUERY = 61697;
        public static final int MSG_PAYMENT_REQUEST = 61696;
        public static final int MSG_PAYMENT_SUCCESS = 61698;

        String p();
    }

    public final void a(Payment payment) {
        if (!this.gm.contains(payment)) {
            getName();
            payment.p() + " has added into availiable payments.";
            this.gm.add(payment);
        }
    }
}
