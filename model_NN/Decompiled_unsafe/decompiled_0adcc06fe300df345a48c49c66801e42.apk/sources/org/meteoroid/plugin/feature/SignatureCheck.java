package org.meteoroid.plugin.feature;

import android.content.DialogInterface;
import android.os.Message;
import com.a.a.d.b;
import com.a.a.e.a;
import java.security.MessageDigest;
import org.meteoroid.core.h;
import org.meteoroid.core.k;
import org.meteoroid.core.l;

public class SignatureCheck implements b, h.a {
    private boolean gr = false;
    private String gs;

    public final void A(String str) {
        int i = 0;
        String C = new com.a.a.e.b(str).C("SIGN");
        try {
            this.gs = a.p(MessageDigest.getInstance("SHA-1").digest(k.getActivity().getPackageManager().getPackageInfo(k.getActivity().getPackageName(), 64).signatures[0].toByteArray()));
            if (C != null) {
                String[] split = C.split(";");
                while (true) {
                    if (i >= split.length) {
                        break;
                    } else if (split[i].equals(this.gs)) {
                        this.gr = true;
                        break;
                    } else {
                        i++;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        h.a(this);
    }

    public final boolean a(Message message) {
        if (message.what != 47872 || this.gr) {
            return false;
        }
        h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"SignatureCheckError", "应用名称", k.ak(), "sign", this.gs});
        l.a("警告", "您正在使用破解版产品，该产品可能会侵害您的利益，请打客服电话010-68948836举报。", "退出", new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                k.ag();
            }
        });
        "Invalid signature:" + this.gs;
        return true;
    }

    public final String getName() {
        return "SignatureCheck";
    }

    public final void onDestroy() {
    }
}
