package org.meteoroid.plugin.feature;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;
import com.a.a.e.c;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.meteoroid.core.h;
import org.meteoroid.core.j;
import org.meteoroid.core.k;
import org.meteoroid.core.l;
import org.meteoroid.plugin.feature.AbstractPaymentManager;

public abstract class AbstractDownloadAndInstall extends BroadcastReceiver implements h.a, AbstractPaymentManager.Payment {
    public static final int INSTALL_FAIL = 3;
    public static final int INSTALL_NOT_START = 0;
    public static final int INSTALL_RUNNING = 1;
    public static final int INSTALL_SUCCESS = 2;
    private static final String fileName = "target.apk";
    private a[] fW;
    private ArrayList<a> fX;
    private a fY;
    private boolean fZ;
    private int ga = 0;
    private boolean gb;
    public com.a.a.e.b gc;
    public boolean gd;
    /* access modifiers changed from: private */
    public String ge;
    /* access modifiers changed from: private */
    public String gf;

    public class a {
        public String gg;
        public boolean gh;
        public String gi;
        public String gj;
        public String packageName;

        public a() {
        }
    }

    private final class b extends AsyncTask<URL, Integer, Long> {
        private b() {
        }

        /* synthetic */ b(AbstractDownloadAndInstall abstractDownloadAndInstall, byte b) {
            this();
        }

        /* access modifiers changed from: private */
        public Long doInBackground(URL... urlArr) {
            File file;
            try {
                if (!new File(AbstractDownloadAndInstall.this.gf).exists()) {
                    File file2 = new File(AbstractDownloadAndInstall.this.ge);
                    file2.mkdirs();
                    if (!k.ah()) {
                        return 0L;
                    }
                    HttpURLConnection httpURLConnection = (HttpURLConnection) urlArr[0].openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.connect();
                    File file3 = new File(file2, AbstractDownloadAndInstall.fileName);
                    try {
                        InputStream inputStream = httpURLConnection.getInputStream();
                        AbstractDownloadAndInstall.c(inputStream);
                        inputStream.close();
                    } catch (IOException e) {
                        file = file3;
                        Log.e(AbstractDownloadAndInstall.this.getName(), "Fail to download the file.");
                        if (file != null && file.exists()) {
                            file.delete();
                        }
                        return -1L;
                    }
                }
                return 100L;
            } catch (IOException e2) {
                file = null;
                Log.e(AbstractDownloadAndInstall.this.getName(), "Fail to download the file.");
                file.delete();
                return -1L;
            }
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            Long l = (Long) obj;
            if (l.longValue() == 100) {
                l.az();
                AbstractDownloadAndInstall.this.installApk();
            } else if (l.longValue() == -1) {
                l.az();
                if (!AbstractDownloadAndInstall.this.gd) {
                    h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
                }
            } else {
                AbstractDownloadAndInstall.this.getName();
                "On Post Execute result..." + l;
            }
        }
    }

    private void b(Exception exc) {
        this.ga = 3;
        int i = this.ga;
        if (exc != null) {
            Log.w(getName(), "Invalid package:" + this.fY.packageName + exc);
        }
        if (!this.gd) {
            h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
        }
    }

    /* access modifiers changed from: private */
    public static void c(InputStream inputStream) {
        FileOutputStream openFileOutput = k.getActivity().openFileOutput(fileName, 1);
        byte[] bArr = new byte[102400];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                openFileOutput.write(bArr, 0, read);
            } else {
                openFileOutput.flush();
                openFileOutput.close();
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void installApk() {
        if (this.gf == null) {
            this.ge = k.getActivity().getFilesDir().getAbsolutePath() + File.separator;
            this.gf = this.ge + fileName;
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(this.gf)), "application/vnd.android.package-archive");
        k.getActivity().startActivity(intent);
    }

    public final void A(String str) {
        this.fX = new ArrayList<>();
        this.ge = k.getActivity().getFilesDir().getAbsolutePath() + File.separator;
        this.gf = this.ge + fileName;
        this.gc = new com.a.a.e.b(str);
        String C = this.gc.C("DOWNLOAD");
        if (C != null) {
            String[] split = C.split("\\;");
            this.fW = new a[split.length];
            for (int i = 0; i < split.length; i++) {
                this.fW[i] = new a();
                this.fW[i].gg = split[i];
                getName();
                "app[" + i + "] target is " + this.fW[i].gg;
            }
        } else {
            Log.e(getName(), "No app target url !!!!!!!");
        }
        String C2 = this.gc.C("PACKAGE");
        if (C2 != null) {
            String[] split2 = C2.split("\\;");
            for (int i2 = 0; i2 < split2.length; i2++) {
                this.fW[i2].packageName = split2[i2];
                this.fW[i2].gh = j.p(0).getSharedPreferences().getBoolean(this.fW[i2].packageName, false);
                getName();
                "app[" + i2 + "] packageName is " + this.fW[i2].packageName + " and install state is " + this.fW[i2].gh;
            }
        }
        String C3 = this.gc.C("IMAGE");
        if (C3 != null) {
            String[] split3 = C3.split("\\;");
            for (int i3 = 0; i3 < split3.length; i3++) {
                this.fW[i3].gi = split3[i3];
                getName();
                "app[" + i3 + "] image is " + this.fW[i3].gi;
            }
        }
        String C4 = this.gc.C("THUMB");
        if (C4 != null) {
            String[] split4 = C4.split("\\;");
            for (int i4 = 0; i4 < split4.length; i4++) {
                this.fW[i4].gj = split4[i4];
                getName();
                "app[" + i4 + "] thumb is " + this.fW[i4].gj;
            }
        }
        String C5 = this.gc.C("FORCELAUNCH");
        if (C5 != null) {
            this.fZ = Boolean.parseBoolean(C5);
        }
        String C6 = this.gc.C("SKIPCHECK");
        if (C6 != null) {
            this.gb = Boolean.parseBoolean(C6);
        }
        for (int i5 = 0; i5 < this.fW.length; i5++) {
            if (!this.fW[i5].gh) {
                this.fX.add(this.fW[i5]);
            }
        }
        h.a(this);
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme(com.umeng.common.a.c);
        k.getActivity().registerReceiver(this, intentFilter);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.core.l.a(java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [?[OBJECT, ARRAY], java.lang.String, int, int]
     candidates:
      org.meteoroid.core.l.a(java.lang.String, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      org.meteoroid.core.l.a(java.lang.String, java.lang.String, boolean, boolean):void */
    public final void a(a aVar) {
        if (aVar.gh) {
            k.a("不能重复安装同一个应用程序", 0);
            if (!this.gd) {
                h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
                return;
            }
            return;
        }
        this.fY = aVar;
        this.ga = 1;
        int i = this.ga;
        h.b(h.a(k.MSG_SYSTEM_LOG_EVENT, new String[]{"StartInstallApp", k.ak() + "=" + aVar.packageName}));
        if (aVar.gg.startsWith("market:") || aVar.gg.startsWith("http:") || aVar.gg.startsWith("https:")) {
            k.v(aVar.gg);
        } else if (aVar.gg.startsWith("download:")) {
            String str = "http:" + aVar.gg.substring(9);
            l.a((String) null, "正在下载", false, true);
            try {
                new b(this, (byte) 0).execute(new URL(str));
            } catch (MalformedURLException e) {
                l.az();
                b(e);
            }
        } else {
            try {
                InputStream t = k.t(c.E(this.fY.gg));
                c(t);
                t.close();
                installApk();
            } catch (Exception e2) {
                b(e2);
            }
        }
    }

    public final boolean a(Message message) {
        if (message.what == 61697) {
            ((AbstractPaymentManager) message.obj).a(this);
        }
        if (message.what != 40961 || this.ga != 1) {
            return false;
        }
        b((Exception) null);
        return false;
    }

    public final List<a> aP() {
        if (this.gb) {
            getName();
        } else {
            for (int i = 0; i < this.fW.length; i++) {
                if (k.w(this.fW[i].packageName)) {
                    this.fX.remove(this.fW[i]);
                }
            }
            getName();
            "Available apps are " + this.fX.size();
        }
        return this.fX;
    }

    public final void onDestroy() {
        k.getActivity().unregisterReceiver(this);
    }

    public void onReceive(Context context, Intent intent) {
        getName();
        "The package has installed." + intent.getDataString();
        if (this.fY != null && this.fY.packageName != null && intent.getDataString().indexOf(this.fY.packageName) != -1) {
            this.ga = 2;
            int i = this.ga;
            j.p(0).getEditor().putBoolean(this.fY.packageName, true).commit();
            this.fY.gh = true;
            if (aP().isEmpty()) {
                h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_NO_MORE, this));
            }
            h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_SUCCESS, this));
            h.b(h.a(k.MSG_SYSTEM_LOG_EVENT, new String[]{"InstallAppSuccess", this.fY.packageName + "=" + k.ak()}));
            if (this.fZ) {
                k.x(this.fY.packageName);
            }
        }
    }
}
