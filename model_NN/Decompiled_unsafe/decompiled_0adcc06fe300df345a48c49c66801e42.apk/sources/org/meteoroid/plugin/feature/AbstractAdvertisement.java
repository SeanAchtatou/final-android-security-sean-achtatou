package org.meteoroid.plugin.feature;

import android.content.Context;
import com.a.a.d.b;
import java.util.TimerTask;
import org.meteoroid.core.h;

public abstract class AbstractAdvertisement extends TimerTask implements b, h.a {
    private static final String ADVERTISEMENTS = "_AB_ADVERTISEMENTS";
    public static final int ALWAYS_SHOW_THE_ADVERTISMENT = 0;
    public static final int MSG_ADVERTISEMENT_CLICK = -2128412415;
    public static final int MSG_ADVERTISEMENT_SHOW = -2128412416;
    private static final String STATUS = "STATUS";
    private int duration = 20;
    private boolean fO = false;
    private boolean fP = false;
    private int fQ = 60;
    private boolean fR = false;
    private boolean fS = false;
    private boolean fT = false;
    private int fU = 1000;
    private boolean fV = true;

    public static boolean x(Context context) {
        return context.getSharedPreferences(ADVERTISEMENTS, 0).getBoolean(STATUS, false);
    }
}
