package org.meteoroid.plugin.feature;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Message;
import com.a.a.d.b;
import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;
import java.util.HashMap;
import org.meteoroid.core.h;
import org.meteoroid.core.j;
import org.meteoroid.core.k;

public class Umeng implements b, h.a {
    private static final long DISABLE = 0;
    private long gz = 0;

    public final void A(String str) {
        MobclickAgent.onError(k.getActivity());
        UmengUpdateAgent.setUpdateOnlyWifi(false);
        UmengUpdateAgent.setUpdateAutoPopup(true);
        UmengUpdateAgent.update(k.getActivity());
        UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {
            public final void onUpdateReturned(int i, UpdateResponse updateResponse) {
                Umeng.this.getName();
                i + " " + updateResponse;
            }
        });
        String C = new com.a.a.e.b(str).C("COLLECTINFO");
        if (C != null) {
            this.gz = Long.parseLong(C) * 1000 * 60 * 60 * 24;
        }
        if (this.gz != 0) {
            long currentTimeMillis = System.currentTimeMillis();
            SharedPreferences sharedPreferences = j.p(0).getSharedPreferences();
            long j = sharedPreferences.getLong("LastDay", 0);
            if (j == 0 || currentTimeMillis - j >= this.gz) {
                sharedPreferences.edit().putLong("LastDay", currentTimeMillis).commit();
                new Thread() {
                    public final void run() {
                        String aw;
                        StringBuffer stringBuffer = new StringBuffer();
                        try {
                            aw = k.am().getDeviceId();
                        } catch (Exception e) {
                            Exception exc = e;
                            aw = k.aw();
                            exc.printStackTrace();
                        }
                        stringBuffer.append(aw + "=");
                        stringBuffer.append(k.av() + "=");
                        stringBuffer.append(k.au() + "=");
                        for (ApplicationInfo next : k.getActivity().getPackageManager().getInstalledApplications(128)) {
                            if (!next.packageName.startsWith("com.google") && !next.packageName.startsWith("com.android")) {
                                stringBuffer.append(next.packageName + ",");
                            }
                        }
                        MobclickAgent.onEvent(k.getActivity(), "CollectAppInfo", stringBuffer.toString());
                    }
                }.start();
            }
        }
        h.a(this);
    }

    public final boolean a(Message message) {
        if (message.what == 40960) {
            MobclickAgent.onPause((Context) message.obj);
        } else if (message.what == 40961) {
            MobclickAgent.onResume((Context) message.obj);
        } else if (message.what == 47887) {
            String[] strArr = (String[]) message.obj;
            if (strArr.length == 1) {
                MobclickAgent.onEvent(k.getActivity(), strArr[0]);
            } else if (strArr.length == 2) {
                MobclickAgent.onEvent(k.getActivity(), strArr[0], strArr[1]);
            } else if (strArr.length > 2) {
                HashMap hashMap = new HashMap();
                for (int i = 1; i < strArr.length; i += 2) {
                    if (i + 1 < strArr.length) {
                        hashMap.put(strArr[i], strArr[i + 1]);
                    }
                }
                MobclickAgent.onEvent(k.getActivity(), strArr[0], hashMap);
            }
        }
        return false;
    }

    public final String getName() {
        return "Umeng";
    }

    public final void onDestroy() {
    }
}
