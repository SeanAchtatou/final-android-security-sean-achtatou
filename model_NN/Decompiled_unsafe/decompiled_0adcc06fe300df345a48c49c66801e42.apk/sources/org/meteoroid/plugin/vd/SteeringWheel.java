package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import com.a.a.e.c;

public class SteeringWheel extends AbstractRoundWidget {
    private static final int ANTICLOCKWISE = 2;
    private static final int CLOCKWISE = 1;
    private static final int LEFT = 0;
    private static final int RIGHT = 1;
    private static final int UNKNOWN = 0;
    private static final VirtualKey[] hp = new VirtualKey[2];
    private final Matrix cx = new Matrix();
    private float hq = -1.0f;
    private float hr = 0.0f;
    public int hs = 135;
    public int ht = 5;
    private int orientation = 0;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.hs = attributeSet.getAttributeIntValue(str, "angleMax", 135);
        this.ht = attributeSet.getAttributeIntValue(str, "angleMin", 5);
        this.gE = c.J(attributeSet.getAttributeValue(str, "bitmap"));
    }

    public final void a(com.a.a.d.c cVar) {
        super.a(cVar);
        hp[0] = new VirtualKey();
        hp[0].hv = "LEFT";
        hp[1] = new VirtualKey();
        hp[1].hv = "RIGHT";
        reset();
    }

    public final boolean e(int i, int i2, int i3, int i4) {
        VirtualKey virtualKey;
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.gL) || sqrt < ((double) this.gM)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                if (this.hq == -1.0f) {
                    this.hq = a((float) i2, (float) i3);
                }
                this.id = i4;
                break;
            case 1:
                if (this.id == i4) {
                    release();
                    break;
                }
                break;
            case 2:
                if (this.id == i4) {
                    this.state = 0;
                    float a = a((float) i2, (float) i3);
                    float f = this.hq;
                    float f2 = a - f;
                    if (this.orientation == 0) {
                        if (Math.abs(f2) > 180.0f) {
                            if (f2 > 0.0f) {
                                this.orientation = 1;
                            } else if (f2 < 0.0f) {
                                this.orientation = 2;
                            }
                        } else if (a > f) {
                            this.orientation = 2;
                        } else if (a < f) {
                            this.orientation = 1;
                        }
                    }
                    if (this.orientation == 1 && f2 > 0.0f) {
                        f2 -= 360.0f;
                    } else if (this.orientation == 2 && f2 < 0.0f) {
                        f2 += 360.0f;
                    }
                    this.hr = f2;
                    if (this.hr > ((float) this.hs)) {
                        this.hr = (float) this.hs;
                    } else if (this.hr < ((float) (this.hs * -1))) {
                        this.hr = (float) (this.hs * -1);
                    }
                    if (this.hr >= ((float) this.ht) && this.hr <= ((float) this.hs)) {
                        virtualKey = hp[0];
                    } else if (this.hr > ((float) (this.ht * -1)) || this.hr < ((float) (this.hs * -1))) {
                        if (this.hr < ((float) this.ht) && this.hr > ((float) (this.ht * -1))) {
                            this.orientation = 0;
                            break;
                        } else {
                            virtualKey = null;
                        }
                    } else {
                        virtualKey = hp[1];
                    }
                    if (this.gN != virtualKey) {
                        if (this.gN != null && this.gN.state == 0) {
                            this.gN.state = 1;
                            VirtualKey.b(this.gN);
                        }
                        this.gN = virtualKey;
                    }
                    if (this.gN != null) {
                        this.gN.state = 0;
                        VirtualKey.b(this.gN);
                        break;
                    }
                }
                break;
        }
        return true;
    }

    public final void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.gH = 0;
            this.gK = true;
        }
        if (this.gK) {
            if (this.gG > 0 && this.state == 1) {
                this.gH++;
                this.bT.setAlpha(255 - ((this.gH * 255) / this.gG));
                if (this.gH >= this.gG) {
                    this.gH = 0;
                    this.gK = false;
                }
            }
            if (this.state == 1) {
                if (this.hr > 0.0f) {
                    this.hr -= 15.0f;
                    this.hr = this.hr < 0.0f ? 0.0f : this.hr;
                } else if (this.hr < 0.0f) {
                    this.hr += 15.0f;
                    this.hr = this.hr > 0.0f ? 0.0f : this.hr;
                }
            }
            if (this.gE != null) {
                canvas.save();
                if (this.hr != 0.0f) {
                    this.cx.setRotate(this.hr * -1.0f, (float) (this.gE[this.state].getWidth() >> 1), (float) (this.gE[this.state].getHeight() >> 1));
                }
                canvas.translate((float) (this.centerX - (this.gE[this.state].getWidth() >> 1)), (float) (this.centerY - (this.gE[this.state].getHeight() >> 1)));
                canvas.drawBitmap(this.gE[this.state], this.cx, null);
                canvas.restore();
                this.cx.reset();
            }
        }
    }

    public final void reset() {
        this.state = 1;
        this.hq = -1.0f;
        this.orientation = 0;
    }
}
