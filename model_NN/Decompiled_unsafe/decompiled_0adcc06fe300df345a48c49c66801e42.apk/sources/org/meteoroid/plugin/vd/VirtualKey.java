package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import org.meteoroid.core.h;

public class VirtualKey extends AbstractButton {
    public static final int MSG_VIRTUAL_KEY_EVENT = 7833601;
    public String hv;
    private boolean hw = true;

    public static final synchronized void b(VirtualKey virtualKey) {
        synchronized (VirtualKey.class) {
            h.b(h.a(MSG_VIRTUAL_KEY_EVENT, virtualKey));
        }
    }

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.hv = attributeSet.getAttributeValue(str, "keyname");
        this.hw = attributeSet.getAttributeBooleanValue(str, "clickThrough", true);
    }

    public final String bc() {
        return this.hv;
    }

    public final boolean e(int i, int i2, int i3, int i4) {
        if (this.gC.contains(i2, i3)) {
            switch (i) {
                case 0:
                case 2:
                    this.id = i4;
                    this.state = 0;
                    b(this);
                    break;
                case 1:
                    this.id = -1;
                    this.state = 1;
                    b(this);
                    break;
            }
            if (!this.hw) {
                return true;
            }
        } else if (this.state == 0 && this.id == i4) {
            this.id = -1;
            this.state = 1;
            b(this);
        }
        return false;
    }
}
