package org.meteoroid.plugin.vd;

import org.meteoroid.core.g;

public final class MuteSwitcher extends BooleanSwitcher {
    public final void aU() {
        g.b(true);
    }

    public final void aV() {
        g.b(false);
    }
}
