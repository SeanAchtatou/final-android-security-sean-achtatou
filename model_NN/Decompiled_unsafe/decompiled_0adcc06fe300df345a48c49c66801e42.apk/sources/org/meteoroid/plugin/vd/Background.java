package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.a.a.d.c;

public class Background implements c.a {
    int color = -16777216;
    private c eG;
    private Paint fx;
    private boolean gK = true;
    Bitmap gP;
    Rect rect;

    public void a(AttributeSet attributeSet, String str) {
        this.rect = com.a.a.e.c.H(attributeSet.getAttributeValue(str, "rect"));
        String attributeValue = attributeSet.getAttributeValue(str, "bitmap");
        if (attributeValue != null) {
            this.gP = com.a.a.e.c.I(attributeValue);
        } else {
            this.color = Integer.valueOf(attributeSet.getAttributeValue(str, "color"), 16).intValue();
        }
    }

    public final void a(c cVar) {
        this.eG = cVar;
        if (this.gP == null) {
            this.fx = new Paint();
            this.fx.setAntiAlias(true);
            this.fx.setStyle(Paint.Style.FILL);
        }
    }

    public final boolean aK() {
        return true;
    }

    public final c aR() {
        return this.eG;
    }

    public final boolean d(int i, int i2, int i3, int i4) {
        return false;
    }

    public final boolean isTouchable() {
        return false;
    }

    public void onDraw(Canvas canvas) {
        if (this.gK) {
            if (this.gP == null) {
                this.fx.setColor(this.color);
                canvas.drawRect(this.rect, this.fx);
                return;
            }
            canvas.drawBitmap(this.gP, (Rect) null, this.rect, (Paint) null);
        }
    }

    public final void setVisible(boolean z) {
        this.gK = z;
    }
}
