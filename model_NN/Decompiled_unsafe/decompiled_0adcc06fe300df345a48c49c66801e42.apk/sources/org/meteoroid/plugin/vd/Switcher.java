package org.meteoroid.plugin.vd;

public abstract class Switcher extends AbstractButton {
    public abstract void aT();

    public final boolean e(int i, int i2, int i3, int i4) {
        if (!this.gC.contains(i2, i3) || i != 0) {
            return false;
        }
        aT();
        return false;
    }
}
