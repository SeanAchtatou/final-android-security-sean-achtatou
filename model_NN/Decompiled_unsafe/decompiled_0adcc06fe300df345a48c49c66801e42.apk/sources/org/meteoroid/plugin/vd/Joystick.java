package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import com.a.a.e.c;

public class Joystick extends AbstractRoundWidget {
    public static final int EIGHT_DIR_CONTROL = 8;
    public static final int EIGHT_DIR_CONTROL_NUM = 10;
    public static final int FOUR_DIR_CONTROL = 4;
    public static final int FOUR_DIR_CONTROL_NUM = 6;
    private static final VirtualKey[] gO = new VirtualKey[5];
    private static final VirtualKey[] gY = new VirtualKey[9];
    private boolean gK = true;
    int gZ;
    int ha;
    Bitmap[] hb;
    Bitmap[] hc;
    int mode;

    static VirtualKey a(float f, int i) {
        return i == 8 ? gY[(int) ((((double) f) + 22.5d) / 45.0d)] : gO[(int) ((45.0f + f) / 90.0f)];
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.hb = c.J(attributeSet.getAttributeValue(str, "stick"));
        this.hc = c.J(attributeSet.getAttributeValue(str, "base"));
        this.mode = attributeSet.getAttributeIntValue(str, "mode", 4);
    }

    public final void a(com.a.a.d.c cVar) {
        super.a(cVar);
        if (this.mode == 8) {
            gY[0] = new VirtualKey();
            gY[0].hv = "RIGHT";
            gY[1] = new VirtualKey();
            gY[1].hv = "NUM_3";
            gY[2] = new VirtualKey();
            gY[2].hv = "UP";
            gY[3] = new VirtualKey();
            gY[3].hv = "NUM_1";
            gY[4] = new VirtualKey();
            gY[4].hv = "LEFT";
            gY[5] = new VirtualKey();
            gY[5].hv = "NUM_7";
            gY[6] = new VirtualKey();
            gY[6].hv = "DOWN";
            gY[7] = new VirtualKey();
            gY[7].hv = "NUM_9";
            gY[8] = gY[0];
        } else if (this.mode == 10) {
            gY[0] = new VirtualKey();
            gY[0].hv = "NUM_6";
            gY[1] = new VirtualKey();
            gY[1].hv = "NUM_3";
            gY[2] = new VirtualKey();
            gY[2].hv = "NUM_2";
            gY[3] = new VirtualKey();
            gY[3].hv = "NUM_1";
            gY[4] = new VirtualKey();
            gY[4].hv = "NUM_4";
            gY[5] = new VirtualKey();
            gY[5].hv = "NUM_7";
            gY[6] = new VirtualKey();
            gY[6].hv = "NUM_8";
            gY[7] = new VirtualKey();
            gY[7].hv = "NUM_9";
            gY[8] = gY[0];
        } else if (this.mode == 6) {
            gO[0] = new VirtualKey();
            gO[0].hv = "NUM_6";
            gO[1] = new VirtualKey();
            gO[1].hv = "NUM_2";
            gO[2] = new VirtualKey();
            gO[2].hv = "NUM_4";
            gO[3] = new VirtualKey();
            gO[3].hv = "NUM_8";
            gO[4] = gO[0];
        } else {
            gO[0] = new VirtualKey();
            gO[0].hv = "RIGHT";
            gO[1] = new VirtualKey();
            gO[1].hv = "UP";
            gO[2] = new VirtualKey();
            gO[2].hv = "LEFT";
            gO[3] = new VirtualKey();
            gO[3].hv = "DOWN";
            gO[4] = gO[0];
        }
        reset();
    }

    public final boolean aK() {
        return (this.hc == null && this.hb == null) ? false : true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean e(int i, int i2, int i3, int i4) {
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.gL) || sqrt < ((double) this.gM)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                this.id = i4;
                break;
            case 1:
                if (this.id != i4) {
                    return true;
                }
                release();
                return true;
            case 2:
                break;
            default:
                return true;
        }
        if (this.id != i4) {
            return true;
        }
        this.state = 0;
        this.gZ = i2;
        this.ha = i3;
        VirtualKey a = a(a((float) i2, (float) i3), this.mode);
        if (this.gN != a) {
            if (this.gN != null && this.gN.state == 0) {
                this.gN.state = 1;
                VirtualKey.b(this.gN);
            }
            this.gN = a;
        }
        this.gN.state = 0;
        VirtualKey.b(this.gN);
        return true;
    }

    public void onDraw(Canvas canvas) {
        Paint paint = null;
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.gH = 0;
            this.gK = true;
        }
        if (this.gK) {
            if (this.gG > 0 && this.state == 1) {
                this.gH++;
                this.bT.setAlpha(255 - ((this.gH * 255) / this.gG));
                if (this.gH >= this.gG) {
                    this.gH = 0;
                    this.gK = false;
                }
            }
            if (a(this.hc)) {
                canvas.drawBitmap(this.hc[this.state], (float) (this.centerX - (this.hc[this.state].getWidth() / 2)), (float) (this.centerY - (this.hc[this.state].getHeight() / 2)), (this.gG == -1 || this.state != 1) ? null : this.bT);
            }
            if (a(this.hb)) {
                Bitmap bitmap = this.hb[this.state];
                float width = (float) (this.gZ - (this.hb[this.state].getWidth() / 2));
                float height = (float) (this.ha - (this.hb[this.state].getHeight() / 2));
                if (this.gG != -1 && this.state == 1) {
                    paint = this.bT;
                }
                canvas.drawBitmap(bitmap, width, height, paint);
            }
        }
    }

    public final void reset() {
        this.gZ = this.centerX;
        this.ha = this.centerY;
        this.state = 1;
    }

    public final void setVisible(boolean z) {
        this.gK = z;
    }
}
