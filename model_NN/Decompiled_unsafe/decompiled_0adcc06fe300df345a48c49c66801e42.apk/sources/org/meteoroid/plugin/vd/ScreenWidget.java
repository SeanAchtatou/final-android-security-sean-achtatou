package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;
import com.a.a.d.c;

public abstract class ScreenWidget implements c.a {
    private final Rect hd = new Rect();
    public final Paint he = new Paint();
    public Rect hf;
    private float hg;
    private float hh;
    private boolean hi;

    public abstract void a(int i, float f, float f2, int i2);

    public final void a(Rect rect) {
        this.hd.left = rect.left;
        this.hd.top = rect.top;
        this.hd.right = rect.right;
        this.hd.bottom = rect.bottom;
    }

    public final boolean aK() {
        return true;
    }

    public abstract Bitmap aN();

    public final Rect aZ() {
        return this.hd;
    }

    public final void ba() {
        if (aN() != null) {
            this.hg = ((float) this.hd.width()) / ((float) aN().getWidth());
            this.hh = ((float) this.hd.height()) / ((float) aN().getHeight());
            if (this.hg == 1.0f && this.hh == 1.0f) {
                this.he.setFilterBitmap(false);
            } else {
                this.he.setFilterBitmap(true);
            }
            "Scale Screen to " + this.hd.width() + "x" + this.hd.height();
        }
    }

    public final void d(boolean z) {
        if (this.hg != 1.0f || this.hh != 1.0f) {
            this.he.setFilterBitmap(z);
        }
    }

    public final boolean d(int i, int i2, int i3, int i4) {
        if (!this.hd.contains(i2, i3) || !this.hi) {
            return false;
        }
        a(i, ((float) (i2 - this.hd.left)) / this.hg, ((float) (i3 - this.hd.top)) / this.hh, i4);
        return false;
    }

    public final boolean isTouchable() {
        return this.hi;
    }

    public final void setTouchable(boolean z) {
        this.hi = z;
    }
}
