package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import org.meteoroid.core.h;
import org.meteoroid.core.k;

public final class URLButton extends SimpleButton {
    public String[] hu;
    private int index;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.hu = attributeSet.getAttributeValue(str, "value").split(",");
    }

    public final void onClick() {
        if (this.hu != null && this.hu[this.index] != null) {
            h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"URLClick", k.ak() + "=" + this.hu[this.index]});
            k.v(this.hu[this.index]);
        }
    }
}
