package org.meteoroid.plugin.vd;

import android.util.AttributeSet;

public abstract class BooleanSwitcher extends Switcher {
    public static final int OFF = 1;
    public static final int ON = 0;

    private void aS() {
        if (this.state == 0) {
            aU();
        } else {
            aV();
        }
    }

    public void a(AttributeSet attributeSet, String str) {
        int i = 0;
        super.a(attributeSet, str);
        if (attributeSet.getAttributeBooleanValue(str, "value", false)) {
            i = 1;
        }
        this.state = i;
        aS();
    }

    public final void aT() {
        int i = 1;
        if (this.state == 1) {
            i = 0;
        }
        this.state = i;
        aS();
    }

    public abstract void aU();

    public abstract void aV();
}
