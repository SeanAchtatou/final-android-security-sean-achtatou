package org.meteoroid.plugin.vd;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.AttributeSet;
import android.util.Log;
import com.a.a.e.c;
import org.meteoroid.core.f;
import org.meteoroid.core.m;

public final class SensorSwitcher extends BooleanSwitcher implements SensorEventListener {
    private static final int DOWN = 1;
    private static final int LEFT = 2;
    private static final int RIGHT = 3;
    private static final int UP = 0;
    private static final VirtualKey[] gO = new VirtualKey[4];
    private int gV;
    private int[] hj;
    private int hk = 2;
    private int hl = 2;
    private VirtualKey hm;
    private final int[] hn = new int[3];
    private int ho = 0;
    private int x;
    private int y;
    private int z;

    private synchronized void a(VirtualKey virtualKey) {
        if (virtualKey != this.hm) {
            bb();
        }
        if (virtualKey != null && virtualKey.state == 1) {
            virtualKey.state = 0;
            VirtualKey.b(virtualKey);
            this.hm = virtualKey;
        }
        if (virtualKey != null) {
            "sensor vb:[" + virtualKey.hv + "|" + virtualKey.state + "]";
        }
    }

    private void bb() {
        if (this.hm != null && this.hm.state == 0) {
            this.hm.state = 1;
            VirtualKey.b(this.hm);
            this.hm = null;
        }
    }

    public final void a(AttributeSet attributeSet, String str) {
        this.gE = c.J(attributeSet.getAttributeValue(str, "bitmap"));
        String attributeValue = attributeSet.getAttributeValue(str, "touch");
        if (attributeValue != null) {
            this.gC = c.H(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            this.gD = c.H(attributeValue2);
        }
        this.state = 1;
        this.gG = attributeSet.getAttributeIntValue(str, "fade", -1);
        String attributeValue3 = attributeSet.getAttributeValue(str, "value");
        if (attributeValue3 != null) {
            String[] split = attributeValue3.split(",");
            if (split.length > 0) {
                try {
                    if (Integer.parseInt(split[0]) == 0) {
                        aU();
                    }
                } catch (NumberFormatException e) {
                    if (Boolean.parseBoolean(split[0])) {
                        aU();
                    }
                }
            }
            if (split.length >= 3) {
                this.hk = Integer.parseInt(split[1]);
                this.hl = Integer.parseInt(split[2]);
            }
            if (split.length >= 6) {
                this.hj = new int[3];
                this.hj[0] = Integer.parseInt(split[3]);
                this.hj[1] = Integer.parseInt(split[4]);
                this.hj[2] = Integer.parseInt(split[5]);
            }
        }
    }

    public final void a(com.a.a.d.c cVar) {
        super.a(cVar);
        gO[0] = new VirtualKey();
        gO[0].hv = "UP";
        gO[0].state = 1;
        gO[1] = new VirtualKey();
        gO[1].hv = "DOWN";
        gO[1].state = 1;
        gO[2] = new VirtualKey();
        gO[2].hv = "LEFT";
        gO[2].state = 1;
        gO[3] = new VirtualKey();
        gO[3].hv = "RIGHT";
        gO[3].state = 1;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.ho = 0;
        this.hn[0] = 0;
        this.hn[1] = 0;
        this.hn[2] = 0;
        bb();
    }

    public final void aU() {
        f.a((SensorEventListener) this);
        Log.w("SensorSwitcher", "Switch on.");
    }

    public final void aV() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.ho = 0;
        this.hn[0] = 0;
        this.hn[1] = 0;
        this.hn[2] = 0;
        bb();
        f.b((SensorEventListener) this);
        Log.w("SensorSwitcher", "Switch off.");
    }

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        "sensor:[" + ((int) sensorEvent.values[0]) + "|" + ((int) sensorEvent.values[1]) + "|" + ((int) sensorEvent.values[2]) + "]";
        this.gV = m.eG.getOrientation();
        if (this.gV == 1) {
            this.x = -((int) sensorEvent.values[0]);
            this.y = (int) sensorEvent.values[1];
        } else if (this.gV == 0) {
            this.x = (int) sensorEvent.values[1];
            this.y = (int) sensorEvent.values[0];
        } else {
            Log.w("SensorSwitcher", "deviceOrientation:" + this.gV);
            return;
        }
        this.z = (int) sensorEvent.values[2];
        if (this.ho <= 0) {
            if (this.hj != null) {
                this.hn[0] = this.hj[0];
                this.hn[1] = this.hj[1];
                this.hn[2] = this.hj[2];
            } else {
                this.hn[0] = this.x;
                this.hn[1] = this.y;
                this.hn[2] = this.z;
            }
            "sensor_init:[" + this.hn[0] + "|" + this.hn[1] + "|" + this.hn[2] + "]";
            this.ho++;
            return;
        }
        int i = this.x - this.hn[0];
        int i2 = this.y - this.hn[1];
        int i3 = this.z - this.hn[2];
        if (Math.abs(i) < this.hk && Math.abs(i2) < this.hl && Math.abs(i3) < this.hl) {
            bb();
        } else if (Math.abs(i) >= this.hk) {
            if (i < 0) {
                a(gO[2]);
            } else {
                a(gO[3]);
            }
        } else if (Math.abs(i2) >= this.hl) {
            if (i2 > 0) {
                a(gO[1]);
            } else {
                a(gO[0]);
            }
            a(this.hm);
        } else if (Math.abs(i3) < this.hl) {
        } else {
            if (i3 > 0) {
                a(gO[0]);
            } else {
                a(gO[1]);
            }
        }
    }
}
