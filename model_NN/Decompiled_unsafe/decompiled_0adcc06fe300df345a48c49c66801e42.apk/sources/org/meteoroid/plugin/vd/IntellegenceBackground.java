package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import com.a.a.e.c;

public final class IntellegenceBackground extends Background {
    public static final int SCREEN_BOTTOM = 1;
    public static final int SCREEN_LEFT = 2;
    public static final int SCREEN_RIGHT = 3;
    public static final int SCREEN_TOP = 0;
    int mode;

    public final void a(AttributeSet attributeSet, String str) {
        this.rect = c.H(attributeSet.getAttributeValue(str, "rect"));
        this.mode = attributeSet.getAttributeIntValue(str, "mode", 0);
    }

    public final void onDraw(Canvas canvas) {
        Bitmap aN;
        if (!(aR() == null || (aN = ((DefaultVirtualDevice) aR()).aX().aN()) == null)) {
            int i = -16777216;
            int width = aN.getWidth();
            int height = aN.getHeight();
            switch (this.mode) {
                case 0:
                    i = aN.getPixel(width >> 1, 0);
                    break;
                case 1:
                    i = aN.getPixel(width >> 1, height - 1);
                    break;
                case 2:
                    i = aN.getPixel(0, height >> 1);
                    break;
                case 3:
                    i = aN.getPixel(width - 1, height >> 1);
                    break;
            }
            if (this.rect != null) {
                this.color = i;
            }
        }
        super.onDraw(canvas);
    }
}
