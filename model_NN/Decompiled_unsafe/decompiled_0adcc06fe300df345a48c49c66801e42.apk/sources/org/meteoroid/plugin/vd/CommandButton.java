package org.meteoroid.plugin.vd;

import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import org.meteoroid.core.h;
import org.meteoroid.core.i;
import org.meteoroid.core.k;
import org.meteoroid.plugin.feature.AbstractAdvertisement;
import org.meteoroid.plugin.feature.AbstractDownloadAndInstall;

public final class CommandButton extends SimpleButton {
    public static final int MSG_APPLOTTERY = 74710;
    public static final int MSG_CHECKIN = -2004318072;
    public static final int MSG_DISABLE_AD = 9520139;
    public static final int MSG_REQUEST_RELOAD_SKIN = 952848;
    public static final int MSG_SOCIAL_PLATFORM_LAUNCH = 1013249;
    public static final int MSG_TIME_UP = 9520138;
    private String gQ;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.gQ = attributeSet.getAttributeValue(str, "value").trim();
        h.b((int) MSG_APPLOTTERY, "APPLOTTERY");
        h.b((int) MSG_CHECKIN, "CHECKIN");
        h.b((int) MSG_SOCIAL_PLATFORM_LAUNCH, "MSG_SOCIAL_PLATFORM_LAUNCH");
        h.b((int) MSG_REQUEST_RELOAD_SKIN, "MSG_REQUEST_RELOAD_SKIN");
        h.b((int) MSG_TIME_UP, "MSG_TIME_UP");
        h.b((int) MSG_DISABLE_AD, "MSG_DISABLE_AD");
    }

    public final void onClick() {
        int indexOf;
        String str = null;
        if (this.gQ.startsWith("CMD_EXIT")) {
            k.al();
        } else if (this.gQ.equalsIgnoreCase("CMD_ABOUT")) {
            h.o(i.MSG_OPTIONMENU_ABOUT);
        } else if (this.gQ.equalsIgnoreCase("CMD_MENU")) {
            k.getHandler().post(new Runnable() {
                public final void run() {
                    k.getActivity().openOptionsMenu();
                }
            });
        } else if (this.gQ.startsWith("CMD_CHECKIN")) {
            h.o(MSG_CHECKIN);
        } else if (this.gQ.startsWith("CMD_APPLOTTERY")) {
            h.o(MSG_APPLOTTERY);
        } else if (this.gQ.startsWith("CMD_SNS")) {
            h.o(MSG_SOCIAL_PLATFORM_LAUNCH);
        } else if (this.gQ.startsWith("CMD_RELOADSKIN")) {
            h.o(MSG_REQUEST_RELOAD_SKIN);
        } else if (this.gQ.startsWith("CMD_TIMEUP")) {
            h.o(MSG_TIME_UP);
        } else if (this.gQ.startsWith("CMD_INSTALLAPP")) {
            AnonymousClass2 r1 = new AbstractDownloadAndInstall() {
                public final String getName() {
                    return "AbstractDownloadAndInstall";
                }

                public final String p() {
                    return "AbstractDownloadAndInstall";
                }
            };
            String str2 = this.gQ;
            int indexOf2 = str2.indexOf(40);
            if (!(indexOf2 == -1 || (indexOf = str2.indexOf(41, indexOf2)) == -1)) {
                str = str2.substring(indexOf2 + 1, indexOf);
            }
            r1.A(str);
            r1.gd = true;
            if (r1.aP().isEmpty()) {
                k.a("您已经安装了该应用", 0);
            } else {
                r1.a(r1.aP().get(0));
            }
        } else if (!this.gQ.startsWith("CMD_DISABLE_AD")) {
            Log.w("CommandButton", "NULL command:" + this.gQ);
        } else if (AbstractAdvertisement.x(k.getActivity())) {
            k.a("广告功能已经关闭", 0);
        } else {
            h.a(new h.a() {
                public final boolean a(Message message) {
                    if (message.what != 61698) {
                        return false;
                    }
                    h.o(CommandButton.MSG_DISABLE_AD);
                    k.a("广告功能关闭成功", 0);
                    return true;
                }
            });
            h.o(15391744);
        }
    }
}
