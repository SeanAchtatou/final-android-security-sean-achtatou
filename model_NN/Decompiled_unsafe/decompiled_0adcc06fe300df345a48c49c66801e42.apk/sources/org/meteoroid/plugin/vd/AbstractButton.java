package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.a.a.d.c;

public abstract class AbstractButton implements c.a {
    public static final int PRESSED = 0;
    public static final int RELEASED = 1;
    public static final int UNAVAILABLE_POINTER_ID = -1;
    Paint bT = new Paint();
    private int count;
    int delay;
    private c eG;
    int fQ;
    Rect gC;
    Rect gD;
    Bitmap[] gE;
    boolean gF;
    int gG;
    int gH;
    boolean gI = true;
    private int gJ;
    boolean gK = true;
    int id = -1;
    int state = 1;

    public void a(AttributeSet attributeSet, String str) {
        this.gE = com.a.a.e.c.J(attributeSet.getAttributeValue(str, "bitmap"));
        String attributeValue = attributeSet.getAttributeValue(str, "touch");
        if (attributeValue != null) {
            this.gC = com.a.a.e.c.H(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            this.gD = com.a.a.e.c.H(attributeValue2);
        }
        String attributeValue3 = attributeSet.getAttributeValue(str, "fade");
        if (attributeValue3 != null) {
            String[] split = attributeValue3.split(",");
            if (split.length > 0) {
                this.gG = Integer.parseInt(split[0]);
            } else {
                this.gG = -1;
            }
            if (split.length >= 2) {
                this.delay = Integer.parseInt(split[1]);
            }
        }
        if (this.gE != null && this.gE.length > 2) {
            this.gF = true;
            this.fQ = attributeSet.getAttributeIntValue(str, "interval", 40);
        }
    }

    public void a(c cVar) {
        this.eG = cVar;
        if (this.gC == null) {
            this.gC = this.gD;
        }
    }

    public final boolean a(Bitmap[] bitmapArr) {
        return bitmapArr != null && this.state >= 0 && this.state <= bitmapArr.length + -1 && bitmapArr[this.state] != null;
    }

    public boolean aK() {
        return this.gE != null;
    }

    public final int aQ() {
        return this.state;
    }

    public final boolean d(int i, int i2, int i3, int i4) {
        return e(i, i2, i3, i4);
    }

    public abstract boolean e(int i, int i2, int i3, int i4);

    public final boolean isTouchable() {
        return true;
    }

    public void onDraw(Canvas canvas) {
        if (this.gF) {
            this.count++;
            if (this.count >= this.fQ) {
                this.count = 0;
                this.gJ++;
                if (this.gE != null && this.gJ >= this.gE.length) {
                    this.gJ = 0;
                }
            }
        } else if (this.delay > 0) {
            this.delay--;
            return;
        } else {
            if (this.state == 0) {
                this.gH = 0;
                this.gK = true;
            }
            if (this.gK) {
                if (this.gG > 0 && this.state == 1) {
                    this.gH++;
                    this.bT.setAlpha(255 - ((this.gH * 255) / this.gG));
                    if (this.gH >= this.gG) {
                        this.gH = 0;
                        this.gK = false;
                    }
                }
                this.gJ = this.state;
            } else {
                return;
            }
        }
        if (a(this.gE)) {
            canvas.drawBitmap(this.gE[this.gJ], (Rect) null, this.gD, (this.gG == -1 || this.state != 1) ? null : this.bT);
        }
    }

    public void setVisible(boolean z) {
        this.gK = z;
    }
}
