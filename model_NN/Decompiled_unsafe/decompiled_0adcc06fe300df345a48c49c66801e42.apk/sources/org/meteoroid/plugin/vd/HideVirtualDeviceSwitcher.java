package org.meteoroid.plugin.vd;

import com.a.a.d.c;
import java.util.Iterator;
import org.meteoroid.core.m;

public class HideVirtualDeviceSwitcher extends BooleanSwitcher {
    public final void aU() {
        DefaultVirtualDevice defaultVirtualDevice = (DefaultVirtualDevice) m.eG;
        Iterator<c.a> it = defaultVirtualDevice.aW().iterator();
        while (it.hasNext()) {
            c.a next = it.next();
            if (next != defaultVirtualDevice.aX()) {
                next.setVisible(false);
            }
        }
    }

    public final void aV() {
        DefaultVirtualDevice defaultVirtualDevice = (DefaultVirtualDevice) m.eG;
        Iterator<c.a> it = defaultVirtualDevice.aW().iterator();
        while (it.hasNext()) {
            c.a next = it.next();
            if (next != defaultVirtualDevice.aX()) {
                next.setVisible(true);
            }
        }
    }

    public final void setVisible(boolean z) {
    }
}
