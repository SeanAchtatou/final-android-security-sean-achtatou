package org.meteoroid.plugin.vd;

import org.meteoroid.core.k;

public class CallOptionMenu extends SimpleButton {
    private boolean cI;

    public final void onClick() {
        if (this.cI) {
            k.getActivity().closeOptionsMenu();
        } else {
            k.getActivity().openOptionsMenu();
        }
        this.cI = !this.cI;
    }
}
