package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import com.a.a.e.c;

public class ArcadeJoyStick extends AbstractRoundWidget {
    public static final int CENTER = 0;
    public static final int DOWN = 2;
    public static final int EIGHT_DIR_CONTROL = 8;
    public static final int FOUR_DIR_CONTROL = 4;
    public static final int LEFT = 3;
    public static final int RIGHT = 4;
    public static final int UP = 1;
    private static final VirtualKey[] gO = new VirtualKey[5];
    private boolean gK = true;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.gE = c.J(attributeSet.getAttributeValue(str, "bitmap"));
    }

    public final void a(com.a.a.d.c cVar) {
        super.a(cVar);
        gO[0] = new VirtualKey();
        gO[0].hv = "RIGHT";
        gO[1] = new VirtualKey();
        gO[1].hv = "UP";
        gO[2] = new VirtualKey();
        gO[2].hv = "LEFT";
        gO[3] = new VirtualKey();
        gO[3].hv = "DOWN";
        gO[4] = gO[0];
        this.state = 0;
    }

    public final boolean aK() {
        return this.gE != null;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean e(int i, int i2, int i3, int i4) {
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.gL) || sqrt < ((double) this.gM)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                this.id = i4;
                break;
            case 1:
                if (this.id != i4) {
                    return true;
                }
                release();
                return true;
            case 2:
                break;
            default:
                return true;
        }
        if (this.id != i4) {
            return true;
        }
        float a = a((float) i2, (float) i3);
        this.state = (a < 45.0f || a >= 135.0f) ? (a < 135.0f || a >= 225.0f) ? (a < 225.0f || a >= 315.0f) ? (a >= 315.0f || a < 45.0f) ? 4 : 0 : 2 : 3 : 1;
        VirtualKey virtualKey = gO[(int) ((a + 45.0f) / 90.0f)];
        if (this.gN != virtualKey) {
            if (this.gN != null && this.gN.state == 0) {
                this.gN.state = 1;
                VirtualKey.b(this.gN);
            }
            this.gN = virtualKey;
        }
        this.gN.state = 0;
        VirtualKey.b(this.gN);
        return true;
    }

    public final void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.gH = 0;
            this.gK = true;
        }
        if (this.gK) {
            if (this.gG > 0 && this.state == 1) {
                this.gH++;
                this.bT.setAlpha(255 - ((this.gH * 255) / this.gG));
                if (this.gH >= this.gG) {
                    this.gH = 0;
                    this.gK = false;
                }
            }
            if (a(this.gE)) {
                canvas.drawBitmap(this.gE[this.state], (float) (this.centerX - (this.gE[this.state].getWidth() / 2)), (float) (this.centerY - (this.gE[this.state].getHeight() / 2)), (Paint) null);
            }
        }
    }

    public final void reset() {
        this.state = 0;
    }

    public final void setVisible(boolean z) {
        this.gK = z;
    }
}
