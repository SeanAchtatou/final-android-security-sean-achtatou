package org.meteoroid.plugin.vd;

import android.util.Log;
import android.view.Display;
import com.a.a.d.c;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Properties;
import org.meteoroid.core.e;
import org.meteoroid.core.f;
import org.meteoroid.core.h;
import org.meteoroid.core.k;

public class DefaultVirtualDevice implements c {
    public static final int WIDGET_TYPE_ARCADE_JOYSTICK = 11;
    public static final int WIDGET_TYPE_BACKGROUND = 0;
    public static final int WIDGET_TYPE_CALL_OPTIONMENU = 12;
    public static final int WIDGET_TYPE_CHECKIN = 13;
    public static final int WIDGET_TYPE_DEVICE_SCREEN = 2;
    public static final int WIDGET_TYPE_DYNAMICJOYSTICK = 15;
    public static final int WIDGET_TYPE_EXIT_BUTTON = 6;
    public static final int WIDGET_TYPE_HIDE_VD = 10;
    public static final int WIDGET_TYPE_INTELEGENCE_BG = 7;
    public static final int WIDGET_TYPE_JOYSTICK = 3;
    public static final int WIDGET_TYPE_MUTE_SWITCHER = 4;
    public static final int WIDGET_TYPE_SENSOR_SWITCHER = 5;
    public static final int WIDGET_TYPE_SNSBUTTON = 14;
    public static final int WIDGET_TYPE_STEERINGWHEEL = 9;
    public static final int WIDGET_TYPE_URL_BUTTON = 8;
    public static final int WIDGET_TYPE_VIRTUAL_BUTTON = 1;
    public static final String[] gU = {"Background", "VirtualKey", "ScreenWidget", "Joystick", "MuteSwitcher", "SensorSwitcher", "CommandButton", "IntellegenceBackground", "URLButton", "SteeringWheel", "HideVirtualDeviceSwitcher", "ArcadeJoyStick", "CallOptionMenu", "CheckinButton", "SNSButton", "DynamicJoystick"};
    private final LinkedHashSet<c.a> gS = new LinkedHashSet<>();
    private ScreenWidget gT;
    private int gV;

    private void a(Properties properties) {
        ScreenWidget screenWidget;
        if (properties.containsKey("widget.orientation")) {
            String property = properties.getProperty("widget.orientation");
            this.gV = 2;
            if (property.equals("landscape")) {
                this.gV = 0;
            } else if (property.equals("portrait")) {
                this.gV = 1;
            } else if (property.equals("auto")) {
                this.gV = 4;
            } else {
                Log.w(c.LOG_TAG, "Orientation not specificed. It will be decided by user. ");
            }
            k.q(this.gV);
        }
        if (properties.containsKey("widget.num")) {
            int parseInt = Integer.parseInt(properties.getProperty("widget.num"));
            a aVar = new a(properties);
            for (int i = 1; i <= parseInt; i++) {
                try {
                    if (properties.containsKey("widget." + i + ".type")) {
                        String property2 = properties.getProperty("widget." + i + ".type");
                        try {
                            int parseInt2 = Integer.parseInt(property2);
                            if (parseInt2 < 0 || parseInt2 >= gU.length) {
                                Log.w(c.LOG_TAG, "Unknown widget type:" + parseInt2);
                            } else {
                                property2 = gU[parseInt2];
                                if (property2.equals(gU[2])) {
                                    screenWidget = (ScreenWidget) org.meteoroid.core.c.ds;
                                } else {
                                    "Construct a [" + property2 + "] widget.";
                                    screenWidget = (c.a) Class.forName("org.meteoroid.plugin.vd." + property2).newInstance();
                                }
                                screenWidget.a(aVar, "widget." + i + ".");
                                if (!this.gS.contains(screenWidget)) {
                                    screenWidget.a(this);
                                    if (screenWidget instanceof ScreenWidget) {
                                        ScreenWidget screenWidget2 = (ScreenWidget) screenWidget;
                                        screenWidget2.ba();
                                        if (this.gT != null) {
                                            ScreenWidget screenWidget3 = this.gT;
                                            this.gS.remove(screenWidget3);
                                            e.b(screenWidget3);
                                            f.b(screenWidget3);
                                        }
                                        this.gT = screenWidget2;
                                    }
                                    this.gS.add(screenWidget);
                                    if (screenWidget.aK()) {
                                        e.a(screenWidget);
                                    }
                                    if (screenWidget.isTouchable()) {
                                        f.a(screenWidget);
                                    }
                                }
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        "Widget " + i + " not exist! Checkout it is not missing.";
                    }
                } catch (Exception e2) {
                    Log.w(c.LOG_TAG, "Init widget[" + i + "] error." + e2);
                    e2.printStackTrace();
                }
            }
        }
        properties.clear();
        System.gc();
    }

    private static String aY() {
        Display defaultDisplay = k.getActivity().getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        int min = Math.min(width, height);
        int max = Math.max(width, height);
        "Screen full width is " + min + "px and height is " + max + "px.";
        if (min == 480 && max == 854) {
            return "fwvga";
        }
        if (min == 480 && max == 960) {
            return "uwvga";
        }
        if (min == 800 && max == 960) {
            return "dualwvga";
        }
        if (min == 480 && max > 640 && max <= 800) {
            return "wvga";
        }
        if (min == 360 && max == 640) {
            return "nhd";
        }
        if (min == 320 && max >= 460 && max <= 500) {
            return "hvga";
        }
        if (min == 240 && max == 320) {
            return "qvga";
        }
        if (min == 240 && max <= 400 && max > 320) {
            return "wqvga";
        }
        if (min == 480 && max == 640) {
            return "vga";
        }
        if (min == 600 && max == 800) {
            return "svga";
        }
        if (min <= 600 && min > 540 && max <= 1024 && max > 960) {
            return "wsvga";
        }
        if (min <= 768 && min > 700 && max == 1024) {
            return "xga";
        }
        if (min == 640 && max <= 960 && max >= 800) {
            return "retina";
        }
        if (min == 540 && max <= 960 && max >= 800) {
            return "qhd";
        }
        if (min <= 800 && min > 720 && max <= 1280 && max >= 1100) {
            return "wxga";
        }
        if (min <= 720 && min > 640 && max <= 1280 && max >= 1100) {
            return "720hd";
        }
        if (min <= 768 && min >= 640 && max == 1366) {
            return "hd";
        }
        if (min <= 1080 && min >= 900 && max >= 1760) {
            return "1080hd";
        }
        Log.w(c.LOG_TAG, "Unkown screen resolution:" + min + "x" + max);
        return null;
    }

    public final LinkedHashSet<c.a> aW() {
        return this.gS;
    }

    public final ScreenWidget aX() {
        return this.gT;
    }

    public final int getOrientation() {
        return this.gV;
    }

    public final void onCreate() {
        h.b((int) VirtualKey.MSG_VIRTUAL_KEY_EVENT, "MSG_VIRTUAL_BUTTON_EVENT");
        try {
            Properties properties = new Properties();
            properties.load(k.getActivity().getResources().openRawResource(com.a.a.e.c.G("vd_" + aY())));
            a(properties);
        } catch (IOException e) {
            Log.e(c.LOG_TAG, "Oooooooops, Fail to load virtual device by resolution. Maybe the resolution is odd or missing." + e);
        }
    }

    public final void onDestroy() {
        Iterator<c.a> it = this.gS.iterator();
        while (it.hasNext()) {
            c.a next = it.next();
            e.b(next);
            f.b(next);
        }
        this.gS.clear();
    }

    public final void z(String str) {
        onDestroy();
        com.a.a.e.c.bd();
        if (str == null) {
            onCreate();
            return;
        }
        Properties properties = new Properties();
        try {
            properties.load(k.t(str + File.separator + "res" + File.separator + "raw" + File.separator + "vd_" + aY() + ".properties"));
        } catch (IOException e) {
            Log.e(c.LOG_TAG, "Error in reloading vd:" + str);
        }
        com.a.a.e.c.D(str + File.separator + "res" + File.separator + "drawable-nodpi");
        a(properties);
        com.a.a.e.c.D(null);
    }
}
