package org.meteoroid.plugin.device;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Region;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import com.a.a.a.g;
import com.a.a.a.i;
import com.androidbox.g5msnjhckhdeoe.R;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.TimerTask;
import java.util.regex.Pattern;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.PlayerListener;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.core.f;
import org.meteoroid.core.g;
import org.meteoroid.core.h;
import org.meteoroid.core.k;
import org.meteoroid.core.m;
import org.meteoroid.plugin.vd.DefaultVirtualDevice;
import org.meteoroid.plugin.vd.ScreenWidget;
import org.meteoroid.plugin.vd.VirtualKey;

public class MIDPDevice extends ScreenWidget implements com.a.a.d.a, f.a, f.e, h.a {
    public static final int ASTERISK = 11;
    public static final int AUTO_DETECT = -1;
    public static final int DOWN = 1;
    public static final int FIRE = 4;
    public static final int GAME_A = 5;
    public static final int GAME_B = 6;
    public static final int GAME_C = 7;
    public static final int GAME_D = 8;
    public static final int LEFT = 2;
    public static final int MSG_MIDP_COMMAND_EVENT = 44034;
    public static final int MSG_MIDP_DISPLAY_CALL_SERIALLY = 44035;
    public static final int MSG_MIDP_MIDLET_NOTIFYDESTROYED = 44036;
    public static final int MUTE_SWITCH = 2;
    public static final int POUND = 12;
    public static final int POWER = 0;
    public static final int RIGHT = 3;
    public static final int SENSOR_SWITCH = 1;
    public static final int SOFT1 = 9;
    public static final int SOFT2 = 10;
    public static final int UP = 0;
    public static final int URL = 8;
    public static final String[] eO = {"UP", "DOWN", "LEFT", "RIGHT", "SELECT", "GAME_A", "GAME_B", "GAME_C", "GAME_D", "SOFT1", "SOFT2", "ASTERISK", "POUND", "UP_LEFT", "UP_RIGHT", "DOWN_LEFT", "DOWN_RIGHT"};
    private static final HashMap<String, Integer> eW = new HashMap<>(eO.length);
    private static final Properties fe = new Properties();
    public static String fn = "";
    private boolean eP;
    private boolean eQ;
    private boolean eR;
    private boolean eS;
    private boolean eT;
    private boolean eU;
    private boolean eV;
    private Bitmap eX;
    private Bitmap eY;
    private Bitmap eZ;
    private Canvas fa;
    private d fb;
    private d fc;
    private d fd;
    /* access modifiers changed from: private */
    public volatile int ff = -1;
    /* access modifiers changed from: private */
    public volatile int fg = -1;
    private boolean fh = false;
    private int fi;
    private int fj;
    private int fk;
    private int fl;
    private int fm;
    public boolean fo = false;
    public String fp = "";
    private int height = -1;
    private int width = -1;

    private final class a extends TimerTask {
        private a() {
        }

        /* synthetic */ a(MIDPDevice mIDPDevice, byte b) {
            this();
        }

        public final void run() {
            if (MIDPDevice.this.ff == -1 && MIDPDevice.this.fg != -1) {
                MIDPDevice.this.h(1, MIDPDevice.this.r(MIDPDevice.this.fg));
                int unused = MIDPDevice.this.fg = -1;
            }
        }
    }

    public static final class b {
        public final Paint bT = new Paint(1);
        public Paint.FontMetricsInt ft;
        private final char[] fu = new char[1];

        public b(Typeface typeface, int i, boolean z) {
            this.bT.setTypeface(typeface);
            this.bT.setTextSize((float) i);
            this.bT.setUnderlineText(z);
            this.ft = this.bT.getFontMetricsInt();
        }
    }

    public static final class c {
        public static int SIZE_LARGE = 16;
        public static int SIZE_MEDIUM = 14;
        public static int SIZE_SMALL = 12;
        private static final HashMap<g, b> fv = new HashMap<>();

        public static b b(g gVar) {
            boolean z = true;
            int i = 0;
            b bVar = fv.get(gVar);
            if (bVar != null) {
                return bVar;
            }
            Typeface typeface = Typeface.SANS_SERIF;
            if (gVar.x() == 0) {
                typeface = Typeface.SANS_SERIF;
            } else if (gVar.x() == 32) {
                typeface = Typeface.MONOSPACE;
            } else if (gVar.x() == 64) {
                typeface = Typeface.SANS_SERIF;
            }
            if ((gVar.getStyle() & 0) != 0) {
            }
            int i2 = (gVar.getStyle() & 1) != 0 ? 1 : 0;
            if ((gVar.getStyle() & 2) != 0) {
                i2 |= 2;
            }
            if ((gVar.getStyle() & 4) == 0) {
                z = false;
            }
            if (gVar.getSize() == 8) {
                i = SIZE_SMALL;
            } else if (gVar.getSize() == 0) {
                i = SIZE_MEDIUM;
            } else if (gVar.getSize() == 16) {
                i = SIZE_LARGE;
            }
            b bVar2 = new b(Typeface.create(typeface, i2), i, z);
            fv.put(gVar, bVar2);
            return bVar2;
        }
    }

    public static class d extends com.a.a.a.h {
        private static final DashPathEffect fz = new DashPathEffect(new float[]{5.0f, 5.0f}, 0.0f);
        private Canvas fA;
        private g fB;
        private int fC;
        private Matrix fD;
        private Bitmap fE;
        private int fF;
        private int fG;
        private int fH;
        private int fI;
        private Paint fw;
        private Paint fx;
        private b fy;

        private d() {
            this.fw = new Paint();
            this.fx = new Paint();
            this.fC = 0;
            this.fD = new Matrix();
            this.fF = 0;
            this.fG = 0;
            this.fH = 0;
            this.fI = 0;
            this.fw.setAntiAlias(false);
            this.fw.setStyle(Paint.Style.STROKE);
            this.fx.setAntiAlias(false);
            this.fx.setStyle(Paint.Style.FILL);
        }

        public d(Canvas canvas) {
            this();
            a(canvas);
        }

        private void a(Canvas canvas) {
            this.fA = canvas;
            a(g.w());
            translate(-y(), -z());
            setColor(0);
            if (this.fE != null) {
                c(0, 0, this.fE.getWidth(), this.fE.getHeight());
            } else {
                c(0, 0, org.meteoroid.core.c.ds.getWidth(), org.meteoroid.core.c.ds.getHeight());
            }
        }

        public final void A() {
            a(this.fA);
        }

        public final void a(int i, int i2, int i3, int i4) {
            "drawRect[" + i + "][" + i2 + "][23" + "][29" + "]";
            if (this.fH > 0 && this.fI > 0) {
                this.fA.drawRect((float) i, (float) i2, (float) (i + 23), (float) (i2 + 29), this.fw);
            }
        }

        public final void a(g gVar) {
            if (gVar == null) {
                gVar = g.w();
            }
            this.fB = gVar;
            this.fy = c.b(gVar);
        }

        public final void a(i iVar, int i, int i2, int i3) {
            if (iVar != null && this.fH > 0 && this.fI > 0 && iVar.cv != null) {
                if (i3 == 0) {
                    i3 = 20;
                }
                if ((i3 & 8) != 0) {
                    i -= iVar.width;
                } else if ((i3 & 1) != 0) {
                    i -= iVar.width / 2;
                }
                if ((i3 & 32) != 0) {
                    i2 -= iVar.height;
                } else if ((i3 & 2) != 0) {
                    i2 -= iVar.height / 2;
                }
                this.fA.drawBitmap(iVar.cv, (float) i, (float) i2, (Paint) null);
            }
        }

        public final void a(String str, int i, int i2, int i3) {
            if (this.fH > 0 && this.fI > 0) {
                if (i3 == 0) {
                    i3 = 20;
                }
                if ((i3 & 16) != 0) {
                    i2 -= this.fy.ft.top;
                } else if ((i3 & 32) != 0) {
                    i2 -= this.fy.ft.bottom;
                } else if ((i3 & 2) != 0) {
                    i2 += ((this.fy.ft.descent - this.fy.ft.ascent) / 2) - this.fy.ft.descent;
                }
                if ((i3 & 1) != 0) {
                    this.fy.bT.setTextAlign(Paint.Align.CENTER);
                } else if ((i3 & 8) != 0) {
                    this.fy.bT.setTextAlign(Paint.Align.RIGHT);
                } else if ((i3 & 4) != 0) {
                    this.fy.bT.setTextAlign(Paint.Align.LEFT);
                }
                this.fy.bT.setColor(this.fw.getColor());
                this.fA.drawText(str, (float) i, (float) i2, this.fy.bT);
            }
        }

        public final void b(int i, int i2, int i3, int i4) {
            if (this.fH > 0 && this.fI > 0) {
                this.fA.drawRect(0.0f, 0.0f, 240.0f, 320.0f, this.fx);
            }
        }

        public final void c(int i, int i2, int i3, int i4) {
            this.fF = i;
            this.fG = i2;
            this.fH = i3;
            this.fI = i4;
            if (i3 > 0 && i4 > 0) {
                this.fA.clipRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), Region.Op.REPLACE);
            }
        }

        public final void setColor(int i) {
            this.fw.setColor(-16777216 | i);
            this.fx.setColor(-16777216 | i);
            super.setColor(i);
        }

        public final void translate(int i, int i2) {
            if (i != 0 || i2 != 0) {
                super.translate(i, i2);
                this.fA.translate((float) i, (float) i2);
                c(this.fF - i, this.fG - i2, this.fH, this.fI);
            }
        }
    }

    public static final class e implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, Player {
        private static final String LOG_TAG = "MIDP player";
        private g.a fJ;
        private HashSet<PlayerListener> fK;
        private int fL = 1;
        private int fM;
        private boolean fN;
        private int state = 100;

        public e(g.a aVar) {
            this.fJ = aVar;
            aVar.dY = org.meteoroid.core.g.Z();
            aVar.dZ.setOnCompletionListener(this);
            aVar.dZ.setOnPreparedListener(this);
            this.fK = new HashSet<>();
        }

        private final void a(String str, Object obj) {
            Iterator<PlayerListener> it = this.fK.iterator();
            while (it.hasNext()) {
                PlayerListener next = it.next();
                if (next != null) {
                    next.B();
                }
            }
        }

        private void aO() {
            try {
                if (this.state == 100) {
                    this.fJ.dZ.reset();
                    this.fJ.dZ.setDataSource(this.fJ.dW);
                    this.state = Player.REALIZED;
                }
            } catch (Exception e) {
                Log.w(LOG_TAG, e);
                throw new MediaException();
            }
        }

        public final void f(int i) {
            "setLoopCount " + -1 + ".";
            this.fL = -1;
        }

        public final void onCompletion(MediaPlayer mediaPlayer) {
            "playedCount:" + this.fM + " loopCount:" + this.fL;
            if (mediaPlayer == this.fJ.dZ) {
                this.fM++;
                if (this.fM < this.fL) {
                    try {
                        this.state = 100;
                        start();
                    } catch (MediaException e) {
                        e.printStackTrace();
                    }
                } else if (this.fK.isEmpty()) {
                    try {
                        stop();
                    } catch (MediaException e2) {
                        e2.printStackTrace();
                    }
                } else {
                    a(PlayerListener.END_OF_MEDIA, null);
                }
            }
        }

        public final void onPrepared(MediaPlayer mediaPlayer) {
            "onPrepared:" + this.fM + " loopCount:" + this.fL;
        }

        public final void start() {
            aO();
            aO();
            if (this.state == 200) {
                try {
                    this.fJ.dZ.prepare();
                    this.state = Player.PREFETCHED;
                } catch (Exception e) {
                    a(PlayerListener.ERROR, e.getMessage());
                    throw new MediaException();
                }
            }
            if (this.state == 300) {
                try {
                    if (this.fL == -1) {
                        this.fJ.dZ.setLooping(true);
                    } else {
                        this.fJ.dZ.setLooping(false);
                    }
                    if (this.fN) {
                        this.fJ.dZ.prepare();
                        this.fN = false;
                    }
                    this.fJ.dZ.start();
                    this.fJ.dX = true;
                    this.state = Player.STARTED;
                    a(PlayerListener.STARTED, null);
                } catch (Exception e2) {
                    a(PlayerListener.ERROR, e2.getMessage());
                    throw new MediaException();
                }
            }
        }

        public final void stop() {
            if (this.state == 400) {
                try {
                    if (this.fJ.dZ.isPlaying()) {
                        this.fJ.dZ.stop();
                    }
                    this.fJ.dX = false;
                    this.fM = 0;
                    this.state = Player.PREFETCHED;
                    this.fN = true;
                    a(PlayerListener.STOPPED, null);
                } catch (Exception e) {
                    a(PlayerListener.ERROR, e.getMessage());
                    e.printStackTrace();
                    throw new MediaException();
                } catch (Throwable th) {
                    this.state = Player.PREFETCHED;
                    this.fN = true;
                    a(PlayerListener.STOPPED, null);
                    throw th;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.plugin.device.MIDPDevice.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      org.meteoroid.plugin.device.MIDPDevice.b(org.meteoroid.plugin.device.MIDPDevice, int):int
      org.meteoroid.plugin.device.MIDPDevice.b(java.lang.String, boolean):boolean */
    private void a(Properties properties) {
        try {
            if (properties.containsKey("screen.width")) {
                this.width = Integer.parseInt(properties.getProperty("screen.width"));
                "Set screen width " + this.width;
            }
            if (properties.containsKey("screen.height")) {
                this.height = Integer.parseInt(properties.getProperty("screen.height"));
                "Set screen height " + this.height;
            }
            if (properties.containsKey("font.size.large")) {
                c.SIZE_LARGE = Integer.parseInt(properties.getProperty("font.size.large"));
                "Set font.size.large " + c.SIZE_LARGE;
            }
            if (properties.containsKey("font.size.medium")) {
                c.SIZE_MEDIUM = Integer.parseInt(properties.getProperty("font.size.medium"));
                "Set font.size.medium " + c.SIZE_MEDIUM;
            }
            if (properties.containsKey("font.size.small")) {
                c.SIZE_SMALL = Integer.parseInt(properties.getProperty("font.size.small"));
                "Set font.size.small " + c.SIZE_SMALL;
            }
            for (int i = 0; i < 10; i++) {
                if (properties.containsKey("key." + i)) {
                    eW.put("NUM_" + i, Integer.valueOf(Integer.parseInt(properties.getProperty("key." + i).trim())));
                }
            }
            for (int i2 = 0; i2 < eO.length; i2++) {
                if (properties.containsKey("key." + eO[i2])) {
                    eW.put(eO[i2], Integer.valueOf(Integer.parseInt(properties.getProperty("key." + eO[i2]).trim())));
                }
            }
            for (Object next : properties.keySet()) {
                System.setProperty((String) next, properties.getProperty((String) next));
            }
        } catch (Exception e2) {
            Log.w("MIDPDevice", "Device property exception. " + e2);
        }
        this.eQ = b("hasRepeatEvents", false);
        this.eS = b("hasPointerMotionEvents", true);
        this.eT = b("isDoubleBuffered", true);
        this.eP = b("isPositiveUpdate", false);
        this.eU = b("enableMultiTouch", false);
        this.eV = b("fixTouchPosition", false);
    }

    private static d b(Bitmap bitmap) {
        return new d(new Canvas(bitmap));
    }

    private boolean b(String str, boolean z) {
        "Get device property:" + str;
        String property = fe.getProperty(str);
        return property == null ? z : Boolean.parseBoolean(property);
    }

    private final void g(int i, int i2) {
        int u = u(i2);
        if (u == -1) {
            return;
        }
        if (i == 0) {
            this.fm = (1 << u) | this.fm;
            return;
        }
        this.fm = (1 << u) ^ this.fm;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void lock() {
        /*
            r2 = this;
            monitor-enter(r2)
            r0 = 100
            r2.wait(r0)     // Catch:{ InterruptedException -> 0x0008, all -> 0x000a }
        L_0x0006:
            monitor-exit(r2)
            return
        L_0x0008:
            r0 = move-exception
            goto L_0x0006
        L_0x000a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.device.MIDPDevice.lock():void");
    }

    /* access modifiers changed from: private */
    public final int r(int i) {
        switch (i) {
            case 4:
                if (k.au() == null || !k.au().startsWith("R800")) {
                    return 0;
                }
                return eW.get("NUM_5").intValue();
            case 7:
            case 116:
                return eW.get("NUM_0").intValue();
            case 8:
            case com.a.a.a.b.KEY_NUM3:
                return eW.get("NUM_1").intValue();
            case 9:
            case 33:
                return eW.get("NUM_2").intValue();
            case 10:
            case 46:
                return eW.get("NUM_3").intValue();
            case 11:
            case 47:
                return eW.get("NUM_4").intValue();
            case 12:
            case 32:
                return eW.get("NUM_5").intValue();
            case DefaultVirtualDevice.WIDGET_TYPE_CHECKIN /*13*/:
            case 34:
                return eW.get("NUM_6").intValue();
            case DefaultVirtualDevice.WIDGET_TYPE_SNSBUTTON /*14*/:
            case com.a.a.a.b.KEY_NUM6:
            case 99:
                return eW.get("NUM_7").intValue();
            case DefaultVirtualDevice.WIDGET_TYPE_DYNAMICJOYSTICK /*15*/:
            case com.a.a.a.b.KEY_NUM4:
                return eW.get("NUM_8").intValue();
            case 16:
            case 31:
            case Player.UNREALIZED:
                return eW.get("NUM_9").intValue();
            case 17:
            case 61:
            case 102:
                return eW.get(eO[11]).intValue();
            case 18:
            case com.a.a.a.b.KEY_NUM7:
            case 103:
                return eW.get(eO[12]).intValue();
            case 19:
                return eW.get(eO[0]).intValue();
            case 20:
                return eW.get(eO[1]).intValue();
            case 21:
                return eW.get(eO[2]).intValue();
            case 22:
                return eW.get(eO[3]).intValue();
            case 23:
                return eW.get(eO[4]).intValue();
            case 44:
            case 86:
            case 88:
            case 93:
            case 108:
            case 186:
                return t(2);
            case 45:
            case 85:
            case 87:
            case 92:
            case 109:
            case 183:
                return t(1);
            case 66:
                return eW.get(eO[9]).intValue();
            default:
                return 0;
        }
    }

    public static int s(int i) {
        if (eW.containsKey(eO[0]) && i == eW.get(eO[0]).intValue()) {
            return 1;
        }
        if (eW.containsKey(eO[1]) && i == eW.get(eO[1]).intValue()) {
            return 6;
        }
        if (eW.containsKey(eO[2]) && i == eW.get(eO[2]).intValue()) {
            return 2;
        }
        if (eW.containsKey(eO[3]) && i == eW.get(eO[3]).intValue()) {
            return 5;
        }
        if (eW.containsKey(eO[4]) && i == eW.get(eO[4]).intValue()) {
            return 8;
        }
        if (eW.containsKey(eO[5]) && i == eW.get(eO[5]).intValue()) {
            return 9;
        }
        if (eW.containsKey(eO[6]) && i == eW.get(eO[6]).intValue()) {
            return 10;
        }
        if (eW.containsKey(eO[7]) && i == eW.get(eO[7]).intValue()) {
            return 11;
        }
        if (eW.containsKey(eO[12]) && i == eW.get(eO[12]).intValue()) {
            return 35;
        }
        if (eW.containsKey(eO[11]) && i == eW.get(eO[11]).intValue()) {
            return 42;
        }
        if (eW.containsKey("NUM_0") && i == eW.get("NUM_0").intValue()) {
            return 48;
        }
        if (eW.containsKey("NUM_1") && i == eW.get("NUM_1").intValue()) {
            return 49;
        }
        if (eW.containsKey("NUM_2") && i == eW.get("NUM_2").intValue()) {
            return 50;
        }
        if (eW.containsKey("NUM_3") && i == eW.get("NUM_3").intValue()) {
            return 51;
        }
        if (eW.containsKey("NUM_4") && i == eW.get("NUM_4").intValue()) {
            return 52;
        }
        if (eW.containsKey("NUM_5") && i == eW.get("NUM_5").intValue()) {
            return 53;
        }
        if (eW.containsKey("NUM_6") && i == eW.get("NUM_6").intValue()) {
            return 54;
        }
        if (eW.containsKey("NUM_7") && i == eW.get("NUM_7").intValue()) {
            return 55;
        }
        if (!eW.containsKey("NUM_8") || i != eW.get("NUM_8").intValue()) {
            return (!eW.containsKey("NUM_9") || i != eW.get("NUM_9").intValue()) ? 0 : 57;
        }
        return 56;
    }

    public static int t(int i) {
        if (i == 1) {
            return eW.get(eO[9]).intValue();
        }
        if (i == 2) {
            return eW.get(eO[10]).intValue();
        }
        return 0;
    }

    private final int u(int i) {
        if (i == t(1)) {
            return 17;
        }
        if (i == t(2)) {
            return 18;
        }
        switch (s(i)) {
            case 1:
                return 12;
            case 2:
                return 13;
            case 5:
                return 14;
            case 6:
                return 15;
            case 8:
                return 16;
            case com.a.a.a.b.KEY_POUND:
                return 11;
            case com.a.a.a.b.KEY_STAR:
                return 10;
            case com.a.a.a.b.KEY_NUM0:
                return 0;
            case com.a.a.a.b.KEY_NUM1:
                return 1;
            case com.a.a.a.b.KEY_NUM2:
                return 2;
            case com.a.a.a.b.KEY_NUM3:
                return 3;
            case com.a.a.a.b.KEY_NUM4:
                return 4;
            case com.a.a.a.b.KEY_NUM5:
                return 5;
            case com.a.a.a.b.KEY_NUM6:
                return 6;
            case com.a.a.a.b.KEY_NUM7:
                return 7;
            case com.a.a.a.b.KEY_NUM8:
                return 8;
            case com.a.a.a.b.KEY_NUM9:
                return 9;
            default:
                return -1;
        }
    }

    private synchronized void unlock() {
        notify();
    }

    private boolean v(int i) {
        return ((1 << u(i)) & this.fm) != 0;
    }

    public final void a(int i, float f, float f2, int i2) {
        if (this.eU || i2 == 0) {
            h.b(h.a(com.a.a.d.a.MSG_DEVICE_TOUCH_EVENT, new int[]{i, (int) f, (int) f2, i2}));
        }
    }

    public final void a(AttributeSet attributeSet, String str) {
        setTouchable(attributeSet.getAttributeBooleanValue(str, "touchable", false));
        this.eR = isTouchable();
        d(attributeSet.getAttributeBooleanValue(str, "filter", true));
        String attributeValue = attributeSet.getAttributeValue(str, "origrect");
        if (attributeValue != null) {
            this.hf = com.a.a.e.c.H(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            a(com.a.a.e.c.H(attributeValue2));
        }
    }

    public final void a(com.a.a.d.c cVar) {
        if (!m.eI) {
            if (this.eT) {
                this.eX = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.eY = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.fc = b(this.eX);
                this.fd = b(this.eY);
                this.fb = this.fc;
                this.eZ = this.eY;
            } else {
                this.eX = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.eY = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.eZ = this.eY;
                this.fa = new Canvas(this.eZ);
                this.fb = b(this.eX);
            }
        }
        ba();
        if (this.eP) {
            m.aH();
        }
        com.a.a.a.e.a((MIDlet) null);
        com.a.a.a.e.e(getWidth(), getHeight());
    }

    public final boolean a(Message message) {
        boolean z;
        boolean z2 = false;
        if (message.what == 47872) {
            final HashMap hashMap = new HashMap();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(k.getActivity().getAssets().open(com.a.a.e.c.E(new String(com.a.a.e.a.B("TUVUQS1JTkYvTUFOSUZFU1QuTUY=")))), com.umeng.common.b.e.f));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    readLine.trim();
                    int indexOf = readLine.indexOf(58);
                    if (indexOf >= 0) {
                        String trim = readLine.substring(0, indexOf).trim();
                        String trim2 = readLine.substring(indexOf + 1).trim();
                        if (Pattern.compile("\\bMIDlet-\\d").matcher(readLine).find()) {
                            int indexOf2 = trim2.indexOf(44);
                            int lastIndexOf = trim2.lastIndexOf(44);
                            if (indexOf2 < 0 || lastIndexOf < 0) {
                                Log.w("MIDPDevice", "The midlet " + trim + ":" + trim2 + " where p1=" + indexOf2 + " p2=" + lastIndexOf);
                            } else {
                                String trim3 = trim2.substring(0, indexOf2).trim();
                                trim2.substring(indexOf2 + 1, lastIndexOf).trim();
                                hashMap.put(trim3, trim2.substring(lastIndexOf + 1).trim());
                                "The midlet " + readLine + " has added.";
                            }
                        } else {
                            org.meteoroid.core.a.f(trim, trim2);
                        }
                    }
                }
                if (hashMap.isEmpty()) {
                    Log.w("MIDPDevice", "No midlets found in MANIFEST.MF.");
                    k.a(k.getString(R.string.no_midlet_found), 1);
                    return true;
                } else if (hashMap.size() == 1) {
                    org.meteoroid.core.a.n((String) hashMap.values().toArray()[0]);
                    return true;
                } else {
                    final String[] strArr = new String[hashMap.keySet().size()];
                    hashMap.keySet().toArray(strArr);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(k.getActivity());
                    builder.setItems(strArr, new DialogInterface.OnClickListener() {
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            org.meteoroid.core.a.n((String) hashMap.get(strArr[i]));
                        }
                    });
                    builder.setCancelable(false);
                    k.getHandler().post(new Runnable() {
                        public final void run() {
                            builder.create().show();
                        }
                    });
                    return true;
                }
            } catch (IOException e2) {
                Log.w("MIDPDevice", "MANIFEST.MF may not exist or invalid.");
                k.a(k.getString(R.string.no_midlet_found), 1);
                return true;
            }
        } else if (message.what == 44036) {
            h.o(k.MSG_SYSTEM_EXIT);
            return true;
        } else if (message.what == -2023686143) {
            k.u((String) message.obj);
            h.c(MIDlet.MIDLET_PLATFORM_REQUEST_FINISH, Boolean.FALSE);
            return true;
        } else {
            switch (message.what) {
                case MSG_MIDP_COMMAND_EVENT /*44034*/:
                    com.a.a.a.e.a((MIDlet) null);
                    com.a.a.a.e.a((com.a.a.a.c) message.obj);
                    return true;
                case com.a.a.d.a.MSG_DEVICE_TOUCH_EVENT:
                    int[] iArr = (int[]) message.obj;
                    "action[" + iArr[0] + "]x:" + iArr[1] + " y:" + iArr[2];
                    switch (iArr[0]) {
                        case 0:
                            if (this.eR) {
                                com.a.a.a.e.a((MIDlet) null).b(iArr[1], iArr[2]);
                                this.fi = iArr[1];
                                this.fj = iArr[2];
                            }
                            z = true;
                            break;
                        case 1:
                            if (this.eR) {
                                com.a.a.a.e.a((MIDlet) null).c(iArr[1], iArr[2]);
                            }
                            z = true;
                            break;
                        case 2:
                            if (this.eR && this.eS) {
                                int i = iArr[1];
                                int i2 = iArr[2];
                                if (!this.eV) {
                                    z2 = true;
                                } else {
                                    if (this.fk == 0) {
                                        this.fk = getWidth() / 60;
                                    }
                                    if (this.fl == 0) {
                                        this.fl = getHeight() / 60;
                                    }
                                    if (Math.abs(i - this.fi) >= this.fk || Math.abs(i2 - this.fj) >= this.fl) {
                                        z2 = true;
                                    }
                                }
                                if (z2) {
                                    com.a.a.a.e.a((MIDlet) null).d(iArr[1], iArr[2]);
                                }
                            }
                            z = true;
                            break;
                        default:
                            return false;
                    }
                case VirtualKey.MSG_VIRTUAL_KEY_EVENT /*7833601*/:
                    int aQ = ((VirtualKey) message.obj).aQ();
                    String bc = ((VirtualKey) message.obj).bc();
                    return h(aQ, eW.containsKey(bc) ? eW.get(bc).intValue() : 65535);
                default:
                    z = false;
                    break;
            }
            return z;
        }
    }

    public final boolean a(KeyEvent keyEvent) {
        if (!this.fh) {
            k.as().schedule(new a(this, (byte) 0), 500, 700);
            this.fh = true;
        }
        "Native key event:" + keyEvent.getAction() + "[" + keyEvent.getKeyCode() + "]";
        if (keyEvent.getAction() == 1) {
            this.fg = keyEvent.getKeyCode();
            this.ff = -1;
        } else if (keyEvent.getAction() == 0) {
            if (this.ff == -1 && this.fg == -1) {
                h(0, r(keyEvent.getKeyCode()));
            }
            this.ff = keyEvent.getKeyCode();
        }
        return false;
    }

    public final com.a.a.a.h aL() {
        return this.fb;
    }

    public final void aM() {
        if (this.eT) {
            if (this.eZ == this.eX) {
                this.eZ = this.eY;
                this.fb = this.fc;
            } else {
                this.eZ = this.eX;
                this.fb = this.fd;
            }
        } else if (this.eX != null && !this.eX.isRecycled()) {
            this.fa.drawBitmap(this.eX, 0.0f, 0.0f, (Paint) null);
        }
        if (this.fb != null) {
            this.fb.A();
        }
        if (this.eP) {
            unlock();
        } else {
            m.aI();
        }
    }

    public final Bitmap aN() {
        return this.eZ;
    }

    public final int getHeight() {
        return this.height == -1 ? aZ().height() : this.height;
    }

    public final int getWidth() {
        return this.width == -1 ? aZ().width() : this.width;
    }

    public final boolean h(int i, int i2) {
        if (i2 == 0) {
            return false;
        }
        switch (i) {
            case 0:
                if (!v(i2)) {
                    com.a.a.a.e.a((MIDlet) null).b(i2);
                    "Dispatch key event type: DOWN [" + i2 + "]";
                } else if (this.eQ) {
                    com.a.a.a.e.a((MIDlet) null).d(i2);
                    "Dispatch key event type: REPEAT [" + i2 + "]";
                }
                g(i, i2);
                return true;
            case 1:
                if (v(i2)) {
                    com.a.a.a.e.a((MIDlet) null).c(i2);
                    "Dispatch key event type: UP [" + i2 + "]";
                    g(i, i2);
                }
                return true;
            default:
                "Unkown key event type:" + i + "[" + i2 + "]";
                return false;
        }
    }

    public final void onCreate() {
        h.b((int) com.a.a.d.a.MSG_DEVICE_TOUCH_EVENT, "MSG_DEVICE_TOUCH_EVENT");
        h.b((int) MSG_MIDP_COMMAND_EVENT, "MSG_MIDP_COMMAND_EVENT");
        h.b((int) MSG_MIDP_DISPLAY_CALL_SERIALLY, "MSG_MIDP_DISPLAY_CALL_SERIALLY");
        h.b((int) com.a.a.d.a.MSG_DEVICE_REQUEST_REFRESH, "MSG_DEVICE_REQUEST_REFRESH");
        h.a(this);
        fe.clear();
        try {
            fe.load(k.getActivity().getResources().openRawResource(com.a.a.e.c.G("device")));
        } catch (IOException e2) {
            Log.e("MIDPDevice", "device.properties not exist or valid." + e2);
        }
        a(fe);
        f.a((f.a) this);
        f.a((f.e) this);
        System.gc();
    }

    public final void onDestroy() {
        if (this.eX != null) {
            this.eX.recycle();
        }
        this.eX = null;
        if (this.eY != null) {
            this.eY.recycle();
        }
        this.eY = null;
        if (this.eZ != null) {
            this.eZ.recycle();
        }
        this.eZ = null;
        this.fb = null;
        boolean z = this.fo;
    }

    public final void onDraw(Canvas canvas) {
        if (this.eZ != null && !this.eZ.isRecycled()) {
            canvas.drawBitmap(this.eZ, this.hf, aZ(), this.he);
        }
        if (this.eP) {
            lock();
        }
    }

    public final void setVisible(boolean z) {
    }
}
