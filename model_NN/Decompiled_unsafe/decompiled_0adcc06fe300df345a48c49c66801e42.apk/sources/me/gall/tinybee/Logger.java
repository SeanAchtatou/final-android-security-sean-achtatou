package me.gall.tinybee;

import android.content.Context;
import java.util.Map;

public interface Logger {

    public interface OnlineParamCallback {
        void requestComplete(Map<String, String> map);

        void requestError();
    }

    void finish();

    Context getContext();

    boolean hasOfflineData();

    void init();

    boolean isSandboxMode();

    void onPause(Context context);

    void onResume(Context context);

    void send(String str);

    void send(String str, Map<String, String> map);

    void setOnlineParamCallback(OnlineParamCallback onlineParamCallback);

    void syncOfflineData();
}
