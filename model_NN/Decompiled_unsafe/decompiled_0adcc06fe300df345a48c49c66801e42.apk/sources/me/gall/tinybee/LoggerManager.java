package me.gall.tinybee;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Process;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import me.gall.tinybee.TinybeeLogger;

public class LoggerManager {
    private static final String LOGGER_TYPE_TINYBEE = "tinybee";
    private static final String TAG = "LoggerManager";
    private static final String _TB_LOGGER_KEYS = "_TB_LOGGER_KEYS_";
    private static boolean isTestMode = false;
    private static HashMap<String, Logger> loggers = new HashMap<>();

    static final class LoggerConfiguration {
        private String appId;
        private String channelId;
        private String channelName;
        private String packageName;
        private boolean sandboxMode;
        private String type;

        public LoggerConfiguration(String str, String str2, String str3, String str4, String str5, boolean z) {
            this.type = str;
            this.appId = str2;
            this.channelId = str3;
            this.channelName = str4;
            this.packageName = str5;
            this.sandboxMode = z;
        }

        public final String getAppId() {
            return this.appId;
        }

        public final String getChannelId() {
            return this.channelId;
        }

        public final String getChannelName() {
            return this.channelName;
        }

        public final String getPackageName() {
            return this.packageName;
        }

        public final String getType() {
            return this.type;
        }

        public final boolean isSandboxMode() {
            return this.sandboxMode;
        }

        public final void setAppId(String str) {
            this.appId = str;
        }

        public final void setChannelId(String str) {
            this.channelId = str;
        }

        public final void setChannelName(String str) {
            this.channelName = str;
        }

        public final void setPackageName(String str) {
            this.packageName = str;
        }

        public final void setSandboxMode(boolean z) {
            this.sandboxMode = z;
        }

        public final void setType(String str) {
            this.type = str;
        }
    }

    public static class NetworkStateChangeReceiver extends BroadcastReceiver {
        protected static final String CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";
        protected static final String WIFI_STATE_CHANGED = "android.net.wifi.WIFI_STATE_CHANGED";
        protected static final String WIFI_STATE_CHANGED_2 = "android.net.wifi.supplicant.CONNECTION_CHANGE";

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ((action.equals(CONNECTIVITY_CHANGE) || action.equals(WIFI_STATE_CHANGED) || action.equals(WIFI_STATE_CHANGED_2)) && TinybeeLogger.Config.isConnect(context)) {
                List<Logger> access$0 = LoggerManager.listAllLoggers(context);
                String.valueOf(access$0.size()) + " loggers wait to sync.";
                for (Logger logger : access$0) {
                    if (logger.hasOfflineData()) {
                        logger.syncOfflineData();
                    }
                }
            }
        }
    }

    public static void enableTestMode(boolean z) {
        isTestMode = z;
    }

    static void forceFinish() {
        Log.w(TAG, "Force finish.");
        for (Logger finish : loggers.values()) {
            finish.finish();
        }
        for (Logger next : loggers.values()) {
            if (next.getContext() != null) {
                try {
                    ((Activity) next.getContext()).finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        System.gc();
        Process.killProcess(Process.myPid());
        System.exit(0);
    }

    private static String generateLoggerKey(String str, String str2, String str3) {
        return String.valueOf(str) + "_TB_" + str2 + "_TB_" + str3;
    }

    public static Logger getLogger(Context context) {
        boolean z;
        Exception e;
        String str = null;
        String str2 = "";
        String str3 = "";
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            str = applicationInfo.metaData.getString("TB_APP_ID");
            str2 = applicationInfo.metaData.getString("TB_CHANNEL_ID");
            str3 = applicationInfo.metaData.getString("TB_CHANNEL_NAME");
            z = applicationInfo.metaData.getBoolean("TB_SANDBOX_MODE");
            if (str == null) {
                try {
                    throw new Exception("TB_APP_ID must be valid.");
                } catch (Exception e2) {
                    e = e2;
                    Log.w(TAG, e.getMessage());
                    return getLogger(context, str, str2, str3, z);
                }
            }
        } catch (Exception e3) {
            Exception exc = e3;
            z = false;
            e = exc;
        }
        return getLogger(context, str, str2, str3, z);
    }

    public static Logger getLogger(Context context, String str) {
        return getLogger(context, str, "", "", false);
    }

    public static Logger getLogger(Context context, String str, String str2) {
        return getLogger(context, str, str2, "", false);
    }

    public static Logger getLogger(Context context, String str, String str2, String str3) {
        return getLogger(context, str, str2, str3, false);
    }

    public static Logger getLogger(Context context, String str, String str2, String str3, boolean z) {
        String generateLoggerKey = generateLoggerKey(str, str2, str3);
        if (!loggers.containsKey(generateLoggerKey)) {
            if (str == null || str.equals("")) {
                throw new NullPointerException("APPID can not be null");
            }
            LoggerConfiguration loggerConfiguration = new LoggerConfiguration(LOGGER_TYPE_TINYBEE, str, str2 == null ? "" : str2, str3 == null ? "" : str3, context.getPackageName(), z);
            try {
                TinybeeLogger tinybeeLogger = new TinybeeLogger(context, loggerConfiguration);
                "Logger " + str + " is done.";
                saveLoggerConfiguration(context, loggerConfiguration);
                tinybeeLogger.init();
                loggers.put(generateLoggerKey, tinybeeLogger);
            } catch (Exception e) {
                Log.e(TAG, "Logger could not create because of " + e.getMessage());
                new EmptyLogger();
            }
        }
        return loggers.get(generateLoggerKey);
    }

    public static boolean isTestMode() {
        return isTestMode;
    }

    /* access modifiers changed from: private */
    public static List<Logger> listAllLoggers(Context context) {
        ArrayList arrayList = new ArrayList();
        for (String next : context.getSharedPreferences(_TB_LOGGER_KEYS, 0).getAll().keySet()) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(_TB_LOGGER_KEYS + next, 0);
            if (next != null) {
                String string = sharedPreferences.getString("CHANNELID", "");
                String string2 = sharedPreferences.getString("CHANNELNAME", "");
                arrayList.add(new TinybeeLogger(context, new LoggerConfiguration(sharedPreferences.getString("TYPE", LOGGER_TYPE_TINYBEE), next, string, string2, context.getPackageName(), sharedPreferences.getBoolean("SANDBOXMODE", false))));
            }
        }
        return arrayList;
    }

    private static void saveLoggerConfiguration(Context context, LoggerConfiguration loggerConfiguration) {
        if (!context.getSharedPreferences(_TB_LOGGER_KEYS, 0).contains(loggerConfiguration.getAppId())) {
            SharedPreferences.Editor edit = context.getSharedPreferences(_TB_LOGGER_KEYS, 0).edit();
            edit.putString(loggerConfiguration.getAppId(), _TB_LOGGER_KEYS + loggerConfiguration.getAppId());
            edit.commit();
            SharedPreferences.Editor edit2 = context.getSharedPreferences(_TB_LOGGER_KEYS + loggerConfiguration.getAppId(), 0).edit();
            edit2.putString("CHANNELID", loggerConfiguration.getChannelId());
            edit2.putString("CHANNELNAME", loggerConfiguration.getChannelName());
            edit2.putString("TYPE", loggerConfiguration.getType());
            edit2.putBoolean("SANDBOXMODE", loggerConfiguration.isSandboxMode());
            edit2.commit();
        }
    }
}
