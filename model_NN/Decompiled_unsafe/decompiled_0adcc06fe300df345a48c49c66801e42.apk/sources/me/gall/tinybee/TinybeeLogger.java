package me.gall.tinybee;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.a.a.b.a;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import me.gall.tinybee.Logger;
import me.gall.tinybee.LoggerManager;
import org.json.JSONException;
import org.json.JSONObject;

public class TinybeeLogger implements Logger {
    public static final String TAG = "TinybeeLogger";
    private static final String TINYBEE_RUNTIMES = "_TB_RUNTIMES";
    private static final String TINYBEE_UUIDS = "_TB_UUIDS";
    public static final int URL_TYPE_APK_DOWNLOAD = 0;
    public static final int URL_TYPE_WEB_PAGE = 1;
    private static final String UUID = "UUID";
    public static final int VIEW_DIALOG = 2;
    public static final int VIEW_FULLSCREEN = 1;
    public static final int VIEW_NORMAL = 0;
    private static final String _TB_launch = "_TB_launch";
    /* access modifiers changed from: private */
    public static boolean networkError;
    /* access modifiers changed from: private */
    public static int retry;
    /* access modifiers changed from: private */
    public LoggerManager.LoggerConfiguration conf;
    private Context context;
    private ExecutorService executorService;

    public static final class Config {
        private static final String HOST = "http://tinybee.savenumber.com/";
        private static final String HOST_TEST = "http://test.gall.me/tinybee/";
        protected static String network = "";

        protected static boolean checkCallingPermission(String str, Context context) {
            return Binder.getCallingPid() == Process.myPid() || context.checkCallingPermission(str) == 0;
        }

        static String getAnalysisURL(boolean z) {
            return String.valueOf(getHost(z)) + "analysis/";
        }

        protected static String getAndroidID(Context context) {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (!"9774d56d682e549c".equals(string)) {
                try {
                    Class<?> cls = Class.forName("android.os.SystemProperties");
                    return (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
                } catch (Exception e) {
                }
            }
            return string;
        }

        public static String getDataConnectionNetworkInfo(Context context) {
            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
                return null;
            }
            try {
                return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0).getExtraInfo();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        protected static String getHost(boolean z) {
            return z ? HOST_TEST : HOST;
        }

        protected static String getIMEI(Context context) {
            try {
                return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected static String getIMSI(Context context) {
            try {
                return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        static String getLiveParamsURL(boolean z) {
            return String.valueOf(getHost(z)) + "customparameters/";
        }

        protected static String getMAC(Context context) {
            try {
                WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                return (wifiManager == null ? null : wifiManager.getConnectionInfo()).getMacAddress();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected static String getManufacturer() {
            return Build.MANUFACTURER;
        }

        protected static String getModel() {
            return Build.MODEL;
        }

        protected static String getNetwork(Context context) {
            if (!isConnect(context)) {
                return "NONETWORK";
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            try {
                if (activeNetworkInfo.getTypeName().toUpperCase().indexOf("MOBILE") != -1) {
                    network = activeNetworkInfo.getExtraInfo();
                } else if (activeNetworkInfo.getTypeName().toUpperCase().indexOf("WIFI") != -1) {
                    network = "WIFI";
                }
            } catch (Exception e) {
                network = "NONETWORK";
            }
            return network;
        }

        protected static String getNumber(Context context) {
            try {
                return ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected static String getOs() {
            return "android";
        }

        protected static String getOsVerSion() {
            return Build.VERSION.RELEASE;
        }

        protected static String getResolution(Context context) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            return String.valueOf(displayMetrics.widthPixels) + "*" + displayMetrics.heightPixels;
        }

        private static TelephonyManager getTelephonyManager(Context context) {
            return (TelephonyManager) context.getSystemService("phone");
        }

        protected static long getTimeStamp() {
            return System.currentTimeMillis();
        }

        static String getUpdateInfoURL(boolean z) {
            return String.valueOf(getHost(z)) + "update/info";
        }

        public static final boolean isConnect(Context context) {
            return isWifiConnect(context) || isDataConnect(context);
        }

        public static boolean isDataConnect(Context context) {
            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
                return true;
            }
            try {
                NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0);
                if (networkInfo.isConnectedOrConnecting() || getTelephonyManager(context).getDataState() == 2) {
                    "mobile is connected. Type:" + networkInfo.getTypeName() + " APN:" + networkInfo.getExtraInfo();
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        public static boolean isWifiConnect(Context context) {
            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != 0) {
                return true;
            }
            try {
                return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isConnected();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        protected static final String replace(String str, String str2, String str3) {
            try {
                return str.replace(str2, str3);
            } catch (Exception e) {
                Log.e(TinybeeLogger.TAG, "Config.replace()出错:" + e.getMessage());
                return str;
            }
        }

        public static void setMobileDataEnabled(Context context, boolean z) {
            "SDK_INT is " + Build.VERSION.SDK_INT;
            if (Build.VERSION.SDK_INT >= 9) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                Field declaredField = Class.forName(connectivityManager.getClass().getName()).getDeclaredField("mService");
                declaredField.setAccessible(true);
                Object obj = declaredField.get(connectivityManager);
                Method declaredMethod = Class.forName(obj.getClass().getName()).getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(obj, Boolean.valueOf(z));
            } else {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (telephonyManager.getDataState() != 2 || !z) {
                    telephonyManager.getDataState();
                }
                Method declaredMethod2 = Class.forName(telephonyManager.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
                declaredMethod2.setAccessible(true);
                Object invoke = declaredMethod2.invoke(telephonyManager, new Object[0]);
                Class<?> cls = Class.forName(invoke.getClass().getName());
                Method declaredMethod3 = !z ? cls.getDeclaredMethod("disableDataConnectivity", new Class[0]) : cls.getDeclaredMethod("enableDataConnectivity", new Class[0]);
                declaredMethod3.setAccessible(true);
                declaredMethod3.invoke(invoke, new Object[0]);
            }
            SystemClock.sleep(2000);
        }

        public static void setWifiEnabled(Context context, boolean z) {
            ((WifiManager) context.getSystemService("wifi")).setWifiEnabled(z);
            SystemClock.sleep(2000);
        }

        static boolean testConnection() {
            return false;
        }
    }

    static class EventTask implements Runnable {
        public static final String APPID = "APPID";
        public static final String TIMESTAMP = "TIMESTAMP";
        private LoggerManager.LoggerConfiguration conf;
        protected Context context;
        private String eventId;
        /* access modifiers changed from: private */
        public JSONObject params;
        /* access modifiers changed from: private */
        public long timestamp;

        public EventTask(Context context2, LoggerManager.LoggerConfiguration loggerConfiguration, String str) {
            if (context2 == null || loggerConfiguration == null) {
                throw new IllegalArgumentException("Context or appId could not be null.");
            }
            this.context = context2;
            this.conf = loggerConfiguration;
            this.eventId = str;
            this.params = new JSONObject();
        }

        public void addParam(String str, String str2) {
            if (str == null || str2 == null) {
                Log.w(TinybeeLogger.TAG, String.valueOf(str) + " must not be null.");
                return;
            }
            try {
                this.params.put(str, str2);
            } catch (JSONException e) {
                Log.e(TinybeeLogger.TAG, e.getMessage());
                e.printStackTrace();
            }
        }

        public synchronized void appendOfflineData(Context context2) {
            SharedPreferences sharedPreferences = context2.getSharedPreferences("_TB_" + this.conf.getAppId(), 0);
            int i = sharedPreferences.getInt("_TB_Length", 0);
            "save length:" + i;
            SharedPreferences.Editor edit = sharedPreferences.edit();
            String str = null;
            if (this.params.length() > 0) {
                str = this.params.toString();
            }
            long timeStamp = Config.getTimeStamp();
            "save()  event:" + this.eventId + "   value:" + str + "   timestamp:" + timeStamp;
            edit.putString("_TB_KEY_" + i + "event", this.eventId);
            edit.putString("_TB_KEY_" + i + "value", str);
            edit.putString("_TB_KEY_" + i + "timeStamp", String.valueOf(timeStamp));
            edit.putInt("_TB_Length", i + 1);
            edit.commit();
        }

        public boolean catchErrorCode(int i) {
            return false;
        }

        public void consumeContent(String str) {
        }

        public LoggerManager.LoggerConfiguration getConf() {
            return this.conf;
        }

        public String getEventId() {
            return this.eventId;
        }

        public long getTimestamp() {
            return this.timestamp;
        }

        public boolean isNetworkError() {
            return TinybeeLogger.networkError;
        }

        public String read(InputStream inputStream, String str) {
            byte[] bArr = new byte[a.GAME_B_PRESSED];
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(a.GAME_B_PRESSED);
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    byteArrayOutputStream.flush();
                    byteArrayOutputStream.close();
                    String str2 = new String(byteArrayOutputStream.toByteArray(), str);
                    "Data:" + str2;
                    return str2;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:26:0x017b  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x019a  */
        /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void registerForUUID() {
            /*
                r6 = this;
                r1 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x01a0 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a0 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x01a0 }
                boolean r3 = r3.isSandboxMode()     // Catch:{ Exception -> 0x01a0 }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getAnalysisURL(r3)     // Catch:{ Exception -> 0x01a0 }
                java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x01a0 }
                r2.<init>(r3)     // Catch:{ Exception -> 0x01a0 }
                java.lang.String r3 = "_TB_register"
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x01a0 }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01a0 }
                r0.<init>(r2)     // Catch:{ Exception -> 0x01a0 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a0 }
                java.lang.String r3 = "Register:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x01a0 }
                java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x01a0 }
                r2.toString()     // Catch:{ Exception -> 0x01a0 }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x01a0 }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x01a0 }
                r1 = 1
                r0.setDoOutput(r1)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1 = 1
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r1 = "POST"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1 = 0
                r0.setUseCaches(r1)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r1 = "Content-Type"
                java.lang.String r2 = "application/json"
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.<init>()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "APPID"
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = r3.getAppId()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "MODEL"
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getModel()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "MANUFACTURER"
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getManufacturer()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "OS"
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getOs()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "RESOLUTION"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getResolution(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "NETWORK"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getNetwork(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "IMEI"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getIMEI(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "MAC"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getMAC(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "CHANNELID"
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = r3.getChannelId()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "CHANNELNAME"
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = r3.getChannelName()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "ANDROID_ID"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getAndroidID(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "IMSI"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getIMSI(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "PHONENUM"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getNumber(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = "CONTENT:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = r1.toString()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r2.toString()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.io.OutputStream r2 = r0.getOutputStream()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = "UTF-8"
                byte[] r1 = r1.getBytes(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r2.write(r1)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r2.flush()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r2.close()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = "ResponseCode:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r2.toString()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r2 = 200(0xc8, float:2.8E-43)
                if (r1 == r2) goto L_0x0129
                r2 = 201(0xc9, float:2.82E-43)
                if (r1 == r2) goto L_0x0129
                r2 = 304(0x130, float:4.26E-43)
                if (r1 != r2) goto L_0x017f
            L_0x0129:
                org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = "UTF-8"
                java.lang.String r2 = r6.read(r2, r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.<init>(r2)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "UUID"
                java.lang.String r1 = r1.getString(r2)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                if (r1 == 0) goto L_0x0165
                int r2 = r1.length()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                if (r2 <= 0) goto L_0x0165
                android.content.Context r2 = r6.context     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = r3.getAppId()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                me.gall.tinybee.TinybeeLogger.saveUUID(r2, r3, r1)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r3 = "Get uuid:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r1.toString()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                if (r0 == 0) goto L_0x0164
                r0.disconnect()
            L_0x0164:
                return
            L_0x0165:
                java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r2 = "Failed to get uuid."
                r1.<init>(r2)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                throw r1     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
            L_0x016d:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x0171:
                r0.printStackTrace()     // Catch:{ all -> 0x019e }
                r0 = 3
                r2 = 0
                r6.retry(r0, r2)     // Catch:{ all -> 0x019e }
                if (r1 == 0) goto L_0x0164
                r1.disconnect()
                goto L_0x0164
            L_0x017f:
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r4 = "Invalid response:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                r2.<init>(r1)     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
                throw r2     // Catch:{ Exception -> 0x016d, all -> 0x0194 }
            L_0x0194:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x0198:
                if (r1 == 0) goto L_0x019d
                r1.disconnect()
            L_0x019d:
                throw r0
            L_0x019e:
                r0 = move-exception
                goto L_0x0198
            L_0x01a0:
                r0 = move-exception
                goto L_0x0171
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.tinybee.TinybeeLogger.EventTask.registerForUUID():void");
        }

        public void retry(int i, boolean z) {
            TinybeeLogger.retry = TinybeeLogger.retry + 1;
            "Retry count:" + TinybeeLogger.retry;
            if (TinybeeLogger.retry > i || TinybeeLogger.networkError) {
                TinybeeLogger.networkError = true;
                if (z) {
                    Log.w(TinybeeLogger.TAG, "Network fatal error. Save right now.");
                    appendOfflineData(this.context);
                    return;
                }
                return;
            }
            run();
        }

        public void run() {
            if (!Config.isConnect(this.context) || TinybeeLogger.networkError) {
                "Network disabled. Save event[" + this.eventId + "].";
                appendOfflineData(this.context);
                return;
            }
            uploadEvent();
        }

        public void setEventId(String str) {
            this.eventId = str;
        }

        public void setTimestamp(long j) {
            this.timestamp = j;
        }

        /* JADX WARNING: Removed duplicated region for block: B:39:0x0144  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void uploadEvent() {
            /*
                r6 = this;
                android.content.Context r0 = r6.context
                me.gall.tinybee.LoggerManager$LoggerConfiguration r1 = r6.conf
                java.lang.String r1 = r1.getAppId()
                java.lang.String r0 = me.gall.tinybee.TinybeeLogger.getUUID(r0, r1)
                if (r0 != 0) goto L_0x0011
                r6.registerForUUID()
            L_0x0011:
                r1 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x014a }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x014a }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x014a }
                boolean r3 = r3.isSandboxMode()     // Catch:{ Exception -> 0x014a }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getAnalysisURL(r3)     // Catch:{ Exception -> 0x014a }
                java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x014a }
                r2.<init>(r3)     // Catch:{ Exception -> 0x014a }
                java.lang.String r3 = r6.eventId     // Catch:{ Exception -> 0x014a }
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x014a }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x014a }
                r0.<init>(r2)     // Catch:{ Exception -> 0x014a }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x014a }
                java.lang.String r3 = "POST:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x014a }
                java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x014a }
                r2.toString()     // Catch:{ Exception -> 0x014a }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x014a }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x014a }
                r1 = 1
                r0.setDoOutput(r1)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r1 = 1
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r1 = "POST"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r1 = 0
                r0.setUseCaches(r1)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r1 = "Content-Type"
                java.lang.String r2 = "application/json"
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r1 = "UUID"
                android.content.Context r2 = r6.context     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r3 = r3.getAppId()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r2 = me.gall.tinybee.TinybeeLogger.getUUID(r2, r3)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                long r1 = r6.timestamp     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r3 = 0
                int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
                if (r1 == 0) goto L_0x0084
                java.lang.String r1 = "TIMESTAMP"
                long r2 = r6.timestamp     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
            L_0x0084:
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                org.json.JSONObject r1 = r6.params     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r2 = "APPID"
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r3 = r3.getAppId()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                org.json.JSONObject r1 = r6.params     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                int r1 = r1.length()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                if (r1 <= 0) goto L_0x00cb
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r2 = "CONTENT:"
                r1.<init>(r2)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                org.json.JSONObject r2 = r6.params     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r1.toString()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                org.json.JSONObject r2 = r6.params     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r3 = "UTF-8"
                byte[] r2 = r2.getBytes(r3)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r1.write(r2)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r1.flush()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r1.close()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
            L_0x00cb:
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r3 = "ResponseCode:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r2.toString()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r2 = 200(0xc8, float:2.8E-43)
                if (r1 == r2) goto L_0x00e5
                r2 = 201(0xc9, float:2.82E-43)
                if (r1 != r2) goto L_0x00f8
            L_0x00e5:
                java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r2 = "UTF-8"
                java.lang.String r1 = r6.read(r1, r2)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r6.consumeContent(r1)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
            L_0x00f2:
                if (r0 == 0) goto L_0x00f7
                r0.disconnect()
            L_0x00f7:
                return
            L_0x00f8:
                r2 = 304(0x130, float:4.26E-43)
                if (r1 == r2) goto L_0x00f2
                boolean r2 = r6.catchErrorCode(r1)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                if (r2 != 0) goto L_0x0129
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r4 = "Invalid response:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r2.<init>(r1)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                throw r2     // Catch:{ Exception -> 0x0117, all -> 0x013e }
            L_0x0117:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x011b:
                r0.printStackTrace()     // Catch:{ all -> 0x0148 }
                r0 = 3
                r2 = 1
                r6.retry(r0, r2)     // Catch:{ all -> 0x0148 }
                if (r1 == 0) goto L_0x00f7
                r1.disconnect()
                goto L_0x00f7
            L_0x0129:
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r3 = "ResponseCode:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                java.lang.String r2 = " has been caught."
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                r1.toString()     // Catch:{ Exception -> 0x0117, all -> 0x013e }
                goto L_0x00f2
            L_0x013e:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x0142:
                if (r1 == 0) goto L_0x0147
                r1.disconnect()
            L_0x0147:
                throw r0
            L_0x0148:
                r0 = move-exception
                goto L_0x0142
            L_0x014a:
                r0 = move-exception
                goto L_0x011b
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.tinybee.TinybeeLogger.EventTask.uploadEvent():void");
        }
    }

    class RequestLiveParamsTask extends EventTask {
        private static final String TB_LIVE_PARAMS = "_TB_LIVE_PARAMS_";
        private Logger.OnlineParamCallback callback;

        public RequestLiveParamsTask(Context context, LoggerManager.LoggerConfiguration loggerConfiguration, Logger.OnlineParamCallback onlineParamCallback) {
            super(context, loggerConfiguration, null);
            this.callback = onlineParamCallback;
        }

        private String getLastTimestamp() {
            return this.context.getSharedPreferences("_TB_" + getConf().getAppId(), 0).getString("_TB_LAST_LIVEPARAM_REQUEST_TIMESTAMP", "0");
        }

        private Map<String, String> getLiveParams() {
            return this.context.getSharedPreferences(TB_LIVE_PARAMS + getConf().getAppId(), 0).getAll();
        }

        private void updateLastTimestamp(String str) {
            this.context.getSharedPreferences("_TB_" + getConf().getAppId(), 0).edit().putString("_TB_LAST_LIVEPARAM_REQUEST_TIMESTAMP", str).commit();
        }

        private void updateLiveParams(JSONObject jSONObject) {
            SharedPreferences.Editor edit = this.context.getSharedPreferences(TB_LIVE_PARAMS + getConf().getAppId(), 0).edit();
            edit.clear();
            edit.commit();
            try {
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    edit.putString(next, jSONObject.getString(next));
                }
            } catch (Exception e) {
                "Live params persist error." + e;
            }
            edit.commit();
        }

        public void appendOfflineData(Context context) {
        }

        /* JADX WARNING: Removed duplicated region for block: B:23:0x00da  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00f9  */
        /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void requestLiveParams() {
            /*
                r6 = this;
                r1 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x00ff }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ff }
                me.gall.tinybee.TinybeeLogger r3 = me.gall.tinybee.TinybeeLogger.this     // Catch:{ Exception -> 0x00ff }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r3.conf     // Catch:{ Exception -> 0x00ff }
                boolean r3 = r3.isSandboxMode()     // Catch:{ Exception -> 0x00ff }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getLiveParamsURL(r3)     // Catch:{ Exception -> 0x00ff }
                java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x00ff }
                r2.<init>(r3)     // Catch:{ Exception -> 0x00ff }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.getConf()     // Catch:{ Exception -> 0x00ff }
                java.lang.String r3 = r3.getAppId()     // Catch:{ Exception -> 0x00ff }
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00ff }
                java.lang.String r3 = "?v=2"
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00ff }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00ff }
                r0.<init>(r2)     // Catch:{ Exception -> 0x00ff }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ff }
                java.lang.String r3 = "GET:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x00ff }
                java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x00ff }
                r2.toString()     // Catch:{ Exception -> 0x00ff }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00ff }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00ff }
                r1 = 1
                r0.setDoOutput(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r1 = 1
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.String r1 = "GET"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r1 = 0
                r0.setUseCaches(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.String r1 = r6.getLastTimestamp()     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.String r2 = "TIMESTAMP"
                r0.setRequestProperty(r2, r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.String r3 = "TIMESTAMP:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r1.toString()     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.String r3 = "ResponseCode:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r2.toString()     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r2 = 200(0xc8, float:2.8E-43)
                if (r1 != r2) goto L_0x00ba
                org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.String r3 = "UTF-8"
                java.lang.String r2 = r6.read(r2, r3)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r1.<init>(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r6.updateLiveParams(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                long r1 = me.gall.tinybee.TinybeeLogger.Config.getTimeStamp()     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r6.updateLastTimestamp(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                me.gall.tinybee.Logger$OnlineParamCallback r1 = r6.callback     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                if (r1 == 0) goto L_0x00b4
                me.gall.tinybee.Logger$OnlineParamCallback r1 = r6.callback     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.util.Map r2 = r6.getLiveParams()     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r1.requestComplete(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
            L_0x00b4:
                if (r0 == 0) goto L_0x00b9
                r0.disconnect()
            L_0x00b9:
                return
            L_0x00ba:
                r2 = 304(0x130, float:4.26E-43)
                if (r1 != r2) goto L_0x00de
                me.gall.tinybee.Logger$OnlineParamCallback r1 = r6.callback     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                if (r1 == 0) goto L_0x00b4
                me.gall.tinybee.Logger$OnlineParamCallback r1 = r6.callback     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.util.Map r2 = r6.getLiveParams()     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r1.requestComplete(r2)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                goto L_0x00b4
            L_0x00cc:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x00d0:
                r0.printStackTrace()     // Catch:{ all -> 0x00fd }
                r0 = 3
                r2 = 1
                r6.retry(r0, r2)     // Catch:{ all -> 0x00fd }
                if (r1 == 0) goto L_0x00b9
                r1.disconnect()
                goto L_0x00b9
            L_0x00de:
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.String r4 = "Invalid response:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                r2.<init>(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
                throw r2     // Catch:{ Exception -> 0x00cc, all -> 0x00f3 }
            L_0x00f3:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x00f7:
                if (r1 == 0) goto L_0x00fc
                r1.disconnect()
            L_0x00fc:
                throw r0
            L_0x00fd:
                r0 = move-exception
                goto L_0x00f7
            L_0x00ff:
                r0 = move-exception
                goto L_0x00d0
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.tinybee.TinybeeLogger.RequestLiveParamsTask.requestLiveParams():void");
        }

        public void run() {
            if (Config.isConnect(this.context) && !isNetworkError()) {
                requestLiveParams();
            } else if (this.callback != null) {
                this.callback.requestComplete(getLiveParams());
            }
        }
    }

    static class RequestUpdateTask extends EventTask {
        public RequestUpdateTask(Context context, LoggerManager.LoggerConfiguration loggerConfiguration) {
            super(context, loggerConfiguration, null);
        }

        private String getLastTimestamp() {
            return this.context.getSharedPreferences("_TB_" + getConf().getAppId(), 0).getString("_TB_LAST_UPDATE_TIMESTAMP", "0");
        }

        private void updateLastTimestamp(String str) {
            this.context.getSharedPreferences("_TB_" + getConf().getAppId(), 0).edit().putString("_TB_LAST_UPDATE_TIMESTAMP", str).commit();
        }

        public void appendOfflineData(Context context) {
        }

        public void consumeContent(String str) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                TinybeeLogger.saveUpdateInfo(this.context, getConf().getAppId(), jSONObject.getString("TITLE"), jSONObject.getInt("VIEW"), jSONObject.getString("CONTENT"), jSONObject.getInt("URL_TYPE"), jSONObject.getString("URL"), jSONObject.getInt("VERSION"), jSONObject.getBoolean("FORCE"), jSONObject.has("DELAY") ? jSONObject.getInt("DELAY") : 0);
                TinybeeLogger.updateIfNeed(this.context, getConf());
            } catch (JSONException e) {
                Log.w(TinybeeLogger.TAG, "Invalid update info:" + e);
            }
        }

        public void run() {
            if (Config.isConnect(this.context) && !isNetworkError()) {
                updateInfo();
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:33:0x0118  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void updateInfo() {
            /*
                r6 = this;
                r1 = 0
                java.lang.String r2 = "1"
                android.content.Context r0 = r6.context     // Catch:{ Exception -> 0x00cc }
                android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ Exception -> 0x00cc }
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x00cc }
                java.lang.String r3 = r3.getPackageName()     // Catch:{ Exception -> 0x00cc }
                r4 = 0
                android.content.pm.PackageInfo r0 = r0.getPackageInfo(r3, r4)     // Catch:{ Exception -> 0x00cc }
                int r0 = r0.versionCode     // Catch:{ Exception -> 0x00cc }
                java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x00cc }
            L_0x001a:
                java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x0133 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0133 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r4 = r6.getConf()     // Catch:{ Exception -> 0x0133 }
                boolean r4 = r4.isSandboxMode()     // Catch:{ Exception -> 0x0133 }
                java.lang.String r4 = me.gall.tinybee.TinybeeLogger.Config.getUpdateInfoURL(r4)     // Catch:{ Exception -> 0x0133 }
                java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0133 }
                r3.<init>(r4)     // Catch:{ Exception -> 0x0133 }
                java.lang.String r4 = "?appid="
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0133 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r4 = r6.getConf()     // Catch:{ Exception -> 0x0133 }
                java.lang.String r4 = r4.getAppId()     // Catch:{ Exception -> 0x0133 }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0133 }
                java.lang.String r4 = "&timestamp="
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0133 }
                java.lang.String r4 = r6.getLastTimestamp()     // Catch:{ Exception -> 0x0133 }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0133 }
                java.lang.String r4 = "&packagename="
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0133 }
                android.content.Context r4 = r6.context     // Catch:{ Exception -> 0x0133 }
                java.lang.String r4 = r4.getPackageName()     // Catch:{ Exception -> 0x0133 }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0133 }
                java.lang.String r4 = "&versioncode="
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0133 }
                java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x0133 }
                java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0133 }
                r2.<init>(r0)     // Catch:{ Exception -> 0x0133 }
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0133 }
                java.lang.String r3 = "GET:"
                r0.<init>(r3)     // Catch:{ Exception -> 0x0133 }
                java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0133 }
                r0.toString()     // Catch:{ Exception -> 0x0133 }
                java.net.URLConnection r0 = r2.openConnection()     // Catch:{ Exception -> 0x0133 }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0133 }
                r1 = 1
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.String r1 = "GET"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                r1 = 0
                r0.setUseCaches(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.String r3 = "ResponseCode:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                r2.toString()     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                r2 = 200(0xc8, float:2.8E-43)
                if (r1 != r2) goto L_0x00d3
                long r1 = me.gall.tinybee.TinybeeLogger.Config.getTimeStamp()     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                r6.updateLastTimestamp(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.String r2 = "UTF-8"
                java.lang.String r1 = r6.read(r1, r2)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                r6.consumeContent(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
            L_0x00c6:
                if (r0 == 0) goto L_0x00cb
                r0.disconnect()
            L_0x00cb:
                return
            L_0x00cc:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ Exception -> 0x0133 }
                r0 = r2
                goto L_0x001a
            L_0x00d3:
                r2 = 304(0x130, float:4.26E-43)
                if (r1 != r2) goto L_0x00f5
                long r1 = me.gall.tinybee.TinybeeLogger.Config.getTimeStamp()     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                r6.updateLastTimestamp(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                goto L_0x00c6
            L_0x00e3:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x00e7:
                r0.printStackTrace()     // Catch:{ all -> 0x0131 }
                r0 = 3
                r2 = 1
                r6.retry(r0, r2)     // Catch:{ all -> 0x0131 }
                if (r1 == 0) goto L_0x00cb
                r1.disconnect()
                goto L_0x00cb
            L_0x00f5:
                r2 = 404(0x194, float:5.66E-43)
                if (r1 != r2) goto L_0x011c
                long r1 = me.gall.tinybee.TinybeeLogger.Config.getTimeStamp()     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                r6.updateLastTimestamp(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                android.content.Context r1 = r6.context     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r2 = r6.getConf()     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.String r2 = r2.getAppId()     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                me.gall.tinybee.TinybeeLogger.clearUpdateInfo(r1, r2)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                goto L_0x00c6
            L_0x0112:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x0116:
                if (r1 == 0) goto L_0x011b
                r1.disconnect()
            L_0x011b:
                throw r0
            L_0x011c:
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.String r4 = "Invalid response:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                r2.<init>(r1)     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
                throw r2     // Catch:{ Exception -> 0x00e3, all -> 0x0112 }
            L_0x0131:
                r0 = move-exception
                goto L_0x0116
            L_0x0133:
                r0 = move-exception
                goto L_0x00e7
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.tinybee.TinybeeLogger.RequestUpdateTask.updateInfo():void");
        }
    }

    protected TinybeeLogger(Context context2, LoggerManager.LoggerConfiguration loggerConfiguration) {
        if (loggerConfiguration == null) {
            throw new NullPointerException("LoggerConfiguration must not be null.");
        }
        this.context = context2;
        this.conf = loggerConfiguration;
        if (this.executorService == null) {
            this.executorService = Executors.newSingleThreadExecutor();
        }
    }

    /* access modifiers changed from: private */
    public static void clearUpdateInfo(Context context2, String str) {
        SharedPreferences.Editor edit = context2.getSharedPreferences("_TB_UPDATEINFO_" + str, 0).edit();
        edit.clear();
        edit.commit();
    }

    private void executeTask(Runnable runnable) {
        if (this.executorService.isShutdown() || this.executorService.isTerminated()) {
            this.executorService = null;
            this.executorService = Executors.newSingleThreadExecutor();
        }
        this.executorService.execute(runnable);
    }

    private static void fillIntent(Intent intent, Context context2, LoggerManager.LoggerConfiguration loggerConfiguration) {
        SharedPreferences sharedPreferences = context2.getSharedPreferences("_TB_UPDATEINFO_" + loggerConfiguration.getAppId(), 0);
        intent.putExtra("Title", sharedPreferences.getString("Title", "Untilted"));
        intent.putExtra("View", sharedPreferences.getInt("View", 0));
        intent.putExtra("Content", sharedPreferences.getString("Content", ""));
        intent.putExtra("URL_Type", sharedPreferences.getInt("URL_Type", 1));
        intent.putExtra("URL", sharedPreferences.getString("URL", "http://gall.me"));
        intent.putExtra("Version", sharedPreferences.getInt("Version", 1));
        intent.putExtra("Force", sharedPreferences.getBoolean("Force", true));
        intent.putExtra("AppId", loggerConfiguration.getAppId());
        intent.putExtra("ChannelId", loggerConfiguration.getChannelId());
        intent.putExtra("ChannelName", loggerConfiguration.getChannelName());
        intent.putExtra("SandboxMode", loggerConfiguration.isSandboxMode());
    }

    /* access modifiers changed from: private */
    public static String getUUID(Context context2, String str) {
        return context2.getSharedPreferences(TINYBEE_UUIDS, 0).getString(str, null);
    }

    private synchronized List<EventTask> retrieveOfflineTasks(Context context2, String str) {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList();
            SharedPreferences sharedPreferences = context2.getSharedPreferences("_TB_" + str, 0);
            int i = sharedPreferences.getInt("_TB_Length", 0);
            "load() length = " + i;
            for (int i2 = 0; i2 < i; i2++) {
                String string = sharedPreferences.getString("_TB_KEY_" + i2 + "event", "");
                String string2 = sharedPreferences.getString("_TB_KEY_" + i2 + "value", null);
                String string3 = sharedPreferences.getString("_TB_KEY_" + i2 + "timeStamp", "0");
                "load()  event:" + string + "   value:" + string2 + "   timestamp:" + string3;
                if (string != null) {
                    EventTask eventTask = new EventTask(context2, this.conf, string);
                    if (string2 != null) {
                        try {
                            eventTask.params = new JSONObject(string2);
                        } catch (JSONException e) {
                            "invalid content = " + string2;
                        }
                    }
                    eventTask.timestamp = Long.parseLong(string3);
                    arrayList.add(eventTask);
                }
            }
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.clear();
            edit.commit();
        }
        return arrayList;
    }

    private synchronized void saveAtOnce() {
        if (!this.executorService.isShutdown()) {
            List<Runnable> shutdownNow = this.executorService.shutdownNow();
            "There are " + shutdownNow.size() + " tasks running. Append offline data immediately.";
            if (!shutdownNow.isEmpty()) {
                Iterator<Runnable> it = shutdownNow.iterator();
                while (it.hasNext()) {
                    ((EventTask) it.next()).appendOfflineData(this.context);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static void saveUUID(Context context2, String str, String str2) {
        context2.getSharedPreferences(TINYBEE_UUIDS, 0).edit().putString(str, str2).commit();
    }

    /* access modifiers changed from: private */
    public static void saveUpdateInfo(Context context2, String str, String str2, int i, String str3, int i2, String str4, int i3, boolean z, int i4) {
        SharedPreferences.Editor edit = context2.getSharedPreferences("_TB_UPDATEINFO_" + str, 0).edit();
        edit.clear();
        edit.commit();
        edit.putString("Title", str2);
        edit.putInt("View", i);
        edit.putString("Content", str3);
        edit.putInt("URL_Type", i2);
        edit.putString("URL", str4);
        edit.putInt("Version", i3);
        edit.putBoolean("Force", z);
        edit.putInt("Delay", i4);
        edit.commit();
    }

    /* access modifiers changed from: private */
    public static void showUpdateAcitivity(Context context2, LoggerManager.LoggerConfiguration loggerConfiguration) {
        Intent intent = new Intent();
        fillIntent(intent, context2, loggerConfiguration);
        intent.setFlags(603979776);
        intent.setClass(context2, TinybeeUpdateActivity.class);
        context2.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public static void updateIfNeed(final Context context2, final LoggerManager.LoggerConfiguration loggerConfiguration) {
        int i;
        int i2 = context2.getSharedPreferences("_TB_UPDATEINFO_" + loggerConfiguration.getAppId(), 0).getInt("Version", 1);
        try {
            i = context2.getPackageManager().getPackageInfo(context2.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            i = 1;
        }
        if (i2 > i) {
            int i3 = context2.getSharedPreferences("_TB_UPDATEINFO_" + loggerConfiguration.getAppId(), 0).getInt("Delay", 0);
            "Delay:" + i3;
            if (i3 > 0) {
                new Timer().schedule(new TimerTask() {
                    public void run() {
                        TinybeeLogger.showUpdateAcitivity(context2, loggerConfiguration);
                    }
                }, (long) (i3 * 1000));
            } else {
                showUpdateAcitivity(context2, loggerConfiguration);
            }
        } else {
            "No need to update." + i2 + "<=" + i;
        }
    }

    public void finish() {
        long j = this.context.getSharedPreferences(TINYBEE_RUNTIMES, 0).getLong(this.conf.getAppId(), 0);
        if (j == 0) {
            Log.w(TAG, "Invalid start time 0. May init not called");
            return;
        }
        long timeStamp = Config.getTimeStamp() - j;
        EventTask eventTask = new EventTask(this.context, this.conf, "_TB_runtime");
        eventTask.addParam("RUNTIME", String.valueOf(timeStamp));
        eventTask.appendOfflineData(this.context);
    }

    public Context getContext() {
        return this.context;
    }

    public boolean hasOfflineData() {
        return this.context.getSharedPreferences(new StringBuilder("_TB_").append(this.conf.getAppId()).toString(), 0).getInt("_TB_Length", 0) > 0;
    }

    public void init() {
        updateIfNeed(this.context, this.conf);
        "SandboxMode is " + isSandboxMode();
        launch();
        if (hasOfflineData()) {
            syncOfflineData();
        }
        this.context.getSharedPreferences(TINYBEE_RUNTIMES, 0).edit().putLong(this.conf.getAppId(), Config.getTimeStamp()).commit();
    }

    public boolean isSandboxMode() {
        return this.context.getSharedPreferences("_TB_" + this.conf.getAppId(), 0).getBoolean("_TB_SANDBOX", false);
    }

    /* access modifiers changed from: protected */
    public void launch() {
        EventTask eventTask = new EventTask(this.context, this.conf, _TB_launch);
        eventTask.addParam("NETWORK", Config.getNetwork(this.context));
        eventTask.addParam("IMSI", Config.getIMSI(this.context));
        eventTask.addParam("CHANNELNAME", this.conf.getChannelName());
        eventTask.addParam("CHANNELID", this.conf.getChannelId());
        try {
            eventTask.addParam("VERSIONSCODE", String.valueOf(this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionCode));
            eventTask.addParam("VERSIONSNAME", String.valueOf(this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
        }
        eventTask.addParam("PACKAGENAME", this.context.getPackageName());
        executeTask(eventTask);
        executeTask(new RequestUpdateTask(this.context, this.conf));
    }

    public void onPause(Context context2) {
        saveAtOnce();
    }

    public void onResume(Context context2) {
        if (hasOfflineData()) {
            syncOfflineData();
        }
    }

    public void send(String str) {
        send(str, null);
    }

    public void send(String str, Map<String, String> map) {
        EventTask eventTask = new EventTask(this.context, this.conf, str);
        if (map != null) {
            for (String next : map.keySet()) {
                if (map.containsKey(next)) {
                    eventTask.addParam(next, map.get(next));
                }
            }
        }
        executeTask(eventTask);
    }

    public void setOnlineParamCallback(Logger.OnlineParamCallback onlineParamCallback) {
        if (onlineParamCallback != null) {
            updateOnlineParam(onlineParamCallback);
        }
    }

    public void syncOfflineData() {
        if (Config.isConnect(this.context)) {
            for (EventTask next : retrieveOfflineTasks(this.context, this.conf.getAppId())) {
                "Start to sync " + next.getEventId();
                executeTask(next);
            }
        }
    }

    public void updateOnlineParam(Logger.OnlineParamCallback onlineParamCallback) {
        try {
            executeTask(new RequestLiveParamsTask(this.context, this.conf, onlineParamCallback));
        } catch (Exception e) {
            Log.e(TAG, "初始化或更新在线参数异常:" + e.getMessage());
        }
    }
}
