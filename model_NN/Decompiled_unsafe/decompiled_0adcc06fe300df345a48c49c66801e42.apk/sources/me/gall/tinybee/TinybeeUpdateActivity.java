package me.gall.tinybee;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.umeng.common.b.e;
import com.umeng.common.net.l;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import javax.microedition.media.control.MetaDataControl;
import me.gall.tinybee.TinybeeLogger;

public class TinybeeUpdateActivity extends Activity implements Handler.Callback {
    public static final String LOG = "TinybeeUpdate";
    public static final int MSG_ID_REFRESH_BUTTON_TEXT = 1;
    private static final String fileName = "tb_update.apk";
    /* access modifiers changed from: private */
    public Button confirm;
    private boolean force;
    public Handler handler;
    /* access modifiers changed from: private */
    public Logger logger;
    /* access modifiers changed from: private */
    public Map<String, String> params;
    /* access modifiers changed from: private */
    public String url;
    /* access modifiers changed from: private */
    public int urlType;
    private int viewType;
    private WebView wv;

    final class DownloadAndInstallTask extends AsyncTask<URL, Integer, Long> {
        DownloadAndInstallTask() {
        }

        private void downloadFile(int i, InputStream inputStream) {
            "Download file totalSize:" + i;
            FileOutputStream openFileOutput = TinybeeUpdateActivity.this.openFileOutput(TinybeeUpdateActivity.fileName, 1);
            byte[] bArr = new byte[10240];
            int i2 = 0;
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    openFileOutput.flush();
                    openFileOutput.close();
                    return;
                }
                openFileOutput.write(bArr, 0, read);
                i2 += read;
                "Download part size:" + i2;
                if (TinybeeUpdateActivity.this.confirm != null) {
                    NumberFormat percentInstance = NumberFormat.getPercentInstance();
                    percentInstance.setMaximumIntegerDigits(3);
                    percentInstance.setMinimumIntegerDigits(1);
                    if (i2 > i) {
                        i2 = i;
                    }
                    String format = percentInstance.format((double) (((float) i2) / ((float) i)));
                    Message obtainMessage = TinybeeUpdateActivity.this.handler.obtainMessage(1);
                    obtainMessage.obj = "下载中(" + format + ")";
                    TinybeeUpdateActivity.this.handler.sendMessage(obtainMessage);
                }
            }
        }

        /* access modifiers changed from: protected */
        public final Long doInBackground(URL... urlArr) {
            int i;
            try {
                File file = new File(TinybeeUpdateActivity.this.getFilePath());
                if (file.exists()) {
                    file.delete();
                }
                HttpURLConnection httpURLConnection = (HttpURLConnection) urlArr[0].openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                try {
                    i = Integer.parseInt(httpURLConnection.getHeaderField("Content-Length"));
                } catch (Exception e) {
                    e.printStackTrace();
                    i = 10485760;
                }
                downloadFile(i, inputStream);
                TinybeeUpdateActivity.this.logger.send("_TB_UPDATE_SUCCESS", TinybeeUpdateActivity.this.params);
                inputStream.close();
                Message obtainMessage = TinybeeUpdateActivity.this.handler.obtainMessage(1);
                obtainMessage.obj = "安装";
                TinybeeUpdateActivity.this.handler.sendMessage(obtainMessage);
                return 100L;
            } catch (IOException e2) {
                Log.e(TinybeeUpdateActivity.LOG, "Fail to download the file." + e2);
                return -1L;
            }
        }

        /* access modifiers changed from: protected */
        public final void onPostExecute(Long l) {
            if (l.longValue() == 100) {
                TinybeeUpdateActivity.this.installApk();
            } else if (l.longValue() == -1) {
                AlertDialog.Builder builder = new AlertDialog.Builder(TinybeeUpdateActivity.this);
                builder.setMessage("呃，下载出了点小问题，您可以立刻重试或者退出");
                builder.setPositiveButton("重试", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        try {
                            new DownloadAndInstallTask().execute(new URL(TinybeeUpdateActivity.this.url));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton("退出", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TinybeeUpdateActivity.this.finish();
                    }
                });
                builder.setCancelable(false);
                builder.show();
            } else {
                "On Post Execute result..." + l;
            }
        }
    }

    public static final View findViewByName(Context context, View view, String str) {
        return view.findViewById(getViewIdentifier(context, str));
    }

    private static final int getDrawableIdentifier(Context context, String str) {
        return getIdentifier(context, str, "drawable");
    }

    /* access modifiers changed from: private */
    public String getFilePath() {
        return String.valueOf(getFilesDir().getAbsolutePath()) + File.separator + fileName;
    }

    private static final int getIdentifier(Context context, String str, String str2) {
        int identifier = context.getResources().getIdentifier(str, str2, context.getPackageName());
        if (identifier != 0) {
            return identifier;
        }
        throw new FileNotFoundException(String.valueOf(str) + " is not found in res/" + str2 + "/.");
    }

    private static final int getLayoutIdentifier(Context context, String str) {
        return getIdentifier(context, str, "layout");
    }

    private static final int getViewIdentifier(Context context, String str) {
        try {
            return getIdentifier(context, str, "id");
        } catch (FileNotFoundException e) {
            throw new NullPointerException(String.valueOf(str) + " is not found.");
        }
    }

    private static final View inflateView(Context context, String str) {
        return ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(getLayoutIdentifier(context, str), (ViewGroup) null);
    }

    /* access modifiers changed from: private */
    public void installApk() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(getFilePath())), "application/vnd.android.package-archive");
        startActivity(intent);
    }

    public static int px2dip(Context context, int i) {
        return (int) TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics());
    }

    public boolean handleMessage(Message message) {
        if (message.what != 1) {
            return false;
        }
        String str = (String) message.obj;
        this.confirm.setText(str);
        if (!str.equals("安装")) {
            return false;
        }
        this.confirm.setEnabled(true);
        return false;
    }

    public void onBackPressed() {
        if (!this.force) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        this.logger = LoggerManager.getLogger(this, intent.getStringExtra("AppId"), intent.getStringExtra("ChannelId"), intent.getStringExtra("ChannelName"), intent.getBooleanExtra("SandboxMode", false));
        this.viewType = intent.getIntExtra("View", 0);
        this.force = intent.getBooleanExtra("Force", true);
        "Force:" + this.force;
        this.handler = new Handler(this);
        getWindow().requestFeature(1);
        this.url = intent.getStringExtra("URL");
        this.params = new HashMap();
        try {
            this.params.put("VERSIONSCODE", String.valueOf(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.params.put("PACKAGENAME", getPackageName());
        if (this.viewType == 0 || this.viewType == 2) {
            this.urlType = intent.getIntExtra("URL_Type", 1);
            try {
                View inflateView = inflateView(this, "tb_update_main_normal");
                if (this.viewType == 2) {
                    setTheme(16973835);
                }
                setContentView(inflateView);
                ((TextView) findViewByName(this, inflateView, MetaDataControl.TITLE_KEY)).setText(intent.getStringExtra("Title"));
                WebView webView = (WebView) findViewByName(this, inflateView, "webcontainer");
                if (this.viewType == 2) {
                    webView.setLayoutParams(new LinearLayout.LayoutParams(-1, px2dip(this, 240)));
                }
                webView.getSettings().setDefaultTextEncodingName(e.f);
                webView.setScrollBarStyle(33554432);
                webView.loadDataWithBaseURL(null, intent.getStringExtra("Content"), "text/html", e.f, null);
                this.confirm = (Button) findViewByName(this, inflateView, "confirm");
                this.confirm.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        TinybeeUpdateActivity.this.logger.send("_TB_UPDATE_CONFIRM", TinybeeUpdateActivity.this.params);
                        if (TinybeeUpdateActivity.this.urlType == 1) {
                            TinybeeUpdateActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(TinybeeUpdateActivity.this.url)));
                        } else if (TinybeeUpdateActivity.this.urlType != 0) {
                        } else {
                            if (((Button) view).getText().equals("安装")) {
                                TinybeeUpdateActivity.this.installApk();
                                return;
                            }
                            ((Button) view).setText("开始下载");
                            view.postInvalidate();
                            try {
                                new DownloadAndInstallTask().execute(new URL(TinybeeUpdateActivity.this.url));
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                            view.setEnabled(false);
                        }
                    }
                });
                ((Button) findViewByName(this, inflateView, l.c)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        TinybeeUpdateActivity.this.logger.send("_TB_UPDATE_CANCEL", TinybeeUpdateActivity.this.params);
                        TinybeeUpdateActivity.this.finish();
                    }
                });
            } catch (FileNotFoundException e2) {
                e2.printStackTrace();
            }
        } else {
            this.wv = new WebView(this);
            this.wv.setVerticalScrollBarEnabled(false);
            this.wv.loadUrl(this.url);
            setContentView(this.wv);
        }
        this.logger.send("_TB_UPDATE_INTRO", this.params);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.force) {
            LoggerManager.forceFinish();
        }
    }

    public void onResume() {
        super.onResume();
        if (!TinybeeLogger.Config.isConnect(this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("哎哟，您的网络不给力啊，赶快检查检查");
            builder.setPositiveButton("设置网络", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    TinybeeUpdateActivity.this.startActivity(new Intent("android.settings.WIFI_SETTINGS"));
                }
            });
            builder.setNegativeButton("退出", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    TinybeeUpdateActivity.this.finish();
                }
            });
            builder.setCancelable(false);
            builder.show();
        }
        if (TinybeeLogger.Config.isConnect(this) && this.wv != null) {
            this.wv.reload();
        }
    }
}
