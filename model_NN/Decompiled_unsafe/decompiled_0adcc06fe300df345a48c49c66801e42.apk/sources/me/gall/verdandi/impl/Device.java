package me.gall.verdandi.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import me.gall.verdandi.IDevice;

public class Device implements IDevice {

    public static class IntentIntegrator {
        private static final String BSPLUS_PACKAGE = "com.srowen.bs.android";
        private static final String BS_PACKAGE = "com.google.zxing.client.android";
        public static final String DEFAULT_MESSAGE = "This application requires Barcode Scanner. Would you like to install it?";
        public static final String DEFAULT_NO = "No";
        public static final String DEFAULT_TITLE = "Install Barcode Scanner?";
        public static final String DEFAULT_YES = "Yes";
        private static final int INSTALL_PLUGIN_REQUEST_CODE = 54260;
        private static final String PLUGIN_APK_FILENAME = "zxing.apk";
        private static final String PLUGIN_PACKAGE_NAME = "com.google.zxing.client.android";
        public static final int REQUEST_CODE = 49374;
        private static final String TAG = IntentIntegrator.class.getSimpleName();
        public static final Collection<String> de = Collections.unmodifiableCollection(Arrays.asList("UPC_A", "UPC_E", "EAN_8", "EAN_13", "RSS_14"));
        public static final Collection<String> df = Collections.unmodifiableCollection(Arrays.asList("UPC_A", "UPC_E", "EAN_8", "EAN_13", "CODE_39", "CODE_93", "CODE_128", "ITF", "RSS_14", "RSS_EXPANDED"));
        public static final Collection<String> dg = Collections.singleton("QR_CODE");
        public static final Collection<String> dh = Collections.singleton("DATA_MATRIX");
        public static final Collection<String> di = null;
        public static final Collection<String> dj = Collections.singleton("com.google.zxing.client.android");
        public static final Collection<String> dk = Collections.unmodifiableCollection(Arrays.asList("com.google.zxing.client.android", BSPLUS_PACKAGE, "com.srowen.bs.android.simple"));
    }
}
