package me.gall.verdandi.impl;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Message;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.microedition.media.control.MetaDataControl;
import me.gall.skuld.SNSPlatformManager;
import me.gall.skuld.adapter.FeatureNotSupportException;
import me.gall.skuld.adapter.SNSPlatformAdapter;
import me.gall.verdandi.ISocial;
import org.meteoroid.core.h;
import org.meteoroid.core.k;

public final class Social implements DialogInterface.OnClickListener, ISocial, h.a {
    private static final String LOG = "Social";
    private SNSPlatformAdapter dl;
    private int dm = 1;
    private List<String> dn = new ArrayList();

    /* renamed from: do  reason: not valid java name */
    private Map<String, String> f0do = new HashMap();
    private AlarmManager dp = null;

    public static class AlarmReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            Intent intent2;
            PackageManager.NameNotFoundException e;
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            int intValue = ((Integer) intent.getExtras().get("iconID")).intValue();
            CharSequence charSequence = (CharSequence) intent.getExtras().get(MetaDataControl.TITLE_KEY);
            CharSequence charSequence2 = (CharSequence) intent.getExtras().get("content");
            String str = (String) intent.getExtras().get("url");
            Notification notification = new Notification();
            notification.icon = intValue;
            if (str != null) {
                intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
            } else {
                Intent intent3 = new Intent();
                try {
                    PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    Intent intent4 = new Intent("android.intent.action.MAIN", (Uri) null);
                    intent4.addCategory("android.intent.category.LAUNCHER");
                    intent4.setPackage(packageInfo.packageName);
                    ResolveInfo next = context.getPackageManager().queryIntentActivities(intent4, 0).iterator().next();
                    if (next != null) {
                        String str2 = next.activityInfo.packageName;
                        String str3 = next.activityInfo.name;
                        intent2 = new Intent("android.intent.action.MAIN");
                        try {
                            intent2.addCategory("android.intent.category.LAUNCHER");
                            intent2.setComponent(new ComponentName(str2, str3));
                        } catch (PackageManager.NameNotFoundException e2) {
                            e = e2;
                        }
                    } else {
                        intent2 = intent3;
                    }
                } catch (PackageManager.NameNotFoundException e3) {
                    PackageManager.NameNotFoundException nameNotFoundException = e3;
                    intent2 = intent3;
                    e = nameNotFoundException;
                    e.printStackTrace();
                    notification.setLatestEventInfo(context, charSequence, charSequence2, PendingIntent.getActivity(context, 0, intent2, 134217728));
                    notificationManager.notify(1, notification);
                }
            }
            notification.setLatestEventInfo(context, charSequence, charSequence2, PendingIntent.getActivity(context, 0, intent2, 134217728));
            notificationManager.notify(1, notification);
        }
    }

    public final boolean a(Message message) {
        if (message.what == 40961) {
            SNSPlatformManager.onResume();
            return false;
        } else if (message.what == 40960) {
            SNSPlatformManager.onPause();
            return false;
        } else if (message.what != 40968) {
            return false;
        } else {
            SNSPlatformManager.onDestroy();
            return false;
        }
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == -1) {
            try {
                if (this.dl == null) {
                    SNSPlatformManager.a(k.getActivity(), k.ak(), new HashMap());
                    this.dl = SNSPlatformManager.J();
                    h.a(this);
                }
                this.dl.h(0);
            } catch (FeatureNotSupportException e) {
                e.printStackTrace();
            }
        }
    }
}
