package me.gall.skuld.util;

public final class Configuration {
    private static final String LOG_TAG = "skuld:Configuration";

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0047, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0048, code lost:
        r4 = r0;
        r0 = null;
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x004b, code lost:
        java.lang.String.valueOf(r1.getMessage()) + " int to String error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006d, code lost:
        r1 = r0;
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return java.lang.String.valueOf(r0.metaData.getInt(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0023, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x006c A[ExcHandler: ClassCastException (r0v10 'e' java.lang.ClassCastException A[CUSTOM_DECLARE]), Splitter:B:3:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0022 A[ExcHandler: NameNotFoundException (r0v7 'e' android.content.pm.PackageManager$NameNotFoundException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String h(android.content.Context r5, java.lang.String r6) {
        /*
            r1 = 0
            android.content.pm.PackageManager r0 = r5.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0022, ClassCastException -> 0x0047 }
            java.lang.String r2 = r5.getPackageName()     // Catch:{ NameNotFoundException -> 0x0022, ClassCastException -> 0x0047 }
            r3 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r2 = r0.getApplicationInfo(r2, r3)     // Catch:{ NameNotFoundException -> 0x0022, ClassCastException -> 0x0047 }
            android.os.Bundle r0 = r2.metaData     // Catch:{ NameNotFoundException -> 0x0022, ClassCastException -> 0x006c }
            java.lang.String r0 = r0.getString(r6)     // Catch:{ NameNotFoundException -> 0x0022, ClassCastException -> 0x006c }
            if (r0 != 0) goto L_0x0021
            android.os.Bundle r1 = r2.metaData     // Catch:{ NameNotFoundException -> 0x0070, ClassCastException -> 0x006c }
            int r1 = r1.getInt(r6)     // Catch:{ NameNotFoundException -> 0x0070, ClassCastException -> 0x006c }
            java.lang.String r0 = java.lang.String.valueOf(r1)     // Catch:{ NameNotFoundException -> 0x0070, ClassCastException -> 0x006c }
        L_0x0021:
            return r0
        L_0x0022:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x0026:
            java.lang.String r2 = "skuld:Configuration"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r1 = r1.getMessage()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r3.<init>(r1)
            java.lang.String r1 = " unknown meta key or not exist:"
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r1 = r1.toString()
            android.util.Log.w(r2, r1)
            goto L_0x0021
        L_0x0047:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x004b:
            android.os.Bundle r0 = r0.metaData
            int r0 = r0.getInt(r6)
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r1 = r1.getMessage()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r2.<init>(r1)
            java.lang.String r1 = " int to String error"
            java.lang.StringBuilder r1 = r2.append(r1)
            r1.toString()
            goto L_0x0021
        L_0x006c:
            r0 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x004b
        L_0x0070:
            r1 = move-exception
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: me.gall.skuld.util.Configuration.h(android.content.Context, java.lang.String):java.lang.String");
    }
}
