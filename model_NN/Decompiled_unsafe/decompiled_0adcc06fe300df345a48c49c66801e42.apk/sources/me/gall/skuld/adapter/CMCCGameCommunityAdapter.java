package me.gall.skuld.adapter;

import android.content.DialogInterface;
import cn.emagsoftware.gamecommunity.api.GameCommunity;
import java.util.Map;
import me.gall.skuld.SNSPlatformManager;
import me.gall.skuld.util.DataMapper;

public class CMCCGameCommunityAdapter extends a implements DialogInterface.OnClickListener {
    public final /* bridge */ /* synthetic */ String M() {
        return super.M();
    }

    public final /* bridge */ /* synthetic */ void a(DataMapper dataMapper) {
        super.a(dataMapper);
    }

    public final void b(Map<String, String> map) {
        super.b(map);
        getClass().getSimpleName();
        "Key:" + super.getKey() + " Secret:" + super.M() + " Id:" + super.getId();
        if (super.getKey().length() < 12) {
            StringBuffer stringBuffer = new StringBuffer();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= 12 - super.getKey().length()) {
                    break;
                }
                stringBuffer.append('0');
                i = i2 + 1;
            }
            super.setKey(stringBuffer.append(super.getKey()).toString());
            getClass().getSimpleName();
            "Fix key:" + super.getKey();
        }
        GameCommunity.initializeSDK(SNSPlatformManager.K(), SNSPlatformManager.L(), super.getKey(), super.M(), super.getId(), SNSPlatformManager.K().getPackageName());
    }

    public final /* bridge */ /* synthetic */ void b(DataMapper dataMapper) {
        super.b((DataMapper<String>) dataMapper);
    }

    public final /* bridge */ /* synthetic */ void c(DataMapper dataMapper) {
        super.c(dataMapper);
    }

    public final /* bridge */ /* synthetic */ String getId() {
        return super.getId();
    }

    public final /* bridge */ /* synthetic */ String getKey() {
        return super.getKey();
    }

    public final void h(int i) {
        GameCommunity.launchGameCommunity(SNSPlatformManager.K());
    }

    public final /* bridge */ /* synthetic */ void l(String str) {
        super.l(str);
    }

    public final /* bridge */ /* synthetic */ void m(String str) {
        super.m(str);
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == -1) {
            GameCommunity.launchGameCommunity(SNSPlatformManager.K());
        } else {
            dialogInterface.dismiss();
        }
    }

    public final void onDestroy() {
        GameCommunity.exit();
    }

    public final void onPause() {
    }

    public final void onResume() {
    }

    public final /* bridge */ /* synthetic */ void setKey(String str) {
        super.setKey(str);
    }
}
