package me.gall.skuld.adapter;

import android.os.AsyncTask;
import java.util.Map;

public interface SNSPlatformAdapter {
    public static final int LAUNCH_DASHBOARD = 0;
    public static final int LAUNCH_GAME_ARCHIEVEMENT = 3;
    public static final int LAUNCH_GAME_DETAIL = 1;
    public static final int LAUNCH_GAME_LEADERBOARD = 2;
    public static final int LAUNCH_GAME_OTHER = 4;

    public interface AsyncResultCallback<R> {
        void P();

        void Q();
    }

    public interface AsyncWorkerAndResultCallback<R> extends AsyncResultCallback<R> {
        R R();
    }

    public static class SkuldAsyncTask<R> extends AsyncTask<Void, Void, R> {
        private AsyncWorkerAndResultCallback<R> cW;
        private String cX;

        private R S() {
            try {
                if (this.cW != null) {
                    return this.cW.R();
                }
                throw new Exception("AsyncResultCallback could not be null");
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public /* synthetic */ Object doInBackground(Object... objArr) {
            return S();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(R r) {
            if (r != null) {
                this.cW.P();
                return;
            }
            AsyncWorkerAndResultCallback<R> asyncWorkerAndResultCallback = this.cW;
            String str = this.cX;
            asyncWorkerAndResultCallback.Q();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }
    }

    void b(Map<String, String> map);

    void h(int i);

    void onDestroy();

    void onPause();

    void onResume();
}
