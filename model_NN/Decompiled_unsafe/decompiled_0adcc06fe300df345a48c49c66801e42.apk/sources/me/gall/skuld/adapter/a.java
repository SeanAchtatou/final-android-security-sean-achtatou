package me.gall.skuld.adapter;

import java.util.Map;
import me.gall.skuld.SNSPlatformManager;
import me.gall.skuld.util.Configuration;
import me.gall.skuld.util.DataMapper;

abstract class a implements SNSPlatformAdapter {
    public static final String META_TAG_SKULD_ADAPTER_APP_ID = "SKULD_ADAPTER_APP_ID";
    public static final String META_TAG_SKULD_ADAPTER_APP_KEY = "SKULD_ADAPTER_APP_KEY";
    public static final String META_TAG_SKULD_ADAPTER_APP_SECRET = "SKULD_ADAPTER_APP_SECRET";
    public static final String META_TAG_SKULD_ARCHIEVEMENT_ID_MAPPER = "SKULD_ARCHIEVEMENT_ID_MAPPER";
    public static final String META_TAG_SKULD_BILLING_ID_MAPPER = "SKULD_BILLING_ID_MAPPER";
    public static final String META_TAG_SKULD_LEADERBOARD_ID_MAPPER = "SKULD_LEADERBOARD_ID_MAPPER";
    private DataMapper<String> cY;
    private DataMapper<String> cZ;
    private DataMapper<String> da;
    private String db;
    private String dc;
    private String dd;

    a() {
    }

    public String M() {
        return this.dc;
    }

    public void a(DataMapper<String> dataMapper) {
        this.cZ = dataMapper;
    }

    public void b(Map<String, String> map) {
        String h = Configuration.h(SNSPlatformManager.K(), META_TAG_SKULD_LEADERBOARD_ID_MAPPER);
        if (h != null) {
            c(new DataMapper().b(h.split(";")));
        }
        String h2 = Configuration.h(SNSPlatformManager.K(), META_TAG_SKULD_ARCHIEVEMENT_ID_MAPPER);
        if (h2 != null) {
            a(new DataMapper().b(h2.split(";")));
        }
        String h3 = Configuration.h(SNSPlatformManager.K(), META_TAG_SKULD_BILLING_ID_MAPPER);
        if (h3 != null) {
            b((DataMapper<String>) new DataMapper().b(h3.split(";")));
        }
        setKey(Configuration.h(SNSPlatformManager.K(), META_TAG_SKULD_ADAPTER_APP_KEY));
        l(Configuration.h(SNSPlatformManager.K(), META_TAG_SKULD_ADAPTER_APP_SECRET));
        m(Configuration.h(SNSPlatformManager.K(), META_TAG_SKULD_ADAPTER_APP_ID));
    }

    public void b(DataMapper<String> dataMapper) {
        this.da = dataMapper;
    }

    public void c(DataMapper<String> dataMapper) {
        this.cY = dataMapper;
    }

    public String getId() {
        return this.dd;
    }

    public String getKey() {
        return this.db;
    }

    public void l(String str) {
        this.dc = str;
    }

    public void m(String str) {
        this.dd = str;
    }

    public void setKey(String str) {
        this.db = str;
    }
}
