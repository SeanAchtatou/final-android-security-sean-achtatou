package me.gall.skuld.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.tencent.webnet.WebNetEvent;
import com.tencent.webnet.WebNetInterface;
import java.io.FileNotFoundException;
import java.util.Map;
import me.gall.skuld.SNSPlatformManager;
import me.gall.skuld.adapter.SNSPlatformAdapter;
import me.gall.skuld.util.DataMapper;
import org.opensocial.models.skuld.Billing;

public class QQGameCenterAdapter extends a implements WebNetEvent {
    /* access modifiers changed from: private */
    public static SNSPlatformAdapter.AsyncResultCallback<Billing> cO;
    private boolean cN;

    public static class SMSPaymentActivity extends Activity implements Handler.Callback {
        private static final int HIDE_PROCESSING_DIALOG = 2;
        private static final String LOG = "SMSPaymentActivity";
        private static final int SHOW_PROCESSING_DIALOG = 1;
        /* access modifiers changed from: private */
        public String cR;
        private ProgressDialog cS;
        private ViewGroup cT;
        /* access modifiers changed from: private */
        public Handler handler;

        /* access modifiers changed from: private */
        public void O() {
            this.handler.sendEmptyMessage(2);
            finish();
        }

        private static View findViewByName(Context context, View view, String str) {
            return view.findViewById(getViewIdentifier(context, str));
        }

        private static int getIdentifier(Context context, String str, String str2) {
            int identifier = context.getResources().getIdentifier(str, str2, context.getPackageName());
            if (identifier != 0) {
                return identifier;
            }
            throw new FileNotFoundException(String.valueOf(str) + " is not found in res/" + str2 + "/.");
        }

        private static int getViewIdentifier(Context context, String str) {
            try {
                return getIdentifier(context, str, "id");
            } catch (FileNotFoundException e) {
                throw new NullPointerException(String.valueOf(str) + " is not found.");
            }
        }

        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (getWindow().isActive()) {
                        if (this.cS == null) {
                            this.cS = new ProgressDialog(this);
                            this.cS.setMessage("请稍候");
                            this.cS.setCancelable(false);
                        }
                        if (!this.cS.isShowing()) {
                            this.cS.show();
                            break;
                        }
                    }
                    break;
                case 2:
                    if (this.cS != null && this.cS.isShowing()) {
                        this.cS.hide();
                        break;
                    }
            }
            return false;
        }

        public void onBackPressed() {
            O();
        }

        /* access modifiers changed from: protected */
        public void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            getWindow().requestFeature(1);
            Intent intent = getIntent();
            this.handler = new Handler(this);
            String stringExtra = intent.hasExtra("TEXT") ? intent.getStringExtra("TEXT") : null;
            this.cR = intent.getStringExtra("BILLINGID");
            try {
                this.cT = (ViewGroup) ((LayoutInflater) getSystemService("layout_inflater")).inflate(getIdentifier(this, "skuld_layout_main", "layout"), (ViewGroup) null);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            setContentView(this.cT);
            "TEXT:" + stringExtra;
            ((TextView) findViewByName(this, this.cT, "skuld_view_content")).setText(stringExtra);
            ((Button) findViewByName(this, this.cT, "skuld_view_confirm")).setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    SMSPaymentActivity.this.handler.sendEmptyMessage(1);
                    String str = "SMS_TECENT_BILLING_MARK:" + System.currentTimeMillis();
                    "Sending :" + SMSPaymentActivity.this.cR + " mark:" + str;
                    WebNetInterface.SMSBillingPoint(Integer.parseInt(SMSPaymentActivity.this.cR), str);
                    SMSPaymentActivity.this.handler.postDelayed(new Runnable() {
                        public final void run() {
                            SMSPaymentActivity.this.O();
                        }
                    }, 5000);
                }
            });
            ((Button) findViewByName(this, this.cT, "skuld_view_cancel")).setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    if (QQGameCenterAdapter.cO != null) {
                        QQGameCenterAdapter.cO.Q();
                    }
                    SMSPaymentActivity.this.O();
                }
            });
        }

        public void onDestroy() {
            if (this.cS != null) {
                this.cS.dismiss();
            }
            this.cS = null;
            super.onDestroy();
        }
    }

    public final /* bridge */ /* synthetic */ String M() {
        return super.M();
    }

    public final /* bridge */ /* synthetic */ void a(DataMapper dataMapper) {
        super.a(dataMapper);
    }

    public final void b(Map<String, String> map) {
        super.b(map);
        WebNetInterface.Init(SNSPlatformManager.K(), this);
    }

    public final /* bridge */ /* synthetic */ void b(DataMapper dataMapper) {
        super.b((DataMapper<String>) dataMapper);
    }

    public final /* bridge */ /* synthetic */ void c(DataMapper dataMapper) {
        super.c(dataMapper);
    }

    public final /* bridge */ /* synthetic */ String getId() {
        return super.getId();
    }

    public final /* bridge */ /* synthetic */ String getKey() {
        return super.getKey();
    }

    public final void h(int i) {
        SNSPlatformManager.K().runOnUiThread(new Runnable(0) {
            public final void run() {
                if (0 == 0) {
                    WebNetInterface.StartWeb(SNSPlatformManager.K());
                } else {
                    WebNetInterface.StartWeb(SNSPlatformManager.K(), 0);
                }
            }
        });
    }

    public final /* bridge */ /* synthetic */ void l(String str) {
        super.l(str);
    }

    public final /* bridge */ /* synthetic */ void m(String str) {
        super.m(str);
    }

    public final void onDestroy() {
        if (this.cN) {
            WebNetInterface.Destroy();
        }
    }

    public final void onPause() {
    }

    public final void onResume() {
        WebNetInterface.SetCurActivity(SNSPlatformManager.K());
    }

    public final /* bridge */ /* synthetic */ void setKey(String str) {
        super.setKey(str);
    }
}
