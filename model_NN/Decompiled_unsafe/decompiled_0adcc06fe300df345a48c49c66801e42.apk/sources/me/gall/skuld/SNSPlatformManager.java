package me.gall.skuld;

import android.app.Activity;
import android.util.Log;
import java.util.Map;
import me.gall.skuld.adapter.SNSPlatformAdapter;
import me.gall.skuld.util.Configuration;

public final class SNSPlatformManager {
    public static final String META_TAG_SKULD_ADAPTER_NAME = "SKULD_ADAPTER_NAME";
    private static final String TAG = "SNSPlatformManager";
    private static SNSPlatformAdapter cJ;
    private static Activity cK;
    private static String cL;
    private static boolean cM;

    public static SNSPlatformAdapter J() {
        if (cM) {
            return cJ;
        }
        throw new NullPointerException("SNSPlatformAdapter has not initilized. May be SKULD_ADAPTER_NAME in the manifest is wrong spelled or check the log for detail.");
    }

    public static Activity K() {
        return cK;
    }

    public static String L() {
        return cL;
    }

    public static void a(Activity activity, String str, Map<String, String> map) {
        cK = activity;
        cL = str;
        try {
            Class<?> cls = Class.forName("me.gall.skuld.adapter." + Configuration.h(cK, META_TAG_SKULD_ADAPTER_NAME) + "Adapter");
            cls.getName();
            SNSPlatformAdapter sNSPlatformAdapter = (SNSPlatformAdapter) cls.newInstance();
            cJ = sNSPlatformAdapter;
            sNSPlatformAdapter.b(map);
            cM = true;
        } catch (Exception e) {
            Log.e(TAG, "The SDK is not initilized because of " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static final void onDestroy() {
        if (cM) {
            cJ.onDestroy();
        } else {
            Log.w(TAG, "Skuld is not initilized yet.");
        }
    }

    public static final void onPause() {
        if (cM) {
            cJ.onPause();
        } else {
            Log.w(TAG, "Skuld is not initilized yet.");
        }
    }

    public static final void onResume() {
        if (cM) {
            cJ.onResume();
        } else {
            Log.w(TAG, "Skuld is not initilized yet.");
        }
    }
}
