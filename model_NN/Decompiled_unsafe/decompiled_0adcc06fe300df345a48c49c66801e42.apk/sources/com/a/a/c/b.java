package com.a.a.c;

import android.util.Log;
import com.a.a.b.a;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import javax.microedition.media.control.ToneControl;
import javax.microedition.rms.RecordListener;

public final class b {
    public static final int AUTHMODE_ANY = 1;
    public static final int AUTHMODE_PRIVATE = 0;
    public static final String LOG_TAG = "RMS";
    private static final String PREFIX = "rms_";
    private HashMap<Integer, byte[]> cE = new HashMap<>();
    private int cF;
    private long cG;
    private HashSet<RecordListener> cH = new HashSet<>();
    private boolean cI;
    private int cr;
    private String name;
    private int version;

    private b(String str) {
        this.name = str;
    }

    private void E() {
        this.cr = 0;
        for (byte[] length : this.cE.values()) {
            this.cr = length.length + this.cr;
        }
        "calcTheSizeOfRecords:" + this.cr;
    }

    private synchronized void F() {
        if (org.meteoroid.core.b.p(PREFIX + this.name)) {
            try {
                DataInputStream dataInputStream = new DataInputStream(org.meteoroid.core.b.r(PREFIX + this.name));
                int readInt = dataInputStream.readInt();
                "Current version:" + this.version + " newest version:" + readInt;
                dataInputStream.close();
                if (readInt > this.version) {
                    a(new DataInputStream(org.meteoroid.core.b.r(PREFIX + this.name)));
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
        return;
    }

    private synchronized void G() {
        DataOutputStream dataOutputStream = new DataOutputStream(org.meteoroid.core.b.q(PREFIX + this.name));
        a(dataOutputStream);
        dataOutputStream.flush();
        dataOutputStream.close();
        E();
    }

    public static b a(String str, boolean z) {
        "openRecordStore:" + str;
        b bVar = new b(str);
        if (org.meteoroid.core.b.p(PREFIX + str)) {
            str + " exist.";
            try {
                DataInputStream dataInputStream = new DataInputStream(org.meteoroid.core.b.r(PREFIX + str));
                bVar.a(dataInputStream);
                dataInputStream.close();
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
                throw new c();
            }
        } else {
            str + " not exist and necessary to create.";
            try {
                bVar.G();
            } catch (Exception e2) {
                Log.e(LOG_TAG, e2.getMessage());
                throw new c();
            }
        }
        bVar.cI = true;
        return bVar;
    }

    private synchronized void a(DataInputStream dataInputStream) {
        synchronized (this) {
            DataInputStream dataInputStream2 = new DataInputStream(dataInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[a.GAME_B_PRESSED];
            while (true) {
                int read = dataInputStream2.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            DataInputStream dataInputStream3 = new DataInputStream(new ByteArrayInputStream(byteArray));
            this.version = dataInputStream3.readInt();
            this.cG = dataInputStream3.readLong();
            this.cF = dataInputStream3.readInt();
            int readInt = dataInputStream3.readInt();
            for (int i = 0; i < readInt; i++) {
                int readInt2 = dataInputStream3.readInt();
                byte[] bArr2 = new byte[dataInputStream3.readInt()];
                dataInputStream3.read(bArr2);
                this.cE.put(Integer.valueOf(readInt2), bArr2);
            }
            try {
                String readUTF = dataInputStream3.readUTF();
                dataInputStream3.close();
                byte[] bArr3 = new byte[((byteArray.length - readUTF.getBytes().length) - 2)];
                System.arraycopy(byteArray, 0, bArr3, 0, bArr3.length);
                if (!readUTF.equals(b(bArr3, "MD5"))) {
                    Log.w(LOG_TAG, "Verify sign fail:" + readUTF);
                    throw new IllegalArgumentException("Invalid sign. The file has been modified.");
                }
            } catch (IOException e) {
            } catch (Exception e2) {
                throw new IOException(e2.getMessage());
            }
            E();
        }
    }

    private synchronized void a(DataOutputStream dataOutputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
        this.version++;
        dataOutputStream2.writeInt(this.version);
        dataOutputStream2.writeLong(System.currentTimeMillis());
        dataOutputStream2.writeInt(this.cF);
        dataOutputStream2.writeInt(this.cE.size());
        for (Map.Entry next : this.cE.entrySet()) {
            dataOutputStream2.writeInt(((Integer) next.getKey()).intValue());
            byte[] bArr = (byte[]) next.getValue();
            dataOutputStream2.writeInt(bArr.length);
            dataOutputStream2.write(bArr);
        }
        byteArrayOutputStream.flush();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String b = b(byteArray, "MD5");
        "Generate sign:" + b;
        dataOutputStream.write(byteArray);
        dataOutputStream.writeUTF(b);
        dataOutputStream.flush();
    }

    private static String b(byte[] bArr, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str.toUpperCase());
            instance.reset();
            instance.update(bArr);
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < digest.length; i++) {
                if (Integer.toHexString(digest[i] & ToneControl.SILENCE).length() == 1) {
                    stringBuffer.append("0").append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
                } else {
                    stringBuffer.append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
                }
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean isOpen() {
        if (this.cI) {
            return this.cI;
        }
        Log.e(LOG_TAG, "RecordStoreNotOpenException");
        throw new f();
    }

    public final void H() {
        isOpen();
        F();
        "closeRecordStore" + this.name + " at version:" + this.version;
        this.cH.clear();
        this.cH = null;
        this.cE.clear();
        this.cI = false;
        this.cE = null;
        this.name = null;
        System.gc();
    }

    public final int I() {
        isOpen();
        F();
        "getNumRecords" + this.cE.size();
        return this.cE.size();
    }

    public final int d(byte[] bArr, int i, int i2) {
        isOpen();
        this.cF++;
        "addRecord:" + this.cF + " offset:0" + " numBytes:" + i2;
        byte[] bArr2 = new byte[i2];
        if (i2 != 0) {
            System.arraycopy(bArr, 0, bArr2, 0, i2);
        }
        this.cE.put(Integer.valueOf(this.cF), bArr2);
        try {
            G();
            Iterator<RecordListener> it = this.cH.iterator();
            while (it.hasNext()) {
                it.next();
                int i3 = this.cF;
            }
            return this.cF;
        } catch (Exception e) {
            throw new c();
        }
    }

    public final byte[] g(int i) {
        isOpen();
        F();
        if (this.cE.containsKey(1)) {
            "getRecord" + 1 + " length:" + this.cE.get(1).length;
            return this.cE.get(1);
        }
        throw new a("recordId=" + 1);
    }
}
