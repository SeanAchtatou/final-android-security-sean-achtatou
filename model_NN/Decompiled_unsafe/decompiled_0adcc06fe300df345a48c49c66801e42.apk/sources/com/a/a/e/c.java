package com.a.a.e;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;
import com.androidbox.g5msnjhckhdeoe.R;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import org.meteoroid.core.e;
import org.meteoroid.core.k;

public final class c {
    public static final String LOG_TAG = "ResourceUtils";
    private static String hN;
    private static final HashMap<String, Bitmap> hO = new HashMap<>();

    public static void D(String str) {
        if (str != null) {
            hN = str + File.separator;
        } else {
            hN = null;
        }
    }

    public static final String E(String str) {
        return "abresource" + File.separator + str;
    }

    private static int F(String str) {
        try {
            return b(R.drawable.class, str);
        } catch (Exception e) {
            throw new IOException("Fail to get resource id:" + str);
        }
    }

    public static int G(String str) {
        try {
            return b(R.raw.class, str);
        } catch (Exception e) {
            throw new IOException("Fail to get raw id:" + str);
        }
    }

    public static Rect H(String str) {
        if (str == null) {
            Log.w(LOG_TAG, "Rect string couldn't be null.");
            return null;
        }
        String[] split = str.split(",");
        if (split.length != 4) {
            throw new IllegalArgumentException("Invalid rect string:" + str);
        }
        int parseInt = Integer.parseInt(split[0].trim());
        int parseInt2 = Integer.parseInt(split[1].trim());
        return new Rect(parseInt, parseInt2, Integer.parseInt(split[2].trim()) + parseInt, Integer.parseInt(split[3].trim()) + parseInt2);
    }

    public static Bitmap I(String str) {
        if (str == null) {
            Log.e(LOG_TAG, "Bitmap string couldn't be null.");
            return null;
        } else if (str.trim().length() == 0) {
            return null;
        } else {
            if (hO.containsKey(str)) {
                "Wow, " + str + " hit the bitmap cache.";
                return hO.get(str);
            }
            if (hN != null) {
                Bitmap b = e.b(k.t(hN + str + ".png"));
                hO.put(str, b);
                return b;
            }
            try {
                Bitmap decodeResource = BitmapFactory.decodeResource(k.getActivity().getResources(), F(str.trim()));
                hO.put(str, decodeResource);
                return decodeResource;
            } catch (Exception e) {
                throw new IOException("Fail to load bitmap " + str + ":" + e.toString());
            }
        }
    }

    public static Bitmap[] J(String str) {
        if (str == null) {
            Log.e(LOG_TAG, "Bitmap string couldn't be null.");
            return null;
        }
        String[] split = str.split(",");
        if (split.length <= 0) {
            return null;
        }
        Bitmap[] bitmapArr = new Bitmap[split.length];
        for (int i = 0; i < split.length; i++) {
            bitmapArr[i] = I(split[i].trim());
        }
        return bitmapArr;
    }

    public static final String K(String str) {
        int lastIndexOf = str.lastIndexOf(File.separator);
        return lastIndexOf == -1 ? "" : str.substring(0, lastIndexOf + 1);
    }

    public static final String L(String str) {
        int lastIndexOf = str.lastIndexOf(File.separator);
        return lastIndexOf == -1 ? str : str.substring(lastIndexOf + 1);
    }

    private static int b(Class<?> cls, String str) {
        return cls.getField(str).getInt(null);
    }

    public static void bd() {
        hO.clear();
    }
}
