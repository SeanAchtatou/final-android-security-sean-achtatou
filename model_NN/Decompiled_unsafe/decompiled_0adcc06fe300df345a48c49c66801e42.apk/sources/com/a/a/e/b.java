package com.a.a.e;

import android.util.Log;
import java.util.HashMap;

public final class b {
    private HashMap<String, String> hM = new HashMap<>();

    public b(String str) {
        if (str != null && str.length() > 0) {
            String[] split = str.split(",");
            for (int i = 0; i < split.length; i++) {
                int indexOf = split[i].indexOf(":");
                if (indexOf != -1) {
                    String trim = split[i].substring(0, indexOf).trim();
                    String substring = split[i].substring(indexOf + 1);
                    this.hM.put(trim, substring);
                    trim + ":" + substring;
                } else {
                    Log.w("Configuration", "Unkown args for configuration." + split[i]);
                }
            }
        }
    }

    public final String C(String str) {
        return this.hM.get(str);
    }
}
