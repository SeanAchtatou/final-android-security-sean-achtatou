package com.a.a.a;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

public final class a extends j {
    public static final int FOREVER = -2;
    public static final c bU = new c("", 4, 0);
    protected static Context context = null;
    private int bV;
    private LinearLayout bW;

    public static int k() {
        return 2000;
    }

    public final int getTimeout() {
        return this.bV;
    }

    public final View getView() {
        return this.bW;
    }

    public final int l() {
        return 5;
    }
}
