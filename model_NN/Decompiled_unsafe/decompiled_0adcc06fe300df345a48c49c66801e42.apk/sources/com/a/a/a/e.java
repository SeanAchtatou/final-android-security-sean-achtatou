package com.a.a.a;

import android.os.Message;
import android.util.Log;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.core.a;
import org.meteoroid.core.c;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.plugin.device.MIDPDevice;

public final class e implements h.a {
    public static final int ALERT = 3;
    public static final int CHOICE_GROUP_ELEMENT = 2;
    public static final int COLOR_BACKGROUND = 0;
    public static final int COLOR_BORDER = 4;
    public static final int COLOR_FOREGROUND = 1;
    public static final int COLOR_HIGHLIGHTED_BACKGROUND = 2;
    public static final int COLOR_HIGHLIGHTED_BORDER = 5;
    public static final int COLOR_HIGHLIGHTED_FOREGROUND = 3;
    public static final int LIST_ELEMENT = 1;
    public static final String LOG_TAG = "Display";
    private static f bZ = null;
    public static final e ca = new e();
    private static MIDlet cb;
    private static volatile int cf;
    private Message cc = h.a(MIDPDevice.MSG_MIDP_DISPLAY_CALL_SERIALLY, null, 0, h.MSG_ARG2_DONT_RECYCLE_ME);
    private boolean cd;
    private volatile boolean ce = true;

    private e() {
        h.a(this);
    }

    public static e a(MIDlet mIDlet) {
        if (mIDlet != null) {
            cb = mIDlet;
        }
        return ca;
    }

    private final void a(b bVar) {
        this.ce = false;
        try {
            bVar.c(((MIDPDevice) c.ds).aL());
        } catch (Exception e) {
            Log.w(LOG_TAG, "Exception in paint!" + e);
            e.printStackTrace();
        }
        ((MIDPDevice) c.ds).aM();
        this.ce = true;
    }

    public static void a(c cVar) {
        if (bZ != null) {
            bZ.u();
            f fVar = bZ;
        }
    }

    public static void e(int i, int i2) {
        if (bZ != null) {
            f fVar = bZ;
        }
    }

    private static boolean s() {
        return bZ != null && (bZ.l() == 0 || bZ.l() == 1);
    }

    public final void a(f fVar) {
        if (fVar != bZ && fVar != null) {
            if (fVar.l() != 5) {
                if (bZ != null && bZ.l() == 5) {
                    l.aA();
                }
                if (bZ != null && bZ.isShown()) {
                    bZ.cg = null;
                }
                if (bZ == null || !((bZ.l() == 0 || bZ.l() == 1) && (fVar.l() == 0 || fVar.l() == 1))) {
                    l.a(fVar);
                    return;
                }
                bZ.v();
                bZ.cg = ca;
                l.b(fVar);
                bZ = fVar;
                fVar.n();
                h.b((int) l.MSG_VIEW_CHANGED, fVar);
                return;
            }
            a aVar = (a) fVar;
            int timeout = aVar.getTimeout();
            l.a(aVar.getTitle(), null, aVar.getView(), false, null);
            if (timeout != -2) {
                if (timeout <= 0) {
                    timeout = a.k();
                }
                l.a((long) timeout);
            }
        }
    }

    public final boolean a(Message message) {
        if ((message.what == 44287 || message.what == 40965) && s()) {
            if (this.ce) {
                a((b) bZ);
            } else {
                if (cf > 0) {
                    cf--;
                }
                "Still not ready for repaint. Try later for " + cf;
            }
        } else if (message.what == 23041) {
            if (message.obj != null && (message.obj instanceof f)) {
                f fVar = (f) message.obj;
                bZ = fVar;
                fVar.cg = this;
                this.cd = true;
                if (bZ.l() == 0) {
                    b((b) bZ);
                }
            }
        } else if (message.what == 44035) {
            if (message.obj != null) {
                ((Runnable) message.obj).run();
            }
            return true;
        }
        return false;
    }

    public final void b(int i) {
        if (this.cd) {
            this.cd = false;
        }
        if (bZ != null && a.U()) {
            if (!bZ.t().isEmpty()) {
                com.a.a.d.a aVar = c.ds;
                if (i == MIDPDevice.t(1)) {
                    a(bZ.t().get(0));
                } else if (bZ.t().size() > 1) {
                    com.a.a.d.a aVar2 = c.ds;
                    if (i == MIDPDevice.t(2)) {
                        a(bZ.t().get(1));
                    }
                }
            }
            if (bZ.l() == 1) {
                ((com.a.a.b.a) bZ).f(0, i);
            }
            bZ.b(i);
        }
    }

    public final synchronized void b(int i, int i2) {
        if (this.cd) {
            this.cd = false;
        }
        if (bZ != null && a.U()) {
            try {
                f fVar = bZ;
            } catch (Exception e) {
            }
        }
    }

    public final void b(b bVar) {
        if (s() && bVar == bZ) {
            if (this.ce) {
                cf = 0;
                a(bVar);
            } else if (cf <= 1) {
                h.o(com.a.a.d.a.MSG_DEVICE_REQUEST_REFRESH);
                cf++;
            }
        }
    }

    public final void c(int i) {
        if (!this.cd && bZ != null && a.U()) {
            if (bZ.l() == 1) {
                ((com.a.a.b.a) bZ).f(1, i);
            }
            f fVar = bZ;
        }
    }

    public final synchronized void c(int i, int i2) {
        if (!this.cd) {
            if (bZ != null && a.U()) {
                try {
                    f fVar = bZ;
                } catch (Exception e) {
                }
            }
        }
    }

    public final void d(int i) {
        if (!this.cd && bZ != null && a.U()) {
            f fVar = bZ;
        }
    }

    public final synchronized void d(int i, int i2) {
        if (!this.cd) {
            if (bZ != null && a.U()) {
                try {
                    f fVar = bZ;
                } catch (Exception e) {
                }
            }
        }
    }
}
