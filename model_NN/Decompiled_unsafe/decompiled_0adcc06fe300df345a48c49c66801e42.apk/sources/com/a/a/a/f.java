package com.a.a.a;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import org.meteoroid.core.i;
import org.meteoroid.core.l;

public abstract class f implements l.a {
    public static final int ALERT = 5;
    public static final int CANVAS = 0;
    public static final int FORM = 2;
    public static final int GAMECANVAS = 1;
    public static final int LIST = 3;
    public static final int TEXTBOX = 4;
    protected e cg = null;
    public boolean ch = false;
    private ArrayList<c> ci = new ArrayList<>();
    private d cj = null;
    private String ck;
    private boolean cl;
    private int height = -1;
    private int width = -1;

    f() {
    }

    /* access modifiers changed from: protected */
    public void b(int i) {
    }

    public final String getTitle() {
        return this.ck;
    }

    /* access modifiers changed from: protected */
    public void i() {
    }

    public final boolean isShown() {
        return this.cl;
    }

    public abstract int l();

    public void n() {
        Iterator<c> it = this.ci.iterator();
        while (it.hasNext()) {
            i.a(it.next());
        }
        if (!this.cl) {
            try {
                getClass().getSimpleName() + " shownotify called.";
            } catch (Exception e) {
                Log.w("Displayable", e);
            }
            this.cl = true;
        }
    }

    public boolean o() {
        return false;
    }

    public final ArrayList<c> t() {
        return this.ci;
    }

    public final d u() {
        return this.cj;
    }

    public void v() {
        Iterator<c> it = this.ci.iterator();
        while (it.hasNext()) {
            i.b(it.next());
        }
        if (this.cl) {
            try {
                i();
                getClass().getSimpleName() + " hidenotify called.";
            } catch (Exception e) {
                Log.w("Displayable", e);
            }
            this.cl = false;
        }
    }
}
