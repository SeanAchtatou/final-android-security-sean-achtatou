package com.a.a.a;

import java.util.Hashtable;

public final class g {
    public static final int FACE_MONOSPACE = 32;
    public static final int FACE_PROPORTIONAL = 64;
    public static final int FACE_SYSTEM = 0;
    public static final int FONT_INPUT_TEXT = 1;
    public static final int FONT_STATIC_TEXT = 0;
    public static final int SIZE_LARGE = 16;
    public static final int SIZE_MEDIUM = 0;
    public static final int SIZE_SMALL = 8;
    public static final int STYLE_BOLD = 1;
    public static final int STYLE_ITALIC = 2;
    public static final int STYLE_PLAIN = 0;
    public static final int STYLE_UNDERLINED = 4;
    private static final g cm = new g(0, 0, 0);
    private static g[] cn = {cm, cm};
    private static final Hashtable<Integer, g> co = new Hashtable<>();
    private int cp;
    private int cq;
    private int cr;

    private g(int i, int i2, int i3) {
        this.cp = i;
        this.cq = i2;
        this.cr = i3;
    }

    public static g a(int i, int i2, int i3) {
        Integer num = new Integer(8);
        g gVar = co.get(num);
        if (gVar != null) {
            return gVar;
        }
        g gVar2 = new g(0, 0, 8);
        co.put(num, gVar2);
        return gVar2;
    }

    public static g w() {
        return cm;
    }

    public final int getSize() {
        return this.cr;
    }

    public final int getStyle() {
        return this.cq;
    }

    public final int hashCode() {
        return this.cq + this.cr + this.cp;
    }

    public final int x() {
        return this.cp;
    }
}
