package com.a.a.a;

import android.view.View;
import android.widget.Button;
import org.meteoroid.core.h;
import org.meteoroid.core.i;
import org.meteoroid.core.k;
import org.meteoroid.plugin.device.MIDPDevice;

public final class c implements View.OnClickListener, i.c {
    public static final int BACK = 2;
    public static final int CANCEL = 3;
    public static final int EXIT = 7;
    public static final int HELP = 5;
    public static final int ITEM = 8;
    public static final int OK = 4;
    public static final int SCREEN = 1;
    public static final int STOP = 6;
    private int bX = 4;
    private Button bY = new Button(k.getActivity());
    private String label;
    private int priority = 0;

    public c(String str, int i, int i2) {
        this.label = str;
        this.bY.setText(str);
        this.bY.setOnClickListener(this);
    }

    public final int getId() {
        return this.priority;
    }

    public final void onClick(View view) {
        h.b(h.a(MIDPDevice.MSG_MIDP_COMMAND_EVENT, this));
    }

    public final String p() {
        return this.label;
    }

    public final Button q() {
        return this.bY;
    }

    public final void r() {
        h.b(h.a(MIDPDevice.MSG_MIDP_COMMAND_EVENT, this));
    }
}
