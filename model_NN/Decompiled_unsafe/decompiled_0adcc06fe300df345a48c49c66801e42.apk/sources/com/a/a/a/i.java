package com.a.a.a;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import java.io.IOException;
import javax.microedition.enhance.MIDPHelper;
import org.meteoroid.core.e;

public class i {
    private static final Matrix cx = new Matrix();
    public Bitmap cv;
    public boolean cw;
    public int height;
    public int width;

    private i(Bitmap bitmap) {
        this(bitmap, false);
    }

    private i(Bitmap bitmap, boolean z) {
        this.cv = bitmap;
        if (bitmap != null) {
            this.width = bitmap.getWidth();
            this.height = bitmap.getHeight();
        }
        this.cw = false;
    }

    public static i k(String str) {
        Bitmap b = e.b(MIDPHelper.j(str));
        if (b == null) {
            throw new IOException();
        }
        i iVar = new i(b);
        iVar.cw = false;
        return iVar;
    }

    public final int getHeight() {
        return this.height;
    }

    public final int getWidth() {
        return this.width;
    }
}
