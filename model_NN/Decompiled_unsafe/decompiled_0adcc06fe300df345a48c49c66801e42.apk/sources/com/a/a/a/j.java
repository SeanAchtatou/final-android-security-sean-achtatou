package com.a.a.a;

import android.view.ViewGroup;
import java.util.Iterator;
import org.meteoroid.core.k;

public abstract class j extends f {
    public final void n() {
        super.n();
        k.getHandler().post(new Runnable() {
            public final void run() {
                Iterator<c> it = j.this.t().iterator();
                while (it.hasNext()) {
                    c next = it.next();
                    "Add command " + next.p();
                    ((ViewGroup) j.this.getView()).addView(next.q());
                }
            }
        });
    }

    public final boolean o() {
        k.getActivity().openOptionsMenu();
        return true;
    }

    public final void v() {
        super.v();
        k.getHandler().post(new Runnable() {
            public final void run() {
                Iterator<c> it = j.this.t().iterator();
                while (it.hasNext()) {
                    c next = it.next();
                    "Remove command " + next.p();
                    ((ViewGroup) j.this.getView()).removeView(next.q());
                }
            }
        });
    }
}
