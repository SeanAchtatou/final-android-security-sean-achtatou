package com.umeng.update;

public class UpdateStatus {
    public static final int No = 1;
    public static final int NoneWifi = 2;
    public static final int Timeout = 3;
    public static final int Yes = 0;
}
