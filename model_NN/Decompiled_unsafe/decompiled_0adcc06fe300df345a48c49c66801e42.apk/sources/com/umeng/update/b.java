package com.umeng.update;

import android.os.Handler;
import android.os.Message;

class b extends Handler {
    b() {
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        if (UmengUpdateAgent.f != null) {
            UmengUpdateAgent.f.onUpdateReturned(message.what, (UpdateResponse) message.obj);
        }
    }
}
