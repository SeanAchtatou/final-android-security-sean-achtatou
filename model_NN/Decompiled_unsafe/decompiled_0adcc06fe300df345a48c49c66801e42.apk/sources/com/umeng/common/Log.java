package com.umeng.common;

public class Log {
    public static boolean LOG = false;

    public static void a(String str, String str2) {
        boolean z = LOG;
    }

    public static void a(String str, String str2, Exception exc) {
        if (LOG) {
            String.valueOf(exc.toString()) + ":  [" + str2 + "]";
        }
    }

    public static void b(String str, String str2) {
        if (LOG) {
            android.util.Log.e(str, str2);
        }
    }

    public static void b(String str, String str2, Exception exc) {
        if (LOG) {
            android.util.Log.e(str, String.valueOf(exc.toString()) + ":  [" + str2 + "]");
            StackTraceElement[] stackTrace = exc.getStackTrace();
            int length = stackTrace.length;
            for (int i = 0; i < length; i++) {
                android.util.Log.e(str, "        at\t " + stackTrace[i].toString());
            }
        }
    }

    public static void c(String str, String str2) {
        boolean z = LOG;
    }

    public static void c(String str, String str2, Exception exc) {
        if (LOG) {
            String.valueOf(exc.toString()) + ":  [" + str2 + "]";
        }
    }

    public static void d(String str, String str2) {
        boolean z = LOG;
    }

    public static void d(String str, String str2, Exception exc) {
        if (LOG) {
            String.valueOf(exc.toString()) + ":  [" + str2 + "]";
        }
    }

    public static void e(String str, String str2) {
        if (LOG) {
            android.util.Log.w(str, str2);
        }
    }

    public static void e(String str, String str2, Exception exc) {
        if (LOG) {
            android.util.Log.w(str, String.valueOf(exc.toString()) + ":  [" + str2 + "]");
            StackTraceElement[] stackTrace = exc.getStackTrace();
            int length = stackTrace.length;
            for (int i = 0; i < length; i++) {
                android.util.Log.w(str, "        at\t " + stackTrace[i].toString());
            }
        }
    }
}
