package com.umeng.common;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.a.a.b.a;
import com.nokia.mid.ui.DirectGraphics;
import com.umeng.common.b.g;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.microedition.khronos.opengles.GL10;

public class b {
    protected static final String a = b.class.getName();
    protected static final String b = "Unknown";
    public static final int c = 8;
    private static final String d = "2G/3G";
    private static final String e = "Wi-Fi";

    private static int a(Object obj, String str) {
        try {
            Field declaredField = DisplayMetrics.class.getDeclaredField(str);
            declaredField.setAccessible(true);
            return declaredField.getInt(obj);
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    public static int a(Date date, Date date2) {
        if (!date.after(date2)) {
            Date date3 = date2;
            date2 = date;
            date = date3;
        }
        return (int) ((date.getTime() - date2.getTime()) / 1000);
    }

    public static String a() {
        String str;
        String str2 = null;
        try {
            FileReader fileReader = new FileReader("/proc/cpuinfo");
            try {
                BufferedReader bufferedReader = new BufferedReader(fileReader, a.GAME_B_PRESSED);
                str2 = bufferedReader.readLine();
                bufferedReader.close();
                fileReader.close();
                str = str2;
            } catch (IOException e2) {
                Log.e(a, "Could not read from file /proc/cpuinfo", e2);
                str = str2;
            }
        } catch (FileNotFoundException e3) {
            Log.e(a, "Could not open file /proc/cpuinfo", e3);
            str = str2;
        }
        if (str != null) {
            str = str.substring(str.indexOf(58) + 1);
        }
        return str.trim();
    }

    public static String a(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    public static Date a(String str) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
        } catch (Exception e2) {
            return null;
        }
    }

    public static boolean a(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals(Locale.CHINA.toString());
    }

    public static boolean a(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }

    public static boolean a(String str, Context context) {
        try {
            context.getPackageManager().getPackageInfo(str, 1);
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        }
    }

    public static String[] a(GL10 gl10) {
        try {
            return new String[]{gl10.glGetString(7936), gl10.glGetString(7937)};
        } catch (Exception e2) {
            Log.e(a, "Could not read gpu infor:", e2);
            return new String[0];
        }
    }

    public static Set<String> b(Context context) {
        int i = 0;
        HashSet hashSet = new HashSet();
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        while (true) {
            int i2 = i;
            if (i2 >= installedPackages.size()) {
                return hashSet;
            }
            hashSet.add(installedPackages.get(i2).packageName);
            i = i2 + 1;
        }
    }

    public static boolean b() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static String c() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static boolean c(Context context) {
        return context.getResources().getConfiguration().orientation == 1;
    }

    public static String d(Context context) {
        try {
            return String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode);
        } catch (PackageManager.NameNotFoundException e2) {
            return b;
        }
    }

    public static String e(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            return b;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String f(android.content.Context r4) {
        /*
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r4.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            if (r0 != 0) goto L_0x0011
            java.lang.String r1 = com.umeng.common.b.a
            java.lang.String r2 = "No IMEI."
            android.util.Log.w(r1, r2)
        L_0x0011:
            java.lang.String r1 = ""
            java.lang.String r2 = "android.permission.READ_PHONE_STATE"
            boolean r2 = a(r4, r2)     // Catch:{ Exception -> 0x005c }
            if (r2 == 0) goto L_0x0064
            java.lang.String r0 = r0.getDeviceId()     // Catch:{ Exception -> 0x005c }
        L_0x001f:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x005b
            java.lang.String r0 = com.umeng.common.b.a
            java.lang.String r1 = "No IMEI."
            android.util.Log.w(r0, r1)
            java.lang.String r0 = q(r4)
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x005b
            java.lang.String r0 = com.umeng.common.b.a
            java.lang.String r1 = "Failed to take mac as IMEI. Try to use Secure.ANDROID_ID instead."
            android.util.Log.w(r0, r1)
            android.content.ContentResolver r0 = r4.getContentResolver()
            java.lang.String r1 = "android_id"
            java.lang.String r0 = android.provider.Settings.Secure.getString(r0, r1)
            java.lang.String r1 = com.umeng.common.b.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "getDeviceId: Secure.ANDROID_ID: "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            android.util.Log.w(r1, r2)
        L_0x005b:
            return r0
        L_0x005c:
            r0 = move-exception
            java.lang.String r2 = com.umeng.common.b.a
            java.lang.String r3 = "No IMEI."
            android.util.Log.w(r2, r3, r0)
        L_0x0064:
            r0 = r1
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.common.b.f(android.content.Context):java.lang.String");
    }

    public static String g(Context context) {
        return g.b(f(context));
    }

    public static String h(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            return telephonyManager == null ? b : telephonyManager.getNetworkOperatorName();
        } catch (Exception e2) {
            e2.printStackTrace();
            return b;
        }
    }

    public static String i(Context context) {
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            return String.valueOf(String.valueOf(displayMetrics.heightPixels)) + "*" + String.valueOf(displayMetrics.widthPixels);
        } catch (Exception e2) {
            e2.printStackTrace();
            return b;
        }
    }

    public static String[] j(Context context) {
        String[] strArr = {b, b};
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
            strArr[0] = b;
            return strArr;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            strArr[0] = b;
            return strArr;
        } else if (connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            strArr[0] = e;
            return strArr;
        } else {
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
            if (networkInfo.getState() != NetworkInfo.State.CONNECTED) {
                return strArr;
            }
            strArr[0] = d;
            strArr[1] = networkInfo.getSubtypeName();
            return strArr;
        }
    }

    public static boolean k(Context context) {
        return e.equals(j(context)[0]);
    }

    public static Location l(Context context) {
        Location lastKnownLocation;
        Location lastKnownLocation2;
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            if (a(context, "android.permission.ACCESS_FINE_LOCATION") && (lastKnownLocation2 = locationManager.getLastKnownLocation("gps")) != null) {
                String str = a;
                "get location from gps:" + lastKnownLocation2.getLatitude() + "," + lastKnownLocation2.getLongitude();
                return lastKnownLocation2;
            } else if (!a(context, "android.permission.ACCESS_COARSE_LOCATION") || (lastKnownLocation = locationManager.getLastKnownLocation("network")) == null) {
                String str2 = a;
                return null;
            } else {
                String str3 = a;
                "get location from network:" + lastKnownLocation.getLatitude() + "," + lastKnownLocation.getLongitude();
                return lastKnownLocation;
            }
        } catch (Exception e2) {
            Log.e(a, e2.getMessage());
            return null;
        }
    }

    public static boolean m(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                return activeNetworkInfo.isConnectedOrConnecting();
            }
            return false;
        } catch (Exception e2) {
            return true;
        }
    }

    public static int n(Context context) {
        try {
            Calendar instance = Calendar.getInstance(w(context));
            if (instance != null) {
                return instance.getTimeZone().getRawOffset() / 3600000;
            }
        } catch (Exception e2) {
            String str = a;
        }
        return 8;
    }

    public static String[] o(Context context) {
        String[] strArr = new String[2];
        try {
            Locale w = w(context);
            if (w != null) {
                strArr[0] = w.getCountry();
                strArr[1] = w.getLanguage();
            }
            if (TextUtils.isEmpty(strArr[0])) {
                strArr[0] = b;
            }
            if (TextUtils.isEmpty(strArr[1])) {
                strArr[1] = b;
            }
        } catch (Exception e2) {
            Log.e(a, "error in getLocaleInfo", e2);
        }
        return strArr;
    }

    public static String p(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                String string = applicationInfo.metaData.getString("UMENG_APPKEY");
                if (string != null) {
                    return string.trim();
                }
                Log.e(a, "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.");
            }
        } catch (Exception e2) {
            Log.e(a, "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.", e2);
        }
        return null;
    }

    public static String q(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
            if (a(context, "android.permission.ACCESS_WIFI_STATE")) {
                return wifiManager.getConnectionInfo().getMacAddress();
            }
            Log.w(a, "Could not get mac address.[no permission android.permission.ACCESS_WIFI_STATE");
            return "";
        } catch (Exception e2) {
            Log.w(a, "Could not get mac address." + e2.toString());
        }
    }

    public static String r(Context context) {
        int i;
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            int i2 = 0;
            if ((context.getApplicationInfo().flags & DirectGraphics.FLIP_HORIZONTAL) == 0) {
                i = a(displayMetrics, "noncompatWidthPixels");
                i2 = a(displayMetrics, "noncompatHeightPixels");
            } else {
                i = -1;
            }
            if (i == -1 || i2 == -1) {
                i = displayMetrics.widthPixels;
                i2 = displayMetrics.heightPixels;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(i);
            stringBuffer.append("*");
            stringBuffer.append(i2);
            return stringBuffer.toString();
        } catch (Exception e2) {
            Log.e(a, "read resolution fail", e2);
            return b;
        }
    }

    public static String s(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
        } catch (Exception e2) {
            String str = a;
            return b;
        }
    }

    public static String t(Context context) {
        Object obj;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (!(applicationInfo == null || applicationInfo.metaData == null || (obj = applicationInfo.metaData.get("UMENG_CHANNEL")) == null)) {
                String obj2 = obj.toString();
                if (obj2 != null) {
                    return obj2;
                }
                String str = a;
                return b;
            }
        } catch (Exception e2) {
            String str2 = a;
            e2.printStackTrace();
        }
        return b;
    }

    public static String u(Context context) {
        return context.getPackageName();
    }

    public static String v(Context context) {
        return context.getPackageManager().getApplicationLabel(context.getApplicationInfo()).toString();
    }

    private static Locale w(Context context) {
        Locale locale = null;
        try {
            Configuration configuration = new Configuration();
            Settings.System.getConfiguration(context.getContentResolver(), configuration);
            locale = configuration.locale;
        } catch (Exception e2) {
            Log.e(a, "fail to read user config locale");
        }
        return locale == null ? Locale.getDefault() : locale;
    }
}
