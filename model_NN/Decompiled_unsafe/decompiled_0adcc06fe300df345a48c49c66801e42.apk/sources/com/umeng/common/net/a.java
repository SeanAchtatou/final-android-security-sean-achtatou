package com.umeng.common.net;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import com.umeng.common.Log;

public class a {
    /* access modifiers changed from: private */
    public static final String b = a.class.getName();
    final Messenger a = new Messenger(new b());
    /* access modifiers changed from: private */
    public Context c;
    /* access modifiers changed from: private */
    public k d;
    /* access modifiers changed from: private */
    public Messenger e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String[] i;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public ServiceConnection k = new b(this);

    /* renamed from: com.umeng.common.net.a$a  reason: collision with other inner class name */
    static class C0002a {
        public String a;
        public String b;
        public String c;
        public String[] d = null;
        public boolean e = false;

        public C0002a(String str, String str2, String str3) {
            this.a = str;
            this.b = str2;
            this.c = str3;
        }

        public static C0002a a(Bundle bundle) {
            C0002a aVar = new C0002a(bundle.getString("mComponentName"), bundle.getString("mTitle"), bundle.getString("mUrl"));
            aVar.d = bundle.getStringArray("reporturls");
            aVar.e = bundle.getBoolean("rich_notification");
            return aVar;
        }

        public Bundle a() {
            Bundle bundle = new Bundle();
            bundle.putString("mComponentName", this.a);
            bundle.putString("mTitle", this.b);
            bundle.putString("mUrl", this.c);
            bundle.putStringArray("reporturls", this.d);
            bundle.putBoolean("rich_notification", this.e);
            return bundle;
        }
    }

    class b extends Handler {
        b() {
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public void handleMessage(Message message) {
            try {
                Log.c(a.b, "DownloadAgent.handleMessage(" + message.what + "): ");
                switch (message.what) {
                    case 3:
                        if (a.this.d != null) {
                            a.this.d.a(message.arg1);
                            return;
                        }
                        return;
                    case 4:
                    default:
                        super.handleMessage(message);
                        return;
                    case 5:
                        a.this.c.unbindService(a.this.k);
                        if (a.this.d == null) {
                            return;
                        }
                        if (message.arg1 == 1) {
                            a.this.d.a(message.arg1, message.getData().getString("filename"));
                            return;
                        }
                        a.this.d.a(0, null);
                        Log.c(a.b, "DownloadAgent.handleMessage(DownloadingService.DOWNLOAD_COMPLETE_FAIL): ");
                        return;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.c(a.b, "DownloadAgent.handleMessage(" + message.what + "): " + e.getMessage());
            }
            e.printStackTrace();
            Log.c(a.b, "DownloadAgent.handleMessage(" + message.what + "): " + e.getMessage());
        }
    }

    public a(Context context, String str, String str2, String str3, k kVar) {
        this.c = context.getApplicationContext();
        this.f = str;
        this.g = str2;
        this.h = str3;
        this.d = kVar;
    }

    public void a() {
        this.c.bindService(new Intent(this.c, DownloadingService.class), this.k, 1);
    }

    public void a(boolean z) {
        this.j = z;
    }

    public void a(String[] strArr) {
        this.i = strArr;
    }
}
