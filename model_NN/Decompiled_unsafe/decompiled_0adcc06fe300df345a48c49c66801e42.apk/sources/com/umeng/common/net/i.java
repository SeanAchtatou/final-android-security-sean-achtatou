package com.umeng.common.net;

import com.umeng.common.b.g;
import java.io.IOException;
import java.util.Map;
import java.util.Random;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

class i implements Runnable {
    private final /* synthetic */ String[] a;
    private final /* synthetic */ boolean b;
    private final /* synthetic */ Map c;

    i(String[] strArr, boolean z, Map map) {
        this.a = strArr;
        this.b = z;
        this.c = map;
    }

    public void run() {
        int nextInt = new Random().nextInt(1000);
        if (this.a == null) {
            String unused = DownloadingService.j;
            String.valueOf(nextInt) + "service report: urls is null";
            return;
        }
        String[] strArr = this.a;
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            String str = strArr[i];
            String a2 = g.a();
            String str2 = a2.split(" ")[0];
            String str3 = a2.split(" ")[1];
            long currentTimeMillis = System.currentTimeMillis();
            StringBuilder sb = new StringBuilder(str);
            sb.append("&data=" + str2);
            sb.append("&time=" + str3);
            sb.append("&ts=" + currentTimeMillis);
            if (this.b) {
                sb.append(new StringBuilder("&action_type=1").toString());
            } else {
                sb.append(new StringBuilder("&action_type=-2").toString());
            }
            if (this.c != null) {
                for (String str4 : this.c.keySet()) {
                    sb.append("&" + str4 + "=" + ((String) this.c.get(str4)));
                }
            }
            try {
                String unused2 = DownloadingService.j;
                String.valueOf(nextInt) + ": service report:\tget: " + sb.toString();
                HttpGet httpGet = new HttpGet(sb.toString());
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
                HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
                HttpResponse execute = new DefaultHttpClient(basicHttpParams).execute(httpGet);
                String unused3 = DownloadingService.j;
                String.valueOf(nextInt) + ": service report:status code:  " + execute.getStatusLine().getStatusCode();
                if (execute.getStatusLine().getStatusCode() != 200) {
                    i++;
                } else {
                    return;
                }
            } catch (ClientProtocolException e) {
                String unused4 = DownloadingService.j;
                String.valueOf(nextInt) + ": service report:\tClientProtocolException,Failed to send message." + str;
            } catch (IOException e2) {
                String unused5 = DownloadingService.j;
                String.valueOf(nextInt) + ": service report:\tIOException,Failed to send message." + str;
            }
        }
    }
}
