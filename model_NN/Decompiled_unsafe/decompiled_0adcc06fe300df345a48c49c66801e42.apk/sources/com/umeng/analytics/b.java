package com.umeng.analytics;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.nokia.mid.ui.DirectGraphics;
import com.umeng.common.Log;
import com.umeng.common.b.e;
import com.umeng.common.b.f;
import com.umeng.common.b.g;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.microedition.media.PlayerListener;
import javax.microedition.media.control.MetaDataControl;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class b implements g {
    private final String A = com.umeng.common.a.g;
    private final String B = "body";
    private final String C = "session_id";
    private final String D = MetaDataControl.DATE_KEY;
    private final String E = "time";
    private final String F = "start_millis";
    private final String G = "end_millis";
    private final String H = "duration";
    private final String I = "activities";
    private final String J = "header";
    private final String K = "uptr";
    private final String L = "dntr";
    private final String M = "acc";
    private final String N = "tag";
    private final String O = "label";
    private final String P = "id";
    private final String Q = "ts";
    private final String R = "du";
    private final String S = "context";
    private final String T = "last_config_time";
    private final String U = "report_policy";
    private final String V = "online_params";
    private final String W = "report_interval";
    String a = null;
    String b = null;
    UmengOnlineConfigureListener c = null;
    String d = "";
    String e = "";
    private final a f = new a();
    /* access modifiers changed from: private */
    public final d g = new d();
    private final ReportPolicy h = new ReportPolicy();
    private String i;
    private final Handler j;
    private final int k = 0;
    private final int l = 1;
    private final int m = 2;
    private final int n = 3;
    private final int o = 4;
    private final int p = 5;
    private final int q = 6;
    private final String r = com.umeng.common.a.b;
    private final String s = PlayerListener.ERROR;
    private final String t = "event";
    private final String u = "ekv";
    private final String v = "launch";
    private final String w = "flush";
    private final String x = "terminate";
    private final String y = "online_config";
    private final String z = "cmd_cache_buffer";

    private final class a extends Thread {
        private final Object b = new Object();
        private Context c;
        private int d;
        private String e;
        private String f;
        private int g;
        private long h;
        private Map<String, String> i;
        private String j;

        a(Context context, int i2) {
            this.c = context.getApplicationContext();
            this.d = i2;
        }

        a(Context context, String str, String str2, long j2, int i2, int i3) {
            this.c = context.getApplicationContext();
            this.e = str;
            this.f = str2;
            this.g = i2;
            this.d = i3;
            this.h = j2;
        }

        a(Context context, String str, Map<String, String> map, long j2, int i2) {
            this.c = context.getApplicationContext();
            this.e = str;
            this.i = map;
            this.d = i2;
            this.h = j2;
        }

        a(Context context, String str, Map<String, String> map, String str2, int i2) {
            this.c = context.getApplicationContext();
            this.e = str;
            this.i = map;
            this.j = str2;
            this.d = i2;
        }

        public final void run() {
            try {
                synchronized (this.b) {
                    if (this.d == 0) {
                        try {
                            if (this.c == null) {
                                Log.b("MobclickAgent", "unexpected null context in invokehander flag=0");
                                return;
                            }
                            b.this.j(this.c);
                        } catch (Exception e2) {
                            Log.b("MobclickAgent", "unexpected null context in invokehander flag=0", e2);
                        }
                    } else if (this.d == 1) {
                        b.this.g(this.c);
                    } else if (this.d == 2) {
                        b.this.m(this.c);
                    } else if (this.d == 3) {
                        b.this.b(this.c, this.e, this.f, this.h, this.g);
                    } else if (this.d == 4) {
                        b.this.b(this.c, this.e, this.i, this.h);
                    } else if (this.d == 5) {
                        b.this.b(this.c, this.e, this.i, this.j);
                    } else if (this.d == 6) {
                        b.this.d(this.c, this.e, this.j);
                    }
                }
            } catch (Exception e3) {
                Log.b("MobclickAgent", "Exception occurred in invokehander.", e3);
            }
        }
    }

    /* renamed from: com.umeng.analytics.b$b  reason: collision with other inner class name */
    private final class C0001b implements Runnable {
        private final Object b = new Object();
        private Context c;
        private JSONObject d;

        C0001b(b bVar, Context context, JSONObject jSONObject) {
            this.c = context.getApplicationContext();
            this.d = jSONObject;
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r4 = this;
                org.json.JSONObject r0 = r4.d     // Catch:{ Exception -> 0x003d }
                java.lang.String r1 = "type"
                java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x003d }
                java.lang.String r1 = "online_config"
                boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x003d }
                if (r0 == 0) goto L_0x001a
                com.umeng.analytics.b r0 = com.umeng.analytics.b.this     // Catch:{ Exception -> 0x003d }
                android.content.Context r1 = r4.c     // Catch:{ Exception -> 0x003d }
                org.json.JSONObject r2 = r4.d     // Catch:{ Exception -> 0x003d }
                r0.b(r1, r2)     // Catch:{ Exception -> 0x003d }
            L_0x0019:
                return
            L_0x001a:
                org.json.JSONObject r0 = r4.d     // Catch:{ Exception -> 0x003d }
                java.lang.String r1 = "type"
                java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x003d }
                java.lang.String r1 = "cmd_cache_buffer"
                boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x003d }
                if (r0 == 0) goto L_0x0049
                java.lang.Object r1 = r4.b     // Catch:{ Exception -> 0x003d }
                monitor-enter(r1)     // Catch:{ Exception -> 0x003d }
                com.umeng.analytics.b r0 = com.umeng.analytics.b.this     // Catch:{ all -> 0x003a }
                com.umeng.analytics.d r0 = r0.g     // Catch:{ all -> 0x003a }
                android.content.Context r2 = r4.c     // Catch:{ all -> 0x003a }
                r0.a(r2)     // Catch:{ all -> 0x003a }
                monitor-exit(r1)     // Catch:{ all -> 0x003a }
                goto L_0x0019
            L_0x003a:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ Exception -> 0x003d }
                throw r0     // Catch:{ Exception -> 0x003d }
            L_0x003d:
                r0 = move-exception
                java.lang.String r1 = "MobclickAgent"
                java.lang.String r2 = "Exception occurred in ReportMessageHandler"
                com.umeng.common.Log.b(r1, r2)
                r0.printStackTrace()
                goto L_0x0019
            L_0x0049:
                org.json.JSONObject r0 = r4.d     // Catch:{ Exception -> 0x003d }
                java.lang.String r1 = "type"
                java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x003d }
                java.lang.String r1 = "flush"
                boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x003d }
                if (r0 == 0) goto L_0x0075
                java.lang.Object r1 = r4.b     // Catch:{ Exception -> 0x003d }
                monitor-enter(r1)     // Catch:{ Exception -> 0x003d }
                com.umeng.analytics.b r0 = com.umeng.analytics.b.this     // Catch:{ all -> 0x0072 }
                com.umeng.analytics.d r0 = r0.g     // Catch:{ all -> 0x0072 }
                android.content.Context r2 = r4.c     // Catch:{ all -> 0x0072 }
                r0.a(r2)     // Catch:{ all -> 0x0072 }
                com.umeng.analytics.b r0 = com.umeng.analytics.b.this     // Catch:{ all -> 0x0072 }
                android.content.Context r2 = r4.c     // Catch:{ all -> 0x0072 }
                org.json.JSONObject r3 = r4.d     // Catch:{ all -> 0x0072 }
                r0.a(r2, r3)     // Catch:{ all -> 0x0072 }
                monitor-exit(r1)     // Catch:{ all -> 0x0072 }
                goto L_0x0019
            L_0x0072:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ Exception -> 0x003d }
                throw r0     // Catch:{ Exception -> 0x003d }
            L_0x0075:
                java.lang.Object r1 = r4.b     // Catch:{ Exception -> 0x003d }
                monitor-enter(r1)     // Catch:{ Exception -> 0x003d }
                com.umeng.analytics.b r0 = com.umeng.analytics.b.this     // Catch:{ all -> 0x008e }
                com.umeng.analytics.d r0 = r0.g     // Catch:{ all -> 0x008e }
                android.content.Context r2 = r4.c     // Catch:{ all -> 0x008e }
                r0.a(r2)     // Catch:{ all -> 0x008e }
                com.umeng.analytics.b r0 = com.umeng.analytics.b.this     // Catch:{ all -> 0x008e }
                android.content.Context r2 = r4.c     // Catch:{ all -> 0x008e }
                org.json.JSONObject r3 = r4.d     // Catch:{ all -> 0x008e }
                r0.a(r2, r3)     // Catch:{ all -> 0x008e }
                monitor-exit(r1)     // Catch:{ all -> 0x008e }
                goto L_0x0019
            L_0x008e:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ Exception -> 0x003d }
                throw r0     // Catch:{ Exception -> 0x003d }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.analytics.b.C0001b.run():void");
        }
    }

    b() {
        HandlerThread handlerThread = new HandlerThread("MobclickAgent");
        handlerThread.start();
        this.j = new Handler(handlerThread.getLooper());
    }

    private String a(Context context, String str, long j2) {
        StringBuilder sb = new StringBuilder();
        sb.append(j2).append(str).append(g.b(com.umeng.common.b.f(context)));
        return g.a(sb.toString());
    }

    private String a(Context context, String str, SharedPreferences sharedPreferences) {
        d(context, sharedPreferences);
        long currentTimeMillis = System.currentTimeMillis();
        String a2 = a(context, str, currentTimeMillis);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(com.umeng.common.a.g, str);
        edit.putString("session_id", a2);
        edit.putLong("start_millis", currentTimeMillis);
        edit.putLong("end_millis", -1);
        edit.putLong("duration", 0);
        edit.putString("activities", "");
        edit.remove("last_terminate_location_time");
        edit.commit();
        c(context, sharedPreferences);
        return a2;
    }

    private String a(Context context, JSONObject jSONObject, String str, boolean z2, String str2) {
        HttpPost httpPost = new HttpPost(str);
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        httpPost.addHeader("X-Umeng-Sdk", n(context));
        try {
            String a2 = f.a(context);
            if (a2 != null) {
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(a2, 80));
            }
            String jSONObject2 = jSONObject.toString();
            Log.a("MobclickAgent", jSONObject2);
            if (!e.s || z2) {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new BasicNameValuePair("content", jSONObject2));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, e.f));
            } else {
                byte[] a3 = f.a("content=" + jSONObject2, "utf-8");
                httpPost.addHeader("Content-Encoding", "deflate");
                httpPost.setEntity(new InputStreamEntity(new ByteArrayInputStream(a3), (long) f.a));
            }
            SharedPreferences.Editor edit = h.c(context).edit();
            Date date = new Date();
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            long time = new Date().getTime() - date.getTime();
            if (execute.getStatusLine().getStatusCode() == 200) {
                Log.a("MobclickAgent", "Sent message to " + str);
                edit.putLong("req_time", time);
                edit.commit();
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    return a(entity.getContent());
                }
                return null;
            }
            edit.putLong("req_time", -1);
            return null;
        } catch (ClientProtocolException e2) {
            Log.a("MobclickAgent", "ClientProtocolException,Failed to send message.", e2);
            return null;
        } catch (IOException e3) {
            Log.a("MobclickAgent", "IOException,Failed to send message.", e3);
            return null;
        }
    }

    private String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), DirectGraphics.FLIP_HORIZONTAL);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e2) {
                        Log.b("MobclickAgent", "Caught IOException in convertStreamToString()", e2);
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(readLine) + "\n");
                }
            } catch (IOException e3) {
                Log.b("MobclickAgent", "Caught IOException in convertStreamToString()", e3);
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e4) {
                    Log.b("MobclickAgent", "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e5) {
                    Log.b("MobclickAgent", "Caught IOException in convertStreamToString()", e5);
                    return null;
                }
            }
        }
    }

    private JSONArray a(JSONObject jSONObject, JSONArray jSONArray) {
        boolean z2;
        try {
            String string = jSONObject.getString("tag");
            String string2 = jSONObject.has("label") ? jSONObject.getString("label") : null;
            String string3 = jSONObject.getString(MetaDataControl.DATE_KEY);
            int length = jSONArray.length() - 1;
            while (true) {
                if (length < 0) {
                    z2 = false;
                    break;
                }
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(length);
                if (string2 == null && !jSONObject2.has("label")) {
                    if (string.equals(jSONObject2.get("tag")) && string3.equals(jSONObject2.get(MetaDataControl.DATE_KEY))) {
                        jSONObject2.put("acc", jSONObject2.getInt("acc") + 1);
                        z2 = true;
                        break;
                    }
                } else if (string2 != null && jSONObject2.has("label") && string.equals(jSONObject2.get("tag")) && string2.equals(jSONObject2.get("label")) && string3.equals(jSONObject2.get(MetaDataControl.DATE_KEY))) {
                    jSONObject2.put("acc", jSONObject2.getInt("acc") + 1);
                    z2 = true;
                    break;
                }
                length--;
            }
            if (!z2) {
                jSONArray.put(jSONObject);
            }
        } catch (Exception e2) {
            Log.a("MobclickAgent", "custom log merge error in tryToSendMessage", e2);
            jSONArray.put(jSONObject);
        }
        return jSONArray;
    }

    private void a(Context context, SharedPreferences sharedPreferences) {
        Location l2;
        long currentTimeMillis = System.currentTimeMillis();
        if (e.g && currentTimeMillis - sharedPreferences.getLong("last_terminate_location_time", 0) > 10000 && (l2 = com.umeng.common.b.l(context)) != null) {
            if (l2.getTime() != sharedPreferences.getLong("gps_time", 0)) {
                sharedPreferences.edit().putFloat("lng", (float) l2.getLongitude()).putFloat("lat", (float) l2.getLatitude()).putFloat("alt", (float) l2.getAltitude()).putLong("gps_time", l2.getTime()).putLong("last_terminate_location_time", currentTimeMillis).commit();
            }
        }
    }

    private void a(Context context, SharedPreferences sharedPreferences, String str, String str2, long j2, int i2) {
        String string = sharedPreferences.getString("session_id", "");
        String a2 = g.a();
        String str3 = a2.split(" ")[0];
        String str4 = a2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(com.umeng.common.a.b, "event");
            jSONObject.put("session_id", string);
            jSONObject.put(MetaDataControl.DATE_KEY, str3);
            jSONObject.put("time", str4);
            jSONObject.put("tag", str);
            if (!TextUtils.isEmpty(str2)) {
                jSONObject.put("label", str2);
            }
            if (j2 > 0) {
                jSONObject.put("du", j2);
            }
            jSONObject.put("acc", i2);
            this.j.post(new C0001b(this, context, jSONObject));
        } catch (JSONException e2) {
            Log.a("MobclickAgent", "json error in emitCustomLogReport", e2);
        }
    }

    private void a(Context context, SharedPreferences sharedPreferences, String str, JSONObject jSONObject) {
        String string = sharedPreferences.getString("session_id", "");
        JSONObject jSONObject2 = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        try {
            jSONObject.put("id", str);
            jSONObject.put("ts", System.currentTimeMillis() / 1000);
            jSONArray.put(jSONObject);
            jSONObject2.put(com.umeng.common.a.b, "ekv");
            jSONObject2.put(string, jSONArray);
            this.j.post(new C0001b(this, context, jSONObject2));
        } catch (JSONException e2) {
            Log.a("MobclickAgent", "json error in emitCustomLogReport", e2);
            e2.printStackTrace();
        }
    }

    private void a(Context context, JSONArray jSONArray) {
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i2);
                if (jSONObject != null && jSONObject.has(MetaDataControl.DATE_KEY) && jSONObject.has("time") && jSONObject.has("context")) {
                    if (jSONObject.has("version")) {
                        if (jSONObject.getString("version") != null && jSONObject.getString("version").equals(com.umeng.common.b.d(context))) {
                            jSONObject.remove("version");
                        }
                    }
                    this.j.post(new C0001b(this, context, jSONObject));
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private void a(String str) {
        Log.a("MobclickAgent", str);
    }

    private boolean a(SharedPreferences sharedPreferences) {
        return System.currentTimeMillis() - sharedPreferences.getLong("end_millis", -1) > e.d;
    }

    private boolean a(JSONObject jSONObject, JSONObject jSONObject2) {
        try {
            String str = (String) jSONObject.remove("cache_version");
            return str == null || !str.equals(jSONObject2.getString(com.umeng.common.a.f));
        } catch (Exception e2) {
            Log.a("MobclickAgent", "Fail to filter message", e2);
        }
    }

    private String b(Context context, SharedPreferences sharedPreferences) {
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong("start_millis", valueOf.longValue());
        edit.putLong("end_millis", -1);
        edit.commit();
        return sharedPreferences.getString("session_id", null);
    }

    private JSONArray b(JSONObject jSONObject, JSONArray jSONArray) {
        if (jSONArray != null && jSONObject != null) {
            try {
                String next = jSONObject.keys().next();
                int length = jSONArray.length() - 1;
                while (true) {
                    if (length < 0) {
                        jSONArray.put(jSONObject);
                        break;
                    }
                    JSONObject jSONObject2 = (JSONObject) jSONArray.get(length);
                    if (jSONObject2.has(next)) {
                        jSONObject2.getJSONArray(next).put((JSONObject) jSONObject.getJSONArray(next).get(0));
                        break;
                    }
                    length--;
                }
            } catch (Exception e2) {
                Log.a("MobclickAgent", "custom log merge error in tryToSendMessage", e2);
            }
        }
        return jSONArray;
    }

    /* access modifiers changed from: private */
    public synchronized void b(Context context, String str, String str2, long j2, int i2) {
        SharedPreferences e2 = h.e(context);
        if (e2 != null) {
            a(context, e2, str, str2, j2, i2);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(Context context, String str, Map<String, String> map, long j2) {
        SharedPreferences e2 = h.e(context);
        if (e2 != null) {
            try {
                JSONObject jSONObject = new JSONObject();
                Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
                int i2 = 0;
                while (it.hasNext() && i2 < 10) {
                    int i3 = i2 + 1;
                    Map.Entry next = it.next();
                    jSONObject.put((String) next.getKey(), (String) next.getValue());
                    i2 = i3;
                }
                if (j2 > 0) {
                    jSONObject.put("du", j2);
                }
                a(context, e2, str, jSONObject);
            } catch (Exception e3) {
                Log.a("MobclickAgent", "exception when convert map to json");
            }
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void b(Context context, String str, Map<String, String> map, String str2) {
        SharedPreferences e2 = h.e(context);
        if (e2 != null) {
            try {
                f(context, "_kvts" + str + str2);
                JSONObject jSONObject = new JSONObject();
                Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
                int i2 = 0;
                while (it.hasNext() && i2 < 10) {
                    int i3 = i2 + 1;
                    Map.Entry next = it.next();
                    jSONObject.put((String) next.getKey(), (String) next.getValue());
                    i2 = i3;
                }
                e2.edit().putString("_kvvl" + str + str2, jSONObject.toString()).commit();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.analytics.b.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean, java.lang.String):java.lang.String
     arg types: [android.content.Context, org.json.JSONObject, java.lang.String, int, java.lang.String]
     candidates:
      com.umeng.analytics.b.a(com.umeng.analytics.b, android.content.Context, java.lang.String, java.util.Map, long):void
      com.umeng.analytics.b.a(com.umeng.analytics.b, android.content.Context, java.lang.String, java.util.Map, java.lang.String):void
      com.umeng.analytics.b.a(android.content.Context, org.json.JSONObject, org.json.JSONObject, org.json.JSONObject, java.lang.String):org.json.JSONObject
      com.umeng.analytics.b.a(android.content.Context, java.lang.String, java.lang.String, long, int):void
      com.umeng.analytics.b.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean, java.lang.String):java.lang.String */
    /* access modifiers changed from: private */
    public void b(Context context, JSONObject jSONObject) {
        Log.a("MobclickAgent", "start to check onlineConfig info ...");
        String a2 = a(context, jSONObject, "http://oc.umeng.com/check_config_update", true, "online_config");
        if (a2 == null) {
            a2 = a(context, jSONObject, "http://oc.umeng.co/check_config_update", true, "online_config");
        }
        if (a2 != null) {
            Log.a("MobclickAgent", "get onlineConfig info succeed !");
            d(context, a2);
            return;
        }
        if (this.c != null) {
            this.c.onDataReceived(null);
        }
        Log.a("MobclickAgent", "get onlineConfig info failed !");
    }

    private void c(Context context, SharedPreferences sharedPreferences) {
        Location l2;
        String string = sharedPreferences.getString("session_id", null);
        if (string == null) {
            Log.a("MobclickAgent", "Missing session_id, ignore message");
            return;
        }
        String a2 = g.a();
        String str = a2.split(" ")[0];
        String str2 = a2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(com.umeng.common.a.b, "launch");
            jSONObject.put("session_id", string);
            jSONObject.put(MetaDataControl.DATE_KEY, str);
            jSONObject.put("time", str2);
            if (e.g && (l2 = com.umeng.common.b.l(context)) != null) {
                double longitude = l2.getLongitude();
                double latitude = l2.getLatitude();
                double altitude = l2.getAltitude();
                long time = l2.getTime();
                if (time != sharedPreferences.getLong("gps_time", 0)) {
                    jSONObject.put("lng", longitude);
                    jSONObject.put("lat", latitude);
                    jSONObject.put("alt", altitude);
                    jSONObject.put("gps_time", time);
                    sharedPreferences.edit().putLong("gps_time", time).commit();
                }
            }
            this.j.post(new C0001b(this, context, jSONObject));
        } catch (JSONException e2) {
            Log.b("MobclickAgent", "json error in emitNewSessionReport", e2);
        }
    }

    private void d(Context context, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("session_id", null);
        if (string == null) {
            Log.a("MobclickAgent", "Missing session_id, ignore message in emitLastEndSessionReport");
            return;
        }
        Long valueOf = Long.valueOf(sharedPreferences.getLong("duration", -1));
        if (valueOf.longValue() <= 0) {
            valueOf = 0L;
        }
        String a2 = g.a();
        String str = a2.split(" ")[0];
        String str2 = a2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(com.umeng.common.a.b, "terminate");
            jSONObject.put("session_id", string);
            jSONObject.put(MetaDataControl.DATE_KEY, str);
            jSONObject.put("time", str2);
            jSONObject.put("duration", String.valueOf(valueOf.longValue() / 1000));
            if (e.h) {
                String string2 = sharedPreferences.getString("activities", "");
                if (!"".equals(string2)) {
                    String[] split = string2.split(";");
                    JSONArray jSONArray = new JSONArray();
                    for (String jSONArray2 : split) {
                        jSONArray.put(new JSONArray(jSONArray2));
                    }
                    jSONObject.put("activities", jSONArray);
                }
            }
            long[] e2 = e(context, sharedPreferences);
            if (e2 != null) {
                jSONObject.put("uptr", e2[1]);
                jSONObject.put("dntr", e2[0]);
            }
            if (e.g && sharedPreferences.contains("last_terminate_location_time")) {
                jSONObject.put("lat", (double) sharedPreferences.getFloat("lat", 0.0f));
                jSONObject.put("lng", (double) sharedPreferences.getFloat("lng", 0.0f));
                jSONObject.put("alt", (double) sharedPreferences.getFloat("alt", 0.0f));
                jSONObject.put("gps_time", sharedPreferences.getLong("gps_time", 0));
            }
            this.j.post(new C0001b(this, context, jSONObject));
        } catch (JSONException e3) {
            Log.b("MobclickAgent", "json error in emitLastEndSessionReport", e3);
        }
    }

    private void d(Context context, String str) {
        SharedPreferences b2 = h.b(context);
        try {
            JSONObject jSONObject = new JSONObject(str);
            try {
                if (jSONObject.has("last_config_time")) {
                    b2.edit().putString("umeng_last_config_time", jSONObject.getString("last_config_time")).commit();
                }
            } catch (Exception e2) {
                Log.a("MobclickAgent", "save online config time", e2);
            }
            long j2 = -1;
            try {
                if (jSONObject.has("report_interval")) {
                    j2 = (long) (jSONObject.getInt("report_interval") * 1000);
                }
            } catch (Exception e3) {
            }
            try {
                if (jSONObject.has("report_policy")) {
                    this.h.a(context, jSONObject.getInt("report_policy"), j2);
                }
            } catch (Exception e4) {
                Log.a("MobclickAgent", "save online config policy", e4);
            }
            JSONObject jSONObject2 = null;
            try {
                if (jSONObject.has("online_params")) {
                    JSONObject jSONObject3 = new JSONObject(jSONObject.getString("online_params"));
                    Iterator<String> keys = jSONObject3.keys();
                    SharedPreferences.Editor edit = b2.edit();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        edit.putString(next, jSONObject3.getString(next));
                    }
                    edit.commit();
                    Log.a("MobclickAgent", "get online setting params: " + jSONObject3);
                    jSONObject2 = jSONObject3;
                }
                if (this.c != null) {
                    this.c.onDataReceived(jSONObject2);
                }
            } catch (Exception e5) {
                Log.a("MobclickAgent", "save online config params", e5);
            }
        } catch (Exception e6) {
            Log.a("MobclickAgent", "not json string");
        }
    }

    /* access modifiers changed from: private */
    public synchronized void d(Context context, String str, String str2) {
        SharedPreferences e2 = h.e(context);
        if (e2 != null) {
            try {
                int g2 = g(context, "_kvts" + str + str2);
                if (g2 < 0) {
                    a("event duration less than 0 in ekvEvnetEnd");
                } else {
                    JSONObject jSONObject = new JSONObject(e2.getString("_kvvl" + str + str2, null));
                    jSONObject.put("du", g2);
                    a(context, e2, str, jSONObject);
                }
            } catch (Exception e3) {
                a("exception in onLogDurationInternalEnd");
            }
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void e(Context context, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(com.umeng.common.a.b, "online_config");
            jSONObject.put(com.umeng.common.a.g, str);
            jSONObject.put(com.umeng.common.a.f, com.umeng.common.b.d(context));
            jSONObject.put(com.umeng.common.a.c, com.umeng.common.b.u(context));
            jSONObject.put(com.umeng.common.a.h, "4.5");
            jSONObject.put(com.umeng.common.a.e, g.b(com.umeng.common.b.f(context)));
            jSONObject.put(com.umeng.common.a.d, i(context));
            jSONObject.put("report_policy", this.h.b(context));
            jSONObject.put("last_config_time", q(context));
            this.j.post(new C0001b(this, context, jSONObject));
        } catch (Exception e2) {
            Log.b("MobclickAgent", "exception in onlineConfigInternal");
        }
        return;
    }

    private long[] e(Context context, SharedPreferences sharedPreferences) {
        try {
            Class<?> cls = Class.forName("android.net.TrafficStats");
            Method method = cls.getMethod("getUidRxBytes", Integer.TYPE);
            Method method2 = cls.getMethod("getUidTxBytes", Integer.TYPE);
            int i2 = context.getApplicationInfo().uid;
            if (i2 == -1) {
                return null;
            }
            long[] jArr = {((Long) method.invoke(null, Integer.valueOf(i2))).longValue(), ((Long) method2.invoke(null, Integer.valueOf(i2))).longValue()};
            if (jArr[0] <= 0 || jArr[1] <= 0) {
                return null;
            }
            long j2 = sharedPreferences.getLong("traffics_up", -1);
            long j3 = sharedPreferences.getLong("traffics_down", -1);
            sharedPreferences.edit().putLong("traffics_up", jArr[1]).putLong("traffics_down", jArr[0]).commit();
            if (j2 <= 0 || j3 <= 0) {
                return null;
            }
            jArr[0] = jArr[0] - j3;
            jArr[1] = jArr[1] - j2;
            if (jArr[0] <= 0 || jArr[1] <= 0) {
                return null;
            }
            return jArr;
        } catch (Exception e2) {
            Log.a("MobclickAgent", "sdk less than 2.2 has get no traffic");
            return null;
        }
    }

    private void f(Context context, String str) {
        try {
            if (e.k) {
                this.g.a(str);
                return;
            }
            i a2 = i.a(context, str);
            a2.a(Long.valueOf(System.currentTimeMillis()));
            a2.a(context);
        } catch (Exception e2) {
            Log.a("MobclickAgent", "exception in save event begin info");
        }
    }

    private int g(Context context, String str) {
        try {
            long b2 = e.k ? this.g.b(str) : i.a(context, str).a().longValue();
            if (b2 > 0) {
                return (int) (System.currentTimeMillis() - b2);
            }
            return -1;
        } catch (Exception e2) {
            Log.a("MobclickAgent", "exception in get event duration", e2);
            return -1;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void g(Context context) {
        this.h.c(context);
        SharedPreferences e2 = h.e(context);
        if (e2 != null) {
            if (a(e2)) {
                Log.a("MobclickAgent", "Start new session: " + a(context, h(context), e2));
            } else {
                Log.a("MobclickAgent", "Extend current session: " + b(context, e2));
            }
        }
    }

    private String h(Context context) {
        return this.b == null ? com.umeng.common.b.p(context) : this.b;
    }

    private String i(Context context) {
        return this.a == null ? com.umeng.common.b.t(context) : this.a;
    }

    /* access modifiers changed from: private */
    public synchronized void j(Context context) {
        SharedPreferences e2 = h.e(context);
        if (e2 != null) {
            long j2 = e2.getLong("start_millis", -1);
            if (j2 == -1) {
                Log.b("MobclickAgent", "onEndSession called before onStartSession");
            } else {
                long currentTimeMillis = System.currentTimeMillis();
                long j3 = currentTimeMillis - j2;
                long j4 = e2.getLong("duration", 0);
                SharedPreferences.Editor edit = e2.edit();
                if (e.h) {
                    String string = e2.getString("activities", "");
                    String str = this.i;
                    if (!"".equals(string)) {
                        string = String.valueOf(string) + ";";
                    }
                    edit.remove("activities");
                    edit.putString("activities", String.valueOf(string) + "[" + str + "," + (j3 / 1000) + "]");
                }
                edit.putLong("start_millis", -1);
                edit.putLong("end_millis", currentTimeMillis);
                edit.putLong("duration", j3 + j4);
                edit.commit();
            }
            a(context, e2);
            if (this.g.a() > 0) {
                k(context);
            }
        }
    }

    private void k(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(com.umeng.common.a.b, "cmd_cache_buffer");
            this.j.post(new C0001b(this, context, jSONObject));
        } catch (JSONException e2) {
            Log.b("MobclickAgent", "json error in emitCache");
        }
    }

    private synchronized void l(Context context) {
        p(context);
    }

    /* access modifiers changed from: private */
    public synchronized void m(Context context) {
        if (this.f != null) {
            JSONArray b2 = this.f.b(context);
            if (!(b2 == null || b2.length() == 0)) {
                a(context, b2);
            }
        }
    }

    private String n(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Android");
        stringBuffer.append("/");
        stringBuffer.append("4.5");
        stringBuffer.append(" ");
        try {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(context.getPackageManager().getApplicationLabel(context.getApplicationInfo()).toString());
            stringBuffer2.append("/");
            stringBuffer2.append(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
            stringBuffer2.append(" ");
            stringBuffer2.append(Build.MODEL);
            stringBuffer2.append("/");
            stringBuffer2.append(Build.VERSION.RELEASE);
            stringBuffer2.append(" ");
            stringBuffer2.append(g.b(com.umeng.common.b.f(context)));
            stringBuffer.append(URLEncoder.encode(stringBuffer2.toString()));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return stringBuffer.toString();
    }

    private JSONObject o(Context context) {
        JSONObject h2;
        JSONObject jSONObject = new JSONObject();
        try {
            String f2 = com.umeng.common.b.f(context);
            if (f2 == null || f2.equals("")) {
                Log.b("MobclickAgent", "No device id");
                return null;
            }
            String h3 = h(context);
            if (h3 == null) {
                Log.b("MobclickAgent", "No appkey");
                return null;
            }
            jSONObject.put("device_id", f2);
            jSONObject.put(com.umeng.common.a.e, g.b(f2));
            jSONObject.put("mc", com.umeng.common.b.q(context));
            jSONObject.put("device_model", Build.MODEL);
            jSONObject.put(com.umeng.common.a.g, h3);
            jSONObject.put(com.umeng.common.a.d, i(context));
            jSONObject.put("app_version", com.umeng.common.b.e(context));
            jSONObject.put(com.umeng.common.a.f, com.umeng.common.b.d(context));
            jSONObject.put("sdk_type", "Android");
            jSONObject.put(com.umeng.common.a.h, "4.5");
            jSONObject.put("os", "Android");
            jSONObject.put("os_version", Build.VERSION.RELEASE);
            jSONObject.put("timezone", com.umeng.common.b.n(context));
            String[] o2 = com.umeng.common.b.o(context);
            if (o2 != null) {
                jSONObject.put("country", o2[0]);
                jSONObject.put("language", o2[1]);
            }
            jSONObject.put("resolution", com.umeng.common.b.r(context));
            String[] j2 = com.umeng.common.b.j(context);
            if (j2 != null && j2[0].equals("2G/3G")) {
                jSONObject.put("access", j2[0]);
                jSONObject.put("access_subtype", j2[1]);
            } else if (j2 != null) {
                jSONObject.put("access", j2[0]);
            } else {
                jSONObject.put("access", "Unknown");
            }
            jSONObject.put("carrier", com.umeng.common.b.s(context));
            jSONObject.put("cpu", com.umeng.common.b.a());
            if (!this.d.equals("")) {
                jSONObject.put("gpu_vender", this.d);
            }
            if (!this.e.equals("")) {
                jSONObject.put("gpu_renderer", this.e);
            }
            if (e.i && (h2 = h.h(context)) != null) {
                jSONObject.put("uinfo", h2);
            }
            jSONObject.put(com.umeng.common.a.c, com.umeng.common.b.u(context));
            return jSONObject;
        } catch (Exception e2) {
            Log.b("MobclickAgent", "getMessageHeader error", e2);
            return null;
        }
    }

    private void p(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(com.umeng.common.a.b, "flush");
            this.j.post(new C0001b(this, context, jSONObject));
        } catch (JSONException e2) {
            Log.b("MobclickAgent", "json error in emitCache");
        }
    }

    private String q(Context context) {
        return h.b(context).getString("umeng_last_config_time", "");
    }

    /* access modifiers changed from: package-private */
    public JSONObject a(Context context, JSONObject jSONObject, JSONObject jSONObject2, JSONObject jSONObject3, String str) {
        SharedPreferences c2 = h.c(context);
        long j2 = c2.getLong("req_time", 0);
        if (j2 != 0) {
            try {
                jSONObject2.put("req_time", j2);
            } catch (JSONException e2) {
                Log.a("MobclickAgent", "json error in tryToSendMessage", e2);
            }
        }
        c2.edit().putString("header", jSONObject2.toString()).commit();
        JSONObject jSONObject4 = new JSONObject();
        if (str == null) {
            return null;
        }
        try {
            if (!"flush".equals(str) || jSONObject != null) {
                if (jSONObject != null) {
                    if (a(jSONObject, jSONObject2)) {
                        jSONObject.remove(PlayerListener.ERROR);
                    }
                }
                if (!"flush".equals(str)) {
                    if (jSONObject == null) {
                        jSONObject = new JSONObject();
                    }
                    if (jSONObject.isNull(str)) {
                        JSONArray jSONArray = new JSONArray();
                        jSONArray.put(jSONObject3);
                        jSONObject.put(str, jSONArray);
                    } else {
                        JSONArray jSONArray2 = jSONObject.getJSONArray(str);
                        if ("ekv".equals(str)) {
                            b(jSONObject3, jSONArray2);
                        } else {
                            jSONArray2.put(jSONObject3);
                        }
                    }
                }
                jSONObject4.put("header", jSONObject2);
                jSONObject4.put("body", jSONObject);
                return jSONObject4;
            }
            Log.e("MobclickAgent", "No cache message to flush in constructMessage");
            return null;
        } catch (JSONException e3) {
            Log.b("MobclickAgent", "Fail to construct json message in tryToSendMessage.", e3);
            h.j(context);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        if (context == null) {
            try {
                Log.b("MobclickAgent", "unexpected null context in onPause");
            } catch (Exception e2) {
                Log.b("MobclickAgent", "Exception occurred in Mobclick.onRause(). ", e2);
            }
        } else if (!context.getClass().getName().equals(this.i)) {
            Log.b("MobclickAgent", "onPause() called without context from corresponding onResume()");
        } else {
            new a(context, 0).start();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str) {
        if (str != null && str != "" && str.length() <= 10240) {
            if (context == null) {
                Log.b("MobclickAgent", "unexpected null context in reportError");
            } else if (this.f != null) {
                this.f.a(context, str);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, String str2) {
        if (context == null || TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            Log.a("MobclickAgent", "invalid params in onEventBegin");
        } else {
            f(context, "_tl" + str + str2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, String str2, long j2, int i2) {
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str) && i2 > 0) {
                    if (this.h.a() || !e.k) {
                        new a(context, str, str2, j2, i2, 3).start();
                        return;
                    } else if (this.g.a(context, str, str2, j2, i2)) {
                        k(context);
                        return;
                    } else {
                        return;
                    }
                }
            } catch (Exception e2) {
                Log.b("MobclickAgent", "Exception occurred in Mobclick.onEvent(). ", e2);
                return;
            }
        }
        Log.a("MobclickAgent", "invalid params in onEvent");
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, Map<String, String> map, long j2) {
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    if (map == null || map.isEmpty()) {
                        Log.a("MobclickAgent", "map is null or empty in onEvent");
                        return;
                    } else if (this.h.a() || !e.k) {
                        new a(context, str, map, j2, 4).start();
                        return;
                    } else if (this.g.a(context, str, map, j2)) {
                        k(context);
                        return;
                    } else {
                        return;
                    }
                }
            } catch (Exception e2) {
                Log.b("MobclickAgent", "Exception occurred in Mobclick.onEvent(). ", e2);
                return;
            }
        }
        Log.a("MobclickAgent", "invalid params in onKVEventEnd");
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, Map<String, String> map, String str2) {
        if (context == null || TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            Log.a("MobclickAgent", "invalid params in onKVEventBegin");
        } else if (map == null || map.isEmpty()) {
            Log.a("MobclickAgent", "map is null or empty in onKVEventBegin");
        } else {
            try {
                if (e.k) {
                    String str3 = String.valueOf(str) + str2;
                    this.g.a(str3, map);
                    this.g.a(str3);
                    return;
                }
                new a(context, str, map, str2, 5).start();
            } catch (Exception e2) {
                Log.a("MobclickAgent", "exception in save k-v event begin inof", e2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.analytics.b.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean, java.lang.String):java.lang.String
     arg types: [android.content.Context, org.json.JSONObject, java.lang.String, int, java.lang.String]
     candidates:
      com.umeng.analytics.b.a(com.umeng.analytics.b, android.content.Context, java.lang.String, java.util.Map, long):void
      com.umeng.analytics.b.a(com.umeng.analytics.b, android.content.Context, java.lang.String, java.util.Map, java.lang.String):void
      com.umeng.analytics.b.a(android.content.Context, org.json.JSONObject, org.json.JSONObject, org.json.JSONObject, java.lang.String):org.json.JSONObject
      com.umeng.analytics.b.a(android.content.Context, java.lang.String, java.lang.String, long, int):void
      com.umeng.analytics.b.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean, java.lang.String):java.lang.String */
    /* access modifiers changed from: protected */
    public void a(Context context, JSONObject jSONObject) {
        String str;
        String str2 = (String) jSONObject.remove(com.umeng.common.a.b);
        JSONObject a2 = a(context, h.i(context), o(context), jSONObject, str2);
        if (a2 != null && !a2.isNull("body")) {
            if (!this.h.a(str2, context)) {
                h.b(context, a2);
                return;
            }
            String str3 = null;
            int i2 = 0;
            while (true) {
                if (i2 < e.p.length) {
                    str = a(context, a2, e.p[i2], false, str2);
                    if (str != null) {
                        break;
                    }
                    i2++;
                    str3 = str;
                } else {
                    str = str3;
                    break;
                }
            }
            if (str != null) {
                Log.a("MobclickAgent", "send applog succeed :" + str);
                h.j(context);
                this.h.a(context);
                return;
            }
            h.b(context, a2);
            Log.a("MobclickAgent", "send applog failed");
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Context context) {
        try {
            String h2 = h(context);
            if (h2 == null || h2.length() == 0) {
                Log.b("MobclickAgent", "unexpected empty appkey in onError");
            } else if (context == null) {
                Log.b("MobclickAgent", "unexpected null context in onError");
            } else {
                if (this.f != null) {
                    this.f.a(context);
                    this.f.a(this);
                }
                new a(context, 2).start();
            }
        } catch (Exception e2) {
            Log.b("MobclickAgent", "Exception occurred in Mobclick.onError()", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Context context, String str) {
        if (context == null || TextUtils.isEmpty(str)) {
            Log.a("MobclickAgent", "invalid params in onEventBegin");
        } else {
            f(context, "_t" + str);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Context context, String str, String str2) {
        if (context == null || TextUtils.isEmpty(str2)) {
            Log.a("MobclickAgent", "invalid params in onEventEnd");
            return;
        }
        int g2 = g(context, "_tl" + str + str2);
        if (g2 < 0) {
            a("event duration less than 0 in onEvnetEnd");
            return;
        }
        a(context, str, str2, (long) g2, 1);
    }

    /* access modifiers changed from: package-private */
    public void c(Context context) {
        if (context == null) {
            try {
                Log.b("MobclickAgent", "unexpected null context in onResume");
            } catch (Exception e2) {
                Log.b("MobclickAgent", "Exception occurred in Mobclick.onResume(). ", e2);
            }
        } else {
            this.i = context.getClass().getName();
            new a(context, 1).start();
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Context context, String str) {
        if (context == null || TextUtils.isEmpty(str)) {
            Log.a("MobclickAgent", "input Context is null or event_id is empty");
            return;
        }
        int g2 = g(context, "_t" + str);
        if (g2 < 0) {
            Log.a("MobclickAgent", "event duration less than 0 in onEventEnd");
            return;
        }
        a(context, str, (String) null, (long) g2, 1);
    }

    /* access modifiers changed from: package-private */
    public void c(Context context, String str, String str2) {
        if (context == null || TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            Log.a("MobclickAgent", "invalid params in onKVEventEnd");
        } else if (e.k) {
            String str3 = String.valueOf(str) + str2;
            int g2 = g(context, str3);
            if (g2 < 0) {
                a("event duration less than 0 in onEvnetEnd");
                return;
            }
            a(context, str, this.g.c(str3), (long) g2);
        } else {
            new a(context, str, (Map<String, String>) null, str2, 6).start();
        }
    }

    /* access modifiers changed from: package-private */
    public void d(Context context) {
        if (context == null) {
            try {
                Log.b("MobclickAgent", "unexpected null context in flush");
            } catch (Exception e2) {
                Log.b("MobclickAgent", "Exception occurred in Mobclick.flush(). ", e2);
                return;
            }
        }
        l(context);
    }

    public void e(Context context) {
        if (context == null) {
            try {
                Log.b("MobclickAgent", "unexpected null context in updateOnlineConfig");
            } catch (Exception e2) {
                Log.b("MobclickAgent", "exception in updateOnlineConfig");
            }
        } else {
            String h2 = h(context);
            if (h2 == null) {
                Log.b("MobclickAgent", "unexpected null appkey in updateOnlineConfig");
            } else {
                new Thread(new c(this, context, h2)).start();
            }
        }
    }

    public void f(Context context) {
        try {
            this.g.a(context);
            j(context);
        } catch (Exception e2) {
            Log.a("MobclickAgent", "Exception in onAppCrash", e2);
        }
    }
}
