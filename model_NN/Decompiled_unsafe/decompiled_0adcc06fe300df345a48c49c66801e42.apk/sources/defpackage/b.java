package defpackage;

import com.a.a.a.g;
import com.a.a.a.h;
import com.a.a.a.i;
import com.nokia.mid.ui.DirectGraphics;
import com.nokia.mid.ui.FullCanvas;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.control.MIDIControl;
import javax.microedition.media.control.MetaDataControl;
import javax.microedition.media.control.ToneControl;

/* renamed from: b  reason: default package */
public final class b extends com.a.a.a.b implements Runnable {
    private static int aA = 0;
    private static int aB = 0;
    private static int aC;
    private static int aD;
    private static boolean aE = false;
    private static boolean aF;
    private static int aG;
    private static int aH = 0;
    private static int aI = 0;
    private static int[] aJ;
    private static int[] aK;
    private static int aL;
    private static int aM;
    private static int aN;
    private static int aO;
    private static int aP;
    private static int aQ;
    private static boolean aR;
    private static boolean aS;
    private static int aT;
    private static int aU;
    private static int aV;
    private static int aW;
    private static int aX;
    private static int aY = 0;
    private static int am;
    private static String[] an;
    private static int ao;
    private static int ap;
    private static boolean aq = true;
    private static boolean ar = false;
    private static boolean as = false;
    private static boolean at = false;
    private static boolean au = false;
    private static boolean av = false;
    private static boolean aw = false;
    private static boolean ax = false;
    private static boolean ay = false;
    private static int az = 0;
    private static String[] p = {"/back.mid", "/happy.mid", "/cry.mid"};
    private static int q;
    private static int x = 104;
    private static int z;
    private i aZ = i("net");
    private c aj;
    private a ak;
    private Player al;
    private i bA;
    private i[][] bB;
    private i[] bC;
    private InputStream bD;
    private com.a.a.c.b bE;
    private i ba = i("sp");
    private i bb;
    private i bc;
    private i bd;
    private i be;
    private i bf;
    private i bg;
    private i bh;
    private i bi;
    private i bj;
    private i bk;
    private i bl;
    private i bm;
    private i bn;
    private i bo;
    private i bp;
    private i bq;
    private i[] br;
    private i[] bs;
    private i[] bt;
    private i[] bu;
    private i[] bv;
    private i bw;
    private i bx;
    private i by;
    private i bz;

    public b() {
        aS = true;
        aC = 101;
        aD = 0;
        p[0] = "/back.mid";
        p[1] = "/happy.mid";
        p[2] = "/cry.mid";
        getClass();
        this.bD = MIDPHelper.j(p[0]);
        try {
            this.al = Manager.a(this.bD, "audio/midi");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MediaException e2) {
            e2.printStackTrace();
        }
        if (aS) {
            try {
                this.al.f(-1);
                this.al.start();
            } catch (MediaException e3) {
                e3.printStackTrace();
            }
        }
    }

    private void a() {
        this.aZ = null;
        System.gc();
        this.bb = i(MetaDataControl.TITLE_KEY);
        this.bc = i("titleBack1");
        this.bd = i("titleBack2");
        this.be = i("arrow");
        this.bf = i("menu");
        this.bg = i("menu1");
        this.aj = new c();
        this.ak = new a();
        this.bw = i("img/gameArrow");
        this.bx = i("img/arrowTD");
        this.bh = i("img/map");
        this.bi = i("img/gameBack1");
        this.bj = i("img/gameBack2");
        this.bk = i("img/talkBar");
        this.bp = i("img/shadow");
        this.bl = i("img/player");
        this.bm = i("img/position");
        this.bn = i("img/menuBar");
        this.bo = i("img/selectBar");
        this.bq = i("girl/body");
        this.bt = new i[8];
        for (int i = 0; i < 8; i++) {
            this.bt[i] = i(new StringBuffer("girl/dress").append(i).toString());
        }
        this.bu = new i[8];
        for (int i2 = 0; i2 < 8; i2++) {
            this.bu[i2] = i(new StringBuffer("girl/dress_").append(i2).toString());
        }
        this.br = new i[3];
        for (int i3 = 0; i3 < 3; i3++) {
            this.br[i3] = i(new StringBuffer("girl/face").append(i3).toString());
        }
        this.bs = new i[7];
        for (int i4 = 0; i4 < 7; i4++) {
            this.bs[i4] = i(new StringBuffer("girl/hair").append(i4).toString());
        }
        this.bv = new i[7];
        for (int i5 = 0; i5 < 7; i5++) {
            this.bv[i5] = i(new StringBuffer("girl/shoes").append(i5).toString());
        }
        this.by = i("img/store");
        this.bz = i("img/store00");
        this.bA = i("action/round");
        this.bB = new i[6][];
        this.bB[0] = new i[3];
        this.bB[1] = new i[4];
        this.bB[2] = new i[4];
        this.bB[3] = new i[2];
        this.bB[4] = new i[4];
        this.bB[5] = new i[1];
        this.bB[0][0] = i("action/action_00");
        this.bB[0][1] = i("action/action_01");
        this.bB[0][2] = i("action/action_02");
        this.bB[1][0] = i("action/action_10");
        this.bB[1][1] = i("action/action_11");
        this.bB[1][2] = i("action/action_12");
        this.bB[1][3] = i("action/action_13");
        this.bB[2][0] = i("action/action_20");
        this.bB[2][1] = i("action/action_21");
        this.bB[2][2] = i("action/action_22");
        this.bB[2][3] = i("action/action_23");
        this.bB[3][0] = i("action/action_30");
        this.bB[3][1] = i("action/action_31");
        this.bB[4][0] = i("action/action_40");
        this.bB[4][1] = i("action/action_41");
        this.bB[4][2] = i("action/action_42");
        this.bB[4][3] = i("action/action_43");
        this.bB[5][0] = i("action/action_50");
        this.bC = new i[2];
        this.bC[0] = i("action/happy");
        this.bC[1] = i("action/cry");
    }

    private void a(h hVar) {
        hVar.c(0, 0, 240, 320);
        hVar.b(255, 255, 255);
        hVar.b(0, 0, 240, 320);
        if (az % 6 < 3) {
            for (int i = 0; i < 8; i++) {
                for (int i2 = 0; i2 < 10; i2++) {
                    if ((i + i2) % 2 == 0) {
                        hVar.a(this.bc, this.bc.getWidth() * i, this.bc.getHeight() * i2, 0);
                    } else {
                        hVar.a(this.bd, this.bd.getWidth() * i, this.bd.getHeight() * i2, 0);
                    }
                }
            }
        } else {
            for (int i3 = 0; i3 < 8; i3++) {
                for (int i4 = 0; i4 < 10; i4++) {
                    if ((i3 + i4) % 2 == 0) {
                        hVar.a(this.bd, this.bd.getWidth() * i3, this.bd.getHeight() * i4, 0);
                    } else {
                        hVar.a(this.bc, this.bc.getWidth() * i3, this.bc.getHeight() * i4, 0);
                    }
                }
            }
        }
        hVar.a(this.bb, (240 - this.bb.getWidth()) / 2, ((320 - this.bb.getHeight()) / 2) - 20, 0);
        if (az % 4 < 2) {
            hVar.c(40, 260, 10, 13);
            hVar.a(this.be, 40, 260, 0);
            hVar.c(190, 260, 10, 13);
            hVar.a(this.be, (int) DirectGraphics.ROTATE_180, 260, 0);
        }
    }

    private void a(h hVar, int i, int i2, int i3, int i4) {
        hVar.c(178, 55, 25, 19);
        hVar.a(this.br[i], 178 - ((aV % 2) * 25), 55, 0);
        hVar.c(0, 0, 240, 320);
        hVar.a(this.bp, 190, this.bq.getHeight() + 74 + 2, 3);
        hVar.a(this.bq, 190, 74, 17);
        hVar.a(this.bv[i4], 190, this.bq.getHeight() + 74 + 2, 33);
        hVar.a(this.bt[i3], 190, 74, 17);
        hVar.a(this.bs[i2], 190, 33, 17);
    }

    private static void a(h hVar, String str, int i, int i2, int i3, int i4, int i5) {
        hVar.setColor(i3);
        hVar.a(str, i - 1, i2, i5);
        hVar.a(str, i, i2 - 1, i5);
        hVar.a(str, i + 1, i2, i5);
        hVar.a(str, i, i2 + 1, i5);
        hVar.setColor(i4);
        hVar.a(str, i, i2, i5);
    }

    private void b() {
        int i = 0;
        if (ax || ay) {
            ax = false;
            ay = false;
            aD = 102;
            ao = 0;
        }
        if (aw) {
            aw = false;
            if (aL < this.aj.bI.length - 1) {
                aL++;
            } else {
                aL = 0;
            }
        }
        if (av) {
            av = false;
            if (aL > 0) {
                aL--;
            } else {
                aL = this.aj.bI.length - 1;
            }
        }
        if (at) {
            at = false;
            aD = 101;
            m();
        }
        if (au) {
            au = false;
            aG = 0;
            switch (aL) {
                case 0:
                    aH = 0;
                    for (int i2 : this.ak.Y[0]) {
                        if (i2 == 1) {
                            aH++;
                        }
                    }
                    aJ = new int[aH];
                    int i3 = 0;
                    for (int i4 = 0; i4 < this.ak.Y[0].length; i4++) {
                        if (this.ak.Y[0][i4] == 1) {
                            aJ[i3] = i4;
                            i3++;
                        }
                    }
                    aD = 104;
                    return;
                case 1:
                    aI = 0;
                    for (int i5 : this.ak.Y[1]) {
                        if (i5 == 1) {
                            aI++;
                        }
                    }
                    aK = new int[aI];
                    for (int i6 = 0; i6 < this.ak.Y[1].length; i6++) {
                        if (this.ak.Y[1][i6] == 1) {
                            aK[i] = i6;
                            i++;
                        }
                    }
                    aD = 105;
                    return;
                case 2:
                    aD = 101;
                    m();
                    return;
                default:
                    return;
            }
        }
    }

    private void b(h hVar) {
        hVar.c(0, 0, 240, 320);
        for (int i = 0; i < 9; i++) {
            for (int i2 = 0; i2 < 11; i2++) {
                if ((i + i2) % 2 == 0) {
                    hVar.a(this.bi, this.bi.getWidth() * i, this.bi.getHeight() * i2, 0);
                } else {
                    hVar.a(this.bj, this.bj.getWidth() * i, this.bj.getHeight() * i2, 0);
                }
            }
        }
        switch (aD) {
            case 101:
                hVar.a(this.bh, 0, 0, 0);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                switch (aQ) {
                    case 0:
                        hVar.a(this.bm, 110, 130, 0);
                        break;
                    case 1:
                        hVar.a(this.bm, 110, 25, 0);
                        break;
                    case 2:
                        hVar.a(this.bm, 190, 80, 0);
                        break;
                    case 3:
                        hVar.a(this.bm, 190, 170, 0);
                        break;
                    case 4:
                        hVar.a(this.bm, 20, 170, 0);
                        break;
                    case 5:
                        hVar.a(this.bm, 20, 80, 0);
                        break;
                }
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, this.aj.p[aQ], 120, 4, 16777215, 354981, 17);
                hVar.setColor(14502145);
                hVar.a(new StringBuffer("今天是").append(q).append("月").append(am).append("日").toString(), 56, 278, 0);
                hVar.a("我要到哪里去呢？", 56, 296, 0);
                return;
            case 102:
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, this.aj.p[aQ], 120, 4, 16777215, 354981, 17);
                a(hVar, aM, aN, aO, aP);
                hVar.c(0, 0, 240, 320);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                for (int i3 = 0; i3 < this.aj.bL[aQ][0].length; i3++) {
                    if (aL == i3) {
                        hVar.setColor(16777215);
                        hVar.a(this.bo, 2, (i3 * 27) + 31, 0);
                        hVar.a(this.aj.bL[aQ][0][i3], 18, (i3 * 27) + 37, 0);
                    } else {
                        hVar.setColor(16773464);
                        hVar.a(this.bo, -13, (i3 * 27) + 31, 0);
                        hVar.a(this.aj.bL[aQ][0][i3], 3, (i3 * 27) + 37, 0);
                    }
                }
                hVar.setColor(14502145);
                if (aF) {
                    hVar.a("金钱不足！", 56, 278, 0);
                } else {
                    an = null;
                    an = h(this.aj.bL[aQ][1][aL]);
                    hVar.c(0, 278, 240, 36);
                    for (int i4 = 0; i4 < an.length; i4++) {
                        hVar.a(an[i4], 56, ao + 278 + (i4 * 18), 0);
                    }
                }
                switch (aQ) {
                    case 0:
                        hVar.c((90 - ((aV % 2) * 3)) + 60, 125, 18, 14);
                        hVar.a(this.bw, (90 - ((aV % 2) * 3)) + 60, 125, 0);
                        hVar.c(((aV % 2) * 3) + 152 + 60, 125, 18, 14);
                        hVar.a(this.bw, ((aV % 2) * 3) + 134 + 60, 125, 0);
                        return;
                    default:
                        return;
                }
            case 103:
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, "更衣室", 120, 4, 16777215, 354981, 17);
                a(hVar, aM, aN, aO, aP);
                hVar.c((90 - ((aV % 2) * 3)) + 60, 125, 18, 14);
                hVar.a(this.bw, (90 - ((aV % 2) * 3)) + 60, 125, 0);
                hVar.c(((aV % 2) * 3) + 152 + 60, 125, 18, 14);
                hVar.a(this.bw, ((aV % 2) * 3) + 134 + 60, 125, 0);
                hVar.c(0, 0, 240, 320);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                for (int i5 = 0; i5 < this.aj.bI.length; i5++) {
                    if (aL == i5) {
                        hVar.setColor(16777215);
                        hVar.a(this.bo, 2, (i5 * 27) + 31, 0);
                        hVar.a(this.aj.bI[i5], 18, (i5 * 27) + 37, 0);
                    } else {
                        hVar.setColor(16773464);
                        hVar.a(this.bo, -13, (i5 * 27) + 31, 0);
                        hVar.a(this.aj.bI[i5], 3, (i5 * 27) + 37, 0);
                    }
                }
                hVar.setColor(14502145);
                hVar.a(this.aj.bJ[aL << 1], 56, 278, 0);
                hVar.a(this.aj.bJ[(aL << 1) + 1], 56, 296, 0);
                return;
            case 104:
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, "我的衣柜", 120, 4, 16777215, 354981, 17);
                a(hVar, aM, aN, aO, aP);
                hVar.c(0, 0, 240, 320);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                hVar.a(this.by, 3, 33, 0);
                for (int i6 = 0; i6 < aH; i6++) {
                    hVar.a(this.bu[aJ[i6]], ((i6 % 3) * 27) + 21, ((i6 / 3) * 38) + 57, 3);
                }
                hVar.b(0, 255, 0);
                hVar.a(((aG % 3) * 27) + 10, ((aG / 3) * 38) + 43, 23, 29);
                hVar.setColor(14502145);
                return;
            case 105:
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, "我的鞋柜", 120, 4, 16777215, 354981, 17);
                a(hVar, aM, aN, aO, aP);
                hVar.c(0, 0, 240, 320);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                hVar.a(this.by, 3, 33, 0);
                for (int i7 = 0; i7 < aI; i7++) {
                    hVar.a(this.bv[aK[i7]], ((i7 % 3) * 27) + 21, ((i7 / 3) * 38) + 57, 3);
                }
                hVar.b(0, 255, 0);
                hVar.a(((aG % 3) * 27) + 10, ((aG / 3) * 38) + 43, 23, 29);
                hVar.setColor(14502145);
                return;
            case 106:
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, this.aj.p[aQ], 120, 4, 16777215, 354981, 17);
                hVar.a(this.bA, 120, 160, 3);
                hVar.c(120 - (this.bB[aQ][aL].getWidth() >> 2), 160 - (this.bB[aQ][aL].getHeight() >> 1), this.bB[aQ][aL].getWidth() >> 1, this.bB[aQ][aL].getHeight());
                hVar.a(this.bB[aQ][aL], (120 - (this.bB[aQ][aL].getWidth() >> 2)) + ((this.bB[aQ][aL].getWidth() >> 1) * (aV % 2)), 160, 3);
                hVar.c(0, 0, 240, 320);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                hVar.setColor(14502145);
                ap = 0;
                hVar.c(0, 278, 240, 36);
                for (int i8 = 0; i8 < this.ak.ae.length; i8++) {
                    if (i8 < 2 || i8 > 7) {
                        if (aQ == 0 && aL == 0 && i8 == 0) {
                            hVar.a("体力全恢复", 56, ao + 278 + (ap * 18), 0);
                            ap++;
                        } else if (this.ak.ag[aQ][aL][i8] > 0) {
                            hVar.a(new StringBuffer().append(this.aj.bK[i8]).append("增加 ").append(this.ak.ag[aQ][aL][i8]).toString(), 56, ao + 278 + (ap * 18), 0);
                            ap++;
                        } else if (this.ak.ag[aQ][aL][i8] < 0) {
                            hVar.a(new StringBuffer().append(this.aj.bK[i8]).append("减少 ").append(this.ak.ag[aQ][aL][i8] * -1).toString(), 56, ao + 278 + (ap * 18), 0);
                            ap++;
                        }
                    } else if (this.ak.ag[aQ][aL][i8] > 0) {
                        if (this.ak.ae[1] >= 80) {
                            hVar.a(new StringBuffer().append(this.aj.bK[i8]).append("增加 ").append(this.ak.ag[aQ][aL][i8] * 2).toString(), 56, ao + 278 + (ap * 18), 0);
                            ap++;
                        } else if (this.ak.ae[1] <= 20) {
                            hVar.a(new StringBuffer().append(this.aj.bK[i8]).append("增加 ").append(this.ak.ag[aQ][aL][i8] * 0).toString(), 56, ao + 278 + (ap * 18), 0);
                            ap++;
                        } else {
                            hVar.a(new StringBuffer().append(this.aj.bK[i8]).append("增加 ").append(this.ak.ag[aQ][aL][i8]).toString(), 56, ao + 278 + (ap * 18), 0);
                            ap++;
                        }
                    } else if (this.ak.ag[aQ][aL][i8] < 0) {
                        hVar.a(new StringBuffer().append(this.aj.bK[i8]).append("减少 ").append(this.ak.ag[aQ][aL][i8] * -1).toString(), 56, ao + 278 + (ap * 18), 0);
                        ap++;
                    }
                    if (aQ == 3 && aL == 0 && i8 == 0) {
                        hVar.a("体力上限增加 5", 56, ao + 278 + (ap * 18), 0);
                        ap++;
                    }
                }
                return;
            case 107:
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, "精品服装", 120, 4, 16777215, 354981, 17);
                a(hVar, aM, aN, aO, aP);
                hVar.c(0, 0, 240, 320);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                hVar.a(this.by, 3, 33, 0);
                for (int i9 = 0; i9 < 8; i9++) {
                    hVar.a(this.bu[i9], ((i9 % 3) * 27) + 21, ((i9 / 3) * 38) + 57, 3);
                }
                hVar.b(0, 255, 0);
                hVar.a(((aG % 3) * 27) + 10, ((aG / 3) * 38) + 43, 23, 29);
                hVar.setColor(14502145);
                hVar.a(new StringBuffer("价格：").append(this.ak.aa[0][aG]).toString(), 56, 278, 0);
                hVar.a(new StringBuffer("魅力增加").append(this.ak.aa[1][aG]).toString(), 56, 296, 0);
                return;
            case 108:
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, "名牌鞋袜", 120, 4, 16777215, 354981, 17);
                a(hVar, aM, aN, aO, aP);
                hVar.c(0, 0, 240, 320);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                hVar.a(this.by, 3, 33, 0);
                for (int i10 = 0; i10 < 7; i10++) {
                    hVar.a(this.bv[i10], ((i10 % 3) * 27) + 21, ((i10 / 3) * 38) + 57, 3);
                }
                hVar.b(0, 255, 0);
                hVar.a(((aG % 3) * 27) + 10, ((aG / 3) * 38) + 43, 23, 29);
                hVar.setColor(14502145);
                hVar.a(new StringBuffer("价格：").append(this.ak.ab[0][aG]).toString(), 56, 278, 0);
                hVar.a(new StringBuffer("魅力增加").append(this.ak.ab[1][aG]).toString(), 56, 296, 0);
                return;
            case 109:
            default:
                return;
            case 110:
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, "高级发廊", 120, 4, 16777215, 354981, 17);
                a(hVar, aM, aN, aO, aP);
                hVar.c(0, 0, 240, 320);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                hVar.a(this.bz, 3, 33, 0);
                for (int i11 = 0; i11 < 7; i11++) {
                    hVar.setColor(16670721);
                    hVar.a(this.aj.an[i11], 12, (i11 * 17) + 40, 0);
                }
                hVar.b(0, 0, 255);
                hVar.a(this.aj.an[aG], 12, (aG * 17) + 40, 0);
                hVar.setColor(14502145);
                hVar.a(new StringBuffer("价格：").append(this.ak.ac[0][aG]).toString(), 56, 278, 0);
                hVar.a(new StringBuffer("魅力增加").append(this.ak.ac[1][aG]).toString(), 56, 296, 0);
                return;
            case 111:
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, "我的状态", 120, 4, 16777215, 354981, 17);
                hVar.a(this.bz, 3, 33, 0);
                if (this.ak.ae[2] + this.ak.aa[1][aO] + this.ak.ab[1][aP] + this.ak.ac[1][aN] >= 500 && this.ak.ae[2] + this.ak.aa[1][aO] + this.ak.ab[1][aP] + this.ak.ac[1][aN] < 800) {
                    aY = 1;
                } else if (this.ak.ae[2] + this.ak.aa[1][aO] + this.ak.ab[1][aP] + this.ak.ac[1][aN] >= 800) {
                    aY = 2;
                } else {
                    aY = 0;
                }
                hVar.setColor(16593003);
                if (aU == 0) {
                    hVar.a(new StringBuffer().append(this.aj.bK[10]).append("：").append(this.ak.ae[10] + this.ak.ad[aY]).append("%").toString(), 11, 41, 0);
                    hVar.a(new StringBuffer().append(this.aj.bK[8]).append("：  ").append(this.ak.ae[8]).toString(), 11, 59, 0);
                    hVar.a(new StringBuffer().append(this.aj.bK[0]).append("：  ").append(this.ak.ae[0]).toString(), 11, 77, 0);
                    hVar.a(new StringBuffer().append(this.aj.bK[1]).append("：  ").append(this.ak.ae[1]).toString(), 11, 95, 0);
                    hVar.a(new StringBuffer().append(this.aj.bK[2]).append("：  ").append(this.ak.ae[2] + this.ak.aa[1][aO] + this.ak.ab[1][aP] + this.ak.ac[1][aN]).toString(), 11, 113, 0);
                    hVar.c(69, 139 - ((aV % 2) * 1), 13, 15);
                    hVar.a(this.bx, 69, 124 - ((aV % 2) * 1), 0);
                } else {
                    hVar.a(new StringBuffer().append(this.aj.bK[3]).append("：  ").append(this.ak.ae[3]).toString(), 11, 41, 0);
                    hVar.a(new StringBuffer().append(this.aj.bK[4]).append("：  ").append(this.ak.ae[4]).toString(), 11, 59, 0);
                    hVar.a(new StringBuffer().append(this.aj.bK[5]).append("：  ").append(this.ak.ae[5]).toString(), 11, 77, 0);
                    hVar.a(new StringBuffer().append(this.aj.bK[6]).append("：  ").append(this.ak.ae[6]).toString(), 11, 95, 0);
                    hVar.c(69, 139 - ((aV % 2) * 1), 13, 15);
                    hVar.a(this.bx, 69, 139 - ((aV % 2) * 1), 0);
                }
                a(hVar, aM, aN, aO, aP);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                hVar.setColor(14502145);
                hVar.a("距离都市女孩选秀", 56, 278, 0);
                hVar.a(new StringBuffer("还有").append(365 - this.ak.ae[9]).append("天").toString(), 56, 296, 0);
                return;
            case 112:
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, "发生事件", 120, 4, 16777215, 354981, 17);
                hVar.a(this.bA, 120, 160, 3);
                if (this.ak.ai[aQ][aW][12] == 99) {
                    hVar.c(120 - (this.bC[0].getWidth() >> 2), 160 - (this.bC[0].getHeight() >> 1), this.bC[0].getWidth() >> 1, this.bC[0].getHeight());
                    hVar.a(this.bC[0], (120 - (this.bC[0].getWidth() >> 2)) + ((this.bC[0].getWidth() >> 1) * (aV % 2)), 160, 3);
                } else if (this.ak.ai[aQ][aW][12] == 0) {
                    hVar.c(120 - (this.bC[1].getWidth() >> 2), 160 - (this.bC[1].getHeight() >> 1), this.bC[1].getWidth() >> 1, this.bC[1].getHeight());
                    hVar.a(this.bC[1], (120 - (this.bC[1].getWidth() >> 2)) + ((this.bC[1].getWidth() >> 1) * (aV % 2)), 160, 3);
                }
                hVar.c(0, 0, 240, 320);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                hVar.setColor(14502145);
                an = null;
                an = h(this.aj.bM[aQ][aW]);
                hVar.c(0, 278, 240, 36);
                for (int i12 = 0; i12 < an.length; i12++) {
                    hVar.a(an[i12], 56, ao + 278 + (i12 * 18), 0);
                }
                return;
            case 113:
                switch (z) {
                    case 0:
                        hVar.setColor(0);
                        hVar.b(0, 0, 240, 320);
                        a(hVar, "激动人心啊，年末", 26, 44, 16777215, 354981, 20);
                        a(hVar, "的动感女孩评选已", 26, 64, 16777215, 354981, 20);
                        a(hVar, "经开始了！希望大", 26, 84, 16777215, 354981, 20);
                        a(hVar, "家充分展示自己的", 26, 104, 16777215, 354981, 20);
                        a(hVar, "风采，抓住难得的", 26, 124, 16777215, 354981, 20);
                        a(hVar, new StringBuffer("机会哦~！你有").append(this.ak.ae[10] + this.ak.ad[aY]).append("%").toString(), 26, MIDIControl.NOTE_ON, 16777215, 354981, 20);
                        a(hVar, "的机会入选，加油！", 26, 164, 16777215, 354981, 20);
                        return;
                    case 1:
                        hVar.setColor(0);
                        hVar.b(0, 0, 240, 320);
                        a(hVar, "动感女孩的决赛正", 26, 24, 16777215, 354981, 20);
                        a(hVar, "在进行，你目前的", 26, 44, 16777215, 354981, 20);
                        a(hVar, "综合素质为：", 26, 64, 16777215, 354981, 20);
                        a(hVar, new StringBuffer().append(this.aj.bK[2]).append("：").append(this.ak.ae[2] + this.ak.aa[1][aO] + this.ak.ab[1][aP] + this.ak.ac[1][aN]).toString(), 26, 84, 16777215, 354981, 20);
                        a(hVar, new StringBuffer().append(this.aj.bK[3]).append("：").append(this.ak.ae[3]).toString(), 26, 104, 16777215, 354981, 20);
                        a(hVar, new StringBuffer().append(this.aj.bK[4]).append("：").append(this.ak.ae[4]).toString(), 26, 124, 16777215, 354981, 20);
                        a(hVar, new StringBuffer().append(this.aj.bK[5]).append("：").append(this.ak.ae[5]).toString(), 26, MIDIControl.NOTE_ON, 16777215, 354981, 20);
                        a(hVar, new StringBuffer().append(this.aj.bK[6]).append("：").append(this.ak.ae[6]).toString(), 26, 164, 16777215, 354981, 20);
                        return;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        hVar.setColor(0);
                        hVar.b(0, 0, 240, 320);
                        hVar.setColor(16777215);
                        for (int i13 = 0; i13 < this.aj.bH[z - 2].length; i13++) {
                            hVar.a(this.aj.bH[z - 2][i13], 26, aT + (i13 * 18), 20);
                        }
                        return;
                    case 99:
                        hVar.setColor(0);
                        hVar.b(0, 0, 240, 320);
                        a(hVar, "好遗憾啊。你没能", 26, 44, 16777215, 354981, 20);
                        a(hVar, "入选动感女孩的决", 26, 64, 16777215, 354981, 20);
                        a(hVar, "赛，不过付出的努", 26, 84, 16777215, 354981, 20);
                        a(hVar, "力绝对不会白费的，", 26, 104, 16777215, 354981, 20);
                        a(hVar, "继续加油吧，相信", 26, 124, 16777215, 354981, 20);
                        a(hVar, "你一定能行的。", 26, MIDIControl.NOTE_ON, 16777215, 354981, 20);
                        return;
                    default:
                        return;
                }
            case 114:
                hVar.a(this.bn, (240 - this.bn.getWidth()) / 2, 2, 0);
                a(hVar, "发生事件", 120, 4, 16777215, 354981, 17);
                hVar.a(this.bA, 120, 160, 3);
                hVar.c(120 - (this.bC[1].getWidth() >> 2), 160 - (this.bC[1].getHeight() >> 1), this.bC[1].getWidth() >> 1, this.bC[1].getHeight());
                hVar.a(this.bC[1], (120 - (this.bC[1].getWidth() >> 2)) + ((this.bC[1].getWidth() >> 1) * (aV % 2)), 160, 3);
                hVar.c(0, 0, 240, 320);
                hVar.a(this.bk, (240 - this.bk.getWidth()) / 2, 320 - this.bk.getHeight(), 0);
                hVar.a(this.bl, 4, (320 - this.bl.getHeight()) - 4, 0);
                hVar.setColor(14502145);
                hVar.a("没有体力了，只好", 56, 278, 0);
                hVar.a("去医院修养2天了！", 56, 296, 0);
                return;
        }
    }

    private static void d() {
        aT = 198;
        q = 1;
        am = 1;
        aM = 0;
        aN = 0;
        aO = 0;
        aV = 0;
        aX = 0;
    }

    private static String[] h(String str) {
        String[] strArr = new String[50];
        char[] charArray = str.toCharArray();
        char[] cArr = new char[23];
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i < charArray.length) {
            int i5 = charArray[i] > 128 ? 16 : 6;
            if (charArray[i] == 10 || i2 + i5 > 112) {
                int i6 = i4 + 1;
                strArr[i4] = new String(cArr, 0, i3);
                if (charArray[i] == 10) {
                    while (i6 % 2 != 0) {
                        strArr[i6] = "";
                        i6++;
                    }
                    i3 = 0;
                    i4 = i6;
                    i2 = 0;
                    i++;
                } else {
                    i3 = 0;
                    i4 = i6;
                    i2 = 0;
                }
            }
            if (charArray[i] == ' ' && i2 == 0 && i != 0) {
                i++;
            } else {
                i2 += i5;
                cArr[i3] = charArray[i];
                i3++;
                i++;
            }
        }
        if (i3 > 0 && (i3 > 1 || cArr[0] != '.')) {
            strArr[i4] = new String(cArr, 0, i3);
            i4++;
        }
        String[] strArr2 = new String[i4];
        System.arraycopy(strArr, 0, strArr2, 0, i4);
        return strArr2;
    }

    private i i(String str) {
        i iVar = null;
        System.gc();
        try {
            iVar = i.k(new StringBuffer("/").append(str).append(".png").toString());
        } catch (Exception e) {
        }
        if (aC == 103) {
            aA++;
            m();
            System.out.println(new StringBuffer("logoBar = ").append(aA).toString());
        }
        return iVar;
    }

    private void j() {
        int abs = Math.abs(new Random().nextInt()) % 100;
        int i = 0;
        for (int[] iArr : this.ak.ai[aQ]) {
            i += iArr[11];
        }
        if (abs >= i || aX == 1) {
            aD = 102;
            aL = 0;
            return;
        }
        aL = 0;
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.ak.ai[aQ].length) {
            if (abs < i3 || abs >= this.ak.ai[aQ][i2][11] + i3) {
                i3 += this.ak.ai[aQ][i2][11];
                i2++;
            } else {
                aV = 0;
                aW = i2;
                aD = 112;
                ao = 0;
                if (this.ak.ai[aQ][aW][12] != 99) {
                    int[][][] iArr2 = this.ak.ai;
                    int i4 = aQ;
                    int i5 = aW;
                }
                for (int i6 = 0; i6 < this.ak.ae.length; i6++) {
                    int[] iArr3 = this.ak.ae;
                    iArr3[i6] = iArr3[i6] + this.ak.ai[aQ][aW][i6];
                    if (this.ak.ae[i6] >= this.ak.af[i6]) {
                        this.ak.ae[i6] = this.ak.af[i6];
                    }
                }
                for (int i7 = 0; i7 < this.ak.ae.length; i7++) {
                    if (this.ak.ae[i7] < 0) {
                        this.ak.ae[i7] = 0;
                    }
                }
                int i8 = am + this.ak.ai[aQ][aW][9];
                am = i8;
                if (i8 > this.ak.ah[q - 1]) {
                    am -= this.ak.ah[q - 1];
                    q++;
                    return;
                }
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b(int i) {
        ar = true;
        switch (i) {
            case FullCanvas.KEY_SOFTKEY2 /*-7*/:
                at = true;
                return;
            case FullCanvas.KEY_SOFTKEY1 /*-6*/:
                as = true;
                return;
            case FullCanvas.KEY_SOFTKEY3 /*-5*/:
            case com.a.a.a.b.KEY_NUM5:
                au = true;
                return;
            case FullCanvas.KEY_RIGHT_ARROW /*-4*/:
            case com.a.a.a.b.KEY_NUM6:
                ay = true;
                return;
            case FullCanvas.KEY_LEFT_ARROW /*-3*/:
            case com.a.a.a.b.KEY_NUM4:
                ax = true;
                return;
            case -2:
            case com.a.a.a.b.KEY_NUM8:
                aw = true;
                return;
            case -1:
            case com.a.a.a.b.KEY_NUM2:
                av = true;
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void c(h hVar) {
        hVar.a(g.a(0, 0, 8));
        switch (aC) {
            case 101:
                hVar.c(0, 0, 240, 320);
                hVar.b(255, 255, 255);
                hVar.b(0, 0, 240, 320);
                hVar.a(this.aZ, (240 - this.aZ.getWidth()) / 2, (320 - this.aZ.getHeight()) / 2, 0);
                return;
            case 102:
            default:
                return;
            case 103:
                hVar.c(0, 0, 240, 320);
                hVar.b(255, 255, 255);
                hVar.b(0, 0, 240, 320);
                hVar.a(this.ba, (240 - this.ba.getWidth()) / 2, (320 - this.ba.getHeight()) / 2, 0);
                hVar.setColor(255);
                hVar.a("读取中...", 120, 160, 17);
                return;
            case 104:
            case 111:
                a(hVar);
                hVar.c((240 - this.bf.getWidth()) / 2, 260, this.bf.getWidth(), 13);
                hVar.a(this.bf, (240 - this.bf.getWidth()) / 2, (180 - (aB * 13)) + 80, 0);
                return;
            case 105:
                a(hVar);
                hVar.c((240 - this.bg.getWidth()) / 2, 260, this.bg.getWidth(), 13);
                if (aR) {
                    hVar.a(this.bg, (240 - this.bg.getWidth()) / 2, 260, 0);
                    return;
                } else {
                    hVar.a(this.bg, (240 - this.bg.getWidth()) / 2, 247, 0);
                    return;
                }
            case 106:
                a(hVar);
                hVar.c((240 - this.bg.getWidth()) / 2, 260, this.bg.getWidth(), 13);
                if (aS) {
                    hVar.a(this.bg, (240 - this.bg.getWidth()) / 2, 234, 0);
                    return;
                } else {
                    hVar.a(this.bg, (240 - this.bg.getWidth()) / 2, 221, 0);
                    return;
                }
            case 107:
                hVar.c(0, 0, 240, 320);
                hVar.setColor(0);
                hVar.b(0, 0, 240, 320);
                a(hVar, "帮  助", 120, 5, 16711866, 16777215, 17);
                hVar.c(0, 34, 240, 286);
                hVar.setColor(16777215);
                for (int i = 0; i < this.aj.bF.length; i++) {
                    hVar.a(this.aj.bF[i], 120, aT + (i * 18), 17);
                }
                hVar.a("返回", 220, (int) Player.PREFETCHED, 17);
                return;
            case 108:
                hVar.c(0, 0, 240, 320);
                hVar.setColor(0);
                hVar.b(0, 0, 240, 320);
                a(hVar, "", 120, 15, 16711866, 16777215, 17);
                hVar.setColor(16777215);
                hVar.a("", 120, 40, 17);
                hVar.a("", 120, 58, 17);
                hVar.a("", 120, 76, 17);
                hVar.a("", 120, 94, 17);
                hVar.a("返回", 220, (int) Player.PREFETCHED, 17);
                return;
            case 109:
                hVar.c(0, 0, 240, 320);
                hVar.setColor(0);
                hVar.b(0, 0, 240, 320);
                hVar.b(255, 255, 255);
                hVar.c(0, 40, 240, 240);
                for (int i2 = 0; i2 < this.aj.bG.length; i2++) {
                    hVar.a(this.aj.bG[i2], 120, aT + (i2 * 18), 17);
                }
                return;
            case 110:
                b(hVar);
                return;
        }
    }

    public final void i() {
        if (aC != 110) {
            return;
        }
        if (aD != 106 || aD != 112) {
            aC = 111;
            aB = 5;
            m();
        }
    }

    public final void run() {
        while (aq) {
            switch (aC) {
                case 104:
                    if (ax) {
                        ax = false;
                        if (aB > 0) {
                            aB--;
                        } else {
                            aB = 4;
                        }
                    } else if (ay) {
                        ay = false;
                        if (aB < 4) {
                            aB++;
                        } else {
                            aB = 0;
                        }
                    }
                    if (au || as) {
                        au = false;
                        as = false;
                        switch (aB) {
                            case 0:
                                d();
                                aC = 109;
                                break;
                            case 1:
                                aC = 106;
                                break;
                            case 2:
                                aC = 107;
                                aT = 35;
                                break;
                            case 3:
                                aC = 108;
                                m();
                                break;
                            case 4:
                                Midlet.X.a();
                                break;
                        }
                    }
                    break;
                case 105:
                    if (ax || ay) {
                        ax = false;
                        ay = false;
                        aR = !aR;
                    }
                    if (at) {
                        at = false;
                        aC = x;
                    }
                    if (as || au) {
                        as = false;
                        au = false;
                        if (aR) {
                            d();
                        } else {
                            try {
                                byte[] bArr = new byte[140];
                                int[] iArr = new int[35];
                                this.bE = com.a.a.c.b.a("girl", true);
                                if (this.bE.I() <= 0) {
                                    this.ak.Y[0][0] = 1;
                                    iArr[0] = 1;
                                    this.ak.Y[0][1] = 0;
                                    iArr[1] = 0;
                                    this.ak.Y[0][2] = 0;
                                    iArr[2] = 0;
                                    this.ak.Y[0][3] = 0;
                                    iArr[3] = 0;
                                    this.ak.Y[0][4] = 0;
                                    iArr[4] = 0;
                                    this.ak.Y[0][5] = 0;
                                    iArr[5] = 0;
                                    this.ak.Y[0][6] = 0;
                                    iArr[6] = 0;
                                    this.ak.Y[0][7] = 0;
                                    iArr[7] = 0;
                                    this.ak.Y[1][0] = 1;
                                    iArr[8] = 1;
                                    this.ak.Y[1][1] = 0;
                                    iArr[9] = 0;
                                    this.ak.Y[1][2] = 0;
                                    iArr[10] = 0;
                                    this.ak.Y[1][3] = 0;
                                    iArr[11] = 0;
                                    this.ak.Y[1][4] = 0;
                                    iArr[12] = 0;
                                    this.ak.Y[1][5] = 0;
                                    iArr[13] = 0;
                                    this.ak.Y[1][6] = 0;
                                    iArr[14] = 0;
                                    this.ak.ae[0] = 100;
                                    iArr[15] = 100;
                                    this.ak.ae[1] = 50;
                                    iArr[16] = 50;
                                    this.ak.ae[2] = 15;
                                    iArr[17] = 15;
                                    this.ak.ae[3] = 15;
                                    iArr[18] = 15;
                                    this.ak.ae[4] = 40;
                                    iArr[19] = 40;
                                    this.ak.ae[5] = 20;
                                    iArr[20] = 20;
                                    this.ak.ae[6] = 20;
                                    iArr[21] = 20;
                                    this.ak.ae[7] = 20;
                                    iArr[22] = 20;
                                    this.ak.ae[8] = 500;
                                    iArr[23] = 500;
                                    this.ak.ae[9] = 1;
                                    iArr[24] = 1;
                                    this.ak.ae[10] = 0;
                                    iArr[25] = 0;
                                    this.ak.af[0] = 100;
                                    iArr[26] = 100;
                                    aT = 198;
                                    iArr[27] = 198;
                                    q = 1;
                                    iArr[28] = 1;
                                    am = 1;
                                    iArr[29] = 1;
                                    aM = 0;
                                    iArr[30] = 0;
                                    aN = 0;
                                    iArr[31] = 0;
                                    aO = 0;
                                    iArr[32] = 0;
                                    aV = 0;
                                    iArr[33] = 0;
                                    aX = 0;
                                    iArr[34] = 0;
                                    for (int i = 0; i < iArr.length; i++) {
                                        int i2 = iArr[i];
                                        int i3 = i * 4;
                                        int i4 = 0;
                                        while (i4 < 4) {
                                            bArr[i3] = (byte) i2;
                                            i2 >>= 8;
                                            i4++;
                                            i3++;
                                        }
                                    }
                                    this.bE.d(bArr, 0, bArr.length);
                                } else {
                                    byte[] g = this.bE.g(1);
                                    for (int i5 = 0; i5 < iArr.length; i5++) {
                                        int i6 = 0;
                                        int i7 = (i5 * 4) + 3;
                                        int i8 = 0;
                                        while (i6 < 4) {
                                            i8 = (i8 << 8) | (g[i7] & ToneControl.SILENCE);
                                            i6++;
                                            i7--;
                                        }
                                        iArr[i5] = i8;
                                    }
                                    this.ak.Y[0][0] = iArr[0];
                                    this.ak.Y[0][1] = iArr[1];
                                    this.ak.Y[0][2] = iArr[2];
                                    this.ak.Y[0][3] = iArr[3];
                                    this.ak.Y[0][4] = iArr[4];
                                    this.ak.Y[0][5] = iArr[5];
                                    this.ak.Y[0][6] = iArr[6];
                                    this.ak.Y[0][7] = iArr[7];
                                    this.ak.Y[1][0] = iArr[8];
                                    this.ak.Y[1][1] = iArr[9];
                                    this.ak.Y[1][2] = iArr[10];
                                    this.ak.Y[1][3] = iArr[11];
                                    this.ak.Y[1][4] = iArr[12];
                                    this.ak.Y[1][5] = iArr[13];
                                    this.ak.Y[1][6] = iArr[14];
                                    this.ak.ae[0] = iArr[15];
                                    this.ak.ae[1] = iArr[16];
                                    this.ak.ae[2] = iArr[17];
                                    this.ak.ae[3] = iArr[18];
                                    this.ak.ae[4] = iArr[19];
                                    this.ak.ae[5] = iArr[20];
                                    this.ak.ae[6] = iArr[21];
                                    this.ak.ae[7] = iArr[22];
                                    this.ak.ae[8] = iArr[23];
                                    this.ak.ae[9] = iArr[24];
                                    this.ak.ae[10] = iArr[25];
                                    this.ak.af[0] = iArr[26];
                                    aT = iArr[27];
                                    q = iArr[28];
                                    am = iArr[29];
                                    aM = iArr[30];
                                    aN = iArr[31];
                                    aO = iArr[32];
                                    aV = iArr[33];
                                    aX = iArr[34];
                                }
                                this.bE.H();
                            } catch (Exception e) {
                            }
                        }
                        x = 111;
                        aC = 109;
                        m();
                        break;
                    }
                case 106:
                    if (ax || ay || as || au) {
                        ax = false;
                        ay = false;
                        as = false;
                        au = false;
                        boolean z2 = !aS;
                        aS = z2;
                        if (z2) {
                            try {
                                this.al.f(-1);
                                this.al.start();
                            } catch (MediaException e2) {
                                e2.printStackTrace();
                            }
                        } else {
                            try {
                                this.al.stop();
                            } catch (MediaException e3) {
                                e3.printStackTrace();
                            }
                        }
                    }
                    if (at) {
                        at = false;
                        aC = x;
                        break;
                    }
                    break;
                case 107:
                    if (aw && aT > 35 - ((this.aj.bF.length - 8) * 18)) {
                        aT -= 18;
                    }
                    if (av && aT < 35) {
                        aT += 18;
                    }
                    if (at) {
                        at = false;
                        aC = x;
                    }
                    m();
                    break;
                case 108:
                    if (at) {
                        at = false;
                        aC = x;
                        break;
                    }
                    break;
                case 109:
                    if (au) {
                        au = false;
                        aC = 110;
                        aD = 101;
                        aQ = 0;
                        m();
                        break;
                    }
                    break;
                case 110:
                    if (as && !(aD == 106 && aD == 112)) {
                        as = false;
                        aC = 111;
                        aB = 5;
                        m();
                    }
                    switch (aD) {
                        case 101:
                            if (ay || aw) {
                                ay = false;
                                aw = false;
                                if (aQ >= 5) {
                                    aQ = 0;
                                } else {
                                    aQ++;
                                }
                                m();
                            }
                            if (ax || av) {
                                ax = false;
                                av = false;
                                if (aQ <= 0) {
                                    aQ = 5;
                                } else {
                                    aQ--;
                                }
                                m();
                            }
                            if (au) {
                                au = false;
                                ao = 0;
                                j();
                                aX = 1;
                                aF = false;
                                m();
                            }
                            if (at) {
                                at = false;
                                aD = 111;
                                aU = 0;
                                m();
                                break;
                            }
                            break;
                        case 102:
                            if (aQ == 0 && (ax || ay)) {
                                ax = false;
                                ay = false;
                                aD = 103;
                                aL = 0;
                            }
                            if (aw) {
                                aw = false;
                                if (aL < this.aj.bL[aQ][0].length - 1) {
                                    aL++;
                                } else {
                                    aL = 0;
                                }
                                aF = false;
                                ao = 0;
                            }
                            if (av) {
                                av = false;
                                if (aL > 0) {
                                    aL--;
                                } else {
                                    aL = this.aj.bL[aQ][0].length - 1;
                                }
                                aF = false;
                                ao = 0;
                            }
                            if (at) {
                                at = false;
                                aD = 101;
                                aF = false;
                                m();
                            }
                            if (au) {
                                au = false;
                                if (this.ak.Z[aQ][aL] == 99) {
                                    if (this.ak.ae[8] + this.ak.ag[aQ][aL][8] >= 0) {
                                        aF = false;
                                    } else {
                                        aF = true;
                                    }
                                    if (!aF || this.ak.ag[aQ][aL][8] >= 0) {
                                        aD = 106;
                                        ao = 10;
                                        aV = 0;
                                        for (int i9 = 0; i9 < this.ak.ae.length; i9++) {
                                            if (i9 < 2 || i9 > 7) {
                                                int[] iArr2 = this.ak.ae;
                                                iArr2[i9] = iArr2[i9] + this.ak.ag[aQ][aL][i9];
                                            } else if (this.ak.ag[aQ][aL][i9] <= 0) {
                                                int[] iArr3 = this.ak.ae;
                                                iArr3[i9] = iArr3[i9] + this.ak.ag[aQ][aL][i9];
                                            } else if (this.ak.ae[1] >= 80) {
                                                int[] iArr4 = this.ak.ae;
                                                iArr4[i9] = iArr4[i9] + (this.ak.ag[aQ][aL][i9] * 2);
                                            } else if (this.ak.ae[1] <= 20) {
                                                int[] iArr5 = this.ak.ae;
                                                iArr5[i9] = iArr5[i9] + (this.ak.ag[aQ][aL][i9] * 0);
                                            } else {
                                                int[] iArr6 = this.ak.ae;
                                                iArr6[i9] = iArr6[i9] + this.ak.ag[aQ][aL][i9];
                                            }
                                            if (this.ak.ae[i9] < 0) {
                                                this.ak.ae[i9] = 0;
                                            }
                                            if (aQ == 3 && aL == 0 && i9 == 0) {
                                                int[] iArr7 = this.ak.af;
                                                iArr7[0] = iArr7[0] + 5;
                                            }
                                            if (this.ak.ae[i9] >= this.ak.af[i9]) {
                                                this.ak.ae[i9] = this.ak.af[i9];
                                            }
                                        }
                                        int i10 = am + this.ak.ag[aQ][aL][9];
                                        am = i10;
                                        if (i10 > this.ak.ah[q - 1]) {
                                            am -= this.ak.ah[q - 1];
                                            q++;
                                        }
                                    }
                                } else if (this.ak.Z[aQ][aL] == -1) {
                                    aD = 101;
                                } else if (this.ak.Z[aQ][aL] == 11) {
                                    aD = 107;
                                    aG = 0;
                                    int[] iArr8 = this.ak.ae;
                                    iArr8[9] = iArr8[9] + 2;
                                    int i11 = am + 2;
                                    am = i11;
                                    if (i11 > this.ak.ah[q - 1]) {
                                        am -= this.ak.ah[q - 1];
                                        q++;
                                    }
                                    aX = 0;
                                } else if (this.ak.Z[aQ][aL] == 12) {
                                    aD = 108;
                                    aG = 0;
                                    int[] iArr9 = this.ak.ae;
                                    iArr9[9] = iArr9[9] + 2;
                                    int i12 = am + 2;
                                    am = i12;
                                    if (i12 > this.ak.ah[q - 1]) {
                                        am -= this.ak.ah[q - 1];
                                        q++;
                                    }
                                    aX = 0;
                                } else if (this.ak.Z[aQ][aL] == 13) {
                                    aD = 110;
                                    aG = 0;
                                    int[] iArr10 = this.ak.ae;
                                    iArr10[9] = iArr10[9] + 2;
                                    int i13 = am + 2;
                                    am = i13;
                                    if (i13 > this.ak.ah[q - 1]) {
                                        am -= this.ak.ah[q - 1];
                                        q++;
                                    }
                                    aX = 0;
                                } else if (this.ak.Z[aQ][aL] == 0) {
                                    aD = 101;
                                }
                                m();
                                break;
                            }
                            break;
                        case 103:
                            b();
                            break;
                        case 104:
                            if (ay || aw) {
                                ay = false;
                                aw = false;
                                if (aG < aH - 1) {
                                    aG++;
                                } else {
                                    aG = 0;
                                }
                            }
                            if (ax || av) {
                                ax = false;
                                av = false;
                                if (aG > 0) {
                                    aG--;
                                } else {
                                    aG = aH - 1;
                                }
                            }
                            if (au) {
                                au = false;
                                aO = aJ[aG];
                            }
                            if (at) {
                                at = false;
                                aD = 103;
                                break;
                            }
                            break;
                        case 105:
                            if (ay || aw) {
                                ay = false;
                                aw = false;
                                if (aG < aI - 1) {
                                    aG++;
                                } else {
                                    aG = 0;
                                }
                            }
                            if (ax || av) {
                                ax = false;
                                av = false;
                                if (aG > 0) {
                                    aG--;
                                } else {
                                    aG = aI - 1;
                                }
                            }
                            if (au) {
                                au = false;
                                aP = aK[aG];
                            }
                            if (at) {
                                at = false;
                                aD = 103;
                                break;
                            }
                            break;
                        case 107:
                        case 108:
                        case 110:
                            if (aD == 107) {
                                if (ay || aw) {
                                    ay = false;
                                    aw = false;
                                    if (aG < 7) {
                                        aG++;
                                    } else {
                                        aG = 0;
                                    }
                                }
                                if (ax || av) {
                                    ax = false;
                                    av = false;
                                    if (aG > 0) {
                                        aG--;
                                    } else {
                                        aG = 7;
                                    }
                                }
                                if (au) {
                                    au = false;
                                    if (this.ak.Y[0][aG] == 0 && this.ak.ae[8] >= this.ak.aa[0][aG]) {
                                        this.ak.Y[0][aG] = 1;
                                        int[] iArr11 = this.ak.ae;
                                        iArr11[8] = iArr11[8] - this.ak.aa[0][aG];
                                    }
                                }
                            } else if (aD == 108) {
                                if (ay || aw) {
                                    ay = false;
                                    aw = false;
                                    if (aG < 6) {
                                        aG++;
                                    } else {
                                        aG = 0;
                                    }
                                }
                                if (ax || av) {
                                    ax = false;
                                    av = false;
                                    if (aG > 0) {
                                        aG--;
                                    } else {
                                        aG = 6;
                                    }
                                }
                                if (au) {
                                    au = false;
                                    if (this.ak.Y[1][aG] == 0 && this.ak.ae[8] >= this.ak.ab[0][aG]) {
                                        this.ak.Y[1][aG] = 1;
                                        int[] iArr12 = this.ak.ae;
                                        iArr12[8] = iArr12[8] - this.ak.ab[0][aG];
                                    }
                                }
                            } else if (aD == 110) {
                                if (aw) {
                                    aw = false;
                                    if (aG < 6) {
                                        aG++;
                                    } else {
                                        aG = 0;
                                    }
                                }
                                if (av) {
                                    av = false;
                                    if (aG > 0) {
                                        aG--;
                                    } else {
                                        aG = 6;
                                    }
                                }
                                if (au) {
                                    au = false;
                                    if (aN != aG && this.ak.ae[8] >= this.ak.ac[0][aG]) {
                                        aN = aG;
                                        int[] iArr13 = this.ak.ae;
                                        iArr13[8] = iArr13[8] - this.ak.ac[0][aG];
                                    }
                                }
                            }
                            if (at) {
                                at = false;
                                aD = 111;
                                break;
                            }
                            break;
                        case 111:
                            if (aU == 0) {
                                if (aw) {
                                    aw = false;
                                    aU = 1;
                                }
                            } else if (aU == 1 && av) {
                                av = false;
                                aU = 0;
                            }
                            if (at) {
                                at = false;
                                aQ = 0;
                                if (q == 12 && (am == 16 || am == 23 || am == 30)) {
                                    aD = 113;
                                    z = 0;
                                } else if (this.ak.ae[0] < 10) {
                                    aD = 114;
                                    aV = 0;
                                    this.ak.ae[0] = this.ak.af[0];
                                    int[] iArr14 = this.ak.ae;
                                    iArr14[9] = iArr14[9] + 2;
                                    int i14 = am + this.ak.ag[aQ][aL][9];
                                    am = i14;
                                    if (i14 > this.ak.ah[q - 1]) {
                                        am -= this.ak.ah[q - 1];
                                        q++;
                                    }
                                } else {
                                    aD = 101;
                                }
                                m();
                                break;
                            }
                            break;
                        case 113:
                            if (z == 0) {
                                if (au) {
                                    au = false;
                                    new Random().nextInt();
                                    if (this.ak.ae[10] + this.ak.ad[aY] >= 0) {
                                        z = 1;
                                    } else {
                                        z = 99;
                                    }
                                }
                            } else if (z == 99) {
                                if (au) {
                                    au = false;
                                    if (am == 30) {
                                        aC = 104;
                                    }
                                    aD = 101;
                                }
                            } else if (z == 1 && au) {
                                au = false;
                                aT = 198;
                                int i15 = this.ak.ae[2] + this.ak.aa[1][aO] + this.ak.ab[1][aP] + this.ak.ac[1][aN] + this.ak.ae[3] + this.ak.ae[4] + this.ak.ae[5] + this.ak.ae[6] + this.ak.ae[7];
                                if (i15 >= 400) {
                                    z = 2;
                                } else if (i15 >= 300 && i15 < 400) {
                                    z = 3;
                                } else if (i15 < 200 || i15 >= 300) {
                                    z = 5;
                                } else {
                                    z = 4;
                                }
                            }
                            m();
                            break;
                    }
                case 111:
                    if (ax) {
                        ax = false;
                        if (aB > 0) {
                            aB--;
                        } else {
                            aB = 5;
                        }
                    } else if (ay) {
                        ay = false;
                        if (aB < 5) {
                            aB++;
                        } else {
                            aB = 0;
                        }
                    } else if (at) {
                        at = false;
                        aC = 110;
                        m();
                    }
                    if (au || as) {
                        au = false;
                        as = false;
                        switch (aB) {
                            case 0:
                                d();
                                aC = 109;
                                break;
                            case 1:
                                aC = 106;
                                break;
                            case 2:
                                aC = 107;
                                aT = 35;
                                break;
                            case 3:
                                aC = 108;
                                m();
                                break;
                            case 4:
                                Midlet.X.a();
                                break;
                            case 5:
                                aC = 110;
                                m();
                                break;
                        }
                    }
                    break;
            }
            ar = false;
            as = false;
            at = false;
            au = false;
            av = false;
            aw = false;
            ax = false;
            ay = false;
            switch (aC) {
                case 101:
                    if (az <= 1 && !ar) {
                        az++;
                        break;
                    } else {
                        ar = false;
                        aC = 103;
                        az = 0;
                        m();
                        break;
                    }
                    break;
                case 103:
                    if (az <= 1) {
                        az++;
                        break;
                    } else {
                        ar = false;
                        a();
                        aC = 104;
                        this.ba = null;
                        System.gc();
                        az = 0;
                        m();
                        break;
                    }
                case 104:
                case 105:
                case 106:
                case 111:
                    int i16 = az + 1;
                    az = i16;
                    if (i16 >= 99) {
                        az = 0;
                    }
                    m();
                    break;
                case 109:
                    if (aT > -230) {
                        aT--;
                    } else {
                        aC = 110;
                        aD = 101;
                        aQ = 0;
                    }
                    m();
                    break;
                case 110:
                    switch (aD) {
                        case 102:
                        case 103:
                        case 104:
                        case 105:
                        case 107:
                        case 108:
                        case 110:
                        case 111:
                            if (aV > 99) {
                                aV = 0;
                            } else {
                                aV++;
                                if (an != null) {
                                    if (ao >= (an.length - 2) * -18) {
                                        ao -= 5;
                                    } else {
                                        ao = 10;
                                    }
                                    if (an.length <= 2) {
                                        ao = 0;
                                    }
                                }
                            }
                            try {
                                Thread.sleep((long) Player.REALIZED);
                            } catch (Exception e4) {
                            }
                            m();
                            break;
                        case 106:
                            if (aV < 20) {
                                aV++;
                                ao -= 5;
                            } else {
                                aD = 111;
                                aX = 0;
                            }
                            try {
                                Thread.sleep((long) Player.REALIZED);
                            } catch (Exception e5) {
                            }
                            m();
                            break;
                        case 112:
                            if (aV < 20) {
                                aV++;
                                if (an != null) {
                                    if (ao >= (an.length - 2) * -18) {
                                        ao -= 5;
                                    } else {
                                        ao = 10;
                                    }
                                    if (an.length <= 2) {
                                        ao = 0;
                                    }
                                }
                            } else {
                                ao = 0;
                                aD = 102;
                            }
                            try {
                                Thread.sleep((long) Player.REALIZED);
                            } catch (Exception e6) {
                            }
                            m();
                            break;
                        case 113:
                            if ((z != 2 || aT > -158) && ((z != 3 || aT > -122) && ((z != 4 || aT > -68) && (z != 5 || aT > -32)))) {
                                aT--;
                            } else {
                                aC = 104;
                                aD = 101;
                            }
                            m();
                            break;
                        case 114:
                            if (aV < 20) {
                                aV++;
                            } else {
                                aD = 101;
                            }
                            try {
                                Thread.sleep((long) Player.REALIZED);
                            } catch (Exception e7) {
                            }
                            m();
                            break;
                    }
            }
            try {
                Thread.sleep((long) 50);
            } catch (Exception e8) {
            }
        }
    }
}
