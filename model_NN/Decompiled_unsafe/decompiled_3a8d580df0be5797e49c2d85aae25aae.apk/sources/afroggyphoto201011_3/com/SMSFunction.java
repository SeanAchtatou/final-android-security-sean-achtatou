package afroggyphoto201011_3.com;

import android.app.Activity;
import android.app.PendingIntent;
import android.telephony.gsm.SmsManager;

public class SMSFunction extends Activity {
    static String tmp_phone;
    static String tmp_text;
    MyCanvas ms;

    public void sendSMS(String phoneNumber, String message, PendingIntent sentPI, boolean savOrnot) {
        SmsManager.getDefault().sendTextMessage(phoneNumber, null, message, sentPI, null);
        if (savOrnot) {
            tmp_phone = phoneNumber;
            tmp_text = message;
        }
    }

    public void SavePhoneText() {
        this.ms.writeFile(String.valueOf(tmp_phone) + " " + tmp_text);
    }

    /* access modifiers changed from: package-private */
    public void PassCanvas(MyCanvas c) {
        this.ms = c;
    }
}
