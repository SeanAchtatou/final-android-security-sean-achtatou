package com.android.tencent.zdevs.bah;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

@TargetApi(11)
public class IMEIActivity extends Activity {
    private String imei = "";
    private TextView tvIMEI;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar ab = getActionBar();
        ab.setTitle("Show IMEI");
        ab.setDisplayShowHomeEnabled(false);
        ab.setBackgroundDrawable(new ColorDrawable(Color.rgb(53, 64, 109)));
        RelativeLayout rl = new RelativeLayout(this);
        setContentView(rl);
        TextView tvIMEI2 = new TextView(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-2, -2);
        params.addRule(13);
        this.imei = ((TelephonyManager) getSystemService("phone")).getDeviceId();
        tvIMEI2.setTextSize(0, 36.0f);
        tvIMEI2.setTextColor(Color.rgb(0, 0, 0));
        tvIMEI2.setText("The IMEI of this device is " + this.imei);
        tvIMEI2.setLayoutParams(params);
        rl.addView(tvIMEI2);
    }

    public void onBackPressed() {
        finish();
    }
}
