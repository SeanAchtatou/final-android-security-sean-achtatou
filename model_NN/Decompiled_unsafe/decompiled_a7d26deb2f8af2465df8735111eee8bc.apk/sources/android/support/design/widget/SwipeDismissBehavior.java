package android.support.design.widget;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class SwipeDismissBehavior<V extends View> extends CoordinatorLayout.Behavior<V> {
    private static final float DEFAULT_ALPHA_END_DISTANCE = 0.5f;
    private static final float DEFAULT_ALPHA_START_DISTANCE = 0.0f;
    private static final float DEFAULT_DRAG_DISMISS_THRESHOLD = 0.5f;
    public static final int STATE_DRAGGING = 1;
    public static final int STATE_IDLE = 0;
    public static final int STATE_SETTLING = 2;
    public static final int SWIPE_DIRECTION_ANY = 2;
    public static final int SWIPE_DIRECTION_END_TO_START = 1;
    public static final int SWIPE_DIRECTION_START_TO_END = 0;
    /* access modifiers changed from: private */
    public float mAlphaEndSwipeDistance = 0.5f;
    /* access modifiers changed from: private */
    public float mAlphaStartSwipeDistance = 0.0f;
    private final ViewDragHelper.Callback mDragCallback;
    /* access modifiers changed from: private */
    public float mDragDismissThreshold = 0.5f;
    private boolean mIgnoreEvents;
    /* access modifiers changed from: private */
    public OnDismissListener mListener;
    private float mSensitivity = 0.0f;
    private boolean mSensitivitySet;
    /* access modifiers changed from: private */
    public int mSwipeDirection = 2;
    /* access modifiers changed from: private */
    public ViewDragHelper mViewDragHelper;

    public interface OnDismissListener {
        void onDismiss(View view);

        void onDragStateChanged(int i);
    }

    public SwipeDismissBehavior() {
        ViewDragHelper.Callback callback;
        new ViewDragHelper.Callback() {
            private int mOriginalCapturedViewLeft;

            public boolean tryCaptureView(View view, int i) {
                return SwipeDismissBehavior.this.canSwipeDismissView(view);
            }

            public void onViewCaptured(View view, int i) {
                this.mOriginalCapturedViewLeft = view.getLeft();
            }

            public void onViewDragStateChanged(int i) {
                int i2 = i;
                if (SwipeDismissBehavior.this.mListener != null) {
                    SwipeDismissBehavior.this.mListener.onDragStateChanged(i2);
                }
            }

            public void onViewReleased(View view, float f, float f2) {
                int i;
                Runnable runnable;
                View view2 = view;
                int width = view2.getWidth();
                boolean z = false;
                if (shouldDismiss(view2, f)) {
                    i = view2.getLeft() < this.mOriginalCapturedViewLeft ? this.mOriginalCapturedViewLeft - width : this.mOriginalCapturedViewLeft + width;
                    z = true;
                } else {
                    i = this.mOriginalCapturedViewLeft;
                }
                if (SwipeDismissBehavior.this.mViewDragHelper.settleCapturedViewAt(i, view2.getTop())) {
                    new SettleRunnable(view2, z);
                    ViewCompat.postOnAnimation(view2, runnable);
                } else if (z && SwipeDismissBehavior.this.mListener != null) {
                    SwipeDismissBehavior.this.mListener.onDismiss(view2);
                }
            }

            private boolean shouldDismiss(View view, float f) {
                View view2 = view;
                float f2 = f;
                if (f2 != 0.0f) {
                    boolean z = ViewCompat.getLayoutDirection(view2) == 1;
                    if (SwipeDismissBehavior.this.mSwipeDirection == 2) {
                        return true;
                    }
                    if (SwipeDismissBehavior.this.mSwipeDirection == 0) {
                        return z ? f2 < 0.0f : f2 > 0.0f;
                    } else if (SwipeDismissBehavior.this.mSwipeDirection != 1) {
                        return false;
                    } else {
                        return z ? f2 > 0.0f : f2 < 0.0f;
                    }
                } else {
                    return Math.abs(view2.getLeft() - this.mOriginalCapturedViewLeft) >= Math.round(((float) view2.getWidth()) * SwipeDismissBehavior.this.mDragDismissThreshold);
                }
            }

            public int getViewHorizontalDragRange(View view) {
                return view.getWidth();
            }

            public int clampViewPositionHorizontal(View view, int i, int i2) {
                int width;
                int width2;
                View view2 = view;
                int i3 = i;
                boolean z = ViewCompat.getLayoutDirection(view2) == 1;
                if (SwipeDismissBehavior.this.mSwipeDirection == 0) {
                    if (z) {
                        width = this.mOriginalCapturedViewLeft - view2.getWidth();
                        width2 = this.mOriginalCapturedViewLeft;
                    } else {
                        width = this.mOriginalCapturedViewLeft;
                        width2 = this.mOriginalCapturedViewLeft + view2.getWidth();
                    }
                } else if (SwipeDismissBehavior.this.mSwipeDirection != 1) {
                    width = this.mOriginalCapturedViewLeft - view2.getWidth();
                    width2 = this.mOriginalCapturedViewLeft + view2.getWidth();
                } else if (z) {
                    width = this.mOriginalCapturedViewLeft;
                    width2 = this.mOriginalCapturedViewLeft + view2.getWidth();
                } else {
                    width = this.mOriginalCapturedViewLeft - view2.getWidth();
                    width2 = this.mOriginalCapturedViewLeft;
                }
                return SwipeDismissBehavior.clamp(width, i3, width2);
            }

            public int clampViewPositionVertical(View view, int i, int i2) {
                return view.getTop();
            }

            public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
                View view2 = view;
                int i5 = i;
                float width = ((float) this.mOriginalCapturedViewLeft) + (((float) view2.getWidth()) * SwipeDismissBehavior.this.mAlphaStartSwipeDistance);
                float width2 = ((float) this.mOriginalCapturedViewLeft) + (((float) view2.getWidth()) * SwipeDismissBehavior.this.mAlphaEndSwipeDistance);
                if (((float) i5) <= width) {
                    ViewCompat.setAlpha(view2, 1.0f);
                } else if (((float) i5) >= width2) {
                    ViewCompat.setAlpha(view2, 0.0f);
                } else {
                    ViewCompat.setAlpha(view2, SwipeDismissBehavior.clamp(0.0f, 1.0f - SwipeDismissBehavior.fraction(width, width2, (float) i5), 1.0f));
                }
            }
        };
        this.mDragCallback = callback;
    }

    public void setListener(OnDismissListener onDismissListener) {
        this.mListener = onDismissListener;
    }

    public void setSwipeDirection(int i) {
        this.mSwipeDirection = i;
    }

    public void setDragDismissDistance(float f) {
        this.mDragDismissThreshold = clamp(0.0f, f, 1.0f);
    }

    public void setStartAlphaSwipeDistance(float f) {
        this.mAlphaStartSwipeDistance = clamp(0.0f, f, 1.0f);
    }

    public void setEndAlphaSwipeDistance(float f) {
        this.mAlphaEndSwipeDistance = clamp(0.0f, f, 1.0f);
    }

    public void setSensitivity(float f) {
        this.mSensitivity = f;
        this.mSensitivitySet = true;
    }

    public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
        CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
        View view2 = view;
        MotionEvent motionEvent2 = motionEvent;
        switch (MotionEventCompat.getActionMasked(motionEvent2)) {
            case 1:
            case 3:
                if (this.mIgnoreEvents) {
                    this.mIgnoreEvents = false;
                    return false;
                }
                break;
            case 2:
            default:
                this.mIgnoreEvents = !coordinatorLayout2.isPointInChildBounds(view2, (int) motionEvent2.getX(), (int) motionEvent2.getY());
                break;
        }
        if (this.mIgnoreEvents) {
            return false;
        }
        ensureViewDragHelper(coordinatorLayout2);
        return this.mViewDragHelper.shouldInterceptTouchEvent(motionEvent2);
    }

    public boolean onTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        if (this.mViewDragHelper == null) {
            return false;
        }
        this.mViewDragHelper.processTouchEvent(motionEvent2);
        return true;
    }

    public boolean canSwipeDismissView(@NonNull View view) {
        return true;
    }

    private void ensureViewDragHelper(ViewGroup viewGroup) {
        ViewGroup viewGroup2 = viewGroup;
        if (this.mViewDragHelper == null) {
            this.mViewDragHelper = this.mSensitivitySet ? ViewDragHelper.create(viewGroup2, this.mSensitivity, this.mDragCallback) : ViewDragHelper.create(viewGroup2, this.mDragCallback);
        }
    }

    private class SettleRunnable implements Runnable {
        private final boolean mDismiss;
        private final View mView;

        SettleRunnable(View view, boolean z) {
            this.mView = view;
            this.mDismiss = z;
        }

        public void run() {
            if (SwipeDismissBehavior.this.mViewDragHelper != null && SwipeDismissBehavior.this.mViewDragHelper.continueSettling(true)) {
                ViewCompat.postOnAnimation(this.mView, this);
            } else if (this.mDismiss && SwipeDismissBehavior.this.mListener != null) {
                SwipeDismissBehavior.this.mListener.onDismiss(this.mView);
            }
        }
    }

    /* access modifiers changed from: private */
    public static float clamp(float f, float f2, float f3) {
        return Math.min(Math.max(f, f2), f3);
    }

    /* access modifiers changed from: private */
    public static int clamp(int i, int i2, int i3) {
        return Math.min(Math.max(i, i2), i3);
    }

    public int getDragState() {
        return this.mViewDragHelper != null ? this.mViewDragHelper.getViewDragState() : 0;
    }

    static float fraction(float f, float f2, float f3) {
        float f4 = f;
        return (f3 - f4) / (f2 - f4);
    }
}
