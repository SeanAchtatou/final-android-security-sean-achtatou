package android.support.design.widget;

import android.os.Build;
import android.support.design.widget.ValueAnimatorCompat;
import android.view.View;

class ViewUtils {
    static final ValueAnimatorCompat.Creator DEFAULT_ANIMATOR_CREATOR;
    private static final ViewUtilsImpl IMPL;

    private interface ViewUtilsImpl {
        void setBoundsViewOutlineProvider(View view);
    }

    ViewUtils() {
    }

    static {
        ValueAnimatorCompat.Creator creator;
        ViewUtilsImpl viewUtilsImpl;
        ViewUtilsImpl viewUtilsImpl2;
        new ValueAnimatorCompat.Creator() {
            public ValueAnimatorCompat createAnimator() {
                ValueAnimatorCompat valueAnimatorCompat;
                ValueAnimatorCompat.Impl impl;
                ValueAnimatorCompat.Impl impl2;
                ValueAnimatorCompat.Impl impl3;
                ValueAnimatorCompat valueAnimatorCompat2 = valueAnimatorCompat;
                if (Build.VERSION.SDK_INT >= 12) {
                    impl2 = impl3;
                    new ValueAnimatorCompatImplHoneycombMr1();
                } else {
                    impl2 = impl;
                    new ValueAnimatorCompatImplEclairMr1();
                }
                new ValueAnimatorCompat(impl2);
                return valueAnimatorCompat2;
            }
        };
        DEFAULT_ANIMATOR_CREATOR = creator;
        if (Build.VERSION.SDK_INT >= 21) {
            new ViewUtilsImplLollipop();
            IMPL = viewUtilsImpl2;
            return;
        }
        new ViewUtilsImplBase();
        IMPL = viewUtilsImpl;
    }

    private static class ViewUtilsImplBase implements ViewUtilsImpl {
        private ViewUtilsImplBase() {
        }

        public void setBoundsViewOutlineProvider(View view) {
        }
    }

    private static class ViewUtilsImplLollipop implements ViewUtilsImpl {
        private ViewUtilsImplLollipop() {
        }

        public void setBoundsViewOutlineProvider(View view) {
            ViewUtilsLollipop.setBoundsViewOutlineProvider(view);
        }
    }

    static void setBoundsViewOutlineProvider(View view) {
        IMPL.setBoundsViewOutlineProvider(view);
    }

    static ValueAnimatorCompat createAnimator() {
        return DEFAULT_ANIMATOR_CREATOR.createAnimator();
    }
}
