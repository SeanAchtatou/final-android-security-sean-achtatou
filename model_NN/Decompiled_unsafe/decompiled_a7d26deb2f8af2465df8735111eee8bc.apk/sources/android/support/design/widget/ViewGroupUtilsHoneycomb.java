package android.support.design.widget;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

class ViewGroupUtilsHoneycomb {
    private static final Matrix IDENTITY;
    private static final ThreadLocal<Matrix> sMatrix;
    private static final ThreadLocal<RectF> sRectF;

    ViewGroupUtilsHoneycomb() {
    }

    static {
        ThreadLocal<Matrix> threadLocal;
        ThreadLocal<RectF> threadLocal2;
        Matrix matrix;
        new ThreadLocal<>();
        sMatrix = threadLocal;
        new ThreadLocal<>();
        sRectF = threadLocal2;
        new Matrix();
        IDENTITY = matrix;
    }

    public static void offsetDescendantRect(ViewGroup viewGroup, View view, Rect rect) {
        RectF rectF;
        Matrix matrix;
        ViewGroup viewGroup2 = viewGroup;
        View view2 = view;
        Rect rect2 = rect;
        Matrix matrix2 = sMatrix.get();
        if (matrix2 == null) {
            new Matrix();
            matrix2 = matrix;
            sMatrix.set(matrix2);
        } else {
            matrix2.set(IDENTITY);
        }
        offsetDescendantMatrix(viewGroup2, view2, matrix2);
        RectF rectF2 = sRectF.get();
        if (rectF2 == null) {
            new RectF();
            rectF2 = rectF;
        }
        rectF2.set(rect2);
        boolean mapRect = matrix2.mapRect(rectF2);
        rect2.set((int) (rectF2.left + 0.5f), (int) (rectF2.top + 0.5f), (int) (rectF2.right + 0.5f), (int) (rectF2.bottom + 0.5f));
    }

    static void offsetDescendantMatrix(ViewParent viewParent, View view, Matrix matrix) {
        ViewParent viewParent2 = viewParent;
        View view2 = view;
        Matrix matrix2 = matrix;
        ViewParent parent = view2.getParent();
        if ((parent instanceof View) && parent != viewParent2) {
            View view3 = (View) parent;
            offsetDescendantMatrix(viewParent2, view3, matrix2);
            boolean preTranslate = matrix2.preTranslate((float) (-view3.getScrollX()), (float) (-view3.getScrollY()));
        }
        boolean preTranslate2 = matrix2.preTranslate((float) view2.getLeft(), (float) view2.getTop());
        if (!view2.getMatrix().isIdentity()) {
            boolean preConcat = matrix2.preConcat(view2.getMatrix());
        }
    }
}
