package android.support.design.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.design.R;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.ValueAnimatorCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;

public class CollapsingToolbarLayout extends FrameLayout {
    private static final int SCRIM_ANIMATION_DURATION = 600;
    /* access modifiers changed from: private */
    public final CollapsingTextHelper mCollapsingTextHelper;
    private boolean mCollapsingTitleEnabled;
    /* access modifiers changed from: private */
    public Drawable mContentScrim;
    private int mCurrentOffset;
    private boolean mDrawCollapsingTitle;
    private View mDummyView;
    private int mExpandedMarginBottom;
    private int mExpandedMarginLeft;
    private int mExpandedMarginRight;
    private int mExpandedMarginTop;
    /* access modifiers changed from: private */
    public WindowInsetsCompat mLastInsets;
    private AppBarLayout.OnOffsetChangedListener mOnOffsetChangedListener;
    private boolean mRefreshToolbar;
    private int mScrimAlpha;
    private ValueAnimatorCompat mScrimAnimator;
    private boolean mScrimsAreShown;
    /* access modifiers changed from: private */
    public Drawable mStatusBarScrim;
    private final Rect mTmpRect;
    private Toolbar mToolbar;
    private int mToolbarId;

    static /* synthetic */ WindowInsetsCompat access$002(CollapsingToolbarLayout collapsingToolbarLayout, WindowInsetsCompat windowInsetsCompat) {
        WindowInsetsCompat windowInsetsCompat2 = windowInsetsCompat;
        WindowInsetsCompat windowInsetsCompat3 = windowInsetsCompat2;
        collapsingToolbarLayout.mLastInsets = windowInsetsCompat3;
        return windowInsetsCompat2;
    }

    static /* synthetic */ int access$302(CollapsingToolbarLayout collapsingToolbarLayout, int i) {
        int i2 = i;
        int i3 = i2;
        collapsingToolbarLayout.mCurrentOffset = i3;
        return i2;
    }

    public CollapsingToolbarLayout(Context context) {
        this(context, null);
    }

    public CollapsingToolbarLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CollapsingToolbarLayout(android.content.Context r17, android.util.AttributeSet r18, int r19) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            r3 = r19
            r7 = r0
            r8 = r1
            r9 = r2
            r10 = r3
            r7.<init>(r8, r9, r10)
            r7 = r0
            r8 = 1
            r7.mRefreshToolbar = r8
            r7 = r0
            android.graphics.Rect r8 = new android.graphics.Rect
            r14 = r8
            r8 = r14
            r9 = r14
            r9.<init>()
            r7.mTmpRect = r8
            r7 = r1
            android.support.design.widget.ThemeUtils.checkAppCompatTheme(r7)
            r7 = r0
            android.support.design.widget.CollapsingTextHelper r8 = new android.support.design.widget.CollapsingTextHelper
            r14 = r8
            r8 = r14
            r9 = r14
            r10 = r0
            r9.<init>(r10)
            r7.mCollapsingTextHelper = r8
            r7 = r0
            android.support.design.widget.CollapsingTextHelper r7 = r7.mCollapsingTextHelper
            android.view.animation.Interpolator r8 = android.support.design.widget.AnimationUtils.DECELERATE_INTERPOLATOR
            r7.setTextSizeInterpolator(r8)
            r7 = r1
            r8 = r2
            int[] r9 = android.support.design.R.styleable.CollapsingToolbarLayout
            r10 = r3
            int r11 = android.support.design.R.style.Widget_Design_CollapsingToolbar
            android.content.res.TypedArray r7 = r7.obtainStyledAttributes(r8, r9, r10, r11)
            r4 = r7
            r7 = r0
            android.support.design.widget.CollapsingTextHelper r7 = r7.mCollapsingTextHelper
            r8 = r4
            int r9 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleGravity
            r10 = 8388691(0x800053, float:1.175506E-38)
            int r8 = r8.getInt(r9, r10)
            r7.setExpandedTextGravity(r8)
            r7 = r0
            android.support.design.widget.CollapsingTextHelper r7 = r7.mCollapsingTextHelper
            r8 = r4
            int r9 = android.support.design.R.styleable.CollapsingToolbarLayout_collapsedTitleGravity
            r10 = 8388627(0x800013, float:1.175497E-38)
            int r8 = r8.getInt(r9, r10)
            r7.setCollapsedTextGravity(r8)
            r7 = r0
            r8 = r0
            r9 = r0
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleMargin
            r13 = 0
            int r11 = r11.getDimensionPixelSize(r12, r13)
            r14 = r10
            r15 = r11
            r10 = r15
            r11 = r14
            r12 = r15
            r11.mExpandedMarginBottom = r12
            r14 = r9
            r15 = r10
            r9 = r15
            r10 = r14
            r11 = r15
            r10.mExpandedMarginRight = r11
            r14 = r8
            r15 = r9
            r8 = r15
            r9 = r14
            r10 = r15
            r9.mExpandedMarginTop = r10
            r7.mExpandedMarginLeft = r8
            r7 = r0
            int r7 = android.support.v4.view.ViewCompat.getLayoutDirection(r7)
            r8 = 1
            if (r7 != r8) goto L_0x0175
            r7 = 1
        L_0x008e:
            r5 = r7
            r7 = r4
            int r8 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleMarginStart
            boolean r7 = r7.hasValue(r8)
            if (r7 == 0) goto L_0x00a8
            r7 = r4
            int r8 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleMarginStart
            r9 = 0
            int r7 = r7.getDimensionPixelSize(r8, r9)
            r6 = r7
            r7 = r5
            if (r7 == 0) goto L_0x0178
            r7 = r0
            r8 = r6
            r7.mExpandedMarginRight = r8
        L_0x00a8:
            r7 = r4
            int r8 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleMarginEnd
            boolean r7 = r7.hasValue(r8)
            if (r7 == 0) goto L_0x00c1
            r7 = r4
            int r8 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleMarginEnd
            r9 = 0
            int r7 = r7.getDimensionPixelSize(r8, r9)
            r6 = r7
            r7 = r5
            if (r7 == 0) goto L_0x017e
            r7 = r0
            r8 = r6
            r7.mExpandedMarginLeft = r8
        L_0x00c1:
            r7 = r4
            int r8 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleMarginTop
            boolean r7 = r7.hasValue(r8)
            if (r7 == 0) goto L_0x00d5
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleMarginTop
            r10 = 0
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.mExpandedMarginTop = r8
        L_0x00d5:
            r7 = r4
            int r8 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleMarginBottom
            boolean r7 = r7.hasValue(r8)
            if (r7 == 0) goto L_0x00e9
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleMarginBottom
            r10 = 0
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.mExpandedMarginBottom = r8
        L_0x00e9:
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.CollapsingToolbarLayout_titleEnabled
            r10 = 1
            boolean r8 = r8.getBoolean(r9, r10)
            r7.mCollapsingTitleEnabled = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.CollapsingToolbarLayout_title
            java.lang.CharSequence r8 = r8.getText(r9)
            r7.setTitle(r8)
            r7 = r0
            android.support.design.widget.CollapsingTextHelper r7 = r7.mCollapsingTextHelper
            int r8 = android.support.design.R.style.TextAppearance_Design_CollapsingToolbar_Expanded
            r7.setExpandedTextAppearance(r8)
            r7 = r0
            android.support.design.widget.CollapsingTextHelper r7 = r7.mCollapsingTextHelper
            int r8 = android.support.design.R.style.TextAppearance_AppCompat_Widget_ActionBar_Title
            r7.setCollapsedTextAppearance(r8)
            r7 = r4
            int r8 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleTextAppearance
            boolean r7 = r7.hasValue(r8)
            if (r7 == 0) goto L_0x0126
            r7 = r0
            android.support.design.widget.CollapsingTextHelper r7 = r7.mCollapsingTextHelper
            r8 = r4
            int r9 = android.support.design.R.styleable.CollapsingToolbarLayout_expandedTitleTextAppearance
            r10 = 0
            int r8 = r8.getResourceId(r9, r10)
            r7.setExpandedTextAppearance(r8)
        L_0x0126:
            r7 = r4
            int r8 = android.support.design.R.styleable.CollapsingToolbarLayout_collapsedTitleTextAppearance
            boolean r7 = r7.hasValue(r8)
            if (r7 == 0) goto L_0x013d
            r7 = r0
            android.support.design.widget.CollapsingTextHelper r7 = r7.mCollapsingTextHelper
            r8 = r4
            int r9 = android.support.design.R.styleable.CollapsingToolbarLayout_collapsedTitleTextAppearance
            r10 = 0
            int r8 = r8.getResourceId(r9, r10)
            r7.setCollapsedTextAppearance(r8)
        L_0x013d:
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.CollapsingToolbarLayout_contentScrim
            android.graphics.drawable.Drawable r8 = r8.getDrawable(r9)
            r7.setContentScrim(r8)
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.CollapsingToolbarLayout_statusBarScrim
            android.graphics.drawable.Drawable r8 = r8.getDrawable(r9)
            r7.setStatusBarScrim(r8)
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.CollapsingToolbarLayout_toolbarId
            r10 = -1
            int r8 = r8.getResourceId(r9, r10)
            r7.mToolbarId = r8
            r7 = r4
            r7.recycle()
            r7 = r0
            r8 = 0
            r7.setWillNotDraw(r8)
            r7 = r0
            android.support.design.widget.CollapsingToolbarLayout$1 r8 = new android.support.design.widget.CollapsingToolbarLayout$1
            r14 = r8
            r8 = r14
            r9 = r14
            r10 = r0
            r9.<init>()
            android.support.v4.view.ViewCompat.setOnApplyWindowInsetsListener(r7, r8)
            return
        L_0x0175:
            r7 = 0
            goto L_0x008e
        L_0x0178:
            r7 = r0
            r8 = r6
            r7.mExpandedMarginLeft = r8
            goto L_0x00a8
        L_0x017e:
            r7 = r0
            r8 = r6
            r7.mExpandedMarginRight = r8
            goto L_0x00c1
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CollapsingToolbarLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        AppBarLayout.OnOffsetChangedListener onOffsetChangedListener;
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (parent instanceof AppBarLayout) {
            if (this.mOnOffsetChangedListener == null) {
                new OffsetUpdateListener();
                this.mOnOffsetChangedListener = onOffsetChangedListener;
            }
            ((AppBarLayout) parent).addOnOffsetChangedListener(this.mOnOffsetChangedListener);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        ViewParent parent = getParent();
        if (this.mOnOffsetChangedListener != null && (parent instanceof AppBarLayout)) {
            ((AppBarLayout) parent).removeOnOffsetChangedListener(this.mOnOffsetChangedListener);
        }
        super.onDetachedFromWindow();
    }

    public void draw(Canvas canvas) {
        Canvas canvas2 = canvas;
        super.draw(canvas2);
        ensureToolbar();
        if (this.mToolbar == null && this.mContentScrim != null && this.mScrimAlpha > 0) {
            this.mContentScrim.mutate().setAlpha(this.mScrimAlpha);
            this.mContentScrim.draw(canvas2);
        }
        if (this.mCollapsingTitleEnabled && this.mDrawCollapsingTitle) {
            this.mCollapsingTextHelper.draw(canvas2);
        }
        if (this.mStatusBarScrim != null && this.mScrimAlpha > 0) {
            int systemWindowInsetTop = this.mLastInsets != null ? this.mLastInsets.getSystemWindowInsetTop() : 0;
            if (systemWindowInsetTop > 0) {
                this.mStatusBarScrim.setBounds(0, -this.mCurrentOffset, getWidth(), systemWindowInsetTop - this.mCurrentOffset);
                this.mStatusBarScrim.mutate().setAlpha(this.mScrimAlpha);
                this.mStatusBarScrim.draw(canvas2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        Canvas canvas2 = canvas;
        View view2 = view;
        long j2 = j;
        ensureToolbar();
        if (view2 == this.mToolbar && this.mContentScrim != null && this.mScrimAlpha > 0) {
            this.mContentScrim.mutate().setAlpha(this.mScrimAlpha);
            this.mContentScrim.draw(canvas2);
        }
        return super.drawChild(canvas2, view2, j2);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        int i5 = i;
        int i6 = i2;
        super.onSizeChanged(i5, i6, i3, i4);
        if (this.mContentScrim != null) {
            this.mContentScrim.setBounds(0, 0, i5, i6);
        }
    }

    private void ensureToolbar() {
        if (this.mRefreshToolbar) {
            Toolbar toolbar = null;
            Toolbar toolbar2 = null;
            int i = 0;
            int childCount = getChildCount();
            while (true) {
                if (i >= childCount) {
                    break;
                }
                View childAt = getChildAt(i);
                if (childAt instanceof Toolbar) {
                    if (this.mToolbarId == -1) {
                        toolbar2 = (Toolbar) childAt;
                        break;
                    } else if (this.mToolbarId == childAt.getId()) {
                        toolbar2 = (Toolbar) childAt;
                        break;
                    } else if (toolbar == null) {
                        toolbar = (Toolbar) childAt;
                    }
                }
                i++;
            }
            if (toolbar2 == null) {
                toolbar2 = toolbar;
            }
            this.mToolbar = toolbar2;
            updateDummyView();
            this.mRefreshToolbar = false;
        }
    }

    private void updateDummyView() {
        View view;
        if (!this.mCollapsingTitleEnabled && this.mDummyView != null) {
            ViewParent parent = this.mDummyView.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.mDummyView);
            }
        }
        if (this.mCollapsingTitleEnabled && this.mToolbar != null) {
            if (this.mDummyView == null) {
                new View(getContext());
                this.mDummyView = view;
            }
            if (this.mDummyView.getParent() == null) {
                this.mToolbar.addView(this.mDummyView, -1, -1);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        ensureToolbar();
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int systemWindowInsetTop;
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        super.onLayout(z, i5, i6, i7, i8);
        if (this.mCollapsingTitleEnabled && this.mDummyView != null) {
            this.mDrawCollapsingTitle = this.mDummyView.isShown();
            if (this.mDrawCollapsingTitle) {
                ViewGroupUtils.getDescendantRect(this, this.mDummyView, this.mTmpRect);
                this.mCollapsingTextHelper.setCollapsedBounds(this.mTmpRect.left, i8 - this.mTmpRect.height(), this.mTmpRect.right, i8);
                this.mCollapsingTextHelper.setExpandedBounds(this.mExpandedMarginLeft, this.mTmpRect.bottom + this.mExpandedMarginTop, (i7 - i5) - this.mExpandedMarginRight, (i8 - i6) - this.mExpandedMarginBottom);
                this.mCollapsingTextHelper.recalculate();
            }
        }
        int childCount = getChildCount();
        for (int i9 = 0; i9 < childCount; i9++) {
            View childAt = getChildAt(i9);
            if (this.mLastInsets != null && !ViewCompat.getFitsSystemWindows(childAt) && childAt.getTop() < (systemWindowInsetTop = this.mLastInsets.getSystemWindowInsetTop())) {
                childAt.offsetTopAndBottom(systemWindowInsetTop);
            }
            getViewOffsetHelper(childAt).onViewLayout();
        }
        if (this.mToolbar != null) {
            if (this.mCollapsingTitleEnabled && TextUtils.isEmpty(this.mCollapsingTextHelper.getText())) {
                this.mCollapsingTextHelper.setText(this.mToolbar.getTitle());
            }
            setMinimumHeight(this.mToolbar.getHeight());
        }
    }

    /* access modifiers changed from: private */
    public static ViewOffsetHelper getViewOffsetHelper(View view) {
        ViewOffsetHelper viewOffsetHelper;
        View view2 = view;
        ViewOffsetHelper viewOffsetHelper2 = (ViewOffsetHelper) view2.getTag(R.id.view_offset_helper);
        if (viewOffsetHelper2 == null) {
            new ViewOffsetHelper(view2);
            viewOffsetHelper2 = viewOffsetHelper;
            view2.setTag(R.id.view_offset_helper, viewOffsetHelper2);
        }
        return viewOffsetHelper2;
    }

    public void setTitle(@Nullable CharSequence charSequence) {
        this.mCollapsingTextHelper.setText(charSequence);
    }

    @Nullable
    public CharSequence getTitle() {
        return this.mCollapsingTitleEnabled ? this.mCollapsingTextHelper.getText() : null;
    }

    public void setTitleEnabled(boolean z) {
        boolean z2 = z;
        if (z2 != this.mCollapsingTitleEnabled) {
            this.mCollapsingTitleEnabled = z2;
            updateDummyView();
            requestLayout();
        }
    }

    public boolean isTitleEnabled() {
        return this.mCollapsingTitleEnabled;
    }

    public void setScrimsShown(boolean z) {
        setScrimsShown(z, ViewCompat.isLaidOut(this) && !isInEditMode());
    }

    public void setScrimsShown(boolean z, boolean z2) {
        boolean z3 = z;
        boolean z4 = z2;
        if (this.mScrimsAreShown != z3) {
            if (z4) {
                animateScrim(z3 ? 255 : 0);
            } else {
                setScrimAlpha(z3 ? 255 : 0);
            }
            this.mScrimsAreShown = z3;
        }
    }

    private void animateScrim(int i) {
        ValueAnimatorCompat.AnimatorUpdateListener animatorUpdateListener;
        int i2 = i;
        ensureToolbar();
        if (this.mScrimAnimator == null) {
            this.mScrimAnimator = ViewUtils.createAnimator();
            this.mScrimAnimator.setDuration(SCRIM_ANIMATION_DURATION);
            this.mScrimAnimator.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
            new ValueAnimatorCompat.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimatorCompat valueAnimatorCompat) {
                    CollapsingToolbarLayout.this.setScrimAlpha(valueAnimatorCompat.getAnimatedIntValue());
                }
            };
            this.mScrimAnimator.setUpdateListener(animatorUpdateListener);
        } else if (this.mScrimAnimator.isRunning()) {
            this.mScrimAnimator.cancel();
        }
        this.mScrimAnimator.setIntValues(this.mScrimAlpha, i2);
        this.mScrimAnimator.start();
    }

    /* access modifiers changed from: private */
    public void setScrimAlpha(int i) {
        int i2 = i;
        if (i2 != this.mScrimAlpha) {
            if (!(this.mContentScrim == null || this.mToolbar == null)) {
                ViewCompat.postInvalidateOnAnimation(this.mToolbar);
            }
            this.mScrimAlpha = i2;
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public void setContentScrim(@Nullable Drawable drawable) {
        Drawable drawable2 = drawable;
        if (this.mContentScrim != drawable2) {
            if (this.mContentScrim != null) {
                this.mContentScrim.setCallback(null);
            }
            if (drawable2 != null) {
                this.mContentScrim = drawable2.mutate();
                drawable2.setBounds(0, 0, getWidth(), getHeight());
                drawable2.setCallback(this);
                drawable2.setAlpha(this.mScrimAlpha);
            } else {
                this.mContentScrim = null;
            }
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public void setContentScrimColor(@ColorInt int i) {
        Drawable drawable;
        new ColorDrawable(i);
        setContentScrim(drawable);
    }

    public void setContentScrimResource(@DrawableRes int i) {
        setContentScrim(ContextCompat.getDrawable(getContext(), i));
    }

    public Drawable getContentScrim() {
        return this.mContentScrim;
    }

    public void setStatusBarScrim(@Nullable Drawable drawable) {
        Drawable drawable2 = drawable;
        if (this.mStatusBarScrim != drawable2) {
            if (this.mStatusBarScrim != null) {
                this.mStatusBarScrim.setCallback(null);
            }
            this.mStatusBarScrim = drawable2;
            drawable2.setCallback(this);
            drawable2.mutate().setAlpha(this.mScrimAlpha);
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public void setStatusBarScrimColor(@ColorInt int i) {
        Drawable drawable;
        new ColorDrawable(i);
        setStatusBarScrim(drawable);
    }

    public void setStatusBarScrimResource(@DrawableRes int i) {
        setStatusBarScrim(ContextCompat.getDrawable(getContext(), i));
    }

    public Drawable getStatusBarScrim() {
        return this.mStatusBarScrim;
    }

    public void setCollapsedTitleTextAppearance(@StyleRes int i) {
        this.mCollapsingTextHelper.setCollapsedTextAppearance(i);
    }

    public void setCollapsedTitleTextColor(@ColorInt int i) {
        this.mCollapsingTextHelper.setCollapsedTextColor(i);
    }

    public void setCollapsedTitleGravity(int i) {
        this.mCollapsingTextHelper.setCollapsedTextGravity(i);
    }

    public int getCollapsedTitleGravity() {
        return this.mCollapsingTextHelper.getCollapsedTextGravity();
    }

    public void setExpandedTitleTextAppearance(@StyleRes int i) {
        this.mCollapsingTextHelper.setExpandedTextAppearance(i);
    }

    public void setExpandedTitleColor(@ColorInt int i) {
        this.mCollapsingTextHelper.setExpandedTextColor(i);
    }

    public void setExpandedTitleGravity(int i) {
        this.mCollapsingTextHelper.setExpandedTextGravity(i);
    }

    public int getExpandedTitleGravity() {
        return this.mCollapsingTextHelper.getExpandedTextGravity();
    }

    public void setCollapsedTitleTypeface(@Nullable Typeface typeface) {
        this.mCollapsingTextHelper.setCollapsedTypeface(typeface);
    }

    @NonNull
    public Typeface getCollapsedTitleTypeface() {
        return this.mCollapsingTextHelper.getCollapsedTypeface();
    }

    public void setExpandedTitleTypeface(@Nullable Typeface typeface) {
        this.mCollapsingTextHelper.setExpandedTypeface(typeface);
    }

    @NonNull
    public Typeface getExpandedTitleTypeface() {
        return this.mCollapsingTextHelper.getExpandedTypeface();
    }

    /* access modifiers changed from: package-private */
    public final int getScrimTriggerOffset() {
        return 2 * ViewCompat.getMinimumHeight(this);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        LayoutParams layoutParams;
        new LayoutParams(super.generateDefaultLayoutParams());
        return layoutParams;
    }

    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        FrameLayout.LayoutParams layoutParams;
        new LayoutParams(getContext(), attributeSet);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public FrameLayout.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        FrameLayout.LayoutParams layoutParams2;
        new LayoutParams(layoutParams);
        return layoutParams2;
    }

    public static class LayoutParams extends FrameLayout.LayoutParams {
        public static final int COLLAPSE_MODE_OFF = 0;
        public static final int COLLAPSE_MODE_PARALLAX = 2;
        public static final int COLLAPSE_MODE_PIN = 1;
        private static final float DEFAULT_PARALLAX_MULTIPLIER = 0.5f;
        int mCollapseMode = 0;
        float mParallaxMult = DEFAULT_PARALLAX_MULTIPLIER;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public LayoutParams(android.content.Context r9, android.util.AttributeSet r10) {
            /*
                r8 = this;
                r0 = r8
                r1 = r9
                r2 = r10
                r4 = r0
                r5 = r1
                r6 = r2
                r4.<init>(r5, r6)
                r4 = r0
                r5 = 0
                r4.mCollapseMode = r5
                r4 = r0
                r5 = 1056964608(0x3f000000, float:0.5)
                r4.mParallaxMult = r5
                r4 = r1
                r5 = r2
                int[] r6 = android.support.design.R.styleable.CollapsingAppBarLayout_LayoutParams
                android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r6)
                r3 = r4
                r4 = r0
                r5 = r3
                int r6 = android.support.design.R.styleable.CollapsingAppBarLayout_LayoutParams_layout_collapseMode
                r7 = 0
                int r5 = r5.getInt(r6, r7)
                r4.mCollapseMode = r5
                r4 = r0
                r5 = r3
                int r6 = android.support.design.R.styleable.CollapsingAppBarLayout_LayoutParams_layout_collapseParallaxMultiplier
                r7 = 1056964608(0x3f000000, float:0.5)
                float r5 = r5.getFloat(r6, r7)
                r4.setParallaxMultiplier(r5)
                r4 = r3
                r4.recycle()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CollapsingToolbarLayout.LayoutParams.<init>(android.content.Context, android.util.AttributeSet):void");
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(int i, int i2, int i3) {
            super(i, i2, i3);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(FrameLayout.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public void setCollapseMode(int i) {
            this.mCollapseMode = i;
        }

        public int getCollapseMode() {
            return this.mCollapseMode;
        }

        public void setParallaxMultiplier(float f) {
            this.mParallaxMult = f;
        }

        public float getParallaxMultiplier() {
            return this.mParallaxMult;
        }
    }

    private class OffsetUpdateListener implements AppBarLayout.OnOffsetChangedListener {
        private OffsetUpdateListener() {
        }

        public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
            AppBarLayout appBarLayout2 = appBarLayout;
            int i2 = i;
            int access$302 = CollapsingToolbarLayout.access$302(CollapsingToolbarLayout.this, i2);
            int systemWindowInsetTop = CollapsingToolbarLayout.this.mLastInsets != null ? CollapsingToolbarLayout.this.mLastInsets.getSystemWindowInsetTop() : 0;
            int totalScrollRange = appBarLayout2.getTotalScrollRange();
            int childCount = CollapsingToolbarLayout.this.getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = CollapsingToolbarLayout.this.getChildAt(i3);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                ViewOffsetHelper access$400 = CollapsingToolbarLayout.getViewOffsetHelper(childAt);
                switch (layoutParams.mCollapseMode) {
                    case 1:
                        if ((CollapsingToolbarLayout.this.getHeight() - systemWindowInsetTop) + i2 < childAt.getHeight()) {
                            break;
                        } else {
                            boolean topAndBottomOffset = access$400.setTopAndBottomOffset(-i2);
                            break;
                        }
                    case 2:
                        boolean topAndBottomOffset2 = access$400.setTopAndBottomOffset(Math.round(((float) (-i2)) * layoutParams.mParallaxMult));
                        break;
                }
            }
            if (!(CollapsingToolbarLayout.this.mContentScrim == null && CollapsingToolbarLayout.this.mStatusBarScrim == null)) {
                CollapsingToolbarLayout.this.setScrimsShown(CollapsingToolbarLayout.this.getHeight() + i2 < CollapsingToolbarLayout.this.getScrimTriggerOffset() + systemWindowInsetTop);
            }
            if (CollapsingToolbarLayout.this.mStatusBarScrim != null && systemWindowInsetTop > 0) {
                ViewCompat.postInvalidateOnAnimation(CollapsingToolbarLayout.this);
            }
            CollapsingToolbarLayout.this.mCollapsingTextHelper.setExpansionFraction(((float) Math.abs(i2)) / ((float) ((CollapsingToolbarLayout.this.getHeight() - ViewCompat.getMinimumHeight(CollapsingToolbarLayout.this)) - systemWindowInsetTop)));
            if (Math.abs(i2) == totalScrollRange) {
                ViewCompat.setElevation(appBarLayout2, appBarLayout2.getTargetElevation());
            } else {
                ViewCompat.setElevation(appBarLayout2, 0.0f);
            }
        }
    }
}
