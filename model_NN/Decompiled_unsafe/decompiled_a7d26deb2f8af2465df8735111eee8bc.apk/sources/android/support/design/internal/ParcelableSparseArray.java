package android.support.design.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.util.SparseArray;

public class ParcelableSparseArray extends SparseArray<Parcelable> implements Parcelable {
    public static final Parcelable.Creator<ParcelableSparseArray> CREATOR;

    public ParcelableSparseArray() {
    }

    public ParcelableSparseArray(Parcel parcel, ClassLoader classLoader) {
        Parcel parcel2 = parcel;
        int readInt = parcel2.readInt();
        int[] iArr = new int[readInt];
        parcel2.readIntArray(iArr);
        Parcelable[] readParcelableArray = parcel2.readParcelableArray(classLoader);
        for (int i = 0; i < readInt; i++) {
            put(iArr[i], readParcelableArray[i]);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        Parcel parcel2 = parcel;
        int i2 = i;
        int size = size();
        int[] iArr = new int[size];
        Parcelable[] parcelableArr = new Parcelable[size];
        for (int i3 = 0; i3 < size; i3++) {
            iArr[i3] = keyAt(i3);
            parcelableArr[i3] = (Parcelable) valueAt(i3);
        }
        parcel2.writeInt(size);
        parcel2.writeIntArray(iArr);
        parcel2.writeParcelableArray(parcelableArr, i2);
    }

    static {
        Object obj;
        new ParcelableCompatCreatorCallbacks<ParcelableSparseArray>() {
            public ParcelableSparseArray createFromParcel(Parcel parcel, ClassLoader classLoader) {
                ParcelableSparseArray parcelableSparseArray;
                new ParcelableSparseArray(parcel, classLoader);
                return parcelableSparseArray;
            }

            public ParcelableSparseArray[] newArray(int i) {
                return new ParcelableSparseArray[i];
            }
        };
        CREATOR = ParcelableCompat.newCreator(obj);
    }
}
