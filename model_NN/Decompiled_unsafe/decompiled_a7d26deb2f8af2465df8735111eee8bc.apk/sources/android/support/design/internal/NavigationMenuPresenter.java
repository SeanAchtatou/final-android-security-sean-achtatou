package android.support.design.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.design.R;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;

public class NavigationMenuPresenter implements MenuPresenter {
    private static final String STATE_ADAPTER = "android:menu:adapter";
    private static final String STATE_HIERARCHY = "android:menu:list";
    /* access modifiers changed from: private */
    public NavigationMenuAdapter mAdapter;
    private MenuPresenter.Callback mCallback;
    /* access modifiers changed from: private */
    public LinearLayout mHeaderLayout;
    /* access modifiers changed from: private */
    public ColorStateList mIconTintList;
    private int mId;
    /* access modifiers changed from: private */
    public Drawable mItemBackground;
    /* access modifiers changed from: private */
    public LayoutInflater mLayoutInflater;
    /* access modifiers changed from: private */
    public MenuBuilder mMenu;
    private NavigationMenuView mMenuView;
    /* access modifiers changed from: private */
    public final View.OnClickListener mOnClickListener;
    /* access modifiers changed from: private */
    public int mPaddingSeparator;
    private int mPaddingTopDefault;
    /* access modifiers changed from: private */
    public int mTextAppearance;
    /* access modifiers changed from: private */
    public boolean mTextAppearanceSet;
    /* access modifiers changed from: private */
    public ColorStateList mTextColor;

    private interface NavigationMenuItem {
    }

    public NavigationMenuPresenter() {
        View.OnClickListener onClickListener;
        new View.OnClickListener() {
            public void onClick(View view) {
                NavigationMenuPresenter.this.setUpdateSuspended(true);
                MenuItemImpl itemData = ((NavigationMenuItemView) view).getItemData();
                boolean performItemAction = NavigationMenuPresenter.this.mMenu.performItemAction(itemData, NavigationMenuPresenter.this, 0);
                if (itemData != null && itemData.isCheckable() && performItemAction) {
                    NavigationMenuPresenter.this.mAdapter.setCheckedItem(itemData);
                }
                NavigationMenuPresenter.this.setUpdateSuspended(false);
                NavigationMenuPresenter.this.updateMenuView(false);
            }
        };
        this.mOnClickListener = onClickListener;
    }

    public void initForMenu(Context context, MenuBuilder menuBuilder) {
        Context context2 = context;
        this.mLayoutInflater = LayoutInflater.from(context2);
        this.mMenu = menuBuilder;
        Resources resources = context2.getResources();
        this.mPaddingTopDefault = resources.getDimensionPixelOffset(R.dimen.design_navigation_padding_top_default);
        this.mPaddingSeparator = resources.getDimensionPixelOffset(R.dimen.design_navigation_separator_vertical_padding);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.design.internal.NavigationMenuView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public MenuView getMenuView(ViewGroup viewGroup) {
        NavigationMenuAdapter navigationMenuAdapter;
        ViewGroup viewGroup2 = viewGroup;
        if (this.mMenuView == null) {
            this.mMenuView = (NavigationMenuView) this.mLayoutInflater.inflate(R.layout.design_navigation_menu, viewGroup2, false);
            if (this.mAdapter == null) {
                new NavigationMenuAdapter();
                this.mAdapter = navigationMenuAdapter;
            }
            this.mHeaderLayout = (LinearLayout) this.mLayoutInflater.inflate(R.layout.design_navigation_item_header, (ViewGroup) this.mMenuView, false);
            this.mMenuView.setAdapter(this.mAdapter);
        }
        return this.mMenuView;
    }

    public void updateMenuView(boolean z) {
        if (this.mAdapter != null) {
            this.mAdapter.update();
        }
    }

    public void setCallback(MenuPresenter.Callback callback) {
        this.mCallback = callback;
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        return false;
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        MenuBuilder menuBuilder2 = menuBuilder;
        boolean z2 = z;
        if (this.mCallback != null) {
            this.mCallback.onCloseMenu(menuBuilder2, z2);
        }
    }

    public boolean flagActionItems() {
        return false;
    }

    public boolean expandItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean collapseItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public int getId() {
        return this.mId;
    }

    public void setId(int i) {
        this.mId = i;
    }

    public Parcelable onSaveInstanceState() {
        Bundle bundle;
        SparseArray sparseArray;
        new Bundle();
        Bundle bundle2 = bundle;
        if (this.mMenuView != null) {
            new SparseArray();
            SparseArray sparseArray2 = sparseArray;
            this.mMenuView.saveHierarchyState(sparseArray2);
            bundle2.putSparseParcelableArray("android:menu:list", sparseArray2);
        }
        if (this.mAdapter != null) {
            bundle2.putBundle(STATE_ADAPTER, this.mAdapter.createInstanceState());
        }
        return bundle2;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        Bundle bundle = (Bundle) parcelable;
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.mMenuView.restoreHierarchyState(sparseParcelableArray);
        }
        Bundle bundle2 = bundle.getBundle(STATE_ADAPTER);
        if (bundle2 != null) {
            this.mAdapter.restoreInstanceState(bundle2);
        }
    }

    public void setCheckedItem(MenuItemImpl menuItemImpl) {
        this.mAdapter.setCheckedItem(menuItemImpl);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View inflateHeaderView(@LayoutRes int i) {
        View inflate = this.mLayoutInflater.inflate(i, (ViewGroup) this.mHeaderLayout, false);
        addHeaderView(inflate);
        return inflate;
    }

    public void addHeaderView(@NonNull View view) {
        this.mHeaderLayout.addView(view);
        this.mMenuView.setPadding(0, 0, 0, this.mMenuView.getPaddingBottom());
    }

    public void removeHeaderView(@NonNull View view) {
        this.mHeaderLayout.removeView(view);
        if (this.mHeaderLayout.getChildCount() == 0) {
            this.mMenuView.setPadding(0, this.mPaddingTopDefault, 0, this.mMenuView.getPaddingBottom());
        }
    }

    public int getHeaderCount() {
        return this.mHeaderLayout.getChildCount();
    }

    public View getHeaderView(int i) {
        return this.mHeaderLayout.getChildAt(i);
    }

    @Nullable
    public ColorStateList getItemTintList() {
        return this.mIconTintList;
    }

    public void setItemIconTintList(@Nullable ColorStateList colorStateList) {
        this.mIconTintList = colorStateList;
        updateMenuView(false);
    }

    @Nullable
    public ColorStateList getItemTextColor() {
        return this.mTextColor;
    }

    public void setItemTextColor(@Nullable ColorStateList colorStateList) {
        this.mTextColor = colorStateList;
        updateMenuView(false);
    }

    public void setItemTextAppearance(@StyleRes int i) {
        this.mTextAppearance = i;
        this.mTextAppearanceSet = true;
        updateMenuView(false);
    }

    public Drawable getItemBackground() {
        return this.mItemBackground;
    }

    public void setItemBackground(Drawable drawable) {
        this.mItemBackground = drawable;
    }

    public void setUpdateSuspended(boolean z) {
        boolean z2 = z;
        if (this.mAdapter != null) {
            this.mAdapter.setUpdateSuspended(z2);
        }
    }

    private static abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View view) {
            super(view);
        }
    }

    private static class NormalViewHolder extends ViewHolder {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public NormalViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup, View.OnClickListener onClickListener) {
            super(layoutInflater.inflate(R.layout.design_navigation_item, viewGroup, false));
            this.itemView.setOnClickListener(onClickListener);
        }
    }

    private static class SubheaderViewHolder extends ViewHolder {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public SubheaderViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(R.layout.design_navigation_item_subheader, viewGroup, false));
        }
    }

    private static class SeparatorViewHolder extends ViewHolder {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public SeparatorViewHolder(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(R.layout.design_navigation_item_separator, viewGroup, false));
        }
    }

    private static class HeaderViewHolder extends ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    private class NavigationMenuAdapter extends RecyclerView.Adapter<ViewHolder> {
        private static final String STATE_ACTION_VIEWS = "android:menu:action_views";
        private static final String STATE_CHECKED_ITEM = "android:menu:checked";
        private static final int VIEW_TYPE_HEADER = 3;
        private static final int VIEW_TYPE_NORMAL = 0;
        private static final int VIEW_TYPE_SEPARATOR = 2;
        private static final int VIEW_TYPE_SUBHEADER = 1;
        private MenuItemImpl mCheckedItem;
        private final ArrayList<NavigationMenuItem> mItems;
        private ColorDrawable mTransparentIcon;
        private boolean mUpdateSuspended;

        NavigationMenuAdapter() {
            ArrayList<NavigationMenuItem> arrayList;
            new ArrayList<>();
            this.mItems = arrayList;
            prepareMenuItems();
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public int getItemCount() {
            return this.mItems.size();
        }

        public int getItemViewType(int i) {
            Throwable th;
            NavigationMenuItem navigationMenuItem = this.mItems.get(i);
            if (navigationMenuItem instanceof NavigationMenuSeparatorItem) {
                return 2;
            }
            if (navigationMenuItem instanceof NavigationMenuHeaderItem) {
                return 3;
            }
            if (!(navigationMenuItem instanceof NavigationMenuTextItem)) {
                Throwable th2 = th;
                new RuntimeException("Unknown item type.");
                throw th2;
            } else if (((NavigationMenuTextItem) navigationMenuItem).getMenuItem().hasSubMenu()) {
                return 1;
            } else {
                return 0;
            }
        }

        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            ViewHolder viewHolder;
            ViewHolder viewHolder2;
            ViewHolder viewHolder3;
            ViewHolder viewHolder4;
            ViewGroup viewGroup2 = viewGroup;
            switch (i) {
                case 0:
                    new NormalViewHolder(NavigationMenuPresenter.this.mLayoutInflater, viewGroup2, NavigationMenuPresenter.this.mOnClickListener);
                    return viewHolder4;
                case 1:
                    new SubheaderViewHolder(NavigationMenuPresenter.this.mLayoutInflater, viewGroup2);
                    return viewHolder3;
                case 2:
                    new SeparatorViewHolder(NavigationMenuPresenter.this.mLayoutInflater, viewGroup2);
                    return viewHolder2;
                case 3:
                    new HeaderViewHolder(NavigationMenuPresenter.this.mHeaderLayout);
                    return viewHolder;
                default:
                    return null;
            }
        }

        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            ViewHolder viewHolder2 = viewHolder;
            int i2 = i;
            switch (getItemViewType(i2)) {
                case 0:
                    NavigationMenuItemView navigationMenuItemView = (NavigationMenuItemView) viewHolder2.itemView;
                    navigationMenuItemView.setIconTintList(NavigationMenuPresenter.this.mIconTintList);
                    if (NavigationMenuPresenter.this.mTextAppearanceSet) {
                        navigationMenuItemView.setTextAppearance(navigationMenuItemView.getContext(), NavigationMenuPresenter.this.mTextAppearance);
                    }
                    if (NavigationMenuPresenter.this.mTextColor != null) {
                        navigationMenuItemView.setTextColor(NavigationMenuPresenter.this.mTextColor);
                    }
                    navigationMenuItemView.setBackgroundDrawable(NavigationMenuPresenter.this.mItemBackground != null ? NavigationMenuPresenter.this.mItemBackground.getConstantState().newDrawable() : null);
                    navigationMenuItemView.initialize(((NavigationMenuTextItem) this.mItems.get(i2)).getMenuItem(), 0);
                    return;
                case 1:
                    ((TextView) viewHolder2.itemView).setText(((NavigationMenuTextItem) this.mItems.get(i2)).getMenuItem().getTitle());
                    return;
                case 2:
                    NavigationMenuSeparatorItem navigationMenuSeparatorItem = (NavigationMenuSeparatorItem) this.mItems.get(i2);
                    viewHolder2.itemView.setPadding(0, navigationMenuSeparatorItem.getPaddingTop(), 0, navigationMenuSeparatorItem.getPaddingBottom());
                    return;
                default:
                    return;
            }
        }

        public void onViewRecycled(ViewHolder viewHolder) {
            ViewHolder viewHolder2 = viewHolder;
            if (viewHolder2 instanceof NormalViewHolder) {
                ((NavigationMenuItemView) viewHolder2.itemView).recycle();
            }
        }

        public void update() {
            prepareMenuItems();
            notifyDataSetChanged();
        }

        private void prepareMenuItems() {
            Object obj;
            Object obj2;
            Object obj3;
            Object obj4;
            Object obj5;
            Object obj6;
            if (!this.mUpdateSuspended) {
                this.mUpdateSuspended = true;
                this.mItems.clear();
                new NavigationMenuHeaderItem();
                boolean add = this.mItems.add(obj);
                int i = -1;
                int i2 = 0;
                boolean z = false;
                int size = NavigationMenuPresenter.this.mMenu.getVisibleItems().size();
                for (int i3 = 0; i3 < size; i3++) {
                    MenuItemImpl menuItemImpl = NavigationMenuPresenter.this.mMenu.getVisibleItems().get(i3);
                    if (menuItemImpl.isChecked()) {
                        setCheckedItem(menuItemImpl);
                    }
                    if (menuItemImpl.isCheckable()) {
                        menuItemImpl.setExclusiveCheckable(false);
                    }
                    if (menuItemImpl.hasSubMenu()) {
                        SubMenu subMenu = menuItemImpl.getSubMenu();
                        if (subMenu.hasVisibleItems()) {
                            if (i3 != 0) {
                                new NavigationMenuSeparatorItem(NavigationMenuPresenter.this.mPaddingSeparator, 0);
                                boolean add2 = this.mItems.add(obj6);
                            }
                            new NavigationMenuTextItem(menuItemImpl);
                            boolean add3 = this.mItems.add(obj4);
                            boolean z2 = false;
                            int size2 = this.mItems.size();
                            int size3 = subMenu.size();
                            for (int i4 = 0; i4 < size3; i4++) {
                                MenuItemImpl menuItemImpl2 = (MenuItemImpl) subMenu.getItem(i4);
                                if (menuItemImpl2.isVisible()) {
                                    if (!z2 && menuItemImpl2.getIcon() != null) {
                                        z2 = true;
                                    }
                                    if (menuItemImpl2.isCheckable()) {
                                        menuItemImpl2.setExclusiveCheckable(false);
                                    }
                                    if (menuItemImpl.isChecked()) {
                                        setCheckedItem(menuItemImpl);
                                    }
                                    new NavigationMenuTextItem(menuItemImpl2);
                                    boolean add4 = this.mItems.add(obj5);
                                }
                            }
                            if (z2) {
                                appendTransparentIconIfMissing(size2, this.mItems.size());
                            }
                        }
                    } else {
                        int groupId = menuItemImpl.getGroupId();
                        if (groupId != i) {
                            i2 = this.mItems.size();
                            z = menuItemImpl.getIcon() != null;
                            if (i3 != 0) {
                                i2++;
                                new NavigationMenuSeparatorItem(NavigationMenuPresenter.this.mPaddingSeparator, NavigationMenuPresenter.this.mPaddingSeparator);
                                boolean add5 = this.mItems.add(obj3);
                            }
                        } else if (!z && menuItemImpl.getIcon() != null) {
                            z = true;
                            appendTransparentIconIfMissing(i2, this.mItems.size());
                        }
                        if (z && menuItemImpl.getIcon() == null) {
                            MenuItem icon = menuItemImpl.setIcon(17170445);
                        }
                        new NavigationMenuTextItem(menuItemImpl);
                        boolean add6 = this.mItems.add(obj2);
                        i = groupId;
                    }
                }
                this.mUpdateSuspended = false;
            }
        }

        private void appendTransparentIconIfMissing(int i, int i2) {
            ColorDrawable colorDrawable;
            int i3 = i2;
            for (int i4 = i; i4 < i3; i4++) {
                MenuItemImpl menuItem = ((NavigationMenuTextItem) this.mItems.get(i4)).getMenuItem();
                if (menuItem.getIcon() == null) {
                    if (this.mTransparentIcon == null) {
                        new ColorDrawable(17170445);
                        this.mTransparentIcon = colorDrawable;
                    }
                    MenuItem icon = menuItem.setIcon(this.mTransparentIcon);
                }
            }
        }

        public void setCheckedItem(MenuItemImpl menuItemImpl) {
            MenuItemImpl menuItemImpl2 = menuItemImpl;
            if (this.mCheckedItem != menuItemImpl2 && menuItemImpl2.isCheckable()) {
                if (this.mCheckedItem != null) {
                    MenuItem checked = this.mCheckedItem.setChecked(false);
                }
                this.mCheckedItem = menuItemImpl2;
                MenuItem checked2 = menuItemImpl2.setChecked(true);
            }
        }

        public Bundle createInstanceState() {
            Bundle bundle;
            SparseArray sparseArray;
            SparseArray sparseArray2;
            new Bundle();
            Bundle bundle2 = bundle;
            if (this.mCheckedItem != null) {
                bundle2.putInt(STATE_CHECKED_ITEM, this.mCheckedItem.getItemId());
            }
            new SparseArray();
            SparseArray sparseArray3 = sparseArray;
            Iterator<NavigationMenuItem> it = this.mItems.iterator();
            while (it.hasNext()) {
                NavigationMenuItem next = it.next();
                if (next instanceof NavigationMenuTextItem) {
                    MenuItemImpl menuItem = ((NavigationMenuTextItem) next).getMenuItem();
                    View actionView = menuItem != null ? menuItem.getActionView() : null;
                    if (actionView != null) {
                        new ParcelableSparseArray();
                        SparseArray sparseArray4 = sparseArray2;
                        actionView.saveHierarchyState(sparseArray4);
                        sparseArray3.put(menuItem.getItemId(), sparseArray4);
                    }
                }
            }
            bundle2.putSparseParcelableArray(STATE_ACTION_VIEWS, sparseArray3);
            return bundle2;
        }

        public void restoreInstanceState(Bundle bundle) {
            MenuItemImpl menuItem;
            Bundle bundle2 = bundle;
            int i = bundle2.getInt(STATE_CHECKED_ITEM, 0);
            if (i != 0) {
                this.mUpdateSuspended = true;
                Iterator<NavigationMenuItem> it = this.mItems.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    NavigationMenuItem next = it.next();
                    if ((next instanceof NavigationMenuTextItem) && (menuItem = ((NavigationMenuTextItem) next).getMenuItem()) != null && menuItem.getItemId() == i) {
                        setCheckedItem(menuItem);
                        break;
                    }
                }
                this.mUpdateSuspended = false;
                prepareMenuItems();
            }
            SparseArray sparseParcelableArray = bundle2.getSparseParcelableArray(STATE_ACTION_VIEWS);
            Iterator<NavigationMenuItem> it2 = this.mItems.iterator();
            while (it2.hasNext()) {
                NavigationMenuItem next2 = it2.next();
                if (next2 instanceof NavigationMenuTextItem) {
                    MenuItemImpl menuItem2 = ((NavigationMenuTextItem) next2).getMenuItem();
                    View actionView = menuItem2 != null ? menuItem2.getActionView() : null;
                    if (actionView != null) {
                        actionView.restoreHierarchyState((SparseArray) sparseParcelableArray.get(menuItem2.getItemId()));
                    }
                }
            }
        }

        public void setUpdateSuspended(boolean z) {
            this.mUpdateSuspended = z;
        }
    }

    private static class NavigationMenuTextItem implements NavigationMenuItem {
        private final MenuItemImpl mMenuItem;

        private NavigationMenuTextItem(MenuItemImpl menuItemImpl) {
            this.mMenuItem = menuItemImpl;
        }

        public MenuItemImpl getMenuItem() {
            return this.mMenuItem;
        }
    }

    private static class NavigationMenuSeparatorItem implements NavigationMenuItem {
        private final int mPaddingBottom;
        private final int mPaddingTop;

        public NavigationMenuSeparatorItem(int i, int i2) {
            this.mPaddingTop = i;
            this.mPaddingBottom = i2;
        }

        public int getPaddingTop() {
            return this.mPaddingTop;
        }

        public int getPaddingBottom() {
            return this.mPaddingBottom;
        }
    }

    private static class NavigationMenuHeaderItem implements NavigationMenuItem {
        private NavigationMenuHeaderItem() {
        }
    }
}
