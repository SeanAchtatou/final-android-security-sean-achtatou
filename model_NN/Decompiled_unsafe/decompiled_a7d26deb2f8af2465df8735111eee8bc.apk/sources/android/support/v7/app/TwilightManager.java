package android.support.v7.app;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.content.PermissionChecker;
import android.util.Log;
import java.util.Calendar;

class TwilightManager {
    private static final int SUNRISE = 6;
    private static final int SUNSET = 22;
    private static final String TAG = "TwilightManager";
    private static final TwilightState sTwilightState;
    private final Context mContext;
    private final LocationManager mLocationManager;

    static {
        TwilightState twilightState;
        new TwilightState();
        sTwilightState = twilightState;
    }

    TwilightManager(Context context) {
        Context context2 = context;
        this.mContext = context2;
        this.mLocationManager = (LocationManager) context2.getSystemService("location");
    }

    /* access modifiers changed from: package-private */
    public boolean isNight() {
        TwilightState twilightState = sTwilightState;
        if (isStateValid(twilightState)) {
            return twilightState.isNight;
        }
        Location lastKnownLocation = getLastKnownLocation();
        if (lastKnownLocation != null) {
            updateState(lastKnownLocation);
            return twilightState.isNight;
        }
        int i = Log.i(TAG, "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i2 = Calendar.getInstance().get(11);
        return i2 < 6 || i2 >= 22;
    }

    private Location getLastKnownLocation() {
        Location location = null;
        Location location2 = null;
        if (PermissionChecker.checkSelfPermission(this.mContext, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            location = getLastKnownLocationForProvider("network");
        }
        if (PermissionChecker.checkSelfPermission(this.mContext, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location2 = getLastKnownLocationForProvider("gps");
        }
        if (location2 == null || location == null) {
            return location2 != null ? location2 : location;
        }
        return location2.getTime() > location.getTime() ? location2 : location;
    }

    private Location getLastKnownLocationForProvider(String str) {
        String str2 = str;
        if (this.mLocationManager != null) {
            try {
                if (this.mLocationManager.isProviderEnabled(str2)) {
                    return this.mLocationManager.getLastKnownLocation(str2);
                }
            } catch (Exception e) {
                int d = Log.d(TAG, "Failed to get last known location", e);
            }
        }
        return null;
    }

    private boolean isStateValid(TwilightState twilightState) {
        TwilightState twilightState2 = twilightState;
        return twilightState2 != null && twilightState2.nextUpdate > System.currentTimeMillis();
    }

    private void updateState(@NonNull Location location) {
        long j;
        long j2;
        Location location2 = location;
        TwilightState twilightState = sTwilightState;
        long currentTimeMillis = System.currentTimeMillis();
        TwilightCalculator instance = TwilightCalculator.getInstance();
        instance.calculateTwilight(currentTimeMillis - 86400000, location2.getLatitude(), location2.getLongitude());
        long j3 = instance.sunset;
        instance.calculateTwilight(currentTimeMillis, location2.getLatitude(), location2.getLongitude());
        boolean z = instance.state == 1;
        long j4 = instance.sunrise;
        long j5 = instance.sunset;
        instance.calculateTwilight(currentTimeMillis + 86400000, location2.getLatitude(), location2.getLongitude());
        long j6 = instance.sunrise;
        if (j4 == -1 || j5 == -1) {
            j = currentTimeMillis + 43200000;
        } else {
            if (currentTimeMillis > j5) {
                j2 = 0 + j6;
            } else if (currentTimeMillis > j4) {
                j2 = 0 + j5;
            } else {
                j2 = 0 + j4;
            }
            j = j2 + 60000;
        }
        twilightState.isNight = z;
        twilightState.yesterdaySunset = j3;
        twilightState.todaySunrise = j4;
        twilightState.todaySunset = j5;
        twilightState.tomorrowSunrise = j6;
        twilightState.nextUpdate = j;
    }

    private static class TwilightState {
        boolean isNight;
        long nextUpdate;
        long todaySunrise;
        long todaySunset;
        long tomorrowSunrise;
        long yesterdaySunset;

        private TwilightState() {
        }
    }
}
