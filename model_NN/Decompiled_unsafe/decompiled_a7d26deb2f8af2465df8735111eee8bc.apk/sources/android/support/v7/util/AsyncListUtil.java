package android.support.v7.util;

import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.support.v7.util.ThreadUtil;
import android.support.v7.util.TileList;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;

public class AsyncListUtil<T> {
    private static final boolean DEBUG = false;
    private static final String TAG = "AsyncListUtil";
    private boolean mAllowScrollHints;
    private final ThreadUtil.BackgroundCallback<T> mBackgroundCallback;
    final ThreadUtil.BackgroundCallback<T> mBackgroundProxy;
    final DataCallback<T> mDataCallback;
    int mDisplayedGeneration = 0;
    private int mItemCount = 0;
    private final ThreadUtil.MainThreadCallback<T> mMainThreadCallback;
    final ThreadUtil.MainThreadCallback<T> mMainThreadProxy;
    /* access modifiers changed from: private */
    public final SparseIntArray mMissingPositions;
    final int[] mPrevRange = new int[2];
    int mRequestedGeneration = this.mDisplayedGeneration;
    private int mScrollHint = 0;
    final Class<T> mTClass;
    final TileList<T> mTileList;
    final int mTileSize;
    final int[] mTmpRange = new int[2];
    final int[] mTmpRangeExtended = new int[2];
    final ViewCallback mViewCallback;

    static /* synthetic */ int access$002(AsyncListUtil asyncListUtil, int i) {
        int i2 = i;
        int i3 = i2;
        asyncListUtil.mItemCount = i3;
        return i2;
    }

    static /* synthetic */ boolean access$102(AsyncListUtil asyncListUtil, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        asyncListUtil.mAllowScrollHints = z3;
        return z2;
    }

    private void log(String str, Object... objArr) {
        StringBuilder sb;
        new StringBuilder();
        int d = Log.d(TAG, sb.append("[MAIN] ").append(String.format(str, objArr)).toString());
    }

    public AsyncListUtil(Class<T> cls, int i, DataCallback<T> dataCallback, ViewCallback viewCallback) {
        SparseIntArray sparseIntArray;
        ThreadUtil.MainThreadCallback<T> mainThreadCallback;
        ThreadUtil.BackgroundCallback<T> backgroundCallback;
        TileList<T> tileList;
        Object obj;
        new SparseIntArray();
        this.mMissingPositions = sparseIntArray;
        new ThreadUtil.MainThreadCallback<T>() {
            public void updateItemCount(int i, int i2) {
                int i3 = i2;
                if (isRequestedGeneration(i)) {
                    int access$002 = AsyncListUtil.access$002(AsyncListUtil.this, i3);
                    AsyncListUtil.this.mViewCallback.onDataRefresh();
                    AsyncListUtil.this.mDisplayedGeneration = AsyncListUtil.this.mRequestedGeneration;
                    recycleAllTiles();
                    boolean access$102 = AsyncListUtil.access$102(AsyncListUtil.this, false);
                    AsyncListUtil.this.updateRange();
                }
            }

            public void addTile(int i, TileList.Tile<T> tile) {
                StringBuilder sb;
                TileList.Tile<T> tile2 = tile;
                if (!isRequestedGeneration(i)) {
                    AsyncListUtil.this.mBackgroundProxy.recycleTile(tile2);
                    return;
                }
                TileList.Tile<T> addOrReplace = AsyncListUtil.this.mTileList.addOrReplace(tile2);
                if (addOrReplace != null) {
                    new StringBuilder();
                    int e = Log.e(AsyncListUtil.TAG, sb.append("duplicate tile @").append(addOrReplace.mStartPosition).toString());
                    AsyncListUtil.this.mBackgroundProxy.recycleTile(addOrReplace);
                }
                int i2 = tile2.mStartPosition + tile2.mItemCount;
                int i3 = 0;
                while (i3 < AsyncListUtil.this.mMissingPositions.size()) {
                    int keyAt = AsyncListUtil.this.mMissingPositions.keyAt(i3);
                    if (tile2.mStartPosition > keyAt || keyAt >= i2) {
                        i3++;
                    } else {
                        AsyncListUtil.this.mMissingPositions.removeAt(i3);
                        AsyncListUtil.this.mViewCallback.onItemLoaded(keyAt);
                    }
                }
            }

            public void removeTile(int i, int i2) {
                StringBuilder sb;
                int i3 = i2;
                if (isRequestedGeneration(i)) {
                    TileList.Tile<T> removeAtPos = AsyncListUtil.this.mTileList.removeAtPos(i3);
                    if (removeAtPos == null) {
                        new StringBuilder();
                        int e = Log.e(AsyncListUtil.TAG, sb.append("tile not found @").append(i3).toString());
                        return;
                    }
                    AsyncListUtil.this.mBackgroundProxy.recycleTile(removeAtPos);
                }
            }

            private void recycleAllTiles() {
                for (int i = 0; i < AsyncListUtil.this.mTileList.size(); i++) {
                    AsyncListUtil.this.mBackgroundProxy.recycleTile(AsyncListUtil.this.mTileList.getAtIndex(i));
                }
                AsyncListUtil.this.mTileList.clear();
            }

            private boolean isRequestedGeneration(int i) {
                return i == AsyncListUtil.this.mRequestedGeneration;
            }
        };
        this.mMainThreadCallback = mainThreadCallback;
        new ThreadUtil.BackgroundCallback<T>() {
            private int mFirstRequiredTileStart;
            private int mGeneration;
            private int mItemCount;
            private int mLastRequiredTileStart;
            final SparseBooleanArray mLoadedTiles;
            private TileList.Tile<T> mRecycledRoot;

            {
                SparseBooleanArray sparseBooleanArray;
                new SparseBooleanArray();
                this.mLoadedTiles = sparseBooleanArray;
            }

            public void refresh(int i) {
                this.mGeneration = i;
                this.mLoadedTiles.clear();
                this.mItemCount = AsyncListUtil.this.mDataCallback.refreshData();
                AsyncListUtil.this.mMainThreadProxy.updateItemCount(this.mGeneration, this.mItemCount);
            }

            public void updateRange(int i, int i2, int i3, int i4, int i5) {
                int i6 = i;
                int i7 = i2;
                int i8 = i3;
                int i9 = i4;
                int i10 = i5;
                if (i6 <= i7) {
                    int tileStart = getTileStart(i6);
                    int tileStart2 = getTileStart(i7);
                    this.mFirstRequiredTileStart = getTileStart(i8);
                    this.mLastRequiredTileStart = getTileStart(i9);
                    if (i10 == 1) {
                        requestTiles(this.mFirstRequiredTileStart, tileStart2, i10, true);
                        requestTiles(tileStart2 + AsyncListUtil.this.mTileSize, this.mLastRequiredTileStart, i10, false);
                        return;
                    }
                    requestTiles(tileStart, this.mLastRequiredTileStart, i10, false);
                    requestTiles(this.mFirstRequiredTileStart, tileStart - AsyncListUtil.this.mTileSize, i10, true);
                }
            }

            private int getTileStart(int i) {
                int i2 = i;
                return i2 - (i2 % AsyncListUtil.this.mTileSize);
            }

            private void requestTiles(int i, int i2, int i3, boolean z) {
                int i4 = i;
                int i5 = i2;
                int i6 = i3;
                boolean z2 = z;
                int i7 = i4;
                while (true) {
                    int i8 = i7;
                    if (i8 <= i5) {
                        AsyncListUtil.this.mBackgroundProxy.loadTile(z2 ? (i5 + i4) - i8 : i8, i6);
                        i7 = i8 + AsyncListUtil.this.mTileSize;
                    } else {
                        return;
                    }
                }
            }

            public void loadTile(int i, int i2) {
                int i3 = i;
                int i4 = i2;
                if (!isTileLoaded(i3)) {
                    TileList.Tile acquireTile = acquireTile();
                    acquireTile.mStartPosition = i3;
                    acquireTile.mItemCount = Math.min(AsyncListUtil.this.mTileSize, this.mItemCount - acquireTile.mStartPosition);
                    AsyncListUtil.this.mDataCallback.fillData(acquireTile.mItems, acquireTile.mStartPosition, acquireTile.mItemCount);
                    flushTileCache(i4);
                    addTile(acquireTile);
                }
            }

            public void recycleTile(TileList.Tile<T> tile) {
                TileList.Tile<T> tile2 = tile;
                AsyncListUtil.this.mDataCallback.recycleData(tile2.mItems, tile2.mItemCount);
                tile2.mNext = this.mRecycledRoot;
                this.mRecycledRoot = tile2;
            }

            /* JADX WARN: Type inference failed for: r6v0 */
            /* JADX WARNING: Multi-variable type inference failed */
            /* JADX WARNING: Unknown variable types count: 1 */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            private android.support.v7.util.TileList.Tile<T> acquireTile() {
                /*
                    r7 = this;
                    r0 = r7
                    r2 = r0
                    android.support.v7.util.TileList$Tile<T> r2 = r2.mRecycledRoot
                    if (r2 == 0) goto L_0x0015
                    r2 = r0
                    android.support.v7.util.TileList$Tile<T> r2 = r2.mRecycledRoot
                    r1 = r2
                    r2 = r0
                    r3 = r0
                    android.support.v7.util.TileList$Tile<T> r3 = r3.mRecycledRoot
                    android.support.v7.util.TileList$Tile<T> r3 = r3.mNext
                    r2.mRecycledRoot = r3
                    r2 = r1
                    r0 = r2
                L_0x0014:
                    return r0
                L_0x0015:
                    android.support.v7.util.TileList$Tile r2 = new android.support.v7.util.TileList$Tile
                    r6 = r2
                    r2 = r6
                    r3 = r6
                    r4 = r0
                    android.support.v7.util.AsyncListUtil r4 = android.support.v7.util.AsyncListUtil.this
                    java.lang.Class<T> r4 = r4.mTClass
                    r5 = r0
                    android.support.v7.util.AsyncListUtil r5 = android.support.v7.util.AsyncListUtil.this
                    int r5 = r5.mTileSize
                    r3.<init>(r4, r5)
                    r0 = r2
                    goto L_0x0014
                */
                throw new UnsupportedOperationException("Method not decompiled: android.support.v7.util.AsyncListUtil.AnonymousClass2.acquireTile():android.support.v7.util.TileList$Tile");
            }

            private boolean isTileLoaded(int i) {
                return this.mLoadedTiles.get(i);
            }

            private void addTile(TileList.Tile<T> tile) {
                TileList.Tile<T> tile2 = tile;
                this.mLoadedTiles.put(tile2.mStartPosition, true);
                AsyncListUtil.this.mMainThreadProxy.addTile(this.mGeneration, tile2);
            }

            private void removeTile(int i) {
                int i2 = i;
                this.mLoadedTiles.delete(i2);
                AsyncListUtil.this.mMainThreadProxy.removeTile(this.mGeneration, i2);
            }

            private void flushTileCache(int i) {
                int i2 = i;
                int maxCachedTiles = AsyncListUtil.this.mDataCallback.getMaxCachedTiles();
                while (this.mLoadedTiles.size() >= maxCachedTiles) {
                    int keyAt = this.mLoadedTiles.keyAt(0);
                    int keyAt2 = this.mLoadedTiles.keyAt(this.mLoadedTiles.size() - 1);
                    int i3 = this.mFirstRequiredTileStart - keyAt;
                    int i4 = keyAt2 - this.mLastRequiredTileStart;
                    if (i3 > 0 && (i3 >= i4 || i2 == 2)) {
                        removeTile(keyAt);
                    } else if (i4 <= 0) {
                        return;
                    } else {
                        if (i3 < i4 || i2 == 1) {
                            removeTile(keyAt2);
                        } else {
                            return;
                        }
                    }
                }
            }

            private void log(String str, Object... objArr) {
                StringBuilder sb;
                new StringBuilder();
                int d = Log.d(AsyncListUtil.TAG, sb.append("[BKGR] ").append(String.format(str, objArr)).toString());
            }
        };
        this.mBackgroundCallback = backgroundCallback;
        this.mTClass = cls;
        this.mTileSize = i;
        this.mDataCallback = dataCallback;
        this.mViewCallback = viewCallback;
        new TileList<>(this.mTileSize);
        this.mTileList = tileList;
        new MessageThreadUtil();
        Object obj2 = obj;
        this.mMainThreadProxy = obj2.getMainThreadProxy(this.mMainThreadCallback);
        this.mBackgroundProxy = obj2.getBackgroundProxy(this.mBackgroundCallback);
        refresh();
    }

    private boolean isRefreshPending() {
        return this.mRequestedGeneration != this.mDisplayedGeneration;
    }

    public void onRangeChanged() {
        if (!isRefreshPending()) {
            updateRange();
            this.mAllowScrollHints = true;
        }
    }

    public void refresh() {
        this.mMissingPositions.clear();
        ThreadUtil.BackgroundCallback<T> backgroundCallback = this.mBackgroundProxy;
        int i = this.mRequestedGeneration + 1;
        this.mRequestedGeneration = i;
        backgroundCallback.refresh(i);
    }

    public T getItem(int i) {
        Throwable th;
        StringBuilder sb;
        int i2 = i;
        if (i2 < 0 || i2 >= this.mItemCount) {
            Throwable th2 = th;
            new StringBuilder();
            new IndexOutOfBoundsException(sb.append(i2).append(" is not within 0 and ").append(this.mItemCount).toString());
            throw th2;
        }
        T itemAt = this.mTileList.getItemAt(i2);
        if (itemAt == null && !isRefreshPending()) {
            this.mMissingPositions.put(i2, 0);
        }
        return itemAt;
    }

    public int getItemCount() {
        return this.mItemCount;
    }

    /* access modifiers changed from: private */
    public void updateRange() {
        this.mViewCallback.getItemRangeInto(this.mTmpRange);
        if (this.mTmpRange[0] <= this.mTmpRange[1] && this.mTmpRange[0] >= 0 && this.mTmpRange[1] < this.mItemCount) {
            if (!this.mAllowScrollHints) {
                this.mScrollHint = 0;
            } else if (this.mTmpRange[0] > this.mPrevRange[1] || this.mPrevRange[0] > this.mTmpRange[1]) {
                this.mScrollHint = 0;
            } else if (this.mTmpRange[0] < this.mPrevRange[0]) {
                this.mScrollHint = 1;
            } else if (this.mTmpRange[0] > this.mPrevRange[0]) {
                this.mScrollHint = 2;
            }
            this.mPrevRange[0] = this.mTmpRange[0];
            this.mPrevRange[1] = this.mTmpRange[1];
            this.mViewCallback.extendRangeInto(this.mTmpRange, this.mTmpRangeExtended, this.mScrollHint);
            this.mTmpRangeExtended[0] = Math.min(this.mTmpRange[0], Math.max(this.mTmpRangeExtended[0], 0));
            this.mTmpRangeExtended[1] = Math.max(this.mTmpRange[1], Math.min(this.mTmpRangeExtended[1], this.mItemCount - 1));
            this.mBackgroundProxy.updateRange(this.mTmpRange[0], this.mTmpRange[1], this.mTmpRangeExtended[0], this.mTmpRangeExtended[1], this.mScrollHint);
        }
    }

    public static abstract class DataCallback<T> {
        @WorkerThread
        public abstract void fillData(T[] tArr, int i, int i2);

        @WorkerThread
        public abstract int refreshData();

        @WorkerThread
        public void recycleData(T[] tArr, int i) {
        }

        @WorkerThread
        public int getMaxCachedTiles() {
            return 10;
        }
    }

    public static abstract class ViewCallback {
        public static final int HINT_SCROLL_ASC = 2;
        public static final int HINT_SCROLL_DESC = 1;
        public static final int HINT_SCROLL_NONE = 0;

        @UiThread
        public abstract void getItemRangeInto(int[] iArr);

        @UiThread
        public abstract void onDataRefresh();

        @UiThread
        public abstract void onItemLoaded(int i);

        @UiThread
        public void extendRangeInto(int[] iArr, int[] iArr2, int i) {
            int[] iArr3 = iArr;
            int[] iArr4 = iArr2;
            int i2 = i;
            int i3 = (iArr3[1] - iArr3[0]) + 1;
            int i4 = i3 / 2;
            iArr4[0] = iArr3[0] - (i2 == 1 ? i3 : i4);
            iArr4[1] = iArr3[1] + (i2 == 2 ? i3 : i4);
        }
    }
}
