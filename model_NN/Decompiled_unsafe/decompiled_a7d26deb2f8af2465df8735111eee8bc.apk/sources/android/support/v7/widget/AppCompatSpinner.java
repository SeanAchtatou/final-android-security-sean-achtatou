package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.view.TintableBackgroundView;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.widget.ListPopupWindow;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.ThemedSpinnerAdapter;

public class AppCompatSpinner extends Spinner implements TintableBackgroundView {
    private static final int[] ATTRS_ANDROID_SPINNERMODE = {16843505};
    private static final boolean IS_AT_LEAST_JB = (Build.VERSION.SDK_INT >= 16);
    /* access modifiers changed from: private */
    public static final boolean IS_AT_LEAST_M = (Build.VERSION.SDK_INT >= 23);
    private static final int MAX_ITEMS_MEASURED = 15;
    private static final int MODE_DIALOG = 0;
    private static final int MODE_DROPDOWN = 1;
    private static final int MODE_THEME = -1;
    private static final String TAG = "AppCompatSpinner";
    private AppCompatBackgroundHelper mBackgroundTintHelper;
    /* access modifiers changed from: private */
    public int mDropDownWidth;
    private ListPopupWindow.ForwardingListener mForwardingListener;
    /* access modifiers changed from: private */
    public DropdownPopup mPopup;
    private Context mPopupContext;
    private boolean mPopupSet;
    private SpinnerAdapter mTempAdapter;
    /* access modifiers changed from: private */
    public final Rect mTempRect;
    private TintManager mTintManager;

    public AppCompatSpinner(Context context) {
        this(context, (AttributeSet) null);
    }

    public AppCompatSpinner(Context context, int i) {
        this(context, null, R.attr.spinnerStyle, i);
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.spinnerStyle);
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, -1);
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet, int i, int i2) {
        this(context, attributeSet, i, i2, null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatSpinner(android.content.Context r18, android.util.AttributeSet r19, int r20, int r21, android.content.res.Resources.Theme r22) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            r3 = r20
            r4 = r21
            r5 = r22
            r10 = r0
            r11 = r1
            r12 = r2
            r13 = r3
            r10.<init>(r11, r12, r13)
            r10 = r0
            android.graphics.Rect r11 = new android.graphics.Rect
            r16 = r11
            r11 = r16
            r12 = r16
            r12.<init>()
            r10.mTempRect = r11
            r10 = r1
            r11 = r2
            int[] r12 = android.support.v7.appcompat.R.styleable.Spinner
            r13 = r3
            r14 = 0
            android.support.v7.widget.TintTypedArray r10 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r10, r11, r12, r13, r14)
            r6 = r10
            r10 = r0
            r11 = r6
            android.support.v7.widget.TintManager r11 = r11.getTintManager()
            r10.mTintManager = r11
            r10 = r0
            android.support.v7.widget.AppCompatBackgroundHelper r11 = new android.support.v7.widget.AppCompatBackgroundHelper
            r16 = r11
            r11 = r16
            r12 = r16
            r13 = r0
            r14 = r0
            android.support.v7.widget.TintManager r14 = r14.mTintManager
            r12.<init>(r13, r14)
            r10.mBackgroundTintHelper = r11
            r10 = r5
            if (r10 == 0) goto L_0x010a
            r10 = r0
            android.support.v7.view.ContextThemeWrapper r11 = new android.support.v7.view.ContextThemeWrapper
            r16 = r11
            r11 = r16
            r12 = r16
            r13 = r1
            r14 = r5
            r12.<init>(r13, r14)
            r10.mPopupContext = r11
        L_0x0059:
            r10 = r0
            android.content.Context r10 = r10.mPopupContext
            if (r10 == 0) goto L_0x00e9
            r10 = r4
            r11 = -1
            if (r10 != r11) goto L_0x008c
            int r10 = android.os.Build.VERSION.SDK_INT
            r11 = 11
            if (r10 < r11) goto L_0x0153
            r10 = 0
            r7 = r10
            r10 = r1
            r11 = r2
            int[] r12 = android.support.v7.widget.AppCompatSpinner.ATTRS_ANDROID_SPINNERMODE     // Catch:{ Exception -> 0x0134 }
            r13 = r3
            r14 = 0
            android.content.res.TypedArray r10 = r10.obtainStyledAttributes(r11, r12, r13, r14)     // Catch:{ Exception -> 0x0134 }
            r7 = r10
            r10 = r7
            r11 = 0
            boolean r10 = r10.hasValue(r11)     // Catch:{ Exception -> 0x0134 }
            if (r10 == 0) goto L_0x0085
            r10 = r7
            r11 = 0
            r12 = 0
            int r10 = r10.getInt(r11, r12)     // Catch:{ Exception -> 0x0134 }
            r4 = r10
        L_0x0085:
            r10 = r7
            if (r10 == 0) goto L_0x008c
            r10 = r7
            r10.recycle()
        L_0x008c:
            r10 = r4
            r11 = 1
            if (r10 != r11) goto L_0x00e9
            android.support.v7.widget.AppCompatSpinner$DropdownPopup r10 = new android.support.v7.widget.AppCompatSpinner$DropdownPopup
            r16 = r10
            r10 = r16
            r11 = r16
            r12 = r0
            r13 = r0
            android.content.Context r13 = r13.mPopupContext
            r14 = r2
            r15 = r3
            r11.<init>(r12, r13, r14, r15)
            r7 = r10
            r10 = r0
            android.content.Context r10 = r10.mPopupContext
            r11 = r2
            int[] r12 = android.support.v7.appcompat.R.styleable.Spinner
            r13 = r3
            r14 = 0
            android.support.v7.widget.TintTypedArray r10 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r10, r11, r12, r13, r14)
            r8 = r10
            r10 = r0
            r11 = r8
            int r12 = android.support.v7.appcompat.R.styleable.Spinner_android_dropDownWidth
            r13 = -2
            int r11 = r11.getLayoutDimension(r12, r13)
            r10.mDropDownWidth = r11
            r10 = r7
            r11 = r8
            int r12 = android.support.v7.appcompat.R.styleable.Spinner_android_popupBackground
            android.graphics.drawable.Drawable r11 = r11.getDrawable(r12)
            r10.setBackgroundDrawable(r11)
            r10 = r7
            r11 = r6
            int r12 = android.support.v7.appcompat.R.styleable.Spinner_android_prompt
            java.lang.String r11 = r11.getString(r12)
            r10.setPromptText(r11)
            r10 = r8
            r10.recycle()
            r10 = r0
            r11 = r7
            r10.mPopup = r11
            r10 = r0
            android.support.v7.widget.AppCompatSpinner$1 r11 = new android.support.v7.widget.AppCompatSpinner$1
            r16 = r11
            r11 = r16
            r12 = r16
            r13 = r0
            r14 = r0
            r15 = r7
            r12.<init>(r14, r15)
            r10.mForwardingListener = r11
        L_0x00e9:
            r10 = r6
            r10.recycle()
            r10 = r0
            r11 = 1
            r10.mPopupSet = r11
            r10 = r0
            android.widget.SpinnerAdapter r10 = r10.mTempAdapter
            if (r10 == 0) goto L_0x0101
            r10 = r0
            r11 = r0
            android.widget.SpinnerAdapter r11 = r11.mTempAdapter
            r10.setAdapter(r11)
            r10 = r0
            r11 = 0
            r10.mTempAdapter = r11
        L_0x0101:
            r10 = r0
            android.support.v7.widget.AppCompatBackgroundHelper r10 = r10.mBackgroundTintHelper
            r11 = r2
            r12 = r3
            r10.loadFromAttributes(r11, r12)
            return
        L_0x010a:
            r10 = r6
            int r11 = android.support.v7.appcompat.R.styleable.Spinner_popupTheme
            r12 = 0
            int r10 = r10.getResourceId(r11, r12)
            r7 = r10
            r10 = r7
            if (r10 == 0) goto L_0x0128
            r10 = r0
            android.support.v7.view.ContextThemeWrapper r11 = new android.support.v7.view.ContextThemeWrapper
            r16 = r11
            r11 = r16
            r12 = r16
            r13 = r1
            r14 = r7
            r12.<init>(r13, r14)
            r10.mPopupContext = r11
            goto L_0x0059
        L_0x0128:
            r10 = r0
            boolean r11 = android.support.v7.widget.AppCompatSpinner.IS_AT_LEAST_M
            if (r11 != 0) goto L_0x0132
            r11 = r1
        L_0x012e:
            r10.mPopupContext = r11
            goto L_0x0059
        L_0x0132:
            r11 = 0
            goto L_0x012e
        L_0x0134:
            r10 = move-exception
            r8 = r10
            java.lang.String r10 = "AppCompatSpinner"
            java.lang.String r11 = "Could not read android:spinnerMode"
            r12 = r8
            int r10 = android.util.Log.i(r10, r11, r12)     // Catch:{ all -> 0x0148 }
            r10 = r7
            if (r10 == 0) goto L_0x008c
            r10 = r7
            r10.recycle()
            goto L_0x008c
        L_0x0148:
            r10 = move-exception
            r9 = r10
            r10 = r7
            if (r10 == 0) goto L_0x0151
            r10 = r7
            r10.recycle()
        L_0x0151:
            r10 = r9
            throw r10
        L_0x0153:
            r10 = 1
            r4 = r10
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AppCompatSpinner.<init>(android.content.Context, android.util.AttributeSet, int, int, android.content.res.Resources$Theme):void");
    }

    public Context getPopupContext() {
        if (this.mPopup != null) {
            return this.mPopupContext;
        }
        if (IS_AT_LEAST_M) {
            return super.getPopupContext();
        }
        return null;
    }

    public void setPopupBackgroundDrawable(Drawable drawable) {
        Drawable drawable2 = drawable;
        if (this.mPopup != null) {
            this.mPopup.setBackgroundDrawable(drawable2);
        } else if (IS_AT_LEAST_JB) {
            super.setPopupBackgroundDrawable(drawable2);
        }
    }

    public void setPopupBackgroundResource(@DrawableRes int i) {
        setPopupBackgroundDrawable(getPopupContext().getDrawable(i));
    }

    public Drawable getPopupBackground() {
        if (this.mPopup != null) {
            return this.mPopup.getBackground();
        }
        if (IS_AT_LEAST_JB) {
            return super.getPopupBackground();
        }
        return null;
    }

    public void setDropDownVerticalOffset(int i) {
        int i2 = i;
        if (this.mPopup != null) {
            this.mPopup.setVerticalOffset(i2);
        } else if (IS_AT_LEAST_JB) {
            super.setDropDownVerticalOffset(i2);
        }
    }

    public int getDropDownVerticalOffset() {
        if (this.mPopup != null) {
            return this.mPopup.getVerticalOffset();
        }
        if (IS_AT_LEAST_JB) {
            return super.getDropDownVerticalOffset();
        }
        return 0;
    }

    public void setDropDownHorizontalOffset(int i) {
        int i2 = i;
        if (this.mPopup != null) {
            this.mPopup.setHorizontalOffset(i2);
        } else if (IS_AT_LEAST_JB) {
            super.setDropDownHorizontalOffset(i2);
        }
    }

    public int getDropDownHorizontalOffset() {
        if (this.mPopup != null) {
            return this.mPopup.getHorizontalOffset();
        }
        if (IS_AT_LEAST_JB) {
            return super.getDropDownHorizontalOffset();
        }
        return 0;
    }

    public void setDropDownWidth(int i) {
        int i2 = i;
        if (this.mPopup != null) {
            this.mDropDownWidth = i2;
        } else if (IS_AT_LEAST_JB) {
            super.setDropDownWidth(i2);
        }
    }

    public int getDropDownWidth() {
        if (this.mPopup != null) {
            return this.mDropDownWidth;
        }
        if (IS_AT_LEAST_JB) {
            return super.getDropDownWidth();
        }
        return 0;
    }

    public void setAdapter(SpinnerAdapter spinnerAdapter) {
        ListAdapter listAdapter;
        SpinnerAdapter spinnerAdapter2 = spinnerAdapter;
        if (!this.mPopupSet) {
            this.mTempAdapter = spinnerAdapter2;
            return;
        }
        super.setAdapter(spinnerAdapter2);
        if (this.mPopup != null) {
            new DropDownAdapter(spinnerAdapter2, (this.mPopupContext == null ? getContext() : this.mPopupContext).getTheme());
            this.mPopup.setAdapter(listAdapter);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mPopup != null && this.mPopup.isShowing()) {
            this.mPopup.dismiss();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        if (this.mForwardingListener == null || !this.mForwardingListener.onTouch(this, motionEvent2)) {
            return super.onTouchEvent(motionEvent2);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = i;
        super.onMeasure(i3, i2);
        if (this.mPopup != null && View.MeasureSpec.getMode(i3) == Integer.MIN_VALUE) {
            setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), compatMeasureContentWidth(getAdapter(), getBackground())), View.MeasureSpec.getSize(i3)), getMeasuredHeight());
        }
    }

    public boolean performClick() {
        if (this.mPopup == null || this.mPopup.isShowing()) {
            return super.performClick();
        }
        this.mPopup.show();
        return true;
    }

    public void setPrompt(CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        if (this.mPopup != null) {
            this.mPopup.setPromptText(charSequence2);
        } else {
            super.setPrompt(charSequence2);
        }
    }

    public CharSequence getPrompt() {
        return this.mPopup != null ? this.mPopup.getHintText() : super.getPrompt();
    }

    public void setBackgroundResource(@DrawableRes int i) {
        int i2 = i;
        super.setBackgroundResource(i2);
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.onSetBackgroundResource(i2);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        Drawable drawable2 = drawable;
        super.setBackgroundDrawable(drawable2);
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.onSetBackgroundDrawable(drawable2);
        }
    }

    public void setSupportBackgroundTintList(@Nullable ColorStateList colorStateList) {
        ColorStateList colorStateList2 = colorStateList;
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.setSupportBackgroundTintList(colorStateList2);
        }
    }

    @Nullable
    public ColorStateList getSupportBackgroundTintList() {
        return this.mBackgroundTintHelper != null ? this.mBackgroundTintHelper.getSupportBackgroundTintList() : null;
    }

    public void setSupportBackgroundTintMode(@Nullable PorterDuff.Mode mode) {
        PorterDuff.Mode mode2 = mode;
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.setSupportBackgroundTintMode(mode2);
        }
    }

    @Nullable
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        return this.mBackgroundTintHelper != null ? this.mBackgroundTintHelper.getSupportBackgroundTintMode() : null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.applySupportBackgroundTint();
        }
    }

    /* access modifiers changed from: private */
    public int compatMeasureContentWidth(SpinnerAdapter spinnerAdapter, Drawable drawable) {
        ViewGroup.LayoutParams layoutParams;
        SpinnerAdapter spinnerAdapter2 = spinnerAdapter;
        Drawable drawable2 = drawable;
        if (spinnerAdapter2 == null) {
            return 0;
        }
        int i = 0;
        View view = null;
        int i2 = 0;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 0);
        int max = Math.max(0, getSelectedItemPosition());
        int min = Math.min(spinnerAdapter2.getCount(), max + 15);
        for (int max2 = Math.max(0, max - (15 - (min - max))); max2 < min; max2++) {
            int itemViewType = spinnerAdapter2.getItemViewType(max2);
            if (itemViewType != i2) {
                i2 = itemViewType;
                view = null;
            }
            view = spinnerAdapter2.getView(max2, view, this);
            if (view.getLayoutParams() == null) {
                new ViewGroup.LayoutParams(-2, -2);
                view.setLayoutParams(layoutParams);
            }
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            i = Math.max(i, view.getMeasuredWidth());
        }
        if (drawable2 != null) {
            boolean padding = drawable2.getPadding(this.mTempRect);
            i += this.mTempRect.left + this.mTempRect.right;
        }
        return i;
    }

    private static class DropDownAdapter implements ListAdapter, SpinnerAdapter {
        private SpinnerAdapter mAdapter;
        private ListAdapter mListAdapter;

        public DropDownAdapter(@Nullable SpinnerAdapter spinnerAdapter, @Nullable Resources.Theme theme) {
            SpinnerAdapter spinnerAdapter2 = spinnerAdapter;
            Resources.Theme theme2 = theme;
            this.mAdapter = spinnerAdapter2;
            if (spinnerAdapter2 instanceof ListAdapter) {
                this.mListAdapter = (ListAdapter) spinnerAdapter2;
            }
            if (theme2 == null) {
                return;
            }
            if (AppCompatSpinner.IS_AT_LEAST_M && (spinnerAdapter2 instanceof ThemedSpinnerAdapter)) {
                ThemedSpinnerAdapter themedSpinnerAdapter = (ThemedSpinnerAdapter) spinnerAdapter2;
                if (themedSpinnerAdapter.getDropDownViewTheme() != theme2) {
                    themedSpinnerAdapter.setDropDownViewTheme(theme2);
                }
            } else if (spinnerAdapter2 instanceof ThemedSpinnerAdapter) {
                ThemedSpinnerAdapter themedSpinnerAdapter2 = (ThemedSpinnerAdapter) spinnerAdapter2;
                if (themedSpinnerAdapter2.getDropDownViewTheme() == null) {
                    themedSpinnerAdapter2.setDropDownViewTheme(theme2);
                }
            }
        }

        public int getCount() {
            return this.mAdapter == null ? 0 : this.mAdapter.getCount();
        }

        public Object getItem(int i) {
            return this.mAdapter == null ? null : this.mAdapter.getItem(i);
        }

        public long getItemId(int i) {
            return this.mAdapter == null ? -1 : this.mAdapter.getItemId(i);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            return getDropDownView(i, view, viewGroup);
        }

        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            return this.mAdapter == null ? null : this.mAdapter.getDropDownView(i, view, viewGroup);
        }

        public boolean hasStableIds() {
            return this.mAdapter != null && this.mAdapter.hasStableIds();
        }

        public void registerDataSetObserver(DataSetObserver dataSetObserver) {
            DataSetObserver dataSetObserver2 = dataSetObserver;
            if (this.mAdapter != null) {
                this.mAdapter.registerDataSetObserver(dataSetObserver2);
            }
        }

        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
            DataSetObserver dataSetObserver2 = dataSetObserver;
            if (this.mAdapter != null) {
                this.mAdapter.unregisterDataSetObserver(dataSetObserver2);
            }
        }

        public boolean areAllItemsEnabled() {
            ListAdapter listAdapter = this.mListAdapter;
            if (listAdapter != null) {
                return listAdapter.areAllItemsEnabled();
            }
            return true;
        }

        public boolean isEnabled(int i) {
            int i2 = i;
            ListAdapter listAdapter = this.mListAdapter;
            if (listAdapter != null) {
                return listAdapter.isEnabled(i2);
            }
            return true;
        }

        public int getItemViewType(int i) {
            return 0;
        }

        public int getViewTypeCount() {
            return 1;
        }

        public boolean isEmpty() {
            return getCount() == 0;
        }
    }

    private class DropdownPopup extends ListPopupWindow {
        /* access modifiers changed from: private */
        public ListAdapter mAdapter;
        private CharSequence mHintText;
        private final Rect mVisibleRect;
        final /* synthetic */ AppCompatSpinner this$0;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public DropdownPopup(AppCompatSpinner appCompatSpinner, Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
            Rect rect;
            AdapterView.OnItemClickListener onItemClickListener;
            AppCompatSpinner appCompatSpinner2 = appCompatSpinner;
            this.this$0 = appCompatSpinner2;
            new Rect();
            this.mVisibleRect = rect;
            setAnchorView(appCompatSpinner2);
            setModal(true);
            setPromptPosition(0);
            final AppCompatSpinner appCompatSpinner3 = appCompatSpinner2;
            new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    View view2 = view;
                    int i2 = i;
                    DropdownPopup.this.this$0.setSelection(i2);
                    if (DropdownPopup.this.this$0.getOnItemClickListener() != null) {
                        boolean performItemClick = DropdownPopup.this.this$0.performItemClick(view2, i2, DropdownPopup.this.mAdapter.getItemId(i2));
                    }
                    DropdownPopup.this.dismiss();
                }
            };
            setOnItemClickListener(onItemClickListener);
        }

        public void setAdapter(ListAdapter listAdapter) {
            ListAdapter listAdapter2 = listAdapter;
            super.setAdapter(listAdapter2);
            this.mAdapter = listAdapter2;
        }

        public CharSequence getHintText() {
            return this.mHintText;
        }

        public void setPromptText(CharSequence charSequence) {
            this.mHintText = charSequence;
        }

        /* access modifiers changed from: package-private */
        public void computeContentWidth() {
            int i;
            Drawable background = getBackground();
            int i2 = 0;
            if (background != null) {
                boolean padding = background.getPadding(this.this$0.mTempRect);
                i2 = ViewUtils.isLayoutRtl(this.this$0) ? this.this$0.mTempRect.right : -this.this$0.mTempRect.left;
            } else {
                Rect access$300 = this.this$0.mTempRect;
                this.this$0.mTempRect.right = 0;
                access$300.left = 0;
            }
            int paddingLeft = this.this$0.getPaddingLeft();
            int paddingRight = this.this$0.getPaddingRight();
            int width = this.this$0.getWidth();
            if (this.this$0.mDropDownWidth == -2) {
                int access$500 = this.this$0.compatMeasureContentWidth((SpinnerAdapter) this.mAdapter, getBackground());
                int i3 = (this.this$0.getContext().getResources().getDisplayMetrics().widthPixels - this.this$0.mTempRect.left) - this.this$0.mTempRect.right;
                if (access$500 > i3) {
                    access$500 = i3;
                }
                setContentWidth(Math.max(access$500, (width - paddingLeft) - paddingRight));
            } else if (this.this$0.mDropDownWidth == -1) {
                setContentWidth((width - paddingLeft) - paddingRight);
            } else {
                setContentWidth(this.this$0.mDropDownWidth);
            }
            if (ViewUtils.isLayoutRtl(this.this$0)) {
                i = i2 + ((width - paddingRight) - getWidth());
            } else {
                i = i2 + paddingLeft;
            }
            setHorizontalOffset(i);
        }

        public void show() {
            ViewTreeObserver viewTreeObserver;
            ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener;
            PopupWindow.OnDismissListener onDismissListener;
            boolean isShowing = isShowing();
            computeContentWidth();
            setInputMethodMode(2);
            super.show();
            getListView().setChoiceMode(1);
            setSelection(this.this$0.getSelectedItemPosition());
            if (!isShowing && (viewTreeObserver = this.this$0.getViewTreeObserver()) != null) {
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        if (!DropdownPopup.this.isVisibleToUser(DropdownPopup.this.this$0)) {
                            DropdownPopup.this.dismiss();
                            return;
                        }
                        DropdownPopup.this.computeContentWidth();
                        DropdownPopup.super.show();
                    }
                };
                ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener2 = onGlobalLayoutListener;
                viewTreeObserver.addOnGlobalLayoutListener(onGlobalLayoutListener2);
                final ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener3 = onGlobalLayoutListener2;
                new PopupWindow.OnDismissListener() {
                    public void onDismiss() {
                        ViewTreeObserver viewTreeObserver = DropdownPopup.this.this$0.getViewTreeObserver();
                        if (viewTreeObserver != null) {
                            viewTreeObserver.removeGlobalOnLayoutListener(onGlobalLayoutListener3);
                        }
                    }
                };
                setOnDismissListener(onDismissListener);
            }
        }

        /* access modifiers changed from: private */
        public boolean isVisibleToUser(View view) {
            View view2 = view;
            return ViewCompat.isAttachedToWindow(view2) && view2.getGlobalVisibleRect(this.mVisibleRect);
        }
    }
}
