package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityRecordCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

public class LinearLayoutManager extends RecyclerView.LayoutManager implements ItemTouchHelper.ViewDropHandler {
    private static final boolean DEBUG = false;
    public static final int HORIZONTAL = 0;
    public static final int INVALID_OFFSET = Integer.MIN_VALUE;
    private static final float MAX_SCROLL_FACTOR = 0.33f;
    private static final String TAG = "LinearLayoutManager";
    public static final int VERTICAL = 1;
    final AnchorInfo mAnchorInfo;
    private boolean mLastStackFromEnd;
    private LayoutState mLayoutState;
    int mOrientation;
    OrientationHelper mOrientationHelper;
    SavedState mPendingSavedState;
    int mPendingScrollPosition;
    int mPendingScrollPositionOffset;
    private boolean mRecycleChildrenOnDetach;
    private boolean mReverseLayout;
    boolean mShouldReverseLayout;
    private boolean mSmoothScrollbarEnabled;
    private boolean mStackFromEnd;

    public LinearLayoutManager(Context context) {
        this(context, 1, false);
    }

    public LinearLayoutManager(Context context, int i, boolean z) {
        AnchorInfo anchorInfo;
        this.mReverseLayout = false;
        this.mShouldReverseLayout = false;
        this.mStackFromEnd = false;
        this.mSmoothScrollbarEnabled = true;
        this.mPendingScrollPosition = -1;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        this.mPendingSavedState = null;
        new AnchorInfo();
        this.mAnchorInfo = anchorInfo;
        setOrientation(i);
        setReverseLayout(z);
    }

    public LinearLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        AnchorInfo anchorInfo;
        this.mReverseLayout = false;
        this.mShouldReverseLayout = false;
        this.mStackFromEnd = false;
        this.mSmoothScrollbarEnabled = true;
        this.mPendingScrollPosition = -1;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        this.mPendingSavedState = null;
        new AnchorInfo();
        this.mAnchorInfo = anchorInfo;
        RecyclerView.LayoutManager.Properties properties = getProperties(context, attributeSet, i, i2);
        setOrientation(properties.orientation);
        setReverseLayout(properties.reverseLayout);
        setStackFromEnd(properties.stackFromEnd);
    }

    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        RecyclerView.LayoutParams layoutParams;
        new RecyclerView.LayoutParams(-2, -2);
        return layoutParams;
    }

    public boolean getRecycleChildrenOnDetach() {
        return this.mRecycleChildrenOnDetach;
    }

    public void setRecycleChildrenOnDetach(boolean z) {
        this.mRecycleChildrenOnDetach = z;
    }

    public void onDetachedFromWindow(RecyclerView recyclerView, RecyclerView.Recycler recycler) {
        RecyclerView.Recycler recycler2 = recycler;
        super.onDetachedFromWindow(recyclerView, recycler2);
        if (this.mRecycleChildrenOnDetach) {
            removeAndRecycleAllViews(recycler2);
            recycler2.clear();
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
        super.onInitializeAccessibilityEvent(accessibilityEvent2);
        if (getChildCount() > 0) {
            AccessibilityRecordCompat asRecord = AccessibilityEventCompat.asRecord(accessibilityEvent2);
            asRecord.setFromIndex(findFirstVisibleItemPosition());
            asRecord.setToIndex(findLastVisibleItemPosition());
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState;
        Parcelable parcelable;
        if (this.mPendingSavedState != null) {
            new SavedState(this.mPendingSavedState);
            return parcelable;
        }
        new SavedState();
        SavedState savedState2 = savedState;
        if (getChildCount() > 0) {
            ensureLayoutState();
            boolean z = this.mLastStackFromEnd ^ this.mShouldReverseLayout;
            savedState2.mAnchorLayoutFromEnd = z;
            if (z) {
                View childClosestToEnd = getChildClosestToEnd();
                savedState2.mAnchorOffset = this.mOrientationHelper.getEndAfterPadding() - this.mOrientationHelper.getDecoratedEnd(childClosestToEnd);
                savedState2.mAnchorPosition = getPosition(childClosestToEnd);
            } else {
                View childClosestToStart = getChildClosestToStart();
                savedState2.mAnchorPosition = getPosition(childClosestToStart);
                savedState2.mAnchorOffset = this.mOrientationHelper.getDecoratedStart(childClosestToStart) - this.mOrientationHelper.getStartAfterPadding();
            }
        } else {
            savedState2.invalidateAnchor();
        }
        return savedState2;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2 = parcelable;
        if (parcelable2 instanceof SavedState) {
            this.mPendingSavedState = (SavedState) parcelable2;
            requestLayout();
        }
    }

    public boolean canScrollHorizontally() {
        return this.mOrientation == 0;
    }

    public boolean canScrollVertically() {
        return this.mOrientation == 1;
    }

    public void setStackFromEnd(boolean z) {
        boolean z2 = z;
        assertNotInLayoutOrScroll(null);
        if (this.mStackFromEnd != z2) {
            this.mStackFromEnd = z2;
            requestLayout();
        }
    }

    public boolean getStackFromEnd() {
        return this.mStackFromEnd;
    }

    public int getOrientation() {
        return this.mOrientation;
    }

    public void setOrientation(int i) {
        Throwable th;
        StringBuilder sb;
        int i2 = i;
        if (i2 == 0 || i2 == 1) {
            assertNotInLayoutOrScroll(null);
            if (i2 != this.mOrientation) {
                this.mOrientation = i2;
                this.mOrientationHelper = null;
                requestLayout();
                return;
            }
            return;
        }
        Throwable th2 = th;
        new StringBuilder();
        new IllegalArgumentException(sb.append("invalid orientation:").append(i2).toString());
        throw th2;
    }

    private void resolveShouldLayoutReverse() {
        if (this.mOrientation == 1 || !isLayoutRTL()) {
            this.mShouldReverseLayout = this.mReverseLayout;
        } else {
            this.mShouldReverseLayout = !this.mReverseLayout;
        }
    }

    public boolean getReverseLayout() {
        return this.mReverseLayout;
    }

    public void setReverseLayout(boolean z) {
        boolean z2 = z;
        assertNotInLayoutOrScroll(null);
        if (z2 != this.mReverseLayout) {
            this.mReverseLayout = z2;
            requestLayout();
        }
    }

    public View findViewByPosition(int i) {
        int i2 = i;
        int childCount = getChildCount();
        if (childCount == 0) {
            return null;
        }
        int position = i2 - getPosition(getChildAt(0));
        if (position >= 0 && position < childCount) {
            View childAt = getChildAt(position);
            if (getPosition(childAt) == i2) {
                return childAt;
            }
        }
        return super.findViewByPosition(i2);
    }

    /* access modifiers changed from: protected */
    public int getExtraLayoutSpace(RecyclerView.State state) {
        if (state.hasTargetScrollPosition()) {
            return this.mOrientationHelper.getTotalSpace();
        }
        return 0;
    }

    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int i) {
        LinearSmoothScroller linearSmoothScroller;
        new LinearSmoothScroller(recyclerView.getContext()) {
            public PointF computeScrollVectorForPosition(int i) {
                return LinearLayoutManager.this.computeScrollVectorForPosition(i);
            }
        };
        LinearSmoothScroller linearSmoothScroller2 = linearSmoothScroller;
        linearSmoothScroller2.setTargetPosition(i);
        startSmoothScroll(linearSmoothScroller2);
    }

    public PointF computeScrollVectorForPosition(int i) {
        PointF pointF;
        PointF pointF2;
        int i2 = i;
        if (getChildCount() == 0) {
            return null;
        }
        int i3 = (i2 < getPosition(getChildAt(0))) != this.mShouldReverseLayout ? -1 : 1;
        if (this.mOrientation == 0) {
            new PointF((float) i3, 0.0f);
            return pointF2;
        }
        new PointF(0.0f, (float) i3);
        return pointF;
    }

    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        int i;
        int i2;
        int i3;
        int i4;
        View findViewByPosition;
        int decoratedStart;
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        if (!(this.mPendingSavedState == null && this.mPendingScrollPosition == -1) && state2.getItemCount() == 0) {
            removeAndRecycleAllViews(recycler2);
            return;
        }
        if (this.mPendingSavedState != null && this.mPendingSavedState.hasValidAnchor()) {
            this.mPendingScrollPosition = this.mPendingSavedState.mAnchorPosition;
        }
        ensureLayoutState();
        this.mLayoutState.mRecycle = false;
        resolveShouldLayoutReverse();
        this.mAnchorInfo.reset();
        this.mAnchorInfo.mLayoutFromEnd = this.mShouldReverseLayout ^ this.mStackFromEnd;
        updateAnchorInfoForLayout(recycler2, state2, this.mAnchorInfo);
        int extraLayoutSpace = getExtraLayoutSpace(state2);
        if (this.mLayoutState.mLastScrollDelta >= 0) {
            i2 = extraLayoutSpace;
            i = 0;
        } else {
            i = extraLayoutSpace;
            i2 = 0;
        }
        int startAfterPadding = i + this.mOrientationHelper.getStartAfterPadding();
        int endPadding = i2 + this.mOrientationHelper.getEndPadding();
        if (!(!state2.isPreLayout() || this.mPendingScrollPosition == -1 || this.mPendingScrollPositionOffset == Integer.MIN_VALUE || (findViewByPosition = findViewByPosition(this.mPendingScrollPosition)) == null)) {
            if (this.mShouldReverseLayout) {
                decoratedStart = (this.mOrientationHelper.getEndAfterPadding() - this.mOrientationHelper.getDecoratedEnd(findViewByPosition)) - this.mPendingScrollPositionOffset;
            } else {
                decoratedStart = this.mPendingScrollPositionOffset - (this.mOrientationHelper.getDecoratedStart(findViewByPosition) - this.mOrientationHelper.getStartAfterPadding());
            }
            if (decoratedStart > 0) {
                startAfterPadding += decoratedStart;
            } else {
                endPadding -= decoratedStart;
            }
        }
        onAnchorReady(recycler2, state2, this.mAnchorInfo);
        detachAndScrapAttachedViews(recycler2);
        this.mLayoutState.mIsPreLayout = state2.isPreLayout();
        if (this.mAnchorInfo.mLayoutFromEnd) {
            updateLayoutStateToFillStart(this.mAnchorInfo);
            this.mLayoutState.mExtra = startAfterPadding;
            int fill = fill(recycler2, this.mLayoutState, state2, false);
            i4 = this.mLayoutState.mOffset;
            int i5 = this.mLayoutState.mCurrentPosition;
            if (this.mLayoutState.mAvailable > 0) {
                endPadding += this.mLayoutState.mAvailable;
            }
            updateLayoutStateToFillEnd(this.mAnchorInfo);
            this.mLayoutState.mExtra = endPadding;
            this.mLayoutState.mCurrentPosition += this.mLayoutState.mItemDirection;
            int fill2 = fill(recycler2, this.mLayoutState, state2, false);
            i3 = this.mLayoutState.mOffset;
            if (this.mLayoutState.mAvailable > 0) {
                int i6 = this.mLayoutState.mAvailable;
                updateLayoutStateToFillStart(i5, i4);
                this.mLayoutState.mExtra = i6;
                int fill3 = fill(recycler2, this.mLayoutState, state2, false);
                i4 = this.mLayoutState.mOffset;
            }
        } else {
            updateLayoutStateToFillEnd(this.mAnchorInfo);
            this.mLayoutState.mExtra = endPadding;
            int fill4 = fill(recycler2, this.mLayoutState, state2, false);
            i3 = this.mLayoutState.mOffset;
            int i7 = this.mLayoutState.mCurrentPosition;
            if (this.mLayoutState.mAvailable > 0) {
                startAfterPadding += this.mLayoutState.mAvailable;
            }
            updateLayoutStateToFillStart(this.mAnchorInfo);
            this.mLayoutState.mExtra = startAfterPadding;
            this.mLayoutState.mCurrentPosition += this.mLayoutState.mItemDirection;
            int fill5 = fill(recycler2, this.mLayoutState, state2, false);
            i4 = this.mLayoutState.mOffset;
            if (this.mLayoutState.mAvailable > 0) {
                int i8 = this.mLayoutState.mAvailable;
                updateLayoutStateToFillEnd(i7, i3);
                this.mLayoutState.mExtra = i8;
                int fill6 = fill(recycler2, this.mLayoutState, state2, false);
                i3 = this.mLayoutState.mOffset;
            }
        }
        if (getChildCount() > 0) {
            if (this.mShouldReverseLayout ^ this.mStackFromEnd) {
                int fixLayoutEndGap = fixLayoutEndGap(i3, recycler2, state2, true);
                int i9 = i4 + fixLayoutEndGap;
                int i10 = i3 + fixLayoutEndGap;
                int fixLayoutStartGap = fixLayoutStartGap(i9, recycler2, state2, false);
                i4 = i9 + fixLayoutStartGap;
                i3 = i10 + fixLayoutStartGap;
            } else {
                int fixLayoutStartGap2 = fixLayoutStartGap(i4, recycler2, state2, true);
                int i11 = i4 + fixLayoutStartGap2;
                int i12 = i3 + fixLayoutStartGap2;
                int fixLayoutEndGap2 = fixLayoutEndGap(i12, recycler2, state2, false);
                i4 = i11 + fixLayoutEndGap2;
                i3 = i12 + fixLayoutEndGap2;
            }
        }
        layoutForPredictiveAnimations(recycler2, state2, i4, i3);
        if (!state2.isPreLayout()) {
            this.mPendingScrollPosition = -1;
            this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
            this.mOrientationHelper.onLayoutComplete();
        }
        this.mLastStackFromEnd = this.mStackFromEnd;
        this.mPendingSavedState = null;
    }

    /* access modifiers changed from: package-private */
    public void onAnchorReady(RecyclerView.Recycler recycler, RecyclerView.State state, AnchorInfo anchorInfo) {
    }

    private void layoutForPredictiveAnimations(RecyclerView.Recycler recycler, RecyclerView.State state, int i, int i2) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        int i3 = i;
        int i4 = i2;
        if (state2.willRunPredictiveAnimations() && getChildCount() != 0 && !state2.isPreLayout() && supportsPredictiveItemAnimations()) {
            int i5 = 0;
            int i6 = 0;
            List<RecyclerView.ViewHolder> scrapList = recycler2.getScrapList();
            int size = scrapList.size();
            int position = getPosition(getChildAt(0));
            for (int i7 = 0; i7 < size; i7++) {
                RecyclerView.ViewHolder viewHolder = scrapList.get(i7);
                if (!viewHolder.isRemoved()) {
                    if (((viewHolder.getLayoutPosition() < position) != this.mShouldReverseLayout ? (char) 65535 : 1) == 65535) {
                        i5 += this.mOrientationHelper.getDecoratedMeasurement(viewHolder.itemView);
                    } else {
                        i6 += this.mOrientationHelper.getDecoratedMeasurement(viewHolder.itemView);
                    }
                }
            }
            this.mLayoutState.mScrapList = scrapList;
            if (i5 > 0) {
                updateLayoutStateToFillStart(getPosition(getChildClosestToStart()), i3);
                this.mLayoutState.mExtra = i5;
                this.mLayoutState.mAvailable = 0;
                this.mLayoutState.assignPositionFromScrapList();
                int fill = fill(recycler2, this.mLayoutState, state2, false);
            }
            if (i6 > 0) {
                updateLayoutStateToFillEnd(getPosition(getChildClosestToEnd()), i4);
                this.mLayoutState.mExtra = i6;
                this.mLayoutState.mAvailable = 0;
                this.mLayoutState.assignPositionFromScrapList();
                int fill2 = fill(recycler2, this.mLayoutState, state2, false);
            }
            this.mLayoutState.mScrapList = null;
        }
    }

    private void updateAnchorInfoForLayout(RecyclerView.Recycler recycler, RecyclerView.State state, AnchorInfo anchorInfo) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        AnchorInfo anchorInfo2 = anchorInfo;
        if (!updateAnchorFromPendingData(state2, anchorInfo2) && !updateAnchorFromChildren(recycler2, state2, anchorInfo2)) {
            anchorInfo2.assignCoordinateFromPadding();
            anchorInfo2.mPosition = this.mStackFromEnd ? state2.getItemCount() - 1 : 0;
        }
    }

    private boolean updateAnchorFromChildren(RecyclerView.Recycler recycler, RecyclerView.State state, AnchorInfo anchorInfo) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        AnchorInfo anchorInfo2 = anchorInfo;
        if (getChildCount() == 0) {
            return false;
        }
        View focusedChild = getFocusedChild();
        if (focusedChild != null && anchorInfo2.isViewValidAsAnchor(focusedChild, state2)) {
            anchorInfo2.assignFromViewAndKeepVisibleRect(focusedChild);
            return true;
        } else if (this.mLastStackFromEnd != this.mStackFromEnd) {
            return false;
        } else {
            View findReferenceChildClosestToEnd = anchorInfo2.mLayoutFromEnd ? findReferenceChildClosestToEnd(recycler2, state2) : findReferenceChildClosestToStart(recycler2, state2);
            if (findReferenceChildClosestToEnd == null) {
                return false;
            }
            anchorInfo2.assignFromView(findReferenceChildClosestToEnd);
            if (!state2.isPreLayout() && supportsPredictiveItemAnimations()) {
                if (this.mOrientationHelper.getDecoratedStart(findReferenceChildClosestToEnd) >= this.mOrientationHelper.getEndAfterPadding() || this.mOrientationHelper.getDecoratedEnd(findReferenceChildClosestToEnd) < this.mOrientationHelper.getStartAfterPadding()) {
                    anchorInfo2.mCoordinate = anchorInfo2.mLayoutFromEnd ? this.mOrientationHelper.getEndAfterPadding() : this.mOrientationHelper.getStartAfterPadding();
                }
            }
            return true;
        }
    }

    private boolean updateAnchorFromPendingData(RecyclerView.State state, AnchorInfo anchorInfo) {
        int decoratedStart;
        RecyclerView.State state2 = state;
        AnchorInfo anchorInfo2 = anchorInfo;
        if (state2.isPreLayout() || this.mPendingScrollPosition == -1) {
            return false;
        }
        if (this.mPendingScrollPosition < 0 || this.mPendingScrollPosition >= state2.getItemCount()) {
            this.mPendingScrollPosition = -1;
            this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
            return false;
        }
        anchorInfo2.mPosition = this.mPendingScrollPosition;
        if (this.mPendingSavedState != null && this.mPendingSavedState.hasValidAnchor()) {
            anchorInfo2.mLayoutFromEnd = this.mPendingSavedState.mAnchorLayoutFromEnd;
            if (anchorInfo2.mLayoutFromEnd) {
                anchorInfo2.mCoordinate = this.mOrientationHelper.getEndAfterPadding() - this.mPendingSavedState.mAnchorOffset;
            } else {
                anchorInfo2.mCoordinate = this.mOrientationHelper.getStartAfterPadding() + this.mPendingSavedState.mAnchorOffset;
            }
            return true;
        } else if (this.mPendingScrollPositionOffset == Integer.MIN_VALUE) {
            View findViewByPosition = findViewByPosition(this.mPendingScrollPosition);
            if (findViewByPosition == null) {
                if (getChildCount() > 0) {
                    int position = getPosition(getChildAt(0));
                    anchorInfo2.mLayoutFromEnd = (this.mPendingScrollPosition < position) == this.mShouldReverseLayout;
                }
                anchorInfo2.assignCoordinateFromPadding();
            } else if (this.mOrientationHelper.getDecoratedMeasurement(findViewByPosition) > this.mOrientationHelper.getTotalSpace()) {
                anchorInfo2.assignCoordinateFromPadding();
                return true;
            } else if (this.mOrientationHelper.getDecoratedStart(findViewByPosition) - this.mOrientationHelper.getStartAfterPadding() < 0) {
                anchorInfo2.mCoordinate = this.mOrientationHelper.getStartAfterPadding();
                anchorInfo2.mLayoutFromEnd = false;
                return true;
            } else if (this.mOrientationHelper.getEndAfterPadding() - this.mOrientationHelper.getDecoratedEnd(findViewByPosition) < 0) {
                anchorInfo2.mCoordinate = this.mOrientationHelper.getEndAfterPadding();
                anchorInfo2.mLayoutFromEnd = true;
                return true;
            } else {
                AnchorInfo anchorInfo3 = anchorInfo2;
                if (anchorInfo2.mLayoutFromEnd) {
                    decoratedStart = this.mOrientationHelper.getDecoratedEnd(findViewByPosition) + this.mOrientationHelper.getTotalSpaceChange();
                } else {
                    decoratedStart = this.mOrientationHelper.getDecoratedStart(findViewByPosition);
                }
                anchorInfo3.mCoordinate = decoratedStart;
            }
            return true;
        } else {
            anchorInfo2.mLayoutFromEnd = this.mShouldReverseLayout;
            if (this.mShouldReverseLayout) {
                anchorInfo2.mCoordinate = this.mOrientationHelper.getEndAfterPadding() - this.mPendingScrollPositionOffset;
            } else {
                anchorInfo2.mCoordinate = this.mOrientationHelper.getStartAfterPadding() + this.mPendingScrollPositionOffset;
            }
            return true;
        }
    }

    private int fixLayoutEndGap(int i, RecyclerView.Recycler recycler, RecyclerView.State state, boolean z) {
        int endAfterPadding;
        int i2 = i;
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        boolean z2 = z;
        int endAfterPadding2 = this.mOrientationHelper.getEndAfterPadding() - i2;
        if (endAfterPadding2 <= 0) {
            return 0;
        }
        int i3 = -scrollBy(-endAfterPadding2, recycler2, state2);
        int i4 = i2 + i3;
        if (!z2 || (endAfterPadding = this.mOrientationHelper.getEndAfterPadding() - i4) <= 0) {
            return i3;
        }
        this.mOrientationHelper.offsetChildren(endAfterPadding);
        return endAfterPadding + i3;
    }

    private int fixLayoutStartGap(int i, RecyclerView.Recycler recycler, RecyclerView.State state, boolean z) {
        int startAfterPadding;
        int i2 = i;
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        boolean z2 = z;
        int startAfterPadding2 = i2 - this.mOrientationHelper.getStartAfterPadding();
        if (startAfterPadding2 <= 0) {
            return 0;
        }
        int i3 = -scrollBy(startAfterPadding2, recycler2, state2);
        int i4 = i2 + i3;
        if (!z2 || (startAfterPadding = i4 - this.mOrientationHelper.getStartAfterPadding()) <= 0) {
            return i3;
        }
        this.mOrientationHelper.offsetChildren(-startAfterPadding);
        return i3 - startAfterPadding;
    }

    private void updateLayoutStateToFillEnd(AnchorInfo anchorInfo) {
        AnchorInfo anchorInfo2 = anchorInfo;
        updateLayoutStateToFillEnd(anchorInfo2.mPosition, anchorInfo2.mCoordinate);
    }

    private void updateLayoutStateToFillEnd(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        this.mLayoutState.mAvailable = this.mOrientationHelper.getEndAfterPadding() - i4;
        this.mLayoutState.mItemDirection = this.mShouldReverseLayout ? -1 : 1;
        this.mLayoutState.mCurrentPosition = i3;
        this.mLayoutState.mLayoutDirection = 1;
        this.mLayoutState.mOffset = i4;
        this.mLayoutState.mScrollingOffset = Integer.MIN_VALUE;
    }

    private void updateLayoutStateToFillStart(AnchorInfo anchorInfo) {
        AnchorInfo anchorInfo2 = anchorInfo;
        updateLayoutStateToFillStart(anchorInfo2.mPosition, anchorInfo2.mCoordinate);
    }

    private void updateLayoutStateToFillStart(int i, int i2) {
        int i3 = i2;
        this.mLayoutState.mAvailable = i3 - this.mOrientationHelper.getStartAfterPadding();
        this.mLayoutState.mCurrentPosition = i;
        this.mLayoutState.mItemDirection = this.mShouldReverseLayout ? 1 : -1;
        this.mLayoutState.mLayoutDirection = -1;
        this.mLayoutState.mOffset = i3;
        this.mLayoutState.mScrollingOffset = Integer.MIN_VALUE;
    }

    /* access modifiers changed from: protected */
    public boolean isLayoutRTL() {
        return getLayoutDirection() == 1;
    }

    /* access modifiers changed from: package-private */
    public void ensureLayoutState() {
        if (this.mLayoutState == null) {
            this.mLayoutState = createLayoutState();
        }
        if (this.mOrientationHelper == null) {
            this.mOrientationHelper = OrientationHelper.createOrientationHelper(this, this.mOrientation);
        }
    }

    /* access modifiers changed from: package-private */
    public LayoutState createLayoutState() {
        LayoutState layoutState;
        new LayoutState();
        return layoutState;
    }

    public void scrollToPosition(int i) {
        this.mPendingScrollPosition = i;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        if (this.mPendingSavedState != null) {
            this.mPendingSavedState.invalidateAnchor();
        }
        requestLayout();
    }

    public void scrollToPositionWithOffset(int i, int i2) {
        this.mPendingScrollPosition = i;
        this.mPendingScrollPositionOffset = i2;
        if (this.mPendingSavedState != null) {
            this.mPendingSavedState.invalidateAnchor();
        }
        requestLayout();
    }

    public int scrollHorizontallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        int i2 = i;
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        if (this.mOrientation == 1) {
            return 0;
        }
        return scrollBy(i2, recycler2, state2);
    }

    public int scrollVerticallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        int i2 = i;
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        if (this.mOrientation == 0) {
            return 0;
        }
        return scrollBy(i2, recycler2, state2);
    }

    public int computeHorizontalScrollOffset(RecyclerView.State state) {
        return computeScrollOffset(state);
    }

    public int computeVerticalScrollOffset(RecyclerView.State state) {
        return computeScrollOffset(state);
    }

    public int computeHorizontalScrollExtent(RecyclerView.State state) {
        return computeScrollExtent(state);
    }

    public int computeVerticalScrollExtent(RecyclerView.State state) {
        return computeScrollExtent(state);
    }

    public int computeHorizontalScrollRange(RecyclerView.State state) {
        return computeScrollRange(state);
    }

    public int computeVerticalScrollRange(RecyclerView.State state) {
        return computeScrollRange(state);
    }

    private int computeScrollOffset(RecyclerView.State state) {
        RecyclerView.State state2 = state;
        if (getChildCount() == 0) {
            return 0;
        }
        ensureLayoutState();
        return ScrollbarHelper.computeScrollOffset(state2, this.mOrientationHelper, findFirstVisibleChildClosestToStart(!this.mSmoothScrollbarEnabled, true), findFirstVisibleChildClosestToEnd(!this.mSmoothScrollbarEnabled, true), this, this.mSmoothScrollbarEnabled, this.mShouldReverseLayout);
    }

    private int computeScrollExtent(RecyclerView.State state) {
        RecyclerView.State state2 = state;
        if (getChildCount() == 0) {
            return 0;
        }
        ensureLayoutState();
        return ScrollbarHelper.computeScrollExtent(state2, this.mOrientationHelper, findFirstVisibleChildClosestToStart(!this.mSmoothScrollbarEnabled, true), findFirstVisibleChildClosestToEnd(!this.mSmoothScrollbarEnabled, true), this, this.mSmoothScrollbarEnabled);
    }

    private int computeScrollRange(RecyclerView.State state) {
        RecyclerView.State state2 = state;
        if (getChildCount() == 0) {
            return 0;
        }
        ensureLayoutState();
        return ScrollbarHelper.computeScrollRange(state2, this.mOrientationHelper, findFirstVisibleChildClosestToStart(!this.mSmoothScrollbarEnabled, true), findFirstVisibleChildClosestToEnd(!this.mSmoothScrollbarEnabled, true), this, this.mSmoothScrollbarEnabled);
    }

    public void setSmoothScrollbarEnabled(boolean z) {
        this.mSmoothScrollbarEnabled = z;
    }

    public boolean isSmoothScrollbarEnabled() {
        return this.mSmoothScrollbarEnabled;
    }

    private void updateLayoutState(int i, int i2, boolean z, RecyclerView.State state) {
        int startAfterPadding;
        int i3 = i;
        int i4 = i2;
        boolean z2 = z;
        this.mLayoutState.mExtra = getExtraLayoutSpace(state);
        this.mLayoutState.mLayoutDirection = i3;
        if (i3 == 1) {
            this.mLayoutState.mExtra += this.mOrientationHelper.getEndPadding();
            View childClosestToEnd = getChildClosestToEnd();
            this.mLayoutState.mItemDirection = this.mShouldReverseLayout ? -1 : 1;
            this.mLayoutState.mCurrentPosition = getPosition(childClosestToEnd) + this.mLayoutState.mItemDirection;
            this.mLayoutState.mOffset = this.mOrientationHelper.getDecoratedEnd(childClosestToEnd);
            startAfterPadding = this.mOrientationHelper.getDecoratedEnd(childClosestToEnd) - this.mOrientationHelper.getEndAfterPadding();
        } else {
            View childClosestToStart = getChildClosestToStart();
            this.mLayoutState.mExtra += this.mOrientationHelper.getStartAfterPadding();
            this.mLayoutState.mItemDirection = this.mShouldReverseLayout ? 1 : -1;
            this.mLayoutState.mCurrentPosition = getPosition(childClosestToStart) + this.mLayoutState.mItemDirection;
            this.mLayoutState.mOffset = this.mOrientationHelper.getDecoratedStart(childClosestToStart);
            startAfterPadding = (-this.mOrientationHelper.getDecoratedStart(childClosestToStart)) + this.mOrientationHelper.getStartAfterPadding();
        }
        this.mLayoutState.mAvailable = i4;
        if (z2) {
            this.mLayoutState.mAvailable -= startAfterPadding;
        }
        this.mLayoutState.mScrollingOffset = startAfterPadding;
    }

    /* access modifiers changed from: package-private */
    public int scrollBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        int i2 = i;
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        if (getChildCount() == 0 || i2 == 0) {
            return 0;
        }
        this.mLayoutState.mRecycle = true;
        ensureLayoutState();
        int i3 = i2 > 0 ? 1 : -1;
        int abs = Math.abs(i2);
        updateLayoutState(i3, abs, true, state2);
        int fill = this.mLayoutState.mScrollingOffset + fill(recycler2, this.mLayoutState, state2, false);
        if (fill < 0) {
            return 0;
        }
        int i4 = abs > fill ? i3 * fill : i2;
        this.mOrientationHelper.offsetChildren(-i4);
        this.mLayoutState.mLastScrollDelta = i4;
        return i4;
    }

    public void assertNotInLayoutOrScroll(String str) {
        String str2 = str;
        if (this.mPendingSavedState == null) {
            super.assertNotInLayoutOrScroll(str2);
        }
    }

    private void recycleChildren(RecyclerView.Recycler recycler, int i, int i2) {
        RecyclerView.Recycler recycler2 = recycler;
        int i3 = i;
        int i4 = i2;
        if (i3 != i4) {
            if (i4 > i3) {
                for (int i5 = i4 - 1; i5 >= i3; i5--) {
                    removeAndRecycleViewAt(i5, recycler2);
                }
                return;
            }
            for (int i6 = i3; i6 > i4; i6--) {
                removeAndRecycleViewAt(i6, recycler2);
            }
        }
    }

    private void recycleViewsFromStart(RecyclerView.Recycler recycler, int i) {
        RecyclerView.Recycler recycler2 = recycler;
        int i2 = i;
        if (i2 >= 0) {
            int i3 = i2;
            int childCount = getChildCount();
            if (this.mShouldReverseLayout) {
                for (int i4 = childCount - 1; i4 >= 0; i4--) {
                    if (this.mOrientationHelper.getDecoratedEnd(getChildAt(i4)) > i3) {
                        recycleChildren(recycler2, childCount - 1, i4);
                        return;
                    }
                }
                return;
            }
            for (int i5 = 0; i5 < childCount; i5++) {
                if (this.mOrientationHelper.getDecoratedEnd(getChildAt(i5)) > i3) {
                    recycleChildren(recycler2, 0, i5);
                    return;
                }
            }
        }
    }

    private void recycleViewsFromEnd(RecyclerView.Recycler recycler, int i) {
        RecyclerView.Recycler recycler2 = recycler;
        int i2 = i;
        int childCount = getChildCount();
        if (i2 >= 0) {
            int end = this.mOrientationHelper.getEnd() - i2;
            if (this.mShouldReverseLayout) {
                for (int i3 = 0; i3 < childCount; i3++) {
                    if (this.mOrientationHelper.getDecoratedStart(getChildAt(i3)) < end) {
                        recycleChildren(recycler2, 0, i3);
                        return;
                    }
                }
                return;
            }
            for (int i4 = childCount - 1; i4 >= 0; i4--) {
                if (this.mOrientationHelper.getDecoratedStart(getChildAt(i4)) < end) {
                    recycleChildren(recycler2, childCount - 1, i4);
                    return;
                }
            }
        }
    }

    private void recycleByLayoutState(RecyclerView.Recycler recycler, LayoutState layoutState) {
        RecyclerView.Recycler recycler2 = recycler;
        LayoutState layoutState2 = layoutState;
        if (layoutState2.mRecycle) {
            if (layoutState2.mLayoutDirection == -1) {
                recycleViewsFromEnd(recycler2, layoutState2.mScrollingOffset);
            } else {
                recycleViewsFromStart(recycler2, layoutState2.mScrollingOffset);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int fill(RecyclerView.Recycler recycler, LayoutState layoutState, RecyclerView.State state, boolean z) {
        LayoutChunkResult layoutChunkResult;
        RecyclerView.Recycler recycler2 = recycler;
        LayoutState layoutState2 = layoutState;
        RecyclerView.State state2 = state;
        boolean z2 = z;
        int i = layoutState2.mAvailable;
        if (layoutState2.mScrollingOffset != Integer.MIN_VALUE) {
            if (layoutState2.mAvailable < 0) {
                layoutState2.mScrollingOffset += layoutState2.mAvailable;
            }
            recycleByLayoutState(recycler2, layoutState2);
        }
        int i2 = layoutState2.mAvailable + layoutState2.mExtra;
        new LayoutChunkResult();
        LayoutChunkResult layoutChunkResult2 = layoutChunkResult;
        while (i2 > 0 && layoutState2.hasMore(state2)) {
            layoutChunkResult2.resetInternal();
            layoutChunk(recycler2, state2, layoutState2, layoutChunkResult2);
            if (!layoutChunkResult2.mFinished) {
                layoutState2.mOffset += layoutChunkResult2.mConsumed * layoutState2.mLayoutDirection;
                if (!layoutChunkResult2.mIgnoreConsumed || this.mLayoutState.mScrapList != null || !state2.isPreLayout()) {
                    layoutState2.mAvailable -= layoutChunkResult2.mConsumed;
                    i2 -= layoutChunkResult2.mConsumed;
                }
                if (layoutState2.mScrollingOffset != Integer.MIN_VALUE) {
                    layoutState2.mScrollingOffset += layoutChunkResult2.mConsumed;
                    if (layoutState2.mAvailable < 0) {
                        layoutState2.mScrollingOffset += layoutState2.mAvailable;
                    }
                    recycleByLayoutState(recycler2, layoutState2);
                }
                if (z2 && layoutChunkResult2.mFocusable) {
                    break;
                }
            } else {
                break;
            }
        }
        return i - layoutState2.mAvailable;
    }

    /* access modifiers changed from: package-private */
    public void layoutChunk(RecyclerView.Recycler recycler, RecyclerView.State state, LayoutState layoutState, LayoutChunkResult layoutChunkResult) {
        int paddingTop;
        int decoratedMeasurementInOther;
        int i;
        int i2;
        LayoutState layoutState2 = layoutState;
        LayoutChunkResult layoutChunkResult2 = layoutChunkResult;
        View next = layoutState2.next(recycler);
        if (next == null) {
            layoutChunkResult2.mFinished = true;
            return;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) next.getLayoutParams();
        if (layoutState2.mScrapList == null) {
            if (this.mShouldReverseLayout == (layoutState2.mLayoutDirection == -1)) {
                addView(next);
            } else {
                addView(next, 0);
            }
        } else {
            if (this.mShouldReverseLayout == (layoutState2.mLayoutDirection == -1)) {
                addDisappearingView(next);
            } else {
                addDisappearingView(next, 0);
            }
        }
        measureChildWithMargins(next, 0, 0);
        layoutChunkResult2.mConsumed = this.mOrientationHelper.getDecoratedMeasurement(next);
        if (this.mOrientation == 1) {
            if (isLayoutRTL()) {
                i2 = getWidth() - getPaddingRight();
                i = i2 - this.mOrientationHelper.getDecoratedMeasurementInOther(next);
            } else {
                i = getPaddingLeft();
                i2 = i + this.mOrientationHelper.getDecoratedMeasurementInOther(next);
            }
            if (layoutState2.mLayoutDirection == -1) {
                decoratedMeasurementInOther = layoutState2.mOffset;
                paddingTop = layoutState2.mOffset - layoutChunkResult2.mConsumed;
            } else {
                paddingTop = layoutState2.mOffset;
                decoratedMeasurementInOther = layoutState2.mOffset + layoutChunkResult2.mConsumed;
            }
        } else {
            paddingTop = getPaddingTop();
            decoratedMeasurementInOther = paddingTop + this.mOrientationHelper.getDecoratedMeasurementInOther(next);
            if (layoutState2.mLayoutDirection == -1) {
                i2 = layoutState2.mOffset;
                i = layoutState2.mOffset - layoutChunkResult2.mConsumed;
            } else {
                i = layoutState2.mOffset;
                i2 = layoutState2.mOffset + layoutChunkResult2.mConsumed;
            }
        }
        layoutDecorated(next, i + layoutParams.leftMargin, paddingTop + layoutParams.topMargin, i2 - layoutParams.rightMargin, decoratedMeasurementInOther - layoutParams.bottomMargin);
        if (layoutParams.isItemRemoved() || layoutParams.isItemChanged()) {
            layoutChunkResult2.mIgnoreConsumed = true;
        }
        layoutChunkResult2.mFocusable = next.isFocusable();
    }

    private int convertFocusDirectionToLayoutDirection(int i) {
        switch (i) {
            case 1:
                return -1;
            case 2:
                return 1;
            case 17:
                return this.mOrientation == 0 ? -1 : Integer.MIN_VALUE;
            case 33:
                return this.mOrientation == 1 ? -1 : Integer.MIN_VALUE;
            case 66:
                return this.mOrientation == 0 ? 1 : Integer.MIN_VALUE;
            case TransportMediator.KEYCODE_MEDIA_RECORD:
                return this.mOrientation == 1 ? 1 : Integer.MIN_VALUE;
            default:
                return Integer.MIN_VALUE;
        }
    }

    private View getChildClosestToStart() {
        return getChildAt(this.mShouldReverseLayout ? getChildCount() - 1 : 0);
    }

    private View getChildClosestToEnd() {
        return getChildAt(this.mShouldReverseLayout ? 0 : getChildCount() - 1);
    }

    private View findFirstVisibleChildClosestToStart(boolean z, boolean z2) {
        boolean z3 = z;
        boolean z4 = z2;
        if (this.mShouldReverseLayout) {
            return findOneVisibleChild(getChildCount() - 1, -1, z3, z4);
        }
        return findOneVisibleChild(0, getChildCount(), z3, z4);
    }

    private View findFirstVisibleChildClosestToEnd(boolean z, boolean z2) {
        boolean z3 = z;
        boolean z4 = z2;
        if (this.mShouldReverseLayout) {
            return findOneVisibleChild(0, getChildCount(), z3, z4);
        }
        return findOneVisibleChild(getChildCount() - 1, -1, z3, z4);
    }

    private View findReferenceChildClosestToEnd(RecyclerView.Recycler recycler, RecyclerView.State state) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        return this.mShouldReverseLayout ? findFirstReferenceChild(recycler2, state2) : findLastReferenceChild(recycler2, state2);
    }

    private View findReferenceChildClosestToStart(RecyclerView.Recycler recycler, RecyclerView.State state) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        return this.mShouldReverseLayout ? findLastReferenceChild(recycler2, state2) : findFirstReferenceChild(recycler2, state2);
    }

    private View findFirstReferenceChild(RecyclerView.Recycler recycler, RecyclerView.State state) {
        RecyclerView.State state2 = state;
        return findReferenceChild(recycler, state2, 0, getChildCount(), state2.getItemCount());
    }

    private View findLastReferenceChild(RecyclerView.Recycler recycler, RecyclerView.State state) {
        RecyclerView.State state2 = state;
        return findReferenceChild(recycler, state2, getChildCount() - 1, -1, state2.getItemCount());
    }

    /* access modifiers changed from: package-private */
    public View findReferenceChild(RecyclerView.Recycler recycler, RecyclerView.State state, int i, int i2, int i3) {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        ensureLayoutState();
        View view = null;
        View view2 = null;
        int startAfterPadding = this.mOrientationHelper.getStartAfterPadding();
        int endAfterPadding = this.mOrientationHelper.getEndAfterPadding();
        int i7 = i5 > i4 ? 1 : -1;
        int i8 = i4;
        while (true) {
            int i9 = i8;
            if (i9 != i5) {
                View childAt = getChildAt(i9);
                int position = getPosition(childAt);
                if (position >= 0 && position < i6) {
                    if (((RecyclerView.LayoutParams) childAt.getLayoutParams()).isItemRemoved()) {
                        if (view == null) {
                            view = childAt;
                        }
                    } else if (this.mOrientationHelper.getDecoratedStart(childAt) < endAfterPadding && this.mOrientationHelper.getDecoratedEnd(childAt) >= startAfterPadding) {
                        return childAt;
                    } else {
                        if (view2 == null) {
                            view2 = childAt;
                        }
                    }
                }
                i8 = i9 + i7;
            } else {
                return view2 != null ? view2 : view;
            }
        }
    }

    public int findFirstVisibleItemPosition() {
        View findOneVisibleChild = findOneVisibleChild(0, getChildCount(), false, true);
        return findOneVisibleChild == null ? -1 : getPosition(findOneVisibleChild);
    }

    public int findFirstCompletelyVisibleItemPosition() {
        View findOneVisibleChild = findOneVisibleChild(0, getChildCount(), true, false);
        return findOneVisibleChild == null ? -1 : getPosition(findOneVisibleChild);
    }

    public int findLastVisibleItemPosition() {
        View findOneVisibleChild = findOneVisibleChild(getChildCount() - 1, -1, false, true);
        return findOneVisibleChild == null ? -1 : getPosition(findOneVisibleChild);
    }

    public int findLastCompletelyVisibleItemPosition() {
        View findOneVisibleChild = findOneVisibleChild(getChildCount() - 1, -1, true, false);
        return findOneVisibleChild == null ? -1 : getPosition(findOneVisibleChild);
    }

    /* access modifiers changed from: package-private */
    public View findOneVisibleChild(int i, int i2, boolean z, boolean z2) {
        int i3 = i;
        int i4 = i2;
        boolean z3 = z;
        boolean z4 = z2;
        ensureLayoutState();
        int startAfterPadding = this.mOrientationHelper.getStartAfterPadding();
        int endAfterPadding = this.mOrientationHelper.getEndAfterPadding();
        int i5 = i4 > i3 ? 1 : -1;
        View view = null;
        int i6 = i3;
        while (true) {
            int i7 = i6;
            if (i7 == i4) {
                return view;
            }
            View childAt = getChildAt(i7);
            int decoratedStart = this.mOrientationHelper.getDecoratedStart(childAt);
            int decoratedEnd = this.mOrientationHelper.getDecoratedEnd(childAt);
            if (decoratedStart < endAfterPadding && decoratedEnd > startAfterPadding) {
                if (!z3) {
                    return childAt;
                }
                if (decoratedStart >= startAfterPadding && decoratedEnd <= endAfterPadding) {
                    return childAt;
                }
                if (z4 && view == null) {
                    view = childAt;
                }
            }
            i6 = i7 + i5;
        }
    }

    public View onFocusSearchFailed(View view, int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        View findReferenceChildClosestToEnd;
        View childClosestToEnd;
        int i2 = i;
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        resolveShouldLayoutReverse();
        if (getChildCount() == 0) {
            return null;
        }
        int convertFocusDirectionToLayoutDirection = convertFocusDirectionToLayoutDirection(i2);
        if (convertFocusDirectionToLayoutDirection == Integer.MIN_VALUE) {
            return null;
        }
        ensureLayoutState();
        if (convertFocusDirectionToLayoutDirection == -1) {
            findReferenceChildClosestToEnd = findReferenceChildClosestToStart(recycler2, state2);
        } else {
            findReferenceChildClosestToEnd = findReferenceChildClosestToEnd(recycler2, state2);
        }
        if (findReferenceChildClosestToEnd == null) {
            return null;
        }
        ensureLayoutState();
        updateLayoutState(convertFocusDirectionToLayoutDirection, (int) (MAX_SCROLL_FACTOR * ((float) this.mOrientationHelper.getTotalSpace())), false, state2);
        this.mLayoutState.mScrollingOffset = Integer.MIN_VALUE;
        this.mLayoutState.mRecycle = false;
        int fill = fill(recycler2, this.mLayoutState, state2, true);
        if (convertFocusDirectionToLayoutDirection == -1) {
            childClosestToEnd = getChildClosestToStart();
        } else {
            childClosestToEnd = getChildClosestToEnd();
        }
        if (childClosestToEnd == findReferenceChildClosestToEnd || !childClosestToEnd.isFocusable()) {
            return null;
        }
        return childClosestToEnd;
    }

    private void logChildren() {
        StringBuilder sb;
        int d = Log.d(TAG, "internal representation of views on the screen");
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            new StringBuilder();
            int d2 = Log.d(TAG, sb.append("item ").append(getPosition(childAt)).append(", coord:").append(this.mOrientationHelper.getDecoratedStart(childAt)).toString());
        }
        int d3 = Log.d(TAG, "==============");
    }

    /* access modifiers changed from: package-private */
    public void validateChildOrder() {
        StringBuilder sb;
        RuntimeException runtimeException;
        StringBuilder sb2;
        Throwable th;
        RuntimeException runtimeException2;
        StringBuilder sb3;
        Throwable th2;
        new StringBuilder();
        int d = Log.d(TAG, sb.append("validating child count ").append(getChildCount()).toString());
        if (getChildCount() >= 1) {
            int position = getPosition(getChildAt(0));
            int decoratedStart = this.mOrientationHelper.getDecoratedStart(getChildAt(0));
            if (this.mShouldReverseLayout) {
                int i = 1;
                while (i < getChildCount()) {
                    View childAt = getChildAt(i);
                    int position2 = getPosition(childAt);
                    int decoratedStart2 = this.mOrientationHelper.getDecoratedStart(childAt);
                    if (position2 < position) {
                        logChildren();
                        RuntimeException runtimeException3 = runtimeException2;
                        new StringBuilder();
                        new RuntimeException(sb3.append("detected invalid position. loc invalid? ").append(decoratedStart2 < decoratedStart).toString());
                        throw runtimeException3;
                    } else if (decoratedStart2 > decoratedStart) {
                        logChildren();
                        Throwable th3 = th2;
                        new RuntimeException("detected invalid location");
                        throw th3;
                    } else {
                        i++;
                    }
                }
                return;
            }
            int i2 = 1;
            while (i2 < getChildCount()) {
                View childAt2 = getChildAt(i2);
                int position3 = getPosition(childAt2);
                int decoratedStart3 = this.mOrientationHelper.getDecoratedStart(childAt2);
                if (position3 < position) {
                    logChildren();
                    RuntimeException runtimeException4 = runtimeException;
                    new StringBuilder();
                    new RuntimeException(sb2.append("detected invalid position. loc invalid? ").append(decoratedStart3 < decoratedStart).toString());
                    throw runtimeException4;
                } else if (decoratedStart3 < decoratedStart) {
                    logChildren();
                    Throwable th4 = th;
                    new RuntimeException("detected invalid location");
                    throw th4;
                } else {
                    i2++;
                }
            }
        }
    }

    public boolean supportsPredictiveItemAnimations() {
        return this.mPendingSavedState == null && this.mLastStackFromEnd == this.mStackFromEnd;
    }

    public void prepareForDrop(View view, View view2, int i, int i2) {
        View view3 = view;
        View view4 = view2;
        assertNotInLayoutOrScroll("Cannot drop a view during a scroll or layout calculation");
        ensureLayoutState();
        resolveShouldLayoutReverse();
        int position = getPosition(view3);
        int position2 = getPosition(view4);
        char c = position < position2 ? (char) 1 : 65535;
        if (this.mShouldReverseLayout) {
            if (c == 1) {
                scrollToPositionWithOffset(position2, this.mOrientationHelper.getEndAfterPadding() - (this.mOrientationHelper.getDecoratedStart(view4) + this.mOrientationHelper.getDecoratedMeasurement(view3)));
            } else {
                scrollToPositionWithOffset(position2, this.mOrientationHelper.getEndAfterPadding() - this.mOrientationHelper.getDecoratedEnd(view4));
            }
        } else if (c == 65535) {
            scrollToPositionWithOffset(position2, this.mOrientationHelper.getDecoratedStart(view4));
        } else {
            scrollToPositionWithOffset(position2, this.mOrientationHelper.getDecoratedEnd(view4) - this.mOrientationHelper.getDecoratedMeasurement(view3));
        }
    }

    static class LayoutState {
        static final int INVALID_LAYOUT = Integer.MIN_VALUE;
        static final int ITEM_DIRECTION_HEAD = -1;
        static final int ITEM_DIRECTION_TAIL = 1;
        static final int LAYOUT_END = 1;
        static final int LAYOUT_START = -1;
        static final int SCOLLING_OFFSET_NaN = Integer.MIN_VALUE;
        static final String TAG = "LinearLayoutManager#LayoutState";
        int mAvailable;
        int mCurrentPosition;
        int mExtra = 0;
        boolean mIsPreLayout = false;
        int mItemDirection;
        int mLastScrollDelta;
        int mLayoutDirection;
        int mOffset;
        boolean mRecycle = true;
        List<RecyclerView.ViewHolder> mScrapList = null;
        int mScrollingOffset;

        LayoutState() {
        }

        /* access modifiers changed from: package-private */
        public boolean hasMore(RecyclerView.State state) {
            return this.mCurrentPosition >= 0 && this.mCurrentPosition < state.getItemCount();
        }

        /* access modifiers changed from: package-private */
        public View next(RecyclerView.Recycler recycler) {
            RecyclerView.Recycler recycler2 = recycler;
            if (this.mScrapList != null) {
                return nextViewFromScrapList();
            }
            this.mCurrentPosition = this.mCurrentPosition + this.mItemDirection;
            return recycler2.getViewForPosition(this.mCurrentPosition);
        }

        private View nextViewFromScrapList() {
            int size = this.mScrapList.size();
            for (int i = 0; i < size; i++) {
                View view = this.mScrapList.get(i).itemView;
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                if (!layoutParams.isItemRemoved() && this.mCurrentPosition == layoutParams.getViewLayoutPosition()) {
                    assignPositionFromScrapList(view);
                    return view;
                }
            }
            return null;
        }

        public void assignPositionFromScrapList() {
            assignPositionFromScrapList(null);
        }

        public void assignPositionFromScrapList(View view) {
            View nextViewInLimitedList = nextViewInLimitedList(view);
            if (nextViewInLimitedList == null) {
                this.mCurrentPosition = -1;
            } else {
                this.mCurrentPosition = ((RecyclerView.LayoutParams) nextViewInLimitedList.getLayoutParams()).getViewLayoutPosition();
            }
        }

        public View nextViewInLimitedList(View view) {
            int viewLayoutPosition;
            View view2 = view;
            int size = this.mScrapList.size();
            View view3 = null;
            int i = Integer.MAX_VALUE;
            for (int i2 = 0; i2 < size; i2++) {
                View view4 = this.mScrapList.get(i2).itemView;
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view4.getLayoutParams();
                if (view4 != view2 && !layoutParams.isItemRemoved() && (viewLayoutPosition = (layoutParams.getViewLayoutPosition() - this.mCurrentPosition) * this.mItemDirection) >= 0 && viewLayoutPosition < i) {
                    view3 = view4;
                    i = viewLayoutPosition;
                    if (viewLayoutPosition == 0) {
                        break;
                    }
                }
            }
            return view3;
        }

        /* access modifiers changed from: package-private */
        public void log() {
            StringBuilder sb;
            new StringBuilder();
            int d = Log.d(TAG, sb.append("avail:").append(this.mAvailable).append(", ind:").append(this.mCurrentPosition).append(", dir:").append(this.mItemDirection).append(", offset:").append(this.mOffset).append(", layoutDir:").append(this.mLayoutDirection).toString());
        }
    }

    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR;
        boolean mAnchorLayoutFromEnd;
        int mAnchorOffset;
        int mAnchorPosition;

        public SavedState() {
        }

        SavedState(Parcel parcel) {
            Parcel parcel2 = parcel;
            this.mAnchorPosition = parcel2.readInt();
            this.mAnchorOffset = parcel2.readInt();
            this.mAnchorLayoutFromEnd = parcel2.readInt() == 1;
        }

        public SavedState(SavedState savedState) {
            SavedState savedState2 = savedState;
            this.mAnchorPosition = savedState2.mAnchorPosition;
            this.mAnchorOffset = savedState2.mAnchorOffset;
            this.mAnchorLayoutFromEnd = savedState2.mAnchorLayoutFromEnd;
        }

        /* access modifiers changed from: package-private */
        public boolean hasValidAnchor() {
            return this.mAnchorPosition >= 0;
        }

        /* access modifiers changed from: package-private */
        public void invalidateAnchor() {
            this.mAnchorPosition = -1;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            parcel2.writeInt(this.mAnchorPosition);
            parcel2.writeInt(this.mAnchorOffset);
            parcel2.writeInt(this.mAnchorLayoutFromEnd ? 1 : 0);
        }

        static {
            Parcelable.Creator<SavedState> creator;
            new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(Parcel parcel) {
                    SavedState savedState;
                    new SavedState(parcel);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = creator;
        }
    }

    class AnchorInfo {
        int mCoordinate;
        boolean mLayoutFromEnd;
        int mPosition;

        AnchorInfo() {
        }

        /* access modifiers changed from: package-private */
        public void reset() {
            this.mPosition = -1;
            this.mCoordinate = Integer.MIN_VALUE;
            this.mLayoutFromEnd = false;
        }

        /* access modifiers changed from: package-private */
        public void assignCoordinateFromPadding() {
            this.mCoordinate = this.mLayoutFromEnd ? LinearLayoutManager.this.mOrientationHelper.getEndAfterPadding() : LinearLayoutManager.this.mOrientationHelper.getStartAfterPadding();
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder();
            return sb.append("AnchorInfo{mPosition=").append(this.mPosition).append(", mCoordinate=").append(this.mCoordinate).append(", mLayoutFromEnd=").append(this.mLayoutFromEnd).append('}').toString();
        }

        /* access modifiers changed from: private */
        public boolean isViewValidAsAnchor(View view, RecyclerView.State state) {
            RecyclerView.State state2 = state;
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return !layoutParams.isItemRemoved() && layoutParams.getViewLayoutPosition() >= 0 && layoutParams.getViewLayoutPosition() < state2.getItemCount();
        }

        public void assignFromViewAndKeepVisibleRect(View view) {
            View view2 = view;
            int totalSpaceChange = LinearLayoutManager.this.mOrientationHelper.getTotalSpaceChange();
            if (totalSpaceChange >= 0) {
                assignFromView(view2);
                return;
            }
            this.mPosition = LinearLayoutManager.this.getPosition(view2);
            if (this.mLayoutFromEnd) {
                int endAfterPadding = (LinearLayoutManager.this.mOrientationHelper.getEndAfterPadding() - totalSpaceChange) - LinearLayoutManager.this.mOrientationHelper.getDecoratedEnd(view2);
                this.mCoordinate = LinearLayoutManager.this.mOrientationHelper.getEndAfterPadding() - endAfterPadding;
                if (endAfterPadding > 0) {
                    int decoratedMeasurement = this.mCoordinate - LinearLayoutManager.this.mOrientationHelper.getDecoratedMeasurement(view2);
                    int startAfterPadding = LinearLayoutManager.this.mOrientationHelper.getStartAfterPadding();
                    int min = decoratedMeasurement - (startAfterPadding + Math.min(LinearLayoutManager.this.mOrientationHelper.getDecoratedStart(view2) - startAfterPadding, 0));
                    if (min < 0) {
                        this.mCoordinate = this.mCoordinate + Math.min(endAfterPadding, -min);
                        return;
                    }
                    return;
                }
                return;
            }
            int decoratedStart = LinearLayoutManager.this.mOrientationHelper.getDecoratedStart(view2);
            int startAfterPadding2 = decoratedStart - LinearLayoutManager.this.mOrientationHelper.getStartAfterPadding();
            this.mCoordinate = decoratedStart;
            if (startAfterPadding2 > 0) {
                int endAfterPadding2 = (LinearLayoutManager.this.mOrientationHelper.getEndAfterPadding() - Math.min(0, (LinearLayoutManager.this.mOrientationHelper.getEndAfterPadding() - totalSpaceChange) - LinearLayoutManager.this.mOrientationHelper.getDecoratedEnd(view2))) - (decoratedStart + LinearLayoutManager.this.mOrientationHelper.getDecoratedMeasurement(view2));
                if (endAfterPadding2 < 0) {
                    this.mCoordinate = this.mCoordinate - Math.min(startAfterPadding2, -endAfterPadding2);
                }
            }
        }

        public void assignFromView(View view) {
            View view2 = view;
            if (this.mLayoutFromEnd) {
                this.mCoordinate = LinearLayoutManager.this.mOrientationHelper.getDecoratedEnd(view2) + LinearLayoutManager.this.mOrientationHelper.getTotalSpaceChange();
            } else {
                this.mCoordinate = LinearLayoutManager.this.mOrientationHelper.getDecoratedStart(view2);
            }
            this.mPosition = LinearLayoutManager.this.getPosition(view2);
        }
    }

    protected static class LayoutChunkResult {
        public int mConsumed;
        public boolean mFinished;
        public boolean mFocusable;
        public boolean mIgnoreConsumed;

        protected LayoutChunkResult() {
        }

        /* access modifiers changed from: package-private */
        public void resetInternal() {
            this.mConsumed = 0;
            this.mFinished = false;
            this.mIgnoreConsumed = false;
            this.mFocusable = false;
        }
    }
}
