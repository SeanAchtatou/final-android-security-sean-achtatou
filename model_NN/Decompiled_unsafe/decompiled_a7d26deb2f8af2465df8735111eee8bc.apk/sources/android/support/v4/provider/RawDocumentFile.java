package android.support.v4.provider;

import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

class RawDocumentFile extends DocumentFile {
    private File mFile;

    RawDocumentFile(DocumentFile documentFile, File file) {
        super(documentFile);
        this.mFile = file;
    }

    public DocumentFile createFile(String str, String str2) {
        File file;
        StringBuilder sb;
        DocumentFile documentFile;
        StringBuilder sb2;
        String str3 = str2;
        String extensionFromMimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType(str);
        if (extensionFromMimeType != null) {
            new StringBuilder();
            str3 = sb2.append(str3).append(".").append(extensionFromMimeType).toString();
        }
        new File(this.mFile, str3);
        File file2 = file;
        try {
            boolean createNewFile = file2.createNewFile();
            DocumentFile documentFile2 = documentFile;
            new RawDocumentFile(this, file2);
            return documentFile2;
        } catch (IOException e) {
            new StringBuilder();
            int w = Log.w("DocumentFile", sb.append("Failed to createFile: ").append(e).toString());
            return null;
        }
    }

    public DocumentFile createDirectory(String str) {
        File file;
        DocumentFile documentFile;
        new File(this.mFile, str);
        File file2 = file;
        if (!file2.isDirectory() && !file2.mkdir()) {
            return null;
        }
        new RawDocumentFile(this, file2);
        return documentFile;
    }

    public Uri getUri() {
        return Uri.fromFile(this.mFile);
    }

    public String getName() {
        return this.mFile.getName();
    }

    public String getType() {
        if (this.mFile.isDirectory()) {
            return null;
        }
        return getTypeForName(this.mFile.getName());
    }

    public boolean isDirectory() {
        return this.mFile.isDirectory();
    }

    public boolean isFile() {
        return this.mFile.isFile();
    }

    public long lastModified() {
        return this.mFile.lastModified();
    }

    public long length() {
        return this.mFile.length();
    }

    public boolean canRead() {
        return this.mFile.canRead();
    }

    public boolean canWrite() {
        return this.mFile.canWrite();
    }

    public boolean delete() {
        boolean deleteContents = deleteContents(this.mFile);
        return this.mFile.delete();
    }

    public boolean exists() {
        return this.mFile.exists();
    }

    public DocumentFile[] listFiles() {
        ArrayList arrayList;
        Object obj;
        new ArrayList();
        ArrayList arrayList2 = arrayList;
        File[] listFiles = this.mFile.listFiles();
        if (listFiles != null) {
            File[] fileArr = listFiles;
            int length = fileArr.length;
            for (int i = 0; i < length; i++) {
                new RawDocumentFile(this, fileArr[i]);
                boolean add = arrayList2.add(obj);
            }
        }
        return (DocumentFile[]) arrayList2.toArray(new DocumentFile[arrayList2.size()]);
    }

    public boolean renameTo(String str) {
        File file;
        new File(this.mFile.getParentFile(), str);
        File file2 = file;
        if (!this.mFile.renameTo(file2)) {
            return false;
        }
        this.mFile = file2;
        return true;
    }

    private static String getTypeForName(String str) {
        String mimeTypeFromExtension;
        String str2 = str;
        int lastIndexOf = str2.lastIndexOf(46);
        if (lastIndexOf < 0 || (mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(str2.substring(lastIndexOf + 1).toLowerCase())) == null) {
            return "application/octet-stream";
        }
        return mimeTypeFromExtension;
    }

    private static boolean deleteContents(File file) {
        StringBuilder sb;
        File[] listFiles = file.listFiles();
        boolean z = true;
        if (listFiles != null) {
            File[] fileArr = listFiles;
            int length = fileArr.length;
            for (int i = 0; i < length; i++) {
                File file2 = fileArr[i];
                if (file2.isDirectory()) {
                    z &= deleteContents(file2);
                }
                if (!file2.delete()) {
                    new StringBuilder();
                    int w = Log.w("DocumentFile", sb.append("Failed to delete ").append(file2).toString());
                    z = false;
                }
            }
        }
        return z;
    }
}
