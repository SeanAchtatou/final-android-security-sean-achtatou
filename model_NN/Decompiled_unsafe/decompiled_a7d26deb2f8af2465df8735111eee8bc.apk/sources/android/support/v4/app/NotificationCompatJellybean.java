package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompatBase;
import android.support.v4.app.RemoteInputCompatBase;
import android.util.Log;
import android.util.SparseArray;
import android.widget.RemoteViews;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class NotificationCompatJellybean {
    static final String EXTRA_ACTION_EXTRAS = "android.support.actionExtras";
    static final String EXTRA_GROUP_KEY = "android.support.groupKey";
    static final String EXTRA_GROUP_SUMMARY = "android.support.isGroupSummary";
    static final String EXTRA_LOCAL_ONLY = "android.support.localOnly";
    static final String EXTRA_REMOTE_INPUTS = "android.support.remoteInputs";
    static final String EXTRA_SORT_KEY = "android.support.sortKey";
    static final String EXTRA_USE_SIDE_CHANNEL = "android.support.useSideChannel";
    private static final String KEY_ACTION_INTENT = "actionIntent";
    private static final String KEY_EXTRAS = "extras";
    private static final String KEY_ICON = "icon";
    private static final String KEY_REMOTE_INPUTS = "remoteInputs";
    private static final String KEY_TITLE = "title";
    public static final String TAG = "NotificationCompat";
    private static Class<?> sActionClass;
    private static Field sActionIconField;
    private static Field sActionIntentField;
    private static Field sActionTitleField;
    private static boolean sActionsAccessFailed;
    private static Field sActionsField;
    private static final Object sActionsLock;
    private static Field sExtrasField;
    private static boolean sExtrasFieldAccessFailed;
    private static final Object sExtrasLock;

    NotificationCompatJellybean() {
    }

    static {
        Object obj;
        Object obj2;
        new Object();
        sExtrasLock = obj;
        new Object();
        sActionsLock = obj2;
    }

    public static class Builder implements NotificationBuilderWithBuilderAccessor, NotificationBuilderWithActions {
        private Notification.Builder b;
        private List<Bundle> mActionExtrasList;
        private final Bundle mExtras;

        public Builder(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, int i4, CharSequence charSequence4, boolean z3, Bundle bundle, String str, boolean z4, String str2) {
            List<Bundle> list;
            Notification.Builder builder;
            boolean z5;
            boolean z6;
            boolean z7;
            boolean z8;
            Bundle bundle2;
            Notification notification2 = notification;
            CharSequence charSequence5 = charSequence;
            CharSequence charSequence6 = charSequence2;
            CharSequence charSequence7 = charSequence3;
            int i5 = i;
            PendingIntent pendingIntent3 = pendingIntent;
            PendingIntent pendingIntent4 = pendingIntent2;
            Bitmap bitmap2 = bitmap;
            int i6 = i2;
            int i7 = i3;
            boolean z9 = z;
            boolean z10 = z2;
            int i8 = i4;
            CharSequence charSequence8 = charSequence4;
            boolean z11 = z3;
            Bundle bundle3 = bundle;
            String str3 = str;
            boolean z12 = z4;
            String str4 = str2;
            new ArrayList();
            this.mActionExtrasList = list;
            new Notification.Builder(context);
            Notification.Builder lights = builder.setWhen(notification2.when).setSmallIcon(notification2.icon, notification2.iconLevel).setContent(notification2.contentView).setTicker(notification2.tickerText, remoteViews).setSound(notification2.sound, notification2.audioStreamType).setVibrate(notification2.vibrate).setLights(notification2.ledARGB, notification2.ledOnMS, notification2.ledOffMS);
            if ((notification2.flags & 2) != 0) {
                z5 = true;
            } else {
                z5 = false;
            }
            Notification.Builder ongoing = lights.setOngoing(z5);
            if ((notification2.flags & 8) != 0) {
                z6 = true;
            } else {
                z6 = false;
            }
            Notification.Builder onlyAlertOnce = ongoing.setOnlyAlertOnce(z6);
            if ((notification2.flags & 16) != 0) {
                z7 = true;
            } else {
                z7 = false;
            }
            Notification.Builder deleteIntent = onlyAlertOnce.setAutoCancel(z7).setDefaults(notification2.defaults).setContentTitle(charSequence5).setContentText(charSequence6).setSubText(charSequence8).setContentInfo(charSequence7).setContentIntent(pendingIntent3).setDeleteIntent(notification2.deleteIntent);
            PendingIntent pendingIntent5 = pendingIntent4;
            if ((notification2.flags & 128) != 0) {
                z8 = true;
            } else {
                z8 = false;
            }
            this.b = deleteIntent.setFullScreenIntent(pendingIntent5, z8).setLargeIcon(bitmap2).setNumber(i5).setUsesChronometer(z10).setPriority(i8).setProgress(i6, i7, z9);
            new Bundle();
            this.mExtras = bundle2;
            if (bundle3 != null) {
                this.mExtras.putAll(bundle3);
            }
            if (z11) {
                this.mExtras.putBoolean("android.support.localOnly", true);
            }
            if (str3 != null) {
                this.mExtras.putString("android.support.groupKey", str3);
                if (z12) {
                    this.mExtras.putBoolean("android.support.isGroupSummary", true);
                } else {
                    this.mExtras.putBoolean("android.support.useSideChannel", true);
                }
            }
            if (str4 != null) {
                this.mExtras.putString("android.support.sortKey", str4);
            }
        }

        public void addAction(NotificationCompatBase.Action action) {
            boolean add = this.mActionExtrasList.add(NotificationCompatJellybean.writeActionAndGetExtras(this.b, action));
        }

        public Notification.Builder getBuilder() {
            return this.b;
        }

        public Notification build() {
            Bundle bundle;
            Notification build = this.b.build();
            Bundle extras = NotificationCompatJellybean.getExtras(build);
            new Bundle(this.mExtras);
            Bundle bundle2 = bundle;
            for (String next : this.mExtras.keySet()) {
                if (extras.containsKey(next)) {
                    bundle2.remove(next);
                }
            }
            extras.putAll(bundle2);
            SparseArray<Bundle> buildActionExtrasMap = NotificationCompatJellybean.buildActionExtrasMap(this.mActionExtrasList);
            if (buildActionExtrasMap != null) {
                NotificationCompatJellybean.getExtras(build).putSparseParcelableArray("android.support.actionExtras", buildActionExtrasMap);
            }
            return build;
        }
    }

    public static void addBigTextStyle(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, CharSequence charSequence, boolean z, CharSequence charSequence2, CharSequence charSequence3) {
        Notification.BigTextStyle bigTextStyle;
        CharSequence charSequence4 = charSequence2;
        new Notification.BigTextStyle(notificationBuilderWithBuilderAccessor.getBuilder());
        Notification.BigTextStyle bigText = bigTextStyle.setBigContentTitle(charSequence).bigText(charSequence3);
        if (z) {
            Notification.BigTextStyle summaryText = bigText.setSummaryText(charSequence4);
        }
    }

    public static void addBigPictureStyle(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, CharSequence charSequence, boolean z, CharSequence charSequence2, Bitmap bitmap, Bitmap bitmap2, boolean z2) {
        Notification.BigPictureStyle bigPictureStyle;
        boolean z3 = z;
        CharSequence charSequence3 = charSequence2;
        Bitmap bitmap3 = bitmap2;
        new Notification.BigPictureStyle(notificationBuilderWithBuilderAccessor.getBuilder());
        Notification.BigPictureStyle bigPicture = bigPictureStyle.setBigContentTitle(charSequence).bigPicture(bitmap);
        if (z2) {
            Notification.BigPictureStyle bigLargeIcon = bigPicture.bigLargeIcon(bitmap3);
        }
        if (z3) {
            Notification.BigPictureStyle summaryText = bigPicture.setSummaryText(charSequence3);
        }
    }

    public static void addInboxStyle(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, CharSequence charSequence, boolean z, CharSequence charSequence2, ArrayList<CharSequence> arrayList) {
        Notification.InboxStyle inboxStyle;
        CharSequence charSequence3 = charSequence2;
        ArrayList<CharSequence> arrayList2 = arrayList;
        new Notification.InboxStyle(notificationBuilderWithBuilderAccessor.getBuilder());
        Notification.InboxStyle bigContentTitle = inboxStyle.setBigContentTitle(charSequence);
        if (z) {
            Notification.InboxStyle summaryText = bigContentTitle.setSummaryText(charSequence3);
        }
        Iterator<CharSequence> it = arrayList2.iterator();
        while (it.hasNext()) {
            Notification.InboxStyle addLine = bigContentTitle.addLine(it.next());
        }
    }

    public static SparseArray<Bundle> buildActionExtrasMap(List<Bundle> list) {
        SparseArray<Bundle> sparseArray;
        List<Bundle> list2 = list;
        SparseArray<Bundle> sparseArray2 = null;
        int size = list2.size();
        for (int i = 0; i < size; i++) {
            Bundle bundle = list2.get(i);
            if (bundle != null) {
                if (sparseArray2 == null) {
                    new SparseArray<>();
                    sparseArray2 = sparseArray;
                }
                sparseArray2.put(i, bundle);
            }
        }
        return sparseArray2;
    }

    public static Bundle getExtras(Notification notification) {
        Bundle bundle;
        Notification notification2 = notification;
        Object obj = sExtrasLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (sExtrasFieldAccessFailed) {
                    return null;
                }
                if (sExtrasField == null) {
                    Field declaredField = Notification.class.getDeclaredField(KEY_EXTRAS);
                    if (!Bundle.class.isAssignableFrom(declaredField.getType())) {
                        int e = Log.e(TAG, "Notification.extras field is not of type Bundle");
                        sExtrasFieldAccessFailed = true;
                        return null;
                    }
                    declaredField.setAccessible(true);
                    sExtrasField = declaredField;
                }
                Bundle bundle2 = (Bundle) sExtrasField.get(notification2);
                if (bundle2 == null) {
                    new Bundle();
                    bundle2 = bundle;
                    sExtrasField.set(notification2, bundle2);
                }
                Bundle bundle3 = bundle2;
                return bundle3;
            } catch (IllegalAccessException e2) {
                int e3 = Log.e(TAG, "Unable to access notification extras", e2);
                sExtrasFieldAccessFailed = true;
                return null;
            } catch (NoSuchFieldException e4) {
                int e5 = Log.e(TAG, "Unable to access notification extras", e4);
                sExtrasFieldAccessFailed = true;
                return null;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    public static NotificationCompatBase.Action readAction(NotificationCompatBase.Action.Factory factory, RemoteInputCompatBase.RemoteInput.Factory factory2, int i, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle) {
        NotificationCompatBase.Action.Factory factory3 = factory;
        RemoteInputCompatBase.RemoteInput.Factory factory4 = factory2;
        int i2 = i;
        CharSequence charSequence2 = charSequence;
        PendingIntent pendingIntent2 = pendingIntent;
        Bundle bundle2 = bundle;
        RemoteInputCompatBase.RemoteInput[] remoteInputArr = null;
        if (bundle2 != null) {
            remoteInputArr = RemoteInputCompatJellybean.fromBundleArray(BundleUtil.getBundleArrayFromBundle(bundle2, "android.support.remoteInputs"), factory4);
        }
        return factory3.build(i2, charSequence2, pendingIntent2, bundle2, remoteInputArr);
    }

    public static Bundle writeActionAndGetExtras(Notification.Builder builder, NotificationCompatBase.Action action) {
        Bundle bundle;
        NotificationCompatBase.Action action2 = action;
        Notification.Builder addAction = builder.addAction(action2.getIcon(), action2.getTitle(), action2.getActionIntent());
        new Bundle(action2.getExtras());
        Bundle bundle2 = bundle;
        if (action2.getRemoteInputs() != null) {
            bundle2.putParcelableArray("android.support.remoteInputs", RemoteInputCompatJellybean.toBundleArray(action2.getRemoteInputs()));
        }
        return bundle2;
    }

    public static int getActionCount(Notification notification) {
        Notification notification2 = notification;
        Object obj = sActionsLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                Object[] actionObjectsLocked = getActionObjectsLocked(notification2);
                int length = actionObjectsLocked != null ? actionObjectsLocked.length : 0;
                return length;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.support.v4.app.NotificationCompatBase.Action getAction(android.app.Notification r17, int r18, android.support.v4.app.NotificationCompatBase.Action.Factory r19, android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory r20) {
        /*
            r0 = r17
            r1 = r18
            r2 = r19
            r3 = r20
            java.lang.Object r10 = android.support.v4.app.NotificationCompatJellybean.sActionsLock
            r16 = r10
            r10 = r16
            r11 = r16
            r4 = r11
            monitor-enter(r10)
            r10 = r0
            java.lang.Object[] r10 = getActionObjectsLocked(r10)     // Catch:{ IllegalAccessException -> 0x005e }
            r11 = r1
            r10 = r10[r11]     // Catch:{ IllegalAccessException -> 0x005e }
            r5 = r10
            r10 = 0
            r6 = r10
            r10 = r0
            android.os.Bundle r10 = getExtras(r10)     // Catch:{ IllegalAccessException -> 0x005e }
            r7 = r10
            r10 = r7
            if (r10 == 0) goto L_0x003a
            r10 = r7
            java.lang.String r11 = "android.support.actionExtras"
            android.util.SparseArray r10 = r10.getSparseParcelableArray(r11)     // Catch:{ IllegalAccessException -> 0x005e }
            r8 = r10
            r10 = r8
            if (r10 == 0) goto L_0x003a
            r10 = r8
            r11 = r1
            java.lang.Object r10 = r10.get(r11)     // Catch:{ IllegalAccessException -> 0x005e }
            android.os.Bundle r10 = (android.os.Bundle) r10     // Catch:{ IllegalAccessException -> 0x005e }
            r6 = r10
        L_0x003a:
            r10 = r2
            r11 = r3
            java.lang.reflect.Field r12 = android.support.v4.app.NotificationCompatJellybean.sActionIconField     // Catch:{ IllegalAccessException -> 0x005e }
            r13 = r5
            int r12 = r12.getInt(r13)     // Catch:{ IllegalAccessException -> 0x005e }
            java.lang.reflect.Field r13 = android.support.v4.app.NotificationCompatJellybean.sActionTitleField     // Catch:{ IllegalAccessException -> 0x005e }
            r14 = r5
            java.lang.Object r13 = r13.get(r14)     // Catch:{ IllegalAccessException -> 0x005e }
            java.lang.CharSequence r13 = (java.lang.CharSequence) r13     // Catch:{ IllegalAccessException -> 0x005e }
            java.lang.reflect.Field r14 = android.support.v4.app.NotificationCompatJellybean.sActionIntentField     // Catch:{ IllegalAccessException -> 0x005e }
            r15 = r5
            java.lang.Object r14 = r14.get(r15)     // Catch:{ IllegalAccessException -> 0x005e }
            android.app.PendingIntent r14 = (android.app.PendingIntent) r14     // Catch:{ IllegalAccessException -> 0x005e }
            r15 = r6
            android.support.v4.app.NotificationCompatBase$Action r10 = readAction(r10, r11, r12, r13, r14, r15)     // Catch:{ IllegalAccessException -> 0x005e }
            r11 = r4
            monitor-exit(r11)     // Catch:{ all -> 0x0071 }
            r0 = r10
        L_0x005d:
            return r0
        L_0x005e:
            r10 = move-exception
            r5 = r10
            java.lang.String r10 = "NotificationCompat"
            java.lang.String r11 = "Unable to access notification actions"
            r12 = r5
            int r10 = android.util.Log.e(r10, r11, r12)     // Catch:{ all -> 0x0071 }
            r10 = 1
            android.support.v4.app.NotificationCompatJellybean.sActionsAccessFailed = r10     // Catch:{ all -> 0x0071 }
            r10 = r4
            monitor-exit(r10)     // Catch:{ all -> 0x0071 }
            r10 = 0
            r0 = r10
            goto L_0x005d
        L_0x0071:
            r10 = move-exception
            r9 = r10
            r10 = r4
            monitor-exit(r10)     // Catch:{ all -> 0x0071 }
            r10 = r9
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.NotificationCompatJellybean.getAction(android.app.Notification, int, android.support.v4.app.NotificationCompatBase$Action$Factory, android.support.v4.app.RemoteInputCompatBase$RemoteInput$Factory):android.support.v4.app.NotificationCompatBase$Action");
    }

    private static Object[] getActionObjectsLocked(Notification notification) {
        Notification notification2 = notification;
        Object obj = sActionsLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (!ensureActionReflectionReadyLocked()) {
                    return null;
                }
                Object[] objArr = (Object[]) sActionsField.get(notification2);
                return objArr;
            } catch (IllegalAccessException e) {
                int e2 = Log.e(TAG, "Unable to access notification actions", e);
                sActionsAccessFailed = true;
                Object obj3 = obj2;
                return null;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj4 = obj2;
                throw th2;
            }
        }
    }

    private static boolean ensureActionReflectionReadyLocked() {
        boolean z;
        if (sActionsAccessFailed) {
            return false;
        }
        try {
            if (sActionsField == null) {
                sActionClass = Class.forName("android.app.Notification$Action");
                sActionIconField = sActionClass.getDeclaredField(KEY_ICON);
                sActionTitleField = sActionClass.getDeclaredField(KEY_TITLE);
                sActionIntentField = sActionClass.getDeclaredField(KEY_ACTION_INTENT);
                sActionsField = Notification.class.getDeclaredField("actions");
                sActionsField.setAccessible(true);
            }
        } catch (ClassNotFoundException e) {
            int e2 = Log.e(TAG, "Unable to access notification actions", e);
            sActionsAccessFailed = true;
        } catch (NoSuchFieldException e3) {
            int e4 = Log.e(TAG, "Unable to access notification actions", e3);
            sActionsAccessFailed = true;
        }
        if (!sActionsAccessFailed) {
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public static NotificationCompatBase.Action[] getActionsFromParcelableArrayList(ArrayList<Parcelable> arrayList, NotificationCompatBase.Action.Factory factory, RemoteInputCompatBase.RemoteInput.Factory factory2) {
        ArrayList<Parcelable> arrayList2 = arrayList;
        NotificationCompatBase.Action.Factory factory3 = factory;
        RemoteInputCompatBase.RemoteInput.Factory factory4 = factory2;
        if (arrayList2 == null) {
            return null;
        }
        NotificationCompatBase.Action[] newArray = factory3.newArray(arrayList2.size());
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = getActionFromBundle((Bundle) arrayList2.get(i), factory3, factory4);
        }
        return newArray;
    }

    private static NotificationCompatBase.Action getActionFromBundle(Bundle bundle, NotificationCompatBase.Action.Factory factory, RemoteInputCompatBase.RemoteInput.Factory factory2) {
        Bundle bundle2 = bundle;
        return factory.build(bundle2.getInt(KEY_ICON), bundle2.getCharSequence(KEY_TITLE), (PendingIntent) bundle2.getParcelable(KEY_ACTION_INTENT), bundle2.getBundle(KEY_EXTRAS), RemoteInputCompatJellybean.fromBundleArray(BundleUtil.getBundleArrayFromBundle(bundle2, KEY_REMOTE_INPUTS), factory2));
    }

    /* JADX WARN: Type inference failed for: r9v0 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<android.os.Parcelable> getParcelableArrayListForActions(android.support.v4.app.NotificationCompatBase.Action[] r10) {
        /*
            r0 = r10
            r6 = r0
            if (r6 != 0) goto L_0x0007
            r6 = 0
            r0 = r6
        L_0x0006:
            return r0
        L_0x0007:
            java.util.ArrayList r6 = new java.util.ArrayList
            r9 = r6
            r6 = r9
            r7 = r9
            r8 = r0
            int r8 = r8.length
            r7.<init>(r8)
            r1 = r6
            r6 = r0
            r2 = r6
            r6 = r2
            int r6 = r6.length
            r3 = r6
            r6 = 0
            r4 = r6
        L_0x0019:
            r6 = r4
            r7 = r3
            if (r6 >= r7) goto L_0x002f
            r6 = r2
            r7 = r4
            r6 = r6[r7]
            r5 = r6
            r6 = r1
            r7 = r5
            android.os.Bundle r7 = getBundleForAction(r7)
            boolean r6 = r6.add(r7)
            int r4 = r4 + 1
            goto L_0x0019
        L_0x002f:
            r6 = r1
            r0 = r6
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.NotificationCompatJellybean.getParcelableArrayListForActions(android.support.v4.app.NotificationCompatBase$Action[]):java.util.ArrayList");
    }

    private static Bundle getBundleForAction(NotificationCompatBase.Action action) {
        Bundle bundle;
        NotificationCompatBase.Action action2 = action;
        new Bundle();
        Bundle bundle2 = bundle;
        bundle2.putInt(KEY_ICON, action2.getIcon());
        bundle2.putCharSequence(KEY_TITLE, action2.getTitle());
        bundle2.putParcelable(KEY_ACTION_INTENT, action2.getActionIntent());
        bundle2.putBundle(KEY_EXTRAS, action2.getExtras());
        bundle2.putParcelableArray(KEY_REMOTE_INPUTS, RemoteInputCompatJellybean.toBundleArray(action2.getRemoteInputs()));
        return bundle2;
    }

    public static boolean getLocalOnly(Notification notification) {
        return getExtras(notification).getBoolean("android.support.localOnly");
    }

    public static String getGroup(Notification notification) {
        return getExtras(notification).getString("android.support.groupKey");
    }

    public static boolean isGroupSummary(Notification notification) {
        return getExtras(notification).getBoolean("android.support.isGroupSummary");
    }

    public static String getSortKey(Notification notification) {
        return getExtras(notification).getString("android.support.sortKey");
    }
}
