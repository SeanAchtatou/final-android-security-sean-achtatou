package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat21;
import android.support.v4.content.ContextCompat;
import android.view.View;
import java.util.List;
import java.util.Map;

public class ActivityCompat extends ContextCompat {

    public interface OnRequestPermissionsResultCallback {
        void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr);
    }

    public static boolean invalidateOptionsMenu(Activity activity) {
        Activity activity2 = activity;
        if (Build.VERSION.SDK_INT < 11) {
            return false;
        }
        ActivityCompatHoneycomb.invalidateOptionsMenu(activity2);
        return true;
    }

    public static void startActivity(Activity activity, Intent intent, @Nullable Bundle bundle) {
        Activity activity2 = activity;
        Intent intent2 = intent;
        Bundle bundle2 = bundle;
        if (Build.VERSION.SDK_INT >= 16) {
            ActivityCompatJB.startActivity(activity2, intent2, bundle2);
        } else {
            activity2.startActivity(intent2);
        }
    }

    public static void startActivityForResult(Activity activity, Intent intent, int i, @Nullable Bundle bundle) {
        Activity activity2 = activity;
        Intent intent2 = intent;
        int i2 = i;
        Bundle bundle2 = bundle;
        if (Build.VERSION.SDK_INT >= 16) {
            ActivityCompatJB.startActivityForResult(activity2, intent2, i2, bundle2);
        } else {
            activity2.startActivityForResult(intent2, i2);
        }
    }

    public static void finishAffinity(Activity activity) {
        Activity activity2 = activity;
        if (Build.VERSION.SDK_INT >= 16) {
            ActivityCompatJB.finishAffinity(activity2);
        } else {
            activity2.finish();
        }
    }

    public static void finishAfterTransition(Activity activity) {
        Activity activity2 = activity;
        if (Build.VERSION.SDK_INT >= 21) {
            ActivityCompat21.finishAfterTransition(activity2);
        } else {
            activity2.finish();
        }
    }

    public Uri getReferrer(Activity activity) {
        Activity activity2 = activity;
        if (Build.VERSION.SDK_INT >= 22) {
            return ActivityCompat22.getReferrer(activity2);
        }
        Intent intent = activity2.getIntent();
        Uri uri = (Uri) intent.getParcelableExtra("android.intent.extra.REFERRER");
        if (uri != null) {
            return uri;
        }
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        if (stringExtra != null) {
            return Uri.parse(stringExtra);
        }
        return null;
    }

    public static void setEnterSharedElementCallback(Activity activity, SharedElementCallback sharedElementCallback) {
        Activity activity2 = activity;
        SharedElementCallback sharedElementCallback2 = sharedElementCallback;
        if (Build.VERSION.SDK_INT >= 21) {
            ActivityCompat21.setEnterSharedElementCallback(activity2, createCallback(sharedElementCallback2));
        }
    }

    public static void setExitSharedElementCallback(Activity activity, SharedElementCallback sharedElementCallback) {
        Activity activity2 = activity;
        SharedElementCallback sharedElementCallback2 = sharedElementCallback;
        if (Build.VERSION.SDK_INT >= 21) {
            ActivityCompat21.setExitSharedElementCallback(activity2, createCallback(sharedElementCallback2));
        }
    }

    public static void postponeEnterTransition(Activity activity) {
        Activity activity2 = activity;
        if (Build.VERSION.SDK_INT >= 21) {
            ActivityCompat21.postponeEnterTransition(activity2);
        }
    }

    public static void startPostponedEnterTransition(Activity activity) {
        Activity activity2 = activity;
        if (Build.VERSION.SDK_INT >= 21) {
            ActivityCompat21.startPostponedEnterTransition(activity2);
        }
    }

    public static void requestPermissions(@NonNull Activity activity, @NonNull String[] strArr, int i) {
        Handler handler;
        Runnable runnable;
        Activity activity2 = activity;
        String[] strArr2 = strArr;
        int i2 = i;
        if (Build.VERSION.SDK_INT >= 23) {
            ActivityCompatApi23.requestPermissions(activity2, strArr2, i2);
        } else if (activity2 instanceof OnRequestPermissionsResultCallback) {
            new Handler(Looper.getMainLooper());
            final String[] strArr3 = strArr2;
            final Activity activity3 = activity2;
            final int i3 = i2;
            new Runnable() {
                public void run() {
                    int[] iArr = new int[strArr3.length];
                    PackageManager packageManager = activity3.getPackageManager();
                    String packageName = activity3.getPackageName();
                    int length = strArr3.length;
                    for (int i = 0; i < length; i++) {
                        iArr[i] = packageManager.checkPermission(strArr3[i], packageName);
                    }
                    ((OnRequestPermissionsResultCallback) activity3).onRequestPermissionsResult(i3, strArr3, iArr);
                }
            };
            boolean post = handler.post(runnable);
        }
    }

    public static boolean shouldShowRequestPermissionRationale(@NonNull Activity activity, @NonNull String str) {
        Activity activity2 = activity;
        String str2 = str;
        if (Build.VERSION.SDK_INT >= 23) {
            return ActivityCompatApi23.shouldShowRequestPermissionRationale(activity2, str2);
        }
        return false;
    }

    private static ActivityCompat21.SharedElementCallback21 createCallback(SharedElementCallback sharedElementCallback) {
        ActivityCompat21.SharedElementCallback21 sharedElementCallback21;
        SharedElementCallback sharedElementCallback2 = sharedElementCallback;
        ActivityCompat21.SharedElementCallback21 sharedElementCallback212 = null;
        if (sharedElementCallback2 != null) {
            new SharedElementCallback21Impl(sharedElementCallback2);
            sharedElementCallback212 = sharedElementCallback21;
        }
        return sharedElementCallback212;
    }

    private static class SharedElementCallback21Impl extends ActivityCompat21.SharedElementCallback21 {
        private SharedElementCallback mCallback;

        public SharedElementCallback21Impl(SharedElementCallback sharedElementCallback) {
            this.mCallback = sharedElementCallback;
        }

        public void onSharedElementStart(List<String> list, List<View> list2, List<View> list3) {
            this.mCallback.onSharedElementStart(list, list2, list3);
        }

        public void onSharedElementEnd(List<String> list, List<View> list2, List<View> list3) {
            this.mCallback.onSharedElementEnd(list, list2, list3);
        }

        public void onRejectSharedElements(List<View> list) {
            this.mCallback.onRejectSharedElements(list);
        }

        public void onMapSharedElements(List<String> list, Map<String, View> map) {
            this.mCallback.onMapSharedElements(list, map);
        }

        public Parcelable onCaptureSharedElementSnapshot(View view, Matrix matrix, RectF rectF) {
            return this.mCallback.onCaptureSharedElementSnapshot(view, matrix, rectF);
        }

        public View onCreateSnapshotView(Context context, Parcelable parcelable) {
            return this.mCallback.onCreateSnapshotView(context, parcelable);
        }
    }
}
