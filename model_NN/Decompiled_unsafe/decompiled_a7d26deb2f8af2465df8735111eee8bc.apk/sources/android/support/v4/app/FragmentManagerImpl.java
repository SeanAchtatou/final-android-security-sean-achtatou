package android.support.v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.CallSuper;
import android.support.v4.app.BackStackRecord;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.DebugUtils;
import android.support.v4.util.LogWriter;
import android.support.v4.view.LayoutInflaterFactory;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: FragmentManager */
final class FragmentManagerImpl extends FragmentManager implements LayoutInflaterFactory {
    static final Interpolator ACCELERATE_CUBIC;
    static final Interpolator ACCELERATE_QUINT;
    static final int ANIM_DUR = 220;
    public static final int ANIM_STYLE_CLOSE_ENTER = 3;
    public static final int ANIM_STYLE_CLOSE_EXIT = 4;
    public static final int ANIM_STYLE_FADE_ENTER = 5;
    public static final int ANIM_STYLE_FADE_EXIT = 6;
    public static final int ANIM_STYLE_OPEN_ENTER = 1;
    public static final int ANIM_STYLE_OPEN_EXIT = 2;
    static boolean DEBUG = false;
    static final Interpolator DECELERATE_CUBIC;
    static final Interpolator DECELERATE_QUINT;
    static final boolean HONEYCOMB;
    static final String TAG = "FragmentManager";
    static final String TARGET_REQUEST_CODE_STATE_TAG = "android:target_req_state";
    static final String TARGET_STATE_TAG = "android:target_state";
    static final String USER_VISIBLE_HINT_TAG = "android:user_visible_hint";
    static final String VIEW_STATE_TAG = "android:view_state";
    static Field sAnimationListenerField = null;
    ArrayList<Fragment> mActive;
    ArrayList<Fragment> mAdded;
    ArrayList<Integer> mAvailBackStackIndices;
    ArrayList<Integer> mAvailIndices;
    ArrayList<BackStackRecord> mBackStack;
    ArrayList<FragmentManager.OnBackStackChangedListener> mBackStackChangeListeners;
    ArrayList<BackStackRecord> mBackStackIndices;
    FragmentContainer mContainer;
    FragmentController mController;
    ArrayList<Fragment> mCreatedMenus;
    int mCurState = 0;
    boolean mDestroyed;
    Runnable mExecCommit;
    boolean mExecutingActions;
    boolean mHavePendingDeferredStart;
    FragmentHostCallback mHost;
    boolean mNeedMenuInvalidate;
    String mNoTransactionsBecause;
    Fragment mParent;
    ArrayList<Runnable> mPendingActions;
    SparseArray<Parcelable> mStateArray = null;
    Bundle mStateBundle = null;
    boolean mStateSaved;
    Runnable[] mTmpActions;

    FragmentManagerImpl() {
        Runnable runnable;
        new Runnable() {
            public void run() {
                boolean execPendingActions = FragmentManagerImpl.this.execPendingActions();
            }
        };
        this.mExecCommit = runnable;
    }

    static {
        boolean z;
        Interpolator interpolator;
        Interpolator interpolator2;
        Interpolator interpolator3;
        Interpolator interpolator4;
        if (Build.VERSION.SDK_INT >= 11) {
            z = true;
        } else {
            z = false;
        }
        HONEYCOMB = z;
        new DecelerateInterpolator(2.5f);
        DECELERATE_QUINT = interpolator;
        new DecelerateInterpolator(1.5f);
        DECELERATE_CUBIC = interpolator2;
        new AccelerateInterpolator(2.5f);
        ACCELERATE_QUINT = interpolator3;
        new AccelerateInterpolator(1.5f);
        ACCELERATE_CUBIC = interpolator4;
    }

    /* compiled from: FragmentManager */
    static class AnimateOnHWLayerIfNeededListener implements Animation.AnimationListener {
        private Animation.AnimationListener mOrignalListener = null;
        private boolean mShouldRunOnHWLayer = false;
        /* access modifiers changed from: private */
        public View mView = null;

        public AnimateOnHWLayerIfNeededListener(View view, Animation animation) {
            View view2 = view;
            Animation animation2 = animation;
            if (view2 != null && animation2 != null) {
                this.mView = view2;
            }
        }

        public AnimateOnHWLayerIfNeededListener(View view, Animation animation, Animation.AnimationListener animationListener) {
            View view2 = view;
            Animation animation2 = animation;
            Animation.AnimationListener animationListener2 = animationListener;
            if (view2 != null && animation2 != null) {
                this.mOrignalListener = animationListener2;
                this.mView = view2;
            }
        }

        @CallSuper
        public void onAnimationStart(Animation animation) {
            Runnable runnable;
            Animation animation2 = animation;
            if (this.mView != null) {
                this.mShouldRunOnHWLayer = FragmentManagerImpl.shouldRunOnHWLayer(this.mView, animation2);
                if (this.mShouldRunOnHWLayer) {
                    new Runnable() {
                        public void run() {
                            ViewCompat.setLayerType(AnimateOnHWLayerIfNeededListener.this.mView, 2, null);
                        }
                    };
                    boolean post = this.mView.post(runnable);
                }
            }
            if (this.mOrignalListener != null) {
                this.mOrignalListener.onAnimationStart(animation2);
            }
        }

        @CallSuper
        public void onAnimationEnd(Animation animation) {
            Runnable runnable;
            Animation animation2 = animation;
            if (this.mView != null && this.mShouldRunOnHWLayer) {
                new Runnable() {
                    public void run() {
                        ViewCompat.setLayerType(AnimateOnHWLayerIfNeededListener.this.mView, 0, null);
                    }
                };
                boolean post = this.mView.post(runnable);
            }
            if (this.mOrignalListener != null) {
                this.mOrignalListener.onAnimationEnd(animation2);
            }
        }

        public void onAnimationRepeat(Animation animation) {
            Animation animation2 = animation;
            if (this.mOrignalListener != null) {
                this.mOrignalListener.onAnimationRepeat(animation2);
            }
        }
    }

    static boolean modifiesAlpha(Animation animation) {
        Animation animation2 = animation;
        if (animation2 instanceof AlphaAnimation) {
            return true;
        }
        if (animation2 instanceof AnimationSet) {
            List<Animation> animations = ((AnimationSet) animation2).getAnimations();
            for (int i = 0; i < animations.size(); i++) {
                if (animations.get(i) instanceof AlphaAnimation) {
                    return true;
                }
            }
        }
        return false;
    }

    static boolean shouldRunOnHWLayer(View view, Animation animation) {
        View view2 = view;
        return Build.VERSION.SDK_INT >= 19 && ViewCompat.getLayerType(view2) == 0 && ViewCompat.hasOverlappingRendering(view2) && modifiesAlpha(animation);
    }

    private void throwException(RuntimeException runtimeException) {
        Writer writer;
        PrintWriter printWriter;
        RuntimeException runtimeException2 = runtimeException;
        int e = Log.e(TAG, runtimeException2.getMessage());
        int e2 = Log.e(TAG, "Activity state:");
        new LogWriter(TAG);
        new PrintWriter(writer);
        PrintWriter printWriter2 = printWriter;
        if (this.mHost != null) {
            try {
                this.mHost.onDump("  ", null, printWriter2, new String[0]);
            } catch (Exception e3) {
                int e4 = Log.e(TAG, "Failed dumping state", e3);
            }
        } else {
            try {
                dump("  ", null, printWriter2, new String[0]);
            } catch (Exception e5) {
                int e6 = Log.e(TAG, "Failed dumping state", e5);
            }
        }
        throw runtimeException2;
    }

    public FragmentTransaction beginTransaction() {
        FragmentTransaction fragmentTransaction;
        new BackStackRecord(this);
        return fragmentTransaction;
    }

    public boolean executePendingTransactions() {
        return execPendingActions();
    }

    public void popBackStack() {
        Runnable runnable;
        new Runnable() {
            public void run() {
                boolean popBackStackState = FragmentManagerImpl.this.popBackStackState(FragmentManagerImpl.this.mHost.getHandler(), null, -1, 0);
            }
        };
        enqueueAction(runnable, false);
    }

    public boolean popBackStackImmediate() {
        checkStateLoss();
        boolean executePendingTransactions = executePendingTransactions();
        return popBackStackState(this.mHost.getHandler(), null, -1, 0);
    }

    public void popBackStack(String str, int i) {
        Runnable runnable;
        final String str2 = str;
        final int i2 = i;
        new Runnable() {
            public void run() {
                boolean popBackStackState = FragmentManagerImpl.this.popBackStackState(FragmentManagerImpl.this.mHost.getHandler(), str2, -1, i2);
            }
        };
        enqueueAction(runnable, false);
    }

    public boolean popBackStackImmediate(String str, int i) {
        checkStateLoss();
        boolean executePendingTransactions = executePendingTransactions();
        return popBackStackState(this.mHost.getHandler(), str, -1, i);
    }

    public void popBackStack(int i, int i2) {
        Runnable runnable;
        Throwable th;
        StringBuilder sb;
        int i3 = i;
        int i4 = i2;
        if (i3 < 0) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("Bad id: ").append(i3).toString());
            throw th2;
        }
        final int i5 = i3;
        final int i6 = i4;
        new Runnable() {
            public void run() {
                boolean popBackStackState = FragmentManagerImpl.this.popBackStackState(FragmentManagerImpl.this.mHost.getHandler(), null, i5, i6);
            }
        };
        enqueueAction(runnable, false);
    }

    public boolean popBackStackImmediate(int i, int i2) {
        Throwable th;
        StringBuilder sb;
        int i3 = i;
        int i4 = i2;
        checkStateLoss();
        boolean executePendingTransactions = executePendingTransactions();
        if (i3 >= 0) {
            return popBackStackState(this.mHost.getHandler(), null, i3, i4);
        }
        Throwable th2 = th;
        new StringBuilder();
        new IllegalArgumentException(sb.append("Bad id: ").append(i3).toString());
        throw th2;
    }

    public int getBackStackEntryCount() {
        return this.mBackStack != null ? this.mBackStack.size() : 0;
    }

    public FragmentManager.BackStackEntry getBackStackEntryAt(int i) {
        return this.mBackStack.get(i);
    }

    public void addOnBackStackChangedListener(FragmentManager.OnBackStackChangedListener onBackStackChangedListener) {
        ArrayList<FragmentManager.OnBackStackChangedListener> arrayList;
        FragmentManager.OnBackStackChangedListener onBackStackChangedListener2 = onBackStackChangedListener;
        if (this.mBackStackChangeListeners == null) {
            new ArrayList<>();
            this.mBackStackChangeListeners = arrayList;
        }
        boolean add = this.mBackStackChangeListeners.add(onBackStackChangedListener2);
    }

    public void removeOnBackStackChangedListener(FragmentManager.OnBackStackChangedListener onBackStackChangedListener) {
        FragmentManager.OnBackStackChangedListener onBackStackChangedListener2 = onBackStackChangedListener;
        if (this.mBackStackChangeListeners != null) {
            boolean remove = this.mBackStackChangeListeners.remove(onBackStackChangedListener2);
        }
    }

    public void putFragment(Bundle bundle, String str, Fragment fragment) {
        RuntimeException runtimeException;
        StringBuilder sb;
        Bundle bundle2 = bundle;
        String str2 = str;
        Fragment fragment2 = fragment;
        if (fragment2.mIndex < 0) {
            new StringBuilder();
            new IllegalStateException(sb.append("Fragment ").append(fragment2).append(" is not currently in the FragmentManager").toString());
            throwException(runtimeException);
        }
        bundle2.putInt(str2, fragment2.mIndex);
    }

    public Fragment getFragment(Bundle bundle, String str) {
        RuntimeException runtimeException;
        StringBuilder sb;
        RuntimeException runtimeException2;
        StringBuilder sb2;
        String str2 = str;
        int i = bundle.getInt(str2, -1);
        if (i == -1) {
            return null;
        }
        if (i >= this.mActive.size()) {
            new StringBuilder();
            new IllegalStateException(sb2.append("Fragment no longer exists for key ").append(str2).append(": index ").append(i).toString());
            throwException(runtimeException2);
        }
        Fragment fragment = this.mActive.get(i);
        if (fragment == null) {
            new StringBuilder();
            new IllegalStateException(sb.append("Fragment no longer exists for key ").append(str2).append(": index ").append(i).toString());
            throwException(runtimeException);
        }
        return fragment;
    }

    public List<Fragment> getFragments() {
        return this.mActive;
    }

    public Fragment.SavedState saveFragmentInstanceState(Fragment fragment) {
        Fragment.SavedState savedState;
        Fragment.SavedState savedState2;
        RuntimeException runtimeException;
        StringBuilder sb;
        Fragment fragment2 = fragment;
        if (fragment2.mIndex < 0) {
            new StringBuilder();
            new IllegalStateException(sb.append("Fragment ").append(fragment2).append(" is not currently in the FragmentManager").toString());
            throwException(runtimeException);
        }
        if (fragment2.mState <= 0) {
            return null;
        }
        Bundle saveFragmentBasicState = saveFragmentBasicState(fragment2);
        if (saveFragmentBasicState != null) {
            savedState = savedState2;
            new Fragment.SavedState(saveFragmentBasicState);
        } else {
            savedState = null;
        }
        return savedState;
    }

    public boolean isDestroyed() {
        return this.mDestroyed;
    }

    public String toString() {
        StringBuilder sb;
        new StringBuilder(128);
        StringBuilder sb2 = sb;
        StringBuilder append = sb2.append("FragmentManager{");
        StringBuilder append2 = sb2.append(Integer.toHexString(System.identityHashCode(this)));
        StringBuilder append3 = sb2.append(" in ");
        if (this.mParent != null) {
            DebugUtils.buildShortClassTag(this.mParent, sb2);
        } else {
            DebugUtils.buildShortClassTag(this.mHost, sb2);
        }
        StringBuilder append4 = sb2.append("}}");
        return sb2.toString();
    }

    /* JADX INFO: finally extract failed */
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        StringBuilder sb;
        int size;
        int size2;
        int size3;
        int size4;
        int size5;
        int size6;
        String str2 = str;
        FileDescriptor fileDescriptor2 = fileDescriptor;
        PrintWriter printWriter2 = printWriter;
        String[] strArr2 = strArr;
        new StringBuilder();
        String sb2 = sb.append(str2).append("    ").toString();
        if (this.mActive != null && (size6 = this.mActive.size()) > 0) {
            printWriter2.print(str2);
            printWriter2.print("Active Fragments in ");
            printWriter2.print(Integer.toHexString(System.identityHashCode(this)));
            printWriter2.println(":");
            for (int i = 0; i < size6; i++) {
                Fragment fragment = this.mActive.get(i);
                printWriter2.print(str2);
                printWriter2.print("  #");
                printWriter2.print(i);
                printWriter2.print(": ");
                printWriter2.println(fragment);
                if (fragment != null) {
                    fragment.dump(sb2, fileDescriptor2, printWriter2, strArr2);
                }
            }
        }
        if (this.mAdded != null && (size5 = this.mAdded.size()) > 0) {
            printWriter2.print(str2);
            printWriter2.println("Added Fragments:");
            for (int i2 = 0; i2 < size5; i2++) {
                Fragment fragment2 = this.mAdded.get(i2);
                printWriter2.print(str2);
                printWriter2.print("  #");
                printWriter2.print(i2);
                printWriter2.print(": ");
                printWriter2.println(fragment2.toString());
            }
        }
        if (this.mCreatedMenus != null && (size4 = this.mCreatedMenus.size()) > 0) {
            printWriter2.print(str2);
            printWriter2.println("Fragments Created Menus:");
            for (int i3 = 0; i3 < size4; i3++) {
                Fragment fragment3 = this.mCreatedMenus.get(i3);
                printWriter2.print(str2);
                printWriter2.print("  #");
                printWriter2.print(i3);
                printWriter2.print(": ");
                printWriter2.println(fragment3.toString());
            }
        }
        if (this.mBackStack != null && (size3 = this.mBackStack.size()) > 0) {
            printWriter2.print(str2);
            printWriter2.println("Back Stack:");
            for (int i4 = 0; i4 < size3; i4++) {
                BackStackRecord backStackRecord = this.mBackStack.get(i4);
                printWriter2.print(str2);
                printWriter2.print("  #");
                printWriter2.print(i4);
                printWriter2.print(": ");
                printWriter2.println(backStackRecord.toString());
                backStackRecord.dump(sb2, fileDescriptor2, printWriter2, strArr2);
            }
        }
        synchronized (this) {
            try {
                if (this.mBackStackIndices != null && (size2 = this.mBackStackIndices.size()) > 0) {
                    printWriter2.print(str2);
                    printWriter2.println("Back Stack Indices:");
                    for (int i5 = 0; i5 < size2; i5++) {
                        BackStackRecord backStackRecord2 = this.mBackStackIndices.get(i5);
                        printWriter2.print(str2);
                        printWriter2.print("  #");
                        printWriter2.print(i5);
                        printWriter2.print(": ");
                        printWriter2.println(backStackRecord2);
                    }
                }
                if (this.mAvailBackStackIndices != null && this.mAvailBackStackIndices.size() > 0) {
                    printWriter2.print(str2);
                    printWriter2.print("mAvailBackStackIndices: ");
                    printWriter2.println(Arrays.toString(this.mAvailBackStackIndices.toArray()));
                }
                if (this.mPendingActions != null && (size = this.mPendingActions.size()) > 0) {
                    printWriter2.print(str2);
                    printWriter2.println("Pending Actions:");
                    for (int i6 = 0; i6 < size; i6++) {
                        Runnable runnable = this.mPendingActions.get(i6);
                        printWriter2.print(str2);
                        printWriter2.print("  #");
                        printWriter2.print(i6);
                        printWriter2.print(": ");
                        printWriter2.println(runnable);
                    }
                }
                printWriter2.print(str2);
                printWriter2.println("FragmentManager misc state:");
                printWriter2.print(str2);
                printWriter2.print("  mHost=");
                printWriter2.println(this.mHost);
                printWriter2.print(str2);
                printWriter2.print("  mContainer=");
                printWriter2.println(this.mContainer);
                if (this.mParent != null) {
                    printWriter2.print(str2);
                    printWriter2.print("  mParent=");
                    printWriter2.println(this.mParent);
                }
                printWriter2.print(str2);
                printWriter2.print("  mCurState=");
                printWriter2.print(this.mCurState);
                printWriter2.print(" mStateSaved=");
                printWriter2.print(this.mStateSaved);
                printWriter2.print(" mDestroyed=");
                printWriter2.println(this.mDestroyed);
                if (this.mNeedMenuInvalidate) {
                    printWriter2.print(str2);
                    printWriter2.print("  mNeedMenuInvalidate=");
                    printWriter2.println(this.mNeedMenuInvalidate);
                }
                if (this.mNoTransactionsBecause != null) {
                    printWriter2.print(str2);
                    printWriter2.print("  mNoTransactionsBecause=");
                    printWriter2.println(this.mNoTransactionsBecause);
                }
                if (this.mAvailIndices != null && this.mAvailIndices.size() > 0) {
                    printWriter2.print(str2);
                    printWriter2.print("  mAvailIndices: ");
                    printWriter2.println(Arrays.toString(this.mAvailIndices.toArray()));
                }
            } catch (Throwable th) {
                while (true) {
                    throw th;
                }
            }
        }
    }

    static Animation makeOpenCloseAnimation(Context context, float f, float f2, float f3, float f4) {
        AnimationSet animationSet;
        ScaleAnimation scaleAnimation;
        AlphaAnimation alphaAnimation;
        float f5 = f;
        float f6 = f2;
        new AnimationSet(false);
        AnimationSet animationSet2 = animationSet;
        new ScaleAnimation(f5, f6, f5, f6, 1, 0.5f, 1, 0.5f);
        ScaleAnimation scaleAnimation2 = scaleAnimation;
        scaleAnimation2.setInterpolator(DECELERATE_QUINT);
        scaleAnimation2.setDuration(220);
        animationSet2.addAnimation(scaleAnimation2);
        new AlphaAnimation(f3, f4);
        AlphaAnimation alphaAnimation2 = alphaAnimation;
        alphaAnimation2.setInterpolator(DECELERATE_CUBIC);
        alphaAnimation2.setDuration(220);
        animationSet2.addAnimation(alphaAnimation2);
        return animationSet2;
    }

    static Animation makeFadeAnimation(Context context, float f, float f2) {
        AlphaAnimation alphaAnimation;
        new AlphaAnimation(f, f2);
        AlphaAnimation alphaAnimation2 = alphaAnimation;
        alphaAnimation2.setInterpolator(DECELERATE_CUBIC);
        alphaAnimation2.setDuration(220);
        return alphaAnimation2;
    }

    /* access modifiers changed from: package-private */
    public Animation loadAnimation(Fragment fragment, int i, boolean z, int i2) {
        Animation loadAnimation;
        Fragment fragment2 = fragment;
        int i3 = i;
        boolean z2 = z;
        int i4 = i2;
        Animation onCreateAnimation = fragment2.onCreateAnimation(i3, z2, fragment2.mNextAnim);
        if (onCreateAnimation != null) {
            return onCreateAnimation;
        }
        if (fragment2.mNextAnim != 0 && (loadAnimation = AnimationUtils.loadAnimation(this.mHost.getContext(), fragment2.mNextAnim)) != null) {
            return loadAnimation;
        }
        if (i3 == 0) {
            return null;
        }
        int transitToStyleIndex = transitToStyleIndex(i3, z2);
        if (transitToStyleIndex < 0) {
            return null;
        }
        switch (transitToStyleIndex) {
            case 1:
                return makeOpenCloseAnimation(this.mHost.getContext(), 1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                return makeOpenCloseAnimation(this.mHost.getContext(), 1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                return makeOpenCloseAnimation(this.mHost.getContext(), 0.975f, 1.0f, 0.0f, 1.0f);
            case 4:
                return makeOpenCloseAnimation(this.mHost.getContext(), 1.0f, 1.075f, 1.0f, 0.0f);
            case 5:
                return makeFadeAnimation(this.mHost.getContext(), 0.0f, 1.0f);
            case 6:
                return makeFadeAnimation(this.mHost.getContext(), 1.0f, 0.0f);
            default:
                if (i4 == 0 && this.mHost.onHasWindowAnimations()) {
                    i4 = this.mHost.onGetWindowAnimations();
                }
                if (i4 == 0) {
                    return null;
                }
                return null;
        }
    }

    public void performPendingDeferredStart(Fragment fragment) {
        Fragment fragment2 = fragment;
        if (!fragment2.mDeferStart) {
            return;
        }
        if (this.mExecutingActions) {
            this.mHavePendingDeferredStart = true;
            return;
        }
        fragment2.mDeferStart = false;
        moveToState(fragment2, this.mCurState, 0, 0, false);
    }

    private void setHWLayerAnimListenerIfAlpha(View view, Animation animation) {
        Animation.AnimationListener animationListener;
        View view2 = view;
        Animation animation2 = animation;
        if (view2 != null && animation2 != null && shouldRunOnHWLayer(view2, animation2)) {
            Animation.AnimationListener animationListener2 = null;
            try {
                if (sAnimationListenerField == null) {
                    sAnimationListenerField = Animation.class.getDeclaredField("mListener");
                    sAnimationListenerField.setAccessible(true);
                }
                animationListener2 = (Animation.AnimationListener) sAnimationListenerField.get(animation2);
            } catch (NoSuchFieldException e) {
                int e2 = Log.e(TAG, "No field with the name mListener is found in Animation class", e);
            } catch (IllegalAccessException e3) {
                int e4 = Log.e(TAG, "Cannot access Animation's mListener field", e3);
            }
            new AnimateOnHWLayerIfNeededListener(view2, animation2, animationListener2);
            animation2.setAnimationListener(animationListener);
        }
    }

    /* access modifiers changed from: package-private */
    public void moveToState(Fragment fragment, int i, int i2, int i3, boolean z) {
        Animation.AnimationListener animationListener;
        StringBuilder sb;
        StringBuilder sb2;
        StringBuilder sb3;
        StringBuilder sb4;
        Throwable th;
        StringBuilder sb5;
        StringBuilder sb6;
        StringBuilder sb7;
        StringBuilder sb8;
        RuntimeException runtimeException;
        StringBuilder sb9;
        StringBuilder sb10;
        Throwable th2;
        StringBuilder sb11;
        StringBuilder sb12;
        Fragment fragment2 = fragment;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        boolean z2 = z;
        if ((!fragment2.mAdded || fragment2.mDetached) && i4 > 1) {
            i4 = 1;
        }
        if (fragment2.mRemoving && i4 > fragment2.mState) {
            i4 = fragment2.mState;
        }
        if (fragment2.mDeferStart && fragment2.mState < 4 && i4 > 3) {
            i4 = 3;
        }
        if (fragment2.mState >= i4) {
            if (fragment2.mState > i4) {
                switch (fragment2.mState) {
                    case 5:
                        if (i4 < 5) {
                            if (DEBUG) {
                                new StringBuilder();
                                int v = Log.v(TAG, sb4.append("movefrom RESUMED: ").append(fragment2).toString());
                            }
                            fragment2.performPause();
                            fragment2.mResumed = false;
                        }
                    case 4:
                        if (i4 < 4) {
                            if (DEBUG) {
                                new StringBuilder();
                                int v2 = Log.v(TAG, sb3.append("movefrom STARTED: ").append(fragment2).toString());
                            }
                            fragment2.performStop();
                        }
                    case 3:
                        if (i4 < 3) {
                            if (DEBUG) {
                                new StringBuilder();
                                int v3 = Log.v(TAG, sb2.append("movefrom STOPPED: ").append(fragment2).toString());
                            }
                            fragment2.performReallyStop();
                        }
                    case 2:
                        if (i4 < 2) {
                            if (DEBUG) {
                                new StringBuilder();
                                int v4 = Log.v(TAG, sb.append("movefrom ACTIVITY_CREATED: ").append(fragment2).toString());
                            }
                            if (fragment2.mView != null && this.mHost.onShouldSaveFragmentState(fragment2) && fragment2.mSavedViewState == null) {
                                saveFragmentViewState(fragment2);
                            }
                            fragment2.performDestroyView();
                            if (!(fragment2.mView == null || fragment2.mContainer == null)) {
                                Animation animation = null;
                                if (this.mCurState > 0 && !this.mDestroyed) {
                                    animation = loadAnimation(fragment2, i5, false, i6);
                                }
                                if (animation != null) {
                                    fragment2.mAnimatingAway = fragment2.mView;
                                    fragment2.mStateAfterAnimating = i4;
                                    final Fragment fragment3 = fragment2;
                                    new AnimateOnHWLayerIfNeededListener(fragment2.mView, animation) {
                                        public void onAnimationEnd(Animation animation) {
                                            super.onAnimationEnd(animation);
                                            if (fragment3.mAnimatingAway != null) {
                                                fragment3.mAnimatingAway = null;
                                                FragmentManagerImpl.this.moveToState(fragment3, fragment3.mStateAfterAnimating, 0, 0, false);
                                            }
                                        }
                                    };
                                    animation.setAnimationListener(animationListener);
                                    fragment2.mView.startAnimation(animation);
                                }
                                fragment2.mContainer.removeView(fragment2.mView);
                            }
                            fragment2.mContainer = null;
                            fragment2.mView = null;
                            fragment2.mInnerView = null;
                        }
                    case 1:
                        if (i4 < 1) {
                            if (this.mDestroyed && fragment2.mAnimatingAway != null) {
                                View view = fragment2.mAnimatingAway;
                                fragment2.mAnimatingAway = null;
                                view.clearAnimation();
                            }
                            if (fragment2.mAnimatingAway == null) {
                                if (DEBUG) {
                                    new StringBuilder();
                                    int v5 = Log.v(TAG, sb6.append("movefrom CREATED: ").append(fragment2).toString());
                                }
                                if (!fragment2.mRetaining) {
                                    fragment2.performDestroy();
                                }
                                fragment2.mCalled = false;
                                fragment2.onDetach();
                                if (fragment2.mCalled) {
                                    if (!z2) {
                                        if (fragment2.mRetaining) {
                                            fragment2.mHost = null;
                                            fragment2.mParentFragment = null;
                                            fragment2.mFragmentManager = null;
                                            fragment2.mChildFragmentManager = null;
                                            break;
                                        } else {
                                            makeInactive(fragment2);
                                            break;
                                        }
                                    }
                                } else {
                                    Throwable th3 = th;
                                    new StringBuilder();
                                    new SuperNotCalledException(sb5.append("Fragment ").append(fragment2).append(" did not call through to super.onDetach()").toString());
                                    throw th3;
                                }
                            } else {
                                fragment2.mStateAfterAnimating = i4;
                                i4 = 1;
                                break;
                            }
                        }
                        break;
                }
            }
        } else if (!fragment2.mFromLayout || fragment2.mInLayout) {
            if (fragment2.mAnimatingAway != null) {
                fragment2.mAnimatingAway = null;
                moveToState(fragment2, fragment2.mStateAfterAnimating, 0, 0, true);
            }
            switch (fragment2.mState) {
                case 0:
                    if (DEBUG) {
                        new StringBuilder();
                        int v6 = Log.v(TAG, sb12.append("moveto CREATED: ").append(fragment2).toString());
                    }
                    if (fragment2.mSavedFragmentState != null) {
                        fragment2.mSavedFragmentState.setClassLoader(this.mHost.getContext().getClassLoader());
                        fragment2.mSavedViewState = fragment2.mSavedFragmentState.getSparseParcelableArray(VIEW_STATE_TAG);
                        fragment2.mTarget = getFragment(fragment2.mSavedFragmentState, TARGET_STATE_TAG);
                        if (fragment2.mTarget != null) {
                            fragment2.mTargetRequestCode = fragment2.mSavedFragmentState.getInt(TARGET_REQUEST_CODE_STATE_TAG, 0);
                        }
                        fragment2.mUserVisibleHint = fragment2.mSavedFragmentState.getBoolean(USER_VISIBLE_HINT_TAG, true);
                        if (!fragment2.mUserVisibleHint) {
                            fragment2.mDeferStart = true;
                            if (i4 > 3) {
                                i4 = 3;
                            }
                        }
                    }
                    fragment2.mHost = this.mHost;
                    fragment2.mParentFragment = this.mParent;
                    fragment2.mFragmentManager = this.mParent != null ? this.mParent.mChildFragmentManager : this.mHost.getFragmentManagerImpl();
                    fragment2.mCalled = false;
                    fragment2.onAttach(this.mHost.getContext());
                    if (!fragment2.mCalled) {
                        Throwable th4 = th2;
                        new StringBuilder();
                        new SuperNotCalledException(sb11.append("Fragment ").append(fragment2).append(" did not call through to super.onAttach()").toString());
                        throw th4;
                    }
                    if (fragment2.mParentFragment == null) {
                        this.mHost.onAttachFragment(fragment2);
                    }
                    if (!fragment2.mRetaining) {
                        fragment2.performCreate(fragment2.mSavedFragmentState);
                    }
                    fragment2.mRetaining = false;
                    if (fragment2.mFromLayout) {
                        fragment2.mView = fragment2.performCreateView(fragment2.getLayoutInflater(fragment2.mSavedFragmentState), null, fragment2.mSavedFragmentState);
                        if (fragment2.mView != null) {
                            fragment2.mInnerView = fragment2.mView;
                            if (Build.VERSION.SDK_INT >= 11) {
                                ViewCompat.setSaveFromParentEnabled(fragment2.mView, false);
                            } else {
                                fragment2.mView = NoSaveStateFrameLayout.wrap(fragment2.mView);
                            }
                            if (fragment2.mHidden) {
                                fragment2.mView.setVisibility(8);
                            }
                            fragment2.onViewCreated(fragment2.mView, fragment2.mSavedFragmentState);
                        } else {
                            fragment2.mInnerView = null;
                        }
                    }
                case 1:
                    if (i4 > 1) {
                        if (DEBUG) {
                            new StringBuilder();
                            int v7 = Log.v(TAG, sb10.append("moveto ACTIVITY_CREATED: ").append(fragment2).toString());
                        }
                        if (!fragment2.mFromLayout) {
                            ViewGroup viewGroup = null;
                            if (fragment2.mContainerId != 0) {
                                viewGroup = (ViewGroup) this.mContainer.onFindViewById(fragment2.mContainerId);
                                if (viewGroup == null && !fragment2.mRestored) {
                                    new StringBuilder();
                                    new IllegalArgumentException(sb9.append("No view found for id 0x").append(Integer.toHexString(fragment2.mContainerId)).append(" (").append(fragment2.getResources().getResourceName(fragment2.mContainerId)).append(") for fragment ").append(fragment2).toString());
                                    throwException(runtimeException);
                                }
                            }
                            fragment2.mContainer = viewGroup;
                            fragment2.mView = fragment2.performCreateView(fragment2.getLayoutInflater(fragment2.mSavedFragmentState), viewGroup, fragment2.mSavedFragmentState);
                            if (fragment2.mView != null) {
                                fragment2.mInnerView = fragment2.mView;
                                if (Build.VERSION.SDK_INT >= 11) {
                                    ViewCompat.setSaveFromParentEnabled(fragment2.mView, false);
                                } else {
                                    fragment2.mView = NoSaveStateFrameLayout.wrap(fragment2.mView);
                                }
                                if (viewGroup != null) {
                                    Animation loadAnimation = loadAnimation(fragment2, i5, true, i6);
                                    if (loadAnimation != null) {
                                        setHWLayerAnimListenerIfAlpha(fragment2.mView, loadAnimation);
                                        fragment2.mView.startAnimation(loadAnimation);
                                    }
                                    viewGroup.addView(fragment2.mView);
                                }
                                if (fragment2.mHidden) {
                                    fragment2.mView.setVisibility(8);
                                }
                                fragment2.onViewCreated(fragment2.mView, fragment2.mSavedFragmentState);
                            } else {
                                fragment2.mInnerView = null;
                            }
                        }
                        fragment2.performActivityCreated(fragment2.mSavedFragmentState);
                        if (fragment2.mView != null) {
                            fragment2.restoreViewState(fragment2.mSavedFragmentState);
                        }
                        fragment2.mSavedFragmentState = null;
                    }
                case 2:
                case 3:
                    if (i4 > 3) {
                        if (DEBUG) {
                            new StringBuilder();
                            int v8 = Log.v(TAG, sb8.append("moveto STARTED: ").append(fragment2).toString());
                        }
                        fragment2.performStart();
                    }
                case 4:
                    if (i4 > 4) {
                        if (DEBUG) {
                            new StringBuilder();
                            int v9 = Log.v(TAG, sb7.append("moveto RESUMED: ").append(fragment2).toString());
                        }
                        fragment2.mResumed = true;
                        fragment2.performResume();
                        fragment2.mSavedFragmentState = null;
                        fragment2.mSavedViewState = null;
                        break;
                    }
                    break;
            }
        } else {
            return;
        }
        fragment2.mState = i4;
    }

    /* access modifiers changed from: package-private */
    public void moveToState(Fragment fragment) {
        moveToState(fragment, this.mCurState, 0, 0, false);
    }

    /* access modifiers changed from: package-private */
    public void moveToState(int i, boolean z) {
        moveToState(i, 0, 0, z);
    }

    /* access modifiers changed from: package-private */
    public void moveToState(int i, int i2, int i3, boolean z) {
        Throwable th;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        boolean z2 = z;
        if (this.mHost == null && i4 != 0) {
            Throwable th2 = th;
            new IllegalStateException("No host");
            throw th2;
        } else if (z2 || this.mCurState != i4) {
            this.mCurState = i4;
            if (this.mActive != null) {
                boolean z3 = false;
                for (int i7 = 0; i7 < this.mActive.size(); i7++) {
                    Fragment fragment = this.mActive.get(i7);
                    if (fragment != null) {
                        moveToState(fragment, i4, i5, i6, false);
                        if (fragment.mLoaderManager != null) {
                            z3 |= fragment.mLoaderManager.hasRunningLoaders();
                        }
                    }
                }
                if (!z3) {
                    startPendingDeferredFragments();
                }
                if (this.mNeedMenuInvalidate && this.mHost != null && this.mCurState == 5) {
                    this.mHost.onSupportInvalidateOptionsMenu();
                    this.mNeedMenuInvalidate = false;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void startPendingDeferredFragments() {
        if (this.mActive != null) {
            for (int i = 0; i < this.mActive.size(); i++) {
                Fragment fragment = this.mActive.get(i);
                if (fragment != null) {
                    performPendingDeferredStart(fragment);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void makeActive(Fragment fragment) {
        StringBuilder sb;
        ArrayList<Fragment> arrayList;
        Fragment fragment2 = fragment;
        if (fragment2.mIndex < 0) {
            if (this.mAvailIndices == null || this.mAvailIndices.size() <= 0) {
                if (this.mActive == null) {
                    new ArrayList<>();
                    this.mActive = arrayList;
                }
                fragment2.setIndex(this.mActive.size(), this.mParent);
                boolean add = this.mActive.add(fragment2);
            } else {
                fragment2.setIndex(this.mAvailIndices.remove(this.mAvailIndices.size() - 1).intValue(), this.mParent);
                Fragment fragment3 = this.mActive.set(fragment2.mIndex, fragment2);
            }
            if (DEBUG) {
                new StringBuilder();
                int v = Log.v(TAG, sb.append("Allocated fragment index ").append(fragment2).toString());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void makeInactive(Fragment fragment) {
        ArrayList<Integer> arrayList;
        StringBuilder sb;
        Fragment fragment2 = fragment;
        if (fragment2.mIndex >= 0) {
            if (DEBUG) {
                new StringBuilder();
                int v = Log.v(TAG, sb.append("Freeing fragment index ").append(fragment2).toString());
            }
            Fragment fragment3 = this.mActive.set(fragment2.mIndex, null);
            if (this.mAvailIndices == null) {
                new ArrayList<>();
                this.mAvailIndices = arrayList;
            }
            boolean add = this.mAvailIndices.add(Integer.valueOf(fragment2.mIndex));
            this.mHost.inactivateFragment(fragment2.mWho);
            fragment2.initState();
        }
    }

    public void addFragment(Fragment fragment, boolean z) {
        Throwable th;
        StringBuilder sb;
        StringBuilder sb2;
        ArrayList<Fragment> arrayList;
        Fragment fragment2 = fragment;
        boolean z2 = z;
        if (this.mAdded == null) {
            new ArrayList<>();
            this.mAdded = arrayList;
        }
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb2.append("add: ").append(fragment2).toString());
        }
        makeActive(fragment2);
        if (fragment2.mDetached) {
            return;
        }
        if (this.mAdded.contains(fragment2)) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("Fragment already added: ").append(fragment2).toString());
            throw th2;
        }
        boolean add = this.mAdded.add(fragment2);
        fragment2.mAdded = true;
        fragment2.mRemoving = false;
        if (fragment2.mHasMenu && fragment2.mMenuVisible) {
            this.mNeedMenuInvalidate = true;
        }
        if (z2) {
            moveToState(fragment2);
        }
    }

    public void removeFragment(Fragment fragment, int i, int i2) {
        StringBuilder sb;
        Fragment fragment2 = fragment;
        int i3 = i;
        int i4 = i2;
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb.append("remove: ").append(fragment2).append(" nesting=").append(fragment2.mBackStackNesting).toString());
        }
        boolean z = !fragment2.isInBackStack();
        if (!fragment2.mDetached || z) {
            if (this.mAdded != null) {
                boolean remove = this.mAdded.remove(fragment2);
            }
            if (fragment2.mHasMenu && fragment2.mMenuVisible) {
                this.mNeedMenuInvalidate = true;
            }
            fragment2.mAdded = false;
            fragment2.mRemoving = true;
            moveToState(fragment2, z ? 0 : 1, i3, i4, false);
        }
    }

    public void hideFragment(Fragment fragment, int i, int i2) {
        StringBuilder sb;
        Fragment fragment2 = fragment;
        int i3 = i;
        int i4 = i2;
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb.append("hide: ").append(fragment2).toString());
        }
        if (!fragment2.mHidden) {
            fragment2.mHidden = true;
            if (fragment2.mView != null) {
                Animation loadAnimation = loadAnimation(fragment2, i3, false, i4);
                if (loadAnimation != null) {
                    setHWLayerAnimListenerIfAlpha(fragment2.mView, loadAnimation);
                    fragment2.mView.startAnimation(loadAnimation);
                }
                fragment2.mView.setVisibility(8);
            }
            if (fragment2.mAdded && fragment2.mHasMenu && fragment2.mMenuVisible) {
                this.mNeedMenuInvalidate = true;
            }
            fragment2.onHiddenChanged(true);
        }
    }

    public void showFragment(Fragment fragment, int i, int i2) {
        StringBuilder sb;
        Fragment fragment2 = fragment;
        int i3 = i;
        int i4 = i2;
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb.append("show: ").append(fragment2).toString());
        }
        if (fragment2.mHidden) {
            fragment2.mHidden = false;
            if (fragment2.mView != null) {
                Animation loadAnimation = loadAnimation(fragment2, i3, true, i4);
                if (loadAnimation != null) {
                    setHWLayerAnimListenerIfAlpha(fragment2.mView, loadAnimation);
                    fragment2.mView.startAnimation(loadAnimation);
                }
                fragment2.mView.setVisibility(0);
            }
            if (fragment2.mAdded && fragment2.mHasMenu && fragment2.mMenuVisible) {
                this.mNeedMenuInvalidate = true;
            }
            fragment2.onHiddenChanged(false);
        }
    }

    public void detachFragment(Fragment fragment, int i, int i2) {
        StringBuilder sb;
        StringBuilder sb2;
        Fragment fragment2 = fragment;
        int i3 = i;
        int i4 = i2;
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb2.append("detach: ").append(fragment2).toString());
        }
        if (!fragment2.mDetached) {
            fragment2.mDetached = true;
            if (fragment2.mAdded) {
                if (this.mAdded != null) {
                    if (DEBUG) {
                        new StringBuilder();
                        int v2 = Log.v(TAG, sb.append("remove from detach: ").append(fragment2).toString());
                    }
                    boolean remove = this.mAdded.remove(fragment2);
                }
                if (fragment2.mHasMenu && fragment2.mMenuVisible) {
                    this.mNeedMenuInvalidate = true;
                }
                fragment2.mAdded = false;
                moveToState(fragment2, 1, i3, i4, false);
            }
        }
    }

    public void attachFragment(Fragment fragment, int i, int i2) {
        StringBuilder sb;
        Throwable th;
        StringBuilder sb2;
        ArrayList<Fragment> arrayList;
        StringBuilder sb3;
        Fragment fragment2 = fragment;
        int i3 = i;
        int i4 = i2;
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb3.append("attach: ").append(fragment2).toString());
        }
        if (fragment2.mDetached) {
            fragment2.mDetached = false;
            if (!fragment2.mAdded) {
                if (this.mAdded == null) {
                    new ArrayList<>();
                    this.mAdded = arrayList;
                }
                if (this.mAdded.contains(fragment2)) {
                    Throwable th2 = th;
                    new StringBuilder();
                    new IllegalStateException(sb2.append("Fragment already added: ").append(fragment2).toString());
                    throw th2;
                }
                if (DEBUG) {
                    new StringBuilder();
                    int v2 = Log.v(TAG, sb.append("add from attach: ").append(fragment2).toString());
                }
                boolean add = this.mAdded.add(fragment2);
                fragment2.mAdded = true;
                if (fragment2.mHasMenu && fragment2.mMenuVisible) {
                    this.mNeedMenuInvalidate = true;
                }
                moveToState(fragment2, this.mCurState, i3, i4, false);
            }
        }
    }

    public Fragment findFragmentById(int i) {
        int i2 = i;
        if (this.mAdded != null) {
            for (int size = this.mAdded.size() - 1; size >= 0; size--) {
                Fragment fragment = this.mAdded.get(size);
                if (fragment != null && fragment.mFragmentId == i2) {
                    return fragment;
                }
            }
        }
        if (this.mActive != null) {
            for (int size2 = this.mActive.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = this.mActive.get(size2);
                if (fragment2 != null && fragment2.mFragmentId == i2) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public Fragment findFragmentByTag(String str) {
        String str2 = str;
        if (!(this.mAdded == null || str2 == null)) {
            for (int size = this.mAdded.size() - 1; size >= 0; size--) {
                Fragment fragment = this.mAdded.get(size);
                if (fragment != null && str2.equals(fragment.mTag)) {
                    return fragment;
                }
            }
        }
        if (!(this.mActive == null || str2 == null)) {
            for (int size2 = this.mActive.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = this.mActive.get(size2);
                if (fragment2 != null && str2.equals(fragment2.mTag)) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public Fragment findFragmentByWho(String str) {
        String str2 = str;
        if (!(this.mActive == null || str2 == null)) {
            for (int size = this.mActive.size() - 1; size >= 0; size--) {
                Fragment fragment = this.mActive.get(size);
                if (fragment != null) {
                    Fragment findFragmentByWho = fragment.findFragmentByWho(str2);
                    Fragment fragment2 = findFragmentByWho;
                    if (findFragmentByWho != null) {
                        return fragment2;
                    }
                }
            }
        }
        return null;
    }

    private void checkStateLoss() {
        Throwable th;
        StringBuilder sb;
        Throwable th2;
        if (this.mStateSaved) {
            Throwable th3 = th2;
            new IllegalStateException("Can not perform this action after onSaveInstanceState");
            throw th3;
        } else if (this.mNoTransactionsBecause != null) {
            Throwable th4 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("Can not perform this action inside of ").append(this.mNoTransactionsBecause).toString());
            throw th4;
        }
    }

    public void enqueueAction(Runnable runnable, boolean z) {
        Throwable th;
        ArrayList<Runnable> arrayList;
        Runnable runnable2 = runnable;
        if (!z) {
            checkStateLoss();
        }
        synchronized (this) {
            try {
                if (this.mDestroyed || this.mHost == null) {
                    Throwable th2 = th;
                    new IllegalStateException("Activity has been destroyed");
                    throw th2;
                }
                if (this.mPendingActions == null) {
                    new ArrayList();
                    this.mPendingActions = arrayList;
                }
                boolean add = this.mPendingActions.add(runnable2);
                if (this.mPendingActions.size() == 1) {
                    this.mHost.getHandler().removeCallbacks(this.mExecCommit);
                    boolean post = this.mHost.getHandler().post(this.mExecCommit);
                }
            } catch (Throwable th3) {
                throw th3;
            }
        }
    }

    public int allocBackStackIndex(BackStackRecord backStackRecord) {
        StringBuilder sb;
        ArrayList<BackStackRecord> arrayList;
        StringBuilder sb2;
        BackStackRecord backStackRecord2 = backStackRecord;
        synchronized (this) {
            try {
                if (this.mAvailBackStackIndices == null || this.mAvailBackStackIndices.size() <= 0) {
                    if (this.mBackStackIndices == null) {
                        new ArrayList();
                        this.mBackStackIndices = arrayList;
                    }
                    int size = this.mBackStackIndices.size();
                    if (DEBUG) {
                        new StringBuilder();
                        int v = Log.v(TAG, sb.append("Setting back stack index ").append(size).append(" to ").append(backStackRecord2).toString());
                    }
                    boolean add = this.mBackStackIndices.add(backStackRecord2);
                    int i = size;
                    return i;
                }
                int intValue = this.mAvailBackStackIndices.remove(this.mAvailBackStackIndices.size() - 1).intValue();
                if (DEBUG) {
                    new StringBuilder();
                    int v2 = Log.v(TAG, sb2.append("Adding back stack index ").append(intValue).append(" with ").append(backStackRecord2).toString());
                }
                BackStackRecord backStackRecord3 = this.mBackStackIndices.set(intValue, backStackRecord2);
                int i2 = intValue;
                return i2;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void setBackStackIndex(int i, BackStackRecord backStackRecord) {
        StringBuilder sb;
        StringBuilder sb2;
        ArrayList<Integer> arrayList;
        StringBuilder sb3;
        ArrayList<BackStackRecord> arrayList2;
        int i2 = i;
        BackStackRecord backStackRecord2 = backStackRecord;
        synchronized (this) {
            try {
                if (this.mBackStackIndices == null) {
                    new ArrayList();
                    this.mBackStackIndices = arrayList2;
                }
                int size = this.mBackStackIndices.size();
                if (i2 < size) {
                    if (DEBUG) {
                        new StringBuilder();
                        int v = Log.v(TAG, sb3.append("Setting back stack index ").append(i2).append(" to ").append(backStackRecord2).toString());
                    }
                    BackStackRecord backStackRecord3 = this.mBackStackIndices.set(i2, backStackRecord2);
                } else {
                    while (size < i2) {
                        boolean add = this.mBackStackIndices.add(null);
                        if (this.mAvailBackStackIndices == null) {
                            new ArrayList();
                            this.mAvailBackStackIndices = arrayList;
                        }
                        if (DEBUG) {
                            new StringBuilder();
                            int v2 = Log.v(TAG, sb2.append("Adding available back stack index ").append(size).toString());
                        }
                        boolean add2 = this.mAvailBackStackIndices.add(Integer.valueOf(size));
                        size++;
                    }
                    if (DEBUG) {
                        new StringBuilder();
                        int v3 = Log.v(TAG, sb.append("Adding back stack index ").append(i2).append(" with ").append(backStackRecord2).toString());
                    }
                    boolean add3 = this.mBackStackIndices.add(backStackRecord2);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    public void freeBackStackIndex(int i) {
        StringBuilder sb;
        ArrayList<Integer> arrayList;
        int i2 = i;
        synchronized (this) {
            try {
                BackStackRecord backStackRecord = this.mBackStackIndices.set(i2, null);
                if (this.mAvailBackStackIndices == null) {
                    new ArrayList();
                    this.mAvailBackStackIndices = arrayList;
                }
                if (DEBUG) {
                    new StringBuilder();
                    int v = Log.v(TAG, sb.append("Freeing back stack index ").append(i2).toString());
                }
                boolean add = this.mAvailBackStackIndices.add(Integer.valueOf(i2));
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean execPendingActions() {
        boolean z;
        Throwable th;
        Throwable th2;
        if (this.mExecutingActions) {
            Throwable th3 = th2;
            new IllegalStateException("Recursive entry to executePendingTransactions");
            throw th3;
        } else if (Looper.myLooper() != this.mHost.getHandler().getLooper()) {
            Throwable th4 = th;
            new IllegalStateException("Must be called from main thread of process");
            throw th4;
        } else {
            boolean z2 = false;
            while (true) {
                z = z2;
                synchronized (this) {
                    try {
                        if (this.mPendingActions != null && this.mPendingActions.size() != 0) {
                            int size = this.mPendingActions.size();
                            if (this.mTmpActions == null || this.mTmpActions.length < size) {
                                this.mTmpActions = new Runnable[size];
                            }
                            Object[] array = this.mPendingActions.toArray(this.mTmpActions);
                            this.mPendingActions.clear();
                            this.mHost.getHandler().removeCallbacks(this.mExecCommit);
                            this.mExecutingActions = true;
                            for (int i = 0; i < size; i++) {
                                this.mTmpActions[i].run();
                                this.mTmpActions[i] = null;
                            }
                            this.mExecutingActions = false;
                            z2 = true;
                        }
                    } catch (Throwable th5) {
                        while (true) {
                            throw th5;
                        }
                    }
                }
            }
            if (this.mHavePendingDeferredStart) {
                boolean z3 = false;
                for (int i2 = 0; i2 < this.mActive.size(); i2++) {
                    Fragment fragment = this.mActive.get(i2);
                    if (!(fragment == null || fragment.mLoaderManager == null)) {
                        z3 |= fragment.mLoaderManager.hasRunningLoaders();
                    }
                }
                if (!z3) {
                    this.mHavePendingDeferredStart = false;
                    startPendingDeferredFragments();
                }
            }
            return z;
        }
    }

    /* access modifiers changed from: package-private */
    public void reportBackStackChanged() {
        if (this.mBackStackChangeListeners != null) {
            for (int i = 0; i < this.mBackStackChangeListeners.size(); i++) {
                this.mBackStackChangeListeners.get(i).onBackStackChanged();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void addBackStackState(BackStackRecord backStackRecord) {
        ArrayList<BackStackRecord> arrayList;
        BackStackRecord backStackRecord2 = backStackRecord;
        if (this.mBackStack == null) {
            new ArrayList<>();
            this.mBackStack = arrayList;
        }
        boolean add = this.mBackStack.add(backStackRecord2);
        reportBackStackChanged();
    }

    /* access modifiers changed from: package-private */
    public boolean popBackStackState(Handler handler, String str, int i, int i2) {
        ArrayList arrayList;
        SparseArray sparseArray;
        SparseArray sparseArray2;
        StringBuilder sb;
        SparseArray sparseArray3;
        SparseArray sparseArray4;
        String str2 = str;
        int i3 = i;
        int i4 = i2;
        if (this.mBackStack == null) {
            return false;
        }
        if (str2 == null && i3 < 0 && (i4 & 1) == 0) {
            int size = this.mBackStack.size() - 1;
            if (size < 0) {
                return false;
            }
            BackStackRecord remove = this.mBackStack.remove(size);
            new SparseArray();
            SparseArray sparseArray5 = sparseArray3;
            new SparseArray();
            SparseArray sparseArray6 = sparseArray4;
            remove.calculateBackFragments(sparseArray5, sparseArray6);
            BackStackRecord.TransitionState popFromBackStack = remove.popFromBackStack(true, null, sparseArray5, sparseArray6);
            reportBackStackChanged();
        } else {
            int i5 = -1;
            if (str2 != null || i3 >= 0) {
                i5 = this.mBackStack.size() - 1;
                while (i5 >= 0) {
                    BackStackRecord backStackRecord = this.mBackStack.get(i5);
                    if ((str2 != null && str2.equals(backStackRecord.getName())) || (i3 >= 0 && i3 == backStackRecord.mIndex)) {
                        break;
                    }
                    i5--;
                }
                if (i5 < 0) {
                    return false;
                }
                if ((i4 & 1) != 0) {
                    while (true) {
                        i5--;
                        if (i5 < 0) {
                            break;
                        }
                        BackStackRecord backStackRecord2 = this.mBackStack.get(i5);
                        if ((str2 == null || !str2.equals(backStackRecord2.getName())) && (i3 < 0 || i3 != backStackRecord2.mIndex)) {
                            break;
                        }
                    }
                }
            }
            if (i5 == this.mBackStack.size() - 1) {
                return false;
            }
            new ArrayList();
            ArrayList arrayList2 = arrayList;
            for (int size2 = this.mBackStack.size() - 1; size2 > i5; size2--) {
                boolean add = arrayList2.add(this.mBackStack.remove(size2));
            }
            int size3 = arrayList2.size() - 1;
            new SparseArray();
            SparseArray sparseArray7 = sparseArray;
            new SparseArray();
            SparseArray sparseArray8 = sparseArray2;
            for (int i6 = 0; i6 <= size3; i6++) {
                ((BackStackRecord) arrayList2.get(i6)).calculateBackFragments(sparseArray7, sparseArray8);
            }
            BackStackRecord.TransitionState transitionState = null;
            int i7 = 0;
            while (i7 <= size3) {
                if (DEBUG) {
                    new StringBuilder();
                    int v = Log.v(TAG, sb.append("Popping back stack state: ").append(arrayList2.get(i7)).toString());
                }
                transitionState = ((BackStackRecord) arrayList2.get(i7)).popFromBackStack(i7 == size3, transitionState, sparseArray7, sparseArray8);
                i7++;
            }
            reportBackStackChanged();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<Fragment> retainNonConfig() {
        StringBuilder sb;
        ArrayList<Fragment> arrayList;
        ArrayList<Fragment> arrayList2 = null;
        if (this.mActive != null) {
            for (int i = 0; i < this.mActive.size(); i++) {
                Fragment fragment = this.mActive.get(i);
                if (fragment != null && fragment.mRetainInstance) {
                    if (arrayList2 == null) {
                        new ArrayList<>();
                        arrayList2 = arrayList;
                    }
                    boolean add = arrayList2.add(fragment);
                    fragment.mRetaining = true;
                    fragment.mTargetIndex = fragment.mTarget != null ? fragment.mTarget.mIndex : -1;
                    if (DEBUG) {
                        new StringBuilder();
                        int v = Log.v(TAG, sb.append("retainNonConfig: keeping retained ").append(fragment).toString());
                    }
                }
            }
        }
        return arrayList2;
    }

    /* access modifiers changed from: package-private */
    public void saveFragmentViewState(Fragment fragment) {
        SparseArray<Parcelable> sparseArray;
        Fragment fragment2 = fragment;
        if (fragment2.mInnerView != null) {
            if (this.mStateArray == null) {
                new SparseArray<>();
                this.mStateArray = sparseArray;
            } else {
                this.mStateArray.clear();
            }
            fragment2.mInnerView.saveHierarchyState(this.mStateArray);
            if (this.mStateArray.size() > 0) {
                fragment2.mSavedViewState = this.mStateArray;
                this.mStateArray = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Bundle saveFragmentBasicState(Fragment fragment) {
        Bundle bundle;
        Bundle bundle2;
        Bundle bundle3;
        Fragment fragment2 = fragment;
        Bundle bundle4 = null;
        if (this.mStateBundle == null) {
            new Bundle();
            this.mStateBundle = bundle3;
        }
        fragment2.performSaveInstanceState(this.mStateBundle);
        if (!this.mStateBundle.isEmpty()) {
            bundle4 = this.mStateBundle;
            this.mStateBundle = null;
        }
        if (fragment2.mView != null) {
            saveFragmentViewState(fragment2);
        }
        if (fragment2.mSavedViewState != null) {
            if (bundle4 == null) {
                new Bundle();
                bundle4 = bundle2;
            }
            bundle4.putSparseParcelableArray(VIEW_STATE_TAG, fragment2.mSavedViewState);
        }
        if (!fragment2.mUserVisibleHint) {
            if (bundle4 == null) {
                new Bundle();
                bundle4 = bundle;
            }
            bundle4.putBoolean(USER_VISIBLE_HINT_TAG, fragment2.mUserVisibleHint);
        }
        return bundle4;
    }

    /* access modifiers changed from: package-private */
    public Parcelable saveAllState() {
        FragmentManagerState fragmentManagerState;
        int size;
        BackStackState backStackState;
        StringBuilder sb;
        int size2;
        StringBuilder sb2;
        RuntimeException runtimeException;
        StringBuilder sb3;
        FragmentState fragmentState;
        StringBuilder sb4;
        Bundle bundle;
        RuntimeException runtimeException2;
        StringBuilder sb5;
        RuntimeException runtimeException3;
        StringBuilder sb6;
        boolean execPendingActions = execPendingActions();
        if (HONEYCOMB) {
            this.mStateSaved = true;
        }
        if (this.mActive == null || this.mActive.size() <= 0) {
            return null;
        }
        int size3 = this.mActive.size();
        FragmentState[] fragmentStateArr = new FragmentState[size3];
        boolean z = false;
        for (int i = 0; i < size3; i++) {
            Fragment fragment = this.mActive.get(i);
            if (fragment != null) {
                if (fragment.mIndex < 0) {
                    new StringBuilder();
                    new IllegalStateException(sb6.append("Failure saving state: active ").append(fragment).append(" has cleared index: ").append(fragment.mIndex).toString());
                    throwException(runtimeException3);
                }
                z = true;
                new FragmentState(fragment);
                FragmentState fragmentState2 = fragmentState;
                fragmentStateArr[i] = fragmentState2;
                if (fragment.mState <= 0 || fragmentState2.mSavedFragmentState != null) {
                    fragmentState2.mSavedFragmentState = fragment.mSavedFragmentState;
                } else {
                    fragmentState2.mSavedFragmentState = saveFragmentBasicState(fragment);
                    if (fragment.mTarget != null) {
                        if (fragment.mTarget.mIndex < 0) {
                            new StringBuilder();
                            new IllegalStateException(sb5.append("Failure saving state: ").append(fragment).append(" has target not in fragment manager: ").append(fragment.mTarget).toString());
                            throwException(runtimeException2);
                        }
                        if (fragmentState2.mSavedFragmentState == null) {
                            new Bundle();
                            fragmentState2.mSavedFragmentState = bundle;
                        }
                        putFragment(fragmentState2.mSavedFragmentState, TARGET_STATE_TAG, fragment.mTarget);
                        if (fragment.mTargetRequestCode != 0) {
                            fragmentState2.mSavedFragmentState.putInt(TARGET_REQUEST_CODE_STATE_TAG, fragment.mTargetRequestCode);
                        }
                    }
                }
                if (DEBUG) {
                    new StringBuilder();
                    int v = Log.v(TAG, sb4.append("Saved state of ").append(fragment).append(": ").append(fragmentState2.mSavedFragmentState).toString());
                }
            }
        }
        if (!z) {
            if (DEBUG) {
                int v2 = Log.v(TAG, "saveAllState: no fragments!");
            }
            return null;
        }
        int[] iArr = null;
        BackStackState[] backStackStateArr = null;
        if (this.mAdded != null && (size2 = this.mAdded.size()) > 0) {
            iArr = new int[size2];
            for (int i2 = 0; i2 < size2; i2++) {
                iArr[i2] = this.mAdded.get(i2).mIndex;
                if (iArr[i2] < 0) {
                    new StringBuilder();
                    new IllegalStateException(sb3.append("Failure saving state: active ").append(this.mAdded.get(i2)).append(" has cleared index: ").append(iArr[i2]).toString());
                    throwException(runtimeException);
                }
                if (DEBUG) {
                    new StringBuilder();
                    int v3 = Log.v(TAG, sb2.append("saveAllState: adding fragment #").append(i2).append(": ").append(this.mAdded.get(i2)).toString());
                }
            }
        }
        if (this.mBackStack != null && (size = this.mBackStack.size()) > 0) {
            backStackStateArr = new BackStackState[size];
            for (int i3 = 0; i3 < size; i3++) {
                new BackStackState(this.mBackStack.get(i3));
                backStackStateArr[i3] = backStackState;
                if (DEBUG) {
                    new StringBuilder();
                    int v4 = Log.v(TAG, sb.append("saveAllState: adding back stack #").append(i3).append(": ").append(this.mBackStack.get(i3)).toString());
                }
            }
        }
        new FragmentManagerState();
        FragmentManagerState fragmentManagerState2 = fragmentManagerState;
        fragmentManagerState2.mActive = fragmentStateArr;
        fragmentManagerState2.mAdded = iArr;
        fragmentManagerState2.mBackStack = backStackStateArr;
        return fragmentManagerState2;
    }

    /* access modifiers changed from: package-private */
    public void restoreAllState(Parcelable parcelable, List<Fragment> list) {
        ArrayList<Fragment> arrayList;
        ArrayList<BackStackRecord> arrayList2;
        StringBuilder sb;
        Writer writer;
        PrintWriter printWriter;
        ArrayList<Fragment> arrayList3;
        Throwable th;
        StringBuilder sb2;
        RuntimeException runtimeException;
        StringBuilder sb3;
        StringBuilder sb4;
        StringBuilder sb5;
        ArrayList<Integer> arrayList4;
        StringBuilder sb6;
        StringBuilder sb7;
        Parcelable parcelable2 = parcelable;
        List<Fragment> list2 = list;
        if (parcelable2 != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable2;
            if (fragmentManagerState.mActive != null) {
                if (list2 != null) {
                    for (int i = 0; i < list2.size(); i++) {
                        Fragment fragment = list2.get(i);
                        if (DEBUG) {
                            new StringBuilder();
                            int v = Log.v(TAG, sb7.append("restoreAllState: re-attaching retained ").append(fragment).toString());
                        }
                        FragmentState fragmentState = fragmentManagerState.mActive[fragment.mIndex];
                        fragmentState.mInstance = fragment;
                        fragment.mSavedViewState = null;
                        fragment.mBackStackNesting = 0;
                        fragment.mInLayout = false;
                        fragment.mAdded = false;
                        fragment.mTarget = null;
                        if (fragmentState.mSavedFragmentState != null) {
                            fragmentState.mSavedFragmentState.setClassLoader(this.mHost.getContext().getClassLoader());
                            fragment.mSavedViewState = fragmentState.mSavedFragmentState.getSparseParcelableArray(VIEW_STATE_TAG);
                            fragment.mSavedFragmentState = fragmentState.mSavedFragmentState;
                        }
                    }
                }
                new ArrayList<>(fragmentManagerState.mActive.length);
                this.mActive = arrayList;
                if (this.mAvailIndices != null) {
                    this.mAvailIndices.clear();
                }
                for (int i2 = 0; i2 < fragmentManagerState.mActive.length; i2++) {
                    FragmentState fragmentState2 = fragmentManagerState.mActive[i2];
                    if (fragmentState2 != null) {
                        Fragment instantiate = fragmentState2.instantiate(this.mHost, this.mParent);
                        if (DEBUG) {
                            new StringBuilder();
                            int v2 = Log.v(TAG, sb6.append("restoreAllState: active #").append(i2).append(": ").append(instantiate).toString());
                        }
                        boolean add = this.mActive.add(instantiate);
                        fragmentState2.mInstance = null;
                    } else {
                        boolean add2 = this.mActive.add(null);
                        if (this.mAvailIndices == null) {
                            new ArrayList<>();
                            this.mAvailIndices = arrayList4;
                        }
                        if (DEBUG) {
                            new StringBuilder();
                            int v3 = Log.v(TAG, sb5.append("restoreAllState: avail #").append(i2).toString());
                        }
                        boolean add3 = this.mAvailIndices.add(Integer.valueOf(i2));
                    }
                }
                if (list2 != null) {
                    for (int i3 = 0; i3 < list2.size(); i3++) {
                        Fragment fragment2 = list2.get(i3);
                        if (fragment2.mTargetIndex >= 0) {
                            if (fragment2.mTargetIndex < this.mActive.size()) {
                                fragment2.mTarget = this.mActive.get(fragment2.mTargetIndex);
                            } else {
                                new StringBuilder();
                                int w = Log.w(TAG, sb4.append("Re-attaching retained fragment ").append(fragment2).append(" target no longer exists: ").append(fragment2.mTargetIndex).toString());
                                fragment2.mTarget = null;
                            }
                        }
                    }
                }
                if (fragmentManagerState.mAdded != null) {
                    new ArrayList<>(fragmentManagerState.mAdded.length);
                    this.mAdded = arrayList3;
                    for (int i4 = 0; i4 < fragmentManagerState.mAdded.length; i4++) {
                        Fragment fragment3 = this.mActive.get(fragmentManagerState.mAdded[i4]);
                        if (fragment3 == null) {
                            new StringBuilder();
                            new IllegalStateException(sb3.append("No instantiated fragment for index #").append(fragmentManagerState.mAdded[i4]).toString());
                            throwException(runtimeException);
                        }
                        fragment3.mAdded = true;
                        if (DEBUG) {
                            new StringBuilder();
                            int v4 = Log.v(TAG, sb2.append("restoreAllState: added #").append(i4).append(": ").append(fragment3).toString());
                        }
                        if (this.mAdded.contains(fragment3)) {
                            Throwable th2 = th;
                            new IllegalStateException("Already added!");
                            throw th2;
                        }
                        boolean add4 = this.mAdded.add(fragment3);
                    }
                } else {
                    this.mAdded = null;
                }
                if (fragmentManagerState.mBackStack != null) {
                    new ArrayList<>(fragmentManagerState.mBackStack.length);
                    this.mBackStack = arrayList2;
                    for (int i5 = 0; i5 < fragmentManagerState.mBackStack.length; i5++) {
                        BackStackRecord instantiate2 = fragmentManagerState.mBackStack[i5].instantiate(this);
                        if (DEBUG) {
                            new StringBuilder();
                            int v5 = Log.v(TAG, sb.append("restoreAllState: back stack #").append(i5).append(" (index ").append(instantiate2.mIndex).append("): ").append(instantiate2).toString());
                            new LogWriter(TAG);
                            new PrintWriter(writer);
                            instantiate2.dump("  ", printWriter, false);
                        }
                        boolean add5 = this.mBackStack.add(instantiate2);
                        if (instantiate2.mIndex >= 0) {
                            setBackStackIndex(instantiate2.mIndex, instantiate2);
                        }
                    }
                    return;
                }
                this.mBackStack = null;
            }
        }
    }

    public void attachController(FragmentHostCallback fragmentHostCallback, FragmentContainer fragmentContainer, Fragment fragment) {
        Throwable th;
        FragmentHostCallback fragmentHostCallback2 = fragmentHostCallback;
        FragmentContainer fragmentContainer2 = fragmentContainer;
        Fragment fragment2 = fragment;
        if (this.mHost != null) {
            Throwable th2 = th;
            new IllegalStateException("Already attached");
            throw th2;
        }
        this.mHost = fragmentHostCallback2;
        this.mContainer = fragmentContainer2;
        this.mParent = fragment2;
    }

    public void noteStateNotSaved() {
        this.mStateSaved = false;
    }

    public void dispatchCreate() {
        this.mStateSaved = false;
        moveToState(1, false);
    }

    public void dispatchActivityCreated() {
        this.mStateSaved = false;
        moveToState(2, false);
    }

    public void dispatchStart() {
        this.mStateSaved = false;
        moveToState(4, false);
    }

    public void dispatchResume() {
        this.mStateSaved = false;
        moveToState(5, false);
    }

    public void dispatchPause() {
        moveToState(4, false);
    }

    public void dispatchStop() {
        this.mStateSaved = true;
        moveToState(3, false);
    }

    public void dispatchReallyStop() {
        moveToState(2, false);
    }

    public void dispatchDestroyView() {
        moveToState(1, false);
    }

    public void dispatchDestroy() {
        this.mDestroyed = true;
        boolean execPendingActions = execPendingActions();
        moveToState(0, false);
        this.mHost = null;
        this.mContainer = null;
        this.mParent = null;
    }

    public void dispatchConfigurationChanged(Configuration configuration) {
        Configuration configuration2 = configuration;
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                Fragment fragment = this.mAdded.get(i);
                if (fragment != null) {
                    fragment.performConfigurationChanged(configuration2);
                }
            }
        }
    }

    public void dispatchLowMemory() {
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                Fragment fragment = this.mAdded.get(i);
                if (fragment != null) {
                    fragment.performLowMemory();
                }
            }
        }
    }

    public boolean dispatchCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        ArrayList<Fragment> arrayList;
        Menu menu2 = menu;
        MenuInflater menuInflater2 = menuInflater;
        boolean z = false;
        ArrayList<Fragment> arrayList2 = null;
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                Fragment fragment = this.mAdded.get(i);
                if (fragment != null && fragment.performCreateOptionsMenu(menu2, menuInflater2)) {
                    z = true;
                    if (arrayList2 == null) {
                        new ArrayList<>();
                        arrayList2 = arrayList;
                    }
                    boolean add = arrayList2.add(fragment);
                }
            }
        }
        if (this.mCreatedMenus != null) {
            for (int i2 = 0; i2 < this.mCreatedMenus.size(); i2++) {
                Fragment fragment2 = this.mCreatedMenus.get(i2);
                if (arrayList2 == null || !arrayList2.contains(fragment2)) {
                    fragment2.onDestroyOptionsMenu();
                }
            }
        }
        this.mCreatedMenus = arrayList2;
        return z;
    }

    public boolean dispatchPrepareOptionsMenu(Menu menu) {
        Menu menu2 = menu;
        boolean z = false;
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                Fragment fragment = this.mAdded.get(i);
                if (fragment != null && fragment.performPrepareOptionsMenu(menu2)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public boolean dispatchOptionsItemSelected(MenuItem menuItem) {
        MenuItem menuItem2 = menuItem;
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                Fragment fragment = this.mAdded.get(i);
                if (fragment != null && fragment.performOptionsItemSelected(menuItem2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean dispatchContextItemSelected(MenuItem menuItem) {
        MenuItem menuItem2 = menuItem;
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                Fragment fragment = this.mAdded.get(i);
                if (fragment != null && fragment.performContextItemSelected(menuItem2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void dispatchOptionsMenuClosed(Menu menu) {
        Menu menu2 = menu;
        if (this.mAdded != null) {
            for (int i = 0; i < this.mAdded.size(); i++) {
                Fragment fragment = this.mAdded.get(i);
                if (fragment != null) {
                    fragment.performOptionsMenuClosed(menu2);
                }
            }
        }
    }

    public static int reverseTransit(int i) {
        int i2 = 0;
        switch (i) {
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN:
                i2 = 8194;
                break;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE:
                i2 = 4099;
                break;
            case 8194:
                i2 = 4097;
                break;
        }
        return i2;
    }

    public static int transitToStyleIndex(int i, boolean z) {
        boolean z2 = z;
        int i2 = -1;
        switch (i) {
            case FragmentTransaction.TRANSIT_FRAGMENT_OPEN:
                i2 = z2 ? 1 : 2;
                break;
            case FragmentTransaction.TRANSIT_FRAGMENT_FADE:
                i2 = z2 ? 5 : 6;
                break;
            case 8194:
                i2 = z2 ? 3 : 4;
                break;
        }
        return i2;
    }

    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        Throwable th;
        StringBuilder sb;
        Throwable th2;
        StringBuilder sb2;
        StringBuilder sb3;
        Throwable th3;
        StringBuilder sb4;
        View view2 = view;
        Context context2 = context;
        AttributeSet attributeSet2 = attributeSet;
        if (!"fragment".equals(str)) {
            return null;
        }
        String attributeValue = attributeSet2.getAttributeValue(null, "class");
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet2, FragmentTag.Fragment);
        if (attributeValue == null) {
            attributeValue = obtainStyledAttributes.getString(0);
        }
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (!Fragment.isSupportFragmentClass(this.mHost.getContext(), attributeValue)) {
            return null;
        }
        int id = view2 != null ? view2.getId() : 0;
        if (id == -1 && resourceId == -1 && string == null) {
            Throwable th4 = th3;
            new StringBuilder();
            new IllegalArgumentException(sb4.append(attributeSet2.getPositionDescription()).append(": Must specify unique android:id, android:tag, or have a parent with an id for ").append(attributeValue).toString());
            throw th4;
        }
        Fragment findFragmentById = resourceId != -1 ? findFragmentById(resourceId) : null;
        if (findFragmentById == null && string != null) {
            findFragmentById = findFragmentByTag(string);
        }
        if (findFragmentById == null && id != -1) {
            findFragmentById = findFragmentById(id);
        }
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb3.append("onCreateView: id=0x").append(Integer.toHexString(resourceId)).append(" fname=").append(attributeValue).append(" existing=").append(findFragmentById).toString());
        }
        if (findFragmentById == null) {
            findFragmentById = Fragment.instantiate(context2, attributeValue);
            findFragmentById.mFromLayout = true;
            findFragmentById.mFragmentId = resourceId != 0 ? resourceId : id;
            findFragmentById.mContainerId = id;
            findFragmentById.mTag = string;
            findFragmentById.mInLayout = true;
            findFragmentById.mFragmentManager = this;
            findFragmentById.mHost = this.mHost;
            findFragmentById.onInflate(this.mHost.getContext(), attributeSet2, findFragmentById.mSavedFragmentState);
            addFragment(findFragmentById, true);
        } else if (findFragmentById.mInLayout) {
            Throwable th5 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append(attributeSet2.getPositionDescription()).append(": Duplicate id 0x").append(Integer.toHexString(resourceId)).append(", tag ").append(string).append(", or parent id 0x").append(Integer.toHexString(id)).append(" with another fragment for ").append(attributeValue).toString());
            throw th5;
        } else {
            findFragmentById.mInLayout = true;
            if (!findFragmentById.mRetaining) {
                findFragmentById.onInflate(this.mHost.getContext(), attributeSet2, findFragmentById.mSavedFragmentState);
            }
        }
        if (this.mCurState >= 1 || !findFragmentById.mFromLayout) {
            moveToState(findFragmentById);
        } else {
            moveToState(findFragmentById, 1, 0, 0, false);
        }
        if (findFragmentById.mView == null) {
            Throwable th6 = th2;
            new StringBuilder();
            new IllegalStateException(sb2.append("Fragment ").append(attributeValue).append(" did not create a view.").toString());
            throw th6;
        }
        if (resourceId != 0) {
            findFragmentById.mView.setId(resourceId);
        }
        if (findFragmentById.mView.getTag() == null) {
            findFragmentById.mView.setTag(string);
        }
        return findFragmentById.mView;
    }

    /* access modifiers changed from: package-private */
    public LayoutInflaterFactory getLayoutInflaterFactory() {
        return this;
    }

    /* compiled from: FragmentManager */
    static class FragmentTag {
        public static final int[] Fragment = {16842755, 16842960, 16842961};
        public static final int Fragment_id = 1;
        public static final int Fragment_name = 0;
        public static final int Fragment_tag = 2;

        FragmentTag() {
        }
    }
}
