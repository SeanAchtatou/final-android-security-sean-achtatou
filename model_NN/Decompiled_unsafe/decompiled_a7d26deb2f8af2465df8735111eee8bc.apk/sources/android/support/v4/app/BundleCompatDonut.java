package android.support.v4.app;

import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class BundleCompatDonut {
    private static final String TAG = "BundleCompatDonut";
    private static Method sGetIBinderMethod;
    private static boolean sGetIBinderMethodFetched;
    private static Method sPutIBinderMethod;
    private static boolean sPutIBinderMethodFetched;

    BundleCompatDonut() {
    }

    public static IBinder getBinder(Bundle bundle, String str) {
        Bundle bundle2 = bundle;
        String str2 = str;
        if (!sGetIBinderMethodFetched) {
            try {
                sGetIBinderMethod = Bundle.class.getMethod("getIBinder", String.class);
                sGetIBinderMethod.setAccessible(true);
            } catch (NoSuchMethodException e) {
                int i = Log.i(TAG, "Failed to retrieve getIBinder method", e);
            }
            sGetIBinderMethodFetched = true;
        }
        if (sGetIBinderMethod != null) {
            try {
                return (IBinder) sGetIBinderMethod.invoke(bundle2, str2);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                int i2 = Log.i(TAG, "Failed to invoke getIBinder via reflection", e2);
                sGetIBinderMethod = null;
            }
        }
        return null;
    }

    public static void putBinder(Bundle bundle, String str, IBinder iBinder) {
        Bundle bundle2 = bundle;
        String str2 = str;
        IBinder iBinder2 = iBinder;
        if (!sPutIBinderMethodFetched) {
            Class<Bundle> cls = Bundle.class;
            try {
                Class[] clsArr = new Class[2];
                clsArr[0] = String.class;
                Class[] clsArr2 = clsArr;
                clsArr2[1] = IBinder.class;
                sPutIBinderMethod = cls.getMethod("putIBinder", clsArr2);
                sPutIBinderMethod.setAccessible(true);
            } catch (NoSuchMethodException e) {
                int i = Log.i(TAG, "Failed to retrieve putIBinder method", e);
            }
            sPutIBinderMethodFetched = true;
        }
        if (sPutIBinderMethod != null) {
            try {
                Object[] objArr = new Object[2];
                objArr[0] = str2;
                Object[] objArr2 = objArr;
                objArr2[1] = iBinder2;
                Object invoke = sPutIBinderMethod.invoke(bundle2, objArr2);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                int i2 = Log.i(TAG, "Failed to invoke putIBinder via reflection", e2);
                sPutIBinderMethod = null;
            }
        }
    }
}
