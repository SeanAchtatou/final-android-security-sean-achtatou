package android.support.v4.app;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.RemoteInputCompatBase;

class RemoteInputCompatJellybean {
    public static final String EXTRA_RESULTS_DATA = "android.remoteinput.resultsData";
    private static final String KEY_ALLOW_FREE_FORM_INPUT = "allowFreeFormInput";
    private static final String KEY_CHOICES = "choices";
    private static final String KEY_EXTRAS = "extras";
    private static final String KEY_LABEL = "label";
    private static final String KEY_RESULT_KEY = "resultKey";
    public static final String RESULTS_CLIP_LABEL = "android.remoteinput.results";

    RemoteInputCompatJellybean() {
    }

    static RemoteInputCompatBase.RemoteInput fromBundle(Bundle bundle, RemoteInputCompatBase.RemoteInput.Factory factory) {
        Bundle bundle2 = bundle;
        return factory.build(bundle2.getString(KEY_RESULT_KEY), bundle2.getCharSequence(KEY_LABEL), bundle2.getCharSequenceArray(KEY_CHOICES), bundle2.getBoolean(KEY_ALLOW_FREE_FORM_INPUT), bundle2.getBundle(KEY_EXTRAS));
    }

    static Bundle toBundle(RemoteInputCompatBase.RemoteInput remoteInput) {
        Bundle bundle;
        RemoteInputCompatBase.RemoteInput remoteInput2 = remoteInput;
        new Bundle();
        Bundle bundle2 = bundle;
        bundle2.putString(KEY_RESULT_KEY, remoteInput2.getResultKey());
        bundle2.putCharSequence(KEY_LABEL, remoteInput2.getLabel());
        bundle2.putCharSequenceArray(KEY_CHOICES, remoteInput2.getChoices());
        bundle2.putBoolean(KEY_ALLOW_FREE_FORM_INPUT, remoteInput2.getAllowFreeFormInput());
        bundle2.putBundle(KEY_EXTRAS, remoteInput2.getExtras());
        return bundle2;
    }

    static RemoteInputCompatBase.RemoteInput[] fromBundleArray(Bundle[] bundleArr, RemoteInputCompatBase.RemoteInput.Factory factory) {
        Bundle[] bundleArr2 = bundleArr;
        RemoteInputCompatBase.RemoteInput.Factory factory2 = factory;
        if (bundleArr2 == null) {
            return null;
        }
        RemoteInputCompatBase.RemoteInput[] newArray = factory2.newArray(bundleArr2.length);
        for (int i = 0; i < bundleArr2.length; i++) {
            newArray[i] = fromBundle(bundleArr2[i], factory2);
        }
        return newArray;
    }

    static Bundle[] toBundleArray(RemoteInputCompatBase.RemoteInput[] remoteInputArr) {
        RemoteInputCompatBase.RemoteInput[] remoteInputArr2 = remoteInputArr;
        if (remoteInputArr2 == null) {
            return null;
        }
        Bundle[] bundleArr = new Bundle[remoteInputArr2.length];
        for (int i = 0; i < remoteInputArr2.length; i++) {
            bundleArr[i] = toBundle(remoteInputArr2[i]);
        }
        return bundleArr;
    }

    static Bundle getResultsFromIntent(Intent intent) {
        ClipData clipData = intent.getClipData();
        if (clipData == null) {
            return null;
        }
        ClipDescription description = clipData.getDescription();
        if (!description.hasMimeType("text/vnd.android.intent")) {
            return null;
        }
        if (description.getLabel().equals("android.remoteinput.results")) {
            return (Bundle) clipData.getItemAt(0).getIntent().getExtras().getParcelable("android.remoteinput.resultsData");
        }
        return null;
    }

    static void addResultsToIntent(RemoteInputCompatBase.RemoteInput[] remoteInputArr, Intent intent, Bundle bundle) {
        Bundle bundle2;
        Intent intent2;
        Intent intent3 = intent;
        Bundle bundle3 = bundle;
        new Bundle();
        Bundle bundle4 = bundle2;
        RemoteInputCompatBase.RemoteInput[] remoteInputArr2 = remoteInputArr;
        int length = remoteInputArr2.length;
        for (int i = 0; i < length; i++) {
            RemoteInputCompatBase.RemoteInput remoteInput = remoteInputArr2[i];
            Object obj = bundle3.get(remoteInput.getResultKey());
            if (obj instanceof CharSequence) {
                bundle4.putCharSequence(remoteInput.getResultKey(), (CharSequence) obj);
            }
        }
        new Intent();
        Intent intent4 = intent2;
        Intent putExtra = intent4.putExtra("android.remoteinput.resultsData", bundle4);
        intent3.setClipData(ClipData.newIntent("android.remoteinput.results", intent4));
    }
}
