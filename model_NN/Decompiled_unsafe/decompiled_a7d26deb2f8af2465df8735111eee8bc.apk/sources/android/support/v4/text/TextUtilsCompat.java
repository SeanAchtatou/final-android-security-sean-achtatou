package android.support.v4.text;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Locale;

public class TextUtilsCompat {
    /* access modifiers changed from: private */
    public static String ARAB_SCRIPT_SUBTAG = "Arab";
    /* access modifiers changed from: private */
    public static String HEBR_SCRIPT_SUBTAG = "Hebr";
    private static final TextUtilsCompatImpl IMPL;
    public static final Locale ROOT;

    private static class TextUtilsCompatImpl {
        private TextUtilsCompatImpl() {
        }

        @NonNull
        public String htmlEncode(@NonNull String str) {
            StringBuilder sb;
            String str2 = str;
            new StringBuilder();
            StringBuilder sb2 = sb;
            for (int i = 0; i < str2.length(); i++) {
                char charAt = str2.charAt(i);
                switch (charAt) {
                    case '\"':
                        StringBuilder append = sb2.append("&quot;");
                        break;
                    case '&':
                        StringBuilder append2 = sb2.append("&amp;");
                        break;
                    case '\'':
                        StringBuilder append3 = sb2.append("&#39;");
                        break;
                    case '<':
                        StringBuilder append4 = sb2.append("&lt;");
                        break;
                    case '>':
                        StringBuilder append5 = sb2.append("&gt;");
                        break;
                    default:
                        StringBuilder append6 = sb2.append(charAt);
                        break;
                }
            }
            return sb2.toString();
        }

        public int getLayoutDirectionFromLocale(@Nullable Locale locale) {
            Locale locale2 = locale;
            if (locale2 != null && !locale2.equals(TextUtilsCompat.ROOT)) {
                String maximizeAndGetScript = ICUCompat.maximizeAndGetScript(locale2);
                if (maximizeAndGetScript == null) {
                    return getLayoutDirectionFromFirstChar(locale2);
                }
                if (maximizeAndGetScript.equalsIgnoreCase(TextUtilsCompat.ARAB_SCRIPT_SUBTAG) || maximizeAndGetScript.equalsIgnoreCase(TextUtilsCompat.HEBR_SCRIPT_SUBTAG)) {
                    return 1;
                }
            }
            return 0;
        }

        private static int getLayoutDirectionFromFirstChar(@NonNull Locale locale) {
            Locale locale2 = locale;
            switch (Character.getDirectionality(locale2.getDisplayName(locale2).charAt(0))) {
                case 1:
                case 2:
                    return 1;
                default:
                    return 0;
            }
        }
    }

    private static class TextUtilsCompatJellybeanMr1Impl extends TextUtilsCompatImpl {
        private TextUtilsCompatJellybeanMr1Impl() {
            super();
        }

        @NonNull
        public String htmlEncode(@NonNull String str) {
            return TextUtilsCompatJellybeanMr1.htmlEncode(str);
        }

        public int getLayoutDirectionFromLocale(@Nullable Locale locale) {
            return TextUtilsCompatJellybeanMr1.getLayoutDirectionFromLocale(locale);
        }
    }

    static {
        TextUtilsCompatImpl textUtilsCompatImpl;
        Locale locale;
        TextUtilsCompatImpl textUtilsCompatImpl2;
        if (Build.VERSION.SDK_INT >= 17) {
            new TextUtilsCompatJellybeanMr1Impl();
            IMPL = textUtilsCompatImpl2;
        } else {
            new TextUtilsCompatImpl();
            IMPL = textUtilsCompatImpl;
        }
        new Locale("", "");
        ROOT = locale;
    }

    @NonNull
    public static String htmlEncode(@NonNull String str) {
        return IMPL.htmlEncode(str);
    }

    public static int getLayoutDirectionFromLocale(@Nullable Locale locale) {
        return IMPL.getLayoutDirectionFromLocale(locale);
    }
}
