package android.support.v4.util;

public class SimpleArrayMap<K, V> {
    private static final int BASE_SIZE = 4;
    private static final int CACHE_SIZE = 10;
    private static final boolean DEBUG = false;
    private static final String TAG = "ArrayMap";
    static Object[] mBaseCache;
    static int mBaseCacheSize;
    static Object[] mTwiceBaseCache;
    static int mTwiceBaseCacheSize;
    Object[] mArray;
    int[] mHashes;
    int mSize;

    /* access modifiers changed from: package-private */
    public int indexOf(Object obj, int i) {
        Object obj2 = obj;
        int i2 = i;
        int i3 = this.mSize;
        if (i3 == 0) {
            return -1;
        }
        int binarySearch = ContainerHelpers.binarySearch(this.mHashes, i3, i2);
        if (binarySearch < 0) {
            return binarySearch;
        }
        if (obj2.equals(this.mArray[binarySearch << 1])) {
            return binarySearch;
        }
        int i4 = binarySearch + 1;
        while (i4 < i3 && this.mHashes[i4] == i2) {
            if (obj2.equals(this.mArray[i4 << 1])) {
                return i4;
            }
            i4++;
        }
        int i5 = binarySearch - 1;
        while (i5 >= 0 && this.mHashes[i5] == i2) {
            if (obj2.equals(this.mArray[i5 << 1])) {
                return i5;
            }
            i5--;
        }
        return i4 ^ -1;
    }

    /* access modifiers changed from: package-private */
    public int indexOfNull() {
        int i = this.mSize;
        if (i == 0) {
            return -1;
        }
        int binarySearch = ContainerHelpers.binarySearch(this.mHashes, i, 0);
        if (binarySearch < 0) {
            return binarySearch;
        }
        if (null == this.mArray[binarySearch << 1]) {
            return binarySearch;
        }
        int i2 = binarySearch + 1;
        while (i2 < i && this.mHashes[i2] == 0) {
            if (null == this.mArray[i2 << 1]) {
                return i2;
            }
            i2++;
        }
        int i3 = binarySearch - 1;
        while (i3 >= 0 && this.mHashes[i3] == 0) {
            if (null == this.mArray[i3 << 1]) {
                return i3;
            }
            i3--;
        }
        return i2 ^ -1;
    }

    /* JADX INFO: finally extract failed */
    private void allocArrays(int i) {
        int i2 = i;
        if (i2 == 8) {
            Class<ArrayMap> cls = ArrayMap.class;
            Class<ArrayMap> cls2 = cls;
            synchronized (cls) {
                try {
                    if (mTwiceBaseCache != null) {
                        Object[] objArr = mTwiceBaseCache;
                        this.mArray = objArr;
                        mTwiceBaseCache = (Object[]) objArr[0];
                        this.mHashes = (int[]) objArr[1];
                        objArr[1] = null;
                        objArr[0] = null;
                        mTwiceBaseCacheSize--;
                        return;
                    }
                } catch (Throwable th) {
                    while (true) {
                        throw th;
                    }
                }
            }
        } else if (i2 == 4) {
            Class<ArrayMap> cls3 = ArrayMap.class;
            Class<ArrayMap> cls4 = cls3;
            synchronized (cls3) {
                try {
                    if (mBaseCache != null) {
                        Object[] objArr2 = mBaseCache;
                        this.mArray = objArr2;
                        mBaseCache = (Object[]) objArr2[0];
                        this.mHashes = (int[]) objArr2[1];
                        objArr2[1] = null;
                        objArr2[0] = null;
                        mBaseCacheSize--;
                        return;
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    Class<ArrayMap> cls5 = cls4;
                    throw th3;
                }
            }
        }
        this.mHashes = new int[i2];
        this.mArray = new Object[(i2 << 1)];
    }

    /* JADX INFO: finally extract failed */
    private static void freeArrays(int[] iArr, Object[] objArr, int i) {
        int[] iArr2 = iArr;
        Object[] objArr2 = objArr;
        int i2 = i;
        if (iArr2.length == 8) {
            Class<ArrayMap> cls = ArrayMap.class;
            Class<ArrayMap> cls2 = cls;
            synchronized (cls) {
                try {
                    if (mTwiceBaseCacheSize < 10) {
                        objArr2[0] = mTwiceBaseCache;
                        objArr2[1] = iArr2;
                        for (int i3 = (i2 << 1) - 1; i3 >= 2; i3--) {
                            objArr2[i3] = null;
                        }
                        mTwiceBaseCache = objArr2;
                        mTwiceBaseCacheSize++;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    Class<ArrayMap> cls3 = cls2;
                    throw th2;
                }
            }
        } else if (iArr2.length == 4) {
            Class<ArrayMap> cls4 = ArrayMap.class;
            Class<ArrayMap> cls5 = cls4;
            synchronized (cls4) {
                try {
                    if (mBaseCacheSize < 10) {
                        objArr2[0] = mBaseCache;
                        objArr2[1] = iArr2;
                        for (int i4 = (i2 << 1) - 1; i4 >= 2; i4--) {
                            objArr2[i4] = null;
                        }
                        mBaseCache = objArr2;
                        mBaseCacheSize++;
                    }
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    Class<ArrayMap> cls6 = cls5;
                    throw th4;
                }
            }
        }
    }

    public SimpleArrayMap() {
        this.mHashes = ContainerHelpers.EMPTY_INTS;
        this.mArray = ContainerHelpers.EMPTY_OBJECTS;
        this.mSize = 0;
    }

    public SimpleArrayMap(int i) {
        int i2 = i;
        if (i2 == 0) {
            this.mHashes = ContainerHelpers.EMPTY_INTS;
            this.mArray = ContainerHelpers.EMPTY_OBJECTS;
        } else {
            allocArrays(i2);
        }
        this.mSize = 0;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SimpleArrayMap(SimpleArrayMap simpleArrayMap) {
        this();
        SimpleArrayMap simpleArrayMap2 = simpleArrayMap;
        if (simpleArrayMap2 != null) {
            putAll(simpleArrayMap2);
        }
    }

    public void clear() {
        if (this.mSize != 0) {
            freeArrays(this.mHashes, this.mArray, this.mSize);
            this.mHashes = ContainerHelpers.EMPTY_INTS;
            this.mArray = ContainerHelpers.EMPTY_OBJECTS;
            this.mSize = 0;
        }
    }

    public void ensureCapacity(int i) {
        int i2 = i;
        if (this.mHashes.length < i2) {
            int[] iArr = this.mHashes;
            Object[] objArr = this.mArray;
            allocArrays(i2);
            if (this.mSize > 0) {
                System.arraycopy(iArr, 0, this.mHashes, 0, this.mSize);
                System.arraycopy(objArr, 0, this.mArray, 0, this.mSize << 1);
            }
            freeArrays(iArr, objArr, this.mSize);
        }
    }

    public boolean containsKey(Object obj) {
        return indexOfKey(obj) >= 0;
    }

    public int indexOfKey(Object obj) {
        Object obj2 = obj;
        return obj2 == null ? indexOfNull() : indexOf(obj2, obj2.hashCode());
    }

    /* access modifiers changed from: package-private */
    public int indexOfValue(Object obj) {
        Object obj2 = obj;
        int i = this.mSize * 2;
        Object[] objArr = this.mArray;
        if (obj2 == null) {
            for (int i2 = 1; i2 < i; i2 += 2) {
                if (objArr[i2] == null) {
                    return i2 >> 1;
                }
            }
        } else {
            for (int i3 = 1; i3 < i; i3 += 2) {
                if (obj2.equals(objArr[i3])) {
                    return i3 >> 1;
                }
            }
        }
        return -1;
    }

    public boolean containsValue(Object obj) {
        return indexOfValue(obj) >= 0;
    }

    public V get(Object obj) {
        int indexOfKey = indexOfKey(obj);
        return indexOfKey >= 0 ? this.mArray[(indexOfKey << 1) + 1] : null;
    }

    public K keyAt(int i) {
        return this.mArray[i << 1];
    }

    public V valueAt(int i) {
        return this.mArray[(i << 1) + 1];
    }

    public V setValueAt(int i, V v) {
        int i2 = (i << 1) + 1;
        V v2 = this.mArray[i2];
        this.mArray[i2] = v;
        return v2;
    }

    public boolean isEmpty() {
        return this.mSize <= 0;
    }

    public V put(K k, V v) {
        int hashCode;
        int indexOf;
        K k2 = k;
        V v2 = v;
        if (k2 == null) {
            hashCode = 0;
            indexOf = indexOfNull();
        } else {
            hashCode = k2.hashCode();
            indexOf = indexOf(k2, hashCode);
        }
        if (indexOf >= 0) {
            int i = (indexOf << 1) + 1;
            V v3 = this.mArray[i];
            this.mArray[i] = v2;
            return v3;
        }
        int i2 = indexOf ^ -1;
        if (this.mSize >= this.mHashes.length) {
            int i3 = this.mSize >= 8 ? this.mSize + (this.mSize >> 1) : this.mSize >= 4 ? 8 : 4;
            int[] iArr = this.mHashes;
            Object[] objArr = this.mArray;
            allocArrays(i3);
            if (this.mHashes.length > 0) {
                System.arraycopy(iArr, 0, this.mHashes, 0, iArr.length);
                System.arraycopy(objArr, 0, this.mArray, 0, objArr.length);
            }
            freeArrays(iArr, objArr, this.mSize);
        }
        if (i2 < this.mSize) {
            System.arraycopy(this.mHashes, i2, this.mHashes, i2 + 1, this.mSize - i2);
            System.arraycopy(this.mArray, i2 << 1, this.mArray, (i2 + 1) << 1, (this.mSize - i2) << 1);
        }
        this.mHashes[i2] = hashCode;
        this.mArray[i2 << 1] = k2;
        this.mArray[(i2 << 1) + 1] = v2;
        this.mSize = this.mSize + 1;
        return null;
    }

    public void putAll(SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        SimpleArrayMap<? extends K, ? extends V> simpleArrayMap2 = simpleArrayMap;
        int i = simpleArrayMap2.mSize;
        ensureCapacity(this.mSize + i);
        if (this.mSize != 0) {
            for (int i2 = 0; i2 < i; i2++) {
                Object put = put(simpleArrayMap2.keyAt(i2), simpleArrayMap2.valueAt(i2));
            }
        } else if (i > 0) {
            System.arraycopy(simpleArrayMap2.mHashes, 0, this.mHashes, 0, i);
            System.arraycopy(simpleArrayMap2.mArray, 0, this.mArray, 0, i << 1);
            this.mSize = i;
        }
    }

    public V remove(Object obj) {
        int indexOfKey = indexOfKey(obj);
        if (indexOfKey >= 0) {
            return removeAt(indexOfKey);
        }
        return null;
    }

    public V removeAt(int i) {
        int i2 = i;
        V v = this.mArray[(i2 << 1) + 1];
        if (this.mSize <= 1) {
            freeArrays(this.mHashes, this.mArray, this.mSize);
            this.mHashes = ContainerHelpers.EMPTY_INTS;
            this.mArray = ContainerHelpers.EMPTY_OBJECTS;
            this.mSize = 0;
        } else if (this.mHashes.length <= 8 || this.mSize >= this.mHashes.length / 3) {
            this.mSize = this.mSize - 1;
            if (i2 < this.mSize) {
                System.arraycopy(this.mHashes, i2 + 1, this.mHashes, i2, this.mSize - i2);
                System.arraycopy(this.mArray, (i2 + 1) << 1, this.mArray, i2 << 1, (this.mSize - i2) << 1);
            }
            this.mArray[this.mSize << 1] = null;
            this.mArray[(this.mSize << 1) + 1] = null;
        } else {
            int i3 = this.mSize > 8 ? this.mSize + (this.mSize >> 1) : 8;
            int[] iArr = this.mHashes;
            Object[] objArr = this.mArray;
            allocArrays(i3);
            this.mSize = this.mSize - 1;
            if (i2 > 0) {
                System.arraycopy(iArr, 0, this.mHashes, 0, i2);
                System.arraycopy(objArr, 0, this.mArray, 0, i2 << 1);
            }
            if (i2 < this.mSize) {
                System.arraycopy(iArr, i2 + 1, this.mHashes, i2, this.mSize - i2);
                System.arraycopy(objArr, (i2 + 1) << 1, this.mArray, i2 << 1, (this.mSize - i2) << 1);
            }
        }
        return v;
    }

    public int size() {
        return this.mSize;
    }

    /* JADX WARN: Type inference failed for: r10v0, types: [java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r10) {
        /*
            r9 = this;
            r0 = r9
            r1 = r10
            r7 = r0
            r8 = r1
            if (r7 != r8) goto L_0x0009
            r7 = 1
            r0 = r7
        L_0x0008:
            return r0
        L_0x0009:
            r7 = r1
            boolean r7 = r7 instanceof java.util.Map
            if (r7 == 0) goto L_0x006a
            r7 = r1
            java.util.Map r7 = (java.util.Map) r7
            r2 = r7
            r7 = r0
            int r7 = r7.size()
            r8 = r2
            int r8 = r8.size()
            if (r7 == r8) goto L_0x0021
            r7 = 0
            r0 = r7
            goto L_0x0008
        L_0x0021:
            r7 = 0
            r3 = r7
        L_0x0023:
            r7 = r3
            r8 = r0
            int r8 = r8.mSize     // Catch:{ NullPointerException -> 0x0060, ClassCastException -> 0x0065 }
            if (r7 >= r8) goto L_0x005d
            r7 = r0
            r8 = r3
            java.lang.Object r7 = r7.keyAt(r8)     // Catch:{ NullPointerException -> 0x0060, ClassCastException -> 0x0065 }
            r4 = r7
            r7 = r0
            r8 = r3
            java.lang.Object r7 = r7.valueAt(r8)     // Catch:{ NullPointerException -> 0x0060, ClassCastException -> 0x0065 }
            r5 = r7
            r7 = r2
            r8 = r4
            java.lang.Object r7 = r7.get(r8)     // Catch:{ NullPointerException -> 0x0060, ClassCastException -> 0x0065 }
            r6 = r7
            r7 = r5
            if (r7 != 0) goto L_0x004f
            r7 = r6
            if (r7 != 0) goto L_0x004c
            r7 = r2
            r8 = r4
            boolean r7 = r7.containsKey(r8)     // Catch:{ NullPointerException -> 0x0060, ClassCastException -> 0x0065 }
            if (r7 != 0) goto L_0x005a
        L_0x004c:
            r7 = 0
            r0 = r7
            goto L_0x0008
        L_0x004f:
            r7 = r5
            r8 = r6
            boolean r7 = r7.equals(r8)     // Catch:{ NullPointerException -> 0x0060, ClassCastException -> 0x0065 }
            if (r7 != 0) goto L_0x005a
            r7 = 0
            r0 = r7
            goto L_0x0008
        L_0x005a:
            int r3 = r3 + 1
            goto L_0x0023
        L_0x005d:
            r7 = 1
            r0 = r7
            goto L_0x0008
        L_0x0060:
            r7 = move-exception
            r3 = r7
            r7 = 0
            r0 = r7
            goto L_0x0008
        L_0x0065:
            r7 = move-exception
            r3 = r7
            r7 = 0
            r0 = r7
            goto L_0x0008
        L_0x006a:
            r7 = 0
            r0 = r7
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.util.SimpleArrayMap.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int[] iArr = this.mHashes;
        Object[] objArr = this.mArray;
        int i = 0;
        int i2 = 0;
        int i3 = 1;
        int i4 = this.mSize;
        while (i2 < i4) {
            Object obj = objArr[i3];
            i += iArr[i2] ^ (obj == null ? 0 : obj.hashCode());
            i2++;
            i3 += 2;
        }
        return i;
    }

    public String toString() {
        StringBuilder sb;
        if (isEmpty()) {
            return "{}";
        }
        new StringBuilder(this.mSize * 28);
        StringBuilder sb2 = sb;
        StringBuilder append = sb2.append('{');
        for (int i = 0; i < this.mSize; i++) {
            if (i > 0) {
                StringBuilder append2 = sb2.append(", ");
            }
            Object keyAt = keyAt(i);
            if (keyAt != this) {
                StringBuilder append3 = sb2.append(keyAt);
            } else {
                StringBuilder append4 = sb2.append("(this Map)");
            }
            StringBuilder append5 = sb2.append('=');
            Object valueAt = valueAt(i);
            if (valueAt != this) {
                StringBuilder append6 = sb2.append(valueAt);
            } else {
                StringBuilder append7 = sb2.append("(this Map)");
            }
        }
        StringBuilder append8 = sb2.append('}');
        return sb2.toString();
    }
}
