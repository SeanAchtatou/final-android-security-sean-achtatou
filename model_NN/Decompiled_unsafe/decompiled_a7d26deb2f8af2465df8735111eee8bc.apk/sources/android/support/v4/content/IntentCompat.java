package android.support.v4.content;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;

public class IntentCompat {
    public static final String ACTION_EXTERNAL_APPLICATIONS_AVAILABLE = "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE";
    public static final String ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE = "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE";
    public static final String EXTRA_CHANGED_PACKAGE_LIST = "android.intent.extra.changed_package_list";
    public static final String EXTRA_CHANGED_UID_LIST = "android.intent.extra.changed_uid_list";
    public static final String EXTRA_HTML_TEXT = "android.intent.extra.HTML_TEXT";
    public static final int FLAG_ACTIVITY_CLEAR_TASK = 32768;
    public static final int FLAG_ACTIVITY_TASK_ON_HOME = 16384;
    private static final IntentCompatImpl IMPL;

    interface IntentCompatImpl {
        Intent makeMainActivity(ComponentName componentName);

        Intent makeMainSelectorActivity(String str, String str2);

        Intent makeRestartActivityTask(ComponentName componentName);
    }

    static class IntentCompatImplBase implements IntentCompatImpl {
        IntentCompatImplBase() {
        }

        public Intent makeMainActivity(ComponentName componentName) {
            Intent intent;
            new Intent("android.intent.action.MAIN");
            Intent intent2 = intent;
            Intent component = intent2.setComponent(componentName);
            Intent addCategory = intent2.addCategory("android.intent.category.LAUNCHER");
            return intent2;
        }

        public Intent makeMainSelectorActivity(String str, String str2) {
            Intent intent;
            new Intent(str);
            Intent intent2 = intent;
            Intent addCategory = intent2.addCategory(str2);
            return intent2;
        }

        public Intent makeRestartActivityTask(ComponentName componentName) {
            Intent makeMainActivity = makeMainActivity(componentName);
            Intent addFlags = makeMainActivity.addFlags(268468224);
            return makeMainActivity;
        }
    }

    static class IntentCompatImplHC extends IntentCompatImplBase {
        IntentCompatImplHC() {
        }

        public Intent makeMainActivity(ComponentName componentName) {
            return IntentCompatHoneycomb.makeMainActivity(componentName);
        }

        public Intent makeRestartActivityTask(ComponentName componentName) {
            return IntentCompatHoneycomb.makeRestartActivityTask(componentName);
        }
    }

    static class IntentCompatImplIcsMr1 extends IntentCompatImplHC {
        IntentCompatImplIcsMr1() {
        }

        public Intent makeMainSelectorActivity(String str, String str2) {
            return IntentCompatIcsMr1.makeMainSelectorActivity(str, str2);
        }
    }

    static {
        IntentCompatImpl intentCompatImpl;
        IntentCompatImpl intentCompatImpl2;
        IntentCompatImpl intentCompatImpl3;
        int i = Build.VERSION.SDK_INT;
        if (i >= 15) {
            new IntentCompatImplIcsMr1();
            IMPL = intentCompatImpl3;
        } else if (i >= 11) {
            new IntentCompatImplHC();
            IMPL = intentCompatImpl2;
        } else {
            new IntentCompatImplBase();
            IMPL = intentCompatImpl;
        }
    }

    private IntentCompat() {
    }

    public static Intent makeMainActivity(ComponentName componentName) {
        return IMPL.makeMainActivity(componentName);
    }

    public static Intent makeMainSelectorActivity(String str, String str2) {
        return IMPL.makeMainSelectorActivity(str, str2);
    }

    public static Intent makeRestartActivityTask(ComponentName componentName) {
        return IMPL.makeRestartActivityTask(componentName);
    }
}
