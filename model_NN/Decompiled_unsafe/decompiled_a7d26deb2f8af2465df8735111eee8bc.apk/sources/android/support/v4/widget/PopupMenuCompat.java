package android.support.v4.widget;

import android.os.Build;
import android.view.View;

public class PopupMenuCompat {
    static final PopupMenuImpl IMPL;

    interface PopupMenuImpl {
        View.OnTouchListener getDragToOpenListener(Object obj);
    }

    static class BasePopupMenuImpl implements PopupMenuImpl {
        BasePopupMenuImpl() {
        }

        public View.OnTouchListener getDragToOpenListener(Object obj) {
            return null;
        }
    }

    static class KitKatPopupMenuImpl extends BasePopupMenuImpl {
        KitKatPopupMenuImpl() {
        }

        public View.OnTouchListener getDragToOpenListener(Object obj) {
            return PopupMenuCompatKitKat.getDragToOpenListener(obj);
        }
    }

    static {
        PopupMenuImpl popupMenuImpl;
        PopupMenuImpl popupMenuImpl2;
        if (Build.VERSION.SDK_INT >= 19) {
            new KitKatPopupMenuImpl();
            IMPL = popupMenuImpl2;
            return;
        }
        new BasePopupMenuImpl();
        IMPL = popupMenuImpl;
    }

    private PopupMenuCompat() {
    }

    public static View.OnTouchListener getDragToOpenListener(Object obj) {
        return IMPL.getDragToOpenListener(obj);
    }
}
