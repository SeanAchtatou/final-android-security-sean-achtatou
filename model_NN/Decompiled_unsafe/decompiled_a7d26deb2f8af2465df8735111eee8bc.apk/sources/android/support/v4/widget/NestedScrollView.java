package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.NestedScrollingChild;
import android.support.v4.view.NestedScrollingChildHelper;
import android.support.v4.view.NestedScrollingParent;
import android.support.v4.view.NestedScrollingParentHelper;
import android.support.v4.view.ScrollingView;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityRecordCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import java.util.ArrayList;

public class NestedScrollView extends FrameLayout implements NestedScrollingParent, NestedScrollingChild, ScrollingView {
    private static final AccessibilityDelegate ACCESSIBILITY_DELEGATE;
    static final int ANIMATED_SCROLL_GAP = 250;
    private static final int INVALID_POINTER = -1;
    static final float MAX_SCROLL_FACTOR = 0.5f;
    private static final int[] SCROLLVIEW_STYLEABLE = {16843130};
    private static final String TAG = "NestedScrollView";
    private int mActivePointerId;
    private final NestedScrollingChildHelper mChildHelper;
    private View mChildToScrollTo;
    private EdgeEffectCompat mEdgeGlowBottom;
    private EdgeEffectCompat mEdgeGlowTop;
    private boolean mFillViewport;
    private boolean mIsBeingDragged;
    private boolean mIsLaidOut;
    private boolean mIsLayoutDirty;
    private int mLastMotionY;
    private long mLastScroll;
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    private int mNestedYOffset;
    private OnScrollChangeListener mOnScrollChangeListener;
    private final NestedScrollingParentHelper mParentHelper;
    private SavedState mSavedState;
    private final int[] mScrollConsumed;
    private final int[] mScrollOffset;
    private ScrollerCompat mScroller;
    private boolean mSmoothScrollingEnabled;
    private final Rect mTempRect;
    private int mTouchSlop;
    private VelocityTracker mVelocityTracker;
    private float mVerticalScrollFactor;

    public interface OnScrollChangeListener {
        void onScrollChange(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4);
    }

    static {
        AccessibilityDelegate accessibilityDelegate;
        new AccessibilityDelegate();
        ACCESSIBILITY_DELEGATE = accessibilityDelegate;
    }

    public NestedScrollView(Context context) {
        this(context, null);
    }

    public NestedScrollView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public NestedScrollView(android.content.Context r12, android.util.AttributeSet r13, int r14) {
        /*
            r11 = this;
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r14
            r5 = r0
            r6 = r1
            r7 = r2
            r8 = r3
            r5.<init>(r6, r7, r8)
            r5 = r0
            android.graphics.Rect r6 = new android.graphics.Rect
            r10 = r6
            r6 = r10
            r7 = r10
            r7.<init>()
            r5.mTempRect = r6
            r5 = r0
            r6 = 1
            r5.mIsLayoutDirty = r6
            r5 = r0
            r6 = 0
            r5.mIsLaidOut = r6
            r5 = r0
            r6 = 0
            r5.mChildToScrollTo = r6
            r5 = r0
            r6 = 0
            r5.mIsBeingDragged = r6
            r5 = r0
            r6 = 1
            r5.mSmoothScrollingEnabled = r6
            r5 = r0
            r6 = -1
            r5.mActivePointerId = r6
            r5 = r0
            r6 = 2
            int[] r6 = new int[r6]
            r5.mScrollOffset = r6
            r5 = r0
            r6 = 2
            int[] r6 = new int[r6]
            r5.mScrollConsumed = r6
            r5 = r0
            r5.initScrollView()
            r5 = r1
            r6 = r2
            int[] r7 = android.support.v4.widget.NestedScrollView.SCROLLVIEW_STYLEABLE
            r8 = r3
            r9 = 0
            android.content.res.TypedArray r5 = r5.obtainStyledAttributes(r6, r7, r8, r9)
            r4 = r5
            r5 = r0
            r6 = r4
            r7 = 0
            r8 = 0
            boolean r6 = r6.getBoolean(r7, r8)
            r5.setFillViewport(r6)
            r5 = r4
            r5.recycle()
            r5 = r0
            android.support.v4.view.NestedScrollingParentHelper r6 = new android.support.v4.view.NestedScrollingParentHelper
            r10 = r6
            r6 = r10
            r7 = r10
            r8 = r0
            r7.<init>(r8)
            r5.mParentHelper = r6
            r5 = r0
            android.support.v4.view.NestedScrollingChildHelper r6 = new android.support.v4.view.NestedScrollingChildHelper
            r10 = r6
            r6 = r10
            r7 = r10
            r8 = r0
            r7.<init>(r8)
            r5.mChildHelper = r6
            r5 = r0
            r6 = 1
            r5.setNestedScrollingEnabled(r6)
            r5 = r0
            android.support.v4.widget.NestedScrollView$AccessibilityDelegate r6 = android.support.v4.widget.NestedScrollView.ACCESSIBILITY_DELEGATE
            android.support.v4.view.ViewCompat.setAccessibilityDelegate(r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.NestedScrollView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setNestedScrollingEnabled(boolean z) {
        this.mChildHelper.setNestedScrollingEnabled(z);
    }

    public boolean isNestedScrollingEnabled() {
        return this.mChildHelper.isNestedScrollingEnabled();
    }

    public boolean startNestedScroll(int i) {
        return this.mChildHelper.startNestedScroll(i);
    }

    public void stopNestedScroll() {
        this.mChildHelper.stopNestedScroll();
    }

    public boolean hasNestedScrollingParent() {
        return this.mChildHelper.hasNestedScrollingParent();
    }

    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return this.mChildHelper.dispatchNestedScroll(i, i2, i3, i4, iArr);
    }

    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return this.mChildHelper.dispatchNestedPreScroll(i, i2, iArr, iArr2);
    }

    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return this.mChildHelper.dispatchNestedFling(f, f2, z);
    }

    public boolean dispatchNestedPreFling(float f, float f2) {
        return this.mChildHelper.dispatchNestedPreFling(f, f2);
    }

    public boolean onStartNestedScroll(View view, View view2, int i) {
        return (i & 2) != 0;
    }

    public void onNestedScrollAccepted(View view, View view2, int i) {
        this.mParentHelper.onNestedScrollAccepted(view, view2, i);
        boolean startNestedScroll = startNestedScroll(2);
    }

    public void onStopNestedScroll(View view) {
        this.mParentHelper.onStopNestedScroll(view);
        stopNestedScroll();
    }

    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        int i5 = i4;
        int scrollY = getScrollY();
        scrollBy(0, i5);
        int scrollY2 = getScrollY() - scrollY;
        boolean dispatchNestedScroll = dispatchNestedScroll(0, scrollY2, 0, i5 - scrollY2, null);
    }

    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
    }

    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        float f3 = f2;
        if (z) {
            return false;
        }
        flingWithNestedDispatch((int) f3);
        return true;
    }

    public boolean onNestedPreFling(View view, float f, float f2) {
        return false;
    }

    public int getNestedScrollAxes() {
        return this.mParentHelper.getNestedScrollAxes();
    }

    public boolean shouldDelayChildPressedState() {
        return true;
    }

    /* access modifiers changed from: protected */
    public float getTopFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int scrollY = getScrollY();
        if (scrollY < verticalFadingEdgeLength) {
            return ((float) scrollY) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    /* access modifiers changed from: protected */
    public float getBottomFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int bottom = (getChildAt(0).getBottom() - getScrollY()) - (getHeight() - getPaddingBottom());
        if (bottom < verticalFadingEdgeLength) {
            return ((float) bottom) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    public int getMaxScrollAmount() {
        return (int) (MAX_SCROLL_FACTOR * ((float) getHeight()));
    }

    private void initScrollView() {
        ScrollerCompat scrollerCompat;
        new ScrollerCompat(getContext(), null);
        this.mScroller = scrollerCompat;
        setFocusable(true);
        setDescendantFocusability(262144);
        setWillNotDraw(false);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
        this.mMinimumVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
        this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
    }

    public void addView(View view) {
        Throwable th;
        View view2 = view;
        if (getChildCount() > 0) {
            Throwable th2 = th;
            new IllegalStateException("ScrollView can host only one direct child");
            throw th2;
        }
        super.addView(view2);
    }

    public void addView(View view, int i) {
        Throwable th;
        View view2 = view;
        int i2 = i;
        if (getChildCount() > 0) {
            Throwable th2 = th;
            new IllegalStateException("ScrollView can host only one direct child");
            throw th2;
        }
        super.addView(view2, i2);
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        Throwable th;
        View view2 = view;
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        if (getChildCount() > 0) {
            Throwable th2 = th;
            new IllegalStateException("ScrollView can host only one direct child");
            throw th2;
        }
        super.addView(view2, layoutParams2);
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        Throwable th;
        View view2 = view;
        int i2 = i;
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        if (getChildCount() > 0) {
            Throwable th2 = th;
            new IllegalStateException("ScrollView can host only one direct child");
            throw th2;
        }
        super.addView(view2, i2, layoutParams2);
    }

    public void setOnScrollChangeListener(OnScrollChangeListener onScrollChangeListener) {
        this.mOnScrollChangeListener = onScrollChangeListener;
    }

    private boolean canScroll() {
        View childAt = getChildAt(0);
        if (childAt == null) {
            return false;
        }
        return getHeight() < (childAt.getHeight() + getPaddingTop()) + getPaddingBottom();
    }

    public boolean isFillViewport() {
        return this.mFillViewport;
    }

    public void setFillViewport(boolean z) {
        boolean z2 = z;
        if (z2 != this.mFillViewport) {
            this.mFillViewport = z2;
            requestLayout();
        }
    }

    public boolean isSmoothScrollingEnabled() {
        return this.mSmoothScrollingEnabled;
    }

    public void setSmoothScrollingEnabled(boolean z) {
        this.mSmoothScrollingEnabled = z;
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        super.onScrollChanged(i5, i6, i7, i8);
        if (this.mOnScrollChangeListener != null) {
            this.mOnScrollChangeListener.onScrollChange(this, i5, i6, i7, i8);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        super.onMeasure(i3, i4);
        if (this.mFillViewport && View.MeasureSpec.getMode(i4) != 0 && getChildCount() > 0) {
            View childAt = getChildAt(0);
            int measuredHeight = getMeasuredHeight();
            if (childAt.getMeasuredHeight() < measuredHeight) {
                childAt.measure(getChildMeasureSpec(i3, getPaddingLeft() + getPaddingRight(), ((FrameLayout.LayoutParams) childAt.getLayoutParams()).width), View.MeasureSpec.makeMeasureSpec((measuredHeight - getPaddingTop()) - getPaddingBottom(), 1073741824));
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        KeyEvent keyEvent2 = keyEvent;
        return super.dispatchKeyEvent(keyEvent2) || executeKeyEvent(keyEvent2);
    }

    public boolean executeKeyEvent(KeyEvent keyEvent) {
        boolean z;
        KeyEvent keyEvent2 = keyEvent;
        this.mTempRect.setEmpty();
        if (canScroll()) {
            boolean z2 = false;
            if (keyEvent2.getAction() == 0) {
                switch (keyEvent2.getKeyCode()) {
                    case 19:
                        if (keyEvent2.isAltPressed()) {
                            z2 = fullScroll(33);
                            break;
                        } else {
                            z2 = arrowScroll(33);
                            break;
                        }
                    case 20:
                        if (keyEvent2.isAltPressed()) {
                            z2 = fullScroll(TransportMediator.KEYCODE_MEDIA_RECORD);
                            break;
                        } else {
                            z2 = arrowScroll(TransportMediator.KEYCODE_MEDIA_RECORD);
                            break;
                        }
                    case 62:
                        boolean pageScroll = pageScroll(keyEvent2.isShiftPressed() ? 33 : TransportMediator.KEYCODE_MEDIA_RECORD);
                        break;
                }
            }
            return z2;
        } else if (!isFocused() || keyEvent2.getKeyCode() == 4) {
            return false;
        } else {
            View findFocus = findFocus();
            if (findFocus == this) {
                findFocus = null;
            }
            View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, TransportMediator.KEYCODE_MEDIA_RECORD);
            if (findNextFocus == null || findNextFocus == this || !findNextFocus.requestFocus(TransportMediator.KEYCODE_MEDIA_RECORD)) {
                z = false;
            } else {
                z = true;
            }
            return z;
        }
    }

    private boolean inChild(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (getChildCount() <= 0) {
            return false;
        }
        int scrollY = getScrollY();
        View childAt = getChildAt(0);
        return i4 >= childAt.getTop() - scrollY && i4 < childAt.getBottom() - scrollY && i3 >= childAt.getLeft() && i3 < childAt.getRight();
    }

    private void initOrResetVelocityTracker() {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        } else {
            this.mVelocityTracker.clear();
        }
    }

    private void initVelocityTrackerIfNotExists() {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
    }

    private void recycleVelocityTracker() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        boolean z2 = z;
        if (z2) {
            recycleVelocityTracker();
        }
        super.requestDisallowInterceptTouchEvent(z2);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        StringBuilder sb;
        MotionEvent motionEvent2 = motionEvent;
        int action = motionEvent2.getAction();
        if (action == 2 && this.mIsBeingDragged) {
            return true;
        }
        switch (action & 255) {
            case 0:
                int y = (int) motionEvent2.getY();
                if (inChild((int) motionEvent2.getX(), y)) {
                    this.mLastMotionY = y;
                    this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                    initOrResetVelocityTracker();
                    this.mVelocityTracker.addMovement(motionEvent2);
                    this.mIsBeingDragged = !this.mScroller.isFinished();
                    boolean startNestedScroll = startNestedScroll(2);
                    break;
                } else {
                    this.mIsBeingDragged = false;
                    recycleVelocityTracker();
                    break;
                }
            case 1:
            case 3:
                this.mIsBeingDragged = false;
                this.mActivePointerId = -1;
                recycleVelocityTracker();
                if (this.mScroller.springBack(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                    ViewCompat.postInvalidateOnAnimation(this);
                }
                stopNestedScroll();
                break;
            case 2:
                int i = this.mActivePointerId;
                if (i != -1) {
                    int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, i);
                    if (findPointerIndex != -1) {
                        int y2 = (int) MotionEventCompat.getY(motionEvent2, findPointerIndex);
                        if (Math.abs(y2 - this.mLastMotionY) > this.mTouchSlop && (getNestedScrollAxes() & 2) == 0) {
                            this.mIsBeingDragged = true;
                            this.mLastMotionY = y2;
                            initVelocityTrackerIfNotExists();
                            this.mVelocityTracker.addMovement(motionEvent2);
                            this.mNestedYOffset = 0;
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                                break;
                            }
                        }
                    } else {
                        new StringBuilder();
                        int e = Log.e(TAG, sb.append("Invalid pointerId=").append(i).append(" in onInterceptTouchEvent").toString());
                        break;
                    }
                }
                break;
            case 6:
                onSecondaryPointerUp(motionEvent2);
                break;
        }
        return this.mIsBeingDragged;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        StringBuilder sb;
        ViewParent parent;
        MotionEvent motionEvent2 = motionEvent;
        initVelocityTrackerIfNotExists();
        MotionEvent obtain = MotionEvent.obtain(motionEvent2);
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
        if (actionMasked == 0) {
            this.mNestedYOffset = 0;
        }
        obtain.offsetLocation(0.0f, (float) this.mNestedYOffset);
        switch (actionMasked) {
            case 0:
                if (getChildCount() != 0) {
                    boolean z = !this.mScroller.isFinished();
                    boolean z2 = z;
                    this.mIsBeingDragged = z;
                    if (z2 && (parent = getParent()) != null) {
                        parent.requestDisallowInterceptTouchEvent(true);
                    }
                    if (!this.mScroller.isFinished()) {
                        this.mScroller.abortAnimation();
                    }
                    this.mLastMotionY = (int) motionEvent2.getY();
                    this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                    boolean startNestedScroll = startNestedScroll(2);
                    break;
                } else {
                    return false;
                }
            case 1:
                if (this.mIsBeingDragged) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
                    int yVelocity = (int) VelocityTrackerCompat.getYVelocity(velocityTracker, this.mActivePointerId);
                    if (Math.abs(yVelocity) > this.mMinimumVelocity) {
                        flingWithNestedDispatch(-yVelocity);
                    } else if (this.mScroller.springBack(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                        ViewCompat.postInvalidateOnAnimation(this);
                    }
                }
                this.mActivePointerId = -1;
                endDrag();
                break;
            case 2:
                int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, this.mActivePointerId);
                if (findPointerIndex != -1) {
                    int y = (int) MotionEventCompat.getY(motionEvent2, findPointerIndex);
                    int i = this.mLastMotionY - y;
                    if (dispatchNestedPreScroll(0, i, this.mScrollConsumed, this.mScrollOffset)) {
                        i -= this.mScrollConsumed[1];
                        obtain.offsetLocation(0.0f, (float) this.mScrollOffset[1]);
                        this.mNestedYOffset = this.mNestedYOffset + this.mScrollOffset[1];
                    }
                    if (!this.mIsBeingDragged && Math.abs(i) > this.mTouchSlop) {
                        ViewParent parent2 = getParent();
                        if (parent2 != null) {
                            parent2.requestDisallowInterceptTouchEvent(true);
                        }
                        this.mIsBeingDragged = true;
                        if (i > 0) {
                            i -= this.mTouchSlop;
                        } else {
                            i += this.mTouchSlop;
                        }
                    }
                    if (this.mIsBeingDragged) {
                        this.mLastMotionY = y - this.mScrollOffset[1];
                        int scrollY = getScrollY();
                        int scrollRange = getScrollRange();
                        int overScrollMode = ViewCompat.getOverScrollMode(this);
                        boolean z3 = overScrollMode == 0 || (overScrollMode == 1 && scrollRange > 0);
                        if (overScrollByCompat(0, i, 0, getScrollY(), 0, scrollRange, 0, 0, true) && !hasNestedScrollingParent()) {
                            this.mVelocityTracker.clear();
                        }
                        int scrollY2 = getScrollY() - scrollY;
                        if (!dispatchNestedScroll(0, scrollY2, 0, i - scrollY2, this.mScrollOffset)) {
                            if (z3) {
                                ensureGlows();
                                int i2 = scrollY + i;
                                if (i2 < 0) {
                                    boolean onPull = this.mEdgeGlowTop.onPull(((float) i) / ((float) getHeight()), MotionEventCompat.getX(motionEvent2, findPointerIndex) / ((float) getWidth()));
                                    if (!this.mEdgeGlowBottom.isFinished()) {
                                        boolean onRelease = this.mEdgeGlowBottom.onRelease();
                                    }
                                } else if (i2 > scrollRange) {
                                    boolean onPull2 = this.mEdgeGlowBottom.onPull(((float) i) / ((float) getHeight()), 1.0f - (MotionEventCompat.getX(motionEvent2, findPointerIndex) / ((float) getWidth())));
                                    if (!this.mEdgeGlowTop.isFinished()) {
                                        boolean onRelease2 = this.mEdgeGlowTop.onRelease();
                                    }
                                }
                                if (this.mEdgeGlowTop != null && (!this.mEdgeGlowTop.isFinished() || !this.mEdgeGlowBottom.isFinished())) {
                                    ViewCompat.postInvalidateOnAnimation(this);
                                    break;
                                }
                            }
                        } else {
                            this.mLastMotionY = this.mLastMotionY - this.mScrollOffset[1];
                            obtain.offsetLocation(0.0f, (float) this.mScrollOffset[1]);
                            this.mNestedYOffset = this.mNestedYOffset + this.mScrollOffset[1];
                            break;
                        }
                    }
                } else {
                    new StringBuilder();
                    int e = Log.e(TAG, sb.append("Invalid pointerId=").append(this.mActivePointerId).append(" in onTouchEvent").toString());
                    break;
                }
                break;
            case 3:
                if (this.mIsBeingDragged && getChildCount() > 0 && this.mScroller.springBack(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                    ViewCompat.postInvalidateOnAnimation(this);
                }
                this.mActivePointerId = -1;
                endDrag();
                break;
            case 5:
                int actionIndex = MotionEventCompat.getActionIndex(motionEvent2);
                this.mLastMotionY = (int) MotionEventCompat.getY(motionEvent2, actionIndex);
                this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, actionIndex);
                break;
            case 6:
                onSecondaryPointerUp(motionEvent2);
                this.mLastMotionY = (int) MotionEventCompat.getY(motionEvent2, MotionEventCompat.findPointerIndex(motionEvent2, this.mActivePointerId));
                break;
        }
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.addMovement(obtain);
        }
        obtain.recycle();
        return true;
    }

    private void onSecondaryPointerUp(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int action = (motionEvent2.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
        if (MotionEventCompat.getPointerId(motionEvent2, action) == this.mActivePointerId) {
            int i = action == 0 ? 1 : 0;
            this.mLastMotionY = (int) MotionEventCompat.getY(motionEvent2, i);
            this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, i);
            if (this.mVelocityTracker != null) {
                this.mVelocityTracker.clear();
            }
        }
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        if ((MotionEventCompat.getSource(motionEvent2) & 2) != 0) {
            switch (motionEvent2.getAction()) {
                case 8:
                    if (!this.mIsBeingDragged) {
                        float axisValue = MotionEventCompat.getAxisValue(motionEvent2, 9);
                        if (axisValue != 0.0f) {
                            int verticalScrollFactorCompat = (int) (axisValue * getVerticalScrollFactorCompat());
                            int scrollRange = getScrollRange();
                            int scrollY = getScrollY();
                            int i = scrollY - verticalScrollFactorCompat;
                            if (i < 0) {
                                i = 0;
                            } else if (i > scrollRange) {
                                i = scrollRange;
                            }
                            if (i != scrollY) {
                                super.scrollTo(getScrollX(), i);
                                return true;
                            }
                        }
                    }
                    break;
            }
        }
        return false;
    }

    private float getVerticalScrollFactorCompat() {
        TypedValue typedValue;
        Throwable th;
        if (this.mVerticalScrollFactor == 0.0f) {
            new TypedValue();
            TypedValue typedValue2 = typedValue;
            Context context = getContext();
            if (!context.getTheme().resolveAttribute(16842829, typedValue2, true)) {
                Throwable th2 = th;
                new IllegalStateException("Expected theme to define listPreferredItemHeight.");
                throw th2;
            }
            this.mVerticalScrollFactor = typedValue2.getDimension(context.getResources().getDisplayMetrics());
        }
        return this.mVerticalScrollFactor;
    }

    /* access modifiers changed from: protected */
    public void onOverScrolled(int i, int i2, boolean z, boolean z2) {
        super.scrollTo(i, i2);
    }

    /* access modifiers changed from: package-private */
    public boolean overScrollByCompat(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z) {
        boolean z2;
        int i9 = i;
        int i10 = i2;
        int i11 = i3;
        int i12 = i4;
        int i13 = i5;
        int i14 = i6;
        int i15 = i7;
        int i16 = i8;
        int overScrollMode = ViewCompat.getOverScrollMode(this);
        boolean z3 = computeHorizontalScrollRange() > computeHorizontalScrollExtent();
        boolean z4 = computeVerticalScrollRange() > computeVerticalScrollExtent();
        boolean z5 = overScrollMode == 0 || (overScrollMode == 1 && z3);
        boolean z6 = overScrollMode == 0 || (overScrollMode == 1 && z4);
        int i17 = i11 + i9;
        if (!z5) {
            i15 = 0;
        }
        int i18 = i12 + i10;
        if (!z6) {
            i16 = 0;
        }
        int i19 = -i15;
        int i20 = i15 + i13;
        int i21 = -i16;
        int i22 = i16 + i14;
        boolean z7 = false;
        if (i17 > i20) {
            i17 = i20;
            z7 = true;
        } else if (i17 < i19) {
            i17 = i19;
            z7 = true;
        }
        boolean z8 = false;
        if (i18 > i22) {
            i18 = i22;
            z8 = true;
        } else if (i18 < i21) {
            i18 = i21;
            z8 = true;
        }
        if (z8) {
            boolean springBack = this.mScroller.springBack(i17, i18, 0, 0, 0, getScrollRange());
        }
        onOverScrolled(i17, i18, z7, z8);
        if (z7 || z8) {
            z2 = true;
        } else {
            z2 = false;
        }
        return z2;
    }

    /* access modifiers changed from: private */
    public int getScrollRange() {
        int i = 0;
        if (getChildCount() > 0) {
            i = Math.max(0, getChildAt(0).getHeight() - ((getHeight() - getPaddingBottom()) - getPaddingTop()));
        }
        return i;
    }

    private View findFocusableViewInBounds(boolean z, int i, int i2) {
        boolean z2 = z;
        int i3 = i;
        int i4 = i2;
        ArrayList focusables = getFocusables(2);
        View view = null;
        boolean z3 = false;
        int size = focusables.size();
        for (int i5 = 0; i5 < size; i5++) {
            View view2 = (View) focusables.get(i5);
            int top = view2.getTop();
            int bottom = view2.getBottom();
            if (i3 < bottom && top < i4) {
                boolean z4 = i3 < top && bottom < i4;
                if (view == null) {
                    view = view2;
                    z3 = z4;
                } else {
                    boolean z5 = (z2 && top < view.getTop()) || (!z2 && bottom > view.getBottom());
                    if (z3) {
                        if (z4 && z5) {
                            view = view2;
                        }
                    } else if (z4) {
                        view = view2;
                        z3 = true;
                    } else if (z5) {
                        view = view2;
                    }
                }
            }
        }
        return view;
    }

    public boolean pageScroll(int i) {
        int i2 = i;
        boolean z = i2 == 130;
        int height = getHeight();
        if (z) {
            this.mTempRect.top = getScrollY() + height;
            int childCount = getChildCount();
            if (childCount > 0) {
                View childAt = getChildAt(childCount - 1);
                if (this.mTempRect.top + height > childAt.getBottom()) {
                    this.mTempRect.top = childAt.getBottom() - height;
                }
            }
        } else {
            this.mTempRect.top = getScrollY() - height;
            if (this.mTempRect.top < 0) {
                this.mTempRect.top = 0;
            }
        }
        this.mTempRect.bottom = this.mTempRect.top + height;
        return scrollAndFocus(i2, this.mTempRect.top, this.mTempRect.bottom);
    }

    public boolean fullScroll(int i) {
        int childCount;
        int i2 = i;
        boolean z = i2 == 130;
        int height = getHeight();
        this.mTempRect.top = 0;
        this.mTempRect.bottom = height;
        if (z && (childCount = getChildCount()) > 0) {
            this.mTempRect.bottom = getChildAt(childCount - 1).getBottom() + getPaddingBottom();
            this.mTempRect.top = this.mTempRect.bottom - height;
        }
        return scrollAndFocus(i2, this.mTempRect.top, this.mTempRect.bottom);
    }

    private boolean scrollAndFocus(int i, int i2, int i3) {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        boolean z = true;
        int height = getHeight();
        int scrollY = getScrollY();
        int i7 = scrollY + height;
        boolean z2 = i4 == 33;
        View findFocusableViewInBounds = findFocusableViewInBounds(z2, i5, i6);
        if (findFocusableViewInBounds == null) {
            findFocusableViewInBounds = this;
        }
        if (i5 < scrollY || i6 > i7) {
            doScrollY(z2 ? i5 - scrollY : i6 - i7);
        } else {
            z = false;
        }
        if (findFocusableViewInBounds != findFocus()) {
            boolean requestFocus = findFocusableViewInBounds.requestFocus(i4);
        }
        return z;
    }

    public boolean arrowScroll(int i) {
        int i2 = i;
        View findFocus = findFocus();
        if (findFocus == this) {
            findFocus = null;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, i2);
        int maxScrollAmount = getMaxScrollAmount();
        if (findNextFocus == null || !isWithinDeltaOfScreen(findNextFocus, maxScrollAmount, getHeight())) {
            int i3 = maxScrollAmount;
            if (i2 == 33 && getScrollY() < i3) {
                i3 = getScrollY();
            } else if (i2 == 130 && getChildCount() > 0) {
                int bottom = getChildAt(0).getBottom();
                int scrollY = (getScrollY() + getHeight()) - getPaddingBottom();
                if (bottom - scrollY < maxScrollAmount) {
                    i3 = bottom - scrollY;
                }
            }
            if (i3 == 0) {
                return false;
            }
            doScrollY(i2 == 130 ? i3 : -i3);
        } else {
            findNextFocus.getDrawingRect(this.mTempRect);
            offsetDescendantRectToMyCoords(findNextFocus, this.mTempRect);
            doScrollY(computeScrollDeltaToGetChildRectOnScreen(this.mTempRect));
            boolean requestFocus = findNextFocus.requestFocus(i2);
        }
        if (findFocus != null && findFocus.isFocused() && isOffScreen(findFocus)) {
            int descendantFocusability = getDescendantFocusability();
            setDescendantFocusability(131072);
            boolean requestFocus2 = requestFocus();
            setDescendantFocusability(descendantFocusability);
        }
        return true;
    }

    private boolean isOffScreen(View view) {
        return !isWithinDeltaOfScreen(view, 0, getHeight());
    }

    private boolean isWithinDeltaOfScreen(View view, int i, int i2) {
        View view2 = view;
        int i3 = i;
        int i4 = i2;
        view2.getDrawingRect(this.mTempRect);
        offsetDescendantRectToMyCoords(view2, this.mTempRect);
        return this.mTempRect.bottom + i3 >= getScrollY() && this.mTempRect.top - i3 <= getScrollY() + i4;
    }

    private void doScrollY(int i) {
        int i2 = i;
        if (i2 == 0) {
            return;
        }
        if (this.mSmoothScrollingEnabled) {
            smoothScrollBy(0, i2);
        } else {
            scrollBy(0, i2);
        }
    }

    public final void smoothScrollBy(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (getChildCount() != 0) {
            if (AnimationUtils.currentAnimationTimeMillis() - this.mLastScroll > 250) {
                int max = Math.max(0, getChildAt(0).getHeight() - ((getHeight() - getPaddingBottom()) - getPaddingTop()));
                int scrollY = getScrollY();
                this.mScroller.startScroll(getScrollX(), scrollY, 0, Math.max(0, Math.min(scrollY + i4, max)) - scrollY);
                ViewCompat.postInvalidateOnAnimation(this);
            } else {
                if (!this.mScroller.isFinished()) {
                    this.mScroller.abortAnimation();
                }
                scrollBy(i3, i4);
            }
            this.mLastScroll = AnimationUtils.currentAnimationTimeMillis();
        }
    }

    public final void smoothScrollTo(int i, int i2) {
        smoothScrollBy(i - getScrollX(), i2 - getScrollY());
    }

    public int computeVerticalScrollRange() {
        int childCount = getChildCount();
        int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
        if (childCount == 0) {
            return height;
        }
        int bottom = getChildAt(0).getBottom();
        int scrollY = getScrollY();
        int max = Math.max(0, bottom - height);
        if (scrollY < 0) {
            bottom -= scrollY;
        } else if (scrollY > max) {
            bottom += scrollY - max;
        }
        return bottom;
    }

    public int computeVerticalScrollOffset() {
        return Math.max(0, super.computeVerticalScrollOffset());
    }

    public int computeVerticalScrollExtent() {
        return super.computeVerticalScrollExtent();
    }

    public int computeHorizontalScrollRange() {
        return super.computeHorizontalScrollRange();
    }

    public int computeHorizontalScrollOffset() {
        return super.computeHorizontalScrollOffset();
    }

    public int computeHorizontalScrollExtent() {
        return super.computeHorizontalScrollExtent();
    }

    /* access modifiers changed from: protected */
    public void measureChild(View view, int i, int i2) {
        View view2 = view;
        view2.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight(), view2.getLayoutParams().width), View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    /* access modifiers changed from: protected */
    public void measureChildWithMargins(View view, int i, int i2, int i3, int i4) {
        View view2 = view;
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view2.getLayoutParams();
        view2.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i2, marginLayoutParams.width), View.MeasureSpec.makeMeasureSpec(marginLayoutParams.topMargin + marginLayoutParams.bottomMargin, 0));
    }

    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.mScroller.getCurrX();
            int currY = this.mScroller.getCurrY();
            if (scrollX != currX || scrollY != currY) {
                int scrollRange = getScrollRange();
                int overScrollMode = ViewCompat.getOverScrollMode(this);
                boolean z = overScrollMode == 0 || (overScrollMode == 1 && scrollRange > 0);
                boolean overScrollByCompat = overScrollByCompat(currX - scrollX, currY - scrollY, scrollX, scrollY, 0, scrollRange, 0, 0, false);
                if (z) {
                    ensureGlows();
                    if (currY <= 0 && scrollY > 0) {
                        boolean onAbsorb = this.mEdgeGlowTop.onAbsorb((int) this.mScroller.getCurrVelocity());
                    } else if (currY >= scrollRange && scrollY < scrollRange) {
                        boolean onAbsorb2 = this.mEdgeGlowBottom.onAbsorb((int) this.mScroller.getCurrVelocity());
                    }
                }
            }
        }
    }

    private void scrollToChild(View view) {
        View view2 = view;
        view2.getDrawingRect(this.mTempRect);
        offsetDescendantRectToMyCoords(view2, this.mTempRect);
        int computeScrollDeltaToGetChildRectOnScreen = computeScrollDeltaToGetChildRectOnScreen(this.mTempRect);
        if (computeScrollDeltaToGetChildRectOnScreen != 0) {
            scrollBy(0, computeScrollDeltaToGetChildRectOnScreen);
        }
    }

    private boolean scrollToChildRect(Rect rect, boolean z) {
        boolean z2 = z;
        int computeScrollDeltaToGetChildRectOnScreen = computeScrollDeltaToGetChildRectOnScreen(rect);
        boolean z3 = computeScrollDeltaToGetChildRectOnScreen != 0;
        if (z3) {
            if (z2) {
                scrollBy(0, computeScrollDeltaToGetChildRectOnScreen);
            } else {
                smoothScrollBy(0, computeScrollDeltaToGetChildRectOnScreen);
            }
        }
        return z3;
    }

    /* access modifiers changed from: protected */
    public int computeScrollDeltaToGetChildRectOnScreen(Rect rect) {
        int i;
        int i2;
        Rect rect2 = rect;
        if (getChildCount() == 0) {
            return 0;
        }
        int height = getHeight();
        int scrollY = getScrollY();
        int i3 = scrollY + height;
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        if (rect2.top > 0) {
            scrollY += verticalFadingEdgeLength;
        }
        if (rect2.bottom < getChildAt(0).getHeight()) {
            i3 -= verticalFadingEdgeLength;
        }
        int i4 = 0;
        if (rect2.bottom > i3 && rect2.top > scrollY) {
            if (rect2.height() > height) {
                i2 = 0 + (rect2.top - scrollY);
            } else {
                i2 = 0 + (rect2.bottom - i3);
            }
            i4 = Math.min(i2, getChildAt(0).getBottom() - i3);
        } else if (rect2.top < scrollY && rect2.bottom < i3) {
            if (rect2.height() > height) {
                i = 0 - (i3 - rect2.bottom);
            } else {
                i = 0 - (scrollY - rect2.top);
            }
            i4 = Math.max(i, -getScrollY());
        }
        return i4;
    }

    public void requestChildFocus(View view, View view2) {
        View view3 = view;
        View view4 = view2;
        if (!this.mIsLayoutDirty) {
            scrollToChild(view4);
        } else {
            this.mChildToScrollTo = view4;
        }
        super.requestChildFocus(view3, view4);
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        int i2 = i;
        Rect rect2 = rect;
        if (i2 == 2) {
            i2 = 130;
        } else if (i2 == 1) {
            i2 = 33;
        }
        View findNextFocus = rect2 == null ? FocusFinder.getInstance().findNextFocus(this, null, i2) : FocusFinder.getInstance().findNextFocusFromRect(this, rect2, i2);
        if (findNextFocus == null) {
            return false;
        }
        if (isOffScreen(findNextFocus)) {
            return false;
        }
        return findNextFocus.requestFocus(i2, rect2);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        View view2 = view;
        Rect rect2 = rect;
        rect2.offset(view2.getLeft() - view2.getScrollX(), view2.getTop() - view2.getScrollY());
        return scrollToChildRect(rect2, z);
    }

    public void requestLayout() {
        this.mIsLayoutDirty = true;
        super.requestLayout();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i2;
        int i6 = i4;
        super.onLayout(z, i, i5, i3, i6);
        this.mIsLayoutDirty = false;
        if (this.mChildToScrollTo != null && isViewDescendantOf(this.mChildToScrollTo, this)) {
            scrollToChild(this.mChildToScrollTo);
        }
        this.mChildToScrollTo = null;
        if (!this.mIsLaidOut) {
            if (this.mSavedState != null) {
                scrollTo(getScrollX(), this.mSavedState.scrollPosition);
                this.mSavedState = null;
            }
            int max = Math.max(0, (getChildCount() > 0 ? getChildAt(0).getMeasuredHeight() : 0) - (((i6 - i5) - getPaddingBottom()) - getPaddingTop()));
            if (getScrollY() > max) {
                scrollTo(getScrollX(), max);
            } else if (getScrollY() < 0) {
                scrollTo(getScrollX(), 0);
            }
        }
        scrollTo(getScrollX(), getScrollY());
        this.mIsLaidOut = true;
    }

    public void onAttachedToWindow() {
        this.mIsLaidOut = false;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        int i5 = i4;
        super.onSizeChanged(i, i2, i3, i5);
        View findFocus = findFocus();
        if (null != findFocus && this != findFocus && isWithinDeltaOfScreen(findFocus, 0, i5)) {
            findFocus.getDrawingRect(this.mTempRect);
            offsetDescendantRectToMyCoords(findFocus, this.mTempRect);
            doScrollY(computeScrollDeltaToGetChildRectOnScreen(this.mTempRect));
        }
    }

    private static boolean isViewDescendantOf(View view, View view2) {
        View view3 = view;
        View view4 = view2;
        if (view3 == view4) {
            return true;
        }
        ViewParent parent = view3.getParent();
        return (parent instanceof ViewGroup) && isViewDescendantOf((View) parent, view4);
    }

    public void fling(int i) {
        int i2 = i;
        if (getChildCount() > 0) {
            int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
            this.mScroller.fling(getScrollX(), getScrollY(), 0, i2, 0, 0, 0, Math.max(0, getChildAt(0).getHeight() - height), 0, height / 2);
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    private void flingWithNestedDispatch(int i) {
        int i2 = i;
        int scrollY = getScrollY();
        boolean z = (scrollY > 0 || i2 > 0) && (scrollY < getScrollRange() || i2 < 0);
        if (!dispatchNestedPreFling(0.0f, (float) i2)) {
            boolean dispatchNestedFling = dispatchNestedFling(0.0f, (float) i2, z);
            if (z) {
                fling(i2);
            }
        }
    }

    private void endDrag() {
        this.mIsBeingDragged = false;
        recycleVelocityTracker();
        stopNestedScroll();
        if (this.mEdgeGlowTop != null) {
            boolean onRelease = this.mEdgeGlowTop.onRelease();
            boolean onRelease2 = this.mEdgeGlowBottom.onRelease();
        }
    }

    public void scrollTo(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (getChildCount() > 0) {
            View childAt = getChildAt(0);
            int clamp = clamp(i3, (getWidth() - getPaddingRight()) - getPaddingLeft(), childAt.getWidth());
            int clamp2 = clamp(i4, (getHeight() - getPaddingBottom()) - getPaddingTop(), childAt.getHeight());
            if (clamp != getScrollX() || clamp2 != getScrollY()) {
                super.scrollTo(clamp, clamp2);
            }
        }
    }

    private void ensureGlows() {
        EdgeEffectCompat edgeEffectCompat;
        EdgeEffectCompat edgeEffectCompat2;
        if (ViewCompat.getOverScrollMode(this) == 2) {
            this.mEdgeGlowTop = null;
            this.mEdgeGlowBottom = null;
        } else if (this.mEdgeGlowTop == null) {
            Context context = getContext();
            new EdgeEffectCompat(context);
            this.mEdgeGlowTop = edgeEffectCompat;
            new EdgeEffectCompat(context);
            this.mEdgeGlowBottom = edgeEffectCompat2;
        }
    }

    public void draw(Canvas canvas) {
        Canvas canvas2 = canvas;
        super.draw(canvas2);
        if (this.mEdgeGlowTop != null) {
            int scrollY = getScrollY();
            if (!this.mEdgeGlowTop.isFinished()) {
                int save = canvas2.save();
                int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
                canvas2.translate((float) getPaddingLeft(), (float) Math.min(0, scrollY));
                this.mEdgeGlowTop.setSize(width, getHeight());
                if (this.mEdgeGlowTop.draw(canvas2)) {
                    ViewCompat.postInvalidateOnAnimation(this);
                }
                canvas2.restoreToCount(save);
            }
            if (!this.mEdgeGlowBottom.isFinished()) {
                int save2 = canvas2.save();
                int width2 = (getWidth() - getPaddingLeft()) - getPaddingRight();
                int height = getHeight();
                canvas2.translate((float) ((-width2) + getPaddingLeft()), (float) (Math.max(getScrollRange(), scrollY) + height));
                canvas2.rotate(180.0f, (float) width2, 0.0f);
                this.mEdgeGlowBottom.setSize(width2, height);
                if (this.mEdgeGlowBottom.draw(canvas2)) {
                    ViewCompat.postInvalidateOnAnimation(this);
                }
                canvas2.restoreToCount(save2);
            }
        }
    }

    private static int clamp(int i, int i2, int i3) {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        if (i5 >= i6 || i4 < 0) {
            return 0;
        }
        if (i5 + i4 > i6) {
            return i6 - i5;
        }
        return i4;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.mSavedState = savedState;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState;
        new SavedState(super.onSaveInstanceState());
        SavedState savedState2 = savedState;
        savedState2.scrollPosition = getScrollY();
        return savedState2;
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR;
        public int scrollPosition;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public SavedState(android.os.Parcel r5) {
            /*
                r4 = this;
                r0 = r4
                r1 = r5
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                r2 = r0
                r3 = r1
                int r3 = r3.readInt()
                r2.scrollPosition = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.NestedScrollView.SavedState.<init>(android.os.Parcel):void");
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            super.writeToParcel(parcel2, i);
            parcel2.writeInt(this.scrollPosition);
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder();
            return sb.append("HorizontalScrollView.SavedState{").append(Integer.toHexString(System.identityHashCode(this))).append(" scrollPosition=").append(this.scrollPosition).append("}").toString();
        }

        static {
            Parcelable.Creator<SavedState> creator;
            new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(Parcel parcel) {
                    SavedState savedState;
                    new SavedState(parcel);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = creator;
        }
    }

    static class AccessibilityDelegate extends AccessibilityDelegateCompat {
        AccessibilityDelegate() {
        }

        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            View view2 = view;
            int i2 = i;
            if (super.performAccessibilityAction(view2, i2, bundle)) {
                return true;
            }
            NestedScrollView nestedScrollView = (NestedScrollView) view2;
            if (!nestedScrollView.isEnabled()) {
                return false;
            }
            switch (i2) {
                case 4096:
                    int min = Math.min(nestedScrollView.getScrollY() + ((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()), nestedScrollView.getScrollRange());
                    if (min == nestedScrollView.getScrollY()) {
                        return false;
                    }
                    nestedScrollView.smoothScrollTo(0, min);
                    return true;
                case 8192:
                    int max = Math.max(nestedScrollView.getScrollY() - ((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()), 0);
                    if (max == nestedScrollView.getScrollY()) {
                        return false;
                    }
                    nestedScrollView.smoothScrollTo(0, max);
                    return true;
                default:
                    return false;
            }
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            int access$000;
            View view2 = view;
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
            super.onInitializeAccessibilityNodeInfo(view2, accessibilityNodeInfoCompat2);
            NestedScrollView nestedScrollView = (NestedScrollView) view2;
            accessibilityNodeInfoCompat2.setClassName(ScrollView.class.getName());
            if (nestedScrollView.isEnabled() && (access$000 = nestedScrollView.getScrollRange()) > 0) {
                accessibilityNodeInfoCompat2.setScrollable(true);
                if (nestedScrollView.getScrollY() > 0) {
                    accessibilityNodeInfoCompat2.addAction(8192);
                }
                if (nestedScrollView.getScrollY() < access$000) {
                    accessibilityNodeInfoCompat2.addAction(4096);
                }
            }
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            View view2 = view;
            AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
            super.onInitializeAccessibilityEvent(view2, accessibilityEvent2);
            NestedScrollView nestedScrollView = (NestedScrollView) view2;
            accessibilityEvent2.setClassName(ScrollView.class.getName());
            AccessibilityRecordCompat asRecord = AccessibilityEventCompat.asRecord(accessibilityEvent2);
            asRecord.setScrollable(nestedScrollView.getScrollRange() > 0);
            asRecord.setScrollX(nestedScrollView.getScrollX());
            asRecord.setScrollY(nestedScrollView.getScrollY());
            asRecord.setMaxScrollX(nestedScrollView.getScrollX());
            asRecord.setMaxScrollY(nestedScrollView.getScrollRange());
        }
    }
}
