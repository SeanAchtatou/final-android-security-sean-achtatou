package android.support.v4.view;

import android.content.Context;
import android.support.v4.view.LayoutInflaterCompatBase;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import java.lang.reflect.Field;

class LayoutInflaterCompatHC {
    private static final String TAG = "LayoutInflaterCompatHC";
    private static boolean sCheckedField;
    private static Field sLayoutInflaterFactory2Field;

    LayoutInflaterCompatHC() {
    }

    static class FactoryWrapperHC extends LayoutInflaterCompatBase.FactoryWrapper implements LayoutInflater.Factory2 {
        FactoryWrapperHC(LayoutInflaterFactory layoutInflaterFactory) {
            super(layoutInflaterFactory);
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            return this.mDelegateFactory.onCreateView(view, str, context, attributeSet);
        }
    }

    static void setFactory(LayoutInflater layoutInflater, LayoutInflaterFactory layoutInflaterFactory) {
        LayoutInflater.Factory2 factory2;
        LayoutInflater.Factory2 factory22;
        LayoutInflater layoutInflater2 = layoutInflater;
        LayoutInflaterFactory layoutInflaterFactory2 = layoutInflaterFactory;
        if (layoutInflaterFactory2 != null) {
            factory2 = factory22;
            new FactoryWrapperHC(layoutInflaterFactory2);
        } else {
            factory2 = null;
        }
        LayoutInflater.Factory2 factory23 = factory2;
        layoutInflater2.setFactory2(factory23);
        LayoutInflater.Factory factory = layoutInflater2.getFactory();
        if (factory instanceof LayoutInflater.Factory2) {
            forceSetFactory2(layoutInflater2, (LayoutInflater.Factory2) factory);
        } else {
            forceSetFactory2(layoutInflater2, factory23);
        }
    }

    static void forceSetFactory2(LayoutInflater layoutInflater, LayoutInflater.Factory2 factory2) {
        StringBuilder sb;
        StringBuilder sb2;
        LayoutInflater layoutInflater2 = layoutInflater;
        LayoutInflater.Factory2 factory22 = factory2;
        if (!sCheckedField) {
            try {
                sLayoutInflaterFactory2Field = LayoutInflater.class.getDeclaredField("mFactory2");
                sLayoutInflaterFactory2Field.setAccessible(true);
            } catch (NoSuchFieldException e) {
                new StringBuilder();
                int e2 = Log.e(TAG, sb2.append("forceSetFactory2 Could not find field 'mFactory2' on class ").append(LayoutInflater.class.getName()).append("; inflation may have unexpected results.").toString(), e);
            }
            sCheckedField = true;
        }
        if (sLayoutInflaterFactory2Field != null) {
            try {
                sLayoutInflaterFactory2Field.set(layoutInflater2, factory22);
            } catch (IllegalAccessException e3) {
                new StringBuilder();
                int e4 = Log.e(TAG, sb.append("forceSetFactory2 could not set the Factory2 on LayoutInflater ").append(layoutInflater2).append("; inflation may have unexpected results.").toString(), e3);
            }
        }
    }
}
