package android.support.v4.os;

import android.os.Build;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.IOException;

public class EnvironmentCompat {
    public static final String MEDIA_UNKNOWN = "unknown";
    private static final String TAG = "EnvironmentCompat";

    public static String getStorageState(File file) {
        StringBuilder sb;
        File file2 = file;
        if (Build.VERSION.SDK_INT >= 19) {
            return EnvironmentCompatKitKat.getStorageState(file2);
        }
        try {
            if (file2.getCanonicalPath().startsWith(Environment.getExternalStorageDirectory().getCanonicalPath())) {
                return Environment.getExternalStorageState();
            }
        } catch (IOException e) {
            new StringBuilder();
            int w = Log.w(TAG, sb.append("Failed to resolve canonical path: ").append(e).toString());
        }
        return MEDIA_UNKNOWN;
    }
}
