package com.flad.i;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flad.f.c;
import com.flad.h.e;
import com.flad.h.f;

public class a extends Dialog {
    /* access modifiers changed from: private */
    public Context a;
    private Bitmap b = null;
    /* access modifiers changed from: private */
    public com.flad.a.a c;
    private String d = "";

    public a(Context context, com.flad.a.a aVar) {
        super(context);
        this.a = context;
        this.c = aVar;
    }

    private int a(int i) {
        return (int) (0.5f + (this.a.getResources().getDisplayMetrics().density * ((float) i)));
    }

    private View a() {
        RelativeLayout relativeLayout = new RelativeLayout(this.a);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        relativeLayout.setBackgroundColor(0);
        RelativeLayout relativeLayout2 = new RelativeLayout(this.a);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        relativeLayout2.setLayoutParams(layoutParams);
        ImageView b2 = b();
        b2.setOnClickListener(new b(this));
        ImageView c2 = c();
        c2.setOnClickListener(new c(this));
        TextView d2 = d();
        relativeLayout2.addView(b2);
        relativeLayout2.addView(c2);
        relativeLayout2.addView(d2);
        relativeLayout.addView(relativeLayout2);
        return relativeLayout;
    }

    private ImageView b() {
        ImageView imageView = new ImageView(this.a);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setAdjustViewBounds(true);
        imageView.setLayoutParams(layoutParams);
        imageView.setImageBitmap(this.b);
        return imageView;
    }

    private ImageView c() {
        ImageView imageView = new ImageView(this.a);
        imageView.setImageDrawable(new BitmapDrawable(BitmapFactory.decodeStream(d.a(d.a()))));
        int a2 = a(20);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2, a2);
        layoutParams.addRule(11);
        imageView.setLayoutParams(layoutParams);
        return imageView;
    }

    private TextView d() {
        TextView textView = new TextView(this.a);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(11);
        layoutParams.addRule(12);
        textView.setLayoutParams(layoutParams);
        textView.setTextColor(-1);
        textView.setTextSize(15.0f);
        textView.setText("广告");
        return textView;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        window.setBackgroundDrawable(new ColorDrawable());
        DisplayMetrics displayMetrics = this.a.getResources().getDisplayMetrics();
        int a2 = a(16);
        int width = this.b.getWidth();
        int height = this.b.getHeight();
        int i = displayMetrics.widthPixels - (a2 * 4);
        attributes.width = i;
        attributes.height = (i * height) / width;
        window.setAttributes(attributes);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        this.b = f.a(e.a(String.valueOf(e.b(this.c.b())) + ".png"));
        setContentView(a());
        new c(this.a).a("show_ad");
    }
}
