package com.flad.b;

import com.flad.g.d;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

class c implements Runnable {
    final /* synthetic */ a a;
    private final /* synthetic */ String b;

    c(a aVar, String str) {
        this.a = aVar;
        this.b = str;
    }

    public void run() {
        File file = new File(this.a.d, this.a.e);
        try {
            if (!file.exists()) {
                file.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                if (this.a.a(this.b, fileOutputStream)) {
                    d.a("ImageLoader", String.valueOf(this.a.d) + this.a.e + "download success");
                } else {
                    d.a("ImageLoader", String.valueOf(this.a.d) + this.a.e + "download fail");
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e2) {
            d.a("ImageLoader", String.valueOf(this.a.d) + this.a.e + "download fail");
        }
    }
}
