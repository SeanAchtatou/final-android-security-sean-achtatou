package com.flad.b;

import android.os.Handler;
import android.os.Looper;
import java.io.File;

public class a {
    private int a;
    private Handler b = new b(this, Looper.getMainLooper());
    private e c = new e();
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;

    private a(String str, String str2) {
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        }
        this.e = str2;
        this.d = str;
    }

    public static a a(String str, String str2) {
        return new a(str, str2);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.io.BufferedOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.io.BufferedInputStream} */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0061 A[SYNTHETIC, Splitter:B:31:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0066 A[SYNTHETIC, Splitter:B:34:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0097 A[SYNTHETIC, Splitter:B:53:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x009c A[SYNTHETIC, Splitter:B:56:0x009c] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00a3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r11, java.io.OutputStream r12) {
        /*
            r10 = this;
            r7 = 3
            r2 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ IOException -> 0x00c5, all -> 0x008c }
            r0.<init>(r11)     // Catch:{ IOException -> 0x00c5, all -> 0x008c }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x00c5, all -> 0x008c }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x00c5, all -> 0x008c }
            r1 = 3000(0xbb8, float:4.204E-42)
            r0.setConnectTimeout(r1)     // Catch:{ IOException -> 0x00ca, all -> 0x00b7 }
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x00ca, all -> 0x00b7 }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ IOException -> 0x00ca, all -> 0x00b7 }
            r4 = 8192(0x2000, float:1.14794E-41)
            r3.<init>(r1, r4)     // Catch:{ IOException -> 0x00ca, all -> 0x00b7 }
            java.io.BufferedOutputStream r4 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x00d1, all -> 0x00da }
            r1 = 8192(0x2000, float:1.14794E-41)
            r4.<init>(r12, r1)     // Catch:{ IOException -> 0x00d1, all -> 0x00da }
        L_0x0024:
            int r1 = r3.read()     // Catch:{ IOException -> 0x0040, all -> 0x00ba }
            r2 = -1
            if (r1 != r2) goto L_0x003c
            if (r0 == 0) goto L_0x0030
            r0.disconnect()
        L_0x0030:
            if (r4 == 0) goto L_0x0035
            r4.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0035:
            if (r3 == 0) goto L_0x003a
            r3.close()     // Catch:{ IOException -> 0x007d }
        L_0x003a:
            r0 = 1
        L_0x003b:
            return r0
        L_0x003c:
            r4.write(r1)     // Catch:{ IOException -> 0x0040, all -> 0x00ba }
            goto L_0x0024
        L_0x0040:
            r1 = move-exception
            r2 = r3
            r3 = r4
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0046:
            java.lang.String r4 = "ImageLoader"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            java.lang.String r6 = "downloadBitmap failed."
            r5.<init>(r6)     // Catch:{ all -> 0x00bd }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ all -> 0x00bd }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00bd }
            com.flad.g.d.b(r4, r0)     // Catch:{ all -> 0x00bd }
            if (r1 == 0) goto L_0x005f
            r1.disconnect()
        L_0x005f:
            if (r3 == 0) goto L_0x0064
            r3.close()     // Catch:{ IOException -> 0x0082 }
        L_0x0064:
            if (r2 == 0) goto L_0x0069
            r2.close()     // Catch:{ IOException -> 0x0087 }
        L_0x0069:
            int r0 = r10.a
            if (r0 >= r7) goto L_0x0076
            int r0 = r10.a
            int r0 = r0 + 1
            r10.a = r0
            r10.a(r11, r12)
        L_0x0076:
            r0 = 0
            goto L_0x003b
        L_0x0078:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0035
        L_0x007d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003a
        L_0x0082:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0064
        L_0x0087:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0069
        L_0x008c:
            r0 = move-exception
            r1 = r0
            r3 = r2
            r0 = r2
        L_0x0090:
            if (r0 == 0) goto L_0x0095
            r0.disconnect()
        L_0x0095:
            if (r2 == 0) goto L_0x009a
            r2.close()     // Catch:{ IOException -> 0x00ad }
        L_0x009a:
            if (r3 == 0) goto L_0x009f
            r3.close()     // Catch:{ IOException -> 0x00b2 }
        L_0x009f:
            int r0 = r10.a
            if (r0 >= r7) goto L_0x00ac
            int r0 = r10.a
            int r0 = r0 + 1
            r10.a = r0
            r10.a(r11, r12)
        L_0x00ac:
            throw r1
        L_0x00ad:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x009a
        L_0x00b2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x009f
        L_0x00b7:
            r1 = move-exception
            r3 = r2
            goto L_0x0090
        L_0x00ba:
            r1 = move-exception
            r2 = r4
            goto L_0x0090
        L_0x00bd:
            r0 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0090
        L_0x00c5:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x0046
        L_0x00ca:
            r1 = move-exception
            r3 = r2
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0046
        L_0x00d1:
            r1 = move-exception
            r8 = r0
            r0 = r1
            r1 = r8
            r9 = r3
            r3 = r2
            r2 = r9
            goto L_0x0046
        L_0x00da:
            r1 = move-exception
            goto L_0x0090
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flad.b.a.a(java.lang.String, java.io.OutputStream):boolean");
    }

    public void a(String str) {
        c cVar = new c(this, str);
        com.flad.e.a.a();
        com.flad.e.a.a(cVar);
    }
}
