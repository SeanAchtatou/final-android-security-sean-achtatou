package com.flad.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.ImageView;
import com.flad.g.d;

class b extends Handler {
    final /* synthetic */ a a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(a aVar, Looper looper) {
        super(looper);
        this.a = aVar;
    }

    public void handleMessage(Message message) {
        d dVar = (d) message.obj;
        ImageView imageView = dVar.a;
        if (((String) imageView.getTag()).equals(dVar.b)) {
            imageView.setImageBitmap(dVar.c);
        } else {
            d.c("ImageLoader", "set image bitmap,but url has changed, ignored!");
        }
    }
}
