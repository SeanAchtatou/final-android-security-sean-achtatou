package com.flad.g;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Calendar;

public class c {
    private static final String s = SharedPreferences.class.getSimpleName();
    private static c t;
    public String a = "flad";
    public String b = "app_key";
    public String c = "get_ad_time";
    public String d = "is_get_ad";
    public String e = "Display_interval";
    public String f = "activation_interval";
    public String g = "ad_data";
    public String h = "is_do";
    public String i = "install";
    public String j = "show_count";
    public String k = "report";
    public String l = "active_reprot";
    public String m = "active";
    public String n = "install_pakg";
    public String o = "last_active";
    public String p = "install_id";
    public String q = "show_tiem";
    public String r = "ad_id";
    private Context u;
    private SharedPreferences v;

    private c(Context context) {
        this.u = context;
    }

    public static c a(Context context) {
        synchronized (c.class) {
            try {
                if (t == null) {
                    t = new c(context);
                }
            } catch (Throwable th) {
                while (true) {
                    Class<c> cls = c.class;
                    throw th;
                }
            }
        }
        return t;
    }

    public SharedPreferences a() {
        synchronized (t) {
            if (this.v == null) {
                this.v = this.u.getSharedPreferences(this.a, 0);
            }
        }
        return this.v;
    }

    public String a(String str) {
        return a().getString(str, "");
    }

    public void a(String str, int i2) {
        a().edit().putInt(str, i2).commit();
    }

    public void a(String str, long j2) {
        a().edit().putLong(str, j2).commit();
    }

    public void a(String str, String str2) {
        a().edit().putString(str, str2).commit();
    }

    public void a(String str, boolean z) {
        a().edit().putBoolean(str, z).commit();
    }

    public int b(String str, int i2) {
        return a().getInt(str, i2);
    }

    public long b(String str) {
        return a().getLong(str, 0);
    }

    public boolean b() {
        boolean z = false;
        synchronized (t) {
            if (a().getInt(this.c, 0) != Calendar.getInstance().get(6)) {
                z = true;
            }
        }
        return z;
    }

    public boolean b(String str, boolean z) {
        return a().getBoolean(str, z);
    }

    public String c() {
        return a().getString(this.b, "");
    }

    public int d() {
        return a().getInt(this.e, 300);
    }

    public boolean e() {
        SharedPreferences a2 = a();
        return System.currentTimeMillis() - a2.getLong(this.o, 0) > 1000 * ((long) a2.getInt(this.f, 0));
    }
}
