package com.flad.g;

import android.content.Context;
import android.os.Handler;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class b {
    private static b b;
    private AtomicBoolean a = new AtomicBoolean(false);
    private Handler c;
    private ScheduledExecutorService d;
    private long e = 0;

    private b(Context context) {
        this.c = new Handler(context.getMainLooper());
        this.d = Executors.newScheduledThreadPool(1);
    }

    public static b a(Context context) {
        if (b == null) {
            synchronized (b.class) {
                try {
                    if (b == null) {
                        b = new b(context);
                        b(context);
                    }
                } catch (Throwable th) {
                    while (true) {
                        Class<b> cls = b.class;
                        throw th;
                    }
                }
            }
        }
        return b;
    }

    private static void b(Context context) {
    }

    public void a(Runnable runnable, long j) {
        d.a("ScheduledPoolUtils", "call execute");
        synchronized (b) {
            d.a("ScheduledPoolUtils", "isExecute:" + this.a.get());
            if (!this.a.get()) {
                try {
                    if (this.d == null) {
                        this.d = Executors.newScheduledThreadPool(1);
                    }
                    d.a("ScheduledPoolUtils", "广告间隔时间:" + j);
                    d.a("ScheduledPoolUtils", "定时启动广告run:" + Thread.currentThread().getName());
                    this.d.scheduleAtFixedRate(runnable, this.e, j, TimeUnit.SECONDS);
                    this.a.getAndSet(true);
                } catch (Exception e2) {
                    d.a("ScheduledPoolUtils", "启动定时执行广告选择任务异常" + e2.getLocalizedMessage());
                    d.c("ScheduledPoolUtils", e2.getMessage());
                }
                return;
            }
            return;
        }
    }

    public boolean a() {
        boolean z;
        synchronized (this) {
            d.b("ScheduledPoolUtils", "is execute: " + this.a.get());
            z = this.a.get();
        }
        return z;
    }
}
