package com.flad.e;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.flad.g.d;
import com.flad.h.g;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class b implements Runnable {
    HashMap a;
    /* access modifiers changed from: private */
    public e b;
    private g c;
    private HttpUriRequest d;
    private List e;
    private HttpResponse f;
    private DefaultHttpClient g;
    private String h;
    private String i;
    private Handler j;
    /* access modifiers changed from: private */
    public String k = "";
    private Context l;

    public b(Context context, g gVar, List list, e eVar) {
        this.c = gVar;
        this.h = gVar.a().trim();
        this.e = list;
        this.b = eVar;
        this.j = new Handler(Looper.getMainLooper());
        this.g = new DefaultHttpClient();
        this.a = new HashMap();
        this.l = context;
    }

    private String a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                return byteArrayOutputStream.toString();
            }
            byteArrayOutputStream.write(read);
        }
    }

    private void a(String str) {
        if (this.b != null) {
            this.j.post(new d(this, str));
        }
    }

    /* access modifiers changed from: protected */
    public void a(HttpUriRequest httpUriRequest) {
        this.a.clear();
        this.a.put("Accept-Charset", "UTF-8,*");
        this.a.put("Accept-Encoding", "gzip");
        this.a.put("Cache-Control", "no-cache");
        this.a.put("user-agent", "umakesdk");
        if (httpUriRequest != null && this.a != null) {
            for (Map.Entry entry : this.a.entrySet()) {
                if (entry.getKey() != null) {
                    httpUriRequest.addHeader((String) entry.getKey(), (String) entry.getValue());
                }
            }
        }
    }

    public void run() {
        d.b("HttpRequest", "http request run");
        if (g.a(this.l) || this.b == null) {
            try {
                if (this.b != null) {
                    try {
                        List a2 = this.b.a();
                        if (a2 != null) {
                            this.e = a2;
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                if (this.c.b().equals("get")) {
                    Log.e("HttpRequest", "http request get");
                    StringBuilder sb = new StringBuilder();
                    if (this.e == null || this.e.size() <= 0) {
                        this.i = this.h;
                    } else {
                        for (f fVar : this.e) {
                            String a3 = fVar.a();
                            String b2 = fVar.b();
                            if (sb.length() == 0) {
                                sb.append("?" + a3 + "=" + URLEncoder.encode(b2, "UTF-8"));
                            } else {
                                sb.append("&" + a3 + "=" + URLEncoder.encode(b2, "utf-8"));
                            }
                        }
                        this.i = String.valueOf(this.h) + sb.toString();
                    }
                    d.a("HttpRequest", "get url:" + this.i);
                    this.d = new HttpGet(this.i);
                } else if (this.c.b().equals("post")) {
                    d.b("HttpRequest", "http request post");
                    this.d = new HttpPost(this.h);
                    if (this.e != null && this.e.size() > 0) {
                        ArrayList arrayList = new ArrayList();
                        if (this.e != null && this.e.size() > 0) {
                            d.a("HttpRequest", "POST 参数打印开始------------");
                            for (f fVar2 : this.e) {
                                String a4 = fVar2.a();
                                String b3 = fVar2.b();
                                d.a("HttpRequest", fVar2.toString());
                                arrayList.add(new BasicNameValuePair(a4, b3));
                            }
                            d.a("HttpRequest", "POST 参数打印结束------------");
                            this.d.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
                        }
                    }
                } else {
                    d.b("HttpRequest", "exit");
                    return;
                }
                d.a("HttpRequest", "http url ->" + this.h);
                this.d.getParams().setParameter("http.connection.timeout", 5000);
                this.d.getParams().setParameter("http.socket.timeout", 5000);
                a(this.d);
                this.f = this.g.execute(this.d);
                int statusCode = this.f.getStatusLine().getStatusCode();
                d.b("HttpRequest", "http response statusCode: " + statusCode);
                if (this.b == null) {
                    return;
                }
                if (statusCode == 200) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    if (this.f.getEntity() == null || this.f.getEntity().getContentEncoding() == null || this.f.getEntity().getContentEncoding().getValue() == null) {
                        this.f.getEntity().writeTo(byteArrayOutputStream);
                        this.k = new String(byteArrayOutputStream.toByteArray()).trim();
                        d.b("HttpRequest", "ContentEncoding 为null");
                    } else {
                        d.b("HttpRequest", "into getEntity");
                        if (this.f.getEntity().getContentEncoding().getValue().contains("gzip")) {
                            d.b("HttpRequest", "支持gzip");
                            InputStream content = this.f.getEntity().getContent();
                            GZIPInputStream gZIPInputStream = new GZIPInputStream(content);
                            this.k = a(gZIPInputStream);
                            gZIPInputStream.close();
                            content.close();
                        } else {
                            this.f.getEntity().writeTo(byteArrayOutputStream);
                            this.k = new String(byteArrayOutputStream.toByteArray()).trim();
                            d.b("HttpRequest", "不支持gzip");
                        }
                    }
                    this.b.c(this.k);
                    this.j.post(new c(this));
                } else if (statusCode == 204) {
                    this.k = "204";
                    this.b.a(this.k);
                } else {
                    d.b("HttpRequest", "网络错误");
                    a("响应码: " + statusCode);
                }
            } catch (Exception e3) {
                e3.printStackTrace();
                d.b("HttpRequest", "net request erroe" + e3.getMessage());
                a("netError:" + e3.getMessage());
            }
        } else {
            Log.e("HttpRequest", "network is not connect!");
            a("network is not connection.");
            this.b.c("network is not connection.");
        }
    }
}
