package com.flad.e;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class a {
    public static final int a = ((d * 2) + 1);
    static LinkedBlockingDeque b = new LinkedBlockingDeque();
    static ExecutorService c = new ThreadPoolExecutor(e, a, 10, TimeUnit.SECONDS, b, new ThreadPoolExecutor.DiscardOldestPolicy());
    private static final int d = Runtime.getRuntime().availableProcessors();
    private static final int e = (d + 1);
    private static a f = null;

    private a() {
    }

    public static a a() {
        a aVar;
        synchronized (a.class) {
            try {
                if (f == null) {
                    f = new a();
                }
                aVar = f;
            } catch (Throwable th) {
                Class<a> cls = a.class;
                throw th;
            }
        }
        return aVar;
    }

    public static void a(Runnable runnable) {
        if (c != null && runnable != null) {
            try {
                c.execute(runnable);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
