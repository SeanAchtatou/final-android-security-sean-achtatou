package com.flad.e;

public class f {
    public String a;
    public String b;

    public f(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public String toString() {
        return "RequestParameter [key=" + this.a + ", value=" + this.b + "]";
    }
}
