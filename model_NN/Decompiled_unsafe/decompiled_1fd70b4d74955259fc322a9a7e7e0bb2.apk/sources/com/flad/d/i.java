package com.flad.d;

import android.text.TextUtils;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.UUID;

public class i {
    private static String a() {
        return UUID.randomUUID().toString().trim().replaceAll("-", "");
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes("UTF-8"));
            return new BigInteger(1, instance.digest()).toString(16);
        } catch (Exception e) {
            return a();
        }
    }

    public static String a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("location");
        if (TextUtils.isEmpty(headerField)) {
            headerField = httpURLConnection.getHeaderField("Location");
        }
        try {
            new URL(headerField);
            return headerField;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return String.valueOf(httpURLConnection.getURL().getProtocol()) + "://" + httpURLConnection.getURL().getHost() + headerField;
        }
    }
}
