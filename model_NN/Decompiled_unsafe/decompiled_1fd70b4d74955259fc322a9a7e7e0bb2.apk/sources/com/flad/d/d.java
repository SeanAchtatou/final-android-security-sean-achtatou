package com.flad.d;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;

public class d {
    private static d b;
    private PriorityBlockingQueue a = new PriorityBlockingQueue(20);
    private Set c = new HashSet();
    private b[] d;
    private a e = new a();
    private String f;

    private d() {
    }

    public static d a() {
        if (b == null) {
            synchronized (d.class) {
                try {
                    if (b == null) {
                        b = new d();
                    }
                } catch (Throwable th) {
                    while (true) {
                        Class<d> cls = d.class;
                        throw th;
                    }
                }
            }
        }
        return b;
    }

    private void c() {
        if (this.d != null) {
            synchronized (this.d) {
                for (b bVar : this.d) {
                    if (bVar != null) {
                        bVar.b();
                    }
                }
            }
        }
    }

    public int a(e eVar) {
        if (this.d == null) {
            throw new RuntimeException("download queue have't init.");
        }
        int i = -1;
        if (a(eVar.e()) == null) {
            synchronized (this.c) {
                this.c.add(eVar);
                i = 1;
            }
            eVar.a(this);
            eVar.a(this.e);
            this.a.add(eVar);
            this.e.d(eVar);
        }
        return i;
    }

    public d a(int i) {
        c();
        if (this.d == null) {
            this.d = new b[i];
            for (int i2 = 0; i2 < this.d.length; i2++) {
                this.d[i2] = new b(this.a, this.e);
                this.d[i2].a(0.02f);
                this.d[i2].a();
            }
            return this;
        }
        throw new RuntimeException("download queue already init");
    }

    public e a(String str) {
        e eVar;
        synchronized (this.c) {
            Iterator it = this.c.iterator();
            while (true) {
                if (it.hasNext()) {
                    eVar = (e) it.next();
                    if (str.equals(eVar.e())) {
                        break;
                    }
                } else {
                    eVar = null;
                    break;
                }
            }
        }
        return eVar;
    }

    public String b() {
        return this.f;
    }

    public void b(e eVar) {
        synchronized (this.c) {
            this.c.remove(eVar);
        }
    }

    public void b(String str) {
        this.f = str;
    }
}
