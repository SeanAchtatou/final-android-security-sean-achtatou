package com.flad.d;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

public class e implements Comparable {
    private AtomicInteger a;
    private AtomicInteger b;
    private AtomicInteger c;
    private d d;
    private String e;
    private String f;
    private String g;
    private String h;
    private boolean i;
    private c j;
    private f k;
    private g l;
    private a m;
    private long n;
    private boolean o;
    private long p;

    public e(String str, String str2) {
        this(str, str2, null);
    }

    public e(String str, String str2, c cVar) {
        this.a = new AtomicInteger(3);
        this.b = new AtomicInteger(10);
        this.c = new AtomicInteger(5);
        this.o = false;
        this.e = str;
        this.f = str;
        this.j = cVar;
        this.k = f.Low;
        this.l = g.Waiting;
        this.g = str2 == null ? i.a(str) : str2;
    }

    /* renamed from: a */
    public int compareTo(e eVar) {
        if (this.k.ordinal() == eVar.k.ordinal()) {
            return 0;
        }
        return this.k.ordinal() - eVar.k.ordinal();
    }

    public e a(int i2) {
        this.a = new AtomicInteger(i2);
        return this;
    }

    public g a() {
        return this.l;
    }

    public void a(long j2) {
        this.n = j2;
    }

    public void a(a aVar) {
        this.m = aVar;
    }

    public void a(c cVar) {
        this.j = cVar;
    }

    public void a(d dVar) {
        this.d = dVar;
    }

    public void a(g gVar) {
        this.l = gVar;
    }

    public void a(String str) {
        this.e = str;
    }

    public c b() {
        return this.j;
    }

    public void b(long j2) {
        this.p = j2;
    }

    public void b(String str) {
        this.h = str;
    }

    public boolean c() {
        return this.i;
    }

    public void d() {
        if (this.d != null) {
            this.d.b(this);
        }
    }

    public String e() {
        return this.e;
    }

    public long f() {
        return k().length();
    }

    public int g() {
        this.o = true;
        return this.a.decrementAndGet();
    }

    public int h() {
        return this.b.decrementAndGet();
    }

    public String i() {
        return String.valueOf(this.h) + File.separator + i.a(e()) + ".tmp";
    }

    public String j() {
        return String.valueOf(this.h) + File.separator + this.g;
    }

    public File k() {
        return new File(i());
    }

    public void l() {
        if (k().exists()) {
            k().delete();
        }
    }

    public boolean m() {
        return this.o;
    }

    public int n() {
        return this.c.getAndDecrement();
    }

    public String toString() {
        return "DownloadRequest [mProgressiveRetryTimes=" + this.a + ", mTotalRetryTimes=" + this.b + ", mRedirectTimes=" + this.c + ", mQueue=" + this.d + ", mUrl=" + this.e + ", mOriginalUrl=" + this.f + ", mFileName=" + this.g + ", mBasePath=" + this.h + ", mCancle=" + this.i + ", mListener=" + this.j + ", mPriority=" + this.k + ", mState=" + this.l + ", mDelivery=" + this.m + ", totalSize=" + this.n + ", isRetry=" + this.o + ", downloadedSize=" + this.p + "]";
    }
}
