package com.flad.d;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.BlockingQueue;

class b implements Runnable {
    private float a = 0.02f;
    private BlockingQueue b;
    private Thread c;
    private a d;
    private volatile boolean e;

    b(BlockingQueue blockingQueue, a aVar) {
        this.b = blockingQueue;
        this.d = aVar;
        this.c = new Thread(this);
        this.c.setName(b.class.getSimpleName());
    }

    private int a(byte[] bArr, InputStream inputStream) {
        try {
            return inputStream.read(bArr);
        } catch (IOException e2) {
            e2.printStackTrace();
            return "unexpected end of stream".equals(e2.getMessage()) ? -1 : Integer.MAX_VALUE;
        }
    }

    private void a(e eVar) {
        eVar.a(g.Failed);
        this.d.a(eVar, 3, "download thread may be interrupted");
    }

    private void a(e eVar, int i, String str) {
        eVar.a(g.Failed);
        if (i != 1 || eVar.g() < 0 || eVar.h() < 0) {
            eVar.d();
            this.d.a(eVar, i, str);
            return;
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e2) {
            h.a(e2.getMessage());
            if (this.e) {
                eVar.d();
                return;
            }
        }
        h.b("retry download.");
        c(eVar);
    }

    private void a(e eVar, HttpURLConnection httpURLConnection) {
        eVar.a(g.Success);
        eVar.d();
        File k = eVar.k();
        if (k.exists()) {
            k.renameTo(new File(eVar.j()));
        }
        this.d.b(eVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007c, code lost:
        r3 = r15.k().length();
        r15.b(r3);
        r15.a(r3);
        e(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x008f, code lost:
        if (r3 != r7) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0091, code lost:
        a(r15, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        a(r15, 1, "file size error");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        a(r15, 1, "transfer error");
     */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:39:0x00b4=Splitter:B:39:0x00b4, B:57:0x0104=Splitter:B:57:0x0104, B:32:0x0096=Splitter:B:32:0x0096, B:48:0x00d7=Splitter:B:48:0x00d7} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.net.HttpURLConnection r14, com.flad.d.e r15) {
        /*
            r13 = this;
            int r0 = r14.getContentLength()
            long r0 = (long) r0
            java.lang.String r2 = "dispatcher"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "download file length: "
            r3.<init>(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            com.flad.g.d.b(r2, r3)
            java.io.File r3 = r15.k()
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x014a, SocketTimeoutException -> 0x0141, IOException -> 0x013a, Exception -> 0x0133, all -> 0x0126 }
            java.lang.String r4 = "rw"
            r2.<init>(r3, r4)     // Catch:{ FileNotFoundException -> 0x014a, SocketTimeoutException -> 0x0141, IOException -> 0x013a, Exception -> 0x0133, all -> 0x0126 }
            long r5 = r3.length()     // Catch:{ FileNotFoundException -> 0x014f, SocketTimeoutException -> 0x0146, IOException -> 0x013e, Exception -> 0x0137, all -> 0x0130 }
            long r7 = r0 + r5
            r0 = 0
            int r0 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0033
            r2.seek(r5)     // Catch:{ FileNotFoundException -> 0x014f, SocketTimeoutException -> 0x0146, IOException -> 0x013e, Exception -> 0x0137, all -> 0x0130 }
        L_0x0033:
            r15.b(r5)     // Catch:{ FileNotFoundException -> 0x014f, SocketTimeoutException -> 0x0146, IOException -> 0x013e, Exception -> 0x0137, all -> 0x0130 }
            r15.a(r7)     // Catch:{ FileNotFoundException -> 0x014f, SocketTimeoutException -> 0x0146, IOException -> 0x013e, Exception -> 0x0137, all -> 0x0130 }
            boolean r0 = r15.m()     // Catch:{ FileNotFoundException -> 0x014f, SocketTimeoutException -> 0x0146, IOException -> 0x013e, Exception -> 0x0137, all -> 0x0130 }
            if (r0 != 0) goto L_0x0042
            r13.d(r15)     // Catch:{ FileNotFoundException -> 0x014f, SocketTimeoutException -> 0x0146, IOException -> 0x013e, Exception -> 0x0137, all -> 0x0130 }
        L_0x0042:
            java.io.InputStream r1 = r14.getInputStream()     // Catch:{ FileNotFoundException -> 0x014f, SocketTimeoutException -> 0x0146, IOException -> 0x013e, Exception -> 0x0137, all -> 0x0130 }
            if (r1 == 0) goto L_0x006b
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r9 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            r3 = 0
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x0072
            float r0 = (float) r7     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            float r3 = r13.a     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            float r0 = r0 * r3
        L_0x0056:
            r4 = 0
            r3 = 0
        L_0x0058:
            java.lang.Thread r10 = java.lang.Thread.currentThread()     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            boolean r10 = r10.isInterrupted()     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            if (r10 != 0) goto L_0x0068
            boolean r10 = r15.c()     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            if (r10 == 0) goto L_0x0075
        L_0x0068:
            r15.d()     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
        L_0x006b:
            r13.a(r2)
            r13.a(r1)
        L_0x0071:
            return
        L_0x0072:
            int r0 = r9.length     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            float r0 = (float) r0     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            goto L_0x0056
        L_0x0075:
            int r10 = r13.a(r9, r1)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            r11 = -1
            if (r10 != r11) goto L_0x00ca
            java.io.File r0 = r15.k()     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            long r3 = r0.length()     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            r15.b(r3)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            r15.a(r3)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            r13.e(r15)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            int r0 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x00ac
            r13.a(r15, r14)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            goto L_0x006b
        L_0x0095:
            r0 = move-exception
        L_0x0096:
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0153 }
            com.flad.d.h.b(r3)     // Catch:{ all -> 0x0153 }
            r3 = 4
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0153 }
            r13.a(r15, r3, r0)     // Catch:{ all -> 0x0153 }
            r13.a(r2)
            r13.a(r1)
            goto L_0x0071
        L_0x00ac:
            r0 = 1
            java.lang.String r3 = "file size error"
            r13.a(r15, r0, r3)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            goto L_0x006b
        L_0x00b3:
            r0 = move-exception
        L_0x00b4:
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0153 }
            com.flad.d.h.b(r3)     // Catch:{ all -> 0x0153 }
            r3 = 1
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0153 }
            r13.a(r15, r3, r0)     // Catch:{ all -> 0x0153 }
            r13.a(r2)
            r13.a(r1)
            goto L_0x0071
        L_0x00ca:
            r11 = 2147483647(0x7fffffff, float:NaN)
            if (r10 != r11) goto L_0x00ed
            r0 = 1
            java.lang.String r3 = "transfer error"
            r13.a(r15, r0, r3)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            goto L_0x006b
        L_0x00d6:
            r0 = move-exception
        L_0x00d7:
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0153 }
            com.flad.d.h.b(r3)     // Catch:{ all -> 0x0153 }
            r3 = 4
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0153 }
            r13.a(r15, r3, r0)     // Catch:{ all -> 0x0153 }
            r13.a(r2)
            r13.a(r1)
            goto L_0x0071
        L_0x00ed:
            r11 = 3
            r15.a(r11)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            long r11 = (long) r10     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            long r5 = r5 + r11
            r11 = 0
            r2.write(r9, r11, r10)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            r15.b(r5)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            int r4 = r4 + r10
            if (r3 != 0) goto L_0x011b
            r3 = 1
            r13.e(r15)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            goto L_0x0058
        L_0x0103:
            r0 = move-exception
        L_0x0104:
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0153 }
            com.flad.d.h.b(r3)     // Catch:{ all -> 0x0153 }
            r3 = 0
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0153 }
            r13.a(r15, r3, r0)     // Catch:{ all -> 0x0153 }
            r13.a(r2)
            r13.a(r1)
            goto L_0x0071
        L_0x011b:
            float r10 = (float) r4
            int r10 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r10 < 0) goto L_0x0058
            r13.e(r15)     // Catch:{ FileNotFoundException -> 0x0095, SocketTimeoutException -> 0x00b3, IOException -> 0x00d6, Exception -> 0x0103 }
            r4 = 0
            goto L_0x0058
        L_0x0126:
            r0 = move-exception
            r2 = 0
            r1 = 0
        L_0x0129:
            r13.a(r2)
            r13.a(r1)
            throw r0
        L_0x0130:
            r0 = move-exception
            r1 = 0
            goto L_0x0129
        L_0x0133:
            r0 = move-exception
            r2 = 0
            r1 = 0
            goto L_0x0104
        L_0x0137:
            r0 = move-exception
            r1 = 0
            goto L_0x0104
        L_0x013a:
            r0 = move-exception
            r2 = 0
            r1 = 0
            goto L_0x00d7
        L_0x013e:
            r0 = move-exception
            r1 = 0
            goto L_0x00d7
        L_0x0141:
            r0 = move-exception
            r2 = 0
            r1 = 0
            goto L_0x00b4
        L_0x0146:
            r0 = move-exception
            r1 = 0
            goto L_0x00b4
        L_0x014a:
            r0 = move-exception
            r2 = 0
            r1 = 0
            goto L_0x0096
        L_0x014f:
            r0 = move-exception
            r1 = 0
            goto L_0x0096
        L_0x0153:
            r0 = move-exception
            goto L_0x0129
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flad.d.b.a(java.net.HttpURLConnection, com.flad.d.e):void");
    }

    private HttpURLConnection b(e eVar) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(eVar.e()).openConnection();
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
        httpURLConnection.setConnectTimeout(20000);
        httpURLConnection.setReadTimeout(20000);
        long f = eVar.f();
        if (f > 0) {
            httpURLConnection.setRequestProperty("Range", "bytes=" + f + "-");
            eVar.b(f);
        }
        return httpURLConnection;
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0015  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c(com.flad.d.e r4) {
        /*
            r3 = this;
            r1 = 0
            java.net.HttpURLConnection r1 = r3.b(r4)     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
            int r0 = r1.getResponseCode()     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
            switch(r0) {
                case 200: goto L_0x0019;
                case 206: goto L_0x001c;
                case 301: goto L_0x002f;
                case 302: goto L_0x002f;
                case 303: goto L_0x002f;
                case 307: goto L_0x002f;
                case 416: goto L_0x005f;
                default: goto L_0x000c;
            }     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
        L_0x000c:
            java.lang.String r2 = r1.getResponseMessage()     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
            r3.a(r4, r0, r2)     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
        L_0x0013:
            if (r1 == 0) goto L_0x0018
            r1.disconnect()
        L_0x0018:
            return
        L_0x0019:
            r4.l()     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
        L_0x001c:
            r3.a(r1, r4)     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
            goto L_0x0013
        L_0x0020:
            r0 = move-exception
            r2 = 2
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0072 }
            r3.a(r4, r2, r0)     // Catch:{ all -> 0x0072 }
            if (r1 == 0) goto L_0x0018
            r1.disconnect()
            goto L_0x0018
        L_0x002f:
            int r2 = r4.n()     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
            if (r2 < 0) goto L_0x004a
            java.lang.String r0 = com.flad.d.i.a(r1)     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
            r4.a(r0)     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
            r3.c(r4)     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
            goto L_0x0013
        L_0x0040:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0072 }
            if (r1 == 0) goto L_0x0018
            r1.disconnect()
            goto L_0x0018
        L_0x004a:
            java.lang.String r2 = "redirect too many times"
            r3.a(r4, r0, r2)     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
            goto L_0x0013
        L_0x0050:
            r0 = move-exception
            r2 = 1
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0072 }
            r3.a(r4, r2, r0)     // Catch:{ all -> 0x0072 }
            if (r1 == 0) goto L_0x0018
            r1.disconnect()
            goto L_0x0018
        L_0x005f:
            r4.l()     // Catch:{ MalformedURLException -> 0x0020, UnknownHostException -> 0x0040, IOException -> 0x0050, Exception -> 0x0063 }
            goto L_0x000c
        L_0x0063:
            r0 = move-exception
            r2 = 0
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0072 }
            r3.a(r4, r2, r0)     // Catch:{ all -> 0x0072 }
            if (r1 == 0) goto L_0x0018
            r1.disconnect()
            goto L_0x0018
        L_0x0072:
            r0 = move-exception
            if (r1 == 0) goto L_0x0078
            r1.disconnect()
        L_0x0078:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flad.d.b.c(com.flad.d.e):void");
    }

    private void d(e eVar) {
        if (!eVar.c()) {
            eVar.a(g.Running);
            this.d.c(eVar);
        }
    }

    private void e(e eVar) {
        eVar.a(g.Running);
        if (!eVar.c()) {
            this.d.a(eVar);
        }
    }

    public void a() {
        this.e = false;
        this.c.start();
    }

    public void a(float f) {
        if (f > 1.0f || f <= 0.0f) {
            throw new IllegalArgumentException("intervalDownloadPercent must be between 0 to 1");
        }
        this.a = f;
    }

    public void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void b() {
        this.e = true;
        this.c.interrupt();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r4 = this;
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r1 = 10
            r0.setPriority(r1)
            r0 = 0
            r1 = r0
        L_0x000b:
            boolean r0 = r4.e
            if (r0 == 0) goto L_0x0010
            return
        L_0x0010:
            java.util.concurrent.BlockingQueue r0 = r4.b     // Catch:{ InterruptedException -> 0x001d }
            java.lang.Object r0 = r0.take()     // Catch:{ InterruptedException -> 0x001d }
            com.flad.d.e r0 = (com.flad.d.e) r0     // Catch:{ InterruptedException -> 0x001d }
            r4.c(r0)     // Catch:{ InterruptedException -> 0x003e }
            r1 = r0
            goto L_0x000b
        L_0x001d:
            r0 = move-exception
            r3 = r0
            r0 = r1
            r1 = r3
        L_0x0021:
            r1.printStackTrace()
            boolean r2 = r4.e
            if (r0 == 0) goto L_0x003c
            r1 = 1
        L_0x0029:
            r1 = r1 & r2
            if (r1 == 0) goto L_0x003a
            r0.d()
            com.flad.d.g r1 = r0.a()
            com.flad.d.g r2 = com.flad.d.g.Success
            if (r1 == r2) goto L_0x003a
            r4.a(r0)
        L_0x003a:
            r1 = r0
            goto L_0x000b
        L_0x003c:
            r1 = 0
            goto L_0x0029
        L_0x003e:
            r1 = move-exception
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flad.d.b.run():void");
    }
}
