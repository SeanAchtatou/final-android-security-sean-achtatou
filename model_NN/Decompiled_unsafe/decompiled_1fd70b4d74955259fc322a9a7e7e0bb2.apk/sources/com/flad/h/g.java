package com.flad.h;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.io.File;

public class g {
    private static String a = "";
    private static String b = "";
    private static String c = "";

    public static void a(Context context, File file) {
        Uri fromFile = Uri.fromFile(file);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
        intent.setFlags(268435456);
        context.startActivity(intent);
    }

    public static boolean a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            return activeNetworkInfo.isAvailable();
        }
        return false;
    }

    public static boolean a(Context context, String str) {
        if (context == null || str == null) {
            return false;
        }
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            for (String equals : strArr) {
                if (str.equals(equals)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String b(Context context) {
        if (context != null) {
            try {
                if (TextUtils.isEmpty(a)) {
                    a = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
                    if (a == null || a.trim().length() == 0 || a.matches("0+")) {
                        a = "000000000000000";
                    }
                }
            } catch (Exception e) {
                a = "000000000000000";
            }
        }
        return a;
    }
}
