package com.flad.h;

import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class e {
    private static final String a = e.class.getSimpleName();

    public static File a() {
        try {
            File file = new File(String.valueOf(Environment.getExternalStorageDirectory().getPath()) + "/Android/data/yd");
            if (file.exists() || file.mkdirs()) {
                return file;
            }
            a(file);
            return file;
        } catch (Exception e) {
            Log.e(a, e.getLocalizedMessage());
            return null;
        }
    }

    public static File a(String str) {
        File a2 = a();
        if (a2 != null) {
            return new File(a2, str);
        }
        return null;
    }

    private static void a(File file) {
        File file2 = new File(String.valueOf(file.getAbsolutePath()) + System.currentTimeMillis());
        file.renameTo(file2);
        file2.delete();
    }

    private static byte[] a(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            return instance.digest();
        } catch (NoSuchAlgorithmException e) {
            Log.e(a, e.getLocalizedMessage());
            return null;
        }
    }

    public static String b(String str) {
        String d = d(str);
        return TextUtils.isEmpty(d) ? c(str) : d;
    }

    private static String c(String str) {
        return String.valueOf(str.hashCode());
    }

    private static String d(String str) {
        try {
            return new BigInteger(a(str.getBytes())).abs().toString(36);
        } catch (Exception e) {
            return null;
        }
    }
}
