package com.flad.h;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import com.flad.c.a;
import com.flad.d.d;
import com.flad.d.e;
import java.io.File;

public class b {
    private static b c;
    d a = d.a().a(3);
    e b;

    private b() {
        this.a.b(e.a().getAbsolutePath());
    }

    public static b a() {
        if (c == null) {
            c = new b();
        }
        return c;
    }

    public static boolean a(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            return activeNetworkInfo.isAvailable();
        }
        return false;
    }

    public void a(Context context, String str, boolean z) {
        File a2 = e.a(String.valueOf(e.b(str)) + ".apk");
        if (a2 == null || !a2.exists()) {
            if (!a(context)) {
                return;
            }
            if (TextUtils.isEmpty(str)) {
                com.flad.g.d.b("DownLoadUtils", "download url data can't download，downurl=null");
                return;
            }
            String str2 = String.valueOf(e.b(str)) + ".apk";
            c cVar = new c(this, a2, z, context);
            this.b = this.a.a(str);
            if (this.b == null) {
                this.b = new e(str, str2);
                this.b.b(this.a.b());
                this.b.a(5);
                this.a.a(this.b);
                this.b.a(cVar);
            }
        } else if (z) {
            a.a(context).a();
        }
    }
}
