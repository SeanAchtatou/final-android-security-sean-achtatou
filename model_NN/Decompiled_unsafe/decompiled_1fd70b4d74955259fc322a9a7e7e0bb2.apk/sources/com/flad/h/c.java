package com.flad.h;

import android.content.Context;
import android.os.Handler;
import com.flad.d.e;
import com.flad.g.d;
import java.io.File;

class c implements com.flad.d.c {
    final /* synthetic */ b a;
    private final /* synthetic */ File b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ Context d;

    c(b bVar, File file, boolean z, Context context) {
        this.a = bVar;
        this.b = file;
        this.c = z;
        this.d = context;
    }

    public void a(e eVar) {
        new com.flad.f.c(this.d).a("down_induce_start");
    }

    public void b(e eVar) {
    }

    public void c(e eVar) {
    }

    public void d(e eVar) {
        d.b("DownLoadUtils", "download onSuccess");
        if (this.b.exists() && this.c) {
            new Handler(this.d.getMainLooper()).post(new d(this, this.d));
        }
        new com.flad.f.c(this.d).a("down_induce_success");
    }

    public void e(e eVar) {
    }
}
