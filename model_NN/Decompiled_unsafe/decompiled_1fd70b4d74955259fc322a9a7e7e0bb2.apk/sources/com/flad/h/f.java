package com.flad.h;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import java.io.File;

public class f {
    private static final String a = f.class.getSimpleName();

    public static Bitmap a(File file) {
        if (file != null) {
            try {
                if (file.exists()) {
                    String absolutePath = file.getAbsolutePath();
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= 2) {
                            break;
                        }
                        try {
                            options.inSampleSize = i2 + 1;
                            return BitmapFactory.decodeFile(absolutePath, options);
                        } catch (OutOfMemoryError e) {
                            Log.e(a, e.getLocalizedMessage());
                            System.gc();
                            i = i2 + 1;
                        }
                    }
                }
            } catch (Exception e2) {
                Log.e(a, e2.getLocalizedMessage());
            }
        }
        return null;
    }
}
