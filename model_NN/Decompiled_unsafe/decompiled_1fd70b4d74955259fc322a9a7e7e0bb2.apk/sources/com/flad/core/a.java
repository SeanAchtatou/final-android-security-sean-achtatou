package com.flad.core;

import android.content.BroadcastReceiver;

public class a extends BroadcastReceiver {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flad.g.c.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.flad.g.c.b(java.lang.String, int):int
      com.flad.g.c.b(java.lang.String, boolean):boolean */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        r0 = r7.getString("adid");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x012e, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x013b, code lost:
        r1 = r0;
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x013f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        r2 = r0;
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r11, android.content.Intent r12) {
        /*
            r10 = this;
            r1 = 0
            r3 = 1
            r7 = 6
            r2 = 0
            com.flad.g.c r4 = com.flad.g.c.a(r11)
            if (r12 != 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            java.lang.String r0 = "ScreenReceiver"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "onReceive: "
            r5.<init>(r6)
            java.lang.String r6 = r12.getAction()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.flad.g.d.b(r0, r5)
            java.lang.String r0 = "android.intent.action.SCREEN_OFF"
            java.lang.String r5 = r12.getAction()
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x0039
            com.flad.core.r.screenIsOn = r2
            java.lang.String r0 = "ScreenReceiver"
            java.lang.String r1 = "screen is off "
            com.flad.g.d.b(r0, r1)
            goto L_0x000a
        L_0x0039:
            java.lang.String r0 = "android.intent.action.SCREEN_ON"
            java.lang.String r5 = r12.getAction()
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x004f
            com.flad.core.r.screenIsOn = r3
            java.lang.String r0 = "ScreenReceiver"
            java.lang.String r1 = "screen is on "
            com.flad.g.d.b(r0, r1)
            goto L_0x000a
        L_0x004f:
            java.lang.String r0 = "android.intent.action.USER_PRESENT"
            java.lang.String r5 = r12.getAction()
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x00aa
            java.lang.String r0 = r4.i
            int r0 = r4.b(r0, r2)
            if (r0 != r7) goto L_0x0077
            java.lang.String r0 = r4.k
            int r0 = r4.b(r0, r2)
            r1 = 9
            if (r0 == r1) goto L_0x0077
            com.flad.f.c r0 = new com.flad.f.c
            r0.<init>(r11)
            java.lang.String r1 = "install_ad_succ"
            r0.a(r1)
        L_0x0077:
            java.lang.String r0 = r4.m
            boolean r0 = r4.b(r0, r2)
            if (r0 == 0) goto L_0x0091
            java.lang.String r0 = r4.l
            boolean r0 = r4.b(r0, r2)
            if (r0 != 0) goto L_0x0091
            com.flad.f.c r0 = new com.flad.f.c
            r0.<init>(r11)
            java.lang.String r1 = "active_sdk"
            r0.a(r1)
        L_0x0091:
            java.lang.String r0 = r4.i
            int r0 = r4.b(r0, r2)
            if (r0 != r7) goto L_0x000a
            boolean r0 = r4.e()
            if (r0 == 0) goto L_0x000a
            java.lang.String r0 = r4.n
            java.lang.String r0 = r4.a(r0)
            com.flad.g.a.a(r11, r0)
            goto L_0x000a
        L_0x00aa:
            java.lang.String r0 = "android.intent.action.PACKAGE_ADDED"
            java.lang.String r5 = r12.getAction()
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x000a
            java.lang.String r0 = r12.getDataString()     // Catch:{ Exception -> 0x0116 }
            java.lang.String r5 = ":"
            java.lang.String[] r0 = r0.split(r5)     // Catch:{ Exception -> 0x0116 }
            if (r0 == 0) goto L_0x000a
            int r5 = r0.length     // Catch:{ Exception -> 0x0116 }
            if (r5 <= r3) goto L_0x000a
            r5 = 1
            r5 = r0[r5]     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = r4.g     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = r4.a(r0)     // Catch:{ Exception -> 0x0116 }
            org.json.JSONArray r6 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0134 }
            r6.<init>(r0)     // Catch:{ JSONException -> 0x0134 }
            r0 = r2
        L_0x00d4:
            int r7 = r6.length()     // Catch:{ JSONException -> 0x0134 }
            if (r0 < r7) goto L_0x0119
            r0 = r1
            r1 = r2
        L_0x00dc:
            boolean r2 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x0116 }
            if (r2 != 0) goto L_0x000a
            if (r1 == 0) goto L_0x000a
            java.lang.String r1 = "ScreenReceiver"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0116 }
            java.lang.String r3 = "instal->"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0116 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0116 }
            com.flad.g.d.b(r1, r2)     // Catch:{ Exception -> 0x0116 }
            com.flad.g.a.a(r11, r5)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r1 = r4.i     // Catch:{ Exception -> 0x0116 }
            r2 = 6
            r4.a(r1, r2)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r1 = r4.n     // Catch:{ Exception -> 0x0116 }
            r4.a(r1, r5)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r1 = r4.p     // Catch:{ Exception -> 0x0116 }
            r4.a(r1, r0)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r0 = r4.o     // Catch:{ Exception -> 0x0116 }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0116 }
            r4.a(r0, r1)     // Catch:{ Exception -> 0x0116 }
            goto L_0x000a
        L_0x0116:
            r0 = move-exception
            goto L_0x000a
        L_0x0119:
            org.json.JSONObject r7 = r6.getJSONObject(r0)     // Catch:{ JSONException -> 0x0134 }
            java.lang.String r8 = "package_name"
            java.lang.String r8 = r7.getString(r8)     // Catch:{ JSONException -> 0x0134 }
            boolean r8 = r5.equals(r8)     // Catch:{ JSONException -> 0x0134 }
            if (r8 == 0) goto L_0x0131
            java.lang.String r0 = "adid"
            java.lang.String r0 = r7.getString(r0)     // Catch:{ JSONException -> 0x013f }
            r1 = r3
            goto L_0x00dc
        L_0x0131:
            int r0 = r0 + 1
            goto L_0x00d4
        L_0x0134:
            r0 = move-exception
            r9 = r0
            r0 = r2
            r2 = r9
        L_0x0138:
            r2.printStackTrace()     // Catch:{ Exception -> 0x0116 }
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00dc
        L_0x013f:
            r0 = move-exception
            r2 = r0
            r0 = r3
            goto L_0x0138
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flad.core.a.onReceive(android.content.Context, android.content.Intent):void");
    }
}
