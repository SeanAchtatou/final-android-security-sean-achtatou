package com.flad.core;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.IntentFilter;
import com.flad.f.a;
import com.flad.g.b;
import com.flad.g.c;

@TargetApi(14)
public class r {
    /* access modifiers changed from: private */
    public static Context mContext;
    static c mPreference;
    private static a mReceiver;
    public static volatile boolean screenIsOn = true;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flad.g.c.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.flad.g.c.a(java.lang.String, int):void
      com.flad.g.c.a(java.lang.String, long):void
      com.flad.g.c.a(java.lang.String, java.lang.String):void
      com.flad.g.c.a(java.lang.String, boolean):void */
    public static void a(Context context, String str) {
        mContext = context;
        mPreference = c.a(context);
        mPreference.a(mPreference.b, str);
        mPreference.a(mPreference.m, true);
        registerReceiver();
        doAd(str);
    }

    /* access modifiers changed from: private */
    public static boolean checkOpen() {
        return mPreference.a(mPreference.h).equals("1");
    }

    /* access modifiers changed from: private */
    public static boolean checkShow() {
        return System.currentTimeMillis() - mPreference.b(mPreference.q) > ((long) (mPreference.d() * 1000));
    }

    private static void doAd(String str) {
        if (mPreference.b()) {
            new a(mContext).a(str);
        }
        startAd();
    }

    private static void registerReceiver() {
        if (mReceiver == null) {
            mReceiver = new a();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            IntentFilter intentFilter2 = new IntentFilter();
            intentFilter2.addAction("android.intent.action.PACKAGE_ADDED");
            intentFilter2.addDataScheme("package");
            mContext.getApplicationContext().registerReceiver(mReceiver, intentFilter);
            mContext.getApplicationContext().registerReceiver(mReceiver, intentFilter2);
        }
    }

    private static void startAd() {
        if (!b.a(mContext).a()) {
            b.a(mContext).a(new c(), 30);
        }
    }
}
