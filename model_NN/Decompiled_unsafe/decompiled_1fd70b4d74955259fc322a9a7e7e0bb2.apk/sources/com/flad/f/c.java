package com.flad.f;

import android.content.Context;
import android.provider.Settings;
import com.flad.e.a;
import com.flad.e.b;
import com.flad.e.f;
import com.flad.e.g;
import java.util.ArrayList;

public class c {
    String a = "http://report.imokbang.com/report.do";
    /* access modifiers changed from: private */
    public com.flad.g.c b;
    private Context c;

    public c(Context context) {
        this.c = context;
        this.b = com.flad.g.c.a(context);
    }

    public String a() {
        return Settings.Secure.getString(this.c.getContentResolver(), "android_id");
    }

    public void a(String str) {
        f fVar;
        a.a();
        g gVar = new g();
        gVar.b("post");
        gVar.a(this.a);
        ArrayList arrayList = new ArrayList();
        f fVar2 = new f("event", str);
        f fVar3 = new f("r", "this is a test demo");
        f fVar4 = new f("k", "65537");
        f fVar5 = new f("uid", a());
        f fVar6 = new f("sv", "US004");
        f fVar7 = new f("kv", "20001_c");
        f fVar8 = new f("appkey", this.b.c());
        f fVar9 = new f("platform", "1");
        if (str.equals("install_ad_succ")) {
            fVar = new f("pname", this.b.a(this.b.n));
            arrayList.add(new f("adid", this.b.a(this.b.p)));
        } else if (str.equals("active_sdk")) {
            fVar = new f("pname", this.c.getPackageName());
        } else {
            fVar = new f("pname", this.c.getPackageName());
            arrayList.add(new f("adid", this.b.a(this.b.r)));
        }
        f fVar10 = new f("mod", "103330636190245379280366631103191948937734961051393247131715385665213115380281");
        f fVar11 = new f("imei", com.flad.h.g.b(this.c));
        arrayList.add(fVar2);
        arrayList.add(fVar3);
        arrayList.add(fVar4);
        arrayList.add(fVar5);
        arrayList.add(fVar6);
        arrayList.add(fVar7);
        arrayList.add(fVar8);
        arrayList.add(fVar9);
        arrayList.add(fVar);
        arrayList.add(fVar10);
        arrayList.add(fVar11);
        a.a(new b(this.c, gVar, arrayList, new d(this, str)));
    }
}
