package com.flad.f;

import android.text.TextUtils;
import com.flad.b.a;
import com.flad.e.e;
import com.flad.g.d;
import java.util.Calendar;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class b implements e {
    final /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    public List a() {
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flad.g.c.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.flad.g.c.a(java.lang.String, int):void
      com.flad.g.c.a(java.lang.String, long):void
      com.flad.g.c.a(java.lang.String, java.lang.String):void
      com.flad.g.c.a(java.lang.String, boolean):void */
    public void a(String str) {
        d.b("Adrequest", str);
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.getString("status").equals("S0001")) {
                    JSONObject jSONObject2 = new JSONObject(jSONObject.getJSONObject("data").getString("New_Parameter_SDK"));
                    String string = jSONObject2.getString("Display_interval");
                    String string2 = jSONObject2.getString("switch");
                    String string3 = jSONObject2.getString("Activation_interval");
                    this.a.b.a(this.a.b.e, Integer.valueOf(string).intValue());
                    this.a.b.a(this.a.b.h, string2);
                    this.a.b.a(this.a.b.f, Integer.valueOf(string3).intValue());
                    JSONArray jSONArray = jSONObject2.getJSONArray("ad_url");
                    this.a.b.a(this.a.b.g, jSONArray.toString());
                    for (int i = 0; i < jSONArray.length(); i++) {
                        JSONObject jSONObject3 = jSONArray.getJSONObject(i);
                        String string4 = jSONObject3.getString("apk");
                        String string5 = jSONObject3.getString("img");
                        a.a(com.flad.h.e.a().toString(), String.valueOf(com.flad.h.e.b(string5)) + ".png").a(string5);
                        if (i == 0) {
                            com.flad.h.b.a().a(this.a.c, string4, true);
                        } else {
                            com.flad.h.b.a().a(this.a.c, string4, false);
                        }
                        this.a.b.a(this.a.b.r, jSONObject3.getString("adid"));
                    }
                }
                this.a.b.a(this.a.b.d, true);
                this.a.b.a(this.a.b.c, Calendar.getInstance().get(6));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void b(String str) {
        d.b("Adrequest", str);
    }

    public void c(String str) {
    }
}
