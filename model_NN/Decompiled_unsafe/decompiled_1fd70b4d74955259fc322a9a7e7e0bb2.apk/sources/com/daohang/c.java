package com.daohang;

class c implements Runnable {
    final /* synthetic */ b a;
    private final /* synthetic */ String b;
    private final /* synthetic */ long c;
    private final /* synthetic */ int d;
    private final /* synthetic */ String e;

    c(b bVar, String str, long j, int i, String str2) {
        this.a = bVar;
        this.b = str;
        this.c = j;
        this.d = i;
        this.e = str2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:111:0x05ba, code lost:
        if (r0.intValue() == r9) goto L_0x05d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x05be, code lost:
        if (com.daohang.DataApp.c <= 0) goto L_0x05d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x05c0, code lost:
        com.daohang.k.c("limit:" + r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x05dc, code lost:
        if (r2 < r13.a.d.size()) goto L_0x063b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x05de, code lost:
        r4 = r0;
        r5 = false;
        r3 = r2;
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x063b, code lost:
        r4 = r0;
        r5 = false;
        r3 = r2;
        r2 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r13 = this;
            r0 = 0
            com.daohang.DataApp.c = r0
            java.lang.String r1 = ""
            r6 = 0
            r5 = 1
            int r2 = android.os.Build.VERSION.SDK_INT
            r0 = 30
            r3 = 9
            if (r2 == r3) goto L_0x0013
            r3 = 10
            if (r2 != r3) goto L_0x0648
        L_0x0013:
            r0 = 50
            r9 = r0
        L_0x0016:
            android.content.Context r0 = com.daohang.DataApp.a()     // Catch:{ Exception -> 0x011f }
            com.daohang.ClockReceiver.a(r0)     // Catch:{ Exception -> 0x011f }
            long r2 = r13.c     // Catch:{ Exception -> 0x011f }
            r10 = 0
            int r0 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x002a
            long r2 = r13.c     // Catch:{ Exception -> 0x011f }
            android.os.SystemClock.sleep(r2)     // Catch:{ Exception -> 0x011f }
        L_0x002a:
            r0 = 0
            java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x011f }
            com.daohang.b r0 = r13.a     // Catch:{ Exception -> 0x0114 }
            r0.a()     // Catch:{ Exception -> 0x0114 }
        L_0x0033:
            com.daohang.b r0 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r0 = r0.c     // Catch:{ Exception -> 0x011f }
            int r0 = r0.size()     // Catch:{ Exception -> 0x011f }
            if (r0 > 0) goto L_0x0044
            com.daohang.b r0 = r13.a     // Catch:{ Exception -> 0x011f }
            r0.b()     // Catch:{ Exception -> 0x011f }
        L_0x0044:
            java.lang.Boolean r0 = com.daohang.k.a     // Catch:{ Exception -> 0x011f }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x011f }
            if (r0 == 0) goto L_0x0052
            r0 = 5554(0x15b2, float:7.783E-42)
        L_0x004e:
            r2 = 5642(0x160a, float:7.906E-42)
            if (r0 < r2) goto L_0x01be
        L_0x0052:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011f }
            java.lang.String r2 = "All:"
            r0.<init>(r2)     // Catch:{ Exception -> 0x011f }
            com.daohang.b r2 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r2 = r2.c     // Catch:{ Exception -> 0x011f }
            int r2 = r2.size()     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x011f }
            java.lang.String r2 = "_Next "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x011f }
            int r2 = r13.d     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x011f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x011f }
            com.daohang.k.c(r0)     // Catch:{ Exception -> 0x011f }
            int r4 = r13.d     // Catch:{ Exception -> 0x011f }
            r2 = 1
        L_0x007d:
            if (r2 != 0) goto L_0x01fa
            com.daohang.b r0 = r13.a
            android.content.Context r0 = r0.a
            java.lang.String r1 = "setting"
            r2 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)
            android.content.SharedPreferences$Editor r1 = r0.edit()
            java.lang.String r2 = "pos"
            r3 = 0
            android.content.SharedPreferences$Editor r1 = r1.putInt(r2, r3)
            r1.commit()
            android.content.SharedPreferences$Editor r0 = r0.edit()
            java.lang.String r1 = "time"
            r2 = 0
            android.content.SharedPreferences$Editor r0 = r0.putInt(r1, r2)
            r0.commit()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "OK_"
            r0.<init>(r1)
            com.daohang.b r1 = r13.a
            java.util.ArrayList r1 = r1.c
            int r1 = r1.size()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r13.b
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.daohang.k.d(r0)
            java.lang.String r0 = ""
            com.daohang.b r1 = r13.a     // Catch:{ Exception -> 0x0635 }
            java.util.ArrayList r1 = r1.c     // Catch:{ Exception -> 0x0635 }
            int r1 = r1.size()     // Catch:{ Exception -> 0x0635 }
            java.lang.String r0 = java.lang.Integer.toString(r1)     // Catch:{ Exception -> 0x0635 }
        L_0x00de:
            com.daohang.k r1 = new com.daohang.k
            r1.<init>()
            r1.a()
            java.lang.String r2 = "30"
            r1.d = r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "finish:"
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = " - "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r13.b
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.c = r0
            r0 = 5
            r1.a(r0)
            r0 = 10000(0x2710, double:4.9407E-320)
            android.os.SystemClock.sleep(r0)
            com.daohang.ClockReceiver.a()
        L_0x0113:
            return
        L_0x0114:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x011f }
            java.lang.String r0 = "con2"
            com.daohang.k.c(r0)     // Catch:{ Exception -> 0x011f }
            goto L_0x0033
        L_0x011f:
            r0 = move-exception
            java.lang.String r1 = "errsendk"
            com.daohang.k.c(r1)     // Catch:{ all -> 0x0307 }
            r0.printStackTrace()     // Catch:{ all -> 0x0307 }
            com.daohang.b r0 = r13.a
            android.content.Context r0 = r0.a
            java.lang.String r1 = "setting"
            r2 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)
            android.content.SharedPreferences$Editor r1 = r0.edit()
            java.lang.String r2 = "pos"
            r3 = 0
            android.content.SharedPreferences$Editor r1 = r1.putInt(r2, r3)
            r1.commit()
            android.content.SharedPreferences$Editor r0 = r0.edit()
            java.lang.String r1 = "time"
            r2 = 0
            android.content.SharedPreferences$Editor r0 = r0.putInt(r1, r2)
            r0.commit()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "OK_"
            r0.<init>(r1)
            com.daohang.b r1 = r13.a
            java.util.ArrayList r1 = r1.c
            int r1 = r1.size()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r13.b
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.daohang.k.d(r0)
            java.lang.String r0 = ""
            com.daohang.b r1 = r13.a     // Catch:{ Exception -> 0x0629 }
            java.util.ArrayList r1 = r1.c     // Catch:{ Exception -> 0x0629 }
            int r1 = r1.size()     // Catch:{ Exception -> 0x0629 }
            java.lang.String r0 = java.lang.Integer.toString(r1)     // Catch:{ Exception -> 0x0629 }
        L_0x0187:
            com.daohang.k r1 = new com.daohang.k
            r1.<init>()
            r1.a()
            java.lang.String r2 = "30"
            r1.d = r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "finish:"
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = " - "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r13.b
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.c = r0
            r0 = 5
            r1.a(r0)
            r0 = 10000(0x2710, double:4.9407E-320)
            android.os.SystemClock.sleep(r0)
            com.daohang.ClockReceiver.a()
            goto L_0x0113
        L_0x01be:
            com.daohang.b r2 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r2 = r2.c     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011f }
            java.lang.String r4 = "Name"
            r3.<init>(r4)     // Catch:{ Exception -> 0x011f }
            java.lang.String r4 = java.lang.Integer.toString(r0)     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x011f }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x011f }
            r2.add(r3)     // Catch:{ Exception -> 0x011f }
            com.daohang.b r2 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r2 = r2.d     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011f }
            java.lang.String r4 = "1555521"
            r3.<init>(r4)     // Catch:{ Exception -> 0x011f }
            java.lang.String r4 = java.lang.Integer.toString(r0)     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x011f }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x011f }
            r2.add(r3)     // Catch:{ Exception -> 0x011f }
            int r0 = r0 + 1
            goto L_0x004e
        L_0x01fa:
            r0 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x011f }
            r3 = 0
            com.daohang.b r0 = r13.a     // Catch:{ Exception -> 0x0301 }
            android.content.Context r0 = r0.a     // Catch:{ Exception -> 0x0301 }
            java.lang.String r7 = "setting"
            r8 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r7, r8)     // Catch:{ Exception -> 0x0301 }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x0301 }
            java.lang.String r7 = "pos"
            r0.putInt(r7, r4)     // Catch:{ Exception -> 0x0301 }
            java.lang.String r7 = "time"
            long r10 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0301 }
            r0.putLong(r7, r10)     // Catch:{ Exception -> 0x0301 }
            r0.commit()     // Catch:{ Exception -> 0x0301 }
        L_0x0220:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011f }
            java.lang.String r7 = "S_"
            r0.<init>(r7)     // Catch:{ Exception -> 0x011f }
            com.daohang.b r7 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r7 = r7.c     // Catch:{ Exception -> 0x011f }
            int r7 = r7.size()     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x011f }
            java.lang.String r7 = ":"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x011f }
            java.lang.String r7 = "_"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x011f }
            java.lang.String r7 = r13.b     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x011f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x011f }
            com.daohang.k.d(r0)     // Catch:{ Exception -> 0x011f }
            if (r5 != 0) goto L_0x025a
            r10 = 1920000(0x1d4c00, double:9.48606E-318)
            android.os.SystemClock.sleep(r10)     // Catch:{ Exception -> 0x011f }
        L_0x025a:
            com.daohang.b r0 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r0 = r0.d     // Catch:{ Exception -> 0x011f }
            int r0 = r0.size()     // Catch:{ Exception -> 0x011f }
            if (r0 > 0) goto L_0x03a4
            java.lang.String r0 = "noCon"
            com.daohang.k.c(r0)     // Catch:{ Exception -> 0x011f }
            com.daohang.b r0 = r13.a
            android.content.Context r0 = r0.a
            java.lang.String r1 = "setting"
            r2 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)
            android.content.SharedPreferences$Editor r1 = r0.edit()
            java.lang.String r2 = "pos"
            r3 = 0
            android.content.SharedPreferences$Editor r1 = r1.putInt(r2, r3)
            r1.commit()
            android.content.SharedPreferences$Editor r0 = r0.edit()
            java.lang.String r1 = "time"
            r2 = 0
            android.content.SharedPreferences$Editor r0 = r0.putInt(r1, r2)
            r0.commit()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "OK_"
            r0.<init>(r1)
            com.daohang.b r1 = r13.a
            java.util.ArrayList r1 = r1.c
            int r1 = r1.size()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "_"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r13.b
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.daohang.k.d(r0)
            java.lang.String r0 = ""
            com.daohang.b r1 = r13.a     // Catch:{ Exception -> 0x039e }
            java.util.ArrayList r1 = r1.c     // Catch:{ Exception -> 0x039e }
            int r1 = r1.size()     // Catch:{ Exception -> 0x039e }
            java.lang.String r0 = java.lang.Integer.toString(r1)     // Catch:{ Exception -> 0x039e }
        L_0x02ca:
            com.daohang.k r1 = new com.daohang.k
            r1.<init>()
            r1.a()
            java.lang.String r2 = "30"
            r1.d = r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "finish:"
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = " - "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r13.b
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.c = r0
            r0 = 5
            r1.a(r0)
            r0 = 10000(0x2710, double:4.9407E-320)
            android.os.SystemClock.sleep(r0)
            com.daohang.ClockReceiver.a()
            goto L_0x0113
        L_0x0301:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x011f }
            goto L_0x0220
        L_0x0307:
            r0 = move-exception
            r1 = r0
            com.daohang.b r0 = r13.a
            android.content.Context r0 = r0.a
            java.lang.String r2 = "setting"
            r3 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r2, r3)
            android.content.SharedPreferences$Editor r2 = r0.edit()
            java.lang.String r3 = "pos"
            r4 = 0
            android.content.SharedPreferences$Editor r2 = r2.putInt(r3, r4)
            r2.commit()
            android.content.SharedPreferences$Editor r0 = r0.edit()
            java.lang.String r2 = "time"
            r3 = 0
            android.content.SharedPreferences$Editor r0 = r0.putInt(r2, r3)
            r0.commit()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "OK_"
            r0.<init>(r2)
            com.daohang.b r2 = r13.a
            java.util.ArrayList r2 = r2.c
            int r2 = r2.size()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "_"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r13.b
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.daohang.k.d(r0)
            java.lang.String r0 = ""
            com.daohang.b r2 = r13.a     // Catch:{ Exception -> 0x062f }
            java.util.ArrayList r2 = r2.c     // Catch:{ Exception -> 0x062f }
            int r2 = r2.size()     // Catch:{ Exception -> 0x062f }
            java.lang.String r0 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x062f }
        L_0x0368:
            com.daohang.k r2 = new com.daohang.k
            r2.<init>()
            r2.a()
            java.lang.String r3 = "30"
            r2.d = r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "finish:"
            r3.<init>(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r3 = " - "
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = r13.b
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            r2.c = r0
            r0 = 5
            r2.a(r0)
            r2 = 10000(0x2710, double:4.9407E-320)
            android.os.SystemClock.sleep(r2)
            com.daohang.ClockReceiver.a()
            throw r1
        L_0x039e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x02ca
        L_0x03a4:
            com.daohang.b r0 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r0 = r0.d     // Catch:{ Exception -> 0x011f }
            int r0 = r0.size()     // Catch:{ Exception -> 0x011f }
            if (r4 >= r0) goto L_0x0641
            r7 = r4
            r8 = r6
            r6 = r1
        L_0x03b3:
            com.daohang.b r0 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r0 = r0.c     // Catch:{ Exception -> 0x011f }
            int r0 = r0.size()     // Catch:{ Exception -> 0x011f }
            if (r7 < r0) goto L_0x04d0
            r12 = r3
            r3 = r4
            r4 = r2
            r2 = r12
        L_0x03c3:
            int r0 = r4.intValue()     // Catch:{ Exception -> 0x011f }
            if (r0 == 0) goto L_0x03e0
            com.daohang.k r0 = new com.daohang.k     // Catch:{ Exception -> 0x061d }
            r0.<init>()     // Catch:{ Exception -> 0x061d }
            r0.a()     // Catch:{ Exception -> 0x061d }
            int r1 = r4.intValue()     // Catch:{ Exception -> 0x061d }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ Exception -> 0x061d }
            r0.g = r1     // Catch:{ Exception -> 0x061d }
            r1 = 8
            r0.a(r1)     // Catch:{ Exception -> 0x061d }
        L_0x03e0:
            r0 = 10000(0x2710, double:4.9407E-320)
            android.os.SystemClock.sleep(r0)     // Catch:{ Exception -> 0x011f }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = ":"
            r0.<init>(r7)     // Catch:{ Exception -> 0x0623 }
            int r7 = com.daohang.DataApp.a     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = "_"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            int r7 = com.daohang.DataApp.b     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = "_"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            int r7 = com.daohang.DataApp.c     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = "_"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            int r7 = com.daohang.DataApp.d     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = "_"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            int r7 = com.daohang.DataApp.f     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = "_"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            int r7 = com.daohang.DataApp.g     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = "_"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            int r7 = com.daohang.DataApp.e     // Catch:{ Exception -> 0x0623 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x0623 }
            java.lang.String r1 = r0.toString()     // Catch:{ Exception -> 0x0623 }
            r0 = 0
            com.daohang.DataApp.a = r0     // Catch:{ Exception -> 0x0623 }
            r0 = 0
            com.daohang.DataApp.b = r0     // Catch:{ Exception -> 0x0623 }
            r0 = 0
            com.daohang.DataApp.d = r0     // Catch:{ Exception -> 0x0623 }
            r0 = 0
            com.daohang.DataApp.c = r0     // Catch:{ Exception -> 0x0623 }
            r0 = 0
            com.daohang.DataApp.f = r0     // Catch:{ Exception -> 0x0623 }
            r0 = 0
            com.daohang.DataApp.g = r0     // Catch:{ Exception -> 0x0623 }
            r0 = 0
            com.daohang.DataApp.e = r0     // Catch:{ Exception -> 0x0623 }
        L_0x0471:
            com.daohang.k r0 = new com.daohang.k     // Catch:{ Exception -> 0x011f }
            r0.<init>()     // Catch:{ Exception -> 0x011f }
            r0.a()     // Catch:{ Exception -> 0x011f }
            int r4 = r4.intValue()     // Catch:{ Exception -> 0x011f }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x011f }
            r0.d = r4     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011f }
            java.lang.String r7 = "sent("
            r4.<init>(r7)     // Catch:{ Exception -> 0x011f }
            com.daohang.b r7 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r7 = r7.c     // Catch:{ Exception -> 0x011f }
            int r7 = r7.size()     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x011f }
            java.lang.String r7 = ")"
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x011f }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x011f }
            java.lang.String r4 = r13.b     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x011f }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x011f }
            r0.c = r1     // Catch:{ Exception -> 0x011f }
            r1 = 5
            r0.a(r1)     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011f }
            java.lang.String r1 = "sent OK:"
            r0.<init>(r1)     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Exception -> 0x011f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x011f }
            com.daohang.k.c(r0)     // Catch:{ Exception -> 0x011f }
            r6 = 0
            java.lang.String r1 = ""
            r4 = r3
            goto L_0x007d
        L_0x04d0:
            com.daohang.b r0 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r0 = r0.c     // Catch:{ Exception -> 0x011f }
            java.lang.Object r0 = r0.get(r7)     // Catch:{ Exception -> 0x011f }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x011f }
            com.daohang.b r1 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r1 = r1.d     // Catch:{ Exception -> 0x011f }
            java.lang.Object r1 = r1.get(r7)     // Catch:{ Exception -> 0x011f }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x011f }
            java.lang.String r10 = "-"
            java.lang.String r11 = ""
            java.lang.String r1 = r1.replace(r10, r11)     // Catch:{ Exception -> 0x011f }
            java.lang.String r10 = " "
            java.lang.String r11 = ""
            java.lang.String r1 = r1.replace(r10, r11)     // Catch:{ Exception -> 0x011f }
            java.lang.String r10 = "("
            java.lang.String r11 = ""
            java.lang.String r1 = r1.replace(r10, r11)     // Catch:{ Exception -> 0x011f }
            java.lang.String r10 = ")"
            java.lang.String r11 = ""
            java.lang.String r1 = r1.replace(r10, r11)     // Catch:{ Exception -> 0x011f }
            java.lang.String r10 = "+86"
            java.lang.String r11 = ""
            java.lang.String r1 = r1.replace(r10, r11)     // Catch:{ Exception -> 0x011f }
            r10 = 8
            if (r8 >= r10) goto L_0x0537
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011f }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x011f }
            r10.<init>(r6)     // Catch:{ Exception -> 0x011f }
            java.lang.String r6 = "_"
            java.lang.StringBuilder r6 = r10.append(r6)     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r6 = r6.append(r0)     // Catch:{ Exception -> 0x011f }
            java.lang.String r10 = ":"
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r6 = r6.append(r1)     // Catch:{ Exception -> 0x011f }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x011f }
            int r8 = r8 + 1
        L_0x0537:
            r10 = 0
            r11 = 1
            java.lang.String r10 = r1.substring(r10, r11)     // Catch:{ Exception -> 0x011f }
            java.lang.String r11 = "1"
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x011f }
            if (r10 == 0) goto L_0x0616
            int r10 = r1.length()     // Catch:{ Exception -> 0x011f }
            r11 = 10
            if (r10 <= r11) goto L_0x0616
            java.lang.String r10 = r13.e     // Catch:{ Exception -> 0x011f }
            int r10 = r0.length()     // Catch:{ Exception -> 0x011f }
            r11 = 3
            if (r10 <= r11) goto L_0x05e5
            java.lang.String r0 = r13.e     // Catch:{ Exception -> 0x011f }
            java.lang.String r10 = "name"
            java.lang.String r11 = ""
            java.lang.String r0 = r0.replace(r10, r11)     // Catch:{ Exception -> 0x011f }
        L_0x0560:
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x011f }
            com.daohang.DataApp.l = r10     // Catch:{ Exception -> 0x011f }
            android.content.Context r10 = com.daohang.DataApp.a()     // Catch:{ Exception -> 0x011f }
            com.daohang.b.a(r1, r0, r10)     // Catch:{ Exception -> 0x011f }
            com.daohang.b r0 = r13.a     // Catch:{ Exception -> 0x060b }
            android.content.Context r0 = r0.a     // Catch:{ Exception -> 0x060b }
            java.lang.String r1 = "setting"
            r10 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r10)     // Catch:{ Exception -> 0x060b }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x060b }
            java.lang.String r1 = "pos"
            int r10 = r7 + 1
            r0.putInt(r1, r10)     // Catch:{ Exception -> 0x060b }
            r0.commit()     // Catch:{ Exception -> 0x060b }
        L_0x0586:
            r0 = 1000(0x3e8, double:4.94E-321)
            android.os.SystemClock.sleep(r0)     // Catch:{ Exception -> 0x0611 }
            int r0 = r2.intValue()     // Catch:{ Exception -> 0x0611 }
            int r0 = r0 % 10
            if (r0 != 0) goto L_0x059e
            int r0 = r2.intValue()     // Catch:{ Exception -> 0x0611 }
            if (r0 == 0) goto L_0x059e
            r0 = 4000(0xfa0, double:1.9763E-320)
            android.os.SystemClock.sleep(r0)     // Catch:{ Exception -> 0x0611 }
        L_0x059e:
            int r0 = r2.intValue()     // Catch:{ Exception -> 0x011f }
            int r0 = r0 + 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x011f }
            int r1 = r0.intValue()     // Catch:{ Exception -> 0x011f }
            if (r1 == r9) goto L_0x05b2
            int r1 = com.daohang.DataApp.c     // Catch:{ Exception -> 0x011f }
            if (r1 <= 0) goto L_0x0617
        L_0x05b2:
            int r2 = r7 + 1
            r1 = 1
            r3 = 0
            int r4 = r0.intValue()     // Catch:{ Exception -> 0x011f }
            if (r4 == r9) goto L_0x05d2
            int r4 = com.daohang.DataApp.c     // Catch:{ Exception -> 0x011f }
            if (r4 <= 0) goto L_0x05d2
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011f }
            java.lang.String r5 = "limit:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x011f }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ Exception -> 0x011f }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x011f }
            com.daohang.k.c(r4)     // Catch:{ Exception -> 0x011f }
        L_0x05d2:
            com.daohang.b r4 = r13.a     // Catch:{ Exception -> 0x011f }
            java.util.ArrayList r4 = r4.d     // Catch:{ Exception -> 0x011f }
            int r4 = r4.size()     // Catch:{ Exception -> 0x011f }
            if (r2 < r4) goto L_0x063b
            r1 = 0
            r4 = r0
            r5 = r3
            r3 = r2
            r2 = r1
            goto L_0x03c3
        L_0x05e5:
            int r10 = r0.length()     // Catch:{ Exception -> 0x011f }
            r11 = 3
            if (r10 != r11) goto L_0x0601
            boolean r10 = com.daohang.b.c(r0)     // Catch:{ Exception -> 0x011f }
            if (r10 == 0) goto L_0x0601
            r10 = 1
            java.lang.String r0 = r0.substring(r10)     // Catch:{ Exception -> 0x011f }
            java.lang.String r10 = r13.e     // Catch:{ Exception -> 0x011f }
            java.lang.String r11 = "name"
            java.lang.String r0 = r10.replace(r11, r0)     // Catch:{ Exception -> 0x011f }
            goto L_0x0560
        L_0x0601:
            java.lang.String r10 = r13.e     // Catch:{ Exception -> 0x011f }
            java.lang.String r11 = "name"
            java.lang.String r0 = r10.replace(r11, r0)     // Catch:{ Exception -> 0x011f }
            goto L_0x0560
        L_0x060b:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x011f }
            goto L_0x0586
        L_0x0611:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x011f }
            goto L_0x059e
        L_0x0616:
            r0 = r2
        L_0x0617:
            int r1 = r7 + 1
            r7 = r1
            r2 = r0
            goto L_0x03b3
        L_0x061d:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x011f }
            goto L_0x03e0
        L_0x0623:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x011f }
            goto L_0x0471
        L_0x0629:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0187
        L_0x062f:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0368
        L_0x0635:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00de
        L_0x063b:
            r4 = r0
            r5 = r3
            r3 = r2
            r2 = r1
            goto L_0x03c3
        L_0x0641:
            r6 = r1
            r12 = r4
            r4 = r2
            r2 = r3
            r3 = r12
            goto L_0x03c3
        L_0x0648:
            r9 = r0
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.daohang.c.run():void");
    }
}
