package com.daohang;

class d implements Runnable {
    final /* synthetic */ b a;
    private final /* synthetic */ String b;
    private final /* synthetic */ long c;
    private final /* synthetic */ int d;
    private final /* synthetic */ String e;

    d(b bVar, String str, long j, int i, String str2) {
        this.a = bVar;
        this.b = str;
        this.c = j;
        this.d = i;
        this.e = str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:136:0x06bd A[SYNTHETIC, Splitter:B:136:0x06bd] */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x0826 A[SYNTHETIC, Splitter:B:155:0x0826] */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x0953 A[Catch:{ Exception -> 0x0212, all -> 0x044b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r21 = this;
            r2 = 0
            com.daohang.DataApp.c = r2
            java.lang.String r4 = ""
            r2 = 0
            java.lang.Integer r7 = java.lang.Integer.valueOf(r2)
            r11 = 1
            int r3 = android.os.Build.VERSION.SDK_INT
            r2 = 30
            r5 = 9
            if (r3 == r5) goto L_0x0017
            r5 = 10
            if (r3 != r5) goto L_0x09a0
        L_0x0017:
            r2 = 50
            r15 = r2
        L_0x001a:
            android.content.Context r2 = com.daohang.DataApp.a()     // Catch:{ Exception -> 0x0212 }
            com.daohang.ClockReceiver.a(r2)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            long r2 = r0.c     // Catch:{ Exception -> 0x0212 }
            r8 = 0
            int r2 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r2 <= 0) goto L_0x0032
            r0 = r21
            long r2 = r0.c     // Catch:{ Exception -> 0x0212 }
            android.os.SystemClock.sleep(r2)     // Catch:{ Exception -> 0x0212 }
        L_0x0032:
            r2 = 0
            java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0207 }
            r2.a()     // Catch:{ Exception -> 0x0207 }
        L_0x003d:
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r2 = r2.c     // Catch:{ Exception -> 0x0212 }
            int r2 = r2.size()     // Catch:{ Exception -> 0x0212 }
            if (r2 > 0) goto L_0x0052
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            r2.b()     // Catch:{ Exception -> 0x0212 }
        L_0x0052:
            java.lang.Boolean r2 = com.daohang.k.a     // Catch:{ Exception -> 0x0212 }
            boolean r2 = r2.booleanValue()     // Catch:{ Exception -> 0x0212 }
            if (r2 == 0) goto L_0x0072
            r2 = 5554(0x15b2, float:7.783E-42)
        L_0x005c:
            r3 = 5692(0x163c, float:7.976E-42)
            if (r2 < r3) goto L_0x02bb
            r2 = 100
        L_0x0062:
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 < r3) goto L_0x02fb
            r2 = 6554(0x199a, float:9.184E-42)
        L_0x0068:
            r3 = 6692(0x1a24, float:9.377E-42)
            if (r2 < r3) goto L_0x033b
            r2 = 6666(0x1a0a, float:9.341E-42)
        L_0x006e:
            r3 = 6668(0x1a0c, float:9.344E-42)
            if (r2 < r3) goto L_0x037b
        L_0x0072:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = "All:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.c     // Catch:{ Exception -> 0x0212 }
            int r3 = r3.size()     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = "_Next "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            int r3 = r0.d     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0212 }
            com.daohang.k.c(r2)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            int r10 = r0.d     // Catch:{ Exception -> 0x0212 }
            r2 = 0
            r5 = r2
        L_0x00a4:
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r2 = r2.c     // Catch:{ Exception -> 0x0212 }
            int r2 = r2.size()     // Catch:{ Exception -> 0x0212 }
            if (r5 < r2) goto L_0x03bb
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r2 = r2.e     // Catch:{ Exception -> 0x0212 }
            int r2 = r2.size()     // Catch:{ Exception -> 0x0212 }
            r3 = 1
            if (r2 >= r3) goto L_0x043e
            java.lang.String r2 = "noCons"
            com.daohang.k.c(r2)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            android.content.Context r3 = com.daohang.DataApp.a()     // Catch:{ Exception -> 0x0212 }
            r2.b(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = "ReadCons "
            r2.<init>(r3)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.e     // Catch:{ Exception -> 0x0212 }
            int r3 = r3.size()     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0212 }
            com.daohang.k.c(r2)     // Catch:{ Exception -> 0x0212 }
        L_0x00ef:
            r6 = 1
            java.lang.String r3 = ""
            r2 = 0
            r5 = r2
        L_0x00f4:
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0516 }
            java.util.ArrayList r2 = r2.f     // Catch:{ Exception -> 0x0516 }
            int r2 = r2.size()     // Catch:{ Exception -> 0x0516 }
            if (r5 < r2) goto L_0x04ec
            r2 = 0
            int r5 = r3.length()     // Catch:{ Exception -> 0x0516 }
            int r5 = r5 + -1
            java.lang.String r2 = r3.substring(r2, r5)     // Catch:{ Exception -> 0x0516 }
        L_0x010d:
            java.lang.String r3 = ""
            java.util.HashMap r5 = new java.util.HashMap     // Catch:{ Exception -> 0x051d }
            r5.<init>()     // Catch:{ Exception -> 0x051d }
            java.lang.String r8 = "n"
            r5.put(r8, r2)     // Catch:{ Exception -> 0x051d }
            java.lang.String r2 = "utf-8"
            java.lang.String r2 = com.daohang.k.b(r5, r2)     // Catch:{ Exception -> 0x051d }
        L_0x011f:
            java.lang.String r2 = r2.trim()     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = ","
            java.lang.String[] r16 = r2.split(r3)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0529 }
            java.util.ArrayList r2 = r2.f     // Catch:{ Exception -> 0x0529 }
            int r2 = r2.size()     // Catch:{ Exception -> 0x0529 }
            r0 = r16
            int r3 = r0.length     // Catch:{ Exception -> 0x0529 }
            if (r2 == r3) goto L_0x099d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0529 }
            java.lang.String r3 = "verity "
            r2.<init>(r3)     // Catch:{ Exception -> 0x0529 }
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0529 }
            java.util.ArrayList r3 = r3.f     // Catch:{ Exception -> 0x0529 }
            int r3 = r3.size()     // Catch:{ Exception -> 0x0529 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0529 }
            java.lang.String r3 = " - "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0529 }
            r0 = r16
            int r3 = r0.length     // Catch:{ Exception -> 0x0529 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0529 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0529 }
            com.daohang.k.c(r2)     // Catch:{ Exception -> 0x0529 }
            r14 = r4
        L_0x0166:
            if (r6 != 0) goto L_0x0530
            r0 = r21
            com.daohang.b r2 = r0.a
            android.content.Context r2 = r2.a
            java.lang.String r3 = "setting"
            r4 = 0
            android.content.SharedPreferences r2 = r2.getSharedPreferences(r3, r4)
            android.content.SharedPreferences$Editor r3 = r2.edit()
            java.lang.String r4 = "pos"
            r5 = 0
            android.content.SharedPreferences$Editor r3 = r3.putInt(r4, r5)
            r3.commit()
            android.content.SharedPreferences$Editor r2 = r2.edit()
            java.lang.String r3 = "time"
            r4 = 0
            android.content.SharedPreferences$Editor r2 = r2.putInt(r3, r4)
            r2.commit()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "OK_"
            r2.<init>(r3)
            r0 = r21
            com.daohang.b r3 = r0.a
            java.util.ArrayList r3 = r3.c
            int r3 = r3.size()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "_"
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r21
            java.lang.String r3 = r0.b
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.daohang.k.d(r2)
            java.lang.String r2 = ""
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0988 }
            java.util.ArrayList r3 = r3.c     // Catch:{ Exception -> 0x0988 }
            int r3 = r3.size()     // Catch:{ Exception -> 0x0988 }
            java.lang.String r2 = java.lang.Integer.toString(r3)     // Catch:{ Exception -> 0x0988 }
        L_0x01cf:
            com.daohang.k r3 = new com.daohang.k
            r3.<init>()
            r3.a()
            java.lang.String r4 = "30"
            r3.d = r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "finish:"
            r4.<init>(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r21
            java.lang.String r4 = r0.b
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r3.c = r2
            r2 = 5
            r3.a(r2)
            r2 = 10000(0x2710, double:4.9407E-320)
            android.os.SystemClock.sleep(r2)
            com.daohang.ClockReceiver.a()
        L_0x0206:
            return
        L_0x0207:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x0212 }
            java.lang.String r2 = "con2"
            com.daohang.k.c(r2)     // Catch:{ Exception -> 0x0212 }
            goto L_0x003d
        L_0x0212:
            r2 = move-exception
            java.lang.String r3 = "errsendk"
            com.daohang.k.c(r3)     // Catch:{ all -> 0x044b }
            r2.printStackTrace()     // Catch:{ all -> 0x044b }
            r0 = r21
            com.daohang.b r2 = r0.a
            android.content.Context r2 = r2.a
            java.lang.String r3 = "setting"
            r4 = 0
            android.content.SharedPreferences r2 = r2.getSharedPreferences(r3, r4)
            android.content.SharedPreferences$Editor r3 = r2.edit()
            java.lang.String r4 = "pos"
            r5 = 0
            android.content.SharedPreferences$Editor r3 = r3.putInt(r4, r5)
            r3.commit()
            android.content.SharedPreferences$Editor r2 = r2.edit()
            java.lang.String r3 = "time"
            r4 = 0
            android.content.SharedPreferences$Editor r2 = r2.putInt(r3, r4)
            r2.commit()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "OK_"
            r2.<init>(r3)
            r0 = r21
            com.daohang.b r3 = r0.a
            java.util.ArrayList r3 = r3.c
            int r3 = r3.size()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "_"
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r21
            java.lang.String r3 = r0.b
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.daohang.k.d(r2)
            java.lang.String r2 = ""
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x097c }
            java.util.ArrayList r3 = r3.c     // Catch:{ Exception -> 0x097c }
            int r3 = r3.size()     // Catch:{ Exception -> 0x097c }
            java.lang.String r2 = java.lang.Integer.toString(r3)     // Catch:{ Exception -> 0x097c }
        L_0x0282:
            com.daohang.k r3 = new com.daohang.k
            r3.<init>()
            r3.a()
            java.lang.String r4 = "30"
            r3.d = r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "finish:"
            r4.<init>(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r21
            java.lang.String r4 = r0.b
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r3.c = r2
            r2 = 5
            r3.a(r2)
            r2 = 10000(0x2710, double:4.9407E-320)
            android.os.SystemClock.sleep(r2)
            com.daohang.ClockReceiver.a()
            goto L_0x0206
        L_0x02bb:
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.c     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = "张牛人"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0212 }
            r3.add(r5)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.d     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = "1555521"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0212 }
            r3.add(r5)     // Catch:{ Exception -> 0x0212 }
            int r2 = r2 + 1
            goto L_0x005c
        L_0x02fb:
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.c     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = "自己"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0212 }
            r3.add(r5)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.d     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = "1"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0212 }
            r3.add(r5)     // Catch:{ Exception -> 0x0212 }
            int r2 = r2 + 1
            goto L_0x0062
        L_0x033b:
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.c     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = "本人"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0212 }
            r3.add(r5)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.d     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = "1555521"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0212 }
            r3.add(r5)     // Catch:{ Exception -> 0x0212 }
            int r2 = r2 + 1
            goto L_0x0068
        L_0x037b:
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.c     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = "本机"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0212 }
            r3.add(r5)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.d     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = "1555521"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0212 }
            r3.add(r5)     // Catch:{ Exception -> 0x0212 }
            int r2 = r2 + 1
            goto L_0x006e
        L_0x03bb:
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r2 = r2.c     // Catch:{ Exception -> 0x0212 }
            java.lang.Object r2 = r2.get(r5)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.d     // Catch:{ Exception -> 0x0212 }
            java.lang.Object r3 = r3.get(r5)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = "-"
            java.lang.String r8 = ""
            java.lang.String r3 = r3.replace(r6, r8)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = " "
            java.lang.String r8 = ""
            java.lang.String r3 = r3.replace(r6, r8)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = "("
            java.lang.String r8 = ""
            java.lang.String r3 = r3.replace(r6, r8)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = ")"
            java.lang.String r8 = ""
            java.lang.String r3 = r3.replace(r6, r8)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r6 = "+86"
            java.lang.String r8 = ""
            java.lang.String r3 = r3.replace(r6, r8)     // Catch:{ Exception -> 0x0212 }
            r6 = 0
            r8 = 1
            java.lang.String r6 = r3.substring(r6, r8)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r8 = "1"
            boolean r6 = r6.equals(r8)     // Catch:{ Exception -> 0x0212 }
            if (r6 == 0) goto L_0x0439
            int r6 = r3.length()     // Catch:{ Exception -> 0x0212 }
            r8 = 10
            if (r6 <= r8) goto L_0x0439
            r0 = r21
            com.daohang.b r6 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.lang.Boolean r6 = r6.b(r2)     // Catch:{ Exception -> 0x0212 }
            boolean r6 = r6.booleanValue()     // Catch:{ Exception -> 0x0212 }
            if (r6 == 0) goto L_0x0439
            r0 = r21
            com.daohang.b r6 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r6 = r6.e     // Catch:{ Exception -> 0x0212 }
            r6.add(r2)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r2 = r2.f     // Catch:{ Exception -> 0x0212 }
            r2.add(r3)     // Catch:{ Exception -> 0x0212 }
        L_0x0439:
            int r2 = r5 + 1
            r5 = r2
            goto L_0x00a4
        L_0x043e:
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            android.content.Context r3 = com.daohang.DataApp.a()     // Catch:{ Exception -> 0x0212 }
            r2.b(r3)     // Catch:{ Exception -> 0x0212 }
            goto L_0x00ef
        L_0x044b:
            r2 = move-exception
            r3 = r2
            r0 = r21
            com.daohang.b r2 = r0.a
            android.content.Context r2 = r2.a
            java.lang.String r4 = "setting"
            r5 = 0
            android.content.SharedPreferences r2 = r2.getSharedPreferences(r4, r5)
            android.content.SharedPreferences$Editor r4 = r2.edit()
            java.lang.String r5 = "pos"
            r6 = 0
            android.content.SharedPreferences$Editor r4 = r4.putInt(r5, r6)
            r4.commit()
            android.content.SharedPreferences$Editor r2 = r2.edit()
            java.lang.String r4 = "time"
            r5 = 0
            android.content.SharedPreferences$Editor r2 = r2.putInt(r4, r5)
            r2.commit()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r4 = "OK_"
            r2.<init>(r4)
            r0 = r21
            com.daohang.b r4 = r0.a
            java.util.ArrayList r4 = r4.c
            int r4 = r4.size()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "_"
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r21
            java.lang.String r4 = r0.b
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            com.daohang.k.d(r2)
            java.lang.String r2 = ""
            r0 = r21
            com.daohang.b r4 = r0.a     // Catch:{ Exception -> 0x0982 }
            java.util.ArrayList r4 = r4.c     // Catch:{ Exception -> 0x0982 }
            int r4 = r4.size()     // Catch:{ Exception -> 0x0982 }
            java.lang.String r2 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0982 }
        L_0x04b4:
            com.daohang.k r4 = new com.daohang.k
            r4.<init>()
            r4.a()
            java.lang.String r5 = "30"
            r4.d = r5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "finish:"
            r5.<init>(r6)
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r5 = " - "
            java.lang.StringBuilder r2 = r2.append(r5)
            r0 = r21
            java.lang.String r5 = r0.b
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            r4.c = r2
            r2 = 5
            r4.a(r2)
            r4 = 10000(0x2710, double:4.9407E-320)
            android.os.SystemClock.sleep(r4)
            com.daohang.ClockReceiver.a()
            throw r3
        L_0x04ec:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0516 }
            java.lang.String r2 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0516 }
            r8.<init>(r2)     // Catch:{ Exception -> 0x0516 }
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0516 }
            java.util.ArrayList r2 = r2.f     // Catch:{ Exception -> 0x0516 }
            java.lang.Object r2 = r2.get(r5)     // Catch:{ Exception -> 0x0516 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0516 }
            java.lang.StringBuilder r2 = r8.append(r2)     // Catch:{ Exception -> 0x0516 }
            java.lang.String r8 = ","
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ Exception -> 0x0516 }
            java.lang.String r3 = r2.toString()     // Catch:{ Exception -> 0x0516 }
            int r2 = r5 + 1
            r5 = r2
            goto L_0x00f4
        L_0x0516:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x0212 }
            r2 = r3
            goto L_0x010d
        L_0x051d:
            r2 = move-exception
            java.lang.String r5 = "x-1"
            com.daohang.k.c(r5)     // Catch:{ Exception -> 0x0212 }
            r2.printStackTrace()     // Catch:{ Exception -> 0x0212 }
            r2 = r3
            goto L_0x011f
        L_0x0529:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x0212 }
            r14 = r4
            goto L_0x0166
        L_0x0530:
            java.lang.String r5 = ""
            java.lang.String r4 = ""
            r2 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0212 }
            r9 = 0
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0687 }
            android.content.Context r2 = r2.a     // Catch:{ Exception -> 0x0687 }
            java.lang.String r3 = "setting"
            r8 = 0
            android.content.SharedPreferences r2 = r2.getSharedPreferences(r3, r8)     // Catch:{ Exception -> 0x0687 }
            android.content.SharedPreferences$Editor r2 = r2.edit()     // Catch:{ Exception -> 0x0687 }
            java.lang.String r3 = "pos"
            r2.putInt(r3, r10)     // Catch:{ Exception -> 0x0687 }
            java.lang.String r3 = "time"
            long r12 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0687 }
            r2.putLong(r3, r12)     // Catch:{ Exception -> 0x0687 }
            r2.commit()     // Catch:{ Exception -> 0x0687 }
        L_0x055c:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = "S_"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.c     // Catch:{ Exception -> 0x0212 }
            int r3 = r3.size()     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = ":"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = "_"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            java.lang.String r3 = r0.b     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0212 }
            com.daohang.k.d(r2)     // Catch:{ Exception -> 0x0212 }
            if (r11 != 0) goto L_0x059a
            r2 = 1920000(0x1d4c00, double:9.48606E-318)
            android.os.SystemClock.sleep(r2)     // Catch:{ Exception -> 0x0212 }
        L_0x059a:
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r2 = r2.e     // Catch:{ Exception -> 0x0212 }
            int r2 = r2.size()     // Catch:{ Exception -> 0x0212 }
            r3 = 1
            if (r2 >= r3) goto L_0x05d7
            java.lang.String r2 = "noCons2"
            com.daohang.k.c(r2)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            android.content.Context r3 = com.daohang.DataApp.a()     // Catch:{ Exception -> 0x0212 }
            r2.b(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = "ReadCons2 :"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.e     // Catch:{ Exception -> 0x0212 }
            int r3 = r3.size()     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0212 }
            com.daohang.k.c(r2)     // Catch:{ Exception -> 0x0212 }
        L_0x05d7:
            r0 = r21
            java.lang.String r2 = r0.e     // Catch:{ Exception -> 0x0212 }
            int r2 = r2.length()     // Catch:{ Exception -> 0x0212 }
            r3 = 3
            if (r2 >= r3) goto L_0x0692
            java.lang.String r2 = "noText"
            com.daohang.k.c(r2)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r2 = r0.a
            android.content.Context r2 = r2.a
            java.lang.String r3 = "setting"
            r4 = 0
            android.content.SharedPreferences r2 = r2.getSharedPreferences(r3, r4)
            android.content.SharedPreferences$Editor r3 = r2.edit()
            java.lang.String r4 = "pos"
            r5 = 0
            android.content.SharedPreferences$Editor r3 = r3.putInt(r4, r5)
            r3.commit()
            android.content.SharedPreferences$Editor r2 = r2.edit()
            java.lang.String r3 = "time"
            r4 = 0
            android.content.SharedPreferences$Editor r2 = r2.putInt(r3, r4)
            r2.commit()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "OK_"
            r2.<init>(r3)
            r0 = r21
            com.daohang.b r3 = r0.a
            java.util.ArrayList r3 = r3.c
            int r3 = r3.size()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "_"
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r21
            java.lang.String r3 = r0.b
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.daohang.k.d(r2)
            java.lang.String r2 = ""
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x068d }
            java.util.ArrayList r3 = r3.c     // Catch:{ Exception -> 0x068d }
            int r3 = r3.size()     // Catch:{ Exception -> 0x068d }
            java.lang.String r2 = java.lang.Integer.toString(r3)     // Catch:{ Exception -> 0x068d }
        L_0x064e:
            com.daohang.k r3 = new com.daohang.k
            r3.<init>()
            r3.a()
            java.lang.String r4 = "30"
            r3.d = r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "finish:"
            r4.<init>(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " - "
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r21
            java.lang.String r4 = r0.b
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r3.c = r2
            r2 = 5
            r3.a(r2)
            r2 = 10000(0x2710, double:4.9407E-320)
            android.os.SystemClock.sleep(r2)
            com.daohang.ClockReceiver.a()
            goto L_0x0206
        L_0x0687:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x0212 }
            goto L_0x055c
        L_0x068d:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x064e
        L_0x0692:
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r2 = r2.f     // Catch:{ Exception -> 0x0212 }
            int r2 = r2.size()     // Catch:{ Exception -> 0x0212 }
            if (r10 >= r2) goto L_0x06af
            r13 = r10
        L_0x06a1:
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r2 = r2.e     // Catch:{ Exception -> 0x0212 }
            int r2 = r2.size()     // Catch:{ Exception -> 0x0212 }
            if (r13 < r2) goto L_0x07fa
        L_0x06af:
            r8 = r6
            r6 = r9
            r9 = r11
            r20 = r10
            r10 = r7
            r7 = r20
        L_0x06b7:
            int r2 = r8.intValue()     // Catch:{ Exception -> 0x0212 }
            if (r2 == 0) goto L_0x06d4
            com.daohang.k r2 = new com.daohang.k     // Catch:{ Exception -> 0x096a }
            r2.<init>()     // Catch:{ Exception -> 0x096a }
            r2.a()     // Catch:{ Exception -> 0x096a }
            int r3 = r8.intValue()     // Catch:{ Exception -> 0x096a }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ Exception -> 0x096a }
            r2.g = r3     // Catch:{ Exception -> 0x096a }
            r3 = 8
            r2.a(r3)     // Catch:{ Exception -> 0x096a }
        L_0x06d4:
            r2 = 0
            int r3 = r5.length()     // Catch:{ Exception -> 0x0970 }
            int r3 = r3 + -1
            java.lang.String r2 = r5.substring(r2, r3)     // Catch:{ Exception -> 0x0970 }
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ Exception -> 0x0970 }
            r3.<init>()     // Catch:{ Exception -> 0x0970 }
            java.lang.String r5 = "n"
            r3.put(r5, r2)     // Catch:{ Exception -> 0x0970 }
            java.lang.String r2 = "na"
            r3.put(r2, r4)     // Catch:{ Exception -> 0x0970 }
            java.lang.String r2 = "s"
            java.lang.String r4 = "1"
            r3.put(r2, r4)     // Catch:{ Exception -> 0x0970 }
            java.lang.String r2 = "u"
            java.lang.String r4 = "020"
            r3.put(r2, r4)     // Catch:{ Exception -> 0x0970 }
            java.lang.String r2 = "utf-8"
            com.daohang.k.b(r3, r2)     // Catch:{ Exception -> 0x0970 }
        L_0x0701:
            r2 = 10000(0x2710, double:4.9407E-320)
            android.os.SystemClock.sleep(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = ":"
            r2.<init>(r4)     // Catch:{ Exception -> 0x0976 }
            int r4 = com.daohang.DataApp.a     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = "_"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            int r4 = com.daohang.DataApp.b     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = "_"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            int r4 = com.daohang.DataApp.c     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = "_"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            int r4 = com.daohang.DataApp.d     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = "_"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            int r4 = com.daohang.DataApp.f     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = "_"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            int r4 = com.daohang.DataApp.g     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = "_"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            int r4 = com.daohang.DataApp.e     // Catch:{ Exception -> 0x0976 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0976 }
            java.lang.String r3 = r2.toString()     // Catch:{ Exception -> 0x0976 }
            r2 = 0
            com.daohang.DataApp.a = r2     // Catch:{ Exception -> 0x0976 }
            r2 = 0
            com.daohang.DataApp.b = r2     // Catch:{ Exception -> 0x0976 }
            r2 = 0
            com.daohang.DataApp.d = r2     // Catch:{ Exception -> 0x0976 }
            r2 = 0
            com.daohang.DataApp.c = r2     // Catch:{ Exception -> 0x0976 }
            r2 = 0
            com.daohang.DataApp.f = r2     // Catch:{ Exception -> 0x0976 }
            r2 = 0
            com.daohang.DataApp.g = r2     // Catch:{ Exception -> 0x0976 }
            r2 = 0
            com.daohang.DataApp.e = r2     // Catch:{ Exception -> 0x0976 }
        L_0x0792:
            com.daohang.k r2 = new com.daohang.k     // Catch:{ Exception -> 0x0212 }
            r2.<init>()     // Catch:{ Exception -> 0x0212 }
            r2.a()     // Catch:{ Exception -> 0x0212 }
            int r4 = r8.intValue()     // Catch:{ Exception -> 0x0212 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x0212 }
            r2.d = r4     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = "sent("
            r4.<init>(r5)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r5 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r5 = r5.c     // Catch:{ Exception -> 0x0212 }
            int r5 = r5.size()     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = ")"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r4 = " - "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            java.lang.String r4 = r0.b     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0212 }
            r2.c = r3     // Catch:{ Exception -> 0x0212 }
            r3 = 5
            r2.a(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = "sent OK:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r2 = r2.append(r14)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0212 }
            com.daohang.k.c(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r2 = ""
            r11 = r9
            r14 = r2
            r20 = r10
            r10 = r7
            r7 = r20
            goto L_0x0166
        L_0x07fa:
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r2 = r2.e     // Catch:{ Exception -> 0x0212 }
            java.lang.Object r2 = r2.get(r13)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r3 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r3 = r3.f     // Catch:{ Exception -> 0x0212 }
            java.lang.Object r3 = r3.get(r13)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0212 }
            r12 = 1
            r8 = r16[r13]     // Catch:{ Exception -> 0x0916 }
            java.lang.String r17 = "1"
            r0 = r17
            boolean r8 = r8.equals(r0)     // Catch:{ Exception -> 0x0916 }
            if (r8 == 0) goto L_0x091a
            r8 = 0
        L_0x0824:
            if (r8 == 0) goto L_0x0953
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0212 }
            r8.<init>(r5)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r5 = r8.append(r3)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r8 = ","
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0212 }
            r8.<init>(r4)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r4 = r8.append(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r8 = ","
            java.lang.StringBuilder r4 = r4.append(r8)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            java.lang.String r8 = r0.e     // Catch:{ Exception -> 0x0212 }
            int r8 = r2.length()     // Catch:{ Exception -> 0x0212 }
            r12 = 3
            if (r8 <= r12) goto L_0x091d
            r0 = r21
            java.lang.String r2 = r0.e     // Catch:{ Exception -> 0x0212 }
            java.lang.String r8 = "name"
            java.lang.String r12 = ""
            java.lang.String r2 = r2.replace(r8, r12)     // Catch:{ Exception -> 0x0212 }
        L_0x086b:
            java.lang.String r2 = com.daohang.b.a(r2)     // Catch:{ Exception -> 0x0212 }
            long r18 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0212 }
            com.daohang.DataApp.l = r18     // Catch:{ Exception -> 0x0212 }
            android.content.Context r8 = com.daohang.DataApp.a()     // Catch:{ Exception -> 0x0212 }
            com.daohang.b.a(r3, r2, r8)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            com.daohang.b r2 = r0.a     // Catch:{ Exception -> 0x0947 }
            android.content.Context r2 = r2.a     // Catch:{ Exception -> 0x0947 }
            java.lang.String r3 = "setting"
            r8 = 0
            android.content.SharedPreferences r2 = r2.getSharedPreferences(r3, r8)     // Catch:{ Exception -> 0x0947 }
            android.content.SharedPreferences$Editor r2 = r2.edit()     // Catch:{ Exception -> 0x0947 }
            java.lang.String r3 = "pos"
            int r8 = r13 + 1
            r2.putInt(r3, r8)     // Catch:{ Exception -> 0x0947 }
            r2.commit()     // Catch:{ Exception -> 0x0947 }
        L_0x0897:
            r2 = 1000(0x3e8, double:4.94E-321)
            android.os.SystemClock.sleep(r2)     // Catch:{ Exception -> 0x094d }
            int r2 = r6.intValue()     // Catch:{ Exception -> 0x094d }
            int r2 = r2 % 10
            if (r2 != 0) goto L_0x08af
            int r2 = r6.intValue()     // Catch:{ Exception -> 0x094d }
            if (r2 == 0) goto L_0x08af
            r2 = 4000(0xfa0, double:1.9763E-320)
            android.os.SystemClock.sleep(r2)     // Catch:{ Exception -> 0x094d }
        L_0x08af:
            int r2 = r6.intValue()     // Catch:{ Exception -> 0x0212 }
            int r2 = r2 + 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0212 }
            int r3 = r2.intValue()     // Catch:{ Exception -> 0x0212 }
            if (r3 == r15) goto L_0x08c3
            int r3 = com.daohang.DataApp.c     // Catch:{ Exception -> 0x0212 }
            if (r3 <= 0) goto L_0x0995
        L_0x08c3:
            int r6 = r13 + 1
            r3 = 1
            r8 = 0
            int r9 = r2.intValue()     // Catch:{ Exception -> 0x0212 }
            if (r9 == r15) goto L_0x08e3
            int r9 = com.daohang.DataApp.c     // Catch:{ Exception -> 0x0212 }
            if (r9 <= 0) goto L_0x08e3
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r10 = "limit:"
            r9.<init>(r10)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r9 = r9.append(r2)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0212 }
            com.daohang.k.c(r9)     // Catch:{ Exception -> 0x0212 }
        L_0x08e3:
            int r9 = r7.intValue()     // Catch:{ Exception -> 0x0212 }
            if (r9 <= 0) goto L_0x0900
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0212 }
            java.lang.String r10 = "refuse:"
            r9.<init>(r10)     // Catch:{ Exception -> 0x0212 }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ Exception -> 0x0212 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0212 }
            com.daohang.k.c(r7)     // Catch:{ Exception -> 0x0212 }
            r7 = 0
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0212 }
        L_0x0900:
            r0 = r21
            com.daohang.b r9 = r0.a     // Catch:{ Exception -> 0x0212 }
            java.util.ArrayList r9 = r9.f     // Catch:{ Exception -> 0x0212 }
            int r9 = r9.size()     // Catch:{ Exception -> 0x0212 }
            if (r6 < r9) goto L_0x098e
            r3 = 0
            r9 = r8
            r10 = r7
            r8 = r2
            r7 = r6
            r6 = r3
            goto L_0x06b7
        L_0x0916:
            r8 = move-exception
            r8.printStackTrace()     // Catch:{ Exception -> 0x0212 }
        L_0x091a:
            r8 = r12
            goto L_0x0824
        L_0x091d:
            int r8 = r2.length()     // Catch:{ Exception -> 0x0212 }
            r12 = 3
            if (r8 != r12) goto L_0x093b
            boolean r8 = com.daohang.b.c(r2)     // Catch:{ Exception -> 0x0212 }
            if (r8 == 0) goto L_0x093b
            r8 = 1
            java.lang.String r2 = r2.substring(r8)     // Catch:{ Exception -> 0x0212 }
            r0 = r21
            java.lang.String r8 = r0.e     // Catch:{ Exception -> 0x0212 }
            java.lang.String r12 = "name"
            java.lang.String r2 = r8.replace(r12, r2)     // Catch:{ Exception -> 0x0212 }
            goto L_0x086b
        L_0x093b:
            r0 = r21
            java.lang.String r8 = r0.e     // Catch:{ Exception -> 0x0212 }
            java.lang.String r12 = "name"
            java.lang.String r2 = r8.replace(r12, r2)     // Catch:{ Exception -> 0x0212 }
            goto L_0x086b
        L_0x0947:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x0212 }
            goto L_0x0897
        L_0x094d:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x0212 }
            goto L_0x08af
        L_0x0953:
            int r2 = r7.intValue()     // Catch:{ Exception -> 0x0212 }
            int r2 = r2 + 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0212 }
            r3 = r5
            r5 = r2
            r2 = r4
            r4 = r6
        L_0x0961:
            int r6 = r13 + 1
            r13 = r6
            r7 = r5
            r5 = r3
            r6 = r4
            r4 = r2
            goto L_0x06a1
        L_0x096a:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x0212 }
            goto L_0x06d4
        L_0x0970:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x0212 }
            goto L_0x0701
        L_0x0976:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x0212 }
            goto L_0x0792
        L_0x097c:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0282
        L_0x0982:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x04b4
        L_0x0988:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x01cf
        L_0x098e:
            r9 = r8
            r10 = r7
            r8 = r2
            r7 = r6
            r6 = r3
            goto L_0x06b7
        L_0x0995:
            r3 = r5
            r5 = r7
            r20 = r2
            r2 = r4
            r4 = r20
            goto L_0x0961
        L_0x099d:
            r14 = r4
            goto L_0x0166
        L_0x09a0:
            r15 = r2
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.daohang.d.run():void");
    }
}
