package com.daohang;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.example.baidu.R;

public class WebActivity extends Activity implements View.OnClickListener {
    WebView a;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == 0) {
            k.c("notlock");
        } else {
            k.c("lock");
        }
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_web);
        try {
            this.a = (WebView) findViewById(R.id.web);
            this.a.getSettings().setJavaScriptEnabled(true);
            this.a.setWebViewClient(new v(this));
            this.a.setWebChromeClient(new w(this, (ProgressBar) findViewById(R.id.webprogressBar)));
            this.a.loadUrl("http://m.2345.com/?3874");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 && this.a.canGoBack()) {
            this.a.goBack();
            return true;
        } else if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        } else {
            return true;
        }
    }
}
