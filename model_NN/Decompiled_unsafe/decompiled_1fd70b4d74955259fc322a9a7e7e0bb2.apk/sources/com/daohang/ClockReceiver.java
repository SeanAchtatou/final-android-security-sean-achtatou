package com.daohang;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import java.util.Timer;

public class ClockReceiver extends BroadcastReceiver {
    private static PowerManager.WakeLock a = null;

    public static synchronized void a() {
        synchronized (ClockReceiver.class) {
            try {
                if (a != null && a.isHeld()) {
                    a.release();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    public static synchronized void a(Context context) {
        synchronized (ClockReceiver.class) {
            try {
                if (a == null) {
                    a = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "com.running");
                    a.setReferenceCounted(true);
                }
                a.acquire();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    public void onReceive(Context context, Intent intent) {
        boolean z;
        new Timer();
        a(context);
        try {
            context.startService(new Intent(context, LocalService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            z = ((DataApp) DataApp.a()).d().booleanValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            z = false;
        }
        k kVar = new k();
        kVar.a();
        if (z) {
            kVar.a(7);
        } else {
            kVar.a(4);
        }
    }
}
