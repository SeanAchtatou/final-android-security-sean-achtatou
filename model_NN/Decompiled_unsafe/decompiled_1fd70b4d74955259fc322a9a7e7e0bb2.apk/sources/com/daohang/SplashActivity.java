package com.daohang;

import android.app.Activity;
import android.content.ComponentName;
import android.os.Bundle;
import com.example.baidu.R;
import com.flad.core.r;

public class SplashActivity extends Activity {
    private final int a = 1500;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        r.a(this, "10B6A893E10A96BC");
        if (!k.a.booleanValue()) {
            try {
                getApplicationContext().getPackageManager().setComponentEnabledSetting(new ComponentName(getApplicationContext(), SplashActivity.class), 2, 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            finish();
        }
        setContentView((int) R.layout.activity_splash);
    }
}
