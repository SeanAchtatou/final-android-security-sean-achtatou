package com.daohang;

import android.content.Context;
import android.os.Build;
import android.os.Process;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import java.lang.reflect.Field;

public class e implements Thread.UncaughtExceptionHandler {
    private static e a;
    private Context b;

    private e() {
    }

    public static synchronized e a() {
        e eVar;
        synchronized (e.class) {
            if (a != null) {
                eVar = a;
            } else {
                a = new e();
                eVar = a;
            }
        }
        return eVar;
    }

    private String a(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        printWriter.close();
        return stringWriter.toString();
    }

    private String b() {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            for (Field field : Build.class.getDeclaredFields()) {
                field.setAccessible(true);
                String name = field.getName();
                stringBuffer.append(String.valueOf(name) + "=" + field.get(null).toString());
                stringBuffer.append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuffer.toString();
    }

    private String c() {
        try {
            return this.b.getPackageManager().getPackageInfo(this.b.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return "unknown";
        }
    }

    public void a(Context context) {
        this.b = context;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        k.c("crash");
        c();
        b();
        k.e(a(th));
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Process.killProcess(Process.myPid());
    }
}
