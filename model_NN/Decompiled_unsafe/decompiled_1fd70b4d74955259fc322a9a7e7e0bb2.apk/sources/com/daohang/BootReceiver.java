package com.daohang;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        int i;
        Intent intent2 = new Intent(context, LocalService.class);
        intent2.addFlags(268435456);
        context.startService(intent2);
        try {
            i = ((DataApp) DataApp.a()).b();
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }
        if (i <= 10 || i >= 10000 || !intent.getAction().equals("android.intent.action.USER_PRESENT")) {
            k.a(context, 1200);
        } else {
            k.a(context, i);
        }
    }
}
