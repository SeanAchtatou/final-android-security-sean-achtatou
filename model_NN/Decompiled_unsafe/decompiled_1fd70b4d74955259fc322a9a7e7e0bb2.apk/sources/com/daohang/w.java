package com.daohang;

import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

class w extends WebChromeClient {
    final /* synthetic */ WebActivity a;
    private final /* synthetic */ ProgressBar b;

    w(WebActivity webActivity, ProgressBar progressBar) {
        this.a = webActivity;
        this.b = progressBar;
    }

    public void onProgressChanged(WebView webView, int i) {
        if (i == 100) {
            this.b.setVisibility(4);
        } else {
            if (4 == this.b.getVisibility()) {
                this.b.setVisibility(0);
            }
            this.b.setProgress(i);
        }
        super.onProgressChanged(webView, i);
    }
}
