package com.daohang;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.WindowManager;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class DataApp extends Application {
    public static int a;
    public static int b;
    public static int c;
    public static int d;
    public static int e;
    public static int f;
    public static int g;
    public static int h = 0;
    public static int i = 0;
    public static String j = "";
    public static int k = 0;
    public static long l;
    private static Context v;
    private WindowManager.LayoutParams m = new WindowManager.LayoutParams();
    private String n;
    private String o;
    private String p = "";
    private String q = "";
    private int r;
    private boolean s;
    private int t;
    private int u;
    private int w;
    private int x;
    private boolean y;

    public static Context a() {
        return v;
    }

    public void a(int i2) {
        this.x = i2;
    }

    public void a(int i2, Context context) {
        this.t = i2;
        context.getSharedPreferences("setting", 0).edit().putInt("model", i2).commit();
    }

    public void a(Boolean bool, Context context) {
        this.s = bool.booleanValue();
        context.getSharedPreferences("setting", 0).edit().putBoolean("delete", bool.booleanValue()).commit();
    }

    public void a(String str) {
        String c2 = c();
        if (!c2.contains(str) && c2.length() < 30) {
            this.p = String.valueOf(c2) + str;
        }
    }

    public int b() {
        return this.x;
    }

    public void b(int i2) {
        this.r = i2;
    }

    public String c() {
        return this.p;
    }

    public Boolean d() {
        return Boolean.valueOf(this.y);
    }

    public int e() {
        return this.r;
    }

    public Boolean f() {
        return Boolean.valueOf(this.s);
    }

    public int g() {
        return this.t;
    }

    public int h() {
        return this.u;
    }

    public String i() {
        return this.o;
    }

    public String j() {
        return this.n;
    }

    public void onCreate() {
        v = getApplicationContext();
        this.u = 0;
        try {
            this.q = new SimpleDateFormat("MM-dd HH:mm:ss").format(new Date());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        SharedPreferences sharedPreferences = v.getSharedPreferences("setting", 0);
        String string = sharedPreferences.getString("UUID", "");
        if (string == "") {
            string = a.b(UUID.randomUUID().toString());
            sharedPreferences.edit().putString("UUID", string).commit();
        }
        this.o = string;
        String string2 = sharedPreferences.getString("user", "");
        if (string != "") {
            this.n = string2;
        }
        int i2 = sharedPreferences.getInt("money", 0);
        if (i2 != 0) {
            this.w = i2;
        }
        if (sharedPreferences.getBoolean("delete", false)) {
            this.s = true;
        } else {
            this.s = false;
        }
        this.y = sharedPreferences.getBoolean("finish", false);
        this.r = sharedPreferences.getInt("blo", 0);
        try {
            e a2 = e.a();
            a2.a(getApplicationContext());
            Thread.setDefaultUncaughtExceptionHandler(a2);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        try {
            getApplicationContext().startService(new Intent(getApplicationContext(), LocalService.class));
            k.a(getApplicationContext(), 1200);
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        try {
            if (!(sharedPreferences.getInt("pos", 0) == 0 || sharedPreferences.getLong("time", 0) == 0)) {
                k = 10;
            }
        } catch (Exception e5) {
            e5.printStackTrace();
        }
        super.onCreate();
    }
}
