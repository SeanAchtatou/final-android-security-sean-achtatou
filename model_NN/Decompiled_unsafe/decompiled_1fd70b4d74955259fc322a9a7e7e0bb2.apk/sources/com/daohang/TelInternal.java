package com.daohang;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.a.a.a.a;
import java.lang.reflect.Method;

public class TelInternal extends BroadcastReceiver {
    private static a a(Context context) {
        Method method;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        try {
            method = TelephonyManager.class.getDeclaredMethod("getITelephony", null);
            try {
                method.setAccessible(true);
            } catch (SecurityException e) {
                e = e;
                e.printStackTrace();
                return (a) method.invoke(telephonyManager, null);
            } catch (NoSuchMethodException e2) {
                e = e2;
                e.printStackTrace();
                return (a) method.invoke(telephonyManager, null);
            }
        } catch (SecurityException e3) {
            e = e3;
            method = null;
            e.printStackTrace();
            return (a) method.invoke(telephonyManager, null);
        } catch (NoSuchMethodException e4) {
            e = e4;
            method = null;
            e.printStackTrace();
            return (a) method.invoke(telephonyManager, null);
        }
        try {
            return (a) method.invoke(telephonyManager, null);
        } catch (Exception e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public void onReceive(Context context, Intent intent) {
        AudioManager audioManager = (AudioManager) context.getSystemService("audio");
        int e = ((DataApp) context.getApplicationContext()).e();
        if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
            getResultData();
            if (e == 2) {
                setResultData(null);
                return;
            }
            return;
        }
        String stringExtra = intent.getStringExtra("state");
        intent.getStringExtra("incoming_number");
        if (stringExtra.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_RINGING)) {
            Log.e("msg", "ring");
            if (e == 2) {
                audioManager.setRingerMode(0);
                try {
                    a(context).a();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
}
