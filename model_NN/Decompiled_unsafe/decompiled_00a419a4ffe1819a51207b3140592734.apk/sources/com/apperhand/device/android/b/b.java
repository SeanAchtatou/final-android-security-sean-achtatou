package com.apperhand.device.android.b;

import android.content.Context;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.device.a.c.a;
import com.apperhand.device.a.d.f;
import com.apperhand.device.android.c.c;
import com.apperhand.device.android.c.d;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.apache.http.message.BasicHeader;

/* compiled from: TransportHttpApache */
public final class b implements a {
    private final String a;
    private final Context b;
    private final com.apperhand.device.a.a c;
    private final com.apperhand.device.a.b d;

    public b(Context context, com.apperhand.device.a.a aVar, com.apperhand.device.a.b bVar, String str) {
        this.b = context;
        this.c = aVar;
        this.d = bVar;
        this.a = str;
    }

    public final <T extends BaseResponse> T a(Object obj, Command.Commands commands, Class<T> cls) throws f {
        String a2 = a.a(obj);
        ArrayList arrayList = new ArrayList();
        String a3 = d.a(this.b);
        try {
            a3 = URLEncoder.encode(a3, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        arrayList.add(new BasicHeader("device-id", a3));
        arrayList.add(new BasicHeader("protocol-version", this.c.b()));
        arrayList.add(new BasicHeader("User-Agent", this.c.c()));
        String e2 = this.d.e();
        if (e2 != null) {
            arrayList.add(new BasicHeader("ab-ts", e2));
        }
        byte[] bytes = a2.getBytes();
        if (this.a == null || this.a.equals("")) {
            throw new f(f.a.GENERAL_ERROR, "Unable to handle the command. The server url is not set correctly!!!");
        }
        String uri = commands.getUri();
        if (uri == null) {
            String str = new String(com.apperhand.device.a.d.b.a(com.apperhand.device.a.d.a.a(commands.getInternalUri())));
            commands.setUri(str);
            uri = str;
        }
        return (BaseResponse) a.a(c.a(this.a.endsWith("/") ? this.a.substring(0, this.a.length() - 1) + uri : this.a + uri, bytes, arrayList), cls);
    }
}
