package com.apperhand.device.android.c;

import android.content.Context;
import android.content.SharedPreferences;
import com.apperhand.device.a.d.d;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* compiled from: AndroidParametersManager */
public final class a implements d {
    private static final a a = new a();
    private Map<String, String> b = new HashMap();
    private Map<String, String> c = new HashMap();
    private ReadWriteLock d = new ReentrantReadWriteLock();

    private a() {
    }

    public static final a b() {
        return a;
    }

    public final Collection<String> a() {
        if (this.c != null) {
            return this.c.values();
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003c, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0040, code lost:
        throw r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(java.lang.String r4, java.lang.String r5) {
        /*
            r3 = this;
            java.util.concurrent.locks.ReadWriteLock r0 = r3.d
            java.util.concurrent.locks.Lock r0 = r0.readLock()
            r0.lock()
            java.util.Map<java.lang.String, java.lang.String> r1 = r3.b     // Catch:{ all -> 0x0041 }
            boolean r1 = r1.containsKey(r4)     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x001e
            java.util.Map<java.lang.String, java.lang.String> r1 = r3.b     // Catch:{ all -> 0x0041 }
            java.lang.Object r3 = r1.get(r4)     // Catch:{ all -> 0x0041 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x0041 }
            r1 = r3
        L_0x001a:
            r0.unlock()
            return r1
        L_0x001e:
            java.util.concurrent.locks.ReadWriteLock r1 = r3.d     // Catch:{ all -> 0x0041 }
            java.util.concurrent.locks.Lock r1 = r1.readLock()     // Catch:{ all -> 0x0041 }
            r1.lock()     // Catch:{ all -> 0x0041 }
            java.util.Map<java.lang.String, java.lang.String> r2 = r3.c     // Catch:{ all -> 0x003c }
            if (r2 != 0) goto L_0x0032
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ all -> 0x003c }
            r2.<init>()     // Catch:{ all -> 0x003c }
            r3.c = r2     // Catch:{ all -> 0x003c }
        L_0x0032:
            java.util.Map<java.lang.String, java.lang.String> r2 = r3.c     // Catch:{ all -> 0x003c }
            r2.put(r4, r4)     // Catch:{ all -> 0x003c }
            r1.unlock()     // Catch:{ all -> 0x0041 }
            r1 = r5
            goto L_0x001a
        L_0x003c:
            r2 = move-exception
            r1.unlock()     // Catch:{ all -> 0x0041 }
            throw r2     // Catch:{ all -> 0x0041 }
        L_0x0041:
            r1 = move-exception
            r0.unlock()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.apperhand.device.android.c.a.a(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX INFO: finally extract failed */
    public final boolean b(String str, String str2) {
        Lock readLock = this.d.readLock();
        readLock.lock();
        try {
            if (this.b == null) {
                this.b = new HashMap();
            }
            this.b.put(str, str2);
            readLock.unlock();
            if (this.c == null || this.c.size() <= 0) {
                return true;
            }
            this.c.remove(str);
            return true;
        } catch (Throwable th) {
            readLock.unlock();
            throw th;
        }
    }

    public final void a(Context context) {
        Lock writeLock = this.d.writeLock();
        writeLock.lock();
        try {
            this.b = context.getSharedPreferences("com.apperhand.parameters", 0).getAll();
        } finally {
            writeLock.unlock();
        }
    }

    public final void b(Context context) {
        Lock writeLock = this.d.writeLock();
        writeLock.lock();
        try {
            SharedPreferences.Editor edit = context.getSharedPreferences("com.apperhand.parameters", 0).edit();
            for (String next : this.b.keySet()) {
                edit.putString(next, this.b.get(next));
            }
            edit.commit();
        } finally {
            writeLock.unlock();
        }
    }
}
