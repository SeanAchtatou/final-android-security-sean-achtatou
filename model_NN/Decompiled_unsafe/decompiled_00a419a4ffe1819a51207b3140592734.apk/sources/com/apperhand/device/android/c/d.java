package com.apperhand.device.android.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import com.apperhand.device.a.d.b;
import java.util.UUID;

/* compiled from: Utils */
public final class d {
    public static String a(Context context) {
        String str;
        String str2;
        String str3;
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.apperhand.global", 0);
        String string = sharedPreferences.getString("ENC_DEVICE_ID", null);
        if (string != null) {
            return string;
        }
        String string2 = sharedPreferences.getString("DEVICE_ID", null);
        if (string2 != null) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            String b = b.b(string2);
            edit.putString("ENC_DEVICE_ID", b);
            edit.remove("DEVICE_ID");
            edit.commit();
            return b;
        }
        String string3 = sharedPreferences.getString("ENC_DUMMY_ID", null);
        if (string3 != null) {
            return string3;
        }
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            str = telephonyManager == null ? null : telephonyManager.getDeviceId();
            if (str == null || str.trim().equals("") || str.equalsIgnoreCase("NULL")) {
                str = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
            }
        } catch (Exception e) {
            str = null;
        }
        if (str == null || str.trim().equals("") || str.equalsIgnoreCase("NULL")) {
            str2 = "kaka" + UUID.randomUUID().toString();
        } else {
            str2 = null;
        }
        if (str2 == null) {
            str3 = b.b(str);
        } else {
            str3 = str2;
        }
        SharedPreferences.Editor edit2 = sharedPreferences.edit();
        edit2.putString("ENC_DUMMY_ID", str3);
        edit2.commit();
        return str3;
    }
}
