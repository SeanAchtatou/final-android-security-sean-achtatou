package com.apperhand.device.android.a;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import com.apperhand.common.dto.AssetInformation;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandInformation;
import com.apperhand.common.dto.Shortcut;
import com.apperhand.device.a.d.b;
import com.apperhand.device.a.d.f;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: AndroidShortcutsDMA */
public final class d implements com.apperhand.device.a.a.d {
    private static final Map<String, String> a;
    private Context b;
    private a c = null;
    private List<a> d = new ArrayList();

    static {
        HashMap hashMap = new HashMap();
        a = hashMap;
        hashMap.put("com.motorola.blur.home", "com.android.launcher");
    }

    /* compiled from: AndroidShortcutsDMA */
    static class a {
        public String a;
        public String b;
        public boolean c;

        a() {
        }

        public final String toString() {
            return "LauncherNames [packageName=" + this.a + ", name=" + this.b + ", default4User=" + this.c + "]";
        }
    }

    public final void a() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        PackageManager packageManager = this.b.getPackageManager();
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
        this.d.clear();
        this.c = null;
        for (ResolveInfo resolveInfo : queryIntentActivities) {
            a aVar = new a();
            this.d.add(aVar);
            ActivityInfo activityInfo = resolveInfo.activityInfo;
            String str = a.get(activityInfo.packageName);
            if (str != null) {
                aVar.a = str;
            } else {
                aVar.a = activityInfo.packageName;
            }
            ArrayList arrayList = new ArrayList();
            packageManager.getPreferredActivities(new ArrayList(), arrayList, activityInfo.packageName);
            aVar.c = arrayList.size() > 0;
            if (aVar.c) {
                this.c = aVar;
            }
            String str2 = activityInfo.name;
            int lastIndexOf = str2.lastIndexOf(".");
            if (lastIndexOf > 0) {
                aVar.b = str2.substring(0, lastIndexOf);
            }
        }
        if (this.d.size() == 1) {
            this.c = this.d.get(0);
            this.c.c = true;
        }
        List<a> list = this.d;
    }

    public d(Context context) {
        this.b = context;
    }

    public final CommandInformation a(List<String> list) {
        a aVar;
        String str;
        CommandInformation commandInformation = new CommandInformation(Command.Commands.SHORTCUTS);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Launchers = [").append(this.d.toString()).append("]#NL#");
        ArrayList arrayList = new ArrayList();
        commandInformation.setAssets(arrayList);
        commandInformation.setValid(true);
        List<Cursor> d2 = d();
        try {
            int i = 0;
            for (Cursor next : d2) {
                aVar = this.d.get(i);
                if (next != null) {
                    List<Shortcut> a2 = a(next);
                    if (a2 == null) {
                        stringBuffer.append("Success reading cursor of ").append(aVar).append(", but the cursor is empty#NL#");
                        commandInformation.setValid(false);
                    } else {
                        String[] columnNames = next.getColumnNames();
                        if (columnNames != null) {
                            str = Arrays.asList(columnNames).toString();
                        } else {
                            str = "Unknown";
                        }
                        stringBuffer.append("Success reading cursor of ").append(aVar).append(" with columns ").append(str).append("#NL#");
                        for (Shortcut next2 : a2) {
                            for (String next3 : list) {
                                String link = next2.getLink();
                                if (!(link == null || link.indexOf(next3) == -1)) {
                                    AssetInformation assetInformation = new AssetInformation();
                                    assetInformation.setUrl(link);
                                    assetInformation.setPosition(next2.getScreen());
                                    assetInformation.setState(AssetInformation.State.EXIST);
                                    arrayList.add(assetInformation);
                                    HashMap hashMap = new HashMap();
                                    hashMap.put("Launcher", aVar);
                                    assetInformation.setParameters(hashMap);
                                }
                            }
                        }
                    }
                } else {
                    stringBuffer.append("Couldn't check ").append(aVar.toString()).append("#NL#");
                    commandInformation.setValid(false);
                }
                i++;
            }
            b(d2);
            commandInformation.setMessage(stringBuffer.toString());
            return commandInformation;
        } catch (Exception e) {
            stringBuffer.append("Error reading cursor of ").append(aVar).append(e.getMessage()).append("#NL#");
            commandInformation.setValid(false);
        } catch (Throwable th) {
            b(d2);
            commandInformation.setMessage(stringBuffer.toString());
            throw th;
        }
    }

    public final String c() {
        return this.d.toString();
    }

    public final boolean b() {
        boolean z;
        if (this.c != null) {
            Cursor a2 = a(this.c);
            if (a2 == null) {
                return false;
            }
            a2.close();
            return true;
        }
        List<Cursor> d2 = d();
        if (d2.size() <= 0) {
            return false;
        }
        Iterator<Cursor> it = d2.iterator();
        while (true) {
            if (it.hasNext()) {
                if (it.next() == null) {
                    z = false;
                    break;
                }
            } else {
                z = true;
                break;
            }
        }
        b(d2);
        return z;
    }

    /* JADX INFO: finally extract failed */
    public final boolean b(Shortcut shortcut) throws f {
        boolean z;
        boolean z2;
        if (this.c != null) {
            Cursor a2 = a(this.c);
            if (a2 == null) {
                return false;
            }
            try {
                z2 = a(a2, shortcut);
            } catch (Exception e) {
                z2 = true;
            }
            a2.close();
            return z2;
        }
        List<Cursor> d2 = d();
        try {
            for (Cursor a3 : d2) {
                try {
                    z = a(a3, shortcut);
                    continue;
                } catch (Exception e2) {
                    z = true;
                    continue;
                }
                if (z) {
                    b(d2);
                    return true;
                }
            }
            b(d2);
            return false;
        } catch (Throwable th) {
            b(d2);
            throw th;
        }
    }

    public final int a(String str) {
        Cursor a2;
        int i;
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }
        if (this.c == null || (a2 = a(this.c)) == null) {
            return -999;
        }
        if (a2 != null) {
            try {
                int columnIndex = a2.getColumnIndex("screen");
                int columnIndex2 = a2.getColumnIndex("title");
                while (true) {
                    if (!a2.moveToNext()) {
                        break;
                    }
                    String string = a2.getString(columnIndex2);
                    if (string != null && string.equals(str)) {
                        i = a2.getInt(columnIndex);
                        break;
                    }
                }
            } catch (Exception e2) {
                a2.close();
                return -999;
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        }
        i = -999;
        a2.close();
        return i;
    }

    private static String b(String str) {
        return "content://" + str + ".settings/favorites?notify=false";
    }

    private static boolean a(Cursor cursor, Shortcut shortcut) throws Exception {
        if (cursor == null) {
            return false;
        }
        int columnIndex = cursor.getColumnIndex("intent");
        String a2 = b.a(shortcut.getLink());
        while (cursor.moveToNext()) {
            String string = cursor.getString(columnIndex);
            if (string != null && string.indexOf(a2) >= 0) {
                return true;
            }
        }
        return false;
    }

    private Cursor a(a aVar) {
        Cursor cursor = null;
        if (aVar == null) {
            return null;
        }
        try {
            cursor = this.b.getContentResolver().query(Uri.parse(b(aVar.a)), null, null, null, null);
        } catch (SecurityException e) {
        }
        if (cursor == null && aVar.b != null && !aVar.b.equals(aVar.a)) {
            try {
                return this.b.getContentResolver().query(Uri.parse(b(aVar.b)), null, null, null, null);
            } catch (SecurityException e2) {
            }
        }
        return cursor;
    }

    private List<Cursor> d() {
        ArrayList arrayList = new ArrayList();
        for (a a2 : this.d) {
            arrayList.add(a(a2));
        }
        return arrayList;
    }

    private static void b(List<Cursor> list) {
        for (Cursor next : list) {
            if (next != null) {
                next.close();
            }
        }
    }

    private static List<Shortcut> a(Cursor cursor) throws Exception {
        ArrayList arrayList = new ArrayList();
        try {
            int columnIndex = cursor.getColumnIndex("_id");
            int columnIndex2 = cursor.getColumnIndex("title");
            int columnIndex3 = cursor.getColumnIndex("intent");
            int columnIndex4 = cursor.getColumnIndex("screen");
            while (cursor.moveToNext()) {
                Shortcut shortcut = new Shortcut();
                shortcut.setId(cursor.getLong(columnIndex));
                shortcut.setName(cursor.getString(columnIndex2));
                String string = cursor.getString(columnIndex3);
                if (string != null && !string.equals("") && string.indexOf("#") > 0) {
                    string = string.substring(0, string.indexOf("#"));
                }
                shortcut.setLink(string);
                shortcut.setScreen(cursor.getInt(columnIndex4));
                arrayList.add(shortcut);
            }
            return arrayList;
        } finally {
            cursor.close();
        }
    }

    public final void a(Shortcut shortcut) {
        String name = shortcut.getName();
        String link = shortcut.getLink();
        byte[] icon = shortcut.getIcon();
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(link));
        intent.addFlags(268435456);
        intent.addFlags(67108864);
        Intent intent2 = new Intent();
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        intent2.putExtra("android.intent.extra.shortcut.NAME", name);
        if (icon == null) {
            byte[] bArr = new byte[0];
            intent2.putExtra("android.intent.extra.shortcut.ICON", BitmapFactory.decodeByteArray(bArr, 0, bArr.length));
        } else {
            intent2.putExtra("android.intent.extra.shortcut.ICON", BitmapFactory.decodeByteArray(icon, 0, icon.length));
        }
        intent2.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        this.b.sendBroadcast(intent2);
    }
}
