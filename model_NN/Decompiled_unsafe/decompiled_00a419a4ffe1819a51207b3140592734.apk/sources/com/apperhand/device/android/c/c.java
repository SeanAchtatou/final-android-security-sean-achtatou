package com.apperhand.device.android.c;

import org.apache.http.HttpResponse;
import org.apache.http.message.AbstractHttpMessage;

/* compiled from: NetworkUtils */
public final class c {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.apperhand.device.android.c.c.a(java.lang.String, org.apache.http.message.AbstractHttpMessage, org.apache.http.HttpResponse):java.lang.StringBuilder
     arg types: [java.lang.String, org.apache.http.client.methods.HttpPost, org.apache.http.HttpResponse]
     candidates:
      com.apperhand.device.android.c.c.a(java.lang.String, byte[], java.util.List<org.apache.http.Header>):java.lang.String
      com.apperhand.device.android.c.c.a(java.lang.String, org.apache.http.message.AbstractHttpMessage, org.apache.http.HttpResponse):java.lang.StringBuilder */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x00b1=Splitter:B:23:0x00b1, B:61:0x016c=Splitter:B:61:0x016c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r10, byte[] r11, java.util.List<org.apache.http.Header> r12) throws com.apperhand.device.a.d.f {
        /*
            r1 = 60000(0xea60, float:8.4078E-41)
            r8 = 0
            r6 = 0
            org.apache.http.params.BasicHttpParams r0 = new org.apache.http.params.BasicHttpParams
            r0.<init>()
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r1)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r1)
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient
            r1.<init>(r0)
            com.apperhand.device.android.c.c$1 r0 = new com.apperhand.device.android.c.c$1
            r0.<init>()
            r1.setHttpRequestRetryHandler(r0)
            org.apache.http.client.methods.HttpPost r2 = new org.apache.http.client.methods.HttpPost
            r2.<init>(r10)
            if (r12 == 0) goto L_0x0038
            java.util.Iterator r3 = r12.iterator()
        L_0x0028:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0038
            java.lang.Object r0 = r3.next()
            org.apache.http.Header r0 = (org.apache.http.Header) r0
            r2.setHeader(r0)
            goto L_0x0028
        L_0x0038:
            org.apache.http.params.HttpParams r0 = r2.getParams()
            java.lang.String r3 = "http.protocol.expect-continue"
            r0.setBooleanParameter(r3, r6)
            java.lang.String r0 = "Content-Type"
            java.lang.String r3 = "application/json"
            r2.setHeader(r0, r3)
            java.lang.String r0 = "Accept-Encoding"
            java.lang.String r3 = "gzip"
            r2.setHeader(r0, r3)
            java.lang.String r0 = "Accept"
            java.lang.String r3 = "application/json"
            r2.setHeader(r0, r3)
            int r0 = r11.length     // Catch:{ IOException -> 0x0107 }
            r3 = 2048(0x800, float:2.87E-42)
            if (r0 >= r3) goto L_0x00e6
            org.apache.http.entity.ByteArrayEntity r0 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ IOException -> 0x0107 }
            r0.<init>(r11)     // Catch:{ IOException -> 0x0107 }
        L_0x0060:
            r2.setEntity(r0)     // Catch:{ IOException -> 0x0107 }
        L_0x0063:
            org.apache.http.HttpResponse r0 = r1.execute(r2)     // Catch:{ IOException -> 0x01b3, RuntimeException -> 0x01b0 }
            org.apache.http.HttpEntity r3 = r0.getEntity()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            org.apache.http.StatusLine r4 = r0.getStatusLine()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            int r4 = r4.getStatusCode()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r4 == r5) goto L_0x0112
            if (r3 == 0) goto L_0x007c
            r3.consumeContent()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
        L_0x007c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            r3.<init>()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            java.lang.String r4 = "Status code is "
            java.lang.StringBuilder r4 = r3.append(r4)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            org.apache.http.StatusLine r5 = r0.getStatusLine()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            int r5 = r5.getStatusCode()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            java.lang.String r5 = ", "
            r4.append(r5)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            java.lang.StringBuilder r4 = a(r10, r2, r0)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            r3.append(r4)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            com.apperhand.device.a.d.f r4 = new com.apperhand.device.a.d.f     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            com.apperhand.device.a.d.f$a r5 = com.apperhand.device.a.d.f.a.GENERAL_ERROR     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            r6 = 0
            r7 = 0
            r4.<init>(r5, r3, r6, r7)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            throw r4     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
        L_0x00ad:
            r3 = move-exception
            r9 = r3
            r3 = r0
            r0 = r9
        L_0x00b1:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00dd }
            r4.<init>()     // Catch:{ all -> 0x00dd }
            java.lang.String r5 = "Error execute Exception "
            java.lang.StringBuilder r5 = r4.append(r5)     // Catch:{ all -> 0x00dd }
            java.lang.String r6 = r0.getMessage()     // Catch:{ all -> 0x00dd }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00dd }
            java.lang.String r6 = ", "
            r5.append(r6)     // Catch:{ all -> 0x00dd }
            java.lang.StringBuilder r2 = a(r10, r2, r3)     // Catch:{ all -> 0x00dd }
            r4.append(r2)     // Catch:{ all -> 0x00dd }
            com.apperhand.device.a.d.f r2 = new com.apperhand.device.a.d.f     // Catch:{ all -> 0x00dd }
            com.apperhand.device.a.d.f$a r3 = com.apperhand.device.a.d.f.a.GENERAL_ERROR     // Catch:{ all -> 0x00dd }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00dd }
            r5 = 0
            r2.<init>(r3, r4, r0, r5)     // Catch:{ all -> 0x00dd }
            throw r2     // Catch:{ all -> 0x00dd }
        L_0x00dd:
            r0 = move-exception
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
            throw r0
        L_0x00e6:
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0107 }
            r0.<init>()     // Catch:{ IOException -> 0x0107 }
            java.util.zip.GZIPOutputStream r3 = new java.util.zip.GZIPOutputStream     // Catch:{ IOException -> 0x0107 }
            r3.<init>(r0)     // Catch:{ IOException -> 0x0107 }
            r3.write(r11)     // Catch:{ IOException -> 0x0107 }
            r3.close()     // Catch:{ IOException -> 0x0107 }
            org.apache.http.entity.ByteArrayEntity r3 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ IOException -> 0x0107 }
            byte[] r0 = r0.toByteArray()     // Catch:{ IOException -> 0x0107 }
            r3.<init>(r0)     // Catch:{ IOException -> 0x0107 }
            java.lang.String r0 = "gzip"
            r3.setContentEncoding(r0)     // Catch:{ IOException -> 0x0107 }
            r0 = r3
            goto L_0x0060
        L_0x0107:
            r0 = move-exception
            org.apache.http.entity.ByteArrayEntity r0 = new org.apache.http.entity.ByteArrayEntity
            r0.<init>(r11)
            r2.setEntity(r0)
            goto L_0x0063
        L_0x0112:
            if (r3 == 0) goto L_0x01c0
            java.io.InputStream r4 = r3.getContent()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            org.apache.http.Header r3 = r3.getContentEncoding()     // Catch:{ all -> 0x01b7 }
            if (r3 == 0) goto L_0x01bd
            if (r4 == 0) goto L_0x01bd
            org.apache.http.HeaderElement[] r3 = r3.getElements()     // Catch:{ all -> 0x01b7 }
            r5 = r6
        L_0x0125:
            int r6 = r3.length     // Catch:{ all -> 0x01b7 }
            if (r5 >= r6) goto L_0x01bd
            r6 = r3[r5]     // Catch:{ all -> 0x01b7 }
            java.lang.String r6 = r6.getName()     // Catch:{ all -> 0x01b7 }
            java.lang.String r7 = "gzip"
            boolean r6 = r6.equalsIgnoreCase(r7)     // Catch:{ all -> 0x01b7 }
            if (r6 == 0) goto L_0x019b
            java.util.zip.GZIPInputStream r3 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x01b7 }
            r3.<init>(r4)     // Catch:{ all -> 0x01b7 }
        L_0x013b:
            if (r3 == 0) goto L_0x01bb
            java.io.StringWriter r4 = new java.io.StringWriter     // Catch:{ all -> 0x015e }
            r4.<init>()     // Catch:{ all -> 0x015e }
            r5 = 1024(0x400, float:1.435E-42)
            char[] r5 = new char[r5]     // Catch:{ all -> 0x015e }
            java.io.BufferedReader r6 = new java.io.BufferedReader     // Catch:{ all -> 0x015e }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ all -> 0x015e }
            java.lang.String r8 = "UTF-8"
            r7.<init>(r3, r8)     // Catch:{ all -> 0x015e }
            r6.<init>(r7)     // Catch:{ all -> 0x015e }
        L_0x0152:
            int r7 = r6.read(r5)     // Catch:{ all -> 0x015e }
            r8 = -1
            if (r7 == r8) goto L_0x019e
            r8 = 0
            r4.write(r5, r8, r7)     // Catch:{ all -> 0x015e }
            goto L_0x0152
        L_0x015e:
            r4 = move-exception
            r9 = r4
            r4 = r3
            r3 = r9
        L_0x0162:
            if (r4 == 0) goto L_0x0167
            r4.close()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
        L_0x0167:
            throw r3     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
        L_0x0168:
            r3 = move-exception
            r9 = r3
            r3 = r0
            r0 = r9
        L_0x016c:
            r2.abort()     // Catch:{ all -> 0x00dd }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00dd }
            r4.<init>()     // Catch:{ all -> 0x00dd }
            java.lang.String r5 = "Error execute Exception "
            java.lang.StringBuilder r5 = r4.append(r5)     // Catch:{ all -> 0x00dd }
            java.lang.String r6 = r0.getMessage()     // Catch:{ all -> 0x00dd }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00dd }
            java.lang.String r6 = ", "
            r5.append(r6)     // Catch:{ all -> 0x00dd }
            java.lang.StringBuilder r2 = a(r10, r2, r3)     // Catch:{ all -> 0x00dd }
            r4.append(r2)     // Catch:{ all -> 0x00dd }
            com.apperhand.device.a.d.f r2 = new com.apperhand.device.a.d.f     // Catch:{ all -> 0x00dd }
            com.apperhand.device.a.d.f$a r3 = com.apperhand.device.a.d.f.a.GENERAL_ERROR     // Catch:{ all -> 0x00dd }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00dd }
            r5 = 0
            r2.<init>(r3, r4, r0, r5)     // Catch:{ all -> 0x00dd }
            throw r2     // Catch:{ all -> 0x00dd }
        L_0x019b:
            int r5 = r5 + 1
            goto L_0x0125
        L_0x019e:
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x015e }
        L_0x01a2:
            if (r3 == 0) goto L_0x01b9
            r3.close()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0168 }
            r0 = r4
        L_0x01a8:
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
            return r0
        L_0x01b0:
            r0 = move-exception
            r3 = r8
            goto L_0x016c
        L_0x01b3:
            r0 = move-exception
            r3 = r8
            goto L_0x00b1
        L_0x01b7:
            r3 = move-exception
            goto L_0x0162
        L_0x01b9:
            r0 = r4
            goto L_0x01a8
        L_0x01bb:
            r4 = r8
            goto L_0x01a2
        L_0x01bd:
            r3 = r4
            goto L_0x013b
        L_0x01c0:
            r0 = r8
            goto L_0x01a8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.apperhand.device.android.c.c.a(java.lang.String, byte[], java.util.List):java.lang.String");
    }

    private static StringBuilder a(String str, AbstractHttpMessage abstractHttpMessage, HttpResponse httpResponse) {
        StringBuilder sb = new StringBuilder();
        if (str != null) {
            sb.append("address = [").append(str).append("],");
        }
        if (!(abstractHttpMessage == null || abstractHttpMessage.getAllHeaders() == null)) {
            sb.append("Headers = [").append(abstractHttpMessage.getAllHeaders()).append("],");
        }
        if (!(httpResponse == null || httpResponse.getStatusLine() == null)) {
            sb.append("statusLine = [").append(httpResponse.getStatusLine()).append("]");
        }
        return sb;
    }
}
