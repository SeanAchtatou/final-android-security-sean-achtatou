package com.apperhand.device.a.d;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: GeneralUtils */
public final class b {
    private static final byte[] a = {97, 110, 100, 114, 111, 105, 100, 46, 105, 110, 116, 101, 110, 116, 46, 98, 114, 111, 119, 115, 101, 114, 46, 83, 69, 84, 95, 72, 79, 77, 69, 80, 65, 71, 69};

    public static String a(String str) {
        int indexOf;
        if (str == null || str.equals("") || (indexOf = str.indexOf("?")) <= 0) {
            return str;
        }
        return str.substring(0, indexOf);
    }

    public static String b(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.reset();
            return a.a(instance.digest(str.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            return str;
        }
    }

    public static byte[] a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        byte[] bArr2 = a;
        int length = bArr.length;
        byte[] bArr3 = new byte[length];
        int length2 = bArr2.length;
        int i = 0;
        int i2 = 0;
        while (i2 < length) {
            if (i >= length2) {
                i = 0;
            }
            bArr3[i2] = (byte) (bArr[i2] ^ bArr2[i]);
            i2++;
            i++;
        }
        return bArr3;
    }
}
