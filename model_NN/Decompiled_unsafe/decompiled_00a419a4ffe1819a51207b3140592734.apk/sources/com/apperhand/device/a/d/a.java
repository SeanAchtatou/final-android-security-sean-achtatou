package com.apperhand.device.a.d;

import java.io.UnsupportedEncodingException;
import org.codehaus.jackson.impl.JsonWriteContext;

/* compiled from: Base64 */
public class a {
    static final /* synthetic */ boolean a = (!a.class.desiredAssertionStatus());

    /* renamed from: com.apperhand.device.a.d.a$a  reason: collision with other inner class name */
    /* compiled from: Base64 */
    static abstract class C0000a {
        public byte[] a;
        public int b;

        C0000a() {
        }
    }

    public static byte[] a(String str) {
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        b bVar = new b(new byte[((length * 3) / 4)]);
        if (!bVar.a(bytes, length)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (bVar.b == bVar.a.length) {
            return bVar.a;
        } else {
            byte[] bArr = new byte[bVar.b];
            System.arraycopy(bVar.a, 0, bArr, 0, bVar.b);
            return bArr;
        }
    }

    /* compiled from: Base64 */
    static class b extends C0000a {
        private static final int[] c = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
        private static final int[] d = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
        private int e = 0;
        private int f = 0;
        private final int[] g = c;

        public b(byte[] bArr) {
            this.a = bArr;
        }

        /* JADX WARNING: Removed duplicated region for block: B:53:0x0113  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x0119  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x0122  */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x0131  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean a(byte[] r12, int r13) {
            /*
                r11 = this;
                int r0 = r11.e
                r1 = 6
                if (r0 != r1) goto L_0x0007
                r0 = 0
            L_0x0006:
                return r0
            L_0x0007:
                r0 = 0
                int r1 = r13 + 0
                int r2 = r11.e
                int r3 = r11.f
                r4 = 0
                byte[] r5 = r11.a
                int[] r6 = r11.g
                r9 = r4
                r4 = r0
                r0 = r9
                r10 = r2
                r2 = r3
                r3 = r10
            L_0x0019:
                if (r4 >= r1) goto L_0x0108
                if (r3 != 0) goto L_0x0062
            L_0x001d:
                int r7 = r4 + 4
                if (r7 > r1) goto L_0x0060
                byte r2 = r12[r4]
                r2 = r2 & 255(0xff, float:3.57E-43)
                r2 = r6[r2]
                int r2 = r2 << 18
                int r7 = r4 + 1
                byte r7 = r12[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                int r7 = r7 << 12
                r2 = r2 | r7
                int r7 = r4 + 2
                byte r7 = r12[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                int r7 = r7 << 6
                r2 = r2 | r7
                int r7 = r4 + 3
                byte r7 = r12[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                r2 = r2 | r7
                if (r2 < 0) goto L_0x0060
                int r7 = r0 + 2
                byte r8 = (byte) r2
                r5[r7] = r8
                int r7 = r0 + 1
                int r8 = r2 >> 8
                byte r8 = (byte) r8
                r5[r7] = r8
                int r7 = r2 >> 16
                byte r7 = (byte) r7
                r5[r0] = r7
                int r0 = r0 + 3
                int r4 = r4 + 4
                goto L_0x001d
            L_0x0060:
                if (r4 >= r1) goto L_0x0108
            L_0x0062:
                int r7 = r4 + 1
                byte r4 = r12[r4]
                r4 = r4 & 255(0xff, float:3.57E-43)
                r4 = r6[r4]
                switch(r3) {
                    case 0: goto L_0x006f;
                    case 1: goto L_0x007f;
                    case 2: goto L_0x0091;
                    case 3: goto L_0x00b4;
                    case 4: goto L_0x00ee;
                    case 5: goto L_0x00ff;
                    default: goto L_0x006d;
                }
            L_0x006d:
                r4 = r7
                goto L_0x0019
            L_0x006f:
                if (r4 < 0) goto L_0x0077
                int r2 = r3 + 1
                r3 = r2
                r2 = r4
                r4 = r7
                goto L_0x0019
            L_0x0077:
                r8 = -1
                if (r4 == r8) goto L_0x006d
                r0 = 6
                r11.e = r0
                r0 = 0
                goto L_0x0006
            L_0x007f:
                if (r4 < 0) goto L_0x0088
                int r2 = r2 << 6
                r2 = r2 | r4
                int r3 = r3 + 1
                r4 = r7
                goto L_0x0019
            L_0x0088:
                r8 = -1
                if (r4 == r8) goto L_0x006d
                r0 = 6
                r11.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0091:
                if (r4 < 0) goto L_0x009a
                int r2 = r2 << 6
                r2 = r2 | r4
                int r3 = r3 + 1
                r4 = r7
                goto L_0x0019
            L_0x009a:
                r8 = -2
                if (r4 != r8) goto L_0x00ab
                int r3 = r0 + 1
                int r4 = r2 >> 4
                byte r4 = (byte) r4
                r5[r0] = r4
                r0 = 4
                r4 = r7
                r9 = r0
                r0 = r3
                r3 = r9
                goto L_0x0019
            L_0x00ab:
                r8 = -1
                if (r4 == r8) goto L_0x006d
                r0 = 6
                r11.e = r0
                r0 = 0
                goto L_0x0006
            L_0x00b4:
                if (r4 < 0) goto L_0x00d0
                int r2 = r2 << 6
                r2 = r2 | r4
                int r3 = r0 + 2
                byte r4 = (byte) r2
                r5[r3] = r4
                int r3 = r0 + 1
                int r4 = r2 >> 8
                byte r4 = (byte) r4
                r5[r3] = r4
                int r3 = r2 >> 16
                byte r3 = (byte) r3
                r5[r0] = r3
                int r0 = r0 + 3
                r3 = 0
                r4 = r7
                goto L_0x0019
            L_0x00d0:
                r8 = -2
                if (r4 != r8) goto L_0x00e5
                int r3 = r0 + 1
                int r4 = r2 >> 2
                byte r4 = (byte) r4
                r5[r3] = r4
                int r3 = r2 >> 10
                byte r3 = (byte) r3
                r5[r0] = r3
                int r0 = r0 + 2
                r3 = 5
                r4 = r7
                goto L_0x0019
            L_0x00e5:
                r8 = -1
                if (r4 == r8) goto L_0x006d
                r0 = 6
                r11.e = r0
                r0 = 0
                goto L_0x0006
            L_0x00ee:
                r8 = -2
                if (r4 != r8) goto L_0x00f6
                int r3 = r3 + 1
                r4 = r7
                goto L_0x0019
            L_0x00f6:
                r8 = -1
                if (r4 == r8) goto L_0x006d
                r0 = 6
                r11.e = r0
                r0 = 0
                goto L_0x0006
            L_0x00ff:
                r8 = -1
                if (r4 == r8) goto L_0x006d
                r0 = 6
                r11.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0108:
                r1 = r2
                switch(r3) {
                    case 0: goto L_0x010c;
                    case 1: goto L_0x0113;
                    case 2: goto L_0x0119;
                    case 3: goto L_0x0122;
                    case 4: goto L_0x0131;
                    default: goto L_0x010c;
                }
            L_0x010c:
                r11.e = r3
                r11.b = r0
                r0 = 1
                goto L_0x0006
            L_0x0113:
                r0 = 6
                r11.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0119:
                int r2 = r0 + 1
                int r1 = r1 >> 4
                byte r1 = (byte) r1
                r5[r0] = r1
                r0 = r2
                goto L_0x010c
            L_0x0122:
                int r2 = r0 + 1
                int r4 = r1 >> 10
                byte r4 = (byte) r4
                r5[r0] = r4
                int r0 = r2 + 1
                int r1 = r1 >> 2
                byte r1 = (byte) r1
                r5[r2] = r1
                goto L_0x010c
            L_0x0131:
                r0 = 6
                r11.e = r0
                r0 = 0
                goto L_0x0006
            */
            throw new UnsupportedOperationException("Method not decompiled: com.apperhand.device.a.d.a.b.a(byte[], int):boolean");
        }
    }

    public static String a(byte[] bArr) {
        try {
            int length = bArr.length;
            c cVar = new c();
            int i = (length / 3) * 4;
            if (!cVar.d) {
                switch (length % 3) {
                    case JsonWriteContext.STATUS_OK_AFTER_COMMA /*1*/:
                        i += 2;
                        break;
                    case JsonWriteContext.STATUS_OK_AFTER_COLON /*2*/:
                        i += 3;
                        break;
                }
            } else if (length % 3 > 0) {
                i += 4;
            }
            if (cVar.e && length > 0) {
                i += (((length - 1) / 57) + 1) * (cVar.f ? 2 : 1);
            }
            cVar.a = new byte[i];
            cVar.a(bArr, length);
            if (a || cVar.b == i) {
                return new String(cVar.a, "US-ASCII");
            }
            throw new AssertionError();
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    /* compiled from: Base64 */
    static class c extends C0000a {
        static final /* synthetic */ boolean g;
        private static final byte[] h = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        private static final byte[] i = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        int c = 0;
        public final boolean d = true;
        public final boolean e = false;
        public final boolean f = false;
        private final byte[] j = new byte[2];
        private int k;
        private final byte[] l = h;

        static {
            boolean z;
            if (!a.class.desiredAssertionStatus()) {
                z = true;
            } else {
                z = false;
            }
            g = z;
        }

        public c() {
            this.a = null;
            this.k = this.e ? 19 : -1;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x005e  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0109  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0170  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean a(byte[] r12, int r13) {
            /*
                r11 = this;
                byte[] r0 = r11.l
                byte[] r1 = r11.a
                r2 = 0
                int r3 = r11.k
                r4 = 0
                int r5 = r13 + 0
                r6 = -1
                int r7 = r11.c
                switch(r7) {
                    case 0: goto L_0x00b3;
                    case 1: goto L_0x00b8;
                    case 2: goto L_0x00de;
                    default: goto L_0x0010;
                }
            L_0x0010:
                r10 = r6
                r6 = r4
                r4 = r10
            L_0x0013:
                r7 = -1
                if (r4 == r7) goto L_0x022a
                r7 = 0
                int r2 = r2 + 1
                int r8 = r4 >> 18
                r8 = r8 & 63
                byte r8 = r0[r8]
                r1[r7] = r8
                r7 = 1
                int r2 = r2 + 1
                int r8 = r4 >> 12
                r8 = r8 & 63
                byte r8 = r0[r8]
                r1[r7] = r8
                r7 = 2
                int r2 = r2 + 1
                int r8 = r4 >> 6
                r8 = r8 & 63
                byte r8 = r0[r8]
                r1[r7] = r8
                r7 = 3
                int r2 = r2 + 1
                r4 = r4 & 63
                byte r4 = r0[r4]
                r1[r7] = r4
                int r3 = r3 + -1
                if (r3 != 0) goto L_0x022a
                boolean r3 = r11.f
                if (r3 == 0) goto L_0x004f
                r3 = 4
                int r2 = r2 + 1
                r4 = 13
                r1[r3] = r4
            L_0x004f:
                int r3 = r2 + 1
                r4 = 10
                r1[r2] = r4
                r2 = 19
                r4 = r3
                r3 = r2
                r2 = r6
            L_0x005a:
                int r6 = r2 + 3
                if (r6 > r5) goto L_0x0100
                byte r6 = r12[r2]
                r6 = r6 & 255(0xff, float:3.57E-43)
                int r6 = r6 << 16
                int r7 = r2 + 1
                byte r7 = r12[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                int r7 = r7 << 8
                r6 = r6 | r7
                int r7 = r2 + 2
                byte r7 = r12[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r6 = r6 | r7
                int r7 = r6 >> 18
                r7 = r7 & 63
                byte r7 = r0[r7]
                r1[r4] = r7
                int r7 = r4 + 1
                int r8 = r6 >> 12
                r8 = r8 & 63
                byte r8 = r0[r8]
                r1[r7] = r8
                int r7 = r4 + 2
                int r8 = r6 >> 6
                r8 = r8 & 63
                byte r8 = r0[r8]
                r1[r7] = r8
                int r7 = r4 + 3
                r6 = r6 & 63
                byte r6 = r0[r6]
                r1[r7] = r6
                int r2 = r2 + 3
                int r4 = r4 + 4
                int r3 = r3 + -1
                if (r3 != 0) goto L_0x005a
                boolean r3 = r11.f
                if (r3 == 0) goto L_0x0227
                int r3 = r4 + 1
                r6 = 13
                r1[r4] = r6
            L_0x00aa:
                int r4 = r3 + 1
                r6 = 10
                r1[r3] = r6
                r3 = 19
                goto L_0x005a
            L_0x00b3:
                r10 = r6
                r6 = r4
                r4 = r10
                goto L_0x0013
            L_0x00b8:
                r7 = 2
                if (r7 > r5) goto L_0x0010
                byte[] r6 = r11.j
                r7 = 0
                byte r6 = r6[r7]
                r6 = r6 & 255(0xff, float:3.57E-43)
                int r6 = r6 << 16
                r7 = 0
                int r4 = r4 + 1
                byte r7 = r12[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                int r7 = r7 << 8
                r6 = r6 | r7
                r7 = 1
                int r4 = r4 + 1
                byte r7 = r12[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r6 = r6 | r7
                r7 = 0
                r11.c = r7
                r10 = r6
                r6 = r4
                r4 = r10
                goto L_0x0013
            L_0x00de:
                if (r5 <= 0) goto L_0x0010
                byte[] r6 = r11.j
                r7 = 0
                byte r6 = r6[r7]
                r6 = r6 & 255(0xff, float:3.57E-43)
                int r6 = r6 << 16
                byte[] r7 = r11.j
                r8 = 1
                byte r7 = r7[r8]
                r7 = r7 & 255(0xff, float:3.57E-43)
                int r7 = r7 << 8
                r6 = r6 | r7
                r7 = 0
                int r4 = r4 + 1
                byte r7 = r12[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r6 = r6 | r7
                r7 = 0
                r11.c = r7
                goto L_0x0010
            L_0x0100:
                int r6 = r11.c
                int r6 = r2 - r6
                r7 = 1
                int r7 = r5 - r7
                if (r6 != r7) goto L_0x0170
                r6 = 0
                int r7 = r11.c
                if (r7 <= 0) goto L_0x016b
                byte[] r7 = r11.j
                r8 = 0
                int r6 = r6 + 1
                byte r7 = r7[r8]
                r10 = r7
                r7 = r2
                r2 = r10
            L_0x0118:
                r2 = r2 & 255(0xff, float:3.57E-43)
                int r2 = r2 << 4
                int r8 = r11.c
                int r6 = r8 - r6
                r11.c = r6
                int r6 = r4 + 1
                int r8 = r2 >> 6
                r8 = r8 & 63
                byte r8 = r0[r8]
                r1[r4] = r8
                int r4 = r6 + 1
                r2 = r2 & 63
                byte r0 = r0[r2]
                r1[r6] = r0
                boolean r0 = r11.d
                if (r0 == 0) goto L_0x0224
                int r0 = r4 + 1
                r2 = 61
                r1[r4] = r2
                int r2 = r0 + 1
                r4 = 61
                r1[r0] = r4
                r0 = r2
            L_0x0145:
                boolean r2 = r11.e
                if (r2 == 0) goto L_0x015b
                boolean r2 = r11.f
                if (r2 == 0) goto L_0x0154
                int r2 = r0 + 1
                r4 = 13
                r1[r0] = r4
                r0 = r2
            L_0x0154:
                int r2 = r0 + 1
                r4 = 10
                r1[r0] = r4
                r0 = r2
            L_0x015b:
                r1 = r0
                r0 = r7
            L_0x015d:
                boolean r2 = com.apperhand.device.a.d.a.c.g
                if (r2 != 0) goto L_0x020e
                int r2 = r11.c
                if (r2 == 0) goto L_0x020e
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x016b:
                int r7 = r2 + 1
                byte r2 = r12[r2]
                goto L_0x0118
            L_0x0170:
                int r6 = r11.c
                int r6 = r2 - r6
                r7 = 2
                int r7 = r5 - r7
                if (r6 != r7) goto L_0x01f0
                r6 = 0
                int r7 = r11.c
                r8 = 1
                if (r7 <= r8) goto L_0x01e3
                byte[] r7 = r11.j
                r8 = 0
                int r6 = r6 + 1
                byte r7 = r7[r8]
                r10 = r7
                r7 = r2
                r2 = r10
            L_0x0189:
                r2 = r2 & 255(0xff, float:3.57E-43)
                int r2 = r2 << 10
                int r8 = r11.c
                if (r8 <= 0) goto L_0x01e8
                byte[] r8 = r11.j
                int r9 = r6 + 1
                byte r6 = r8[r6]
                r8 = r7
                r7 = r9
            L_0x0199:
                r6 = r6 & 255(0xff, float:3.57E-43)
                int r6 = r6 << 2
                r2 = r2 | r6
                int r6 = r11.c
                int r6 = r6 - r7
                r11.c = r6
                int r6 = r4 + 1
                int r7 = r2 >> 12
                r7 = r7 & 63
                byte r7 = r0[r7]
                r1[r4] = r7
                int r4 = r6 + 1
                int r7 = r2 >> 6
                r7 = r7 & 63
                byte r7 = r0[r7]
                r1[r6] = r7
                int r6 = r4 + 1
                r2 = r2 & 63
                byte r0 = r0[r2]
                r1[r4] = r0
                boolean r0 = r11.d
                if (r0 == 0) goto L_0x0222
                int r0 = r6 + 1
                r2 = 61
                r1[r6] = r2
            L_0x01c9:
                boolean r2 = r11.e
                if (r2 == 0) goto L_0x01df
                boolean r2 = r11.f
                if (r2 == 0) goto L_0x01d8
                int r2 = r0 + 1
                r4 = 13
                r1[r0] = r4
                r0 = r2
            L_0x01d8:
                int r2 = r0 + 1
                r4 = 10
                r1[r0] = r4
                r0 = r2
            L_0x01df:
                r1 = r0
                r0 = r8
                goto L_0x015d
            L_0x01e3:
                int r7 = r2 + 1
                byte r2 = r12[r2]
                goto L_0x0189
            L_0x01e8:
                int r8 = r7 + 1
                byte r7 = r12[r7]
                r10 = r7
                r7 = r6
                r6 = r10
                goto L_0x0199
            L_0x01f0:
                boolean r0 = r11.e
                if (r0 == 0) goto L_0x020a
                if (r4 <= 0) goto L_0x020a
                r0 = 19
                if (r3 == r0) goto L_0x020a
                boolean r0 = r11.f
                if (r0 == 0) goto L_0x0220
                int r0 = r4 + 1
                r6 = 13
                r1[r4] = r6
            L_0x0204:
                int r4 = r0 + 1
                r6 = 10
                r1[r0] = r6
            L_0x020a:
                r0 = r2
                r1 = r4
                goto L_0x015d
            L_0x020e:
                boolean r2 = com.apperhand.device.a.d.a.c.g
                if (r2 != 0) goto L_0x021a
                if (r0 == r5) goto L_0x021a
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x021a:
                r11.b = r1
                r11.k = r3
                r0 = 1
                return r0
            L_0x0220:
                r0 = r4
                goto L_0x0204
            L_0x0222:
                r0 = r6
                goto L_0x01c9
            L_0x0224:
                r0 = r4
                goto L_0x0145
            L_0x0227:
                r3 = r4
                goto L_0x00aa
            L_0x022a:
                r4 = r2
                r2 = r6
                goto L_0x005a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.apperhand.device.a.d.a.c.a(byte[], int):boolean");
        }
    }

    private a() {
    }
}
