package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.Activation;

public class ActivationResponse extends BaseResponse {
    private static final long serialVersionUID = 5276891535662703443L;
    private Activation activation;

    public ActivationResponse() {
        this(null);
    }

    public ActivationResponse(Activation activation2) {
        this.activation = activation2;
    }

    public Activation getActivation() {
        return this.activation;
    }

    public void setActivation(Activation activation2) {
        this.activation = activation2;
    }

    public String toString() {
        return "ActivationResponse [activation=" + this.activation + ", toString()=" + super.toString() + "]";
    }
}
