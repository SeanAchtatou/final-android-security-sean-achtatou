package com.apperhand.common.dto;

public enum Status {
    ADD,
    DELETE,
    EXISTS,
    UPDATE
}
