package com.apperhand.common.dto;

import java.util.Map;

public class Activation extends BaseDTO {
    private static final long serialVersionUID = 3085832803649412152L;
    private String eula;
    private Map<String, String> parameters;

    public Activation() {
        this(null, null);
    }

    public Activation(String str, Map<String, String> map) {
        this.eula = str;
        this.parameters = map;
    }

    public String getEula() {
        return this.eula;
    }

    public void setEula(String str) {
        this.eula = str;
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String, String> map) {
        this.parameters = map;
    }

    public String toString() {
        return "Activation [eula=" + this.eula + ", parameters=" + this.parameters + "]";
    }
}
