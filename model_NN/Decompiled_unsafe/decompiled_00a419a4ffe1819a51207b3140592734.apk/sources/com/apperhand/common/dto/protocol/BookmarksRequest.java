package com.apperhand.common.dto.protocol;

public class BookmarksRequest extends BaseRequest {
    private static final long serialVersionUID = -4893793282001662961L;

    public String toString() {
        return "BookmarksRequest [toString()=" + super.toString() + "]";
    }
}
