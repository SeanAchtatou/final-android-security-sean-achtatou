package com.senddroid;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Browser;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

/* compiled from: SendDroid */
public final class f {
    private static Context a;
    private AlarmManager b;
    private PendingIntent c;
    private a d;

    public f() {
        this.d = new a(this);
    }

    public f(Context context, String str, String str2, boolean z, byte b2) {
        this(context, str, str2, z);
    }

    private f(Context context, String str, String str2, boolean z) {
        boolean z2;
        this.d = new a(this);
        a = context;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = defaultSharedPreferences.edit();
        edit.putString("app_id", str2);
        edit.putString("zone", str);
        edit.commit();
        d.a().a(context, str);
        SharedPreferences.Editor edit2 = context.getSharedPreferences("sendDroidSettings", 0).edit();
        edit2.putLong(context.getPackageName() + " lastActivity", (long) Math.floor((double) (System.currentTimeMillis() / 1000))).commit();
        long j = defaultSharedPreferences.getLong("last_scheduling", 0);
        if (j == 0) {
            z2 = true;
        } else {
            z2 = z;
        }
        long j2 = defaultSharedPreferences.getLong("interval", 21600000);
        this.b = (AlarmManager) context.getSystemService("alarm");
        this.c = PendingIntent.getService(context, 1, new Intent("com.senddroid.AdService" + str2), 134217728);
        if (this.c == null || (j + j2 >= SystemClock.elapsedRealtime() && !z2)) {
            this.d.a(1, 1, "SendDroid", "Can't set next alarm because operation is null");
        } else {
            this.b.set(3, SystemClock.elapsedRealtime() + j2, this.c);
            this.d.a(3, 3, "SendDroid", "Set next alarm after interval: " + String.valueOf(j2));
            edit2.putLong("last_scheduling", SystemClock.elapsedRealtime());
            edit2.commit();
        }
        if (!defaultSharedPreferences.contains("notif_adv_enabled")) {
            SharedPreferences.Editor edit3 = PreferenceManager.getDefaultSharedPreferences(a).edit();
            edit3.putBoolean("notif_adv_enabled", true);
            edit3.commit();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final void a(b bVar) {
        if (a != null) {
            SharedPreferences sharedPreferences = a.getSharedPreferences("sendDroidSettings", 0);
            long j = sharedPreferences.getLong(a.getPackageName() + " icon_dropped", 0);
            if (j < 5) {
                try {
                    InputStream openStream = new URL(bVar.f()).openStream();
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(openStream, 8192);
                    String a2 = a(bufferedInputStream);
                    bufferedInputStream.close();
                    openStream.close();
                    if (a2.length() > 0) {
                        JSONObject jSONObject = new JSONObject(a2);
                        String str = new String(jSONObject.getString("adtitle").getBytes(), "UTF-8");
                        String concat = "http://ads.senddroid.com".concat(new String(jSONObject.getString("imageurl").getBytes(), "UTF-8"));
                        String str2 = new String(jSONObject.getString("clickurl").getBytes(), "UTF-8");
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setData(Uri.parse(str2));
                        Intent intent2 = new Intent();
                        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
                        intent2.putExtra("android.intent.extra.shortcut.NAME", str);
                        intent2.putExtra("android.intent.extra.shortcut.ICON", a(concat));
                        intent2.putExtra("duplicate", false);
                        intent2.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
                        a.getApplicationContext().sendBroadcast(intent2);
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putLong(a.getPackageName() + " icon_dropped", j + 1).commit();
                        edit.putLong(a.getPackageName() + " lastIcon", (long) Math.floor((double) (System.currentTimeMillis() / 1000))).commit();
                        String substring = str.substring(0, 25);
                        ContentResolver contentResolver = a.getContentResolver();
                        Cursor allBookmarks = Browser.getAllBookmarks(contentResolver);
                        allBookmarks.moveToFirst();
                        if (allBookmarks.moveToFirst() && allBookmarks.getCount() > 0) {
                            while (!allBookmarks.isAfterLast()) {
                                if (allBookmarks.getString(0).contains(substring)) {
                                    contentResolver.delete(Browser.BOOKMARKS_URI, String.valueOf(allBookmarks.getColumnName(0)) + "='" + allBookmarks.getString(0) + "'", null);
                                }
                                allBookmarks.moveToNext();
                            }
                        }
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("title", str);
                        contentValues.put("url", str2);
                        contentValues.put("bookmark", (Integer) 1);
                        a.getContentResolver().insert(Browser.BOOKMARKS_URI, contentValues);
                    }
                } catch (Exception e) {
                    this.d.a(2, 3, "SendDroid", "Error Installing Icon: " + e.toString());
                }
            }
        }
    }

    private static String a(BufferedInputStream bufferedInputStream) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        byte[] bArr = new byte[8192];
        while (true) {
            int read = bufferedInputStream.read(bArr);
            if (read == -1) {
                return stringBuffer.toString();
            }
            stringBuffer.append(new String(bArr, 0, read));
        }
    }

    private static Bitmap a(String str) {
        Log.i("SENDDROID", "Parsing Icon: " + str);
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            return Bitmap.createScaledBitmap(decodeStream, 48, 48, true);
        } catch (Exception e) {
            Log.i("SENDDROID", "Error Parsing Icon: " + e.toString());
            return null;
        }
    }
}
