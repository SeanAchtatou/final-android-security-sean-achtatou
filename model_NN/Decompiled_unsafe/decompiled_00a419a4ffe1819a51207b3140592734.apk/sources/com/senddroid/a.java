package com.senddroid;

import android.util.Log;
import org.codehaus.jackson.impl.JsonWriteContext;

/* compiled from: AdLog */
public final class a {
    private static int c = 0;
    private int a = 0;
    private Object b;

    public a(Object obj) {
        this.b = obj;
        a(c);
    }

    public final void a(int i, int i2, String str, String str2) {
        String str3 = "[" + Integer.toHexString(this.b.hashCode()) + "]" + str;
        if (i <= this.a) {
            switch (i2) {
                case JsonWriteContext.STATUS_OK_AFTER_COMMA /*1*/:
                    Log.e(str3, str2 + ' ');
                    return;
                case JsonWriteContext.STATUS_OK_AFTER_COLON /*2*/:
                    Log.w(str3, str2 + ' ');
                    return;
                default:
                    Log.i(str3, str2 + ' ');
                    return;
            }
        }
    }

    public final void a(int i) {
        this.a = i;
        switch (i) {
            case JsonWriteContext.STATUS_OK_AFTER_COMMA /*1*/:
                a(1, 3, "SetLogLevel", "LOG_LEVEL_1");
                return;
            case JsonWriteContext.STATUS_OK_AFTER_COLON /*2*/:
                a(1, 3, "SetLogLevel", "LOG_LEVEL_2");
                return;
            case JsonWriteContext.STATUS_OK_AFTER_SPACE /*3*/:
                a(1, 3, "SetLogLevel", "LOG_LEVEL_3");
                return;
            default:
                a(1, 3, "SetLogLevel", "LOG_LEVEL_NONE");
                return;
        }
    }
}
