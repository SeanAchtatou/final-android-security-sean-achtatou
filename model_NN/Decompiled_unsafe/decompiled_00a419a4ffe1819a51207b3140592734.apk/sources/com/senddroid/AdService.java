package com.senddroid;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.JSONException;
import org.json.JSONObject;

public class AdService extends Service {
    PowerManager.WakeLock a;
    /* access modifiers changed from: private */
    public a b = new a(this);
    /* access modifiers changed from: private */
    public b c;
    /* access modifiers changed from: private */
    public LocationManager d;
    /* access modifiers changed from: private */
    public d e;
    private PowerManager f;

    static /* synthetic */ String a(BufferedInputStream bufferedInputStream) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        byte[] bArr = new byte[8192];
        while (true) {
            int read = bufferedInputStream.read(bArr);
            if (read == -1) {
                return stringBuffer.toString();
            }
            stringBuffer.append(new String(bArr, 0, read));
        }
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        if (this.f == null) {
            this.f = (PowerManager) getSystemService("power");
        }
        if (this.a == null) {
            this.a = this.f.newWakeLock(1, "My Tag");
        }
        this.a.acquire();
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String string = defaultSharedPreferences.getString("app_id", "");
        int i2 = defaultSharedPreferences.getInt("log_level", -1);
        if (i2 >= 0) {
            this.b.a(i2);
        }
        this.b.a(2, 3, "AdService", "AdService.onStart #" + String.valueOf(i));
        long j = defaultSharedPreferences.getLong("interval", 21600000);
        AlarmManager alarmManager = (AlarmManager) getSystemService("alarm");
        PendingIntent service = PendingIntent.getService(this, 1, new Intent("com.senddroid.AdService" + string), 134217728);
        if (service != null) {
            alarmManager.set(3, SystemClock.elapsedRealtime() + j, service);
            this.b.a(2, 3, "AdService", "set next alarm after that interval:" + String.valueOf(j));
            SharedPreferences.Editor edit = defaultSharedPreferences.edit();
            edit.putLong("last_scheduling", SystemClock.elapsedRealtime());
            edit.commit();
        }
        if (!defaultSharedPreferences.getBoolean("notif_adv_enabled", false)) {
            this.b.a(2, 3, "AdService", "Notification advertisement is disabled... skipped #" + String.valueOf(i));
            return;
        }
        this.b.a(2, 3, "AdService", "execute ShowNotificationAdvertisementTask");
        new c(this).execute(0);
        this.b.a(2, 3, "AdService", "execute ShowIconDropTask");
        new b(this).execute(0);
    }

    private class c extends AsyncTask<Integer, Integer, Integer> {
        /* synthetic */ c(AdService adService) {
            this((byte) 0);
        }

        private c(byte b) {
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return a();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            try {
                AdService.this.a.release();
            } catch (Exception e) {
            }
        }

        private Integer a() {
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(AdService.this);
            b unused = AdService.this.c = new b(AdService.this.b);
            AdService.this.c.a(AdService.this);
            AdService.this.c.b(defaultSharedPreferences.getString("zone", ""));
            LocationManager unused2 = AdService.this.d = (LocationManager) AdService.this.getSystemService("location");
            try {
                new a(AdService.this, AdService.this.c).start();
                AdService.this.b.a(2, 3, "AdService", "AutoDetectParametersThread started");
                Thread.sleep(30000);
                if (AdService.this.c.b() == null || AdService.this.c.c() == null) {
                    c a2 = c.a();
                    AdService.this.c.c(a2.b());
                    AdService.this.c.d(a2.c());
                    AdService.this.b.a(2, 2, "AutoDetectedParametersSet.Gps/Network=", "(" + a2.b() + ";" + a2.c() + ")");
                }
            } catch (Exception e) {
                AdService.this.b.a(1, 1, "sleep in ShowNotificationAdvertisementTask", e.getMessage());
            }
            try {
                URL url = new URL(AdService.this.c.e());
                InputStream openStream = url.openStream();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(openStream, 8192);
                String a3 = AdService.a(bufferedInputStream);
                bufferedInputStream.close();
                openStream.close();
                AdService.this.b.a(2, 3, "AdService", "Banner downloaded: " + url.toString());
                if (a3.length() <= 0) {
                    return null;
                }
                JSONObject jSONObject = new JSONObject(a3);
                NotificationManager notificationManager = (NotificationManager) AdService.this.getSystemService("notification");
                try {
                    String str = new String(jSONObject.getString("adtitle").getBytes(), "UTF-8");
                    String str2 = new String(jSONObject.getString("adtext").getBytes(), "UTF-8");
                    int i = defaultSharedPreferences.getInt("icon_resource", -1);
                    if (i <= 0) {
                        i = 17301620;
                    }
                    Notification notification = new Notification(i, str, System.currentTimeMillis());
                    notification.tickerText = str2;
                    if (AdService.this.getPackageManager().checkPermission("android.permission.VIBRATE", AdService.this.getApplicationContext().getPackageName()) == 0) {
                        notification.vibrate = new long[]{0, 100, 200, 300};
                    }
                    notification.ledOffMS = 300;
                    notification.ledOnMS = 300;
                    notification.flags = 17;
                    notification.setLatestEventInfo(AdService.this, str, str2, PendingIntent.getActivity(AdService.this, 0, new Intent("android.intent.action.VIEW", Uri.parse(jSONObject.getString("clickurl"))), 268435456));
                    notificationManager.cancel(0);
                    notificationManager.notify(0, notification);
                    AdService.this.b.a(2, 3, "AdService", "notification showed: " + notification.toString());
                    AdService.this.getApplicationContext().getSharedPreferences("sendDroidSettings", 0).edit().putLong(AdService.this.getApplicationContext().getPackageName() + " lastAd", (long) Math.floor((double) (System.currentTimeMillis() / 1000))).commit();
                    return null;
                } catch (Exception e2) {
                    return null;
                }
            } catch (MalformedURLException e3) {
                AdService.this.b.a(1, 1, "AdService", e3.getMessage());
                e3.printStackTrace();
            } catch (IOException e4) {
                AdService.this.b.a(1, 1, "AdService", e4.getMessage());
                e4.printStackTrace();
            } catch (JSONException e5) {
                AdService.this.b.a(1, 1, "AdService", e5.getMessage());
                e5.printStackTrace();
            }
        }
    }

    private class b extends AsyncTask<Integer, Integer, Integer> {
        /* synthetic */ b(AdService adService) {
            this((byte) 0);
        }

        private b(byte b) {
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return a();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            try {
                AdService.this.a.release();
            } catch (Exception e) {
            }
        }

        private Integer a() {
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(AdService.this);
            b unused = AdService.this.c = new b(AdService.this.b);
            AdService.this.c.a(AdService.this);
            AdService.this.c.b(defaultSharedPreferences.getString("zone", ""));
            LocationManager unused2 = AdService.this.d = (LocationManager) AdService.this.getSystemService("location");
            try {
                new a(AdService.this, AdService.this.c).start();
                AdService.this.b.a(2, 3, "AdService", "AutoDetectParametersThread started");
                Thread.sleep(30000);
                if (AdService.this.c.b() == null || AdService.this.c.c() == null) {
                    c a2 = c.a();
                    AdService.this.c.c(a2.b());
                    AdService.this.c.d(a2.c());
                    AdService.this.b.a(2, 2, "AutoDetectedParametersSet.Gps/Network=", "(" + a2.b() + ";" + a2.c() + ")");
                }
            } catch (Exception e) {
                AdService.this.b.a(1, 1, "sleep in ShowNotificationAdvertisementTask", e.getMessage());
            }
            new f().a(AdService.this.c);
            return null;
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private class a extends Thread {
        private Context b;
        private b c;

        public a(Context context, b bVar) {
            this.b = context;
            this.c = bVar;
        }

        /* JADX WARNING: Removed duplicated region for block: B:38:0x0102 A[Catch:{ Exception -> 0x0178 }] */
        /* JADX WARNING: Removed duplicated region for block: B:60:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r11 = this;
                r2 = 0
                r4 = 0
                r10 = 0
                r9 = 1
                r8 = 2
                com.senddroid.b r0 = r11.c
                if (r0 == 0) goto L_0x010a
                com.senddroid.c r7 = com.senddroid.c.a()
                com.senddroid.b r0 = r11.c
                java.lang.String r0 = r0.b()
                if (r0 == 0) goto L_0x001e
                com.senddroid.b r0 = r11.c
                java.lang.String r0 = r0.c()
                if (r0 != 0) goto L_0x00ae
            L_0x001e:
                android.content.Context r0 = r11.b
                java.lang.String r1 = "android.permission.ACCESS_FINE_LOCATION"
                int r1 = r0.checkCallingOrSelfPermission(r1)
                com.senddroid.AdService r5 = com.senddroid.AdService.this
                android.content.Context r0 = r11.b
                java.lang.String r6 = "location"
                java.lang.Object r0 = r0.getSystemService(r6)
                android.location.LocationManager r0 = (android.location.LocationManager) r0
                android.location.LocationManager unused = r5.d = r0
                if (r1 != 0) goto L_0x011b
                com.senddroid.AdService r0 = com.senddroid.AdService.this
                android.location.LocationManager r0 = r0.d
                java.lang.String r1 = "gps"
                boolean r0 = r0.isProviderEnabled(r1)
                if (r0 == 0) goto L_0x010b
                com.senddroid.AdService r0 = com.senddroid.AdService.this
                com.senddroid.AdService$d r1 = new com.senddroid.AdService$d
                com.senddroid.AdService r5 = com.senddroid.AdService.this
                com.senddroid.AdService r6 = com.senddroid.AdService.this
                android.location.LocationManager r6 = r6.d
                r1.<init>(r6, r7)
                com.senddroid.AdService.d unused = r0.e = r1
                com.senddroid.AdService r0 = com.senddroid.AdService.this
                android.location.LocationManager r0 = r0.d
                java.lang.String r1 = "gps"
                com.senddroid.AdService r5 = com.senddroid.AdService.this
                com.senddroid.AdService$d r5 = r5.e
                android.os.Looper r6 = android.os.Looper.getMainLooper()
                r0.requestLocationUpdates(r1, r2, r4, r5, r6)
                r0 = r10
            L_0x006d:
                if (r0 == 0) goto L_0x00ae
                android.content.Context r0 = r11.b
                java.lang.String r1 = "android.permission.ACCESS_COARSE_LOCATION"
                int r0 = r0.checkCallingOrSelfPermission(r1)
                if (r0 != 0) goto L_0x013a
                com.senddroid.AdService r0 = com.senddroid.AdService.this
                android.location.LocationManager r0 = r0.d
                java.lang.String r1 = "network"
                boolean r0 = r0.isProviderEnabled(r1)
                if (r0 == 0) goto L_0x012b
                com.senddroid.AdService r0 = com.senddroid.AdService.this
                com.senddroid.AdService$d r1 = new com.senddroid.AdService$d
                com.senddroid.AdService r5 = com.senddroid.AdService.this
                com.senddroid.AdService r6 = com.senddroid.AdService.this
                android.location.LocationManager r6 = r6.d
                r1.<init>(r6, r7)
                com.senddroid.AdService.d unused = r0.e = r1
                com.senddroid.AdService r0 = com.senddroid.AdService.this
                android.location.LocationManager r0 = r0.d
                java.lang.String r1 = "network"
                com.senddroid.AdService r5 = com.senddroid.AdService.this
                com.senddroid.AdService$d r5 = r5.e
                android.os.Looper r6 = android.os.Looper.getMainLooper()
                r0.requestLocationUpdates(r1, r2, r4, r5, r6)
            L_0x00ae:
                com.senddroid.b r0 = r11.c
                java.lang.String r0 = r0.a()
                if (r0 != 0) goto L_0x00d2
                java.lang.String r0 = r7.d()
                if (r0 != 0) goto L_0x0149
                com.senddroid.AdService r0 = com.senddroid.AdService.this
                java.lang.String r0 = com.senddroid.AdService.a(r0)
                if (r0 == 0) goto L_0x00d2
                int r1 = r0.length()
                if (r1 <= 0) goto L_0x00d2
                com.senddroid.b r1 = r11.c
                r1.a(r0)
                r7.c(r0)
            L_0x00d2:
                com.senddroid.b r0 = r11.c
                java.lang.Integer r0 = r0.d()
                if (r0 != 0) goto L_0x010a
                java.lang.Integer r0 = r7.e()
                if (r0 != 0) goto L_0x016e
                r1 = 0
                android.content.Context r0 = r11.b     // Catch:{ Exception -> 0x0178 }
                java.lang.String r2 = "connectivity"
                java.lang.Object r0 = r0.getSystemService(r2)     // Catch:{ Exception -> 0x0178 }
                android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Exception -> 0x0178 }
                android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Exception -> 0x0178 }
                if (r0 == 0) goto L_0x017a
                int r2 = r0.getType()     // Catch:{ Exception -> 0x0178 }
                int r0 = r0.getSubtype()     // Catch:{ Exception -> 0x0178 }
                if (r2 != r9) goto L_0x0153
                r0 = 1
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0178 }
            L_0x0100:
                if (r0 == 0) goto L_0x010a
                com.senddroid.b r1 = r11.c     // Catch:{ Exception -> 0x0178 }
                r1.a(r0)     // Catch:{ Exception -> 0x0178 }
                r7.a(r0)     // Catch:{ Exception -> 0x0178 }
            L_0x010a:
                return
            L_0x010b:
                com.senddroid.AdService r0 = com.senddroid.AdService.this
                com.senddroid.a r0 = r0.b
                java.lang.String r1 = "AutoDetectedParametersSet.Gps"
                java.lang.String r5 = "not avalable"
                r0.a(r8, r8, r1, r5)
                r0 = r9
                goto L_0x006d
            L_0x011b:
                com.senddroid.AdService r0 = com.senddroid.AdService.this
                com.senddroid.a r0 = r0.b
                java.lang.String r1 = "AutoDetectedParametersSet.Gps"
                java.lang.String r5 = "no permission ACCESS_FINE_LOCATION"
                r0.a(r8, r8, r1, r5)
                r0 = r9
                goto L_0x006d
            L_0x012b:
                com.senddroid.AdService r0 = com.senddroid.AdService.this
                com.senddroid.a r0 = r0.b
                java.lang.String r1 = "AutoDetectedParametersSet.Network"
                java.lang.String r2 = "not avalable"
                r0.a(r8, r8, r1, r2)
                goto L_0x00ae
            L_0x013a:
                com.senddroid.AdService r0 = com.senddroid.AdService.this
                com.senddroid.a r0 = r0.b
                java.lang.String r1 = "AutoDetectedParametersSet.Network"
                java.lang.String r2 = "no permission ACCESS_COARSE_LOCATION"
                r0.a(r8, r8, r1, r2)
                goto L_0x00ae
            L_0x0149:
                com.senddroid.b r0 = r11.c
                java.lang.String r1 = r7.d()
                r0.a(r1)
                goto L_0x00d2
            L_0x0153:
                if (r2 != 0) goto L_0x017a
                if (r0 != r8) goto L_0x015d
                r0 = 0
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0178 }
                goto L_0x0100
            L_0x015d:
                if (r0 != r9) goto L_0x0165
                r0 = 0
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0178 }
                goto L_0x0100
            L_0x0165:
                r2 = 3
                if (r0 != r2) goto L_0x017a
                r0 = 1
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0178 }
                goto L_0x0100
            L_0x016e:
                com.senddroid.b r0 = r11.c
                java.lang.Integer r1 = r7.e()
                r0.a(r1)
                goto L_0x010a
            L_0x0178:
                r0 = move-exception
                goto L_0x010a
            L_0x017a:
                r0 = r1
                goto L_0x0100
            */
            throw new UnsupportedOperationException("Method not decompiled: com.senddroid.AdService.a.run():void");
        }
    }

    private class d implements LocationListener {
        private LocationManager b;
        private c c;

        public d(LocationManager locationManager, c cVar) {
            this.b = locationManager;
            this.c = cVar;
        }

        public final void onLocationChanged(Location location) {
            this.b.removeUpdates(this);
            try {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                AdService.this.c.c(Double.toString(latitude));
                AdService.this.c.d(Double.toString(longitude));
                this.c.a(Double.toString(latitude));
                this.c.b(Double.toString(longitude));
                AdService.this.b.a(3, 3, "LocationChanged=", "(" + this.c.b() + ";" + this.c.c() + ")");
            } catch (Exception e) {
                AdService.this.b.a(2, 1, "LocationChanged", e.getMessage());
            }
        }

        public final void onProviderDisabled(String str) {
        }

        public final void onProviderEnabled(String str) {
        }

        public final void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r4) {
        /*
            java.lang.Class<android.webkit.WebSettings> r0 = android.webkit.WebSettings.class
            r1 = 2
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0036 }
            r2 = 0
            java.lang.Class<android.content.Context> r3 = android.content.Context.class
            r1[r2] = r3     // Catch:{ Exception -> 0x0036 }
            r2 = 1
            java.lang.Class<android.webkit.WebView> r3 = android.webkit.WebView.class
            r1[r2] = r3     // Catch:{ Exception -> 0x0036 }
            java.lang.reflect.Constructor r1 = r0.getDeclaredConstructor(r1)     // Catch:{ Exception -> 0x0036 }
            r0 = 1
            r1.setAccessible(r0)     // Catch:{ Exception -> 0x0036 }
            r0 = 2
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0030 }
            r2 = 0
            r0[r2] = r4     // Catch:{ all -> 0x0030 }
            r2 = 1
            r3 = 0
            r0[r2] = r3     // Catch:{ all -> 0x0030 }
            java.lang.Object r0 = r1.newInstance(r0)     // Catch:{ all -> 0x0030 }
            android.webkit.WebSettings r0 = (android.webkit.WebSettings) r0     // Catch:{ all -> 0x0030 }
            java.lang.String r0 = r0.getUserAgentString()     // Catch:{ all -> 0x0030 }
            r2 = 0
            r1.setAccessible(r2)     // Catch:{ Exception -> 0x0036 }
        L_0x002f:
            return r0
        L_0x0030:
            r0 = move-exception
            r2 = 0
            r1.setAccessible(r2)     // Catch:{ Exception -> 0x0036 }
            throw r0     // Catch:{ Exception -> 0x0036 }
        L_0x0036:
            r0 = move-exception
            android.webkit.WebView r0 = new android.webkit.WebView
            r0.<init>(r4)
            android.webkit.WebSettings r0 = r0.getSettings()
            java.lang.String r0 = r0.getUserAgentString()
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.senddroid.AdService.a(android.content.Context):java.lang.String");
    }
}
