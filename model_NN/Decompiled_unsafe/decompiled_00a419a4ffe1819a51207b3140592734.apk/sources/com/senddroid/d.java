package com.senddroid;

import android.content.Context;
import android.util.Log;

/* compiled from: InstallTracker */
public final class d {
    /* access modifiers changed from: private */
    public static String e = "push.senddroid.com";
    /* access modifiers changed from: private */
    public static String f = "/install.php";
    private static d g = null;
    /* access modifiers changed from: private */
    public Context a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d = null;
    /* access modifiers changed from: private */
    public a h = new a(this);
    private Runnable i = new e(this);

    private d() {
    }

    public static d a() {
        if (g == null) {
            g = new d();
        }
        return g;
    }

    public final void a(Context context, String str) {
        if (context != null) {
            this.a = context;
            this.c = str;
            this.b = this.a.getPackageName();
            if (this.a.getSharedPreferences("sendDroidSettings", 0).getLong(this.b + " installed", 0) == 0) {
                this.d = g.a(this.a);
                new Thread(this.i).start();
                return;
            }
            Log.i("InstallTracker", "Install already tracked");
        }
    }
}
