package com.senddroid;

/* compiled from: AutoDetectedParametersSet */
public final class c {
    private static c a;
    private String b;
    private String c;
    private String d;
    private Integer e;

    private c() {
    }

    public static synchronized c a() {
        c cVar;
        synchronized (c.class) {
            if (a == null) {
                a = new c();
            }
            cVar = a;
        }
        return cVar;
    }

    public final String b() {
        return this.b;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final String c() {
        return this.c;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String d() {
        return this.d;
    }

    public final void c(String str) {
        this.d = str;
    }

    public final Integer e() {
        return this.e;
    }

    public final void a(Integer num) {
        this.e = num;
    }
}
