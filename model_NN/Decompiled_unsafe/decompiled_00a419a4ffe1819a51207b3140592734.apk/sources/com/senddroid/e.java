package com.senddroid;

import android.util.Log;
import java.io.IOException;
import java.net.URLEncoder;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/* compiled from: InstallTracker */
class e implements Runnable {
    final /* synthetic */ d a;

    e(d dVar) {
        this.a = dVar;
    }

    public final void run() {
        StringBuilder sb = new StringBuilder("http://" + d.e + d.f);
        sb.append("?pkg=" + this.a.b);
        if (this.a.c != null) {
            try {
                sb.append("&app=" + URLEncoder.encode(this.a.c, "UTF-8"));
            } catch (Exception e) {
            }
        }
        sb.append("&udid=" + g.b(this.a.a));
        if (this.a.d != null) {
            try {
                sb.append("&ua=" + URLEncoder.encode(this.a.d, "UTF-8"));
            } catch (Exception e2) {
            }
        }
        String sb2 = sb.toString();
        this.a.h.a(3, 3, "InstallTracker", "Install track: " + sb2);
        try {
            HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(sb2));
            if (execute.getStatusLine().getStatusCode() != 200) {
                Log.i("InstallTracker", "Install track failed: Status code " + execute.getStatusLine().getStatusCode() + " != 200");
                return;
            }
            Log.i("InstallTracker", "Install track successful");
            this.a.a.getSharedPreferences("sendDroidSettings", 0).edit().putLong(this.a.b + " installed", (long) Math.floor((double) (System.currentTimeMillis() / 1000))).commit();
        } catch (ClientProtocolException e3) {
            Log.i("InstallTracker", "Install track failed: ClientProtocolException (no signal?)");
        } catch (IOException e4) {
            Log.i("InstallTracker", "Install track failed: IOException (no signal?)");
        }
    }
}
