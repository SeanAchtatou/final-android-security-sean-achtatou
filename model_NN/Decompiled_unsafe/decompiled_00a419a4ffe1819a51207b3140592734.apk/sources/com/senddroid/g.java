package com.senddroid;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.lang.reflect.Constructor;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/* compiled from: Utils */
public final class g {
    private static String a = null;
    private static String b = null;

    private static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                byte b3 = b2 & 255;
                if (b3 < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toHexString(b3));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static String a(Context context) {
        Constructor<WebSettings> declaredConstructor;
        if (a == null) {
            try {
                declaredConstructor = WebSettings.class.getDeclaredConstructor(Context.class, WebView.class);
                declaredConstructor.setAccessible(true);
                a = declaredConstructor.newInstance(context, null).getUserAgentString();
                declaredConstructor.setAccessible(false);
            } catch (Exception e) {
                a = new WebView(context).getSettings().getUserAgentString();
            } catch (Throwable th) {
                declaredConstructor.setAccessible(false);
                throw th;
            }
        }
        return a;
    }

    private static synchronized String c(Context context) {
        String str;
        synchronized (g.class) {
            if (b == null) {
                File file = new File(context.getFilesDir(), "INSTALLATION");
                try {
                    if (!file.exists()) {
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        fileOutputStream.write(UUID.randomUUID().toString().getBytes());
                        fileOutputStream.close();
                    }
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
                    byte[] bArr = new byte[((int) randomAccessFile.length())];
                    randomAccessFile.readFully(bArr);
                    randomAccessFile.close();
                    b = new String(bArr);
                } catch (Exception e) {
                    b = "1234567890";
                }
            }
            str = b;
        }
        return str;
    }

    public static String b(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            deviceId = c(context);
        }
        return a(deviceId);
    }
}
