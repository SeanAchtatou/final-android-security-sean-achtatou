package com.senddroid;

import android.content.Context;
import android.content.SharedPreferences;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/* compiled from: AdRequest */
public final class b {
    private Map<String, String> a = Collections.synchronizedMap(new HashMap());
    private String b = "http://push.senddroid.com/ad.php";
    private Hashtable<String, String> c;
    private a d;

    public b(a aVar) {
        this.d = aVar;
    }

    public final void a(Context context) {
        String b2 = g.b(context);
        this.d.a(2, 3, "deviceIdMD5", b2);
        if (b2 != null && b2.length() > 0) {
            this.a.put("udid", b2);
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("sendDroidSettings", 0);
        this.a.put("inst", Long.toString(sharedPreferences.getLong(context.getPackageName() + " installed", 0)));
        this.a.put("act", Long.toString(sharedPreferences.getLong(context.getPackageName() + " lastActivity", 0)));
        this.a.put("last", Long.toString(sharedPreferences.getLong(context.getPackageName() + " lastAd", 0)));
        this.a.put("icon", Long.toString(sharedPreferences.getLong(context.getPackageName() + " lastIcon", 0)));
    }

    public final b a(String str) {
        if (str != null) {
            synchronized (this.a) {
                this.a.put("ua", str);
            }
        }
        return this;
    }

    public final b b(String str) {
        if (str != null) {
            synchronized (this.a) {
                this.a.put("app", str);
            }
        }
        return this;
    }

    public final b c(String str) {
        if (str != null) {
            synchronized (this.a) {
                this.a.put("lat", str);
            }
        }
        return this;
    }

    public final b d(String str) {
        if (str != null) {
            synchronized (this.a) {
                this.a.put("long", str);
            }
        }
        return this;
    }

    public final b a(Integer num) {
        if (num != null) {
            synchronized (this.a) {
                this.a.put("connection_speed", String.valueOf(num));
            }
        }
        return this;
    }

    public final String a() {
        String str;
        synchronized (this.a) {
            str = this.a.get("ua");
        }
        return str;
    }

    public final String b() {
        String str;
        synchronized (this.a) {
            str = this.a.get("lat");
        }
        return str;
    }

    public final String c() {
        String str;
        synchronized (this.a) {
            str = this.a.get("long");
        }
        return str;
    }

    public final Integer d() {
        Integer valueOf;
        synchronized (this.a) {
            String str = this.a.get("connection_speed");
            valueOf = str != null ? Integer.valueOf(Integer.parseInt(str)) : null;
        }
        return valueOf;
    }

    public final synchronized String e() throws IllegalStateException {
        return toString();
    }

    public final synchronized String f() throws IllegalStateException {
        return g();
    }

    public final synchronized String toString() {
        StringBuilder sb;
        sb = new StringBuilder();
        sb.append(this.b + "?adtype=4");
        a(sb, this.a);
        a(sb, this.c);
        return sb.toString();
    }

    private synchronized String g() {
        StringBuilder sb;
        sb = new StringBuilder();
        sb.append(this.b + "?adtype=9");
        a(sb, this.a);
        a(sb, this.c);
        return sb.toString();
    }

    private static void a(StringBuilder sb, Map<String, String> map) {
        if (map != null) {
            synchronized (map) {
                for (String next : map.keySet()) {
                    String str = map.get(next);
                    if (str != null) {
                        try {
                            sb.append("&" + URLEncoder.encode(next, "UTF-8") + "=" + URLEncoder.encode(str, "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
