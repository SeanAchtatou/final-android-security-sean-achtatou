package com.airpush.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.WebView;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SetPreferences */
public final class p {
    private static String A = "0";
    private static String B = "0";
    private static String C = "0";
    private static String D = "0";
    private static String E = "0";
    private static String F = "0";
    private static String G = "0";
    /* access modifiers changed from: private */
    public static String H = "0";
    /* access modifiers changed from: private */
    public static String I = "0";
    private static Context J;
    private static String K;
    private static SharedPreferences L;
    protected static JSONObject a = null;
    protected static String b = "0";
    protected static List<NameValuePair> c;
    protected static String d;
    private static int h;
    private static String o;
    private static String p;
    private static String q;
    private static boolean r;
    private static String s = "4.02";
    private static String t = "unknown";
    private static String u = "unknown";
    private static String v = "unknown";
    private static String w = "00";
    private static String x = "invalid";
    private static String y = "0";
    private static String z = "0";
    private String e = "0";
    private String f = "0";
    private boolean g;
    private boolean i;
    private String j;
    private boolean k;
    private boolean l;
    private boolean m;
    private Location n;

    /* access modifiers changed from: protected */
    public final void a(Context context, String str, String str2, boolean z2, boolean z3, boolean z4) {
        J = context;
        D = str;
        E = str2;
        this.k = z4;
        this.l = z2;
        this.m = z3;
        r = false;
        try {
            p = new WebView(J).getSettings().getUserAgentString();
            Log.i("User Agent", "User Agent : " + this.k);
            q = ((ConnectivityManager) J.getSystemService("connectivity")).getActiveNetworkInfo().getTypeName().equals("WIFI") ? "1" : "0";
            Context context2 = J;
            if (context2.getPackageManager().checkPermission("android.permission.ACCESS_COARSE_LOCATION", context2.getPackageName()) == 0 && context2.getPackageManager().checkPermission("android.permission.ACCESS_FINE_LOCATION", context2.getPackageName()) == 0) {
                LocationManager locationManager = (LocationManager) context2.getSystemService("location");
                if (locationManager.isProviderEnabled("gps")) {
                    new Criteria().setAccuracy(1);
                    this.n = locationManager.getLastKnownLocation("gps");
                    if (this.n == null) {
                        locationManager.requestLocationUpdates("gps", 0, 0.0f, new a());
                    } else {
                        H = String.valueOf(this.n.getLongitude());
                        I = String.valueOf(this.n.getLatitude());
                    }
                } else {
                    new Criteria().setAccuracy(1);
                    this.n = locationManager.getLastKnownLocation("network");
                    if (this.n == null) {
                        locationManager.requestLocationUpdates("network", 0, 0.0f, new a());
                    } else {
                        H = String.valueOf(this.n.getLongitude());
                        I = String.valueOf(this.n.getLatitude());
                    }
                }
            }
            TelephonyManager telephonyManager = (TelephonyManager) J.getSystemService("phone");
            this.f = telephonyManager.getDeviceId();
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(this.f.getBytes(), 0, this.f.length());
            b = new BigInteger(1, instance.digest()).toString(16);
            w = new Date().toString();
            B = Build.MODEL;
            C = Build.MANUFACTURER;
            A = telephonyManager.getNetworkOperatorName();
            z = telephonyManager.getSimOperatorName();
            y = Build.VERSION.SDK;
            o = Settings.Secure.getString(J.getContentResolver(), "android_id");
            x = J.getPackageName();
            G = String.valueOf(b) + D + w;
            MessageDigest instance2 = MessageDigest.getInstance("MD5");
            instance2.update(G.getBytes(), 0, G.length());
            G = new BigInteger(1, instance2.digest()).toString(16);
            try {
                w = new Date().toString();
                SharedPreferences.Editor edit = J.getSharedPreferences("dataPrefs", 2).edit();
                edit.putString("apikey", E);
                edit.putString("appId", D);
                edit.putString("imei", b);
                edit.putString("connectionType", q);
                edit.putString("token", G);
                edit.putString("request_timestamp", w);
                edit.putString("packageName", x);
                edit.putString("version", y);
                edit.putString("carrier", z);
                edit.putString("networkOperator", A);
                edit.putString("phoneModel", B);
                edit.putString("manufacturer", C);
                edit.putString("longitude", H);
                edit.putString("latitude", I);
                edit.putString("sdkversion", "4.02");
                edit.putString("android_id", o);
                edit.putBoolean("showDialog", this.g);
                edit.putBoolean("showAd", this.i);
                edit.putBoolean("testMode", r);
                edit.putBoolean("doPush", this.k);
                edit.putBoolean("doSearch", this.l);
                edit.putBoolean("searchIconTestMode", this.m);
                edit.putInt("icon", h);
                edit.putString("useragent", p);
                this.j = d.a(String.valueOf(D) + this.f + q + G + w + x + y + z + A + B + C + H + I + p);
                edit.putString("asp", this.j);
                edit.putString("imeinumber", this.f);
                edit.commit();
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            Log.i("AirpushSDK", "SetPrefrences Error : " + e3.toString());
        }
    }

    private static String c(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            a = jSONObject;
            return jSONObject.getString("appid");
        } catch (JSONException e2) {
            return "invalid Id";
        }
    }

    private static String d(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            a = jSONObject;
            return jSONObject.getString("authkey");
        } catch (JSONException e2) {
            return "invalid key";
        }
    }

    protected static List<NameValuePair> a(Context context) {
        try {
            if (!context.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("dataPrefs", 1);
                D = sharedPreferences.getString("appId", "invalid");
                E = sharedPreferences.getString("apikey", "airpush");
                b = sharedPreferences.getString("imei", "invalid");
                G = sharedPreferences.getString("token", "invalid");
                w = new Date().toString();
                x = sharedPreferences.getString("packageName", "invalid");
                y = sharedPreferences.getString("version", "invalid");
                z = sharedPreferences.getString("carrier", "invalid");
                A = sharedPreferences.getString("networkOperator", "invalid");
                B = sharedPreferences.getString("phoneModel", "invalid");
                C = sharedPreferences.getString("manufacturer", "invalid");
                H = sharedPreferences.getString("longitude", "invalid");
                I = sharedPreferences.getString("latitude", "invalid");
                s = sharedPreferences.getString("sdkversion", "4.02");
                q = sharedPreferences.getString("connectionType", "0");
                r = sharedPreferences.getBoolean("testMode", false);
                p = sharedPreferences.getString("useragent", "Default");
                h = sharedPreferences.getInt("icon", 17301514);
                o = sharedPreferences.getString("android_id", "Android_id");
            } else {
                x = J.getPackageName();
                String a2 = g.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + x, J);
                K = a2;
                D = c(a2);
                E = d(K);
            }
            try {
                ArrayList arrayList = new ArrayList();
                c = arrayList;
                arrayList.add(new BasicNameValuePair("apikey", E));
                c.add(new BasicNameValuePair("appId", D));
                c.add(new BasicNameValuePair("imei", b));
                c.add(new BasicNameValuePair("token", G));
                c.add(new BasicNameValuePair("request_timestamp", w));
                c.add(new BasicNameValuePair("packageName", x));
                c.add(new BasicNameValuePair("version", y));
                c.add(new BasicNameValuePair("carrier", z));
                c.add(new BasicNameValuePair("networkOperator", A));
                c.add(new BasicNameValuePair("phoneModel", B));
                c.add(new BasicNameValuePair("manufacturer", C));
                c.add(new BasicNameValuePair("longitude", H));
                c.add(new BasicNameValuePair("latitude", I));
                c.add(new BasicNameValuePair("sdkversion", s));
                c.add(new BasicNameValuePair("wifi", q));
                c.add(new BasicNameValuePair("useragent", p));
                c.add(new BasicNameValuePair("android_id", o));
                c.add(new BasicNameValuePair("longitude", H));
                c.add(new BasicNameValuePair("latitude", I));
                d = "http://api.airpush.com/v2/api.php?apikey=" + E + "&appId=" + D + "&imei=" + b + "&token=" + G + "&request_timestamp=" + w + "&packageName=" + x + "&version=" + y + "&carrier=" + z + "&networkOperator=" + A + "&phoneModel=" + B + "&manufacturer=" + C + "&longitude=" + H + "&latitude=" + I + "&sdkversion=" + s + "&wifi=" + q + "&useragent=" + p;
            } catch (Exception e2) {
                Log.i("AirpushSDK", "SetValues Error : " + e2.toString());
            }
        } catch (Exception e3) {
            Log.i("AirpushSDK", "Fetching Local Data Error : " + e3.toString());
        }
        return c;
    }

    protected static String a(Context context, String str) {
        if (context.getSharedPreferences("ipPrefs", 1).equals(null)) {
            return "not Found";
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("ipPrefs", 1);
        L = sharedPreferences;
        return sharedPreferences.getString(str, "not Found");
    }

    public static boolean b(Context context) {
        if (context.getSharedPreferences("sdkPrefs", 1).equals(null)) {
            return true;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("sdkPrefs", 1);
        if (sharedPreferences.contains("SDKEnabled")) {
            return sharedPreferences.getBoolean("SDKEnabled", false);
        }
        return true;
    }

    /* compiled from: SetPreferences */
    public class a implements LocationListener {
        public a() {
        }

        public final void onLocationChanged(Location location) {
            try {
                p.H = String.valueOf(location.getLongitude());
                p.I = String.valueOf(location.getLatitude());
            } catch (Exception e) {
            }
        }

        public final void onProviderDisabled(String str) {
        }

        public final void onProviderEnabled(String str) {
        }

        public final void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }
}
