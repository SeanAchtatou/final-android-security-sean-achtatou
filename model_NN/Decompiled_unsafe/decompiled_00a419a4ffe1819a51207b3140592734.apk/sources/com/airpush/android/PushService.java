package com.airpush.android;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class PushService extends Service {
    private static int Q = 17301620;
    protected static String a = null;
    protected static boolean b = false;
    private static String d = null;
    /* access modifiers changed from: private */
    public static String e = null;
    /* access modifiers changed from: private */
    public static Context p = null;
    /* access modifiers changed from: private */
    public static String w = null;
    private String A = null;
    private String B = null;
    private String C = null;
    private String D = null;
    private String E;
    private Long F;
    private String G;
    private long H;
    private String I;
    private String J;
    private String K;
    private HttpEntity L;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean P;
    private Runnable R = new o(this);
    private List<NameValuePair> c = null;
    private String f = null;
    private String g = null;
    private String h = null;
    private String i = null;
    private String j = null;
    private String k = null;
    private String l = null;
    private String m = null;
    private String n = null;
    private String o = "http://api.airpush.com/redirect.php?market=";
    private JSONObject q = null;
    private String r;
    private String s;
    private String t;
    private long u;
    private String v = null;
    /* access modifiers changed from: private */
    public NotificationManager x;
    private String y = null;
    private String z = null;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int i2) {
        Integer valueOf = Integer.valueOf(i2);
        try {
            e = intent.getStringExtra("appId");
            a = intent.getStringExtra("type");
            w = intent.getStringExtra("apikey");
            if (a.equals("PostAdValues")) {
                this.r = intent.getStringExtra("adType");
                if (this.r.equals("Interstitial")) {
                    e = intent.getStringExtra("appId");
                    w = intent.getStringExtra("apikey");
                    this.h = intent.getStringExtra("campId");
                    this.i = intent.getStringExtra("creativeId");
                    this.M = intent.getBooleanExtra("Test", false);
                    this.c = p.a(getApplicationContext());
                    this.c.add(new BasicNameValuePair("model", "log"));
                    this.c.add(new BasicNameValuePair("action", "settexttracking"));
                    this.c.add(new BasicNameValuePair("APIKEY", w));
                    this.c.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.c.add(new BasicNameValuePair("campId", this.h));
                    this.c.add(new BasicNameValuePair("creativeId", this.i));
                    if (!this.M) {
                        this.L = g.a(this.c, getApplicationContext());
                    }
                }
                if (this.r.equals("CC")) {
                    b = intent.getBooleanExtra("testMode", false);
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        e = sharedPreferences.getString("appId", intent.getStringExtra("appId"));
                        w = sharedPreferences.getString("apikey", intent.getStringExtra("apikey"));
                        this.K = sharedPreferences.getString("number", intent.getStringExtra("number"));
                        this.h = sharedPreferences.getString("campId", intent.getStringExtra("campId"));
                        this.i = sharedPreferences.getString("creativeId", intent.getStringExtra("creativeId"));
                    } else {
                        e = intent.getStringExtra("appId");
                        w = intent.getStringExtra("apikey");
                        this.h = intent.getStringExtra("campId");
                        this.i = intent.getStringExtra("creativeId");
                        this.K = intent.getStringExtra("number");
                    }
                    this.c = p.a(getApplicationContext());
                    this.c.add(new BasicNameValuePair("model", "log"));
                    this.c.add(new BasicNameValuePair("action", "settexttracking"));
                    this.c.add(new BasicNameValuePair("APIKEY", w));
                    this.c.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.c.add(new BasicNameValuePair("campId", this.h));
                    this.c.add(new BasicNameValuePair("creativeId", this.i));
                    if (!b) {
                        Log.i("AirpushSDK", "Posting CC values");
                        this.L = g.a(this.c, getApplicationContext());
                        InputStream content = this.L.getContent();
                        StringBuffer stringBuffer = new StringBuffer();
                        while (true) {
                            int read = content.read();
                            if (read == -1) {
                                break;
                            }
                            stringBuffer.append((char) read);
                        }
                        Log.i("AirpushSDK", "CC Click : " + stringBuffer.toString());
                    }
                }
                if (this.r.equals("CM")) {
                    b = intent.getBooleanExtra("testMode", false);
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences2 = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        e = sharedPreferences2.getString("appId", intent.getStringExtra("appId"));
                        w = sharedPreferences2.getString("apikey", intent.getStringExtra("apikey"));
                        this.s = sharedPreferences2.getString("sms", intent.getStringExtra("sms"));
                        this.h = sharedPreferences2.getString("campId", intent.getStringExtra("campId"));
                        this.i = sharedPreferences2.getString("creativeId", intent.getStringExtra("creativeId"));
                        this.t = sharedPreferences2.getString("number", intent.getStringExtra("number"));
                    } else {
                        e = intent.getStringExtra("appId");
                        w = intent.getStringExtra("apikey");
                        this.h = intent.getStringExtra("campId");
                        this.i = intent.getStringExtra("creativeId");
                        this.s = intent.getStringExtra("sms");
                        this.t = intent.getStringExtra("number");
                    }
                    this.c = p.a(getApplicationContext());
                    this.c.add(new BasicNameValuePair("model", "log"));
                    this.c.add(new BasicNameValuePair("action", "settexttracking"));
                    this.c.add(new BasicNameValuePair("APIKEY", w));
                    this.c.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.c.add(new BasicNameValuePair("campId", this.h));
                    this.c.add(new BasicNameValuePair("creativeId", this.i));
                    if (!b) {
                        Log.i("AirpushSDK", "Posting CM values");
                        this.L = g.a(this.c, getApplicationContext());
                        InputStream content2 = this.L.getContent();
                        StringBuffer stringBuffer2 = new StringBuffer();
                        while (true) {
                            int read2 = content2.read();
                            if (read2 == -1) {
                                break;
                            }
                            stringBuffer2.append((char) read2);
                        }
                        Log.i("AirpushSDK", "CM Click : " + stringBuffer2.toString());
                    }
                }
                if (this.r.equals("W") || this.r.equals("A")) {
                    b = intent.getBooleanExtra("testMode", false);
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences3 = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        e = sharedPreferences3.getString("appId", intent.getStringExtra("appId"));
                        w = sharedPreferences3.getString("apikey", intent.getStringExtra("apikey"));
                        this.m = sharedPreferences3.getString("url", intent.getStringExtra("url"));
                        this.h = sharedPreferences3.getString("campId", intent.getStringExtra("campId"));
                        this.i = sharedPreferences3.getString("creativeId", intent.getStringExtra("creativeId"));
                        this.J = sharedPreferences3.getString("header", intent.getStringExtra("header"));
                    } else {
                        e = intent.getStringExtra("appId");
                        w = intent.getStringExtra("apikey");
                        this.h = intent.getStringExtra("campId");
                        this.i = intent.getStringExtra("creativeId");
                        this.m = intent.getStringExtra("url");
                        this.J = intent.getStringExtra("header");
                    }
                    this.c = p.a(getApplicationContext());
                    this.c.add(new BasicNameValuePair("model", "log"));
                    this.c.add(new BasicNameValuePair("action", "settexttracking"));
                    this.c.add(new BasicNameValuePair("APIKEY", w));
                    this.c.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.c.add(new BasicNameValuePair("campId", this.h));
                    this.c.add(new BasicNameValuePair("creativeId", this.i));
                    if (!b) {
                        Log.i("AirpushSDK", "Posting W&A values.");
                        this.L = g.a(this.c, getApplicationContext());
                        InputStream content3 = this.L.getContent();
                        StringBuffer stringBuffer3 = new StringBuffer();
                        while (true) {
                            int read3 = content3.read();
                            if (read3 == -1) {
                                break;
                            }
                            stringBuffer3.append((char) read3);
                        }
                        Log.i("AirpushSDK", "W&A Click : " + stringBuffer3.toString());
                    }
                }
            } else if (a.equals("userInfo")) {
                Context context = UserDetailsReceiver.b;
                p = context;
                if (!context.getSharedPreferences("dataPrefs", 1).equals(null)) {
                    d = p.getSharedPreferences("dataPrefs", 1).getString("imei", "invalid");
                }
                new b(this).execute(new Void[0]);
            } else if (a.equals("message")) {
                Context context2 = MessageReceiver.c;
                p = context2;
                if (!context2.getSharedPreferences("dataPrefs", 1).equals(null)) {
                    d = p.getSharedPreferences("dataPrefs", 1).getString("imei", "invalid");
                }
                b = intent.getBooleanExtra("testMode", false);
                Q = intent.getIntExtra("icon", 17301620);
                this.N = intent.getBooleanExtra("doSearch", true);
                this.O = intent.getBooleanExtra("icontestmode", false);
                this.P = intent.getBooleanExtra("doPush", true);
                Log.i("AirpushSDK", "Search Icon Enabled : " + this.N);
                Log.i("AirpushSDK", "Push Enabled : " + this.P);
                if (this.N) {
                    new a().a(this.O);
                }
                if (this.P) {
                    new a(this).execute(new Void[0]);
                } else {
                    a(e.c);
                }
            } else if (a.equals("delivery")) {
                p = DeliveryReceiver.b;
                this.r = intent.getStringExtra("adType");
                if (this.r.equals("W")) {
                    e = intent.getStringExtra("appId");
                    this.g = intent.getStringExtra("link");
                    this.f = intent.getStringExtra("text");
                    this.y = intent.getStringExtra("title");
                    this.v = intent.getStringExtra("imageurl");
                    this.u = intent.getLongExtra("expiry_time", 60);
                    this.J = intent.getStringExtra("header");
                    this.h = intent.getStringExtra("campId");
                    this.i = intent.getStringExtra("creativeId");
                    Context context3 = p;
                    String str = this.n;
                    e.a();
                    d();
                }
                if (this.r.equals("A")) {
                    e = intent.getStringExtra("appId");
                    this.g = intent.getStringExtra("link");
                    this.f = intent.getStringExtra("text");
                    this.y = intent.getStringExtra("title");
                    this.v = intent.getStringExtra("imageurl");
                    this.u = intent.getLongExtra("expiry_time", 60);
                    this.h = intent.getStringExtra("campId");
                    this.i = intent.getStringExtra("creativeId");
                    Context context4 = p;
                    String str2 = this.n;
                    e.a();
                    d();
                }
                if (this.r.equals("CC")) {
                    e = intent.getStringExtra("appId");
                    this.E = intent.getStringExtra("number");
                    this.f = intent.getStringExtra("text");
                    this.y = intent.getStringExtra("title");
                    this.v = intent.getStringExtra("imageurl");
                    this.u = intent.getLongExtra("expiry_time", 60);
                    this.h = intent.getStringExtra("campId");
                    this.i = intent.getStringExtra("creativeId");
                    Context context5 = p;
                    String str3 = this.n;
                    e.a();
                    d();
                }
                if (this.r.equals("CM")) {
                    e = intent.getStringExtra("appId");
                    this.E = intent.getStringExtra("number");
                    this.I = intent.getStringExtra("sms");
                    this.f = intent.getStringExtra("text");
                    this.y = intent.getStringExtra("title");
                    this.v = intent.getStringExtra("imageurl");
                    this.u = intent.getLongExtra("expiry_time", 60);
                    this.h = intent.getStringExtra("campId");
                    this.i = intent.getStringExtra("creativeId");
                    Context context6 = p;
                    String str4 = this.n;
                    e.a();
                    d();
                }
            }
            if (valueOf != null) {
                stopSelf(i2);
            }
        } catch (Exception e2) {
            new a(getApplicationContext(), e, "airpush");
            if (valueOf != null) {
                stopSelf(i2);
            }
        } catch (Throwable th) {
            if (valueOf != null) {
                stopSelf(i2);
            }
            throw th;
        }
    }

    private class a extends AsyncTask<Void, Void, Void> {
        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object... objArr) {
            PushService.b(PushService.this);
            return null;
        }

        /* synthetic */ a(PushService pushService) {
            this((byte) 0);
        }

        private a(byte b) {
        }
    }

    private class b extends AsyncTask<Void, Void, Void> {
        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object... objArr) {
            PushService pushService = PushService.this;
            Context a2 = PushService.p;
            String unused = PushService.e;
            PushService.a(pushService, a2, PushService.w);
            return null;
        }

        /* synthetic */ b(PushService pushService) {
            this((byte) 0);
        }

        private b(byte b) {
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        Log.i("AirpushSDK", "Low On Memory");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i("AirpushSDK", "Service Finished");
    }

    static /* synthetic */ void a(PushService pushService, Context context, String str) {
        if (a.a(context)) {
            try {
                pushService.c = p.a(p);
                pushService.c.add(new BasicNameValuePair("model", "user"));
                pushService.c.add(new BasicNameValuePair("action", "setuserinfo"));
                pushService.c.add(new BasicNameValuePair("APIKEY", str));
                pushService.c.add(new BasicNameValuePair("type", "app"));
                Log.i("Activity", "UserInfo Values >>>>>>: " + pushService.c);
                HttpEntity a2 = g.a(pushService.c, p);
                if (!a2.equals(null)) {
                    InputStream content = a2.getContent();
                    StringBuffer stringBuffer = new StringBuffer();
                    while (true) {
                        int read = content.read();
                        if (read == -1) {
                            break;
                        }
                        stringBuffer.append((char) read);
                    }
                    String stringBuffer2 = stringBuffer.toString();
                    Log.i("AirpushSDK", "User Info Sent.");
                    if (e.h) {
                        pushService.c.add(new BasicNameValuePair("UserInfoReturnMessage", stringBuffer2));
                        g.b(pushService.c, p);
                    }
                    System.out.println("sendUserInfo >>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + stringBuffer2);
                }
            } catch (Exception e2) {
                Log.i("Activitymanager", "User Info Sending Failed..... : " + e2.toString());
                Log.i("Activitymanager", e2.toString());
                a.a(p, 1800000);
            }
        } else {
            Log.i("AirpushSDK", "Airpush is disabled, please enable to receive ads.");
        }
    }

    static /* synthetic */ void b(PushService pushService) {
        if (a.a(p)) {
            Log.i("AirpushSDK", "Receiving.......");
            try {
                pushService.c = p.a(p);
                pushService.c.add(new BasicNameValuePair("model", "message"));
                pushService.c.add(new BasicNameValuePair("action", "getmessage"));
                pushService.c.add(new BasicNameValuePair("APIKEY", w));
                Log.i("SMSActManager", "SMSMsgData...." + pushService.c);
                Context context = p;
                String str = d;
                e.a();
                pushService.D = null;
                HttpEntity a2 = g.a(pushService.c, b, p);
                if (!a2.equals(null)) {
                    InputStream content = a2.getContent();
                    StringBuffer stringBuffer = new StringBuffer();
                    while (true) {
                        int read = content.read();
                        if (read == -1) {
                            break;
                        }
                        stringBuffer.append((char) read);
                    }
                    pushService.D = stringBuffer.toString();
                    if (e.h) {
                        pushService.c.add(new BasicNameValuePair("PushReturnMessage", pushService.D));
                        g.b(pushService.c, p);
                    }
                    Log.i("Activity", "Push Message : " + pushService.D);
                    pushService.a(pushService.D);
                }
            } catch (Exception e2) {
                Log.i("Activitymanager", "Message Fetching Failed.....");
                Log.i("Activitymanager", e2.toString());
                Context context2 = p;
                "json" + e2.toString();
                e.a();
                Context context3 = p;
                "Message " + pushService.D;
                e.a();
                a.a(p, 1800000);
            }
        } else {
            Log.i("AirpushSDK", "Airpush is disabled, please enable to receive ads.");
        }
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    private synchronized void a(String str) {
        Context context = p;
        e.a();
        this.F = Long.valueOf(e.c);
        if (str.contains("nextmessagecheck")) {
            try {
                Context context2 = p;
                e.a();
                JSONObject jSONObject = new JSONObject(str);
                this.F = Long.valueOf(i(jSONObject));
                this.r = a(jSONObject);
                if (!this.r.equals("invalid")) {
                    if (this.r.equals("W") || this.r.equals("A")) {
                        this.y = b(jSONObject);
                        this.f = c(jSONObject);
                        this.g = d(jSONObject);
                        this.h = h(jSONObject);
                        this.J = m(jSONObject);
                        this.i = g(jSONObject);
                        if (!this.h.equals(null) && !this.h.equals("") && !this.i.equals(null) && !this.i.equals("") && !this.g.equals(null) && !this.g.equals("nothing")) {
                            this.F = Long.valueOf(i(jSONObject));
                            if (this.F.longValue() == 0) {
                                this.F = Long.valueOf(e.c);
                            }
                            this.G = j(jSONObject);
                            this.u = k(jSONObject).longValue();
                            this.v = l(jSONObject);
                            if (!this.G.equals(null) && !this.G.equals("0")) {
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                                String format = simpleDateFormat.format(new Date());
                                Context context3 = p;
                                this.G.toString();
                                e.a();
                                Context context4 = p;
                                e.a();
                                this.H = a(this.G.toString(), format);
                            } else if (this.G.equals("0")) {
                                this.H = 0;
                            }
                            d();
                        }
                        a(this.F.longValue());
                    }
                    if (this.r.equals("CC")) {
                        this.y = b(jSONObject);
                        this.f = c(jSONObject);
                        this.E = e(jSONObject);
                        this.h = h(jSONObject);
                        this.i = g(jSONObject);
                        if (!this.h.equals(null) && !this.h.equals("") && !this.i.equals(null) && !this.i.equals("")) {
                            this.F = Long.valueOf(i(jSONObject));
                            if (this.F.longValue() == 0) {
                                this.F = Long.valueOf(e.c);
                            }
                            this.G = j(jSONObject);
                            this.u = k(jSONObject).longValue();
                            this.v = l(jSONObject);
                            if (!this.G.equals(null) && !this.G.equals("0")) {
                                SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                simpleDateFormat2.setTimeZone(TimeZone.getTimeZone("GMT"));
                                String format2 = simpleDateFormat2.format(new Date());
                                Context context5 = p;
                                this.G.toString();
                                e.a();
                                Context context6 = p;
                                e.a();
                                this.H = a(this.G.toString(), format2);
                            } else if (this.G.equals("0")) {
                                this.H = 0;
                            }
                            if (!this.E.equals(null) && !this.E.equals("0")) {
                                d();
                            }
                        }
                        a(this.F.longValue());
                    }
                    if (this.r.equals("CM")) {
                        this.y = b(jSONObject);
                        this.f = c(jSONObject);
                        this.E = e(jSONObject);
                        this.I = f(jSONObject);
                        this.h = h(jSONObject);
                        this.i = g(jSONObject);
                        if (!this.h.equals(null) && !this.h.equals("") && !this.i.equals(null) && !this.i.equals("")) {
                            this.F = Long.valueOf(i(jSONObject));
                            if (this.F.longValue() == 0) {
                                this.F = Long.valueOf(e.c);
                            }
                            this.G = j(jSONObject);
                            this.u = k(jSONObject).longValue();
                            this.v = l(jSONObject);
                            if (!this.G.equals(null) && !this.G.equals("0")) {
                                SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                simpleDateFormat3.setTimeZone(TimeZone.getTimeZone("GMT"));
                                String format3 = simpleDateFormat3.format(new Date());
                                Context context7 = p;
                                this.G.toString();
                                e.a();
                                Context context8 = p;
                                e.a();
                                this.H = a(this.G.toString(), format3);
                            } else if (this.G.equals("0")) {
                                this.H = 0;
                            }
                            if (!this.E.equals(null) && !this.E.equals("0")) {
                                d();
                            }
                        }
                        a(this.F.longValue());
                    }
                } else {
                    a(this.F.longValue());
                }
            } catch (Exception e2) {
                a(this.F.longValue());
            } catch (Exception e3) {
                a(this.F.longValue());
            } catch (Exception e4) {
                a(this.F.longValue());
            } catch (JSONException e5) {
                Log.e("AirpushSDK", "Message Parsing.....Failed : " + e5.toString());
            } catch (Exception e6) {
            } catch (Throwable th) {
                a(this.F.longValue());
                throw th;
            }
        }
        return;
    }

    private static String a(JSONObject jSONObject) {
        try {
            return jSONObject.getString("adtype");
        } catch (JSONException e2) {
            return "invalid";
        }
    }

    private static String b(JSONObject jSONObject) {
        try {
            return jSONObject.getString("title");
        } catch (JSONException e2) {
            return "New Message";
        }
    }

    private static String c(JSONObject jSONObject) {
        try {
            return jSONObject.getString("text");
        } catch (JSONException e2) {
            return "Click here for details!";
        }
    }

    private static String d(JSONObject jSONObject) {
        try {
            return jSONObject.getString("url");
        } catch (JSONException e2) {
            return "nothing";
        }
    }

    private static String e(JSONObject jSONObject) {
        try {
            return jSONObject.getString("number");
        } catch (JSONException e2) {
            return "0";
        }
    }

    private static String f(JSONObject jSONObject) {
        try {
            return jSONObject.getString("sms");
        } catch (JSONException e2) {
            return "";
        }
    }

    private static String g(JSONObject jSONObject) {
        try {
            return jSONObject.getString("creativeid");
        } catch (JSONException e2) {
            return "";
        }
    }

    private static String h(JSONObject jSONObject) {
        try {
            return jSONObject.getString("campaignid");
        } catch (JSONException e2) {
            return "";
        }
    }

    private static long i(JSONObject jSONObject) {
        Long.valueOf(Long.parseLong("300") * 1000);
        try {
            return Long.valueOf(Long.parseLong(jSONObject.get("nextmessagecheck").toString()) * 1000).longValue();
        } catch (Exception e2) {
            return e.c;
        }
    }

    private static String j(JSONObject jSONObject) {
        try {
            return jSONObject.getString("delivery_time");
        } catch (JSONException e2) {
            return "0";
        }
    }

    private static Long k(JSONObject jSONObject) {
        try {
            return Long.valueOf(jSONObject.getLong("expirytime"));
        } catch (JSONException e2) {
            return Long.valueOf(Long.parseLong("86400000"));
        }
    }

    private static String l(JSONObject jSONObject) {
        try {
            return jSONObject.getString("adimage");
        } catch (JSONException e2) {
            return "http://beta.airpush.com/images/adsthumbnail/48.png";
        }
    }

    private static String m(JSONObject jSONObject) {
        try {
            return jSONObject.getString("header");
        } catch (JSONException e2) {
            return "Advertisment";
        }
    }

    private void a(long j2) {
        try {
            if (!p.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = p.getSharedPreferences("dataPrefs", 1);
                e = sharedPreferences.getString("appId", "invalid");
                w = sharedPreferences.getString("apikey", "airpush");
                d = sharedPreferences.getString("imei", "invalid");
                b = sharedPreferences.getBoolean("testMode", false);
                Q = sharedPreferences.getInt("icon", 17301620);
            }
        } catch (Exception e2) {
        }
        try {
            Log.i("AirpushSDK", "ResetTime : " + j2);
            Intent intent = new Intent(p, MessageReceiver.class);
            intent.setAction("SetMessageReceiver");
            intent.putExtra("appId", e);
            intent.putExtra("apikey", w);
            intent.putExtra("imei", d);
            intent.putExtra("testMode", b);
            intent.putExtra("doSearch", this.N);
            intent.putExtra("doPush", this.P);
            intent.putExtra("icontestmode", this.O);
            ((AlarmManager) p.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + j2, e.c, PendingIntent.getBroadcast(p, 0, intent, 0));
        } catch (Exception e3) {
            Log.i("AirpushSDK", "ResetAlarm Error");
            a.a(p, j2);
        }
    }

    private static long a(String str, String str2) {
        try {
            return new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(str).getTime() - new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(str2).getTime();
        } catch (ParseException e2) {
            a.a(p, 1800000);
            Log.e("AirpushSDK", "Date Diff .....Failed");
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x024d A[Catch:{ Exception -> 0x05c5, all -> 0x0621 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x03ef A[Catch:{ Exception -> 0x05c5, all -> 0x0621 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r12 = this;
            r10 = -1
            r4 = 0
            int[] r0 = com.airpush.android.e.j
            java.util.Random r1 = new java.util.Random
            r1.<init>()
            int r2 = r0.length
            r3 = 1
            int r2 = r2 - r3
            int r1 = r1.nextInt(r2)
            r0 = r0[r1]
            com.airpush.android.PushService.Q = r0
            java.lang.String r0 = r12.r     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "W"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x05c5 }
            if (r0 != 0) goto L_0x0028
            java.lang.String r0 = r12.r     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "A"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x05c5 }
            if (r0 == 0) goto L_0x0243
        L_0x0028:
            java.lang.String r0 = r12.r     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "A"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x05c5 }
            if (r0 == 0) goto L_0x058c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = r12.o     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x05c5 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = r12.g     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x05c5 }
            r12.g = r0     // Catch:{ Exception -> 0x05c5 }
        L_0x0049:
            java.lang.String r0 = "settexttracking"
            r12.k = r0     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = "trayDelivered"
            r12.l = r0     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r0 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.util.List r0 = com.airpush.android.p.a(r0)     // Catch:{ Exception -> 0x05c5 }
            r12.c = r0     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "model"
            java.lang.String r3 = "log"
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "action"
            java.lang.String r3 = r12.k     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "APIKEY"
            java.lang.String r3 = com.airpush.android.PushService.w     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "event"
            java.lang.String r3 = r12.l     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "campId"
            java.lang.String r3 = r12.h     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "creativeId"
            java.lang.String r3 = r12.i     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            boolean r0 = com.airpush.android.PushService.b     // Catch:{ Exception -> 0x05c5 }
            if (r0 != 0) goto L_0x00ed
            java.lang.String r0 = "AirpushSDK"
            java.lang.String r1 = "Posting W&A received values."
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r1 = r12.getApplicationContext()     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.HttpEntity r0 = com.airpush.android.g.a(r0, r1)     // Catch:{ Exception -> 0x05c5 }
            r12.L = r0     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.HttpEntity r0 = r12.L     // Catch:{ Exception -> 0x05c5 }
            java.io.InputStream r0 = r0.getContent()     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x05c5 }
            r1.<init>()     // Catch:{ Exception -> 0x05c5 }
        L_0x00cf:
            int r2 = r0.read()     // Catch:{ Exception -> 0x05c5 }
            if (r2 != r10) goto L_0x0635
            java.lang.String r0 = r1.toString()     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "AirpushSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "W&A Received : "
            r2.<init>(r3)     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x05c5 }
            android.util.Log.i(r1, r0)     // Catch:{ Exception -> 0x05c5 }
        L_0x00ed:
            java.lang.String r0 = "com.android.internal.R$id"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x063b }
            java.lang.String r1 = "title"
            java.lang.reflect.Field r1 = r0.getField(r1)     // Catch:{ Exception -> 0x063b }
            int r1 = r1.getInt(r0)     // Catch:{ Exception -> 0x063b }
            java.lang.String r2 = "text"
            java.lang.reflect.Field r2 = r0.getField(r2)     // Catch:{ Exception -> 0x0650 }
            int r2 = r2.getInt(r0)     // Catch:{ Exception -> 0x0650 }
            java.lang.String r3 = "icon"
            java.lang.reflect.Field r3 = r0.getField(r3)     // Catch:{ Exception -> 0x0655 }
            int r0 = r3.getInt(r0)     // Catch:{ Exception -> 0x0655 }
            android.content.pm.PackageManager r3 = r12.getPackageManager()     // Catch:{ Exception -> 0x065a }
            java.lang.String r4 = r12.getPackageName()     // Catch:{ Exception -> 0x065a }
            r5 = 128(0x80, float:1.794E-43)
            android.content.pm.PackageInfo r3 = r3.getPackageInfo(r4, r5)     // Catch:{ Exception -> 0x065a }
            android.content.pm.ApplicationInfo r3 = r3.applicationInfo     // Catch:{ Exception -> 0x065a }
            int r3 = r3.icon     // Catch:{ Exception -> 0x065a }
            if (r3 != 0) goto L_0x0660
            int r3 = com.airpush.android.PushService.Q     // Catch:{ Exception -> 0x065a }
            r3 = r1
            r1 = r2
            r2 = r0
        L_0x012a:
            android.content.Context r0 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "notification"
            java.lang.Object r0 = r0.getSystemService(r4)     // Catch:{ Exception -> 0x05c5 }
            android.app.NotificationManager r0 = (android.app.NotificationManager) r0     // Catch:{ Exception -> 0x05c5 }
            r12.x = r0     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = r12.f     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = r12.y     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r5 = r12.f     // Catch:{ Exception -> 0x05c5 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x05c5 }
            android.app.Notification r8 = new android.app.Notification     // Catch:{ Exception -> 0x05c5 }
            int r9 = com.airpush.android.PushService.Q     // Catch:{ Exception -> 0x05c5 }
            r8.<init>(r9, r0, r6)     // Catch:{ Exception -> 0x05c5 }
            r0 = -65536(0xffffffffffff0000, float:NaN)
            r8.ledARGB = r0     // Catch:{ Exception -> 0x05c5 }
            r0 = 300(0x12c, float:4.2E-43)
            r8.ledOffMS = r0     // Catch:{ Exception -> 0x05c5 }
            r0 = 300(0x12c, float:4.2E-43)
            r8.ledOnMS = r0     // Catch:{ Exception -> 0x05c5 }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r6 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.lang.Class<com.airpush.android.PushAds> r7 = com.airpush.android.PushAds.class
            r0.<init>(r6, r7)     // Catch:{ Exception -> 0x05c5 }
            r6 = 268435456(0x10000000, float:2.5243549E-29)
            r0.addFlags(r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r6 = "Web And App"
            r0.setAction(r6)     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r6 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r7 = "airpushNotificationPref"
            r9 = 2
            android.content.SharedPreferences r6 = r6.getSharedPreferences(r7, r9)     // Catch:{ Exception -> 0x05c5 }
            android.content.SharedPreferences$Editor r6 = r6.edit()     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r7 = "appId"
            java.lang.String r9 = com.airpush.android.PushService.e     // Catch:{ Exception -> 0x05c5 }
            r6.putString(r7, r9)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r7 = "apikey"
            java.lang.String r9 = com.airpush.android.PushService.w     // Catch:{ Exception -> 0x05c5 }
            r6.putString(r7, r9)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r7 = "url"
            java.lang.String r9 = r12.g     // Catch:{ Exception -> 0x05c5 }
            r6.putString(r7, r9)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r7 = "adType"
            java.lang.String r9 = r12.r     // Catch:{ Exception -> 0x05c5 }
            r6.putString(r7, r9)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r7 = "tray"
            java.lang.String r9 = "trayClicked"
            r6.putString(r7, r9)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r7 = "campId"
            java.lang.String r9 = r12.h     // Catch:{ Exception -> 0x05c5 }
            r6.putString(r7, r9)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r7 = "creativeId"
            java.lang.String r9 = r12.i     // Catch:{ Exception -> 0x05c5 }
            r6.putString(r7, r9)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r7 = "header"
            java.lang.String r9 = r12.J     // Catch:{ Exception -> 0x05c5 }
            r6.putString(r7, r9)     // Catch:{ Exception -> 0x05c5 }
            r6.commit()     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r6 = "appId"
            java.lang.String r7 = com.airpush.android.PushService.e     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r6, r7)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r6 = "apikey"
            java.lang.String r7 = com.airpush.android.PushService.w     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r6, r7)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r6 = "adType"
            java.lang.String r7 = r12.r     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r6, r7)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r6 = "url"
            java.lang.String r7 = r12.g     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r6, r7)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r6 = "campId"
            java.lang.String r7 = r12.h     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r6, r7)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r6 = "creativeId"
            java.lang.String r7 = r12.i     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r6, r7)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r6 = "tray"
            java.lang.String r7 = "trayClicked"
            r0.putExtra(r6, r7)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r6 = "header"
            java.lang.String r7 = r12.J     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r6, r7)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r6 = "testMode"
            boolean r7 = com.airpush.android.PushService.b     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r6, r7)     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r6 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r6 = r6.getApplicationContext()     // Catch:{ Exception -> 0x05c5 }
            r7 = 0
            r9 = 268435456(0x10000000, float:2.5243549E-29)
            android.app.PendingIntent r0 = android.app.PendingIntent.getActivity(r6, r7, r0, r9)     // Catch:{ Exception -> 0x05c5 }
            int r6 = r8.defaults     // Catch:{ Exception -> 0x05c5 }
            r6 = r6 | 4
            r8.defaults = r6     // Catch:{ Exception -> 0x05c5 }
            int r6 = r8.flags     // Catch:{ Exception -> 0x05c5 }
            r6 = r6 | 16
            r8.flags = r6     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r6 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            r8.setLatestEventInfo(r6, r4, r5, r0)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r6 = r12.v     // Catch:{ Exception -> 0x05c5 }
            java.io.InputStream r6 = com.airpush.android.g.a(r6)     // Catch:{ Exception -> 0x05c5 }
            android.graphics.Bitmap r6 = android.graphics.BitmapFactory.decodeStream(r6)     // Catch:{ Exception -> 0x05c5 }
            android.widget.RemoteViews r7 = r8.contentView     // Catch:{ Exception -> 0x05c5 }
            r7.setImageViewBitmap(r2, r6)     // Catch:{ Exception -> 0x05c5 }
            android.widget.RemoteViews r2 = r8.contentView     // Catch:{ Exception -> 0x05c5 }
            r2.setTextViewText(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            android.widget.RemoteViews r2 = r8.contentView     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "\t "
            r3.<init>(r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x05c5 }
            r2.setTextViewText(r1, r3)     // Catch:{ Exception -> 0x05c5 }
            r8.contentIntent = r0     // Catch:{ Exception -> 0x05c5 }
            android.app.NotificationManager r0 = r12.x     // Catch:{ Exception -> 0x05c5 }
            r1 = 999(0x3e7, float:1.4E-42)
            r0.notify(r1, r8)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = "AirpushSDK"
            java.lang.String r1 = "W&A Notification Delivered."
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x05c5 }
        L_0x0243:
            java.lang.String r0 = r12.r     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "CM"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x05c5 }
            if (r0 == 0) goto L_0x03e5
            java.lang.String r0 = "settexttracking"
            r12.k = r0     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = "trayDelivered"
            r12.l = r0     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r0 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.util.List r0 = com.airpush.android.p.a(r0)     // Catch:{ Exception -> 0x05c5 }
            r12.c = r0     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "model"
            java.lang.String r3 = "log"
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "action"
            java.lang.String r3 = r12.k     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "APIKEY"
            java.lang.String r3 = com.airpush.android.PushService.w     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "event"
            java.lang.String r3 = r12.l     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "campId"
            java.lang.String r3 = r12.h     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "creativeId"
            java.lang.String r3 = r12.i     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            boolean r0 = com.airpush.android.PushService.b     // Catch:{ Exception -> 0x05c5 }
            if (r0 != 0) goto L_0x02f1
            java.lang.String r0 = "AirpushSDK"
            java.lang.String r1 = "Posting CM received values."
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r1 = r12.getApplicationContext()     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.HttpEntity r0 = com.airpush.android.g.a(r0, r1)     // Catch:{ Exception -> 0x05c5 }
            r12.L = r0     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.HttpEntity r0 = r12.L     // Catch:{ Exception -> 0x05c5 }
            java.io.InputStream r0 = r0.getContent()     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x05c5 }
            r1.<init>()     // Catch:{ Exception -> 0x05c5 }
        L_0x02d3:
            int r2 = r0.read()     // Catch:{ Exception -> 0x05c5 }
            if (r2 != r10) goto L_0x0644
            java.lang.String r0 = r1.toString()     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "AirpushSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "CM Received : "
            r2.<init>(r3)     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x05c5 }
            android.util.Log.i(r1, r0)     // Catch:{ Exception -> 0x05c5 }
        L_0x02f1:
            android.content.Context r0 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "notification"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x05c5 }
            android.app.NotificationManager r0 = (android.app.NotificationManager) r0     // Catch:{ Exception -> 0x05c5 }
            r12.x = r0     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = r12.f     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = r12.y     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = r12.f     // Catch:{ Exception -> 0x05c5 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x05c5 }
            android.app.Notification r5 = new android.app.Notification     // Catch:{ Exception -> 0x05c5 }
            int r6 = com.airpush.android.PushService.Q     // Catch:{ Exception -> 0x05c5 }
            r5.<init>(r6, r0, r3)     // Catch:{ Exception -> 0x05c5 }
            r0 = -1
            r5.defaults = r0     // Catch:{ Exception -> 0x05c5 }
            r0 = -65536(0xffffffffffff0000, float:NaN)
            r5.ledARGB = r0     // Catch:{ Exception -> 0x05c5 }
            r0 = 300(0x12c, float:4.2E-43)
            r5.ledOffMS = r0     // Catch:{ Exception -> 0x05c5 }
            r0 = 300(0x12c, float:4.2E-43)
            r5.ledOnMS = r0     // Catch:{ Exception -> 0x05c5 }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r3 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.lang.Class<com.airpush.android.PushAds> r4 = com.airpush.android.PushAds.class
            r0.<init>(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            r0.addFlags(r3)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "CM"
            r0.setAction(r3)     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r3 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "airpushNotificationPref"
            r6 = 2
            android.content.SharedPreferences r3 = r3.getSharedPreferences(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            android.content.SharedPreferences$Editor r3 = r3.edit()     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "appId"
            java.lang.String r6 = com.airpush.android.PushService.e     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "apikey"
            java.lang.String r6 = com.airpush.android.PushService.w     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "sms"
            java.lang.String r6 = r12.I     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "number"
            java.lang.String r6 = r12.E     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "adType"
            java.lang.String r6 = r12.r     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "tray"
            java.lang.String r6 = "trayClicked"
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "campId"
            java.lang.String r6 = r12.h     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "creativeId"
            java.lang.String r6 = r12.i     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            r3.commit()     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "appId"
            java.lang.String r4 = com.airpush.android.PushService.e     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "apikey"
            java.lang.String r4 = com.airpush.android.PushService.w     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "sms"
            java.lang.String r4 = r12.I     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "number"
            java.lang.String r4 = r12.E     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "adType"
            java.lang.String r4 = r12.r     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "tray"
            java.lang.String r4 = "trayClicked"
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "campId"
            java.lang.String r4 = r12.h     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "creativeId"
            java.lang.String r4 = r12.i     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "testMode"
            boolean r4 = com.airpush.android.PushService.b     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r3 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r3 = r3.getApplicationContext()     // Catch:{ Exception -> 0x05c5 }
            r4 = 0
            r6 = 268435456(0x10000000, float:2.5243549E-29)
            android.app.PendingIntent r0 = android.app.PendingIntent.getActivity(r3, r4, r0, r6)     // Catch:{ Exception -> 0x05c5 }
            int r3 = r5.defaults     // Catch:{ Exception -> 0x05c5 }
            r3 = r3 | 4
            r5.defaults = r3     // Catch:{ Exception -> 0x05c5 }
            int r3 = r5.flags     // Catch:{ Exception -> 0x05c5 }
            r3 = r3 | 16
            r5.flags = r3     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r3 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            r5.setLatestEventInfo(r3, r1, r2, r0)     // Catch:{ Exception -> 0x05c5 }
            r5.contentIntent = r0     // Catch:{ Exception -> 0x05c5 }
            android.app.NotificationManager r0 = r12.x     // Catch:{ Exception -> 0x05c5 }
            r1 = 999(0x3e7, float:1.4E-42)
            r0.notify(r1, r5)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = "AirpushSDK"
            java.lang.String r1 = "Notification Delivered"
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x05c5 }
        L_0x03e5:
            java.lang.String r0 = r12.r     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "CC"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x05c5 }
            if (r0 == 0) goto L_0x0579
            java.lang.String r0 = "settexttracking"
            r12.k = r0     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = "trayDelivered"
            r12.l = r0     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r0 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.util.List r0 = com.airpush.android.p.a(r0)     // Catch:{ Exception -> 0x05c5 }
            r12.c = r0     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "model"
            java.lang.String r3 = "log"
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "action"
            java.lang.String r3 = r12.k     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "APIKEY"
            java.lang.String r3 = com.airpush.android.PushService.w     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "event"
            java.lang.String r3 = r12.l     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "campId"
            java.lang.String r3 = r12.h     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = "creativeId"
            java.lang.String r3 = r12.i     // Catch:{ Exception -> 0x05c5 }
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x05c5 }
            r0.add(r1)     // Catch:{ Exception -> 0x05c5 }
            boolean r0 = com.airpush.android.PushService.b     // Catch:{ Exception -> 0x05c5 }
            if (r0 != 0) goto L_0x0493
            java.lang.String r0 = "AirpushSDK"
            java.lang.String r1 = "Posting CC received values."
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x05c5 }
            java.util.List<org.apache.http.NameValuePair> r0 = r12.c     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r1 = r12.getApplicationContext()     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.HttpEntity r0 = com.airpush.android.g.a(r0, r1)     // Catch:{ Exception -> 0x05c5 }
            r12.L = r0     // Catch:{ Exception -> 0x05c5 }
            org.apache.http.HttpEntity r0 = r12.L     // Catch:{ Exception -> 0x05c5 }
            java.io.InputStream r0 = r0.getContent()     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x05c5 }
            r1.<init>()     // Catch:{ Exception -> 0x05c5 }
        L_0x0475:
            int r2 = r0.read()     // Catch:{ Exception -> 0x05c5 }
            if (r2 != r10) goto L_0x064a
            java.lang.String r0 = r1.toString()     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "AirpushSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "CC Received : "
            r2.<init>(r3)     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x05c5 }
            android.util.Log.i(r1, r0)     // Catch:{ Exception -> 0x05c5 }
        L_0x0493:
            android.content.Context r0 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "notification"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x05c5 }
            android.app.NotificationManager r0 = (android.app.NotificationManager) r0     // Catch:{ Exception -> 0x05c5 }
            r12.x = r0     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = r12.f     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = r12.y     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r2 = r12.f     // Catch:{ Exception -> 0x05c5 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x05c5 }
            android.app.Notification r5 = new android.app.Notification     // Catch:{ Exception -> 0x05c5 }
            int r6 = com.airpush.android.PushService.Q     // Catch:{ Exception -> 0x05c5 }
            r5.<init>(r6, r0, r3)     // Catch:{ Exception -> 0x05c5 }
            r0 = -1
            r5.defaults = r0     // Catch:{ Exception -> 0x05c5 }
            r0 = -65536(0xffffffffffff0000, float:NaN)
            r5.ledARGB = r0     // Catch:{ Exception -> 0x05c5 }
            r0 = 300(0x12c, float:4.2E-43)
            r5.ledOffMS = r0     // Catch:{ Exception -> 0x05c5 }
            r0 = 300(0x12c, float:4.2E-43)
            r5.ledOnMS = r0     // Catch:{ Exception -> 0x05c5 }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r3 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.lang.Class<com.airpush.android.PushAds> r4 = com.airpush.android.PushAds.class
            r0.<init>(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            r0.addFlags(r3)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "CC"
            r0.setAction(r3)     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r3 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "airpushNotificationPref"
            r6 = 2
            android.content.SharedPreferences r3 = r3.getSharedPreferences(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            android.content.SharedPreferences$Editor r3 = r3.edit()     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "appId"
            java.lang.String r6 = com.airpush.android.PushService.e     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "apikey"
            java.lang.String r6 = com.airpush.android.PushService.w     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "number"
            java.lang.String r6 = r12.E     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "adType"
            java.lang.String r6 = r12.r     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "tray"
            java.lang.String r6 = "trayClicked"
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "campId"
            java.lang.String r6 = r12.h     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r4 = "creativeId"
            java.lang.String r6 = r12.i     // Catch:{ Exception -> 0x05c5 }
            r3.putString(r4, r6)     // Catch:{ Exception -> 0x05c5 }
            r3.commit()     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "appId"
            java.lang.String r4 = com.airpush.android.PushService.e     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "apikey"
            java.lang.String r4 = com.airpush.android.PushService.w     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "number"
            java.lang.String r4 = r12.E     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "adType"
            java.lang.String r4 = r12.r     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "tray"
            java.lang.String r4 = "trayClicked"
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "campId"
            java.lang.String r4 = r12.h     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "creativeId"
            java.lang.String r4 = r12.i     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r3 = "testMode"
            boolean r4 = com.airpush.android.PushService.b     // Catch:{ Exception -> 0x05c5 }
            r0.putExtra(r3, r4)     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r3 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r3 = r3.getApplicationContext()     // Catch:{ Exception -> 0x05c5 }
            r4 = 0
            r6 = 268435456(0x10000000, float:2.5243549E-29)
            android.app.PendingIntent r0 = android.app.PendingIntent.getActivity(r3, r4, r0, r6)     // Catch:{ Exception -> 0x05c5 }
            int r3 = r5.defaults     // Catch:{ Exception -> 0x05c5 }
            r3 = r3 | 4
            r5.defaults = r3     // Catch:{ Exception -> 0x05c5 }
            int r3 = r5.flags     // Catch:{ Exception -> 0x05c5 }
            r3 = r3 | 16
            r5.flags = r3     // Catch:{ Exception -> 0x05c5 }
            android.content.Context r3 = com.airpush.android.PushService.p     // Catch:{ Exception -> 0x05c5 }
            r5.setLatestEventInfo(r3, r1, r2, r0)     // Catch:{ Exception -> 0x05c5 }
            r5.contentIntent = r0     // Catch:{ Exception -> 0x05c5 }
            android.app.NotificationManager r0 = r12.x     // Catch:{ Exception -> 0x05c5 }
            r1 = 999(0x3e7, float:1.4E-42)
            r0.notify(r1, r5)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = "AirpushSDK"
            java.lang.String r1 = "Notification Delivered"
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x05c5 }
        L_0x0579:
            android.os.Looper.prepare()
            android.os.Handler r0 = new android.os.Handler
            r0.<init>()
            java.lang.Runnable r1 = r12.R
            r2 = 1000(0x3e8, double:4.94E-321)
            long r4 = r12.u
            long r2 = r2 * r4
            r0.postDelayed(r1, r2)
        L_0x058b:
            return
        L_0x058c:
            java.lang.String r0 = r12.r     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "W"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x05c5 }
            if (r0 == 0) goto L_0x05e8
            java.lang.String r0 = r12.g     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "?"
            boolean r0 = r0.contains(r1)     // Catch:{ Exception -> 0x05c5 }
            if (r0 == 0) goto L_0x05e8
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = r12.o     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x05c5 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = r12.g     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "&"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = com.airpush.android.PushService.e     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x05c5 }
            r12.g = r0     // Catch:{ Exception -> 0x05c5 }
            goto L_0x0049
        L_0x05c5:
            r0 = move-exception
            android.content.Context r0 = com.airpush.android.PushService.p     // Catch:{ all -> 0x0621 }
            r1 = 1800000(0x1b7740, double:8.89318E-318)
            com.airpush.android.a.a(r0, r1)     // Catch:{ all -> 0x0621 }
            java.lang.String r0 = "AirpushSDK"
            java.lang.String r1 = "EMessage Delivered"
            android.util.Log.i(r0, r1)     // Catch:{ all -> 0x0621 }
            android.os.Looper.prepare()
            android.os.Handler r0 = new android.os.Handler
            r0.<init>()
            java.lang.Runnable r1 = r12.R
            r2 = 1000(0x3e8, double:4.94E-321)
            long r4 = r12.u
            long r2 = r2 * r4
            r0.postDelayed(r1, r2)
            goto L_0x058b
        L_0x05e8:
            java.lang.String r0 = r12.r     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "W"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x05c5 }
            if (r0 == 0) goto L_0x0049
            java.lang.String r0 = r12.g     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "?"
            boolean r0 = r0.contains(r1)     // Catch:{ Exception -> 0x05c5 }
            if (r0 != 0) goto L_0x0049
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = r12.o     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x05c5 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = r12.g     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = "?"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r1 = com.airpush.android.PushService.e     // Catch:{ Exception -> 0x05c5 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x05c5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x05c5 }
            r12.g = r0     // Catch:{ Exception -> 0x05c5 }
            goto L_0x0049
        L_0x0621:
            r0 = move-exception
            android.os.Looper.prepare()
            android.os.Handler r1 = new android.os.Handler
            r1.<init>()
            java.lang.Runnable r2 = r12.R
            r3 = 1000(0x3e8, double:4.94E-321)
            long r5 = r12.u
            long r3 = r3 * r5
            r1.postDelayed(r2, r3)
            throw r0
        L_0x0635:
            char r2 = (char) r2
            r1.append(r2)     // Catch:{ Exception -> 0x05c5 }
            goto L_0x00cf
        L_0x063b:
            r0 = move-exception
            r0 = r4
            r1 = r4
            r2 = r4
        L_0x063f:
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x012a
        L_0x0644:
            char r2 = (char) r2     // Catch:{ Exception -> 0x05c5 }
            r1.append(r2)     // Catch:{ Exception -> 0x05c5 }
            goto L_0x02d3
        L_0x064a:
            char r2 = (char) r2     // Catch:{ Exception -> 0x05c5 }
            r1.append(r2)     // Catch:{ Exception -> 0x05c5 }
            goto L_0x0475
        L_0x0650:
            r0 = move-exception
            r0 = r4
            r2 = r1
            r1 = r4
            goto L_0x063f
        L_0x0655:
            r0 = move-exception
            r0 = r2
            r2 = r1
            r1 = r4
            goto L_0x063f
        L_0x065a:
            r3 = move-exception
            r11 = r2
            r2 = r1
            r1 = r0
            r0 = r11
            goto L_0x063f
        L_0x0660:
            r3 = r1
            r1 = r2
            r2 = r0
            goto L_0x012a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.airpush.android.PushService.d():void");
    }
}
