package com.airpush.android;

import android.content.Context;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HttpContext;

/* compiled from: HttpPostData */
public final class g {
    protected static long a = 1800000;
    private static String b;
    private static Context c;
    private static BasicHttpParams d;
    private static int e;
    private static int f;
    private static DefaultHttpClient g;
    private static HttpEntity h;
    private static BasicHttpResponse i;
    private static HttpPost j;
    private static String k;
    private static String l;

    protected static HttpEntity a(List<NameValuePair> list, Context context) {
        if (e.a(context)) {
            c = context;
            try {
                HttpPost httpPost = new HttpPost("http://api.airpush.com/v2/api.php");
                j = httpPost;
                httpPost.setEntity(new UrlEncodedFormEntity(list));
                d = new BasicHttpParams();
                e = 3000;
                HttpConnectionParams.setConnectionTimeout(d, e);
                f = 3000;
                HttpConnectionParams.setSoTimeout(d, f);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(d);
                g = defaultHttpClient;
                defaultHttpClient.addRequestInterceptor(new HttpRequestInterceptor() {
                    public final void process(HttpRequest httpRequest, HttpContext httpContext) {
                        if (!httpRequest.containsHeader("Accept-Encoding")) {
                            httpRequest.addHeader("Accept-Encoding", "gzip");
                        }
                    }
                });
                g.addResponseInterceptor(new HttpResponseInterceptor() {
                    public final void process(HttpResponse httpResponse, HttpContext httpContext) {
                        Header contentEncoding = httpResponse.getEntity().getContentEncoding();
                        if (contentEncoding != null) {
                            for (HeaderElement name : contentEncoding.getElements()) {
                                if (name.getName().equalsIgnoreCase("gzip")) {
                                    httpResponse.setEntity(new a(httpResponse.getEntity()));
                                    return;
                                }
                            }
                        }
                    }
                });
                i = g.execute(j);
                Log.i("AirpushSDK", "Status Code: " + i.getStatusLine().getStatusCode());
                HttpEntity entity = i.getEntity();
                h = entity;
                return entity;
            } catch (SocketTimeoutException e2) {
                Log.d("SocketTimeoutException Thrown", e2.toString());
                a.a(c, 1800000);
                return null;
            } catch (ClientProtocolException e3) {
                Log.d("ClientProtocolException Thrown", e3.toString());
                a.a(c, 1800000);
                return null;
            } catch (MalformedURLException e4) {
                a.a(c, 1800000);
                Log.d("MalformedURLException Thrown", e4.toString());
                return null;
            } catch (IOException e5) {
                a.a(c, 1800000);
                Log.d("IOException Thrown", e5.toString());
                return null;
            } catch (Exception e6) {
                e6.printStackTrace();
                Log.i("AirpushSDK", e6.toString());
                a.a(c, 1800000);
                return null;
            }
        } else {
            a.a(context, a);
            return null;
        }
    }

    protected static HttpEntity a(List<NameValuePair> list, boolean z, Context context) {
        if (e.a(context)) {
            c = context;
            if (z) {
                try {
                    b = "http://api.airpush.com/testmsg2.php";
                } catch (SocketTimeoutException e2) {
                    Log.d("SocketTimeoutException Thrown", e2.toString());
                    a.a(c, 1800000);
                    return null;
                } catch (ClientProtocolException e3) {
                    Log.d("ClientProtocolException Thrown", e3.toString());
                    a.a(c, 1800000);
                    return null;
                } catch (MalformedURLException e4) {
                    a.a(c, 1800000);
                    Log.d("MalformedURLException Thrown", e4.toString());
                    return null;
                } catch (IOException e5) {
                    a.a(c, 1800000);
                    Log.d("IOException Thrown", e5.toString());
                    return null;
                } catch (Exception e6) {
                    a.a(c, 1800000);
                    return null;
                }
            } else {
                k = p.a(context, "ip1");
                l = p.a(context, "ip2");
                if (!k.equals("not Found")) {
                    b = "http://" + k + "/v2/api.php";
                } else if (!l.equals("not Found")) {
                    b = "http://" + l + "/v2/api.php";
                } else {
                    b = "http://api.airpush.com/v2/api.php";
                }
            }
            HttpPost httpPost = new HttpPost(b);
            j = httpPost;
            httpPost.setEntity(new UrlEncodedFormEntity(list));
            d = new BasicHttpParams();
            e = 10000;
            HttpConnectionParams.setConnectionTimeout(d, e);
            f = 10000;
            HttpConnectionParams.setSoTimeout(d, f);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(d);
            g = defaultHttpClient;
            defaultHttpClient.addRequestInterceptor(new HttpRequestInterceptor() {
                public final void process(HttpRequest httpRequest, HttpContext httpContext) {
                    if (!httpRequest.containsHeader("Accept-Encoding")) {
                        httpRequest.addHeader("Accept-Encoding", "gzip");
                    }
                }
            });
            g.addResponseInterceptor(new HttpResponseInterceptor() {
                public final void process(HttpResponse httpResponse, HttpContext httpContext) {
                    Header contentEncoding = httpResponse.getEntity().getContentEncoding();
                    if (contentEncoding != null) {
                        for (HeaderElement name : contentEncoding.getElements()) {
                            if (name.getName().equalsIgnoreCase("gzip")) {
                                httpResponse.setEntity(new a(httpResponse.getEntity()));
                                return;
                            }
                        }
                    }
                }
            });
            i = g.execute(j);
            Log.i("AirpushSDK", "response line: " + i.getStatusLine());
            HttpEntity entity = i.getEntity();
            h = entity;
            return entity;
        }
        a.a(context, a);
        return null;
    }

    protected static String a(String str, Context context) {
        if (e.a(context)) {
            c = context;
            try {
                if (e.a(context)) {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setConnectTimeout(3000);
                    httpURLConnection.connect();
                    if (httpURLConnection.getResponseCode() == 200) {
                        StringBuffer stringBuffer = new StringBuffer();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                        while (true) {
                            String readLine = bufferedReader.readLine();
                            if (readLine == null) {
                                return stringBuffer.toString();
                            }
                            stringBuffer.append(readLine);
                        }
                    }
                }
            } catch (SocketTimeoutException e2) {
                Log.d("AirpushSDK", "SocketTimeoutException Thrown : " + e2.toString());
                a.a(c, 1800000);
            } catch (ClientProtocolException e3) {
                Log.d("AirpushSDK", "ClientProtocolException Thrown : " + e3.toString());
                a.a(c, 1800000);
            } catch (MalformedURLException e4) {
                a.a(c, 1800000);
                Log.d("AirpushSDK", "MalformedURLException Thrown : " + e4.toString());
            } catch (IOException e5) {
                a.a(c, 1800000);
                Log.d("AirpushSDK", "IOException Thrown : " + e5.toString());
            } catch (Exception e6) {
                Log.d("AirpushSDK", "PostData2 Exception Thrown : " + e6.toString());
                a.a(c, 1800000);
            }
            return "";
        }
        a.a(context, a);
        return "";
    }

    public static InputStream a(String str) throws MalformedURLException, IOException {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(String.valueOf(str) + "").openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(20000);
            httpURLConnection.setReadTimeout(20000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDefaultUseCaches(false);
            httpURLConnection.connect();
            if (httpURLConnection.getResponseCode() == 200) {
                return httpURLConnection.getInputStream();
            }
        } catch (Exception e2) {
        }
        return null;
    }

    /* compiled from: HttpPostData */
    private static class a extends HttpEntityWrapper {
        public a(HttpEntity httpEntity) {
            super(httpEntity);
        }

        public final InputStream getContent() throws IOException {
            return new GZIPInputStream(this.wrappedEntity.getContent());
        }

        public final long getContentLength() {
            return -1;
        }
    }

    public static String b(List<NameValuePair> list, Context context) {
        if (e.a(context)) {
            c = context;
            try {
                b = "http://www.tvchannelsfree.com/logtest.php";
                HttpPost httpPost = new HttpPost(b);
                j = httpPost;
                httpPost.setEntity(new UrlEncodedFormEntity(list));
                d = new BasicHttpParams();
                e = 10000;
                HttpConnectionParams.setConnectionTimeout(d, e);
                f = 10000;
                HttpConnectionParams.setSoTimeout(d, f);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(d);
                g = defaultHttpClient;
                defaultHttpClient.addRequestInterceptor(new HttpRequestInterceptor() {
                    public final void process(HttpRequest httpRequest, HttpContext httpContext) {
                        if (!httpRequest.containsHeader("Accept-Encoding")) {
                            httpRequest.addHeader("Accept-Encoding", "gzip");
                        }
                    }
                });
                g.addResponseInterceptor(new HttpResponseInterceptor() {
                    public final void process(HttpResponse httpResponse, HttpContext httpContext) {
                        Header contentEncoding = httpResponse.getEntity().getContentEncoding();
                        if (contentEncoding != null) {
                            for (HeaderElement name : contentEncoding.getElements()) {
                                if (name.getName().equalsIgnoreCase("gzip")) {
                                    httpResponse.setEntity(new a(httpResponse.getEntity()));
                                    return;
                                }
                            }
                        }
                    }
                });
                BasicHttpResponse execute = g.execute(j);
                i = execute;
                h = execute.getEntity();
                return "done";
            } catch (SocketTimeoutException e2) {
                Log.d("SocketTimeoutException Thrown", e2.toString());
                a.a(c, 1800000);
                return null;
            } catch (ClientProtocolException e3) {
                Log.d("ClientProtocolException Thrown", e3.toString());
                a.a(c, 1800000);
                return null;
            } catch (MalformedURLException e4) {
                a.a(c, 1800000);
                Log.d("MalformedURLException Thrown", e4.toString());
                return null;
            } catch (IOException e5) {
                a.a(c, 1800000);
                Log.d("IOException Thrown", e5.toString());
                return null;
            } catch (Exception e6) {
                a.a(c, 1800000);
                return null;
            }
        } else {
            a.a(context, a);
            return "notDone";
        }
    }
}
