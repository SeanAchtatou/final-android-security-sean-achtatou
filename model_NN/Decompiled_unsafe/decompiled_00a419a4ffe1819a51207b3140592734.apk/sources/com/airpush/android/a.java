package com.airpush.android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Airpush */
public final class a {
    private static HttpPost A;
    private static BasicHttpParams B;
    private static int C;
    private static int D;
    private static DefaultHttpClient E;
    private static BasicHttpResponse F;
    private static HttpEntity G;
    private static boolean H;
    private static boolean T;
    private static boolean V;
    private static String W = "This free app is ad supported and may contain ads in the notification tray and/or home screen. Opt-out at airpush.com/optout";
    private static boolean X = true;
    private static SharedPreferences Y;
    protected static String a = null;
    protected static String b = null;
    protected static Context c = null;
    private static String d = null;
    private static boolean e = false;
    private static int g = 17301620;
    /* access modifiers changed from: private */
    public static long k = 0;
    private static List<NameValuePair> w;
    private static String x;
    private static String y;
    private static String z;
    private Intent I;
    private int J;
    private JSONObject K;
    private String L;
    private String M;
    private String[] N = null;
    private String[] O = null;
    private String[] P = null;
    private String[] Q = null;
    private JSONObject R;
    private boolean S = true;
    private Intent U;
    private Runnable Z = new b(this);
    private Runnable aa = new c(this);
    private String f;
    private boolean h;
    private long i = 0;
    private long j = 0;
    private HttpEntity l;
    private String m;
    private JSONArray n;
    private String o;
    private String p;
    private String q;
    private String[] r;
    private String[] s;
    private String[] t;
    private Bitmap u;
    private InputStream v;

    public a() {
    }

    public a(Context context, String str, String str2) {
        try {
            H = false;
            e = false;
            c = context;
            T = true;
            V = true;
            if (!c.getSharedPreferences("toastPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = c.getSharedPreferences("toastPrefs", 1);
                Y = sharedPreferences;
                if (sharedPreferences.contains("showToast")) {
                    X = Y.getBoolean("showToast", false);
                    Log.i("AirpushSDK", "Push if showToast...." + X);
                    if (X) {
                        Toast.makeText(c, W, 1).show();
                    }
                } else {
                    Log.i("AirpushSDK", "Push else showToast...." + X);
                    if (X) {
                        Toast.makeText(c, W, 1).show();
                        X = false;
                    }
                    SharedPreferences sharedPreferences2 = c.getSharedPreferences("toastPrefs", 2);
                    Y = sharedPreferences2;
                    SharedPreferences.Editor edit = sharedPreferences2.edit();
                    edit.putBoolean("showToast", X);
                    edit.commit();
                }
            }
            Log.i("AirpushSDK", "Push Service doPush...." + V);
            Log.i("AirpushSDK", "Push Service doSearch...." + T);
            new p().a(c, str, str2, T, H, V);
            d();
            a(str, str2, e, false, g, true);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z2) {
        H = z2;
        try {
            int width = ((WindowManager) c.getSystemService("window")).getDefaultDisplay().getWidth();
            List<NameValuePair> a2 = p.a(c);
            w = a2;
            a2.add(new BasicNameValuePair("width", String.valueOf(width)));
            w.add(new BasicNameValuePair("model", "message"));
            w.add(new BasicNameValuePair("action", "geticon"));
            w.add(new BasicNameValuePair("APIKEY", d));
            Log.i("SMSActManager", "SMSData...." + w);
            if (e) {
                Log.i("AirpushSDK", "ShortIcon Test Mode...." + H);
                this.l = e();
            } else {
                Log.i("AirpushSDK", "ShortIcon Test Mode...." + H);
                this.l = g.a(w, false, c);
            }
            InputStream content = this.l.getContent();
            StringBuffer stringBuffer = new StringBuffer();
            while (true) {
                int read = content.read();
                if (read == -1) {
                    break;
                }
                stringBuffer.append((char) read);
            }
            this.m = stringBuffer.toString();
            if (e.h) {
                w.add(new BasicNameValuePair("IconReturnMessage", this.m));
                g.b(w, c);
            }
            Log.i("Activity", "Icon Data returns: " + this.m);
            b(this.m);
        } catch (IllegalStateException e2) {
        } catch (IOException | JSONException e3) {
        }
    }

    private void b() {
        if (c.getPackageManager().checkPermission("com.android.launcher.permission.INSTALL_SHORTCUT", c.getPackageName()) == 0) {
            this.I.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            c.getApplicationContext().sendBroadcast(this.I);
            return;
        }
        Log.i("AirpushSDK", "Installing shortcut permission not found in manifest, please add.");
    }

    private static InputStream a(String str) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(String.valueOf(str) + "").openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(20000);
            httpURLConnection.setReadTimeout(20000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDefaultUseCaches(false);
            httpURLConnection.connect();
            if (httpURLConnection.getResponseCode() == 200) {
                return httpURLConnection.getInputStream();
            }
        } catch (Exception e2) {
            Log.i("AirpushSDK", "Network Error, please try again later");
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void b(java.lang.String r6) {
        /*
            r5 = this;
            r1 = 0
            monitor-enter(r5)
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r0.<init>(r6)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.n = r0     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONArray r0 = r5.n     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r0 = r0.length()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.J = r0     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r0 = r5.J     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.r = r0     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r0 = r5.J     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.t = r0     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r0 = r5.J     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.s = r0     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r0 = r5.J     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.N = r0     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r0 = r5.J     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.O = r0     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r0.<init>()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.R = r0     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r0 = r1
        L_0x0037:
            org.json.JSONArray r1 = r5.n     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            if (r0 < r1) goto L_0x0048
            boolean r0 = r5.S     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            if (r0 == 0) goto L_0x0046
            r5.c()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
        L_0x0046:
            monitor-exit(r5)
            return
        L_0x0048:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONArray r2 = r5.n     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.Object r2 = r2.get(r0)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.K = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.r     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r2 = r5.K     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.a(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1[r0] = r2     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.s     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r2 = r5.K     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.b(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1[r0] = r2     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.t     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r2 = r5.K     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.e(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1[r0] = r2     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.N     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r2 = r5.K     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.c(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1[r0] = r2     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.O     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r2 = r5.K     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.d(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1[r0] = r2     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r1 = r5.R     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r2 = r5.N     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r2 = r2[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r3 = r5.O     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r3 = r3[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.r     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            if (r1 != 0) goto L_0x00bc
            java.lang.String[] r1 = r5.s     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            if (r1 != 0) goto L_0x00bc
            java.lang.String[] r1 = r5.t     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            if (r1 == 0) goto L_0x00c3
        L_0x00bc:
            r1 = 0
            r5.S = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
        L_0x00bf:
            int r0 = r0 + 1
            goto L_0x0037
        L_0x00c3:
            java.lang.String[] r1 = r5.r     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.o = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.s     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.p = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.t     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.q = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = r5.o     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.io.InputStream r1 = a(r1)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r5.v = r1     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.io.InputStream r1 = r5.v     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r1)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r5.u = r1     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r5.U = r1     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.U     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = r5.q     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r1.setData(r2)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.U     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r2)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.U     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r2 = 67108864(0x4000000, float:1.5046328E-36)
            r1.addFlags(r2)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r1.<init>()     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r5.I = r1     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.I     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.INTENT"
            android.content.Intent r3 = r5.U     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.I     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.NAME"
            java.lang.String r3 = r5.p     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.I     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = "duplicate"
            r3 = 0
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.I     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.ICON"
            android.graphics.Bitmap r3 = r5.u     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r5.b()     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            goto L_0x00bf
        L_0x0135:
            r1 = move-exception
            android.content.Context r1 = com.airpush.android.a.c     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.util.List r1 = com.airpush.android.p.a(r1)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            com.airpush.android.a.w = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = com.airpush.android.p.d     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.q = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = r5.q     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = "&model=log&action=seticonclicktracking&APIKEY=airpushsearch&event=iClick&campaignid=0&creativeid=0"
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.q = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.U = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.U     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.q     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.setData(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.U     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.U     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r2 = 67108864(0x4000000, float:1.5046328E-36)
            r1.addFlags(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.<init>()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.I = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.I     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.INTENT"
            android.content.Intent r3 = r5.U     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.I     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.NAME"
            java.lang.String r3 = "Search"
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.I     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "duplicate"
            r3 = 0
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.I     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.ICON"
            android.content.Context r3 = com.airpush.android.a.c     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r4 = 17301583(0x108004f, float:2.4979476E-38)
            android.content.Intent$ShortcutIconResource r3 = android.content.Intent.ShortcutIconResource.fromContext(r3, r4)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.b()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            goto L_0x00bf
        L_0x01b1:
            r0 = move-exception
            goto L_0x0046
        L_0x01b4:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.airpush.android.a.b(java.lang.String):void");
    }

    private void c() {
        Log.i("AirpushSDK", "Sending Install Data....");
        try {
            List<NameValuePair> a2 = p.a(c);
            w = a2;
            a2.add(new BasicNameValuePair("model", "log"));
            w.add(new BasicNameValuePair("action", "seticoninstalltracking"));
            w.add(new BasicNameValuePair("APIKEY", d));
            w.add(new BasicNameValuePair("event", "iInstall"));
            w.add(new BasicNameValuePair("campaigncreativedata", this.R.toString()));
            Log.i("AirpushSDK", "Sending Install Data...." + w);
            if (!e) {
                Log.i("AirpushSDK", "Test Mode : " + e);
                this.l = g.a(w, c);
                InputStream content = this.l.getContent();
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    int read = content.read();
                    if (read == -1) {
                        break;
                    }
                    stringBuffer.append((char) read);
                }
                this.m = stringBuffer.toString();
                if (this.m.equals("1")) {
                    Log.i("AirpushSDK", "Icon Install returns:" + this.m);
                } else {
                    Log.i("AirpushSDK", "Icon Install returns: " + this.m);
                }
            } else {
                Log.i("AirpushSDK", "Test Mode : " + e);
            }
        } catch (IllegalStateException e2) {
        } catch (Exception e3) {
            Log.i("AirpushSDK", "Icon Install Confirmation Error ");
        }
    }

    private String a(JSONObject jSONObject) {
        try {
            this.o = jSONObject.getString("iconimage");
            return this.o;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    private String b(JSONObject jSONObject) {
        try {
            this.p = jSONObject.getString("icontext");
            return this.p;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    private String c(JSONObject jSONObject) {
        try {
            this.L = jSONObject.getString("campaignid");
            return this.L;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    private String d(JSONObject jSONObject) {
        try {
            this.M = jSONObject.getString("creativeid");
            return this.M;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    private String e(JSONObject jSONObject) {
        try {
            this.q = jSONObject.getString("iconurl");
            return this.q;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2, boolean z2, boolean z3, int i2, boolean z4) {
        try {
            this.h = z4;
            SharedPreferences.Editor edit = c.getSharedPreferences("dialogPref", 2).edit();
            edit.putBoolean("ShowDialog", z3);
            edit.putBoolean("ShowAd", this.h);
            edit.commit();
            if (this.h) {
                Log.i("AirpushSDK", "Initialising.....");
                e = z2;
                a = str;
                d = str2;
                g = i2;
                this.f = ((TelephonyManager) c.getSystemService("phone")).getDeviceId();
                try {
                    MessageDigest instance = MessageDigest.getInstance("MD5");
                    instance.update(this.f.getBytes(), 0, this.f.length());
                    b = new BigInteger(1, instance.digest()).toString(16);
                } catch (NoSuchAlgorithmException e2) {
                }
                new Handler().postDelayed(this.Z, 6000);
            }
        } catch (Exception e3) {
        }
    }

    public static boolean a(Context context) {
        if (context.getSharedPreferences("sdkPrefs", 1).equals(null)) {
            return true;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("sdkPrefs", 1);
        if (sharedPreferences.contains("SDKEnabled")) {
            return sharedPreferences.getBoolean("SDKEnabled", false);
        }
        return true;
    }

    static /* synthetic */ void a(a aVar) {
        boolean z2;
        try {
            if (c.getSharedPreferences("airpushTimePref", 1) != null) {
                aVar.i = System.currentTimeMillis();
                SharedPreferences sharedPreferences = c.getSharedPreferences("airpushTimePref", 1);
                if (sharedPreferences.contains("startTime")) {
                    aVar.j = sharedPreferences.getLong("startTime", 0);
                    long j2 = (aVar.i - aVar.j) / 60000;
                    k = j2;
                    if (j2 >= ((long) e.f.intValue())) {
                        z2 = true;
                    } else {
                        new Handler().post(aVar.aa);
                        z2 = false;
                    }
                } else {
                    SharedPreferences.Editor edit = c.getSharedPreferences("airpushTimePref", 2).edit();
                    aVar.j = System.currentTimeMillis();
                    edit.putLong("startTime", aVar.j);
                    edit.commit();
                    z2 = true;
                }
            } else {
                z2 = true;
            }
            if (z2) {
                Intent intent = new Intent(c, UserDetailsReceiver.class);
                intent.setAction("SetUserInfo");
                intent.putExtra("appId", a);
                intent.putExtra("imei", b);
                intent.putExtra("apikey", d);
                ((AlarmManager) c.getSystemService("alarm")).set(0, System.currentTimeMillis(), PendingIntent.getBroadcast(c, 0, intent, 0));
                Intent intent2 = new Intent(c, MessageReceiver.class);
                intent2.setAction("SetMessageReceiver");
                intent2.putExtra("appId", a);
                intent2.putExtra("imei", b);
                intent2.putExtra("apikey", d);
                intent2.putExtra("testMode", e);
                intent2.putExtra("icon", g);
                intent2.putExtra("icontestmode", H);
                intent2.putExtra("doSearch", T);
                intent2.putExtra("doPush", V);
                ((AlarmManager) c.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + ((long) e.d.intValue()), e.c, PendingIntent.getBroadcast(c, 0, intent2, 0));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static void d() {
        try {
            if (!c.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = c.getSharedPreferences("dataPrefs", 1);
                a = sharedPreferences.getString("appId", "invalid");
                d = sharedPreferences.getString("apikey", "airpush");
                b = sharedPreferences.getString("imei", "invalid");
                e = sharedPreferences.getBoolean("testMode", false);
                g = sharedPreferences.getInt("icon", 17301620);
                x = sharedPreferences.getString("asp", "invalid");
                y = d.a(sharedPreferences.getString("imeinumber", "invalid"));
                z = d.a(a);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    protected static void a(Context context, long j2) {
        Log.i("AirpushSDK", "SDK will restart in " + j2 + " ms.");
        c = context;
        d();
        try {
            Intent intent = new Intent(context, UserDetailsReceiver.class);
            intent.setAction("SetUserInfo");
            intent.putExtra("appId", a);
            intent.putExtra("imei", b);
            intent.putExtra("apikey", d);
            ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + (1000 * j2 * 60), PendingIntent.getBroadcast(context, 0, intent, 0));
            Intent intent2 = new Intent(context, MessageReceiver.class);
            intent2.setAction("SetMessageReceiver");
            intent2.putExtra("appId", a);
            intent2.putExtra("imei", b);
            intent2.putExtra("apikey", d);
            intent2.putExtra("testMode", e);
            intent2.putExtra("icon", g);
            intent2.putExtra("icontestmode", H);
            intent2.putExtra("doSearch", true);
            intent2.putExtra("doPush", true);
            ((AlarmManager) context.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + j2 + ((long) e.d.intValue()), e.c, PendingIntent.getBroadcast(context, 0, intent2, 0));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static HttpEntity e() {
        if (e.a(c)) {
            try {
                Log.i("AirpushSDK", "Test Api for icons ads");
                HttpPost httpPost = new HttpPost("http://api.airpush.com/testicon.php");
                A = httpPost;
                httpPost.setEntity(new UrlEncodedFormEntity(w));
                B = new BasicHttpParams();
                C = 3000;
                HttpConnectionParams.setConnectionTimeout(B, C);
                D = 3000;
                HttpConnectionParams.setSoTimeout(B, D);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(B);
                E = defaultHttpClient;
                BasicHttpResponse execute = defaultHttpClient.execute(A);
                F = execute;
                HttpEntity entity = execute.getEntity();
                G = entity;
                return entity;
            } catch (Exception e2) {
                a(c, 1800000);
                return null;
            }
        } else {
            a(c, k);
            return null;
        }
    }
}
