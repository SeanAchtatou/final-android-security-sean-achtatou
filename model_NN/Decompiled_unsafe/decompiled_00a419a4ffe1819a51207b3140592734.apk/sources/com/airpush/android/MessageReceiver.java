package com.airpush.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;
import java.util.List;
import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageReceiver extends BroadcastReceiver {
    protected static String a = null;
    protected static String b = "Invalid";
    protected static Context c;
    private List<NameValuePair> d = null;
    private String e = null;
    private boolean f;
    private int g;
    private JSONObject h;
    private String i;
    private String j;
    private Runnable k = new h(this);
    private boolean l;
    private boolean m;
    private boolean n;

    public void onReceive(Context context, Intent intent) {
        c = context;
        if (p.b(context)) {
            try {
                if (e.a(c)) {
                    Log.i("AirpushSDK", "Receiving Message.....");
                    if (intent.getAction().equals("SetMessageReceiver")) {
                        a();
                    }
                    Intent intent2 = new Intent();
                    intent2.setAction("com.airpush.android.PushServiceStart" + b);
                    intent2.putExtra("appId", b);
                    intent2.putExtra("type", "message");
                    intent2.putExtra("apikey", this.e);
                    intent2.putExtra("testMode", this.f);
                    intent2.putExtra("icon", this.g);
                    intent2.putExtra("icontestmode", this.l);
                    intent2.putExtra("doSearch", this.m);
                    intent2.putExtra("doPush", this.n);
                    if (!intent2.equals(null)) {
                        context.startService(intent2);
                        return;
                    }
                    a();
                    if (b.equals("invalid") || b.equals(null)) {
                        new Handler().postDelayed(this.k, 1800000);
                    }
                    new a(c, b, "airpush");
                }
            } catch (Exception e2) {
                Log.e("AirpushSDK", "Receving Message.....Failed : ");
                a();
                new a(c, b, "airpush");
            }
        } else {
            Log.i("AirpushSDK", "SDK is disabled, please enable to receive Ads !");
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        try {
            if (!c.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = c.getSharedPreferences("dataPrefs", 1);
                b = sharedPreferences.getString("appId", "invalid");
                this.e = sharedPreferences.getString("apikey", "airpush");
                a = sharedPreferences.getString("imei", "invalid");
                this.f = sharedPreferences.getBoolean("testMode", false);
                this.n = sharedPreferences.getBoolean("doPush", true);
                this.m = sharedPreferences.getBoolean("doSearch", true);
                this.l = sharedPreferences.getBoolean("searchIconTestMode", false);
                this.g = sharedPreferences.getInt("icon", 17301620);
                return;
            }
            this.i = c.getPackageName();
            this.j = g.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + this.i, c);
            b = a(this.j);
            this.e = b(this.j);
        } catch (Exception e2) {
            this.i = c.getPackageName();
            this.j = g.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + this.i, c);
            b = a(this.j);
            this.e = b(this.j);
            new a(c, b, "airpush");
        }
    }

    private String a(String str) {
        try {
            this.h = new JSONObject(str);
            return this.h.getString("appid");
        } catch (JSONException e2) {
            return "invalid Id";
        }
    }

    private String b(String str) {
        try {
            this.h = new JSONObject(str);
            return this.h.getString("authkey");
        } catch (JSONException e2) {
            return "invalid key";
        }
    }
}
