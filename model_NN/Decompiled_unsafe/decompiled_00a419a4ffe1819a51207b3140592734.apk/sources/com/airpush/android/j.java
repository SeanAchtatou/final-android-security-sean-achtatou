package com.airpush.android;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import org.apache.http.message.BasicNameValuePair;

/* compiled from: PushAds */
class j implements View.OnClickListener {
    final /* synthetic */ PushAds a;

    j(PushAds pushAds) {
        this.a = pushAds;
    }

    public final void onClick(View view) {
        try {
            PushAds.a(this.a, this.a.u);
            PushAds.f = p.a(this.a.getApplicationContext());
            PushAds.f.add(new BasicNameValuePair("model", "log"));
            PushAds.f.add(new BasicNameValuePair("action", "setfptracking"));
            PushAds.f.add(new BasicNameValuePair("APIKEY", this.a.k));
            PushAds.f.add(new BasicNameValuePair("event", "fclick"));
            PushAds.f.add(new BasicNameValuePair("campId", this.a.J));
            PushAds.f.add(new BasicNameValuePair("creativeId", this.a.d));
            new Handler().postDelayed(this.a.Q, 3000);
        } catch (Exception e) {
            Log.i("AirpushSDK", "Display Ad Network Error, please try again later. ");
        }
    }
}
