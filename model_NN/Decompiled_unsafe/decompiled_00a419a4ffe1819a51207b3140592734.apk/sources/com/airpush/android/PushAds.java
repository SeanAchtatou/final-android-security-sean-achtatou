package com.airpush.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import java.io.InputStream;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class PushAds extends Activity implements View.OnClickListener {
    private static String B = "1";
    private static HttpPost C;
    private static BasicHttpParams D;
    private static int E;
    private static int F;
    private static DefaultHttpClient G;
    private static BasicHttpResponse H;
    private static HttpEntity I;
    private static String b = null;
    /* access modifiers changed from: private */
    public static List<NameValuePair> f = null;
    private static Context m;
    /* access modifiers changed from: private */
    public boolean A;
    /* access modifiers changed from: private */
    public String J;
    private String K = "#FFFFFF";
    private String L = "#FFFFFF";
    private Runnable M = new i(this);
    private View.OnClickListener N = new j(this);
    private View.OnClickListener O = new k(this);
    private Runnable P = new l(this);
    /* access modifiers changed from: private */
    public Runnable Q = new m(this);
    private View.OnClickListener R = new n(this);
    String a = "http://api.airpush.com/api.php";
    private String c = null;
    /* access modifiers changed from: private */
    public String d = null;
    private String e = null;
    private String g;
    private String h;
    private String i;
    private String j;
    /* access modifiers changed from: private */
    public String k = null;
    private String l;
    /* access modifiers changed from: private */
    public boolean n = true;
    private boolean o = false;
    private int p = 17301620;
    /* access modifiers changed from: private */
    public boolean q = true;
    private Intent r;
    /* access modifiers changed from: private */
    public HttpEntity s;
    private String t;
    /* access modifiers changed from: private */
    public String u;
    private int v;
    private int w;
    private boolean x = true;
    private String y = "#000000";
    private String z = "#000000";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        m = getApplicationContext();
        this.r = getIntent();
        this.t = this.r.getAction();
        getWindowManager().getDefaultDisplay();
        this.w = 320;
        this.v = 350;
        this.g = this.r.getStringExtra("adType");
        if (this.g.equals("searchad")) {
            Log.i("AirpushSDK", "Search Clicked");
            return;
        }
        if (this.g.equals("ShoWDialog")) {
            this.e = this.r.getStringExtra("appId");
            this.k = this.r.getStringExtra("apikey");
            this.o = this.r.getBooleanExtra("test", false);
            this.p = this.r.getIntExtra("icon", 17301620);
            try {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(true);
                builder.setMessage("Support the App developer by enabling ads in the notification tray, limited to 1 per day.");
                builder.setPositiveButton("I Agree", new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.airpush.android.PushAds.a(com.airpush.android.PushAds, boolean):void
                     arg types: [com.airpush.android.PushAds, int]
                     candidates:
                      com.airpush.android.PushAds.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context):org.apache.http.HttpEntity
                      com.airpush.android.PushAds.a(com.airpush.android.PushAds, java.lang.String):void
                      com.airpush.android.PushAds.a(com.airpush.android.PushAds, boolean):void */
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        PushAds.this.n = false;
                        PushAds.this.q = true;
                        PushAds.k(PushAds.this);
                        PushAds.this.finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.airpush.android.PushAds.a(com.airpush.android.PushAds, boolean):void
                     arg types: [com.airpush.android.PushAds, int]
                     candidates:
                      com.airpush.android.PushAds.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context):org.apache.http.HttpEntity
                      com.airpush.android.PushAds.a(com.airpush.android.PushAds, java.lang.String):void
                      com.airpush.android.PushAds.a(com.airpush.android.PushAds, boolean):void */
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        PushAds.this.n = false;
                        PushAds.this.q = false;
                        PushAds.k(PushAds.this);
                        PushAds.this.finish();
                    }
                });
                builder.create();
                builder.show();
            } catch (Exception e2) {
            }
        }
        if (this.t.equals("CC")) {
            if (this.g.equals("CC")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (m.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences = m.getSharedPreferences("airpushNotificationPref", 1);
                    this.e = sharedPreferences.getString("appId", this.r.getStringExtra("appId"));
                    this.k = sharedPreferences.getString("apikey", this.r.getStringExtra("apikey"));
                    this.j = sharedPreferences.getString("number", this.r.getStringExtra("number"));
                    this.c = sharedPreferences.getString("campId", this.r.getStringExtra("campId"));
                    this.d = sharedPreferences.getString("creativeId", this.r.getStringExtra("creativeId"));
                } else {
                    this.e = this.r.getStringExtra("appId");
                    this.k = this.r.getStringExtra("apikey");
                    this.c = this.r.getStringExtra("campId");
                    this.d = this.r.getStringExtra("creativeId");
                    this.j = this.r.getStringExtra("number");
                }
                Intent intent = new Intent();
                intent.setAction("com.airpush.android.PushServiceStart" + this.e);
                intent.putExtra("type", "PostAdValues");
                intent.putExtras(this.r);
                startService(intent);
                Log.i("AirpushSDK", "Pushing CC Ads.....");
                startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + this.j)));
            }
        } else if (this.t.equals("CM")) {
            if (this.g.equals("CM")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (m.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences2 = m.getSharedPreferences("airpushNotificationPref", 1);
                    this.e = sharedPreferences2.getString("appId", this.r.getStringExtra("appId"));
                    this.k = sharedPreferences2.getString("apikey", this.r.getStringExtra("apikey"));
                    this.h = sharedPreferences2.getString("sms", this.r.getStringExtra("sms"));
                    this.c = sharedPreferences2.getString("campId", this.r.getStringExtra("campId"));
                    this.d = sharedPreferences2.getString("creativeId", this.r.getStringExtra("creativeId"));
                    this.i = sharedPreferences2.getString("number", this.r.getStringExtra("number"));
                } else {
                    this.e = this.r.getStringExtra("appId");
                    this.k = this.r.getStringExtra("apikey");
                    this.c = this.r.getStringExtra("campId");
                    this.d = this.r.getStringExtra("creativeId");
                    this.h = this.r.getStringExtra("sms");
                    this.i = this.r.getStringExtra("number");
                }
                Intent intent2 = new Intent();
                intent2.setAction("com.airpush.android.PushServiceStart" + this.e);
                intent2.putExtra("type", "PostAdValues");
                intent2.putExtras(this.r);
                startService(intent2);
                Log.i("AirpushSDK", "Pushing CM Ads.....");
                Intent intent3 = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + this.i));
                intent3.putExtra("sms_body", this.h);
                startActivity(intent3);
            }
        } else if (!this.t.equals("Web And App")) {
        } else {
            if (this.g.equals("W") || this.g.equals("A")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (m.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences3 = m.getSharedPreferences("airpushNotificationPref", 1);
                    this.e = sharedPreferences3.getString("appId", this.r.getStringExtra("appId"));
                    this.k = sharedPreferences3.getString("apikey", this.r.getStringExtra("apikey"));
                    b = sharedPreferences3.getString("url", this.r.getStringExtra("url"));
                    this.c = sharedPreferences3.getString("campId", this.r.getStringExtra("campId"));
                    this.d = sharedPreferences3.getString("creativeId", this.r.getStringExtra("creativeId"));
                    this.l = sharedPreferences3.getString("header", this.r.getStringExtra("header"));
                } else {
                    this.e = this.r.getStringExtra("appId");
                    this.k = this.r.getStringExtra("apikey");
                    this.c = this.r.getStringExtra("campId");
                    this.d = this.r.getStringExtra("creativeId");
                    b = this.r.getStringExtra("url");
                    this.l = this.r.getStringExtra("header");
                }
                Intent intent4 = new Intent();
                intent4.setAction("com.airpush.android.PushServiceStart" + this.e);
                intent4.putExtra("type", "PostAdValues");
                intent4.putExtras(this.r);
                startService(intent4);
                setTitle(this.l);
                String str = b;
                Log.i("AirpushSDK", "Pushing Web and App Ads.....");
                f fVar = new f(this);
                fVar.loadUrl(str);
                setContentView(fVar);
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }

    static /* synthetic */ void k(PushAds pushAds) {
        a aVar = new a();
        Context context = m;
        aVar.a(pushAds.e, pushAds.k, pushAds.o, pushAds.n, pushAds.p, pushAds.q);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        finish();
    }

    static /* synthetic */ void h(PushAds pushAds) {
        List<NameValuePair> a2 = p.a(m);
        f = a2;
        a2.add(new BasicNameValuePair("model", "log"));
        f.add(new BasicNameValuePair("action", "settexttracking"));
        f.add(new BasicNameValuePair("APIKEY", pushAds.k));
        f.add(new BasicNameValuePair("event", "trayDelivered"));
        f.add(new BasicNameValuePair("campId", pushAds.c));
        f.add(new BasicNameValuePair("creativeId", pushAds.d));
        pushAds.s = a(f, pushAds.getApplicationContext());
        StringBuffer stringBuffer = new StringBuffer();
        try {
            InputStream content = pushAds.s.getContent();
            while (true) {
                int read = content.read();
                if (read == -1) {
                    break;
                }
                stringBuffer.append((char) read);
            }
        } catch (Exception e2) {
        }
        stringBuffer.toString();
    }

    static /* synthetic */ void i(PushAds pushAds) {
        pushAds.s = g.a(f, pushAds.getApplicationContext());
        StringBuffer stringBuffer = new StringBuffer();
        try {
            InputStream content = pushAds.s.getContent();
            while (true) {
                int read = content.read();
                if (read == -1) {
                    break;
                }
                stringBuffer.append((char) read);
            }
        } catch (Exception e2) {
        }
        stringBuffer.toString();
    }

    static /* synthetic */ void a(PushAds pushAds, String str) {
        Log.i("AirpushSDK", "Displaying Ad.");
        f fVar = new f(pushAds);
        fVar.loadUrl(str);
        pushAds.setContentView(fVar);
    }

    public void onClick(View view) {
    }

    private static HttpEntity a(List<NameValuePair> list, Context context) {
        if (e.a(context)) {
            try {
                HttpPost httpPost = new HttpPost("http://api.airpush.com/v2/api.php");
                C = httpPost;
                httpPost.setEntity(new UrlEncodedFormEntity(list));
                D = new BasicHttpParams();
                E = 3000;
                HttpConnectionParams.setConnectionTimeout(D, E);
                F = 3000;
                HttpConnectionParams.setSoTimeout(D, F);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(D);
                G = defaultHttpClient;
                BasicHttpResponse execute = defaultHttpClient.execute(C);
                H = execute;
                HttpEntity entity = execute.getEntity();
                I = entity;
                return entity;
            } catch (Exception e2) {
                a.a(context, 1800000);
                return null;
            }
        } else {
            a.a(context, 3600000);
            return null;
        }
    }
}
