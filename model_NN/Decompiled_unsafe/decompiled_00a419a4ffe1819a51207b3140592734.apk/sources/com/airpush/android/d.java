package com.airpush.android;

/* compiled from: Base64 */
public final class d {
    private static final String a = System.getProperty("line.separator");
    private static final char[] b = new char[64];
    private static final byte[] c = new byte[128];

    static {
        char c2 = 'A';
        int i = 0;
        while (c2 <= 'Z') {
            b[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = 'a';
        while (c3 <= 'z') {
            b[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        char c4 = '0';
        while (c4 <= '9') {
            b[i] = c4;
            c4 = (char) (c4 + 1);
            i++;
        }
        b[i] = '+';
        b[i + 1] = '/';
        for (int i2 = 0; i2 < c.length; i2++) {
            c[i2] = -1;
        }
        for (int i3 = 0; i3 < 64; i3++) {
            c[b[i3]] = (byte) i3;
        }
    }

    public static String a(String str) {
        byte[] bytes = str.getBytes();
        return new String(a(bytes, bytes.length));
    }

    private static char[] a(byte[] bArr, int i) {
        int i2;
        byte b2;
        int i3;
        byte b3;
        char c2;
        char c3;
        int i4 = ((i * 4) + 2) / 3;
        char[] cArr = new char[(((i + 2) / 3) * 4)];
        int i5 = i + 0;
        int i6 = 0;
        int i7 = 0;
        while (i7 < i5) {
            int i8 = i7 + 1;
            byte b4 = bArr[i7] & 255;
            if (i8 < i5) {
                i2 = i8 + 1;
                b2 = bArr[i8] & 255;
            } else {
                i2 = i8;
                b2 = 0;
            }
            if (i2 < i5) {
                i3 = i2 + 1;
                b3 = bArr[i2] & 255;
            } else {
                i3 = i2;
                b3 = 0;
            }
            int i9 = b4 >>> 2;
            int i10 = ((b4 & 3) << 4) | (b2 >>> 4);
            int i11 = ((b2 & 15) << 2) | (b3 >>> 6);
            byte b5 = b3 & 63;
            int i12 = i6 + 1;
            cArr[i6] = b[i9];
            int i13 = i12 + 1;
            cArr[i12] = b[i10];
            if (i13 < i4) {
                c2 = b[i11];
            } else {
                c2 = '=';
            }
            cArr[i13] = c2;
            int i14 = i13 + 1;
            if (i14 < i4) {
                c3 = b[b5];
            } else {
                c3 = '=';
            }
            cArr[i14] = c3;
            i6 = i14 + 1;
            i7 = i3;
        }
        return cArr;
    }

    private d() {
    }
}
