package com.test.t000004;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class SHMenu extends Activity implements View.OnClickListener {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.menu);
        startService(new Intent(this, SHServ.class));
        View findViewById = findViewById(R.id.play);
        View findViewById2 = findViewById(R.id.about);
        findViewById.setOnClickListener(this);
        findViewById2.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.play /*2131034114*/:
                startActivity(new Intent(this, SHActivity.class));
                return;
            case R.id.about /*2131034115*/:
                startActivity(new Intent(this, AboutAct.class));
                return;
            default:
                return;
        }
    }
}
