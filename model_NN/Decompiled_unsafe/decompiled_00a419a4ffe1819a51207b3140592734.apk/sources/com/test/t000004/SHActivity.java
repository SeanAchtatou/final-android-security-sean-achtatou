package com.test.t000004;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class SHActivity extends Activity {
    WebView a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.main);
        if (Build.VERSION.SDK_INT < 9) {
            setRequestedOrientation(0);
        } else {
            setRequestedOrientation(6);
        }
        this.a = (WebView) findViewById(R.id.web);
        this.a.setBackgroundColor(-1);
        this.a.getSettings().setJavaScriptEnabled(true);
        this.a.getSettings().setPluginsEnabled(true);
        this.a.getSettings().setAllowFileAccess(true);
        this.a.getSettings().setSupportZoom(false);
        this.a.getSettings().setBuiltInZoomControls(false);
        this.a.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        this.a.setSoundEffectsEnabled(false);
        this.a.loadUrl("file:///android_asset/main.html");
    }

    public void onPause() {
        super.onPause();
        if (this.a != null) {
            this.a.onPause();
        }
    }

    public void onResume() {
        super.onResume();
        if (this.a != null) {
            this.a.onResume();
        }
    }
}
