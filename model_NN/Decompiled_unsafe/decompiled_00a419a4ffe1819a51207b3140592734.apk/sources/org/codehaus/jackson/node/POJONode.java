package org.codehaus.jackson.node;

import java.io.IOException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.SerializerProvider;

public final class POJONode extends ValueNode {
    protected final Object _value;

    public POJONode(Object obj) {
        this._value = obj;
    }

    public final JsonToken asToken() {
        return JsonToken.VALUE_EMBEDDED_OBJECT;
    }

    public final boolean isPojo() {
        return true;
    }

    public final String getValueAsText() {
        return this._value == null ? "null" : this._value.toString();
    }

    public final boolean getValueAsBoolean(boolean z) {
        if (this._value == null || !(this._value instanceof Boolean)) {
            return z;
        }
        return ((Boolean) this._value).booleanValue();
    }

    public final int getValueAsInt(int i) {
        if (this._value instanceof Number) {
            return ((Number) this._value).intValue();
        }
        return i;
    }

    public final long getValueAsLong(long j) {
        if (this._value instanceof Number) {
            return ((Number) this._value).longValue();
        }
        return j;
    }

    public final double getValueAsDouble(double d) {
        if (this._value instanceof Number) {
            return ((Number) this._value).doubleValue();
        }
        return d;
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        if (this._value == null) {
            jsonGenerator.writeNull();
        } else {
            jsonGenerator.writeObject(this._value);
        }
    }

    public final Object getPojo() {
        return this._value;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        POJONode pOJONode = (POJONode) obj;
        if (this._value == null) {
            return pOJONode._value == null;
        }
        return this._value.equals(pOJONode._value);
    }

    public final int hashCode() {
        return this._value.hashCode();
    }

    public final String toString() {
        return String.valueOf(this._value);
    }
}
