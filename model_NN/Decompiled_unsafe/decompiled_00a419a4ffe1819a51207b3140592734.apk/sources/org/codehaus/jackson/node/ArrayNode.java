package org.codehaus.jackson.node;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.node.ContainerNode;

public final class ArrayNode extends ContainerNode {
    protected ArrayList<JsonNode> _children;

    public ArrayNode(JsonNodeFactory jsonNodeFactory) {
        super(jsonNodeFactory);
    }

    public final JsonToken asToken() {
        return JsonToken.START_ARRAY;
    }

    public final boolean isArray() {
        return true;
    }

    public final int size() {
        if (this._children == null) {
            return 0;
        }
        return this._children.size();
    }

    public final Iterator<JsonNode> getElements() {
        return this._children == null ? ContainerNode.NoNodesIterator.instance() : this._children.iterator();
    }

    public final JsonNode get(int i) {
        if (i < 0 || this._children == null || i >= this._children.size()) {
            return null;
        }
        return this._children.get(i);
    }

    public final JsonNode get(String str) {
        return null;
    }

    public final JsonNode path(String str) {
        return MissingNode.getInstance();
    }

    public final JsonNode path(int i) {
        if (i < 0 || this._children == null || i >= this._children.size()) {
            return MissingNode.getInstance();
        }
        return this._children.get(i);
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartArray();
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                ((BaseJsonNode) it.next()).writeTo(jsonGenerator);
            }
        }
        jsonGenerator.writeEndArray();
    }

    public final JsonNode findValue(String str) {
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                JsonNode findValue = it.next().findValue(str);
                if (findValue != null) {
                    return findValue;
                }
            }
        }
        return null;
    }

    public final List<JsonNode> findValues(String str, List<JsonNode> list) {
        if (this._children == null) {
            return list;
        }
        Iterator<JsonNode> it = this._children.iterator();
        List<JsonNode> list2 = list;
        while (it.hasNext()) {
            list2 = it.next().findValues(str, list2);
        }
        return list2;
    }

    public final List<String> findValuesAsText(String str, List<String> list) {
        if (this._children == null) {
            return list;
        }
        Iterator<JsonNode> it = this._children.iterator();
        List<String> list2 = list;
        while (it.hasNext()) {
            list2 = it.next().findValuesAsText(str, list2);
        }
        return list2;
    }

    public final ObjectNode findParent(String str) {
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                JsonNode findParent = it.next().findParent(str);
                if (findParent != null) {
                    return (ObjectNode) findParent;
                }
            }
        }
        return null;
    }

    public final List<JsonNode> findParents(String str, List<JsonNode> list) {
        if (this._children == null) {
            return list;
        }
        Iterator<JsonNode> it = this._children.iterator();
        List<JsonNode> list2 = list;
        while (it.hasNext()) {
            list2 = it.next().findParents(str, list2);
        }
        return list2;
    }

    public final JsonNode set(int i, JsonNode jsonNode) {
        JsonNode jsonNode2;
        if (jsonNode == null) {
            jsonNode2 = nullNode();
        } else {
            jsonNode2 = jsonNode;
        }
        return _set(i, jsonNode2);
    }

    public final void add(JsonNode jsonNode) {
        JsonNode jsonNode2;
        if (jsonNode == null) {
            jsonNode2 = nullNode();
        } else {
            jsonNode2 = jsonNode;
        }
        _add(jsonNode2);
    }

    public final JsonNode addAll(ArrayNode arrayNode) {
        int size = arrayNode.size();
        if (size > 0) {
            if (this._children == null) {
                this._children = new ArrayList<>(size + 2);
            }
            arrayNode.addContentsTo(this._children);
        }
        return this;
    }

    public final JsonNode addAll(Collection<JsonNode> collection) {
        if (collection.size() > 0) {
            if (this._children == null) {
                this._children = new ArrayList<>(collection);
            } else {
                this._children.addAll(collection);
            }
        }
        return this;
    }

    public final void insert(int i, JsonNode jsonNode) {
        JsonNode jsonNode2;
        if (jsonNode == null) {
            jsonNode2 = nullNode();
        } else {
            jsonNode2 = jsonNode;
        }
        _insert(i, jsonNode2);
    }

    public final JsonNode remove(int i) {
        if (i < 0 || this._children == null || i >= this._children.size()) {
            return null;
        }
        return this._children.remove(i);
    }

    public final ArrayNode removeAll() {
        this._children = null;
        return this;
    }

    public final ArrayNode addArray() {
        ArrayNode arrayNode = arrayNode();
        _add(arrayNode);
        return arrayNode;
    }

    public final ObjectNode addObject() {
        ObjectNode objectNode = objectNode();
        _add(objectNode);
        return objectNode;
    }

    public final void addPOJO(Object obj) {
        if (obj == null) {
            addNull();
        } else {
            _add(POJONode(obj));
        }
    }

    public final void addNull() {
        _add(nullNode());
    }

    public final void add(int i) {
        _add(numberNode(i));
    }

    public final void add(long j) {
        _add(numberNode(j));
    }

    public final void add(float f) {
        _add(numberNode(f));
    }

    public final void add(double d) {
        _add(numberNode(d));
    }

    public final void add(BigDecimal bigDecimal) {
        if (bigDecimal == null) {
            addNull();
        } else {
            _add(numberNode(bigDecimal));
        }
    }

    public final void add(String str) {
        if (str == null) {
            addNull();
        } else {
            _add(textNode(str));
        }
    }

    public final void add(boolean z) {
        _add(booleanNode(z));
    }

    public final void add(byte[] bArr) {
        if (bArr == null) {
            addNull();
        } else {
            _add(binaryNode(bArr));
        }
    }

    public final ArrayNode insertArray(int i) {
        ArrayNode arrayNode = arrayNode();
        _insert(i, arrayNode);
        return arrayNode;
    }

    public final ObjectNode insertObject(int i) {
        ObjectNode objectNode = objectNode();
        _insert(i, objectNode);
        return objectNode;
    }

    public final void insertPOJO(int i, Object obj) {
        if (obj == null) {
            insertNull(i);
        } else {
            _insert(i, POJONode(obj));
        }
    }

    public final void insertNull(int i) {
        _insert(i, nullNode());
    }

    public final void insert(int i, int i2) {
        _insert(i, numberNode(i2));
    }

    public final void insert(int i, long j) {
        _insert(i, numberNode(j));
    }

    public final void insert(int i, float f) {
        _insert(i, numberNode(f));
    }

    public final void insert(int i, double d) {
        _insert(i, numberNode(d));
    }

    public final void insert(int i, BigDecimal bigDecimal) {
        if (bigDecimal == null) {
            insertNull(i);
        } else {
            _insert(i, numberNode(bigDecimal));
        }
    }

    public final void insert(int i, String str) {
        if (str == null) {
            insertNull(i);
        } else {
            _insert(i, textNode(str));
        }
    }

    public final void insert(int i, boolean z) {
        _insert(i, booleanNode(z));
    }

    public final void insert(int i, byte[] bArr) {
        if (bArr == null) {
            insertNull(i);
        } else {
            _insert(i, binaryNode(bArr));
        }
    }

    /* access modifiers changed from: protected */
    public final void addContentsTo(List<JsonNode> list) {
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                list.add(it.next());
            }
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ArrayNode arrayNode = (ArrayNode) obj;
        if (this._children == null || this._children.size() == 0) {
            return arrayNode.size() == 0;
        }
        return arrayNode._sameChildren(this._children);
    }

    public final int hashCode() {
        if (this._children == null) {
            return 1;
        }
        int size = this._children.size();
        Iterator<JsonNode> it = this._children.iterator();
        while (it.hasNext()) {
            JsonNode next = it.next();
            if (next != null) {
                size ^= next.hashCode();
            }
        }
        return size;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((size() << 4) + 16);
        sb.append('[');
        if (this._children != null) {
            int size = this._children.size();
            for (int i = 0; i < size; i++) {
                if (i > 0) {
                    sb.append(',');
                }
                sb.append(this._children.get(i).toString());
            }
        }
        sb.append(']');
        return sb.toString();
    }

    public final JsonNode _set(int i, JsonNode jsonNode) {
        if (this._children != null && i >= 0 && i < this._children.size()) {
            return this._children.set(i, jsonNode);
        }
        throw new IndexOutOfBoundsException("Illegal index " + i + ", array size " + size());
    }

    private void _add(JsonNode jsonNode) {
        if (this._children == null) {
            this._children = new ArrayList<>();
        }
        this._children.add(jsonNode);
    }

    private void _insert(int i, JsonNode jsonNode) {
        if (this._children == null) {
            this._children = new ArrayList<>();
            this._children.add(jsonNode);
        } else if (i < 0) {
            this._children.add(0, jsonNode);
        } else if (i >= this._children.size()) {
            this._children.add(jsonNode);
        } else {
            this._children.add(i, jsonNode);
        }
    }

    private boolean _sameChildren(ArrayList<JsonNode> arrayList) {
        int size = arrayList.size();
        if (size() != size) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (!this._children.get(i).equals(arrayList.get(i))) {
                return false;
            }
        }
        return true;
    }
}
