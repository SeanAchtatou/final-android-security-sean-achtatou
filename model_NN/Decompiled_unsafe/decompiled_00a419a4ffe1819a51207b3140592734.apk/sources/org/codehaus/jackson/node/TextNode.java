package org.codehaus.jackson.node;

import java.io.IOException;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.Base64Variants;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonLocation;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.io.NumberInput;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.util.CharTypes;

public final class TextNode extends ValueNode {
    static final TextNode EMPTY_STRING_NODE = new TextNode("");
    static final int INT_SPACE = 32;
    final String _value;

    public TextNode(String str) {
        this._value = str;
    }

    public static TextNode valueOf(String str) {
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return EMPTY_STRING_NODE;
        }
        return new TextNode(str);
    }

    public final JsonToken asToken() {
        return JsonToken.VALUE_STRING;
    }

    public final boolean isTextual() {
        return true;
    }

    public final String getTextValue() {
        return this._value;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002a, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002d, code lost:
        r3 = r4 + 1;
        r4 = r1.charAt(r4);
        r6 = r12.decodeBase64Char(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0037, code lost:
        if (r6 >= 0) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0039, code lost:
        _reportInvalidBase64(r12, r4, 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003d, code lost:
        r4 = (r5 << 6) | r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0040, code lost:
        if (r3 < r2) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0042, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0045, code lost:
        r5 = r3 + 1;
        r3 = r1.charAt(r3);
        r6 = r12.decodeBase64Char(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004f, code lost:
        if (r6 >= 0) goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r6 == -2) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0053, code lost:
        _reportInvalidBase64(r12, r3, 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0057, code lost:
        if (r5 < r2) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0059, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005c, code lost:
        r3 = r5 + 1;
        r5 = r1.charAt(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0066, code lost:
        if (r12.usesPaddingChar(r5) != false) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0068, code lost:
        _reportInvalidBase64(r12, r5, 3, "expected padding character '" + r12.getPaddingChar() + "'");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0088, code lost:
        r0.append(r4 >> 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008e, code lost:
        r3 = (r4 << 6) | r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0091, code lost:
        if (r5 < r2) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0093, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0096, code lost:
        r4 = r5 + 1;
        r5 = r1.charAt(r5);
        r6 = r12.decodeBase64Char(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a0, code lost:
        if (r6 >= 0) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a2, code lost:
        if (r6 == -2) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a4, code lost:
        _reportInvalidBase64(r12, r5, 3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a7, code lost:
        r0.appendTwoBytes(r3 >> 2);
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00af, code lost:
        r0.appendThreeBytes((r3 << 6) | r6);
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001f, code lost:
        r5 = r12.decodeBase64Char(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0023, code lost:
        if (r5 >= 0) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0025, code lost:
        _reportInvalidBase64(r12, r3, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0028, code lost:
        if (r4 < r2) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] getBinaryValue(org.codehaus.jackson.Base64Variant r12) throws java.io.IOException {
        /*
            r11 = this;
            r10 = 3
            r9 = 0
            r8 = -2
            org.codehaus.jackson.util.ByteArrayBuilder r0 = new org.codehaus.jackson.util.ByteArrayBuilder
            r1 = 100
            r0.<init>(r1)
            java.lang.String r1 = r11._value
            int r2 = r1.length()
            r3 = r9
        L_0x0011:
            if (r3 >= r2) goto L_0x00b8
        L_0x0013:
            int r4 = r3 + 1
            char r3 = r1.charAt(r3)
            if (r4 >= r2) goto L_0x00b8
            r5 = 32
            if (r3 <= r5) goto L_0x00bd
            int r5 = r12.decodeBase64Char(r3)
            if (r5 >= 0) goto L_0x0028
            r11._reportInvalidBase64(r12, r3, r9)
        L_0x0028:
            if (r4 < r2) goto L_0x002d
            r11._reportBase64EOF()
        L_0x002d:
            int r3 = r4 + 1
            char r4 = r1.charAt(r4)
            int r6 = r12.decodeBase64Char(r4)
            if (r6 >= 0) goto L_0x003d
            r7 = 1
            r11._reportInvalidBase64(r12, r4, r7)
        L_0x003d:
            int r4 = r5 << 6
            r4 = r4 | r6
            if (r3 < r2) goto L_0x0045
            r11._reportBase64EOF()
        L_0x0045:
            int r5 = r3 + 1
            char r3 = r1.charAt(r3)
            int r6 = r12.decodeBase64Char(r3)
            if (r6 >= 0) goto L_0x008e
            if (r6 == r8) goto L_0x0057
            r6 = 2
            r11._reportInvalidBase64(r12, r3, r6)
        L_0x0057:
            if (r5 < r2) goto L_0x005c
            r11._reportBase64EOF()
        L_0x005c:
            int r3 = r5 + 1
            char r5 = r1.charAt(r5)
            boolean r6 = r12.usesPaddingChar(r5)
            if (r6 != 0) goto L_0x0088
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "expected padding character '"
            java.lang.StringBuilder r6 = r6.append(r7)
            char r7 = r12.getPaddingChar()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = "'"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r11._reportInvalidBase64(r12, r5, r10, r6)
        L_0x0088:
            int r4 = r4 >> 4
            r0.append(r4)
            goto L_0x0011
        L_0x008e:
            int r3 = r4 << 6
            r3 = r3 | r6
            if (r5 < r2) goto L_0x0096
            r11._reportBase64EOF()
        L_0x0096:
            int r4 = r5 + 1
            char r5 = r1.charAt(r5)
            int r6 = r12.decodeBase64Char(r5)
            if (r6 >= 0) goto L_0x00af
            if (r6 == r8) goto L_0x00a7
            r11._reportInvalidBase64(r12, r5, r10)
        L_0x00a7:
            int r3 = r3 >> 2
            r0.appendTwoBytes(r3)
            r3 = r4
            goto L_0x0011
        L_0x00af:
            int r3 = r3 << 6
            r3 = r3 | r6
            r0.appendThreeBytes(r3)
            r3 = r4
            goto L_0x0011
        L_0x00b8:
            byte[] r0 = r0.toByteArray()
            return r0
        L_0x00bd:
            r3 = r4
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.node.TextNode.getBinaryValue(org.codehaus.jackson.Base64Variant):byte[]");
    }

    public final byte[] getBinaryValue() throws IOException {
        return getBinaryValue(Base64Variants.getDefaultVariant());
    }

    public final String getValueAsText() {
        return this._value;
    }

    public final boolean getValueAsBoolean(boolean z) {
        if (this._value == null || !"true".equals(this._value.trim())) {
            return z;
        }
        return true;
    }

    public final int getValueAsInt(int i) {
        return NumberInput.parseAsInt(this._value, i);
    }

    public final long getValueAsLong(long j) {
        return NumberInput.parseAsLong(this._value, j);
    }

    public final double getValueAsDouble(double d) {
        return NumberInput.parseAsDouble(this._value, d);
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        if (this._value == null) {
            jsonGenerator.writeNull();
        } else {
            jsonGenerator.writeString(this._value);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return ((TextNode) obj)._value.equals(this._value);
    }

    public final int hashCode() {
        return this._value.hashCode();
    }

    public final String toString() {
        int length = this._value.length();
        StringBuilder sb = new StringBuilder((length >> 4) + length + 2);
        appendQuoted(sb, this._value);
        return sb.toString();
    }

    protected static void appendQuoted(StringBuilder sb, String str) {
        sb.append('\"');
        CharTypes.appendQuoted(sb, str);
        sb.append('\"');
    }

    /* access modifiers changed from: protected */
    public final void _reportInvalidBase64(Base64Variant base64Variant, char c, int i) throws JsonParseException {
        _reportInvalidBase64(base64Variant, c, i, null);
    }

    /* access modifiers changed from: protected */
    public final void _reportInvalidBase64(Base64Variant base64Variant, char c, int i, String str) throws JsonParseException {
        String str2;
        if (c <= INT_SPACE) {
            str2 = "Illegal white space character (code 0x" + Integer.toHexString(c) + ") as character #" + (i + 1) + " of 4-char base64 unit: can only used between units";
        } else if (base64Variant.usesPaddingChar(c)) {
            str2 = "Unexpected padding character ('" + base64Variant.getPaddingChar() + "') as character #" + (i + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
        } else if (!Character.isDefined(c) || Character.isISOControl(c)) {
            str2 = "Illegal character (code 0x" + Integer.toHexString(c) + ") in base64 content";
        } else {
            str2 = "Illegal character '" + c + "' (code 0x" + Integer.toHexString(c) + ") in base64 content";
        }
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        throw new JsonParseException(str2, JsonLocation.NA);
    }

    /* access modifiers changed from: protected */
    public final void _reportBase64EOF() throws JsonParseException {
        throw new JsonParseException("Unexpected end-of-String when base64 content", JsonLocation.NA);
    }
}
