package org.codehaus.jackson.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

public final class UTF8Writer extends Writer {
    static final int SURR1_FIRST = 55296;
    static final int SURR1_LAST = 56319;
    static final int SURR2_FIRST = 56320;
    static final int SURR2_LAST = 57343;
    protected final IOContext _context;
    OutputStream _out;
    byte[] _outBuffer;
    final int _outBufferEnd;
    int _outPtr;
    int _surrogate = 0;

    public UTF8Writer(IOContext iOContext, OutputStream outputStream) {
        this._context = iOContext;
        this._out = outputStream;
        this._outBuffer = iOContext.allocWriteEncodingBuffer();
        this._outBufferEnd = this._outBuffer.length - 4;
        this._outPtr = 0;
    }

    public final Writer append(char c) throws IOException {
        write(c);
        return this;
    }

    public final void close() throws IOException {
        if (this._out != null) {
            if (this._outPtr > 0) {
                this._out.write(this._outBuffer, 0, this._outPtr);
                this._outPtr = 0;
            }
            OutputStream outputStream = this._out;
            this._out = null;
            byte[] bArr = this._outBuffer;
            if (bArr != null) {
                this._outBuffer = null;
                this._context.releaseWriteEncodingBuffer(bArr);
            }
            outputStream.close();
            int i = this._surrogate;
            this._surrogate = 0;
            if (i > 0) {
                throwIllegal(i);
            }
        }
    }

    public final void flush() throws IOException {
        if (this._outPtr > 0) {
            this._out.write(this._outBuffer, 0, this._outPtr);
            this._outPtr = 0;
        }
        this._out.flush();
    }

    public final void write(char[] cArr) throws IOException {
        write(cArr, 0, cArr.length);
    }

    public final void write(char[] cArr, int i, int i2) throws IOException {
        int i3;
        int i4;
        int i5;
        int i6;
        char c;
        if (i2 >= 2) {
            if (this._surrogate > 0) {
                write(convertSurrogate(cArr[i]));
                i4 = i + 1;
                i3 = i2 - 1;
            } else {
                i3 = i2;
                i4 = i;
            }
            int i7 = this._outPtr;
            byte[] bArr = this._outBuffer;
            int i8 = this._outBufferEnd;
            int i9 = i3 + i4;
            int i10 = i7;
            int i11 = i4;
            int i12 = i10;
            while (true) {
                if (i11 >= i9) {
                    i5 = i12;
                    break;
                }
                if (i12 >= i8) {
                    this._out.write(bArr, 0, i12);
                    i12 = 0;
                }
                int i13 = i11 + 1;
                char c2 = cArr[i11];
                if (c2 < 128) {
                    int i14 = i12 + 1;
                    bArr[i12] = (byte) c2;
                    int i15 = i9 - i13;
                    int i16 = i8 - i14;
                    if (i15 > i16) {
                        i15 = i16;
                    }
                    int i17 = i15 + i13;
                    i6 = i14;
                    while (i13 < i17) {
                        int i18 = i13 + 1;
                        char c3 = cArr[i13];
                        if (c3 < 128) {
                            bArr[i6] = (byte) c3;
                            i6++;
                            i13 = i18;
                        } else {
                            c = c3;
                            i13 = i18;
                        }
                    }
                    i12 = i6;
                    i11 = i13;
                } else {
                    char c4 = c2;
                    i6 = i12;
                    c = c4;
                }
                if (c >= 2048) {
                    if (c >= SURR1_FIRST && c <= SURR2_LAST) {
                        if (c > SURR1_LAST) {
                            this._outPtr = i6;
                            throwIllegal(c);
                        }
                        this._surrogate = c;
                        if (i13 >= i9) {
                            i5 = i6;
                            break;
                        }
                        int i19 = i13 + 1;
                        int convertSurrogate = convertSurrogate(cArr[i13]);
                        if (convertSurrogate > 1114111) {
                            this._outPtr = i6;
                            throwIllegal(convertSurrogate);
                        }
                        int i20 = i6 + 1;
                        bArr[i6] = (byte) ((convertSurrogate >> 18) | 240);
                        int i21 = i20 + 1;
                        bArr[i20] = (byte) (((convertSurrogate >> 12) & 63) | 128);
                        int i22 = i21 + 1;
                        bArr[i21] = (byte) (((convertSurrogate >> 6) & 63) | 128);
                        bArr[i22] = (byte) ((convertSurrogate & 63) | 128);
                        i11 = i19;
                        i12 = i22 + 1;
                    } else {
                        int i23 = i6 + 1;
                        bArr[i6] = (byte) ((c >> 12) | 224);
                        int i24 = i23 + 1;
                        bArr[i23] = (byte) (((c >> 6) & 63) | 128);
                        bArr[i24] = (byte) ((c & '?') | 128);
                        i12 = i24 + 1;
                        i11 = i13;
                    }
                } else {
                    int i25 = i6 + 1;
                    bArr[i6] = (byte) ((c >> 6) | 192);
                    bArr[i25] = (byte) ((c & '?') | 128);
                    i12 = i25 + 1;
                    i11 = i13;
                }
            }
            this._outPtr = i5;
        } else if (i2 == 1) {
            write(cArr[i]);
        }
    }

    public final void write(int i) throws IOException {
        int i2;
        int i3;
        if (this._surrogate > 0) {
            i2 = convertSurrogate(i);
        } else if (i < SURR1_FIRST || i > SURR2_LAST) {
            i2 = i;
        } else {
            if (i > SURR1_LAST) {
                throwIllegal(i);
            }
            this._surrogate = i;
            return;
        }
        if (this._outPtr >= this._outBufferEnd) {
            this._out.write(this._outBuffer, 0, this._outPtr);
            this._outPtr = 0;
        }
        if (i2 < 128) {
            byte[] bArr = this._outBuffer;
            int i4 = this._outPtr;
            this._outPtr = i4 + 1;
            bArr[i4] = (byte) i2;
            return;
        }
        int i5 = this._outPtr;
        if (i2 < 2048) {
            int i6 = i5 + 1;
            this._outBuffer[i5] = (byte) ((i2 >> 6) | 192);
            this._outBuffer[i6] = (byte) ((i2 & 63) | 128);
            i3 = i6 + 1;
        } else if (i2 <= 65535) {
            int i7 = i5 + 1;
            this._outBuffer[i5] = (byte) ((i2 >> 12) | 224);
            int i8 = i7 + 1;
            this._outBuffer[i7] = (byte) (((i2 >> 6) & 63) | 128);
            this._outBuffer[i8] = (byte) ((i2 & 63) | 128);
            i3 = i8 + 1;
        } else {
            if (i2 > 1114111) {
                throwIllegal(i2);
            }
            int i9 = i5 + 1;
            this._outBuffer[i5] = (byte) ((i2 >> 18) | 240);
            int i10 = i9 + 1;
            this._outBuffer[i9] = (byte) (((i2 >> 12) & 63) | 128);
            int i11 = i10 + 1;
            this._outBuffer[i10] = (byte) (((i2 >> 6) & 63) | 128);
            this._outBuffer[i11] = (byte) ((i2 & 63) | 128);
            i3 = i11 + 1;
        }
        this._outPtr = i3;
    }

    public final void write(String str) throws IOException {
        write(str, 0, str.length());
    }

    public final void write(String str, int i, int i2) throws IOException {
        int i3;
        int i4;
        int i5;
        int i6;
        char c;
        if (i2 >= 2) {
            if (this._surrogate > 0) {
                write(convertSurrogate(str.charAt(i)));
                i4 = i + 1;
                i3 = i2 - 1;
            } else {
                i3 = i2;
                i4 = i;
            }
            int i7 = this._outPtr;
            byte[] bArr = this._outBuffer;
            int i8 = this._outBufferEnd;
            int i9 = i3 + i4;
            int i10 = i7;
            int i11 = i4;
            int i12 = i10;
            while (true) {
                if (i11 >= i9) {
                    i5 = i12;
                    break;
                }
                if (i12 >= i8) {
                    this._out.write(bArr, 0, i12);
                    i12 = 0;
                }
                int i13 = i11 + 1;
                char charAt = str.charAt(i11);
                if (charAt < 128) {
                    int i14 = i12 + 1;
                    bArr[i12] = (byte) charAt;
                    int i15 = i9 - i13;
                    int i16 = i8 - i14;
                    if (i15 > i16) {
                        i15 = i16;
                    }
                    int i17 = i15 + i13;
                    i6 = i14;
                    while (i13 < i17) {
                        int i18 = i13 + 1;
                        char charAt2 = str.charAt(i13);
                        if (charAt2 < 128) {
                            bArr[i6] = (byte) charAt2;
                            i6++;
                            i13 = i18;
                        } else {
                            c = charAt2;
                            i13 = i18;
                        }
                    }
                    i12 = i6;
                    i11 = i13;
                } else {
                    char c2 = charAt;
                    i6 = i12;
                    c = c2;
                }
                if (c >= 2048) {
                    if (c >= SURR1_FIRST && c <= SURR2_LAST) {
                        if (c > SURR1_LAST) {
                            this._outPtr = i6;
                            throwIllegal(c);
                        }
                        this._surrogate = c;
                        if (i13 >= i9) {
                            i5 = i6;
                            break;
                        }
                        int i19 = i13 + 1;
                        int convertSurrogate = convertSurrogate(str.charAt(i13));
                        if (convertSurrogate > 1114111) {
                            this._outPtr = i6;
                            throwIllegal(convertSurrogate);
                        }
                        int i20 = i6 + 1;
                        bArr[i6] = (byte) ((convertSurrogate >> 18) | 240);
                        int i21 = i20 + 1;
                        bArr[i20] = (byte) (((convertSurrogate >> 12) & 63) | 128);
                        int i22 = i21 + 1;
                        bArr[i21] = (byte) (((convertSurrogate >> 6) & 63) | 128);
                        bArr[i22] = (byte) ((convertSurrogate & 63) | 128);
                        i11 = i19;
                        i12 = i22 + 1;
                    } else {
                        int i23 = i6 + 1;
                        bArr[i6] = (byte) ((c >> 12) | 224);
                        int i24 = i23 + 1;
                        bArr[i23] = (byte) (((c >> 6) & 63) | 128);
                        bArr[i24] = (byte) ((c & '?') | 128);
                        i12 = i24 + 1;
                        i11 = i13;
                    }
                } else {
                    int i25 = i6 + 1;
                    bArr[i6] = (byte) ((c >> 6) | 192);
                    bArr[i25] = (byte) ((c & '?') | 128);
                    i12 = i25 + 1;
                    i11 = i13;
                }
            }
            this._outPtr = i5;
        } else if (i2 == 1) {
            write(str.charAt(i));
        }
    }

    private int convertSurrogate(int i) throws IOException {
        int i2 = this._surrogate;
        this._surrogate = 0;
        if (i >= SURR2_FIRST && i <= SURR2_LAST) {
            return ((i2 - SURR1_FIRST) << 10) + 65536 + (i - SURR2_FIRST);
        }
        throw new IOException("Broken surrogate pair: first char 0x" + Integer.toHexString(i2) + ", second 0x" + Integer.toHexString(i) + "; illegal combination");
    }

    private void throwIllegal(int i) throws IOException {
        if (i > 1114111) {
            throw new IOException("Illegal character point (0x" + Integer.toHexString(i) + ") to output; max is 0x10FFFF as per RFC 4627");
        } else if (i < SURR1_FIRST) {
            throw new IOException("Illegal character point (0x" + Integer.toHexString(i) + ") to output");
        } else if (i <= SURR1_LAST) {
            throw new IOException("Unmatched first part of surrogate pair (0x" + Integer.toHexString(i) + ")");
        } else {
            throw new IOException("Unmatched second part of surrogate pair (0x" + Integer.toHexString(i) + ")");
        }
    }
}
