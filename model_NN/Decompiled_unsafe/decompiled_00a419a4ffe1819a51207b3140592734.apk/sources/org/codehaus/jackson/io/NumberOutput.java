package org.codehaus.jackson.io;

public final class NumberOutput {
    private static int BILLION = 1000000000;
    static final char[] FULL_TRIPLETS = new char[4000];
    static final byte[] FULL_TRIPLETS_B = new byte[4000];
    static final char[] LEADING_TRIPLETS = new char[4000];
    private static long MAX_INT_AS_LONG = 2147483647L;
    private static int MILLION = 1000000;
    private static long MIN_INT_AS_LONG = -2147483648L;
    private static final char NULL_CHAR = 0;
    static final String SMALLEST_LONG = String.valueOf(Long.MIN_VALUE);
    private static long TEN_BILLION_L = 10000000000L;
    private static long THOUSAND_L = 1000;
    static final String[] sSmallIntStrs = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    static final String[] sSmallIntStrs2 = {"-1", "-2", "-3", "-4", "-5", "-6", "-7", "-8", "-9", "-10"};

    static {
        char c;
        char c2;
        int i = 0;
        int i2 = 0;
        while (i < 10) {
            char c3 = (char) (i + 48);
            if (i == 0) {
                c = 0;
            } else {
                c = c3;
            }
            int i3 = i2;
            int i4 = 0;
            while (i4 < 10) {
                char c4 = (char) (i4 + 48);
                if (i == 0 && i4 == 0) {
                    c2 = 0;
                } else {
                    c2 = c4;
                }
                int i5 = i3;
                for (int i6 = 0; i6 < 10; i6++) {
                    char c5 = (char) (i6 + 48);
                    LEADING_TRIPLETS[i5] = c;
                    LEADING_TRIPLETS[i5 + 1] = c2;
                    LEADING_TRIPLETS[i5 + 2] = c5;
                    FULL_TRIPLETS[i5] = c3;
                    FULL_TRIPLETS[i5 + 1] = c4;
                    FULL_TRIPLETS[i5 + 2] = c5;
                    i5 += 4;
                }
                i4++;
                i3 = i5;
            }
            i++;
            i2 = i3;
        }
        for (int i7 = 0; i7 < 4000; i7++) {
            FULL_TRIPLETS_B[i7] = (byte) FULL_TRIPLETS[i7];
        }
    }

    public static int outputInt(int i, char[] cArr, int i2) {
        int i3;
        int i4;
        int outputLeadingTriplet;
        if (i >= 0) {
            i3 = i2;
            i4 = i;
        } else if (i == Integer.MIN_VALUE) {
            return outputLong((long) i, cArr, i2);
        } else {
            i3 = i2 + 1;
            cArr[i2] = '-';
            i4 = -i;
        }
        if (i4 >= MILLION) {
            boolean z = i4 >= BILLION;
            if (z) {
                i4 -= BILLION;
                if (i4 >= BILLION) {
                    i4 -= BILLION;
                    cArr[i3] = '2';
                    i3++;
                } else {
                    cArr[i3] = '1';
                    i3++;
                }
            }
            int i5 = i4 / 1000;
            int i6 = i4 - (i5 * 1000);
            int i7 = i5 / 1000;
            int i8 = i5 - (i7 * 1000);
            if (z) {
                outputLeadingTriplet = outputFullTriplet(i7, cArr, i3);
            } else {
                outputLeadingTriplet = outputLeadingTriplet(i7, cArr, i3);
            }
            return outputFullTriplet(i6, cArr, outputFullTriplet(i8, cArr, outputLeadingTriplet));
        } else if (i4 >= 1000) {
            int i9 = i4 / 1000;
            return outputFullTriplet(i4 - (i9 * 1000), cArr, outputLeadingTriplet(i9, cArr, i3));
        } else if (i4 >= 10) {
            return outputLeadingTriplet(i4, cArr, i3);
        } else {
            cArr[i3] = (char) (i4 + 48);
            return i3 + 1;
        }
    }

    public static int outputInt(int i, byte[] bArr, int i2) {
        int i3;
        int i4;
        int outputLeadingTriplet;
        if (i >= 0) {
            i3 = i2;
            i4 = i;
        } else if (i == Integer.MIN_VALUE) {
            return outputLong((long) i, bArr, i2);
        } else {
            i3 = i2 + 1;
            bArr[i2] = 45;
            i4 = -i;
        }
        if (i4 >= MILLION) {
            boolean z = i4 >= BILLION;
            if (z) {
                i4 -= BILLION;
                if (i4 >= BILLION) {
                    i4 -= BILLION;
                    bArr[i3] = 50;
                    i3++;
                } else {
                    bArr[i3] = 49;
                    i3++;
                }
            }
            int i5 = i4 / 1000;
            int i6 = i4 - (i5 * 1000);
            int i7 = i5 / 1000;
            int i8 = i5 - (i7 * 1000);
            if (z) {
                outputLeadingTriplet = outputFullTriplet(i7, bArr, i3);
            } else {
                outputLeadingTriplet = outputLeadingTriplet(i7, bArr, i3);
            }
            return outputFullTriplet(i6, bArr, outputFullTriplet(i8, bArr, outputLeadingTriplet));
        } else if (i4 >= 1000) {
            int i9 = i4 / 1000;
            return outputFullTriplet(i4 - (i9 * 1000), bArr, outputLeadingTriplet(i9, bArr, i3));
        } else if (i4 >= 10) {
            return outputLeadingTriplet(i4, bArr, i3);
        } else {
            bArr[i3] = (byte) (i4 + 48);
            return i3 + 1;
        }
    }

    public static int outputLong(long j, char[] cArr, int i) {
        int i2;
        long j2;
        if (j < 0) {
            if (j > MIN_INT_AS_LONG) {
                return outputInt((int) j, cArr, i);
            }
            if (j == Long.MIN_VALUE) {
                int length = SMALLEST_LONG.length();
                SMALLEST_LONG.getChars(0, length, cArr, i);
                return length + i;
            }
            i2 = i + 1;
            cArr[i] = '-';
            j2 = -j;
        } else if (j <= MAX_INT_AS_LONG) {
            return outputInt((int) j, cArr, i);
        } else {
            i2 = i;
            j2 = j;
        }
        int calcLongStrLength = calcLongStrLength(j2) + i2;
        long j3 = j2;
        int i3 = calcLongStrLength;
        while (j3 > MAX_INT_AS_LONG) {
            i3 -= 3;
            long j4 = j3 / THOUSAND_L;
            outputFullTriplet((int) (j3 - (THOUSAND_L * j4)), cArr, i3);
            j3 = j4;
        }
        int i4 = i3;
        int i5 = (int) j3;
        while (i5 >= 1000) {
            i4 -= 3;
            int i6 = i5 / 1000;
            outputFullTriplet(i5 - (i6 * 1000), cArr, i4);
            i5 = i6;
        }
        outputLeadingTriplet(i5, cArr, i2);
        return calcLongStrLength;
    }

    public static int outputLong(long j, byte[] bArr, int i) {
        int i2;
        long j2;
        if (j < 0) {
            if (j > MIN_INT_AS_LONG) {
                return outputInt((int) j, bArr, i);
            }
            if (j == Long.MIN_VALUE) {
                int length = SMALLEST_LONG.length();
                int i3 = 0;
                int i4 = i;
                while (i3 < length) {
                    bArr[i4] = (byte) SMALLEST_LONG.charAt(i3);
                    i3++;
                    i4++;
                }
                return i4;
            }
            i2 = i + 1;
            bArr[i] = 45;
            j2 = -j;
        } else if (j <= MAX_INT_AS_LONG) {
            return outputInt((int) j, bArr, i);
        } else {
            i2 = i;
            j2 = j;
        }
        int calcLongStrLength = calcLongStrLength(j2) + i2;
        long j3 = j2;
        int i5 = calcLongStrLength;
        while (j3 > MAX_INT_AS_LONG) {
            i5 -= 3;
            long j4 = j3 / THOUSAND_L;
            outputFullTriplet((int) (j3 - (THOUSAND_L * j4)), bArr, i5);
            j3 = j4;
        }
        int i6 = i5;
        int i7 = (int) j3;
        while (i7 >= 1000) {
            i6 -= 3;
            int i8 = i7 / 1000;
            outputFullTriplet(i7 - (i8 * 1000), bArr, i6);
            i7 = i8;
        }
        outputLeadingTriplet(i7, bArr, i2);
        return calcLongStrLength;
    }

    public static String toString(int i) {
        if (i < sSmallIntStrs.length) {
            if (i >= 0) {
                return sSmallIntStrs[i];
            }
            int i2 = (-i) - 1;
            if (i2 < sSmallIntStrs2.length) {
                return sSmallIntStrs2[i2];
            }
        }
        return Integer.toString(i);
    }

    public static String toString(long j) {
        if (j > 2147483647L || j < -2147483648L) {
            return Long.toString(j);
        }
        return toString((int) j);
    }

    public static String toString(double d) {
        return Double.toString(d);
    }

    private static int outputLeadingTriplet(int i, char[] cArr, int i2) {
        int i3;
        int i4 = i << 2;
        int i5 = i4 + 1;
        char c = LEADING_TRIPLETS[i4];
        if (c != 0) {
            cArr[i2] = c;
            i3 = i2 + 1;
        } else {
            i3 = i2;
        }
        int i6 = i5 + 1;
        char c2 = LEADING_TRIPLETS[i5];
        if (c2 != 0) {
            cArr[i3] = c2;
            i3++;
        }
        int i7 = i3 + 1;
        cArr[i3] = LEADING_TRIPLETS[i6];
        return i7;
    }

    private static int outputLeadingTriplet(int i, byte[] bArr, int i2) {
        int i3;
        int i4 = i << 2;
        int i5 = i4 + 1;
        char c = LEADING_TRIPLETS[i4];
        if (c != 0) {
            bArr[i2] = (byte) c;
            i3 = i2 + 1;
        } else {
            i3 = i2;
        }
        int i6 = i5 + 1;
        char c2 = LEADING_TRIPLETS[i5];
        if (c2 != 0) {
            bArr[i3] = (byte) c2;
            i3++;
        }
        int i7 = i3 + 1;
        bArr[i3] = (byte) LEADING_TRIPLETS[i6];
        return i7;
    }

    private static int outputFullTriplet(int i, char[] cArr, int i2) {
        int i3 = i << 2;
        int i4 = i2 + 1;
        int i5 = i3 + 1;
        cArr[i2] = FULL_TRIPLETS[i3];
        int i6 = i4 + 1;
        cArr[i4] = FULL_TRIPLETS[i5];
        int i7 = i6 + 1;
        cArr[i6] = FULL_TRIPLETS[i5 + 1];
        return i7;
    }

    private static int outputFullTriplet(int i, byte[] bArr, int i2) {
        int i3 = i << 2;
        int i4 = i2 + 1;
        int i5 = i3 + 1;
        bArr[i2] = FULL_TRIPLETS_B[i3];
        int i6 = i4 + 1;
        bArr[i4] = FULL_TRIPLETS_B[i5];
        int i7 = i6 + 1;
        bArr[i6] = FULL_TRIPLETS_B[i5 + 1];
        return i7;
    }

    private static int calcLongStrLength(long j) {
        int i = 10;
        for (long j2 = TEN_BILLION_L; j >= j2 && i != 19; j2 = (j2 << 1) + (j2 << 3)) {
            i++;
        }
        return i;
    }
}
