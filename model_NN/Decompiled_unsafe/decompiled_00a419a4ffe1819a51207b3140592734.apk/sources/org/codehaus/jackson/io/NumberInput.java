package org.codehaus.jackson.io;

public final class NumberInput {
    static final long L_BILLION = 1000000000;
    static final String MAX_LONG_STR = String.valueOf(Long.MAX_VALUE);
    static final String MIN_LONG_STR_NO_SIGN = String.valueOf(Long.MIN_VALUE).substring(1);

    public static final int parseInt(char[] cArr, int i, int i2) {
        int i3 = cArr[i] - '0';
        int i4 = i2 + i;
        int i5 = i + 1;
        if (i5 >= i4) {
            return i3;
        }
        int i6 = (i3 * 10) + (cArr[i5] - '0');
        int i7 = i5 + 1;
        if (i7 >= i4) {
            return i6;
        }
        int i8 = (i6 * 10) + (cArr[i7] - '0');
        int i9 = i7 + 1;
        if (i9 >= i4) {
            return i8;
        }
        int i10 = (i8 * 10) + (cArr[i9] - '0');
        int i11 = i9 + 1;
        if (i11 >= i4) {
            return i10;
        }
        int i12 = (i10 * 10) + (cArr[i11] - '0');
        int i13 = i11 + 1;
        if (i13 >= i4) {
            return i12;
        }
        int i14 = (i12 * 10) + (cArr[i13] - '0');
        int i15 = i13 + 1;
        if (i15 >= i4) {
            return i14;
        }
        int i16 = (i14 * 10) + (cArr[i15] - '0');
        int i17 = i15 + 1;
        if (i17 >= i4) {
            return i16;
        }
        int i18 = (i16 * 10) + (cArr[i17] - '0');
        int i19 = i17 + 1;
        if (i19 < i4) {
            return (i18 * 10) + (cArr[i19] - '0');
        }
        return i18;
    }

    public static final int parseInt(String str) {
        char c;
        int i;
        int i2;
        char charAt = str.charAt(0);
        int length = str.length();
        boolean z = charAt == '-';
        if (z) {
            if (length == 1 || length > 10) {
                return Integer.parseInt(str);
            }
            i = 1 + 1;
            c = str.charAt(1);
        } else if (length > 9) {
            return Integer.parseInt(str);
        } else {
            c = charAt;
            i = 1;
        }
        if (c > '9' || c < '0') {
            return Integer.parseInt(str);
        }
        int i3 = c - '0';
        if (i < length) {
            int i4 = i + 1;
            char charAt2 = str.charAt(i);
            if (charAt2 > '9' || charAt2 < '0') {
                return Integer.parseInt(str);
            }
            i2 = (charAt2 - '0') + (i3 * 10);
            if (i4 < length) {
                int i5 = i4 + 1;
                char charAt3 = str.charAt(i4);
                if (charAt3 > '9' || charAt3 < '0') {
                    return Integer.parseInt(str);
                }
                i2 = (i2 * 10) + (charAt3 - '0');
                if (i5 < length) {
                    while (true) {
                        int i6 = i5 + 1;
                        char charAt4 = str.charAt(i5);
                        if (charAt4 <= '9' && charAt4 >= '0') {
                            i2 = (i2 * 10) + (charAt4 - '0');
                            if (i6 >= length) {
                                break;
                            }
                            i5 = i6;
                        }
                    }
                    return Integer.parseInt(str);
                }
            }
        } else {
            i2 = i3;
        }
        return z ? -i2 : i2;
    }

    public static final long parseLong(char[] cArr, int i, int i2) {
        int i3 = i2 - 9;
        return (((long) parseInt(cArr, i, i3)) * L_BILLION) + ((long) parseInt(cArr, i3 + i, 9));
    }

    public static final long parseLong(String str) {
        if (str.length() <= 9) {
            return (long) parseInt(str);
        }
        return Long.parseLong(str);
    }

    public static final boolean inLongRange(char[] cArr, int i, int i2, boolean z) {
        String str = z ? MIN_LONG_STR_NO_SIGN : MAX_LONG_STR;
        int length = str.length();
        if (i2 < length) {
            return true;
        }
        if (i2 > length) {
            return false;
        }
        for (int i3 = 0; i3 < length; i3++) {
            if (cArr[i + i3] > str.charAt(i3)) {
                return false;
            }
        }
        return true;
    }

    public static final boolean inLongRange(String str, boolean z) {
        String str2 = z ? MIN_LONG_STR_NO_SIGN : MAX_LONG_STR;
        int length = str2.length();
        int length2 = str.length();
        if (length2 < length) {
            return true;
        }
        if (length2 > length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (str.charAt(i) > str2.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int parseAsInt(java.lang.String r6, int r7) {
        /*
            r4 = 0
            if (r6 != 0) goto L_0x0005
            r0 = r7
        L_0x0004:
            return r0
        L_0x0005:
            java.lang.String r0 = r6.trim()
            int r1 = r0.length()
            if (r1 != 0) goto L_0x0011
            r0 = r7
            goto L_0x0004
        L_0x0011:
            if (r1 <= 0) goto L_0x0050
            char r2 = r0.charAt(r4)
            r3 = 43
            if (r2 != r3) goto L_0x003a
            r1 = 1
            java.lang.String r0 = r0.substring(r1)
            int r1 = r0.length()
            r2 = r0
            r0 = r4
        L_0x0026:
            if (r0 >= r1) goto L_0x0048
            char r3 = r2.charAt(r0)
            r4 = 57
            if (r3 > r4) goto L_0x0034
            r4 = 48
            if (r3 >= r4) goto L_0x0045
        L_0x0034:
            double r0 = java.lang.Double.parseDouble(r2)     // Catch:{ NumberFormatException -> 0x0044 }
            int r0 = (int) r0
            goto L_0x0004
        L_0x003a:
            r3 = 45
            if (r2 != r3) goto L_0x0050
            int r2 = r4 + 1
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x0026
        L_0x0044:
            r3 = move-exception
        L_0x0045:
            int r0 = r0 + 1
            goto L_0x0026
        L_0x0048:
            int r0 = java.lang.Integer.parseInt(r2)     // Catch:{ NumberFormatException -> 0x004d }
            goto L_0x0004
        L_0x004d:
            r0 = move-exception
            r0 = r7
            goto L_0x0004
        L_0x0050:
            r2 = r0
            r0 = r4
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.io.NumberInput.parseAsInt(java.lang.String, int):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long parseAsLong(java.lang.String r6, long r7) {
        /*
            r4 = 0
            if (r6 != 0) goto L_0x0005
            r0 = r7
        L_0x0004:
            return r0
        L_0x0005:
            java.lang.String r0 = r6.trim()
            int r1 = r0.length()
            if (r1 != 0) goto L_0x0011
            r0 = r7
            goto L_0x0004
        L_0x0011:
            if (r1 <= 0) goto L_0x0050
            char r2 = r0.charAt(r4)
            r3 = 43
            if (r2 != r3) goto L_0x003a
            r1 = 1
            java.lang.String r0 = r0.substring(r1)
            int r1 = r0.length()
            r2 = r0
            r0 = r4
        L_0x0026:
            if (r0 >= r1) goto L_0x0048
            char r3 = r2.charAt(r0)
            r4 = 57
            if (r3 > r4) goto L_0x0034
            r4 = 48
            if (r3 >= r4) goto L_0x0045
        L_0x0034:
            double r0 = java.lang.Double.parseDouble(r2)     // Catch:{ NumberFormatException -> 0x0044 }
            long r0 = (long) r0
            goto L_0x0004
        L_0x003a:
            r3 = 45
            if (r2 != r3) goto L_0x0050
            int r2 = r4 + 1
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x0026
        L_0x0044:
            r3 = move-exception
        L_0x0045:
            int r0 = r0 + 1
            goto L_0x0026
        L_0x0048:
            long r0 = java.lang.Long.parseLong(r2)     // Catch:{ NumberFormatException -> 0x004d }
            goto L_0x0004
        L_0x004d:
            r0 = move-exception
            r0 = r7
            goto L_0x0004
        L_0x0050:
            r2 = r0
            r0 = r4
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.io.NumberInput.parseAsLong(java.lang.String, long):long");
    }

    public static double parseAsDouble(String str, double d) {
        if (str == null) {
            return d;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            return d;
        }
        try {
            return Double.parseDouble(trim);
        } catch (NumberFormatException e) {
            return d;
        }
    }
}
