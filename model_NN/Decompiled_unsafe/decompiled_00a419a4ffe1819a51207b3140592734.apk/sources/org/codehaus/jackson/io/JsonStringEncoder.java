package org.codehaus.jackson.io;

import java.lang.ref.SoftReference;
import org.codehaus.jackson.util.BufferRecycler;
import org.codehaus.jackson.util.ByteArrayBuilder;
import org.codehaus.jackson.util.CharTypes;
import org.codehaus.jackson.util.TextBuffer;

public final class JsonStringEncoder {
    private static final byte[] HEX_BYTES = CharTypes.copyHexBytes();
    private static final char[] HEX_CHARS = CharTypes.copyHexChars();
    private static final int INT_0 = 48;
    private static final int INT_BACKSLASH = 92;
    private static final int INT_U = 117;
    private static final int SURR1_FIRST = 55296;
    private static final int SURR1_LAST = 56319;
    private static final int SURR2_FIRST = 56320;
    private static final int SURR2_LAST = 57343;
    protected static final ThreadLocal<SoftReference<JsonStringEncoder>> _threadEncoder = new ThreadLocal<>();
    protected ByteArrayBuilder _byteBuilder;
    protected final char[] _quoteBuffer = new char[6];
    protected TextBuffer _textBuffer;

    public JsonStringEncoder() {
        this._quoteBuffer[0] = '\\';
        this._quoteBuffer[2] = '0';
        this._quoteBuffer[3] = '0';
    }

    public static JsonStringEncoder getInstance() {
        SoftReference softReference = _threadEncoder.get();
        JsonStringEncoder jsonStringEncoder = softReference == null ? null : (JsonStringEncoder) softReference.get();
        if (jsonStringEncoder != null) {
            return jsonStringEncoder;
        }
        JsonStringEncoder jsonStringEncoder2 = new JsonStringEncoder();
        _threadEncoder.set(new SoftReference(jsonStringEncoder2));
        return jsonStringEncoder2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        r7 = r5 + 1;
        r5 = _appendSingleEscape(r2[r12.charAt(r5)], r11._quoteBuffer);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004e, code lost:
        if ((r1 + r5) <= r6.length) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0050, code lost:
        r8 = r6.length - r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0052, code lost:
        if (r8 <= 0) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0054, code lost:
        java.lang.System.arraycopy(r11._quoteBuffer, 0, r6, r1, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0059, code lost:
        r6 = r0.finishCurrentSegment();
        r5 = r5 - r8;
        java.lang.System.arraycopy(r11._quoteBuffer, r8, r6, r1, r5);
        r1 = r1 + r5;
        r5 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0066, code lost:
        java.lang.System.arraycopy(r11._quoteBuffer, 0, r6, r1, r5);
        r1 = r1 + r5;
        r5 = r7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final char[] quoteAsString(java.lang.String r12) {
        /*
            r11 = this;
            r10 = 0
            org.codehaus.jackson.util.TextBuffer r0 = r11._textBuffer
            if (r0 != 0) goto L_0x000d
            org.codehaus.jackson.util.TextBuffer r0 = new org.codehaus.jackson.util.TextBuffer
            r1 = 0
            r0.<init>(r1)
            r11._textBuffer = r0
        L_0x000d:
            char[] r1 = r0.emptyAndGetCurrentSegment()
            int[] r2 = org.codehaus.jackson.util.CharTypes.getOutputEscapes()
            int r3 = r2.length
            int r4 = r12.length()
            r5 = r10
            r6 = r1
            r1 = r10
        L_0x001d:
            if (r5 >= r4) goto L_0x006f
        L_0x001f:
            char r7 = r12.charAt(r5)
            if (r7 >= r3) goto L_0x0029
            r8 = r2[r7]
            if (r8 != 0) goto L_0x003d
        L_0x0029:
            int r8 = r6.length
            if (r1 < r8) goto L_0x0032
            char[] r1 = r0.finishCurrentSegment()
            r6 = r1
            r1 = r10
        L_0x0032:
            int r8 = r1 + 1
            r6[r1] = r7
            int r1 = r5 + 1
            if (r1 >= r4) goto L_0x006e
            r5 = r1
            r1 = r8
            goto L_0x001f
        L_0x003d:
            int r7 = r5 + 1
            char r5 = r12.charAt(r5)
            r5 = r2[r5]
            char[] r8 = r11._quoteBuffer
            int r5 = r11._appendSingleEscape(r5, r8)
            int r8 = r1 + r5
            int r9 = r6.length
            if (r8 <= r9) goto L_0x0066
            int r8 = r6.length
            int r8 = r8 - r1
            if (r8 <= 0) goto L_0x0059
            char[] r9 = r11._quoteBuffer
            java.lang.System.arraycopy(r9, r10, r6, r1, r8)
        L_0x0059:
            char[] r6 = r0.finishCurrentSegment()
            int r5 = r5 - r8
            char[] r9 = r11._quoteBuffer
            java.lang.System.arraycopy(r9, r8, r6, r1, r5)
            int r1 = r1 + r5
            r5 = r7
            goto L_0x001d
        L_0x0066:
            char[] r8 = r11._quoteBuffer
            java.lang.System.arraycopy(r8, r10, r6, r1, r5)
            int r1 = r1 + r5
            r5 = r7
            goto L_0x001d
        L_0x006e:
            r1 = r8
        L_0x006f:
            r0.setCurrentLength(r1)
            char[] r0 = r0.contentsAsArray()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.io.JsonStringEncoder.quoteAsString(java.lang.String):char[]");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003e, code lost:
        if (r3 < r2.length) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0040, code lost:
        r2 = r0.finishCurrentSegment();
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0045, code lost:
        r6 = r4 + 1;
        r4 = r12.charAt(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004b, code lost:
        if (r4 > 127) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004d, code lost:
        r2 = _appendByteEscape(r5[r4], r0, r3);
        r4 = r6;
        r10 = r2;
        r2 = r0.getCurrentSegment();
        r3 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005e, code lost:
        if (r4 > 2047) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0060, code lost:
        r2[r3] = (byte) ((r4 >> 6) | 192);
        r4 = r3 + 1;
        r5 = r6;
        r3 = r2;
        r2 = (r4 & '?') | 128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0073, code lost:
        if (r4 < r3.length) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0075, code lost:
        r3 = r0.finishCurrentSegment();
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007a, code lost:
        r6 = r4 + 1;
        r3[r4] = (byte) r2;
        r2 = r3;
        r4 = r5;
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0086, code lost:
        if (r4 < org.codehaus.jackson.io.JsonStringEncoder.SURR1_FIRST) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x008b, code lost:
        if (r4 <= org.codehaus.jackson.io.JsonStringEncoder.SURR2_LAST) goto L_0x00b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008d, code lost:
        r5 = r3 + 1;
        r2[r3] = (byte) ((r4 >> 12) | 224);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0097, code lost:
        if (r5 < r2.length) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0099, code lost:
        r2 = r0.finishCurrentSegment();
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x009e, code lost:
        r2[r3] = (byte) (((r4 >> 6) & 63) | 128);
        r4 = r3 + 1;
        r5 = r6;
        r3 = r2;
        r2 = (r4 & '?') | 128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b6, code lost:
        if (r4 <= org.codehaus.jackson.io.JsonStringEncoder.SURR1_LAST) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b8, code lost:
        _throwIllegalSurrogate(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00bb, code lost:
        if (r6 < r1) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00bd, code lost:
        _throwIllegalSurrogate(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c0, code lost:
        r5 = r6 + 1;
        r4 = _convertSurrogate(r4, r12.charAt(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00cd, code lost:
        if (r4 <= 1114111) goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00cf, code lost:
        _throwIllegalSurrogate(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00d2, code lost:
        r6 = r3 + 1;
        r2[r3] = (byte) ((r4 >> 18) | 240);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00dc, code lost:
        if (r6 < r2.length) goto L_0x0115;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00de, code lost:
        r2 = r0.finishCurrentSegment();
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00e3, code lost:
        r6 = r3 + 1;
        r2[r3] = (byte) (((r4 >> 12) & 63) | 128);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ef, code lost:
        if (r6 < r2.length) goto L_0x0113;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f1, code lost:
        r2 = r0.finishCurrentSegment();
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00f6, code lost:
        r2[r3] = (byte) (((r4 >> 6) & 63) | 128);
        r4 = r3 + 1;
        r10 = r2;
        r2 = (r4 & '?') | 128;
        r3 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0113, code lost:
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0115, code lost:
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0117, code lost:
        r3 = r5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] quoteAsUTF8(java.lang.String r12) {
        /*
            r11 = this;
            r9 = 127(0x7f, float:1.78E-43)
            r8 = 0
            org.codehaus.jackson.util.ByteArrayBuilder r0 = r11._byteBuilder
            if (r0 != 0) goto L_0x000f
            org.codehaus.jackson.util.ByteArrayBuilder r0 = new org.codehaus.jackson.util.ByteArrayBuilder
            r1 = 0
            r0.<init>(r1)
            r11._byteBuilder = r0
        L_0x000f:
            int r1 = r12.length()
            byte[] r2 = r0.resetAndGetFirstSegment()
            r3 = r8
            r4 = r8
        L_0x0019:
            if (r4 >= r1) goto L_0x0119
            int[] r5 = org.codehaus.jackson.util.CharTypes.getOutputEscapes()
        L_0x001f:
            char r6 = r12.charAt(r4)
            if (r6 > r9) goto L_0x003d
            r7 = r5[r6]
            if (r7 != 0) goto L_0x003d
            int r7 = r2.length
            if (r3 < r7) goto L_0x0031
            byte[] r2 = r0.finishCurrentSegment()
            r3 = r8
        L_0x0031:
            int r7 = r3 + 1
            byte r6 = (byte) r6
            r2[r3] = r6
            int r3 = r4 + 1
            if (r3 >= r1) goto L_0x010b
            r4 = r3
            r3 = r7
            goto L_0x001f
        L_0x003d:
            int r6 = r2.length
            if (r3 < r6) goto L_0x0045
            byte[] r2 = r0.finishCurrentSegment()
            r3 = r8
        L_0x0045:
            int r6 = r4 + 1
            char r4 = r12.charAt(r4)
            if (r4 > r9) goto L_0x005c
            r2 = r5[r4]
            int r2 = r11._appendByteEscape(r2, r0, r3)
            byte[] r3 = r0.getCurrentSegment()
            r4 = r6
            r10 = r2
            r2 = r3
            r3 = r10
            goto L_0x0019
        L_0x005c:
            r5 = 2047(0x7ff, float:2.868E-42)
            if (r4 > r5) goto L_0x0083
            int r5 = r3 + 1
            int r7 = r4 >> 6
            r7 = r7 | 192(0xc0, float:2.69E-43)
            byte r7 = (byte) r7
            r2[r3] = r7
            r3 = r4 & 63
            r3 = r3 | 128(0x80, float:1.794E-43)
            r4 = r5
            r5 = r6
            r10 = r3
            r3 = r2
            r2 = r10
        L_0x0072:
            int r6 = r3.length
            if (r4 < r6) goto L_0x007a
            byte[] r3 = r0.finishCurrentSegment()
            r4 = r8
        L_0x007a:
            int r6 = r4 + 1
            byte r2 = (byte) r2
            r3[r4] = r2
            r2 = r3
            r4 = r5
            r3 = r6
            goto L_0x0019
        L_0x0083:
            r5 = 55296(0xd800, float:7.7486E-41)
            if (r4 < r5) goto L_0x008d
            r5 = 57343(0xdfff, float:8.0355E-41)
            if (r4 <= r5) goto L_0x00b3
        L_0x008d:
            int r5 = r3 + 1
            int r7 = r4 >> 12
            r7 = r7 | 224(0xe0, float:3.14E-43)
            byte r7 = (byte) r7
            r2[r3] = r7
            int r3 = r2.length
            if (r5 < r3) goto L_0x0117
            byte[] r2 = r0.finishCurrentSegment()
            r3 = r8
        L_0x009e:
            int r5 = r3 + 1
            int r7 = r4 >> 6
            r7 = r7 & 63
            r7 = r7 | 128(0x80, float:1.794E-43)
            byte r7 = (byte) r7
            r2[r3] = r7
            r3 = r4 & 63
            r3 = r3 | 128(0x80, float:1.794E-43)
            r4 = r5
            r5 = r6
            r10 = r3
            r3 = r2
            r2 = r10
            goto L_0x0072
        L_0x00b3:
            r5 = 56319(0xdbff, float:7.892E-41)
            if (r4 <= r5) goto L_0x00bb
            r11._throwIllegalSurrogate(r4)
        L_0x00bb:
            if (r6 < r1) goto L_0x00c0
            r11._throwIllegalSurrogate(r4)
        L_0x00c0:
            int r5 = r6 + 1
            char r6 = r12.charAt(r6)
            int r4 = r11._convertSurrogate(r4, r6)
            r6 = 1114111(0x10ffff, float:1.561202E-39)
            if (r4 <= r6) goto L_0x00d2
            r11._throwIllegalSurrogate(r4)
        L_0x00d2:
            int r6 = r3 + 1
            int r7 = r4 >> 18
            r7 = r7 | 240(0xf0, float:3.36E-43)
            byte r7 = (byte) r7
            r2[r3] = r7
            int r3 = r2.length
            if (r6 < r3) goto L_0x0115
            byte[] r2 = r0.finishCurrentSegment()
            r3 = r8
        L_0x00e3:
            int r6 = r3 + 1
            int r7 = r4 >> 12
            r7 = r7 & 63
            r7 = r7 | 128(0x80, float:1.794E-43)
            byte r7 = (byte) r7
            r2[r3] = r7
            int r3 = r2.length
            if (r6 < r3) goto L_0x0113
            byte[] r2 = r0.finishCurrentSegment()
            r3 = r8
        L_0x00f6:
            int r6 = r3 + 1
            int r7 = r4 >> 6
            r7 = r7 & 63
            r7 = r7 | 128(0x80, float:1.794E-43)
            byte r7 = (byte) r7
            r2[r3] = r7
            r3 = r4 & 63
            r3 = r3 | 128(0x80, float:1.794E-43)
            r4 = r6
            r10 = r2
            r2 = r3
            r3 = r10
            goto L_0x0072
        L_0x010b:
            r0 = r7
        L_0x010c:
            org.codehaus.jackson.util.ByteArrayBuilder r1 = r11._byteBuilder
            byte[] r0 = r1.completeAndCoalesce(r0)
            return r0
        L_0x0113:
            r3 = r6
            goto L_0x00f6
        L_0x0115:
            r3 = r6
            goto L_0x00e3
        L_0x0117:
            r3 = r5
            goto L_0x009e
        L_0x0119:
            r0 = r3
            goto L_0x010c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.io.JsonStringEncoder.quoteAsUTF8(java.lang.String):byte[]");
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    public final byte[] encodeAsUTF8(String str) {
        int i;
        int i2;
        int i3;
        int _convertSurrogate;
        int i4;
        int i5;
        ByteArrayBuilder byteArrayBuilder = this._byteBuilder;
        if (byteArrayBuilder == null) {
            byteArrayBuilder = new ByteArrayBuilder((BufferRecycler) null);
            this._byteBuilder = byteArrayBuilder;
        }
        int length = str.length();
        byte[] resetAndGetFirstSegment = byteArrayBuilder.resetAndGetFirstSegment();
        int i6 = 0;
        int i7 = 0;
        byte[] bArr = resetAndGetFirstSegment;
        int length2 = resetAndGetFirstSegment.length;
        loop0:
        while (true) {
            if (i7 >= length) {
                i = i6;
                break;
            }
            int i8 = i7 + 1;
            char charAt = str.charAt(i7);
            int i9 = i6;
            byte[] bArr2 = bArr;
            int i10 = length2;
            char c = charAt;
            while (c <= 127) {
                if (i9 >= i10) {
                    byte[] finishCurrentSegment = byteArrayBuilder.finishCurrentSegment();
                    i9 = 0;
                    byte[] bArr3 = finishCurrentSegment;
                    i10 = finishCurrentSegment.length;
                    bArr2 = bArr3;
                }
                int i11 = i9 + 1;
                bArr2[i9] = (byte) c;
                if (i8 >= length) {
                    i = i11;
                    break loop0;
                }
                char charAt2 = str.charAt(i8);
                i8++;
                c = charAt2;
                i9 = i11;
            }
            if (i9 >= i10) {
                byte[] finishCurrentSegment2 = byteArrayBuilder.finishCurrentSegment();
                i9 = 0;
                byte[] bArr4 = finishCurrentSegment2;
                i10 = finishCurrentSegment2.length;
                bArr2 = bArr4;
            }
            if (c < 2048) {
                bArr2[i9] = (byte) ((c >> 6) | 192);
                i3 = i9 + 1;
                _convertSurrogate = c;
            } else if (c < SURR1_FIRST || c > SURR2_LAST) {
                int i12 = i9 + 1;
                bArr2[i9] = (byte) ((c >> 12) | 224);
                if (i12 >= i10) {
                    byte[] finishCurrentSegment3 = byteArrayBuilder.finishCurrentSegment();
                    i2 = 0;
                    byte[] bArr5 = finishCurrentSegment3;
                    i10 = finishCurrentSegment3.length;
                    bArr2 = bArr5;
                } else {
                    i2 = i12;
                }
                bArr2[i2] = (byte) (((c >> 6) & 63) | 128);
                i3 = i2 + 1;
                _convertSurrogate = c;
            } else {
                if (c > SURR1_LAST) {
                    _throwIllegalSurrogate(c);
                }
                if (i8 >= length) {
                    _throwIllegalSurrogate(c);
                }
                int i13 = i8 + 1;
                _convertSurrogate = _convertSurrogate(c, str.charAt(i8));
                if (_convertSurrogate > 1114111) {
                    _throwIllegalSurrogate(_convertSurrogate);
                }
                int i14 = i9 + 1;
                bArr2[i9] = (byte) ((_convertSurrogate >> 18) | 240);
                if (i14 >= i10) {
                    byte[] finishCurrentSegment4 = byteArrayBuilder.finishCurrentSegment();
                    i4 = 0;
                    byte[] bArr6 = finishCurrentSegment4;
                    i10 = finishCurrentSegment4.length;
                    bArr2 = bArr6;
                } else {
                    i4 = i14;
                }
                int i15 = i4 + 1;
                bArr2[i4] = (byte) (((_convertSurrogate >> 12) & 63) | 128);
                if (i15 >= i10) {
                    byte[] finishCurrentSegment5 = byteArrayBuilder.finishCurrentSegment();
                    i5 = 0;
                    byte[] bArr7 = finishCurrentSegment5;
                    i10 = finishCurrentSegment5.length;
                    bArr2 = bArr7;
                } else {
                    i5 = i15;
                }
                bArr2[i5] = (byte) (((_convertSurrogate >> 6) & 63) | 128);
                i3 = i5 + 1;
                i8 = i13;
            }
            if (i3 >= i10) {
                byte[] finishCurrentSegment6 = byteArrayBuilder.finishCurrentSegment();
                i3 = 0;
                byte[] bArr8 = finishCurrentSegment6;
                i10 = finishCurrentSegment6.length;
                bArr2 = bArr8;
            }
            int i16 = i3 + 1;
            bArr2[i3] = (byte) ((_convertSurrogate & 63) | 128);
            length2 = i10;
            i7 = i8;
            bArr = bArr2;
            i6 = i16;
        }
        return this._byteBuilder.completeAndCoalesce(i);
    }

    private int _appendSingleEscape(int i, char[] cArr) {
        if (i < 0) {
            int i2 = -(i + 1);
            cArr[1] = 'u';
            cArr[4] = HEX_CHARS[i2 >> 4];
            cArr[5] = HEX_CHARS[i2 & 15];
            return 6;
        }
        cArr[1] = (char) i;
        return 2;
    }

    private int _appendByteEscape(int i, ByteArrayBuilder byteArrayBuilder, int i2) {
        byteArrayBuilder.setCurrentSegmentLength(i2);
        byteArrayBuilder.append(INT_BACKSLASH);
        if (i < 0) {
            int i3 = -(i + 1);
            byteArrayBuilder.append(INT_U);
            byteArrayBuilder.append(INT_0);
            byteArrayBuilder.append(INT_0);
            byteArrayBuilder.append(HEX_BYTES[i3 >> 4]);
            byteArrayBuilder.append(HEX_BYTES[i3 & 15]);
        } else {
            byteArrayBuilder.append((byte) i);
        }
        return byteArrayBuilder.getCurrentSegmentLength();
    }

    private int _convertSurrogate(int i, int i2) {
        if (i2 >= SURR2_FIRST && i2 <= SURR2_LAST) {
            return 65536 + ((i - SURR1_FIRST) << 10) + (i2 - SURR2_FIRST);
        }
        throw new IllegalArgumentException("Broken surrogate pair: first char 0x" + Integer.toHexString(i) + ", second 0x" + Integer.toHexString(i2) + "; illegal combination");
    }

    private void _throwIllegalSurrogate(int i) {
        if (i > 1114111) {
            throw new IllegalArgumentException("Illegal character point (0x" + Integer.toHexString(i) + ") to output; max is 0x10FFFF as per RFC 4627");
        } else if (i < SURR1_FIRST) {
            throw new IllegalArgumentException("Illegal character point (0x" + Integer.toHexString(i) + ") to output");
        } else if (i <= SURR1_LAST) {
            throw new IllegalArgumentException("Unmatched first part of surrogate pair (0x" + Integer.toHexString(i) + ")");
        } else {
            throw new IllegalArgumentException("Unmatched second part of surrogate pair (0x" + Integer.toHexString(i) + ")");
        }
    }
}
