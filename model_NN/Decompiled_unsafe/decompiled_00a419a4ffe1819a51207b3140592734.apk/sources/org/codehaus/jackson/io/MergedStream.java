package org.codehaus.jackson.io;

import java.io.IOException;
import java.io.InputStream;

public final class MergedStream extends InputStream {
    byte[] _buffer;
    protected final IOContext _context;
    final int _end;
    final InputStream _in;
    int _ptr;

    public MergedStream(IOContext iOContext, InputStream inputStream, byte[] bArr, int i, int i2) {
        this._context = iOContext;
        this._in = inputStream;
        this._buffer = bArr;
        this._ptr = i;
        this._end = i2;
    }

    public final int available() throws IOException {
        if (this._buffer != null) {
            return this._end - this._ptr;
        }
        return this._in.available();
    }

    public final void close() throws IOException {
        freeMergedBuffer();
        this._in.close();
    }

    public final void mark(int i) {
        if (this._buffer == null) {
            this._in.mark(i);
        }
    }

    public final boolean markSupported() {
        return this._buffer == null && this._in.markSupported();
    }

    public final int read() throws IOException {
        if (this._buffer == null) {
            return this._in.read();
        }
        byte[] bArr = this._buffer;
        int i = this._ptr;
        this._ptr = i + 1;
        byte b = bArr[i] & 255;
        if (this._ptr < this._end) {
            return b;
        }
        freeMergedBuffer();
        return b;
    }

    public final int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        if (this._buffer == null) {
            return this._in.read(bArr, i, i2);
        }
        int i3 = this._end - this._ptr;
        if (i2 <= i3) {
            i3 = i2;
        }
        System.arraycopy(this._buffer, this._ptr, bArr, i, i3);
        this._ptr += i3;
        if (this._ptr < this._end) {
            return i3;
        }
        freeMergedBuffer();
        return i3;
    }

    public final void reset() throws IOException {
        if (this._buffer == null) {
            this._in.reset();
        }
    }

    public final long skip(long j) throws IOException {
        long j2;
        long j3;
        if (this._buffer != null) {
            int i = this._end - this._ptr;
            if (((long) i) > j) {
                this._ptr += (int) j;
                return j;
            }
            freeMergedBuffer();
            j2 = ((long) i) + 0;
            j3 = j - ((long) i);
        } else {
            j2 = 0;
            j3 = j;
        }
        if (j3 > 0) {
            return j2 + this._in.skip(j3);
        }
        return j2;
    }

    private void freeMergedBuffer() {
        byte[] bArr = this._buffer;
        if (bArr != null) {
            this._buffer = null;
            this._context.releaseReadIOBuffer(bArr);
        }
    }
}
