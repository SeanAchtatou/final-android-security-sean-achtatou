package org.codehaus.jackson.io;

import java.io.CharConversionException;
import java.io.IOException;
import java.io.InputStream;

public final class UTF32Reader extends BaseReader {
    final boolean mBigEndian;
    int mByteCount = 0;
    int mCharCount = 0;
    char mSurrogate = 0;

    public final /* bridge */ /* synthetic */ void close() throws IOException {
        super.close();
    }

    public final /* bridge */ /* synthetic */ int read() throws IOException {
        return super.read();
    }

    public UTF32Reader(IOContext iOContext, InputStream inputStream, byte[] bArr, int i, int i2, boolean z) {
        super(iOContext, inputStream, bArr, i, i2);
        this.mBigEndian = z;
    }

    public final int read(char[] cArr, int i, int i2) throws IOException {
        int i3;
        int i4;
        byte b;
        int i5;
        byte b2;
        if (this._buffer == null) {
            return -1;
        }
        if (i2 <= 0) {
            return i2;
        }
        if (i < 0 || i + i2 > cArr.length) {
            reportBounds(cArr, i, i2);
        }
        int i6 = i2 + i;
        if (this.mSurrogate != 0) {
            i3 = i + 1;
            cArr[i] = this.mSurrogate;
            this.mSurrogate = 0;
        } else {
            int i7 = this._length - this._ptr;
            if (i7 < 4 && !loadMore(i7)) {
                return -1;
            }
            i3 = i;
        }
        while (true) {
            if (i3 >= i6) {
                i4 = i3;
                break;
            }
            int i8 = this._ptr;
            if (this.mBigEndian) {
                b = (this._buffer[i8 + 3] & 255) | (this._buffer[i8] << 24) | ((this._buffer[i8 + 1] & 255) << 16) | ((this._buffer[i8 + 2] & 255) << 8);
            } else {
                b = (this._buffer[i8 + 3] << 24) | (this._buffer[i8] & 255) | ((this._buffer[i8 + 1] & 255) << 8) | ((this._buffer[i8 + 2] & 255) << 16);
            }
            this._ptr += 4;
            if (b > 65535) {
                if (b > 1114111) {
                    reportInvalid(b, i3 - i, "(above " + Integer.toHexString(1114111) + ") ");
                }
                int i9 = b - 65536;
                int i10 = i3 + 1;
                cArr[i3] = (char) (55296 + (i9 >> 10));
                b2 = 56320 | (i9 & 1023);
                if (i10 >= i6) {
                    this.mSurrogate = (char) b2;
                    i4 = i10;
                    break;
                }
                i5 = i10;
            } else {
                byte b3 = b;
                i5 = i3;
                b2 = b3;
            }
            int i11 = i5 + 1;
            cArr[i5] = (char) b2;
            if (this._ptr >= this._length) {
                i4 = i11;
                break;
            }
            i3 = i11;
        }
        int i12 = i4 - i;
        this.mCharCount += i12;
        return i12;
    }

    private void reportUnexpectedEOF(int i, int i2) throws IOException {
        throw new CharConversionException("Unexpected EOF in the middle of a 4-byte UTF-32 char: got " + i + ", needed " + i2 + ", at char #" + this.mCharCount + ", byte #" + (this.mByteCount + i) + ")");
    }

    private void reportInvalid(int i, int i2, String str) throws IOException {
        throw new CharConversionException("Invalid UTF-32 character 0x" + Integer.toHexString(i) + str + " at char #" + (this.mCharCount + i2) + ", byte #" + ((this.mByteCount + this._ptr) - 1) + ")");
    }

    private boolean loadMore(int i) throws IOException {
        this.mByteCount += this._length - i;
        if (i > 0) {
            if (this._ptr > 0) {
                for (int i2 = 0; i2 < i; i2++) {
                    this._buffer[i2] = this._buffer[this._ptr + i2];
                }
                this._ptr = 0;
            }
            this._length = i;
        } else {
            this._ptr = 0;
            int read = this._in.read(this._buffer);
            if (read <= 0) {
                this._length = 0;
                if (read < 0) {
                    freeBuffers();
                    return false;
                }
                reportStrangeStream();
            }
            this._length = read;
        }
        while (this._length < 4) {
            int read2 = this._in.read(this._buffer, this._length, this._buffer.length - this._length);
            if (read2 <= 0) {
                if (read2 < 0) {
                    freeBuffers();
                    reportUnexpectedEOF(this._length, 4);
                }
                reportStrangeStream();
            }
            this._length = read2 + this._length;
        }
        return true;
    }
}
