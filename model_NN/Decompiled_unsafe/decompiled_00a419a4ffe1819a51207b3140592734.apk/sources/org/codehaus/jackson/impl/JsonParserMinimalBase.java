package org.codehaus.jackson.impl;

import java.io.IOException;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonStreamContext;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.io.NumberInput;

public abstract class JsonParserMinimalBase extends JsonParser {
    protected static final int INT_APOSTROPHE = 39;
    protected static final int INT_ASTERISK = 42;
    protected static final int INT_BACKSLASH = 92;
    protected static final int INT_COLON = 58;
    protected static final int INT_COMMA = 44;
    protected static final int INT_CR = 13;
    protected static final int INT_LBRACKET = 91;
    protected static final int INT_LCURLY = 123;
    protected static final int INT_LF = 10;
    protected static final int INT_QUOTE = 34;
    protected static final int INT_RBRACKET = 93;
    protected static final int INT_RCURLY = 125;
    protected static final int INT_SLASH = 47;
    protected static final int INT_SPACE = 32;
    protected static final int INT_TAB = 9;
    protected static final int INT_b = 98;
    protected static final int INT_f = 102;
    protected static final int INT_n = 110;
    protected static final int INT_r = 114;
    protected static final int INT_t = 116;
    protected static final int INT_u = 117;

    /* access modifiers changed from: protected */
    public abstract void _handleEOF() throws JsonParseException;

    public abstract void close() throws IOException;

    public abstract byte[] getBinaryValue(Base64Variant base64Variant) throws IOException, JsonParseException;

    public abstract String getCurrentName() throws IOException, JsonParseException;

    public abstract JsonStreamContext getParsingContext();

    public abstract String getText() throws IOException, JsonParseException;

    public abstract char[] getTextCharacters() throws IOException, JsonParseException;

    public abstract int getTextLength() throws IOException, JsonParseException;

    public abstract int getTextOffset() throws IOException, JsonParseException;

    public abstract boolean hasTextCharacters();

    public abstract boolean isClosed();

    public abstract JsonToken nextToken() throws IOException, JsonParseException;

    protected JsonParserMinimalBase() {
    }

    protected JsonParserMinimalBase(int i) {
        super(i);
    }

    public JsonParser skipChildren() throws IOException, JsonParseException {
        if (this._currToken == JsonToken.START_OBJECT || this._currToken == JsonToken.START_ARRAY) {
            int i = 1;
            while (true) {
                JsonToken nextToken = nextToken();
                if (nextToken != null) {
                    switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[nextToken.ordinal()]) {
                        case JsonWriteContext.STATUS_OK_AFTER_COMMA /*1*/:
                        case JsonWriteContext.STATUS_OK_AFTER_COLON /*2*/:
                            i++;
                            break;
                        case JsonWriteContext.STATUS_OK_AFTER_SPACE /*3*/:
                        case JsonWriteContext.STATUS_EXPECT_VALUE /*4*/:
                            i--;
                            if (i != 0) {
                                break;
                            } else {
                                return this;
                            }
                    }
                } else {
                    _handleEOF();
                    return this;
                }
            }
        } else {
            return this;
        }
    }

    /* renamed from: org.codehaus.jackson.impl.JsonParserMinimalBase$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$codehaus$jackson$JsonToken = new int[JsonToken.values().length];

        static {
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.START_OBJECT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.START_ARRAY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.END_OBJECT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.END_ARRAY.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_NUMBER_INT.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_TRUE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_FALSE.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_NULL.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_EMBEDDED_OBJECT.ordinal()] = JsonParserMinimalBase.INT_TAB;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_STRING.ordinal()] = JsonParserMinimalBase.INT_LF;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_NUMBER_FLOAT.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean getValueAsBoolean(boolean r6) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r5 = this;
            r4 = 0
            r3 = 1
            org.codehaus.jackson.JsonToken r1 = r5._currToken
            if (r1 == 0) goto L_0x0013
            int[] r1 = org.codehaus.jackson.impl.JsonParserMinimalBase.AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken
            org.codehaus.jackson.JsonToken r2 = r5._currToken
            int r2 = r2.ordinal()
            r1 = r1[r2]
            switch(r1) {
                case 5: goto L_0x0015;
                case 6: goto L_0x001f;
                case 7: goto L_0x0021;
                case 8: goto L_0x0021;
                case 9: goto L_0x0023;
                case 10: goto L_0x0034;
                default: goto L_0x0013;
            }
        L_0x0013:
            r1 = r6
        L_0x0014:
            return r1
        L_0x0015:
            int r1 = r5.getIntValue()
            if (r1 == 0) goto L_0x001d
            r1 = r3
            goto L_0x0014
        L_0x001d:
            r1 = r4
            goto L_0x0014
        L_0x001f:
            r1 = r3
            goto L_0x0014
        L_0x0021:
            r1 = r4
            goto L_0x0014
        L_0x0023:
            java.lang.Object r1 = r5.getEmbeddedObject()
            boolean r2 = r1 instanceof java.lang.Boolean
            if (r2 == 0) goto L_0x0034
            r0 = r1
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            r5 = r0
            boolean r1 = r5.booleanValue()
            goto L_0x0014
        L_0x0034:
            java.lang.String r1 = r5.getText()
            java.lang.String r1 = r1.trim()
            java.lang.String r2 = "true"
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x0013
            r1 = r3
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.JsonParserMinimalBase.getValueAsBoolean(boolean):boolean");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public int getValueAsInt(int i) throws IOException, JsonParseException {
        if (this._currToken != null) {
            switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[this._currToken.ordinal()]) {
                case JsonWriteContext.STATUS_EXPECT_NAME /*5*/:
                case 11:
                    return getIntValue();
                case 6:
                    return 1;
                case 7:
                case 8:
                    return 0;
                case INT_TAB /*9*/:
                    Object embeddedObject = getEmbeddedObject();
                    if (embeddedObject instanceof Number) {
                        return ((Number) embeddedObject).intValue();
                    }
                    break;
                case INT_LF /*10*/:
                    return NumberInput.parseAsInt(getText(), i);
            }
        }
        return i;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public long getValueAsLong(long j) throws IOException, JsonParseException {
        if (this._currToken != null) {
            switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[this._currToken.ordinal()]) {
                case JsonWriteContext.STATUS_EXPECT_NAME /*5*/:
                case 11:
                    return getLongValue();
                case 6:
                    return 1;
                case 7:
                case 8:
                    return 0;
                case INT_TAB /*9*/:
                    Object embeddedObject = getEmbeddedObject();
                    if (embeddedObject instanceof Number) {
                        return ((Number) embeddedObject).longValue();
                    }
                    break;
                case INT_LF /*10*/:
                    return NumberInput.parseAsLong(getText(), j);
            }
        }
        return j;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public double getValueAsDouble(double d) throws IOException, JsonParseException {
        if (this._currToken != null) {
            switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[this._currToken.ordinal()]) {
                case JsonWriteContext.STATUS_EXPECT_NAME /*5*/:
                case 11:
                    return getDoubleValue();
                case 6:
                    return 1.0d;
                case 7:
                case 8:
                    return 0.0d;
                case INT_TAB /*9*/:
                    Object embeddedObject = getEmbeddedObject();
                    if (embeddedObject instanceof Number) {
                        return ((Number) embeddedObject).doubleValue();
                    }
                    break;
                case INT_LF /*10*/:
                    return NumberInput.parseAsDouble(getText(), d);
            }
        }
        return d;
    }

    /* access modifiers changed from: protected */
    public void _reportUnexpectedChar(int i, String str) throws JsonParseException {
        String str2 = "Unexpected character (" + _getCharDesc(i) + ")";
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        _reportError(str2);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidEOF() throws JsonParseException {
        _reportInvalidEOF(" in " + this._currToken);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidEOF(String str) throws JsonParseException {
        _reportError("Unexpected end-of-input" + str);
    }

    /* access modifiers changed from: protected */
    public void _throwInvalidSpace(int i) throws JsonParseException {
        _reportError("Illegal character (" + _getCharDesc((char) i) + "): only regular white space (\\r, \\n, \\t) is allowed between tokens");
    }

    /* access modifiers changed from: protected */
    public void _throwUnquotedSpace(int i, String str) throws JsonParseException {
        if (!isEnabled(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS) || i >= INT_SPACE) {
            _reportError("Illegal unquoted character (" + _getCharDesc((char) i) + "): has to be escaped using backslash to be included in " + str);
        }
    }

    /* access modifiers changed from: protected */
    public char _handleUnrecognizedCharacterEscape(char c) throws JsonProcessingException {
        if (!isEnabled(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER)) {
            _reportError("Unrecognized character escape " + _getCharDesc(c));
        }
        return c;
    }

    protected static final String _getCharDesc(int i) {
        char c = (char) i;
        if (Character.isISOControl(c)) {
            return "(CTRL-CHAR, code " + i + ")";
        }
        if (i > 255) {
            return "'" + c + "' (code " + i + " / 0x" + Integer.toHexString(i) + ")";
        }
        return "'" + c + "' (code " + i + ")";
    }

    /* access modifiers changed from: protected */
    public final void _reportError(String str) throws JsonParseException {
        throw _constructError(str);
    }

    /* access modifiers changed from: protected */
    public final void _wrapError(String str, Throwable th) throws JsonParseException {
        throw _constructError(str, th);
    }

    /* access modifiers changed from: protected */
    public final void _throwInternal() {
        throw new RuntimeException("Internal error: this code path should never get executed");
    }

    /* access modifiers changed from: protected */
    public final JsonParseException _constructError(String str, Throwable th) {
        return new JsonParseException(str, getCurrentLocation(), th);
    }
}
