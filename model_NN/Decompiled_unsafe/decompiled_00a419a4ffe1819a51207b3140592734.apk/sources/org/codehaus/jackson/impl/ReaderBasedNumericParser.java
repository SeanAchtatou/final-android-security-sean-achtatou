package org.codehaus.jackson.impl;

import java.io.Reader;
import org.codehaus.jackson.io.IOContext;

public abstract class ReaderBasedNumericParser extends ReaderBasedParserBase {
    public ReaderBasedNumericParser(IOContext iOContext, int i, Reader reader) {
        super(iOContext, i, reader);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00c4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.codehaus.jackson.JsonToken parseNumberText(int r12) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r11 = this;
            r7 = 2
            r6 = 1
            r8 = 0
            r10 = 57
            r9 = 48
            r0 = 45
            if (r12 != r0) goto L_0x004b
            r0 = r6
        L_0x000c:
            int r1 = r11._inputPtr
            int r2 = r1 - r6
            int r3 = r11._inputEnd
            if (r0 == 0) goto L_0x0028
            int r4 = r11._inputEnd
            if (r1 >= r4) goto L_0x00b9
            char[] r4 = r11._inputBuffer
            int r5 = r1 + 1
            char r1 = r4[r1]
            if (r1 > r10) goto L_0x0022
            if (r1 >= r9) goto L_0x0027
        L_0x0022:
            java.lang.String r4 = "expected digit (0-9) to follow minus sign, for valid numeric value"
            r11.reportUnexpectedNumberChar(r1, r4)
        L_0x0027:
            r1 = r5
        L_0x0028:
            r4 = r1
            r1 = r6
        L_0x002a:
            int r5 = r11._inputEnd
            if (r4 >= r5) goto L_0x00b9
            char[] r5 = r11._inputBuffer
            int r6 = r4 + 1
            char r4 = r5[r4]
            if (r4 < r9) goto L_0x004d
            if (r4 > r10) goto L_0x004d
            int r1 = r1 + 1
            if (r1 != r7) goto L_0x00d0
            char[] r4 = r11._inputBuffer
            int r5 = r6 - r7
            char r4 = r4[r5]
            if (r4 != r9) goto L_0x00d0
            java.lang.String r4 = "Leading zeroes not allowed"
            r11.reportInvalidNumber(r4)
            r4 = r6
            goto L_0x002a
        L_0x004b:
            r0 = r8
            goto L_0x000c
        L_0x004d:
            r5 = 46
            if (r4 != r5) goto L_0x00cc
            r4 = r8
            r5 = r6
        L_0x0053:
            if (r5 >= r3) goto L_0x00b9
            char[] r6 = r11._inputBuffer
            int r7 = r5 + 1
            char r5 = r6[r5]
            if (r5 < r9) goto L_0x0063
            if (r5 > r10) goto L_0x0063
            int r4 = r4 + 1
            r5 = r7
            goto L_0x0053
        L_0x0063:
            if (r4 != 0) goto L_0x006a
            java.lang.String r6 = "Decimal point not followed by a digit"
            r11.reportUnexpectedNumberChar(r5, r6)
        L_0x006a:
            r6 = r5
            r5 = r7
        L_0x006c:
            r7 = 101(0x65, float:1.42E-43)
            if (r6 == r7) goto L_0x0074
            r7 = 69
            if (r6 != r7) goto L_0x00ca
        L_0x0074:
            if (r5 >= r3) goto L_0x00b9
            char[] r6 = r11._inputBuffer
            int r7 = r5 + 1
            char r5 = r6[r5]
            r6 = 45
            if (r5 == r6) goto L_0x0084
            r6 = 43
            if (r5 != r6) goto L_0x00c6
        L_0x0084:
            if (r7 >= r3) goto L_0x00b9
            char[] r5 = r11._inputBuffer
            int r6 = r7 + 1
            char r5 = r5[r7]
            r7 = r5
            r5 = r8
        L_0x008e:
            if (r7 > r10) goto L_0x009f
            if (r7 < r9) goto L_0x009f
            int r5 = r5 + 1
            if (r6 >= r3) goto L_0x00b9
            char[] r7 = r11._inputBuffer
            int r8 = r6 + 1
            char r6 = r7[r6]
            r7 = r6
            r6 = r8
            goto L_0x008e
        L_0x009f:
            if (r5 != 0) goto L_0x00a6
            java.lang.String r3 = "Exponent indicator not followed by a digit"
            r11.reportUnexpectedNumberChar(r7, r3)
        L_0x00a6:
            r3 = r5
            r5 = r6
        L_0x00a8:
            int r5 = r5 + -1
            r11._inputPtr = r5
            int r5 = r5 - r2
            org.codehaus.jackson.util.TextBuffer r6 = r11._textBuffer
            char[] r7 = r11._inputBuffer
            r6.resetWithShared(r7, r2, r5)
            org.codehaus.jackson.JsonToken r0 = r11.reset(r0, r1, r4, r3)
        L_0x00b8:
            return r0
        L_0x00b9:
            if (r0 == 0) goto L_0x00c4
            int r1 = r2 + 1
        L_0x00bd:
            r11._inputPtr = r1
            org.codehaus.jackson.JsonToken r0 = r11.parseNumberText2(r0)
            goto L_0x00b8
        L_0x00c4:
            r1 = r2
            goto L_0x00bd
        L_0x00c6:
            r6 = r7
            r7 = r5
            r5 = r8
            goto L_0x008e
        L_0x00ca:
            r3 = r8
            goto L_0x00a8
        L_0x00cc:
            r5 = r6
            r6 = r4
            r4 = r8
            goto L_0x006c
        L_0x00d0:
            r4 = r6
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.ReaderBasedNumericParser.parseNumberText(int):org.codehaus.jackson.JsonToken");
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x00e1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final org.codehaus.jackson.JsonToken parseNumberText2(boolean r15) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r14 = this;
            r8 = 45
            r12 = 57
            r11 = 48
            r10 = 1
            r9 = 0
            org.codehaus.jackson.util.TextBuffer r0 = r14._textBuffer
            char[] r0 = r0.emptyAndGetCurrentSegment()
            if (r15 == 0) goto L_0x0191
            int r1 = r9 + 1
            r0[r9] = r8
        L_0x0014:
            r2 = r0
            r0 = r9
        L_0x0016:
            int r3 = r14._inputPtr
            int r4 = r14._inputEnd
            if (r3 < r4) goto L_0x00f8
            boolean r3 = r14.loadMore()
            if (r3 != 0) goto L_0x00f8
            r3 = r10
            r4 = r9
        L_0x0024:
            if (r0 != 0) goto L_0x0046
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Missing integer part (next char "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = _getCharDesc(r4)
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = ")"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r14.reportInvalidNumber(r5)
        L_0x0046:
            r5 = 46
            if (r4 != r5) goto L_0x0186
            int r5 = r1 + 1
            r2[r1] = r4
            r1 = r9
            r13 = r4
            r4 = r5
            r5 = r2
            r2 = r13
        L_0x0053:
            int r6 = r14._inputPtr
            int r7 = r14._inputEnd
            if (r6 < r7) goto L_0x0128
            boolean r6 = r14.loadMore()
            if (r6 != 0) goto L_0x0128
            r3 = r2
            r2 = r10
        L_0x0061:
            if (r1 != 0) goto L_0x0068
            java.lang.String r6 = "Decimal point not followed by a digit"
            r14.reportUnexpectedNumberChar(r3, r6)
        L_0x0068:
            r6 = 101(0x65, float:1.42E-43)
            if (r3 == r6) goto L_0x0070
            r6 = 69
            if (r3 != r6) goto L_0x017d
        L_0x0070:
            int r6 = r5.length
            if (r4 < r6) goto L_0x007b
            org.codehaus.jackson.util.TextBuffer r4 = r14._textBuffer
            char[] r4 = r4.finishCurrentSegment()
            r5 = r4
            r4 = r9
        L_0x007b:
            int r6 = r4 + 1
            r5[r4] = r3
            int r3 = r14._inputPtr
            int r4 = r14._inputEnd
            if (r3 >= r4) goto L_0x014a
            char[] r3 = r14._inputBuffer
            int r4 = r14._inputPtr
            int r7 = r4 + 1
            r14._inputPtr = r7
            char r3 = r3[r4]
        L_0x008f:
            if (r3 == r8) goto L_0x0095
            r4 = 43
            if (r3 != r4) goto L_0x0176
        L_0x0095:
            int r4 = r5.length
            if (r6 < r4) goto L_0x0173
            org.codehaus.jackson.util.TextBuffer r4 = r14._textBuffer
            char[] r4 = r4.finishCurrentSegment()
            r5 = r4
            r4 = r9
        L_0x00a0:
            int r6 = r4 + 1
            r5[r4] = r3
            int r3 = r14._inputPtr
            int r4 = r14._inputEnd
            if (r3 >= r4) goto L_0x0152
            char[] r3 = r14._inputBuffer
            int r4 = r14._inputPtr
            int r7 = r4 + 1
            r14._inputPtr = r7
            char r3 = r3[r4]
            r4 = r9
            r13 = r6
            r6 = r5
            r5 = r13
        L_0x00b8:
            r13 = r4
            r4 = r3
            r3 = r13
        L_0x00bb:
            if (r4 > r12) goto L_0x016e
            if (r4 < r11) goto L_0x016e
            int r3 = r3 + 1
            int r7 = r6.length
            if (r5 < r7) goto L_0x00cc
            org.codehaus.jackson.util.TextBuffer r5 = r14._textBuffer
            char[] r5 = r5.finishCurrentSegment()
            r6 = r5
            r5 = r9
        L_0x00cc:
            int r7 = r5 + 1
            r6[r5] = r4
            int r5 = r14._inputPtr
            int r8 = r14._inputEnd
            if (r5 < r8) goto L_0x015e
            boolean r5 = r14.loadMore()
            if (r5 != 0) goto L_0x015e
            r2 = r3
            r5 = r7
            r3 = r10
        L_0x00df:
            if (r2 != 0) goto L_0x00e6
            java.lang.String r6 = "Exponent indicator not followed by a digit"
            r14.reportUnexpectedNumberChar(r4, r6)
        L_0x00e6:
            r4 = r5
        L_0x00e7:
            if (r3 != 0) goto L_0x00ee
            int r3 = r14._inputPtr
            int r3 = r3 - r10
            r14._inputPtr = r3
        L_0x00ee:
            org.codehaus.jackson.util.TextBuffer r3 = r14._textBuffer
            r3.setCurrentLength(r4)
            org.codehaus.jackson.JsonToken r0 = r14.reset(r15, r0, r1, r2)
            return r0
        L_0x00f8:
            char[] r3 = r14._inputBuffer
            int r4 = r14._inputPtr
            int r5 = r4 + 1
            r14._inputPtr = r5
            char r3 = r3[r4]
            if (r3 < r11) goto L_0x018d
            if (r3 > r12) goto L_0x018d
            int r0 = r0 + 1
            r4 = 2
            if (r0 != r4) goto L_0x0116
            int r4 = r1 - r10
            char r4 = r2[r4]
            if (r4 != r11) goto L_0x0116
            java.lang.String r4 = "Leading zeroes not allowed"
            r14.reportInvalidNumber(r4)
        L_0x0116:
            int r4 = r2.length
            if (r1 < r4) goto L_0x0121
            org.codehaus.jackson.util.TextBuffer r1 = r14._textBuffer
            char[] r1 = r1.finishCurrentSegment()
            r2 = r1
            r1 = r9
        L_0x0121:
            int r4 = r1 + 1
            r2[r1] = r3
            r1 = r4
            goto L_0x0016
        L_0x0128:
            char[] r2 = r14._inputBuffer
            int r6 = r14._inputPtr
            int r7 = r6 + 1
            r14._inputPtr = r7
            char r2 = r2[r6]
            if (r2 < r11) goto L_0x0181
            if (r2 > r12) goto L_0x0181
            int r1 = r1 + 1
            int r6 = r5.length
            if (r4 < r6) goto L_0x0143
            org.codehaus.jackson.util.TextBuffer r4 = r14._textBuffer
            char[] r4 = r4.finishCurrentSegment()
            r5 = r4
            r4 = r9
        L_0x0143:
            int r6 = r4 + 1
            r5[r4] = r2
            r4 = r6
            goto L_0x0053
        L_0x014a:
            java.lang.String r3 = "expected a digit for number exponent"
            char r3 = r14.getNextChar(r3)
            goto L_0x008f
        L_0x0152:
            java.lang.String r3 = "expected a digit for number exponent"
            char r3 = r14.getNextChar(r3)
            r4 = r9
            r13 = r6
            r6 = r5
            r5 = r13
            goto L_0x00b8
        L_0x015e:
            char[] r4 = r14._inputBuffer
            int r5 = r14._inputPtr
            int r8 = r5 + 1
            r14._inputPtr = r8
            char r4 = r4[r5]
            r5 = r7
            r13 = r3
            r3 = r4
            r4 = r13
            goto L_0x00b8
        L_0x016e:
            r13 = r3
            r3 = r2
            r2 = r13
            goto L_0x00df
        L_0x0173:
            r4 = r6
            goto L_0x00a0
        L_0x0176:
            r4 = r3
            r3 = r9
            r13 = r6
            r6 = r5
            r5 = r13
            goto L_0x00bb
        L_0x017d:
            r3 = r2
            r2 = r9
            goto L_0x00e7
        L_0x0181:
            r13 = r3
            r3 = r2
            r2 = r13
            goto L_0x0061
        L_0x0186:
            r5 = r2
            r2 = r3
            r3 = r4
            r4 = r1
            r1 = r9
            goto L_0x0068
        L_0x018d:
            r4 = r3
            r3 = r9
            goto L_0x0024
        L_0x0191:
            r1 = r9
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.ReaderBasedNumericParser.parseNumberText2(boolean):org.codehaus.jackson.JsonToken");
    }
}
