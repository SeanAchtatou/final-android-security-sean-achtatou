package org.codehaus.jackson.map.ser;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.RandomAccess;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.JsonSerializable;
import org.codehaus.jackson.map.JsonSerializableWithType;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.SerializerFactory;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ext.OptionalHandlerFactory;
import org.codehaus.jackson.map.introspect.Annotated;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.AnnotatedMethod;
import org.codehaus.jackson.map.introspect.BasicBeanDescription;
import org.codehaus.jackson.map.jsontype.NamedType;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.map.ser.ArraySerializers;
import org.codehaus.jackson.map.ser.StdSerializers;
import org.codehaus.jackson.map.ser.impl.IndexedStringListSerializer;
import org.codehaus.jackson.map.ser.impl.ObjectArraySerializer;
import org.codehaus.jackson.map.ser.impl.StringCollectionSerializer;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.map.util.ClassUtil;
import org.codehaus.jackson.map.util.EnumValues;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.util.TokenBuffer;

public abstract class BasicSerializerFactory extends SerializerFactory {
    static final JsonSerializer<?> MARKER_COLLECTION = new SerializerMarker();
    static final JsonSerializer<?> MARKER_INDEXED_LIST = new SerializerMarker();
    static final JsonSerializer<?> MARKER_OBJECT_ARRAY = new SerializerMarker();
    static final JsonSerializer<?> MARKER_OBJECT_MAP = new SerializerMarker();
    static final JsonSerializer<?> MARKER_STRING_ARRAY = new SerializerMarker();
    protected static final HashMap<String, JsonSerializer<?>> _concrete = new HashMap<>();
    protected static final HashMap<String, Class<? extends JsonSerializer<?>>> _concreteLazy = new HashMap<>();
    protected OptionalHandlerFactory optionalHandlers = OptionalHandlerFactory.instance;

    private static final class SerializerMarker extends JsonSerializer<Object> {
        private SerializerMarker() {
        }

        public final void serialize(Object obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        }
    }

    static {
        _concrete.put(String.class.getName(), new StdSerializers.StringSerializer());
        ToStringSerializer toStringSerializer = ToStringSerializer.instance;
        _concrete.put(StringBuffer.class.getName(), toStringSerializer);
        _concrete.put(StringBuilder.class.getName(), toStringSerializer);
        _concrete.put(Character.class.getName(), toStringSerializer);
        _concrete.put(Character.TYPE.getName(), toStringSerializer);
        _concrete.put(Boolean.TYPE.getName(), new StdSerializers.BooleanSerializer(true));
        _concrete.put(Boolean.class.getName(), new StdSerializers.BooleanSerializer(false));
        StdSerializers.IntegerSerializer integerSerializer = new StdSerializers.IntegerSerializer();
        _concrete.put(Integer.class.getName(), integerSerializer);
        _concrete.put(Integer.TYPE.getName(), integerSerializer);
        _concrete.put(Long.class.getName(), StdSerializers.LongSerializer.instance);
        _concrete.put(Long.TYPE.getName(), StdSerializers.LongSerializer.instance);
        _concrete.put(Byte.class.getName(), StdSerializers.IntLikeSerializer.instance);
        _concrete.put(Byte.TYPE.getName(), StdSerializers.IntLikeSerializer.instance);
        _concrete.put(Short.class.getName(), StdSerializers.IntLikeSerializer.instance);
        _concrete.put(Short.TYPE.getName(), StdSerializers.IntLikeSerializer.instance);
        _concrete.put(Float.class.getName(), StdSerializers.FloatSerializer.instance);
        _concrete.put(Float.TYPE.getName(), StdSerializers.FloatSerializer.instance);
        _concrete.put(Double.class.getName(), StdSerializers.DoubleSerializer.instance);
        _concrete.put(Double.TYPE.getName(), StdSerializers.DoubleSerializer.instance);
        StdSerializers.NumberSerializer numberSerializer = new StdSerializers.NumberSerializer();
        _concrete.put(BigInteger.class.getName(), numberSerializer);
        _concrete.put(BigDecimal.class.getName(), numberSerializer);
        _concrete.put(Calendar.class.getName(), StdSerializers.CalendarSerializer.instance);
        _concrete.put(Date.class.getName(), StdSerializers.UtilDateSerializer.instance);
        _concrete.put(java.sql.Date.class.getName(), new StdSerializers.SqlDateSerializer());
        _concrete.put(Time.class.getName(), new StdSerializers.SqlTimeSerializer());
        _concrete.put(Timestamp.class.getName(), StdSerializers.UtilDateSerializer.instance);
        _concrete.put(boolean[].class.getName(), new ArraySerializers.BooleanArraySerializer());
        _concrete.put(byte[].class.getName(), new ArraySerializers.ByteArraySerializer());
        _concrete.put(char[].class.getName(), new ArraySerializers.CharArraySerializer());
        _concrete.put(short[].class.getName(), new ArraySerializers.ShortArraySerializer());
        _concrete.put(int[].class.getName(), new ArraySerializers.IntArraySerializer());
        _concrete.put(long[].class.getName(), new ArraySerializers.LongArraySerializer());
        _concrete.put(float[].class.getName(), new ArraySerializers.FloatArraySerializer());
        _concrete.put(double[].class.getName(), new ArraySerializers.DoubleArraySerializer());
        _concrete.put(Object[].class.getName(), MARKER_OBJECT_ARRAY);
        _concrete.put(String[].class.getName(), MARKER_STRING_ARRAY);
        _concrete.put(ArrayList.class.getName(), MARKER_INDEXED_LIST);
        _concrete.put(Vector.class.getName(), MARKER_INDEXED_LIST);
        _concrete.put(LinkedList.class.getName(), MARKER_COLLECTION);
        _concrete.put(HashMap.class.getName(), MARKER_OBJECT_MAP);
        _concrete.put(Hashtable.class.getName(), MARKER_OBJECT_MAP);
        _concrete.put(LinkedHashMap.class.getName(), MARKER_OBJECT_MAP);
        _concrete.put(TreeMap.class.getName(), MARKER_OBJECT_MAP);
        _concrete.put(Properties.class.getName(), MARKER_OBJECT_MAP);
        _concrete.put(HashSet.class.getName(), MARKER_COLLECTION);
        _concrete.put(LinkedHashSet.class.getName(), MARKER_COLLECTION);
        _concrete.put(TreeSet.class.getName(), MARKER_COLLECTION);
        for (Map.Entry next : new JdkSerializers().provide()) {
            Object value = next.getValue();
            if (value instanceof JsonSerializer) {
                _concrete.put(((Class) next.getKey()).getName(), (JsonSerializer) value);
            } else if (value instanceof Class) {
                _concreteLazy.put(((Class) next.getKey()).getName(), (Class) value);
            } else {
                throw new IllegalStateException("Internal error: unrecognized value of type " + next.getClass().getName());
            }
        }
        _concreteLazy.put(TokenBuffer.class.getName(), StdSerializers.TokenBufferSerializer.class);
    }

    protected BasicSerializerFactory() {
    }

    public JsonSerializer<Object> createSerializer(SerializationConfig serializationConfig, JavaType javaType, BeanProperty beanProperty) {
        BasicBeanDescription basicBeanDescription = (BasicBeanDescription) serializationConfig.introspect(javaType);
        JsonSerializer<?> findSerializerFromAnnotation = findSerializerFromAnnotation(serializationConfig, basicBeanDescription.getClassInfo(), beanProperty);
        if (findSerializerFromAnnotation == null && (findSerializerFromAnnotation = findSerializerByLookup(javaType, serializationConfig, basicBeanDescription, beanProperty)) == null && (findSerializerFromAnnotation = findSerializerByPrimaryType(javaType, serializationConfig, basicBeanDescription, beanProperty)) == null) {
            return findSerializerByAddonType(serializationConfig, javaType, basicBeanDescription, beanProperty);
        }
        return findSerializerFromAnnotation;
    }

    public TypeSerializer createTypeSerializer(SerializationConfig serializationConfig, JavaType javaType, BeanProperty beanProperty) {
        Collection<NamedType> collectAndResolveSubtypes;
        TypeResolverBuilder<?> typeResolverBuilder;
        AnnotatedClass classInfo = ((BasicBeanDescription) serializationConfig.introspectClassAnnotations(javaType.getRawClass())).getClassInfo();
        AnnotationIntrospector annotationIntrospector = serializationConfig.getAnnotationIntrospector();
        TypeResolverBuilder<?> findTypeResolver = annotationIntrospector.findTypeResolver(classInfo, javaType);
        if (findTypeResolver == null) {
            typeResolverBuilder = serializationConfig.getDefaultTyper(javaType);
            collectAndResolveSubtypes = null;
        } else {
            collectAndResolveSubtypes = serializationConfig.getSubtypeResolver().collectAndResolveSubtypes(classInfo, serializationConfig, annotationIntrospector);
            typeResolverBuilder = findTypeResolver;
        }
        if (typeResolverBuilder == null) {
            return null;
        }
        return typeResolverBuilder.buildTypeSerializer(javaType, collectAndResolveSubtypes, beanProperty);
    }

    public final JsonSerializer<?> getNullSerializer() {
        return NullSerializer.instance;
    }

    public final JsonSerializer<?> findSerializerByLookup(JavaType javaType, SerializationConfig serializationConfig, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        Class cls;
        String name = javaType.getRawClass().getName();
        JsonSerializer<?> jsonSerializer = _concrete.get(name);
        if (jsonSerializer == null && (cls = _concreteLazy.get(name)) != null) {
            try {
                jsonSerializer = (JsonSerializer) cls.newInstance();
            } catch (Exception e) {
                throw new IllegalStateException("Failed to instantiate standard serializer (of type " + cls.getName() + "): " + e.getMessage(), e);
            }
        }
        if (jsonSerializer == null) {
            return this.optionalHandlers.findSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
        }
        if (jsonSerializer == MARKER_OBJECT_MAP) {
            return buildMapSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
        }
        if (jsonSerializer == MARKER_OBJECT_ARRAY) {
            return buildObjectArraySerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
        }
        if (jsonSerializer == MARKER_STRING_ARRAY) {
            return new ArraySerializers.StringArraySerializer(beanProperty);
        }
        if (jsonSerializer == MARKER_INDEXED_LIST) {
            if (javaType.getContentType().getRawClass() == String.class) {
                return new IndexedStringListSerializer(beanProperty);
            }
            return buildIndexedListSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
        } else if (jsonSerializer != MARKER_COLLECTION) {
            return jsonSerializer;
        } else {
            if (javaType.getContentType().getRawClass() == String.class) {
                return new StringCollectionSerializer(beanProperty);
            }
            return buildCollectionSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
        }
    }

    public final JsonSerializer<?> findSerializerByPrimaryType(JavaType javaType, SerializationConfig serializationConfig, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        Class<?> rawClass = javaType.getRawClass();
        if (JsonSerializable.class.isAssignableFrom(rawClass)) {
            if (JsonSerializableWithType.class.isAssignableFrom(rawClass)) {
                return StdSerializers.SerializableWithTypeSerializer.instance;
            }
            return StdSerializers.SerializableSerializer.instance;
        } else if (Map.class.isAssignableFrom(rawClass)) {
            if (EnumMap.class.isAssignableFrom(rawClass)) {
                return buildEnumMapSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
            }
            return buildMapSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
        } else if (Object[].class.isAssignableFrom(rawClass)) {
            return buildObjectArraySerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
        } else {
            if (!List.class.isAssignableFrom(rawClass)) {
                AnnotatedMethod findJsonValueMethod = basicBeanDescription.findJsonValueMethod();
                if (findJsonValueMethod != null) {
                    return new JsonValueSerializer(findJsonValueMethod.getAnnotated(), findSerializerFromAnnotation(serializationConfig, findJsonValueMethod, beanProperty), beanProperty);
                } else if (Number.class.isAssignableFrom(rawClass)) {
                    return StdSerializers.NumberSerializer.instance;
                } else {
                    if (Enum.class.isAssignableFrom(rawClass)) {
                        return EnumSerializer.construct(rawClass, serializationConfig, basicBeanDescription);
                    }
                    if (Calendar.class.isAssignableFrom(rawClass)) {
                        return StdSerializers.CalendarSerializer.instance;
                    }
                    if (Date.class.isAssignableFrom(rawClass)) {
                        return StdSerializers.UtilDateSerializer.instance;
                    }
                    if (!Collection.class.isAssignableFrom(rawClass)) {
                        return null;
                    }
                    if (EnumSet.class.isAssignableFrom(rawClass)) {
                        return buildEnumSetSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
                    }
                    return buildCollectionSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
                }
            } else if (rawClass == List.class || rawClass == AbstractList.class || RandomAccess.class.isAssignableFrom(rawClass)) {
                return buildIndexedListSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
            } else {
                return buildCollectionSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
            }
        }
    }

    public final JsonSerializer<?> findSerializerByAddonType(SerializationConfig serializationConfig, JavaType javaType, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        Class<?> rawClass = javaType.getRawClass();
        if (Iterator.class.isAssignableFrom(rawClass)) {
            return buildIteratorSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
        }
        if (Iterable.class.isAssignableFrom(rawClass)) {
            return buildIterableSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty);
        }
        if (CharSequence.class.isAssignableFrom(rawClass)) {
            return ToStringSerializer.instance;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public JsonSerializer<Object> findSerializerFromAnnotation(SerializationConfig serializationConfig, Annotated annotated, BeanProperty beanProperty) {
        Object findSerializer = serializationConfig.getAnnotationIntrospector().findSerializer(annotated, beanProperty);
        if (findSerializer == null) {
            return null;
        }
        if (findSerializer instanceof JsonSerializer) {
            return (JsonSerializer) findSerializer;
        }
        if (!(findSerializer instanceof Class)) {
            throw new IllegalStateException("AnnotationIntrospector returned value of type " + findSerializer.getClass().getName() + "; expected type JsonSerializer or Class<JsonSerializer> instead");
        }
        Class cls = (Class) findSerializer;
        if (JsonSerializer.class.isAssignableFrom(cls)) {
            return (JsonSerializer) ClassUtil.createInstance(cls, serializationConfig.isEnabled(SerializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS));
        }
        throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<JsonSerializer>");
    }

    /* access modifiers changed from: protected */
    public JsonSerializer<?> buildMapSerializer(SerializationConfig serializationConfig, JavaType javaType, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        AnnotationIntrospector annotationIntrospector = serializationConfig.getAnnotationIntrospector();
        TypeSerializer createTypeSerializer = createTypeSerializer(serializationConfig, javaType.getContentType(), beanProperty);
        return MapSerializer.construct(annotationIntrospector.findPropertiesToIgnore(basicBeanDescription.getClassInfo()), javaType, usesStaticTyping(serializationConfig, basicBeanDescription, createTypeSerializer), createTypeSerializer, beanProperty);
    }

    /* access modifiers changed from: protected */
    public JsonSerializer<?> buildEnumMapSerializer(SerializationConfig serializationConfig, JavaType javaType, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        EnumValues enumValues;
        JavaType keyType = javaType.getKeyType();
        JavaType contentType = javaType.getContentType();
        if (keyType.isEnumType()) {
            enumValues = EnumValues.construct(keyType.getRawClass(), serializationConfig.getAnnotationIntrospector());
        } else {
            enumValues = null;
        }
        TypeSerializer createTypeSerializer = createTypeSerializer(serializationConfig, contentType, beanProperty);
        return new EnumMapSerializer(contentType, usesStaticTyping(serializationConfig, basicBeanDescription, createTypeSerializer), enumValues, createTypeSerializer, beanProperty);
    }

    /* access modifiers changed from: protected */
    public JsonSerializer<?> buildObjectArraySerializer(SerializationConfig serializationConfig, JavaType javaType, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        JavaType contentType = javaType.getContentType();
        TypeSerializer createTypeSerializer = createTypeSerializer(serializationConfig, contentType, beanProperty);
        return new ObjectArraySerializer(contentType, usesStaticTyping(serializationConfig, basicBeanDescription, createTypeSerializer), createTypeSerializer, beanProperty);
    }

    /* access modifiers changed from: protected */
    public JsonSerializer<?> buildIndexedListSerializer(SerializationConfig serializationConfig, JavaType javaType, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        JavaType contentType = javaType.getContentType();
        TypeSerializer createTypeSerializer = createTypeSerializer(serializationConfig, contentType, beanProperty);
        return ContainerSerializers.indexedListSerializer(contentType, usesStaticTyping(serializationConfig, basicBeanDescription, createTypeSerializer), createTypeSerializer, beanProperty);
    }

    /* access modifiers changed from: protected */
    public JsonSerializer<?> buildCollectionSerializer(SerializationConfig serializationConfig, JavaType javaType, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        JavaType contentType = javaType.getContentType();
        TypeSerializer createTypeSerializer = createTypeSerializer(serializationConfig, contentType, beanProperty);
        return ContainerSerializers.collectionSerializer(contentType, usesStaticTyping(serializationConfig, basicBeanDescription, createTypeSerializer), createTypeSerializer, beanProperty);
    }

    /* access modifiers changed from: protected */
    public JsonSerializer<?> buildIteratorSerializer(SerializationConfig serializationConfig, JavaType javaType, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        JavaType containedType = javaType.containedType(0);
        if (containedType == null) {
            containedType = TypeFactory.type(Object.class);
        }
        TypeSerializer createTypeSerializer = createTypeSerializer(serializationConfig, containedType, beanProperty);
        return ContainerSerializers.iteratorSerializer(containedType, usesStaticTyping(serializationConfig, basicBeanDescription, createTypeSerializer), createTypeSerializer, beanProperty);
    }

    /* access modifiers changed from: protected */
    public JsonSerializer<?> buildIterableSerializer(SerializationConfig serializationConfig, JavaType javaType, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        JavaType containedType = javaType.containedType(0);
        if (containedType == null) {
            containedType = TypeFactory.type(Object.class);
        }
        TypeSerializer createTypeSerializer = createTypeSerializer(serializationConfig, containedType, beanProperty);
        return ContainerSerializers.iterableSerializer(containedType, usesStaticTyping(serializationConfig, basicBeanDescription, createTypeSerializer), createTypeSerializer, beanProperty);
    }

    /* access modifiers changed from: protected */
    public JsonSerializer<?> buildEnumSetSerializer(SerializationConfig serializationConfig, JavaType javaType, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        JavaType contentType = javaType.getContentType();
        if (!contentType.isEnumType()) {
            contentType = null;
        }
        return ContainerSerializers.enumSetSerializer(contentType, beanProperty);
    }

    /* access modifiers changed from: protected */
    public boolean usesStaticTyping(SerializationConfig serializationConfig, BasicBeanDescription basicBeanDescription, TypeSerializer typeSerializer) {
        if (typeSerializer != null) {
            return false;
        }
        JsonSerialize.Typing findSerializationTyping = serializationConfig.getAnnotationIntrospector().findSerializationTyping(basicBeanDescription.getClassInfo());
        if (findSerializationTyping != null) {
            return findSerializationTyping == JsonSerialize.Typing.STATIC;
        }
        return serializationConfig.isEnabled(SerializationConfig.Feature.USE_STATIC_TYPING);
    }
}
