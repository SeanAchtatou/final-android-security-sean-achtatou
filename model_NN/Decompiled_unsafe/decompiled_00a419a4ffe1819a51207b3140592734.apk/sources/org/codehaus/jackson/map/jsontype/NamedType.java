package org.codehaus.jackson.map.jsontype;

public final class NamedType {
    protected final Class<?> _class;
    protected final int _hashCode;
    protected String _name;

    public NamedType(Class<?> cls) {
        this(cls, null);
    }

    public NamedType(Class<?> cls, String str) {
        this._class = cls;
        this._hashCode = cls.getName().hashCode();
        setName(str);
    }

    public final Class<?> getType() {
        return this._class;
    }

    public final String getName() {
        return this._name;
    }

    public final void setName(String str) {
        String str2;
        if (str == null || str.length() == 0) {
            str2 = null;
        } else {
            str2 = str;
        }
        this._name = str2;
    }

    public final boolean hasName() {
        return this._name != null;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return this._class == ((NamedType) obj)._class;
    }

    public final int hashCode() {
        return this._hashCode;
    }

    public final String toString() {
        return "[NamedType, class " + this._class.getName() + ", name: " + (this._name == null ? "null" : "'" + this._name + "'") + "]";
    }
}
