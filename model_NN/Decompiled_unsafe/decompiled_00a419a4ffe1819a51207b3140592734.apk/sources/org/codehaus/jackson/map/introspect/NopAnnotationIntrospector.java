package org.codehaus.jackson.map.introspect;

import java.lang.annotation.Annotation;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.KeyDeserializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.JavaType;

public class NopAnnotationIntrospector extends AnnotationIntrospector {
    public static final NopAnnotationIntrospector instance = new NopAnnotationIntrospector();

    public boolean isHandled(Annotation annotation) {
        return false;
    }

    public String findEnumValue(Enum<?> enumR) {
        return null;
    }

    public Boolean findCachability(AnnotatedClass annotatedClass) {
        return null;
    }

    public String findRootName(AnnotatedClass annotatedClass) {
        return null;
    }

    public String[] findPropertiesToIgnore(AnnotatedClass annotatedClass) {
        return null;
    }

    public Boolean findIgnoreUnknownProperties(AnnotatedClass annotatedClass) {
        return null;
    }

    public VisibilityChecker<?> findAutoDetectVisibility(AnnotatedClass annotatedClass, VisibilityChecker<?> visibilityChecker) {
        return visibilityChecker;
    }

    public boolean isIgnorableConstructor(AnnotatedConstructor annotatedConstructor) {
        return false;
    }

    public boolean isIgnorableMethod(AnnotatedMethod annotatedMethod) {
        return false;
    }

    public boolean isIgnorableField(AnnotatedField annotatedField) {
        return false;
    }

    public Object findSerializer(Annotated annotated, BeanProperty beanProperty) {
        return null;
    }

    public JsonSerialize.Inclusion findSerializationInclusion(Annotated annotated, JsonSerialize.Inclusion inclusion) {
        return JsonSerialize.Inclusion.ALWAYS;
    }

    public Class<?> findSerializationType(Annotated annotated) {
        return null;
    }

    public JsonSerialize.Typing findSerializationTyping(Annotated annotated) {
        return null;
    }

    public Class<?>[] findSerializationViews(Annotated annotated) {
        return null;
    }

    public String[] findSerializationPropertyOrder(AnnotatedClass annotatedClass) {
        return null;
    }

    public Boolean findSerializationSortAlphabetically(AnnotatedClass annotatedClass) {
        return null;
    }

    public String findGettablePropertyName(AnnotatedMethod annotatedMethod) {
        return null;
    }

    public boolean hasAsValueAnnotation(AnnotatedMethod annotatedMethod) {
        return false;
    }

    public String findDeserializablePropertyName(AnnotatedField annotatedField) {
        return null;
    }

    public Class<?> findDeserializationContentType(Annotated annotated, JavaType javaType, String str) {
        return null;
    }

    public Class<?> findDeserializationKeyType(Annotated annotated, JavaType javaType, String str) {
        return null;
    }

    public Class<?> findDeserializationType(Annotated annotated, JavaType javaType, String str) {
        return null;
    }

    public Object findDeserializer(Annotated annotated, BeanProperty beanProperty) {
        return null;
    }

    public Class<KeyDeserializer> findKeyDeserializer(Annotated annotated) {
        return null;
    }

    public Class<JsonDeserializer<?>> findContentDeserializer(Annotated annotated) {
        return null;
    }

    public String findPropertyNameForParam(AnnotatedParameter annotatedParameter) {
        return null;
    }

    public String findSerializablePropertyName(AnnotatedField annotatedField) {
        return null;
    }

    public String findSettablePropertyName(AnnotatedMethod annotatedMethod) {
        return null;
    }
}
