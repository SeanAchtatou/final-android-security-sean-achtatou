package org.codehaus.jackson.map.type;

import org.codehaus.jackson.type.JavaType;

public final class CollectionType extends TypeBase {
    final JavaType _elementType;

    private CollectionType(Class<?> cls, JavaType javaType) {
        super(cls, javaType.hashCode());
        this._elementType = javaType;
    }

    /* access modifiers changed from: protected */
    public final JavaType _narrow(Class<?> cls) {
        return new CollectionType(cls, this._elementType);
    }

    public final JavaType narrowContentsBy(Class<?> cls) {
        if (cls == this._elementType.getRawClass()) {
            return this;
        }
        return new CollectionType(this._class, this._elementType.narrowBy(cls)).copyHandlers(this);
    }

    public static CollectionType construct(Class<?> cls, JavaType javaType) {
        return new CollectionType(cls, javaType);
    }

    public final CollectionType withTypeHandler(Object obj) {
        CollectionType collectionType = new CollectionType(this._class, this._elementType);
        collectionType._typeHandler = obj;
        return collectionType;
    }

    public final CollectionType withContentTypeHandler(Object obj) {
        return new CollectionType(this._class, this._elementType.withTypeHandler(obj));
    }

    /* access modifiers changed from: protected */
    public final String buildCanonicalName() {
        StringBuilder sb = new StringBuilder();
        sb.append(this._class.getName());
        if (this._elementType != null) {
            sb.append('<');
            sb.append(this._elementType.toCanonical());
            sb.append('>');
        }
        return sb.toString();
    }

    public final JavaType getContentType() {
        return this._elementType;
    }

    public final int containedTypeCount() {
        return 1;
    }

    public final JavaType containedType(int i) {
        if (i == 0) {
            return this._elementType;
        }
        return null;
    }

    public final String containedTypeName(int i) {
        if (i == 0) {
            return "E";
        }
        return null;
    }

    public final StringBuilder getErasedSignature(StringBuilder sb) {
        return _classSignature(this._class, sb, true);
    }

    public final StringBuilder getGenericSignature(StringBuilder sb) {
        _classSignature(this._class, sb, false);
        sb.append('<');
        this._elementType.getGenericSignature(sb);
        sb.append(">;");
        return sb;
    }

    public final boolean isContainerType() {
        return true;
    }

    public final String toString() {
        return "[collection type; class " + this._class.getName() + ", contains " + this._elementType + "]";
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        CollectionType collectionType = (CollectionType) obj;
        return this._class == collectionType._class && this._elementType.equals(collectionType._elementType);
    }
}
