package org.codehaus.jackson.map;

import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.Base64Variants;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.NopAnnotationIntrospector;
import org.codehaus.jackson.map.introspect.VisibilityChecker;
import org.codehaus.jackson.map.jsontype.SubtypeResolver;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.map.jsontype.impl.StdSubtypeResolver;
import org.codehaus.jackson.map.type.ClassKey;
import org.codehaus.jackson.map.util.LinkedNode;
import org.codehaus.jackson.map.util.StdDateFormat;
import org.codehaus.jackson.node.JsonNodeFactory;
import org.codehaus.jackson.type.JavaType;

public class DeserializationConfig implements MapperConfig<DeserializationConfig> {
    protected static final DateFormat DEFAULT_DATE_FORMAT = StdDateFormat.instance;
    protected static final int DEFAULT_FEATURE_FLAGS = Feature.collectDefaults();
    protected AbstractTypeResolver _abstractTypeResolver;
    protected AnnotationIntrospector _annotationIntrospector;
    protected ClassIntrospector<? extends BeanDescription> _classIntrospector;
    protected DateFormat _dateFormat = DEFAULT_DATE_FORMAT;
    protected int _featureFlags = DEFAULT_FEATURE_FLAGS;
    protected HashMap<ClassKey, Class<?>> _mixInAnnotations;
    protected boolean _mixInAnnotationsShared;
    protected JsonNodeFactory _nodeFactory;
    protected LinkedNode<DeserializationProblemHandler> _problemHandlers;
    protected SubtypeResolver _subtypeResolver;
    protected final TypeResolverBuilder<?> _typer;
    protected VisibilityChecker<?> _visibilityChecker;

    public enum Feature {
        USE_ANNOTATIONS(true),
        AUTO_DETECT_SETTERS(true),
        AUTO_DETECT_CREATORS(true),
        AUTO_DETECT_FIELDS(true),
        USE_GETTERS_AS_SETTERS(true),
        CAN_OVERRIDE_ACCESS_MODIFIERS(true),
        USE_BIG_DECIMAL_FOR_FLOATS(false),
        USE_BIG_INTEGER_FOR_INTS(false),
        READ_ENUMS_USING_TO_STRING(false),
        FAIL_ON_UNKNOWN_PROPERTIES(true),
        FAIL_ON_NULL_FOR_PRIMITIVES(false),
        FAIL_ON_NUMBERS_FOR_ENUMS(false),
        WRAP_EXCEPTIONS(true),
        WRAP_ROOT_VALUE(false);
        
        final boolean _defaultState;

        public static int collectDefaults() {
            int i = 0;
            for (Feature feature : values()) {
                if (feature.enabledByDefault()) {
                    i |= feature.getMask();
                }
            }
            return i;
        }

        private Feature(boolean z) {
            this._defaultState = z;
        }

        public final boolean enabledByDefault() {
            return this._defaultState;
        }

        public final int getMask() {
            return 1 << ordinal();
        }
    }

    public DeserializationConfig(ClassIntrospector<? extends BeanDescription> classIntrospector, AnnotationIntrospector annotationIntrospector, VisibilityChecker<?> visibilityChecker, SubtypeResolver subtypeResolver) {
        this._classIntrospector = classIntrospector;
        this._annotationIntrospector = annotationIntrospector;
        this._typer = null;
        this._visibilityChecker = visibilityChecker;
        this._subtypeResolver = subtypeResolver;
        this._nodeFactory = JsonNodeFactory.instance;
    }

    protected DeserializationConfig(DeserializationConfig deserializationConfig, HashMap<ClassKey, Class<?>> hashMap, TypeResolverBuilder<?> typeResolverBuilder, VisibilityChecker<?> visibilityChecker, SubtypeResolver subtypeResolver) {
        this._classIntrospector = deserializationConfig._classIntrospector;
        this._annotationIntrospector = deserializationConfig._annotationIntrospector;
        this._abstractTypeResolver = deserializationConfig._abstractTypeResolver;
        this._featureFlags = deserializationConfig._featureFlags;
        this._problemHandlers = deserializationConfig._problemHandlers;
        this._dateFormat = deserializationConfig._dateFormat;
        this._nodeFactory = deserializationConfig._nodeFactory;
        this._mixInAnnotations = hashMap;
        this._typer = typeResolverBuilder;
        this._visibilityChecker = visibilityChecker;
        this._subtypeResolver = subtypeResolver;
    }

    public void enable(Feature feature) {
        this._featureFlags |= feature.getMask();
    }

    public void disable(Feature feature) {
        this._featureFlags &= feature.getMask() ^ -1;
    }

    public void set(Feature feature, boolean z) {
        if (z) {
            enable(feature);
        } else {
            disable(feature);
        }
    }

    public final boolean isEnabled(Feature feature) {
        return (this._featureFlags & feature.getMask()) != 0;
    }

    public void fromAnnotations(Class<?> cls) {
        this._visibilityChecker = this._annotationIntrospector.findAutoDetectVisibility(AnnotatedClass.construct(cls, this._annotationIntrospector, null), this._visibilityChecker);
    }

    public DeserializationConfig createUnshared(TypeResolverBuilder<?> typeResolverBuilder, VisibilityChecker<?> visibilityChecker, SubtypeResolver subtypeResolver) {
        HashMap<ClassKey, Class<?>> hashMap = this._mixInAnnotations;
        this._mixInAnnotationsShared = true;
        return new DeserializationConfig(this, hashMap, typeResolverBuilder, visibilityChecker, subtypeResolver);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.DeserializationConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver):org.codehaus.jackson.map.DeserializationConfig
     arg types: [org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver]
     candidates:
      org.codehaus.jackson.map.DeserializationConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder, org.codehaus.jackson.map.introspect.VisibilityChecker, org.codehaus.jackson.map.jsontype.SubtypeResolver):org.codehaus.jackson.map.MapperConfig
      org.codehaus.jackson.map.MapperConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver):T
      org.codehaus.jackson.map.DeserializationConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver):org.codehaus.jackson.map.DeserializationConfig */
    public DeserializationConfig createUnshared(JsonNodeFactory jsonNodeFactory) {
        DeserializationConfig createUnshared = createUnshared(this._typer, this._visibilityChecker, this._subtypeResolver);
        createUnshared.setNodeFactory(jsonNodeFactory);
        return createUnshared;
    }

    public void setIntrospector(ClassIntrospector<? extends BeanDescription> classIntrospector) {
        this._classIntrospector = classIntrospector;
    }

    public AnnotationIntrospector getAnnotationIntrospector() {
        if (isEnabled(Feature.USE_ANNOTATIONS)) {
            return this._annotationIntrospector;
        }
        return NopAnnotationIntrospector.instance;
    }

    public void setAnnotationIntrospector(AnnotationIntrospector annotationIntrospector) {
        this._annotationIntrospector = annotationIntrospector;
    }

    public void insertAnnotationIntrospector(AnnotationIntrospector annotationIntrospector) {
        this._annotationIntrospector = AnnotationIntrospector.Pair.create(annotationIntrospector, this._annotationIntrospector);
    }

    public void appendAnnotationIntrospector(AnnotationIntrospector annotationIntrospector) {
        this._annotationIntrospector = AnnotationIntrospector.Pair.create(this._annotationIntrospector, annotationIntrospector);
    }

    public void setMixInAnnotations(Map<Class<?>, Class<?>> map) {
        HashMap<ClassKey, Class<?>> hashMap = null;
        if (map != null && map.size() > 0) {
            HashMap<ClassKey, Class<?>> hashMap2 = new HashMap<>(map.size());
            for (Map.Entry next : map.entrySet()) {
                hashMap2.put(new ClassKey((Class) next.getKey()), next.getValue());
            }
            hashMap = hashMap2;
        }
        this._mixInAnnotationsShared = false;
        this._mixInAnnotations = hashMap;
    }

    public void addMixInAnnotations(Class<?> cls, Class<?> cls2) {
        if (this._mixInAnnotations == null || this._mixInAnnotationsShared) {
            this._mixInAnnotationsShared = false;
            this._mixInAnnotations = new HashMap<>();
        }
        this._mixInAnnotations.put(new ClassKey(cls), cls2);
    }

    public Class<?> findMixInClassFor(Class<?> cls) {
        if (this._mixInAnnotations == null) {
            return null;
        }
        return this._mixInAnnotations.get(new ClassKey(cls));
    }

    public DateFormat getDateFormat() {
        return this._dateFormat;
    }

    public void setDateFormat(DateFormat dateFormat) {
        DateFormat dateFormat2;
        if (dateFormat == null) {
            dateFormat2 = StdDateFormat.instance;
        } else {
            dateFormat2 = dateFormat;
        }
        this._dateFormat = dateFormat2;
    }

    public VisibilityChecker<?> getDefaultVisibilityChecker() {
        return this._visibilityChecker;
    }

    public TypeResolverBuilder<?> getDefaultTyper(JavaType javaType) {
        return this._typer;
    }

    public SubtypeResolver getSubtypeResolver() {
        if (this._subtypeResolver == null) {
            this._subtypeResolver = new StdSubtypeResolver();
        }
        return this._subtypeResolver;
    }

    public void setSubtypeResolver(SubtypeResolver subtypeResolver) {
        this._subtypeResolver = subtypeResolver;
    }

    public <T extends BeanDescription> T introspectClassAnnotations(Class<?> cls) {
        return this._classIntrospector.forClassAnnotations(this, cls, this);
    }

    public <T extends BeanDescription> T introspectDirectClassAnnotations(Class<?> cls) {
        return this._classIntrospector.forDirectClassAnnotations(this, cls, this);
    }

    public LinkedNode<DeserializationProblemHandler> getProblemHandlers() {
        return this._problemHandlers;
    }

    public void addHandler(DeserializationProblemHandler deserializationProblemHandler) {
        if (!LinkedNode.contains(this._problemHandlers, deserializationProblemHandler)) {
            this._problemHandlers = new LinkedNode<>(deserializationProblemHandler, this._problemHandlers);
        }
    }

    public void clearHandlers() {
        this._problemHandlers = null;
    }

    public <T extends BeanDescription> T introspect(JavaType javaType) {
        return this._classIntrospector.forDeserialization(this, javaType, this);
    }

    public <T extends BeanDescription> T introspectForCreation(JavaType javaType) {
        return this._classIntrospector.forCreation(this, javaType, this);
    }

    public AbstractTypeResolver getAbstractTypeResolver() {
        return this._abstractTypeResolver;
    }

    public void setAbstractTypeResolver(AbstractTypeResolver abstractTypeResolver) {
        this._abstractTypeResolver = abstractTypeResolver;
    }

    public Base64Variant getBase64Variant() {
        return Base64Variants.getDefaultVariant();
    }

    public void setNodeFactory(JsonNodeFactory jsonNodeFactory) {
        this._nodeFactory = jsonNodeFactory;
    }

    public final JsonNodeFactory getNodeFactory() {
        return this._nodeFactory;
    }
}
