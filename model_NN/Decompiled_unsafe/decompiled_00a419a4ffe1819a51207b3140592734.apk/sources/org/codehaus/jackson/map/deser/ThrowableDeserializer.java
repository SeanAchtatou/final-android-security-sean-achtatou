package org.codehaus.jackson.map.deser;

import java.io.IOException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonMappingException;

public class ThrowableDeserializer extends BeanDeserializer {
    protected static final String PROP_NAME_MESSAGE = "message";

    public ThrowableDeserializer(BeanDeserializer beanDeserializer) {
        super(beanDeserializer);
    }

    public Object deserializeFromObject(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Object[] objArr;
        Object obj;
        int i;
        if (this._propertyBasedCreator != null) {
            return _deserializeUsingPropertyBased(jsonParser, deserializationContext);
        }
        if (this._delegatingCreator != null) {
            return this._delegatingCreator.deserialize(jsonParser, deserializationContext);
        }
        if (this._beanType.isAbstract()) {
            throw JsonMappingException.from(jsonParser, "Can not instantiate abstract type " + this._beanType + " (need to add/enable type information?)");
        } else if (this._stringCreator == null) {
            throw new JsonMappingException("Can not deserialize Throwable of type " + this._beanType + " without having either single-String-arg constructor; or explicit @JsonCreator");
        } else {
            int i2 = 0;
            Object[] objArr2 = null;
            Object obj2 = null;
            while (jsonParser.getCurrentToken() != JsonToken.END_OBJECT) {
                String currentName = jsonParser.getCurrentName();
                SettableBeanProperty find = this._beanProperties.find(currentName);
                jsonParser.nextToken();
                if (find != null) {
                    if (obj2 != null) {
                        find.deserializeAndSet(jsonParser, deserializationContext, obj2);
                        int i3 = i2;
                        objArr = objArr2;
                        obj = obj2;
                        i = i3;
                    } else {
                        if (objArr2 == null) {
                            int size = this._beanProperties.size();
                            objArr2 = new Object[(size + size)];
                        }
                        int i4 = i2 + 1;
                        objArr2[i2] = find;
                        objArr2[i4] = find.deserialize(jsonParser, deserializationContext);
                        objArr = objArr2;
                        obj = obj2;
                        i = i4 + 1;
                    }
                } else if (PROP_NAME_MESSAGE.equals(currentName)) {
                    Object construct = this._stringCreator.construct(jsonParser.getText());
                    if (objArr2 != null) {
                        for (int i5 = 0; i5 < i2; i5 += 2) {
                            ((SettableBeanProperty) objArr2[i5]).set(construct, objArr2[i5 + 1]);
                        }
                        i = i2;
                        obj = construct;
                        objArr = null;
                    } else {
                        i = i2;
                        objArr = objArr2;
                        obj = construct;
                    }
                } else if (this._ignorableProps != null && this._ignorableProps.contains(currentName)) {
                    jsonParser.skipChildren();
                    int i6 = i2;
                    objArr = objArr2;
                    obj = obj2;
                    i = i6;
                } else if (this._anySetter != null) {
                    this._anySetter.deserializeAndSet(jsonParser, deserializationContext, obj2, currentName);
                    int i7 = i2;
                    objArr = objArr2;
                    obj = obj2;
                    i = i7;
                } else {
                    handleUnknownProperty(jsonParser, deserializationContext, obj2, currentName);
                    int i8 = i2;
                    objArr = objArr2;
                    obj = obj2;
                    i = i8;
                }
                jsonParser.nextToken();
                int i9 = i;
                obj2 = obj;
                objArr2 = objArr;
                i2 = i9;
            }
            if (obj2 != null) {
                return obj2;
            }
            Object construct2 = this._stringCreator.construct(null);
            if (objArr2 == null) {
                return construct2;
            }
            for (int i10 = 0; i10 < i2; i10 += 2) {
                ((SettableBeanProperty) objArr2[i10]).set(construct2, objArr2[i10 + 1]);
            }
            return construct2;
        }
    }
}
