package org.codehaus.jackson.map.deser.impl;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Collection;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.TypeDeserializer;
import org.codehaus.jackson.map.annotate.JacksonStdImpl;
import org.codehaus.jackson.map.deser.ContainerDeserializer;
import org.codehaus.jackson.type.JavaType;

@JacksonStdImpl
public final class StringCollectionDeserializer extends ContainerDeserializer<Collection<String>> {
    protected final JavaType _collectionType;
    final Constructor<Collection<String>> _defaultCtor;
    protected final boolean _isDefaultDeserializer;
    protected final JsonDeserializer<String> _valueDeserializer;

    public final /* bridge */ /* synthetic */ Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Object obj) throws IOException, JsonProcessingException {
        return deserialize(jsonParser, deserializationContext, (Collection<String>) ((Collection) obj));
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [org.codehaus.jackson.map.JsonDeserializer<?>, org.codehaus.jackson.map.JsonDeserializer, org.codehaus.jackson.map.JsonDeserializer<java.lang.String>] */
    /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.reflect.Constructor<java.util.Collection<java.lang.String>>, java.lang.reflect.Constructor<?>] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public StringCollectionDeserializer(org.codehaus.jackson.type.JavaType r2, org.codehaus.jackson.map.JsonDeserializer<?> r3, java.lang.reflect.Constructor<?> r4) {
        /*
            r1 = this;
            java.lang.Class r0 = r2.getRawClass()
            r1.<init>(r0)
            r1._collectionType = r2
            r1._valueDeserializer = r3
            r1._defaultCtor = r4
            boolean r0 = r1.isDefaultSerializer(r3)
            r1._isDefaultDeserializer = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.deser.impl.StringCollectionDeserializer.<init>(org.codehaus.jackson.type.JavaType, org.codehaus.jackson.map.JsonDeserializer, java.lang.reflect.Constructor):void");
    }

    public final JavaType getContentType() {
        return this._collectionType.getContentType();
    }

    public final JsonDeserializer<Object> getContentDeserializer() {
        return this._valueDeserializer;
    }

    public final Collection<String> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        if (!jsonParser.isExpectedStartArrayToken()) {
            throw deserializationContext.mappingException(this._collectionType.getRawClass());
        }
        try {
            return deserialize(jsonParser, deserializationContext, (Collection<String>) this._defaultCtor.newInstance(new Object[0]));
        } catch (Exception e) {
            throw deserializationContext.instantiationException(this._collectionType.getRawClass(), e);
        }
    }

    public final Collection<String> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Collection<String> collection) throws IOException, JsonProcessingException {
        if (!this._isDefaultDeserializer) {
            return deserializeUsingCustom(jsonParser, deserializationContext, collection);
        }
        while (true) {
            JsonToken nextToken = jsonParser.nextToken();
            if (nextToken == JsonToken.END_ARRAY) {
                return collection;
            }
            collection.add(nextToken == JsonToken.VALUE_NULL ? null : jsonParser.getText());
        }
    }

    private Collection<String> deserializeUsingCustom(JsonParser jsonParser, DeserializationContext deserializationContext, Collection<String> collection) throws IOException, JsonProcessingException {
        String deserialize;
        JsonDeserializer<String> jsonDeserializer = this._valueDeserializer;
        while (true) {
            JsonToken nextToken = jsonParser.nextToken();
            if (nextToken == JsonToken.END_ARRAY) {
                return collection;
            }
            if (nextToken == JsonToken.VALUE_NULL) {
                deserialize = null;
            } else {
                deserialize = jsonDeserializer.deserialize(jsonParser, deserializationContext);
            }
            collection.add(deserialize);
        }
    }

    public final Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, TypeDeserializer typeDeserializer) throws IOException, JsonProcessingException {
        return typeDeserializer.deserializeTypedFromArray(jsonParser, deserializationContext);
    }
}
