package org.codehaus.jackson.map.util;

import java.io.IOException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.JsonSerializableWithType;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.type.JavaType;

public class JSONWrappedObject implements JsonSerializableWithType {
    protected final String _prefix;
    protected final JavaType _serializationType;
    protected final String _suffix;
    protected final Object _value;

    public JSONWrappedObject(String str, String str2, Object obj) {
        this(str, str2, obj, (JavaType) null);
    }

    public JSONWrappedObject(String str, String str2, Object obj, JavaType javaType) {
        this._prefix = str;
        this._suffix = str2;
        this._value = obj;
        this._serializationType = javaType;
    }

    public JSONWrappedObject(String str, String str2, Object obj, Class<?> cls) {
        this._prefix = str;
        this._suffix = str2;
        this._value = obj;
        this._serializationType = cls == null ? null : TypeFactory.type(cls);
    }

    public void serializeWithType(JsonGenerator jsonGenerator, SerializerProvider serializerProvider, TypeSerializer typeSerializer) throws IOException, JsonProcessingException {
        serialize(jsonGenerator, serializerProvider);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(org.codehaus.jackson.type.JavaType, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object>
     arg types: [org.codehaus.jackson.type.JavaType, int, ?[OBJECT, ARRAY]]
     candidates:
      org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(java.lang.Class<?>, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object>
      org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(org.codehaus.jackson.type.JavaType, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(java.lang.Class<?>, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object>
     arg types: [java.lang.Class<?>, int, ?[OBJECT, ARRAY]]
     candidates:
      org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(org.codehaus.jackson.type.JavaType, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object>
      org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(java.lang.Class<?>, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object> */
    public void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        if (this._prefix != null) {
            jsonGenerator.writeRaw(this._prefix);
        }
        if (this._value == null) {
            serializerProvider.defaultSerializeNull(jsonGenerator);
        } else if (this._serializationType != null) {
            serializerProvider.findTypedValueSerializer(this._serializationType, true, (BeanProperty) null).serialize(this._value, jsonGenerator, serializerProvider);
        } else {
            serializerProvider.findTypedValueSerializer(this._value.getClass(), true, (BeanProperty) null).serialize(this._value, jsonGenerator, serializerProvider);
        }
        if (this._suffix != null) {
            jsonGenerator.writeRaw(this._suffix);
        }
    }

    public String getPrefix() {
        return this._prefix;
    }

    public String getSuffix() {
        return this._suffix;
    }

    public Object getValue() {
        return this._value;
    }

    public JavaType getSerializationType() {
        return this._serializationType;
    }
}
