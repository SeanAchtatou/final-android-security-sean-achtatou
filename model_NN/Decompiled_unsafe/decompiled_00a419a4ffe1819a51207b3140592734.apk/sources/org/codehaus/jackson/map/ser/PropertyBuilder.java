package org.codehaus.jackson.map.ser;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.codehaus.jackson.impl.JsonWriteContext;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.TypeSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.introspect.Annotated;
import org.codehaus.jackson.map.introspect.AnnotatedField;
import org.codehaus.jackson.map.introspect.AnnotatedMember;
import org.codehaus.jackson.map.introspect.AnnotatedMethod;
import org.codehaus.jackson.map.introspect.BasicBeanDescription;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.map.util.Annotations;
import org.codehaus.jackson.type.JavaType;

public class PropertyBuilder {
    protected final AnnotationIntrospector _annotationIntrospector = this._config.getAnnotationIntrospector();
    protected final BasicBeanDescription _beanDesc;
    protected final SerializationConfig _config;
    protected Object _defaultBean;
    protected final JsonSerialize.Inclusion _outputProps;

    public PropertyBuilder(SerializationConfig serializationConfig, BasicBeanDescription basicBeanDescription) {
        this._config = serializationConfig;
        this._beanDesc = basicBeanDescription;
        this._outputProps = basicBeanDescription.findSerializationInclusion(serializationConfig.getSerializationInclusion());
    }

    public Annotations getClassAnnotations() {
        return this._beanDesc.getClassAnnotations();
    }

    /* access modifiers changed from: protected */
    public BeanPropertyWriter buildWriter(String str, JavaType javaType, JsonSerializer<Object> jsonSerializer, TypeSerializer typeSerializer, TypeSerializer typeSerializer2, AnnotatedMember annotatedMember, boolean z) {
        Method annotated;
        Field field;
        Object obj;
        boolean z2;
        if (annotatedMember instanceof AnnotatedField) {
            annotated = null;
            field = ((AnnotatedField) annotatedMember).getAnnotated();
        } else {
            annotated = ((AnnotatedMethod) annotatedMember).getAnnotated();
            field = null;
        }
        JavaType findSerializationType = findSerializationType(annotatedMember, z);
        if (typeSerializer2 != null) {
            if (findSerializationType == null) {
                findSerializationType = javaType;
            }
            if (findSerializationType.getContentType() == null) {
                throw new IllegalStateException("Problem trying to create BeanPropertyWriter for property '" + str + "' (of type " + this._beanDesc.getType() + "); serialization type " + findSerializationType + " has no content");
            }
            findSerializationType = findSerializationType.withContentTypeHandler(typeSerializer2);
            findSerializationType.getContentType();
        }
        JavaType javaType2 = findSerializationType;
        Object obj2 = null;
        JsonSerialize.Inclusion findSerializationInclusion = this._annotationIntrospector.findSerializationInclusion(annotatedMember, this._outputProps);
        if (findSerializationInclusion != null) {
            switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$map$annotate$JsonSerialize$Inclusion[findSerializationInclusion.ordinal()]) {
                case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                    obj2 = getDefaultValue(str, annotated, field);
                    if (obj2 != null) {
                        obj = obj2;
                        z2 = false;
                        break;
                    }
                case JsonWriteContext.STATUS_OK_AFTER_COLON:
                    obj = obj2;
                    z2 = true;
                    break;
            }
            return new BeanPropertyWriter(annotatedMember, this._beanDesc.getClassAnnotations(), str, javaType, jsonSerializer, typeSerializer, javaType2, annotated, field, z2, obj);
        }
        obj = null;
        z2 = false;
        return new BeanPropertyWriter(annotatedMember, this._beanDesc.getClassAnnotations(), str, javaType, jsonSerializer, typeSerializer, javaType2, annotated, field, z2, obj);
    }

    /* renamed from: org.codehaus.jackson.map.ser.PropertyBuilder$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$codehaus$jackson$map$annotate$JsonSerialize$Inclusion = new int[JsonSerialize.Inclusion.values().length];

        static {
            try {
                $SwitchMap$org$codehaus$jackson$map$annotate$JsonSerialize$Inclusion[JsonSerialize.Inclusion.NON_DEFAULT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$map$annotate$JsonSerialize$Inclusion[JsonSerialize.Inclusion.NON_NULL.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public JavaType findSerializationType(Annotated annotated, boolean z) {
        boolean z2;
        Class<?> findSerializationType = this._annotationIntrospector.findSerializationType(annotated);
        if (findSerializationType != null) {
            Class<?> rawType = annotated.getRawType();
            if (findSerializationType.isAssignableFrom(rawType)) {
                return TypeFactory.type(findSerializationType);
            }
            if (rawType.isAssignableFrom(findSerializationType)) {
                return TypeFactory.type(findSerializationType);
            }
            throw new IllegalArgumentException("Illegal concrete-type annotation for method '" + annotated.getName() + "': class " + findSerializationType.getName() + " not a super-type of (declared) class " + rawType.getName());
        }
        JsonSerialize.Typing findSerializationTyping = this._annotationIntrospector.findSerializationTyping(annotated);
        if (findSerializationTyping != null) {
            z2 = findSerializationTyping == JsonSerialize.Typing.STATIC;
        } else {
            z2 = z;
        }
        if (z2) {
            return TypeFactory.type(annotated.getGenericType(), this._beanDesc.getType());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Object getDefaultBean() {
        if (this._defaultBean == null) {
            this._defaultBean = this._beanDesc.instantiateBean(this._config.isEnabled(SerializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS));
            if (this._defaultBean == null) {
                throw new IllegalArgumentException("Class " + this._beanDesc.getClassInfo().getAnnotated().getName() + " has no default constructor; can not instantiate default bean value to support 'properties=JsonSerialize.Inclusion.NON_DEFAULT' annotation");
            }
        }
        return this._defaultBean;
    }

    /* access modifiers changed from: protected */
    public Object getDefaultValue(String str, Method method, Field field) {
        Object defaultBean = getDefaultBean();
        if (method == null) {
            return field.get(defaultBean);
        }
        try {
            return method.invoke(defaultBean, new Object[0]);
        } catch (Exception e) {
            return _throwWrapped(e, str, defaultBean);
        }
    }

    /* access modifiers changed from: protected */
    public Object _throwWrapped(Exception exc, String str, Object obj) {
        Throwable th = exc;
        while (th.getCause() != null) {
            th = th.getCause();
        }
        if (th instanceof Error) {
            throw ((Error) th);
        } else if (th instanceof RuntimeException) {
            throw ((RuntimeException) th);
        } else {
            throw new IllegalArgumentException("Failed to get property '" + str + "' of default " + obj.getClass().getName() + " instance");
        }
    }
}
