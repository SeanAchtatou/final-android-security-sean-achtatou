package org.codehaus.jackson.map;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.KeyDeserializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.introspect.Annotated;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.AnnotatedConstructor;
import org.codehaus.jackson.map.introspect.AnnotatedField;
import org.codehaus.jackson.map.introspect.AnnotatedMember;
import org.codehaus.jackson.map.introspect.AnnotatedMethod;
import org.codehaus.jackson.map.introspect.AnnotatedParameter;
import org.codehaus.jackson.map.introspect.NopAnnotationIntrospector;
import org.codehaus.jackson.map.introspect.VisibilityChecker;
import org.codehaus.jackson.map.jsontype.NamedType;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.type.JavaType;

public abstract class AnnotationIntrospector {
    public abstract VisibilityChecker<?> findAutoDetectVisibility(AnnotatedClass annotatedClass, VisibilityChecker<?> visibilityChecker);

    public abstract Boolean findCachability(AnnotatedClass annotatedClass);

    public abstract Class<? extends JsonDeserializer<?>> findContentDeserializer(Annotated annotated);

    public abstract String findDeserializablePropertyName(AnnotatedField annotatedField);

    public abstract Class<?> findDeserializationContentType(Annotated annotated, JavaType javaType, String str);

    public abstract Class<?> findDeserializationKeyType(Annotated annotated, JavaType javaType, String str);

    public abstract Class<?> findDeserializationType(Annotated annotated, JavaType javaType, String str);

    public abstract Object findDeserializer(Annotated annotated, BeanProperty beanProperty);

    public abstract String findEnumValue(Enum<?> enumR);

    public abstract String findGettablePropertyName(AnnotatedMethod annotatedMethod);

    public abstract Boolean findIgnoreUnknownProperties(AnnotatedClass annotatedClass);

    public abstract Class<? extends KeyDeserializer> findKeyDeserializer(Annotated annotated);

    public abstract String[] findPropertiesToIgnore(AnnotatedClass annotatedClass);

    public abstract String findPropertyNameForParam(AnnotatedParameter annotatedParameter);

    public abstract String findRootName(AnnotatedClass annotatedClass);

    public abstract String findSerializablePropertyName(AnnotatedField annotatedField);

    public abstract JsonSerialize.Inclusion findSerializationInclusion(Annotated annotated, JsonSerialize.Inclusion inclusion);

    public abstract String[] findSerializationPropertyOrder(AnnotatedClass annotatedClass);

    public abstract Boolean findSerializationSortAlphabetically(AnnotatedClass annotatedClass);

    public abstract Class<?> findSerializationType(Annotated annotated);

    public abstract JsonSerialize.Typing findSerializationTyping(Annotated annotated);

    public abstract Class<?>[] findSerializationViews(Annotated annotated);

    public abstract Object findSerializer(Annotated annotated, BeanProperty beanProperty);

    public abstract String findSettablePropertyName(AnnotatedMethod annotatedMethod);

    public abstract boolean hasAsValueAnnotation(AnnotatedMethod annotatedMethod);

    public abstract boolean isHandled(Annotation annotation);

    public abstract boolean isIgnorableConstructor(AnnotatedConstructor annotatedConstructor);

    public abstract boolean isIgnorableField(AnnotatedField annotatedField);

    public abstract boolean isIgnorableMethod(AnnotatedMethod annotatedMethod);

    public static class ReferenceProperty {
        private final String _name;
        private final Type _type;

        public enum Type {
            MANAGED_REFERENCE,
            BACK_REFERENCE
        }

        public ReferenceProperty(Type type, String str) {
            this._type = type;
            this._name = str;
        }

        public static ReferenceProperty managed(String str) {
            return new ReferenceProperty(Type.MANAGED_REFERENCE, str);
        }

        public static ReferenceProperty back(String str) {
            return new ReferenceProperty(Type.BACK_REFERENCE, str);
        }

        public Type getType() {
            return this._type;
        }

        public String getName() {
            return this._name;
        }

        public boolean isManagedReference() {
            return this._type == Type.MANAGED_REFERENCE;
        }

        public boolean isBackReference() {
            return this._type == Type.BACK_REFERENCE;
        }
    }

    public static AnnotationIntrospector nopInstance() {
        return NopAnnotationIntrospector.instance;
    }

    public static AnnotationIntrospector pair(AnnotationIntrospector annotationIntrospector, AnnotationIntrospector annotationIntrospector2) {
        return new Pair(annotationIntrospector, annotationIntrospector2);
    }

    public Collection<AnnotationIntrospector> allIntrospectors() {
        return Collections.singletonList(this);
    }

    public Collection<AnnotationIntrospector> allIntrospectors(Collection<AnnotationIntrospector> collection) {
        collection.add(this);
        return collection;
    }

    public Boolean isIgnorableType(AnnotatedClass annotatedClass) {
        return null;
    }

    public Object findFilterId(AnnotatedClass annotatedClass) {
        return null;
    }

    public TypeResolverBuilder<?> findTypeResolver(AnnotatedClass annotatedClass, JavaType javaType) {
        return null;
    }

    public TypeResolverBuilder<?> findPropertyTypeResolver(AnnotatedMember annotatedMember, JavaType javaType) {
        return null;
    }

    public TypeResolverBuilder<?> findPropertyContentTypeResolver(AnnotatedMember annotatedMember, JavaType javaType) {
        return null;
    }

    public List<NamedType> findSubtypes(Annotated annotated) {
        return null;
    }

    public String findTypeName(AnnotatedClass annotatedClass) {
        return null;
    }

    public ReferenceProperty findReferenceType(AnnotatedMember annotatedMember) {
        return null;
    }

    @Deprecated
    public Object findSerializer(Annotated annotated) {
        return findSerializer(annotated, null);
    }

    @Deprecated
    public final Object findDeserializer(Annotated annotated) {
        return findDeserializer(annotated, null);
    }

    public boolean hasAnySetterAnnotation(AnnotatedMethod annotatedMethod) {
        return false;
    }

    public boolean hasAnyGetterAnnotation(AnnotatedMethod annotatedMethod) {
        return false;
    }

    public boolean hasCreatorAnnotation(Annotated annotated) {
        return false;
    }

    public static class Pair extends AnnotationIntrospector {
        protected final AnnotationIntrospector _primary;
        protected final AnnotationIntrospector _secondary;

        public Pair(AnnotationIntrospector annotationIntrospector, AnnotationIntrospector annotationIntrospector2) {
            this._primary = annotationIntrospector;
            this._secondary = annotationIntrospector2;
        }

        public static AnnotationIntrospector create(AnnotationIntrospector annotationIntrospector, AnnotationIntrospector annotationIntrospector2) {
            if (annotationIntrospector == null) {
                return annotationIntrospector2;
            }
            if (annotationIntrospector2 == null) {
                return annotationIntrospector;
            }
            return new Pair(annotationIntrospector, annotationIntrospector2);
        }

        public Collection<AnnotationIntrospector> allIntrospectors() {
            return allIntrospectors(new ArrayList());
        }

        public Collection<AnnotationIntrospector> allIntrospectors(Collection<AnnotationIntrospector> collection) {
            this._primary.allIntrospectors(collection);
            this._secondary.allIntrospectors(collection);
            return collection;
        }

        public boolean isHandled(Annotation annotation) {
            return this._primary.isHandled(annotation) || this._secondary.isHandled(annotation);
        }

        public Boolean findCachability(AnnotatedClass annotatedClass) {
            Boolean findCachability = this._primary.findCachability(annotatedClass);
            if (findCachability == null) {
                return this._secondary.findCachability(annotatedClass);
            }
            return findCachability;
        }

        public String findRootName(AnnotatedClass annotatedClass) {
            String findRootName;
            String findRootName2 = this._primary.findRootName(annotatedClass);
            if (findRootName2 == null) {
                return this._secondary.findRootName(annotatedClass);
            }
            return (findRootName2.length() > 0 || (findRootName = this._secondary.findRootName(annotatedClass)) == null) ? findRootName2 : findRootName;
        }

        public String[] findPropertiesToIgnore(AnnotatedClass annotatedClass) {
            String[] findPropertiesToIgnore = this._primary.findPropertiesToIgnore(annotatedClass);
            if (findPropertiesToIgnore == null) {
                return this._secondary.findPropertiesToIgnore(annotatedClass);
            }
            return findPropertiesToIgnore;
        }

        public Boolean findIgnoreUnknownProperties(AnnotatedClass annotatedClass) {
            Boolean findIgnoreUnknownProperties = this._primary.findIgnoreUnknownProperties(annotatedClass);
            if (findIgnoreUnknownProperties == null) {
                return this._secondary.findIgnoreUnknownProperties(annotatedClass);
            }
            return findIgnoreUnknownProperties;
        }

        public Boolean isIgnorableType(AnnotatedClass annotatedClass) {
            Boolean isIgnorableType = this._primary.isIgnorableType(annotatedClass);
            if (isIgnorableType == null) {
                return this._secondary.isIgnorableType(annotatedClass);
            }
            return isIgnorableType;
        }

        public Object findFilterId(AnnotatedClass annotatedClass) {
            Object findFilterId = this._primary.findFilterId(annotatedClass);
            if (findFilterId == null) {
                return this._secondary.findFilterId(annotatedClass);
            }
            return findFilterId;
        }

        public VisibilityChecker<?> findAutoDetectVisibility(AnnotatedClass annotatedClass, VisibilityChecker<?> visibilityChecker) {
            return this._primary.findAutoDetectVisibility(annotatedClass, this._secondary.findAutoDetectVisibility(annotatedClass, visibilityChecker));
        }

        public TypeResolverBuilder<?> findTypeResolver(AnnotatedClass annotatedClass, JavaType javaType) {
            TypeResolverBuilder<?> findTypeResolver = this._primary.findTypeResolver(annotatedClass, javaType);
            if (findTypeResolver == null) {
                return this._secondary.findTypeResolver(annotatedClass, javaType);
            }
            return findTypeResolver;
        }

        public TypeResolverBuilder<?> findPropertyTypeResolver(AnnotatedMember annotatedMember, JavaType javaType) {
            TypeResolverBuilder<?> findPropertyTypeResolver = this._primary.findPropertyTypeResolver(annotatedMember, javaType);
            if (findPropertyTypeResolver == null) {
                return this._secondary.findPropertyTypeResolver(annotatedMember, javaType);
            }
            return findPropertyTypeResolver;
        }

        public TypeResolverBuilder<?> findPropertyContentTypeResolver(AnnotatedMember annotatedMember, JavaType javaType) {
            TypeResolverBuilder<?> findPropertyContentTypeResolver = this._primary.findPropertyContentTypeResolver(annotatedMember, javaType);
            if (findPropertyContentTypeResolver == null) {
                return this._secondary.findPropertyContentTypeResolver(annotatedMember, javaType);
            }
            return findPropertyContentTypeResolver;
        }

        public List<NamedType> findSubtypes(Annotated annotated) {
            List<NamedType> findSubtypes = this._primary.findSubtypes(annotated);
            List<NamedType> findSubtypes2 = this._secondary.findSubtypes(annotated);
            if (findSubtypes == null || findSubtypes.isEmpty()) {
                return findSubtypes2;
            }
            if (findSubtypes2 == null || findSubtypes2.isEmpty()) {
                return findSubtypes;
            }
            ArrayList arrayList = new ArrayList(findSubtypes.size() + findSubtypes2.size());
            arrayList.addAll(findSubtypes);
            arrayList.addAll(findSubtypes2);
            return arrayList;
        }

        public String findTypeName(AnnotatedClass annotatedClass) {
            String findTypeName = this._primary.findTypeName(annotatedClass);
            if (findTypeName == null || findTypeName.length() == 0) {
                return this._secondary.findTypeName(annotatedClass);
            }
            return findTypeName;
        }

        public ReferenceProperty findReferenceType(AnnotatedMember annotatedMember) {
            ReferenceProperty findReferenceType = this._primary.findReferenceType(annotatedMember);
            if (findReferenceType == null) {
                return this._secondary.findReferenceType(annotatedMember);
            }
            return findReferenceType;
        }

        public boolean isIgnorableMethod(AnnotatedMethod annotatedMethod) {
            return this._primary.isIgnorableMethod(annotatedMethod) || this._secondary.isIgnorableMethod(annotatedMethod);
        }

        public boolean isIgnorableConstructor(AnnotatedConstructor annotatedConstructor) {
            return this._primary.isIgnorableConstructor(annotatedConstructor) || this._secondary.isIgnorableConstructor(annotatedConstructor);
        }

        public boolean isIgnorableField(AnnotatedField annotatedField) {
            return this._primary.isIgnorableField(annotatedField) || this._secondary.isIgnorableField(annotatedField);
        }

        public Object findSerializer(Annotated annotated, BeanProperty beanProperty) {
            Object findSerializer = this._primary.findSerializer(annotated, beanProperty);
            if (findSerializer == null) {
                return this._secondary.findSerializer(annotated, beanProperty);
            }
            return findSerializer;
        }

        public JsonSerialize.Inclusion findSerializationInclusion(Annotated annotated, JsonSerialize.Inclusion inclusion) {
            return this._primary.findSerializationInclusion(annotated, this._secondary.findSerializationInclusion(annotated, inclusion));
        }

        public Class<?> findSerializationType(Annotated annotated) {
            Class<?> findSerializationType = this._primary.findSerializationType(annotated);
            if (findSerializationType == null) {
                return this._secondary.findSerializationType(annotated);
            }
            return findSerializationType;
        }

        public JsonSerialize.Typing findSerializationTyping(Annotated annotated) {
            JsonSerialize.Typing findSerializationTyping = this._primary.findSerializationTyping(annotated);
            if (findSerializationTyping == null) {
                return this._secondary.findSerializationTyping(annotated);
            }
            return findSerializationTyping;
        }

        public Class<?>[] findSerializationViews(Annotated annotated) {
            Class<?>[] findSerializationViews = this._primary.findSerializationViews(annotated);
            if (findSerializationViews == null) {
                return this._secondary.findSerializationViews(annotated);
            }
            return findSerializationViews;
        }

        public String[] findSerializationPropertyOrder(AnnotatedClass annotatedClass) {
            String[] findSerializationPropertyOrder = this._primary.findSerializationPropertyOrder(annotatedClass);
            if (findSerializationPropertyOrder == null) {
                return this._secondary.findSerializationPropertyOrder(annotatedClass);
            }
            return findSerializationPropertyOrder;
        }

        public Boolean findSerializationSortAlphabetically(AnnotatedClass annotatedClass) {
            Boolean findSerializationSortAlphabetically = this._primary.findSerializationSortAlphabetically(annotatedClass);
            if (findSerializationSortAlphabetically == null) {
                return this._secondary.findSerializationSortAlphabetically(annotatedClass);
            }
            return findSerializationSortAlphabetically;
        }

        public String findGettablePropertyName(AnnotatedMethod annotatedMethod) {
            String findGettablePropertyName;
            String findGettablePropertyName2 = this._primary.findGettablePropertyName(annotatedMethod);
            if (findGettablePropertyName2 == null) {
                return this._secondary.findGettablePropertyName(annotatedMethod);
            }
            if (findGettablePropertyName2.length() != 0 || (findGettablePropertyName = this._secondary.findGettablePropertyName(annotatedMethod)) == null) {
                return findGettablePropertyName2;
            }
            return findGettablePropertyName;
        }

        public boolean hasAsValueAnnotation(AnnotatedMethod annotatedMethod) {
            return this._primary.hasAsValueAnnotation(annotatedMethod) || this._secondary.hasAsValueAnnotation(annotatedMethod);
        }

        public String findEnumValue(Enum<?> enumR) {
            String findEnumValue = this._primary.findEnumValue(enumR);
            if (findEnumValue == null) {
                return this._secondary.findEnumValue(enumR);
            }
            return findEnumValue;
        }

        public String findSerializablePropertyName(AnnotatedField annotatedField) {
            String findSerializablePropertyName;
            String findSerializablePropertyName2 = this._primary.findSerializablePropertyName(annotatedField);
            if (findSerializablePropertyName2 == null) {
                return this._secondary.findSerializablePropertyName(annotatedField);
            }
            if (findSerializablePropertyName2.length() != 0 || (findSerializablePropertyName = this._secondary.findSerializablePropertyName(annotatedField)) == null) {
                return findSerializablePropertyName2;
            }
            return findSerializablePropertyName;
        }

        public Object findDeserializer(Annotated annotated, BeanProperty beanProperty) {
            Object findDeserializer = this._primary.findDeserializer(annotated, beanProperty);
            if (findDeserializer == null) {
                return this._secondary.findDeserializer(annotated, beanProperty);
            }
            return findDeserializer;
        }

        public Class<? extends KeyDeserializer> findKeyDeserializer(Annotated annotated) {
            Class<? extends KeyDeserializer> findKeyDeserializer = this._primary.findKeyDeserializer(annotated);
            if (findKeyDeserializer == null || findKeyDeserializer == KeyDeserializer.None.class) {
                return this._secondary.findKeyDeserializer(annotated);
            }
            return findKeyDeserializer;
        }

        public Class<? extends JsonDeserializer<?>> findContentDeserializer(Annotated annotated) {
            Class<? extends JsonDeserializer<?>> findContentDeserializer = this._primary.findContentDeserializer(annotated);
            if (findContentDeserializer == null || findContentDeserializer == JsonDeserializer.None.class) {
                return this._secondary.findContentDeserializer(annotated);
            }
            return findContentDeserializer;
        }

        public Class<?> findDeserializationType(Annotated annotated, JavaType javaType, String str) {
            Class<?> findDeserializationType = this._primary.findDeserializationType(annotated, javaType, str);
            if (findDeserializationType == null) {
                return this._secondary.findDeserializationType(annotated, javaType, str);
            }
            return findDeserializationType;
        }

        public Class<?> findDeserializationKeyType(Annotated annotated, JavaType javaType, String str) {
            Class<?> findDeserializationKeyType = this._primary.findDeserializationKeyType(annotated, javaType, str);
            if (findDeserializationKeyType == null) {
                return this._secondary.findDeserializationKeyType(annotated, javaType, str);
            }
            return findDeserializationKeyType;
        }

        public Class<?> findDeserializationContentType(Annotated annotated, JavaType javaType, String str) {
            Class<?> findDeserializationContentType = this._primary.findDeserializationContentType(annotated, javaType, str);
            if (findDeserializationContentType == null) {
                return this._secondary.findDeserializationContentType(annotated, javaType, str);
            }
            return findDeserializationContentType;
        }

        public String findSettablePropertyName(AnnotatedMethod annotatedMethod) {
            String findSettablePropertyName;
            String findSettablePropertyName2 = this._primary.findSettablePropertyName(annotatedMethod);
            if (findSettablePropertyName2 == null) {
                return this._secondary.findSettablePropertyName(annotatedMethod);
            }
            if (findSettablePropertyName2.length() != 0 || (findSettablePropertyName = this._secondary.findSettablePropertyName(annotatedMethod)) == null) {
                return findSettablePropertyName2;
            }
            return findSettablePropertyName;
        }

        public boolean hasAnySetterAnnotation(AnnotatedMethod annotatedMethod) {
            return this._primary.hasAnySetterAnnotation(annotatedMethod) || this._secondary.hasAnySetterAnnotation(annotatedMethod);
        }

        public boolean hasAnyGetterAnnotation(AnnotatedMethod annotatedMethod) {
            return this._primary.hasAnyGetterAnnotation(annotatedMethod) || this._secondary.hasAnyGetterAnnotation(annotatedMethod);
        }

        public boolean hasCreatorAnnotation(Annotated annotated) {
            return this._primary.hasCreatorAnnotation(annotated) || this._secondary.hasCreatorAnnotation(annotated);
        }

        public String findDeserializablePropertyName(AnnotatedField annotatedField) {
            String findDeserializablePropertyName;
            String findDeserializablePropertyName2 = this._primary.findDeserializablePropertyName(annotatedField);
            if (findDeserializablePropertyName2 == null) {
                return this._secondary.findDeserializablePropertyName(annotatedField);
            }
            if (findDeserializablePropertyName2.length() != 0 || (findDeserializablePropertyName = this._secondary.findDeserializablePropertyName(annotatedField)) == null) {
                return findDeserializablePropertyName2;
            }
            return findDeserializablePropertyName;
        }

        public String findPropertyNameForParam(AnnotatedParameter annotatedParameter) {
            String findPropertyNameForParam = this._primary.findPropertyNameForParam(annotatedParameter);
            if (findPropertyNameForParam == null) {
                return this._secondary.findPropertyNameForParam(annotatedParameter);
            }
            return findPropertyNameForParam;
        }
    }
}
