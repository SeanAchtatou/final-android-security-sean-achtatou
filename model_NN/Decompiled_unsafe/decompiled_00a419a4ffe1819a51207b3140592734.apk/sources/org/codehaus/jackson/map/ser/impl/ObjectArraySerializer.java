package org.codehaus.jackson.map.ser.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ResolvableSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;
import org.codehaus.jackson.map.annotate.JacksonStdImpl;
import org.codehaus.jackson.map.ser.ArraySerializers;
import org.codehaus.jackson.map.ser.ContainerSerializerBase;
import org.codehaus.jackson.map.ser.impl.PropertySerializerMap;
import org.codehaus.jackson.map.type.ArrayType;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.schema.JsonSchema;
import org.codehaus.jackson.schema.SchemaAware;
import org.codehaus.jackson.type.JavaType;

@JacksonStdImpl
public class ObjectArraySerializer extends ArraySerializers.AsArraySerializer<Object[]> implements ResolvableSerializer {
    protected PropertySerializerMap _dynamicSerializers = PropertySerializerMap.emptyMap();
    protected JsonSerializer<Object> _elementSerializer;
    protected final JavaType _elementType;
    protected final boolean _staticTyping;

    public ObjectArraySerializer(JavaType javaType, boolean z, TypeSerializer typeSerializer, BeanProperty beanProperty) {
        super(Object[].class, typeSerializer, beanProperty);
        this._elementType = javaType;
        this._staticTyping = z;
    }

    public ContainerSerializerBase<?> _withValueTypeSerializer(TypeSerializer typeSerializer) {
        return new ObjectArraySerializer(this._elementType, this._staticTyping, typeSerializer, this._property);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003c, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003d, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003e, code lost:
        r2 = 0;
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0058, code lost:
        throw ((java.lang.Error) r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x005d, code lost:
        throw org.codehaus.jackson.map.JsonMappingException.wrapWithPath(r7, r1, r2);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003b A[ExcHandler: IOException (r0v9 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:9:0x0018] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0059  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void serializeContents(java.lang.Object[] r8, org.codehaus.jackson.JsonGenerator r9, org.codehaus.jackson.map.SerializerProvider r10) throws java.io.IOException, org.codehaus.jackson.JsonGenerationException {
        /*
            r7 = this;
            int r0 = r8.length
            if (r0 != 0) goto L_0x0004
        L_0x0003:
            return
        L_0x0004:
            org.codehaus.jackson.map.JsonSerializer<java.lang.Object> r1 = r7._elementSerializer
            if (r1 == 0) goto L_0x000e
            org.codehaus.jackson.map.JsonSerializer<java.lang.Object> r0 = r7._elementSerializer
            r7.serializeContentsUsing(r8, r9, r10, r0)
            goto L_0x0003
        L_0x000e:
            org.codehaus.jackson.map.TypeSerializer r1 = r7._valueTypeSerializer
            if (r1 == 0) goto L_0x0016
            r7.serializeTypedContents(r8, r9, r10)
            goto L_0x0003
        L_0x0016:
            r1 = 0
            r2 = 0
            org.codehaus.jackson.map.ser.impl.PropertySerializerMap r3 = r7._dynamicSerializers     // Catch:{ IOException -> 0x003b, Exception -> 0x003d }
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x001d:
            if (r2 >= r0) goto L_0x0003
            r1 = r8[r2]     // Catch:{ IOException -> 0x003b, Exception -> 0x005e }
            if (r1 != 0) goto L_0x0029
            r10.defaultSerializeNull(r9)     // Catch:{ IOException -> 0x003b, Exception -> 0x005e }
        L_0x0026:
            int r2 = r2 + 1
            goto L_0x001d
        L_0x0029:
            java.lang.Class r4 = r1.getClass()     // Catch:{ IOException -> 0x003b, Exception -> 0x005e }
            org.codehaus.jackson.map.JsonSerializer r5 = r3.serializerFor(r4)     // Catch:{ IOException -> 0x003b, Exception -> 0x005e }
            if (r5 != 0) goto L_0x0060
            org.codehaus.jackson.map.JsonSerializer r4 = r7._findAndAddDynamic(r3, r4, r10)     // Catch:{ IOException -> 0x003b, Exception -> 0x005e }
        L_0x0037:
            r4.serialize(r1, r9, r10)     // Catch:{ IOException -> 0x003b, Exception -> 0x005e }
            goto L_0x0026
        L_0x003b:
            r0 = move-exception
            throw r0
        L_0x003d:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0041:
            r7 = r0
        L_0x0042:
            boolean r0 = r7 instanceof java.lang.reflect.InvocationTargetException
            if (r0 == 0) goto L_0x0052
            java.lang.Throwable r0 = r7.getCause()
            if (r0 == 0) goto L_0x0052
            java.lang.Throwable r0 = r7.getCause()
            r7 = r0
            goto L_0x0042
        L_0x0052:
            boolean r0 = r7 instanceof java.lang.Error
            if (r0 == 0) goto L_0x0059
            java.lang.Error r7 = (java.lang.Error) r7
            throw r7
        L_0x0059:
            org.codehaus.jackson.map.JsonMappingException r0 = org.codehaus.jackson.map.JsonMappingException.wrapWithPath(r7, r1, r2)
            throw r0
        L_0x005e:
            r0 = move-exception
            goto L_0x0041
        L_0x0060:
            r4 = r5
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.ser.impl.ObjectArraySerializer.serializeContents(java.lang.Object[], org.codehaus.jackson.JsonGenerator, org.codehaus.jackson.map.SerializerProvider):void");
    }

    public void serializeContentsUsing(Object[] objArr, JsonGenerator jsonGenerator, SerializerProvider serializerProvider, JsonSerializer<Object> jsonSerializer) throws IOException, JsonGenerationException {
        Throwable th;
        int length = objArr.length;
        TypeSerializer typeSerializer = this._valueTypeSerializer;
        int i = 0;
        Object obj = null;
        while (i < length) {
            try {
                obj = objArr[i];
                if (obj == null) {
                    serializerProvider.defaultSerializeNull(jsonGenerator);
                } else if (typeSerializer == null) {
                    jsonSerializer.serialize(obj, jsonGenerator, serializerProvider);
                } else {
                    jsonSerializer.serializeWithType(obj, jsonGenerator, serializerProvider, typeSerializer);
                }
                i++;
            } catch (IOException e) {
                throw e;
            } catch (Exception e2) {
                e = e2;
                Object obj2 = obj;
                while (true) {
                    th = e;
                    if ((th instanceof InvocationTargetException) && th.getCause() != null) {
                        e = th.getCause();
                    }
                }
                if (th instanceof Error) {
                    throw ((Error) th);
                }
                throw JsonMappingException.wrapWithPath(th, obj2, i);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0029, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002b, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0043, code lost:
        throw ((java.lang.Error) r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0048, code lost:
        throw org.codehaus.jackson.map.JsonMappingException.wrapWithPath(r8, r1, r2);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0028 A[ExcHandler: IOException (r0v9 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void serializeTypedContents(java.lang.Object[] r9, org.codehaus.jackson.JsonGenerator r10, org.codehaus.jackson.map.SerializerProvider r11) throws java.io.IOException, org.codehaus.jackson.JsonGenerationException {
        /*
            r8 = this;
            int r0 = r9.length
            org.codehaus.jackson.map.TypeSerializer r1 = r8._valueTypeSerializer
            r2 = 0
            r3 = 0
            org.codehaus.jackson.map.ser.impl.PropertySerializerMap r4 = r8._dynamicSerializers     // Catch:{ IOException -> 0x0028, Exception -> 0x002a }
            r7 = r3
            r3 = r2
            r2 = r7
        L_0x000a:
            if (r3 >= r0) goto L_0x0049
            r2 = r9[r3]     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
            if (r2 != 0) goto L_0x0016
            r11.defaultSerializeNull(r10)     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
        L_0x0013:
            int r3 = r3 + 1
            goto L_0x000a
        L_0x0016:
            java.lang.Class r5 = r2.getClass()     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
            org.codehaus.jackson.map.JsonSerializer r6 = r4.serializerFor(r5)     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
            if (r6 != 0) goto L_0x004e
            org.codehaus.jackson.map.JsonSerializer r5 = r8._findAndAddDynamic(r4, r5, r11)     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
        L_0x0024:
            r5.serializeWithType(r2, r10, r11, r1)     // Catch:{ IOException -> 0x0028, Exception -> 0x004a }
            goto L_0x0013
        L_0x0028:
            r0 = move-exception
            throw r0
        L_0x002a:
            r0 = move-exception
            r1 = r3
        L_0x002c:
            r8 = r0
        L_0x002d:
            boolean r0 = r8 instanceof java.lang.reflect.InvocationTargetException
            if (r0 == 0) goto L_0x003d
            java.lang.Throwable r0 = r8.getCause()
            if (r0 == 0) goto L_0x003d
            java.lang.Throwable r0 = r8.getCause()
            r8 = r0
            goto L_0x002d
        L_0x003d:
            boolean r0 = r8 instanceof java.lang.Error
            if (r0 == 0) goto L_0x0044
            java.lang.Error r8 = (java.lang.Error) r8
            throw r8
        L_0x0044:
            org.codehaus.jackson.map.JsonMappingException r0 = org.codehaus.jackson.map.JsonMappingException.wrapWithPath(r8, r1, r2)
            throw r0
        L_0x0049:
            return
        L_0x004a:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x002c
        L_0x004e:
            r5 = r6
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.ser.impl.ObjectArraySerializer.serializeTypedContents(java.lang.Object[], org.codehaus.jackson.JsonGenerator, org.codehaus.jackson.map.SerializerProvider):void");
    }

    public JsonNode getSchema(SerializerProvider serializerProvider, Type type) throws JsonMappingException {
        ObjectNode createSchemaNode = createSchemaNode("array", true);
        if (type != null) {
            JavaType type2 = TypeFactory.type(type);
            if (type2.isArrayType()) {
                Class<?> rawClass = ((ArrayType) type2).getContentType().getRawClass();
                if (rawClass == Object.class) {
                    createSchemaNode.put("items", JsonSchema.getDefaultSchemaNode());
                } else {
                    JsonSerializer<Object> findValueSerializer = serializerProvider.findValueSerializer(rawClass, this._property);
                    createSchemaNode.put("items", findValueSerializer instanceof SchemaAware ? ((SchemaAware) findValueSerializer).getSchema(serializerProvider, null) : JsonSchema.getDefaultSchemaNode());
                }
            }
        }
        return createSchemaNode;
    }

    public void resolve(SerializerProvider serializerProvider) throws JsonMappingException {
        if (this._staticTyping) {
            this._elementSerializer = serializerProvider.findValueSerializer(this._elementType, this._property);
        }
    }

    /* access modifiers changed from: protected */
    public final JsonSerializer<Object> _findAndAddDynamic(PropertySerializerMap propertySerializerMap, Class<?> cls, SerializerProvider serializerProvider) throws JsonMappingException {
        PropertySerializerMap.SerializerAndMapResult findAndAddSerializer = propertySerializerMap.findAndAddSerializer(cls, serializerProvider, this._property);
        if (propertySerializerMap != findAndAddSerializer.map) {
            this._dynamicSerializers = findAndAddSerializer.map;
        }
        return findAndAddSerializer.serializer;
    }
}
