package org.codehaus.jackson.map.introspect;

import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

public final class AnnotatedMethod extends AnnotatedWithParams {
    protected final Method _method;
    protected Class<?>[] _paramTypes;

    public AnnotatedMethod(Method method, AnnotationMap annotationMap, AnnotationMap[] annotationMapArr) {
        super(annotationMap, annotationMapArr);
        this._method = method;
    }

    public final AnnotatedMethod withMethod(Method method) {
        return new AnnotatedMethod(method, this._annotations, this._paramAnnotations);
    }

    public final Method getAnnotated() {
        return this._method;
    }

    public final int getModifiers() {
        return this._method.getModifiers();
    }

    public final String getName() {
        return this._method.getName();
    }

    public final Type getGenericType() {
        return this._method.getGenericReturnType();
    }

    public final Class<?> getRawType() {
        return this._method.getReturnType();
    }

    public final Class<?> getDeclaringClass() {
        return this._method.getDeclaringClass();
    }

    public final Member getMember() {
        return this._method;
    }

    public final AnnotatedParameter getParameter(int i) {
        return new AnnotatedParameter(this, getParameterType(i), this._paramAnnotations[i]);
    }

    public final int getParameterCount() {
        return getParameterTypes().length;
    }

    public final Type[] getParameterTypes() {
        return this._method.getGenericParameterTypes();
    }

    public final Class<?> getParameterClass(int i) {
        Class<?>[] parameterTypes = this._method.getParameterTypes();
        if (i >= parameterTypes.length) {
            return null;
        }
        return parameterTypes[i];
    }

    public final Type getParameterType(int i) {
        Type[] genericParameterTypes = this._method.getGenericParameterTypes();
        if (i >= genericParameterTypes.length) {
            return null;
        }
        return genericParameterTypes[i];
    }

    public final Class<?>[] getParameterClasses() {
        if (this._paramTypes == null) {
            this._paramTypes = this._method.getParameterTypes();
        }
        return this._paramTypes;
    }

    public final String getFullName() {
        return getDeclaringClass().getName() + "#" + getName() + "(" + getParameterCount() + " params)";
    }

    public final String toString() {
        return "[method " + getName() + ", annotations: " + this._annotations + "]";
    }
}
