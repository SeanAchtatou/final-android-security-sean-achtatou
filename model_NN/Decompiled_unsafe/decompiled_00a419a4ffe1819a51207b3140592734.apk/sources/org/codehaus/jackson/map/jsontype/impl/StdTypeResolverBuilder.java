package org.codehaus.jackson.map.jsontype.impl;

import java.util.Collection;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.impl.JsonWriteContext;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.TypeDeserializer;
import org.codehaus.jackson.map.TypeSerializer;
import org.codehaus.jackson.map.jsontype.NamedType;
import org.codehaus.jackson.map.jsontype.TypeIdResolver;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.type.JavaType;

public class StdTypeResolverBuilder implements TypeResolverBuilder<StdTypeResolverBuilder> {
    protected TypeIdResolver _customIdResolver;
    protected JsonTypeInfo.Id _idType;
    protected JsonTypeInfo.As _includeAs;
    protected String _typeProperty;

    public StdTypeResolverBuilder init(JsonTypeInfo.Id id, TypeIdResolver typeIdResolver) {
        if (id == null) {
            throw new IllegalArgumentException("idType can not be null");
        }
        this._idType = id;
        this._customIdResolver = typeIdResolver;
        this._typeProperty = id.getDefaultPropertyName();
        return this;
    }

    public TypeSerializer buildTypeSerializer(JavaType javaType, Collection<NamedType> collection, BeanProperty beanProperty) {
        TypeIdResolver idResolver = idResolver(javaType, collection, true, false);
        switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$As[this._includeAs.ordinal()]) {
            case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                return new AsArrayTypeSerializer(idResolver, beanProperty);
            case JsonWriteContext.STATUS_OK_AFTER_COLON:
                return new AsPropertyTypeSerializer(idResolver, beanProperty, this._typeProperty);
            case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                return new AsWrapperTypeSerializer(idResolver, beanProperty);
            default:
                throw new IllegalStateException("Do not know how to construct standard type serializer for inclusion type: " + this._includeAs);
        }
    }

    public TypeDeserializer buildTypeDeserializer(JavaType javaType, Collection<NamedType> collection, BeanProperty beanProperty) {
        TypeIdResolver idResolver = idResolver(javaType, collection, false, true);
        switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$As[this._includeAs.ordinal()]) {
            case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                return new AsArrayTypeDeserializer(javaType, idResolver, beanProperty);
            case JsonWriteContext.STATUS_OK_AFTER_COLON:
                return new AsPropertyTypeDeserializer(javaType, idResolver, beanProperty, this._typeProperty);
            case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                return new AsWrapperTypeDeserializer(javaType, idResolver, beanProperty);
            default:
                throw new IllegalStateException("Do not know how to construct standard type serializer for inclusion type: " + this._includeAs);
        }
    }

    public StdTypeResolverBuilder inclusion(JsonTypeInfo.As as) {
        if (as == null) {
            throw new IllegalArgumentException("includeAs can not be null");
        }
        this._includeAs = as;
        return this;
    }

    public StdTypeResolverBuilder typeProperty(String str) {
        String str2;
        if (str == null || str.length() == 0) {
            str2 = this._idType.getDefaultPropertyName();
        } else {
            str2 = str;
        }
        this._typeProperty = str2;
        return this;
    }

    public String getTypeProperty() {
        return this._typeProperty;
    }

    /* access modifiers changed from: protected */
    public TypeIdResolver idResolver(JavaType javaType, Collection<NamedType> collection, boolean z, boolean z2) {
        if (this._customIdResolver != null) {
            return this._customIdResolver;
        }
        if (this._idType == null) {
            throw new IllegalStateException("Can not build, 'init()' not yet called");
        }
        switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$Id[this._idType.ordinal()]) {
            case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                return new ClassNameIdResolver(javaType);
            case JsonWriteContext.STATUS_OK_AFTER_COLON:
                return new MinimalClassNameIdResolver(javaType);
            case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                return TypeNameIdResolver.construct(javaType, collection, z, z2);
            default:
                throw new IllegalStateException("Do not know how to construct standard type id resolver for idType: " + this._idType);
        }
    }

    /* renamed from: org.codehaus.jackson.map.jsontype.impl.StdTypeResolverBuilder$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$As = new int[JsonTypeInfo.As.values().length];
        static final /* synthetic */ int[] $SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$Id = new int[JsonTypeInfo.Id.values().length];

        static {
            try {
                $SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$Id[JsonTypeInfo.Id.CLASS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$Id[JsonTypeInfo.Id.MINIMAL_CLASS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$Id[JsonTypeInfo.Id.NAME.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$Id[JsonTypeInfo.Id.CUSTOM.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$Id[JsonTypeInfo.Id.NONE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$As[JsonTypeInfo.As.WRAPPER_ARRAY.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$As[JsonTypeInfo.As.PROPERTY.ordinal()] = 2;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$annotate$JsonTypeInfo$As[JsonTypeInfo.As.WRAPPER_OBJECT.ordinal()] = 3;
            } catch (NoSuchFieldError e8) {
            }
        }
    }
}
