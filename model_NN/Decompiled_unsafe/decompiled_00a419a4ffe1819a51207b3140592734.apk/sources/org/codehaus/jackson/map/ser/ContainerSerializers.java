package org.codehaus.jackson.map.ser;

import java.io.IOException;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ResolvableSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;
import org.codehaus.jackson.map.annotate.JacksonStdImpl;
import org.codehaus.jackson.map.ser.impl.PropertySerializerMap;
import org.codehaus.jackson.type.JavaType;

public final class ContainerSerializers {
    private ContainerSerializers() {
    }

    public static ContainerSerializerBase<?> indexedListSerializer(JavaType javaType, boolean z, TypeSerializer typeSerializer, BeanProperty beanProperty) {
        return new IndexedListSerializer(javaType, z, typeSerializer, beanProperty);
    }

    public static ContainerSerializerBase<?> collectionSerializer(JavaType javaType, boolean z, TypeSerializer typeSerializer, BeanProperty beanProperty) {
        return new CollectionSerializer(javaType, z, typeSerializer, beanProperty);
    }

    public static ContainerSerializerBase<?> iteratorSerializer(JavaType javaType, boolean z, TypeSerializer typeSerializer, BeanProperty beanProperty) {
        return new IteratorSerializer(javaType, z, typeSerializer, beanProperty);
    }

    public static ContainerSerializerBase<?> iterableSerializer(JavaType javaType, boolean z, TypeSerializer typeSerializer, BeanProperty beanProperty) {
        return new IterableSerializer(javaType, z, typeSerializer, beanProperty);
    }

    public static JsonSerializer<?> enumSetSerializer(JavaType javaType, BeanProperty beanProperty) {
        return new EnumSetSerializer(javaType, beanProperty);
    }

    public static abstract class AsArraySerializer<T> extends ContainerSerializerBase<T> implements ResolvableSerializer {
        protected PropertySerializerMap _dynamicSerializers;
        protected JsonSerializer<Object> _elementSerializer;
        protected final JavaType _elementType;
        protected final BeanProperty _property;
        protected final boolean _staticTyping;
        protected final TypeSerializer _valueTypeSerializer;

        /* access modifiers changed from: protected */
        public abstract void serializeContents(T t, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException;

        protected AsArraySerializer(Class<?> cls, JavaType javaType, boolean z, TypeSerializer typeSerializer, BeanProperty beanProperty) {
            super(cls, false);
            boolean z2;
            this._elementType = javaType;
            if (z || (javaType != null && javaType.isFinal())) {
                z2 = true;
            } else {
                z2 = false;
            }
            this._staticTyping = z2;
            this._valueTypeSerializer = typeSerializer;
            this._property = beanProperty;
            this._dynamicSerializers = PropertySerializerMap.emptyMap();
        }

        public final void serialize(T t, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonGenerator.writeStartArray();
            serializeContents(t, jsonGenerator, serializerProvider);
            jsonGenerator.writeEndArray();
        }

        public final void serializeWithType(T t, JsonGenerator jsonGenerator, SerializerProvider serializerProvider, TypeSerializer typeSerializer) throws IOException, JsonGenerationException {
            typeSerializer.writeTypePrefixForArray(t, jsonGenerator);
            serializeContents(t, jsonGenerator, serializerProvider);
            typeSerializer.writeTypeSuffixForArray(t, jsonGenerator);
        }

        /* JADX WARNING: Removed duplicated region for block: B:20:0x004c  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public org.codehaus.jackson.JsonNode getSchema(org.codehaus.jackson.map.SerializerProvider r7, java.lang.reflect.Type r8) throws org.codehaus.jackson.map.JsonMappingException {
            /*
                r6 = this;
                r5 = 1
                r4 = 0
                java.lang.String r0 = "array"
                org.codehaus.jackson.node.ObjectNode r0 = r6.createSchemaNode(r0, r5)
                if (r8 == 0) goto L_0x0058
                org.codehaus.jackson.type.JavaType r1 = org.codehaus.jackson.map.type.TypeFactory.type(r8)
                org.codehaus.jackson.type.JavaType r1 = r1.getContentType()
                if (r1 != 0) goto L_0x0028
                boolean r2 = r8 instanceof java.lang.reflect.ParameterizedType
                if (r2 == 0) goto L_0x0028
                java.lang.reflect.ParameterizedType r8 = (java.lang.reflect.ParameterizedType) r8
                java.lang.reflect.Type[] r2 = r8.getActualTypeArguments()
                int r3 = r2.length
                if (r3 != r5) goto L_0x0028
                r1 = 0
                r1 = r2[r1]
                org.codehaus.jackson.type.JavaType r1 = org.codehaus.jackson.map.type.TypeFactory.type(r1)
            L_0x0028:
                if (r1 != 0) goto L_0x0030
                org.codehaus.jackson.type.JavaType r2 = r6._elementType
                if (r2 == 0) goto L_0x0030
                org.codehaus.jackson.type.JavaType r1 = r6._elementType
            L_0x0030:
                if (r1 == 0) goto L_0x0055
                java.lang.Class r2 = r1.getRawClass()
                java.lang.Class<java.lang.Object> r3 = java.lang.Object.class
                if (r2 == r3) goto L_0x0056
                org.codehaus.jackson.map.BeanProperty r2 = r6._property
                org.codehaus.jackson.map.JsonSerializer r6 = r7.findValueSerializer(r1, r2)
                boolean r1 = r6 instanceof org.codehaus.jackson.schema.SchemaAware
                if (r1 == 0) goto L_0x0056
                org.codehaus.jackson.schema.SchemaAware r6 = (org.codehaus.jackson.schema.SchemaAware) r6
                org.codehaus.jackson.JsonNode r1 = r6.getSchema(r7, r4)
            L_0x004a:
                if (r1 != 0) goto L_0x0050
                org.codehaus.jackson.JsonNode r1 = org.codehaus.jackson.schema.JsonSchema.getDefaultSchemaNode()
            L_0x0050:
                java.lang.String r2 = "items"
                r0.put(r2, r1)
            L_0x0055:
                return r0
            L_0x0056:
                r1 = r4
                goto L_0x004a
            L_0x0058:
                r1 = r4
                goto L_0x0028
            */
            throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.ser.ContainerSerializers.AsArraySerializer.getSchema(org.codehaus.jackson.map.SerializerProvider, java.lang.reflect.Type):org.codehaus.jackson.JsonNode");
        }

        public void resolve(SerializerProvider serializerProvider) throws JsonMappingException {
            if (this._staticTyping && this._elementType != null) {
                this._elementSerializer = serializerProvider.findValueSerializer(this._elementType, this._property);
            }
        }

        /* access modifiers changed from: protected */
        public final JsonSerializer<Object> _findAndAddDynamic(PropertySerializerMap propertySerializerMap, Class<?> cls, SerializerProvider serializerProvider) throws JsonMappingException {
            PropertySerializerMap.SerializerAndMapResult findAndAddSerializer = propertySerializerMap.findAndAddSerializer(cls, serializerProvider, this._property);
            if (propertySerializerMap != findAndAddSerializer.map) {
                this._dynamicSerializers = findAndAddSerializer.map;
            }
            return findAndAddSerializer.serializer;
        }
    }

    @JacksonStdImpl
    public static class IndexedListSerializer extends AsArraySerializer<List<?>> {
        public /* bridge */ /* synthetic */ void serializeContents(Object obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            serializeContents((List<?>) ((List) obj), jsonGenerator, serializerProvider);
        }

        public IndexedListSerializer(JavaType javaType, boolean z, TypeSerializer typeSerializer, BeanProperty beanProperty) {
            super(List.class, javaType, z, typeSerializer, beanProperty);
        }

        public ContainerSerializerBase<?> _withValueTypeSerializer(TypeSerializer typeSerializer) {
            return new IndexedListSerializer(this._elementType, this._staticTyping, typeSerializer, this._property);
        }

        public void serializeContents(List<?> list, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            JsonSerializer<Object> jsonSerializer;
            if (this._elementSerializer != null) {
                serializeContentsUsing(list, jsonGenerator, serializerProvider, this._elementSerializer);
            } else if (this._valueTypeSerializer != null) {
                serializeTypedContents(list, jsonGenerator, serializerProvider);
            } else {
                int size = list.size();
                if (size != 0) {
                    try {
                        PropertySerializerMap propertySerializerMap = this._dynamicSerializers;
                        for (int i = 0; i < size; i++) {
                            Object obj = list.get(i);
                            if (obj == null) {
                                serializerProvider.defaultSerializeNull(jsonGenerator);
                            } else {
                                Class<?> cls = obj.getClass();
                                JsonSerializer<Object> serializerFor = propertySerializerMap.serializerFor(cls);
                                if (serializerFor == null) {
                                    jsonSerializer = _findAndAddDynamic(propertySerializerMap, cls, serializerProvider);
                                } else {
                                    jsonSerializer = serializerFor;
                                }
                                jsonSerializer.serialize(obj, jsonGenerator, serializerProvider);
                            }
                        }
                    } catch (Exception e) {
                        wrapAndThrow(serializerProvider, e, list, 0);
                    }
                }
            }
        }

        public void serializeContentsUsing(List<?> list, JsonGenerator jsonGenerator, SerializerProvider serializerProvider, JsonSerializer<Object> jsonSerializer) throws IOException, JsonGenerationException {
            int size = list.size();
            if (size != 0) {
                TypeSerializer typeSerializer = this._valueTypeSerializer;
                for (int i = 0; i < size; i++) {
                    Object obj = list.get(i);
                    if (obj == null) {
                        try {
                            serializerProvider.defaultSerializeNull(jsonGenerator);
                        } catch (Exception e) {
                            wrapAndThrow(serializerProvider, e, list, i);
                        }
                    } else if (typeSerializer == null) {
                        jsonSerializer.serialize(obj, jsonGenerator, serializerProvider);
                    } else {
                        jsonSerializer.serializeWithType(obj, jsonGenerator, serializerProvider, typeSerializer);
                    }
                }
            }
        }

        public void serializeTypedContents(List<?> list, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            JsonSerializer<Object> jsonSerializer;
            int size = list.size();
            if (size != 0) {
                try {
                    TypeSerializer typeSerializer = this._valueTypeSerializer;
                    PropertySerializerMap propertySerializerMap = this._dynamicSerializers;
                    for (int i = 0; i < size; i++) {
                        Object obj = list.get(i);
                        if (obj == null) {
                            serializerProvider.defaultSerializeNull(jsonGenerator);
                        } else {
                            Class<?> cls = obj.getClass();
                            JsonSerializer<Object> serializerFor = propertySerializerMap.serializerFor(cls);
                            if (serializerFor == null) {
                                jsonSerializer = _findAndAddDynamic(propertySerializerMap, cls, serializerProvider);
                            } else {
                                jsonSerializer = serializerFor;
                            }
                            jsonSerializer.serializeWithType(obj, jsonGenerator, serializerProvider, typeSerializer);
                        }
                    }
                } catch (Exception e) {
                    wrapAndThrow(serializerProvider, e, list, 0);
                }
            }
        }
    }

    @JacksonStdImpl
    public static class CollectionSerializer extends AsArraySerializer<Collection<?>> {
        public /* bridge */ /* synthetic */ void serializeContents(Object obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            serializeContents((Collection<?>) ((Collection) obj), jsonGenerator, serializerProvider);
        }

        public CollectionSerializer(JavaType javaType, boolean z, TypeSerializer typeSerializer, BeanProperty beanProperty) {
            super(Collection.class, javaType, z, typeSerializer, beanProperty);
        }

        public ContainerSerializerBase<?> _withValueTypeSerializer(TypeSerializer typeSerializer) {
            return new CollectionSerializer(this._elementType, this._staticTyping, typeSerializer, this._property);
        }

        public void serializeContents(Collection<?> collection, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            JsonSerializer<Object> jsonSerializer;
            if (this._elementSerializer != null) {
                serializeContentsUsing(collection, jsonGenerator, serializerProvider, this._elementSerializer);
                return;
            }
            Iterator<?> it = collection.iterator();
            if (it.hasNext()) {
                PropertySerializerMap propertySerializerMap = this._dynamicSerializers;
                TypeSerializer typeSerializer = this._valueTypeSerializer;
                int i = 0;
                do {
                    try {
                        Object next = it.next();
                        if (next == null) {
                            serializerProvider.defaultSerializeNull(jsonGenerator);
                        } else {
                            Class<?> cls = next.getClass();
                            JsonSerializer<Object> serializerFor = propertySerializerMap.serializerFor(cls);
                            if (serializerFor == null) {
                                jsonSerializer = _findAndAddDynamic(propertySerializerMap, cls, serializerProvider);
                            } else {
                                jsonSerializer = serializerFor;
                            }
                            if (typeSerializer == null) {
                                jsonSerializer.serialize(next, jsonGenerator, serializerProvider);
                            } else {
                                jsonSerializer.serializeWithType(next, jsonGenerator, serializerProvider, typeSerializer);
                            }
                        }
                        i++;
                    } catch (Exception e) {
                        wrapAndThrow(serializerProvider, e, collection, i);
                        return;
                    }
                } while (it.hasNext());
            }
        }

        public void serializeContentsUsing(Collection<?> collection, JsonGenerator jsonGenerator, SerializerProvider serializerProvider, JsonSerializer<Object> jsonSerializer) throws IOException, JsonGenerationException {
            Iterator<?> it = collection.iterator();
            if (it.hasNext()) {
                TypeSerializer typeSerializer = this._valueTypeSerializer;
                int i = 0;
                do {
                    Object next = it.next();
                    if (next == null) {
                        try {
                            serializerProvider.defaultSerializeNull(jsonGenerator);
                        } catch (Exception e) {
                            wrapAndThrow(serializerProvider, e, collection, i);
                        }
                    } else if (typeSerializer == null) {
                        jsonSerializer.serialize(next, jsonGenerator, serializerProvider);
                    } else {
                        jsonSerializer.serializeWithType(next, jsonGenerator, serializerProvider, typeSerializer);
                    }
                    i++;
                } while (it.hasNext());
            }
        }
    }

    @JacksonStdImpl
    public static class IteratorSerializer extends AsArraySerializer<Iterator<?>> {
        public /* bridge */ /* synthetic */ void serializeContents(Object obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            serializeContents((Iterator<?>) ((Iterator) obj), jsonGenerator, serializerProvider);
        }

        public IteratorSerializer(JavaType javaType, boolean z, TypeSerializer typeSerializer, BeanProperty beanProperty) {
            super(Iterator.class, javaType, z, typeSerializer, beanProperty);
        }

        public ContainerSerializerBase<?> _withValueTypeSerializer(TypeSerializer typeSerializer) {
            return new IteratorSerializer(this._elementType, this._staticTyping, typeSerializer, this._property);
        }

        public void serializeContents(Iterator<?> it, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            JsonSerializer<Object> findValueSerializer;
            Class<?> cls;
            JsonSerializer<Object> jsonSerializer;
            Class<?> cls2 = null;
            if (it.hasNext()) {
                TypeSerializer typeSerializer = this._valueTypeSerializer;
                JsonSerializer<Object> jsonSerializer2 = null;
                do {
                    Object next = it.next();
                    if (next == null) {
                        serializerProvider.defaultSerializeNull(jsonGenerator);
                    } else {
                        Class<?> cls3 = next.getClass();
                        if (cls3 == cls2) {
                            jsonSerializer = jsonSerializer2;
                            Class<?> cls4 = cls2;
                            findValueSerializer = jsonSerializer2;
                            cls = cls4;
                        } else {
                            findValueSerializer = serializerProvider.findValueSerializer(cls3, this._property);
                            cls = cls3;
                            jsonSerializer = findValueSerializer;
                        }
                        if (typeSerializer == null) {
                            findValueSerializer.serialize(next, jsonGenerator, serializerProvider);
                            cls2 = cls;
                            jsonSerializer2 = jsonSerializer;
                        } else {
                            findValueSerializer.serializeWithType(next, jsonGenerator, serializerProvider, typeSerializer);
                            cls2 = cls;
                            jsonSerializer2 = jsonSerializer;
                        }
                    }
                } while (it.hasNext());
            }
        }
    }

    @JacksonStdImpl
    public static class IterableSerializer extends AsArraySerializer<Iterable<?>> {
        public /* bridge */ /* synthetic */ void serializeContents(Object obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            serializeContents((Iterable<?>) ((Iterable) obj), jsonGenerator, serializerProvider);
        }

        public IterableSerializer(JavaType javaType, boolean z, TypeSerializer typeSerializer, BeanProperty beanProperty) {
            super(Iterable.class, javaType, z, typeSerializer, beanProperty);
        }

        public ContainerSerializerBase<?> _withValueTypeSerializer(TypeSerializer typeSerializer) {
            return new IterableSerializer(this._elementType, this._staticTyping, typeSerializer, this._property);
        }

        public void serializeContents(Iterable<?> iterable, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            JsonSerializer<Object> findValueSerializer;
            Class<?> cls;
            JsonSerializer<Object> jsonSerializer;
            Class<?> cls2 = null;
            Iterator<?> it = iterable.iterator();
            if (it.hasNext()) {
                TypeSerializer typeSerializer = this._valueTypeSerializer;
                JsonSerializer<Object> jsonSerializer2 = null;
                do {
                    Object next = it.next();
                    if (next == null) {
                        serializerProvider.defaultSerializeNull(jsonGenerator);
                    } else {
                        Class<?> cls3 = next.getClass();
                        if (cls3 == cls2) {
                            jsonSerializer = jsonSerializer2;
                            Class<?> cls4 = cls2;
                            findValueSerializer = jsonSerializer2;
                            cls = cls4;
                        } else {
                            findValueSerializer = serializerProvider.findValueSerializer(cls3, this._property);
                            cls = cls3;
                            jsonSerializer = findValueSerializer;
                        }
                        if (typeSerializer == null) {
                            findValueSerializer.serialize(next, jsonGenerator, serializerProvider);
                            cls2 = cls;
                            jsonSerializer2 = jsonSerializer;
                        } else {
                            findValueSerializer.serializeWithType(next, jsonGenerator, serializerProvider, typeSerializer);
                            cls2 = cls;
                            jsonSerializer2 = jsonSerializer;
                        }
                    }
                } while (it.hasNext());
            }
        }
    }

    public static class EnumSetSerializer extends AsArraySerializer<EnumSet<? extends Enum<?>>> {
        public /* bridge */ /* synthetic */ void serializeContents(Object obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            serializeContents((EnumSet<? extends Enum<?>>) ((EnumSet) obj), jsonGenerator, serializerProvider);
        }

        public EnumSetSerializer(JavaType javaType, BeanProperty beanProperty) {
            super(EnumSet.class, javaType, true, null, beanProperty);
        }

        public ContainerSerializerBase<?> _withValueTypeSerializer(TypeSerializer typeSerializer) {
            return this;
        }

        public void serializeContents(EnumSet<? extends Enum<?>> enumSet, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            JsonSerializer<Object> jsonSerializer = this._elementSerializer;
            Iterator it = enumSet.iterator();
            JsonSerializer<Object> jsonSerializer2 = jsonSerializer;
            while (it.hasNext()) {
                Enum enumR = (Enum) it.next();
                if (jsonSerializer2 == null) {
                    jsonSerializer2 = serializerProvider.findValueSerializer(enumR.getDeclaringClass(), this._property);
                }
                jsonSerializer2.serialize(enumR, jsonGenerator, serializerProvider);
            }
        }
    }
}
