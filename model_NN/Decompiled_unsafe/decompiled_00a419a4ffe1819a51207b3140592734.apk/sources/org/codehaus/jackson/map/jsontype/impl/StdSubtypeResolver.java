package org.codehaus.jackson.map.jsontype.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.MapperConfig;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.AnnotatedMember;
import org.codehaus.jackson.map.jsontype.NamedType;
import org.codehaus.jackson.map.jsontype.SubtypeResolver;

public class StdSubtypeResolver extends SubtypeResolver {
    protected LinkedHashSet<NamedType> _registeredSubtypes;

    public void registerSubtypes(NamedType... namedTypeArr) {
        if (this._registeredSubtypes == null) {
            this._registeredSubtypes = new LinkedHashSet<>();
        }
        for (NamedType add : namedTypeArr) {
            this._registeredSubtypes.add(add);
        }
    }

    public void registerSubtypes(Class<?>... clsArr) {
        NamedType[] namedTypeArr = new NamedType[clsArr.length];
        int length = clsArr.length;
        for (int i = 0; i < length; i++) {
            namedTypeArr[i] = new NamedType(clsArr[i]);
        }
        registerSubtypes(namedTypeArr);
    }

    public Collection<NamedType> collectAndResolveSubtypes(AnnotatedMember annotatedMember, MapperConfig<?> mapperConfig, AnnotationIntrospector annotationIntrospector) {
        List<NamedType> findSubtypes = annotationIntrospector.findSubtypes(annotatedMember);
        if (findSubtypes == null || findSubtypes.isEmpty()) {
            return null;
        }
        return _collectAndResolve(annotatedMember, mapperConfig, annotationIntrospector, findSubtypes);
    }

    public Collection<NamedType> collectAndResolveSubtypes(AnnotatedClass annotatedClass, MapperConfig<?> mapperConfig, AnnotationIntrospector annotationIntrospector) {
        HashMap hashMap = new HashMap();
        if (this._registeredSubtypes != null) {
            Class<?> rawType = annotatedClass.getRawType();
            Iterator<NamedType> it = this._registeredSubtypes.iterator();
            while (it.hasNext()) {
                NamedType next = it.next();
                if (rawType.isAssignableFrom(next.getType())) {
                    _collectAndResolve(AnnotatedClass.constructWithoutSuperTypes(next.getType(), annotationIntrospector, mapperConfig), next, mapperConfig, annotationIntrospector, hashMap);
                }
            }
        }
        _collectAndResolve(annotatedClass, new NamedType(annotatedClass.getRawType(), null), mapperConfig, annotationIntrospector, hashMap);
        return new ArrayList(hashMap.values());
    }

    /* access modifiers changed from: protected */
    public Collection<NamedType> _collectAndResolve(AnnotatedMember annotatedMember, MapperConfig<?> mapperConfig, AnnotationIntrospector annotationIntrospector, Collection<NamedType> collection) {
        HashSet hashSet = new HashSet(collection);
        ArrayList arrayList = new ArrayList(collection);
        for (int i = 0; i < arrayList.size(); i++) {
            NamedType namedType = (NamedType) arrayList.get(i);
            AnnotatedClass constructWithoutSuperTypes = AnnotatedClass.constructWithoutSuperTypes(namedType.getType(), annotationIntrospector, mapperConfig);
            if (!namedType.hasName()) {
                namedType.setName(annotationIntrospector.findTypeName(constructWithoutSuperTypes));
            }
            List<NamedType> findSubtypes = annotationIntrospector.findSubtypes(constructWithoutSuperTypes);
            if (findSubtypes != null) {
                for (NamedType next : findSubtypes) {
                    if (hashSet.add(next)) {
                        arrayList.add(next);
                    }
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void _collectAndResolve(AnnotatedClass annotatedClass, NamedType namedType, MapperConfig<?> mapperConfig, AnnotationIntrospector annotationIntrospector, HashMap<NamedType, NamedType> hashMap) {
        NamedType namedType2;
        NamedType namedType3;
        String findTypeName;
        if (namedType.hasName() || (findTypeName = annotationIntrospector.findTypeName(annotatedClass)) == null) {
            namedType2 = namedType;
        } else {
            namedType2 = new NamedType(namedType.getType(), findTypeName);
        }
        if (!hashMap.containsKey(namedType2)) {
            hashMap.put(namedType2, namedType2);
            List<NamedType> findSubtypes = annotationIntrospector.findSubtypes(annotatedClass);
            if (findSubtypes != null && !findSubtypes.isEmpty()) {
                for (NamedType next : findSubtypes) {
                    AnnotatedClass constructWithoutSuperTypes = AnnotatedClass.constructWithoutSuperTypes(next.getType(), annotationIntrospector, mapperConfig);
                    if (!next.hasName()) {
                        namedType3 = new NamedType(next.getType(), annotationIntrospector.findTypeName(constructWithoutSuperTypes));
                    } else {
                        namedType3 = next;
                    }
                    _collectAndResolve(constructWithoutSuperTypes, namedType3, mapperConfig, annotationIntrospector, hashMap);
                }
            }
        } else if (namedType2.hasName() && !hashMap.get(namedType2).hasName()) {
            hashMap.put(namedType2, namedType2);
        }
    }
}
