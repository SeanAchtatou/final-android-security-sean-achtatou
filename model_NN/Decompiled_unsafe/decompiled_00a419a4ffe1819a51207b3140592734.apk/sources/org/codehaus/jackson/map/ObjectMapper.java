package org.codehaus.jackson.map;

import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.PrettyPrinter;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.Versioned;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.impl.JsonWriteContext;
import org.codehaus.jackson.io.SegmentedStringWriter;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.Module;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.deser.BeanDeserializerModifier;
import org.codehaus.jackson.map.deser.StdDeserializationContext;
import org.codehaus.jackson.map.deser.StdDeserializerProvider;
import org.codehaus.jackson.map.introspect.BasicClassIntrospector;
import org.codehaus.jackson.map.introspect.JacksonAnnotationIntrospector;
import org.codehaus.jackson.map.introspect.VisibilityChecker;
import org.codehaus.jackson.map.jsontype.NamedType;
import org.codehaus.jackson.map.jsontype.SubtypeResolver;
import org.codehaus.jackson.map.jsontype.TypeIdResolver;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.map.jsontype.impl.StdSubtypeResolver;
import org.codehaus.jackson.map.jsontype.impl.StdTypeResolverBuilder;
import org.codehaus.jackson.map.ser.BeanSerializerFactory;
import org.codehaus.jackson.map.ser.BeanSerializerModifier;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.StdSerializerProvider;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.JsonNodeFactory;
import org.codehaus.jackson.node.NullNode;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.node.TreeTraversingParser;
import org.codehaus.jackson.schema.JsonSchema;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.type.TypeReference;
import org.codehaus.jackson.util.ByteArrayBuilder;
import org.codehaus.jackson.util.DefaultPrettyPrinter;
import org.codehaus.jackson.util.TokenBuffer;
import org.codehaus.jackson.util.VersionUtil;

public class ObjectMapper extends ObjectCodec implements Versioned {
    protected static final AnnotationIntrospector DEFAULT_ANNOTATION_INTROSPECTOR = new JacksonAnnotationIntrospector();
    protected static final ClassIntrospector<? extends BeanDescription> DEFAULT_INTROSPECTOR = BasicClassIntrospector.instance;
    private static final JavaType JSON_NODE_TYPE = TypeFactory.type(JsonNode.class);
    protected static final VisibilityChecker<?> STD_VISIBILITY_CHECKER = VisibilityChecker.Std.defaultInstance();
    protected TypeResolverBuilder<?> _defaultTyper;
    protected DeserializationConfig _deserializationConfig;
    protected DeserializerProvider _deserializerProvider;
    protected final JsonFactory _jsonFactory;
    protected final ConcurrentHashMap<JavaType, JsonDeserializer<Object>> _rootDeserializers;
    protected SerializationConfig _serializationConfig;
    protected SerializerFactory _serializerFactory;
    protected SerializerProvider _serializerProvider;
    protected SubtypeResolver _subtypeResolver;
    protected ClassLoader _valueClassLoader;
    protected VisibilityChecker<?> _visibilityChecker;

    public enum DefaultTyping {
        JAVA_LANG_OBJECT,
        OBJECT_AND_NON_CONCRETE,
        NON_CONCRETE_AND_ARRAYS,
        NON_FINAL
    }

    public static class DefaultTypeResolverBuilder extends StdTypeResolverBuilder {
        protected final DefaultTyping _appliesFor;

        public DefaultTypeResolverBuilder(DefaultTyping defaultTyping) {
            this._appliesFor = defaultTyping;
        }

        public TypeDeserializer buildTypeDeserializer(JavaType javaType, Collection<NamedType> collection, BeanProperty beanProperty) {
            if (useForType(javaType)) {
                return super.buildTypeDeserializer(javaType, collection, beanProperty);
            }
            return null;
        }

        public TypeSerializer buildTypeSerializer(JavaType javaType, Collection<NamedType> collection, BeanProperty beanProperty) {
            if (useForType(javaType)) {
                return super.buildTypeSerializer(javaType, collection, beanProperty);
            }
            return null;
        }

        public boolean useForType(JavaType javaType) {
            JavaType javaType2;
            JavaType javaType3;
            switch (AnonymousClass2.$SwitchMap$org$codehaus$jackson$map$ObjectMapper$DefaultTyping[this._appliesFor.ordinal()]) {
                case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                    if (javaType.isArrayType()) {
                        javaType2 = javaType.getContentType();
                        break;
                    }
                case JsonWriteContext.STATUS_OK_AFTER_COLON:
                    javaType2 = javaType;
                    break;
                case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                    if (javaType.isArrayType()) {
                        javaType3 = javaType.getContentType();
                    } else {
                        javaType3 = javaType;
                    }
                    return !javaType3.isFinal();
                default:
                    if (javaType.getRawClass() == Object.class) {
                        return true;
                    }
                    return false;
            }
            return javaType2.getRawClass() == Object.class || !javaType2.isConcrete();
        }
    }

    /* renamed from: org.codehaus.jackson.map.ObjectMapper$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$codehaus$jackson$map$ObjectMapper$DefaultTyping = new int[DefaultTyping.values().length];

        static {
            try {
                $SwitchMap$org$codehaus$jackson$map$ObjectMapper$DefaultTyping[DefaultTyping.NON_CONCRETE_AND_ARRAYS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$map$ObjectMapper$DefaultTyping[DefaultTyping.OBJECT_AND_NON_CONCRETE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$map$ObjectMapper$DefaultTyping[DefaultTyping.NON_FINAL.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public ObjectMapper() {
        this(null, null, null);
    }

    public ObjectMapper(JsonFactory jsonFactory) {
        this(jsonFactory, null, null);
    }

    @Deprecated
    public ObjectMapper(SerializerFactory serializerFactory) {
        this(null, null, null);
        setSerializerFactory(serializerFactory);
    }

    public ObjectMapper(JsonFactory jsonFactory, SerializerProvider serializerProvider, DeserializerProvider deserializerProvider) {
        this(jsonFactory, serializerProvider, deserializerProvider, null, null);
    }

    public ObjectMapper(JsonFactory jsonFactory, SerializerProvider serializerProvider, DeserializerProvider deserializerProvider, SerializationConfig serializationConfig, DeserializationConfig deserializationConfig) {
        SerializerProvider serializerProvider2;
        DeserializerProvider deserializerProvider2;
        this._rootDeserializers = new ConcurrentHashMap<>(64, 0.6f, 2);
        this._jsonFactory = jsonFactory == null ? new MappingJsonFactory(this) : jsonFactory;
        this._visibilityChecker = STD_VISIBILITY_CHECKER;
        this._serializationConfig = serializationConfig != null ? serializationConfig : new SerializationConfig(DEFAULT_INTROSPECTOR, DEFAULT_ANNOTATION_INTROSPECTOR, this._visibilityChecker, null);
        this._deserializationConfig = deserializationConfig != null ? deserializationConfig : new DeserializationConfig(DEFAULT_INTROSPECTOR, DEFAULT_ANNOTATION_INTROSPECTOR, this._visibilityChecker, null);
        if (serializerProvider == null) {
            serializerProvider2 = new StdSerializerProvider();
        } else {
            serializerProvider2 = serializerProvider;
        }
        this._serializerProvider = serializerProvider2;
        if (deserializerProvider == null) {
            deserializerProvider2 = new StdDeserializerProvider();
        } else {
            deserializerProvider2 = deserializerProvider;
        }
        this._deserializerProvider = deserializerProvider2;
        this._serializerFactory = BeanSerializerFactory.instance;
    }

    public Version version() {
        return VersionUtil.versionFor(getClass());
    }

    public ObjectMapper setSerializerFactory(SerializerFactory serializerFactory) {
        this._serializerFactory = serializerFactory;
        return this;
    }

    public ObjectMapper setSerializerProvider(SerializerProvider serializerProvider) {
        this._serializerProvider = serializerProvider;
        return this;
    }

    public SerializerProvider getSerializerProvider() {
        return this._serializerProvider;
    }

    public ObjectMapper setDeserializerProvider(DeserializerProvider deserializerProvider) {
        this._deserializerProvider = deserializerProvider;
        return this;
    }

    public DeserializerProvider getDeserializerProvider() {
        return this._deserializerProvider;
    }

    public ObjectMapper setNodeFactory(JsonNodeFactory jsonNodeFactory) {
        this._deserializationConfig.setNodeFactory(jsonNodeFactory);
        return this;
    }

    public VisibilityChecker<?> getVisibilityChecker() {
        return this._visibilityChecker;
    }

    public void setVisibilityChecker(VisibilityChecker<?> visibilityChecker) {
        this._visibilityChecker = visibilityChecker;
    }

    public SubtypeResolver getSubtypeResolver() {
        if (this._subtypeResolver == null) {
            this._subtypeResolver = new StdSubtypeResolver();
        }
        return this._subtypeResolver;
    }

    public void setSubtypeResolver(SubtypeResolver subtypeResolver) {
        this._subtypeResolver = subtypeResolver;
    }

    public void registerSubtypes(Class<?>... clsArr) {
        getSubtypeResolver().registerSubtypes(clsArr);
    }

    public void registerSubtypes(NamedType... namedTypeArr) {
        getSubtypeResolver().registerSubtypes(namedTypeArr);
    }

    public void registerModule(Module module) {
        if (module.getModuleName() == null) {
            throw new IllegalArgumentException("Module without defined name");
        } else if (module.version() == null) {
            throw new IllegalArgumentException("Module without defined version");
        } else {
            module.setupModule(new Module.SetupContext() {
                public Version getMapperVersion() {
                    return ObjectMapper.this.version();
                }

                public DeserializationConfig getDeserializationConfig() {
                    return this.getDeserializationConfig();
                }

                public SerializationConfig getSerializationConfig() {
                    return this.getSerializationConfig();
                }

                public SerializationConfig getSeserializationConfig() {
                    return getSerializationConfig();
                }

                public void addSerializers(Serializers serializers) {
                    this._serializerFactory = this._serializerFactory.withAdditionalSerializers(serializers);
                }

                public void addBeanSerializerModifier(BeanSerializerModifier beanSerializerModifier) {
                    this._serializerFactory = this._serializerFactory.withSerializerModifier(beanSerializerModifier);
                }

                public void addBeanDeserializerModifier(BeanDeserializerModifier beanDeserializerModifier) {
                    this._deserializerProvider = this._deserializerProvider.withDeserializerModifier(beanDeserializerModifier);
                }

                public void addDeserializers(Deserializers deserializers) {
                    this._deserializerProvider = this._deserializerProvider.withAdditionalDeserializers(deserializers);
                }

                public void insertAnnotationIntrospector(AnnotationIntrospector annotationIntrospector) {
                    this._deserializationConfig.insertAnnotationIntrospector(annotationIntrospector);
                    this._serializationConfig.insertAnnotationIntrospector(annotationIntrospector);
                }

                public void appendAnnotationIntrospector(AnnotationIntrospector annotationIntrospector) {
                    this._deserializationConfig.appendAnnotationIntrospector(annotationIntrospector);
                    this._serializationConfig.appendAnnotationIntrospector(annotationIntrospector);
                }

                public void setMixInAnnotations(Class<?> cls, Class<?> cls2) {
                    this._deserializationConfig.addMixInAnnotations(cls, cls2);
                    this._serializationConfig.addMixInAnnotations(cls, cls2);
                }
            });
        }
    }

    public SerializationConfig getSerializationConfig() {
        return this._serializationConfig;
    }

    public SerializationConfig copySerializationConfig() {
        return this._serializationConfig.createUnshared(this._defaultTyper, this._visibilityChecker, this._subtypeResolver, null);
    }

    public ObjectMapper setSerializationConfig(SerializationConfig serializationConfig) {
        this._serializationConfig = serializationConfig;
        return this;
    }

    public ObjectMapper configure(SerializationConfig.Feature feature, boolean z) {
        this._serializationConfig.set(feature, z);
        return this;
    }

    public DeserializationConfig getDeserializationConfig() {
        return this._deserializationConfig;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.DeserializationConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver):org.codehaus.jackson.map.DeserializationConfig
     arg types: [org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver]
     candidates:
      org.codehaus.jackson.map.DeserializationConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder, org.codehaus.jackson.map.introspect.VisibilityChecker, org.codehaus.jackson.map.jsontype.SubtypeResolver):org.codehaus.jackson.map.MapperConfig
      org.codehaus.jackson.map.MapperConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver):T
      org.codehaus.jackson.map.DeserializationConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver):org.codehaus.jackson.map.DeserializationConfig */
    public DeserializationConfig copyDeserializationConfig() {
        return this._deserializationConfig.createUnshared(this._defaultTyper, this._visibilityChecker, this._subtypeResolver);
    }

    public ObjectMapper setDeserializationConfig(DeserializationConfig deserializationConfig) {
        this._deserializationConfig = deserializationConfig;
        return this;
    }

    public ObjectMapper configure(DeserializationConfig.Feature feature, boolean z) {
        this._deserializationConfig.set(feature, z);
        return this;
    }

    public JsonFactory getJsonFactory() {
        return this._jsonFactory;
    }

    public ObjectMapper configure(JsonParser.Feature feature, boolean z) {
        this._jsonFactory.configure(feature, z);
        return this;
    }

    public ObjectMapper configure(JsonGenerator.Feature feature, boolean z) {
        this._jsonFactory.configure(feature, z);
        return this;
    }

    public JsonNodeFactory getNodeFactory() {
        return this._deserializationConfig.getNodeFactory();
    }

    public ObjectMapper enableDefaultTyping() {
        return enableDefaultTyping(DefaultTyping.OBJECT_AND_NON_CONCRETE);
    }

    public ObjectMapper enableDefaultTyping(DefaultTyping defaultTyping) {
        return enableDefaultTyping(defaultTyping, JsonTypeInfo.As.WRAPPER_ARRAY);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.jsontype.TypeResolverBuilder.init(org.codehaus.jackson.annotate.JsonTypeInfo$Id, org.codehaus.jackson.map.jsontype.TypeIdResolver):T
     arg types: [org.codehaus.jackson.annotate.JsonTypeInfo$Id, ?[OBJECT, ARRAY]]
     candidates:
      org.codehaus.jackson.map.jsontype.impl.StdTypeResolverBuilder.init(org.codehaus.jackson.annotate.JsonTypeInfo$Id, org.codehaus.jackson.map.jsontype.TypeIdResolver):org.codehaus.jackson.map.jsontype.impl.StdTypeResolverBuilder
      org.codehaus.jackson.map.jsontype.TypeResolverBuilder.init(org.codehaus.jackson.annotate.JsonTypeInfo$Id, org.codehaus.jackson.map.jsontype.TypeIdResolver):T */
    public ObjectMapper enableDefaultTyping(DefaultTyping defaultTyping, JsonTypeInfo.As as) {
        return setDefaultTyping(new DefaultTypeResolverBuilder(defaultTyping).init(JsonTypeInfo.Id.CLASS, (TypeIdResolver) null).inclusion(as));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.jsontype.TypeResolverBuilder.init(org.codehaus.jackson.annotate.JsonTypeInfo$Id, org.codehaus.jackson.map.jsontype.TypeIdResolver):T
     arg types: [org.codehaus.jackson.annotate.JsonTypeInfo$Id, ?[OBJECT, ARRAY]]
     candidates:
      org.codehaus.jackson.map.jsontype.impl.StdTypeResolverBuilder.init(org.codehaus.jackson.annotate.JsonTypeInfo$Id, org.codehaus.jackson.map.jsontype.TypeIdResolver):org.codehaus.jackson.map.jsontype.impl.StdTypeResolverBuilder
      org.codehaus.jackson.map.jsontype.TypeResolverBuilder.init(org.codehaus.jackson.annotate.JsonTypeInfo$Id, org.codehaus.jackson.map.jsontype.TypeIdResolver):T */
    public ObjectMapper enableDefaultTypingAsProperty(DefaultTyping defaultTyping, String str) {
        return setDefaultTyping(new DefaultTypeResolverBuilder(defaultTyping).init(JsonTypeInfo.Id.CLASS, (TypeIdResolver) null).inclusion(JsonTypeInfo.As.PROPERTY).typeProperty(str));
    }

    public ObjectMapper disableDefaultTyping() {
        return setDefaultTyping(null);
    }

    @Deprecated
    public ObjectMapper setDefaltTyping(TypeResolverBuilder<?> typeResolverBuilder) {
        this._defaultTyper = typeResolverBuilder;
        return this;
    }

    public ObjectMapper setDefaultTyping(TypeResolverBuilder<?> typeResolverBuilder) {
        this._defaultTyper = typeResolverBuilder;
        return this;
    }

    public <T> T readValue(JsonParser jsonParser, Class cls) throws IOException, JsonParseException, JsonMappingException {
        return _readValue(copyDeserializationConfig(), jsonParser, TypeFactory.type(cls));
    }

    public <T> T readValue(JsonParser jsonParser, Class<T> cls, DeserializationConfig deserializationConfig) throws IOException, JsonParseException, JsonMappingException {
        return _readValue(deserializationConfig, jsonParser, TypeFactory.type(cls));
    }

    public <T> T readValue(JsonParser jsonParser, TypeReference<?> typeReference) throws IOException, JsonParseException, JsonMappingException {
        return _readValue(copyDeserializationConfig(), jsonParser, TypeFactory.type(typeReference));
    }

    public <T> T readValue(JsonParser jsonParser, TypeReference<?> typeReference, DeserializationConfig deserializationConfig) throws IOException, JsonParseException, JsonMappingException {
        return _readValue(deserializationConfig, jsonParser, TypeFactory.type(typeReference));
    }

    public <T> T readValue(JsonParser jsonParser, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return _readValue(copyDeserializationConfig(), jsonParser, javaType);
    }

    public <T> T readValue(JsonParser jsonParser, JavaType javaType, DeserializationConfig deserializationConfig) throws IOException, JsonParseException, JsonMappingException {
        return _readValue(deserializationConfig, jsonParser, javaType);
    }

    public JsonNode readTree(JsonParser jsonParser) throws IOException, JsonProcessingException {
        return readTree(jsonParser, copyDeserializationConfig());
    }

    public JsonNode readTree(JsonParser jsonParser, DeserializationConfig deserializationConfig) throws IOException, JsonProcessingException {
        JsonNode jsonNode = (JsonNode) _readValue(deserializationConfig, jsonParser, JSON_NODE_TYPE);
        return jsonNode == null ? NullNode.instance : jsonNode;
    }

    public JsonNode readTree(InputStream inputStream) throws IOException, JsonProcessingException {
        JsonNode jsonNode = (JsonNode) readValue(inputStream, JSON_NODE_TYPE);
        return jsonNode == null ? NullNode.instance : jsonNode;
    }

    public JsonNode readTree(Reader reader) throws IOException, JsonProcessingException {
        JsonNode jsonNode = (JsonNode) readValue(reader, JSON_NODE_TYPE);
        return jsonNode == null ? NullNode.instance : jsonNode;
    }

    public JsonNode readTree(String str) throws IOException, JsonProcessingException {
        JsonNode jsonNode = (JsonNode) readValue(str, JSON_NODE_TYPE);
        return jsonNode == null ? NullNode.instance : jsonNode;
    }

    public void writeValue(JsonGenerator jsonGenerator, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        SerializationConfig copySerializationConfig = copySerializationConfig();
        if (!copySerializationConfig.isEnabled(SerializationConfig.Feature.CLOSE_CLOSEABLE) || !(obj instanceof Closeable)) {
            this._serializerProvider.serializeValue(copySerializationConfig, jsonGenerator, obj, this._serializerFactory);
            if (copySerializationConfig.isEnabled(SerializationConfig.Feature.FLUSH_AFTER_WRITE_VALUE)) {
                jsonGenerator.flush();
                return;
            }
            return;
        }
        _writeCloseableValue(jsonGenerator, obj, copySerializationConfig);
    }

    public void writeValue(JsonGenerator jsonGenerator, Object obj, SerializationConfig serializationConfig) throws IOException, JsonGenerationException, JsonMappingException {
        if (!serializationConfig.isEnabled(SerializationConfig.Feature.CLOSE_CLOSEABLE) || !(obj instanceof Closeable)) {
            this._serializerProvider.serializeValue(serializationConfig, jsonGenerator, obj, this._serializerFactory);
            if (serializationConfig.isEnabled(SerializationConfig.Feature.FLUSH_AFTER_WRITE_VALUE)) {
                jsonGenerator.flush();
                return;
            }
            return;
        }
        _writeCloseableValue(jsonGenerator, obj, serializationConfig);
    }

    public void writeTree(JsonGenerator jsonGenerator, JsonNode jsonNode) throws IOException, JsonProcessingException {
        SerializationConfig copySerializationConfig = copySerializationConfig();
        this._serializerProvider.serializeValue(copySerializationConfig, jsonGenerator, jsonNode, this._serializerFactory);
        if (copySerializationConfig.isEnabled(SerializationConfig.Feature.FLUSH_AFTER_WRITE_VALUE)) {
            jsonGenerator.flush();
        }
    }

    public void writeTree(JsonGenerator jsonGenerator, JsonNode jsonNode, SerializationConfig serializationConfig) throws IOException, JsonProcessingException {
        this._serializerProvider.serializeValue(serializationConfig, jsonGenerator, jsonNode, this._serializerFactory);
        if (serializationConfig.isEnabled(SerializationConfig.Feature.FLUSH_AFTER_WRITE_VALUE)) {
            jsonGenerator.flush();
        }
    }

    public ObjectNode createObjectNode() {
        return this._deserializationConfig.getNodeFactory().objectNode();
    }

    public ArrayNode createArrayNode() {
        return this._deserializationConfig.getNodeFactory().arrayNode();
    }

    public JsonParser treeAsTokens(JsonNode jsonNode) {
        return new TreeTraversingParser(jsonNode, this);
    }

    public <T> T treeToValue(JsonNode jsonNode, Class<T> cls) throws IOException, JsonParseException, JsonMappingException {
        return readValue(treeAsTokens(jsonNode), cls);
    }

    public <T extends JsonNode> T valueToTree(Object obj) throws IllegalArgumentException {
        if (obj == null) {
            return null;
        }
        TokenBuffer tokenBuffer = new TokenBuffer(this);
        try {
            writeValue(tokenBuffer, obj);
            JsonParser asParser = tokenBuffer.asParser();
            T readTree = readTree(asParser);
            asParser.close();
            return readTree;
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public boolean canSerialize(Class<?> cls) {
        return this._serializerProvider.hasSerializerFor(this._serializationConfig, cls, this._serializerFactory);
    }

    public boolean canDeserialize(JavaType javaType) {
        return this._deserializerProvider.hasValueDeserializerFor(this._deserializationConfig, javaType);
    }

    public <T> T readValue(File file, Class cls) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(file), TypeFactory.type(cls));
    }

    public <T> T readValue(File file, TypeReference typeReference) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(file), TypeFactory.type(typeReference));
    }

    public <T> T readValue(File file, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(file), javaType);
    }

    public <T> T readValue(URL url, Class cls) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(url), TypeFactory.type(cls));
    }

    public <T> T readValue(URL url, TypeReference typeReference) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(url), TypeFactory.type(typeReference));
    }

    public <T> T readValue(URL url, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(url), javaType);
    }

    public <T> T readValue(String str, Class cls) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(str), TypeFactory.type(cls));
    }

    public <T> T readValue(String str, TypeReference typeReference) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(str), TypeFactory.type(typeReference));
    }

    public <T> T readValue(String str, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(str), javaType);
    }

    public <T> T readValue(Reader reader, Class cls) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(reader), TypeFactory.type(cls));
    }

    public <T> T readValue(Reader reader, TypeReference typeReference) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(reader), TypeFactory.type(typeReference));
    }

    public <T> T readValue(Reader reader, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(reader), javaType);
    }

    public <T> T readValue(InputStream inputStream, Class cls) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(inputStream), TypeFactory.type(cls));
    }

    public <T> T readValue(InputStream inputStream, TypeReference typeReference) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(inputStream), TypeFactory.type(typeReference));
    }

    public <T> T readValue(InputStream inputStream, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(inputStream), javaType);
    }

    public <T> T readValue(byte[] bArr, int i, int i2, Class<T> cls) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(bArr, i, i2), TypeFactory.type(cls));
    }

    public <T> T readValue(byte[] bArr, int i, int i2, TypeReference typeReference) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(bArr, i, i2), TypeFactory.type(typeReference));
    }

    public <T> T readValue(byte[] bArr, int i, int i2, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return _readMapAndClose(this._jsonFactory.createJsonParser(bArr, i, i2), javaType);
    }

    public <T> T readValue(JsonNode jsonNode, Class cls) throws IOException, JsonParseException, JsonMappingException {
        return _readValue(copyDeserializationConfig(), jsonNode.traverse(), TypeFactory.type(cls));
    }

    public <T> T readValue(JsonNode jsonNode, TypeReference typeReference) throws IOException, JsonParseException, JsonMappingException {
        return _readValue(copyDeserializationConfig(), jsonNode.traverse(), TypeFactory.type(typeReference));
    }

    public <T> T readValue(JsonNode jsonNode, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        return _readValue(copyDeserializationConfig(), jsonNode.traverse(), javaType);
    }

    public void writeValue(File file, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(file, JsonEncoding.UTF8), obj);
    }

    public void writeValue(OutputStream outputStream, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(outputStream, JsonEncoding.UTF8), obj);
    }

    public void writeValue(Writer writer, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(writer), obj);
    }

    public String writeValueAsString(Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        SegmentedStringWriter segmentedStringWriter = new SegmentedStringWriter(this._jsonFactory._getBufferRecycler());
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(segmentedStringWriter), obj);
        return segmentedStringWriter.getAndClear();
    }

    public byte[] writeValueAsBytes(Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        ByteArrayBuilder byteArrayBuilder = new ByteArrayBuilder(this._jsonFactory._getBufferRecycler());
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(byteArrayBuilder, JsonEncoding.UTF8), obj);
        byte[] byteArray = byteArrayBuilder.toByteArray();
        byteArrayBuilder.release();
        return byteArray;
    }

    @Deprecated
    public void writeValueUsingView(JsonGenerator jsonGenerator, Object obj, Class<?> cls) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(jsonGenerator, obj, cls);
    }

    @Deprecated
    public void writeValueUsingView(Writer writer, Object obj, Class<?> cls) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(writer), obj, cls);
    }

    @Deprecated
    public void writeValueUsingView(OutputStream outputStream, Object obj, Class<?> cls) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(outputStream, JsonEncoding.UTF8), obj, cls);
    }

    public ObjectWriter writer() {
        return new ObjectWriter(this, null, null, null);
    }

    public ObjectWriter viewWriter(Class<?> cls) {
        return new ObjectWriter(this, cls, null, null);
    }

    public ObjectWriter typedWriter(Class<?> cls) {
        return new ObjectWriter(this, null, cls == null ? null : TypeFactory.type(cls), null);
    }

    public ObjectWriter typedWriter(JavaType javaType) {
        return new ObjectWriter(this, null, javaType, null);
    }

    public ObjectWriter typedWriter(TypeReference<?> typeReference) {
        return new ObjectWriter(this, null, typeReference == null ? null : TypeFactory.type(typeReference), null);
    }

    public ObjectWriter prettyPrintingWriter(PrettyPrinter prettyPrinter) {
        PrettyPrinter prettyPrinter2;
        if (prettyPrinter == null) {
            prettyPrinter2 = ObjectWriter.NULL_PRETTY_PRINTER;
        } else {
            prettyPrinter2 = prettyPrinter;
        }
        return new ObjectWriter(this, null, null, prettyPrinter2);
    }

    public ObjectWriter defaultPrettyPrintingWriter() {
        return new ObjectWriter(this, null, null, _defaultPrettyPrinter());
    }

    public ObjectWriter filteredWriter(FilterProvider filterProvider) {
        return new ObjectWriter(this, filterProvider);
    }

    public ObjectReader reader() {
        return new ObjectReader(this, null, null);
    }

    public ObjectReader updatingReader(Object obj) {
        return new ObjectReader(this, TypeFactory.type(obj.getClass()), obj);
    }

    public ObjectReader reader(JavaType javaType) {
        return new ObjectReader(this, javaType, null);
    }

    public ObjectReader reader(Class<?> cls) {
        return reader(TypeFactory.type(cls));
    }

    public ObjectReader reader(TypeReference<?> typeReference) {
        return reader(TypeFactory.type(typeReference));
    }

    public ObjectReader reader(JsonNodeFactory jsonNodeFactory) {
        return new ObjectReader(this, null, null).withNodeFactory(jsonNodeFactory);
    }

    public <T> T convertValue(Object obj, Class<T> cls) throws IllegalArgumentException {
        return _convert(obj, TypeFactory.type(cls));
    }

    public <T> T convertValue(Object obj, TypeReference typeReference) throws IllegalArgumentException {
        return _convert(obj, TypeFactory.type(typeReference));
    }

    public <T> T convertValue(Object obj, JavaType javaType) throws IllegalArgumentException {
        return _convert(obj, javaType);
    }

    /* access modifiers changed from: protected */
    public Object _convert(Object obj, JavaType javaType) throws IllegalArgumentException {
        if (obj == null) {
            return null;
        }
        TokenBuffer tokenBuffer = new TokenBuffer(this);
        try {
            writeValue(tokenBuffer, obj);
            JsonParser asParser = tokenBuffer.asParser();
            Object readValue = readValue(asParser, javaType);
            asParser.close();
            return readValue;
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public JsonSchema generateJsonSchema(Class<?> cls) throws JsonMappingException {
        return generateJsonSchema(cls, copySerializationConfig());
    }

    public JsonSchema generateJsonSchema(Class<?> cls, SerializationConfig serializationConfig) throws JsonMappingException {
        return this._serializerProvider.generateJsonSchema(cls, serializationConfig, this._serializerFactory);
    }

    /* access modifiers changed from: protected */
    public PrettyPrinter _defaultPrettyPrinter() {
        return new DefaultPrettyPrinter();
    }

    /* access modifiers changed from: protected */
    public final void _configAndWriteValue(JsonGenerator jsonGenerator, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        SerializationConfig copySerializationConfig = copySerializationConfig();
        if (copySerializationConfig.isEnabled(SerializationConfig.Feature.INDENT_OUTPUT)) {
            jsonGenerator.useDefaultPrettyPrinter();
        }
        if (!copySerializationConfig.isEnabled(SerializationConfig.Feature.CLOSE_CLOSEABLE) || !(obj instanceof Closeable)) {
            boolean z = false;
            try {
                this._serializerProvider.serializeValue(copySerializationConfig, jsonGenerator, obj, this._serializerFactory);
                try {
                    jsonGenerator.close();
                } catch (Throwable th) {
                    Throwable th2 = th;
                    z = true;
                    th = th2;
                }
            } catch (Throwable th3) {
                th = th3;
                if (!z) {
                    try {
                        jsonGenerator.close();
                    } catch (IOException e) {
                    }
                }
                throw th;
            }
        } else {
            _configAndWriteCloseable(jsonGenerator, obj, copySerializationConfig);
        }
    }

    /* access modifiers changed from: protected */
    public final void _configAndWriteValue(JsonGenerator jsonGenerator, Object obj, Class<?> cls) throws IOException, JsonGenerationException, JsonMappingException {
        SerializationConfig copySerializationConfig = copySerializationConfig();
        if (copySerializationConfig.isEnabled(SerializationConfig.Feature.INDENT_OUTPUT)) {
            jsonGenerator.useDefaultPrettyPrinter();
        }
        copySerializationConfig.setSerializationView(cls);
        if (!copySerializationConfig.isEnabled(SerializationConfig.Feature.CLOSE_CLOSEABLE) || !(obj instanceof Closeable)) {
            boolean z = false;
            try {
                this._serializerProvider.serializeValue(copySerializationConfig, jsonGenerator, obj, this._serializerFactory);
                try {
                    jsonGenerator.close();
                } catch (Throwable th) {
                    Throwable th2 = th;
                    z = true;
                    th = th2;
                }
            } catch (Throwable th3) {
                th = th3;
                if (!z) {
                    try {
                        jsonGenerator.close();
                    } catch (IOException e) {
                    }
                }
                throw th;
            }
        } else {
            _configAndWriteCloseable(jsonGenerator, obj, copySerializationConfig);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x001a A[SYNTHETIC, Splitter:B:11:0x001a] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x001f A[SYNTHETIC, Splitter:B:14:0x001f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void _configAndWriteCloseable(org.codehaus.jackson.JsonGenerator r7, java.lang.Object r8, org.codehaus.jackson.map.SerializationConfig r9) throws java.io.IOException, org.codehaus.jackson.JsonGenerationException, org.codehaus.jackson.map.JsonMappingException {
        /*
            r6 = this;
            r4 = 0
            r0 = r8
            java.io.Closeable r0 = (java.io.Closeable) r0
            r1 = r0
            org.codehaus.jackson.map.SerializerProvider r2 = r6._serializerProvider     // Catch:{ all -> 0x0013 }
            org.codehaus.jackson.map.SerializerFactory r3 = r6._serializerFactory     // Catch:{ all -> 0x0013 }
            r2.serializeValue(r9, r7, r8, r3)     // Catch:{ all -> 0x0013 }
            r7.close()     // Catch:{ all -> 0x0027 }
            r1.close()     // Catch:{ all -> 0x002d }
            return
        L_0x0013:
            r2 = move-exception
            r3 = r7
            r5 = r1
            r1 = r2
            r2 = r5
        L_0x0018:
            if (r3 == 0) goto L_0x001d
            r3.close()     // Catch:{ IOException -> 0x0023 }
        L_0x001d:
            if (r2 == 0) goto L_0x0022
            r2.close()     // Catch:{ IOException -> 0x0025 }
        L_0x0022:
            throw r1
        L_0x0023:
            r3 = move-exception
            goto L_0x001d
        L_0x0025:
            r2 = move-exception
            goto L_0x0022
        L_0x0027:
            r2 = move-exception
            r3 = r4
            r5 = r1
            r1 = r2
            r2 = r5
            goto L_0x0018
        L_0x002d:
            r1 = move-exception
            r2 = r4
            r3 = r4
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.ObjectMapper._configAndWriteCloseable(org.codehaus.jackson.JsonGenerator, java.lang.Object, org.codehaus.jackson.map.SerializationConfig):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0021 A[SYNTHETIC, Splitter:B:12:0x0021] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void _writeCloseableValue(org.codehaus.jackson.JsonGenerator r6, java.lang.Object r7, org.codehaus.jackson.map.SerializationConfig r8) throws java.io.IOException, org.codehaus.jackson.JsonGenerationException, org.codehaus.jackson.map.JsonMappingException {
        /*
            r5 = this;
            r0 = r7
            java.io.Closeable r0 = (java.io.Closeable) r0
            r1 = r0
            org.codehaus.jackson.map.SerializerProvider r2 = r5._serializerProvider     // Catch:{ all -> 0x001b }
            org.codehaus.jackson.map.SerializerFactory r3 = r5._serializerFactory     // Catch:{ all -> 0x001b }
            r2.serializeValue(r8, r6, r7, r3)     // Catch:{ all -> 0x001b }
            org.codehaus.jackson.map.SerializationConfig$Feature r2 = org.codehaus.jackson.map.SerializationConfig.Feature.FLUSH_AFTER_WRITE_VALUE     // Catch:{ all -> 0x001b }
            boolean r2 = r8.isEnabled(r2)     // Catch:{ all -> 0x001b }
            if (r2 == 0) goto L_0x0016
            r6.flush()     // Catch:{ all -> 0x001b }
        L_0x0016:
            r2 = 0
            r1.close()     // Catch:{ all -> 0x0027 }
            return
        L_0x001b:
            r2 = move-exception
            r4 = r2
            r2 = r1
            r1 = r4
        L_0x001f:
            if (r2 == 0) goto L_0x0024
            r2.close()     // Catch:{ IOException -> 0x0025 }
        L_0x0024:
            throw r1
        L_0x0025:
            r2 = move-exception
            goto L_0x0024
        L_0x0027:
            r1 = move-exception
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.ObjectMapper._writeCloseableValue(org.codehaus.jackson.JsonGenerator, java.lang.Object, org.codehaus.jackson.map.SerializationConfig):void");
    }

    /* access modifiers changed from: protected */
    public Object _readValue(DeserializationConfig deserializationConfig, JsonParser jsonParser, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        Object obj;
        JsonToken _initForReading = _initForReading(jsonParser);
        if (_initForReading == JsonToken.VALUE_NULL || _initForReading == JsonToken.END_ARRAY || _initForReading == JsonToken.END_OBJECT) {
            obj = null;
        } else {
            obj = _findRootDeserializer(deserializationConfig, javaType).deserialize(jsonParser, _createDeserializationContext(jsonParser, deserializationConfig));
        }
        jsonParser.clearCurrentToken();
        return obj;
    }

    /* access modifiers changed from: protected */
    public Object _readMapAndClose(JsonParser jsonParser, JavaType javaType) throws IOException, JsonParseException, JsonMappingException {
        Object obj;
        try {
            JsonToken _initForReading = _initForReading(jsonParser);
            if (_initForReading == JsonToken.VALUE_NULL || _initForReading == JsonToken.END_ARRAY || _initForReading == JsonToken.END_OBJECT) {
                obj = null;
            } else {
                DeserializationConfig copyDeserializationConfig = copyDeserializationConfig();
                obj = _findRootDeserializer(copyDeserializationConfig, javaType).deserialize(jsonParser, _createDeserializationContext(jsonParser, copyDeserializationConfig));
            }
            jsonParser.clearCurrentToken();
            return obj;
        } finally {
            try {
                jsonParser.close();
            } catch (IOException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public JsonToken _initForReading(JsonParser jsonParser) throws IOException, JsonParseException, JsonMappingException {
        JsonToken currentToken = jsonParser.getCurrentToken();
        if (currentToken != null || (currentToken = jsonParser.nextToken()) != null) {
            return currentToken;
        }
        throw new EOFException("No content to map to Object due to end of input");
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _findRootDeserializer(DeserializationConfig deserializationConfig, JavaType javaType) throws JsonMappingException {
        JsonDeserializer<Object> jsonDeserializer = this._rootDeserializers.get(javaType);
        if (jsonDeserializer == null) {
            jsonDeserializer = this._deserializerProvider.findTypedValueDeserializer(deserializationConfig, javaType, null);
            if (jsonDeserializer == null) {
                throw new JsonMappingException("Can not find a deserializer for type " + javaType);
            }
            this._rootDeserializers.put(javaType, jsonDeserializer);
        }
        return jsonDeserializer;
    }

    /* access modifiers changed from: protected */
    public DeserializationContext _createDeserializationContext(JsonParser jsonParser, DeserializationConfig deserializationConfig) {
        return new StdDeserializationContext(deserializationConfig, jsonParser, this._deserializerProvider);
    }
}
