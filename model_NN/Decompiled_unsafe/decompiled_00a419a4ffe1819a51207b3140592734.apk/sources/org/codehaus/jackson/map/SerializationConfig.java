package org.codehaus.jackson.map;

import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.VisibilityChecker;
import org.codehaus.jackson.map.jsontype.SubtypeResolver;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.map.jsontype.impl.StdSubtypeResolver;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.type.ClassKey;
import org.codehaus.jackson.map.util.StdDateFormat;
import org.codehaus.jackson.type.JavaType;

public class SerializationConfig implements MapperConfig<SerializationConfig> {
    protected static final int DEFAULT_FEATURE_FLAGS = Feature.collectDefaults();
    protected AnnotationIntrospector _annotationIntrospector;
    protected ClassIntrospector<? extends BeanDescription> _classIntrospector;
    protected DateFormat _dateFormat = StdDateFormat.instance;
    protected int _featureFlags = DEFAULT_FEATURE_FLAGS;
    protected FilterProvider _filterProvider;
    protected HashMap<ClassKey, Class<?>> _mixInAnnotations;
    protected boolean _mixInAnnotationsShared;
    protected JsonSerialize.Inclusion _serializationInclusion = null;
    protected Class<?> _serializationView;
    protected SubtypeResolver _subtypeResolver;
    protected final TypeResolverBuilder<?> _typer;
    protected VisibilityChecker<?> _visibilityChecker;

    public enum Feature {
        USE_ANNOTATIONS(true),
        AUTO_DETECT_GETTERS(true),
        AUTO_DETECT_IS_GETTERS(true),
        AUTO_DETECT_FIELDS(true),
        CAN_OVERRIDE_ACCESS_MODIFIERS(true),
        WRITE_NULL_PROPERTIES(true),
        USE_STATIC_TYPING(false),
        DEFAULT_VIEW_INCLUSION(true),
        WRAP_ROOT_VALUE(false),
        INDENT_OUTPUT(false),
        FAIL_ON_EMPTY_BEANS(true),
        WRAP_EXCEPTIONS(true),
        CLOSE_CLOSEABLE(false),
        FLUSH_AFTER_WRITE_VALUE(true),
        WRITE_DATES_AS_TIMESTAMPS(true),
        WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS(false),
        WRITE_ENUMS_USING_TO_STRING(false),
        WRITE_NULL_MAP_VALUES(true);
        
        final boolean _defaultState;

        public static int collectDefaults() {
            int i = 0;
            for (Feature feature : values()) {
                if (feature.enabledByDefault()) {
                    i |= feature.getMask();
                }
            }
            return i;
        }

        private Feature(boolean z) {
            this._defaultState = z;
        }

        public final boolean enabledByDefault() {
            return this._defaultState;
        }

        public final int getMask() {
            return 1 << ordinal();
        }
    }

    public SerializationConfig(ClassIntrospector<? extends BeanDescription> classIntrospector, AnnotationIntrospector annotationIntrospector, VisibilityChecker<?> visibilityChecker, SubtypeResolver subtypeResolver) {
        this._classIntrospector = classIntrospector;
        this._annotationIntrospector = annotationIntrospector;
        this._typer = null;
        this._visibilityChecker = visibilityChecker;
        this._subtypeResolver = subtypeResolver;
        this._filterProvider = null;
    }

    protected SerializationConfig(SerializationConfig serializationConfig, HashMap<ClassKey, Class<?>> hashMap, TypeResolverBuilder<?> typeResolverBuilder, VisibilityChecker<?> visibilityChecker, SubtypeResolver subtypeResolver, FilterProvider filterProvider) {
        this._classIntrospector = serializationConfig._classIntrospector;
        this._annotationIntrospector = serializationConfig._annotationIntrospector;
        this._featureFlags = serializationConfig._featureFlags;
        this._dateFormat = serializationConfig._dateFormat;
        this._serializationInclusion = serializationConfig._serializationInclusion;
        this._serializationView = serializationConfig._serializationView;
        this._mixInAnnotations = hashMap;
        this._typer = typeResolverBuilder;
        this._visibilityChecker = visibilityChecker;
        this._subtypeResolver = subtypeResolver;
        this._filterProvider = filterProvider;
    }

    protected SerializationConfig(SerializationConfig serializationConfig, FilterProvider filterProvider) {
        this._classIntrospector = serializationConfig._classIntrospector;
        this._annotationIntrospector = serializationConfig._annotationIntrospector;
        this._featureFlags = serializationConfig._featureFlags;
        this._dateFormat = serializationConfig._dateFormat;
        this._serializationInclusion = serializationConfig._serializationInclusion;
        this._serializationView = serializationConfig._serializationView;
        this._mixInAnnotations = serializationConfig._mixInAnnotations;
        this._typer = serializationConfig._typer;
        this._visibilityChecker = serializationConfig._visibilityChecker;
        this._subtypeResolver = serializationConfig._subtypeResolver;
        this._filterProvider = filterProvider;
    }

    public SerializationConfig withFilters(FilterProvider filterProvider) {
        return new SerializationConfig(this, filterProvider);
    }

    public SerializationConfig createUnshared(TypeResolverBuilder<?> typeResolverBuilder, VisibilityChecker<?> visibilityChecker, SubtypeResolver subtypeResolver, FilterProvider filterProvider) {
        HashMap<ClassKey, Class<?>> hashMap = this._mixInAnnotations;
        this._mixInAnnotationsShared = true;
        return new SerializationConfig(this, hashMap, typeResolverBuilder, visibilityChecker, subtypeResolver, filterProvider);
    }

    public void fromAnnotations(Class<?> cls) {
        AnnotatedClass construct = AnnotatedClass.construct(cls, this._annotationIntrospector, null);
        this._visibilityChecker = this._annotationIntrospector.findAutoDetectVisibility(construct, this._visibilityChecker);
        JsonSerialize.Inclusion findSerializationInclusion = this._annotationIntrospector.findSerializationInclusion(construct, null);
        if (findSerializationInclusion != this._serializationInclusion) {
            setSerializationInclusion(findSerializationInclusion);
        }
        JsonSerialize.Typing findSerializationTyping = this._annotationIntrospector.findSerializationTyping(construct);
        if (findSerializationTyping != null) {
            set(Feature.USE_STATIC_TYPING, findSerializationTyping == JsonSerialize.Typing.STATIC);
        }
    }

    public SerializationConfig createUnshared(TypeResolverBuilder<?> typeResolverBuilder, VisibilityChecker<?> visibilityChecker, SubtypeResolver subtypeResolver) {
        HashMap<ClassKey, Class<?>> hashMap = this._mixInAnnotations;
        this._mixInAnnotationsShared = true;
        return new SerializationConfig(this, hashMap, typeResolverBuilder, visibilityChecker, subtypeResolver, null);
    }

    public void setIntrospector(ClassIntrospector<? extends BeanDescription> classIntrospector) {
        this._classIntrospector = classIntrospector;
    }

    public AnnotationIntrospector getAnnotationIntrospector() {
        if (isEnabled(Feature.USE_ANNOTATIONS)) {
            return this._annotationIntrospector;
        }
        return AnnotationIntrospector.nopInstance();
    }

    public void setAnnotationIntrospector(AnnotationIntrospector annotationIntrospector) {
        this._annotationIntrospector = annotationIntrospector;
    }

    public void insertAnnotationIntrospector(AnnotationIntrospector annotationIntrospector) {
        this._annotationIntrospector = AnnotationIntrospector.Pair.create(annotationIntrospector, this._annotationIntrospector);
    }

    public void appendAnnotationIntrospector(AnnotationIntrospector annotationIntrospector) {
        this._annotationIntrospector = AnnotationIntrospector.Pair.create(this._annotationIntrospector, annotationIntrospector);
    }

    public void setMixInAnnotations(Map<Class<?>, Class<?>> map) {
        HashMap<ClassKey, Class<?>> hashMap = null;
        if (map != null && map.size() > 0) {
            HashMap<ClassKey, Class<?>> hashMap2 = new HashMap<>(map.size());
            for (Map.Entry next : map.entrySet()) {
                hashMap2.put(new ClassKey((Class) next.getKey()), next.getValue());
            }
            hashMap = hashMap2;
        }
        this._mixInAnnotationsShared = false;
        this._mixInAnnotations = hashMap;
    }

    public void addMixInAnnotations(Class<?> cls, Class<?> cls2) {
        if (this._mixInAnnotations == null || this._mixInAnnotationsShared) {
            this._mixInAnnotationsShared = false;
            this._mixInAnnotations = new HashMap<>();
        }
        this._mixInAnnotations.put(new ClassKey(cls), cls2);
    }

    public Class<?> findMixInClassFor(Class<?> cls) {
        if (this._mixInAnnotations == null) {
            return null;
        }
        return this._mixInAnnotations.get(new ClassKey(cls));
    }

    public DateFormat getDateFormat() {
        return this._dateFormat;
    }

    public void setDateFormat(DateFormat dateFormat) {
        this._dateFormat = dateFormat;
        set(Feature.WRITE_DATES_AS_TIMESTAMPS, dateFormat == null);
    }

    public TypeResolverBuilder<?> getDefaultTyper(JavaType javaType) {
        return this._typer;
    }

    public VisibilityChecker<?> getDefaultVisibilityChecker() {
        return this._visibilityChecker;
    }

    public <T extends BeanDescription> T introspectClassAnnotations(Class<?> cls) {
        return this._classIntrospector.forClassAnnotations(this, cls, this);
    }

    public <T extends BeanDescription> T introspectDirectClassAnnotations(Class<?> cls) {
        return this._classIntrospector.forDirectClassAnnotations(this, cls, this);
    }

    public void enable(Feature feature) {
        this._featureFlags |= feature.getMask();
    }

    public void disable(Feature feature) {
        this._featureFlags &= feature.getMask() ^ -1;
    }

    public void set(Feature feature, boolean z) {
        if (z) {
            enable(feature);
        } else {
            disable(feature);
        }
    }

    public final boolean isEnabled(Feature feature) {
        return (this._featureFlags & feature.getMask()) != 0;
    }

    public <T extends BeanDescription> T introspect(JavaType javaType) {
        return this._classIntrospector.forSerialization(this, javaType, this);
    }

    public SubtypeResolver getSubtypeResolver() {
        if (this._subtypeResolver == null) {
            this._subtypeResolver = new StdSubtypeResolver();
        }
        return this._subtypeResolver;
    }

    public void setSubtypeResolver(SubtypeResolver subtypeResolver) {
        this._subtypeResolver = subtypeResolver;
    }

    public Class<?> getSerializationView() {
        return this._serializationView;
    }

    public JsonSerialize.Inclusion getSerializationInclusion() {
        if (this._serializationInclusion != null) {
            return this._serializationInclusion;
        }
        return isEnabled(Feature.WRITE_NULL_PROPERTIES) ? JsonSerialize.Inclusion.ALWAYS : JsonSerialize.Inclusion.NON_NULL;
    }

    public void setSerializationInclusion(JsonSerialize.Inclusion inclusion) {
        this._serializationInclusion = inclusion;
        if (inclusion == JsonSerialize.Inclusion.NON_NULL) {
            disable(Feature.WRITE_NULL_PROPERTIES);
        } else {
            enable(Feature.WRITE_NULL_PROPERTIES);
        }
    }

    public void setSerializationView(Class<?> cls) {
        this._serializationView = cls;
    }

    public FilterProvider getFilterProvider() {
        return this._filterProvider;
    }

    public String toString() {
        return "[SerializationConfig: flags=0x" + Integer.toHexString(this._featureFlags) + "]";
    }
}
