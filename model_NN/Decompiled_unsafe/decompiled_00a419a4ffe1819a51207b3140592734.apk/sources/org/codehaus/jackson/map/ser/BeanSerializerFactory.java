package org.codehaus.jackson.map.ser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.BeanDescription;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.SerializerFactory;
import org.codehaus.jackson.map.Serializers;
import org.codehaus.jackson.map.TypeSerializer;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.AnnotatedField;
import org.codehaus.jackson.map.introspect.AnnotatedMember;
import org.codehaus.jackson.map.introspect.AnnotatedMethod;
import org.codehaus.jackson.map.introspect.BasicBeanDescription;
import org.codehaus.jackson.map.introspect.VisibilityChecker;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.map.type.TypeBindings;
import org.codehaus.jackson.map.util.ArrayBuilders;
import org.codehaus.jackson.map.util.ClassUtil;
import org.codehaus.jackson.type.JavaType;

public class BeanSerializerFactory extends BasicSerializerFactory {
    public static final BeanSerializerFactory instance = new BeanSerializerFactory(null);
    protected final SerializerFactory.Config _factoryConfig;

    public static class ConfigImpl extends SerializerFactory.Config {
        protected static final BeanSerializerModifier[] NO_MODIFIERS = new BeanSerializerModifier[0];
        protected static final Serializers[] NO_SERIALIZERS = new Serializers[0];
        protected final Serializers[] _additionalSerializers;
        protected final BeanSerializerModifier[] _modifiers;

        public ConfigImpl() {
            this(null, null);
        }

        protected ConfigImpl(Serializers[] serializersArr, BeanSerializerModifier[] beanSerializerModifierArr) {
            Serializers[] serializersArr2;
            BeanSerializerModifier[] beanSerializerModifierArr2;
            if (serializersArr == null) {
                serializersArr2 = NO_SERIALIZERS;
            } else {
                serializersArr2 = serializersArr;
            }
            this._additionalSerializers = serializersArr2;
            if (beanSerializerModifierArr == null) {
                beanSerializerModifierArr2 = NO_MODIFIERS;
            } else {
                beanSerializerModifierArr2 = beanSerializerModifierArr;
            }
            this._modifiers = beanSerializerModifierArr2;
        }

        public SerializerFactory.Config withAdditionalSerializers(Serializers serializers) {
            if (serializers != null) {
                return new ConfigImpl((Serializers[]) ArrayBuilders.insertInList(this._additionalSerializers, serializers), this._modifiers);
            }
            throw new IllegalArgumentException("Can not pass null Serializers");
        }

        public SerializerFactory.Config withSerializerModifier(BeanSerializerModifier beanSerializerModifier) {
            if (beanSerializerModifier == null) {
                throw new IllegalArgumentException("Can not pass null modifier");
            }
            return new ConfigImpl(this._additionalSerializers, (BeanSerializerModifier[]) ArrayBuilders.insertInList(this._modifiers, beanSerializerModifier));
        }

        public boolean hasSerializers() {
            return this._additionalSerializers.length > 0;
        }

        public boolean hasSerializerModifiers() {
            return this._modifiers.length > 0;
        }

        public Iterable<Serializers> serializers() {
            return ArrayBuilders.arrayAsIterable(this._additionalSerializers);
        }

        public Iterable<BeanSerializerModifier> serializerModifiers() {
            return ArrayBuilders.arrayAsIterable(this._modifiers);
        }
    }

    @Deprecated
    protected BeanSerializerFactory() {
        this(null);
    }

    protected BeanSerializerFactory(SerializerFactory.Config config) {
        SerializerFactory.Config config2;
        if (config == null) {
            config2 = new ConfigImpl();
        } else {
            config2 = config;
        }
        this._factoryConfig = config2;
    }

    public SerializerFactory.Config getConfig() {
        return this._factoryConfig;
    }

    public SerializerFactory withConfig(SerializerFactory.Config config) {
        if (this._factoryConfig == config) {
            return this;
        }
        if (getClass() == BeanSerializerFactory.class) {
            return new BeanSerializerFactory(config);
        }
        throw new IllegalStateException("Subtype of BeanSerializerFactory (" + getClass().getName() + ") has not properly overridden method 'withAdditionalSerializers': can not instantiate subtype with " + "additional serializer definitions");
    }

    public JsonSerializer<Object> createSerializer(SerializationConfig serializationConfig, JavaType javaType, BeanProperty beanProperty) {
        BasicBeanDescription basicBeanDescription = (BasicBeanDescription) serializationConfig.introspect(javaType);
        JsonSerializer<?> findSerializerFromAnnotation = findSerializerFromAnnotation(serializationConfig, basicBeanDescription.getClassInfo(), beanProperty);
        if (findSerializerFromAnnotation == null && (findSerializerFromAnnotation = _findFirstSerializer(this._factoryConfig.serializers(), serializationConfig, javaType, basicBeanDescription, beanProperty)) == null && (findSerializerFromAnnotation = super.findSerializerByLookup(javaType, serializationConfig, basicBeanDescription, beanProperty)) == null && (findSerializerFromAnnotation = super.findSerializerByPrimaryType(javaType, serializationConfig, basicBeanDescription, beanProperty)) == null && (findSerializerFromAnnotation = findBeanSerializer(serializationConfig, javaType, basicBeanDescription, beanProperty)) == null) {
            return super.findSerializerByAddonType(serializationConfig, javaType, basicBeanDescription, beanProperty);
        }
        return findSerializerFromAnnotation;
    }

    private static JsonSerializer<?> _findFirstSerializer(Iterable<Serializers> iterable, SerializationConfig serializationConfig, JavaType javaType, BeanDescription beanDescription, BeanProperty beanProperty) {
        for (Serializers findSerializer : iterable) {
            JsonSerializer<?> findSerializer2 = findSerializer.findSerializer(serializationConfig, javaType, beanDescription, beanProperty);
            if (findSerializer2 != null) {
                return findSerializer2;
            }
        }
        return null;
    }

    public JsonSerializer<Object> findBeanSerializer(SerializationConfig serializationConfig, JavaType javaType, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        if (!isPotentialBeanType(javaType.getRawClass())) {
            return null;
        }
        JsonSerializer<?> constructBeanSerializer = constructBeanSerializer(serializationConfig, basicBeanDescription, beanProperty);
        if (!this._factoryConfig.hasSerializerModifiers()) {
            return constructBeanSerializer;
        }
        for (BeanSerializerModifier modifySerializer : this._factoryConfig.serializerModifiers()) {
            constructBeanSerializer = modifySerializer.modifySerializer(serializationConfig, basicBeanDescription, constructBeanSerializer);
        }
        return constructBeanSerializer;
    }

    public TypeSerializer findPropertyTypeSerializer(JavaType javaType, SerializationConfig serializationConfig, AnnotatedMember annotatedMember, BeanProperty beanProperty) {
        AnnotationIntrospector annotationIntrospector = serializationConfig.getAnnotationIntrospector();
        TypeResolverBuilder<?> findPropertyTypeResolver = annotationIntrospector.findPropertyTypeResolver(annotatedMember, javaType);
        if (findPropertyTypeResolver == null) {
            return createTypeSerializer(serializationConfig, javaType, beanProperty);
        }
        return findPropertyTypeResolver.buildTypeSerializer(javaType, serializationConfig.getSubtypeResolver().collectAndResolveSubtypes(annotatedMember, serializationConfig, annotationIntrospector), beanProperty);
    }

    public TypeSerializer findPropertyContentTypeSerializer(JavaType javaType, SerializationConfig serializationConfig, AnnotatedMember annotatedMember, BeanProperty beanProperty) {
        JavaType contentType = javaType.getContentType();
        AnnotationIntrospector annotationIntrospector = serializationConfig.getAnnotationIntrospector();
        TypeResolverBuilder<?> findPropertyContentTypeResolver = annotationIntrospector.findPropertyContentTypeResolver(annotatedMember, javaType);
        if (findPropertyContentTypeResolver == null) {
            return createTypeSerializer(serializationConfig, contentType, beanProperty);
        }
        return findPropertyContentTypeResolver.buildTypeSerializer(contentType, serializationConfig.getSubtypeResolver().collectAndResolveSubtypes(annotatedMember, serializationConfig, annotationIntrospector), beanProperty);
    }

    /* access modifiers changed from: protected */
    public JsonSerializer<Object> constructBeanSerializer(SerializationConfig serializationConfig, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) {
        List<BeanPropertyWriter> list;
        List<BeanPropertyWriter> list2;
        List<BeanPropertyWriter> list3;
        if (basicBeanDescription.getBeanClass() == Object.class) {
            throw new IllegalArgumentException("Can not create bean serializer for Object.class");
        }
        BeanSerializerBuilder constructBeanSerializerBuilder = constructBeanSerializerBuilder(basicBeanDescription);
        List<BeanPropertyWriter> findBeanProperties = findBeanProperties(serializationConfig, basicBeanDescription);
        AnnotatedMethod findAnyGetter = basicBeanDescription.findAnyGetter();
        if (this._factoryConfig.hasSerializerModifiers()) {
            if (findBeanProperties == null) {
                findBeanProperties = new ArrayList<>();
            }
            Iterator<BeanSerializerModifier> it = this._factoryConfig.serializerModifiers().iterator();
            while (true) {
                list3 = findBeanProperties;
                if (!it.hasNext()) {
                    break;
                }
                findBeanProperties = it.next().changeProperties(serializationConfig, basicBeanDescription, list3);
            }
            findBeanProperties = list3;
        }
        if (findBeanProperties != null && findBeanProperties.size() != 0) {
            list = sortBeanProperties(serializationConfig, basicBeanDescription, filterBeanProperties(serializationConfig, basicBeanDescription, findBeanProperties));
        } else if (findAnyGetter != null) {
            list = Collections.emptyList();
        } else if (basicBeanDescription.hasKnownClassAnnotations()) {
            return constructBeanSerializerBuilder.createDummy();
        } else {
            return null;
        }
        if (this._factoryConfig.hasSerializerModifiers()) {
            Iterator<BeanSerializerModifier> it2 = this._factoryConfig.serializerModifiers().iterator();
            while (true) {
                list2 = list;
                if (!it2.hasNext()) {
                    break;
                }
                list = it2.next().orderProperties(serializationConfig, basicBeanDescription, list2);
            }
            list = list2;
        }
        constructBeanSerializerBuilder.setProperties(list);
        constructBeanSerializerBuilder.setFilterId(findFilterId(serializationConfig, basicBeanDescription));
        if (findAnyGetter != null) {
            JavaType type = findAnyGetter.getType(basicBeanDescription.bindingsForBeanType());
            constructBeanSerializerBuilder.setAnyGetter(new AnyGetterWriter(findAnyGetter, MapSerializer.construct(null, type, serializationConfig.isEnabled(SerializationConfig.Feature.USE_STATIC_TYPING), createTypeSerializer(serializationConfig, type.getContentType(), beanProperty), beanProperty)));
        }
        processViews(serializationConfig, constructBeanSerializerBuilder);
        if (this._factoryConfig.hasSerializerModifiers()) {
            for (BeanSerializerModifier updateBuilder : this._factoryConfig.serializerModifiers()) {
                constructBeanSerializerBuilder = updateBuilder.updateBuilder(serializationConfig, basicBeanDescription, constructBeanSerializerBuilder);
            }
        }
        return constructBeanSerializerBuilder.build();
    }

    /* access modifiers changed from: protected */
    public BeanPropertyWriter constructFilteredBeanWriter(BeanPropertyWriter beanPropertyWriter, Class<?>[] clsArr) {
        return FilteredBeanPropertyWriter.constructViewBased(beanPropertyWriter, clsArr);
    }

    /* access modifiers changed from: protected */
    public PropertyBuilder constructPropertyBuilder(SerializationConfig serializationConfig, BasicBeanDescription basicBeanDescription) {
        return new PropertyBuilder(serializationConfig, basicBeanDescription);
    }

    /* access modifiers changed from: protected */
    public BeanSerializerBuilder constructBeanSerializerBuilder(BasicBeanDescription basicBeanDescription) {
        return new BeanSerializerBuilder(basicBeanDescription);
    }

    /* access modifiers changed from: protected */
    public Object findFilterId(SerializationConfig serializationConfig, BasicBeanDescription basicBeanDescription) {
        return serializationConfig.getAnnotationIntrospector().findFilterId(basicBeanDescription.getClassInfo());
    }

    /* access modifiers changed from: protected */
    public boolean isPotentialBeanType(Class<?> cls) {
        return ClassUtil.canBeABeanType(cls) == null && !ClassUtil.isProxyType(cls);
    }

    /* access modifiers changed from: protected */
    public List<BeanPropertyWriter> findBeanProperties(SerializationConfig serializationConfig, BasicBeanDescription basicBeanDescription) {
        VisibilityChecker defaultVisibilityChecker = serializationConfig.getDefaultVisibilityChecker();
        if (!serializationConfig.isEnabled(SerializationConfig.Feature.AUTO_DETECT_GETTERS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withGetterVisibility(JsonAutoDetect.Visibility.NONE);
        }
        if (!serializationConfig.isEnabled(SerializationConfig.Feature.AUTO_DETECT_IS_GETTERS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withIsGetterVisibility(JsonAutoDetect.Visibility.NONE);
        }
        if (!serializationConfig.isEnabled(SerializationConfig.Feature.AUTO_DETECT_FIELDS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withFieldVisibility(JsonAutoDetect.Visibility.NONE);
        }
        AnnotationIntrospector annotationIntrospector = serializationConfig.getAnnotationIntrospector();
        VisibilityChecker<?> findAutoDetectVisibility = annotationIntrospector.findAutoDetectVisibility(basicBeanDescription.getClassInfo(), defaultVisibilityChecker);
        LinkedHashMap<String, AnnotatedMethod> findGetters = basicBeanDescription.findGetters(findAutoDetectVisibility, null);
        LinkedHashMap<String, AnnotatedField> findSerializableFields = basicBeanDescription.findSerializableFields(findAutoDetectVisibility, findGetters.keySet());
        removeIgnorableTypes(serializationConfig, basicBeanDescription, findGetters);
        removeIgnorableTypes(serializationConfig, basicBeanDescription, findSerializableFields);
        if (findGetters.isEmpty() && findSerializableFields.isEmpty()) {
            return null;
        }
        boolean usesStaticTyping = usesStaticTyping(serializationConfig, basicBeanDescription, null);
        PropertyBuilder constructPropertyBuilder = constructPropertyBuilder(serializationConfig, basicBeanDescription);
        ArrayList arrayList = new ArrayList(findGetters.size());
        TypeBindings bindingsForBeanType = basicBeanDescription.bindingsForBeanType();
        for (Map.Entry next : findSerializableFields.entrySet()) {
            AnnotationIntrospector.ReferenceProperty findReferenceType = annotationIntrospector.findReferenceType((AnnotatedMember) next.getValue());
            if (findReferenceType == null || !findReferenceType.isBackReference()) {
                arrayList.add(_constructWriter(serializationConfig, bindingsForBeanType, constructPropertyBuilder, usesStaticTyping, (String) next.getKey(), (AnnotatedMember) next.getValue()));
            }
        }
        for (Map.Entry next2 : findGetters.entrySet()) {
            AnnotationIntrospector.ReferenceProperty findReferenceType2 = annotationIntrospector.findReferenceType((AnnotatedMember) next2.getValue());
            if (findReferenceType2 == null || !findReferenceType2.isBackReference()) {
                arrayList.add(_constructWriter(serializationConfig, bindingsForBeanType, constructPropertyBuilder, usesStaticTyping, (String) next2.getKey(), (AnnotatedMember) next2.getValue()));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<BeanPropertyWriter> filterBeanProperties(SerializationConfig serializationConfig, BasicBeanDescription basicBeanDescription, List<BeanPropertyWriter> list) {
        String[] findPropertiesToIgnore = serializationConfig.getAnnotationIntrospector().findPropertiesToIgnore(basicBeanDescription.getClassInfo());
        if (findPropertiesToIgnore != null && findPropertiesToIgnore.length > 0) {
            HashSet arrayToSet = ArrayBuilders.arrayToSet(findPropertiesToIgnore);
            Iterator<BeanPropertyWriter> it = list.iterator();
            while (it.hasNext()) {
                if (arrayToSet.contains(it.next().getName())) {
                    it.remove();
                }
            }
        }
        return list;
    }

    /* access modifiers changed from: protected */
    public List<BeanPropertyWriter> sortBeanProperties(SerializationConfig serializationConfig, BasicBeanDescription basicBeanDescription, List<BeanPropertyWriter> list) {
        List<String> findCreatorPropertyNames = basicBeanDescription.findCreatorPropertyNames();
        AnnotationIntrospector annotationIntrospector = serializationConfig.getAnnotationIntrospector();
        AnnotatedClass classInfo = basicBeanDescription.getClassInfo();
        String[] findSerializationPropertyOrder = annotationIntrospector.findSerializationPropertyOrder(classInfo);
        Boolean findSerializationSortAlphabetically = annotationIntrospector.findSerializationSortAlphabetically(classInfo);
        boolean z = findSerializationSortAlphabetically != null && findSerializationSortAlphabetically.booleanValue();
        if (z || !findCreatorPropertyNames.isEmpty() || findSerializationPropertyOrder != null) {
            return _sortBeanProperties(list, findCreatorPropertyNames, findSerializationPropertyOrder, z);
        }
        return list;
    }

    /* access modifiers changed from: protected */
    public void processViews(SerializationConfig serializationConfig, BeanSerializerBuilder beanSerializerBuilder) {
        List<BeanPropertyWriter> properties = beanSerializerBuilder.getProperties();
        if (serializationConfig.isEnabled(SerializationConfig.Feature.DEFAULT_VIEW_INCLUSION)) {
            int size = properties.size();
            int i = 0;
            BeanPropertyWriter[] beanPropertyWriterArr = null;
            while (i < size) {
                BeanPropertyWriter beanPropertyWriter = properties.get(i);
                Class<?>[] views = beanPropertyWriter.getViews();
                if (views != null) {
                    if (beanPropertyWriterArr == null) {
                        beanPropertyWriterArr = new BeanPropertyWriter[properties.size()];
                    }
                    beanPropertyWriterArr[i] = constructFilteredBeanWriter(beanPropertyWriter, views);
                }
                i++;
                beanPropertyWriterArr = beanPropertyWriterArr;
            }
            if (beanPropertyWriterArr != null) {
                for (int i2 = 0; i2 < size; i2++) {
                    if (beanPropertyWriterArr[i2] == null) {
                        beanPropertyWriterArr[i2] = properties.get(i2);
                    }
                }
                beanSerializerBuilder.setFilteredProperties(beanPropertyWriterArr);
                return;
            }
            return;
        }
        ArrayList arrayList = new ArrayList(properties.size());
        for (BeanPropertyWriter next : properties) {
            Class<?>[] views2 = next.getViews();
            if (views2 != null) {
                arrayList.add(constructFilteredBeanWriter(next, views2));
            }
        }
        beanSerializerBuilder.setFilteredProperties((BeanPropertyWriter[]) arrayList.toArray(new BeanPropertyWriter[arrayList.size()]));
    }

    /* access modifiers changed from: protected */
    public <T extends AnnotatedMember> void removeIgnorableTypes(SerializationConfig serializationConfig, BasicBeanDescription basicBeanDescription, Map<String, T> map) {
        Boolean bool;
        if (!map.isEmpty()) {
            AnnotationIntrospector annotationIntrospector = serializationConfig.getAnnotationIntrospector();
            Iterator<Map.Entry<String, T>> it = map.entrySet().iterator();
            HashMap hashMap = new HashMap();
            while (it.hasNext()) {
                Class<?> rawType = ((AnnotatedMember) it.next().getValue()).getRawType();
                Boolean bool2 = (Boolean) hashMap.get(rawType);
                if (bool2 == null) {
                    Boolean isIgnorableType = annotationIntrospector.isIgnorableType(((BasicBeanDescription) serializationConfig.introspectClassAnnotations(rawType)).getClassInfo());
                    if (isIgnorableType == null) {
                        isIgnorableType = Boolean.FALSE;
                    }
                    hashMap.put(rawType, isIgnorableType);
                    bool = isIgnorableType;
                } else {
                    bool = bool2;
                }
                if (bool.booleanValue()) {
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public BeanPropertyWriter _constructWriter(SerializationConfig serializationConfig, TypeBindings typeBindings, PropertyBuilder propertyBuilder, boolean z, String str, AnnotatedMember annotatedMember) {
        TypeSerializer typeSerializer;
        if (serializationConfig.isEnabled(SerializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            annotatedMember.fixAccess();
        }
        JavaType type = annotatedMember.getType(typeBindings);
        BeanProperty.Std std = new BeanProperty.Std(str, type, propertyBuilder.getClassAnnotations(), annotatedMember);
        JsonSerializer<Object> findSerializerFromAnnotation = findSerializerFromAnnotation(serializationConfig, annotatedMember, std);
        if (ClassUtil.isCollectionMapOrArray(type.getRawClass())) {
            typeSerializer = findPropertyContentTypeSerializer(type, serializationConfig, annotatedMember, std);
        } else {
            typeSerializer = null;
        }
        BeanPropertyWriter buildWriter = propertyBuilder.buildWriter(str, type, findSerializerFromAnnotation, findPropertyTypeSerializer(type, serializationConfig, annotatedMember, std), typeSerializer, annotatedMember, z);
        buildWriter.setViews(serializationConfig.getAnnotationIntrospector().findSerializationViews(annotatedMember));
        return buildWriter;
    }

    /* access modifiers changed from: protected */
    public List<BeanPropertyWriter> _sortBeanProperties(List<BeanPropertyWriter> list, List<String> list2, String[] strArr, boolean z) {
        Map linkedHashMap;
        int size = list.size();
        if (z) {
            linkedHashMap = new TreeMap();
        } else {
            linkedHashMap = new LinkedHashMap(size * 2);
        }
        for (BeanPropertyWriter next : list) {
            linkedHashMap.put(next.getName(), next);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap(size * 2);
        if (strArr != null) {
            for (String str : strArr) {
                BeanPropertyWriter beanPropertyWriter = (BeanPropertyWriter) linkedHashMap.get(str);
                if (beanPropertyWriter != null) {
                    linkedHashMap2.put(str, beanPropertyWriter);
                }
            }
        }
        for (String next2 : list2) {
            BeanPropertyWriter beanPropertyWriter2 = (BeanPropertyWriter) linkedHashMap.get(next2);
            if (beanPropertyWriter2 != null) {
                linkedHashMap2.put(next2, beanPropertyWriter2);
            }
        }
        linkedHashMap2.putAll(linkedHashMap);
        return new ArrayList(linkedHashMap2.values());
    }
}
