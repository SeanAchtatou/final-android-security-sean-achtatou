package org.codehaus.jackson.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import org.codehaus.jackson.util.BufferRecycler;

public final class TextBuffer {
    static final int MAX_SEGMENT_LEN = 262144;
    static final int MIN_SEGMENT_LEN = 1000;
    static final char[] NO_CHARS = new char[0];
    private final BufferRecycler _allocator;
    private char[] _currentSegment;
    private int _currentSize;
    private boolean _hasSegments = false;
    private char[] _inputBuffer;
    private int _inputLen;
    private int _inputStart;
    private char[] _resultArray;
    private String _resultString;
    private int _segmentSize;
    private ArrayList<char[]> _segments;

    public TextBuffer(BufferRecycler bufferRecycler) {
        this._allocator = bufferRecycler;
    }

    public final void releaseBuffers() {
        if (this._allocator == null) {
            resetWithEmpty();
        } else if (this._currentSegment != null) {
            resetWithEmpty();
            char[] cArr = this._currentSegment;
            this._currentSegment = null;
            this._allocator.releaseCharBuffer(BufferRecycler.CharBufferType.TEXT_BUFFER, cArr);
        }
    }

    public final void resetWithEmpty() {
        this._inputStart = -1;
        this._currentSize = 0;
        this._inputLen = 0;
        this._inputBuffer = null;
        this._resultString = null;
        this._resultArray = null;
        if (this._hasSegments) {
            clearSegments();
        }
    }

    public final void resetWithShared(char[] cArr, int i, int i2) {
        this._resultString = null;
        this._resultArray = null;
        this._inputBuffer = cArr;
        this._inputStart = i;
        this._inputLen = i2;
        if (this._hasSegments) {
            clearSegments();
        }
    }

    public final void resetWithCopy(char[] cArr, int i, int i2) {
        this._inputBuffer = null;
        this._inputStart = -1;
        this._inputLen = 0;
        this._resultString = null;
        this._resultArray = null;
        if (this._hasSegments) {
            clearSegments();
        } else if (this._currentSegment == null) {
            this._currentSegment = findBuffer(i2);
        }
        this._segmentSize = 0;
        this._currentSize = 0;
        append(cArr, i, i2);
    }

    public final void resetWithString(String str) {
        this._inputBuffer = null;
        this._inputStart = -1;
        this._inputLen = 0;
        this._resultString = str;
        this._resultArray = null;
        if (this._hasSegments) {
            clearSegments();
        }
        this._currentSize = 0;
    }

    private final char[] findBuffer(int i) {
        if (this._allocator != null) {
            return this._allocator.allocCharBuffer(BufferRecycler.CharBufferType.TEXT_BUFFER, i);
        }
        return new char[Math.max(i, (int) MIN_SEGMENT_LEN)];
    }

    private final void clearSegments() {
        this._hasSegments = false;
        this._segments.clear();
        this._segmentSize = 0;
        this._currentSize = 0;
    }

    public final int size() {
        if (this._inputStart >= 0) {
            return this._inputLen;
        }
        return this._segmentSize + this._currentSize;
    }

    public final int getTextOffset() {
        if (this._inputStart >= 0) {
            return this._inputStart;
        }
        return 0;
    }

    public final char[] getTextBuffer() {
        if (this._inputStart >= 0) {
            return this._inputBuffer;
        }
        if (!this._hasSegments) {
            return this._currentSegment;
        }
        return contentsAsArray();
    }

    public final String contentsAsString() {
        if (this._resultString == null) {
            if (this._resultArray != null) {
                this._resultString = new String(this._resultArray);
            } else if (this._inputStart < 0) {
                int i = this._segmentSize;
                int i2 = this._currentSize;
                if (i == 0) {
                    this._resultString = i2 == 0 ? "" : new String(this._currentSegment, 0, i2);
                } else {
                    StringBuilder sb = new StringBuilder(i + i2);
                    if (this._segments != null) {
                        int size = this._segments.size();
                        for (int i3 = 0; i3 < size; i3++) {
                            char[] cArr = this._segments.get(i3);
                            sb.append(cArr, 0, cArr.length);
                        }
                    }
                    sb.append(this._currentSegment, 0, this._currentSize);
                    this._resultString = sb.toString();
                }
            } else if (this._inputLen <= 0) {
                this._resultString = "";
                return "";
            } else {
                this._resultString = new String(this._inputBuffer, this._inputStart, this._inputLen);
            }
        }
        return this._resultString;
    }

    public final char[] contentsAsArray() {
        char[] cArr = this._resultArray;
        if (cArr != null) {
            return cArr;
        }
        char[] buildResultArray = buildResultArray();
        this._resultArray = buildResultArray;
        return buildResultArray;
    }

    public final BigDecimal contentsAsDecimal() throws NumberFormatException {
        if (this._resultArray != null) {
            return new BigDecimal(this._resultArray);
        }
        if (this._inputStart >= 0) {
            return new BigDecimal(this._inputBuffer, this._inputStart, this._inputLen);
        }
        if (this._segmentSize == 0) {
            return new BigDecimal(this._currentSegment, 0, this._currentSize);
        }
        return new BigDecimal(contentsAsArray());
    }

    public final double contentsAsDouble() throws NumberFormatException {
        return Double.parseDouble(contentsAsString());
    }

    public final void ensureNotShared() {
        if (this._inputStart >= 0) {
            unshare(16);
        }
    }

    public final void append(char c) {
        if (this._inputStart >= 0) {
            unshare(16);
        }
        this._resultString = null;
        this._resultArray = null;
        char[] cArr = this._currentSegment;
        if (this._currentSize >= cArr.length) {
            expand(1);
            cArr = this._currentSegment;
        }
        int i = this._currentSize;
        this._currentSize = i + 1;
        cArr[i] = c;
    }

    public final void append(char[] cArr, int i, int i2) {
        int i3;
        int i4;
        if (this._inputStart >= 0) {
            unshare(i2);
        }
        this._resultString = null;
        this._resultArray = null;
        char[] cArr2 = this._currentSegment;
        int length = cArr2.length - this._currentSize;
        if (length >= i2) {
            System.arraycopy(cArr, i, cArr2, this._currentSize, i2);
            this._currentSize += i2;
            return;
        }
        if (length > 0) {
            System.arraycopy(cArr, i, cArr2, this._currentSize, length);
            int i5 = i2 - length;
            i4 = i + length;
            i3 = i5;
        } else {
            i3 = i2;
            i4 = i;
        }
        expand(i3);
        System.arraycopy(cArr, i4, this._currentSegment, 0, i3);
        this._currentSize = i3;
    }

    public final void append(String str, int i, int i2) {
        int i3;
        int i4;
        if (this._inputStart >= 0) {
            unshare(i2);
        }
        this._resultString = null;
        this._resultArray = null;
        char[] cArr = this._currentSegment;
        int length = cArr.length - this._currentSize;
        if (length >= i2) {
            str.getChars(i, i + i2, cArr, this._currentSize);
            this._currentSize += i2;
            return;
        }
        if (length > 0) {
            str.getChars(i, i + length, cArr, this._currentSize);
            i3 = i2 - length;
            i4 = length + i;
        } else {
            i3 = i2;
            i4 = i;
        }
        expand(i3);
        str.getChars(i4, i4 + i3, this._currentSegment, 0);
        this._currentSize = i3;
    }

    public final char[] getCurrentSegment() {
        if (this._inputStart >= 0) {
            unshare(1);
        } else {
            char[] cArr = this._currentSegment;
            if (cArr == null) {
                this._currentSegment = findBuffer(0);
            } else if (this._currentSize >= cArr.length) {
                expand(1);
            }
        }
        return this._currentSegment;
    }

    public final char[] emptyAndGetCurrentSegment() {
        this._inputStart = -1;
        this._currentSize = 0;
        this._inputLen = 0;
        this._inputBuffer = null;
        this._resultString = null;
        this._resultArray = null;
        if (this._hasSegments) {
            clearSegments();
        }
        char[] cArr = this._currentSegment;
        if (cArr != null) {
            return cArr;
        }
        char[] findBuffer = findBuffer(0);
        this._currentSegment = findBuffer;
        return findBuffer;
    }

    public final int getCurrentSegmentSize() {
        return this._currentSize;
    }

    public final void setCurrentLength(int i) {
        this._currentSize = i;
    }

    public final char[] finishCurrentSegment() {
        if (this._segments == null) {
            this._segments = new ArrayList<>();
        }
        this._hasSegments = true;
        this._segments.add(this._currentSegment);
        int length = this._currentSegment.length;
        this._segmentSize += length;
        char[] _charArray = _charArray(Math.min(length + (length >> 1), (int) MAX_SEGMENT_LEN));
        this._currentSize = 0;
        this._currentSegment = _charArray;
        return _charArray;
    }

    public final char[] expandCurrentSegment() {
        char[] cArr = this._currentSegment;
        int length = cArr.length;
        this._currentSegment = _charArray(length == MAX_SEGMENT_LEN ? 262145 : Math.min((int) MAX_SEGMENT_LEN, (length >> 1) + length));
        System.arraycopy(cArr, 0, this._currentSegment, 0, length);
        return this._currentSegment;
    }

    public final String toString() {
        return contentsAsString();
    }

    private void unshare(int i) {
        int i2 = this._inputLen;
        this._inputLen = 0;
        char[] cArr = this._inputBuffer;
        this._inputBuffer = null;
        int i3 = this._inputStart;
        this._inputStart = -1;
        int i4 = i2 + i;
        if (this._currentSegment == null || i4 > this._currentSegment.length) {
            this._currentSegment = findBuffer(i4);
        }
        if (i2 > 0) {
            System.arraycopy(cArr, i3, this._currentSegment, 0, i2);
        }
        this._segmentSize = 0;
        this._currentSize = i2;
    }

    private void expand(int i) {
        if (this._segments == null) {
            this._segments = new ArrayList<>();
        }
        char[] cArr = this._currentSegment;
        this._hasSegments = true;
        this._segments.add(cArr);
        this._segmentSize += cArr.length;
        int length = cArr.length;
        int i2 = length >> 1;
        if (i2 < i) {
            i2 = i;
        }
        char[] _charArray = _charArray(Math.min((int) MAX_SEGMENT_LEN, length + i2));
        this._currentSize = 0;
        this._currentSegment = _charArray;
    }

    private char[] buildResultArray() {
        int i;
        if (this._resultString != null) {
            return this._resultString.toCharArray();
        }
        if (this._inputStart < 0) {
            int size = size();
            if (size <= 0) {
                return NO_CHARS;
            }
            char[] _charArray = _charArray(size);
            if (this._segments != null) {
                int size2 = this._segments.size();
                int i2 = 0;
                int i3 = 0;
                while (i2 < size2) {
                    char[] cArr = this._segments.get(i2);
                    int length = cArr.length;
                    System.arraycopy(cArr, 0, _charArray, i3, length);
                    i2++;
                    i3 += length;
                }
                i = i3;
            } else {
                i = 0;
            }
            System.arraycopy(this._currentSegment, 0, _charArray, i, this._currentSize);
            return _charArray;
        } else if (this._inputLen <= 0) {
            return NO_CHARS;
        } else {
            char[] _charArray2 = _charArray(this._inputLen);
            System.arraycopy(this._inputBuffer, this._inputStart, _charArray2, 0, this._inputLen);
            return _charArray2;
        }
    }

    private final char[] _charArray(int i) {
        return new char[i];
    }
}
