package org.codehaus.jackson.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonLocation;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonStreamContext;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.SerializableString;
import org.codehaus.jackson.impl.JsonParserMinimalBase;
import org.codehaus.jackson.impl.JsonReadContext;
import org.codehaus.jackson.impl.JsonWriteContext;
import org.codehaus.jackson.io.SerializedString;

public class TokenBuffer extends JsonGenerator {
    protected static final int DEFAULT_PARSER_FEATURES = JsonParser.Feature.collectDefaults();
    protected int _appendOffset;
    protected boolean _closed;
    protected Segment _first;
    protected int _generatorFeatures = DEFAULT_PARSER_FEATURES;
    protected Segment _last;
    protected ObjectCodec _objectCodec;
    protected JsonWriteContext _writeContext = JsonWriteContext.createRootContext();

    public TokenBuffer(ObjectCodec objectCodec) {
        this._objectCodec = objectCodec;
        Segment segment = new Segment();
        this._last = segment;
        this._first = segment;
        this._appendOffset = 0;
    }

    public JsonParser asParser() {
        return asParser(this._objectCodec);
    }

    public JsonParser asParser(ObjectCodec objectCodec) {
        return new Parser(this._first, objectCodec);
    }

    public JsonParser asParser(JsonParser jsonParser) {
        Parser parser = new Parser(this._first, jsonParser.getCodec());
        parser.setLocation(jsonParser.getTokenLocation());
        return parser;
    }

    public void serialize(JsonGenerator jsonGenerator) throws IOException, JsonGenerationException {
        Segment segment = this._first;
        int i = -1;
        while (true) {
            i++;
            if (i >= 16) {
                Segment next = segment.next();
                if (next != null) {
                    segment = next;
                    i = 0;
                } else {
                    return;
                }
            }
            JsonToken type = segment.type(i);
            if (type != null) {
                switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[type.ordinal()]) {
                    case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                        jsonGenerator.writeStartObject();
                        break;
                    case JsonWriteContext.STATUS_OK_AFTER_COLON:
                        jsonGenerator.writeEndObject();
                        break;
                    case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                        jsonGenerator.writeStartArray();
                        break;
                    case JsonWriteContext.STATUS_EXPECT_VALUE:
                        jsonGenerator.writeEndArray();
                        break;
                    case JsonWriteContext.STATUS_EXPECT_NAME:
                        Object obj = segment.get(i);
                        if (!(obj instanceof SerializableString)) {
                            jsonGenerator.writeFieldName((String) obj);
                            break;
                        } else {
                            jsonGenerator.writeFieldName((SerializableString) obj);
                            break;
                        }
                    case 6:
                        Object obj2 = segment.get(i);
                        if (!(obj2 instanceof SerializableString)) {
                            jsonGenerator.writeString((String) obj2);
                            break;
                        } else {
                            jsonGenerator.writeString((SerializableString) obj2);
                            break;
                        }
                    case 7:
                        Number number = (Number) segment.get(i);
                        if (!(number instanceof BigInteger)) {
                            if (!(number instanceof Long)) {
                                jsonGenerator.writeNumber(number.intValue());
                                break;
                            } else {
                                jsonGenerator.writeNumber(number.longValue());
                                break;
                            }
                        } else {
                            jsonGenerator.writeNumber((BigInteger) number);
                            break;
                        }
                    case 8:
                        Object obj3 = segment.get(i);
                        if (obj3 instanceof BigDecimal) {
                            jsonGenerator.writeNumber((BigDecimal) obj3);
                            break;
                        } else if (obj3 instanceof Float) {
                            jsonGenerator.writeNumber(((Float) obj3).floatValue());
                            break;
                        } else if (obj3 instanceof Double) {
                            jsonGenerator.writeNumber(((Double) obj3).doubleValue());
                            break;
                        } else if (obj3 == null) {
                            jsonGenerator.writeNull();
                            break;
                        } else if (obj3 instanceof String) {
                            jsonGenerator.writeNumber((String) obj3);
                            break;
                        } else {
                            throw new JsonGenerationException("Unrecognized value type for VALUE_NUMBER_FLOAT: " + obj3.getClass().getName() + ", can not serialize");
                        }
                    case 9:
                        jsonGenerator.writeBoolean(true);
                        break;
                    case 10:
                        jsonGenerator.writeBoolean(false);
                        break;
                    case 11:
                        jsonGenerator.writeNull();
                        break;
                    case 12:
                        jsonGenerator.writeObject(segment.get(i));
                        break;
                    default:
                        throw new RuntimeException("Internal error: should never end up through this code path");
                }
            } else {
                return;
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[TokenBuffer: ");
        JsonParser asParser = asParser();
        int i = 0;
        while (true) {
            try {
                JsonToken nextToken = asParser.nextToken();
                if (nextToken == null) {
                    break;
                }
                if (i < 100) {
                    if (i > 0) {
                        sb.append(", ");
                    }
                    sb.append(nextToken.toString());
                }
                i++;
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
        if (i >= 100) {
            sb.append(" ... (truncated ").append(i - 100).append(" entries)");
        }
        sb.append(']');
        return sb.toString();
    }

    public JsonGenerator enable(JsonGenerator.Feature feature) {
        this._generatorFeatures |= feature.getMask();
        return this;
    }

    public JsonGenerator disable(JsonGenerator.Feature feature) {
        this._generatorFeatures &= feature.getMask() ^ -1;
        return this;
    }

    public boolean isEnabled(JsonGenerator.Feature feature) {
        return (this._generatorFeatures & feature.getMask()) != 0;
    }

    public JsonGenerator useDefaultPrettyPrinter() {
        return this;
    }

    public JsonGenerator setCodec(ObjectCodec objectCodec) {
        this._objectCodec = objectCodec;
        return this;
    }

    public ObjectCodec getCodec() {
        return this._objectCodec;
    }

    public final JsonWriteContext getOutputContext() {
        return this._writeContext;
    }

    public void flush() throws IOException {
    }

    public void close() throws IOException {
        this._closed = true;
    }

    public boolean isClosed() {
        return this._closed;
    }

    public final void writeStartArray() throws IOException, JsonGenerationException {
        _append(JsonToken.START_ARRAY);
        this._writeContext = this._writeContext.createChildArrayContext();
    }

    public final void writeEndArray() throws IOException, JsonGenerationException {
        _append(JsonToken.END_ARRAY);
        JsonWriteContext parent = this._writeContext.getParent();
        if (parent != null) {
            this._writeContext = parent;
        }
    }

    public final void writeStartObject() throws IOException, JsonGenerationException {
        _append(JsonToken.START_OBJECT);
        this._writeContext = this._writeContext.createChildObjectContext();
    }

    public final void writeEndObject() throws IOException, JsonGenerationException {
        _append(JsonToken.END_OBJECT);
        JsonWriteContext parent = this._writeContext.getParent();
        if (parent != null) {
            this._writeContext = parent;
        }
    }

    public final void writeFieldName(String str) throws IOException, JsonGenerationException {
        _append(JsonToken.FIELD_NAME, str);
        this._writeContext.writeFieldName(str);
    }

    public void writeFieldName(SerializableString serializableString) throws IOException, JsonGenerationException {
        _append(JsonToken.FIELD_NAME, serializableString);
        this._writeContext.writeFieldName(serializableString.getValue());
    }

    public void writeFieldName(SerializedString serializedString) throws IOException, JsonGenerationException {
        _append(JsonToken.FIELD_NAME, serializedString);
        this._writeContext.writeFieldName(serializedString.getValue());
    }

    public void writeString(String str) throws IOException, JsonGenerationException {
        if (str == null) {
            writeNull();
        } else {
            _append(JsonToken.VALUE_STRING, str);
        }
    }

    public void writeString(char[] cArr, int i, int i2) throws IOException, JsonGenerationException {
        writeString(new String(cArr, i, i2));
    }

    public void writeString(SerializableString serializableString) throws IOException, JsonGenerationException {
        if (serializableString == null) {
            writeNull();
        } else {
            _append(JsonToken.VALUE_STRING, serializableString);
        }
    }

    public void writeRawUTF8String(byte[] bArr, int i, int i2) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeUTF8String(byte[] bArr, int i, int i2) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRaw(String str) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRaw(String str, int i, int i2) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRaw(char[] cArr, int i, int i2) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRaw(char c) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRawValue(String str) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRawValue(String str, int i, int i2) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeRawValue(char[] cArr, int i, int i2) throws IOException, JsonGenerationException {
        _reportUnsupportedOperation();
    }

    public void writeNumber(int i) throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NUMBER_INT, Integer.valueOf(i));
    }

    public void writeNumber(long j) throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NUMBER_INT, Long.valueOf(j));
    }

    public void writeNumber(double d) throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NUMBER_FLOAT, Double.valueOf(d));
    }

    public void writeNumber(float f) throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NUMBER_FLOAT, Float.valueOf(f));
    }

    public void writeNumber(BigDecimal bigDecimal) throws IOException, JsonGenerationException {
        if (bigDecimal == null) {
            writeNull();
        } else {
            _append(JsonToken.VALUE_NUMBER_FLOAT, bigDecimal);
        }
    }

    public void writeNumber(BigInteger bigInteger) throws IOException, JsonGenerationException {
        if (bigInteger == null) {
            writeNull();
        } else {
            _append(JsonToken.VALUE_NUMBER_INT, bigInteger);
        }
    }

    public void writeNumber(String str) throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NUMBER_FLOAT, str);
    }

    public void writeBoolean(boolean z) throws IOException, JsonGenerationException {
        _append(z ? JsonToken.VALUE_TRUE : JsonToken.VALUE_FALSE);
    }

    public void writeNull() throws IOException, JsonGenerationException {
        _append(JsonToken.VALUE_NULL);
    }

    public void writeObject(Object obj) throws IOException, JsonProcessingException {
        _append(JsonToken.VALUE_EMBEDDED_OBJECT, obj);
    }

    public void writeTree(JsonNode jsonNode) throws IOException, JsonProcessingException {
        _append(JsonToken.VALUE_EMBEDDED_OBJECT, jsonNode);
    }

    public void writeBinary(Base64Variant base64Variant, byte[] bArr, int i, int i2) throws IOException, JsonGenerationException {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        writeObject(bArr2);
    }

    public void copyCurrentEvent(JsonParser jsonParser) throws IOException, JsonProcessingException {
        switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[jsonParser.getCurrentToken().ordinal()]) {
            case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                writeStartObject();
                return;
            case JsonWriteContext.STATUS_OK_AFTER_COLON:
                writeEndObject();
                return;
            case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                writeStartArray();
                return;
            case JsonWriteContext.STATUS_EXPECT_VALUE:
                writeEndArray();
                return;
            case JsonWriteContext.STATUS_EXPECT_NAME:
                writeFieldName(jsonParser.getCurrentName());
                return;
            case 6:
                if (jsonParser.hasTextCharacters()) {
                    writeString(jsonParser.getTextCharacters(), jsonParser.getTextOffset(), jsonParser.getTextLength());
                    return;
                } else {
                    writeString(jsonParser.getText());
                    return;
                }
            case 7:
                switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonParser$NumberType[jsonParser.getNumberType().ordinal()]) {
                    case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                        writeNumber(jsonParser.getIntValue());
                        return;
                    case JsonWriteContext.STATUS_OK_AFTER_COLON:
                        writeNumber(jsonParser.getBigIntegerValue());
                        return;
                    default:
                        writeNumber(jsonParser.getLongValue());
                        return;
                }
            case 8:
                switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonParser$NumberType[jsonParser.getNumberType().ordinal()]) {
                    case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                        writeNumber(jsonParser.getDecimalValue());
                        return;
                    case JsonWriteContext.STATUS_EXPECT_VALUE:
                        writeNumber(jsonParser.getFloatValue());
                        return;
                    default:
                        writeNumber(jsonParser.getDoubleValue());
                        return;
                }
            case 9:
                writeBoolean(true);
                return;
            case 10:
                writeBoolean(false);
                return;
            case 11:
                writeNull();
                return;
            case 12:
                writeObject(jsonParser.getEmbeddedObject());
                return;
            default:
                throw new RuntimeException("Internal error: should never end up through this code path");
        }
    }

    /* renamed from: org.codehaus.jackson.util.TokenBuffer$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$codehaus$jackson$JsonParser$NumberType = new int[JsonParser.NumberType.values().length];

        static {
            try {
                $SwitchMap$org$codehaus$jackson$JsonParser$NumberType[JsonParser.NumberType.INT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonParser$NumberType[JsonParser.NumberType.BIG_INTEGER.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonParser$NumberType[JsonParser.NumberType.BIG_DECIMAL.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonParser$NumberType[JsonParser.NumberType.FLOAT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonParser$NumberType[JsonParser.NumberType.LONG.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            $SwitchMap$org$codehaus$jackson$JsonToken = new int[JsonToken.values().length];
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.START_OBJECT.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.END_OBJECT.ordinal()] = 2;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.START_ARRAY.ordinal()] = 3;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.END_ARRAY.ordinal()] = 4;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.FIELD_NAME.ordinal()] = 5;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_STRING.ordinal()] = 6;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_NUMBER_INT.ordinal()] = 7;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_NUMBER_FLOAT.ordinal()] = 8;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_TRUE.ordinal()] = 9;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_FALSE.ordinal()] = 10;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_NULL.ordinal()] = 11;
            } catch (NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[JsonToken.VALUE_EMBEDDED_OBJECT.ordinal()] = 12;
            } catch (NoSuchFieldError e17) {
            }
        }
    }

    public void copyCurrentStructure(JsonParser jsonParser) throws IOException, JsonProcessingException {
        JsonToken currentToken = jsonParser.getCurrentToken();
        if (currentToken == JsonToken.FIELD_NAME) {
            writeFieldName(jsonParser.getCurrentName());
            currentToken = jsonParser.nextToken();
        }
        switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[currentToken.ordinal()]) {
            case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                writeStartObject();
                while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
                    copyCurrentStructure(jsonParser);
                }
                writeEndObject();
                return;
            case JsonWriteContext.STATUS_OK_AFTER_COLON:
            default:
                copyCurrentEvent(jsonParser);
                return;
            case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                writeStartArray();
                while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                    copyCurrentStructure(jsonParser);
                }
                writeEndArray();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void _append(JsonToken jsonToken) {
        Segment append = this._last.append(this._appendOffset, jsonToken);
        if (append == null) {
            this._appendOffset++;
            return;
        }
        this._last = append;
        this._appendOffset = 1;
    }

    /* access modifiers changed from: protected */
    public final void _append(JsonToken jsonToken, Object obj) {
        Segment append = this._last.append(this._appendOffset, jsonToken, obj);
        if (append == null) {
            this._appendOffset++;
            return;
        }
        this._last = append;
        this._appendOffset = 1;
    }

    /* access modifiers changed from: protected */
    public void _reportUnsupportedOperation() {
        throw new UnsupportedOperationException("Called operation not supported for TokenBuffer");
    }

    protected static final class Parser extends JsonParserMinimalBase {
        protected transient ByteArrayBuilder _byteBuilder;
        protected boolean _closed;
        protected ObjectCodec _codec;
        protected JsonLocation _location = null;
        protected JsonReadContext _parsingContext;
        protected Segment _segment;
        protected int _segmentPtr;

        public Parser(Segment segment, ObjectCodec objectCodec) {
            super(0);
            this._segment = segment;
            this._segmentPtr = -1;
            this._codec = objectCodec;
            this._parsingContext = JsonReadContext.createRootContext(-1, -1);
        }

        public final void setLocation(JsonLocation jsonLocation) {
            this._location = jsonLocation;
        }

        public final ObjectCodec getCodec() {
            return this._codec;
        }

        public final void setCodec(ObjectCodec objectCodec) {
            this._codec = objectCodec;
        }

        public final JsonToken peekNextToken() throws IOException, JsonParseException {
            Segment segment;
            int i;
            if (this._closed) {
                return null;
            }
            Segment segment2 = this._segment;
            int i2 = this._segmentPtr + 1;
            if (i2 >= 16) {
                segment = segment2 == null ? null : segment2.next();
                i = 0;
            } else {
                int i3 = i2;
                segment = segment2;
                i = i3;
            }
            if (segment == null) {
                return null;
            }
            return segment.type(i);
        }

        public final void close() throws IOException {
            if (!this._closed) {
                this._closed = true;
            }
        }

        public final JsonToken nextToken() throws IOException, JsonParseException {
            if (this._closed || this._segment == null) {
                return null;
            }
            int i = this._segmentPtr + 1;
            this._segmentPtr = i;
            if (i >= 16) {
                this._segmentPtr = 0;
                this._segment = this._segment.next();
                if (this._segment == null) {
                    return null;
                }
            }
            this._currToken = this._segment.type(this._segmentPtr);
            if (this._currToken == JsonToken.FIELD_NAME) {
                Object _currentObject = _currentObject();
                this._parsingContext.setCurrentName(_currentObject instanceof String ? (String) _currentObject : _currentObject.toString());
            } else if (this._currToken == JsonToken.START_OBJECT) {
                this._parsingContext = this._parsingContext.createChildObjectContext(-1, -1);
            } else if (this._currToken == JsonToken.START_ARRAY) {
                this._parsingContext = this._parsingContext.createChildArrayContext(-1, -1);
            } else if (this._currToken == JsonToken.END_OBJECT || this._currToken == JsonToken.END_ARRAY) {
                this._parsingContext = this._parsingContext.getParent();
                if (this._parsingContext == null) {
                    this._parsingContext = JsonReadContext.createRootContext(-1, -1);
                }
            }
            return this._currToken;
        }

        public final boolean isClosed() {
            return this._closed;
        }

        public final JsonStreamContext getParsingContext() {
            return this._parsingContext;
        }

        public final JsonLocation getTokenLocation() {
            return getCurrentLocation();
        }

        public final JsonLocation getCurrentLocation() {
            return this._location == null ? JsonLocation.NA : this._location;
        }

        public final String getCurrentName() {
            return this._parsingContext.getCurrentName();
        }

        public final String getText() {
            if (this._currToken == JsonToken.VALUE_STRING || this._currToken == JsonToken.FIELD_NAME) {
                Object _currentObject = _currentObject();
                if (_currentObject instanceof String) {
                    return (String) _currentObject;
                }
                if (_currentObject == null) {
                    return null;
                }
                return _currentObject.toString();
            } else if (this._currToken == null) {
                return null;
            } else {
                switch (this._currToken) {
                    case VALUE_NUMBER_INT:
                    case VALUE_NUMBER_FLOAT:
                        Object _currentObject2 = _currentObject();
                        if (_currentObject2 == null) {
                            return null;
                        }
                        return _currentObject2.toString();
                    default:
                        return this._currToken.asString();
                }
            }
        }

        public final char[] getTextCharacters() {
            String text = getText();
            if (text == null) {
                return null;
            }
            return text.toCharArray();
        }

        public final int getTextLength() {
            String text = getText();
            if (text == null) {
                return 0;
            }
            return text.length();
        }

        public final int getTextOffset() {
            return 0;
        }

        public final boolean hasTextCharacters() {
            return false;
        }

        public final BigInteger getBigIntegerValue() throws IOException, JsonParseException {
            Number numberValue = getNumberValue();
            if (numberValue instanceof BigInteger) {
                return (BigInteger) numberValue;
            }
            switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonParser$NumberType[getNumberType().ordinal()]) {
                case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                    return ((BigDecimal) numberValue).toBigInteger();
                default:
                    return BigInteger.valueOf(numberValue.longValue());
            }
        }

        public final BigDecimal getDecimalValue() throws IOException, JsonParseException {
            Number numberValue = getNumberValue();
            if (numberValue instanceof BigDecimal) {
                return (BigDecimal) numberValue;
            }
            switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonParser$NumberType[getNumberType().ordinal()]) {
                case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                case JsonWriteContext.STATUS_EXPECT_NAME:
                    return BigDecimal.valueOf(numberValue.longValue());
                case JsonWriteContext.STATUS_OK_AFTER_COLON:
                    return new BigDecimal((BigInteger) numberValue);
                case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                case JsonWriteContext.STATUS_EXPECT_VALUE:
                default:
                    return BigDecimal.valueOf(numberValue.doubleValue());
            }
        }

        public final double getDoubleValue() throws IOException, JsonParseException {
            return getNumberValue().doubleValue();
        }

        public final float getFloatValue() throws IOException, JsonParseException {
            return getNumberValue().floatValue();
        }

        public final int getIntValue() throws IOException, JsonParseException {
            if (this._currToken == JsonToken.VALUE_NUMBER_INT) {
                return ((Number) _currentObject()).intValue();
            }
            return getNumberValue().intValue();
        }

        public final long getLongValue() throws IOException, JsonParseException {
            return getNumberValue().longValue();
        }

        public final JsonParser.NumberType getNumberType() throws IOException, JsonParseException {
            Number numberValue = getNumberValue();
            if (numberValue instanceof Integer) {
                return JsonParser.NumberType.INT;
            }
            if (numberValue instanceof Long) {
                return JsonParser.NumberType.LONG;
            }
            if (numberValue instanceof Double) {
                return JsonParser.NumberType.DOUBLE;
            }
            if (numberValue instanceof BigDecimal) {
                return JsonParser.NumberType.BIG_DECIMAL;
            }
            if (numberValue instanceof Float) {
                return JsonParser.NumberType.FLOAT;
            }
            if (numberValue instanceof BigInteger) {
                return JsonParser.NumberType.BIG_INTEGER;
            }
            return null;
        }

        public final Number getNumberValue() throws IOException, JsonParseException {
            _checkIsNumber();
            return (Number) _currentObject();
        }

        public final Object getEmbeddedObject() {
            if (this._currToken == JsonToken.VALUE_EMBEDDED_OBJECT) {
                return _currentObject();
            }
            return null;
        }

        public final byte[] getBinaryValue(Base64Variant base64Variant) throws IOException, JsonParseException {
            if (this._currToken == JsonToken.VALUE_EMBEDDED_OBJECT) {
                Object _currentObject = _currentObject();
                if (_currentObject instanceof byte[]) {
                    return (byte[]) _currentObject;
                }
            }
            if (this._currToken != JsonToken.VALUE_STRING) {
                throw _constructError("Current token (" + this._currToken + ") not VALUE_STRING (or VALUE_EMBEDDED_OBJECT with byte[]), can not access as binary");
            }
            String text = getText();
            if (text == null) {
                return null;
            }
            ByteArrayBuilder byteArrayBuilder = this._byteBuilder;
            if (byteArrayBuilder == null) {
                byteArrayBuilder = new ByteArrayBuilder(100);
                this._byteBuilder = byteArrayBuilder;
            }
            _decodeBase64(text, byteArrayBuilder, base64Variant);
            return byteArrayBuilder.toByteArray();
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
            throw _constructError("Unexpected end-of-String in base64 content");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
            r1 = r2 + 1;
            r2 = r11.charAt(r2);
            r4 = r13.decodeBase64Char(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0033, code lost:
            if (r4 >= 0) goto L_0x0039;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0035, code lost:
            _reportInvalidBase64(r13, r2, 1, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0039, code lost:
            r2 = (r3 << 6) | r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
            if (r1 < r0) goto L_0x0045;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0044, code lost:
            throw _constructError("Unexpected end-of-String in base64 content");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0045, code lost:
            r3 = r1 + 1;
            r1 = r11.charAt(r1);
            r4 = r13.decodeBase64Char(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x004f, code lost:
            if (r4 >= 0) goto L_0x0093;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0051, code lost:
            if (r4 == -2) goto L_0x0057;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0053, code lost:
            _reportInvalidBase64(r13, r1, 2, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0057, code lost:
            if (r3 < r0) goto L_0x0060;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x005f, code lost:
            throw _constructError("Unexpected end-of-String in base64 content");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0060, code lost:
            r1 = r3 + 1;
            r3 = r11.charAt(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x006a, code lost:
            if (r13.usesPaddingChar(r3) != false) goto L_0x008c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x006c, code lost:
            _reportInvalidBase64(r13, r3, 3, "expected padding character '" + r13.getPaddingChar() + "'");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x008c, code lost:
            r12.append(r2 >> 4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0093, code lost:
            r1 = (r2 << 6) | r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0096, code lost:
            if (r3 < r0) goto L_0x009b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0098, code lost:
            _reportBase64EOF();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x009b, code lost:
            r2 = r3 + 1;
            r3 = r11.charAt(r3);
            r4 = r13.decodeBase64Char(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a5, code lost:
            if (r4 >= 0) goto L_0x00b4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a7, code lost:
            if (r4 == -2) goto L_0x00ac;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a9, code lost:
            _reportInvalidBase64(r13, r3, 3, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ac, code lost:
            r12.appendTwoBytes(r1 >> 2);
            r1 = r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b4, code lost:
            r12.appendThreeBytes((r1 << 6) | r4);
            r1 = r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
            r3 = r13.decodeBase64Char(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x001b, code lost:
            if (r3 >= 0) goto L_0x0020;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
            _reportInvalidBase64(r13, r1, 0, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0020, code lost:
            if (r2 < r0) goto L_0x0029;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void _decodeBase64(java.lang.String r11, org.codehaus.jackson.util.ByteArrayBuilder r12, org.codehaus.jackson.Base64Variant r13) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
            /*
                r10 = this;
                r9 = 3
                r8 = 0
                r7 = -2
                r6 = 0
                int r0 = r11.length()
                r1 = r8
            L_0x0009:
                if (r1 >= r0) goto L_0x00bd
            L_0x000b:
                int r2 = r1 + 1
                char r1 = r11.charAt(r1)
                if (r2 >= r0) goto L_0x00bd
                r3 = 32
                if (r1 <= r3) goto L_0x00be
                int r3 = r13.decodeBase64Char(r1)
                if (r3 >= 0) goto L_0x0020
                r10._reportInvalidBase64(r13, r1, r8, r6)
            L_0x0020:
                if (r2 < r0) goto L_0x0029
                java.lang.String r0 = "Unexpected end-of-String in base64 content"
                org.codehaus.jackson.JsonParseException r0 = r10._constructError(r0)
                throw r0
            L_0x0029:
                int r1 = r2 + 1
                char r2 = r11.charAt(r2)
                int r4 = r13.decodeBase64Char(r2)
                if (r4 >= 0) goto L_0x0039
                r5 = 1
                r10._reportInvalidBase64(r13, r2, r5, r6)
            L_0x0039:
                int r2 = r3 << 6
                r2 = r2 | r4
                if (r1 < r0) goto L_0x0045
                java.lang.String r0 = "Unexpected end-of-String in base64 content"
                org.codehaus.jackson.JsonParseException r0 = r10._constructError(r0)
                throw r0
            L_0x0045:
                int r3 = r1 + 1
                char r1 = r11.charAt(r1)
                int r4 = r13.decodeBase64Char(r1)
                if (r4 >= 0) goto L_0x0093
                if (r4 == r7) goto L_0x0057
                r4 = 2
                r10._reportInvalidBase64(r13, r1, r4, r6)
            L_0x0057:
                if (r3 < r0) goto L_0x0060
                java.lang.String r0 = "Unexpected end-of-String in base64 content"
                org.codehaus.jackson.JsonParseException r0 = r10._constructError(r0)
                throw r0
            L_0x0060:
                int r1 = r3 + 1
                char r3 = r11.charAt(r3)
                boolean r4 = r13.usesPaddingChar(r3)
                if (r4 != 0) goto L_0x008c
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "expected padding character '"
                java.lang.StringBuilder r4 = r4.append(r5)
                char r5 = r13.getPaddingChar()
                java.lang.StringBuilder r4 = r4.append(r5)
                java.lang.String r5 = "'"
                java.lang.StringBuilder r4 = r4.append(r5)
                java.lang.String r4 = r4.toString()
                r10._reportInvalidBase64(r13, r3, r9, r4)
            L_0x008c:
                int r2 = r2 >> 4
                r12.append(r2)
                goto L_0x0009
            L_0x0093:
                int r1 = r2 << 6
                r1 = r1 | r4
                if (r3 < r0) goto L_0x009b
                r10._reportBase64EOF()
            L_0x009b:
                int r2 = r3 + 1
                char r3 = r11.charAt(r3)
                int r4 = r13.decodeBase64Char(r3)
                if (r4 >= 0) goto L_0x00b4
                if (r4 == r7) goto L_0x00ac
                r10._reportInvalidBase64(r13, r3, r9, r6)
            L_0x00ac:
                int r1 = r1 >> 2
                r12.appendTwoBytes(r1)
                r1 = r2
                goto L_0x0009
            L_0x00b4:
                int r1 = r1 << 6
                r1 = r1 | r4
                r12.appendThreeBytes(r1)
                r1 = r2
                goto L_0x0009
            L_0x00bd:
                return
            L_0x00be:
                r1 = r2
                goto L_0x000b
            */
            throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.util.TokenBuffer.Parser._decodeBase64(java.lang.String, org.codehaus.jackson.util.ByteArrayBuilder, org.codehaus.jackson.Base64Variant):void");
        }

        /* access modifiers changed from: protected */
        public final Object _currentObject() {
            return this._segment.get(this._segmentPtr);
        }

        /* access modifiers changed from: protected */
        public final void _checkIsNumber() throws JsonParseException {
            if (this._currToken == null || !this._currToken.isNumeric()) {
                throw _constructError("Current token (" + this._currToken + ") not numeric, can not use numeric value accessors");
            }
        }

        /* access modifiers changed from: protected */
        public final void _reportInvalidBase64(Base64Variant base64Variant, char c, int i, String str) throws JsonParseException {
            String str2;
            if (c <= ' ') {
                str2 = "Illegal white space character (code 0x" + Integer.toHexString(c) + ") as character #" + (i + 1) + " of 4-char base64 unit: can only used between units";
            } else if (base64Variant.usesPaddingChar(c)) {
                str2 = "Unexpected padding character ('" + base64Variant.getPaddingChar() + "') as character #" + (i + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
            } else if (!Character.isDefined(c) || Character.isISOControl(c)) {
                str2 = "Illegal character (code 0x" + Integer.toHexString(c) + ") in base64 content";
            } else {
                str2 = "Illegal character '" + c + "' (code 0x" + Integer.toHexString(c) + ") in base64 content";
            }
            if (str != null) {
                str2 = str2 + ": " + str;
            }
            throw _constructError(str2);
        }

        /* access modifiers changed from: protected */
        public final void _reportBase64EOF() throws JsonParseException {
            throw _constructError("Unexpected end-of-String in base64 content");
        }

        /* access modifiers changed from: protected */
        public final void _handleEOF() throws JsonParseException {
            _throwInternal();
        }
    }

    protected static final class Segment {
        public static final int TOKENS_PER_SEGMENT = 16;
        private static final JsonToken[] TOKEN_TYPES_BY_INDEX = new JsonToken[16];
        protected Segment _next;
        protected long _tokenTypes;
        protected final Object[] _tokens = new Object[16];

        static {
            JsonToken[] values = JsonToken.values();
            System.arraycopy(values, 1, TOKEN_TYPES_BY_INDEX, 1, Math.min(15, values.length - 1));
        }

        public final JsonToken type(int i) {
            long j = this._tokenTypes;
            if (i > 0) {
                j >>= i << 2;
            }
            return TOKEN_TYPES_BY_INDEX[((int) j) & 15];
        }

        public final Object get(int i) {
            return this._tokens[i];
        }

        public final Segment next() {
            return this._next;
        }

        public final Segment append(int i, JsonToken jsonToken) {
            if (i < 16) {
                set(i, jsonToken);
                return null;
            }
            this._next = new Segment();
            this._next.set(0, jsonToken);
            return this._next;
        }

        public final Segment append(int i, JsonToken jsonToken, Object obj) {
            if (i < 16) {
                set(i, jsonToken, obj);
                return null;
            }
            this._next = new Segment();
            this._next.set(0, jsonToken, obj);
            return this._next;
        }

        public final void set(int i, JsonToken jsonToken) {
            long ordinal = (long) jsonToken.ordinal();
            if (i > 0) {
                ordinal <<= i << 2;
            }
            this._tokenTypes = ordinal | this._tokenTypes;
        }

        public final void set(int i, JsonToken jsonToken, Object obj) {
            this._tokens[i] = obj;
            long ordinal = (long) jsonToken.ordinal();
            if (i > 0) {
                ordinal <<= i << 2;
            }
            this._tokenTypes = ordinal | this._tokenTypes;
        }
    }
}
