package org.codehaus.jackson.schema;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;
import org.codehaus.jackson.node.JsonNodeFactory;
import org.codehaus.jackson.node.ObjectNode;

public class JsonSchema {
    private final ObjectNode schema;

    @JsonCreator
    public JsonSchema(ObjectNode objectNode) {
        this.schema = objectNode;
    }

    @JsonValue
    public ObjectNode getSchemaNode() {
        return this.schema;
    }

    public String toString() {
        return this.schema.toString();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof JsonSchema)) {
            return false;
        }
        JsonSchema jsonSchema = (JsonSchema) obj;
        if (this.schema == null) {
            return jsonSchema.schema == null;
        }
        return this.schema.equals(jsonSchema.schema);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.node.ObjectNode.put(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      org.codehaus.jackson.node.ObjectNode.put(java.lang.String, org.codehaus.jackson.JsonNode):org.codehaus.jackson.JsonNode
      org.codehaus.jackson.node.ObjectNode.put(java.lang.String, double):void
      org.codehaus.jackson.node.ObjectNode.put(java.lang.String, float):void
      org.codehaus.jackson.node.ObjectNode.put(java.lang.String, int):void
      org.codehaus.jackson.node.ObjectNode.put(java.lang.String, long):void
      org.codehaus.jackson.node.ObjectNode.put(java.lang.String, java.lang.String):void
      org.codehaus.jackson.node.ObjectNode.put(java.lang.String, java.math.BigDecimal):void
      org.codehaus.jackson.node.ObjectNode.put(java.lang.String, byte[]):void
      org.codehaus.jackson.node.ObjectNode.put(java.lang.String, boolean):void */
    public static JsonNode getDefaultSchemaNode() {
        ObjectNode objectNode = JsonNodeFactory.instance.objectNode();
        objectNode.put("type", "any");
        objectNode.put("optional", true);
        return objectNode;
    }
}
