package com.qq.jce.wup;

import com.qq.taf.RequestPacket;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceUtil;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class UniPacket extends UniAttribute {
    public static final int UniPacketHeadSize = 4;
    static HashMap<String, HashMap<String, byte[]>> cache__tempdata = null;
    protected RequestPacket _package = new RequestPacket();
    private int oldRespIret = 0;

    public UniPacket() {
        this._package.iVersion = 2;
    }

    public <T> void put(String name, T t) {
        if (name.startsWith(".")) {
            throw new IllegalArgumentException("put name can not startwith . , now is " + name);
        }
        super.put(name, t);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
     arg types: [java.util.HashMap, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void */
    public byte[] encode() {
        if (this._package.sServantName.equals("")) {
            throw new IllegalArgumentException("servantName can not is null");
        } else if (this._package.sFuncName.equals("")) {
            throw new IllegalArgumentException("funcName can not is null");
        } else {
            JceOutputStream _os = new JceOutputStream(0);
            _os.setServerEncoding(this.encodeName);
            _os.write((Map) this._data, 0);
            this._package.sBuffer = JceUtil.getJceBufArray(_os.getByteBuffer());
            JceOutputStream _os2 = new JceOutputStream(0);
            _os2.setServerEncoding(this.encodeName);
            writeTo(_os2);
            byte[] bodys = JceUtil.getJceBufArray(_os2.getByteBuffer());
            int size = bodys.length;
            ByteBuffer buf = ByteBuffer.allocate(size + 4);
            buf.putInt(size + 4).put(bodys).flip();
            return buf.array();
        }
    }

    public void decode(byte[] buffer) {
        if (buffer.length < 4) {
            throw new IllegalArgumentException("decode package must include size head");
        }
        try {
            JceInputStream _is = new JceInputStream(buffer, 4);
            _is.setServerEncoding(this.encodeName);
            readFrom(_is);
            JceInputStream _is2 = new JceInputStream(this._package.sBuffer);
            _is2.setServerEncoding(this.encodeName);
            if (cache__tempdata == null) {
                cache__tempdata = new HashMap<>();
                HashMap<String, byte[]> h = new HashMap<>();
                h.put("", new byte[0]);
                cache__tempdata.put("", h);
            }
            this._data = _is2.readMap(cache__tempdata, 0, false);
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            System.out.println("decode error " + WupHexUtil.bytes2HexStr(buffer));
            throw new RuntimeException(e2);
        }
    }

    public String getServantName() {
        return this._package.sServantName;
    }

    public void setServantName(String servantName) {
        this._package.sServantName = servantName;
    }

    public String getFuncName() {
        return this._package.sFuncName;
    }

    public void setFuncName(String sFuncName) {
        this._package.sFuncName = sFuncName;
    }

    public int getRequestId() {
        return this._package.iRequestId;
    }

    public void setRequestId(int iRequestId) {
        this._package.iRequestId = iRequestId;
    }

    public void writeTo(JceOutputStream _os) {
        this._package.writeTo(_os);
    }

    public void readFrom(JceInputStream _is) {
        this._package.readFrom(_is);
    }

    public void display(StringBuilder _os, int _level) {
        this._package.display(_os, _level);
    }

    public UniPacket createResponse() {
        UniPacket packet = new UniPacket();
        packet.setRequestId(getRequestId());
        packet.setServantName(getServantName());
        packet.setFuncName(getFuncName());
        packet.setEncodeName(this.encodeName);
        return packet;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
     arg types: [java.util.HashMap, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void */
    public byte[] createOldRespEncode() {
        JceOutputStream _os = new JceOutputStream(0);
        _os.setServerEncoding(this.encodeName);
        _os.write((Map) this._data, 0);
        byte[] oldSBuffer = JceUtil.getJceBufArray(_os.getByteBuffer());
        JceOutputStream _os2 = new JceOutputStream(0);
        _os2.setServerEncoding(this.encodeName);
        _os2.write(this._package.iVersion, 1);
        _os2.write(this._package.cPacketType, 2);
        _os2.write(this._package.iRequestId, 3);
        _os2.write(this._package.iMessageType, 4);
        _os2.write(this.oldRespIret, 5);
        _os2.write(oldSBuffer, 6);
        _os2.write((Map) this._package.status, 7);
        return JceUtil.getJceBufArray(_os2.getByteBuffer());
    }

    public int getOldRespIret() {
        return this.oldRespIret;
    }

    public void setOldRespIret(int oldRespIret2) {
        this.oldRespIret = oldRespIret2;
    }
}
