package com.mobisage.android;

import MobWin.cnst.PROTOCOL_ENCODING;
import android.os.Environment;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.LayeredSocketFactory;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SNSSSLSocketFactory implements LayeredSocketFactory {
    private SSLContext a = null;

    class a {
        final /* synthetic */ C0012l a;

        public static String a(File file) {
            try {
                FileReader fileReader = new FileReader(file);
                StringBuffer stringBuffer = new StringBuffer();
                char[] cArr = new char[1024];
                while (true) {
                    int read = fileReader.read(cArr);
                    if (read != -1) {
                        stringBuffer.append(cArr, 0, read);
                    } else {
                        fileReader.close();
                        return stringBuffer.toString();
                    }
                }
            } catch (Exception e) {
                return null;
            }
        }

        public static boolean a(String str, LinkedList<String> linkedList) {
            int indexOf = str.indexOf("/*cache begin*/");
            int indexOf2 = str.indexOf("/*cache end*/");
            if (indexOf == -1 || indexOf2 == -1 || indexOf + 15 > indexOf2) {
                return true;
            }
            try {
                JSONArray jSONArray = new JSONArray(str.substring(indexOf + 15, indexOf2));
                for (int i = 0; i < jSONArray.length(); i++) {
                    linkedList.add(jSONArray.getString(i));
                }
                return true;
            } catch (JSONException e) {
                return false;
            }
        }

        public static void a(File file, String str) {
            try {
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(str);
                fileWriter.close();
            } catch (Exception e) {
            }
        }

        public static boolean b(String str, LinkedList<String> linkedList) {
            int indexOf = str.indexOf("/*ad_datas_begin*/");
            int indexOf2 = str.indexOf("/*ad_datas_end*/");
            if (indexOf == -1 || indexOf2 == -1 || indexOf + 18 > indexOf2) {
                return true;
            }
            try {
                JSONArray jSONArray = new JSONArray(str.substring(indexOf + 18, indexOf2));
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    JSONArray jSONArray2 = jSONObject.getJSONArray("cache_items");
                    if (jSONArray2 != null) {
                        for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                            String string = jSONArray2.getString(i2);
                            if (jSONObject.has(string)) {
                                linkedList.add(jSONObject.getString(string));
                            }
                        }
                    }
                }
                return true;
            } catch (JSONException e) {
                return false;
            }
        }

        public static boolean a(MessageDigest messageDigest, String str, StringBuilder sb, StringBuilder sb2) {
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.reset();
                byte[] digest = instance.digest(EncodingUtils.getBytes(str, PROTOCOL_ENCODING.value));
                StringBuilder sb3 = new StringBuilder(digest.length << 1);
                for (int i = 0; i < digest.length; i++) {
                    sb3.append(Character.forDigit((digest[i] & 240) >> 4, 16));
                    sb3.append(Character.forDigit(digest[i] & 15, 16));
                }
                String sb4 = sb3.toString();
                String externalStorageState = Environment.getExternalStorageState();
                if ("unmounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState) || "unmountable".equals(externalStorageState) || "removed".equals(externalStorageState) || "bad_removal".equals(externalStorageState) || "nofs".equals(externalStorageState) || "shared".equals(externalStorageState)) {
                    sb.append(C0018r.e + "/MobiSage" + "/Cache" + "/" + sb4);
                    sb2.append(C0018r.e + "/MobiSage" + "/Temp" + "/" + sb4);
                } else {
                    File externalStorageDirectory = Environment.getExternalStorageDirectory();
                    sb.append(externalStorageDirectory.getAbsolutePath() + "/MobiSage" + "/Cache" + "/" + sb4);
                    sb2.append(externalStorageDirectory.getAbsolutePath() + "/MobiSage" + "/Temp" + "/" + sb4);
                }
                new File(sb.toString()).getParentFile().mkdirs();
                new File(sb2.toString()).getParentFile().mkdirs();
                return true;
            } catch (NoSuchAlgorithmException e) {
                return false;
            }
        }

        private a(C0012l lVar) {
            this.a = lVar;
        }

        /* synthetic */ a(C0012l lVar, byte b) {
            this(lVar);
        }

        public void a() {
            this.a.a.onLoadAdFinish();
        }
    }

    private static SSLContext a() {
        SSLContext sSLContext = null;
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            try {
                instance.init(null, new TrustManager[]{new b((byte) 0)}, new SecureRandom());
                return instance;
            } catch (NoSuchAlgorithmException e) {
                NoSuchAlgorithmException noSuchAlgorithmException = e;
                sSLContext = instance;
                e = noSuchAlgorithmException;
            } catch (KeyManagementException e2) {
                KeyManagementException keyManagementException = e2;
                sSLContext = instance;
                e = keyManagementException;
                e.printStackTrace();
                return sSLContext;
            }
        } catch (NoSuchAlgorithmException e3) {
            e = e3;
            e.printStackTrace();
            return sSLContext;
        } catch (KeyManagementException e4) {
            e = e4;
            e.printStackTrace();
            return sSLContext;
        }
    }

    private SSLContext b() {
        if (this.a == null) {
            this.a = a();
        }
        return this.a;
    }

    public Socket connectSocket(Socket socket, String host, int port, InetAddress localAddress, int localPort, HttpParams params) throws IOException, UnknownHostException, ConnectTimeoutException {
        if (params == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        int connectionTimeout = HttpConnectionParams.getConnectionTimeout(params);
        SSLSocketFactory socketFactory = b().getSocketFactory();
        if (connectionTimeout == 0) {
            return socketFactory.createSocket(host, port, localAddress, localPort);
        }
        Socket createSocket = socketFactory.createSocket();
        InetSocketAddress inetSocketAddress = new InetSocketAddress(localAddress, localPort);
        InetSocketAddress inetSocketAddress2 = new InetSocketAddress(host, port);
        createSocket.bind(inetSocketAddress);
        createSocket.connect(inetSocketAddress2, connectionTimeout);
        return createSocket;
    }

    public Socket createSocket() throws IOException {
        return b().getSocketFactory().createSocket();
    }

    public boolean isSecure(Socket sock) throws IllegalArgumentException {
        if (sock == null) {
            throw new IllegalArgumentException("Socket may not be null");
        } else if (!(sock instanceof SSLSocket)) {
            throw new IllegalArgumentException("Socket not created by this factory");
        } else if (!sock.isClosed()) {
            return true;
        } else {
            throw new IllegalArgumentException("Socket is closed");
        }
    }

    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
        return b().getSocketFactory().createSocket(socket, host, port, autoClose);
    }

    static class b implements X509TrustManager {
        private b() {
        }

        /* synthetic */ b(byte b) {
            this();
        }

        public final void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        }

        public final void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        }

        public final X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }
}
