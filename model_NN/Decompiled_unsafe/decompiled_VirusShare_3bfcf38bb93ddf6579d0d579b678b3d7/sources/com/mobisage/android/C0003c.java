package com.mobisage.android;

import android.os.Handler;
import android.os.Message;
import com.mobisage.android.SNSSSLSocketFactory;
import com.taobao.munion.ads.clientSDK.TaoAdsListener;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: com.mobisage.android.c  reason: case insensitive filesystem */
final class C0003c extends O {
    C0003c(Handler handler) {
        super(handler);
        this.c = 1006;
    }

    public final void a(Message message) {
        if (message.obj instanceof C0002b) {
            C0002b bVar = (C0002b) message.obj;
            this.e.put(bVar.b, bVar);
            if (!bVar.c.containsKey("LpgCache")) {
                this.e.remove(bVar.b);
                if (bVar.g != null) {
                    bVar.g.b(bVar);
                    return;
                }
                return;
            }
            ArrayList<String> stringArrayList = bVar.c.getStringArrayList("LpgCache");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= stringArrayList.size()) {
                    break;
                }
                URI create = URI.create(stringArrayList.get(i2));
                if (create.getScheme().equals("file")) {
                    String a = SNSSSLSocketFactory.a.a(new File(create));
                    LinkedList linkedList = new LinkedList();
                    if (SNSSSLSocketFactory.a.a(a, linkedList) && SNSSSLSocketFactory.a.b(a, linkedList)) {
                        int i3 = 0;
                        while (true) {
                            int i4 = i3;
                            if (i4 >= linkedList.size()) {
                                break;
                            }
                            if (!URI.create((String) linkedList.get(i4)).getScheme().equals("file")) {
                                String str = (String) linkedList.get(i4);
                                if (a.indexOf(str) != -1) {
                                    StringBuilder sb = new StringBuilder();
                                    StringBuilder sb2 = new StringBuilder();
                                    if (!SNSSSLSocketFactory.a.a(null, str, sb, sb2)) {
                                        break;
                                    }
                                    C0002b bVar2 = new C0002b();
                                    bVar2.c.putString("OwnerURL", create.getPath());
                                    bVar2.c.putString("SourceURL", str);
                                    bVar2.c.putString("TempURL", sb2.toString());
                                    bVar2.c.putString("TargetURL", sb.toString());
                                    bVar2.g = this.b;
                                    bVar2.a = bVar.b;
                                    bVar.e.add(bVar2);
                                } else {
                                    continue;
                                }
                            }
                            i3 = i4 + 1;
                        }
                    }
                }
                i = i2 + 1;
            }
            Iterator<C0002b> it = bVar.e.iterator();
            while (it.hasNext()) {
                C0002b next = it.next();
                String string = next.c.getString("SourceURL");
                String substring = string.substring(string.indexOf(".") + 1);
                if (substring.equals("html") || substring.equals("htm")) {
                    Message obtainMessage = this.a.obtainMessage(1014);
                    obtainMessage.obj = next;
                    obtainMessage.sendToTarget();
                } else {
                    Message obtainMessage2 = this.a.obtainMessage(TaoAdsListener.EVENT_TYPE_HOSTS_MODIFIED);
                    obtainMessage2.obj = next;
                    obtainMessage2.sendToTarget();
                }
            }
            if (bVar.a()) {
                this.e.remove(bVar.b);
                if (bVar.g != null) {
                    bVar.g.a(bVar);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(C0002b bVar) {
        if (this.e.containsKey(bVar.a)) {
            C0002b bVar2 = (C0002b) this.e.get(bVar.a);
            bVar2.e.remove(bVar);
            if (bVar.c.containsKey("OwnerURL")) {
                String string = bVar.c.getString("OwnerURL");
                SNSSSLSocketFactory.a.a(new File(string), SNSSSLSocketFactory.a.a(new File(string)).replace(bVar.c.getString("SourceURL"), "file://" + bVar.c.getString("TargetURL")));
            }
            if (bVar2.a()) {
                this.e.remove(bVar2.b);
                if (bVar2.g != null) {
                    bVar2.g.a(bVar2);
                }
            }
        }
    }
}
