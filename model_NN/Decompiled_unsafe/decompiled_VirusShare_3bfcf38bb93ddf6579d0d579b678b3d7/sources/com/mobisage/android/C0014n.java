package com.mobisage.android;

import android.content.Context;
import android.webkit.WebSettings;

/* renamed from: com.mobisage.android.n  reason: case insensitive filesystem */
class C0014n extends af {
    private F a;

    C0014n(Context context) {
        super(context);
        setAnimationCacheEnabled(true);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        getSettings().setRenderPriority(WebSettings.RenderPriority.LOW);
        this.a = new F(context);
        addJavascriptInterface(this.a, "ad");
    }

    public final void a(String str) {
        this.a.d = str;
    }

    public final void b(String str) {
        this.a.e = str;
    }

    public final void c(String str) {
        this.a.a = str;
    }

    public final void d(String str) {
        this.a.b = str;
    }

    public final void e(String str) {
        this.a.c = str;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        this.a.destoryJavascriptAgent();
        this.a = null;
        clearCache(false);
        freeMemory();
        destroyDrawingCache();
        destroy();
        super.finalize();
    }
}
