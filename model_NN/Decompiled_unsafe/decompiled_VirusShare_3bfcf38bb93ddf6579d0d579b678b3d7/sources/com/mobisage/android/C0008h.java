package com.mobisage.android;

import android.text.format.Time;

/* renamed from: com.mobisage.android.h  reason: case insensitive filesystem */
final class C0008h extends C {
    private long a = 0;
    private int b = 0;

    C0008h() {
    }

    public final boolean a() {
        int intValue = ((Integer) C0023w.a().a("intervalcountlimit")).intValue();
        if (intValue == 0) {
            return false;
        }
        Time time = new Time();
        time.setToNow();
        long millis = time.toMillis(true);
        if (millis - this.a > 10000) {
            this.a = millis;
            this.b = 1;
        } else {
            this.b++;
        }
        if (this.b > intValue) {
            return true;
        }
        return false;
    }
}
