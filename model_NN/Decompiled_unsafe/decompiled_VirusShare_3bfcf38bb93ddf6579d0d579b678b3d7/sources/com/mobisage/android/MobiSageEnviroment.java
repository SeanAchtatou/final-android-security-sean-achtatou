package com.mobisage.android;

public final class MobiSageEnviroment {
    public static final int App_Customer_Event = 0;
    public static final int App_Launching_Event = 1;
    public static final int App_Terminating_Event = 2;
    public static final String SDK_Version = "2.3.0";

    public class SystemEvent {
        public static final String App_Launching = "In";
        public static final String App_Terminating = "Out";

        public SystemEvent(MobiSageEnviroment mobiSageEnviroment) {
        }
    }

    public class AdRefreshInterval {
        public static final int Ad_No_Refresh = 0;
        public static final int Ad_Refresh_15 = 1;
        public static final int Ad_Refresh_20 = 2;
        public static final int Ad_Refresh_25 = 3;
        public static final int Ad_Refresh_30 = 4;
        public static final int Ad_Refresh_60 = 5;

        public AdRefreshInterval(MobiSageEnviroment mobiSageEnviroment) {
        }
    }

    static int a(int i) {
        switch (i) {
            case 0:
                return 7200;
            case 1:
                return 15;
            case 2:
            default:
                return 20;
            case 3:
                return 25;
            case 4:
                return 30;
            case 5:
                return 60;
        }
    }
}
