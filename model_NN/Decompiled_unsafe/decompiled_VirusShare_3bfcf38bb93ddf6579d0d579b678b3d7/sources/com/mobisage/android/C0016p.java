package com.mobisage.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/* renamed from: com.mobisage.android.p  reason: case insensitive filesystem */
final class C0016p extends BroadcastReceiver {
    C0016p() {
    }

    public final void onReceive(Context context, Intent intent) {
        Long valueOf = Long.valueOf(intent.getLongExtra("extra_download_id", 0));
        Intent intent2 = new Intent(context, MobiSageApkService.class);
        Bundle bundle = new Bundle();
        bundle.putInt("action", 4);
        bundle.putLong("did", valueOf.longValue());
        intent2.putExtra("ExtraData", bundle);
        context.startService(intent2);
    }
}
