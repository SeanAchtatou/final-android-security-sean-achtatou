package com.mobisage.android;

import MobWin.cnst.PROTOCOL_ENCODING;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;

final class L extends N {
    private InputStream c;
    private BufferedInputStream d;
    private FileOutputStream e;

    public L(MobiSageResMessage mobiSageResMessage) {
        super(mobiSageResMessage);
    }

    public final void a() {
        super.a();
        try {
            if (this.d != null) {
                this.d.close();
            }
            if (this.c != null) {
                this.c.close();
            }
            if (this.e != null) {
                this.e.close();
            }
        } catch (IOException e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public final void run() {
        MobiSageResMessage mobiSageResMessage = (MobiSageResMessage) this.a;
        try {
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", new SNSSSLSocketFactory(), 443));
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            basicHttpParams.setParameter("http.connection.timeout", 5000);
            basicHttpParams.setParameter("http.socket.timeout", 5000);
            HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(basicHttpParams, PROTOCOL_ENCODING.value);
            this.b = new DefaultHttpClient(new SingleClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
            HttpResponse execute = this.b.execute(mobiSageResMessage.createHttpRequest());
            int statusCode = execute.getStatusLine().getStatusCode();
            mobiSageResMessage.result.putInt("StatusCode", statusCode);
            if (statusCode < 400) {
                File file = new File(mobiSageResMessage.tempURL);
                if (!file.exists()) {
                    file.createNewFile();
                }
                if (mobiSageResMessage.d.booleanValue() && statusCode != 206) {
                    file.delete();
                    file.createNewFile();
                }
                this.c = execute.getEntity().getContent();
                this.d = new BufferedInputStream(this.c);
                this.e = new FileOutputStream(file, true);
                byte[] bArr = new byte[65536];
                while (true) {
                    int read = this.d.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    this.e.write(bArr, 0, read);
                }
                this.d.close();
                this.c.close();
                this.e.close();
                if (file.length() != ((long) Integer.parseInt(execute.getHeaders("Content-Length")[0].getValue())) + mobiSageResMessage.e) {
                    file.delete();
                    mobiSageResMessage.result.putInt("StatusCode", 400);
                } else {
                    file.renameTo(new File(mobiSageResMessage.targetURL));
                }
            }
            if (mobiSageResMessage.callback != null) {
                mobiSageResMessage.callback.onMobiSageMessageFinish(mobiSageResMessage);
            }
        } catch (Exception e2) {
            mobiSageResMessage.result.putString("ErrorText", e2.getLocalizedMessage());
            if (mobiSageResMessage.callback != null) {
                mobiSageResMessage.callback.onMobiSageMessageFinish(mobiSageResMessage);
            }
        } catch (Throwable th) {
            if (mobiSageResMessage.callback != null) {
                mobiSageResMessage.callback.onMobiSageMessageFinish(mobiSageResMessage);
            }
            throw th;
        }
        super.run();
    }
}
