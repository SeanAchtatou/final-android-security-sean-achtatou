package com.mobisage.android;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.mobisage.android.w  reason: case insensitive filesystem */
final class C0023w extends G {
    private static C0023w c = new C0023w();
    /* access modifiers changed from: private */
    public JSONObject d = new JSONObject();
    private b e;
    /* access modifiers changed from: private */
    public a f;

    public static C0023w a() {
        return c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private C0023w() {
        C0021u uVar = new C0021u(this.a);
        this.b.put(Integer.valueOf(uVar.c), uVar);
        this.f = new a(this, (byte) 0);
        this.e = new b(this, (byte) 0);
        try {
            this.d.put("intervaltime", 15);
            this.d.put("adanimation", 0);
            this.d.put("adswitch", true);
            this.d.put("intervalswitchtype", 0);
            this.d.put("intervalcountlimit", 0);
            this.d.put("enablelocation", false);
        } catch (JSONException e2) {
        }
        R.a().a(this.e);
    }

    public final Object a(String str) {
        if (!this.d.has(str)) {
            return null;
        }
        try {
            return this.d.get(str);
        } catch (JSONException e2) {
            return null;
        }
    }

    /* renamed from: com.mobisage.android.w$b */
    class b extends Q {
        /* synthetic */ b(C0023w wVar, byte b) {
            this();
        }

        private b() {
            this.c = 5;
            this.d = true;
            this.e = 600;
        }

        public final void run() {
            C0002b bVar = new C0002b();
            bVar.g = C0023w.this.f;
            C0023w.a().a(3000, bVar);
        }
    }

    /* renamed from: com.mobisage.android.w$a */
    class a implements C0001a {
        private a() {
        }

        /* synthetic */ a(C0023w wVar, byte b) {
            this();
        }

        public final void a(C0002b bVar) {
            try {
                JSONObject unused = C0023w.this.d = new JSONObject(bVar.d.getString("ResponseBody"));
            } catch (JSONException e) {
            }
        }

        public final void b(C0002b bVar) {
        }
    }
}
