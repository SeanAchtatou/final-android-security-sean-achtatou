package com.mobisage.android;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import java.io.File;
import java.net.URL;

/* renamed from: com.mobisage.android.f  reason: case insensitive filesystem */
final class C0006f extends C0014n {
    C0006f(Context context, Intent intent) {
        super(context);
        getSettings().setSupportZoom(true);
        getSettings().setBuiltInZoomControls(true);
        setHorizontalScrollBarEnabled(true);
        setVerticalScrollBarEnabled(true);
        getSettings().setRenderPriority(WebSettings.RenderPriority.NORMAL);
        e(intent.getStringExtra("pid"));
        a(intent.getStringExtra("adid"));
        b(intent.getStringExtra("adgroupid"));
        c(intent.getStringExtra("token"));
        if (intent.hasExtra("customdata")) {
            d(intent.getStringExtra("customdata"));
        }
        setWebViewClient(new a(this, (byte) 0));
        loadUrl(intent.getStringExtra("lpg"));
    }

    /* renamed from: com.mobisage.android.f$a */
    class a extends WebViewClient {
        private a(C0006f fVar) {
        }

        /* synthetic */ a(C0006f fVar, byte b) {
            this(fVar);
        }

        public final boolean shouldOverrideUrlLoading(WebView view, String url) {
            try {
                URL url2 = new URL(url);
                String path = url2.getPath();
                if (path.substring(path.lastIndexOf(".") + 1).equals("apk")) {
                    if (Build.VERSION.SDK_INT >= 9) {
                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                        request.setMimeType("application/vnd.android.package-archive");
                        ((DownloadManager) C0018r.h.getSystemService("download")).enqueue(request);
                    } else if (!Environment.getExternalStorageDirectory().exists()) {
                        Toast.makeText(C0018r.h, "Can't find sdcard!", 5).show();
                    } else {
                        Intent intent = new Intent(C0018r.h, MobiSageApkService.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("action", 0);
                        bundle.putString("lpg", url);
                        bundle.putString("name", new File(url2.getFile()).getName());
                        intent.putExtra("ExtraData", bundle);
                        C0018r.h.startService(intent);
                    }
                }
            } catch (Exception e) {
            }
            return super.shouldOverrideUrlLoading(view, url);
        }
    }
}
