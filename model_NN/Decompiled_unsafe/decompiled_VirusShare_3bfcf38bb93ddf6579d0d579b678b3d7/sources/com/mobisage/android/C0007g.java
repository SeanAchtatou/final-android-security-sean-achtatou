package com.mobisage.android;

import android.content.Context;
import android.util.AttributeSet;

/* renamed from: com.mobisage.android.g  reason: case insensitive filesystem */
abstract class C0007g extends C0012l {
    private C0011k a;
    private Integer b = 2;

    public C0007g(Context context, String str, String str2, String str3) {
        super(context, str, str2, str3);
    }

    public C0007g(Context context, int i, String str, String str2, String str3) {
        super(context, i, str, str2, str3);
    }

    public C0007g(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public Integer getAdRefreshInterval() {
        return this.b;
    }

    public void setAdRefreshInterval(Integer adRefreshInterval) {
        this.b = adRefreshInterval;
    }

    /* access modifiers changed from: protected */
    public void initMobiSageAdView(Context context) {
        super.initMobiSageAdView(context);
        sendADRequest();
    }

    /* access modifiers changed from: protected */
    public void destoryAdView() {
        a();
        this.a.a = null;
        this.a = null;
        super.destoryAdView();
    }

    /* access modifiers changed from: protected */
    public boolean requestADFromDE() {
        boolean z;
        boolean z2;
        a();
        if ((((Integer) C0023w.a().a("intervalswitchtype")).intValue() == 1 ? ((Integer) C0023w.a().a("intervaltime")).intValue() : MobiSageEnviroment.a(this.b.intValue())) == 7200) {
            z = false;
        } else {
            z = true;
        }
        if (z) {
            z2 = super.requestADFromDE();
        } else {
            z2 = true;
        }
        if (z2) {
            a(15);
        }
        return z2;
    }

    private void a() {
        if (this.a != null) {
            R.a().b(this.a);
        }
    }

    private void a(int i) {
        if (this.a == null) {
            this.a = new C0011k(this.mainHandler);
        } else {
            R.a().b(this.a);
        }
        this.a.c = 15;
        R.a().a(this.a);
    }

    /* access modifiers changed from: protected */
    public void requestADFinish(C0002b action) {
        a();
        super.requestADFinish(action);
    }

    /* access modifiers changed from: protected */
    public void onLoadAdFinish() {
        int i;
        super.onLoadAdFinish();
        int a2 = MobiSageEnviroment.a(this.b.intValue());
        if (((Integer) C0023w.a().a("intervalswitchtype")).intValue() == 1) {
            i = ((Integer) C0023w.a().a("intervaltime")).intValue();
        } else {
            i = a2;
        }
        if (i == 7200) {
            a(15);
            return;
        }
        if (this.a == null) {
            this.a = new C0011k(this.mainHandler);
        } else {
            R.a().b(this.a);
        }
        this.a.c = (long) i;
        R.a().a(this.a);
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
    }
}
