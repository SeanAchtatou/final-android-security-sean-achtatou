package com.mobisage.android;

import android.os.Handler;
import android.os.Message;
import com.mobisage.android.SNSSSLSocketFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

/* renamed from: com.mobisage.android.d  reason: case insensitive filesystem */
final class C0004d extends O {
    private a g = new a(this, (byte) 0);

    C0004d(Handler handler) {
        super(handler);
        this.c = 1014;
    }

    public final void a(Message message) {
        if (message.obj instanceof C0002b) {
            C0002b bVar = (C0002b) message.obj;
            this.e.put(bVar.b, bVar);
            MobiSageResMessage mobiSageResMessage = new MobiSageResMessage();
            mobiSageResMessage.callback = this.g;
            mobiSageResMessage.sourceURL = bVar.c.getString("SourceURL");
            mobiSageResMessage.targetURL = bVar.c.getString("TragetURL");
            mobiSageResMessage.tempURL = bVar.c.getString("TempURL");
            bVar.f.add(mobiSageResMessage);
            this.f.put(mobiSageResMessage.c, bVar.b);
            H.a().a(mobiSageResMessage);
        } else if (message.obj instanceof MobiSageResMessage) {
            MobiSageResMessage mobiSageResMessage2 = (MobiSageResMessage) message.obj;
            C0002b bVar2 = (C0002b) this.e.get((UUID) this.f.get(mobiSageResMessage2.c));
            this.f.remove(mobiSageResMessage2.c);
            bVar2.f.remove(mobiSageResMessage2);
            if (mobiSageResMessage2.result.getInt("StatusCode") >= 400) {
                c(bVar2);
                return;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(mobiSageResMessage2.targetURL);
            C0002b bVar3 = new C0002b();
            bVar3.c.putString("OwnerURL", bVar2.c.getString("TragetURL"));
            bVar3.c.putStringArrayList("LpgCache", arrayList);
            bVar3.a = bVar2.b;
            bVar3.g = this.b;
            bVar2.e.add(bVar3);
            Message obtainMessage = this.a.obtainMessage(1006);
            obtainMessage.obj = bVar3;
            obtainMessage.sendToTarget();
            if (bVar2.a()) {
                this.e.remove(bVar2.b);
                if (bVar2.g != null) {
                    bVar2.g.a(bVar2);
                }
            }
        }
    }

    /* renamed from: com.mobisage.android.d$a */
    class a implements IMobiSageMessageCallback {
        private a() {
        }

        /* synthetic */ a(C0004d dVar, byte b) {
            this();
        }

        public final void onMobiSageMessageFinish(MobiSageMessage msg) {
            Message obtainMessage = C0004d.this.a.obtainMessage(1014);
            obtainMessage.obj = msg;
            obtainMessage.sendToTarget();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(C0002b bVar) {
        if (this.e.containsKey(bVar.a)) {
            C0002b bVar2 = (C0002b) this.e.get(bVar.a);
            bVar2.e.remove(bVar);
            if (bVar.c.containsKey("OwnerURL")) {
                String string = bVar.c.getString("OwnerURL");
                SNSSSLSocketFactory.a.a(new File(string), SNSSSLSocketFactory.a.a(new File(string)).replace(bVar.c.getString("SourceURL"), "file://" + bVar.c.getString("TargetURL")));
            }
            if (bVar2.a()) {
                this.e.remove(bVar2.b);
                if (bVar2.g != null) {
                    bVar2.g.a(bVar2);
                }
            }
        }
    }
}
