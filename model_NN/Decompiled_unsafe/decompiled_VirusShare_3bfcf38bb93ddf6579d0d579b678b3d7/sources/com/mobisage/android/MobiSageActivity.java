package com.mobisage.android;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import java.io.IOException;

public final class MobiSageActivity extends Activity {
    private af a;
    /* access modifiers changed from: private */
    public Activity b;

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        requestWindowFeature(2);
        this.b = this;
        setProgressBarIndeterminateVisibility(true);
        setProgressBarVisibility(true);
        Intent intent = getIntent();
        if (intent.getStringExtra("type").equals("baidu")) {
            this.a = new C0019s(this, intent);
        } else {
            this.a = new C0006f(this, intent);
        }
        this.a.setWebChromeClient(new a(this, (byte) 0));
        setContentView(this.a);
    }

    class a extends WebChromeClient {
        private a() {
        }

        /* synthetic */ a(MobiSageActivity mobiSageActivity, byte b) {
            this();
        }

        public final void onProgressChanged(WebView webView, int newProgress) {
            MobiSageActivity.this.b.setProgress(newProgress * 100);
        }
    }

    public final boolean onCreateOptionsMenu(Menu menu) {
        try {
            menu.add(0, 0, 0, "后退").setIcon(Drawable.createFromStream(getAssets().open("mobisage/back.png"), null));
            menu.add(0, 1, 0, "前进").setIcon(Drawable.createFromStream(getAssets().open("mobisage/foward.png"), null));
            menu.add(0, 2, 0, "关闭").setIcon(Drawable.createFromStream(getAssets().open("mobisage/close.png"), null));
        } catch (IOException e) {
        }
        return super.onCreateOptionsMenu(menu);
    }

    public final boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                this.a.goBack();
                return true;
            case 1:
                this.a.goForward();
                return true;
            case 2:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
