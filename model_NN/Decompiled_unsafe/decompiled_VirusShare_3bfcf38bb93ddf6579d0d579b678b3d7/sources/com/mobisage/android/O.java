package com.mobisage.android;

import android.os.Handler;
import android.os.Message;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

abstract class O {
    protected Handler a;
    protected a b = new a(this, (byte) 0);
    protected int c;
    protected LinkedList<C> d = new LinkedList<>();
    protected ConcurrentHashMap<UUID, C0002b> e = new ConcurrentHashMap<>();
    protected ConcurrentHashMap<UUID, UUID> f = new ConcurrentHashMap<>();

    protected O(Handler handler) {
        this.a = handler;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        this.a = null;
        this.d.clear();
        this.e.clear();
        this.f.clear();
    }

    /* access modifiers changed from: protected */
    public final boolean b(C0002b bVar) {
        Iterator<C> it = this.d.iterator();
        while (it.hasNext()) {
            if (it.next().a()) {
                return true;
            }
        }
        return false;
    }

    public void a(Message message) {
    }

    public final void c(C0002b bVar) {
        if (this.e.containsKey(bVar.b)) {
            this.e.remove(bVar.b);
            while (bVar.f.size() != 0) {
                MobiSageMessage poll = bVar.f.poll();
                this.f.remove(poll.c);
                H.a().b(poll);
            }
            while (bVar.e.size() != 0) {
                c(bVar.e.poll());
            }
            if (bVar.g != null) {
                bVar.g.b(bVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(C0002b bVar) {
        if (this.e.containsKey(bVar.a)) {
            C0002b bVar2 = this.e.get(bVar.a);
            bVar2.e.remove(bVar);
            if (bVar2.a()) {
                this.e.remove(bVar2.b);
                if (bVar2.g != null) {
                    bVar2.g.a(bVar2);
                }
            }
        }
    }

    public final boolean d(C0002b bVar) {
        if (this.e.containsKey(bVar.b)) {
            return true;
        }
        return false;
    }

    public final void e(C0002b bVar) {
        this.e.remove(bVar.b);
        while (bVar.f.size() != 0) {
            MobiSageMessage poll = bVar.f.poll();
            H.a().b(poll);
            this.f.remove(poll.c);
        }
        while (bVar.e.size() != 0) {
            C0005e.a().a(1007, bVar.e.poll());
        }
    }

    class a implements C0001a {
        private a() {
        }

        /* synthetic */ a(O o, byte b) {
            this();
        }

        public final void a(C0002b bVar) {
            O.this.a(bVar);
        }

        public final void b(C0002b bVar) {
            O.this.c(bVar);
        }
    }
}
