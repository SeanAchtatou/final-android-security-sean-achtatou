package com.mobisage.android;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mobisage.android.SNSSSLSocketFactory;

/* renamed from: com.mobisage.android.m  reason: case insensitive filesystem */
final class C0013m extends WebViewClient {
    public SNSSSLSocketFactory.a a;

    C0013m() {
    }

    public final void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (this.a != null) {
            this.a.a();
        }
    }

    public final void onLoadResource(WebView view, String url) {
        super.onLoadResource(view, url);
    }
}
