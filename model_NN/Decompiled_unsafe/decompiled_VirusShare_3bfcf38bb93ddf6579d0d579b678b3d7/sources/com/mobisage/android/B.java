package com.mobisage.android;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

final class B extends P {
    B() {
    }

    public final Animation b(int i) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(1000);
        return alphaAnimation;
    }

    public final Animation a(int i) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(1000);
        return alphaAnimation;
    }
}
