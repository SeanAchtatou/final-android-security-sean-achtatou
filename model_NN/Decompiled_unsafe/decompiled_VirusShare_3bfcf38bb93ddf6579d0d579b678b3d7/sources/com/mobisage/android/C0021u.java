package com.mobisage.android;

import MobWin.cnst.PROTOCOL_ENCODING;
import android.os.Handler;
import android.os.Message;
import java.util.UUID;
import org.apache.http.util.EncodingUtils;

/* renamed from: com.mobisage.android.u  reason: case insensitive filesystem */
final class C0021u extends O {
    private a g = new a(this, (byte) 0);

    protected C0021u(Handler handler) {
        super(handler);
        this.c = 3000;
    }

    public final void a(Message message) {
        if (message.obj instanceof C0002b) {
            C0002b bVar = (C0002b) message.obj;
            this.e.put(bVar.b, bVar);
            C0022v vVar = new C0022v();
            vVar.callback = this.g;
            if (bVar.c.containsKey("key")) {
                vVar.params.putString("key", bVar.c.getString("key"));
            }
            bVar.f.add(vVar);
            this.f.put(vVar.c, bVar.b);
            H.a().a(vVar);
        } else if (message.obj instanceof C0022v) {
            C0022v vVar2 = (C0022v) message.obj;
            if (this.f.containsKey(vVar2.c)) {
                UUID uuid = (UUID) this.f.get(vVar2.c);
                this.f.remove(vVar2.c);
                if (this.e.containsKey(uuid)) {
                    C0002b bVar2 = (C0002b) this.e.get(uuid);
                    bVar2.f.remove(vVar2);
                    int i = vVar2.result.getInt("StatusCode");
                    if (i == 200 || i == 302) {
                        String string = EncodingUtils.getString(vVar2.result.getByteArray("ResponseBody"), PROTOCOL_ENCODING.value);
                        if (string == null || string.length() == 0) {
                            c(bVar2);
                            return;
                        }
                        bVar2.d.putString("ResponseBody", string);
                        if (bVar2.a()) {
                            this.e.remove(bVar2.b);
                            if (bVar2.g != null) {
                                bVar2.g.a(bVar2);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    c(bVar2);
                }
            }
        }
    }

    /* renamed from: com.mobisage.android.u$a */
    class a implements IMobiSageMessageCallback {
        private a() {
        }

        /* synthetic */ a(C0021u uVar, byte b) {
            this();
        }

        public final void onMobiSageMessageFinish(MobiSageMessage msg) {
            Message obtainMessage = C0021u.this.a.obtainMessage(C0021u.this.c);
            obtainMessage.obj = msg;
            obtainMessage.sendToTarget();
        }
    }
}
