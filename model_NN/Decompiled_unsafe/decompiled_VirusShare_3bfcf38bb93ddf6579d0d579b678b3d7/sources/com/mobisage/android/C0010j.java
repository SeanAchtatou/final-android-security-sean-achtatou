package com.mobisage.android;

import android.os.Handler;
import android.os.Message;
import com.taobao.munion.ads.clientSDK.TaoAdsListener;
import java.util.UUID;

/* renamed from: com.mobisage.android.j  reason: case insensitive filesystem */
final class C0010j extends O {
    private a g = new a(this, (byte) 0);

    C0010j(Handler handler) {
        super(handler);
        this.c = TaoAdsListener.EVENT_TYPE_HOSTS_MODIFIED;
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        super.finalize();
    }

    public final void a(Message message) {
        if (message.obj instanceof C0002b) {
            C0002b bVar = (C0002b) message.obj;
            this.e.put(bVar.b, bVar);
            MobiSageResMessage mobiSageResMessage = new MobiSageResMessage();
            mobiSageResMessage.callback = this.g;
            mobiSageResMessage.sourceURL = bVar.c.getString("SourceURL");
            mobiSageResMessage.tempURL = bVar.c.getString("TempURL");
            mobiSageResMessage.targetURL = bVar.c.getString("TargetURL");
            bVar.f.add(mobiSageResMessage);
            this.f.put(mobiSageResMessage.c, bVar.b);
            H.a().a(mobiSageResMessage);
        } else if (message.obj instanceof MobiSageResMessage) {
            MobiSageResMessage mobiSageResMessage2 = (MobiSageResMessage) message.obj;
            if (this.f.containsKey(mobiSageResMessage2.c)) {
                UUID uuid = (UUID) this.f.get(mobiSageResMessage2.c);
                this.f.remove(mobiSageResMessage2.c);
                if (this.e.containsKey(uuid)) {
                    C0002b bVar2 = (C0002b) this.e.get(uuid);
                    bVar2.f.remove(mobiSageResMessage2);
                    if (mobiSageResMessage2.result.containsKey("ErrorText") || mobiSageResMessage2.result.getInt("StatusCode") >= 400) {
                        c(bVar2);
                    } else if (bVar2.a()) {
                        this.e.remove(bVar2.b);
                        if (bVar2.g != null) {
                            bVar2.g.a(bVar2);
                        }
                    }
                }
            }
        }
    }

    /* renamed from: com.mobisage.android.j$a */
    class a implements IMobiSageMessageCallback {
        private a() {
        }

        /* synthetic */ a(C0010j jVar, byte b) {
            this();
        }

        public final void onMobiSageMessageFinish(MobiSageMessage msg) {
            Message obtainMessage = C0010j.this.a.obtainMessage(TaoAdsListener.EVENT_TYPE_HOSTS_MODIFIED);
            obtainMessage.obj = msg;
            obtainMessage.sendToTarget();
        }
    }
}
