package com.mobisage.android;

public final class MobiSageAnimeType {
    public static final int Anime_Base_Type = 65;
    public static final int Anime_Eyes = 69;
    public static final int Anime_Fade = 68;
    public static final int Anime_LeftToRight = 67;
    public static final int Anime_Random = 1;
    public static final int Anime_Rotation = 65;
    public static final int Anime_TopToBottom = 66;
    public static final int Anime_Type_Count = 5;
}
