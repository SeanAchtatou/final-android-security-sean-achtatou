package com.mobisage.android;

import java.io.File;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

final class H {
    private static H a = new H();
    private ConcurrentHashMap<Integer, ConcurrentLinkedQueue<MobiSageMessage>> b = new ConcurrentHashMap<>();
    private ConcurrentHashMap<UUID, N> c = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, K> d;

    public static H a() {
        return a;
    }

    private H() {
        this.b.put(1, new ConcurrentLinkedQueue());
        this.b.put(2, new ConcurrentLinkedQueue());
        this.b.put(3, new ConcurrentLinkedQueue());
        this.b.put(0, new ConcurrentLinkedQueue());
        this.d = new ConcurrentHashMap<>();
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        super.finalize();
        this.d.clear();
        this.b.clear();
        this.c.clear();
    }

    public final void a(MobiSageMessage mobiSageMessage) {
        if (this.b.containsKey(Integer.valueOf(mobiSageMessage.b))) {
            if (mobiSageMessage.b == 2) {
                MobiSageResMessage mobiSageResMessage = (MobiSageResMessage) mobiSageMessage;
                File file = new File(mobiSageResMessage.targetURL);
                if (file.exists()) {
                    mobiSageResMessage.result.putInt("StatusCode", 200);
                    file.setLastModified(System.currentTimeMillis());
                    if (mobiSageResMessage.callback != null) {
                        mobiSageResMessage.callback.onMobiSageMessageFinish(mobiSageResMessage);
                    }
                } else if (this.d.containsKey(mobiSageResMessage.sourceURL)) {
                    this.d.get(mobiSageResMessage.sourceURL).a.add(mobiSageResMessage);
                } else {
                    K k = new K();
                    k.sourceURL = mobiSageResMessage.sourceURL;
                    k.tempURL = mobiSageResMessage.tempURL;
                    k.targetURL = mobiSageResMessage.targetURL;
                    k.a.add(mobiSageResMessage);
                    this.b.get(Integer.valueOf(mobiSageResMessage.b)).add(k);
                    this.d.put(mobiSageResMessage.sourceURL, k);
                }
            } else {
                this.b.get(Integer.valueOf(mobiSageMessage.b)).add(mobiSageMessage);
            }
            b();
        }
    }

    public final void b(MobiSageMessage mobiSageMessage) {
        if (this.b.containsKey(Integer.valueOf(mobiSageMessage.b))) {
            if (mobiSageMessage.b == 2) {
                MobiSageResMessage mobiSageResMessage = (MobiSageResMessage) mobiSageMessage;
                if (this.d.containsKey(mobiSageResMessage.sourceURL)) {
                    K k = this.d.get(mobiSageResMessage.sourceURL);
                    k.a.remove(mobiSageResMessage);
                    if (k.a.size() == 0) {
                        this.d.remove(k);
                        ConcurrentLinkedQueue concurrentLinkedQueue = this.b.get(Integer.valueOf(mobiSageResMessage.b));
                        if (concurrentLinkedQueue.contains(k)) {
                            concurrentLinkedQueue.remove(k);
                        } else {
                            this.c.get(k.c).a();
                            this.c.remove(k.c);
                        }
                    }
                }
            } else {
                ConcurrentLinkedQueue concurrentLinkedQueue2 = this.b.get(Integer.valueOf(mobiSageMessage.b));
                if (concurrentLinkedQueue2.contains(mobiSageMessage)) {
                    concurrentLinkedQueue2.remove(mobiSageMessage);
                } else if (this.c.containsKey(mobiSageMessage.c)) {
                    this.c.get(mobiSageMessage.c).a();
                    this.c.remove(mobiSageMessage.c);
                }
            }
            b();
        }
    }

    private void b() {
        boolean z;
        if (this.c.size() < 16) {
            ConcurrentLinkedQueue concurrentLinkedQueue = this.b.get(1);
            while (true) {
                if (concurrentLinkedQueue.size() == 0) {
                    z = true;
                    break;
                }
                MobiSageMessage mobiSageMessage = (MobiSageMessage) concurrentLinkedQueue.poll();
                if (mobiSageMessage != null) {
                    N n = (N) mobiSageMessage.createMessageRunnable();
                    this.c.put(mobiSageMessage.c, n);
                    new Thread(n).start();
                    if (this.c.size() >= 16) {
                        z = false;
                        break;
                    }
                }
            }
        } else {
            z = false;
        }
        if (z && c() && !d()) {
        }
    }

    private boolean c() {
        if (this.c.size() >= 12) {
            return false;
        }
        ConcurrentLinkedQueue concurrentLinkedQueue = this.b.get(2);
        while (concurrentLinkedQueue.size() != 0) {
            MobiSageMessage mobiSageMessage = (MobiSageMessage) concurrentLinkedQueue.poll();
            if (mobiSageMessage != null) {
                N n = (N) mobiSageMessage.createMessageRunnable();
                this.c.put(mobiSageMessage.c, n);
                new Thread(n).start();
                if (this.c.size() >= 12) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean d() {
        if (this.c.size() >= 12) {
            return false;
        }
        ConcurrentLinkedQueue concurrentLinkedQueue = this.b.get(3);
        while (concurrentLinkedQueue.size() != 0) {
            MobiSageMessage mobiSageMessage = (MobiSageMessage) concurrentLinkedQueue.poll();
            if (mobiSageMessage != null) {
                N n = (N) mobiSageMessage.createMessageRunnable();
                this.c.put(mobiSageMessage.c, n);
                new Thread(n).start();
                if (this.c.size() >= 12) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void c(MobiSageMessage mobiSageMessage) {
        if (mobiSageMessage.b == 2) {
            MobiSageResMessage mobiSageResMessage = (MobiSageResMessage) mobiSageMessage;
            if (this.d.containsKey(mobiSageResMessage.sourceURL)) {
                this.d.remove(mobiSageResMessage.sourceURL);
            }
        }
        this.c.remove(mobiSageMessage.c);
        b();
    }
}
