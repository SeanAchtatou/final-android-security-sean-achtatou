package com.mobisage.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;
import java.io.File;
import java.net.URLDecoder;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class F {
    String a;
    String b;
    String c;
    String d;
    String e;
    /* access modifiers changed from: private */
    public Context f;

    F(Context context) {
        this.f = context;
    }

    public final void destoryJavascriptAgent() {
        this.f = null;
    }

    public final void impressiontrack(String trackStr) {
        try {
            JSONArray jSONArray = new JSONArray(URLDecoder.decode(trackStr));
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                C0002b bVar = new C0002b();
                bVar.c.putString("AdGroupID", jSONObject.getString("adgroupid"));
                bVar.c.putString("AdID", jSONObject.getString("adid"));
                if (jSONObject.has("token")) {
                    bVar.c.putString("Token", jSONObject.getString("token"));
                } else {
                    bVar.c.putString("Token", this.a);
                }
                if (this.b != null) {
                    bVar.c.putString("CustomData", this.b);
                }
                bVar.c.putString("PublisherID", this.c);
                Y.a().a(u.n, bVar);
            }
        } catch (JSONException e2) {
        }
    }

    public final void clickAd(String trackStr) {
        final HashMap<String, String> parserURIQuery = MobiSageURIUtility.parserURIQuery(trackStr);
        parserURIQuery.put("eventtype", String.valueOf(2));
        switch (Integer.valueOf(parserURIQuery.get("action")).intValue()) {
            case 1:
                c(parserURIQuery);
                return;
            case 2:
                b(parserURIQuery);
                return;
            case 3:
                C0002b bVar = new C0002b();
                bVar.c.putString("EventType", parserURIQuery.get("eventtype"));
                if (parserURIQuery.containsKey("token")) {
                    bVar.c.putString("Token", parserURIQuery.get("token"));
                } else {
                    bVar.c.putString("Token", this.a);
                }
                if (parserURIQuery.containsKey("adid")) {
                    bVar.c.putString("AdID", parserURIQuery.get("adid"));
                } else {
                    bVar.c.putString("AdID", this.d);
                }
                if (parserURIQuery.containsKey("adgroupid")) {
                    bVar.c.putString("AdGroupID", parserURIQuery.get("adgroupid"));
                } else {
                    bVar.c.putString("AdGroupID", this.e);
                }
                if (parserURIQuery.containsKey("tag")) {
                    bVar.c.putString("Tag", parserURIQuery.get("tag"));
                }
                if (this.b != null) {
                    bVar.c.putString("CustomData", this.b);
                }
                bVar.c.putString("PublisherID", this.c);
                Y.a().a(u.o, bVar);
                final AlertDialog create = new AlertDialog.Builder(this.f).create();
                create.setMessage(parserURIQuery.get("tip"));
                create.setCancelable(true);
                create.setButton(-1, "OK", new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        C0002b bVar = new C0002b();
                        bVar.c.putString("EventType", String.valueOf(5));
                        if (parserURIQuery.containsKey("token")) {
                            bVar.c.putString("Token", (String) parserURIQuery.get("token"));
                        } else {
                            bVar.c.putString("Token", F.this.a);
                        }
                        if (parserURIQuery.containsKey("adid")) {
                            bVar.c.putString("AdID", (String) parserURIQuery.get("adid"));
                        } else {
                            bVar.c.putString("AdID", F.this.d);
                        }
                        if (parserURIQuery.containsKey("adgroupid")) {
                            bVar.c.putString("AdGroupID", (String) parserURIQuery.get("adgroupid"));
                        } else {
                            bVar.c.putString("AdGroupID", F.this.e);
                        }
                        if (parserURIQuery.containsKey("tag")) {
                            bVar.c.putString("Tag", (String) parserURIQuery.get("tag"));
                        }
                        if (F.this.b != null) {
                            bVar.c.putString("CustomData", F.this.b);
                        }
                        bVar.c.putString("PublisherID", F.this.c);
                        Y.a().a(u.o, bVar);
                        F.this.f.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(((String) parserURIQuery.get("lpg")).replace("tel://", "tel:"))));
                    }
                });
                create.setButton(-2, "Cancel", new DialogInterface.OnClickListener(this) {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        create.cancel();
                    }
                });
                create.show();
                return;
            case 4:
                a(parserURIQuery);
                return;
            default:
                return;
        }
    }

    public final void onWebClickAd(String trackStr) {
        HashMap<String, String> parserURIQuery = MobiSageURIUtility.parserURIQuery(trackStr);
        parserURIQuery.put("eventtype", String.valueOf(8));
        switch (Integer.valueOf(parserURIQuery.get("action")).intValue()) {
            case 1:
                c(parserURIQuery);
                return;
            case 2:
                b(parserURIQuery);
                return;
            case 3:
                C0002b bVar = new C0002b();
                bVar.c.putString("EventType", parserURIQuery.get("eventtype"));
                if (parserURIQuery.containsKey("token")) {
                    bVar.c.putString("Token", parserURIQuery.get("token"));
                } else {
                    bVar.c.putString("Token", this.a);
                }
                if (parserURIQuery.containsKey("adid")) {
                    bVar.c.putString("AdID", parserURIQuery.get("adid"));
                } else {
                    bVar.c.putString("AdID", this.d);
                }
                if (parserURIQuery.containsKey("adgroupid")) {
                    bVar.c.putString("AdGroupID", parserURIQuery.get("adgroupid"));
                } else {
                    bVar.c.putString("AdGroupID", this.e);
                }
                if (parserURIQuery.containsKey("tag")) {
                    bVar.c.putString("Tag", parserURIQuery.get("tag"));
                }
                if (this.b != null) {
                    bVar.c.putString("CustomData", this.b);
                }
                bVar.c.putString("PublisherID", this.c);
                Y.a().a(u.o, bVar);
                C0002b bVar2 = new C0002b();
                bVar2.c.putString("EventType", String.valueOf(5));
                if (parserURIQuery.containsKey("token")) {
                    bVar2.c.putString("Token", parserURIQuery.get("token"));
                } else {
                    bVar2.c.putString("Token", this.a);
                }
                if (parserURIQuery.containsKey("adid")) {
                    bVar2.c.putString("AdID", parserURIQuery.get("adid"));
                } else {
                    bVar2.c.putString("AdID", this.d);
                }
                if (parserURIQuery.containsKey("adgroupid")) {
                    bVar2.c.putString("AdGroupID", parserURIQuery.get("adgroupid"));
                } else {
                    bVar2.c.putString("AdGroupID", this.e);
                }
                if (parserURIQuery.containsKey("tag")) {
                    bVar2.c.putString("Tag", parserURIQuery.get("tag"));
                }
                if (this.b != null) {
                    bVar2.c.putString("CustomData", this.b);
                }
                bVar2.c.putString("PublisherID", this.c);
                Y.a().a(u.o, bVar2);
                this.f.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(parserURIQuery.get("lpg").replace("tel://", "tel:"))));
                return;
            case 4:
                a(parserURIQuery);
                return;
            default:
                return;
        }
    }

    private void a(HashMap<String, String> hashMap) {
        C0002b bVar = new C0002b();
        bVar.c.putString("EventType", hashMap.get("eventtype"));
        if (hashMap.containsKey("token")) {
            bVar.c.putString("Token", hashMap.get("token"));
        } else {
            bVar.c.putString("Token", this.a);
        }
        if (hashMap.containsKey("adid")) {
            bVar.c.putString("AdID", hashMap.get("adid"));
        } else {
            bVar.c.putString("AdID", this.d);
        }
        if (hashMap.containsKey("adgroupid")) {
            bVar.c.putString("AdGroupID", hashMap.get("adgroupid"));
        } else {
            bVar.c.putString("AdGroupID", this.e);
        }
        if (hashMap.containsKey("tag")) {
            bVar.c.putString("Tag", hashMap.get("tag"));
        }
        if (this.b != null) {
            bVar.c.putString("CustomData", this.b);
        }
        bVar.c.putString("PublisherID", this.c);
        Y.a().a(u.o, bVar);
        this.f.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(hashMap.get("lpg"))));
    }

    private void b(HashMap<String, String> hashMap) {
        C0002b bVar = new C0002b();
        bVar.c.putString("EventType", hashMap.get("eventtype"));
        if (hashMap.containsKey("token")) {
            bVar.c.putString("Token", hashMap.get("token"));
        } else {
            bVar.c.putString("Token", this.a);
        }
        if (hashMap.containsKey("adid")) {
            bVar.c.putString("AdID", hashMap.get("adid"));
        } else {
            bVar.c.putString("AdID", this.d);
        }
        if (hashMap.containsKey("adgroupid")) {
            bVar.c.putString("AdGroupID", hashMap.get("adgroupid"));
        } else {
            bVar.c.putString("AdGroupID", this.e);
        }
        if (hashMap.containsKey("tag")) {
            bVar.c.putString("Tag", hashMap.get("tag"));
        }
        if (this.b != null) {
            bVar.c.putString("CustomData", this.b);
        }
        bVar.c.putString("PublisherID", this.c);
        Y.a().a(u.o, bVar);
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (!externalStorageDirectory.exists() || !externalStorageDirectory.canWrite()) {
            Toast.makeText(this.f, "Can't find sdcard!", 5).show();
            return;
        }
        Intent intent = new Intent(this.f, MobiSageApkService.class);
        Bundle bundle = new Bundle();
        bundle.putInt("action", 0);
        bundle.putString("lpg", hashMap.get("lpg"));
        if (hashMap.containsKey("name")) {
            bundle.putString("name", hashMap.get("name"));
        } else {
            bundle.putString("name", new File(hashMap.get("lpg")).getName());
        }
        intent.putExtra("ExtraData", bundle);
        this.f.startService(intent);
    }

    private void c(HashMap<String, String> hashMap) {
        C0002b bVar = new C0002b();
        bVar.c.putString("EventType", hashMap.get("eventtype"));
        if (hashMap.containsKey("token")) {
            bVar.c.putString("Token", hashMap.get("token"));
        } else {
            bVar.c.putString("Token", this.a);
        }
        if (hashMap.containsKey("adid")) {
            bVar.c.putString("AdID", hashMap.get("adid"));
        } else {
            bVar.c.putString("AdID", this.d);
        }
        if (hashMap.containsKey("adgroupid")) {
            bVar.c.putString("AdGroupID", hashMap.get("adgroupid"));
        } else {
            bVar.c.putString("AdGroupID", this.e);
        }
        if (hashMap.containsKey("tag")) {
            bVar.c.putString("Tag", hashMap.get("tag"));
        }
        if (this.b != null) {
            bVar.c.putString("CustomData", this.b);
        }
        bVar.c.putString("PublisherID", this.c);
        Y.a().a(u.o, bVar);
        Intent intent = new Intent(this.f, MobiSageActivity.class);
        intent.putExtra("type", "ad");
        intent.putExtra("lpg", hashMap.get("lpg"));
        intent.putExtra("adid", hashMap.get("adid"));
        intent.putExtra("adgroupid", hashMap.get("adgroupid"));
        intent.putExtra("token", hashMap.get("token"));
        intent.putExtra("pid", this.c);
        if (this.b != null) {
            intent.putExtra("customdata", this.b);
        }
        this.f.startActivity(intent);
    }
}
