package com.mobisage.android;

import android.content.Context;
import android.webkit.WebSettings;
import android.webkit.WebView;

class af extends WebView {
    af(Context context) {
        super(context);
        WebSettings.ZoomDensity zoomDensity;
        getSettings().setRenderPriority(WebSettings.RenderPriority.NORMAL);
        getSettings().setJavaScriptEnabled(true);
        getSettings().setPluginState(WebSettings.PluginState.ON);
        int i = C0025y.c;
        WebSettings.ZoomDensity zoomDensity2 = WebSettings.ZoomDensity.MEDIUM;
        switch (i) {
            case 120:
                zoomDensity = WebSettings.ZoomDensity.CLOSE;
                break;
            case 160:
                zoomDensity = WebSettings.ZoomDensity.MEDIUM;
                break;
            case 240:
                zoomDensity = WebSettings.ZoomDensity.FAR;
                break;
            default:
                zoomDensity = zoomDensity2;
                break;
        }
        getSettings().setDefaultZoom(zoomDensity);
    }
}
