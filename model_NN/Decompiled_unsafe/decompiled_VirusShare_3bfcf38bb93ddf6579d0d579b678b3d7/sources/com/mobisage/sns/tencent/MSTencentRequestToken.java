package com.mobisage.sns.tencent;

import com.mobisage.sns.common.MSOAConsumer;

public class MSTencentRequestToken extends MSTencentWeiboMessage {
    public MSTencentRequestToken(MSOAConsumer consumer) {
        super(null, consumer);
        this.urlPath = "https://open.t.qq.com/cgi-bin/request_token";
        this.httpMethod = "GET";
        this.paramMap.put("oauth_callback", "null");
    }
}
