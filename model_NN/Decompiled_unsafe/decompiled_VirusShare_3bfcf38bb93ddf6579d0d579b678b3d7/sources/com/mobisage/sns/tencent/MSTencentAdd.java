package com.mobisage.sns.tencent;

import com.mobisage.sns.common.MSOAConsumer;
import com.mobisage.sns.common.MSOAToken;

public class MSTencentAdd extends MSTencentWeiboMessage {
    public MSTencentAdd(MSOAToken token, MSOAConsumer consumer) {
        super(token, consumer);
        this.urlPath = "http://open.t.qq.com/api/t/add";
        this.httpMethod = "POST";
        this.paramMap.put("format", "json");
    }
}
