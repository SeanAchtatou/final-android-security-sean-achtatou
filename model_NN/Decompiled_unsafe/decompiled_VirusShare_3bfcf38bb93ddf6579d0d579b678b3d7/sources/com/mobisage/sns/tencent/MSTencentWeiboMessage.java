package com.mobisage.sns.tencent;

import MobWin.cnst.PROTOCOL_ENCODING;
import android.text.format.Time;
import android.util.Base64;
import com.mobisage.android.MobiSageReqMessage;
import com.mobisage.sns.common.MSOAConsumer;
import com.mobisage.sns.common.MSOAToken;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EncodingUtils;

public abstract class MSTencentWeiboMessage extends MobiSageReqMessage {
    protected MSOAConsumer consumer;
    protected String httpMethod;
    protected HashMap<String, String> paramMap = new HashMap<>();
    protected MSOAToken token;
    protected String urlPath;

    public MSTencentWeiboMessage(MSOAToken token2, MSOAConsumer consumer2) {
        this.token = token2;
        this.consumer = consumer2;
        this.paramMap.put("oauth_consumer_key", consumer2.key);
        this.paramMap.put("oauth_signature_method", "HMAC-SHA1");
        this.paramMap.put("oauth_version", "1.0");
        if (token2 != null) {
            this.paramMap.put("oauth_token", token2.key);
            if (token2.pin != null) {
                this.paramMap.put("oauth_verifier", token2.pin);
            }
        }
    }

    public void addParam(String name, String value) {
        this.paramMap.put(name, value);
    }

    public HttpRequestBase createHttpRequest() throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        if (this.httpMethod.equals("GET")) {
            return new HttpGet(this.urlPath + "?" + generateOAuthParams());
        }
        if (!this.httpMethod.equals("POST")) {
            return null;
        }
        HttpPost httpPost = new HttpPost(this.urlPath);
        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        httpPost.setEntity(new StringEntity(generateOAuthParams()));
        return httpPost;
    }

    /* access modifiers changed from: protected */
    public String generateClearText(String timeStamp, String nonce) {
        LinkedList linkedList = new LinkedList();
        linkedList.add("oauth_nonce=" + nonce);
        linkedList.add("oauth_timestamp=" + timeStamp);
        for (String next : this.paramMap.keySet()) {
            linkedList.add(next + "=" + URLEncoder.encode(this.paramMap.get(next)));
        }
        Collections.sort(linkedList);
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < linkedList.size()) {
                if (i2 != 0) {
                    sb.append("&" + ((String) linkedList.get(i2)));
                } else {
                    sb.append((String) linkedList.get(i2));
                }
                i = i2 + 1;
            } else {
                return this.httpMethod + "&" + URLEncoder.encode(this.urlPath) + "&" + URLEncoder.encode(sb.toString());
            }
        }
    }

    /* access modifiers changed from: protected */
    public String generateSignature(String timeStamp, String nonce) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        String str;
        String generateClearText = generateClearText(timeStamp, nonce);
        if (this.token != null) {
            str = URLEncoder.encode(this.consumer.secret) + "&" + URLEncoder.encode(this.token.secret);
        } else {
            str = URLEncoder.encode(this.consumer.secret) + "&";
        }
        Mac instance = Mac.getInstance("HmacSHA1");
        instance.init(new SecretKeySpec(str.toString().getBytes(), "HmacSHA1"));
        byte[] encode = Base64.encode(instance.doFinal(generateClearText.getBytes()), 0);
        return EncodingUtils.getString(encode, 0, encode.length, PROTOCOL_ENCODING.value);
    }

    /* access modifiers changed from: protected */
    public String generateOAuthParams() throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        Time time = new Time();
        time.setToNow();
        String valueOf = String.valueOf(time.toMillis(true) / 1000);
        String replace = UUID.randomUUID().toString().replace("-", "");
        String trim = generateSignature(valueOf, replace).trim();
        StringBuilder sb = new StringBuilder();
        sb.append("oauth_timestamp=");
        sb.append(valueOf);
        sb.append("&");
        sb.append("oauth_nonce=");
        sb.append(replace);
        sb.append("&");
        sb.append("oauth_signature=");
        sb.append(URLEncoder.encode(trim));
        for (String next : this.paramMap.keySet()) {
            sb.append("&");
            sb.append(next);
            sb.append("=");
            sb.append(URLEncoder.encode(this.paramMap.get(next)));
        }
        return sb.toString();
    }
}
