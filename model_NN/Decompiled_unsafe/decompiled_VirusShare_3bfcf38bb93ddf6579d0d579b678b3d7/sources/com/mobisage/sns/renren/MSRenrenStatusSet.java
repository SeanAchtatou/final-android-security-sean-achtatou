package com.mobisage.sns.renren;

public class MSRenrenStatusSet extends MSRenrenMessage {
    public MSRenrenStatusSet(String accessToken, String secretKey) {
        super(accessToken, secretKey);
        this.urlPath = "http://api.renren.com/restserver.do";
        this.httpMethod = "POST";
        this.paramMap.put("access_token", accessToken);
        this.paramMap.put("v", "2.0");
        this.paramMap.put("method", "status.set");
        this.paramMap.put("format", "json");
    }
}
