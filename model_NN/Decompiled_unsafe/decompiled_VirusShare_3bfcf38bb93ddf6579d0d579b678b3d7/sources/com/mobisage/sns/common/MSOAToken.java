package com.mobisage.sns.common;

import com.mobisage.android.MobiSageURIUtility;
import java.util.HashMap;

public class MSOAToken {
    public String key;
    public String pin;
    public String secret;

    public MSOAToken() {
    }

    public MSOAToken(String responseText) {
        HashMap<String, String> parserURIQuery = MobiSageURIUtility.parserURIQuery(responseText);
        if (parserURIQuery.containsKey("oauth_token")) {
            this.key = parserURIQuery.get("oauth_token");
        }
        if (parserURIQuery.containsKey("oauth_token_secret")) {
            this.secret = parserURIQuery.get("oauth_token_secret");
        }
        if (parserURIQuery.containsKey("oauth_verifier")) {
            this.pin = parserURIQuery.get("oauth_verifier");
        }
    }
}
