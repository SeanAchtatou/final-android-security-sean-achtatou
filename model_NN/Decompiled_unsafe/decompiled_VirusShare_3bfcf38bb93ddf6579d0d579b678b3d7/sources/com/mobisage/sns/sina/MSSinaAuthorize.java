package com.mobisage.sns.sina;

import java.net.URLEncoder;

public class MSSinaAuthorize extends MSSinaWeiboMessage {
    public MSSinaAuthorize(String appKey) {
        super(appKey);
        this.urlPath = "https://api.weibo.com/oauth2/authorize";
        this.httpMethod = "GET";
        this.paramMap.remove("source");
        this.paramMap.put("client_id", appKey);
        this.paramMap.put("redirect_uri", "");
        this.paramMap.put("display", "mobile");
        this.paramMap.put("response_type", "code");
    }

    public String generateURL() {
        StringBuilder sb = new StringBuilder();
        for (String str : this.paramMap.keySet()) {
            if (sb.length() == 0) {
                sb.append("?");
            } else {
                sb.append("&");
            }
            sb.append(str + "=" + URLEncoder.encode((String) this.paramMap.get(str)));
        }
        return this.urlPath + sb.toString();
    }
}
