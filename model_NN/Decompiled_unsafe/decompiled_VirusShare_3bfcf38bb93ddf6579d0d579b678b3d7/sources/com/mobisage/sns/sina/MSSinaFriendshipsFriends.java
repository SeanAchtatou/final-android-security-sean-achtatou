package com.mobisage.sns.sina;

public class MSSinaFriendshipsFriends extends MSSinaWeiboMessage {
    public MSSinaFriendshipsFriends(String appKey, String accessToken) {
        super(appKey, accessToken);
        this.urlPath = "https://api.weibo.com/2/friendships/friends.json";
        this.httpMethod = "GET";
    }
}
