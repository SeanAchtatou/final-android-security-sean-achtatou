package com.mobisage.sns.sina;

public class MSSinaAccessToken extends MSSinaWeiboMessage {
    public MSSinaAccessToken(String appKey, String appSecret) {
        super(appKey);
        this.urlPath = "https://api.weibo.com/oauth2/access_token";
        this.httpMethod = "POST";
        this.paramMap.remove("source");
        this.paramMap.put("client_id", appKey);
        this.paramMap.put("redirect_uri", "");
        this.paramMap.put("client_secret", appSecret);
        this.paramMap.put("grant_type", "authorization_code");
    }
}
