package com.baidu;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

class App implements bl {
    private static final List<String> a = new ArrayList();
    private Map<String, String> b = new HashMap();
    private String c;
    private String d;
    private String e;
    private Bitmap f;
    private List<Snag> g = new ArrayList();

    public class Snag {
        private final Bitmap a;
        private Bitmap b;
        private final String c;

        public Snag(Bitmap bitmap, String str) {
            this.a = bitmap;
            this.c = str;
        }

        public Bitmap getFullBmp() {
            return this.b;
        }

        public String getFullUrl() {
            return this.c;
        }

        public Bitmap getThumbBmp() {
            return this.a;
        }

        public void setFullBmp(Bitmap bitmap) {
            this.b = bitmap;
        }
    }

    static {
        a.add("durl");
        a.add("s");
        a.add("n");
        a.add("d");
        a.add("dn");
        a.add("v");
        a.add("t");
        a.add("o");
        a.add("ov");
        a.add("c");
        a.add("clklog");
        a.add("dev");
        a.add("time");
    }

    private App() {
    }

    public static App a(Context context, String str, JSONObject jSONObject) {
        App app = new App();
        for (String next : a) {
            app.b.put(next, jSONObject.optString(next));
        }
        app.b.put("clklog", str);
        app.f = w.a(context, new URL(jSONObject.optString("w_picurl")));
        JSONArray jSONArray = new JSONArray(jSONObject.optString("a_picurl"));
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            Bitmap bitmap = app.f;
            try {
                bitmap = w.a(context, new URL(jSONObject2.getString("s")));
            } catch (Exception e2) {
                bk.a(e2);
            }
            app.g.add(new Snag(bitmap, jSONObject2.getString("b")));
        }
        return app;
    }

    public static App a(String str, String str2, String str3, String str4, long j, Bitmap bitmap) {
        App app = new App();
        for (String put : a) {
            app.b.put(put, "");
        }
        app.b.put("clklog", str);
        app.b.put("durl", str2);
        app.b.put("n", str3);
        app.b.put("d", str4);
        app.b.put("s", j + "");
        app.f = bitmap;
        return app;
    }

    public List<Snag> a() {
        return this.g;
    }

    public void a(String str) {
        this.e = str;
    }

    public String b() {
        return this.b.get("clklog");
    }

    public String c() {
        return this.b.get("durl");
    }

    public int d() {
        try {
            return Integer.parseInt(this.b.get("dn"));
        } catch (Exception e2) {
            return 0;
        }
    }

    public Bitmap e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        return (obj instanceof App) && ((App) obj).c().equals(c());
    }

    public String f() {
        if (this.c == null) {
            try {
                this.c = Uri.parse(this.b.get("durl")).getLastPathSegment();
            } catch (Exception e2) {
                bk.a(e2);
                this.c = "";
            }
        }
        return this.c;
    }

    public String g() {
        return this.b.get("dev");
    }

    public String h() {
        return this.b.get("time");
    }

    public String i() {
        if (this.d == null) {
            try {
                long parseLong = Long.parseLong(this.b.get("s"));
                if (parseLong < 1024) {
                    this.d = parseLong + "B";
                } else if (parseLong < 1048576) {
                    this.d = (parseLong / 1024) + "KB";
                } else {
                    this.d = (parseLong / 1048576) + "MB";
                }
            } catch (Exception e2) {
                this.d = "";
                bk.a(e2);
            }
        }
        return this.d;
    }

    public String j() {
        return this.b.get("n");
    }

    public String k() {
        return this.b.get("d");
    }

    public String l() {
        return this.b.get("v");
    }

    public String m() {
        return this.b.get("t");
    }

    public String n() {
        return this.e;
    }

    public String toString() {
        return this.b.toString();
    }
}
