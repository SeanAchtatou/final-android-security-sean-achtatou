package com.baidu;

import android.content.Context;
import android.graphics.Bitmap;
import com.tencent.lbsapi.core.e;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONObject;

public class Ad {
    private String a;
    private String b;
    private String c;
    private t d;
    /* access modifiers changed from: private */
    public Bitmap e;
    private Bitmap f;
    private Bitmap g;
    private Bitmap h;
    /* access modifiers changed from: private */
    public String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private bj n;
    private ClickType o = ClickType.BROWSE;
    private int p;
    private int q;
    private int r;
    private int s;
    private long t;

    private Ad() {
    }

    static Ad a(Context context, String str, String str2, String str3, String str4, String str5, String str6, int i2, int i3, int i4, int i5, String str7, String str8, String str9, long j2) {
        Ad a2 = a(context, str, str2, str3, str4, str5, str6, str7, str8, str9, j2);
        if (a2 != null) {
            a2.p = i2;
            a2.r = i4;
            a2.q = i3;
            a2.s = i5;
        }
        return a2;
    }

    private static Ad a(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, long j2) {
        if (str4.equals("")) {
            throw new IllegalArgumentException("id");
        }
        Ad ad = new Ad();
        if (str7.trim().equals("")) {
            ad.n = bj.NONE;
            ad.i = "";
            ad.e = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
        } else {
            int lastIndexOf = str7.toLowerCase().lastIndexOf(46);
            String substring = lastIndexOf >= 0 ? str7.toLowerCase().substring(lastIndexOf) : "";
            if (substring.equals(".gif")) {
                ad.n = bj.GIF;
            } else if (substring.equals(".swf") || substring.equals(".flv")) {
                ad.n = bj.FLASH;
            } else {
                ad.n = bj.STATIC;
            }
            ad.i = w.e(str7);
            ad.e = w.d(false, context, ad.i);
            if (ad.e == null) {
                bk.b("Ad.created", "Empty image, reloaded");
                new a(context, str7, ad).start();
                return null;
            }
        }
        ad.a = str4;
        ad.d = t.a(str5);
        ad.m = str6;
        ad.l = str8;
        ad.c = str.replace("{empty}", "").trim();
        ad.b = str2.replace("$$", " ").trim();
        ad.j = str3;
        if (ad.d != t.APP) {
            try {
                ad.j = String.format(c.a().g() + "/ad.html?url=%s&sn=%s&clk=%s", URLEncoder.encode(str3, e.e), r.f(context), "");
            } catch (UnsupportedEncodingException e2) {
                bk.a("Ad.created", e2);
            }
        }
        ad.k = str9;
        ad.t = j2;
        if (str3.toLowerCase().endsWith(".apk")) {
            ad.o = ClickType.DOWNLOAD;
        }
        if (!ad.m.equals("")) {
            ad.o = ClickType.PHONE;
        }
        if (ad.d == t.APP) {
            ad.o = ClickType.DOWNLOAD;
        }
        c a2 = c.a();
        switch (b.a[ad.o.ordinal()]) {
            case 1:
                ad.f = t.IMAGE == ad.d ? a2.i() : a2.h();
                break;
            case 2:
                ad.f = t.IMAGE == ad.d ? a2.k() : a2.j();
                break;
            case 3:
                ad.f = t.IMAGE == ad.d ? a2.o() : a2.n();
                break;
        }
        ad.g = c.a().m();
        ad.h = c.a().l();
        return ad;
    }

    static Ad a(Context context, JSONObject jSONObject, String str) {
        int i2;
        int i3;
        int i4;
        String string = jSONObject.getString("tit");
        String string2 = jSONObject.getString("desc");
        String string3 = jSONObject.getString("curl");
        String string4 = jSONObject.getString("surl");
        String string5 = jSONObject.getString("type");
        String string6 = jSONObject.getString("phone");
        long j2 = jSONObject.getLong("ad_charge");
        int i5 = jSONObject.getInt("styt");
        if (i5 == 1) {
            int i6 = jSONObject.getInt("titc");
            int i7 = jSONObject.getInt("bgc");
            i2 = jSONObject.getInt("trsp");
            i3 = i7;
            i4 = i6;
        } else {
            i2 = 0;
            i3 = 0;
            i4 = 0;
        }
        return a(context, string, string2, string3, str, string5, string6, i5, i4, i3, i2, jSONObject.getString("w_picurl"), jSONObject.getString("clklogurl"), string4, j2);
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public t b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public Bitmap c() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public Bitmap d() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public Bitmap e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Ad) {
            return m().equals(((Ad) obj).m());
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public Bitmap f() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public Bitmap g() {
        return this.h;
    }

    public ClickType getClickType() {
        return this.o;
    }

    public String getClickURL() {
        return this.j;
    }

    public String getDescription() {
        return this.b;
    }

    public String getPhone() {
        return this.m;
    }

    public String getSURL() {
        return this.k;
    }

    public String getTitle() {
        return this.c;
    }

    public AdType getType() {
        return this.d.c();
    }

    /* access modifiers changed from: package-private */
    public String h() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public int j() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public int k() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public int l() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public String m() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public bj n() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public long o() {
        return this.t;
    }

    public String toString() {
        return String.format("%s; %s; %s; %s; %s", m(), b(), getTitle(), getDescription(), getPhone());
    }
}
