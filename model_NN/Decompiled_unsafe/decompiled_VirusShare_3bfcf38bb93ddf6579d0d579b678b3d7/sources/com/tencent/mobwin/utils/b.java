package com.tencent.mobwin.utils;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.DisplayMetrics;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class b {
    public static final int a = 320;
    public static final String b = ".ad";
    private static final char[] c = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static int a(int i, Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (context instanceof Activity) {
            ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            if (displayMetrics.densityDpi == 120) {
                return (i * 120) / a;
            }
            if (displayMetrics.densityDpi == 160) {
                return (i * 160) / a;
            }
            if (displayMetrics.densityDpi == 240) {
                return (i * 240) / a;
            }
            if (displayMetrics.densityDpi == 320) {
                return i;
            }
        }
        return (displayMetrics.densityDpi * i) / a;
    }

    public static int a(Context context) {
        if (!(context instanceof Activity)) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int a(String str, int i) {
        if (str == null || str.trim().length() == 0) {
            return i;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return i;
        }
    }

    public static long a(String str, long j) {
        if (str == null || str.trim().length() == 0) {
            return j;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e) {
            return j;
        }
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            return a(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (int i = 0; i < bArr.length; i++) {
            sb.append(c[(bArr[i] & 240) >>> 4]);
            sb.append(c[bArr[i] & 15]);
        }
        return sb.toString();
    }

    public static short a(String str, short s) {
        if (str == null || str.trim().length() == 0) {
            return s;
        }
        try {
            return Short.parseShort(str);
        } catch (NumberFormatException e) {
            return s;
        }
    }

    public static int b(Context context) {
        if (!(context instanceof Activity)) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static String b(String str) {
        String str2;
        Uri parse = Uri.parse(str);
        if (parse == null) {
            str2 = str;
        } else {
            str2 = parse.getQuery() != null ? String.valueOf(parse.getPath()) + "_" + parse.getQuery() : parse.getPath();
        }
        return str2 != null ? str2.replace("/", "_").replace("\\", "_").replace("*", "_").replace("?", "_").replace("=", "_").replace(".", "_") : str2;
    }

    public static int c(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (!(context instanceof Activity)) {
            return 160;
        }
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.densityDpi;
    }

    public static String c(String str) {
        Uri parse = Uri.parse(str);
        return parse != null ? parse.getPath() : str;
    }

    public static int d(Context context) {
        return Math.min(a(context), b(context));
    }

    public static int e(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (!(context instanceof Activity)) {
            return 50;
        }
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        if (displayMetrics.densityDpi == 120) {
            return 38;
        }
        if (displayMetrics.densityDpi == 160) {
            return 50;
        }
        if (displayMetrics.densityDpi == 240) {
            return 75;
        }
        if (displayMetrics.densityDpi == 320) {
            return 100;
        }
        return (int) (displayMetrics.density * 50.0f);
    }
}
