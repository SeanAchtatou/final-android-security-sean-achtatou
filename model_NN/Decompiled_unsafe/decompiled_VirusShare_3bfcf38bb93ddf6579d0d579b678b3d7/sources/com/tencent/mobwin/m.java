package com.tencent.mobwin;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.widget.RelativeLayout;
import com.tencent.mobwin.core.A;
import com.tencent.mobwin.core.o;
import com.tencent.mobwin.utils.b;

public class m extends Handler {
    final /* synthetic */ MobinWINBrowserActivity a;

    public m(MobinWINBrowserActivity mobinWINBrowserActivity) {
        this.a = mobinWINBrowserActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 11:
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(11);
                layoutParams.addRule(10);
                layoutParams.topMargin = b.a(20, this.a);
                layoutParams.rightMargin = b.a(20, this.a);
                this.a.d.addView(this.a.D, layoutParams);
                return;
            case 12:
                this.a.d.removeView(this.a.D);
                return;
            case 17:
                try {
                    String string = message.getData().getString("url");
                    Bitmap bitmap = (Bitmap) message.obj;
                    if (bitmap != null) {
                        bitmap.setDensity(0);
                    }
                    String lastPathSegment = Uri.parse(string).getLastPathSegment();
                    A.a().e().put(lastPathSegment, bitmap);
                    o.a("MobwinBrowser", "download embedbrowserRes ok file:" + lastPathSegment);
                    return;
                } catch (Exception e) {
                    return;
                }
            default:
                return;
        }
    }
}
