package com.tencent.mobwin.core.a;

import android.content.Context;
import android.content.SharedPreferences;

public class b {
    public int a = 0;
    public long b = 0;
    public long c = 0;
    public long d = 0;
    public int e = 0;
    public int f = 0;
    public String g = "";
    public String h = "";
    public String i = "";

    public static b a(Context context) {
        b bVar = new b();
        if (context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences("mobwin", 0);
            bVar.a = sharedPreferences.getInt("ADID", 0);
            bVar.b = sharedPreferences.getLong("ADSTART", 0);
            bVar.c = sharedPreferences.getLong("ADEND", 0);
            bVar.d = sharedPreferences.getLong("ADLAST", 0);
            bVar.f = sharedPreferences.getInt("ADRESULTCODE", 0);
            bVar.e = sharedPreferences.getInt("ADGETMILLSECONDS", 0);
            bVar.g = sharedPreferences.getString("SDKTIMESTAMP", "");
            bVar.h = sharedPreferences.getString("CURRENTSID", "");
            bVar.i = sharedPreferences.getString("PRESID", "");
        }
        return bVar;
    }

    public static void a(Context context, b bVar) {
        if (context != null && bVar.a != 0) {
            SharedPreferences.Editor edit = context.getSharedPreferences("mobwin", 0).edit();
            edit.putInt("ADID", bVar.a);
            edit.putLong("ADSTART", bVar.b);
            edit.putLong("ADEND", bVar.c);
            edit.putLong("ADLAST", bVar.d);
            edit.putInt("ADRESULTCODE", bVar.f);
            edit.putInt("ADGETMILLSECONDS", bVar.e);
            edit.putString("SDKTIMESTAMP", bVar.g);
            edit.putString("CURRENTSID", bVar.h);
            edit.putString("PRESID", bVar.i);
            edit.commit();
        }
    }
}
