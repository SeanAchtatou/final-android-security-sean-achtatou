package com.tencent.mobwin.core.view;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

class e extends Handler {
    final /* synthetic */ AniImageView a;

    e(AniImageView aniImageView) {
        this.a = aniImageView;
    }

    public void handleMessage(Message message) {
        if (this.a.b == null) {
            removeMessages(0);
        } else if (this.a.a == -1) {
            removeMessages(0);
        } else {
            if (this.a.a >= this.a.b.size()) {
                if (this.a.a()) {
                    removeMessages(0);
                    return;
                } else if (this.a.b.size() == 0) {
                    removeMessages(0);
                    return;
                } else {
                    this.a.a = 0;
                }
            }
            this.a.setImageBitmap((Bitmap) this.a.b.get(this.a.a));
            this.a.invalidate();
            this.a.d();
        }
    }
}
