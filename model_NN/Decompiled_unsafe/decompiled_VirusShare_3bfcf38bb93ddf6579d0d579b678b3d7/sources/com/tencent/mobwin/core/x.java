package com.tencent.mobwin.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;

public class x {
    public static final String a = "/Tencent/MobWIN/";
    public static final String b = "location.txt";
    private static final String c = "OperateCard:";

    private x() {
    }

    public static String a(Context context, String str) {
        if (!"mounted".equals(Environment.getExternalStorageState()) || !Environment.getExternalStorageDirectory().canWrite()) {
            String absolutePath = context.getFilesDir().getAbsolutePath();
            if (absolutePath.endsWith("/")) {
                absolutePath = absolutePath.substring(0, absolutePath.length() - 2);
            }
            return String.valueOf(absolutePath) + str;
        }
        File file = new File(String.valueOf(Environment.getExternalStorageDirectory().getPath()) + str);
        if (!file.exists()) {
            file.mkdirs();
            long lastModified = file.lastModified();
            SharedPreferences.Editor edit = context.getSharedPreferences("mobwin", 0).edit();
            edit.putLong("MOBWIN_FILE_CREATE_TIME", lastModified);
            edit.commit();
        }
        String absolutePath2 = file.getAbsolutePath();
        return !absolutePath2.endsWith("/") ? String.valueOf(absolutePath2) + "/" : absolutePath2;
    }

    public static void a(long j, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("mobwin", 0);
        long j2 = sharedPreferences.getLong("MOBWIN_FILE_CREATE_TIME", 0);
        long currentTimeMillis = System.currentTimeMillis();
        if (j2 != 0 && currentTimeMillis != 0 && j != 0 && currentTimeMillis - j2 > j) {
            a(a(context, a));
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putLong("MOBWIN_FILE_CREATE_TIME", 0);
            edit.commit();
        } else if (j2 == 0) {
            a(a(context, a));
        }
    }

    public static void a(Context context, byte[] bArr) {
        try {
            String a2 = a(context, a);
            o.a(c, "saveAdBitmap 1:save bitmap:" + a2 + b);
            File file = new File(a2);
            try {
                if (!file.exists()) {
                    file.mkdirs();
                }
                File file2 = new File(a2, b);
                if (file2.exists()) {
                    file2.delete();
                }
                file2.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                fileOutputStream.write(bArr);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void a(String str) {
        try {
            b(str);
            new File(str.toString()).delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(String str, Context context) {
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file = new File(Environment.getExternalStorageDirectory() + a + str);
                if (file.exists()) {
                    file.delete();
                }
            }
            if (context != null) {
                File file2 = new File(String.valueOf(context.getFilesDir().getAbsolutePath()) + a + str);
                if (file2.exists()) {
                    file2.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(String str, byte[] bArr, Context context) {
        try {
            String a2 = a(context, a);
            o.a(c, "saveAdBitmap 1:save bitmap:" + a2 + str);
            File file = new File(a2);
            try {
                if (!file.exists()) {
                    file.mkdirs();
                    long lastModified = file.lastModified();
                    SharedPreferences.Editor edit = context.getSharedPreferences("mobwin", 0).edit();
                    edit.putLong("MOBWIN_FILE_CREATE_TIME", lastModified);
                    edit.commit();
                }
                File file2 = new File(a2, str);
                if (file2.exists()) {
                    file2.delete();
                }
                file2.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                fileOutputStream.write(bArr);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0056, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0069, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0056 A[ExcHandler: FileNotFoundException (r0v9 'e' java.io.FileNotFoundException A[CUSTOM_DECLARE]), Splitter:B:6:0x003e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(android.content.Context r5) {
        /*
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0063 }
            java.lang.String r1 = "/Tencent/MobWIN/"
            java.lang.String r1 = a(r5, r1)     // Catch:{ Exception -> 0x0063 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x0063 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0063 }
            java.lang.String r1 = "location.txt"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0063 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0063 }
            java.lang.String r1 = "OperateCard:"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0063 }
            java.lang.String r3 = "readAdBitmap 1:read bitmap:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0063 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x0063 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0063 }
            com.tencent.mobwin.core.o.a(r1, r2)     // Catch:{ Exception -> 0x0063 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0063 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0063 }
            boolean r0 = r1.exists()     // Catch:{ Exception -> 0x0063 }
            if (r0 != 0) goto L_0x003b
            r0 = r4
        L_0x003a:
            return r0
        L_0x003b:
            r5 = 0
            byte[] r5 = (byte[]) r5     // Catch:{ Exception -> 0x0063 }
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x005c }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x005c }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x005c }
            r0.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x005c }
            int r1 = r0.available()     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x005c }
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x005c }
            r0.read(r1)     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0069 }
            r0.close()     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0069 }
            r0 = r1
            goto L_0x003a
        L_0x0056:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0063 }
            r0 = r4
            goto L_0x003a
        L_0x005c:
            r0 = move-exception
            r1 = r5
        L_0x005e:
            r0.printStackTrace()     // Catch:{ Exception -> 0x0063 }
            r0 = r1
            goto L_0x003a
        L_0x0063:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r4
            goto L_0x003a
        L_0x0069:
            r0 = move-exception
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mobwin.core.x.a(android.content.Context):byte[]");
    }

    public static void b(String str) {
        File file = new File(str);
        if (file.exists() && file.isDirectory()) {
            String[] list = file.list();
            for (int i = 0; i < list.length; i++) {
                File file2 = str.endsWith(File.separator) ? new File(String.valueOf(str) + list[i]) : new File(String.valueOf(str) + File.separator + list[i]);
                if (file2.isFile()) {
                    file2.delete();
                }
                if (file2.isDirectory()) {
                    b(String.valueOf(str) + "/" + list[i]);
                    a(String.valueOf(str) + "/" + list[i]);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0054, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0067, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0054 A[ExcHandler: FileNotFoundException (r0v9 'e' java.io.FileNotFoundException A[CUSTOM_DECLARE]), Splitter:B:6:0x003c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] b(java.lang.String r5, android.content.Context r6) {
        /*
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0061 }
            java.lang.String r1 = "/Tencent/MobWIN/"
            java.lang.String r1 = a(r6, r1)     // Catch:{ Exception -> 0x0061 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x0061 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0061 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x0061 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0061 }
            java.lang.String r1 = "OperateCard:"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0061 }
            java.lang.String r3 = "readAdBitmap 1:read bitmap:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0061 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x0061 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0061 }
            com.tencent.mobwin.core.o.a(r1, r2)     // Catch:{ Exception -> 0x0061 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0061 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0061 }
            boolean r0 = r1.exists()     // Catch:{ Exception -> 0x0061 }
            if (r0 != 0) goto L_0x0039
            r0 = r4
        L_0x0038:
            return r0
        L_0x0039:
            r5 = 0
            byte[] r5 = (byte[]) r5     // Catch:{ Exception -> 0x0061 }
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x005a }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x005a }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x005a }
            r0.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x005a }
            int r1 = r0.available()     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x005a }
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x005a }
            r0.read(r1)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0067 }
            r0.close()     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0067 }
            r0 = r1
            goto L_0x0038
        L_0x0054:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0061 }
            r0 = r4
            goto L_0x0038
        L_0x005a:
            r0 = move-exception
            r1 = r5
        L_0x005c:
            r0.printStackTrace()     // Catch:{ Exception -> 0x0061 }
            r0 = r1
            goto L_0x0038
        L_0x0061:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r4
            goto L_0x0038
        L_0x0067:
            r0 = move-exception
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mobwin.core.x.b(java.lang.String, android.content.Context):byte[]");
    }
}
