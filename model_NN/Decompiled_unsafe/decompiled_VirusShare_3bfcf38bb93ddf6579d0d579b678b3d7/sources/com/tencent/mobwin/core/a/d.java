package com.tencent.mobwin.core.a;

import MobWin.AppColorsSetting;
import MobWin.AppModeSetting;
import MobWin.AppRefreshSetting;
import MobWin.AppSettings;
import MobWin.SysSettings;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.text.TextUtils;
import com.tencent.mobwin.core.o;
import com.tencent.mobwin.core.w;
import java.util.HashMap;

public class d {
    private static String A = null;
    private static String B = null;
    public static final String a = "res_embed_browser";
    public static final String b = "res_banner";
    private static int v = 0;
    private static long y = 86400000;
    private final String c = "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/album.png";
    private final String d = "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/download.png";
    private final String e = "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/location.png";
    private final String f = "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/message.png";
    private final String g = "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/phone.png";
    private final String h = "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/web.png";
    private final String i = "http://softfile.3g.qq.com:8080/mobwin/sdk_res/320_1_2";
    private final String j = "http://softfile.3g.qq.com:8080/mobwin/sdk_res/320_1_2/";
    private final String k = "http://softfile.3g.qq.com:8080/mobwin/sdk_res/320_1_2/";
    private final String l = "http://softfile.3g.qq.com:8080/mobwin/sdk_res/320_1_2/";
    private final String m = "http://120.196.211.8/mobwin/embed_browser/mobWIN_pic/320_1_2";
    private AppRefreshSetting n;
    private String o = "";
    private AppModeSetting p;
    private int q;
    private int r = 1;
    private float s = 0.8f;
    private AppColorsSetting t;
    private long u = 0;
    private long w = 0;
    private long x = 2592000000L;
    private HashMap z;

    public static int a() {
        return v;
    }

    public static String j() {
        return A;
    }

    public static String k() {
        return B;
    }

    public String a(int i2) {
        return (String) this.z.get(Integer.valueOf(i2));
    }

    public void a(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("mobwin", 0);
        this.u = sharedPreferences.getLong("appTimeStamp", 0);
        this.w = sharedPreferences.getLong("sysTimeStamp", 0);
        this.s = sharedPreferences.getFloat("minDisplayRatio", 0.8f);
        this.r = sharedPreferences.getInt("maxAdViews", this.r);
        this.o = sharedPreferences.getString("sdkVersion", "");
        v = sharedPreferences.getInt("safeLevel", 0);
        if (!this.o.equals(w.h())) {
            this.w = 0;
        }
        this.x = sharedPreferences.getLong("timeFileDeletePeriodic", 2592000000L);
        this.n = new AppRefreshSetting();
        this.n.a = sharedPreferences.getBoolean("AppRefreshSetting.isUseSDKDefault", true);
        this.n.b = sharedPreferences.getInt("AppRefreshSetting.refreshInterval", 30);
        this.p = new AppModeSetting();
        this.p.isUseSDKDefault = sharedPreferences.getBoolean("AppModeSetting.isUseSDKDefault", true);
        this.p.appMode = sharedPreferences.getInt("AppModeSetting.appMode", 1);
        this.q = sharedPreferences.getInt("APP_PLAY_STATUS_OPTION", 2);
        this.t = new AppColorsSetting();
        this.t.a = sharedPreferences.getBoolean("AppColorsSetting.isUseSDKDefault", true);
        this.t.d = sharedPreferences.getString("AppColorsSetting.bannerBgColor", "");
        this.t.e = sharedPreferences.getString("AppColorSetting.bannerBgOpacity", "");
        this.t.b = sharedPreferences.getString("AppColorSetting.titleColor", "");
        this.t.c = sharedPreferences.getString("AppColorSetting.wordsColor", "");
        this.z = new HashMap();
        this.z.put(0, sharedPreferences.getString("WWW", "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/web.png"));
        this.z.put(1, sharedPreferences.getString("DOWNLOAD", "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/download.png"));
        this.z.put(2, sharedPreferences.getString("PHONE", "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/phone.png"));
        this.z.put(3, sharedPreferences.getString("SMS", "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/message.png"));
        this.z.put(4, sharedPreferences.getString("LOCATION", "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/location.png"));
        this.z.put(5, sharedPreferences.getString("ALBUM", "http://softfile.3g.qq.com:8080/mobwin/ce_icons/320_1_2/album.png"));
        A = sharedPreferences.getString("EMBEDBROWSERRESOURSE", "http://120.196.211.8/mobwin/embed_browser/mobWIN_pic/320_1_2");
        B = sharedPreferences.getString("BANNERRESOURSE", "http://softfile.3g.qq.com:8080/mobwin/sdk_res/320_1_2");
    }

    public void a(Context context, AppSettings appSettings, SysSettings sysSettings) {
        SharedPreferences.Editor edit = context.getSharedPreferences("mobwin", 0).edit();
        edit.putString("sdkVersion", w.h());
        o.a("SDK", "appSetting" + appSettings);
        o.a("SDK", "sysSettings" + sysSettings);
        if (appSettings != null && appSettings.a) {
            this.u = appSettings.b;
            edit.putLong("appTimeStamp", this.u);
            this.q = appSettings.c;
            edit.putInt("APP_PLAY_STATUS_OPTION", this.q);
            if (appSettings.d != null) {
                this.n = appSettings.d;
                if (this.n.b != 0 && this.n.b < 20) {
                    this.n.b = 20;
                } else if (this.n.b > 180) {
                    this.n.b = 180;
                }
                edit.putBoolean("AppRefreshSetting.isUseSDKDefault", this.n.a);
                edit.putInt("AppRefreshSetting.refreshInterval", this.n.b);
            }
            if (appSettings.e != null) {
                this.p = appSettings.e;
                edit.putBoolean("AppModeSetting.isUseSDKDefault", this.p.isUseSDKDefault);
                edit.putInt("AppModeSetting.appMode", this.p.appMode);
            }
            if (appSettings.f != null) {
                this.t = appSettings.f;
                edit.putBoolean("AppColorsSetting.isUseSDKDefault", this.t.a);
                edit.putString("AppColorsSetting.bannerBgColor", this.t.d);
                edit.putString("AppColorSetting.bannerBgOpacity", this.t.e);
                edit.putString("AppColorSetting.titleColor", this.t.b);
                edit.putString("AppColorSetting.wordsColor", this.t.c);
            }
        }
        if (sysSettings != null && sysSettings.a) {
            if (sysSettings.e > 0) {
                this.r = sysSettings.e;
            }
            if (sysSettings.g() > 0) {
                this.x = ((long) sysSettings.g()) * y;
                edit.putLong("timeFileDeletePeriodic", this.x);
            }
            edit.putInt("maxAdViews", this.r);
            if (sysSettings.h > 0.0f) {
                this.s = sysSettings.h;
            }
            edit.putFloat("minDisplayRatio", this.s);
            this.w = sysSettings.b;
            edit.putLong("sysTimeStamp", this.w);
            if (sysSettings.g >= 0) {
                if (v != sysSettings.g) {
                    this.w = 0;
                    edit.putLong("sysTimeStamp", this.w);
                }
                v = sysSettings.g;
                edit.putInt("safeLevel", v);
            }
            if (sysSettings.c != null) {
                this.z = (HashMap) sysSettings.c;
                edit.putString("WWW", (String) this.z.get(0));
                edit.putString("DOWNLOAD", (String) this.z.get(1));
                edit.putString("PHONE", (String) this.z.get(2));
                edit.putString("SMS", (String) this.z.get(3));
                edit.putString("LOCATION", (String) this.z.get(4));
                edit.putString("ALBUM", (String) this.z.get(5));
            }
            if (sysSettings.i != null) {
                HashMap hashMap = (HashMap) sysSettings.i;
                A = (String) hashMap.get(a);
                edit.putString("EMBEDBROWSERRESOURSE", A);
                B = (String) hashMap.get(b);
                edit.putString("BANNERRESOURSE", B);
            }
        }
        edit.commit();
    }

    public long b() {
        return this.u;
    }

    public long c() {
        return this.w;
    }

    public int d() {
        return this.r;
    }

    public float e() {
        return this.s;
    }

    public AppRefreshSetting f() {
        return this.n;
    }

    public AppModeSetting g() {
        return this.p;
    }

    public long h() {
        return this.x;
    }

    public int i() {
        return this.q;
    }

    public int l() {
        String str = this.t.b;
        try {
            if (!TextUtils.isEmpty(str)) {
                return Color.parseColor("#FF" + str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return a.d;
    }

    public int m() {
        String str = this.t.c;
        try {
            if (!TextUtils.isEmpty(str)) {
                return Color.parseColor("#FF" + str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return a.f;
    }

    public int n() {
        String str = this.t.d;
        try {
            if (!TextUtils.isEmpty(str)) {
                return Color.parseColor("#FF" + str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return a.b;
    }

    public boolean o() {
        return this.t.a;
    }

    public int p() {
        String str = this.t.e;
        try {
            if (!TextUtils.isEmpty(str)) {
                return (Integer.parseInt(str) * 255) / 100;
            }
        } catch (Exception e2) {
        }
        return a.h;
    }
}
