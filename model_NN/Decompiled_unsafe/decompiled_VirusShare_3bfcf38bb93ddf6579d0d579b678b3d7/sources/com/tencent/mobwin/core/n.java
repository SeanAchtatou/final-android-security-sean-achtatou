package com.tencent.mobwin.core;

import android.os.Handler;
import android.os.Message;

class n extends Handler {
    final /* synthetic */ w a;

    n(w wVar) {
        this.a = wVar;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void handleMessage(Message message) {
        if (message != null) {
            try {
                switch (message.what) {
                    case 1:
                        this.a.ap.removeMessages(1);
                        if (!w.Q) {
                            this.a.q();
                            return;
                        }
                        return;
                    case 2:
                        if (this.a.ai != null && this.a.ai.a != null) {
                            w wVar = this.a;
                            wVar.Z = wVar.Z + 1;
                            switch (this.a.ai.a.b) {
                                case 0:
                                    if (this.a.Z >= this.a.ai.a.c.size()) {
                                        this.a.Z = 1;
                                    }
                                    if (this.a.Z < this.a.ai.a.c.size()) {
                                        this.a.ak.setText((CharSequence) this.a.ai.a.c.get(this.a.Z));
                                        break;
                                    }
                                    break;
                                case 2:
                                    if (this.a.Z >= this.a.ai.a.c.size()) {
                                        this.a.Z = 0;
                                    }
                                    if (this.a.Z < this.a.ai.a.c.size()) {
                                        this.a.ak.setText((CharSequence) this.a.ai.a.c.get(this.a.Z));
                                        break;
                                    }
                                    break;
                            }
                            this.a.ap.removeMessages(2);
                            this.a.H();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            e.printStackTrace();
        }
    }
}
