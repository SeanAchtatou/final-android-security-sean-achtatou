package com.tencent.mobwin.core;

import MobWin.ADInfo;
import MobWin.BannerInfo;
import MobWin.ResGetAD;
import MobWin.SettingVersions;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import com.tencent.lbsapi.QLBSNotification;
import com.tencent.lbsapi.QLBSService;
import com.tencent.mobwin.AdView;
import com.tencent.mobwin.MobinWINBrowserActivity;
import com.tencent.mobwin.core.a.a;
import com.tencent.mobwin.core.a.b;
import com.tencent.mobwin.core.a.d;
import com.tencent.mobwin.core.a.e;
import com.tencent.mobwin.core.a.h;
import com.tencent.mobwin.core.view.AniImageView;
import com.tencent.mobwin.core.view.c;
import com.tencent.mobwin.core.view.g;
import com.tencent.mobwin.utils.ApkInstalReceiver;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public final class w extends RelativeLayout implements QLBSNotification {
    private static final int A = 2;
    private static final int B = 3;
    private static final int C = 1001;
    private static final int D = 1002;
    private static final int E = 1003;
    private static final int F = 0;
    private static final int G = 1;
    private static final String H = "APP_ID";
    private static final String I = "APP_CHANNEL";
    private static final String J = "APP_INTERVAL";
    private static final String K = "APP_TESTMODE";
    private static final int L = 1;
    private static final int M = 2;
    private static final int N = 3;
    private static boolean O = false;
    private static boolean P = true;
    /* access modifiers changed from: private */
    public static boolean Q = false;
    /* access modifiers changed from: private */
    public static SettingVersions R = null;
    private static byte[] T = null;
    /* access modifiers changed from: private */
    public static boolean U = false;
    public static final String a = "com.android.browser";
    private static final AtomicInteger aa = new AtomicInteger(520093696);
    private static final String af = "AdModel";
    public static final String b = "com.android.browser.BrowserActivity";
    public static int c = 5500;
    public static final int d = 1;
    public static final int e = 2;
    private static String k = null;
    /* access modifiers changed from: private */
    public static d n = null;
    private static boolean q = false;
    private static final String r = "1.3";
    private static String s = "-";
    private static String t = "";
    private static String u = "-";
    private static int w = 20000;
    private static int x = 20000;
    private static final int y = 0;
    private static final int z = 1;
    private QLBSService S;
    /* access modifiers changed from: private */
    public ADInfo V;
    /* access modifiers changed from: private */
    public Bitmap W;
    private int X;
    /* access modifiers changed from: private */
    public a Y;
    /* access modifiers changed from: private */
    public int Z;
    private final int ab;
    private final int ac;
    private final int ad;
    /* access modifiers changed from: private */
    public boolean ae;
    /* access modifiers changed from: private */
    public b ag;
    /* access modifiers changed from: private */
    public BannerInfo ah;
    /* access modifiers changed from: private */
    public h ai;
    /* access modifiers changed from: private */
    public t aj;
    /* access modifiers changed from: private */
    public TextSwitcher ak;
    /* access modifiers changed from: private */
    public AdView al;
    private g am;
    /* access modifiers changed from: private */
    public boolean an;
    /* access modifiers changed from: private */
    public boolean ao;
    /* access modifiers changed from: private */
    public Handler ap;
    /* access modifiers changed from: private */
    public Handler aq;
    /* access modifiers changed from: private */
    public Handler ar;
    private Handler as;
    /* access modifiers changed from: private */
    public ContentObserver at;
    /* access modifiers changed from: private */
    public ContentObserver au;
    private View.OnClickListener av;
    private View.OnTouchListener aw;
    ArrayList f;
    ArrayList g;
    RelativeLayout h;
    private final String i;
    private boolean j;
    private final String l;
    private final int m;
    private final String o;
    private final String p;
    private int v;

    public w(Context context, AdView adView) {
        super(context);
        this.i = "http://mw.app.qq.com/";
        this.j = false;
        this.l = "Ads by MobWIN";
        this.m = 0;
        this.o = "com.google.android.apps.maps";
        this.p = "com.google.android.maps.MapsActivity";
        this.v = 0;
        this.V = null;
        this.W = null;
        this.X = 0;
        this.Y = a.a();
        this.Z = 0;
        this.ab = aa.incrementAndGet();
        this.ac = aa.incrementAndGet();
        this.ad = aa.incrementAndGet();
        this.ae = false;
        this.f = new ArrayList();
        this.g = null;
        this.ai = null;
        this.al = null;
        this.an = false;
        this.ao = false;
        this.ap = new n(this);
        this.aq = new p(this);
        this.ar = new g(this);
        this.as = new Handler();
        this.at = new h(this, this.as);
        this.au = new i(this, this.as);
        this.av = new k(this);
        this.aw = new d(this);
        a(context, adView, this.Y.a, this.Y.c, this.Y.e, this.Y.g);
    }

    public w(Context context, AdView adView, int i2, int i3, int i4, int i5) {
        super(context);
        this.i = "http://mw.app.qq.com/";
        this.j = false;
        this.l = "Ads by MobWIN";
        this.m = 0;
        this.o = "com.google.android.apps.maps";
        this.p = "com.google.android.maps.MapsActivity";
        this.v = 0;
        this.V = null;
        this.W = null;
        this.X = 0;
        this.Y = a.a();
        this.Z = 0;
        this.ab = aa.incrementAndGet();
        this.ac = aa.incrementAndGet();
        this.ad = aa.incrementAndGet();
        this.ae = false;
        this.f = new ArrayList();
        this.g = null;
        this.ai = null;
        this.al = null;
        this.an = false;
        this.ao = false;
        this.ap = new n(this);
        this.aq = new p(this);
        this.ar = new g(this);
        this.as = new Handler();
        this.at = new h(this, this.as);
        this.au = new i(this, this.as);
        this.av = new k(this);
        this.aw = new d(this);
        a(context, adView, i2, i3, i4, i5);
    }

    public w(Context context, AdView adView, String str, String str2, String str3) {
        super(context);
        this.i = "http://mw.app.qq.com/";
        this.j = false;
        this.l = "Ads by MobWIN";
        this.m = 0;
        this.o = "com.google.android.apps.maps";
        this.p = "com.google.android.maps.MapsActivity";
        this.v = 0;
        this.V = null;
        this.W = null;
        this.X = 0;
        this.Y = a.a();
        this.Z = 0;
        this.ab = aa.incrementAndGet();
        this.ac = aa.incrementAndGet();
        this.ad = aa.incrementAndGet();
        this.ae = false;
        this.f = new ArrayList();
        this.g = null;
        this.ai = null;
        this.al = null;
        this.an = false;
        this.ao = false;
        this.ap = new n(this);
        this.aq = new p(this);
        this.ar = new g(this);
        this.as = new Handler();
        this.at = new h(this, this.as);
        this.au = new i(this, this.as);
        this.av = new k(this);
        this.aw = new d(this);
        this.j = true;
        s = str;
        k = str3;
        if (str2 == null) {
            t = "";
        } else {
            t = str2;
        }
        if (k == null) {
            k = "";
        }
        a(context, adView, this.Y.a, this.Y.c, this.Y.e, this.Y.g);
    }

    private void A() {
        try {
            if (this.f != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= this.f.size()) {
                        this.f.clear();
                        this.f = null;
                        return;
                    }
                    c cVar = (c) this.f.get(i3);
                    cVar.a.recycle();
                    cVar.a = null;
                    i2 = i3 + 1;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void B() {
        try {
            if (this.g != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= this.g.size()) {
                        this.g.clear();
                        this.g = null;
                        return;
                    }
                    c cVar = (c) this.g.get(i3);
                    cVar.a.recycle();
                    cVar.a = null;
                    i2 = i3 + 1;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void C() {
        E();
        this.ag.g = G();
        if (this.ai == null || this.ai.a == null) {
            this.aj.a(getContext(), "http://mw.app.qq.com/", this.ag, 0, this.ah, this.ar);
        } else {
            this.aj.a(getContext(), "http://mw.app.qq.com/", this.ag, this.ai.a.a, this.ah, this.ar);
        }
        o.b("IORY", "获取AD失败上报");
        if (this.ai == null || this.ai.a == null) {
            this.ag.a = 0;
        } else {
            this.ag.a = this.ai.a.a;
        }
        this.ag.b = System.currentTimeMillis();
        this.ag.c = System.currentTimeMillis();
        this.ag.d = 0;
        this.ag.e = 0;
        this.ag.f = 0;
        this.ag.g = "";
    }

    /* access modifiers changed from: private */
    public void D() {
        try {
            F();
            this.ag.g = G();
            if (!(this.ai == null || this.ai.a == null)) {
                o.b("SEND_AD_PLAY", "正常上报了哈");
                this.aj.a(getContext(), "http://mw.app.qq.com/", this.ag, this.ai.a.a, this.ah, this.ar);
                this.ag.a = this.ai.a.a;
                this.ai.d = true;
            }
            this.ag.b = System.currentTimeMillis();
            this.ag.c = System.currentTimeMillis();
            this.ag.d = 0;
            this.ag.e = 0;
            this.ag.g = "";
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void E() {
        if (this.ag.e == 0 && this.v != 0) {
            this.ag.e = ((int) System.currentTimeMillis()) - this.v;
            o.a("IORY", "拉取广告时间" + this.ag.e);
            this.v = 0;
        }
    }

    private void F() {
        long j2;
        if (this.ag.b == 0 || this.ag.a == 0) {
            j2 = 0;
        } else {
            this.ag.c = System.currentTimeMillis();
            j2 = this.ag.c - this.ag.b;
            this.ag.b = 0;
            this.ag.c = 0;
        }
        b bVar = this.ag;
        bVar.d = j2 + bVar.d;
        E();
    }

    private String G() {
        return new SimpleDateFormat("yyyy年MM月dd日   HH:mm:ss").format(new Date(System.currentTimeMillis()));
    }

    /* access modifiers changed from: private */
    public void H() {
        this.ap.sendEmptyMessageDelayed(2, (long) c);
    }

    private void I() {
        this.ap.removeMessages(2);
        this.ap.removeMessages(1);
    }

    /* access modifiers changed from: private */
    public void J() {
        if (w != 0 && !this.j) {
            this.ap.sendEmptyMessageDelayed(1, (long) w);
        }
    }

    private void K() {
        if (this.ai != null) {
            F();
            b.a(getContext(), this.ag);
            I();
        }
    }

    private void L() {
        if (this.ai != null) {
            long j2 = 0;
            this.ag = b.a(getContext());
            if (this.ai.a != null && this.ai.a.a == this.ag.a) {
                j2 = this.ag.d;
                this.ag.b = System.currentTimeMillis();
            }
            I();
            this.ap.sendEmptyMessageDelayed(1, ((long) w) - j2);
            this.ap.sendEmptyMessageDelayed(2, (long) c);
        }
    }

    private void M() {
        try {
            PackageManager packageManager = getContext().getPackageManager();
            String packageName = getContext().getPackageName();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 128);
            Bundle bundle = applicationInfo.metaData;
            if (applicationInfo.targetSdkVersion < 4) {
                a("请设置android:minSdkVersion = 4");
                return;
            }
            if (!this.j) {
                if (bundle == null) {
                    a("无法获取应用程序META_DATA信息，请检查配置文件");
                    return;
                }
                s = bundle.getString(H);
                o.a(af, "appidstr" + s);
                if (s == null) {
                    s = String.valueOf(bundle.getInt(H));
                    P = false;
                    a("没有设置APP_ID");
                    o.a(af, "appidint" + s);
                    return;
                }
                t = String.valueOf(bundle.get(I));
                o.a(af, "appchannelstr" + t);
                if (t == null || "null".equals(t)) {
                    t = "";
                }
            }
            if (packageManager.checkPermission("android.permission.INTERNET", packageName) == -1) {
                P = false;
                a("没有在manifest.xml中赋予权限android.permission.INTERNET");
            } else if (packageManager.checkPermission("android.permission.ACCESS_NETWORK_STATE", packageName) == -1) {
                P = false;
                a("没有在manifest.xml中赋予权限android.permission.ACCESS_NETWORK_STATE");
            } else if (packageManager.checkPermission("android.permission.ACCESS_WIFI_STATE", packageName) == -1) {
                P = false;
                a("没有在manifest.xml中赋予权限android.permission.ACCESS_WIFI_STATE");
            } else if (packageManager.checkPermission("android.permission.READ_PHONE_STATE", packageName) == -1) {
                P = false;
                a("没有在manifest.xml中赋予权限android.permission.READ_PHONE_STATE");
            } else if (packageManager.checkPermission("android.permission.WRITE_EXTERNAL_STORAGE", packageName) == -1) {
                P = false;
                a("没有在manifest.xml中赋予权限android.permission.WRITE_EXTERNAL_STORAGE");
            } else {
                if (packageManager.checkPermission("android.permission.ACCESS_COARSE_LOCATION", packageName) == -1) {
                    q = true;
                } else if (packageManager.checkPermission("android.permission.ACCESS_FINE_LOCATION", packageName) == -1) {
                    q = true;
                } else if (packageManager.checkPermission("android.permission.CHANGE_WIFI_STATE", packageName) == -1) {
                    q = true;
                }
                try {
                    packageManager.getActivityInfo(new ComponentName(getContext().getPackageName(), "com.tencent.mobwin.MobinWINBrowserActivity"), 1);
                } catch (PackageManager.NameNotFoundException e2) {
                    P = false;
                    a("没有在manifest.xml中添加com.tencent.mobwin.MobinWINBrowserActivity");
                    o.a(af, "appidint" + s);
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private Bitmap a(String str, int i2) {
        if (str == null || str.equals("")) {
            o.a(af, "url is empty or null");
            return null;
        }
        o.a(af, "ImageUrl : " + str);
        byte[] b2 = x.b(com.tencent.mobwin.utils.b.b(str), getContext());
        if (b2 == null) {
            this.aj.a(getContext(), str, this.ar, i2);
            return null;
        }
        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(b2, 0, b2.length);
        if (decodeByteArray != null) {
            return decodeByteArray;
        }
        this.aj.a(getContext(), str, this.ar, i2);
        return null;
    }

    public static String a() {
        return k;
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if (i2 >= 520193 && i2 <= 520224) {
            b(1);
        } else if (i2 == 520240) {
            b(13);
        } else {
            b(1);
        }
    }

    /* access modifiers changed from: private */
    public void a(ResGetAD resGetAD) {
        this.V = null;
        if (this.ai == null) {
            this.ai = new h();
        }
        if (!"".equals(resGetAD.e)) {
            this.ag.i = this.ag.h;
            this.ag.h = resGetAD.e;
        }
        if (resGetAD.d() != null) {
            n.a(getContext(), null, resGetAD.d());
            if (R != null) {
                R.setSysSetting(n.c());
            }
        }
        if (resGetAD.e() != null) {
            n.a(getContext(), resGetAD.e(), null);
            if (R != null) {
                R.setAppSetting(n.b());
            }
        }
        g();
        ArrayList c2 = resGetAD.c();
        if (c2.size() > 0) {
            this.V = (ADInfo) c2.get(0);
            if (this.V != null) {
                this.ai.a = this.V;
                this.ai.d = false;
                String a2 = n.a(this.V.g);
                this.ai.c = a(a2, 1);
                if (this.V.b == 0) {
                    if (this.ai.b != null) {
                        this.W = this.ai.b;
                    }
                    this.ai.b = null;
                    z();
                    return;
                }
                String str = this.V.d;
                if (str != null) {
                    byte[] b2 = x.b(com.tencent.mobwin.utils.b.b(str), getContext());
                    if (b2 == null) {
                        this.aj.a(getContext(), str, this.ar, 0);
                        return;
                    }
                    Message obtain = Message.obtain();
                    obtain.what = 7;
                    if (com.tencent.mobwin.utils.b.c(str).toLowerCase().endsWith(".gif")) {
                        obtain.arg1 = 1;
                        obtain.obj = b2;
                    } else {
                        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(b2, 0, b2.length);
                        if (decodeByteArray == null) {
                            this.aj.a(getContext(), str, this.ar, 0);
                            return;
                        } else {
                            obtain.arg1 = 0;
                            obtain.obj = decodeByteArray;
                        }
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString("url", str);
                    obtain.setData(bundle);
                    this.ar.sendMessage(obtain);
                    return;
                }
                b(10);
                this.V = null;
                return;
            }
            b(10);
            this.V = null;
            return;
        }
        b(10);
    }

    private void a(Context context, AdView adView, int i2, int i3, int i4, int i5) {
        M();
        if (P) {
            this.al = adView;
            if (R == null) {
                R = new SettingVersions();
            }
            this.ah = new BannerInfo();
            this.ah.a(-1);
            this.ah.b(-1);
            if (this.S == null && !q) {
                this.S = new QLBSService(context, this, "B1_Mobwin_ad_api", "C_nDTS4sRz", "MobWIN_TEST");
                T = null;
                this.S.startLocation(3);
                T = x.a(getContext());
                o.a("IORYLBS", "qLbsService create.");
            }
            this.aj = new t();
            this.ag = b.a(context);
            this.Y.a = i2;
            a.b = i2;
            this.Y.e = i4;
            a.f = i4;
            if (i5 < 153) {
                this.Y.g = 153;
                a.h = 153;
            } else if (i5 > 255) {
                this.Y.g = 255;
                a.h = 255;
            } else {
                this.Y.g = i5;
                a.h = i5;
            }
            this.Y.c = i3;
            a.d = i3;
            if (n == null) {
                n = new d();
            }
            n.a(context);
            if (!(R == null || n == null)) {
                R.setAppSetting(n.b());
                R.setSysSetting(n.c());
            }
            x.a(n.h(), getContext());
            g();
            o.a(af, "color " + i3 + " " + i5);
            if (C0027a.a != context) {
                C0027a.a = context;
                C0027a.b = 1;
                r();
                return;
            }
            C0027a.b++;
            if (C0027a.b <= n.d()) {
                q();
            } else {
                C0027a.b++;
                if (C0027a.b < n.d()) {
                    q();
                }
            }
            this.X = C0027a.b;
        }
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        try {
            this.h = new RelativeLayout(view.getContext());
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            this.h.setBackgroundDrawable(new BitmapDrawable(com.tencent.mobwin.utils.a.c(view.getWidth(), view.getHeight(), -16777216, 128)));
            addView(this.h, layoutParams);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams2.addRule(15);
            layoutParams2.addRule(11);
            this.am = new g(view.getContext());
            this.am.b(A.a().a("button.png", 1), A.a().a("button_pressed.png", 1), null);
            this.am.setOnClickListener(new j(this));
            addView(this.am, layoutParams2);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void a(String str) {
        setBackgroundDrawable(new BitmapDrawable(com.tencent.mobwin.utils.a.a(com.tencent.mobwin.utils.b.d(getContext()), com.tencent.mobwin.utils.b.e(getContext()), this.Y.a, this.Y.g)));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(13);
        layoutParams.topMargin = com.tencent.mobwin.utils.b.a(4, getContext());
        layoutParams.leftMargin = com.tencent.mobwin.utils.b.a(24, getContext());
        TextView textView = new TextView(getContext());
        textView.setText(str);
        textView.setTextColor(-1);
        textView.setSelected(true);
        textView.setId(this.ac);
        addView(textView, layoutParams);
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        this.ag.f = i2;
        J();
        C();
        if (this.al != null && this.al.getAdListener() != null) {
            this.al.getAdListener().onReceiveFailed(i2);
        }
    }

    /* access modifiers changed from: private */
    public void b(View view) {
        if (this.am != null) {
            removeView(this.am);
        }
        if (this.h != null) {
            removeView(this.h);
        }
    }

    public static byte[] b() {
        if (!q) {
            return T;
        }
        return null;
    }

    public static String c() {
        return s;
    }

    public static String d() {
        return t;
    }

    public static int e() {
        return w;
    }

    public static boolean f() {
        return O;
    }

    public static String h() {
        return r;
    }

    /* access modifiers changed from: private */
    public void l() {
        try {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) getLayoutParams();
            layoutParams.width = com.tencent.mobwin.utils.b.d(getContext());
            layoutParams.height = com.tencent.mobwin.utils.b.e(getContext());
            setLayoutParams(layoutParams);
            removeAllViews();
            this.Z = 0;
            switch (this.ai.a.b) {
                case 0:
                    this.Z = 1;
                    s();
                    H();
                    break;
                case 1:
                    if (this.ai.b == null) {
                        if (this.f != null) {
                            u();
                            break;
                        }
                    } else {
                        v();
                        break;
                    }
                    break;
                case 2:
                    this.Z = 0;
                    w();
                    H();
                    break;
            }
            setOnTouchListener(this.aw);
            setOnClickListener(this.av);
            m();
            if (this.ah.b() > 0 && ((float) this.ah.b()) < ((float) com.tencent.mobwin.utils.b.d(getContext())) * n.e()) {
                Q = true;
                I();
                removeAllViews();
                setOnClickListener(null);
                setOnTouchListener(null);
                a("AdWidth不能小于规定尺寸的80%，请重新设置");
            } else if (this.ah.c() > 0 && ((float) this.ah.c()) < ((float) com.tencent.mobwin.utils.b.e(getContext())) * n.e()) {
                Q = true;
                I();
                removeAllViews();
                setOnClickListener(null);
                setOnTouchListener(null);
                a("AdHeight不能小于规定尺寸的80%，请重新设置");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void m() {
        this.ah.b(getHeight());
        this.ah.a(getWidth());
        o.a("BANNER_WIDTH", String.valueOf(this.ah.b()));
        o.a("BANNER_HEIGHT", String.valueOf(this.ah.c()));
        if (this.h != null) {
            o.a("BUTTON_WIDTH", String.valueOf(this.h.getWidth()));
            o.a("BUTTON_HEIGHT", String.valueOf(this.h.getHeight()));
        }
        ViewGroup viewGroup = (ViewGroup) getParent();
        View rootView = getRootView();
        while (viewGroup != null && viewGroup != rootView) {
            if (viewGroup.getVisibility() == 4 || viewGroup.getVisibility() == 8) {
                this.ah.a((byte) 0);
                o.a("parent_visible", viewGroup + " parent is no visible view");
                return;
            }
            o.a("parent_visible", viewGroup + " parent is visible view");
            viewGroup = (ViewGroup) viewGroup.getParent();
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        A.a((Activity) getContext(), this.ar);
    }

    private void o() {
        if (this.al != null && this.al.getAdListener() != null) {
            this.al.getAdListener().onReceiveAd();
        }
    }

    /* access modifiers changed from: private */
    public void p() {
        if (this.al != null && this.al.getAdListener() != null) {
            this.al.getAdListener().onAdClick();
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        o.a(af, new StringBuilder().append(this.aj).toString());
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("mobwin", 0);
        if (sharedPreferences.getBoolean("REPORTADPLAYDATAFAILED", false)) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean("REPORTADPLAYDATAFAILED", false);
            edit.commit();
            this.ag = b.a(getContext());
            this.aj.a(getContext(), "http://mw.app.qq.com/", this.ag, 0, this.ah, this.ar);
            o.b("IORY", "因为上次发送失败上报");
        }
        this.v = (int) System.currentTimeMillis();
        o.a("IORY", "当前beginGetAdTime" + this.v);
        if (n.i() == 2) {
            this.aj.a(getContext(), "http://mw.app.qq.com/", this.ah, R, this.ar);
        }
    }

    private void r() {
        if (!e.b(getContext())) {
            this.aj.a(getContext(), "http://mw.app.qq.com/", this.ar);
        }
        this.aj.a(getContext(), "http://mw.app.qq.com/", n.b(), n.c(), this.ar);
    }

    private void s() {
        setBackgroundDrawable(new BitmapDrawable(com.tencent.mobwin.utils.a.a(com.tencent.mobwin.utils.b.d(getContext()), com.tencent.mobwin.utils.b.e(getContext()), this.Y.a, this.Y.g)));
        setOnTouchListener(this.aw);
        setOnClickListener(this.av);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageBitmap(A.a().a("banner_frame.png", 1));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        addView(imageView, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(11);
        layoutParams2.addRule(10);
        ImageView imageView2 = new ImageView(getContext());
        if (this.ai != null) {
            imageView2.setImageBitmap(this.ai.c);
        }
        imageView2.setId(this.ad);
        addView(imageView2, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(10);
        layoutParams3.addRule(9);
        layoutParams3.addRule(0, this.ad);
        layoutParams3.topMargin = com.tencent.mobwin.utils.b.a(4, getContext());
        layoutParams3.leftMargin = com.tencent.mobwin.utils.b.a(24, getContext());
        TextView textView = new TextView(getContext());
        if (this.ai.a.c != null && this.ai.a.c.size() > 0) {
            textView.setText((CharSequence) this.ai.a.c.get(0));
        }
        textView.setTextSize(0, (float) com.tencent.mobwin.utils.b.a(36, getContext()));
        textView.setTextColor(this.Y.c);
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        textView.setSelected(true);
        textView.setId(this.ac);
        addView(textView, layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(3, this.ac);
        layoutParams4.addRule(9);
        layoutParams4.addRule(0, this.ad);
        layoutParams4.leftMargin = com.tencent.mobwin.utils.b.a(24, getContext());
        this.ak = new TextSwitcher(getContext());
        this.ak.setFactory(new e(this));
        Animation[] a2 = m.a(this.ak, null, y.SwitchText);
        this.ak.setInAnimation(a2[1]);
        this.ak.setOutAnimation(a2[0]);
        addView(this.ak, layoutParams4);
        t();
    }

    private void t() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(12);
        layoutParams.addRule(11);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageBitmap(A.a().a("mobwinLogo.png", 1));
        addView(imageView, layoutParams);
    }

    private void u() {
        setOnTouchListener(this.aw);
        setOnClickListener(this.av);
        AniImageView aniImageView = new AniImageView(getContext());
        aniImageView.a(this.f);
        aniImageView.a(false);
        aniImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        aniImageView.b();
        addView(aniImageView, new RelativeLayout.LayoutParams(-1, -1));
    }

    private void v() {
        setOnTouchListener(this.aw);
        setOnClickListener(this.av);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageBitmap(this.ai.b);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        addView(imageView, new RelativeLayout.LayoutParams(-1, -1));
    }

    private void w() {
        setBackgroundDrawable(new BitmapDrawable(com.tencent.mobwin.utils.a.a(com.tencent.mobwin.utils.b.d(getContext()), com.tencent.mobwin.utils.b.e(getContext()), this.Y.a, this.Y.g)));
        setOnTouchListener(this.aw);
        setOnClickListener(this.av);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageBitmap(A.a().a("banner_frame.png", 1));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        addView(imageView, layoutParams);
        int a2 = com.tencent.mobwin.utils.b.a(80, getContext());
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(com.tencent.mobwin.utils.b.a(82, getContext()), com.tencent.mobwin.utils.b.a(83, getContext()));
        layoutParams2.addRule(15);
        layoutParams2.addRule(9);
        layoutParams2.leftMargin = com.tencent.mobwin.utils.b.a(12, getContext());
        layoutParams2.rightMargin = com.tencent.mobwin.utils.b.a(12, getContext());
        ImageView imageView2 = new ImageView(getContext());
        imageView2.setImageDrawable(new BitmapDrawable(com.tencent.mobwin.utils.a.a(this.ai.b, a2)));
        imageView2.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView2.setId(this.ab);
        addView(imageView2, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(11);
        layoutParams3.addRule(10);
        ImageView imageView3 = new ImageView(getContext());
        if (this.ai != null) {
            imageView3.setImageBitmap(this.ai.c);
        }
        imageView3.setId(this.ad);
        addView(imageView3, layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(1, this.ab);
        layoutParams4.addRule(0, this.ad);
        layoutParams4.topMargin = com.tencent.mobwin.utils.b.a(18, getContext());
        this.ak = new TextSwitcher(getContext());
        this.ak.setFactory(new f(this));
        Animation[] a3 = m.a(this.ak, null, y.SwitchText);
        this.ak.setInAnimation(a3[1]);
        this.ak.setOutAnimation(a3[0]);
        addView(this.ak, layoutParams4);
        t();
    }

    private void x() {
        o.a("TEST", "admodel destroy");
        I();
        removeAllViews();
        if (!(this.ai == null || this.ai.b == null)) {
            this.ai.b.recycle();
            this.ai.b = null;
        }
        C0027a.a = null;
        this.aj.a();
        A();
        B();
        this.al = null;
        this.aj = null;
    }

    private synchronized void y() {
        try {
            A.f();
            if (this.S != null && !q) {
                this.S.stopLocation();
                this.S.release();
                o.a("IORYLBS", "qLbsService release.");
                this.S = null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void z() {
        setOnClickListener(null);
        setOnTouchListener(null);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) getLayoutParams();
        layoutParams.width = com.tencent.mobwin.utils.b.d(getContext());
        layoutParams.height = com.tencent.mobwin.utils.b.e(getContext());
        setLayoutParams(layoutParams);
        q qVar = new q(this, null);
        Animation[] a2 = m.a(this, qVar, y.SwitchAd);
        c cVar = new c(this, a2[1]);
        qVar.a = cVar;
        if (getChildCount() == 0) {
            post(cVar);
        } else {
            startAnimation(a2[0]);
        }
        o();
    }

    public File a(Context context, String str, int i2) {
        Notification notification = new Notification(17301633, "正在下载", System.currentTimeMillis());
        notification.flags = 2;
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return null;
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setConnectTimeout(5000);
        String substring = str.substring(str.lastIndexOf(47) + 1, str.lastIndexOf("apk") + 3);
        if (substring == null || "".equals(substring.trim())) {
            substring = UUID.randomUUID() + ".apk";
        }
        long contentLength = (long) httpURLConnection.getContentLength();
        InputStream inputStream = httpURLConnection.getInputStream();
        File file = new File(x.a(context, x.a), substring);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        byte[] bArr = new byte[1024];
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        Intent intent = new Intent();
        intent.setFlags(872415232);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        PendingIntent activity = PendingIntent.getActivity(context, 0, intent, 134217728);
        PendingIntent activity2 = PendingIntent.getActivity(context.getApplicationContext(), 0, new Intent(context.getApplicationContext(), MobinWINBrowserActivity.class), 134217728);
        int i3 = 0;
        int i4 = 0;
        while (true) {
            int read = bufferedInputStream.read(bArr);
            if (read == -1) {
                fileOutputStream.close();
                bufferedInputStream.close();
                inputStream.close();
                return file;
            }
            fileOutputStream.write(bArr, 0, read);
            i3 += read;
            if (i4 == 0 || ((int) (((long) (i3 * 100)) / contentLength)) - 10 >= i4) {
                i4 += 10;
                notification.setLatestEventInfo(context.getApplicationContext(), substring, "正在下载    " + (((long) (i3 * 100)) / contentLength) + "%", activity2);
                notificationManager.notify(i2, notification);
            }
            if (((long) i3) == contentLength) {
                notification.setLatestEventInfo(context.getApplicationContext(), substring, "下载完成", activity);
                notificationManager.notify(i2, notification);
                a(file, i2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, String str) {
        new l(this, str, i2).start();
    }

    /* access modifiers changed from: protected */
    public void a(File file, int i2) {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addFlags(268435456);
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            getContext().getApplicationContext().startActivity(intent);
            Context context = getContext();
            getContext();
            ((NotificationManager) context.getSystemService("notification")).cancel(i2);
            String uri = Uri.fromFile(file).toString();
            new ApkInstalReceiver().a(getContext(), this.aq, uri.substring(uri.indexOf("/mnt/")));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void g() {
        if (!n.g().isUseSDKDefault) {
            O = n.g().appMode == 0;
        } else {
            O = false;
        }
        if (!n.f().a) {
            w = n.f().b * 1000;
            o.b("INTERVAL", "interval=" + w);
        } else {
            w = x;
            o.b("INTERVAL", "interval=sdkInterval=" + w);
        }
        if (!n.o()) {
            this.Y.g = n.p();
            this.Y.a = n.n();
            this.Y.e = n.m();
            this.Y.c = n.l();
        } else {
            this.Y.g = a.h;
            this.Y.a = a.b;
            this.Y.e = a.f;
            this.Y.c = a.d;
        }
        if (n.i() != 2) {
            K();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.ae = true;
        if (P) {
            this.ag.c = System.currentTimeMillis();
            b.a(getContext(), this.ag);
            C0027a.a = null;
            x();
            y();
        }
        super.onDetachedFromWindow();
    }

    public void onLocationNotification(int i2) {
        if (!q && this.S != null) {
            if (i2 == 1) {
                this.S.isGpsEnabled();
                this.S.getCarrierId();
                T = this.S.getDeviceData();
                if (T != null && T.length > 0) {
                    x.a(getContext(), T);
                }
                o.b(af, "获取位置信息成功.");
            } else if (i2 == 0) {
                o.b(af, "获取硬件位置信息失败.");
            } else {
                o.b(af, "LBS启动失败,错误码=" + i2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        if (P) {
            o.a("TEST", "admonWindowVisibilityChanged" + i2);
            super.onWindowVisibilityChanged(i2);
            if (i2 != 0) {
                K();
            } else if (n.i() == 2) {
                L();
            }
        }
    }
}
