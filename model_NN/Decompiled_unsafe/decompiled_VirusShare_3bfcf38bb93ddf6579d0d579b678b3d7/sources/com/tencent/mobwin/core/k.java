package com.tencent.mobwin.core;

import android.view.View;

class k implements View.OnClickListener {
    final /* synthetic */ w a;

    k(w wVar) {
        this.a = wVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.mobwin.core.w.a(com.tencent.mobwin.core.w, boolean):void
     arg types: [com.tencent.mobwin.core.w, int]
     candidates:
      com.tencent.mobwin.core.w.a(java.lang.String, int):android.graphics.Bitmap
      com.tencent.mobwin.core.w.a(com.tencent.mobwin.core.w, int):void
      com.tencent.mobwin.core.w.a(com.tencent.mobwin.core.w, MobWin.ResGetAD):void
      com.tencent.mobwin.core.w.a(com.tencent.mobwin.core.w, android.graphics.Bitmap):void
      com.tencent.mobwin.core.w.a(com.tencent.mobwin.core.w, android.view.View):void
      com.tencent.mobwin.core.w.a(int, java.lang.String):void
      com.tencent.mobwin.core.w.a(java.io.File, int):void
      com.tencent.mobwin.core.w.a(com.tencent.mobwin.core.w, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x020e A[Catch:{ Exception -> 0x00ca }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x021d A[Catch:{ Exception -> 0x00ca }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(android.view.View r12) {
        /*
            r11 = this;
            r9 = 0
            com.tencent.mobwin.core.w r0 = r11.a
            com.tencent.mobwin.core.a.h r0 = r0.ai
            MobWin.ADInfo r0 = r0.a
            if (r0 == 0) goto L_0x009e
            java.lang.String r0 = "ClickType"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ca }
            java.lang.String r2 = "clicktype: "
            r1.<init>(r2)     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r2 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.h r2 = r2.ai     // Catch:{ Exception -> 0x00ca }
            MobWin.ADInfo r2 = r2.a     // Catch:{ Exception -> 0x00ca }
            int r2 = r2.g     // Catch:{ Exception -> 0x00ca }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.o.b(r0, r1)     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            r0.p()     // Catch:{ Exception -> 0x00ca }
            java.lang.String r0 = ""
            com.tencent.mobwin.core.w r1 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.b r1 = r1.ag     // Catch:{ Exception -> 0x00ca }
            if (r1 == 0) goto L_0x0265
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.b r0 = r0.ag     // Catch:{ Exception -> 0x00ca }
            java.lang.String r0 = r0.h     // Catch:{ Exception -> 0x00ca }
            r7 = r0
        L_0x0041:
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.t r0 = r0.aj     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r1 = r11.a     // Catch:{ Exception -> 0x00ca }
            android.content.Context r1 = r1.getContext()     // Catch:{ Exception -> 0x00ca }
            java.lang.String r2 = "http://mw.app.qq.com/"
            com.tencent.mobwin.core.w r3 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.h r3 = r3.ai     // Catch:{ Exception -> 0x00ca }
            MobWin.ADInfo r3 = r3.a     // Catch:{ Exception -> 0x00ca }
            int r3 = r3.a     // Catch:{ Exception -> 0x00ca }
            r4 = 0
            com.tencent.mobwin.core.w r5 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.h r5 = r5.ai     // Catch:{ Exception -> 0x00ca }
            MobWin.ADInfo r5 = r5.a     // Catch:{ Exception -> 0x00ca }
            java.lang.String r5 = r5.f     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r6 = r11.a     // Catch:{ Exception -> 0x00ca }
            MobWin.BannerInfo r6 = r6.ah     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r8 = r11.a     // Catch:{ Exception -> 0x00ca }
            android.os.Handler r8 = r8.ar     // Catch:{ Exception -> 0x00ca }
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r0 = "IORY"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ca }
            java.lang.String r2 = "普通点击上报ID="
            r1.<init>(r2)     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r2 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.h r2 = r2.ai     // Catch:{ Exception -> 0x00ca }
            MobWin.ADInfo r2 = r2.a     // Catch:{ Exception -> 0x00ca }
            int r2 = r2.a     // Catch:{ Exception -> 0x00ca }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.o.b(r0, r1)     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.h r0 = r0.ai     // Catch:{ Exception -> 0x00ca }
            MobWin.ADInfo r0 = r0.a     // Catch:{ Exception -> 0x00ca }
            int r0 = r0.g     // Catch:{ Exception -> 0x00ca }
            switch(r0) {
                case 0: goto L_0x009f;
                case 1: goto L_0x00cf;
                case 2: goto L_0x00ef;
                case 3: goto L_0x0134;
                case 4: goto L_0x0188;
                case 5: goto L_0x0231;
                default: goto L_0x009e;
            }     // Catch:{ Exception -> 0x00ca }
        L_0x009e:
            return
        L_0x009f:
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r1 = r11.a     // Catch:{ Exception -> 0x00ca }
            android.content.Context r1 = r1.getContext()     // Catch:{ Exception -> 0x00ca }
            java.lang.Class<com.tencent.mobwin.MobinWINBrowserActivity> r2 = com.tencent.mobwin.MobinWINBrowserActivity.class
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r1 = "URL"
            com.tencent.mobwin.core.w r2 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.h r2 = r2.ai     // Catch:{ Exception -> 0x00ca }
            MobWin.ADInfo r2 = r2.a     // Catch:{ Exception -> 0x00ca }
            java.lang.String r2 = r2.e     // Catch:{ Exception -> 0x00ca }
            r0.putExtra(r1, r2)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r1 = "EFFECTIVE_TYPE"
            java.lang.String r2 = "_WWW"
            r0.putExtra(r1, r2)     // Catch:{ Exception -> 0x00ca }
            android.content.Context r1 = r12.getContext()     // Catch:{ Exception -> 0x00ca }
            r1.startActivity(r0)     // Catch:{ Exception -> 0x00ca }
            goto L_0x009e
        L_0x00ca:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x009e
        L_0x00cf:
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            boolean r0 = r0.an     // Catch:{ Exception -> 0x00ca }
            if (r0 != 0) goto L_0x00e3
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            r0.a(r12)     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            r1 = 1
            r0.an = r1     // Catch:{ Exception -> 0x00ca }
            goto L_0x009e
        L_0x00e3:
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            r0.b(r12)     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            r1 = 0
            r0.an = r1     // Catch:{ Exception -> 0x00ca }
            goto L_0x009e
        L_0x00ef:
            android.content.Context r0 = r12.getContext()     // Catch:{ Exception -> 0x00ca }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x00ca }
            android.net.Uri r1 = android.provider.CallLog.Calls.CONTENT_URI     // Catch:{ Exception -> 0x00ca }
            r2 = 1
            com.tencent.mobwin.core.w r3 = r11.a     // Catch:{ Exception -> 0x00ca }
            android.database.ContentObserver r3 = r3.au     // Catch:{ Exception -> 0x00ca }
            r0.registerContentObserver(r1, r2, r3)     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.h r0 = r0.ai     // Catch:{ Exception -> 0x00ca }
            MobWin.ADInfo r0 = r0.a     // Catch:{ Exception -> 0x00ca }
            java.lang.String r0 = r0.e     // Catch:{ Exception -> 0x00ca }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x00ca }
            java.lang.String r2 = "android.intent.action.DIAL"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ca }
            java.lang.String r4 = "tel:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r0 = android.net.Uri.encode(r0)     // Catch:{ Exception -> 0x00ca }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00ca }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x00ca }
            r1.<init>(r2, r0)     // Catch:{ Exception -> 0x00ca }
            android.content.Context r0 = r12.getContext()     // Catch:{ Exception -> 0x00ca }
            r0.startActivity(r1)     // Catch:{ Exception -> 0x00ca }
            goto L_0x009e
        L_0x0134:
            android.content.Context r0 = r12.getContext()     // Catch:{ Exception -> 0x00ca }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x00ca }
            java.lang.String r1 = "content://sms"
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x00ca }
            r2 = 1
            com.tencent.mobwin.core.w r3 = r11.a     // Catch:{ Exception -> 0x00ca }
            android.database.ContentObserver r3 = r3.at     // Catch:{ Exception -> 0x00ca }
            r0.registerContentObserver(r1, r2, r3)     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.h r0 = r0.ai     // Catch:{ Exception -> 0x00ca }
            MobWin.ADInfo r0 = r0.a     // Catch:{ Exception -> 0x00ca }
            java.lang.String r0 = r0.e     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r1 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.h r1 = r1.ai     // Catch:{ Exception -> 0x00ca }
            MobWin.ADInfo r1 = r1.a     // Catch:{ Exception -> 0x00ca }
            java.lang.String r1 = r1.h     // Catch:{ Exception -> 0x00ca }
            android.content.Intent r2 = new android.content.Intent     // Catch:{ Exception -> 0x00ca }
            java.lang.String r3 = "android.intent.action.SENDTO"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ca }
            java.lang.String r5 = "smsto:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x00ca }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00ca }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x00ca }
            r2.<init>(r3, r0)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r0 = "sms_body"
            r2.putExtra(r0, r1)     // Catch:{ Exception -> 0x00ca }
            android.content.Context r0 = r12.getContext()     // Catch:{ Exception -> 0x00ca }
            r0.startActivity(r2)     // Catch:{ Exception -> 0x00ca }
            goto L_0x009e
        L_0x0188:
            com.tencent.mobwin.core.w r0 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.h r0 = r0.ai     // Catch:{ Exception -> 0x00ca }
            MobWin.ADInfo r0 = r0.a     // Catch:{ Exception -> 0x00ca }
            java.lang.String r0 = r0.e     // Catch:{ Exception -> 0x00ca }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x00ca }
            r1.<init>()     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r1 = r11.a     // Catch:{ Exception -> 0x01d6, all -> 0x01ff }
            android.content.Context r1 = r1.getContext()     // Catch:{ Exception -> 0x01d6, all -> 0x01ff }
            android.content.pm.PackageManager r1 = r1.getPackageManager()     // Catch:{ Exception -> 0x01d6, all -> 0x01ff }
            java.lang.String r2 = "com.google.android.apps.maps"
            r3 = 0
            android.content.pm.PackageInfo r1 = r1.getPackageInfo(r2, r3)     // Catch:{ Exception -> 0x01d6, all -> 0x01ff }
            com.tencent.mobwin.core.w r2 = r11.a     // Catch:{ Exception -> 0x0262, all -> 0x025d }
            android.content.Context r2 = r2.getContext()     // Catch:{ Exception -> 0x0262, all -> 0x025d }
            android.content.pm.PackageManager r2 = r2.getPackageManager()     // Catch:{ Exception -> 0x0262, all -> 0x025d }
            java.lang.String r3 = "com.android.browser"
            r4 = 0
            android.content.pm.PackageInfo r2 = r2.getPackageInfo(r3, r4)     // Catch:{ Exception -> 0x0262, all -> 0x025d }
            android.content.Intent r3 = new android.content.Intent     // Catch:{ Exception -> 0x00ca }
            java.lang.String r4 = "android.intent.action.VIEW"
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x00ca }
            r3.<init>(r4, r0)     // Catch:{ Exception -> 0x00ca }
            if (r1 == 0) goto L_0x0227
            java.lang.String r0 = "com.google.android.apps.maps"
            java.lang.String r1 = "com.google.android.maps.MapsActivity"
            r3.setClassName(r0, r1)     // Catch:{ Exception -> 0x00ca }
        L_0x01cd:
            android.content.Context r0 = r12.getContext()     // Catch:{ Exception -> 0x00ca }
            r0.startActivity(r3)     // Catch:{ Exception -> 0x00ca }
            goto L_0x009e
        L_0x01d6:
            r1 = move-exception
            r1 = r9
        L_0x01d8:
            android.content.Intent r2 = new android.content.Intent     // Catch:{ Exception -> 0x00ca }
            java.lang.String r3 = "android.intent.action.VIEW"
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x00ca }
            r2.<init>(r3, r0)     // Catch:{ Exception -> 0x00ca }
            if (r1 == 0) goto L_0x01f5
            java.lang.String r0 = "com.google.android.apps.maps"
            java.lang.String r1 = "com.google.android.maps.MapsActivity"
            r2.setClassName(r0, r1)     // Catch:{ Exception -> 0x00ca }
        L_0x01ec:
            android.content.Context r0 = r12.getContext()     // Catch:{ Exception -> 0x00ca }
            r0.startActivity(r2)     // Catch:{ Exception -> 0x00ca }
            goto L_0x009e
        L_0x01f5:
            if (r9 == 0) goto L_0x01ec
            java.lang.String r0 = "com.android.browser"
            java.lang.String r1 = "com.android.browser.BrowserActivity"
            r2.setClassName(r0, r1)     // Catch:{ Exception -> 0x00ca }
            goto L_0x01ec
        L_0x01ff:
            r1 = move-exception
            r2 = r9
        L_0x0201:
            android.content.Intent r3 = new android.content.Intent     // Catch:{ Exception -> 0x00ca }
            java.lang.String r4 = "android.intent.action.VIEW"
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x00ca }
            r3.<init>(r4, r0)     // Catch:{ Exception -> 0x00ca }
            if (r2 == 0) goto L_0x021d
            java.lang.String r0 = "com.google.android.apps.maps"
            java.lang.String r2 = "com.google.android.maps.MapsActivity"
            r3.setClassName(r0, r2)     // Catch:{ Exception -> 0x00ca }
        L_0x0215:
            android.content.Context r0 = r12.getContext()     // Catch:{ Exception -> 0x00ca }
            r0.startActivity(r3)     // Catch:{ Exception -> 0x00ca }
            throw r1     // Catch:{ Exception -> 0x00ca }
        L_0x021d:
            if (r9 == 0) goto L_0x0215
            java.lang.String r0 = "com.android.browser"
            java.lang.String r2 = "com.android.browser.BrowserActivity"
            r3.setClassName(r0, r2)     // Catch:{ Exception -> 0x00ca }
            goto L_0x0215
        L_0x0227:
            if (r2 == 0) goto L_0x01cd
            java.lang.String r0 = "com.android.browser"
            java.lang.String r1 = "com.android.browser.BrowserActivity"
            r3.setClassName(r0, r1)     // Catch:{ Exception -> 0x00ca }
            goto L_0x01cd
        L_0x0231:
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.w r1 = r11.a     // Catch:{ Exception -> 0x00ca }
            android.content.Context r1 = r1.getContext()     // Catch:{ Exception -> 0x00ca }
            java.lang.Class<com.tencent.mobwin.MobinWINBrowserActivity> r2 = com.tencent.mobwin.MobinWINBrowserActivity.class
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r1 = "URL"
            com.tencent.mobwin.core.w r2 = r11.a     // Catch:{ Exception -> 0x00ca }
            com.tencent.mobwin.core.a.h r2 = r2.ai     // Catch:{ Exception -> 0x00ca }
            MobWin.ADInfo r2 = r2.a     // Catch:{ Exception -> 0x00ca }
            java.util.ArrayList r2 = r2.i     // Catch:{ Exception -> 0x00ca }
            r0.putStringArrayListExtra(r1, r2)     // Catch:{ Exception -> 0x00ca }
            java.lang.String r1 = "EFFECTIVE_TYPE"
            java.lang.String r2 = "_ALBUM"
            r0.putExtra(r1, r2)     // Catch:{ Exception -> 0x00ca }
            android.content.Context r1 = r12.getContext()     // Catch:{ Exception -> 0x00ca }
            r1.startActivity(r0)     // Catch:{ Exception -> 0x00ca }
            goto L_0x009e
        L_0x025d:
            r2 = move-exception
            r10 = r2
            r2 = r1
            r1 = r10
            goto L_0x0201
        L_0x0262:
            r2 = move-exception
            goto L_0x01d8
        L_0x0265:
            r7 = r0
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mobwin.core.k.onClick(android.view.View):void");
    }
}
