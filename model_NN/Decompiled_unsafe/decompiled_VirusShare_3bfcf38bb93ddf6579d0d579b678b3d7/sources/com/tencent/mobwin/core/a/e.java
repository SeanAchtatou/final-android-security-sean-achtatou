package com.tencent.mobwin.core.a;

import MobWin.ADClickAccInfo;
import MobWin.ADViewAccInfo;
import android.content.Context;
import android.content.SharedPreferences;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class e {
    private static final String a = "AdCount:";
    private static final String b = "tencent.ad.activation";
    private static final String c = "tencent.ad.view";
    private static final String d = "tencent.ad.click";
    private static final String e = "activation";
    private static final String f = "save_date";

    public static void a(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences(b, 0).edit();
        edit.putBoolean(e, true);
        edit.commit();
    }

    public static void a(Context context, String str) {
        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        SharedPreferences sharedPreferences = context.getSharedPreferences(c, 0);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (!sharedPreferences.getString(f, "").equals(format)) {
            edit.clear().commit();
        }
        int i = sharedPreferences.getInt(str, 0);
        edit.putString(f, format);
        edit.putInt(str, i + 1);
        edit.commit();
    }

    public static int b(Context context, String str) {
        return context.getSharedPreferences(c, 0).getInt(str, 0);
    }

    public static boolean b(Context context) {
        return context.getSharedPreferences(b, 0).getBoolean(e, false);
    }

    public static ArrayList c(Context context) {
        ArrayList arrayList = new ArrayList();
        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        SharedPreferences sharedPreferences = context.getSharedPreferences(c, 0);
        if (!sharedPreferences.getString(f, "").equals(format)) {
            sharedPreferences.edit().clear().commit();
        }
        Map<String, ?> all = sharedPreferences.getAll();
        for (String next : all.keySet()) {
            if (!next.equals(f)) {
                ADViewAccInfo aDViewAccInfo = new ADViewAccInfo();
                aDViewAccInfo.ad_id = Integer.parseInt(next);
                aDViewAccInfo.view_count = Integer.parseInt(String.valueOf(all.get(next)));
                arrayList.add(aDViewAccInfo);
            }
        }
        return arrayList;
    }

    public static void c(Context context, String str) {
        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        SharedPreferences sharedPreferences = context.getSharedPreferences(d, 0);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (!sharedPreferences.getString(f, "").equals(format)) {
            edit.clear().commit();
        }
        int i = sharedPreferences.getInt(str, 0);
        edit.putString(f, format);
        edit.putInt(str, i + 1);
        edit.commit();
    }

    public static int d(Context context, String str) {
        return context.getSharedPreferences(d, 0).getInt(str, 0);
    }

    public static ArrayList d(Context context) {
        ArrayList arrayList = new ArrayList();
        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        SharedPreferences sharedPreferences = context.getSharedPreferences(d, 0);
        if (!sharedPreferences.getString(f, "").equals(format)) {
            sharedPreferences.edit().clear().commit();
        }
        Map<String, ?> all = sharedPreferences.getAll();
        for (String next : all.keySet()) {
            if (!next.equals(f)) {
                ADClickAccInfo aDClickAccInfo = new ADClickAccInfo();
                aDClickAccInfo.ad_id = Integer.parseInt(next);
                aDClickAccInfo.click_count = Integer.parseInt(String.valueOf(all.get(next)));
                arrayList.add(aDClickAccInfo);
            }
        }
        return arrayList;
    }
}
