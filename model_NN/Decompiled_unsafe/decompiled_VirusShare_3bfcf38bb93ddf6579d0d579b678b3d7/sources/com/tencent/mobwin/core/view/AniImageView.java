package com.tencent.mobwin.core.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.widget.ImageView;
import com.tencent.mobwin.core.o;
import java.util.ArrayList;

public class AniImageView extends ImageView {
    /* access modifiers changed from: private */
    public int a = -1;
    /* access modifiers changed from: private */
    public ArrayList b;
    private Context c;
    private Animation.AnimationListener d;
    private ArrayList e;
    private boolean f = true;
    private long g = 0;
    private Handler h = new e(this);

    public AniImageView(Context context) {
        super(context);
        this.c = context;
    }

    public AniImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = context;
    }

    public AniImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.c = context;
    }

    /* access modifiers changed from: private */
    public void d() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.g >= ((long) e())) {
            this.a++;
            if (this.a >= this.b.size()) {
                if (a()) {
                    this.h.removeMessages(0);
                    this.a = 0;
                    return;
                }
                if (this.d != null) {
                    this.d.onAnimationEnd(null);
                }
                this.a = 0;
            }
            this.h.sendEmptyMessageDelayed(0, (long) (e() / 2));
            this.g = currentTimeMillis - ((currentTimeMillis - this.g) - ((long) e()));
        }
    }

    private int e() {
        if (this.a < this.e.size()) {
            return ((Integer) this.e.get(this.a)).intValue();
        }
        return 0;
    }

    public void a(Animation.AnimationListener animationListener) {
        this.d = animationListener;
    }

    public void a(ArrayList arrayList) {
        this.b = new ArrayList();
        this.e = new ArrayList();
        for (int i = 0; i < arrayList.size(); i++) {
            c cVar = (c) arrayList.get(i);
            this.e.add(Integer.valueOf(cVar.b));
            this.b.add(cVar.a);
        }
        this.a = 0;
        if (this.b.size() > 0) {
            setImageBitmap((Bitmap) this.b.get(0));
        }
    }

    public void a(ArrayList arrayList, ArrayList arrayList2) {
        this.b = arrayList;
        this.e = arrayList2;
        this.a = 0;
        if (this.b.size() > 0) {
            setImageBitmap((Bitmap) this.b.get(0));
        }
    }

    public void a(boolean z) {
        this.f = z;
    }

    public boolean a() {
        return this.f;
    }

    public void b() {
        if (this.b == null || this.e == null || this.b.size() != this.e.size()) {
            o.a("data", "start error");
            return;
        }
        if (this.d != null) {
            this.d.onAnimationStart(null);
        }
        this.h.sendEmptyMessageDelayed(0, (long) e());
    }

    public void c() {
        this.h.removeMessages(0);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        c();
        if (this.b != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.b.size()) {
                    break;
                }
                ((Bitmap) this.b.get(i2)).recycle();
                i = i2 + 1;
            }
            this.b.clear();
            this.b = null;
        }
        this.d = null;
        this.c = null;
        super.onDetachedFromWindow();
    }
}
