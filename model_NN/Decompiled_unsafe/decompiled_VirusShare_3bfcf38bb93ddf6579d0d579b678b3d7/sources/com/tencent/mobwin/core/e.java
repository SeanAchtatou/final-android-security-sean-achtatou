package com.tencent.mobwin.core;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.tencent.mobwin.utils.b;

class e implements ViewSwitcher.ViewFactory {
    final /* synthetic */ w a;

    e(w wVar) {
        this.a = wVar;
    }

    public View makeView() {
        TextView textView = new TextView(this.a.getContext());
        if (this.a.ai.a.c.size() > 1) {
            textView.setText((CharSequence) this.a.ai.a.c.get(1));
        }
        textView.setTextColor(this.a.Y.e);
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        textView.setSelected(true);
        textView.setTextSize(0, (float) b.a(26, this.a.getContext()));
        return textView;
    }
}
