package com.tencent.lbsapi;

public interface QLBSNotification {
    public static final int RESULT_LOCATION_ERROR = 0;
    public static final int RESULT_LOCATION_SUCCESS = 1;

    void onLocationNotification(int i);
}
