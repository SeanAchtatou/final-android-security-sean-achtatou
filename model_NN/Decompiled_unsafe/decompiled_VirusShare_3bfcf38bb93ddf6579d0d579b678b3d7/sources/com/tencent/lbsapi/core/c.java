package com.tencent.lbsapi.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

class c extends BroadcastReceiver {
    final /* synthetic */ QLBSEngine a;

    c(QLBSEngine qLBSEngine) {
        this.a = qLBSEngine;
    }

    public void onReceive(Context context, Intent intent) {
        Location location = (Location) intent.getParcelableExtra("location");
        if (location != null) {
            this.a.processReceiveGps(location);
        }
    }
}
