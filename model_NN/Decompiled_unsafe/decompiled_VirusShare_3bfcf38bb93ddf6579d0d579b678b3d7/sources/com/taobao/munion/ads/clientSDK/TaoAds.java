package com.taobao.munion.ads.clientSDK;

import android.app.Activity;
import android.util.Log;
import android.view.ViewGroup;
import com.taobao.munion.ads.AdView;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.lang.reflect.InvocationTargetException;

public class TaoAds {
    private static final String a = "ClientSDK";
    private static final String b = "taoads_android_sdk_update.jar";
    private Activity c = null;
    private TaoAdsListener d = null;
    private ViewGroup e = null;
    private String f = null;
    private DexClassLoader g;
    private Class h;
    private Object i;
    private AdView j = null;

    public TaoAds(Activity activity) {
        this.c = activity;
    }

    public void TaoAdsClear() {
        Log.d(a, "clear ads");
        if (this.h != null && this.i != null) {
            try {
                this.h.getDeclaredMethod("clear", new Class[0]).invoke(this.i, new Object[0]);
            } catch (SecurityException e2) {
                e2.printStackTrace();
            } catch (NoSuchMethodException e3) {
                e3.printStackTrace();
            } catch (IllegalArgumentException e4) {
                e4.printStackTrace();
            } catch (IllegalAccessException e5) {
                e5.printStackTrace();
            } catch (InvocationTargetException e6) {
                e6.printStackTrace();
            }
        } else if (this.j != null) {
            this.j.clear();
        }
    }

    public void requestAds() {
        File file = new File(this.c.getFilesDir().getAbsoluteFile() + "/" + b);
        if (file.exists()) {
            Log.d(a, "use the update SDK");
            try {
                this.g = new DexClassLoader(file.toString(), this.c.getFilesDir().getAbsolutePath(), null, TaoAds.class.getClassLoader());
                this.h = this.g.loadClass("com.taobao.munion.update.ads.AdView");
                Log.d(a, "load AdView success: " + this.h.toString());
                if (this.c != null) {
                    this.i = this.h.getConstructor(Activity.class).newInstance(this.c);
                    if (this.e != null) {
                        Log.d(a, "set container: " + this.e.toString());
                        this.h.getMethod("setContainer", ViewGroup.class).invoke(this.i, this.e);
                        if (this.d != null) {
                            Log.d(a, "set listener: " + this.d.toString());
                            this.h.getMethod("setTaoAdsListener", TaoAdsListener.class).invoke(this.i, this.d);
                        }
                        if (this.f != null && !"".equals(this.f)) {
                            Log.d(a, "set pid: " + this.f);
                            this.h.getMethod("setPID", String.class).invoke(this.i, this.f);
                        }
                        Log.d(a, "start request ads");
                        this.h.getMethod("requestAds", new Class[0]).invoke(this.i, new Object[0]);
                        return;
                    }
                    Log.e(a, "setContainer failed, check the parameters of the setContainer()");
                    return;
                }
                Log.e(a, "create TaoAds failed, check the parameters of the constructor.");
            } catch (ClassNotFoundException e2) {
                e2.printStackTrace();
            } catch (SecurityException e3) {
                e3.printStackTrace();
            } catch (NoSuchMethodException e4) {
                e4.printStackTrace();
            } catch (IllegalArgumentException e5) {
                e5.printStackTrace();
            } catch (InstantiationException e6) {
                e6.printStackTrace();
            } catch (IllegalAccessException e7) {
                e7.printStackTrace();
            } catch (InvocationTargetException e8) {
                e8.printStackTrace();
            }
        } else {
            Log.d(a, "use the native SDK");
            if (this.c != null) {
                this.j = new AdView(this.c);
                if (this.e != null) {
                    this.j.setContainer(this.e);
                    if (this.d != null) {
                        this.j.setTaoAdsListener(this.d);
                    }
                    if (this.f != null && !"".equals(this.f)) {
                        this.j.setPID(this.f);
                    }
                    if (this.j == null || this.e == null) {
                        Log.e(a, "TaoAds constructor or setContainer() call failed");
                    } else {
                        this.j.requestAds();
                    }
                } else {
                    Log.e(a, "ads container is null, check the setContainer()");
                }
            } else {
                Log.e(a, "create AdView failed, check the TaoAds() constructor");
            }
        }
    }

    public void setContainer(ViewGroup viewGroup) {
        this.e = viewGroup;
    }

    public void setListener(TaoAdsListener taoAdsListener) {
        this.d = taoAdsListener;
    }

    public void setPid(String str) {
        if (str == null || "".equals(str)) {
            Log.w(a, "PID is null, please check it");
        }
        this.f = str;
    }
}
