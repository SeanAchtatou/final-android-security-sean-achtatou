package com.taobao.munion.ads.internal;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public final class ImageDownloader {
    private static final String a = "ImageDownloader";
    private static final String b = "winsmart";
    private static final long c = 604800000;
    private static Bitmap d = null;
    private static String e = null;
    private static Bitmap f = null;
    private static String g = null;
    private static final int h = 10;
    /* access modifiers changed from: private */
    public static final ConcurrentHashMap j = new ConcurrentHashMap(5);
    private static /* synthetic */ int[] k;
    private final HashMap i = new k(this, 5, 0.75f, true);

    public enum Type {
        AD_IMAGE,
        ICON_IMAGE,
        LOGO_IMAGE,
        BG_IMAGE
    }

    private Bitmap a(String str) {
        String str2 = null;
        try {
            str2 = h.a(MessageDigest.getInstance("MD5").digest(str.getBytes()));
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
        }
        String str3 = String.valueOf(j.b.getFilesDir().getAbsolutePath()) + File.separator + b + File.separator + str2;
        Bitmap decodeFile = BitmapFactory.decodeFile(str3);
        if (decodeFile == null && (decodeFile = b(str)) != null) {
            File file = new File(String.valueOf(j.b.getFilesDir().getAbsolutePath()) + File.separator + b);
            if (!file.exists()) {
                file.mkdirs();
            }
            File file2 = new File(str3);
            if (!file2.exists()) {
                try {
                    file2.createNewFile();
                    FileOutputStream fileOutputStream = new FileOutputStream(file2);
                    decodeFile.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    return decodeFile;
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
        }
        return decodeFile;
    }

    public static void a() {
        File[] listFiles;
        File file = new File(String.valueOf(j.b.getFilesDir().getAbsolutePath()) + File.separator + b);
        if (file.exists() && (listFiles = file.listFiles()) != null) {
            long currentTimeMillis = System.currentTimeMillis();
            for (File file2 : listFiles) {
                if (currentTimeMillis - file2.lastModified() > c) {
                    file2.delete();
                }
            }
        }
    }

    private void a(String str, Bitmap bitmap) {
        if (bitmap != null) {
            synchronized (this.i) {
                this.i.put(str, bitmap);
            }
        }
    }

    private static Bitmap b(String str) {
        HttpEntity entity;
        InputStream inputStream;
        String trim = str != null ? str.trim() : str;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(trim);
        try {
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode != 200) {
                Log.w(a, "Error " + statusCode + " while retrieving bitmap from " + trim);
                return null;
            }
            entity = execute.getEntity();
            if (entity != null) {
                try {
                    InputStream content = entity.getContent();
                    try {
                        Bitmap decodeStream = BitmapFactory.decodeStream(new l(content));
                        if (content != null) {
                            content.close();
                        }
                        entity.consumeContent();
                        return decodeStream;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        inputStream = content;
                        th = th2;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    inputStream = null;
                }
            }
            return null;
            if (inputStream != null) {
                inputStream.close();
            }
            entity.consumeContent();
            throw th;
        } catch (IOException e2) {
            httpGet.abort();
            Log.w(a, "I/O error while retrieving bitmap from " + trim, e2);
        } catch (IllegalStateException e3) {
            httpGet.abort();
            Log.w(a, "Incorrect URL: " + trim);
        } catch (Exception e4) {
            httpGet.abort();
            Log.w(a, "Error while retrieving bitmap from " + trim, e4);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0024, code lost:
        r3 = (android.graphics.Bitmap) r3.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002a, code lost:
        if (r3 == null) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0031, code lost:
        com.taobao.munion.ads.internal.ImageDownloader.j.remove(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        r3 = (java.lang.ref.SoftReference) com.taobao.munion.ads.internal.ImageDownloader.j.get(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        if (r3 == null) goto L_0x0036;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Bitmap c(java.lang.String r4) {
        /*
            r3 = this;
            java.util.HashMap r1 = r3.i
            monitor-enter(r1)
            java.util.HashMap r0 = r3.i     // Catch:{ all -> 0x002e }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x002e }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ all -> 0x002e }
            if (r0 == 0) goto L_0x0019
            java.util.HashMap r2 = r3.i     // Catch:{ all -> 0x002e }
            r2.remove(r4)     // Catch:{ all -> 0x002e }
            java.util.HashMap r2 = r3.i     // Catch:{ all -> 0x002e }
            r2.put(r4, r0)     // Catch:{ all -> 0x002e }
            monitor-exit(r1)     // Catch:{ all -> 0x002e }
        L_0x0018:
            return r0
        L_0x0019:
            monitor-exit(r1)
            java.util.concurrent.ConcurrentHashMap r0 = com.taobao.munion.ads.internal.ImageDownloader.j
            java.lang.Object r3 = r0.get(r4)
            java.lang.ref.SoftReference r3 = (java.lang.ref.SoftReference) r3
            if (r3 == 0) goto L_0x0036
            java.lang.Object r3 = r3.get()
            android.graphics.Bitmap r3 = (android.graphics.Bitmap) r3
            if (r3 == 0) goto L_0x0031
            r0 = r3
            goto L_0x0018
        L_0x002e:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0031:
            java.util.concurrent.ConcurrentHashMap r0 = com.taobao.munion.ads.internal.ImageDownloader.j
            r0.remove(r4)
        L_0x0036:
            r0 = 0
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.taobao.munion.ads.internal.ImageDownloader.c(java.lang.String):android.graphics.Bitmap");
    }

    private void c() {
        this.i.clear();
        j.clear();
    }

    private static /* synthetic */ int[] d() {
        int[] iArr = k;
        if (iArr == null) {
            iArr = new int[Type.values().length];
            try {
                iArr[Type.AD_IMAGE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Type.BG_IMAGE.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Type.ICON_IMAGE.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[Type.LOGO_IMAGE.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            k = iArr;
        }
        return iArr;
    }

    public final Bitmap a(String str, Type type) {
        if (str == null) {
            Log.d(a, "url is null");
            return null;
        }
        switch (d()[type.ordinal()]) {
            case 1:
            case 2:
                Bitmap c2 = c(str);
                if (c2 != null) {
                    return c2;
                }
                Bitmap b2 = b(str);
                if (b2 == null || b2 == null) {
                    return b2;
                }
                synchronized (this.i) {
                    this.i.put(str, b2);
                }
                return b2;
            case 3:
                if (str.equals(g)) {
                    return f;
                }
                Bitmap a2 = a(str);
                g = str;
                f = a2;
                return a2;
            case 4:
                if (str.equals(e)) {
                    return d;
                }
                Bitmap a3 = a(str);
                e = str;
                d = a3;
                return a3;
            default:
                return null;
        }
    }
}
