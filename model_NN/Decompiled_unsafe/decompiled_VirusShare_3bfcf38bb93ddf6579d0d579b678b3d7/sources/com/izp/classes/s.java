package com.izp.classes;

import android.content.SharedPreferences;
import org.xml.sax.Attributes;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

final class s extends DefaultHandler {
    String a;
    final /* synthetic */ q b;

    private s(q qVar) {
        this.b = qVar;
        this.a = "";
    }

    public void characters(char[] cArr, int i, int i2) {
        if (this.a.compareTo("ad_freq") == 0) {
            o.a().b.append(cArr, i, i2);
        } else if (this.a.compareTo("ad_vtime") == 0) {
            o.a().c.append(cArr, i, i2);
        } else if (this.a.compareTo("proxy_url") == 0) {
            o.a().d.append(cArr, i, i2);
        } else if (this.a.compareTo("err_code") == 0) {
            this.b.b.append(cArr, i, i2);
        }
    }

    public void endDocument() {
        if (this.b.b.toString().compareTo("0") == 0) {
            this.b.c.R.add("REQUEST_XML");
            this.b.c.T = (double) System.currentTimeMillis();
            SharedPreferences.Editor edit = this.b.c.K.getContext().getSharedPreferences("izp_pre", 0).edit();
            o.a().d.toString();
            edit.putString("izp_pre", o.a().d.toString());
            edit.commit();
            return;
        }
        this.b.c.c(30000);
    }

    public void endElement(String str, String str2, String str3) {
        this.a = "";
    }

    public void error(SAXParseException sAXParseException) {
        this.b.c.c(30000);
    }

    public void startDocument() {
        this.a = "";
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
        if (str2.compareTo("ad_freq") == 0) {
            this.a = "ad_freq";
            o.a().b.setLength(0);
        } else if (str2.compareTo("ad_vtime") == 0) {
            this.a = "ad_vtime";
            o.a().c.setLength(0);
        } else if (str2.compareTo("proxy_url") == 0) {
            this.a = "proxy_url";
            o.a().d.setLength(0);
        } else if (str2.compareTo("err_code") == 0) {
            this.a = "err_code";
            this.b.b.setLength(0);
        }
    }
}
