package com.izp.views;

public interface IZPDelegate {
    void didReceiveFreshAd(IZPView iZPView, int i);

    void didShowFreshAd(IZPView iZPView);

    void didStopFullScreenAd(IZPView iZPView);

    void errorReport(IZPView iZPView, int i, String str);

    boolean shouldRequestFreshAd(IZPView iZPView);

    boolean shouldShowFreshAd(IZPView iZPView);

    void willLeaveApplication(IZPView iZPView);
}
