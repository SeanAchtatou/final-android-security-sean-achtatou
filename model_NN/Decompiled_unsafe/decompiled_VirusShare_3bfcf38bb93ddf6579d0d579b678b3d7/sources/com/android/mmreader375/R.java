package com.android.mmreader375;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int contents_text = 2130968576;
        public static final int encode_view = 2130968577;
        public static final int help_button_view = 2130968578;
        public static final int help_view = 2130968579;
        public static final int possible_result_points = 2130968580;
        public static final int result_image_border = 2130968581;
        public static final int result_minor_text = 2130968582;
        public static final int result_points = 2130968583;
        public static final int result_text = 2130968584;
        public static final int result_view = 2130968585;
        public static final int sbc_header_text = 2130968586;
        public static final int sbc_header_view = 2130968587;
        public static final int sbc_layout_view = 2130968589;
        public static final int sbc_list_item = 2130968588;
        public static final int sbc_page_number_text = 2130968590;
        public static final int sbc_snippet_text = 2130968591;
        public static final int share_text = 2130968592;
        public static final int status_text = 2130968594;
        public static final int status_view = 2130968593;
        public static final int transparent = 2130968595;
        public static final int viewfinder_frame = 2130968596;
        public static final int viewfinder_laser = 2130968597;
        public static final int viewfinder_mask = 2130968598;
    }

    public static final class drawable {
        public static final int back01 = 2130837504;
        public static final int back02 = 2130837505;
        public static final int back_btn = 2130837506;
        public static final int back_dw = 2130837507;
        public static final int back_nm = 2130837508;
        public static final int back_ul = 2130837509;
        public static final int bg = 2130837510;
        public static final int black = 2130837532;
        public static final int blue = 2130837534;
        public static final int cover = 2130837511;
        public static final int dialog_bg = 2130837512;
        public static final int doc = 2130837513;
        public static final int folder = 2130837514;
        public static final int icon = 2130837515;
        public static final int listview_selected = 2130837516;
        public static final int next_btn = 2130837517;
        public static final int next_dw = 2130837518;
        public static final int next_nm = 2130837519;
        public static final int next_ul = 2130837520;
        public static final int nightbg = 2130837521;
        public static final int other = 2130837533;
        public static final int pre_btn = 2130837522;
        public static final int pre_dw = 2130837523;
        public static final int pre_nm = 2130837524;
        public static final int pre_ul = 2130837525;
        public static final int refresh_btn = 2130837526;
        public static final int refresh_dw = 2130837527;
        public static final int refresh_nm = 2130837528;
        public static final int refresh_ul = 2130837529;
        public static final int weblogo = 2130837530;
        public static final int white = 2130837531;
    }

    public static final class id {
        public static final int auto_focus = 2131034112;
        public static final int barcode_image_view = 2131034122;
        public static final int bottom_panel = 2131034138;
        public static final int buttonCancle = 2131034147;
        public static final int buttonConfirm = 2131034146;
        public static final int closebtn = 2131034141;
        public static final int content = 2131034136;
        public static final int contents_supplement_text_view = 2131034132;
        public static final int contents_text_view = 2131034131;
        public static final int decode = 2131034113;
        public static final int decode_failed = 2131034114;
        public static final int decode_succeeded = 2131034115;
        public static final int filebtn = 2131034140;
        public static final int format_text_view = 2131034124;
        public static final int format_text_view_label = 2131034123;
        public static final int icon = 2131034142;
        public static final int mPath = 2131034145;
        public static final int main = 2131034135;
        public static final int meta_text_view = 2131034130;
        public static final int meta_text_view_label = 2131034129;
        public static final int mtitle = 2131034144;
        public static final int musicname = 2131034137;
        public static final int playbtn = 2131034139;
        public static final int preview_view = 2131034119;
        public static final int quit = 2131034116;
        public static final int restart_preview = 2131034117;
        public static final int result_button_view = 2131034133;
        public static final int result_view = 2131034121;
        public static final int return_scan_result = 2131034118;
        public static final int status_view = 2131034134;
        public static final int text = 2131034143;
        public static final int time_text_view = 2131034128;
        public static final int time_text_view_label = 2131034127;
        public static final int type_text_view = 2131034126;
        public static final int type_text_view_label = 2131034125;
        public static final int viewfinder_view = 2131034120;
    }

    public static final class layout {
        public static final int capture = 2130903040;
        public static final int dailog = 2130903041;
        public static final int file_row = 2130903042;
        public static final int fileselect = 2130903043;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int chart_name = 2131099650;
        public static final int msg_bulk_mode_scanned = 2131099652;
        public static final int msg_camera_framework_bug = 2131099653;
        public static final int msg_default_contents = 2131099654;
        public static final int msg_default_format = 2131099655;
        public static final int msg_default_meta = 2131099656;
        public static final int msg_default_mms_subject = 2131099657;
        public static final int msg_default_status = 2131099658;
        public static final int msg_default_time = 2131099659;
        public static final int msg_default_type = 2131099660;
        public static final int msg_encode_barcode_failed = 2131099661;
        public static final int msg_encode_contents_failed = 2131099662;
        public static final int msg_intent_failed = 2131099663;
        public static final int msg_loading_apps = 2131099664;
        public static final int msg_not_our_results = 2131099665;
        public static final int msg_redirect = 2131099666;
        public static final int msg_sbc_book_not_searchable = 2131099667;
        public static final int msg_sbc_failed = 2131099668;
        public static final int msg_sbc_no_page_returned = 2131099669;
        public static final int msg_sbc_page = 2131099670;
        public static final int msg_sbc_searching_book = 2131099671;
        public static final int msg_sbc_snippet_unavailable = 2131099672;
        public static final int msg_sbc_unknown_page = 2131099673;
        public static final int msg_share_explanation = 2131099674;
        public static final int msg_share_subject_line = 2131099675;
        public static final int msg_share_text = 2131099676;
        public static final int msg_unmount_usb = 2131099677;
        public static final int site_name = 2131099648;
        public static final int text_name = 2131099651;
    }

    public static final class styleable {
        public static final int[] ViewfinderView = new int[0];
    }
}
