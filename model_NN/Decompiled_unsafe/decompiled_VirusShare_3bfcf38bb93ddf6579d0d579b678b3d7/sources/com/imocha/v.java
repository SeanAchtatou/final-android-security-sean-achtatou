package com.imocha;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

final class v extends Dialog {
    final /* synthetic */ ShareActivity a;
    private ExpandableListView b = new ExpandableListView(getContext());
    private ExpandableListAdapter c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v(ShareActivity shareActivity, Context context) {
        super(context);
        this.a = shareActivity;
        String str = Build.VERSION.RELEASE;
        setTitle("通讯录");
        this.c = new ag(this, str);
        this.b.setAdapter(this.c);
        this.b.setOnChildClickListener(new ah(this));
        setContentView(this.b);
        this.b.setCacheColorHint(0);
    }

    public final void cancel() {
        this.a.b.setClickable(true);
        super.cancel();
    }
}
