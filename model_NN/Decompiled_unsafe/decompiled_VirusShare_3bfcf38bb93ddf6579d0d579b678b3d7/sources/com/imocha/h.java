package com.imocha;

import android.util.Log;
import java.lang.ref.WeakReference;

final class h implements Runnable {
    private WeakReference a = null;
    private k b;

    h(IMochaAdView iMochaAdView) {
        this.a = new WeakReference(iMochaAdView);
        this.b = ((am) IMochaAdView.f.get(iMochaAdView.e)).a();
    }

    public final void run() {
        String str;
        IMochaAdView iMochaAdView = (IMochaAdView) this.a.get();
        if (iMochaAdView != null) {
            am amVar = (am) IMochaAdView.f.get(iMochaAdView.e);
            if (amVar == null) {
                try {
                    IMochaAdView.onEvent(iMochaAdView, IMochaEventCode.downloadError4);
                } catch (Exception e) {
                    IMochaAdView.onEvent(iMochaAdView, IMochaEventCode.downloadError2);
                    e.printStackTrace();
                }
            } else {
                Log.v("Animation", "onAnimationEnd");
                int i = (int) (this.b.f * 100.0d);
                iMochaAdView.l.setInitialScale(i);
                iMochaAdView.l.a((am) IMochaAdView.f.get(iMochaAdView.e));
                String str2 = String.valueOf(af.a().g(iMochaAdView.getContext())) + "/" + amVar.c();
                if (amVar.k == m.Html) {
                    str = String.valueOf(str2) + "/" + af.a().d(str2, amVar.k.a());
                } else {
                    str = str2;
                }
                iMochaAdView.l.loadUrl("file://" + str);
                iMochaAdView.s.removeAllViews();
                iMochaAdView.s.addView(iMochaAdView.l);
                IMochaAdView.g(iMochaAdView);
                IMochaAdView.onEvent(iMochaAdView, IMochaEventCode.adDownloadFinish);
                IMochaAdView.sendImpressAdLog(iMochaAdView, (am) IMochaAdView.f.get(iMochaAdView.e));
                iMochaAdView.l.setInitialScale(i);
            }
        }
    }
}
