package com.imocha;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.concurrent.TimeUnit;

final class y extends Dialog implements View.OnClickListener {
    ba a;
    EditText b;
    EditText c;
    private am d = null;
    private /* synthetic */ ba e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    y(ba baVar, Context context, am amVar) {
        super(context);
        this.e = baVar;
        this.d = amVar;
    }

    private Dialog a(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(str);
        builder.setPositiveButton("确定", (DialogInterface.OnClickListener) null);
        return builder.create();
    }

    public final void onClick(View view) {
        String trim = this.b.getText().toString().trim();
        if (trim.length() == 0) {
            a("我的邮箱不能为空").show();
        } else if (trim.length() > 50) {
            a("我的邮箱长度不能超过50个字符").show();
        } else if (trim.indexOf("@") == -1 && trim.indexOf("＠") == -1) {
            a("我的邮箱格式错误").show();
        } else {
            IMochaAdView.b.schedule(new f(this, this, this.d.a.a), 0, TimeUnit.SECONDS);
            cancel();
        }
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getContext();
        this.a = (ba) this.d.b;
        ScrollView scrollView = new ScrollView(getContext());
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 3, 5, 0);
        scrollView.addView(linearLayout);
        LinearLayout linearLayout2 = new LinearLayout(getContext());
        LinearLayout linearLayout3 = new LinearLayout(getContext());
        LinearLayout linearLayout4 = new LinearLayout(getContext());
        LinearLayout linearLayout5 = new LinearLayout(getContext());
        LinearLayout linearLayout6 = new LinearLayout(getContext());
        LinearLayout linearLayout7 = new LinearLayout(getContext());
        TextView textView = new TextView(getContext());
        textView.setText("收件人：" + this.a.a);
        linearLayout2.addView(textView);
        linearLayout.addView(linearLayout2);
        TextView textView2 = new TextView(getContext());
        textView2.setText("*");
        textView2.setTextColor(-65536);
        linearLayout3.addView(textView2);
        TextView textView3 = new TextView(getContext());
        textView3.setText("我的邮箱：");
        linearLayout3.addView(textView3);
        this.b = new EditText(getContext());
        linearLayout3.addView(this.b, new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.addView(linearLayout3);
        this.b.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
        TextView textView4 = new TextView(getContext());
        textView4.setText("签　名：");
        linearLayout4.addView(textView4);
        this.c = new EditText(getContext());
        linearLayout4.addView(this.c, new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.addView(linearLayout4);
        this.c.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        TextView textView5 = new TextView(getContext());
        textView5.setText("主　题：" + this.a.b);
        linearLayout6.addView(textView5);
        linearLayout.addView(linearLayout6);
        TextView textView6 = new TextView(getContext());
        textView6.setText(this.a.c);
        linearLayout5.addView(textView6);
        linearLayout.addView(linearLayout5);
        Button button = new Button(getContext());
        button.setText("发送邮件");
        button.setOnClickListener(this);
        linearLayout7.addView(button, new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.addView(linearLayout7);
        linearLayout.setBackgroundColor(-16777216);
        setContentView(scrollView);
        setContentView(scrollView);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.height = -2;
        attributes.width = -1;
        getWindow().setAttributes(attributes);
    }
}
