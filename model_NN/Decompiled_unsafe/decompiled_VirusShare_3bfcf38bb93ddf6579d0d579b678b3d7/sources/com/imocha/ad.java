package com.imocha;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.util.HashMap;
import org.xml.sax.Attributes;

final class ad extends b {
    private HashMap d = new HashMap();

    ad() {
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2) {
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Context context, am amVar) {
        if (context == null) {
            return false;
        }
        String str = (String) this.d.get("2");
        if (str == null || str.length() <= 0) {
            str = (String) this.d.get("0");
        }
        if (str == null || str.length() <= 0) {
            return false;
        }
        str.trim();
        if (str.length() <= 0 || str.length() <= 0) {
            return false;
        }
        if (str.contains("#hdtsysbrw")) {
            try {
                str = str.replace("#hdtsysbrw", "");
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setData(Uri.parse(str));
                intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                context.startActivity(intent);
            } catch (Exception e) {
                Intent intent2 = new Intent();
                intent2.setClass(context, BrowserActivity.class);
                intent2.putExtra("url", str);
                context.startActivity(intent2);
                return true;
            }
        } else if (str.contains("#hdtmobilemap")) {
            try {
                str = str.replace("#hdtmobilemap", "");
                Intent intent3 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent3.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                context.startActivity(intent3);
            } catch (Exception e2) {
                Intent intent4 = new Intent();
                intent4.setClass(context, BrowserActivity.class);
                intent4.putExtra("url", str);
                context.startActivity(intent4);
                return true;
            }
        } else {
            Intent intent5 = new Intent();
            intent5.setClass(context, BrowserActivity.class);
            intent5.putExtra("url", str);
            context.startActivity(intent5);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void b(String str, Attributes attributes) {
        if (str.equals("url") && attributes.getLength() >= 2) {
            this.d.put(attributes.getValue("os"), attributes.getValue("value"));
        }
    }
}
