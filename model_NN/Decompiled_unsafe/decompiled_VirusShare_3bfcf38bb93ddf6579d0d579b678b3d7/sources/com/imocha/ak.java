package com.imocha;

import android.content.Context;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class ak {
    private static double a = 2048.0d;
    private static double b = 10240.0d;
    private JSONObject c = null;
    private JSONArray d = null;

    ak() {
    }

    private static long a() {
        return (long) ((af.a().e() ? b : a) * 1024.0d);
    }

    private static long a(InputStream inputStream, String str) {
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);
        long j = 0;
        File file = new File(str);
        if (file.exists()) {
            af.a().a(str);
        }
        file.mkdirs();
        while (true) {
            ZipEntry nextEntry = zipInputStream.getNextEntry();
            if (nextEntry == null) {
                zipInputStream.close();
                return j;
            }
            String name = nextEntry.getName();
            j += nextEntry.getSize();
            if (nextEntry.isDirectory()) {
                Log.d("iMocha SDK", String.valueOf(name) + "is a folder");
                new File(String.valueOf(str) + File.separator + name.substring(0, name.length() - 1)).mkdirs();
            } else {
                Log.d("iMocha SDK", String.valueOf(name) + "is a normal file");
                File file2 = new File(String.valueOf(str) + File.separator + name);
                file2.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = zipInputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                    fileOutputStream.flush();
                }
                fileOutputStream.close();
            }
        }
    }

    static void a(double d2) {
        if (d2 <= 2048.0d && d2 > 0.0d) {
            a = d2;
        }
    }

    static boolean a(long j) {
        return j < a();
    }

    private static boolean a(Context context, am amVar) {
        try {
            return af.a().e() ? af.a(context, af.a().g(context), amVar.c(), amVar.h) : af.a().a(context, amVar.c(), amVar.h);
        } catch (Exception e) {
            return false;
        }
    }

    private boolean a(Context context, am amVar, long j) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", amVar.c());
            jSONObject.put("time", System.currentTimeMillis());
            jSONObject.put("useSize", j);
            jSONObject.put("url", amVar.b());
            this.d = this.c.getJSONArray("ad");
            this.d.put(jSONObject);
            this.c.put("ad", this.d);
            this.c.put("useSize", this.c.getLong("useSize") + j);
            if (!af.a().e()) {
                return af.a().a(context, "cfg.bin", this.c.toString().getBytes());
            }
            try {
                return af.a(context, af.a().g(context), "cfg.bin", this.c.toString().getBytes());
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private boolean a(IMochaAdView iMochaAdView, am amVar, long j, long j2) {
        boolean z = false;
        String g = af.a().g(iMochaAdView.getContext());
        if (j2 <= j) {
            try {
                if (amVar.k == m.Html) {
                    if (af.a().d(g, amVar.k.a()) != null) {
                        z = true;
                    }
                } else if (amVar.k == m.Image) {
                    z = a(iMochaAdView.getContext(), amVar);
                }
                if (z) {
                    a(iMochaAdView.getContext(), amVar, j2);
                } else {
                    af.a().a(iMochaAdView.getContext(), amVar.c());
                }
                amVar.h = null;
                return z;
            } catch (Exception e) {
                e.printStackTrace();
                if (0 != 0) {
                    a(iMochaAdView.getContext(), amVar, j2);
                } else {
                    af.a().a(iMochaAdView.getContext(), amVar.c());
                }
                amVar.h = null;
                return false;
            } catch (Throwable th) {
                if (0 != 0) {
                    a(iMochaAdView.getContext(), amVar, j2);
                } else {
                    af.a().a(iMochaAdView.getContext(), amVar.c());
                }
                amVar.h = null;
                throw th;
            }
        } else {
            int i = 0;
            long j3 = 0;
            int i2 = 0;
            long j4 = j;
            while (true) {
                try {
                    if (i2 >= this.d.length()) {
                        break;
                    }
                    JSONObject jSONObject = this.d.getJSONObject(i2);
                    long j5 = jSONObject.getLong("useSize");
                    String string = jSONObject.getString("name");
                    af.a().a(iMochaAdView.getContext(), String.valueOf(g) + "/" + string);
                    j4 += j5;
                    if (j4 > j2) {
                        i = i2 + 1;
                        break;
                    }
                    i2++;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return false;
                }
            }
            JSONArray jSONArray = new JSONArray();
            while (i < this.d.length()) {
                JSONObject jSONObject2 = this.d.getJSONObject(i);
                j3 += jSONObject2.getLong("useSize");
                jSONArray.put(jSONObject2);
                i++;
            }
            this.c.remove("ad");
            this.c.put("ad", jSONArray);
            this.c.put("useSize", j3);
            return a(iMochaAdView, amVar, j4, j2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException} */
    private JSONObject b(Context context) {
        try {
            this.c = new JSONObject(new String(af.b(af.a().g(context), "cfg.bin")));
            this.d = this.c.getJSONArray("ad");
        } catch (Exception e) {
            try {
                this.c = new JSONObject(new String(af.b(af.a().g(context), "temp.bin")));
                this.d = this.c.getJSONArray("ad");
            } catch (Exception e2) {
                af.a().a(af.a().g(context));
                this.c = new JSONObject();
                try {
                    this.c.put("useSize", 0L);
                    this.d = new JSONArray();
                    this.c.put("ad", this.d);
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        }
        return this.c;
    }

    private long c(Context context) {
        b(context);
        try {
            return a() - this.c.getLong("useSize");
        } catch (JSONException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Context context) {
        b(context);
        try {
            JSONArray jSONArray = new JSONArray();
            long j = 0;
            for (int i = 0; i < this.d.length(); i++) {
                JSONObject jSONObject = this.d.getJSONObject(i);
                if ((((((double) (System.currentTimeMillis() - jSONObject.getLong("time"))) / 1000.0d) / 60.0d) / 60.0d) / 24.0d > 30.0d) {
                    af.a().a(context, String.valueOf(af.a().g(context)) + "/" + jSONObject.getString("name"));
                } else {
                    j += jSONObject.getLong("useSize");
                    jSONArray.put(jSONObject);
                }
            }
            this.c.put("ad", jSONArray);
            this.c.put("useSize", j);
            if (af.a().e()) {
                af.a(context, af.a().g(context), "cfg.bin", this.c.toString().getBytes());
            } else {
                af.a().a(context, "cfg.bin", this.c.toString().getBytes());
            }
            this.d = null;
            this.c = null;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            af.a().b(af.a().g(context));
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(IMochaAdView iMochaAdView, am amVar) {
        b(iMochaAdView.getContext());
        int i = 0;
        while (i < this.d.length()) {
            try {
                JSONObject jSONObject = this.d.getJSONObject(i);
                if (jSONObject.getString("url").equals(amVar.b()) && af.c(af.a().g(iMochaAdView.getContext()), jSONObject.getString("name"))) {
                    return true;
                }
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean b(IMochaAdView iMochaAdView, am amVar) {
        String g = af.a().g(iMochaAdView.getContext());
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(amVar.d());
        long c2 = c(iMochaAdView.getContext());
        long j = (long) amVar.i;
        if (amVar.k == m.Html) {
            try {
                j = a(byteArrayInputStream, String.valueOf(g) + "/" + amVar.c());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return a(iMochaAdView, amVar, c2, j);
    }
}
