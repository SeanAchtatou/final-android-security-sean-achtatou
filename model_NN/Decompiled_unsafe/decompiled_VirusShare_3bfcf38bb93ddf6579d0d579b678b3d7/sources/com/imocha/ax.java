package com.imocha;

final class ax implements Runnable {
    private IMochaEventCode a;
    private IMochaAdView b;

    public ax(AdView adView, IMochaEventCode iMochaEventCode) {
        this.a = iMochaEventCode;
        this.b = (IMochaAdView) adView;
    }

    public final void run() {
        if (this.b != null) {
            if (this.b.j == IMochaAdType.BANNER) {
                if (this.b.m != null) {
                    this.b.m.onEvent(this.b, this.a);
                }
            } else if (IMochaAdView.a != null) {
                IMochaAdView.a.onEvent(this.b, this.a);
            }
        } else if (IMochaAdView.a != null) {
            IMochaAdView.a.onEvent(this.b, this.a);
        }
    }
}
