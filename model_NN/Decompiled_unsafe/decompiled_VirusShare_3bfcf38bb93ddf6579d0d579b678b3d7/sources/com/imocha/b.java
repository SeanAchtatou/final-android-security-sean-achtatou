package com.imocha;

import android.content.Context;
import org.xml.sax.Attributes;

abstract class b {
    String a;
    String b;
    String c;

    b() {
    }

    static b a(String str, Attributes attributes) {
        b bVar = null;
        if (str.equals("clk") && attributes != null) {
            int length = attributes.getLength();
            b bVar2 = null;
            for (int i = 0; i < length; i++) {
                String localName = attributes.getLocalName(i);
                if (localName.equals("type")) {
                    String value = attributes.getValue(i);
                    if (value.equals("sms")) {
                        bVar2 = new r();
                    } else if (value.equals("browser")) {
                        bVar2 = new ad();
                    } else if (value.equals("call")) {
                        bVar2 = new d();
                    } else if (value.equals("email")) {
                        bVar2 = new ba();
                    } else if (value.equals("html")) {
                        bVar2 = new ae();
                    }
                }
                if (localName.equals("target")) {
                    bVar2.a = attributes.getValue(i);
                }
            }
            bVar = bVar2;
        }
        return bVar == null ? new n() : bVar;
    }

    /* access modifiers changed from: package-private */
    public abstract void a(String str, String str2);

    /* access modifiers changed from: package-private */
    public abstract boolean a(Context context, am amVar);

    /* access modifiers changed from: package-private */
    public abstract void b(String str, Attributes attributes);
}
