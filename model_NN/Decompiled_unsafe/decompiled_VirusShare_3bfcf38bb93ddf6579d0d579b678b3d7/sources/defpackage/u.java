package defpackage;

import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

/* renamed from: u  reason: default package */
public final class u {
    public static final int a = 1000;
    public static final int b = 1001;
    public static final int c = 1002;
    public static final int d = 1003;
    public static final int e = 1004;
    public static final int f = 1005;
    public static final int g = 1006;
    public static final int h = 1007;
    public static final int i = 1008;
    public static final int j = 2000;
    public static final int k = 2001;
    public static final int l = 2002;
    public static final int m = 2003;
    public static final int n = 2004;
    public static final int o = 2005;
    public static final int p = 2006;
    public static final int q = 2007;
    public static final int r = 2008;
    private static TranslateAnimation s = null;
    private static TranslateAnimation t = null;

    public static Animation a(int i2) {
        switch (i2) {
            case 1001:
                s = new TranslateAnimation((float) (0 - h.a(j.b)), 0.0f, 0.0f, 0.0f);
                break;
            case 1002:
                s = new TranslateAnimation((float) h.a(j.b), 0.0f, 0.0f, 0.0f);
                break;
            case 1003:
                s = new TranslateAnimation(0.0f, 0.0f, (float) (0 - ((h.a(j.b) * 3) / 20)), 0.0f);
                break;
            case 1004:
                s = new TranslateAnimation(0.0f, 0.0f, (float) ((h.a(j.b) * 3) / 20), 0.0f);
                break;
            case 1005:
                s = new TranslateAnimation((float) (0 - h.a(j.b)), 0.0f, (float) (0 - ((h.a(j.b) * 3) / 20)), 0.0f);
                break;
            case 1006:
                s = new TranslateAnimation((float) h.a(j.b), 0.0f, (float) (0 - ((h.a(j.b) * 3) / 20)), 0.0f);
                break;
            case 1007:
                s = new TranslateAnimation((float) (0 - h.a(j.b)), 0.0f, (float) ((h.a(j.b) * 3) / 20), 0.0f);
                break;
            case 1008:
                s = new TranslateAnimation((float) h.a(j.b), 0.0f, (float) ((h.a(j.b) * 3) / 20), 0.0f);
                break;
            default:
                s = new TranslateAnimation((float) (0 - h.a(j.b)), 0.0f, 0.0f, 0.0f);
                break;
        }
        return s;
    }

    public static Animation b(int i2) {
        switch (i2) {
            case k /*2001*/:
                t = new TranslateAnimation(0.0f, (float) (0 - h.a(j.b)), 0.0f, 0.0f);
                break;
            case l /*2002*/:
                t = new TranslateAnimation(0.0f, (float) h.a(j.b), 0.0f, 0.0f);
                break;
            case m /*2003*/:
                t = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (0 - ((h.a(j.b) * 3) / 20)));
                break;
            case n /*2004*/:
                t = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) ((h.a(j.b) * 3) / 20));
                break;
            case o /*2005*/:
                t = new TranslateAnimation(0.0f, (float) (0 - h.a(j.b)), 0.0f, (float) (0 - ((h.a(j.b) * 3) / 20)));
                break;
            case p /*2006*/:
                t = new TranslateAnimation(0.0f, (float) h.a(j.b), 0.0f, (float) (0 - ((h.a(j.b) * 3) / 20)));
                break;
            case q /*2007*/:
                t = new TranslateAnimation(0.0f, (float) (0 - h.a(j.b)), 0.0f, (float) ((h.a(j.b) * 3) / 20));
                break;
            case r /*2008*/:
                t = new TranslateAnimation(0.0f, (float) h.a(j.b), 0.0f, (float) ((h.a(j.b) * 3) / 20));
                break;
            default:
                t = new TranslateAnimation(0.0f, (float) h.a(j.b), 0.0f, 0.0f);
                break;
        }
        return t;
    }
}
