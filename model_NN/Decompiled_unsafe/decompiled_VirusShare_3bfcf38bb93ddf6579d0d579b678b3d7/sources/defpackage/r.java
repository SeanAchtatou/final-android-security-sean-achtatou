package defpackage;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/* renamed from: r  reason: default package */
final class r extends WebViewClient {
    final /* synthetic */ m a;

    r(m mVar) {
        this.a = mVar;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.a.x.canGoBack()) {
            this.a.t.setClickable(true);
            this.a.t.setBackgroundResource(j.b.getResources().getIdentifier("pre_btn", "drawable", j.b.getPackageName()));
        } else {
            this.a.t.setClickable(false);
            this.a.t.setBackgroundResource(j.b.getResources().getIdentifier("pre_ul", "drawable", j.b.getPackageName()));
        }
        if (this.a.x.canGoForward()) {
            this.a.u.setClickable(true);
            this.a.u.setBackgroundResource(j.b.getResources().getIdentifier("next_btn", "drawable", j.b.getPackageName()));
        } else {
            this.a.u.setClickable(false);
            this.a.u.setBackgroundResource(j.b.getResources().getIdentifier("next_ul", "drawable", j.b.getPackageName()));
        }
        this.a.p.setVisibility(8);
        super.onPageFinished(webView, str);
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this.a.p.setVisibility(0);
        super.onPageStarted(webView, str, bitmap);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        webView.loadUrl(str);
        return true;
    }
}
