package defpackage;

import android.view.View;

/* renamed from: o  reason: default package */
final class o implements View.OnClickListener {
    final /* synthetic */ m a;

    o(m mVar) {
        this.a = mVar;
    }

    public final void onClick(View view) {
        if (this.a.x.canGoForward()) {
            this.a.x.goForward();
        }
    }
}
