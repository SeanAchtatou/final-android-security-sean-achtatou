package defpackage;

import android.util.Log;
import com.taobao.munion.ads.AdView;
import java.util.TimerTask;

/* renamed from: g  reason: default package */
public final class g extends TimerTask {
    final /* synthetic */ AdView a;

    public g(AdView adView) {
        this.a = adView;
    }

    public final void run() {
        Log.d("AdView", "Requesting new ads with timer: " + AdView.l);
        this.a.requestAds();
    }
}
