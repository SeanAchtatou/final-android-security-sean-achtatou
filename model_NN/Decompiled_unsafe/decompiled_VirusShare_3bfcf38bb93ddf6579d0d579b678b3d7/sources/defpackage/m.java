package defpackage;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.taobao.munion.ads.AdView;

/* renamed from: m  reason: default package */
public final class m extends LinearLayout {
    public Animation a;
    public Animation b;
    private final int c = 1002;
    private final int d = 1003;
    private final int e = 1004;
    private final int f = 1005;
    private final int g = 1006;
    private final int h = 1007;
    private final int i = 1008;
    private final int j = u.k;
    private final int k = u.l;
    private final int l = u.m;
    private Context m;
    /* access modifiers changed from: private */
    public AdView n;
    private LinearLayout o;
    /* access modifiers changed from: private */
    public RelativeLayout p;
    private RelativeLayout q;
    private ProgressBar r;
    private TextView s;
    /* access modifiers changed from: private */
    public Button t;
    /* access modifiers changed from: private */
    public Button u;
    private Button v;
    private Button w;
    /* access modifiers changed from: private */
    public WebView x;
    private RelativeLayout.LayoutParams y;
    private LinearLayout.LayoutParams z;

    public m(Context context, AdView adView) {
        super(context);
        this.m = context;
        this.n = adView;
        this.o = new LinearLayout(this.m);
        this.o.setId(u.k);
        this.o.setClickable(true);
        this.o.setOrientation(0);
        this.o.setBackgroundColor(Color.argb(255, 33, 33, 33));
        this.t = new Button(this.m);
        this.t.setId(1002);
        this.t.setBackgroundResource(j.b.getResources().getIdentifier("pre_btn", "drawable", j.b.getPackageName()));
        this.t.setOnClickListener(new n(this));
        this.u = new Button(this.m);
        this.u.setId(1007);
        this.u.setBackgroundResource(j.b.getResources().getIdentifier("next_btn", "drawable", j.b.getPackageName()));
        this.u.setOnClickListener(new o(this));
        this.v = new Button(this.m);
        this.v.setId(1003);
        this.v.setBackgroundResource(j.b.getResources().getIdentifier("back_btn", "drawable", j.b.getPackageName()));
        this.v.setOnClickListener(new p(this));
        this.w = new Button(this.m);
        this.w.setId(1008);
        this.w.setBackgroundResource(j.b.getResources().getIdentifier("refresh_btn", "drawable", j.b.getPackageName()));
        this.w.setOnClickListener(new q(this));
        this.z = new LinearLayout.LayoutParams(-2, -2, 1.0f);
        this.o.addView(this.v, this.z);
        this.o.addView(this.t, this.z);
        this.o.addView(this.u, this.z);
        this.o.addView(this.w, this.z);
        this.q = new RelativeLayout(this.m);
        this.q.setId(u.m);
        this.x = new WebView(this.m);
        this.x.setId(1004);
        this.x.setFocusable(true);
        this.x.getSettings().setJavaScriptEnabled(true);
        this.x.getSettings().setLightTouchEnabled(true);
        this.x.getSettings().setSupportZoom(true);
        this.x.setFocusableInTouchMode(true);
        this.x.setWebViewClient(new r(this));
        this.p = new RelativeLayout(this.m);
        this.p.setId(u.l);
        this.p.setVisibility(8);
        this.r = new ProgressBar(this.m);
        this.r.setId(1005);
        this.y = new RelativeLayout.LayoutParams(-2, -2);
        this.y.addRule(13);
        this.p.addView(this.r, this.y);
        this.s = new TextView(this.m);
        this.s.setId(1006);
        this.s.setTextColor(-16777216);
        this.s.setText("页面加载中...");
        this.y = new RelativeLayout.LayoutParams(-2, -2);
        this.y.addRule(3, 1005);
        this.y.addRule(14);
        this.p.addView(this.s, this.y);
        this.y = new RelativeLayout.LayoutParams(-1, -1);
        this.q.addView(this.x, this.y);
        this.y = new RelativeLayout.LayoutParams(-1, -1);
        this.y.addRule(13);
        this.q.addView(this.p, this.y);
        setOrientation(1);
        this.z = new LinearLayout.LayoutParams(-1, -1, 20.0f);
        addView(this.q, this.z);
        this.z = new LinearLayout.LayoutParams(-1, -2, 1.0f);
        addView(this.o, this.z);
        this.a = new TranslateAnimation(0.0f, 0.0f, (float) h.b(this.m), 0.0f);
        this.a.setDuration(800);
        this.b = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) h.b(this.m));
        this.b.setDuration(800);
        this.b.setAnimationListener(new s(this));
    }

    /* access modifiers changed from: private */
    public void a() {
        startAnimation(this.b);
    }

    public final void a(String str) {
        this.x.loadUrl(str);
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return false;
        }
        Log.d("NativeWebView", "back key down");
        a();
        return true;
    }
}
