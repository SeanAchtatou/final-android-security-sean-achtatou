package defpackage;

import android.graphics.Bitmap;
import com.taobao.munion.ads.internal.ImageDownloader;
import java.lang.ref.SoftReference;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: k  reason: default package */
public final class k extends LinkedHashMap {
    final /* synthetic */ ImageDownloader a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(ImageDownloader imageDownloader, int i, float f, boolean z) {
        super(5, 0.75f, true);
        this.a = imageDownloader;
    }

    /* access modifiers changed from: protected */
    public final boolean removeEldestEntry(Map.Entry entry) {
        if (size() <= 10) {
            return false;
        }
        ImageDownloader.j.put((String) entry.getKey(), new SoftReference((Bitmap) entry.getValue()));
        return true;
    }
}
