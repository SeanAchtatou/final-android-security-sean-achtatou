package MobWin;

public final class APP_MODE {
    static final /* synthetic */ boolean $assertionsDisabled = (!APP_MODE.class.desiredAssertionStatus());
    public static final APP_MODE NORMAL = new APP_MODE(1, 1, "NORMAL");
    public static final APP_MODE TEST = new APP_MODE(0, 0, "TEST");
    public static final int _NORMAL = 1;
    public static final int _TEST = 0;
    private static APP_MODE[] __values = new APP_MODE[2];
    private String __T = new String();
    private int __value;

    public static APP_MODE convert(int val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].value() == val) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public static APP_MODE convert(String val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].toString().equals(val)) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public int value() {
        return this.__value;
    }

    public String toString() {
        return this.__T;
    }

    private APP_MODE(int index, int val, String s) {
        this.__T = s;
        this.__value = val;
        __values[index] = this;
    }
}
