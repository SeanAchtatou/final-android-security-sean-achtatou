package MobWin;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.mobwin.core.a.f;

public final class UserInfo extends JceStruct {
    static final /* synthetic */ boolean $assertionsDisabled = (!UserInfo.class.desiredAssertionStatus());
    static int cache_net_type;
    public String brand = "";
    public short dm = 160;
    public String imei = "";
    public String imsi = "";
    public String ip = "";
    public String language = "";
    public byte lwp_support = 0;
    public String mac = "";
    public String manufacturer = "";
    public String meid = "";
    public String model = "";
    public int net_type = 0;
    public String os = "";
    public long phone_number = 0;
    public short screen_x = 0;
    public short screen_y = 0;
    public byte tel_support = 0;
    public byte wifi_support = 0;

    public String className() {
        return "MobWin.UserInfo";
    }

    public String getImei() {
        return this.imei;
    }

    public void setImei(String imei2) {
        this.imei = imei2;
    }

    public String getOs() {
        return this.os;
    }

    public void setOs(String os2) {
        this.os = os2;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand2) {
        this.brand = brand2;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model2) {
        this.model = model2;
    }

    public short getScreen_x() {
        return this.screen_x;
    }

    public void setScreen_x(short screen_x2) {
        this.screen_x = screen_x2;
    }

    public short getScreen_y() {
        return this.screen_y;
    }

    public void setScreen_y(short screen_y2) {
        this.screen_y = screen_y2;
    }

    public int getNet_type() {
        return this.net_type;
    }

    public void setNet_type(int net_type2) {
        this.net_type = net_type2;
    }

    public long getPhone_number() {
        return this.phone_number;
    }

    public void setPhone_number(long phone_number2) {
        this.phone_number = phone_number2;
    }

    public String getImsi() {
        return this.imsi;
    }

    public void setImsi(String imsi2) {
        this.imsi = imsi2;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip2) {
        this.ip = ip2;
    }

    public String getMac() {
        return this.mac;
    }

    public void setMac(String mac2) {
        this.mac = mac2;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language2) {
        this.language = language2;
    }

    public String getMeid() {
        return this.meid;
    }

    public void setMeid(String meid2) {
        this.meid = meid2;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String manufacturer2) {
        this.manufacturer = manufacturer2;
    }

    public byte getTel_support() {
        return this.tel_support;
    }

    public void setTel_support(byte tel_support2) {
        this.tel_support = tel_support2;
    }

    public byte getWifi_support() {
        return this.wifi_support;
    }

    public void setWifi_support(byte wifi_support2) {
        this.wifi_support = wifi_support2;
    }

    public byte getLwp_support() {
        return this.lwp_support;
    }

    public void setLwp_support(byte lwp_support2) {
        this.lwp_support = lwp_support2;
    }

    public short getDm() {
        return this.dm;
    }

    public void setDm(short dm2) {
        this.dm = dm2;
    }

    public UserInfo() {
        setImei(this.imei);
        setOs(this.os);
        setBrand(this.brand);
        setModel(this.model);
        setScreen_x(this.screen_x);
        setScreen_y(this.screen_y);
        setNet_type(this.net_type);
        setPhone_number(this.phone_number);
        setImsi(this.imsi);
        setIp(this.ip);
        setMac(this.mac);
        setLanguage(this.language);
        setMeid(this.meid);
        setManufacturer(this.manufacturer);
        setTel_support(this.tel_support);
        setWifi_support(this.wifi_support);
        setLwp_support(this.lwp_support);
        setDm(this.dm);
    }

    public UserInfo(String imei2, String os2, String brand2, String model2, short screen_x2, short screen_y2, int net_type2, long phone_number2, String imsi2, String ip2, String mac2, String language2, String meid2, String manufacturer2, byte tel_support2, byte wifi_support2, byte lwp_support2, short dm2) {
        setImei(imei2);
        setOs(os2);
        setBrand(brand2);
        setModel(model2);
        setScreen_x(screen_x2);
        setScreen_y(screen_y2);
        setNet_type(net_type2);
        setPhone_number(phone_number2);
        setImsi(imsi2);
        setIp(ip2);
        setMac(mac2);
        setLanguage(language2);
        setMeid(meid2);
        setManufacturer(manufacturer2);
        setTel_support(tel_support2);
        setWifi_support(wifi_support2);
        setLwp_support(lwp_support2);
        setDm(dm2);
    }

    public boolean equals(Object o) {
        UserInfo t = (UserInfo) o;
        return JceUtil.equals(this.imei, t.imei) && JceUtil.equals(this.os, t.os) && JceUtil.equals(this.brand, t.brand) && JceUtil.equals(this.model, t.model) && JceUtil.equals(this.screen_x, t.screen_x) && JceUtil.equals(this.screen_y, t.screen_y) && JceUtil.equals(this.net_type, t.net_type) && JceUtil.equals(this.phone_number, t.phone_number) && JceUtil.equals(this.imsi, t.imsi) && JceUtil.equals(this.ip, t.ip) && JceUtil.equals(this.mac, t.mac) && JceUtil.equals(this.language, t.language) && JceUtil.equals(this.meid, t.meid) && JceUtil.equals(this.manufacturer, t.manufacturer) && JceUtil.equals(this.tel_support, t.tel_support) && JceUtil.equals(this.wifi_support, t.wifi_support) && JceUtil.equals(this.lwp_support, t.lwp_support) && JceUtil.equals(this.dm, t.dm);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void writeTo(JceOutputStream _os) {
        _os.write(this.imei, 0);
        _os.write(this.os, 1);
        _os.write(this.brand, 2);
        _os.write(this.model, 3);
        _os.write(this.screen_x, 4);
        _os.write(this.screen_y, 5);
        _os.write(this.net_type, 6);
        _os.write(this.phone_number, 7);
        if (this.imsi != null) {
            _os.write(this.imsi, 8);
        }
        if (this.ip != null) {
            _os.write(this.ip, 9);
        }
        if (this.mac != null) {
            _os.write(this.mac, 10);
        }
        if (this.language != null) {
            _os.write(this.language, 11);
        }
        if (this.meid != null) {
            _os.write(this.meid, 12);
        }
        if (this.manufacturer != null) {
            _os.write(this.manufacturer, 13);
        }
        _os.write(this.tel_support, 14);
        _os.write(this.wifi_support, 15);
        _os.write(this.lwp_support, 16);
        _os.write(this.dm, 17);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte */
    public void readFrom(JceInputStream _is) {
        setImei(_is.readString(0, true));
        setOs(_is.readString(1, true));
        setBrand(_is.readString(2, true));
        setModel(_is.readString(3, true));
        setScreen_x(_is.read(this.screen_x, 4, true));
        setScreen_y(_is.read(this.screen_y, 5, true));
        setNet_type(_is.read(this.net_type, 6, true));
        setPhone_number(_is.read(this.phone_number, 7, false));
        setImsi(_is.readString(8, false));
        setIp(_is.readString(9, false));
        setMac(_is.readString(10, false));
        setLanguage(_is.readString(11, false));
        setMeid(_is.readString(12, false));
        setManufacturer(_is.readString(13, false));
        setTel_support(_is.read(this.tel_support, 14, false));
        setWifi_support(_is.read(this.wifi_support, 15, false));
        setLwp_support(_is.read(this.lwp_support, 16, false));
        setDm(_is.read(this.dm, 17, false));
    }

    public void display(StringBuilder _os, int _level) {
        JceDisplayer _ds = new JceDisplayer(_os, _level);
        _ds.display(this.imei, f.f);
        _ds.display(this.os, "os");
        _ds.display(this.brand, f.m);
        _ds.display(this.model, f.o);
        _ds.display(this.screen_x, "screen_x");
        _ds.display(this.screen_y, "screen_y");
        _ds.display(this.net_type, "net_type");
        _ds.display(this.phone_number, "phone_number");
        _ds.display(this.imsi, f.h);
        _ds.display(this.ip, "ip");
        _ds.display(this.mac, f.r);
        _ds.display(this.language, "language");
        _ds.display(this.meid, f.g);
        _ds.display(this.manufacturer, f.n);
        _ds.display(this.tel_support, f.c);
        _ds.display(this.wifi_support, f.d);
        _ds.display(this.lwp_support, f.e);
        _ds.display(this.dm, "dm");
    }
}
