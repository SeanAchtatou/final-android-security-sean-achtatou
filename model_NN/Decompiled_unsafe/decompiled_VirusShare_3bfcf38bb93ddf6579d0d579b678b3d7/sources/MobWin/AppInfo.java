package MobWin;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class AppInfo extends JceStruct {
    static final /* synthetic */ boolean $assertionsDisabled = (!AppInfo.class.desiredAssertionStatus());
    static int cache_app_mode;
    public String app_id = "";
    public String app_key = "";
    public int app_mode = 0;
    public String app_name = "";
    public String app_signature = "";
    public String app_version = "";
    public String app_version_code = "";
    public String aux_key = "";
    public String integrateId = "";
    public String release_channel = "";
    public String sdk_version = "";

    public String className() {
        return "MobWin.AppInfo";
    }

    public String getApp_id() {
        return this.app_id;
    }

    public void setApp_id(String app_id2) {
        this.app_id = app_id2;
    }

    public String getApp_key() {
        return this.app_key;
    }

    public void setApp_key(String app_key2) {
        this.app_key = app_key2;
    }

    public String getApp_name() {
        return this.app_name;
    }

    public void setApp_name(String app_name2) {
        this.app_name = app_name2;
    }

    public String getApp_signature() {
        return this.app_signature;
    }

    public void setApp_signature(String app_signature2) {
        this.app_signature = app_signature2;
    }

    public String getApp_version() {
        return this.app_version;
    }

    public void setApp_version(String app_version2) {
        this.app_version = app_version2;
    }

    public String getAux_key() {
        return this.aux_key;
    }

    public void setAux_key(String aux_key2) {
        this.aux_key = aux_key2;
    }

    public String getSdk_version() {
        return this.sdk_version;
    }

    public void setSdk_version(String sdk_version2) {
        this.sdk_version = sdk_version2;
    }

    public int getApp_mode() {
        return this.app_mode;
    }

    public void setApp_mode(int app_mode2) {
        this.app_mode = app_mode2;
    }

    public String getApp_version_code() {
        return this.app_version_code;
    }

    public void setApp_version_code(String app_version_code2) {
        this.app_version_code = app_version_code2;
    }

    public String getRelease_channel() {
        return this.release_channel;
    }

    public void setRelease_channel(String release_channel2) {
        this.release_channel = release_channel2;
    }

    public String getIntegrateId() {
        return this.integrateId;
    }

    public void setIntegrateId(String integrateId2) {
        this.integrateId = integrateId2;
    }

    public AppInfo() {
        setApp_id(this.app_id);
        setApp_key(this.app_key);
        setApp_name(this.app_name);
        setApp_signature(this.app_signature);
        setApp_version(this.app_version);
        setAux_key(this.aux_key);
        setSdk_version(this.sdk_version);
        setApp_mode(this.app_mode);
        setApp_version_code(this.app_version_code);
        setRelease_channel(this.release_channel);
        setIntegrateId(this.integrateId);
    }

    public AppInfo(String app_id2, String app_key2, String app_name2, String app_signature2, String app_version2, String aux_key2, String sdk_version2, int app_mode2, String app_version_code2, String release_channel2, String integrateId2) {
        setApp_id(app_id2);
        setApp_key(app_key2);
        setApp_name(app_name2);
        setApp_signature(app_signature2);
        setApp_version(app_version2);
        setAux_key(aux_key2);
        setSdk_version(sdk_version2);
        setApp_mode(app_mode2);
        setApp_version_code(app_version_code2);
        setRelease_channel(release_channel2);
        setIntegrateId(integrateId2);
    }

    public boolean equals(Object o) {
        AppInfo t = (AppInfo) o;
        return JceUtil.equals(this.app_id, t.app_id) && JceUtil.equals(this.app_key, t.app_key) && JceUtil.equals(this.app_name, t.app_name) && JceUtil.equals(this.app_signature, t.app_signature) && JceUtil.equals(this.app_version, t.app_version) && JceUtil.equals(this.aux_key, t.aux_key) && JceUtil.equals(this.sdk_version, t.sdk_version) && JceUtil.equals(this.app_mode, t.app_mode) && JceUtil.equals(this.app_version_code, t.app_version_code) && JceUtil.equals(this.release_channel, t.release_channel) && JceUtil.equals(this.integrateId, t.integrateId);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void writeTo(JceOutputStream _os) {
        _os.write(this.app_id, 0);
        _os.write(this.app_key, 1);
        _os.write(this.app_name, 2);
        _os.write(this.app_signature, 3);
        _os.write(this.app_version, 4);
        _os.write(this.aux_key, 5);
        _os.write(this.sdk_version, 6);
        _os.write(this.app_mode, 7);
        if (this.app_version_code != null) {
            _os.write(this.app_version_code, 8);
        }
        if (this.release_channel != null) {
            _os.write(this.release_channel, 9);
        }
        if (this.integrateId != null) {
            _os.write(this.integrateId, 10);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public void readFrom(JceInputStream _is) {
        setApp_id(_is.readString(0, true));
        setApp_key(_is.readString(1, true));
        setApp_name(_is.readString(2, true));
        setApp_signature(_is.readString(3, true));
        setApp_version(_is.readString(4, true));
        setAux_key(_is.readString(5, true));
        setSdk_version(_is.readString(6, true));
        setApp_mode(_is.read(this.app_mode, 7, true));
        setApp_version_code(_is.readString(8, false));
        setRelease_channel(_is.readString(9, false));
        setIntegrateId(_is.readString(10, false));
    }

    public void display(StringBuilder _os, int _level) {
        JceDisplayer _ds = new JceDisplayer(_os, _level);
        _ds.display(this.app_id, "app_id");
        _ds.display(this.app_key, "app_key");
        _ds.display(this.app_name, "app_name");
        _ds.display(this.app_signature, "app_signature");
        _ds.display(this.app_version, "app_version");
        _ds.display(this.aux_key, "aux_key");
        _ds.display(this.sdk_version, "sdk_version");
        _ds.display(this.app_mode, "app_mode");
        _ds.display(this.app_version_code, "app_version_code");
        _ds.display(this.release_channel, "release_channel");
        _ds.display(this.integrateId, "integrateId");
    }
}
