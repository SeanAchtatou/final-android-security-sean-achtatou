package MobWin;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class GPS extends JceStruct {
    static final /* synthetic */ boolean $assertionsDisabled = (!GPS.class.desiredAssertionStatus());
    static int cache_eType;
    static byte[] cache_vLBSKeyData;
    public int eType = 0;
    public int iAlt = -10000000;
    public int iLat = 900000000;
    public int iLon = 900000000;
    public byte[] vLBSKeyData = null;

    public String className() {
        return "MobWin.GPS";
    }

    public int getILat() {
        return this.iLat;
    }

    public void setILat(int iLat2) {
        this.iLat = iLat2;
    }

    public int getILon() {
        return this.iLon;
    }

    public void setILon(int iLon2) {
        this.iLon = iLon2;
    }

    public int getIAlt() {
        return this.iAlt;
    }

    public void setIAlt(int iAlt2) {
        this.iAlt = iAlt2;
    }

    public int getEType() {
        return this.eType;
    }

    public void setEType(int eType2) {
        this.eType = eType2;
    }

    public byte[] getVLBSKeyData() {
        return this.vLBSKeyData;
    }

    public void setVLBSKeyData(byte[] vLBSKeyData2) {
        this.vLBSKeyData = vLBSKeyData2;
    }

    public GPS() {
        setILat(this.iLat);
        setILon(this.iLon);
        setIAlt(this.iAlt);
        setEType(this.eType);
        setVLBSKeyData(this.vLBSKeyData);
    }

    public GPS(int iLat2, int iLon2, int iAlt2, int eType2, byte[] vLBSKeyData2) {
        setILat(iLat2);
        setILon(iLon2);
        setIAlt(iAlt2);
        setEType(eType2);
        setVLBSKeyData(vLBSKeyData2);
    }

    public boolean equals(Object o) {
        GPS t = (GPS) o;
        return JceUtil.equals(this.iLat, t.iLat) && JceUtil.equals(this.iLon, t.iLon) && JceUtil.equals(this.iAlt, t.iAlt) && JceUtil.equals(this.eType, t.eType) && JceUtil.equals(this.vLBSKeyData, t.vLBSKeyData);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void writeTo(JceOutputStream _os) {
        _os.write(this.iLat, 0);
        _os.write(this.iLon, 1);
        _os.write(this.iAlt, 2);
        _os.write(this.eType, 3);
        if (this.vLBSKeyData != null) {
            _os.write(this.vLBSKeyData, 4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
     arg types: [byte[], int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[] */
    public void readFrom(JceInputStream _is) {
        setILat(_is.read(this.iLat, 0, true));
        setILon(_is.read(this.iLon, 1, true));
        setIAlt(_is.read(this.iAlt, 2, true));
        setEType(_is.read(this.eType, 3, true));
        if (cache_vLBSKeyData == null) {
            cache_vLBSKeyData = new byte[1];
            cache_vLBSKeyData[0] = 0;
        }
        setVLBSKeyData(_is.read(cache_vLBSKeyData, 4, false));
    }

    public void display(StringBuilder _os, int _level) {
        JceDisplayer _ds = new JceDisplayer(_os, _level);
        _ds.display(this.iLat, "iLat");
        _ds.display(this.iLon, "iLon");
        _ds.display(this.iAlt, "iAlt");
        _ds.display(this.eType, "eType");
        _ds.display(this.vLBSKeyData, "vLBSKeyData");
    }
}
