package LBSAPIProtocol;

public final class DeviceType {
    static final /* synthetic */ boolean $assertionsDisabled = (!DeviceType.class.desiredAssertionStatus());
    public static final DeviceType DEVICE_ANDROID = new DeviceType(0, 1, "DEVICE_ANDROID");
    public static final DeviceType DEVICE_IPHONE = new DeviceType(3, 4, "DEVICE_IPHONE");
    public static final DeviceType DEVICE_S60_V3 = new DeviceType(1, 2, "DEVICE_S60_V3");
    public static final DeviceType DEVICE_S60_V5 = new DeviceType(2, 3, "DEVICE_S60_V5");
    public static final int _DEVICE_ANDROID = 1;
    public static final int _DEVICE_IPHONE = 4;
    public static final int _DEVICE_S60_V3 = 2;
    public static final int _DEVICE_S60_V5 = 3;
    private static DeviceType[] __values = new DeviceType[4];
    private String __T = new String();
    private int __value;

    private DeviceType(int i, int i2, String str) {
        this.__T = str;
        this.__value = i2;
        __values[i] = this;
    }

    public static DeviceType convert(int i) {
        for (int i2 = 0; i2 < __values.length; i2++) {
            if (__values[i2].value() == i) {
                return __values[i2];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public static DeviceType convert(String str) {
        for (int i = 0; i < __values.length; i++) {
            if (__values[i].toString().equals(str)) {
                return __values[i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public String toString() {
        return this.__T;
    }

    public int value() {
        return this.__value;
    }
}
