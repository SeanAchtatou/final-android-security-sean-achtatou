package LBSAPIProtocol;

public final class UINTYPE {
    static final /* synthetic */ boolean $assertionsDisabled = (!UINTYPE.class.desiredAssertionStatus());
    public static final UINTYPE UIN_EMAIL = new UINTYPE(1, 1, "UIN_EMAIL");
    public static final UINTYPE UIN_IMEI = new UINTYPE(3, 3, "UIN_IMEI");
    public static final UINTYPE UIN_MOBILE = new UINTYPE(2, 2, "UIN_MOBILE");
    public static final UINTYPE UIN_QQ = new UINTYPE(0, 0, "UIN_QQ");
    public static final int _UIN_EMAIL = 1;
    public static final int _UIN_IMEI = 3;
    public static final int _UIN_MOBILE = 2;
    public static final int _UIN_QQ = 0;
    private static UINTYPE[] __values = new UINTYPE[4];
    private String __T = new String();
    private int __value;

    private UINTYPE(int i, int i2, String str) {
        this.__T = str;
        this.__value = i2;
        __values[i] = this;
    }

    public static UINTYPE convert(int i) {
        for (int i2 = 0; i2 < __values.length; i2++) {
            if (__values[i2].value() == i) {
                return __values[i2];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public static UINTYPE convert(String str) {
        for (int i = 0; i < __values.length; i++) {
            if (__values[i].toString().equals(str)) {
                return __values[i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public String toString() {
        return this.__T;
    }

    public int value() {
        return this.__value;
    }
}
