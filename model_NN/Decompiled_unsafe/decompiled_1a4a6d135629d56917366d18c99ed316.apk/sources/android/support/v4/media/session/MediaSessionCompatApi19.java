package android.support.v4.media.session;

import android.media.RemoteControlClient;
import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompatApi14;

public class MediaSessionCompatApi19 {
    private static final long ACTION_SET_RATING = 128;
    private static final String METADATA_KEY_RATING = "android.media.metadata.RATING";
    private static final String METADATA_KEY_USER_RATING = "android.media.metadata.USER_RATING";
    private static final String METADATA_KEY_YEAR = "android.media.metadata.YEAR";

    public static void setTransportControlFlags(Object rccObj, long actions) {
        ((RemoteControlClient) rccObj).setTransportControlFlags(getRccTransportControlFlagsFromActions(actions));
    }

    public static Object createMetadataUpdateListener(MediaSessionCompatApi14.Callback callback) {
        return new OnMetadataUpdateListener(callback);
    }

    public static void setMetadata(Object rccObj, Bundle metadata, long actions) {
        RemoteControlClient.MetadataEditor editor = ((RemoteControlClient) rccObj).editMetadata(true);
        MediaSessionCompatApi14.buildOldMetadata(metadata, editor);
        addNewMetadata(metadata, editor);
        if ((128 & actions) != 0) {
            editor.addEditableKey(268435457);
        }
        editor.apply();
    }

    public static void setOnMetadataUpdateListener(Object rccObj, Object onMetadataUpdateObj) {
        ((RemoteControlClient) rccObj).setMetadataUpdateListener((RemoteControlClient.OnMetadataUpdateListener) onMetadataUpdateObj);
    }

    static int getRccTransportControlFlagsFromActions(long actions) {
        int transportControlFlags = MediaSessionCompatApi18.getRccTransportControlFlagsFromActions(actions);
        if ((128 & actions) != 0) {
            return transportControlFlags | 512;
        }
        return transportControlFlags;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.RemoteControlClient.MetadataEditor.putLong(int, long):android.media.RemoteControlClient$MetadataEditor throws java.lang.IllegalArgumentException}
     arg types: [int, long]
     candidates:
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putLong(int, long):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException}
      ClspMth{android.media.MediaMetadataEditor.putLong(int, long):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException}
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putLong(int, long):android.media.RemoteControlClient$MetadataEditor throws java.lang.IllegalArgumentException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException}
     arg types: [int, android.os.Parcelable]
     candidates:
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.RemoteControlClient$MetadataEditor throws java.lang.IllegalArgumentException}
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException} */
    static void addNewMetadata(Bundle metadata, RemoteControlClient.MetadataEditor editor) {
        if (metadata != null) {
            if (metadata.containsKey("android.media.metadata.YEAR")) {
                editor.putLong(8, metadata.getLong("android.media.metadata.YEAR"));
            }
            if (metadata.containsKey("android.media.metadata.RATING")) {
                editor.putObject(101, (Object) metadata.getParcelable("android.media.metadata.RATING"));
            }
            if (metadata.containsKey("android.media.metadata.USER_RATING")) {
                editor.putObject(268435457, (Object) metadata.getParcelable("android.media.metadata.USER_RATING"));
            }
        }
    }

    static class OnMetadataUpdateListener<T extends MediaSessionCompatApi14.Callback> implements RemoteControlClient.OnMetadataUpdateListener {
        protected final T mCallback;

        public OnMetadataUpdateListener(T callback) {
            this.mCallback = callback;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void onMetadataUpdate(int r2, java.lang.Object r3) {
            /*
                r1 = this;
                r0 = 268435457(0x10000001, float:2.5243552E-29)
                if (r2 != r0) goto L_0x000e
                boolean r0 = r3 instanceof android.media.Rating
                if (r0 == 0) goto L_0x000e
                T r0 = r1.mCallback
                r0.onSetRating(r3)
            L_0x000e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.media.session.MediaSessionCompatApi19.OnMetadataUpdateListener.onMetadataUpdate(int, java.lang.Object):void");
        }
    }
}
