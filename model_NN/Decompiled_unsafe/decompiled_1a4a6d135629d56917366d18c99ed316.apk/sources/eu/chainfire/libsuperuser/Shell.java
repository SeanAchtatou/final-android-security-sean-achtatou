package eu.chainfire.libsuperuser;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import eu.chainfire.libsuperuser.StreamGobbler;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Shell {
    protected static String[] availableTestCommands = {"echo -BOC-", "id"};

    public interface OnCommandLineListener extends OnResult, StreamGobbler.OnLineListener {
        void onCommandResult(int i, int i2);
    }

    public interface OnCommandResultListener extends OnResult {
        void onCommandResult(int i, int i2, List<String> list);
    }

    private interface OnResult {
        public static final int SHELL_DIED = -2;
        public static final int SHELL_EXEC_FAILED = -3;
        public static final int SHELL_RUNNING = 0;
        public static final int SHELL_WRONG_UID = -4;
        public static final int WATCHDOG_EXIT = -1;
    }

    @Deprecated
    public static List<String> run(String shell, String[] commands, boolean wantSTDERR) {
        return run(shell, commands, null, wantSTDERR);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01d4, code lost:
        r11 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01d3 A[ExcHandler: InterruptedException (e java.lang.InterruptedException), Splitter:B:8:0x0041] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<java.lang.String> run(java.lang.String r20, java.lang.String[] r21, java.lang.String[] r22, boolean r23) {
        /*
            java.util.Locale r15 = java.util.Locale.ENGLISH
            r0 = r20
            java.lang.String r12 = r0.toUpperCase(r15)
            boolean r15 = eu.chainfire.libsuperuser.Debug.getSanityChecksEnabledEffective()
            if (r15 == 0) goto L_0x0021
            boolean r15 = eu.chainfire.libsuperuser.Debug.onMainThread()
            if (r15 == 0) goto L_0x0021
            java.lang.String r15 = "Application attempted to run a shell command from the main thread"
            eu.chainfire.libsuperuser.Debug.log(r15)
            eu.chainfire.libsuperuser.ShellOnMainThreadException r15 = new eu.chainfire.libsuperuser.ShellOnMainThreadException
            java.lang.String r16 = "Application attempted to run a shell command from the main thread"
            r15.<init>(r16)
            throw r15
        L_0x0021:
            java.lang.String r15 = "[%s%%] START"
            r16 = 1
            r0 = r16
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r16 = r0
            r17 = 0
            r16[r17] = r12
            java.lang.String r15 = java.lang.String.format(r15, r16)
            eu.chainfire.libsuperuser.Debug.logCommand(r15)
            java.util.ArrayList r15 = new java.util.ArrayList
            r15.<init>()
            java.util.List r11 = java.util.Collections.synchronizedList(r15)
            if (r22 == 0) goto L_0x006e
            java.util.HashMap r9 = new java.util.HashMap     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r9.<init>()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.util.Map r15 = java.lang.System.getenv()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r9.putAll(r15)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r0 = r22
            int r0 = r0.length     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r16 = r0
            r15 = 0
        L_0x0053:
            r0 = r16
            if (r15 < r0) goto L_0x0122
            r8 = 0
            int r15 = r9.size()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String[] r0 = new java.lang.String[r15]     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r22 = r0
            java.util.Set r15 = r9.entrySet()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.util.Iterator r16 = r15.iterator()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
        L_0x0068:
            boolean r15 = r16.hasNext()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            if (r15 != 0) goto L_0x0149
        L_0x006e:
            java.lang.Runtime r15 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r0 = r20
            r1 = r22
            java.lang.Process r10 = r15.exec(r0, r1)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.io.OutputStream r15 = r10.getOutputStream()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r3.<init>(r15)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            eu.chainfire.libsuperuser.StreamGobbler r4 = new eu.chainfire.libsuperuser.StreamGobbler     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r16 = java.lang.String.valueOf(r12)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r15.<init>(r16)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r16 = "-"
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r15 = r15.toString()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.io.InputStream r16 = r10.getInputStream()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r0 = r16
            r4.<init>(r15, r0, r11)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            eu.chainfire.libsuperuser.StreamGobbler r2 = new eu.chainfire.libsuperuser.StreamGobbler     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r16 = java.lang.String.valueOf(r12)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r15.<init>(r16)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r16 = "*"
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r16 = r15.toString()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.io.InputStream r17 = r10.getErrorStream()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            if (r23 == 0) goto L_0x017e
            r15 = r11
        L_0x00bd:
            r0 = r16
            r1 = r17
            r2.<init>(r0, r1, r15)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r4.start()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r2.start()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r0 = r21
            int r0 = r0.length     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            r16 = r0
            r15 = 0
        L_0x00d0:
            r0 = r16
            if (r15 < r0) goto L_0x0181
            java.lang.String r15 = "exit\n"
            java.lang.String r16 = "UTF-8"
            byte[] r15 = r15.getBytes(r16)     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            r3.write(r15)     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            r3.flush()     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
        L_0x00e2:
            r10.waitFor()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r3.close()     // Catch:{ IOException -> 0x01d7, InterruptedException -> 0x01d3 }
        L_0x00e8:
            r4.join()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r2.join()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r10.destroy()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            boolean r15 = eu.chainfire.libsuperuser.Shell.SU.isSU(r20)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            if (r15 == 0) goto L_0x0102
            int r15 = r10.exitValue()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r16 = 255(0xff, float:3.57E-43)
            r0 = r16
            if (r15 != r0) goto L_0x0102
            r11 = 0
        L_0x0102:
            java.lang.String r15 = "[%s%%] END"
            r16 = 1
            r0 = r16
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r16 = r0
            r17 = 0
            java.util.Locale r18 = java.util.Locale.ENGLISH
            r0 = r20
            r1 = r18
            java.lang.String r18 = r0.toUpperCase(r1)
            r16[r17] = r18
            java.lang.String r15 = java.lang.String.format(r15, r16)
            eu.chainfire.libsuperuser.Debug.logCommand(r15)
            return r11
        L_0x0122:
            r6 = r22[r15]     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r17 = "="
            r0 = r17
            int r13 = r6.indexOf(r0)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            if (r13 < 0) goto L_0x0145
            r17 = 0
            r0 = r17
            java.lang.String r17 = r6.substring(r0, r13)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            int r18 = r13 + 1
            r0 = r18
            java.lang.String r18 = r6.substring(r0)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r0 = r17
            r1 = r18
            r9.put(r0, r1)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
        L_0x0145:
            int r15 = r15 + 1
            goto L_0x0053
        L_0x0149:
            java.lang.Object r7 = r16.next()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.util.Map$Entry r7 = (java.util.Map.Entry) r7     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.Object r15 = r7.getKey()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r15 = (java.lang.String) r15     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r15 = java.lang.String.valueOf(r15)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r0 = r17
            r0.<init>(r15)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r15 = "="
            r0 = r17
            java.lang.StringBuilder r17 = r0.append(r15)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.Object r15 = r7.getValue()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r15 = (java.lang.String) r15     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r0 = r17
            java.lang.StringBuilder r15 = r0.append(r15)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r15 = r15.toString()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            r22[r8] = r15     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            int r8 = r8 + 1
            goto L_0x0068
        L_0x017e:
            r15 = 0
            goto L_0x00bd
        L_0x0181:
            r14 = r21[r15]     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            java.lang.String r17 = "[%s+] %s"
            r18 = 2
            r0 = r18
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            r18 = r0
            r19 = 0
            r18[r19] = r12     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            r19 = 1
            r18[r19] = r14     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            java.lang.String r17 = java.lang.String.format(r17, r18)     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            eu.chainfire.libsuperuser.Debug.logCommand(r17)     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            java.lang.String r18 = java.lang.String.valueOf(r14)     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            r17.<init>(r18)     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            java.lang.String r18 = "\n"
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            java.lang.String r17 = r17.toString()     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            java.lang.String r18 = "UTF-8"
            byte[] r17 = r17.getBytes(r18)     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            r0 = r17
            r3.write(r0)     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            r3.flush()     // Catch:{ IOException -> 0x01c1, InterruptedException -> 0x01d3 }
            int r15 = r15 + 1
            goto L_0x00d0
        L_0x01c1:
            r5 = move-exception
            java.lang.String r15 = r5.getMessage()     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            java.lang.String r16 = "EPIPE"
            boolean r15 = r15.contains(r16)     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
            if (r15 != 0) goto L_0x00e2
            throw r5     // Catch:{ IOException -> 0x01cf, InterruptedException -> 0x01d3 }
        L_0x01cf:
            r5 = move-exception
            r11 = 0
            goto L_0x0102
        L_0x01d3:
            r5 = move-exception
            r11 = 0
            goto L_0x0102
        L_0x01d7:
            r15 = move-exception
            goto L_0x00e8
        */
        throw new UnsupportedOperationException("Method not decompiled: eu.chainfire.libsuperuser.Shell.run(java.lang.String, java.lang.String[], java.lang.String[], boolean):java.util.List");
    }

    protected static boolean parseAvailableResult(List<String> ret, boolean checkForRoot) {
        if (ret == null) {
            return false;
        }
        boolean echo_seen = false;
        for (String line : ret) {
            if (line.contains("uid=")) {
                return !checkForRoot || line.contains("uid=0");
            }
            if (line.contains("-BOC-")) {
                echo_seen = true;
            }
        }
        return echo_seen;
    }

    public static class SH {
        public static List<String> run(String command) {
            return Shell.run("sh", new String[]{command}, null, false);
        }

        public static List<String> run(List<String> commands) {
            return Shell.run("sh", (String[]) commands.toArray(new String[commands.size()]), null, false);
        }

        public static List<String> run(String[] commands) {
            return Shell.run("sh", commands, null, false);
        }
    }

    public static class SU {
        private static Boolean isSELinuxEnforcing = null;
        private static String[] suVersion = new String[2];

        public static List<String> run(String command) {
            return Shell.run("su", new String[]{command}, null, false);
        }

        public static List<String> run(List<String> commands) {
            return Shell.run("su", (String[]) commands.toArray(new String[commands.size()]), null, false);
        }

        public static List<String> run(String[] commands) {
            return Shell.run("su", commands, null, false);
        }

        public static boolean available() {
            return Shell.parseAvailableResult(run(Shell.availableTestCommands), true);
        }

        public static synchronized String version(boolean internal) {
            String str;
            int idx = 0;
            synchronized (SU.class) {
                if (!internal) {
                    idx = 1;
                }
                if (suVersion[idx] == null) {
                    String version = null;
                    List<String> ret = Shell.run(internal ? "su -V" : "su -v", new String[]{"exit"}, null, false);
                    if (ret != null) {
                        Iterator<String> it = ret.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            String line = it.next();
                            if (internal) {
                                try {
                                    if (Integer.parseInt(line) > 0) {
                                        version = line;
                                        break;
                                    }
                                } catch (NumberFormatException e) {
                                }
                            } else if (!line.trim().equals("")) {
                                version = line;
                                break;
                            }
                        }
                    }
                    suVersion[idx] = version;
                }
                str = suVersion[idx];
            }
            return str;
        }

        public static boolean isSU(String shell) {
            int pos = shell.indexOf(32);
            if (pos >= 0) {
                shell = shell.substring(0, pos);
            }
            int pos2 = shell.lastIndexOf(47);
            if (pos2 >= 0) {
                shell = shell.substring(pos2 + 1);
            }
            return shell.equals("su");
        }

        public static String shell(int uid, String context) {
            String shell = "su";
            if (context != null && isSELinuxEnforcing()) {
                String display = version(false);
                String internal = version(true);
                if (display != null && internal != null && display.endsWith("SUPERSU") && Integer.valueOf(internal).intValue() >= 190) {
                    shell = String.format(Locale.ENGLISH, "%s --context %s", shell, context);
                }
            }
            if (uid <= 0) {
                return shell;
            }
            return String.format(Locale.ENGLISH, "%s %d", shell, Integer.valueOf(uid));
        }

        public static String shellMountMaster() {
            if (Build.VERSION.SDK_INT >= 17) {
                return "su --mount-master";
            }
            return "su";
        }

        public static synchronized boolean isSELinuxEnforcing() {
            boolean booleanValue;
            boolean z;
            InputStream is;
            boolean z2;
            synchronized (SU.class) {
                if (isSELinuxEnforcing == null) {
                    boolean enforcing = null;
                    if (Build.VERSION.SDK_INT >= 17) {
                        if (new File("/sys/fs/selinux/enforce").exists()) {
                            try {
                                is = new FileInputStream("/sys/fs/selinux/enforce");
                                if (is.read() == 49) {
                                    z2 = true;
                                } else {
                                    z2 = false;
                                }
                                enforcing = Boolean.valueOf(z2);
                                is.close();
                            } catch (Exception e) {
                            } catch (Throwable th) {
                                is.close();
                                throw th;
                            }
                        }
                        if (enforcing == null) {
                            if (Build.VERSION.SDK_INT >= 19) {
                                z = true;
                            } else {
                                z = false;
                            }
                            enforcing = Boolean.valueOf(z);
                        }
                    }
                    if (enforcing == null) {
                        enforcing = false;
                    }
                    isSELinuxEnforcing = enforcing;
                }
                booleanValue = isSELinuxEnforcing.booleanValue();
            }
            return booleanValue;
        }

        public static synchronized void clearCachedResults() {
            synchronized (SU.class) {
                isSELinuxEnforcing = null;
                suVersion[0] = null;
                suVersion[1] = null;
            }
        }
    }

    private static class Command {
        private static int commandCounter = 0;
        /* access modifiers changed from: private */
        public final int code;
        /* access modifiers changed from: private */
        public final String[] commands;
        /* access modifiers changed from: private */
        public final String marker;
        /* access modifiers changed from: private */
        public final OnCommandLineListener onCommandLineListener;
        /* access modifiers changed from: private */
        public final OnCommandResultListener onCommandResultListener;

        public Command(String[] commands2, int code2, OnCommandResultListener onCommandResultListener2, OnCommandLineListener onCommandLineListener2) {
            this.commands = commands2;
            this.code = code2;
            this.onCommandResultListener = onCommandResultListener2;
            this.onCommandLineListener = onCommandLineListener2;
            StringBuilder sb = new StringBuilder(String.valueOf(UUID.randomUUID().toString()));
            int i = commandCounter + 1;
            commandCounter = i;
            this.marker = sb.append(String.format("-%08x", Integer.valueOf(i))).toString();
        }
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public boolean autoHandler = true;
        /* access modifiers changed from: private */
        public List<Command> commands = new LinkedList();
        /* access modifiers changed from: private */
        public Map<String, String> environment = new HashMap();
        /* access modifiers changed from: private */
        public Handler handler = null;
        /* access modifiers changed from: private */
        public StreamGobbler.OnLineListener onSTDERRLineListener = null;
        /* access modifiers changed from: private */
        public StreamGobbler.OnLineListener onSTDOUTLineListener = null;
        /* access modifiers changed from: private */
        public String shell = "sh";
        /* access modifiers changed from: private */
        public boolean wantSTDERR = false;
        /* access modifiers changed from: private */
        public int watchdogTimeout = 0;

        public Builder setHandler(Handler handler2) {
            this.handler = handler2;
            return this;
        }

        public Builder setAutoHandler(boolean autoHandler2) {
            this.autoHandler = autoHandler2;
            return this;
        }

        public Builder setShell(String shell2) {
            this.shell = shell2;
            return this;
        }

        public Builder useSH() {
            return setShell("sh");
        }

        public Builder useSU() {
            return setShell("su");
        }

        public Builder setWantSTDERR(boolean wantSTDERR2) {
            this.wantSTDERR = wantSTDERR2;
            return this;
        }

        public Builder addEnvironment(String key, String value) {
            this.environment.put(key, value);
            return this;
        }

        public Builder addEnvironment(Map<String, String> addEnvironment) {
            this.environment.putAll(addEnvironment);
            return this;
        }

        public Builder addCommand(String command) {
            return addCommand(command, 0, (OnCommandResultListener) null);
        }

        public Builder addCommand(String command, int code, OnCommandResultListener onCommandResultListener) {
            return addCommand(new String[]{command}, code, onCommandResultListener);
        }

        public Builder addCommand(List<String> commands2) {
            return addCommand(commands2, 0, (OnCommandResultListener) null);
        }

        public Builder addCommand(List<String> commands2, int code, OnCommandResultListener onCommandResultListener) {
            return addCommand((String[]) commands2.toArray(new String[commands2.size()]), code, onCommandResultListener);
        }

        public Builder addCommand(String[] commands2) {
            return addCommand(commands2, 0, (OnCommandResultListener) null);
        }

        public Builder addCommand(String[] commands2, int code, OnCommandResultListener onCommandResultListener) {
            this.commands.add(new Command(commands2, code, onCommandResultListener, null));
            return this;
        }

        public Builder setOnSTDOUTLineListener(StreamGobbler.OnLineListener onLineListener) {
            this.onSTDOUTLineListener = onLineListener;
            return this;
        }

        public Builder setOnSTDERRLineListener(StreamGobbler.OnLineListener onLineListener) {
            this.onSTDERRLineListener = onLineListener;
            return this;
        }

        public Builder setWatchdogTimeout(int watchdogTimeout2) {
            this.watchdogTimeout = watchdogTimeout2;
            return this;
        }

        public Builder setMinimalLogging(boolean useMinimal) {
            Debug.setLogTypeEnabled(6, !useMinimal);
            return this;
        }

        public Interactive open() {
            return new Interactive(this, null, null);
        }

        public Interactive open(OnCommandResultListener onCommandResultListener) {
            return new Interactive(this, onCommandResultListener, null);
        }
    }

    public static class Interactive {
        private StreamGobbler STDERR;
        private DataOutputStream STDIN;
        private StreamGobbler STDOUT;
        private final boolean autoHandler;
        private volatile List<String> buffer;
        private final Object callbackSync;
        private volatile int callbacks;
        private volatile boolean closed;
        /* access modifiers changed from: private */
        public volatile Command command;
        private final List<Command> commands;
        private final Map<String, String> environment;
        private final Handler handler;
        private volatile boolean idle;
        private final Object idleSync;
        /* access modifiers changed from: private */
        public volatile int lastExitCode;
        /* access modifiers changed from: private */
        public volatile String lastMarkerSTDERR;
        /* access modifiers changed from: private */
        public volatile String lastMarkerSTDOUT;
        /* access modifiers changed from: private */
        public final StreamGobbler.OnLineListener onSTDERRLineListener;
        /* access modifiers changed from: private */
        public final StreamGobbler.OnLineListener onSTDOUTLineListener;
        private Process process;
        private volatile boolean running;
        /* access modifiers changed from: private */
        public final String shell;
        /* access modifiers changed from: private */
        public final boolean wantSTDERR;
        private ScheduledThreadPoolExecutor watchdog;
        private volatile int watchdogCount;
        /* access modifiers changed from: private */
        public int watchdogTimeout;

        private Interactive(final Builder builder, final OnCommandResultListener onCommandResultListener) {
            this.process = null;
            this.STDIN = null;
            this.STDOUT = null;
            this.STDERR = null;
            this.watchdog = null;
            this.running = false;
            this.idle = true;
            this.closed = true;
            this.callbacks = 0;
            this.idleSync = new Object();
            this.callbackSync = new Object();
            this.lastExitCode = 0;
            this.lastMarkerSTDOUT = null;
            this.lastMarkerSTDERR = null;
            this.command = null;
            this.buffer = null;
            this.autoHandler = builder.autoHandler;
            this.shell = builder.shell;
            this.wantSTDERR = builder.wantSTDERR;
            this.commands = builder.commands;
            this.environment = builder.environment;
            this.onSTDOUTLineListener = builder.onSTDOUTLineListener;
            this.onSTDERRLineListener = builder.onSTDERRLineListener;
            this.watchdogTimeout = builder.watchdogTimeout;
            if (Looper.myLooper() == null || builder.handler != null || !this.autoHandler) {
                this.handler = builder.handler;
            } else {
                this.handler = new Handler();
            }
            if (onCommandResultListener != null) {
                this.watchdogTimeout = 60;
                this.commands.add(0, new Command(Shell.availableTestCommands, 0, new OnCommandResultListener() {
                    public void onCommandResult(int commandCode, int exitCode, List<String> output) {
                        if (exitCode == 0 && !Shell.parseAvailableResult(output, SU.isSU(Interactive.this.shell))) {
                            exitCode = -4;
                        }
                        Interactive.this.watchdogTimeout = builder.watchdogTimeout;
                        onCommandResultListener.onCommandResult(0, exitCode, output);
                    }
                }, null));
            }
            if (!open() && onCommandResultListener != null) {
                onCommandResultListener.onCommandResult(0, -3, null);
            }
        }

        /* synthetic */ Interactive(Builder builder, OnCommandResultListener onCommandResultListener, Interactive interactive) {
            this(builder, onCommandResultListener);
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            if (this.closed || !Debug.getSanityChecksEnabledEffective()) {
                super.finalize();
            } else {
                Debug.log(ShellNotClosedException.EXCEPTION_NOT_CLOSED);
                throw new ShellNotClosedException();
            }
        }

        public void addCommand(String command2) {
            addCommand(command2, 0, (OnCommandResultListener) null);
        }

        public void addCommand(String command2, int code, OnCommandResultListener onCommandResultListener) {
            addCommand(new String[]{command2}, code, onCommandResultListener);
        }

        public void addCommand(String command2, int code, OnCommandLineListener onCommandLineListener) {
            addCommand(new String[]{command2}, code, onCommandLineListener);
        }

        public void addCommand(List<String> commands2) {
            addCommand(commands2, 0, (OnCommandResultListener) null);
        }

        public void addCommand(List<String> commands2, int code, OnCommandResultListener onCommandResultListener) {
            addCommand((String[]) commands2.toArray(new String[commands2.size()]), code, onCommandResultListener);
        }

        public void addCommand(List<String> commands2, int code, OnCommandLineListener onCommandLineListener) {
            addCommand((String[]) commands2.toArray(new String[commands2.size()]), code, onCommandLineListener);
        }

        public void addCommand(String[] commands2) {
            addCommand(commands2, 0, (OnCommandResultListener) null);
        }

        public synchronized void addCommand(String[] commands2, int code, OnCommandResultListener onCommandResultListener) {
            this.commands.add(new Command(commands2, code, onCommandResultListener, null));
            runNextCommand();
        }

        public synchronized void addCommand(String[] commands2, int code, OnCommandLineListener onCommandLineListener) {
            this.commands.add(new Command(commands2, code, null, onCommandLineListener));
            runNextCommand();
        }

        private void runNextCommand() {
            runNextCommand(true);
        }

        /* access modifiers changed from: private */
        public synchronized void handleWatchdog() {
            int exitCode;
            if (this.watchdog != null) {
                if (this.watchdogTimeout != 0) {
                    if (!isRunning()) {
                        exitCode = -2;
                        Debug.log(String.format("[%s%%] SHELL_DIED", this.shell.toUpperCase(Locale.ENGLISH)));
                    } else {
                        int i = this.watchdogCount;
                        this.watchdogCount = i + 1;
                        if (i >= this.watchdogTimeout) {
                            exitCode = -1;
                            Debug.log(String.format("[%s%%] WATCHDOG_EXIT", this.shell.toUpperCase(Locale.ENGLISH)));
                        }
                    }
                    postCallback(this.command, exitCode, this.buffer);
                    this.command = null;
                    this.buffer = null;
                    this.idle = true;
                    this.watchdog.shutdown();
                    this.watchdog = null;
                    kill();
                }
            }
        }

        private void startWatchdog() {
            if (this.watchdogTimeout != 0) {
                this.watchdogCount = 0;
                this.watchdog = new ScheduledThreadPoolExecutor(1);
                this.watchdog.scheduleAtFixedRate(new Runnable() {
                    public void run() {
                        Interactive.this.handleWatchdog();
                    }
                }, 1, 1, TimeUnit.SECONDS);
            }
        }

        private void stopWatchdog() {
            if (this.watchdog != null) {
                this.watchdog.shutdownNow();
                this.watchdog = null;
            }
        }

        private void runNextCommand(boolean notifyIdle) {
            boolean running2 = isRunning();
            if (!running2) {
                this.idle = true;
            }
            if (running2 && this.idle && this.commands.size() > 0) {
                Command command2 = this.commands.get(0);
                this.commands.remove(0);
                this.buffer = null;
                this.lastExitCode = 0;
                this.lastMarkerSTDOUT = null;
                this.lastMarkerSTDERR = null;
                if (command2.commands.length > 0) {
                    try {
                        if (command2.onCommandResultListener != null) {
                            this.buffer = Collections.synchronizedList(new ArrayList());
                        }
                        this.idle = false;
                        this.command = command2;
                        startWatchdog();
                        for (String write : command2.commands) {
                            Debug.logCommand(String.format("[%s+] %s", this.shell.toUpperCase(Locale.ENGLISH), write));
                            this.STDIN.write((String.valueOf(write) + "\n").getBytes("UTF-8"));
                        }
                        this.STDIN.write(("echo " + command2.marker + " $?\n").getBytes("UTF-8"));
                        this.STDIN.write(("echo " + command2.marker + " >&2\n").getBytes("UTF-8"));
                        this.STDIN.flush();
                    } catch (IOException e) {
                    }
                } else {
                    runNextCommand(false);
                }
            } else if (!running2) {
                while (this.commands.size() > 0) {
                    postCallback(this.commands.remove(0), -2, null);
                }
            }
            if (this.idle && notifyIdle) {
                synchronized (this.idleSync) {
                    this.idleSync.notifyAll();
                }
            }
        }

        /* access modifiers changed from: private */
        public synchronized void processMarker() {
            if (this.command.marker.equals(this.lastMarkerSTDOUT) && this.command.marker.equals(this.lastMarkerSTDERR)) {
                postCallback(this.command, this.lastExitCode, this.buffer);
                stopWatchdog();
                this.command = null;
                this.buffer = null;
                this.idle = true;
                runNextCommand();
            }
        }

        /* access modifiers changed from: private */
        public synchronized void processLine(String line, StreamGobbler.OnLineListener listener) {
            if (listener != null) {
                if (this.handler != null) {
                    final String fLine = line;
                    final StreamGobbler.OnLineListener fListener = listener;
                    startCallback();
                    this.handler.post(new Runnable() {
                        public void run() {
                            try {
                                fListener.onLine(fLine);
                            } finally {
                                Interactive.this.endCallback();
                            }
                        }
                    });
                } else {
                    listener.onLine(line);
                }
            }
        }

        /* access modifiers changed from: private */
        public synchronized void addBuffer(String line) {
            if (this.buffer != null) {
                this.buffer.add(line);
            }
        }

        private void startCallback() {
            synchronized (this.callbackSync) {
                this.callbacks++;
            }
        }

        private void postCallback(final Command fCommand, final int fExitCode, final List<String> fOutput) {
            if (fCommand.onCommandResultListener != null || fCommand.onCommandLineListener != null) {
                if (this.handler == null) {
                    if (!(fCommand.onCommandResultListener == null || fOutput == null)) {
                        fCommand.onCommandResultListener.onCommandResult(fCommand.code, fExitCode, fOutput);
                    }
                    if (fCommand.onCommandLineListener != null) {
                        fCommand.onCommandLineListener.onCommandResult(fCommand.code, fExitCode);
                        return;
                    }
                    return;
                }
                startCallback();
                this.handler.post(new Runnable() {
                    public void run() {
                        try {
                            if (!(fCommand.onCommandResultListener == null || fOutput == null)) {
                                fCommand.onCommandResultListener.onCommandResult(fCommand.code, fExitCode, fOutput);
                            }
                            if (fCommand.onCommandLineListener != null) {
                                fCommand.onCommandLineListener.onCommandResult(fCommand.code, fExitCode);
                            }
                        } finally {
                            Interactive.this.endCallback();
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public void endCallback() {
            synchronized (this.callbackSync) {
                this.callbacks--;
                if (this.callbacks == 0) {
                    this.callbackSync.notifyAll();
                }
            }
        }

        private synchronized boolean open() {
            boolean z;
            Debug.log(String.format("[%s%%] START", this.shell.toUpperCase(Locale.ENGLISH)));
            try {
                if (this.environment.size() == 0) {
                    this.process = Runtime.getRuntime().exec(this.shell);
                } else {
                    Map<String, String> newEnvironment = new HashMap<>();
                    newEnvironment.putAll(System.getenv());
                    newEnvironment.putAll(this.environment);
                    int i = 0;
                    String[] env = new String[newEnvironment.size()];
                    for (Map.Entry<String, String> entry : newEnvironment.entrySet()) {
                        env[i] = String.valueOf((String) entry.getKey()) + "=" + ((String) entry.getValue());
                        i++;
                    }
                    this.process = Runtime.getRuntime().exec(this.shell, env);
                }
                this.STDIN = new DataOutputStream(this.process.getOutputStream());
                this.STDOUT = new StreamGobbler(String.valueOf(this.shell.toUpperCase(Locale.ENGLISH)) + "-", this.process.getInputStream(), new StreamGobbler.OnLineListener() {
                    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
                        return;
                     */
                    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void onLine(java.lang.String r9) {
                        /*
                            r8 = this;
                            eu.chainfire.libsuperuser.Shell$Interactive r5 = eu.chainfire.libsuperuser.Shell.Interactive.this
                            monitor-enter(r5)
                            eu.chainfire.libsuperuser.Shell$Interactive r4 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.Shell$Command r4 = r4.command     // Catch:{ all -> 0x007d }
                            if (r4 != 0) goto L_0x000d
                            monitor-exit(r5)     // Catch:{ all -> 0x007d }
                        L_0x000c:
                            return
                        L_0x000d:
                            r0 = r9
                            r3 = 0
                            eu.chainfire.libsuperuser.Shell$Interactive r4 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.Shell$Command r4 = r4.command     // Catch:{ all -> 0x007d }
                            java.lang.String r4 = r4.marker     // Catch:{ all -> 0x007d }
                            int r2 = r9.indexOf(r4)     // Catch:{ all -> 0x007d }
                            if (r2 != 0) goto L_0x0080
                            r0 = 0
                            r3 = r9
                        L_0x0021:
                            if (r0 == 0) goto L_0x0042
                            eu.chainfire.libsuperuser.Shell$Interactive r4 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x007d }
                            r4.addBuffer(r0)     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.Shell$Interactive r4 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.Shell$Interactive r6 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.StreamGobbler$OnLineListener r6 = r6.onSTDOUTLineListener     // Catch:{ all -> 0x007d }
                            r4.processLine(r0, r6)     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.Shell$Interactive r4 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.Shell$Interactive r6 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.Shell$Command r6 = r6.command     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.Shell$OnCommandLineListener r6 = r6.onCommandLineListener     // Catch:{ all -> 0x007d }
                            r4.processLine(r0, r6)     // Catch:{ all -> 0x007d }
                        L_0x0042:
                            if (r3 == 0) goto L_0x007b
                            eu.chainfire.libsuperuser.Shell$Interactive r4 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ Exception -> 0x008c }
                            eu.chainfire.libsuperuser.Shell$Interactive r6 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ Exception -> 0x008c }
                            eu.chainfire.libsuperuser.Shell$Command r6 = r6.command     // Catch:{ Exception -> 0x008c }
                            java.lang.String r6 = r6.marker     // Catch:{ Exception -> 0x008c }
                            int r6 = r6.length()     // Catch:{ Exception -> 0x008c }
                            int r6 = r6 + 1
                            java.lang.String r6 = r3.substring(r6)     // Catch:{ Exception -> 0x008c }
                            r7 = 10
                            java.lang.Integer r6 = java.lang.Integer.valueOf(r6, r7)     // Catch:{ Exception -> 0x008c }
                            int r6 = r6.intValue()     // Catch:{ Exception -> 0x008c }
                            r4.lastExitCode = r6     // Catch:{ Exception -> 0x008c }
                        L_0x0067:
                            eu.chainfire.libsuperuser.Shell$Interactive r4 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.Shell$Interactive r6 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.Shell$Command r6 = r6.command     // Catch:{ all -> 0x007d }
                            java.lang.String r6 = r6.marker     // Catch:{ all -> 0x007d }
                            r4.lastMarkerSTDOUT = r6     // Catch:{ all -> 0x007d }
                            eu.chainfire.libsuperuser.Shell$Interactive r4 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x007d }
                            r4.processMarker()     // Catch:{ all -> 0x007d }
                        L_0x007b:
                            monitor-exit(r5)     // Catch:{ all -> 0x007d }
                            goto L_0x000c
                        L_0x007d:
                            r4 = move-exception
                            monitor-exit(r5)     // Catch:{ all -> 0x007d }
                            throw r4
                        L_0x0080:
                            if (r2 <= 0) goto L_0x0021
                            r4 = 0
                            java.lang.String r0 = r9.substring(r4, r2)     // Catch:{ all -> 0x007d }
                            java.lang.String r3 = r9.substring(r2)     // Catch:{ all -> 0x007d }
                            goto L_0x0021
                        L_0x008c:
                            r1 = move-exception
                            r1.printStackTrace()     // Catch:{ all -> 0x007d }
                            goto L_0x0067
                        */
                        throw new UnsupportedOperationException("Method not decompiled: eu.chainfire.libsuperuser.Shell.Interactive.AnonymousClass5.onLine(java.lang.String):void");
                    }
                });
                this.STDERR = new StreamGobbler(String.valueOf(this.shell.toUpperCase(Locale.ENGLISH)) + "*", this.process.getErrorStream(), new StreamGobbler.OnLineListener() {
                    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
                        return;
                     */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void onLine(java.lang.String r6) {
                        /*
                            r5 = this;
                            eu.chainfire.libsuperuser.Shell$Interactive r3 = eu.chainfire.libsuperuser.Shell.Interactive.this
                            monitor-enter(r3)
                            eu.chainfire.libsuperuser.Shell$Interactive r2 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x0051 }
                            eu.chainfire.libsuperuser.Shell$Command r2 = r2.command     // Catch:{ all -> 0x0051 }
                            if (r2 != 0) goto L_0x000d
                            monitor-exit(r3)     // Catch:{ all -> 0x0051 }
                        L_0x000c:
                            return
                        L_0x000d:
                            r0 = r6
                            eu.chainfire.libsuperuser.Shell$Interactive r2 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x0051 }
                            eu.chainfire.libsuperuser.Shell$Command r2 = r2.command     // Catch:{ all -> 0x0051 }
                            java.lang.String r2 = r2.marker     // Catch:{ all -> 0x0051 }
                            int r1 = r6.indexOf(r2)     // Catch:{ all -> 0x0051 }
                            if (r1 != 0) goto L_0x0054
                            r0 = 0
                        L_0x001f:
                            if (r0 == 0) goto L_0x0039
                            eu.chainfire.libsuperuser.Shell$Interactive r2 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x0051 }
                            boolean r2 = r2.wantSTDERR     // Catch:{ all -> 0x0051 }
                            if (r2 == 0) goto L_0x002e
                            eu.chainfire.libsuperuser.Shell$Interactive r2 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x0051 }
                            r2.addBuffer(r0)     // Catch:{ all -> 0x0051 }
                        L_0x002e:
                            eu.chainfire.libsuperuser.Shell$Interactive r2 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x0051 }
                            eu.chainfire.libsuperuser.Shell$Interactive r4 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x0051 }
                            eu.chainfire.libsuperuser.StreamGobbler$OnLineListener r4 = r4.onSTDERRLineListener     // Catch:{ all -> 0x0051 }
                            r2.processLine(r0, r4)     // Catch:{ all -> 0x0051 }
                        L_0x0039:
                            if (r1 < 0) goto L_0x004f
                            eu.chainfire.libsuperuser.Shell$Interactive r2 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x0051 }
                            eu.chainfire.libsuperuser.Shell$Interactive r4 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x0051 }
                            eu.chainfire.libsuperuser.Shell$Command r4 = r4.command     // Catch:{ all -> 0x0051 }
                            java.lang.String r4 = r4.marker     // Catch:{ all -> 0x0051 }
                            r2.lastMarkerSTDERR = r4     // Catch:{ all -> 0x0051 }
                            eu.chainfire.libsuperuser.Shell$Interactive r2 = eu.chainfire.libsuperuser.Shell.Interactive.this     // Catch:{ all -> 0x0051 }
                            r2.processMarker()     // Catch:{ all -> 0x0051 }
                        L_0x004f:
                            monitor-exit(r3)     // Catch:{ all -> 0x0051 }
                            goto L_0x000c
                        L_0x0051:
                            r2 = move-exception
                            monitor-exit(r3)     // Catch:{ all -> 0x0051 }
                            throw r2
                        L_0x0054:
                            if (r1 <= 0) goto L_0x001f
                            r2 = 0
                            java.lang.String r0 = r6.substring(r2, r1)     // Catch:{ all -> 0x0051 }
                            goto L_0x001f
                        */
                        throw new UnsupportedOperationException("Method not decompiled: eu.chainfire.libsuperuser.Shell.Interactive.AnonymousClass6.onLine(java.lang.String):void");
                    }
                });
                this.STDOUT.start();
                this.STDERR.start();
                this.running = true;
                this.closed = false;
                runNextCommand();
                z = true;
            } catch (IOException e) {
                z = false;
            }
            return z;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
            if (eu.chainfire.libsuperuser.Debug.getSanityChecksEnabledEffective() == false) goto L_0x0032;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
            if (eu.chainfire.libsuperuser.Debug.onMainThread() == false) goto L_0x0032;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0022, code lost:
            eu.chainfire.libsuperuser.Debug.log(eu.chainfire.libsuperuser.ShellOnMainThreadException.EXCEPTION_NOT_IDLE);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
            throw new eu.chainfire.libsuperuser.ShellOnMainThreadException(eu.chainfire.libsuperuser.ShellOnMainThreadException.EXCEPTION_NOT_IDLE);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0032, code lost:
            if (r0 != false) goto L_0x0037;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0034, code lost:
            waitForIdle();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
            r7.STDIN.write("exit\n".getBytes("UTF-8"));
            r7.STDIN.flush();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
            r7.process.waitFor();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
            r7.STDIN.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x007b, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0086, code lost:
            if (r1.getMessage().contains("EPIPE") == false) goto L_0x0088;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0088, code lost:
            throw r1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0014, code lost:
            if (r0 != false) goto L_0x0032;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x008b A[ExcHandler: InterruptedException (e java.lang.InterruptedException), Splitter:B:21:0x0037] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() {
            /*
                r7 = this;
                r5 = 1
                r6 = 0
                boolean r0 = r7.isIdle()
                monitor-enter(r7)
                boolean r2 = r7.running     // Catch:{ all -> 0x002f }
                if (r2 != 0) goto L_0x000d
                monitor-exit(r7)     // Catch:{ all -> 0x002f }
            L_0x000c:
                return
            L_0x000d:
                r2 = 0
                r7.running = r2     // Catch:{ all -> 0x002f }
                r2 = 1
                r7.closed = r2     // Catch:{ all -> 0x002f }
                monitor-exit(r7)     // Catch:{ all -> 0x002f }
                if (r0 != 0) goto L_0x0032
                boolean r2 = eu.chainfire.libsuperuser.Debug.getSanityChecksEnabledEffective()
                if (r2 == 0) goto L_0x0032
                boolean r2 = eu.chainfire.libsuperuser.Debug.onMainThread()
                if (r2 == 0) goto L_0x0032
                java.lang.String r2 = "Application attempted to wait for a non-idle shell to close on the main thread"
                eu.chainfire.libsuperuser.Debug.log(r2)
                eu.chainfire.libsuperuser.ShellOnMainThreadException r2 = new eu.chainfire.libsuperuser.ShellOnMainThreadException
                java.lang.String r3 = "Application attempted to wait for a non-idle shell to close on the main thread"
                r2.<init>(r3)
                throw r2
            L_0x002f:
                r2 = move-exception
                monitor-exit(r7)     // Catch:{ all -> 0x002f }
                throw r2
            L_0x0032:
                if (r0 != 0) goto L_0x0037
                r7.waitForIdle()
            L_0x0037:
                java.io.DataOutputStream r2 = r7.STDIN     // Catch:{ IOException -> 0x007b, InterruptedException -> 0x008b }
                java.lang.String r3 = "exit\n"
                java.lang.String r4 = "UTF-8"
                byte[] r3 = r3.getBytes(r4)     // Catch:{ IOException -> 0x007b, InterruptedException -> 0x008b }
                r2.write(r3)     // Catch:{ IOException -> 0x007b, InterruptedException -> 0x008b }
                java.io.DataOutputStream r2 = r7.STDIN     // Catch:{ IOException -> 0x007b, InterruptedException -> 0x008b }
                r2.flush()     // Catch:{ IOException -> 0x007b, InterruptedException -> 0x008b }
            L_0x0049:
                java.lang.Process r2 = r7.process     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
                r2.waitFor()     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
                java.io.DataOutputStream r2 = r7.STDIN     // Catch:{ IOException -> 0x008d, InterruptedException -> 0x008b }
                r2.close()     // Catch:{ IOException -> 0x008d, InterruptedException -> 0x008b }
            L_0x0053:
                eu.chainfire.libsuperuser.StreamGobbler r2 = r7.STDOUT     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
                r2.join()     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
                eu.chainfire.libsuperuser.StreamGobbler r2 = r7.STDERR     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
                r2.join()     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
                r7.stopWatchdog()     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
                java.lang.Process r2 = r7.process     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
                r2.destroy()     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
            L_0x0065:
                java.lang.String r2 = "[%s%%] END"
                java.lang.Object[] r3 = new java.lang.Object[r5]
                java.lang.String r4 = r7.shell
                java.util.Locale r5 = java.util.Locale.ENGLISH
                java.lang.String r4 = r4.toUpperCase(r5)
                r3[r6] = r4
                java.lang.String r2 = java.lang.String.format(r2, r3)
                eu.chainfire.libsuperuser.Debug.log(r2)
                goto L_0x000c
            L_0x007b:
                r1 = move-exception
                java.lang.String r2 = r1.getMessage()     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
                java.lang.String r3 = "EPIPE"
                boolean r2 = r2.contains(r3)     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
                if (r2 != 0) goto L_0x0049
                throw r1     // Catch:{ IOException -> 0x0089, InterruptedException -> 0x008b }
            L_0x0089:
                r2 = move-exception
                goto L_0x0065
            L_0x008b:
                r2 = move-exception
                goto L_0x0065
            L_0x008d:
                r2 = move-exception
                goto L_0x0053
            */
            throw new UnsupportedOperationException("Method not decompiled: eu.chainfire.libsuperuser.Shell.Interactive.close():void");
        }

        public synchronized void kill() {
            this.running = false;
            this.closed = true;
            try {
                this.STDIN.close();
            } catch (IOException e) {
            }
            try {
                this.process.destroy();
            } catch (Exception e2) {
            }
            this.idle = true;
            synchronized (this.idleSync) {
                this.idleSync.notifyAll();
            }
        }

        public boolean isRunning() {
            if (this.process == null) {
                return false;
            }
            try {
                this.process.exitValue();
                return false;
            } catch (IllegalThreadStateException e) {
                return true;
            }
        }

        public synchronized boolean isIdle() {
            if (!isRunning()) {
                this.idle = true;
                synchronized (this.idleSync) {
                    this.idleSync.notifyAll();
                }
            }
            return this.idle;
        }

        public boolean waitForIdle() {
            if (!Debug.getSanityChecksEnabledEffective() || !Debug.onMainThread()) {
                if (isRunning()) {
                    synchronized (this.idleSync) {
                        while (!this.idle) {
                            try {
                                this.idleSync.wait();
                            } catch (InterruptedException e) {
                                return false;
                            }
                        }
                    }
                    if (!(this.handler == null || this.handler.getLooper() == null || this.handler.getLooper() == Looper.myLooper())) {
                        synchronized (this.callbackSync) {
                            while (this.callbacks > 0) {
                                try {
                                    this.callbackSync.wait();
                                } catch (InterruptedException e2) {
                                    return false;
                                }
                            }
                        }
                    }
                }
                return true;
            }
            Debug.log(ShellOnMainThreadException.EXCEPTION_WAIT_IDLE);
            throw new ShellOnMainThreadException(ShellOnMainThreadException.EXCEPTION_WAIT_IDLE);
        }

        public boolean hasHandler() {
            return this.handler != null;
        }
    }
}
