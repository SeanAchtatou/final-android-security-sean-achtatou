package eu.chainfire.libsuperuser;

import android.os.Build;
import eu.chainfire.libsuperuser.Shell;
import java.util.List;
import java.util.Locale;

public class Toolbox {
    private static final int TOYBOX_SDK = 23;
    private static final Object synchronizer = new Object();
    private static volatile String toybox = null;

    public static void init() {
        if (toybox == null) {
            if (Build.VERSION.SDK_INT < 23) {
                toybox = "";
            } else if (!Debug.getSanityChecksEnabledEffective() || !Debug.onMainThread()) {
                synchronized (synchronizer) {
                    toybox = "";
                    List<String> output = Shell.SH.run("toybox");
                    if (output != null) {
                        toybox = " ";
                        for (String line : output) {
                            toybox = String.valueOf(toybox) + line.trim() + " ";
                        }
                    }
                }
            } else {
                Debug.log(ShellOnMainThreadException.EXCEPTION_TOOLBOX);
                throw new ShellOnMainThreadException(ShellOnMainThreadException.EXCEPTION_TOOLBOX);
            }
        }
    }

    public static String command(String format, Object... args) {
        String applet;
        if (Build.VERSION.SDK_INT < 23) {
            return String.format(Locale.ENGLISH, "toolbox " + format, args);
        }
        if (toybox == null) {
            init();
        }
        String format2 = format.trim();
        int p = format2.indexOf(32);
        if (p >= 0) {
            applet = format2.substring(0, p);
        } else {
            applet = format2;
        }
        if (toybox.contains(" " + applet + " ")) {
            return String.format(Locale.ENGLISH, "toybox " + format2, args);
        }
        return String.format(Locale.ENGLISH, "toolbox " + format2, args);
    }
}
