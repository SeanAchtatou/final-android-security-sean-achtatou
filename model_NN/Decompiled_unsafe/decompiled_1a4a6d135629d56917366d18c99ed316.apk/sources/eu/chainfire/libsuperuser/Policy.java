package eu.chainfire.libsuperuser;

import eu.chainfire.libsuperuser.Shell;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class Policy {
    private static final int MAX_POLICY_LENGTH = 4064;
    private static volatile Boolean canInject = null;
    private static volatile boolean injected = false;
    private static final Object synchronizer = new Object();

    /* access modifiers changed from: protected */
    public abstract String[] getPolicies();

    public static boolean haveInjected() {
        return injected;
    }

    public static void resetInjected() {
        synchronized (synchronizer) {
            injected = false;
        }
    }

    public static boolean canInject() {
        boolean booleanValue;
        synchronized (synchronizer) {
            if (canInject != null) {
                booleanValue = canInject.booleanValue();
            } else {
                canInject = false;
                List<String> result = Shell.run("sh", new String[]{"supolicy"}, null, false);
                if (result != null) {
                    Iterator<String> it = result.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (it.next().contains("supolicy")) {
                                canInject = true;
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                booleanValue = canInject.booleanValue();
            }
        }
        return booleanValue;
    }

    public static void resetCanInject() {
        synchronized (synchronizer) {
            canInject = null;
        }
    }

    /* access modifiers changed from: protected */
    public List<String> getInjectCommands() {
        return getInjectCommands(true);
    }

    /* access modifiers changed from: protected */
    public List<String> getInjectCommands(boolean allowBlocking) {
        List<String> commands = null;
        synchronized (synchronizer) {
            if (Shell.SU.isSELinuxEnforcing()) {
                if (!allowBlocking || canInject()) {
                    if (!injected) {
                        String[] policies = getPolicies();
                        if (policies != null && policies.length > 0) {
                            commands = new ArrayList<>();
                            String command = "";
                            for (String policy : policies) {
                                if (command.length() == 0 || command.length() + policy.length() + 3 < MAX_POLICY_LENGTH) {
                                    command = String.valueOf(command) + " \"" + policy + "\"";
                                } else {
                                    commands.add("supolicy --live" + command);
                                    command = "";
                                }
                            }
                            if (command.length() > 0) {
                                commands.add("supolicy --live" + command);
                            }
                        }
                    }
                }
            }
        }
        return commands;
    }

    public void inject() {
        synchronized (synchronizer) {
            List<String> commands = getInjectCommands();
            if (commands != null && commands.size() > 0) {
                Shell.SU.run(commands);
            }
            injected = true;
        }
    }

    public void inject(Shell.Interactive shell, boolean waitForIdle) {
        synchronized (synchronizer) {
            List<String> commands = getInjectCommands(waitForIdle);
            if (commands != null && commands.size() > 0) {
                shell.addCommand(commands);
                if (waitForIdle) {
                    shell.waitForIdle();
                }
            }
            injected = true;
        }
    }
}
