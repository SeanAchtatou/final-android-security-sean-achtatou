package com.google.i18n.phonenumbers;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonemetadata;
import com.google.i18n.phonenumbers.Phonenumber;
import java.lang.Character;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class PhoneNumberMatcher implements Iterator<PhoneNumberMatch> {
    private static final Pattern[] INNER_MATCHES = {Pattern.compile("/+(.*)"), Pattern.compile("(\\([^(]*)"), Pattern.compile("(?:\\p{Z}-|-\\p{Z})\\p{Z}*(.+)"), Pattern.compile("[‒-―－]\\p{Z}*(.+)"), Pattern.compile("\\.+\\p{Z}*([^.]+)"), Pattern.compile("\\p{Z}+(\\P{Z}+)")};
    private static final Pattern LEAD_CLASS;
    private static final Pattern MATCHING_BRACKETS;
    private static final Pattern PATTERN;
    private static final Pattern PUB_PAGES = Pattern.compile("\\d{1,5}-+\\d{1,5}\\s{0,4}\\(\\d{1,4}");
    private static final Pattern SLASH_SEPARATED_DATES = Pattern.compile("(?:(?:[0-3]?\\d/[01]?\\d)|(?:[01]?\\d/[0-3]?\\d))/(?:[12]\\d)?\\d{2}");
    private static final Pattern TIME_STAMPS = Pattern.compile("[12]\\d{3}[-/]?[01]\\d[-/]?[0-3]\\d +[0-2]\\d$");
    private static final Pattern TIME_STAMPS_SUFFIX = Pattern.compile(":[0-5]\\d");
    private PhoneNumberMatch lastMatch = null;
    private final PhoneNumberUtil.Leniency leniency;
    private long maxTries;
    private final PhoneNumberUtil phoneUtil;
    private final String preferredRegion;
    private int searchIndex = 0;
    private State state = State.NOT_READY;
    private final CharSequence text;

    interface NumberGroupingChecker {
        boolean checkGroups(PhoneNumberUtil phoneNumberUtil, Phonenumber.PhoneNumber phoneNumber, StringBuilder sb, String[] strArr);
    }

    private enum State {
        NOT_READY,
        READY,
        DONE
    }

    static {
        String valueOf = String.valueOf(String.valueOf("(\\[（［"));
        String valueOf2 = String.valueOf(String.valueOf(")\\]）］"));
        String nonParens = new StringBuilder(valueOf.length() + 3 + valueOf2.length()).append("[^").append(valueOf).append(valueOf2).append("]").toString();
        String bracketPairLimit = limit(0, 3);
        String valueOf3 = String.valueOf(String.valueOf("(\\[（［"));
        String valueOf4 = String.valueOf(String.valueOf(nonParens));
        String valueOf5 = String.valueOf(String.valueOf(")\\]）］"));
        String valueOf6 = String.valueOf(String.valueOf(nonParens));
        String valueOf7 = String.valueOf(String.valueOf("(\\[（［"));
        String valueOf8 = String.valueOf(String.valueOf(nonParens));
        String valueOf9 = String.valueOf(String.valueOf(")\\]）］"));
        String valueOf10 = String.valueOf(String.valueOf(bracketPairLimit));
        String valueOf11 = String.valueOf(String.valueOf(nonParens));
        MATCHING_BRACKETS = Pattern.compile(new StringBuilder(valueOf3.length() + 26 + valueOf4.length() + valueOf5.length() + valueOf6.length() + valueOf7.length() + valueOf8.length() + valueOf9.length() + valueOf10.length() + valueOf11.length()).append("(?:[").append(valueOf3).append("])?").append("(?:").append(valueOf4).append("+").append("[").append(valueOf5).append("])?").append(valueOf6).append("+").append("(?:[").append(valueOf7).append("]").append(valueOf8).append("+[").append(valueOf9).append("])").append(valueOf10).append(valueOf11).append("*").toString());
        String leadLimit = limit(0, 2);
        String punctuationLimit = limit(0, 4);
        String blockLimit = limit(0, 20);
        String valueOf12 = String.valueOf("[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～]");
        String valueOf13 = String.valueOf(punctuationLimit);
        String punctuation = valueOf13.length() != 0 ? valueOf12.concat(valueOf13) : new String(valueOf12);
        String valueOf14 = String.valueOf(limit(1, 20));
        String digitSequence = valueOf14.length() != 0 ? "\\p{Nd}".concat(valueOf14) : new String("\\p{Nd}");
        String valueOf15 = String.valueOf("(\\[（［");
        String valueOf16 = String.valueOf("+＋");
        String valueOf17 = String.valueOf(String.valueOf(valueOf16.length() != 0 ? valueOf15.concat(valueOf16) : new String(valueOf15)));
        String leadClass = new StringBuilder(valueOf17.length() + 2).append("[").append(valueOf17).append("]").toString();
        LEAD_CLASS = Pattern.compile(leadClass);
        String valueOf18 = String.valueOf(String.valueOf(leadClass));
        String valueOf19 = String.valueOf(String.valueOf(punctuation));
        String valueOf20 = String.valueOf(String.valueOf(leadLimit));
        String valueOf21 = String.valueOf(String.valueOf(digitSequence));
        String valueOf22 = String.valueOf(String.valueOf(punctuation));
        String valueOf23 = String.valueOf(String.valueOf(digitSequence));
        String valueOf24 = String.valueOf(String.valueOf(blockLimit));
        String valueOf25 = String.valueOf(String.valueOf(PhoneNumberUtil.EXTN_PATTERNS_FOR_MATCHING));
        PATTERN = Pattern.compile(new StringBuilder(valueOf18.length() + 13 + valueOf19.length() + valueOf20.length() + valueOf21.length() + valueOf22.length() + valueOf23.length() + valueOf24.length() + valueOf25.length()).append("(?:").append(valueOf18).append(valueOf19).append(")").append(valueOf20).append(valueOf21).append("(?:").append(valueOf22).append(valueOf23).append(")").append(valueOf24).append("(?:").append(valueOf25).append(")?").toString(), 66);
    }

    private static String limit(int lower, int upper) {
        if (lower >= 0 && upper > 0 && upper >= lower) {
            return new StringBuilder(25).append("{").append(lower).append(",").append(upper).append("}").toString();
        }
        throw new IllegalArgumentException();
    }

    PhoneNumberMatcher(PhoneNumberUtil util, String text2, String country, PhoneNumberUtil.Leniency leniency2, long maxTries2) {
        if (util == null || leniency2 == null) {
            throw new NullPointerException();
        } else if (maxTries2 < 0) {
            throw new IllegalArgumentException();
        } else {
            this.phoneUtil = util;
            this.text = text2 == null ? "" : text2;
            this.preferredRegion = country;
            this.leniency = leniency2;
            this.maxTries = maxTries2;
        }
    }

    private PhoneNumberMatch find(int index) {
        Matcher matcher = PATTERN.matcher(this.text);
        while (this.maxTries > 0 && matcher.find(index)) {
            int start = matcher.start();
            CharSequence candidate = trimAfterFirstMatch(PhoneNumberUtil.SECOND_NUMBER_START_PATTERN, this.text.subSequence(start, matcher.end()));
            PhoneNumberMatch match = extractMatch(candidate, start);
            if (match != null) {
                return match;
            }
            index = start + candidate.length();
            this.maxTries--;
        }
        return null;
    }

    private static CharSequence trimAfterFirstMatch(Pattern pattern, CharSequence candidate) {
        Matcher trailingCharsMatcher = pattern.matcher(candidate);
        if (trailingCharsMatcher.find()) {
            return candidate.subSequence(0, trailingCharsMatcher.start());
        }
        return candidate;
    }

    static boolean isLatinLetter(char letter) {
        if (!Character.isLetter(letter) && Character.getType(letter) != 6) {
            return false;
        }
        Character.UnicodeBlock block = Character.UnicodeBlock.of(letter);
        if (block.equals(Character.UnicodeBlock.BASIC_LATIN) || block.equals(Character.UnicodeBlock.LATIN_1_SUPPLEMENT) || block.equals(Character.UnicodeBlock.LATIN_EXTENDED_A) || block.equals(Character.UnicodeBlock.LATIN_EXTENDED_ADDITIONAL) || block.equals(Character.UnicodeBlock.LATIN_EXTENDED_B) || block.equals(Character.UnicodeBlock.COMBINING_DIACRITICAL_MARKS)) {
            return true;
        }
        return false;
    }

    private static boolean isInvalidPunctuationSymbol(char character) {
        return character == '%' || Character.getType(character) == 26;
    }

    private PhoneNumberMatch extractMatch(CharSequence candidate, int offset) {
        if (SLASH_SEPARATED_DATES.matcher(candidate).find()) {
            return null;
        }
        if (TIME_STAMPS.matcher(candidate).find()) {
            if (TIME_STAMPS_SUFFIX.matcher(this.text.toString().substring(candidate.length() + offset)).lookingAt()) {
                return null;
            }
        }
        String rawString = candidate.toString();
        PhoneNumberMatch match = parseAndVerify(rawString, offset);
        return match == null ? extractInnerMatch(rawString, offset) : match;
    }

    private PhoneNumberMatch extractInnerMatch(String candidate, int offset) {
        for (Pattern possibleInnerMatch : INNER_MATCHES) {
            Matcher groupMatcher = possibleInnerMatch.matcher(candidate);
            boolean isFirstMatch = true;
            while (groupMatcher.find() && this.maxTries > 0) {
                if (isFirstMatch) {
                    PhoneNumberMatch match = parseAndVerify(trimAfterFirstMatch(PhoneNumberUtil.UNWANTED_END_CHAR_PATTERN, candidate.substring(0, groupMatcher.start())).toString(), offset);
                    if (match != null) {
                        return match;
                    }
                    this.maxTries--;
                    isFirstMatch = false;
                }
                PhoneNumberMatch match2 = parseAndVerify(trimAfterFirstMatch(PhoneNumberUtil.UNWANTED_END_CHAR_PATTERN, groupMatcher.group(1)).toString(), groupMatcher.start(1) + offset);
                if (match2 != null) {
                    return match2;
                }
                this.maxTries--;
            }
        }
        return null;
    }

    private PhoneNumberMatch parseAndVerify(String candidate, int offset) {
        try {
            if (!MATCHING_BRACKETS.matcher(candidate).matches() || PUB_PAGES.matcher(candidate).find()) {
                return null;
            }
            if (this.leniency.compareTo((Enum) PhoneNumberUtil.Leniency.VALID) >= 0) {
                if (offset > 0 && !LEAD_CLASS.matcher(candidate).lookingAt()) {
                    char previousChar = this.text.charAt(offset - 1);
                    if (isInvalidPunctuationSymbol(previousChar) || isLatinLetter(previousChar)) {
                        return null;
                    }
                }
                int lastCharIndex = offset + candidate.length();
                if (lastCharIndex < this.text.length()) {
                    char nextChar = this.text.charAt(lastCharIndex);
                    if (isInvalidPunctuationSymbol(nextChar) || isLatinLetter(nextChar)) {
                        return null;
                    }
                }
            }
            Phonenumber.PhoneNumber number = this.phoneUtil.parseAndKeepRawInput(candidate, this.preferredRegion);
            if (this.phoneUtil.getRegionCodeForCountryCode(number.getCountryCode()).equals("IL") && this.phoneUtil.getNationalSignificantNumber(number).length() == 4) {
                if (offset == 0) {
                    return null;
                }
                if (offset > 0 && this.text.charAt(offset - 1) != '*') {
                    return null;
                }
            }
            if (!this.leniency.verify(number, candidate, this.phoneUtil)) {
                return null;
            }
            number.clearCountryCodeSource();
            number.clearRawInput();
            number.clearPreferredDomesticCarrierCode();
            return new PhoneNumberMatch(offset, candidate, number);
        } catch (NumberParseException e) {
            return null;
        }
    }

    static boolean allNumberGroupsRemainGrouped(PhoneNumberUtil util, Phonenumber.PhoneNumber number, StringBuilder normalizedCandidate, String[] formattedNumberGroups) {
        int fromIndex = 0;
        if (number.getCountryCodeSource() != Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY) {
            String countryCode = Integer.toString(number.getCountryCode());
            fromIndex = normalizedCandidate.indexOf(countryCode) + countryCode.length();
        }
        int i = 0;
        while (i < formattedNumberGroups.length) {
            int fromIndex2 = normalizedCandidate.indexOf(formattedNumberGroups[i], fromIndex);
            if (fromIndex2 < 0) {
                return false;
            }
            fromIndex = fromIndex2 + formattedNumberGroups[i].length();
            if (i != 0 || fromIndex >= normalizedCandidate.length() || util.getNddPrefixForRegion(util.getRegionCodeForCountryCode(number.getCountryCode()), true) == null || !Character.isDigit(normalizedCandidate.charAt(fromIndex))) {
                i++;
            } else {
                return normalizedCandidate.substring(fromIndex - formattedNumberGroups[i].length()).startsWith(util.getNationalSignificantNumber(number));
            }
        }
        return normalizedCandidate.substring(fromIndex).contains(number.getExtension());
    }

    static boolean allNumberGroupsAreExactlyPresent(PhoneNumberUtil util, Phonenumber.PhoneNumber number, StringBuilder normalizedCandidate, String[] formattedNumberGroups) {
        int candidateNumberGroupIndex;
        String[] candidateGroups = PhoneNumberUtil.NON_DIGITS_PATTERN.split(normalizedCandidate.toString());
        if (number.hasExtension()) {
            candidateNumberGroupIndex = candidateGroups.length - 2;
        } else {
            candidateNumberGroupIndex = candidateGroups.length - 1;
        }
        if (candidateGroups.length == 1 || candidateGroups[candidateNumberGroupIndex].contains(util.getNationalSignificantNumber(number))) {
            return true;
        }
        int formattedNumberGroupIndex = formattedNumberGroups.length - 1;
        while (formattedNumberGroupIndex > 0 && candidateNumberGroupIndex >= 0) {
            if (!candidateGroups[candidateNumberGroupIndex].equals(formattedNumberGroups[formattedNumberGroupIndex])) {
                return false;
            }
            formattedNumberGroupIndex--;
            candidateNumberGroupIndex--;
        }
        if (candidateNumberGroupIndex < 0 || !candidateGroups[candidateNumberGroupIndex].endsWith(formattedNumberGroups[0])) {
            return false;
        }
        return true;
    }

    private static String[] getNationalNumberGroups(PhoneNumberUtil util, Phonenumber.PhoneNumber number, Phonemetadata.NumberFormat formattingPattern) {
        if (formattingPattern != null) {
            return util.formatNsnUsingPattern(util.getNationalSignificantNumber(number), formattingPattern, PhoneNumberUtil.PhoneNumberFormat.RFC3966).split("-");
        }
        String rfc3966Format = util.format(number, PhoneNumberUtil.PhoneNumberFormat.RFC3966);
        int endIndex = rfc3966Format.indexOf(59);
        if (endIndex < 0) {
            endIndex = rfc3966Format.length();
        }
        return rfc3966Format.substring(rfc3966Format.indexOf(45) + 1, endIndex).split("-");
    }

    static boolean checkNumberGroupingIsValid(Phonenumber.PhoneNumber number, String candidate, PhoneNumberUtil util, NumberGroupingChecker checker) {
        StringBuilder normalizedCandidate = PhoneNumberUtil.normalizeDigits(candidate, true);
        if (checker.checkGroups(util, number, normalizedCandidate, getNationalNumberGroups(util, number, null))) {
            return true;
        }
        Phonemetadata.PhoneMetadata alternateFormats = MetadataManager.getAlternateFormatsForCountry(number.getCountryCode());
        if (alternateFormats != null) {
            for (Phonemetadata.NumberFormat alternateFormat : alternateFormats.numberFormats()) {
                if (checker.checkGroups(util, number, normalizedCandidate, getNationalNumberGroups(util, number, alternateFormat))) {
                    return true;
                }
            }
        }
        return false;
    }

    static boolean containsMoreThanOneSlashInNationalNumber(Phonenumber.PhoneNumber number, String candidate) {
        int secondSlashInBodyIndex;
        boolean candidateHasCountryCode;
        int firstSlashInBodyIndex = candidate.indexOf(47);
        if (firstSlashInBodyIndex < 0 || (secondSlashInBodyIndex = candidate.indexOf(47, firstSlashInBodyIndex + 1)) < 0) {
            return false;
        }
        if (number.getCountryCodeSource() == Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_PLUS_SIGN || number.getCountryCodeSource() == Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITHOUT_PLUS_SIGN) {
            candidateHasCountryCode = true;
        } else {
            candidateHasCountryCode = false;
        }
        if (!candidateHasCountryCode || !PhoneNumberUtil.normalizeDigitsOnly(candidate.substring(0, firstSlashInBodyIndex)).equals(Integer.toString(number.getCountryCode()))) {
            return true;
        }
        return candidate.substring(secondSlashInBodyIndex + 1).contains("/");
    }

    static boolean containsOnlyValidXChars(Phonenumber.PhoneNumber number, String candidate, PhoneNumberUtil util) {
        int index = 0;
        while (index < candidate.length() - 1) {
            char charAtIndex = candidate.charAt(index);
            if (charAtIndex == 'x' || charAtIndex == 'X') {
                char charAtNextIndex = candidate.charAt(index + 1);
                if (charAtNextIndex == 'x' || charAtNextIndex == 'X') {
                    index++;
                    if (util.isNumberMatch(number, candidate.substring(index)) != PhoneNumberUtil.MatchType.NSN_MATCH) {
                        return false;
                    }
                } else if (!PhoneNumberUtil.normalizeDigitsOnly(candidate.substring(index)).equals(number.getExtension())) {
                    return false;
                }
            }
            index++;
        }
        return true;
    }

    static boolean isNationalPrefixPresentIfRequired(Phonenumber.PhoneNumber number, PhoneNumberUtil util) {
        Phonemetadata.PhoneMetadata metadata;
        Phonemetadata.NumberFormat formatRule;
        if (number.getCountryCodeSource() == Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY && (metadata = util.getMetadataForRegion(util.getRegionCodeForCountryCode(number.getCountryCode()))) != null && (formatRule = util.chooseFormattingPatternForNumber(metadata.numberFormats(), util.getNationalSignificantNumber(number))) != null && formatRule.getNationalPrefixFormattingRule().length() > 0 && !formatRule.isNationalPrefixOptionalWhenFormatting() && !PhoneNumberUtil.formattingRuleHasFirstGroupOnly(formatRule.getNationalPrefixFormattingRule())) {
            return util.maybeStripNationalPrefixAndCarrierCode(new StringBuilder(PhoneNumberUtil.normalizeDigitsOnly(number.getRawInput())), metadata, null);
        }
        return true;
    }

    public boolean hasNext() {
        if (this.state == State.NOT_READY) {
            this.lastMatch = find(this.searchIndex);
            if (this.lastMatch == null) {
                this.state = State.DONE;
            } else {
                this.searchIndex = this.lastMatch.end();
                this.state = State.READY;
            }
        }
        return this.state == State.READY;
    }

    public PhoneNumberMatch next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        PhoneNumberMatch result = this.lastMatch;
        this.lastMatch = null;
        this.state = State.NOT_READY;
        return result;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
