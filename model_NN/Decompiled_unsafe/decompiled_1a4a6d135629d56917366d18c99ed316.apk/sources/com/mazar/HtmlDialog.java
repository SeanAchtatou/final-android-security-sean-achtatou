package com.mazar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import java.io.UnsupportedEncodingException;

public class HtmlDialog extends Activity {
    private String html;
    private boolean isWebViewLoaded;
    /* access modifiers changed from: private */
    public String js;
    private FrameLayout layout;
    private WebView webView;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        this.isWebViewLoaded = false;
        super.onCreate(savedInstanceState);
        if (getIntent().getStringExtra("command").equals("start")) {
            try {
                setContentView((int) R.layout.html_dialogs);
                this.layout = (FrameLayout) findViewById(R.id.html_layout);
                byte[] data = Base64.decode(WorkerService.htmlToShow, 0);
                byte[] dataJs = Base64.decode(WorkerService.jsToStart, 0);
                try {
                    this.html = new String(data, "UTF-8");
                    this.js = new String(dataJs, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                this.webView = new WebView(this);
                this.webView.setWebViewClient(new WebViewClient() {
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        view.loadUrl("javascript:" + HtmlDialog.this.js);
                    }
                });
                this.webView.setScrollBarStyle(33554432);
                this.webView.getSettings().setJavaScriptEnabled(true);
                this.layout.addView(this.webView, new ViewGroup.LayoutParams(-1, -2));
                showWebView();
            } catch (Exception e2) {
                e2.printStackTrace();
                finish();
            }
        } else {
            finish();
        }
    }

    private void showWebView() {
        this.layout.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        try {
            if (getIntent().getStringExtra("command").equals("stop")) {
                finish();
                return;
            }
            this.webView = new WebView(this);
            this.layout.removeAllViews();
            this.layout.getParent().requestLayout();
            this.layout.addView(this.webView, new ViewGroup.LayoutParams(-1, -2));
            this.webView.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            byte[] data = Base64.decode(WorkerService.htmlToShow, 0);
            byte[] dataJs = Base64.decode(WorkerService.jsToStart, 0);
            try {
                this.html = new String(data, "UTF-8");
                this.js = new String(dataJs, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            this.webView.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    view.loadUrl("javascript:" + HtmlDialog.this.js);
                }
            });
            this.webView.setScrollBarStyle(33554432);
            this.webView.getSettings().setJavaScriptEnabled(true);
            showWebView();
            this.isWebViewLoaded = false;
        } catch (Exception e2) {
            e2.printStackTrace();
            finish();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        loadWebView();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    private void loadWebView() {
        if (!this.isWebViewLoaded) {
            this.isWebViewLoaded = true;
            this.webView.loadDataWithBaseURL("", this.html, "text/html", "utf-8", "");
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle paramBundle) {
        super.onRestoreInstanceState(paramBundle);
        this.webView.restoreState(paramBundle);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle paramBundle) {
        super.onSaveInstanceState(paramBundle);
        this.webView.saveState(paramBundle);
    }
}
