package com.mazar.processes.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;

public final class Statm extends ProcFile {
    public static final Parcelable.Creator<Statm> CREATOR = new Parcelable.Creator<Statm>() {
        public Statm createFromParcel(Parcel source) {
            return new Statm(source, null);
        }

        public Statm[] newArray(int size) {
            return new Statm[size];
        }
    };
    public final String[] fields;

    public static Statm get(int pid) throws IOException {
        return new Statm(String.format("/proc/%d/statm", Integer.valueOf(pid)));
    }

    private Statm(String path) throws IOException {
        super(path);
        this.fields = this.content.split("\\s+");
    }

    /* synthetic */ Statm(Parcel parcel, Statm statm) {
        this(parcel);
    }

    private Statm(Parcel in) {
        super(in);
        this.fields = in.createStringArray();
    }

    public long getSize() {
        return Long.parseLong(this.fields[0]) * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
    }

    public long getResidentSetSize() {
        return Long.parseLong(this.fields[1]) * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeStringArray(this.fields);
    }
}
