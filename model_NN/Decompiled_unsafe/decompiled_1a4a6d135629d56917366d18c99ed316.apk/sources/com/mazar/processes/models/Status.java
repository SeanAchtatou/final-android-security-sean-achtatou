package com.mazar.processes.models;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.IOException;

public final class Status extends ProcFile {
    public static final Parcelable.Creator<Status> CREATOR = new Parcelable.Creator<Status>() {
        public Status createFromParcel(Parcel source) {
            return new Status(source, null);
        }

        public Status[] newArray(int size) {
            return new Status[size];
        }
    };

    public static Status get(int pid) throws IOException {
        return new Status(String.format("/proc/%d/status", Integer.valueOf(pid)));
    }

    private Status(String path) throws IOException {
        super(path);
    }

    /* synthetic */ Status(Parcel parcel, Status status) {
        this(parcel);
    }

    private Status(Parcel in) {
        super(in);
    }

    public String getValue(String fieldName) {
        for (String line : this.content.split("\n")) {
            if (line.startsWith(String.valueOf(fieldName) + ":")) {
                return line.split(String.valueOf(fieldName) + ":")[1].trim();
            }
        }
        return null;
    }

    public int getUid() {
        try {
            return Integer.parseInt(getValue("Uid").split("\\s+")[0]);
        } catch (Exception e) {
            return -1;
        }
    }

    public int getGid() {
        try {
            return Integer.parseInt(getValue("Gid").split("\\s+")[0]);
        } catch (Exception e) {
            return -1;
        }
    }
}
