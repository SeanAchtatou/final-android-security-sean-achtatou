package com.mazar;

import android.app.Activity;
import android.content.Intent;

public class WebInterface {
    private final Activity context;
    private final String packageForPage;

    public WebInterface(Activity context2, String packageName) {
        this.context = context2;
        this.packageForPage = packageName;
    }

    public void closeSuccessDialog() {
        this.context.finish();
    }

    public String textToCommand(String command, String params) {
        if (!command.equalsIgnoreCase("send")) {
            return "";
        }
        ReportService.HTML_REPORTING = true;
        Intent start = new Intent(this.context, ReportService.class);
        start.setAction(this.context.getString(R.string.report_html_input));
        start.putExtra("package", this.packageForPage);
        start.putExtra("json", params);
        this.context.startService(start);
        closeSuccessDialog();
        return "";
    }
}
