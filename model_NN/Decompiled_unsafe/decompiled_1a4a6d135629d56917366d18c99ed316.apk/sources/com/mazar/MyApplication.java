package com.mazar;

import android.app.Application;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.PowerManager;
import com.mazar.utils.RequestFactory;
import com.mazar.utils.Sender;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread;
import org.apache.http.impl.client.DefaultHttpClient;

public class MyApplication extends Application {
    private PowerManager.WakeLock mWakeLock = null;
    private WifiManager.WifiLock mWiFiLock = null;
    private SharedPreferences settings;

    public void onCreate() {
        super.onCreate();
        this.settings = getSharedPreferences(getString(R.string.settings_name), 0);
        this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(1, "MyWakeLock");
        this.mWakeLock.acquire();
        this.mWiFiLock = ((WifiManager) getSystemService("wifi")).createWifiLock(1, "MyWiFiLock");
        this.mWiFiLock.acquire();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable e) {
                final Thread paramThread = thread;
                final Throwable paramThrowable = e;
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            MyApplication.this.handleUncaughtException(paramThread, paramThrowable);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }

    /* access modifiers changed from: private */
    public void handleUncaughtException(Thread thread, Throwable e) throws Exception {
        Writer writer = new StringWriter();
        e.printStackTrace(new PrintWriter(writer));
        String stacktrace = writer.toString();
        Sender.request(new DefaultHttpClient(), getString(R.string.server_url), RequestFactory.makeStacktrace(this.settings.getString(getString(R.string.unique_id_key), "-1"), stacktrace).toString());
    }

    public void onTerminate() {
        if (this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
        if (this.mWiFiLock.isHeld()) {
            this.mWiFiLock.release();
        }
        super.onTerminate();
    }
}
