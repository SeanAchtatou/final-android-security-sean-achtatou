package com.mazar;

import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

public class HookChromeClient extends WebChromeClient {
    public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
        result.confirm(InjDialog.webAppInterface.textToCommand(message, defaultValue));
        return true;
    }
}
