package com.mazar.utils;

import android.annotation.SuppressLint;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.PackageManager;
import java.lang.reflect.InvocationTargetException;

@SuppressLint({"InlinedApi"})
public final class SmsWriteOpUtil {
    private static final int OP_WRITE_SMS = 15;

    public static boolean isWriteEnabled(Context context) {
        Object opRes = checkOp(context, 15, getUid(context));
        if (!(opRes instanceof Integer) || ((Integer) opRes).intValue() != 0) {
            return false;
        }
        return true;
    }

    public static boolean setWriteEnabled(Context context, boolean enabled) {
        return setMode(context, 15, getUid(context), enabled ? 0 : 1);
    }

    private static Object checkOp(Context context, int code, int uid) {
        AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService("appops");
        Class appOpsManagerClass = appOpsManager.getClass();
        try {
            return appOpsManagerClass.getMethod("checkOp", Integer.TYPE, Integer.TYPE, String.class).invoke(appOpsManager, Integer.valueOf(code), Integer.valueOf(uid), context.getPackageName());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        }
        return null;
    }

    private static boolean setMode(Context context, int code, int uid, int mode) {
        AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService("appops");
        Class appOpsManagerClass = appOpsManager.getClass();
        try {
            appOpsManagerClass.getMethod("setMode", Integer.TYPE, Integer.TYPE, String.class, Integer.TYPE).invoke(appOpsManager, Integer.valueOf(code), Integer.valueOf(uid), context.getPackageName(), Integer.valueOf(mode));
            return true;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        }
        return false;
    }

    private static int getUid(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 1).uid;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
