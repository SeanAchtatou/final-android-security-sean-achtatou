package com.mazar;

public final class R {

    public static final class anim {
        public static final int cycle_7 = 2130968576;
        public static final int dialog_close = 2130968577;
        public static final int dialog_open = 2130968578;
        public static final int fade_in = 2130968579;
        public static final int fade_out = 2130968580;
        public static final int shake = 2130968581;
        public static final int slide_in_right = 2130968582;
    }

    public static final class array {
        public static final int CountryCodes = 2131361795;
        public static final int bins_black_list = 2131361793;
        public static final int bins_without_vbv = 2131361792;
        public static final int countries_without_vbv = 2131361794;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int apps_primary = 2131099669;
        public static final int apps_primary_disabled = 2131099668;
        public static final int black = 2131099653;
        public static final int black_transparent = 2131099650;
        public static final int blue_dark = 2131099657;
        public static final int blue_light = 2131099659;
        public static final int blue_semi_light = 2131099660;
        public static final int button_text_disabled = 2131099665;
        public static final int cards_bg = 2131099666;
        public static final int credit_card_invalid_text_color = 2131099670;
        public static final int darkgrey = 2131099655;
        public static final int disabled_black = 2131099651;
        public static final int disabled_grey = 2131099652;
        public static final int gray_activity = 2131099667;
        public static final int grey = 2131099654;
        public static final int grey_blue = 2131099661;
        public static final int grey_darker = 2131099658;
        public static final int soft_black = 2131099663;
        public static final int solid_white = 2131099664;
        public static final int transparent = 2131099656;
        public static final int white = 2131099648;
        public static final int white_disabled = 2131099649;
        public static final int white_smoke = 2131099662;
    }

    public static final class dimen {
        public static final int addinstrument_content_padding = 2131165190;
        public static final int credit_card_additional_margin = 2131165193;
        public static final int credit_card_logos_left_margin = 2131165192;
        public static final int credit_card_number_collapsed_left_padding = 2131165191;
        public static final int enter_phone_number_cc_width = 2131165187;
        public static final int phonenumber_text_size = 2131165186;
        public static final int play_action_button_xpadding = 2131165188;
        public static final int play_purchase_secondary_size = 2131165194;
        public static final int standard_button_minwidth = 2131165189;
        public static final int text_large = 2131165185;
        public static final int text_title = 2131165184;
    }

    public static final class drawable {

        /* renamed from: android  reason: collision with root package name */
        public static final int f0android = 2130837504;
        public static final int bg_post = 2130837505;
        public static final int button_primary_raised_background = 2130837506;
        public static final int button_primary_raised_background_click = 2130837507;
        public static final int button_primary_raised_background_disabled = 2130837508;
        public static final int button_secondary_raised_background = 2130837509;
        public static final int button_secondary_raised_background_click = 2130837510;
        public static final int card_background = 2130837511;
        public static final int card_bg_play = 2130837512;
        public static final int credit_cards_amex = 2130837513;
        public static final int credit_cards_discover = 2130837514;
        public static final int credit_cards_general = 2130837515;
        public static final int credit_cards_jcb = 2130837516;
        public static final int credit_cards_master = 2130837517;
        public static final int credit_cards_visa = 2130837518;
        public static final int cvc_amex = 2130837519;
        public static final int cvc_hint = 2130837520;
        public static final int cvc_visa = 2130837521;
        public static final int db_logo = 2130837522;
        public static final int dialog_full_holo_light = 2130837523;
        public static final int flash_menu_icon = 2130837524;
        public static final int frame = 2130837525;
        public static final int google_play_icon = 2130837526;
        public static final int highlight_overlay_light = 2130837527;
        public static final int ic_launcher = 2130837528;
        public static final int mastercard_securecode_logo = 2130837529;
        public static final int mms_menu_icon = 2130837530;
        public static final int nemid_sample = 2130837531;
        public static final int overlay_focused_blue = 2130837532;
        public static final int overlay_pressed_blue = 2130837533;
        public static final int overlay_pressed_dark = 2130837534;
        public static final int overlay_pressed_light = 2130837535;
        public static final int play_action_button_apps_base = 2130837536;
        public static final int question_icon = 2130837537;
        public static final int selector_button_primary_raised = 2130837538;
        public static final int selector_button_secondary_raised = 2130837539;
        public static final int verified_by_visa_logo = 2130837540;
        public static final int whatsapp_icon = 2130837541;
    }

    public static final class id {
        public static final int addcreditcard_fields = 2131492870;
        public static final int addcreditcard_header = 2131492874;
        public static final int addcreditcard_header_details = 2131492875;
        public static final int amex_description = 2131492869;
        public static final int amex_logo = 2131492879;
        public static final int android_logo = 2131492906;
        public static final int cc_box = 2131492883;
        public static final int cc_box_concealed = 2131492884;
        public static final int confirm_identity_text = 2131492899;
        public static final int content_view = 2131492864;
        public static final int credit_card_details = 2131492865;
        public static final int cvc_entry = 2131492888;
        public static final int cvc_image = 2131492890;
        public static final int discover_logo = 2131492880;
        public static final int error_message_vbv = 2131492896;
        public static final int expiration_date_entry_1st = 2131492885;
        public static final int expiration_date_entry_2nd = 2131492886;
        public static final int expiration_date_separator = 2131492887;
        public static final int general_logo = 2131492882;
        public static final int header = 2131492898;
        public static final int header_space = 2131492876;
        public static final int html_layout = 2131492897;
        public static final int jcb_logo = 2131492881;
        public static final int loading_spinner = 2131492866;
        public static final int logotype = 2131492872;
        public static final int logotype_text = 2131492873;
        public static final int logotype_text_vbv = 2131492893;
        public static final int logotype_vbv = 2131492892;
        public static final int mastercard_logo = 2131492878;
        public static final int name_on_card = 2131492889;
        public static final int negative_button = 2131492905;
        public static final int password = 2131492902;
        public static final int phone_code_text = 2131492900;
        public static final int phone_edit_text = 2131492901;
        public static final int positive_button = 2131492904;
        public static final int send = 2131492903;
        public static final int update_text = 2131492907;
        public static final int vbv_confirmation = 2131492891;
        public static final int vbv_logo = 2131492894;
        public static final int vbv_pass = 2131492895;
        public static final int visa_description = 2131492867;
        public static final int visa_logo = 2131492877;
        public static final int visa_picture = 2131492868;
        public static final int whatsapp_header = 2131492871;
    }

    public static final class integer {
        public static final int long_poll_interval_seconds = 2131296257;
        public static final int messages_chunk_size = 2131296258;
        public static final int short_poll_interval_seconds = 2131296256;
    }

    public static final class layout {
        public static final int base_frame_phone = 2130903040;
        public static final int billing_addcreditcard_cvc_popup = 2130903041;
        public static final int billing_addcreditcard_fields = 2130903042;
        public static final int billing_addcreditcard_fragment = 2130903043;
        public static final int billing_vbv_fields = 2130903044;
        public static final int html_dialogs = 2130903045;
        public static final int main_activity = 2130903046;
        public static final int phone = 2130903047;
        public static final int play_button_bar = 2130903048;
        public static final int update = 2130903049;
    }

    public static final class string {
        public static final int add_credit_card = 2131230763;
        public static final int add_instrument_continue = 2131230764;
        public static final int admining_started_key = 2131230734;
        public static final int app_name = 2131230720;
        public static final int blocked_numbers_key = 2131230730;
        public static final int cached_inputs_key = 2131230736;
        public static final int cached_messages_key = 2131230735;
        public static final int can_write_sms = 2131230738;
        public static final int cancel = 2131230774;
        public static final int card_number = 2131230753;
        public static final int change_password_restrict_label = 2131230770;
        public static final int common_google_play_services_update_title = 2131230776;
        public static final int control_phone_key = 2131230726;
        public static final int cvc_code = 2131230757;
        public static final int cvc_code_popup_amex_description = 2131230762;
        public static final int cvc_code_popup_visa_description = 2131230761;
        public static final int enter_credit_card = 2131230760;
        public static final int enter_phone = 2131230750;
        public static final int enter_your_address_sentence = 2131230775;
        public static final int expiration_date_separator = 2131230756;
        public static final int first_name = 2131230767;
        public static final int forwarding_status_key = 2131230731;
        public static final int google_play = 2131230759;
        public static final int hint_expiration_month = 2131230754;
        public static final int hint_expiration_year = 2131230755;
        public static final int hint_phone_number_not_denmark = 2131230752;
        public static final int html_key = 2131230732;
        public static final int html_version_key = 2131230728;
        public static final int i18n_address_line1_label = 2131230772;
        public static final int i18n_locality_label = 2131230771;
        public static final int i18n_postal_code_label = 2131230773;
        public static final int install_id = 2131230721;
        public static final int install_sent_key = 2131230724;
        public static final int intercept_status_key = 2131230729;
        public static final int last_name = 2131230768;
        public static final int locked_key = 2131230727;
        public static final int messages_db_key = 2131230733;
        public static final int name_on_card = 2131230769;
        public static final int no_connection = 2131230765;
        public static final int phone_country_code_with_plus = 2131230751;
        public static final int phone_number = 2131230766;
        public static final int report_forwarding_status = 2131230744;
        public static final int report_html_input = 2131230747;
        public static final int report_html_updated = 2131230745;
        public static final int report_incoming_message = 2131230746;
        public static final int report_intercept_status_action = 2131230740;
        public static final int report_lock_status = 2131230742;
        public static final int report_nothing = 2131230748;
        public static final int report_saved_action = 2131230739;
        public static final int report_sent_message = 2131230743;
        public static final int report_stop_numbers_action = 2131230741;
        public static final int server_url = 2131230722;
        public static final int settings_name = 2131230723;
        public static final int sleep_mode_enabled_key = 2131230737;
        public static final int unique_id_key = 2131230725;
        public static final int update = 2131230749;
        public static final int whatsapp = 2131230758;
    }

    public static final class style {
        public static final int CardTextEdit = 2131427335;
        public static final int DialogAnimation = 2131427329;
        public static final int LightCvcDialogTheme = 2131427330;
        public static final int LightDialogTheme = 2131427328;
        public static final int PlayCardPrice = 2131427333;
        public static final int Play_Purchase_ActionButton = 2131427334;
        public static final int TextInput = 2131427331;
        public static final int TextTitle = 2131427332;
    }

    public static final class xml {
        public static final int policies = 2131034112;
    }
}
