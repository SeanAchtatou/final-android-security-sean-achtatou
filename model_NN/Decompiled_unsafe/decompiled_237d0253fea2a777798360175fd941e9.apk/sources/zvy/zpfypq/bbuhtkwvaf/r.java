package zvy.zpfypq.bbuhtkwvaf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

@SuppressLint({"NewApi"})
public class r {
    public static boolean c;
    private SharedPreferences b;

    public r(Context context) {
        this.b = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String a(String str) {
        return this.b.getString(str, "");
    }

    /* renamed from: a  reason: collision with other method in class */
    public void m8a(String str) {
        this.b.edit().remove(str).apply();
    }

    public void a(String str, String str2) {
        b(str);
        c(str2);
        this.b.edit().putString(str, str2).apply();
    }

    public void a(String str, boolean z) {
        b(str);
        this.b.edit().putBoolean(str, z).apply();
    }

    /* renamed from: a  reason: collision with other method in class */
    public boolean m9a(String str) {
        return this.b.getBoolean(str, false);
    }

    public void b(String str) {
        if (str == null) {
            try {
                throw new NullPointerException();
            } catch (NullPointerException e) {
                throw e;
            }
        }
    }

    public void c(String str) {
        if (str == null) {
            try {
                throw new NullPointerException();
            } catch (NullPointerException e) {
                throw e;
            }
        }
    }
}
