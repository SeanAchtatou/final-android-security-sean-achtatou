package zvy.zpfypq.bbuhtkwvaf;

import java.io.File;
import java.io.RandomAccessFile;

public class u {
    public static boolean b;

    public static byte[] a(File file) {
        int i = 1024;
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        try {
            int length = (int) randomAccessFile.length();
            if (length <= 1024) {
                i = length;
            }
            byte[] bArr = new byte[i];
            randomAccessFile.readFully(bArr);
            return bArr;
        } finally {
            randomAccessFile.close();
        }
    }

    public static byte[] a(String str) {
        return a(new File(str));
    }

    public static byte[] b(File file) {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        try {
            byte[] bArr = new byte[((int) randomAccessFile.length())];
            randomAccessFile.readFully(bArr);
            return bArr;
        } finally {
            randomAccessFile.close();
        }
    }
}
