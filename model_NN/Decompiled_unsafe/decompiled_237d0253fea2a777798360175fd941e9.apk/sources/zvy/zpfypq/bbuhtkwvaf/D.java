package zvy.zpfypq.bbuhtkwvaf;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.os.Build;

class D implements Runnable {
    final /* synthetic */ IlIllllLILIlIllllIIILllIlIIllIlllllllllllllIllIIIlIIlLII b;
    final /* synthetic */ Drawable c;

    D(IlIllllLILIlIllllIIILllIlIIllIlllllllllllllIllIIIlIIlLII ilIllllLILIlIllllIIILllIlIIllIlllllllllllllIllIIIlIIlLII, Drawable drawable) {
        this.b = ilIllllLILIlIllllIIILllIlIIllIlllllllllllllIllIIIlIIlLII;
        this.c = drawable;
    }

    @TargetApi(16)
    public void run() {
        try {
            if (Build.VERSION.SDK_INT >= 16) {
                this.b.b.setBackground(this.c);
            }
        } catch (RuntimeException e) {
            throw e;
        }
    }
}
