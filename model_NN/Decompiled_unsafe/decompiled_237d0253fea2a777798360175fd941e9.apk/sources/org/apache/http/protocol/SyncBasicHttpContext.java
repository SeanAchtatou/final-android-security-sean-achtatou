package org.apache.http.protocol;

@Deprecated
public class SyncBasicHttpContext extends BasicHttpContext {
    public SyncBasicHttpContext(HttpContext httpContext) {
        throw new RuntimeException("Stub!");
    }

    public Object getAttribute(String str) {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }

    public Object removeAttribute(String str) {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }

    public void setAttribute(String str, Object obj) {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }
}
