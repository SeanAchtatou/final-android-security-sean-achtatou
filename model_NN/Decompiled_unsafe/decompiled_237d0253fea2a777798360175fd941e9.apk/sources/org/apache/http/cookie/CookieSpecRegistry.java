package org.apache.http.cookie;

import java.util.List;
import java.util.Map;
import org.apache.http.params.HttpParams;

@Deprecated
public final class CookieSpecRegistry {
    public CookieSpecRegistry() {
        throw new RuntimeException("Stub!");
    }

    public CookieSpec getCookieSpec(String str) {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }

    public CookieSpec getCookieSpec(String str, HttpParams httpParams) {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }

    public List getSpecNames() {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }

    public void register(String str, CookieSpecFactory cookieSpecFactory) {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }

    public void setItems(Map map) {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }

    public void unregister(String str) {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }
}
