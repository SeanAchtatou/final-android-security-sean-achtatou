package org.apache.http.impl.client;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.client.CredentialsProvider;

@Deprecated
public class BasicCredentialsProvider implements CredentialsProvider {
    public BasicCredentialsProvider() {
        throw new RuntimeException("Stub!");
    }

    public void clear() {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }

    public Credentials getCredentials(AuthScope authScope) {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }

    public void setCredentials(AuthScope authScope, Credentials credentials) {
        synchronized (this) {
            throw new RuntimeException("Stub!");
        }
    }

    public String toString() {
        throw new RuntimeException("Stub!");
    }
}
