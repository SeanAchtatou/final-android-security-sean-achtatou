package android.net.http;

import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;

public final class AndroidHttpClient implements HttpClient {
    AndroidHttpClient() {
        throw new RuntimeException("Stub!");
    }

    public Object execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler responseHandler) {
        throw new RuntimeException("Stub!");
    }

    public Object execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler responseHandler, HttpContext httpContext) {
        throw new RuntimeException("Stub!");
    }

    public Object execute(HttpUriRequest httpUriRequest, ResponseHandler responseHandler) {
        throw new RuntimeException("Stub!");
    }

    public Object execute(HttpUriRequest httpUriRequest, ResponseHandler responseHandler, HttpContext httpContext) {
        throw new RuntimeException("Stub!");
    }

    public HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest) {
        throw new RuntimeException("Stub!");
    }

    public HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest, HttpContext httpContext) {
        throw new RuntimeException("Stub!");
    }

    public HttpResponse execute(HttpUriRequest httpUriRequest) {
        throw new RuntimeException("Stub!");
    }

    public HttpResponse execute(HttpUriRequest httpUriRequest, HttpContext httpContext) {
        throw new RuntimeException("Stub!");
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        throw new RuntimeException("Stub!");
    }

    public ClientConnectionManager getConnectionManager() {
        throw new RuntimeException("Stub!");
    }

    public HttpParams getParams() {
        throw new RuntimeException("Stub!");
    }
}
