package spbj.ahfwykt.yjhtup;

import android.content.SharedPreferences;
import android.os.Build;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

final class j {
    static long a;
    static long b;
    static volatile int c;
    static Object d;
    static List e;
    static final List<String> f = new ArrayList();
    static int g;
    static int h = 0;
    static int i;
    static int j;
    static Object k;
    static final String[] l = new String[4];
    static Object m;
    static Class<?> n;
    static Class<?> o;
    static Method p;
    private static SharedPreferences q;
    private static ArrayList<String> r;

    static {
        b = 0;
        c = 0;
        g = 0;
        i = 0;
        m = "";
        try {
            n = Class.forName("[I").getComponentType();
            o = Class.forName("[J").getComponentType();
            Object invoke = Class.forName("android.app.Application").getMethod("getAssets", new Class[0]).invoke(Acccfaeb.a(), new Object[0]);
            Object invoke2 = invoke.getClass().getMethod("open", Class.forName("java.lang.String")).invoke(invoke, "fileWithConstants");
            byte[] bArr = new byte[((Integer) invoke2.getClass().getMethod("available", new Class[0]).invoke(invoke2, new Object[0])).intValue()];
            invoke2.getClass().getMethod("read", Class.forName("[B")).invoke(invoke2, bArr);
            Object newInstance = Class.forName("java.lang.StringBuilder").newInstance();
            Method method = newInstance.getClass().getMethod("append", Class.forName("[C").getComponentType());
            for (int i2 = 0; i2 < bArr.length; i2++) {
                method.invoke(newInstance, Character.valueOf((char) bArr[i2]));
            }
            String replace = new String(a.a((byte[]) Class.forName("android.util.Base64").getMethod("decode", String.class, n).invoke(null, newInstance.toString(), 0), "285fbf700b")).replace("\\n", "\n");
            r = new ArrayList<>();
            int i3 = 0;
            while (true) {
                int indexOf = replace.indexOf("|", i3);
                if (indexOf < 0) {
                    break;
                }
                r.add(replace.substring(i3, indexOf));
                i3 = indexOf + 1;
            }
            e = (List) b.b(839189).newInstance();
            Method method2 = b.b(839189).getMethod(a(1048841), b.b(839182));
            p = Method.class.getMethod(a(1048720), Boolean.TYPE);
            p.invoke(p, true);
            p.invoke(method2, true);
            method2.invoke(e, b.g());
            k = b.b(839189).newInstance();
            d = b.b(839189).newInstance();
            Method method3 = b.b(839189).getMethod(a(1048797), b.b(839143));
            p.invoke(method3, true);
            method3.invoke(d, a(1048934));
            method3.invoke(d, a(1048935));
            method3.invoke(d, a(1048936));
            method3.invoke(d, a(1048937));
            method3.invoke(d, a(1048938));
            method3.invoke(d, a(1048939));
            method3.invoke(d, a(1048940));
            method3.invoke(d, a(1048941));
            method3.invoke(d, a(1048942));
            method3.invoke(d, a(1048943));
            method3.invoke(d, a(1048944));
            Method method4 = b.b(839153).getMethod(a(1048766), new Class[0]);
            p.invoke(method4, true);
            m = method4.invoke(Acccfaeb.a(), new Object[0]);
        } catch (Throwable th) {
        }
        if (Build.VERSION.SDK_INT >= 23) {
            l[0] = a(1048593);
            l[1] = a(1048594);
            l[2] = a(1048596);
            l[3] = a(1048595);
        }
        try {
            Method method5 = b.b(839153).getMethod(a(1048848), b.b(839141), n);
            p.invoke(method5, true);
            q = (SharedPreferences) method5.invoke(Acccfaeb.a(), a(1048689), 0);
            Method method6 = b.b(839257).getMethod(a(1048740), b.b(839141), o);
            Method method7 = b.b(839257).getMethod(a(1048757), b.b(839141), n);
            p.invoke(method6, true);
            p.invoke(method7, true);
            j = ((Integer) method7.invoke(q, a(1048695), 0)).intValue();
            i = ((Integer) method7.invoke(q, a(1048693), 0)).intValue();
            c = ((Integer) method7.invoke(q, a(1048697), 0)).intValue();
            g = ((Integer) method7.invoke(q, a(1048698), 0)).intValue();
            b = ((Long) method6.invoke(q, a(1048694), 0)).longValue();
            a = ((Long) method6.invoke(q, a(1048696), 0)).longValue();
        } catch (Throwable th2) {
        }
    }

    private static void a(String str, int i2) {
        q.edit().putInt(str, i2).apply();
    }

    private static void a(String str, long j2) {
        q.edit().putLong(str, j2).apply();
    }

    static String a(int i2) {
        return r.get(i2 - 1048575);
    }

    static void a() {
        i = 1;
        a(a(1048693), 1);
    }

    static void a(long j2) {
        b = j2;
        a(a(1048694), j2);
    }

    static void b() {
        j = 1;
        a(a(1048695), 1);
    }

    static void b(int i2) {
        c = i2;
        a(a(1048697), i2);
    }

    static void c() {
        g = 1;
        a(a(1048698), 1);
    }
}
