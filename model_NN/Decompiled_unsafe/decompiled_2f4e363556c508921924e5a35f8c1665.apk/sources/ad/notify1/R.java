package ad.notify1;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int logo = 2130837505;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int data = 2130968576;
        public static final int end = 2130968577;
        public static final int license = 2130968578;
        public static final int old_data = 2130968579;
        public static final int settings = 2130968580;
        public static final int sms = 2130968581;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }
}
