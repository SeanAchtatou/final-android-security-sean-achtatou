package ad.notify;

public interface ThreadOperationListener {
    void threadOperationRun(int i, Object obj);
}
