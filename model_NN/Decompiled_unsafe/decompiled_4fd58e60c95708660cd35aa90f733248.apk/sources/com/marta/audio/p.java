package com.marta.audio;

import android.hardware.Camera;

class p implements Runnable {
    final /* synthetic */ o a;

    p(o oVar) {
        this.a = oVar;
    }

    public void run() {
        if (this.a.a.b != null) {
            this.a.a.b.stopPreview();
            this.a.a.b.release();
            this.a.a.b = (Camera) null;
        }
        this.a.a.finish();
        System.gc();
    }
}
