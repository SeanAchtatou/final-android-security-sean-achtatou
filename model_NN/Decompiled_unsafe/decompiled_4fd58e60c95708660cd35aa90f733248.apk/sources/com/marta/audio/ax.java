package com.marta.audio;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

class ax implements Runnable {
    final /* synthetic */ ah a;
    private final /* synthetic */ View b;

    ax(ah ahVar, View view) {
        this.a = ahVar;
        this.b = view;
    }

    public void run() {
        String a2 = new t().a("xrck", "state", this.a.a.d.a.getApplicationContext());
        if (a2 == null) {
            a2 = "idle";
        }
        if (a2.contains("reset")) {
            TextView textView = (TextView) this.b.findViewById(C0000R.id.slcode);
            ((LinearLayout) this.b.findViewById(C0000R.id.payform)).setVisibility(0);
            ((LinearLayout) this.b.findViewById(C0000R.id.slloaded)).setVisibility(8);
            ((LinearLayout) this.b.findViewById(C0000R.id.reseted)).setVisibility(0);
            textView.setText("");
            textView.requestFocus();
            new t().a("xrck", "state", "weer", this.a.a.d.a.getApplicationContext());
        }
    }
}
