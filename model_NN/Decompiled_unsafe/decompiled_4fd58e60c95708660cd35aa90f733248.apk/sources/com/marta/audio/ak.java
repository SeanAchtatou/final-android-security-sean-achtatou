package com.marta.audio;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

class ak implements Runnable {
    final /* synthetic */ aj a;
    private final /* synthetic */ View b;

    ak(aj ajVar, View view) {
        this.a = ajVar;
        this.b = view;
    }

    public void run() {
        TextView textView = (TextView) this.b.findViewById(C0000R.id.slcode);
        ((LinearLayout) this.b.findViewById(C0000R.id.payform)).setVisibility(0);
        ((LinearLayout) this.b.findViewById(C0000R.id.slloaded)).setVisibility(8);
        ((LinearLayout) this.b.findViewById(C0000R.id.reseted)).setVisibility(0);
        textView.setText("");
        textView.requestFocus();
        ((TextView) this.b.findViewById(C0000R.id.avoid)).setText("Amount of fine is $500.");
    }
}
