package com.marta.audio;

import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

public class ad extends AsyncTask {
    final /* synthetic */ xr a;

    public ad(xr xrVar) {
        this.a = xrVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public View doInBackground(Void... voidArr) {
        xr.b = (WindowManager) this.a.getSystemService("window");
        xr.d = (LayoutInflater) this.a.getSystemService("layout_inflater");
        return xr.d.inflate((int) C0000R.layout.scan, (ViewGroup) null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(View view) {
        xr.b.addView(view, xr.a);
        new ae(this.a).execute(view);
    }
}
