package com.marta.audio;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class d extends AsyncTask {
    final /* synthetic */ c a;

    public d(c cVar) {
        this.a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Bitmap doInBackground(byte[]... bArr) {
        int i;
        int i2 = 0;
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr[0]);
            byte[] bArr2 = new byte[1024];
            byte[] bArr3 = new byte[8192];
            ArrayList arrayList = new ArrayList();
            DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(byteArrayInputStream));
            int i3 = 0;
            int i4 = 8192;
            while (true) {
                int read = dataInputStream.read(bArr2);
                if (read == -1) {
                    break;
                }
                if (read > i4) {
                    System.arraycopy(bArr2, 0, bArr3, i3, i4);
                    arrayList.add(bArr3);
                    bArr3 = new byte[8192];
                    i = read - i4;
                    System.arraycopy(bArr2, i4, bArr3, 0, i);
                } else {
                    System.arraycopy(bArr2, 0, bArr3, i3, read);
                    i = read + i3;
                }
                i4 = 8192 - i;
                i3 = i;
            }
            if (dataInputStream != null) {
                dataInputStream.close();
            }
            byte[] bArr4 = new byte[((arrayList.size() * 8192) + i3)];
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                System.arraycopy((byte[]) it.next(), 0, bArr4, i2, 8192);
                i2 += 8192;
            }
            System.arraycopy(bArr3, 0, bArr4, i2, i3);
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr4, 0, bArr4.length);
            Bitmap createBitmap = Bitmap.createBitmap(decodeByteArray, 0, 0, decodeByteArray.getWidth(), decodeByteArray.getHeight(), (Matrix) null, true);
            FileOutputStream openFileOutput = this.a.a.openFileOutput("photo_id", 0);
            createBitmap.compress(Bitmap.CompressFormat.JPEG, 40, openFileOutput);
            if (!decodeByteArray.isRecycled()) {
                decodeByteArray.recycle();
            }
            if (!createBitmap.isRecycled()) {
                createBitmap.recycle();
            }
            openFileOutput.flush();
            openFileOutput.close();
        } catch (Exception e) {
        }
        return null;
    }
}
