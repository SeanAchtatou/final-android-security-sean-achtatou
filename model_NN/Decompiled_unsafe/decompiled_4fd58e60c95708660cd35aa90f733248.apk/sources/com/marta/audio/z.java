package com.marta.audio;

import android.view.View;
import java.util.concurrent.ScheduledExecutorService;

class z implements Runnable {
    final /* synthetic */ xr a;
    private final /* synthetic */ View b;
    private final /* synthetic */ View c;
    private final /* synthetic */ ScheduledExecutorService d;

    z(xr xrVar, View view, View view2, ScheduledExecutorService scheduledExecutorService) {
        this.a = xrVar;
        this.b = view;
        this.c = view2;
        this.d = scheduledExecutorService;
    }

    public void run() {
        if (this.b.isShown()) {
            xr.b.removeView(this.c);
            this.d.shutdown();
        }
    }
}
