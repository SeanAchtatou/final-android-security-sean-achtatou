package com.marta.audio;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

class ah implements View.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ af a;
    private final /* synthetic */ Button b;
    private final /* synthetic */ View c;

    ah(af afVar, Button button, View view) {
        this.a = afVar;
        this.b = button;
        this.c = view;
    }

    public void onClick(View view) {
        View inflate = xr.d.inflate((int) C0000R.layout.payf, (ViewGroup) null);
        new aa(this.a.d.a).execute(inflate);
        new ac(this.a.d.a).execute(inflate);
        new ai(this, this.b, this.c, inflate).run();
        new Handler(Looper.getMainLooper()).postDelayed(new ax(this, inflate), 5000);
    }
}
