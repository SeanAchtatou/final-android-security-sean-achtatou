package com.marta.audio;

import android.app.Activity;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.LinearLayout;

public class ssPhoto extends Activity {
    SurfaceHolder.Callback a = new l(this);
    /* access modifiers changed from: private */
    public Camera b;
    /* access modifiers changed from: private */
    public LinearLayout c;
    /* access modifiers changed from: private */
    public SurfaceHolder d;
    /* access modifiers changed from: private */
    public q e = q.STOPPED;

    private int a() {
        int i = -1;
        if (getPackageManager().hasSystemFeature("android.hardware.camera.front")) {
            int numberOfCameras = Camera.getNumberOfCameras();
            Camera.CameraInfo b2 = b();
            for (int i2 = 0; i2 < numberOfCameras; i2++) {
                Camera.getCameraInfo(i2, b2);
                if (b2.facing == 1) {
                    i = i2;
                }
            }
        }
        return i;
    }

    private Camera.CameraInfo b() {
        return new Camera.CameraInfo();
    }

    private Camera c() {
        try {
            int a2 = a();
            if (a2 == -1) {
                return null;
            }
            this.b = Camera.open(a2);
            return this.b;
        } catch (RuntimeException e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.photoclass);
        setRequestedOrientation(1);
        this.c = (LinearLayout) findViewById(C0000R.id.photo);
        this.c.setEnabled(false);
        SurfaceView surfaceView = (SurfaceView) findViewById(C0000R.id.photoz);
        this.b = c();
        if (this.b != null) {
            new m(this).start();
            this.d = surfaceView.getHolder();
            this.d.addCallback(this.a);
            return;
        }
        finish();
    }

    public void onStop() {
        super.onStop();
    }
}
