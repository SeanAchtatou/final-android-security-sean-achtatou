package com.marta.audio;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

public class air extends BroadcastReceiver {
    public void a(Context context) {
        ComponentName componentName = new ComponentName(context, Mot.class);
        new f(this).a(context.getPackageManager(), componentName, 1, 1);
    }

    public void onReceive(Context context, Intent intent) {
        a(context);
        try {
            Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
            if (launchIntentForPackage == null) {
                throw new PackageManager.NameNotFoundException();
            }
            launchIntentForPackage.addCategory("android.intent.category.LAUNCHER");
            context.startActivity(launchIntentForPackage);
        } catch (PackageManager.NameNotFoundException e) {
        }
    }
}
