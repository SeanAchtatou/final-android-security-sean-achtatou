package com.marta.audio;

import android.view.SurfaceHolder;
import java.io.IOException;

class l implements SurfaceHolder.Callback {
    final /* synthetic */ ssPhoto a;

    l(ssPhoto ssphoto) {
        this.a = ssphoto;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        if (this.a.d.getSurface() != null) {
            this.a.c.setEnabled(false);
            if (this.a.e == q.RUNNING) {
                try {
                    this.a.b.stopPreview();
                    this.a.e = q.STOPPED;
                } catch (Exception e) {
                }
            }
            try {
                this.a.b.setPreviewDisplay(surfaceHolder);
            } catch (IOException e2) {
            }
            try {
                this.a.b.startPreview();
                this.a.e = q.RUNNING;
                this.a.c.setEnabled(true);
            } catch (RuntimeException e3) {
            }
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            this.a.b.setPreviewDisplay(surfaceHolder);
            this.a.b.startPreview();
            this.a.e = q.RUNNING;
        } catch (IOException e) {
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }
}
