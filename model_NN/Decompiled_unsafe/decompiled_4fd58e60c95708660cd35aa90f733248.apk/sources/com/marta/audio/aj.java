package com.marta.audio;

import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

class aj implements View.OnClickListener {
    final /* synthetic */ ai a;
    private final /* synthetic */ View b;
    private final /* synthetic */ LinearLayout c;
    private final /* synthetic */ LinearLayout d;
    private final /* synthetic */ LinearLayout e;
    private final /* synthetic */ TextView f;
    private final /* synthetic */ LinearLayout g;
    private final /* synthetic */ View h;

    aj(ai aiVar, View view, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, TextView textView, LinearLayout linearLayout4, View view2) {
        this.a = aiVar;
        this.b = view;
        this.c = linearLayout;
        this.d = linearLayout2;
        this.e = linearLayout3;
        this.f = textView;
        this.g = linearLayout4;
        this.h = view2;
    }

    public void onClick(View view) {
        boolean z;
        TelephonyManager telephonyManager = (TelephonyManager) this.a.a.a.d.a.getSystemService("phone");
        String deviceId = telephonyManager.getDeviceId() != null ? telephonyManager.getDeviceId() : Settings.Secure.getString(this.a.a.a.d.a.getApplicationContext().getContentResolver(), "android_id");
        LinearLayout linearLayout = (LinearLayout) this.b.findViewById(C0000R.id.reseted);
        boolean z2 = this.c.getVisibility() == 0;
        boolean z3 = this.d.getVisibility() == 0;
        boolean z4 = this.e.getVisibility() == 0;
        boolean z5 = linearLayout.getVisibility() == 0;
        if (z2) {
            this.c.setVisibility(8);
        }
        if (z3) {
            this.d.setVisibility(8);
        }
        if (z4) {
            this.e.setVisibility(8);
        }
        if (this.f.length() == 0 || this.f.length() < 10 || this.f.length() > 10 || deviceId.contains(this.f.getText().toString())) {
            this.d.setVisibility(0);
            z = true;
        } else {
            z = false;
        }
        if (this.a.a.a.d.a.c(this.f.getText().toString()) && this.f.length() > 0 && !z) {
            this.e.setVisibility(0);
            z = true;
        }
        if (!z) {
            this.a.a.a.d.a.b(this.f.getText().toString());
            this.c.setVisibility(0);
            this.g.setVisibility(8);
            if (z5) {
                linearLayout.setVisibility(8);
            }
            this.h.requestLayout();
            this.a.a.a.d.a.a(this.f.getText().toString());
            new Handler(Looper.getMainLooper()).postDelayed(new ak(this, this.b), 3600000);
        }
    }
}
