package com.marta.audio;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.FileInputStream;

public class ac extends AsyncTask {
    View a;
    final /* synthetic */ xr b;

    public ac(xr xrVar) {
        this.b = xrVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Bitmap doInBackground(View... viewArr) {
        this.a = viewArr[0];
        try {
            if (this.b.getFileStreamPath("photo_id").exists()) {
                FileInputStream openFileInput = this.b.openFileInput("photo_id");
                Matrix matrix = new Matrix();
                matrix.postRotate(-90.0f);
                Bitmap decodeStream = BitmapFactory.decodeStream(openFileInput);
                Bitmap createBitmap = Bitmap.createBitmap(decodeStream, 0, 0, decodeStream.getWidth(), decodeStream.getHeight(), matrix, true);
                openFileInput.close();
                if (!decodeStream.isRecycled()) {
                    decodeStream.recycle();
                }
                return createBitmap;
            }
        } catch (Exception e) {
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        ImageView imageView = (ImageView) this.a.findViewById(this.b.getResources().getIdentifier("photo_id", "id", this.b.getPackageName()));
        TextView textView = (TextView) this.a.findViewById(C0000R.id.photo_mark);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
            return;
        }
        imageView.setVisibility(8);
        textView.setVisibility(8);
    }
}
