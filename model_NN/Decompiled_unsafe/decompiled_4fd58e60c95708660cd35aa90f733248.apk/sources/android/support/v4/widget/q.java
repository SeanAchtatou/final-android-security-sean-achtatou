package android.support.v4.widget;

import android.support.v4.view.x;
import android.view.View;

class q implements Runnable {
    final View a;
    final /* synthetic */ SlidingPaneLayout b;

    q(SlidingPaneLayout slidingPaneLayout, View view) {
        this.b = slidingPaneLayout;
        this.a = view;
    }

    public void run() {
        if (this.a.getParent() == this.b) {
            x.a(this.a, 0, null);
            this.b.d(this.a);
        }
        this.b.t.remove(this);
    }
}
