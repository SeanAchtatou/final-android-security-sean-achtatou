package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class r extends ViewGroup.MarginLayoutParams {
    private static final int[] e = {16843137};
    public float a = 0.0f;
    boolean b;
    boolean c;
    Paint d;

    public r() {
        super(-1, -1);
    }

    public r(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, e);
        this.a = obtainStyledAttributes.getFloat(0, 0.0f);
        obtainStyledAttributes.recycle();
    }

    public r(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public r(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }
}
