package android.support.v4.view;

import android.view.View;
import java.util.Comparator;

class ax implements Comparator {
    ax() {
    }

    /* renamed from: a */
    public int compare(View view, View view2) {
        ar arVar = (ar) view.getLayoutParams();
        ar arVar2 = (ar) view2.getLayoutParams();
        return arVar.a != arVar2.a ? arVar.a ? 1 : -1 : arVar.e - arVar2.e;
    }
}
