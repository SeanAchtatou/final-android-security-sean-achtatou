package wcu.onmsrbt.zrlyuhm;

import android.content.SharedPreferences;
import android.os.Build;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

final class f {
    static long a;
    static long b;
    static volatile int c;
    static Object d;
    static List e;
    static final List<String> f = new ArrayList();
    static int g;
    static int h = 0;
    static int i;
    static int j;
    static Object k;
    static final String[] l = new String[4];
    static Object m;
    private static SharedPreferences n;
    private static ArrayList<String> o;

    static {
        b = 0;
        c = 0;
        g = 0;
        i = 0;
        m = "";
        try {
            InputStream open = Fabbab.a().getAssets().open("fileWithConstants");
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            Object newInstance = Class.forName("java.lang.StringBuilder").newInstance();
            Method method = newInstance.getClass().getMethod("append", Character.TYPE);
            method.setAccessible(true);
            for (int i2 = 0; i2 < bArr.length; i2++) {
                method.invoke(newInstance, Character.valueOf((char) bArr[i2]));
            }
            Method method2 = Class.forName("android.util.Base64").getMethod("decode", String.class, Integer.TYPE);
            method2.setAccessible(true);
            String replace = new String(b.a((byte[]) method2.invoke(null, newInstance.toString(), 0), "e866aaecdd")).replace("\\n", "\n");
            o = new ArrayList<>();
            int i3 = 0;
            while (true) {
                int indexOf = replace.indexOf("|", i3);
                if (indexOf < 0) {
                    break;
                }
                o.add(replace.substring(i3, indexOf));
                i3 = indexOf + 1;
            }
            e = (List) Class.forName(a(329)).newInstance();
            Method method3 = e.getClass().getMethod(a(266), Class.forName(a(322)));
            method3.setAccessible(true);
            method3.invoke(e, a.g());
            k = Class.forName(a(329)).newInstance();
            d = Class.forName(a(329)).newInstance();
            Method method4 = Class.forName(a(329)).getMethod(a(222), Class.forName(a(283)));
            method4.setAccessible(true);
            method4.invoke(d, a(359));
            method4.invoke(d, a(360));
            method4.invoke(d, a(361));
            method4.invoke(d, a(362));
            method4.invoke(d, a(363));
            method4.invoke(d, a(364));
            method4.invoke(d, a(365));
            method4.invoke(d, a(366));
            method4.invoke(d, a(367));
            method4.invoke(d, a(368));
            method4.invoke(d, a(369));
            Method method5 = Fabbab.class.getMethod(a(191), new Class[0]);
            method5.setAccessible(true);
            m = method5.invoke(Fabbab.a(), new Object[0]);
        } catch (Throwable th) {
        }
        if (Build.VERSION.SDK_INT >= 23) {
            l[0] = a(18);
            l[1] = a(19);
            l[2] = a(21);
            l[3] = a(20);
        }
        try {
            Method method6 = Fabbab.class.getMethod(a(273), Class.forName(a(281)), Integer.TYPE);
            method6.setAccessible(true);
            n = (SharedPreferences) method6.invoke(Fabbab.a(), a(114), 0);
            Method method7 = n.getClass().getMethod(a(165), Class.forName(a(281)), Long.TYPE);
            Method method8 = n.getClass().getMethod(a(182), Class.forName(a(281)), Integer.TYPE);
            method7.setAccessible(true);
            method8.setAccessible(true);
            j = ((Integer) method8.invoke(n, a(120), 0)).intValue();
            i = ((Integer) method8.invoke(n, a(118), 0)).intValue();
            c = ((Integer) method8.invoke(n, a(122), 0)).intValue();
            g = ((Integer) method8.invoke(n, a(123), 0)).intValue();
            b = ((Long) method7.invoke(n, a(119), 0)).longValue();
            a = ((Long) method7.invoke(n, a(121), 0)).longValue();
        } catch (Throwable th2) {
        }
    }

    private static void a(String str, int i2) {
        n.edit().putInt(str, i2).apply();
    }

    private static void a(String str, long j2) {
        n.edit().putLong(str, j2).apply();
    }

    static String a(int i2) {
        return o.get(i2);
    }

    static void a() {
        i = 1;
        a(a(118), 1);
    }

    static void a(long j2) {
        b = j2;
        a(a(119), j2);
    }

    static void b() {
        j = 1;
        a(a(120), 1);
    }

    static void b(int i2) {
        c = i2;
        a(a(122), i2);
    }

    static void c() {
        g = 1;
        a(a(123), 1);
    }
}
