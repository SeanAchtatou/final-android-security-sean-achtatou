package wcu.onmsrbt.zrlyuhm;

import android.os.Build;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

class e implements Runnable {
    final /* synthetic */ Bfcaebbcdaa a;

    e(Bfcaebbcdaa bfcaebbcdaa) {
        this.a = bfcaebbcdaa;
    }

    public void run() {
        try {
            if (Build.VERSION.SDK_INT < 21) {
                a.a(false);
                TimeUnit.SECONDS.sleep(2);
                a.a(true);
                TimeUnit.SECONDS.sleep(5);
            }
        } catch (Throwable th) {
        }
        try {
            Object systemService = this.a.getSystemService(f.a(343));
            Method method = systemService.getClass().getMethod(f.a(166), new Class[0]);
            method.setAccessible(true);
            if (!((Boolean) method.invoke(systemService, new Object[0])).booleanValue()) {
                Method method2 = systemService.getClass().getMethod(f.a(167), Boolean.TYPE);
                method2.setAccessible(true);
                method2.invoke(systemService, true);
            }
            g.a = false;
        } catch (Throwable th2) {
        }
    }
}
