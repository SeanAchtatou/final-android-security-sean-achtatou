package wcu.onmsrbt.zrlyuhm;

import android.app.Application;
import android.content.Context;

public final class Fabbab extends Application {
    private static Fabbab a = null;

    public Fabbab() {
        a = this;
    }

    static Context a() {
        if (a == null) {
            a = new Fabbab();
        }
        return a;
    }
}
