package wcu.onmsrbt.zrlyuhm;

import android.os.AsyncTask;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

class d extends AsyncTask<Object, Void, Void> {
    final /* synthetic */ c a;

    d(c cVar) {
        this.a = cVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Object... objArr) {
        try {
            Object b = a.b(f.a(345));
            Method method = b.getClass().getMethod(f.a(237), new Class[0]);
            Method method2 = b.getClass().getMethod(f.a(238), Integer.TYPE);
            method.setAccessible(true);
            method2.setAccessible(true);
            Object invoke = method.invoke(b, new Object[0]);
            method2.invoke(b, 0);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
            }
            Object newInstance = Class.forName(f.a(291)).newInstance();
            Method method3 = newInstance.getClass().getMethod(f.a(137), Class.forName(f.a(281)), Class.forName(f.a(283)));
            method3.setAccessible(true);
            method3.invoke(newInstance, f.a(48), objArr[0]);
            method3.invoke(newInstance, f.a(47), objArr[1]);
            method3.invoke(newInstance, f.a(49), false);
            g.a().a(2, newInstance);
            if (a.c()) {
                a.c(f.a(45));
                a.c(f.a(356));
            }
            method2.invoke(b, invoke);
            return null;
        } catch (Exception e2) {
            a.a((Throwable) e2);
            return null;
        }
    }
}
