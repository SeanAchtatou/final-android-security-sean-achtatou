package android.net.http;

import java.net.CacheRequest;
import java.net.CacheResponse;
import java.net.ResponseCache;
import java.net.URI;
import java.net.URLConnection;
import java.util.Map;

public final class HttpResponseCache extends ResponseCache {
    HttpResponseCache() {
        throw new RuntimeException("Stub!");
    }

    public final CacheResponse get(URI uri, String str, Map map) {
        throw new RuntimeException("Stub!");
    }

    public final CacheRequest put(URI uri, URLConnection uRLConnection) {
        throw new RuntimeException("Stub!");
    }
}
