package jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;

/* renamed from: jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.d  reason: case insensitive filesystem */
public final class C0003d {
    private static Intent a = null;

    /* renamed from: a  reason: collision with other field name */
    private static MediaProjection f16a;

    /* renamed from: a  reason: collision with other field name */
    private static MediaProjectionManager f17a;

    public static MediaProjection a() {
        return f16a;
    }

    @SuppressLint({"NewApi"})
    /* renamed from: a  reason: collision with other method in class */
    public static void m0a() {
        if (a != null) {
            if (f16a != null) {
                f16a.stop();
                f16a = null;
            }
            f16a = f17a.getMediaProjection(-1, (Intent) a.clone());
        }
    }

    protected static void a(Intent intent) {
        a = intent;
    }

    public static void a(MediaProjectionManager mediaProjectionManager) {
        f17a = mediaProjectionManager;
    }
}
