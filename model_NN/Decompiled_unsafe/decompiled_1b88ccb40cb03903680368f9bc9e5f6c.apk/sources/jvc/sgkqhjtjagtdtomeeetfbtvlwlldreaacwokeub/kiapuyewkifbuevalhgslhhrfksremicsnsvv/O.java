package jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;

public final class O extends AsyncTask {
    private int a;

    /* renamed from: a  reason: collision with other field name */
    private Bitmap.Config f10a;

    /* renamed from: a  reason: collision with other field name */
    private FileOutputStream f11a;

    /* renamed from: a  reason: collision with other field name */
    private String f12a;

    /* renamed from: a  reason: collision with other field name */
    private Buffer f13a;
    private int b;

    public O(String str, ByteBuffer byteBuffer, int i, int i2, Bitmap.Config config) {
        this.f13a = byteBuffer;
        this.b = i2;
        this.a = i;
        this.f10a = config;
        this.f12a = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String doInBackground(Bitmap... bitmapArr) {
        try {
            this.f11a = new FileOutputStream(this.f12a);
            bitmapArr[0] = Bitmap.createBitmap(this.a, this.b, this.f10a);
            bitmapArr[0].copyPixelsFromBuffer(this.f13a);
            bitmapArr[0].compress(Bitmap.CompressFormat.JPEG, 35, this.f11a);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return this.f12a;
    }
}
