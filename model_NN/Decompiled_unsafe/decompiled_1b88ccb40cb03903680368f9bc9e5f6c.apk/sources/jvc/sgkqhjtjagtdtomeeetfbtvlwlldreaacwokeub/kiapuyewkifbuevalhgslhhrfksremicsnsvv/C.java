package jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv;

import android.annotation.TargetApi;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;

@TargetApi(23)
public final class C implements Runnable {
    private DevicePolicyManager a;

    /* renamed from: a  reason: collision with other field name */
    private ComponentName f2a;

    /* renamed from: a  reason: collision with other field name */
    Context f3a;

    /* renamed from: a  reason: collision with other field name */
    boolean f4a = false;

    /* renamed from: a  reason: collision with other field name */
    final int[] f5a = {200};
    private boolean b = false;
    private boolean c = false;
    private boolean d = false;

    public C(Context context, DevicePolicyManager devicePolicyManager, ComponentName componentName) {
        this.a = devicePolicyManager;
        this.f3a = context;
        this.f2a = componentName;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0044, code lost:
        if (19 < jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a) goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r3) {
        /*
            r0 = 1
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0010 }
            r2 = 23
            if (r1 < r2) goto L_0x000d
            boolean r1 = android.provider.Settings.canDrawOverlays(r3)     // Catch:{ Exception -> 0x0010 }
            if (r1 == 0) goto L_0x000e
        L_0x000d:
            return r0
        L_0x000e:
            r0 = 0
            goto L_0x000d
        L_0x0010:
            r1 = move-exception
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            r2 = 44
            if (r1 > r2) goto L_0x000d
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            int r1 = r1 / 2
            r2 = 86654(0x1527e, float:1.21428E-40)
            int r1 = r1 * r2
            r2 = 88399(0x1594f, float:1.23873E-40)
            if (r1 < r2) goto L_0x000d
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            int r1 = r1 / 2
            r2 = 43554(0xaa22, float:6.1032E-41)
            int r1 = r1 * r2
            r2 = 14842(0x39fa, float:2.0798E-41)
            if (r1 < r2) goto L_0x000d
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            r2 = 86565(0x15225, float:1.21303E-40)
            if (r1 >= r2) goto L_0x0052
            r1 = 75041(0x12521, float:1.05155E-40)
            int r2 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            if (r1 != r2) goto L_0x0046
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            r1 = 19
            int r2 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            if (r1 >= r2) goto L_0x0052
        L_0x0046:
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            r2 = 37
            if (r1 >= r2) goto L_0x0052
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            r2 = -24509(0xffffffffffffa043, float:NaN)
            if (r1 >= r2) goto L_0x000d
        L_0x0052:
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            r2 = 91720(0x16648, float:1.28527E-40)
            if (r1 <= r2) goto L_0x006a
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            r2 = 62054(0xf266, float:8.6956E-41)
            if (r1 != r2) goto L_0x006e
            java.lang.String r1 = "jtuuz"
            java.lang.String r2 = "no"
            int r1 = r1.indexOf(r2)     // Catch:{ Exception -> 0x00b1 }
            if (r1 != 0) goto L_0x006e
        L_0x006a:
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            if (r1 > 0) goto L_0x0078
        L_0x006e:
            java.lang.String r1 = "chwrykfwbrqectbqcev"
            java.lang.String r2 = "gcyque"
            boolean r1 = r1.contains(r2)     // Catch:{ Exception -> 0x00b1 }
            if (r1 == 0) goto L_0x00ab
        L_0x0078:
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            r2 = 90906(0x1631a, float:1.27386E-40)
            if (r1 <= r2) goto L_0x0090
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            r2 = 81063(0x13ca7, float:1.13593E-40)
            if (r1 != r2) goto L_0x0094
            java.lang.String r1 = "entxj"
            java.lang.String r2 = "np"
            int r1 = r1.indexOf(r2)     // Catch:{ Exception -> 0x00b1 }
            if (r1 != 0) goto L_0x0094
        L_0x0090:
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00b1 }
            if (r1 > 0) goto L_0x009e
        L_0x0094:
            java.lang.String r1 = "jpypvacuvyvsrysxced"
            java.lang.String r2 = "qscyww"
            boolean r1 = r1.contains(r2)     // Catch:{ Exception -> 0x00b1 }
            if (r1 == 0) goto L_0x00a7
        L_0x009e:
            r1 = 8560(0x2170, float:1.1995E-41)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r1     // Catch:{ Exception -> 0x00b1 }
            r1 = 409445(0x63f65, float:5.73755E-40)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r1     // Catch:{ Exception -> 0x00b1 }
        L_0x00a7:
            r1 = 4096(0x1000, float:5.74E-42)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r1     // Catch:{ Exception -> 0x00b1 }
        L_0x00ab:
            r1 = 12081(0x2f31, float:1.6929E-41)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r1     // Catch:{ Exception -> 0x00b1 }
            goto L_0x000d
        L_0x00b1:
            r1 = move-exception
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a()
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.C.a(android.content.Context):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0041, code lost:
        if (19 < jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004d, code lost:
        if (jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a >= -24509) goto L_0x00ac;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean b(android.content.Context r2) {
        /*
            java.lang.String r0 = "keyguard"
            java.lang.Object r0 = r2.getSystemService(r0)     // Catch:{ Exception -> 0x000d }
            android.app.KeyguardManager r0 = (android.app.KeyguardManager) r0     // Catch:{ Exception -> 0x000d }
            boolean r0 = r0.inKeyguardRestrictedInputMode()     // Catch:{ Exception -> 0x000d }
        L_0x000c:
            return r0
        L_0x000d:
            r0 = move-exception
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            r1 = 44
            if (r0 > r1) goto L_0x00ac
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            int r0 = r0 / 2
            r1 = 86654(0x1527e, float:1.21428E-40)
            int r0 = r0 * r1
            r1 = 88399(0x1594f, float:1.23873E-40)
            if (r0 < r1) goto L_0x00ac
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            int r0 = r0 / 2
            r1 = 43554(0xaa22, float:6.1032E-41)
            int r0 = r0 * r1
            r1 = 14842(0x39fa, float:2.0798E-41)
            if (r0 < r1) goto L_0x00ac
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            r1 = 86565(0x15225, float:1.21303E-40)
            if (r0 >= r1) goto L_0x004f
            r0 = 75041(0x12521, float:1.05155E-40)
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            if (r0 != r1) goto L_0x0043
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            r0 = 19
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            if (r0 >= r1) goto L_0x004f
        L_0x0043:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            r1 = 37
            if (r0 >= r1) goto L_0x004f
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            r1 = -24509(0xffffffffffffa043, float:NaN)
            if (r0 >= r1) goto L_0x00ac
        L_0x004f:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            r1 = 91720(0x16648, float:1.28527E-40)
            if (r0 <= r1) goto L_0x0067
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            r1 = 62054(0xf266, float:8.6956E-41)
            if (r0 != r1) goto L_0x006b
            java.lang.String r0 = "jtuuz"
            java.lang.String r1 = "no"
            int r0 = r0.indexOf(r1)     // Catch:{ Exception -> 0x00af }
            if (r0 != 0) goto L_0x006b
        L_0x0067:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            if (r0 > 0) goto L_0x0075
        L_0x006b:
            java.lang.String r0 = "chwrykfwbrqectbqcev"
            java.lang.String r1 = "gcyque"
            boolean r0 = r0.contains(r1)     // Catch:{ Exception -> 0x00af }
            if (r0 == 0) goto L_0x00a8
        L_0x0075:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            r1 = 90906(0x1631a, float:1.27386E-40)
            if (r0 <= r1) goto L_0x008d
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            r1 = 81063(0x13ca7, float:1.13593E-40)
            if (r0 != r1) goto L_0x0091
            java.lang.String r0 = "entxj"
            java.lang.String r1 = "np"
            int r0 = r0.indexOf(r1)     // Catch:{ Exception -> 0x00af }
            if (r0 != 0) goto L_0x0091
        L_0x008d:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x00af }
            if (r0 > 0) goto L_0x009b
        L_0x0091:
            java.lang.String r0 = "jpypvacuvyvsrysxced"
            java.lang.String r1 = "qscyww"
            boolean r0 = r0.contains(r1)     // Catch:{ Exception -> 0x00af }
            if (r0 == 0) goto L_0x00a4
        L_0x009b:
            r0 = 8560(0x2170, float:1.1995E-41)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x00af }
            r0 = 409445(0x63f65, float:5.73755E-40)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x00af }
        L_0x00a4:
            r0 = 4096(0x1000, float:5.74E-42)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x00af }
        L_0x00a8:
            r0 = 12081(0x2f31, float:1.6929E-41)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x00af }
        L_0x00ac:
            r0 = 0
            goto L_0x000c
        L_0x00af:
            r0 = move-exception
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a()
            goto L_0x00ac
        */
        throw new UnsupportedOperationException("Method not decompiled: jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.C.b(android.content.Context):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:115:0x025f, code lost:
        if (19 < jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a) goto L_0x0261;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0269, code lost:
        if (jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a >= -24509) goto L_0x00ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01bc, code lost:
        if (19 < jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a) goto L_0x01be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01c6, code lost:
        if (jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a >= -24509) goto L_0x00c7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x008a A[Catch:{ Exception -> 0x018a }] */
    @android.annotation.SuppressLint({"NewApi"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r14 = this;
            r13 = -24509(0xffffffffffffa043, float:NaN)
            r12 = 23
            r11 = 19
            r10 = 1
            r9 = 0
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.t r7 = new jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.t
            android.content.Context r0 = r14.f3a
            r7.<init>(r0)
            android.database.Cursor r0 = r7.m3a()
            android.content.Context r1 = r14.f3a     // Catch:{ Exception -> 0x022d }
            boolean r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.jleqvmdifhbavistjrzpugyrganfbhfqcotshejsmnezcuxygwmiwkdqybwtvxlkxnaruocplkopzd.a(r1)     // Catch:{ Exception -> 0x022d }
            if (r1 == 0) goto L_0x00c7
            if (r0 == 0) goto L_0x0081
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x022d }
            if (r1 == 0) goto L_0x0081
        L_0x0023:
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x022d }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x022d }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Exception -> 0x022d }
            android.content.Context r4 = r14.f3a     // Catch:{ Exception -> 0x022d }
            java.lang.String r4 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.C0007h.a(r4)     // Catch:{ Exception -> 0x022d }
            java.lang.String r5 = "="
            java.lang.String[] r2 = r2.split(r5)     // Catch:{ Exception -> 0x022d }
            r5 = 1
            r2 = r2[r5]     // Catch:{ Exception -> 0x022d }
            java.lang.String r5 = "="
            java.lang.String[] r3 = r3.split(r5)     // Catch:{ Exception -> 0x022d }
            r5 = 1
            r3 = r3[r5]     // Catch:{ Exception -> 0x022d }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x022d }
            r6 = 3
            r5.<init>(r6)     // Catch:{ Exception -> 0x022d }
            org.apache.http.message.BasicNameValuePair r6 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x022d }
            java.lang.String r8 = "action"
            r6.<init>(r8, r2)     // Catch:{ Exception -> 0x022d }
            r5.add(r6)     // Catch:{ Exception -> 0x022d }
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x022d }
            java.lang.String r6 = "imei"
            r2.<init>(r6, r4)     // Catch:{ Exception -> 0x022d }
            r5.add(r2)     // Catch:{ Exception -> 0x022d }
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x022d }
            java.lang.String r4 = "data"
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x022d }
            r2.<init>(r4, r3)     // Catch:{ Exception -> 0x022d }
            r5.add(r2)     // Catch:{ Exception -> 0x022d }
            android.content.Context r2 = r14.f3a     // Catch:{ Exception -> 0x022d }
            r3 = 1
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.jleqvmdifhbavistjrzpugyrganfbhfqcotshejsmnezcuxygwmiwkdqybwtvxlkxnaruocplkopzd.a(r2, r5, r3)     // Catch:{ Exception -> 0x022d }
            r7.a(r1)     // Catch:{ Exception -> 0x022d }
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x022d }
            if (r1 != 0) goto L_0x0023
        L_0x0081:
            android.content.Context r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.f38a     // Catch:{ Exception -> 0x018a }
            r1 = 0
            java.io.File r0 = r0.getExternalFilesDir(r1)     // Catch:{ Exception -> 0x018a }
            if (r0 == 0) goto L_0x00c7
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x018a }
            r1.<init>()     // Catch:{ Exception -> 0x018a }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x018a }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x018a }
            java.lang.String r1 = "/.s_klasse/"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x018a }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x018a }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x018a }
            r1.<init>(r0)     // Catch:{ Exception -> 0x018a }
            boolean r0 = r1.exists()     // Catch:{ Exception -> 0x018a }
            if (r0 == 0) goto L_0x00c7
            java.io.File[] r0 = r1.listFiles()     // Catch:{ Exception -> 0x018a }
            int r1 = r0.length     // Catch:{ Exception -> 0x018a }
            if (r1 <= 0) goto L_0x00c7
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.y r1 = new jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.y     // Catch:{ Exception -> 0x018a }
            r2 = 0
            r0 = r0[r2]     // Catch:{ Exception -> 0x018a }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x018a }
            android.content.Context r2 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.f38a     // Catch:{ Exception -> 0x018a }
            r1.<init>(r0, r2)     // Catch:{ Exception -> 0x018a }
            r0 = 0
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x018a }
            r1.execute(r0)     // Catch:{ Exception -> 0x018a }
        L_0x00c7:
            r7.close()     // Catch:{ Exception -> 0x022d }
        L_0x00ca:
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r12) goto L_0x02d4
            boolean r0 = r14.d
            if (r0 != 0) goto L_0x00da
            android.content.Context r0 = r14.f3a
            boolean r0 = a(r0)
            r14.d = r0
        L_0x00da:
            boolean r0 = r14.d
            if (r0 != 0) goto L_0x02d0
            boolean r0 = r14.b
            if (r0 != 0) goto L_0x0100
            r14.b = r10
            java.util.Timer r0 = new java.util.Timer
            r0.<init>()
            int[] r4 = new int[r10]
            r1 = 200(0xc8, float:2.8E-43)
            r4[r9] = r1
            int[] r2 = new int[r10]
            r2[r9] = r9
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.E r1 = new jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.E
            r1.<init>(r14, r2, r4, r0)
            r2 = 100
            r4 = r4[r9]
            long r4 = (long) r4
            r0.scheduleAtFixedRate(r1, r2, r4)
        L_0x0100:
            java.lang.String r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.I.h
            android.content.Context r1 = r14.f3a
            boolean r8 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a(r0, r1)
            android.content.Intent r2 = new android.content.Intent
            android.content.Context r0 = r14.f3a
            java.lang.Class<jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.zeauyilegittpkypkubgorixqhxfamfzvroygmclqxklsbdsrczhnsvwhvaftddocenpwjjjmwqubn> r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.zeauyilegittpkypkubgorixqhxfamfzvroygmclqxklsbdsrczhnsvwhvaftddocenpwjjjmwqubn.class
            r2.<init>(r0, r1)
            android.content.Intent r6 = new android.content.Intent
            android.content.Context r0 = r14.f3a
            java.lang.Class<jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.tsunfavfdqcypmyvjfpaadueghmxjhnlowhzzsxjukgiyglcerqqbbtrtxiznwbkeckilovpsdwrmo> r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.tsunfavfdqcypmyvjfpaadueghmxjhnlowhzzsxjukgiyglcerqqbbtrtxiznwbkeckilovpsdwrmo.class
            r6.<init>(r0, r1)
            if (r8 != 0) goto L_0x013f
            boolean r0 = r14.d
            if (r0 == 0) goto L_0x013f
            boolean r0 = r14.f4a
            if (r0 != 0) goto L_0x013f
            r14.f4a = r10
            android.content.Context r0 = r14.f3a
            r0.startService(r2)
            java.util.Timer r0 = new java.util.Timer
            r0.<init>()
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.D r1 = new jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.D
            r1.<init>(r14, r2, r0)
            r2 = 100
            int[] r4 = r14.f5a
            r4 = r4[r9]
            long r4 = (long) r4
            r0.scheduleAtFixedRate(r1, r2, r4)
        L_0x013f:
            android.app.admin.DevicePolicyManager r0 = r14.a
            android.content.ComponentName r1 = r14.f2a
            boolean r1 = r0.isAdminActive(r1)
            android.content.Context r0 = r14.f3a
            boolean r2 = b(r0)
            boolean r0 = r14.d
            if (r0 == 0) goto L_0x0162
            if (r8 == 0) goto L_0x0162
            if (r1 != 0) goto L_0x0162
            if (r2 == 0) goto L_0x02d8
            boolean r0 = r14.c
            if (r0 == 0) goto L_0x0162
            android.content.Context r0 = r14.f3a
            r0.stopService(r6)
            r14.c = r9
        L_0x0162:
            java.lang.String r0 = "dchec2k2"
            java.lang.String r0 = r7.m4a(r0)
            boolean r3 = r14.d
            if (r3 == 0) goto L_0x0186
            if (r8 == 0) goto L_0x0186
            if (r1 == 0) goto L_0x0186
            int r1 = android.os.Build.VERSION.SDK_INT
            if (r1 < r12) goto L_0x030e
            if (r0 != 0) goto L_0x030e
            r0 = 4
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.qsrccvgemwdivuzhjqmkkxosywfoetxmgtjbgdlqlcruhdbivbpkoujznrxpspifznynaywatfaelh.a = r0
            if (r2 == 0) goto L_0x0301
            boolean r0 = r14.c
            if (r0 == 0) goto L_0x0186
            android.content.Context r0 = r14.f3a
            r0.stopService(r6)
            r14.c = r9
        L_0x0186:
            r7.close()
            return
        L_0x018a:
            r0 = move-exception
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            r1 = 44
            if (r0 > r1) goto L_0x00c7
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            int r0 = r0 / 2
            r1 = 86654(0x1527e, float:1.21428E-40)
            int r0 = r0 * r1
            r1 = 88399(0x1594f, float:1.23873E-40)
            if (r0 < r1) goto L_0x00c7
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            int r0 = r0 / 2
            r1 = 43554(0xaa22, float:6.1032E-41)
            int r0 = r0 * r1
            r1 = 14842(0x39fa, float:2.0798E-41)
            if (r0 < r1) goto L_0x00c7
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            r1 = 86565(0x15225, float:1.21303E-40)
            if (r0 >= r1) goto L_0x01c8
            r0 = 75041(0x12521, float:1.05155E-40)
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            if (r0 != r1) goto L_0x01be
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            if (r11 >= r0) goto L_0x01c8
        L_0x01be:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            r1 = 37
            if (r0 >= r1) goto L_0x01c8
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            if (r0 >= r13) goto L_0x00c7
        L_0x01c8:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            r1 = 91720(0x16648, float:1.28527E-40)
            if (r0 <= r1) goto L_0x01e0
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            r1 = 62054(0xf266, float:8.6956E-41)
            if (r0 != r1) goto L_0x01e4
            java.lang.String r0 = "jtuuz"
            java.lang.String r1 = "no"
            int r0 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0227 }
            if (r0 != 0) goto L_0x01e4
        L_0x01e0:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            if (r0 > 0) goto L_0x01ee
        L_0x01e4:
            java.lang.String r0 = "chwrykfwbrqectbqcev"
            java.lang.String r1 = "gcyque"
            boolean r0 = r0.contains(r1)     // Catch:{ Exception -> 0x0227 }
            if (r0 == 0) goto L_0x0221
        L_0x01ee:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            r1 = 90906(0x1631a, float:1.27386E-40)
            if (r0 <= r1) goto L_0x0206
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            r1 = 81063(0x13ca7, float:1.13593E-40)
            if (r0 != r1) goto L_0x020a
            java.lang.String r0 = "entxj"
            java.lang.String r1 = "np"
            int r0 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0227 }
            if (r0 != 0) goto L_0x020a
        L_0x0206:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x0227 }
            if (r0 > 0) goto L_0x0214
        L_0x020a:
            java.lang.String r0 = "jpypvacuvyvsrysxced"
            java.lang.String r1 = "qscyww"
            boolean r0 = r0.contains(r1)     // Catch:{ Exception -> 0x0227 }
            if (r0 == 0) goto L_0x021d
        L_0x0214:
            r0 = 8560(0x2170, float:1.1995E-41)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x0227 }
            r0 = 409445(0x63f65, float:5.73755E-40)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x0227 }
        L_0x021d:
            r0 = 4096(0x1000, float:5.74E-42)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x0227 }
        L_0x0221:
            r0 = 12081(0x2f31, float:1.6929E-41)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x0227 }
            goto L_0x00c7
        L_0x0227:
            r0 = move-exception
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a()     // Catch:{ Exception -> 0x022d }
            goto L_0x00c7
        L_0x022d:
            r0 = move-exception
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            r1 = 44
            if (r0 > r1) goto L_0x00ca
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            int r0 = r0 / 2
            r1 = 86654(0x1527e, float:1.21428E-40)
            int r0 = r0 * r1
            r1 = 88399(0x1594f, float:1.23873E-40)
            if (r0 < r1) goto L_0x00ca
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            int r0 = r0 / 2
            r1 = 43554(0xaa22, float:6.1032E-41)
            int r0 = r0 * r1
            r1 = 14842(0x39fa, float:2.0798E-41)
            if (r0 < r1) goto L_0x00ca
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            r1 = 86565(0x15225, float:1.21303E-40)
            if (r0 >= r1) goto L_0x026b
            r0 = 75041(0x12521, float:1.05155E-40)
            int r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            if (r0 != r1) goto L_0x0261
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            if (r11 >= r0) goto L_0x026b
        L_0x0261:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            r1 = 37
            if (r0 >= r1) goto L_0x026b
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            if (r0 >= r13) goto L_0x00ca
        L_0x026b:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            r1 = 91720(0x16648, float:1.28527E-40)
            if (r0 <= r1) goto L_0x0283
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            r1 = 62054(0xf266, float:8.6956E-41)
            if (r0 != r1) goto L_0x0287
            java.lang.String r0 = "jtuuz"
            java.lang.String r1 = "no"
            int r0 = r0.indexOf(r1)     // Catch:{ Exception -> 0x02ca }
            if (r0 != 0) goto L_0x0287
        L_0x0283:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            if (r0 > 0) goto L_0x0291
        L_0x0287:
            java.lang.String r0 = "chwrykfwbrqectbqcev"
            java.lang.String r1 = "gcyque"
            boolean r0 = r0.contains(r1)     // Catch:{ Exception -> 0x02ca }
            if (r0 == 0) goto L_0x02c4
        L_0x0291:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            r1 = 90906(0x1631a, float:1.27386E-40)
            if (r0 <= r1) goto L_0x02a9
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            r1 = 81063(0x13ca7, float:1.13593E-40)
            if (r0 != r1) goto L_0x02ad
            java.lang.String r0 = "entxj"
            java.lang.String r1 = "np"
            int r0 = r0.indexOf(r1)     // Catch:{ Exception -> 0x02ca }
            if (r0 != 0) goto L_0x02ad
        L_0x02a9:
            int r0 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a     // Catch:{ Exception -> 0x02ca }
            if (r0 > 0) goto L_0x02b7
        L_0x02ad:
            java.lang.String r0 = "jpypvacuvyvsrysxced"
            java.lang.String r1 = "qscyww"
            boolean r0 = r0.contains(r1)     // Catch:{ Exception -> 0x02ca }
            if (r0 == 0) goto L_0x02c0
        L_0x02b7:
            r0 = 8560(0x2170, float:1.1995E-41)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x02ca }
            r0 = 409445(0x63f65, float:5.73755E-40)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x02ca }
        L_0x02c0:
            r0 = 4096(0x1000, float:5.74E-42)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x02ca }
        L_0x02c4:
            r0 = 12081(0x2f31, float:1.6929E-41)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a = r0     // Catch:{ Exception -> 0x02ca }
            goto L_0x00ca
        L_0x02ca:
            r0 = move-exception
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.a()
            goto L_0x00ca
        L_0x02d0:
            r14.b = r9
            goto L_0x0100
        L_0x02d4:
            r14.d = r10
            goto L_0x0100
        L_0x02d8:
            boolean r0 = r14.c
            if (r0 != 0) goto L_0x03ba
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r11) goto L_0x02ee
            android.content.Context r0 = r14.f3a
            r0.startService(r6)
            r14.c = r10
            r0 = r6
        L_0x02e8:
            r3 = 3
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.qsrccvgemwdivuzhjqmkkxosywfoetxmgtjbgdlqlcruhdbivbpkoujznrxpspifznynaywatfaelh.a = r3
            r6 = r0
            goto L_0x0162
        L_0x02ee:
            android.content.Intent r0 = new android.content.Intent
            android.content.Context r3 = r14.f3a
            java.lang.Class<jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.pevuphdgxzoxibrophsewnqmdrgcaryazkoqztyduxjsjmqlmseatijywntkclnvbiwfcluhbkfvgf> r4 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.pevuphdgxzoxibrophsewnqmdrgcaryazkoqztyduxjsjmqlmseatijywntkclnvbiwfcluhbkfvgf.class
            r0.<init>(r3, r4)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.pevuphdgxzoxibrophsewnqmdrgcaryazkoqztyduxjsjmqlmseatijywntkclnvbiwfcluhbkfvgf.a = r10
            android.content.Context r3 = r14.f3a
            r3.startService(r0)
            r14.c = r10
            goto L_0x02e8
        L_0x0301:
            boolean r0 = r14.c
            if (r0 != 0) goto L_0x0186
            android.content.Context r0 = r14.f3a
            r0.startService(r6)
            r14.c = r10
            goto L_0x0186
        L_0x030e:
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r11) goto L_0x03ad
            android.content.Context r0 = r14.f3a
            java.lang.String r0 = r0.getPackageName()
            android.content.Context r1 = r14.f3a
            java.lang.String r1 = android.provider.Telephony.Sms.getDefaultSmsPackage(r1)
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0357
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 >= r12) goto L_0x0339
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 21
            if (r0 < r1) goto L_0x0339
            boolean r0 = r14.c
            if (r0 == 0) goto L_0x0339
            android.content.Context r0 = r14.f3a
            r0.stopService(r6)
            r14.c = r9
        L_0x0339:
            android.content.Context r0 = r14.f3a
            java.lang.String r0 = r0.getPackageName()
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "android.provider.Telephony.ACTION_CHANGE_DEFAULT"
            r1.<init>(r2)
            java.lang.String r2 = "package"
            r1.putExtra(r2, r0)
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r0)
            android.content.Context r0 = r14.f3a
            r0.startActivity(r1)
            goto L_0x0186
        L_0x0357:
            android.content.Context r0 = r14.f3a
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.t r1 = new jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.t
            r1.<init>(r0)
            java.lang.String r3 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.I.g
            java.lang.String r3 = r1.m4a(r3)
            if (r3 != 0) goto L_0x0370
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.n r3 = new jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.n
            r3.<init>(r0)
            java.lang.String[] r0 = new java.lang.String[r9]
            r3.execute(r0)
        L_0x0370:
            r1.close()
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 21
            if (r0 < r1) goto L_0x03a0
            android.content.Intent r0 = new android.content.Intent
            android.content.Context r1 = r14.f3a
            java.lang.Class<jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.wlxyhrduobsovdunjqqsktmvrgiaetvljbipwpzpheyxxmgzdfoylhmzgcbajifqsketwcaunrcnfk> r3 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.wlxyhrduobsovdunjqqsktmvrgiaetvljbipwpzpheyxxmgzdfoylhmzgcbajifqsketwcaunrcnfk.class
            r0.<init>(r1, r3)
            boolean r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.f41a
            if (r1 != 0) goto L_0x038f
            if (r2 != 0) goto L_0x038f
            android.content.Context r1 = r14.f3a
            r1.startService(r0)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.f41a = r10
        L_0x038f:
            boolean r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.f41a
            if (r1 == 0) goto L_0x03a0
            boolean r1 = jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.nfdxusccohwcsezxvupgijektulrfaylhrpzbmonwqnyadqkbtkvfibrmwpmyzjdslojivhaeqgtgx.d
            if (r1 != 0) goto L_0x03a0
            android.content.Context r1 = r14.f3a
            r1.stopService(r0)
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.uhbphqtjpcyywnvsjuqmuojhdnbmbqxfsvgxaltaetaipyoimvlncrckzgkxfzklrgewodiszrdewf.f41a = r9
            jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.nfdxusccohwcsezxvupgijektulrfaylhrpzbmonwqnyadqkbtkvfibrmwpmyzjdslojivhaeqgtgx.c = r10
        L_0x03a0:
            boolean r0 = r14.c
            if (r0 == 0) goto L_0x0186
            android.content.Context r0 = r14.f3a
            r0.stopService(r6)
            r14.c = r9
            goto L_0x0186
        L_0x03ad:
            boolean r0 = r14.c
            if (r0 == 0) goto L_0x0186
            android.content.Context r0 = r14.f3a
            r0.stopService(r6)
            r14.c = r9
            goto L_0x0186
        L_0x03ba:
            r0 = r6
            goto L_0x02e8
        */
        throw new UnsupportedOperationException("Method not decompiled: jvc.sgkqhjtjagtdtomeeetfbtvlwlldreaacwokeub.kiapuyewkifbuevalhgslhhrfksremicsnsvv.C.run():void");
    }
}
