package org.apache.commons.codec.net;

@Deprecated
abstract class RFC1522Codec {
    RFC1522Codec() {
        throw new RuntimeException("Stub!");
    }

    /* access modifiers changed from: protected */
    public String decodeText(String str) {
        throw new RuntimeException("Stub!");
    }

    /* access modifiers changed from: protected */
    public abstract byte[] doDecoding(byte[] bArr);

    /* access modifiers changed from: protected */
    public abstract byte[] doEncoding(byte[] bArr);

    /* access modifiers changed from: protected */
    public String encodeText(String str, String str2) {
        throw new RuntimeException("Stub!");
    }

    /* access modifiers changed from: protected */
    public abstract String getEncoding();
}
