package org.apache.commons.logging.impl;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

@Deprecated
public final class WeakHashtable extends Hashtable {
    public WeakHashtable() {
        throw new RuntimeException("Stub!");
    }

    public final boolean containsKey(Object obj) {
        throw new RuntimeException("Stub!");
    }

    public final Enumeration elements() {
        throw new RuntimeException("Stub!");
    }

    public final Set entrySet() {
        throw new RuntimeException("Stub!");
    }

    public final Object get(Object obj) {
        throw new RuntimeException("Stub!");
    }

    public final boolean isEmpty() {
        throw new RuntimeException("Stub!");
    }

    public final Set keySet() {
        throw new RuntimeException("Stub!");
    }

    public final Enumeration keys() {
        throw new RuntimeException("Stub!");
    }

    public final Object put(Object obj, Object obj2) {
        throw new RuntimeException("Stub!");
    }

    public final void putAll(Map map) {
        throw new RuntimeException("Stub!");
    }

    /* access modifiers changed from: protected */
    public final void rehash() {
        throw new RuntimeException("Stub!");
    }

    public final Object remove(Object obj) {
        throw new RuntimeException("Stub!");
    }

    public final int size() {
        throw new RuntimeException("Stub!");
    }

    public final String toString() {
        throw new RuntimeException("Stub!");
    }

    public final Collection values() {
        throw new RuntimeException("Stub!");
    }
}
