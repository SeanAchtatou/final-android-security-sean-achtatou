package org.apache.http.util;

@Deprecated
public final class CharArrayBuffer {
    public CharArrayBuffer(int i) {
        throw new RuntimeException("Stub!");
    }

    public final void append(char c) {
        throw new RuntimeException("Stub!");
    }

    public final void append(Object obj) {
        throw new RuntimeException("Stub!");
    }

    public final void append(String str) {
        throw new RuntimeException("Stub!");
    }

    public final void append(ByteArrayBuffer byteArrayBuffer, int i, int i2) {
        throw new RuntimeException("Stub!");
    }

    public final void append(CharArrayBuffer charArrayBuffer) {
        throw new RuntimeException("Stub!");
    }

    public final void append(CharArrayBuffer charArrayBuffer, int i, int i2) {
        throw new RuntimeException("Stub!");
    }

    public final void append(byte[] bArr, int i, int i2) {
        throw new RuntimeException("Stub!");
    }

    public final void append(char[] cArr, int i, int i2) {
        throw new RuntimeException("Stub!");
    }

    public final char[] buffer() {
        throw new RuntimeException("Stub!");
    }

    public final int capacity() {
        throw new RuntimeException("Stub!");
    }

    public final char charAt(int i) {
        throw new RuntimeException("Stub!");
    }

    public final void clear() {
        throw new RuntimeException("Stub!");
    }

    public final void ensureCapacity(int i) {
        throw new RuntimeException("Stub!");
    }

    public final int indexOf(int i) {
        throw new RuntimeException("Stub!");
    }

    public final int indexOf(int i, int i2, int i3) {
        throw new RuntimeException("Stub!");
    }

    public final boolean isEmpty() {
        throw new RuntimeException("Stub!");
    }

    public final boolean isFull() {
        throw new RuntimeException("Stub!");
    }

    public final int length() {
        throw new RuntimeException("Stub!");
    }

    public final void setLength(int i) {
        throw new RuntimeException("Stub!");
    }

    public final String substring(int i, int i2) {
        throw new RuntimeException("Stub!");
    }

    public final String substringTrimmed(int i, int i2) {
        throw new RuntimeException("Stub!");
    }

    public final char[] toCharArray() {
        throw new RuntimeException("Stub!");
    }

    public final String toString() {
        throw new RuntimeException("Stub!");
    }
}
