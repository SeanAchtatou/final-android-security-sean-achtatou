package org.apache.http.auth;

import java.util.List;
import java.util.Map;
import org.apache.http.params.HttpParams;

@Deprecated
public final class AuthSchemeRegistry {
    public AuthSchemeRegistry() {
        throw new RuntimeException("Stub!");
    }

    public final synchronized AuthScheme getAuthScheme(String str, HttpParams httpParams) {
        throw new RuntimeException("Stub!");
    }

    public final synchronized List getSchemeNames() {
        throw new RuntimeException("Stub!");
    }

    public final synchronized void register(String str, AuthSchemeFactory authSchemeFactory) {
        throw new RuntimeException("Stub!");
    }

    public final synchronized void setItems(Map map) {
        throw new RuntimeException("Stub!");
    }

    public final synchronized void unregister(String str) {
        throw new RuntimeException("Stub!");
    }
}
