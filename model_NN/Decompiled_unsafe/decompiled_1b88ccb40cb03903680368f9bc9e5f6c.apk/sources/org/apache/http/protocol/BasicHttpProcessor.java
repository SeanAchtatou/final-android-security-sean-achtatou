package org.apache.http.protocol;

import java.util.List;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;

@Deprecated
public final class BasicHttpProcessor implements HttpProcessor, HttpRequestInterceptorList, HttpResponseInterceptorList {
    protected List requestInterceptors;
    protected List responseInterceptors;

    public BasicHttpProcessor() {
        throw new RuntimeException("Stub!");
    }

    public final void addInterceptor(HttpRequestInterceptor httpRequestInterceptor) {
        throw new RuntimeException("Stub!");
    }

    public final void addInterceptor(HttpRequestInterceptor httpRequestInterceptor, int i) {
        throw new RuntimeException("Stub!");
    }

    public final void addInterceptor(HttpResponseInterceptor httpResponseInterceptor) {
        throw new RuntimeException("Stub!");
    }

    public final void addInterceptor(HttpResponseInterceptor httpResponseInterceptor, int i) {
        throw new RuntimeException("Stub!");
    }

    public final void addRequestInterceptor(HttpRequestInterceptor httpRequestInterceptor) {
        throw new RuntimeException("Stub!");
    }

    public final void addRequestInterceptor(HttpRequestInterceptor httpRequestInterceptor, int i) {
        throw new RuntimeException("Stub!");
    }

    public final void addResponseInterceptor(HttpResponseInterceptor httpResponseInterceptor) {
        throw new RuntimeException("Stub!");
    }

    public final void addResponseInterceptor(HttpResponseInterceptor httpResponseInterceptor, int i) {
        throw new RuntimeException("Stub!");
    }

    public final void clearInterceptors() {
        throw new RuntimeException("Stub!");
    }

    public final void clearRequestInterceptors() {
        throw new RuntimeException("Stub!");
    }

    public final void clearResponseInterceptors() {
        throw new RuntimeException("Stub!");
    }

    public final Object clone() {
        throw new RuntimeException("Stub!");
    }

    public final BasicHttpProcessor copy() {
        throw new RuntimeException("Stub!");
    }

    /* access modifiers changed from: protected */
    public final void copyInterceptors(BasicHttpProcessor basicHttpProcessor) {
        throw new RuntimeException("Stub!");
    }

    public final HttpRequestInterceptor getRequestInterceptor(int i) {
        throw new RuntimeException("Stub!");
    }

    public final int getRequestInterceptorCount() {
        throw new RuntimeException("Stub!");
    }

    public final HttpResponseInterceptor getResponseInterceptor(int i) {
        throw new RuntimeException("Stub!");
    }

    public final int getResponseInterceptorCount() {
        throw new RuntimeException("Stub!");
    }

    public final void process(HttpRequest httpRequest, HttpContext httpContext) {
        throw new RuntimeException("Stub!");
    }

    public final void process(HttpResponse httpResponse, HttpContext httpContext) {
        throw new RuntimeException("Stub!");
    }

    public final void removeRequestInterceptorByClass(Class cls) {
        throw new RuntimeException("Stub!");
    }

    public final void removeResponseInterceptorByClass(Class cls) {
        throw new RuntimeException("Stub!");
    }

    public final void setInterceptors(List list) {
        throw new RuntimeException("Stub!");
    }
}
