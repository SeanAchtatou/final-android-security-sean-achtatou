package org.apache.http.conn.scheme;

import java.net.InetAddress;
import java.net.Socket;
import org.apache.http.params.HttpParams;

@Deprecated
public final class PlainSocketFactory implements SocketFactory {
    public PlainSocketFactory() {
        throw new RuntimeException("Stub!");
    }

    public PlainSocketFactory(HostNameResolver hostNameResolver) {
        throw new RuntimeException("Stub!");
    }

    public static PlainSocketFactory getSocketFactory() {
        throw new RuntimeException("Stub!");
    }

    public final Socket connectSocket(Socket socket, String str, int i, InetAddress inetAddress, int i2, HttpParams httpParams) {
        throw new RuntimeException("Stub!");
    }

    public final Socket createSocket() {
        throw new RuntimeException("Stub!");
    }

    public final boolean equals(Object obj) {
        throw new RuntimeException("Stub!");
    }

    public final int hashCode() {
        throw new RuntimeException("Stub!");
    }

    public final boolean isSecure(Socket socket) {
        throw new RuntimeException("Stub!");
    }
}
