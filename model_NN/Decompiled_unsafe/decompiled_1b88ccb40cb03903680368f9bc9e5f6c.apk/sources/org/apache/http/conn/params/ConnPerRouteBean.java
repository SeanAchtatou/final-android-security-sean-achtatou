package org.apache.http.conn.params;

import java.util.Map;
import org.apache.http.conn.routing.HttpRoute;

@Deprecated
public final class ConnPerRouteBean implements ConnPerRoute {
    public static final int DEFAULT_MAX_CONNECTIONS_PER_ROUTE = 2;

    public ConnPerRouteBean() {
        throw new RuntimeException("Stub!");
    }

    public ConnPerRouteBean(int i) {
        throw new RuntimeException("Stub!");
    }

    public final int getDefaultMax() {
        throw new RuntimeException("Stub!");
    }

    public final int getMaxForRoute(HttpRoute httpRoute) {
        throw new RuntimeException("Stub!");
    }

    public final void setDefaultMaxPerRoute(int i) {
        throw new RuntimeException("Stub!");
    }

    public final void setMaxForRoute(HttpRoute httpRoute, int i) {
        throw new RuntimeException("Stub!");
    }

    public final void setMaxForRoutes(Map map) {
        throw new RuntimeException("Stub!");
    }
}
