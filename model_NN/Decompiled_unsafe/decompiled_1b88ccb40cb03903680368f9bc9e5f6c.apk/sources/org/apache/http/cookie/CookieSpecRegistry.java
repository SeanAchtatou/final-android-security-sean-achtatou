package org.apache.http.cookie;

import java.util.List;
import java.util.Map;
import org.apache.http.params.HttpParams;

@Deprecated
public final class CookieSpecRegistry {
    public CookieSpecRegistry() {
        throw new RuntimeException("Stub!");
    }

    public final synchronized CookieSpec getCookieSpec(String str) {
        throw new RuntimeException("Stub!");
    }

    public final synchronized CookieSpec getCookieSpec(String str, HttpParams httpParams) {
        throw new RuntimeException("Stub!");
    }

    public final synchronized List getSpecNames() {
        throw new RuntimeException("Stub!");
    }

    public final synchronized void register(String str, CookieSpecFactory cookieSpecFactory) {
        throw new RuntimeException("Stub!");
    }

    public final synchronized void setItems(Map map) {
        throw new RuntimeException("Stub!");
    }

    public final synchronized void unregister(String str) {
        throw new RuntimeException("Stub!");
    }
}
