package org.apache.http;

@Deprecated
public final class HttpHost {
    public static final String DEFAULT_SCHEME_NAME = "http";
    protected final String hostname;
    protected final String lcHostname;
    protected final int port;
    protected final String schemeName;

    public HttpHost(String str) {
        throw new RuntimeException("Stub!");
    }

    public HttpHost(String str, int i) {
        throw new RuntimeException("Stub!");
    }

    public HttpHost(String str, int i, String str2) {
        throw new RuntimeException("Stub!");
    }

    public HttpHost(HttpHost httpHost) {
        throw new RuntimeException("Stub!");
    }

    public final Object clone() {
        throw new RuntimeException("Stub!");
    }

    public final boolean equals(Object obj) {
        throw new RuntimeException("Stub!");
    }

    public final String getHostName() {
        throw new RuntimeException("Stub!");
    }

    public final int getPort() {
        throw new RuntimeException("Stub!");
    }

    public final String getSchemeName() {
        throw new RuntimeException("Stub!");
    }

    public final int hashCode() {
        throw new RuntimeException("Stub!");
    }

    public final String toHostString() {
        throw new RuntimeException("Stub!");
    }

    public final String toString() {
        throw new RuntimeException("Stub!");
    }

    public final String toURI() {
        throw new RuntimeException("Stub!");
    }
}
