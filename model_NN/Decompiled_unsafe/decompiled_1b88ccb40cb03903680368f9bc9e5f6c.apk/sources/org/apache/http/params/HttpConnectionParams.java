package org.apache.http.params;

@Deprecated
public final class HttpConnectionParams implements CoreConnectionPNames {
    HttpConnectionParams() {
        throw new RuntimeException("Stub!");
    }

    public static int getConnectionTimeout(HttpParams httpParams) {
        throw new RuntimeException("Stub!");
    }

    public static int getLinger(HttpParams httpParams) {
        throw new RuntimeException("Stub!");
    }

    public static int getSoTimeout(HttpParams httpParams) {
        throw new RuntimeException("Stub!");
    }

    public static int getSocketBufferSize(HttpParams httpParams) {
        throw new RuntimeException("Stub!");
    }

    public static boolean getTcpNoDelay(HttpParams httpParams) {
        throw new RuntimeException("Stub!");
    }

    public static boolean isStaleCheckingEnabled(HttpParams httpParams) {
        throw new RuntimeException("Stub!");
    }

    public static void setConnectionTimeout(HttpParams httpParams, int i) {
        throw new RuntimeException("Stub!");
    }

    public static void setLinger(HttpParams httpParams, int i) {
        throw new RuntimeException("Stub!");
    }

    public static void setSoTimeout(HttpParams httpParams, int i) {
        throw new RuntimeException("Stub!");
    }

    public static void setSocketBufferSize(HttpParams httpParams, int i) {
        throw new RuntimeException("Stub!");
    }

    public static void setStaleCheckingEnabled(HttpParams httpParams, boolean z) {
        throw new RuntimeException("Stub!");
    }

    public static void setTcpNoDelay(HttpParams httpParams, boolean z) {
        throw new RuntimeException("Stub!");
    }
}
