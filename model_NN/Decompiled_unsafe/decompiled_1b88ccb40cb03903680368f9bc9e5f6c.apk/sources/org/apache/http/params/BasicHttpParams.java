package org.apache.http.params;

import java.io.Serializable;

@Deprecated
public final class BasicHttpParams extends AbstractHttpParams implements Serializable {
    public BasicHttpParams() {
        throw new RuntimeException("Stub!");
    }

    public final void clear() {
        throw new RuntimeException("Stub!");
    }

    public final Object clone() {
        throw new RuntimeException("Stub!");
    }

    public final HttpParams copy() {
        throw new RuntimeException("Stub!");
    }

    /* access modifiers changed from: protected */
    public final void copyParams(HttpParams httpParams) {
        throw new RuntimeException("Stub!");
    }

    public final Object getParameter(String str) {
        throw new RuntimeException("Stub!");
    }

    public final boolean isParameterSet(String str) {
        throw new RuntimeException("Stub!");
    }

    public final boolean isParameterSetLocally(String str) {
        throw new RuntimeException("Stub!");
    }

    public final boolean removeParameter(String str) {
        throw new RuntimeException("Stub!");
    }

    public final HttpParams setParameter(String str, Object obj) {
        throw new RuntimeException("Stub!");
    }

    public final void setParameters(String[] strArr, Object obj) {
        throw new RuntimeException("Stub!");
    }
}
