package com.itfunz.itfunzsupertools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.File;

public class SimplifyProgram extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.programscrollview);
        String[] simplifyApp = {"AudioEffect.apk", "Calendar.apk", "CalendarProvider.apk", "AccountAndSyncSettings.apk", "Bluetooth.apk", "Browser.apk", "CertInstaller.apk", "CertificateManager.apk", "DigitalClockWidget.apk", "Dock.apk", "Email.apk", "Gallery3D.apk", "Gestures.apk", "MotoCAL.apk", "MotoGAL.apk", "NotesWidget.apk", "PimBackup.apk", "QuickOffice.apk", "SimManager.apk", "Stk.apk", "UCWEB.apk", "WeatherWidget.apk", "About.apk", "FileManager.apk", "Firewall.apk", "SoundRecorder.apk", "Motonav.apk", "YouTube.apk", "Facebook.apk", "HelpCenter.apk", "VoiceSearch.apk", "RootExplorer.apk", "nexusmod.apk", "MagicSmokeWallpapers.apk", "MotoApnControlWidget.apk", "fota.apk", "SwitchPro.apk", "AppInstaller.apk"};
        String[] simplifyAppContent = {"Motorola音效管理程序", "日历程序", "日历数据存储程序", "Google帐户和同步设置", "蓝牙功能", "浏览器", "证书安装", "Wapi证书", "世界时钟", "多媒体底座", "电子邮件", "3D图库", "我的手势", "公司日历", "公司日志", "记事本Widget", "SD卡备份", "QuickOffice", "SIM卡管理", "运营商SIM卡服务", "UCWEB浏览器", "天气插件", "ITFUNZ获取新版本", "MOTO文件管理器", "MOTO来电防火墙", "MOTO录音机", "Motorola官方导航", "YouTube", "Facebook", "MOTO帮助中心", "语音搜索", "一个具有Root权限的文件管理器", "线性光幕动态壁纸", "魔幻烟雾动态壁纸", "Motorola手机上网控制程序", "FOTA空中升级", "七键开关Widget", "批量APK安装程序"};
        int count = simplifyApp.length;
        LinearLayout llayout = (LinearLayout) findViewById(R.id.ProgramList);
        LayoutInflater linflater = (LayoutInflater) getSystemService("layout_inflater");
        int existApp = 0;
        for (int i = 0; i < count; i++) {
            View appDetail = linflater.inflate(R.layout.applicationdetail, (ViewGroup) null);
            TextView tvAppName = (TextView) appDetail.findViewById(R.id.appName);
            TextView tvAppContent = (TextView) appDetail.findViewById(R.id.appContent);
            ImageButton ibAppDelete = (ImageButton) appDetail.findViewById(R.id.appdelete);
            File file = new File(String.valueOf("/system/app/") + simplifyApp[i]);
            long fileSize = file.length() / 1024;
            if (file.exists()) {
                String fileName = simplifyApp[i];
                String fileContent = simplifyAppContent[i];
                if (fileName.length() > 22) {
                    fileName = String.valueOf(fileName.substring(0, 22)) + "...";
                }
                final String str = simplifyApp[i];
                AlertDialog dialog = new AlertDialog.Builder(this).setTitle(fileName).setMessage("程序大小为:" + fileSize + "kb，是否删除？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final ProgressDialog pdialog = ProgressDialog.show(SimplifyProgram.this, "操作提示", "删除中，请稍候...");
                        final Handler handler = new Handler();
                        final String str = str;
                        new Thread() {
                            public void run() {
                                super.run();
                                try {
                                    StringBuilder res = new StringBuilder();
                                    RootScript.runScriptAsRoot("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\n", res, 1000);
                                    new File("/system/app/" + str).delete();
                                    RootScript.runScriptAsRoot("pm uninstall " + SimplifyProgram.this.getPackageManager().getPackageArchiveInfo("/system/app/" + str, 1).applicationInfo.packageName + "\nmount -t yaffs2 -o ro,remount /dev/block/mtdblock6 /system", res, 1000);
                                    Handler handler = handler;
                                    final ProgressDialog progressDialog = pdialog;
                                    final String str = str;
                                    handler.post(new Runnable() {
                                        public void run() {
                                            progressDialog.dismiss();
                                            Toast.makeText(SimplifyProgram.this, String.valueOf(str) + "已成功删除。", 1).show();
                                            SimplifyProgram.this.onCreate(null);
                                        }
                                    });
                                } catch (Exception e) {
                                    Log.i("break", e.toString());
                                }
                            }
                        }.start();
                    }
                }).setNeutralButton("取消", (DialogInterface.OnClickListener) null).create();
                tvAppName.setText(fileName);
                tvAppContent.setText(fileContent);
                final AlertDialog alertDialog = dialog;
                ibAppDelete.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        alertDialog.show();
                    }
                });
                llayout.addView(appDetail);
                existApp++;
            }
        }
        TextView textView = new TextView(this);
        textView.setText("Google服务相关程序--谨慎操作");
        textView.setBackgroundColor(-65536);
        textView.setTextSize(20.0f);
        llayout.addView(textView);
        String[] GoogleApp = {"EnhancedGoogleSearchProvider.apk", "Gmail.apk", "GmailProvider.apk", "GoogleApps.apk", "GoogleCheckin.apk", "GoogleContactsSyncAdapter.apk", "GooglePartnerSetup.apk", "GoogleSettingsProvider.apk", "GoogleSubscribedFeedsProvider.apk", "gtalkservice.apk", "LatinImeTutorial.apk", "MarketUpdater.apk", "MediaUploader.apk", "NetworkLocation.apk", "SetupWizard.apk", "Street.apk", "Talk.apk", "TalkProvider.apk", "Vending.apk", "Maps.apk"};
        String[] GoogleAppContent = {"Google搜索引擎增强服务", "Gmail邮箱", "Gmail邮箱数据存储程序", "Google应用基础服务", "GoogleCheckin服务", "Google通讯录/联系人同步服务", "Google协作程序安装向导", "Google程序设置存储服务", "Google资料同步服务", "Google Talk服务", "输入法教程", "Google Market软件管理程序", "Google媒体数据上传服务", "谷歌网络定位服务", "手机设置向导", "Google街景视图服务", "Google Talk程序", "Google Talk数据存储程序", "电子市场", "Google地图"};
        int googleAppCount = GoogleApp.length;
        for (int i2 = 0; i2 < googleAppCount; i2++) {
            View appDetail2 = linflater.inflate(R.layout.applicationdetail, (ViewGroup) null);
            TextView tvAppName2 = (TextView) appDetail2.findViewById(R.id.appName);
            TextView tvAppContent2 = (TextView) appDetail2.findViewById(R.id.appContent);
            ImageButton ibAppDelete2 = (ImageButton) appDetail2.findViewById(R.id.appdelete);
            File file2 = new File(String.valueOf("/system/app/") + GoogleApp[i2]);
            long fileSize2 = file2.length() / 1024;
            if (file2.exists()) {
                String fileName2 = GoogleApp[i2];
                String fileContent2 = GoogleAppContent[i2];
                if (fileName2.length() > 22) {
                    fileName2 = String.valueOf(fileName2.substring(0, 22)) + "...";
                }
                final String str2 = GoogleApp[i2];
                final Bundle bundle = savedInstanceState;
                AlertDialog dialog2 = new AlertDialog.Builder(this).setTitle(fileName2).setMessage("程序大小为:" + fileSize2 + "kb，是否删除？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final ProgressDialog pdialog = ProgressDialog.show(SimplifyProgram.this, "操作提示", "删除中，请稍候...");
                        final Handler handler = new Handler();
                        final String str = str2;
                        final Bundle bundle = bundle;
                        new Thread() {
                            public void run() {
                                super.run();
                                try {
                                    StringBuilder res = new StringBuilder();
                                    RootScript.runScriptAsRoot("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\n", res, 1000);
                                    new File("/system/app/" + str).delete();
                                    RootScript.runScriptAsRoot("pm uninstall " + SimplifyProgram.this.getPackageManager().getPackageArchiveInfo("/system/app/" + str, 1).applicationInfo.packageName + "\nmount -t yaffs2 -o ro,remount /dev/block/mtdblock6 /system", res, 1000);
                                    Handler handler = handler;
                                    final ProgressDialog progressDialog = pdialog;
                                    final String str = str;
                                    final Bundle bundle = bundle;
                                    handler.post(new Runnable() {
                                        public void run() {
                                            progressDialog.dismiss();
                                            Toast.makeText(SimplifyProgram.this, String.valueOf(str) + "已成功删除。", 1).show();
                                            SimplifyProgram.this.onCreate(bundle);
                                        }
                                    });
                                } catch (Exception e) {
                                    Log.i("break", e.toString());
                                }
                            }
                        }.start();
                    }
                }).setNeutralButton("取消", (DialogInterface.OnClickListener) null).create();
                tvAppName2.setText(fileName2);
                tvAppContent2.setText(fileContent2);
                final AlertDialog alertDialog2 = dialog2;
                ibAppDelete2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        alertDialog2.show();
                    }
                });
                llayout.addView(appDetail2);
                existApp++;
            }
        }
        setTitle("共查找到" + existApp + "个可被精简APK程序");
    }
}
