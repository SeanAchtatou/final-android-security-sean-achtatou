package com.itfunz.itfunzsupertools;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.itfunz.itfunzsupertools.command.ItfunzCheckSum;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.File;
import java.io.IOException;

public class ThemeManager extends PreferenceActivity {
    String Listener;
    String ThemeName;
    public String[] fileFilter = {"AtCmd.apk", "CertInstaller.apk", "DMService.apk", "GoogleCheckin.apk", "GooglePartnerSetup.apk", "HTMLViewer.apk", "MarketUpdater.apk", "MotoAtCmdPlugin.apk", "NetworkLocation.apk", "PhoneConfig.apk", "thumbnailservice.apk", "TtsService.apk", "AccountAndSyncSettings.apk", "GoogleContactsSyncAdapter.apk", "ProgramMenu.apk", "ProgramMenuSystem.apk", "SetupWizard.apk", "GoogleApps.apk"};
    public final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
    PreferenceScreen psInstaller;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Environment.getExternalStorageState().equals("mounted")) {
            setPreferenceScreen(createInstalledLayout());
            setTitle("当前已安装的主题");
            return;
        }
        new AlertDialog.Builder(this).setTitle("内存卡检测").setMessage("未检测到内存卡，请插入内存卡或修改手机连接状态。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ThemeManager.this.finish();
            }
        }).setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return false;
                }
                ThemeManager.this.finish();
                return false;
            }
        }).create().show();
    }

    public Dialog createBackupDialog() {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        return new AlertDialog.Builder(this).setTitle("主题管理器").setMessage("我们检测到您是首次使用主题管理器，我们将在SD卡根目录为您创建名为motoskin的备份文件夹并自动备份默认主题到此目录。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                final ProgressDialog pdialog = ProgressDialog.show(ThemeManager.this, "主题管理器", "备份中，请稍候...");
                final Handler handler = new Handler();
                final SharedPreferences sharedPreferences = settings;
                new Thread() {
                    public void run() {
                        super.run();
                        new File("/sdcard/motoskin/default/app").mkdirs();
                        new File("/sdcard/motoskin/default/framework").mkdirs();
                        RootScript.runRootCommand("cp /system/app/* /sdcard/motoskin/default/app");
                        RootScript.runRootCommand("cp /system/framework/* /sdcard/motoskin/default/framework");
                        if (ItfunzCheckSum.isIsFroyo()) {
                            try {
                                new File("/sdcard/motoskin/default/Is2.2.cfg").createNewFile();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        sharedPreferences.edit().putString("selectTheme", "default").commit();
                        Handler handler = handler;
                        final ProgressDialog progressDialog = pdialog;
                        handler.post(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                                ThemeManager.this.onCreate(null);
                            }
                        });
                    }
                }.start();
            }
        }).create();
    }

    /* access modifiers changed from: private */
    public PreferenceScreen createInstalledLayout() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        PreferenceScreen psLayout = getPreferenceManager().createPreferenceScreen(this);
        File fileList = new File("/sdcard/motoskin/");
        File[] list = fileList.listFiles();
        File fileDefault = new File("/sdcard/motoskin/default");
        if (!fileList.exists() || !fileDefault.exists()) {
            createBackupDialog().show();
        } else {
            for (int i = 0; i < list.length; i++) {
                this.psInstaller = getPreferenceManager().createPreferenceScreen(this);
                String ThemeName2 = list[i].getName();
                String SelectTheme = settings.getString("selectTheme", "default");
                if (list[i].isDirectory()) {
                    if (ThemeName2.equals("default")) {
                        if (ThemeName2.equals(SelectTheme)) {
                            this.psInstaller.setTitle("默认主题(已启用)");
                        } else {
                            this.psInstaller.setTitle("默认主题");
                        }
                    } else if (ThemeName2.equals("Original")) {
                        if (ThemeName2.equals(SelectTheme)) {
                            this.psInstaller.setTitle("原版主题(已启用)");
                        } else {
                            this.psInstaller.setTitle("原版主题");
                        }
                    } else if (ThemeName2.equals(SelectTheme)) {
                        this.psInstaller.setTitle(String.valueOf(ThemeName2) + "(已启用)");
                    } else {
                        this.psInstaller.setTitle(ThemeName2);
                    }
                    this.psInstaller.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                        public boolean onPreferenceClick(Preference preference) {
                            ThemeManager.this.Listener = preference.toString();
                            ThemeManager.this.createSwitchDialog().show();
                            return true;
                        }
                    });
                    psLayout.addPreference(this.psInstaller);
                }
            }
        }
        return psLayout;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.addSubMenu(0, 1, 1, "安 装 主 题").setIcon((int) R.drawable.ic_menu_add);
        menu.addSubMenu(0, 2, 2, "取 消 操 作").setIcon((int) R.drawable.ic_menu_close_clear_cancel);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                startActivity(new Intent(this, ItfunzFileDialog.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public Dialog createSwitchDialog() {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        return new AlertDialog.Builder(this).setTitle("主题管理器").setMessage("请选择您要执行的操作").setPositiveButton("更换", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (ThemeManager.this.Listener.indexOf("(已启用)") != -1) {
                    ThemeManager.this.Listener = ThemeManager.this.Listener.substring(0, ThemeManager.this.Listener.length() - 5);
                } else if (ThemeManager.this.Listener.equals("默认主题")) {
                    if (ThemeManager.this.switchTheme("default")) {
                        settings.edit().putString("selectTheme", "default").commit();
                    }
                } else if (ThemeManager.this.Listener.equals("原版主题")) {
                    if (ThemeManager.this.switchTheme("Original")) {
                        settings.edit().putString("selectTheme", "Original").commit();
                    }
                } else if (ThemeManager.this.switchTheme(ThemeManager.this.Listener)) {
                    settings.edit().putString("selectTheme", ThemeManager.this.Listener).commit();
                }
                ThemeManager.this.setPreferenceScreen(ThemeManager.this.createInstalledLayout());
            }
        }).setNeutralButton("删除", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (ThemeManager.this.Listener.indexOf("(已启用)") != -1) {
                    ThemeManager.this.Listener = ThemeManager.this.Listener.substring(0, ThemeManager.this.Listener.length() - 5);
                }
                if (ThemeManager.this.Listener.equals("默认主题") || ThemeManager.this.Listener.equals("default")) {
                    Toast.makeText(ThemeManager.this, "默认主题不能被删除！", 1).show();
                    return;
                }
                String SelectTheme = settings.getString("selectTheme", "default");
                if (SelectTheme.equals("Original")) {
                    SelectTheme = "原版主题";
                }
                if (ThemeManager.this.Listener.equals(SelectTheme)) {
                    Toast.makeText(ThemeManager.this, "不能删除当前正在使用的主题！", 1).show();
                    return;
                }
                final ProgressDialog pdialog = ProgressDialog.show(ThemeManager.this, "主题管理器", "删除中，请稍候...");
                final Handler handler = new Handler();
                new Thread() {
                    public void run() {
                        super.run();
                        if (ThemeManager.this.Listener.equals("原版主题")) {
                            RootScript.runRootCommand("rm -r /sdcard/motoskin/Original");
                        } else {
                            RootScript.runRootCommand("rm -r /sdcard/motoskin/" + ThemeManager.this.Listener);
                        }
                        Handler handler = handler;
                        final ProgressDialog progressDialog = pdialog;
                        handler.post(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                                ThemeManager.this.setPreferenceScreen(ThemeManager.this.createInstalledLayout());
                            }
                        });
                    }
                }.start();
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create();
    }

    public boolean switchTheme(String Listener2) {
        int state;
        boolean result;
        if (!ItfunzCheckSum.isIsFroyo()) {
            state = 1;
            result = true;
        } else if (new File("/sdcard/motoskin/" + Listener2 + "/Is2.2.cfg").exists()) {
            state = 1;
            result = true;
        } else {
            state = 0;
            result = false;
        }
        switch (state) {
            case 0:
                Toast.makeText(this, "您好，本主题不适用于Android 2.2版本。如果您是主题制作者并制作了2.2的主题，请在app、framework同一目录建立Is2.2.cfg文件即可支持2.2ROM。", 0).show();
                break;
            case 1:
                final String pathApp = "/sdcard/motoskin/" + Listener2 + "/app";
                final String pathFramework = "/sdcard/motoskin/" + Listener2 + "/framework";
                final String[] listApp = new File(pathApp).list();
                ProgressDialog.show(this, "主题管理器", "更换中，请稍候...更换过程中部分程序可能会弹出失去响应提示框，属正常现象，请手动关闭。如果重启后半天没有进入系统，请拔掉电池再开机即可。");
                new Thread() {
                    public void run() {
                        super.run();
                        int result = 0;
                        RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\n");
                        if (listApp != null) {
                            for (int i = 0; i < listApp.length; i++) {
                                int index = 0;
                                while (true) {
                                    if (index >= ThemeManager.this.fileFilter.length) {
                                        break;
                                    } else if (listApp[i].equals(ThemeManager.this.fileFilter[index]) || listApp[i].indexOf("Provider") != -1) {
                                        result = 1;
                                    } else if (!new File("/system/app/" + listApp[i]).exists()) {
                                        result = 1;
                                        break;
                                    } else {
                                        result = 0;
                                        index++;
                                    }
                                }
                                result = 1;
                                if (result == 0) {
                                    RootScript.runRootCommand("cat " + pathApp + "/" + listApp[i] + " > /system/app/" + listApp[i]);
                                }
                            }
                        }
                        String resultCommand3 = "cp -f " + pathFramework + "/services.jar /system/framework/services.bak\nchmod 777 /system/framework/services.bak\nmv /system/framework/services.bak /system/framework/services.jar\nchmod 777 /system/framework/services.jar";
                        String resultCommand4 = "cp -f " + pathFramework + "/framework-res.apk /system/framework/framework-res.bak\nchmod 777 /system/framework/framework-res.bak\nmv /system/framework/framework-res.bak /system/framework/framework-res.apk\nchmod 777 /system/framework/framework-res.apk";
                        String framework = String.valueOf(pathFramework) + "/framework-res.apk";
                        if (new File(String.valueOf(pathFramework) + "/services.jar").exists()) {
                            RootScript.runRootCommand(resultCommand3);
                        }
                        if (new File(framework).exists()) {
                            RootScript.runRootCommand(resultCommand4);
                        }
                        RootScript.runRootCommand("sync");
                        RootScript.runRootCommand("reboot");
                    }
                }.start();
                break;
        }
        return result;
    }
}
