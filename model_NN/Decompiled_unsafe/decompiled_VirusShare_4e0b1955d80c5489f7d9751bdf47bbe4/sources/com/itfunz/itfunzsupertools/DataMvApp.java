package com.itfunz.itfunzsupertools;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;

public class DataMvApp extends TabActivity {
    double appAvailableMB;
    double appUsedMB;
    double dataAvailableMB;
    double dataUsedMB;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PackageManager pm = getPackageManager();
        RootScript.runRootCommand("df > /data/data/com.itfunz.itfunzsupertools/files/space.cfg");
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("/data/data/com.itfunz.itfunzsupertools/files/space.cfg"));
            while (true) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                } else if (line.indexOf("/data") != -1) {
                    String Used = line.substring(line.indexOf(", ") + 2, line.indexOf("used") - 2);
                    String Available = line.substring(line.indexOf("d, ") + 3, line.indexOf("available") - 2);
                    double UsedMB = new BigDecimal(Double.valueOf(Double.valueOf(Used).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                    double AvailableMB = new BigDecimal(Double.valueOf(Double.valueOf(Available).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                    setTitle("已用空间:" + UsedMB + "MB  可用空间:" + AvailableMB + "MB");
                    this.dataUsedMB = UsedMB;
                    this.dataAvailableMB = AvailableMB;
                } else if (line.indexOf("/system") != -1) {
                    String Used2 = line.substring(line.indexOf(", ") + 2, line.indexOf("used") - 2);
                    String Available2 = line.substring(line.indexOf("d, ") + 3, line.indexOf("available") - 2);
                    double UsedMB2 = new BigDecimal(Double.valueOf(Double.valueOf(Used2).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                    double AvailableMB2 = new BigDecimal(Double.valueOf(Double.valueOf(Available2).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                    this.appUsedMB = UsedMB2;
                    this.appAvailableMB = AvailableMB2;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        TabHost thLayout = getTabHost();
        setContentView(thLayout);
        LayoutInflater.from(this).inflate((int) R.layout.datamvapp, (ViewGroup) thLayout.getTabContentView(), true);
        LayoutInflater linflater = (LayoutInflater) getSystemService("layout_inflater");
        File file = new File("/data/app");
        RootScript.runRootCommand("chmod 777 /data/app");
        File[] filesData = file.listFiles();
        final LinearLayout llayoutData = (LinearLayout) findViewById(R.id.DataTab);
        final LinearLayout llayoutApp = (LinearLayout) findViewById(R.id.AppTab);
        if (filesData != null) {
            Arrays.sort(filesData, new Comparator<Object>() {
                public int compare(Object arg0, Object arg1) {
                    return ((File) arg0).getName().toLowerCase().compareTo(((File) arg1).getName().toLowerCase());
                }
            });
        }
        int countData = filesData.length;
        for (int i = 0; i < countData; i++) {
            final File file2 = filesData[i];
            final String filePath = file2.getPath();
            final String fileName = file2.getName();
            final View fileDetail = linflater.inflate(R.layout.filedetailformvapp, (ViewGroup) null);
            TextView tvName = (TextView) fileDetail.findViewById(R.id.FileName);
            TextView tvAbility = (TextView) fileDetail.findViewById(R.id.FileAbility);
            final String str = fileName;
            final LinearLayout linearLayout = llayoutData;
            final View view = fileDetail;
            ((ImageButton) fileDetail.findViewById(R.id.delete)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AlertDialog.Builder message = new AlertDialog.Builder(DataMvApp.this).setTitle("操作提示").setMessage("您确定要删除" + str + "？");
                    final String str = str;
                    final LinearLayout linearLayout = linearLayout;
                    final View view = view;
                    message.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            final Handler handler = new Handler();
                            final ProgressDialog pdialog = ProgressDialog.show(DataMvApp.this, "操作提示", "删除中，请稍候...");
                            final String str = str;
                            final LinearLayout linearLayout = linearLayout;
                            final View view = view;
                            new Thread() {
                                public void run() {
                                    super.run();
                                    try {
                                        StringBuilder res = new StringBuilder();
                                        RootScript.runScriptAsRoot("chmod 777 /data/app\nmount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system", res, 1000);
                                        new File("/data/app/" + str).delete();
                                        RootScript.runScriptAsRoot("pm uninstall " + DataMvApp.this.getPackageManager().getPackageArchiveInfo("/data/app/" + str, 1).applicationInfo.packageName, res, 1000);
                                        RootScript.runScriptAsRoot("chmod 771 /data/app\nmount -t yaffs2 -o ro,remount /dev/block/mtdblock6 /system", res, 1000);
                                        Handler handler = handler;
                                        final ProgressDialog progressDialog = pdialog;
                                        final LinearLayout linearLayout = linearLayout;
                                        final View view = view;
                                        handler.post(new Runnable() {
                                            public void run() {
                                                progressDialog.dismiss();
                                                Toast.makeText(DataMvApp.this, "程序已成功删除。", 0).show();
                                                linearLayout.removeView(view);
                                            }
                                        });
                                    } catch (Exception e) {
                                        Log.i("break", e.toString());
                                    }
                                }
                            }.start();
                        }
                    }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create().show();
                }
            });
            ((ImageButton) fileDetail.findViewById(R.id.Move)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AlertDialog.Builder message = new AlertDialog.Builder(DataMvApp.this).setTitle("操作提示").setMessage("您确定要将" + fileName + "移动到系统目录？");
                    final String str = filePath;
                    final String str2 = fileName;
                    final File file = file2;
                    final LinearLayout linearLayout = llayoutData;
                    final View view = fileDetail;
                    message.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            final ProgressDialog pdialog = ProgressDialog.show(DataMvApp.this, "操作提示", "移动中，请稍候...");
                            final Handler handler = new Handler();
                            final String str = str;
                            final String str2 = str2;
                            final File file = file;
                            final LinearLayout linearLayout = linearLayout;
                            final View view = view;
                            new Thread() {
                                public void run() {
                                    super.run();
                                    RootScript.runRootCommand("chmod 777 /data/app\nmount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system");
                                    try {
                                        FileService.copyFile(str, "/system/app/" + str2);
                                        RootScript.runRootCommand("chmod 777 /system/app/" + str2);
                                        file.delete();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    RootScript.runRootCommand("chmod 771 /data/app\nmount -t yaffs2 -o ro,remount /dev/block/mtdblock6 /system");
                                    Handler handler = handler;
                                    final ProgressDialog progressDialog = pdialog;
                                    final LinearLayout linearLayout = linearLayout;
                                    final View view = view;
                                    handler.post(new Runnable() {
                                        public void run() {
                                            progressDialog.dismiss();
                                            linearLayout.removeView(view);
                                            Toast.makeText(DataMvApp.this, "移动成功，重新打开程序搬家程序后将自动列入到系统目录。", 0).show();
                                            RootScript.runRootCommand("df > /data/data/com.itfunz.itfunzsupertools/files/space.cfg");
                                            try {
                                                BufferedReader reader = new BufferedReader(new FileReader("/data/data/com.itfunz.itfunzsupertools/files/space.cfg"));
                                                while (true) {
                                                    String line = reader.readLine();
                                                    if (line != null) {
                                                        if (line.indexOf("/data") != -1) {
                                                            String Used = line.substring(line.indexOf(", ") + 2, line.indexOf("used") - 2);
                                                            String Available = line.substring(line.indexOf("d, ") + 3, line.indexOf("available") - 2);
                                                            double UsedMB = new BigDecimal(Double.valueOf(Double.valueOf(Used).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                                                            double AvailableMB = new BigDecimal(Double.valueOf(Double.valueOf(Available).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                                                            DataMvApp.this.setTitle("已用空间:" + UsedMB + "MB  可用空间:" + AvailableMB + "MB");
                                                            DataMvApp.this.dataUsedMB = UsedMB;
                                                            DataMvApp.this.dataAvailableMB = AvailableMB;
                                                        }
                                                    } else {
                                                        return;
                                                    }
                                                }
                                            } catch (FileNotFoundException e) {
                                                e.printStackTrace();
                                            } catch (IOException e2) {
                                                e2.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            }.start();
                        }
                    }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create().show();
                }
            });
            long fileSize = file2.length() / 1024;
            String fileNameStr = fileName;
            if (fileName.length() >= 20) {
                fileNameStr = String.valueOf(fileName.substring(0, 20)) + "...";
            }
            tvName.setText(fileNameStr);
            final PackageManager packageManager = pm;
            final String str2 = filePath;
            final ImageView imageView = (ImageView) fileDetail.findViewById(R.id.FileIcon);
            new Handler().post(new Runnable() {
                public void run() {
                    try {
                        imageView.setImageDrawable(packageManager.getApplicationIcon(packageManager.getPackageArchiveInfo(str2, 1).applicationInfo));
                    } catch (Exception e) {
                        imageView.setBackgroundResource(R.drawable.icon_apk);
                    }
                }
            });
            tvAbility.setText("文件大小：" + fileSize + "kb");
            llayoutData.addView(fileDetail);
        }
        RootScript.runRootCommand("chmod 771 /data/app");
        File file3 = new File("/system/app");
        File[] filesApp = file3.listFiles();
        if (file3 != null) {
            Arrays.sort(filesApp, new Comparator<Object>() {
                public int compare(Object arg0, Object arg1) {
                    return ((File) arg0).getName().toLowerCase().compareTo(((File) arg1).getName().toLowerCase());
                }
            });
        }
        int countApp = filesApp.length;
        for (int i2 = 0; i2 < countApp; i2++) {
            File file4 = filesApp[i2];
            String filePath2 = file4.getPath();
            String fileName2 = file4.getName();
            View fileDetail2 = linflater.inflate(R.layout.filedetailformvdata, (ViewGroup) null);
            TextView tvName2 = (TextView) fileDetail2.findViewById(R.id.FileName);
            TextView tvAbility2 = (TextView) fileDetail2.findViewById(R.id.FileAbility);
            final String str3 = fileName2;
            final LinearLayout linearLayout2 = llayoutApp;
            final View view2 = fileDetail2;
            ((ImageButton) fileDetail2.findViewById(R.id.delete)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AlertDialog.Builder message = new AlertDialog.Builder(DataMvApp.this).setTitle("操作提示").setMessage("您确定要删除" + str3 + "？");
                    final String str = str3;
                    final LinearLayout linearLayout = linearLayout2;
                    final View view = view2;
                    message.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            final Handler handler = new Handler();
                            final ProgressDialog pdialog = ProgressDialog.show(DataMvApp.this, "操作提示", "删除中，请稍候...");
                            final String str = str;
                            final LinearLayout linearLayout = linearLayout;
                            final View view = view;
                            new Thread() {
                                public void run() {
                                    super.run();
                                    try {
                                        StringBuilder res = new StringBuilder();
                                        RootScript.runScriptAsRoot("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system", res, 1000);
                                        new File("/system/app/" + str).delete();
                                        RootScript.runScriptAsRoot("pm uninstall " + DataMvApp.this.getPackageManager().getPackageArchiveInfo("/system/app/" + str, 1).applicationInfo.packageName, res, 1000);
                                        RootScript.runScriptAsRoot("mount -t yaffs2 -o ro,remount /dev/block/mtdblock6 /system", res, 1000);
                                        Handler handler = handler;
                                        final ProgressDialog progressDialog = pdialog;
                                        final LinearLayout linearLayout = linearLayout;
                                        final View view = view;
                                        handler.post(new Runnable() {
                                            public void run() {
                                                progressDialog.dismiss();
                                                Toast.makeText(DataMvApp.this, "程序已成功删除。", 0).show();
                                                linearLayout.removeView(view);
                                            }
                                        });
                                    } catch (Exception e) {
                                        Log.i("break", e.toString());
                                    }
                                }
                            }.start();
                        }
                    }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create().show();
                }
            });
            final String str4 = fileName2;
            final String str5 = filePath2;
            final File file5 = file4;
            final View view3 = fileDetail2;
            ((ImageButton) fileDetail2.findViewById(R.id.Move)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AlertDialog.Builder message = new AlertDialog.Builder(DataMvApp.this).setTitle("操作提示").setMessage("您确定要将" + str4 + "移动到用户目录？");
                    final String str = str5;
                    final String str2 = str4;
                    final File file = file5;
                    final LinearLayout linearLayout = llayoutApp;
                    final View view = view3;
                    message.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            final ProgressDialog pdialog = ProgressDialog.show(DataMvApp.this, "操作提示", "移动中，请稍候...");
                            final Handler handler = new Handler();
                            final String str = str;
                            final String str2 = str2;
                            final File file = file;
                            final LinearLayout linearLayout = linearLayout;
                            final View view = view;
                            new Thread() {
                                public void run() {
                                    super.run();
                                    RootScript.runRootCommand("chmod 777 /data/app\nmount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system");
                                    try {
                                        FileService.copyFile(str, "/data/app/" + str2);
                                        RootScript.runRootCommand("chmod 777 /data/app/" + str2);
                                        file.delete();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    RootScript.runRootCommand("chmod 771 /data/app\nmount -t yaffs2 -o ro,remount /dev/block/mtdblock6 /system");
                                    Handler handler = handler;
                                    final ProgressDialog progressDialog = pdialog;
                                    final LinearLayout linearLayout = linearLayout;
                                    final View view = view;
                                    handler.post(new Runnable() {
                                        public void run() {
                                            progressDialog.dismiss();
                                            linearLayout.removeView(view);
                                            Toast.makeText(DataMvApp.this, "移动成功，重新打开程序搬家程序后将自动列入到用户目录。", 0).show();
                                            RootScript.runRootCommand("df > /data/data/com.itfunz.itfunzsupertools/files/space.cfg");
                                            try {
                                                BufferedReader reader = new BufferedReader(new FileReader("/data/data/com.itfunz.itfunzsupertools/files/space.cfg"));
                                                while (true) {
                                                    String line = reader.readLine();
                                                    if (line != null) {
                                                        if (line.indexOf("/system") != -1) {
                                                            String Used = line.substring(line.indexOf(", ") + 2, line.indexOf("used") - 2);
                                                            String Available = line.substring(line.indexOf("d, ") + 3, line.indexOf("available") - 2);
                                                            double UsedMB = new BigDecimal(Double.valueOf(Double.valueOf(Used).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                                                            double AvailableMB = new BigDecimal(Double.valueOf(Double.valueOf(Available).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                                                            DataMvApp.this.setTitle("已用空间:" + UsedMB + "MB  可用空间:" + AvailableMB + "MB");
                                                            DataMvApp.this.appUsedMB = UsedMB;
                                                            DataMvApp.this.appAvailableMB = AvailableMB;
                                                        }
                                                    } else {
                                                        return;
                                                    }
                                                }
                                            } catch (FileNotFoundException e) {
                                                e.printStackTrace();
                                            } catch (IOException e2) {
                                                e2.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            }.start();
                        }
                    }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create().show();
                }
            });
            long fileSize2 = file4.length() / 1024;
            String fileNameStr2 = fileName2;
            if (fileName2.length() > 20) {
                fileNameStr2 = String.valueOf(fileName2.substring(0, 20)) + "...";
            }
            final PackageManager packageManager2 = pm;
            final String str6 = filePath2;
            final ImageView imageView2 = (ImageView) fileDetail2.findViewById(R.id.FileIcon);
            new Handler().post(new Runnable() {
                public void run() {
                    try {
                        imageView2.setImageDrawable(packageManager2.getApplicationIcon(packageManager2.getPackageArchiveInfo(str6, 1).applicationInfo));
                    } catch (Exception e) {
                        imageView2.setBackgroundResource(R.drawable.icon_apk);
                    }
                }
            });
            tvName2.setText(fileNameStr2);
            tvAbility2.setText("文件大小：" + fileSize2 + "kb");
            llayoutApp.addView(fileDetail2);
        }
        thLayout.addTab(thLayout.newTabSpec("dataTab").setIndicator("用户目录").setContent((int) R.id.DataTab));
        thLayout.addTab(thLayout.newTabSpec("appTab").setIndicator("系统目录").setContent((int) R.id.AppTab));
        thLayout.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                if (tabId.equals("dataTab")) {
                    DataMvApp.this.setTitle("已用空间:" + DataMvApp.this.dataUsedMB + "MB  可用空间:" + DataMvApp.this.dataAvailableMB + "MB");
                    return;
                }
                try {
                    BufferedReader reader = new BufferedReader(new FileReader("/data/data/com.itfunz.itfunzsupertools/files/space.cfg"));
                    while (true) {
                        String line = reader.readLine();
                        if (line == null) {
                            return;
                        }
                        if (line.indexOf("/system") != -1) {
                            String Used = line.substring(line.indexOf(", ") + 2, line.indexOf("used") - 2);
                            String Available = line.substring(line.indexOf("d, ") + 3, line.indexOf("available") - 2);
                            double UsedMB = new BigDecimal(Double.valueOf(Double.valueOf(Used).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                            DataMvApp.this.setTitle("已用空间:" + UsedMB + "MB  可用空间:" + new BigDecimal(Double.valueOf(Double.valueOf(Available).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue() + "MB");
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        });
    }
}
