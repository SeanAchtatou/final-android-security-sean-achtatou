package com.itfunz.itfunzsupertools;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import java.io.File;

public class ItfunzFileDialogConfig extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setPreferenceScreen(getDefaultPreferenceScreen());
        getPreferenceManager().findPreference("firstdir").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                ItfunzFileDialogConfig.this.createSetFirstDirDialog().show();
                return true;
            }
        });
    }

    public PreferenceScreen getDefaultPreferenceScreen() {
        PreferenceScreen psLayout = getPreferenceManager().createPreferenceScreen(this);
        PreferenceCategory pcView = new PreferenceCategory(this);
        pcView.setTitle((int) R.string.title_itfunzfiledialog_view);
        psLayout.addPreference(pcView);
        CheckBoxPreference cbpHideFile = new CheckBoxPreference(this);
        cbpHideFile.setTitle((int) R.string.itfunzfiledialogconfig_view_hide);
        cbpHideFile.setSummary((int) R.string.itfunzfiledialogconfig_view_hide_summary);
        cbpHideFile.setKey("file_hide");
        cbpHideFile.setDefaultValue(true);
        psLayout.addPreference(cbpHideFile);
        PreferenceScreen psFirstDir = getPreferenceManager().createPreferenceScreen(this);
        psFirstDir.setTitle("设置起始目录");
        psFirstDir.setSummary("设置进入文件管理器时的初始目录");
        psFirstDir.setKey("firstdir");
        psLayout.addPreference(psFirstDir);
        return psLayout;
    }

    public Dialog createSetFirstDirDialog() {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        LinearLayout llayout = new LinearLayout(this);
        llayout.setOrientation(1);
        final EditText etDir = new EditText(this);
        etDir.setText(settings.getString("firstDir", String.valueOf(File.separator) + Environment.getExternalStorageDirectory().getName()));
        llayout.addView(etDir);
        return new AlertDialog.Builder(this).setTitle("设置起始目录").setView(llayout).setNegativeButton("取消", (DialogInterface.OnClickListener) null).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String resultDir = etDir.getText().toString().trim();
                if (new File(resultDir).listFiles() != null) {
                    settings.edit().putString("firstDir", resultDir).commit();
                } else {
                    Toast.makeText(ItfunzFileDialogConfig.this, "此目录不存在，请重新输入。", 0).show();
                }
            }
        }).create();
    }
}
