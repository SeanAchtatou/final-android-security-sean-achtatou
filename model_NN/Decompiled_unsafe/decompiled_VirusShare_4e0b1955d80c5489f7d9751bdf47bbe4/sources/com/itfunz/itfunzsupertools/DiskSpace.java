package com.itfunz.itfunzsupertools;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DiskSpace extends Activity {
    public static final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
    ArrayList<Map<String, Object>> data;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RootScript.runRootCommand("df > /data/data/com.itfunz.itfunzsupertools/files/space.cfg");
        String systemSpace = "";
        String systemTotal = "";
        String dataSpace = "";
        String dataTotal = "";
        String cacheSpace = "";
        String cacheTotal = "";
        String tmpSpace = "";
        String tmpTotal = "";
        String sdcardSpace = "";
        String sdcardTotal = "";
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("/data/data/com.itfunz.itfunzsupertools/files/space.cfg"));
            while (true) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                } else if (line.indexOf("/system") != -1) {
                    String Total = line.substring(line.indexOf(" ") + 1, line.indexOf("total") - 2);
                    String Used = line.substring(line.indexOf(", ") + 2, line.indexOf("used") - 2);
                    String Available = line.substring(line.indexOf("d, ") + 3, line.indexOf("available") - 2);
                    double TotalMB = new BigDecimal(Double.valueOf(Double.valueOf(Total).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                    systemTotal = "总大小:" + TotalMB + "MB";
                    systemSpace = "已用空间:" + new BigDecimal(Double.valueOf(Double.valueOf(Used).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue() + "MB  可用空间:" + new BigDecimal(Double.valueOf(Double.valueOf(Available).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue() + "MB";
                } else if (line.indexOf("/data") != -1) {
                    String Total2 = line.substring(line.indexOf(" ") + 1, line.indexOf("total") - 2);
                    String Used2 = line.substring(line.indexOf(", ") + 2, line.indexOf("used") - 2);
                    String Available2 = line.substring(line.indexOf("d, ") + 3, line.indexOf("available") - 2);
                    double TotalMB2 = new BigDecimal(Double.valueOf(Double.valueOf(Total2).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                    dataTotal = "总大小:" + TotalMB2 + "MB";
                    dataSpace = "已用空间:" + new BigDecimal(Double.valueOf(Double.valueOf(Used2).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue() + "MB  可用空间:" + new BigDecimal(Double.valueOf(Double.valueOf(Available2).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue() + "MB";
                } else if (line.indexOf("/cache") != -1) {
                    String Total3 = line.substring(line.indexOf(" ") + 1, line.indexOf("total") - 2);
                    String Used3 = line.substring(line.indexOf(", ") + 2, line.indexOf("used") - 2);
                    String Available3 = line.substring(line.indexOf("d, ") + 3, line.indexOf("available") - 2);
                    double TotalMB3 = new BigDecimal(Double.valueOf(Double.valueOf(Total3).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                    cacheTotal = "总大小:" + TotalMB3 + "MB";
                    cacheSpace = "已用空间:" + new BigDecimal(Double.valueOf(Double.valueOf(Used3).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue() + "MB  可用空间:" + new BigDecimal(Double.valueOf(Double.valueOf(Available3).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue() + "MB";
                } else if (line.indexOf("/tmp") != -1) {
                    String Total4 = line.substring(line.indexOf(" ") + 1, line.indexOf("total") - 2);
                    String Used4 = line.substring(line.indexOf(", ") + 2, line.indexOf("used") - 2);
                    String Available4 = line.substring(line.indexOf("d, ") + 3, line.indexOf("available") - 2);
                    double TotalMB4 = new BigDecimal(Double.valueOf(Double.valueOf(Total4).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                    tmpTotal = "总大小:" + TotalMB4 + "MB";
                    tmpSpace = "已用空间:" + new BigDecimal(Double.valueOf(Double.valueOf(Used4).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue() + "MB  可用空间:" + new BigDecimal(Double.valueOf(Double.valueOf(Available4).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue() + "MB";
                } else if (line.indexOf("/sdcard") != -1) {
                    String Total5 = line.substring(line.indexOf(" ") + 1, line.indexOf("total") - 2);
                    String Used5 = line.substring(line.indexOf(", ") + 2, line.indexOf("used") - 2);
                    String Available5 = line.substring(line.indexOf("d, ") + 3, line.indexOf("available") - 2);
                    double TotalMB5 = new BigDecimal(Double.valueOf(Double.valueOf(Total5).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue();
                    sdcardTotal = "总大小:" + TotalMB5 + "MB";
                    sdcardSpace = "已用空间:" + new BigDecimal(Double.valueOf(Double.valueOf(Used5).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue() + "MB  可用空间:" + new BigDecimal(Double.valueOf(Double.valueOf(Available5).doubleValue() / 1024.0d).doubleValue()).setScale(2, 4).doubleValue() + "MB";
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.data = new ArrayList<>();
        HashMap hashMap = new HashMap();
        hashMap.put("FolderIcon", Integer.valueOf((int) R.drawable.icon_folder));
        hashMap.put("FolderName", "系统目录\t" + systemTotal);
        hashMap.put("FolderAbility", systemSpace);
        this.data.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("FolderIcon", Integer.valueOf((int) R.drawable.icon_folder));
        hashMap2.put("FolderName", "用户数据\t" + dataTotal);
        hashMap2.put("FolderAbility", dataSpace);
        this.data.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("FolderIcon", Integer.valueOf((int) R.drawable.icon_folder));
        hashMap3.put("FolderName", "缓存目录\t" + cacheTotal);
        hashMap3.put("FolderAbility", cacheSpace);
        this.data.add(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("FolderIcon", Integer.valueOf((int) R.drawable.icon_folder));
        hashMap4.put("FolderName", "临时目录\t" + tmpTotal);
        hashMap4.put("FolderAbility", tmpSpace);
        this.data.add(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.put("FolderIcon", Integer.valueOf((int) R.drawable.icon_folder));
        hashMap5.put("FolderName", "内存卡    \t" + sdcardTotal);
        hashMap5.put("FolderAbility", sdcardSpace);
        this.data.add(hashMap5);
        ListView listView = new ListView(this);
        listView.setLayoutParams(layoutParams);
        listView.setVerticalScrollBarEnabled(true);
        setTitle("空间概况");
        listView.setAdapter((ListAdapter) new SimpleAdapter(this, this.data, R.layout.diskspace, new String[]{"FolderIcon", "FolderName", "FolderAbility"}, new int[]{R.id.FileIcon, R.id.FileName, R.id.FileAbility}));
        setContentView(listView);
        RootScript.runRootCommand("rm /data/data/com.itfunz.itfunzsupertools/files/space.cfg");
    }
}
