package com.itfunz.itfunzsupertools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class OtherFunction extends Activity {
    private Button CameraSoundOff;
    private Button CleanContactFavorite;
    private Button CleanMarketSearch;
    private Button ScreenCheck;
    private Button SearchKey;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.otherfunction);
        this.ScreenCheck = (Button) findViewById(R.id.screenCheck);
        this.ScreenCheck.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                OtherFunction.this.startActivity(new Intent(OtherFunction.this, ScreenCheckLayout.class));
            }
        });
        this.CleanMarketSearch = (Button) findViewById(R.id.marketClear);
        this.CleanMarketSearch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\n/system/bin/rm /data/data/com.android.vending/databases/suggestions.db");
                Toast.makeText(OtherFunction.this, "Market搜索记录清理完毕,重启手机后生效。", 1).show();
            }
        });
        this.CleanContactFavorite = (Button) findViewById(R.id.contactClear);
        this.CleanContactFavorite.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
            public void onClick(View v) {
                Uri contactsUri = ContactsContract.Contacts.CONTENT_URI;
                ContentValues contentValues = new ContentValues();
                contentValues.clear();
                contentValues.put("last_time_contacted", (Integer) 0);
                contentValues.put("times_contacted", (Integer) 0);
                Toast.makeText(OtherFunction.this, "已成功清理" + OtherFunction.this.getContentResolver().update(contactsUri, contentValues, "times_contacted > 0 OR last_time_contacted != 0", null) + "条记录。", 1).show();
            }
        });
        this.CameraSoundOff = (Button) findViewById(R.id.cameraSoundOff);
        this.CameraSoundOff.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (new File("/system/media/audio/ui/camera_click.ogg").exists()) {
                    RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\nchmod 777 " + "/system/media/audio/ui/camera_click.ogg");
                    RootScript.runRootCommand("mv " + "/system/media/audio/ui/camera_click.ogg" + " " + "/system/media/audio/ui/camera_click_off.ogg");
                    Toast.makeText(OtherFunction.this, "相机快门音已关闭", 0).show();
                    return;
                }
                RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\n");
                RootScript.runRootCommand("mv " + "/system/media/audio/ui/camera_click_off.ogg" + " " + "/system/media/audio/ui/camera_click.ogg");
                Toast.makeText(OtherFunction.this, "相机快门音已开启", 0).show();
            }
        });
        this.SearchKey = (Button) findViewById(R.id.searchKey);
        this.SearchKey.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OtherFunction.this.createSearchKeyDialog().show();
            }
        });
    }

    public Dialog createSearchKeyDialog() {
        if (!new File("/data/data/com.itfunz.itfunzsupertools/files/qwerty_call.kl").exists()) {
            createFile("qwerty_call.kl");
            createFile("qwerty_camera.kl");
            createFile("qwerty_delete.kl");
            createFile("qwerty_left.kl");
            createFile("qwerty_off.kl");
            createFile("qwerty_on.kl");
            createFile("qwerty_right.kl");
        }
        return new AlertDialog.Builder(this).setTitle("可选操作").setItems((int) R.array.searchKey, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/qwerty_off.kl /system/usr/keylayout/qwerty.kl");
                        Toast.makeText(OtherFunction.this, "设置成功，重启后生效！", 0).show();
                        return;
                    case 1:
                        RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/qwerty_call.kl /system/usr/keylayout/qwerty.kl");
                        Toast.makeText(OtherFunction.this, "设置成功，重启后生效！", 0).show();
                        return;
                    case 2:
                        RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/qwerty_delete.kl /system/usr/keylayout/qwerty.kl");
                        Toast.makeText(OtherFunction.this, "设置成功，重启后生效！", 0).show();
                        return;
                    case 3:
                        RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/qwerty_right.kl /system/usr/keylayout/qwerty.kl");
                        Toast.makeText(OtherFunction.this, "设置成功，重启后生效！", 0).show();
                        return;
                    case 4:
                        RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/qwerty_left.kl /system/usr/keylayout/qwerty.kl");
                        Toast.makeText(OtherFunction.this, "设置成功，重启后生效！", 0).show();
                        return;
                    case 5:
                        RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/qwerty_camera.kl /system/usr/keylayout/qwerty.kl");
                        Toast.makeText(OtherFunction.this, "设置成功，重启后生效！", 0).show();
                        return;
                    case 6:
                        RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/qwerty_on.kl /system/usr/keylayout/qwerty.kl");
                        Toast.makeText(OtherFunction.this, "设置成功，重启后生效！", 0).show();
                        return;
                    default:
                        return;
                }
            }
        }).create();
    }

    public void createFile(String fileName) {
        try {
            InputStream is = getAssets().open(fileName);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            FileOutputStream fos = openFileOutput(fileName, 2);
            fos.write(buffer);
            fos.close();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
