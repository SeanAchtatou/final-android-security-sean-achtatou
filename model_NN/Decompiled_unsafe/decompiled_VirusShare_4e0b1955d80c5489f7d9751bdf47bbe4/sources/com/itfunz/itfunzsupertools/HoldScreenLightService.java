package com.itfunz.itfunzsupertools;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import com.itfunz.itfunzsupertools.command.RootScript;

public class HoldScreenLightService extends Service {
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        BroadcastReceiver receiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                final int lightValue = PreferenceManager.getDefaultSharedPreferences(context).getInt(Tools.Light_Value, 2);
                final String IntentAction = intent.getAction();
                FileService.SetSystemConfig("/sys/class/leds/lcd-backlight/brightness", String.valueOf(lightValue));
                for (int i = 1; i <= 5; i++) {
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            if (IntentAction.equals("android.intent.action.SCREEN_OFF")) {
                                RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\nchmod 660 /sys/class/leds/lcd-backlight/brightness\necho 0 > /sys/class/leds/lcd-backlight/brightness");
                            }
                            if (IntentAction.equals("android.intent.action.SCREEN_ON")) {
                                FileService.SetSystemConfig("/sys/class/leds/lcd-backlight/brightness", String.valueOf(lightValue));
                            }
                        }
                    }, (long) (i * 30));
                }
            }
        };
        registerReceiver(receiver, new IntentFilter("android.intent.action.SCREEN_OFF"));
        registerReceiver(receiver, new IntentFilter("android.intent.action.SCREEN_ON"));
    }
}
