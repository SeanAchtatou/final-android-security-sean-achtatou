package com.itfunz.itfunzsupertools;

import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import com.itfunz.itfunzsupertools.command.ItfunzCheckSum;
import java.util.Date;

public class ItfunzSuperToolsService extends Service {
    public static final String FRIDAY = "FRIDAY";
    public static final String MONDAY = "MONDAY";
    public static final String SATURDAY = "SATURDAY";
    public static final String SETTING_INFOS = "SETTING_INFOS";
    public static final String SUNDAY = "SUNDAY";
    public static final String THURSDAY = "THURSDAY";
    public static final String TUESDAY = "TUESDAY";
    public static final String WEDNESDAY = "WEDNESDAY";
    public static AlarmManager alarms1;
    public static AlarmManager alarms2;
    public static AlarmManager alarms3;
    public static AlarmManager alarms4;
    public static AlarmManager alarms5;
    public static AlarmManager alarms6;
    public static AlarmManager alarms7;
    public static KeyguardManager.KeyguardLock mKeyguardLock;
    int Setday = 0;
    Date dateNow = new Date();
    public String friday = "00:00";
    public boolean friday_ = false;
    int h = 0;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    long hour = 0;
    long leftHour = 0;
    long leftMin = 0;
    int m = 0;
    long min = 0;
    public String monday = "00:00";
    public boolean monday_ = false;
    long result = 0;
    public String saturday = "00:00";
    public boolean saturday_ = false;
    long second = 0;
    public String sunday = "00:00";
    public boolean sunday_ = false;
    /* access modifiers changed from: private */
    public Runnable task = new Runnable() {
        public void run() {
            ItfunzSuperToolsService.this.handler.postDelayed(this, 20000);
            ItfunzSuperToolsService.this.sendBroadcast(new Intent("itfunz.screen.refresh"));
        }
    };
    public String thursday = "00:00";
    public boolean thursday_ = false;
    public String tuesday = "00:00";
    public boolean tuesday_ = false;
    public String wednesday = "00:00";
    public boolean wednesday_ = false;

    private boolean getScreenPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("screenLock", true);
    }

    public void onCreate() {
        super.onCreate();
        new Thread() {
            public void run() {
                super.run();
                ItfunzSuperToolsService.this.handler.postDelayed(ItfunzSuperToolsService.this.task, 20000);
            }
        }.start();
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (context.getResources().getConfiguration().orientation == 2) {
                    ItfunzSuperToolsService.this.sendBroadcast(new Intent("itfunz.screen.refresh"));
                } else if (context.getResources().getConfiguration().orientation == 1) {
                    ItfunzSuperToolsService.this.sendBroadcast(new Intent("itfunz.screen.refresh"));
                }
            }
        }, new IntentFilter("android.intent.action.CONFIGURATION_CHANGED"));
        AnonymousClass4 r0 = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals("android.intent.action.MEDIA_MOUNTED") || action.equals("android.intent.action.MEDIA_REMOVED")) {
                    ItfunzSuperToolsService.this.sendBroadcast(new Intent("itfunz.screen.refresh"));
                }
            }
        };
        IntentFilter filter1 = new IntentFilter();
        filter1.addAction("android.intent.action.MEDIA_MOUNTED");
        filter1.addAction("android.intent.action.MEDIA_UNMOUNTED");
        filter1.addAction("android.intent.action.MEDIA_REMOVED");
        filter1.addDataScheme("file");
        registerReceiver(r0, filter1);
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("android.intent.action.SCREEN_ON")) {
                    ItfunzSuperToolsService.this.sendBroadcast(new Intent("itfunz.screen.refresh"));
                }
            }
        }, new IntentFilter("android.intent.action.SCREEN_ON"));
        alarms1 = (AlarmManager) getSystemService("alarm");
        alarms2 = (AlarmManager) getSystemService("alarm");
        alarms3 = (AlarmManager) getSystemService("alarm");
        alarms4 = (AlarmManager) getSystemService("alarm");
        alarms5 = (AlarmManager) getSystemService("alarm");
        alarms6 = (AlarmManager) getSystemService("alarm");
        alarms7 = (AlarmManager) getSystemService("alarm");
        SharedPreferences settings = getSharedPreferences("SETTING_INFOS", 0);
        this.monday = settings.getString("MONDAY", "00:00");
        this.tuesday = settings.getString("TUESDAY", "00:00");
        this.wednesday = settings.getString("WEDNESDAY", "00:00");
        this.thursday = settings.getString("THURSDAY", "00:00");
        this.friday = settings.getString("FRIDAY", "00:00");
        this.saturday = settings.getString("SATURDAY", "00:00");
        this.sunday = settings.getString("SUNDAY", "00:00");
        SharedPreferences config = PreferenceManager.getDefaultSharedPreferences(this);
        this.monday_ = config.getBoolean("MONDAY", false);
        this.tuesday_ = config.getBoolean("TUESDAY", false);
        this.wednesday_ = config.getBoolean("WEDNESDAY", false);
        this.thursday_ = config.getBoolean("THURSDAY", false);
        this.friday_ = config.getBoolean("FRIDAY", false);
        this.saturday_ = config.getBoolean("SATURDAY", false);
        this.sunday_ = config.getBoolean("SUNDAY", false);
        mKeyguardLock = ((KeyguardManager) getSystemService("keyguard")).newKeyguardLock("phone");
        if (getScreenPreference(this)) {
            mKeyguardLock.reenableKeyguard();
        } else {
            mKeyguardLock.disableKeyguard();
        }
        autoPowerDown(this.monday, this.tuesday, this.wednesday, this.thursday, this.friday, this.saturday, this.sunday);
        if (!config.getBoolean("led_light", true)) {
            if (ItfunzCheckSum.isIsFroyo()) {
                FileService.SetSystemConfig("/sys/devices/platform/ld-button-backlight/leds/button-backlight/brightness", "0");
            } else {
                FileService.SetSystemConfig("/sys/devices/platform/button-backlight/leds/button-backlight/brightness", "0");
            }
        }
        boolean light_lock = config.getBoolean("Light_Lock", false);
        int light_value = config.getInt(Tools.Light_Value, 80);
        if (light_lock) {
            FileService.SetSystemConfig("/sys/class/leds/lcd-backlight/brightness", String.valueOf(light_value));
            startService(new Intent(this, HoldScreenLightService.class));
        }
        if (config.getString("led_miss_call", "关闭").equals("关闭")) {
            Intent intent = new Intent(this, CallledService.class);
            intent.setFlags(268435456);
            stopService(intent);
        } else {
            Intent intent2 = new Intent(this, CallledService.class);
            intent2.setFlags(268435456);
            startService(intent2);
        }
        config.edit().putBoolean("readandwrite", false).commit();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private long getTime(String day) {
        if (day == this.monday) {
            this.Setday = 1;
        }
        if (day == this.tuesday) {
            this.Setday = 2;
        }
        if (day == this.wednesday) {
            this.Setday = 3;
        }
        if (day == this.thursday) {
            this.Setday = 4;
        }
        if (day == this.friday) {
            this.Setday = 5;
        }
        if (day == this.saturday) {
            this.Setday = 6;
        }
        if (day == this.sunday) {
            this.Setday = 7;
        }
        if (this.Setday == getDate() || this.Setday == 7) {
            this.h = Integer.valueOf(day.substring(0, day.indexOf(":"))).intValue();
            this.m = Integer.valueOf(day.substring(day.indexOf(":") + 1, day.length())).intValue();
            this.hour = (long) (this.h - this.dateNow.getHours());
            this.min = (long) (this.m - this.dateNow.getMinutes());
            this.second = (long) this.dateNow.getSeconds();
            this.leftHour = (long) (this.dateNow.getHours() - this.h);
            this.leftMin = (long) (this.dateNow.getMinutes() - this.m);
            if (this.h < this.dateNow.getHours()) {
                this.result = ((this.dateNow.getTime() + 604800000) - ((((this.leftHour * 60) + this.leftMin) * 60) * 1000)) - (this.second * 1000);
            } else if (this.m > this.dateNow.getMinutes()) {
                this.result = this.dateNow.getTime() + (((((this.hour * 60) + this.min) * 60) * 1000) - (this.second * 1000));
            } else if (this.m == this.dateNow.getMinutes()) {
                this.result = this.dateNow.getTime() + 604800000;
            } else {
                this.result = (this.dateNow.getTime() + 604800000) - ((long) ((this.m * 60) * 1000));
            }
        } else if (this.Setday > getDate()) {
            this.h = Integer.valueOf(day.substring(0, day.indexOf(":"))).intValue();
            this.m = Integer.valueOf(day.substring(day.indexOf(":") + 1, day.length())).intValue();
            this.hour = (long) (this.h - this.dateNow.getHours());
            this.min = (long) (this.m - this.dateNow.getMinutes());
            this.second = (long) this.dateNow.getSeconds();
            this.leftHour = (long) (this.dateNow.getHours() - this.h);
            this.leftMin = (long) (this.dateNow.getMinutes() - this.m);
            long leftDay = (long) (86400000 * (this.Setday - getDate()));
            long thisTime = ((long) (((this.dateNow.getHours() * 60) + this.dateNow.getMinutes()) * 60 * 1000)) + (this.second * 1000);
            this.result = this.dateNow.getTime() + (leftDay - thisTime) + ((long) (((this.h * 60) + this.m) * 60 * 1000));
        } else {
            countLeftDay(day, this.Setday);
        }
        return this.result;
    }

    private long countLeftDay(String day, int dayint) {
        this.h = Integer.valueOf(day.substring(0, day.indexOf(":"))).intValue();
        this.m = Integer.valueOf(day.substring(day.indexOf(":") + 1, day.length())).intValue();
        this.hour = (long) (this.h - this.dateNow.getHours());
        this.min = (long) (this.m - this.dateNow.getMinutes());
        this.second = (long) this.dateNow.getSeconds();
        this.leftHour = (long) (this.dateNow.getHours() - this.h);
        this.leftMin = (long) (this.dateNow.getMinutes() - this.m);
        this.result = ((this.dateNow.getTime() + ((long) (86400000 * (7 - (getDate() - dayint))))) - ((((this.leftHour * 60) + this.leftMin) * 60) * 1000)) - (this.second * 1000);
        return this.result;
    }

    private int getDate() {
        return new Date().getDay();
    }

    private void autoPowerDown(String day1, String day2, String day3, String day4, String day5, String day6, String day7) {
        if (this.monday_) {
            PendingIntent pendingIntent1 = PendingIntent.getActivity(this, 0, new Intent(this, AutoPowerDownExec.class), 0);
            alarms1.set(0, getTime(day1), pendingIntent1);
        }
        if (this.tuesday_) {
            PendingIntent pendingIntent2 = PendingIntent.getActivity(this, 0, new Intent(this, AutoPowerDownExec.class), 0);
            alarms2.set(0, getTime(day2), pendingIntent2);
        }
        if (this.wednesday_) {
            PendingIntent pendingIntent3 = PendingIntent.getActivity(this, 0, new Intent(this, AutoPowerDownExec.class), 0);
            alarms3.set(0, getTime(day3), pendingIntent3);
        }
        if (this.thursday_) {
            PendingIntent pendingIntent4 = PendingIntent.getActivity(this, 0, new Intent(this, AutoPowerDownExec.class), 0);
            alarms4.set(0, getTime(day4), pendingIntent4);
        }
        if (this.friday_) {
            PendingIntent pendingIntent5 = PendingIntent.getActivity(this, 0, new Intent(this, AutoPowerDownExec.class), 0);
            alarms5.set(0, getTime(day5), pendingIntent5);
        }
        if (this.saturday_) {
            PendingIntent pendingIntent6 = PendingIntent.getActivity(this, 0, new Intent(this, AutoPowerDownExec.class), 0);
            alarms6.set(0, getTime(day6), pendingIntent6);
        }
        if (this.sunday_) {
            PendingIntent pendingIntent7 = PendingIntent.getActivity(this, 0, new Intent(this, AutoPowerDownExec.class), 0);
            alarms7.set(0, getTime(day7), pendingIntent7);
        }
    }
}
