package com.itfunz.itfunzsupertools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class AutoPowerDownExec extends Activity {
    boolean result = false;
    Timer timer = new Timer(true);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CreatePowerDownDialog().show();
        TimerStart();
    }

    public Dialog CreatePowerDownDialog() {
        return new AlertDialog.Builder(this).setTitle("定时关机").setMessage("定时关机时间到，您的手机将在30秒后关机。").setPositiveButton("立即关机", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RootScript.runScriptAsRoot("reboot -p", new StringBuilder(), 1000);
            }
        }).setNegativeButton("取消关机", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AutoPowerDownExec.this.setAlarmManager();
                AutoPowerDownExec.this.result = true;
                AutoPowerDownExec.this.finish();
            }
        }).create();
    }

    public void setAlarmManager() {
        Date dateNow = new Date();
        int day = dateNow.getDay();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, AutoPowerDownExec.class), 0);
        long time = dateNow.getTime() + 604800000;
        switch (day) {
            case 1:
                ItfunzSuperToolsService.alarms1.set(0, time, pendingIntent);
                return;
            case 2:
                ItfunzSuperToolsService.alarms2.set(0, time, pendingIntent);
                return;
            case 3:
                ItfunzSuperToolsService.alarms3.set(0, time, pendingIntent);
                return;
            case 4:
                ItfunzSuperToolsService.alarms4.set(0, time, pendingIntent);
                return;
            case 5:
                ItfunzSuperToolsService.alarms5.set(0, time, pendingIntent);
                return;
            case 6:
                ItfunzSuperToolsService.alarms6.set(0, time, pendingIntent);
                return;
            case 7:
                ItfunzSuperToolsService.alarms7.set(0, time, pendingIntent);
                return;
            default:
                return;
        }
    }

    private void TimerStart() {
        this.timer.schedule(new TimerTask() {
            public void run() {
                try {
                    Thread.sleep(30000);
                    if (!AutoPowerDownExec.this.result) {
                        RootScript.runScriptAsRoot("reboot -p", new StringBuilder(), 1000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                AutoPowerDownExec.this.timer.cancel();
            }
        }, 1);
    }
}
