package com.itfunz.itfunzsupertools;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

public class ScreenCheckLayout extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(savedInstanceState);
        LinearLayout llayout = new LinearLayout(this);
        llayout.setBackgroundColor(-16777216);
        setContentView(llayout);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.addSubMenu(0, 1, 1, "黑色");
        menu.addSubMenu(0, 2, 2, "红色");
        menu.addSubMenu(0, 3, 3, "绿色");
        menu.addSubMenu(0, 4, 4, "黄色");
        menu.addSubMenu(0, 5, 5, "蓝色");
        menu.addSubMenu(0, 6, 6, "白色");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        LinearLayout llayout = new LinearLayout(this);
        switch (item.getItemId()) {
            case 1:
                llayout.setBackgroundColor(-16777216);
                setContentView(llayout);
                break;
            case 2:
                llayout.setBackgroundColor(-65536);
                setContentView(llayout);
                break;
            case 3:
                llayout.setBackgroundColor(-16711936);
                setContentView(llayout);
                break;
            case 4:
                llayout.setBackgroundColor(-256);
                setContentView(llayout);
                break;
            case 5:
                llayout.setBackgroundColor(-16776961);
                setContentView(llayout);
                break;
            case 6:
                llayout.setBackgroundColor(-1);
                setContentView(llayout);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
