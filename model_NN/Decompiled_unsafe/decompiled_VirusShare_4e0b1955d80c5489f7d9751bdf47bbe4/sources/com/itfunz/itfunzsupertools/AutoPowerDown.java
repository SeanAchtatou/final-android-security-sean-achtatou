package com.itfunz.itfunzsupertools;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.widget.TimePicker;

public class AutoPowerDown extends PreferenceActivity {
    public static final String FRIDAY = "FRIDAY";
    public static final String MONDAY = "MONDAY";
    public static final String SATURDAY = "SATURDAY";
    public static final String SETTING_INFOS = "SETTING_INFOS";
    public static final String SUNDAY = "SUNDAY";
    public static final String THURSDAY = "THURSDAY";
    public static final String TUESDAY = "TUESDAY";
    public static final String WEDNESDAY = "WEDNESDAY";
    /* access modifiers changed from: private */
    public String day = null;
    public String friday = "00:00";
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public int m;
    public String monday = "00:00";
    public String saturday = "00:00";
    public String sunday = "00:00";
    public String thursday = "00:00";
    private TimePickerDialog.OnTimeSetListener timesetlistener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String h1;
            String m1;
            AutoPowerDown.this.h = hourOfDay;
            AutoPowerDown.this.m = minute;
            if (AutoPowerDown.this.h < 10) {
                h1 = "0" + AutoPowerDown.this.h;
            } else {
                h1 = String.valueOf(AutoPowerDown.this.h);
            }
            if (AutoPowerDown.this.m < 10) {
                m1 = "0" + AutoPowerDown.this.m;
            } else {
                m1 = String.valueOf(AutoPowerDown.this.m);
            }
            String result = String.valueOf(h1) + ":" + m1;
            AutoPowerDown.this.getPreferenceScreen().findPreference(AutoPowerDown.this.day).setTitle(result);
            AutoPowerDown.this.getSharedPreferences("SETTING_INFOS", 0).edit().putString(AutoPowerDown.this.day, result).commit();
            AutoPowerDown.this.RestartService();
        }
    };
    public String tuesday = "00:00";
    public String wednesday = "00:00";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getSharedPreferences("SETTING_INFOS", 0);
        this.monday = settings.getString("MONDAY", "00:00");
        this.tuesday = settings.getString("TUESDAY", "00:00");
        this.wednesday = settings.getString("WEDNESDAY", "00:00");
        this.thursday = settings.getString("THURSDAY", "00:00");
        this.friday = settings.getString("FRIDAY", "00:00");
        this.saturday = settings.getString("SATURDAY", "00:00");
        this.sunday = settings.getString("SUNDAY", "00:00");
        setPreferenceScreen(createPreferenceScreen());
        getPreferenceScreen().findPreference("MONDAY").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                AutoPowerDown.this.day = "MONDAY";
                if (Boolean.valueOf(newValue.toString()).booleanValue()) {
                    AutoPowerDown.this.onCreateDialog(1).show();
                }
                return true;
            }
        });
        getPreferenceScreen().findPreference("MONDAY").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AutoPowerDown.this.RestartService();
                return true;
            }
        });
        getPreferenceScreen().findPreference("TUESDAY").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                AutoPowerDown.this.day = "TUESDAY";
                if (Boolean.valueOf(newValue.toString()).booleanValue()) {
                    AutoPowerDown.this.onCreateDialog(1).show();
                }
                return true;
            }
        });
        getPreferenceScreen().findPreference("TUESDAY").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AutoPowerDown.this.RestartService();
                return true;
            }
        });
        getPreferenceScreen().findPreference("WEDNESDAY").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                AutoPowerDown.this.day = "WEDNESDAY";
                if (Boolean.valueOf(newValue.toString()).booleanValue()) {
                    AutoPowerDown.this.onCreateDialog(1).show();
                }
                return true;
            }
        });
        getPreferenceScreen().findPreference("WEDNESDAY").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AutoPowerDown.this.RestartService();
                return true;
            }
        });
        getPreferenceScreen().findPreference("THURSDAY").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                AutoPowerDown.this.day = "THURSDAY";
                if (Boolean.valueOf(newValue.toString()).booleanValue()) {
                    AutoPowerDown.this.onCreateDialog(1).show();
                }
                return true;
            }
        });
        getPreferenceScreen().findPreference("THURSDAY").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AutoPowerDown.this.RestartService();
                return true;
            }
        });
        getPreferenceScreen().findPreference("FRIDAY").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                AutoPowerDown.this.day = "FRIDAY";
                if (Boolean.valueOf(newValue.toString()).booleanValue()) {
                    AutoPowerDown.this.onCreateDialog(1).show();
                }
                return true;
            }
        });
        getPreferenceScreen().findPreference("FRIDAY").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AutoPowerDown.this.RestartService();
                return true;
            }
        });
        getPreferenceScreen().findPreference("SATURDAY").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                AutoPowerDown.this.day = "SATURDAY";
                if (Boolean.valueOf(newValue.toString()).booleanValue()) {
                    AutoPowerDown.this.onCreateDialog(1).show();
                }
                return true;
            }
        });
        getPreferenceScreen().findPreference("SATURDAY").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AutoPowerDown.this.RestartService();
                return true;
            }
        });
        getPreferenceScreen().findPreference("SUNDAY").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                AutoPowerDown.this.day = "SUNDAY";
                if (Boolean.valueOf(newValue.toString()).booleanValue()) {
                    AutoPowerDown.this.onCreateDialog(1).show();
                }
                return true;
            }
        });
        getPreferenceScreen().findPreference("SUNDAY").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AutoPowerDown.this.RestartService();
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new TimePickerDialog(this, this.timesetlistener, this.h, this.m, true);
            default:
                return null;
        }
    }

    private PreferenceScreen createPreferenceScreen() {
        PreferenceScreen pfsLayout = getPreferenceManager().createPreferenceScreen(this);
        PreferenceCategory pcfAClock = new PreferenceCategory(this);
        pcfAClock.setTitle((int) R.string.auto_PowerDown);
        pfsLayout.addPreference(pcfAClock);
        CheckBoxPreference pcbMonday = new CheckBoxPreference(this);
        pcbMonday.setKey("MONDAY");
        pcbMonday.setTitle(this.monday);
        pcbMonday.setSummary("星期一");
        pcbMonday.setDefaultValue(false);
        pfsLayout.addPreference(pcbMonday);
        CheckBoxPreference pcbTuesday = new CheckBoxPreference(this);
        pcbTuesday.setKey("TUESDAY");
        pcbTuesday.setTitle(this.tuesday);
        pcbTuesday.setSummary("星期二");
        pcbTuesday.setDefaultValue(false);
        pfsLayout.addPreference(pcbTuesday);
        CheckBoxPreference pcbWednesday = new CheckBoxPreference(this);
        pcbWednesday.setKey("WEDNESDAY");
        pcbWednesday.setTitle(this.wednesday);
        pcbWednesday.setSummary("星期三");
        pcbWednesday.setDefaultValue(false);
        pfsLayout.addPreference(pcbWednesday);
        CheckBoxPreference pcbThursday = new CheckBoxPreference(this);
        pcbThursday.setKey("THURSDAY");
        pcbThursday.setTitle(this.thursday);
        pcbThursday.setSummary("星期四");
        pcbThursday.setDefaultValue(false);
        pfsLayout.addPreference(pcbThursday);
        CheckBoxPreference pcbFriday = new CheckBoxPreference(this);
        pcbFriday.setKey("FRIDAY");
        pcbFriday.setTitle(this.friday);
        pcbFriday.setSummary("星期五");
        pcbFriday.setDefaultValue(false);
        pfsLayout.addPreference(pcbFriday);
        CheckBoxPreference pcbSaturday = new CheckBoxPreference(this);
        pcbSaturday.setKey("SATURDAY");
        pcbSaturday.setTitle(this.saturday);
        pcbSaturday.setSummary("星期六");
        pcbSaturday.setDefaultValue(false);
        pfsLayout.addPreference(pcbSaturday);
        CheckBoxPreference pcbSunday = new CheckBoxPreference(this);
        pcbSunday.setKey("SUNDAY");
        pcbSunday.setTitle(this.sunday);
        pcbSunday.setSummary("星期日");
        pcbSunday.setDefaultValue(false);
        pfsLayout.addPreference(pcbSunday);
        return pfsLayout;
    }

    /* access modifiers changed from: private */
    public void RestartService() {
        Intent i = new Intent(this, ItfunzSuperToolsService.class);
        stopService(i);
        startService(i);
    }
}
