package com.itfunz.itfunzsupertools.command;

import com.itfunz.itfunzsupertools.R;

public class IconFormat {
    public static int getIconFormat(String FileName) {
        String format = FileName.substring(FileName.lastIndexOf(".") + 1, FileName.length()).toLowerCase();
        if (format.equals("aac")) {
            return R.drawable.icon_aac;
        }
        if (format.equals("amt")) {
            return R.drawable.icon_amt;
        }
        if (format.equals("ape")) {
            return R.drawable.icon_ape;
        }
        if (format.equals("apk")) {
            return R.drawable.icon_apk;
        }
        if (format.equals("avi")) {
            return R.drawable.icon_avi;
        }
        if (format.equals("bmp")) {
            return R.drawable.icon_bmp;
        }
        if (format.equals("doc")) {
            return R.drawable.icon_doc;
        }
        if (format.equals("flac")) {
            return R.drawable.icon_flac;
        }
        if (format.equals("jpg")) {
            return R.drawable.icon_jpg;
        }
        if (format.equals("m4a")) {
            return R.drawable.icon_m4a;
        }
        if (format.equals("mid")) {
            return R.drawable.icon_mid;
        }
        if (format.equals("mp3")) {
            return R.drawable.icon_mp3;
        }
        if (format.equals("mp4")) {
            return R.drawable.icon_mp4;
        }
        if (format.equals("ogg")) {
            return R.drawable.icon_ogg;
        }
        if (format.equals("pdf")) {
            return R.drawable.icon_pdf;
        }
        if (format.equals("png")) {
            return R.drawable.icon_png;
        }
        if (format.equals("ppt")) {
            return R.drawable.icon_ppt;
        }
        if (format.equals("rar")) {
            return R.drawable.icon_rar;
        }
        if (format.equals("rm")) {
            return R.drawable.icon_rm;
        }
        if (format.equals("rmvb")) {
            return R.drawable.icon_rmvb;
        }
        if (format.equals("txt") || format.equals("sh")) {
            return R.drawable.icon_txt;
        }
        if (format.equals("wav")) {
            return R.drawable.icon_wav;
        }
        if (format.equals("wma")) {
            return R.drawable.icon_wma;
        }
        if (format.equals("xls")) {
            return R.drawable.icon_xls;
        }
        if (format.equals("zip")) {
            return R.drawable.icon_zip;
        }
        if (format.equals("gif")) {
            return R.drawable.icon_gif;
        }
        if (format.equals("jpeg")) {
            return R.drawable.icon_jpeg;
        }
        if (format.equals("ini") || format.equals("cfg") || format.equals("prop") || format.equals("conf")) {
            return R.drawable.icon_conf;
        }
        return R.drawable.icon_unkown;
    }
}
