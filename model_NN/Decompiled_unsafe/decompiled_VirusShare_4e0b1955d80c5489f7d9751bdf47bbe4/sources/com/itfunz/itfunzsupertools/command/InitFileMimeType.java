package com.itfunz.itfunzsupertools.command;

public class InitFileMimeType {
    private static final String FILE_FORMAT = "File_Format";
    private static final String FILE_MIME_TYPE = "File_Mime_Type";
    private static final String FILE_SUB_TYPE = "File_Sub_Type";
    private static final String FILE_TABLE_NAME = "FileMimeType";

    public static String[] InitSql() {
        String[] sql = new String[200];
        sql[0] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mp3','audio','mpeg3');";
        sql[1] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mp2','audio','mpeg');";
        sql[2] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('wav','audio','wav');";
        sql[3] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('wma','audio','*');";
        sql[4] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('aac','audio','*');";
        sql[5] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('m4a','audio','mp4');";
        sql[6] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mid','audio','midi');";
        sql[7] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('midi','audio','midi');";
        sql[8] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('xm','audio','xm');";
        sql[9] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('3g2','audio','g3p');";
        sql[10] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('aif','audio','aiff');";
        sql[11] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('aifc','audio','aiff');";
        sql[12] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('aiff','audio','aiff');";
        sql[13] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('amr','audio','amr');";
        sql[14] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('au','audio','basic');";
        sql[15] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('gsd','audio','x-gsm');";
        sql[16] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('gsm','audio','x-gsm');";
        sql[17] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('it','audio','it');";
        sql[18] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('jam','audio','x-jam');";
        sql[19] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('kar','audio','midi');";
        sql[20] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('la','audio','nspaudio');";
        sql[21] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('lam','audio','x-liveaduio');";
        sql[22] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('m2a','audio','mpeg');";
        sql[23] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('m3u','audio','x-mpequrl');";
        sql[24] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mod','audio','mod');";
        sql[25] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mpga','audio','mpeg');";
        sql[26] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mv','audio','make');";
        sql[27] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ogg','audio','ogg');";
        sql[28] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pfunk','audio','make');";
        sql[29] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ra','audio','x-realaudio');";
        sql[30] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ram','audio','x-pn-realaudio');";
        sql[186] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rmm','audio','x-pn-realaudio');";
        sql[187] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rmp','audio','x-pn-realaudio');";
        sql[31] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rmi','audio','mid');";
        sql[32] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('s3m','audio','s3m');";
        sql[33] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('sid','audio','x-psid');";
        sql[34] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('snd','audio','basic');";
        sql[35] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('tsi','audio','tsp-audio');";
        sql[36] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('tsp','audio','tsplayer');";
        sql[37] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('voc','audio','voc');";
        sql[38] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('vox','audio','voxware');";
        sql[191] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('flac','audio','*');";
        sql[39] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('wmv','video','wmv');";
        sql[40] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mp4','video','mp4');";
        sql[41] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mpa','video','mpeg');";
        sql[42] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mpe','video','mpeg');";
        sql[43] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mpeg','video','mpeg');";
        sql[44] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mpg','video','mpeg');";
        sql[45] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('3gp','video','g3p');";
        sql[46] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('avi','video','avi');";
        sql[47] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('avs','video','avs-video');";
        sql[48] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('dif','video','x-dv');";
        sql[49] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('dl','video','dl');";
        sql[50] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('dv','video','x-dv');";
        sql[51] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('dvi','video','x-dvi');";
        sql[52] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('fli','video','fli');";
        sql[53] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('gl','video','gl');";
        sql[54] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('m1v','video','mpeg');";
        sql[55] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('m2v','video','mpeg');";
        sql[56] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('moov','video','quicktime');";
        sql[57] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mov','video','quicktime');";
        sql[58] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('movie','video','x-sgi-movie');";
        sql[59] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mv','video','x-sgi-movie');";
        sql[60] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('qt','video','quicktime');";
        sql[61] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('qtc','video','x-qtc');";
        sql[62] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('scm','video','x-scm');";
        sql[63] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('vdo','video','vdo');";
        sql[188] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rmvb','video','*');";
        sql[189] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rm','video','*');";
        sql[64] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('bm','image','bmp');";
        sql[65] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('bmp','image','bmp');";
        sql[66] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('fif','image','fif');";
        sql[67] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('g3','image','g3fax');";
        sql[68] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('gif','image','gif');";
        sql[69] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ico','image','x-icon');";
        sql[70] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pbm','image','x-portable-bitmap');";
        sql[71] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pct','image','x-pict');";
        sql[72] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pcx','image','x-pcx');";
        sql[73] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pic','image','pict');";
        sql[74] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pict','image','pict');";
        sql[75] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pm','image','x-xpixmap');";
        sql[76] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('png','image','png');";
        sql[77] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pnm','image','x-portable-anymap');";
        sql[78] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ppm','image','x-portable-pixmap');";
        sql[79] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('qif','image','x-quicktime');";
        sql[80] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('qti','image','x-quicktime');";
        sql[81] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('qtif','image','x-quicktime');";
        sql[82] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ras','image','cmu-raster');";
        sql[83] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rast','image','cmu-raster');";
        sql[84] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rf','image','vnd.rn-realflash');";
        sql[85] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('svf','image','x-dwg');";
        sql[86] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('svg','image','svg+xml');";
        sql[87] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('tif','image','tiff');";
        sql[88] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('tiff','image','tiff');";
        sql[89] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('x-png','image','png');";
        sql[90] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('xbm','image','xbm');";
        sql[91] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('xif','image','vnd.xiff');";
        sql[92] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('xpm','image','xpm');";
        sql[93] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('jfif','image','jpeg');";
        sql[94] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('jfif-tbnl','image','jpeg');";
        sql[95] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('jpe','image','jpeg');";
        sql[96] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('jpeg','image','jpeg');";
        sql[97] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('jpg','image','jpeg');";
        sql[98] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('jps','image','x-jps');";
        sql[99] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('apk','application','vnd.android.package-archive');";
        sql[100] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('abc','text','vnd.abc');";
        sql[101] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('acgi','text','html');";
        sql[102] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('asm','text','x-asm');";
        sql[103] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('asp','text','asp');";
        sql[104] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('c','text','plain');";
        sql[105] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('c++','text','plain');";
        sql[106] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('cc','text','plain');";
        sql[107] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('conf','text','plain');";
        sql[108] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('cpp','text','x-c');";
        sql[109] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('css','text','css');";
        sql[110] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('csv','text','csv');";
        sql[111] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('cxx','text','plain');";
        sql[112] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('def','text','plain');";
        sql[113] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('el','text','x-script.elisp');";
        sql[114] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('f','text','plain');";
        sql[115] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('g','text','plain');";
        sql[116] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('h','text','plain');";
        sql[117] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('hh','text','plain');";
        sql[118] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('hlb','text','x-script');";
        sql[119] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('htm','text','html');";
        sql[120] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('html','text','html');";
        sql[121] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('htmls','text','html');";
        sql[122] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('htt','text','webviewhtml');";
        sql[123] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('htx','text','html');";
        sql[124] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('idc','text','plain');";
        sql[125] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('java','text','plain');";
        sql[126] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('list','text','plain');";
        sql[127] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('log','text','plain');";
        sql[128] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('lst','text','plain');";
        sql[129] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('m','text','plain');";
        sql[130] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('mar','text','plain');";
        sql[131] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('p','text','x-pascal');";
        sql[132] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pas','text','pascal');";
        sql[133] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pl','text','plain');";
        sql[134] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('py','text','x-script-phyton');";
        sql[135] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rexx','text','x-script.rexx');";
        sql[136] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rt','text','richtext');";
        sql[137] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rtf','text','richtext');";
        sql[138] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rtx','text','richtext');";
        sql[139] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('s','text','x-asm');";
        sql[140] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('sdml','text','plain');";
        sql[141] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('sgm','text','sgml');";
        sql[142] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('sgml','text','sgml');";
        sql[143] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('shtml','text','html');";
        sql[144] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('spc','text','x-speech');";
        sql[145] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('talk','text','x-speech');";
        sql[146] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('tcl','text','x-script.tcl');";
        sql[147] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('tcsh','text','x-script.tcsh');";
        sql[148] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('text','text','plain');";
        sql[149] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('uri','text','uri-list');";
        sql[150] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('wml','text','vnd.wap.xml');";
        sql[151] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('xml','text','xml');";
        sql[152] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('zsh','text','x-script.zsh');";
        sql[153] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('tsv','text','tab-separated-values');";
        sql[182] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('prop','text','plain');";
        sql[183] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ini','text','plain');";
        sql[184] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('cfg','text','plain');";
        sql[185] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('txt','text','plain');";
        sql[190] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('rc','text','plain');";
        sql[154] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('doc','application','vnd.ms-word');";
        sql[155] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('docx','application','vnd.ms-word.document.macroenabled.12');";
        sql[156] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('dotx','application','vnd.ms-word.document.macroenabled.12');";
        sql[157] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('odb','application','*');";
        sql[158] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('odc','application','*');";
        sql[159] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('odf','application','*');";
        sql[160] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('odg','application','*');";
        sql[161] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('odi','application','*');";
        sql[162] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('dom','application','*');";
        sql[163] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('odp','application','*');";
        sql[164] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ods','application','*');";
        sql[165] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('odt','application','*');";
        sql[166] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('otg','application','*');";
        sql[167] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('oth','application','*');";
        sql[168] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('otp','application','*');";
        sql[169] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ots','application','*');";
        sql[170] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ott','application','*');";
        sql[171] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('oxt','application','*');";
        sql[172] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pdf','application','pdf');";
        sql[173] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pps','application','vnd.ms-powerpoint');";
        sql[174] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ppsx','application','vnd.ms-powerpoint.presentation.macroenabled.12');";
        sql[175] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('ppt','application','vnd.ms-powerpoint');";
        sql[176] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pptx','application','vnd.ms-powerpoint.presentation.macroenabled.12');";
        sql[177] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('pwz','application','vnd.ms-powerpoint.presentation.macroenabled.12');";
        sql[178] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('xls','application','vnd.ms-excel');";
        sql[179] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('xlsx','application','vnd.ms-excel');";
        sql[180] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('xlt','application','vnd.ms-excel');";
        sql[181] = "insert into FileMimeType (File_Format, File_Mime_Type, File_Sub_Type) values('xltx','application','vnd.ms-excel');";
        return sql;
    }
}
