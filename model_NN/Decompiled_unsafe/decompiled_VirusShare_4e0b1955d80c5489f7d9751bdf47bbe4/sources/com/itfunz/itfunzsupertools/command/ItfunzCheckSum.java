package com.itfunz.itfunzsupertools.command;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.regex.Pattern;

public class ItfunzCheckSum {
    public static String getCheckSum(String av) {
        long sum = 0;
        try {
            sum = 0 + new ItfunzCheckSum().process(av);
        } catch (Exception e) {
            System.err.println(e);
        }
        String result = Long.toHexString(sum);
        String code = result.substring(result.length() - 4, result.length());
        System.out.println("Checksum is " + code);
        return code;
    }

    public long process(String fileName) {
        long sum = 0;
        try {
            FileInputStream in = new FileInputStream(new File(fileName));
            while (true) {
                int i = in.read();
                if (i == -1) {
                    break;
                }
                sum += (long) i;
            }
            in.close();
        } catch (IOException e) {
            System.out.println("IOException: " + e);
        } catch (Throwable th) {
        }
        return sum;
    }

    public static boolean isNumeric(String str) {
        if (!Pattern.compile("[0-9]*").matcher(str).matches()) {
            return false;
        }
        return true;
    }

    public static boolean isIsFroyo() {
        if (new File("/system/lib/libWifiAPHardware.so").exists()) {
            return true;
        }
        return false;
    }
}
