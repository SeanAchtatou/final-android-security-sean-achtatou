package com.itfunz.itfunzsupertools.command;

public class ItfunzSecurity {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String toMD5(java.lang.String r8) {
        /*
            java.lang.String r6 = ""
            java.lang.String r7 = "MD5"
            java.security.MessageDigest r4 = java.security.MessageDigest.getInstance(r7)     // Catch:{ NoSuchAlgorithmException -> 0x003c }
            byte[] r7 = r8.getBytes()     // Catch:{ NoSuchAlgorithmException -> 0x003c }
            r4.update(r7)     // Catch:{ NoSuchAlgorithmException -> 0x003c }
            byte[] r0 = r4.digest()     // Catch:{ NoSuchAlgorithmException -> 0x003c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ NoSuchAlgorithmException -> 0x003c }
            java.lang.String r7 = ""
            r1.<init>(r7)     // Catch:{ NoSuchAlgorithmException -> 0x003c }
            r5 = 0
        L_0x001b:
            int r7 = r0.length     // Catch:{ NoSuchAlgorithmException -> 0x003c }
            if (r5 < r7) goto L_0x001f
        L_0x001e:
            return r6
        L_0x001f:
            byte r3 = r0[r5]     // Catch:{ NoSuchAlgorithmException -> 0x003c }
            if (r3 >= 0) goto L_0x0025
            int r3 = r3 + 256
        L_0x0025:
            r7 = 16
            if (r3 >= r7) goto L_0x002e
            java.lang.String r7 = "0"
            r1.append(r7)     // Catch:{ NoSuchAlgorithmException -> 0x003c }
        L_0x002e:
            java.lang.String r7 = java.lang.Integer.toHexString(r3)     // Catch:{ NoSuchAlgorithmException -> 0x003c }
            r1.append(r7)     // Catch:{ NoSuchAlgorithmException -> 0x003c }
            java.lang.String r6 = r1.toString()     // Catch:{ NoSuchAlgorithmException -> 0x003c }
            int r5 = r5 + 1
            goto L_0x001b
        L_0x003c:
            r7 = move-exception
            r2 = r7
            r2.printStackTrace()
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.itfunz.itfunzsupertools.command.ItfunzSecurity.toMD5(java.lang.String):java.lang.String");
    }

    public static String toItfunzKey(String str) {
        StringBuffer resultKey = new StringBuffer("");
        for (int i = 0; i < str.length(); i++) {
            resultKey = resultKey.append((char) Integer.parseInt(String.valueOf(str.charAt(i) - (((i + 1) * 17) % 7)), 10));
        }
        return resultKey.toString();
    }

    public static String toOriginalKey(String str) {
        StringBuffer resultKey = new StringBuffer("");
        for (int i = 0; i < str.length(); i++) {
            resultKey = resultKey.append((char) Integer.parseInt(String.valueOf((((i + 1) * 17) % 7) + str.charAt(i)), 10));
        }
        return resultKey.toString();
    }
}
