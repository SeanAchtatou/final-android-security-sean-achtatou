package com.itfunz.itfunzsupertools.command;

import android.app.Activity;
import android.content.Context;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class RootScript extends Activity {
    private static boolean hasroot;

    public static boolean hasRootAccess(Context ctx) {
        if (hasroot) {
            return true;
        }
        try {
            if (runScriptAsRoot("exit 0", null, 20000) == 0) {
                hasroot = true;
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public static int runScriptAsRoot(String script, StringBuilder res, long timeout) {
        ScriptRunner runner = new ScriptRunner(script, res);
        runner.start();
        if (timeout > 0) {
            try {
                runner.join(timeout);
            } catch (InterruptedException e) {
            }
        } else {
            runner.join();
        }
        if (runner.isAlive()) {
            runner.interrupt();
            runner.destroy();
            runner.join(50);
        }
        return runner.exitcode;
    }

    private static final class ScriptRunner extends Thread {
        private Process exec;
        public int exitcode = -1;
        private final StringBuilder res;
        private final String script;

        public ScriptRunner(String script2, StringBuilder res2) {
            this.script = script2;
            this.res = res2;
        }

        public void run() {
            try {
                this.exec = Runtime.getRuntime().exec("su");
                OutputStreamWriter out = new OutputStreamWriter(this.exec.getOutputStream());
                out.write(this.script);
                if (!this.script.endsWith("\n")) {
                    out.write("\n");
                }
                out.flush();
                out.write("exit\n");
                out.flush();
                char[] buf = new char[1024];
                InputStreamReader r = new InputStreamReader(this.exec.getInputStream());
                while (true) {
                    int read = r.read(buf);
                    if (read == -1) {
                        break;
                    } else if (this.res != null) {
                        this.res.append(buf, 0, read);
                    }
                }
                InputStreamReader r2 = new InputStreamReader(this.exec.getErrorStream());
                while (true) {
                    int read2 = r2.read(buf);
                    if (read2 == -1) {
                        break;
                    } else if (this.res != null) {
                        this.res.append(buf, 0, read2);
                    }
                }
                if (this.exec != null) {
                    this.exitcode = this.exec.waitFor();
                }
            } catch (InterruptedException e) {
                if (this.res != null) {
                    this.res.append("\nOperation timed-out");
                }
            } catch (Exception e2) {
                Exception ex = e2;
                if (this.res != null) {
                    this.res.append("\n" + ex);
                }
            } finally {
                destroy();
            }
        }

        public synchronized void destroy() {
            if (this.exec != null) {
                this.exec.destroy();
            }
            this.exec = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x005d A[SYNTHETIC, Splitter:B:16:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0068 A[SYNTHETIC, Splitter:B:22:0x0068] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean runRootCommand(java.lang.String r7) {
        /*
            r3 = 0
            r1 = 0
            java.lang.Runtime r4 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0041 }
            java.lang.String r5 = "su"
            java.lang.Process r3 = r4.exec(r5)     // Catch:{ Exception -> 0x0041 }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0041 }
            java.io.OutputStream r4 = r3.getOutputStream()     // Catch:{ Exception -> 0x0041 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0041 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
            java.lang.String r5 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
            java.lang.String r5 = "\n"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
            r2.writeBytes(r4)     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
            java.lang.String r4 = "exit\n"
            r2.writeBytes(r4)     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
            r2.flush()     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
            r3.waitFor()     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ Exception -> 0x006f }
        L_0x003b:
            r3.destroy()     // Catch:{ Exception -> 0x006f }
        L_0x003e:
            r4 = 1
            r1 = r2
        L_0x0040:
            return r4
        L_0x0041:
            r4 = move-exception
            r0 = r4
        L_0x0043:
            java.lang.String r4 = "*** DEBUG ***"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0065 }
            java.lang.String r6 = "Unexpected error - Here is what I know: "
            r5.<init>(r6)     // Catch:{ all -> 0x0065 }
            java.lang.String r6 = r0.getMessage()     // Catch:{ all -> 0x0065 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0065 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0065 }
            android.util.Log.d(r4, r5)     // Catch:{ all -> 0x0065 }
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ Exception -> 0x0076 }
        L_0x0060:
            r3.destroy()     // Catch:{ Exception -> 0x0076 }
        L_0x0063:
            r4 = 0
            goto L_0x0040
        L_0x0065:
            r4 = move-exception
        L_0x0066:
            if (r1 == 0) goto L_0x006b
            r1.close()     // Catch:{ Exception -> 0x0071 }
        L_0x006b:
            r3.destroy()     // Catch:{ Exception -> 0x0071 }
        L_0x006e:
            throw r4
        L_0x006f:
            r4 = move-exception
            goto L_0x003e
        L_0x0071:
            r5 = move-exception
            goto L_0x006e
        L_0x0073:
            r4 = move-exception
            r1 = r2
            goto L_0x0066
        L_0x0076:
            r4 = move-exception
            goto L_0x0063
        L_0x0078:
            r4 = move-exception
            r0 = r4
            r1 = r2
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.itfunz.itfunzsupertools.command.RootScript.runRootCommand(java.lang.String):boolean");
    }

    public static synchronized String run(String[] cmd, String workdirectory) throws IOException {
        String result;
        synchronized (RootScript.class) {
            result = "";
            try {
                ProcessBuilder builder = new ProcessBuilder(cmd);
                if (workdirectory != null) {
                    builder.directory(new File(workdirectory));
                }
                builder.redirectErrorStream(true);
                InputStream in = builder.start().getInputStream();
                byte[] re = new byte[1024];
                while (in.read(re) != -1) {
                    System.out.println(new String(re));
                    result = String.valueOf(result) + new String(re);
                }
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static boolean getCmdResult(String cmd) {
        try {
            Process pro = Runtime.getRuntime().exec(cmd);
            pro.waitFor();
            BufferedInputStream br = new BufferedInputStream(pro.getInputStream());
            BufferedInputStream br2 = new BufferedInputStream(pro.getErrorStream());
            System.out.println("Input Stream:");
            while (true) {
                int ch = br.read();
                if (ch == -1) {
                    break;
                }
                System.out.print((char) ch);
            }
            System.out.println("Error Stream:");
            while (true) {
                int ch2 = br2.read();
                if (ch2 == -1) {
                    return true;
                }
                System.out.print((char) ch2);
            }
        } catch (IOException e) {
            IOException iOException = e;
            return false;
        } catch (InterruptedException e2) {
            return false;
        }
    }
}
