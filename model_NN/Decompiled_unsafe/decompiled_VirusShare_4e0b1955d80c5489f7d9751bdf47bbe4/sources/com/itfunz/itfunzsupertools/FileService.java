package com.itfunz.itfunzsupertools;

import android.content.Context;
import com.itfunz.itfunzsupertools.command.IconFormat;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FileService {
    static String cmd = null;
    static String cmd1 = null;
    static ArrayList<Map<String, Object>> data;
    static Map<String, Object> item;
    static StringBuilder res;
    static StringBuilder res1;

    public static void createRootCommand() {
        cmd = "/system/bin/sh /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode";
        res = new StringBuilder();
        RootScript.runScriptAsRoot(cmd, res, 1000);
        cmd1 = "/system/bin/sh /data/data/com.itfunz.itfunzsupertools/files/createanddelete.sh";
        res1 = new StringBuilder();
        RootScript.runScriptAsRoot(cmd1, res1, 1000);
    }

    public static void SetSystemConfig(String path, String value) {
        RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\nchmod 660 " + path + "\necho " + value + " > " + path + "\nchmod 440 " + path + "\nexit\n");
    }

    public static void SetSystemDefault(String path, String value) {
        RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\nchmod 660 " + path + "\necho " + value + " > " + path + "\n\nexit\n");
    }

    public static boolean delFolder(String folderPath) {
        boolean result = false;
        try {
            result = delAllFile(folderPath);
            File myFilePath = new File(folderPath.toString());
            myFilePath.delete();
            if (myFilePath.exists()) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    public static boolean delAllFile(String path) {
        File temp;
        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return false;
        }
        if (!file.isDirectory()) {
            return false;
        }
        String[] tempList = file.list();
        for (int i = 0; i < tempList.length; i++) {
            if (path.endsWith(File.separator)) {
                temp = new File(String.valueOf(path) + tempList[i]);
            } else {
                temp = new File(String.valueOf(path) + File.separator + tempList[i]);
            }
            if (temp.isFile()) {
                temp.delete();
            }
            if (temp.isDirectory()) {
                delAllFile(String.valueOf(path) + "/" + tempList[i]);
                delFolder(String.valueOf(path) + "/" + tempList[i]);
                flag = true;
            }
        }
        return flag;
    }

    /* JADX INFO: Multiple debug info for r6v1 java.lang.String: [D('context' android.content.Context), D('permission' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v9 int: [D('permission' java.lang.String), D('lastSeparator' int)] */
    /* JADX INFO: Multiple debug info for r2v1 java.lang.String: [D('upDirPath' java.lang.String), D('pathParent' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v2 java.lang.String: [D('context' android.content.Context), D('permission' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v13 int: [D('command' java.lang.String), D('lastSeparator' int)] */
    /* JADX INFO: Multiple debug info for r5v14 java.lang.String: [D('upDirPath' java.lang.String), D('lastSeparator' int)] */
    public static String getPermissionsByPath(String path, Context context) {
        File filePath = new File(path);
        if (filePath.isFile()) {
            filePath.getParent();
        }
        String pathParent = filePath.getPath();
        String pathName = filePath.getName();
        RootScript.runScriptAsRoot("cd \"" + pathParent + "\"" + "\nls -a -l > /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetail.cfg", new StringBuilder(), 1000);
        if (!new File("/data/data/com.itfunz.itfunzsupertools/files/tmpFileDetail.cfg").exists()) {
            RootScript.runRootCommand("cd \"" + pathParent.substring(0, pathParent.lastIndexOf("/")) + "\"" + "\nls -a -l -R > /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetail.cfg");
        }
        String permission = getFileContent(context, pathName);
        if (permission != null) {
            return permission;
        }
        String pathParent2 = pathParent.substring(0, pathParent.lastIndexOf("/"));
        if (!new File("/data/data/com.itfunz.itfunzsupertools/files/tmpFileDetailEn.cfg").exists()) {
            RootScript.runRootCommand("cd \"" + pathParent2 + "\"" + "\nls -a -l -R > /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetailEn.cfg");
        }
        return getFileContentEn(context, pathName);
    }

    public static String getFileContent(Context context, String fileName) {
        String Content;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(context.openFileInput("tmpFileDetail.cfg")));
            do {
                Content = br.readLine();
                if (Content == null) {
                    return Content;
                }
            } while (Content.lastIndexOf(fileName) == -1);
            return Content.substring(1, 10);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        } catch (IOException e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static String getFileContentEn(Context context, String fileName) {
        String Content;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(context.openFileInput("tmpFileDetailEn.cfg")));
            do {
                Content = br.readLine();
                if (Content == null) {
                    return Content;
                }
            } while (Content.lastIndexOf(fileName) == -1);
            return Content.substring(1, 10);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        } catch (IOException e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static void changeFileRootPermissions(String filePath) {
        RootScript.runRootCommand("chmod 777 \"" + filePath + "\"");
    }

    public static void changeSimpleFilePermissions() {
        RootScript.runRootCommand("mount -t yaffs2 -o ro,remount /rootfs /\nmount -t yaffs2 -o ro,remount /dev/block/mtdblock6 /system\nexit");
    }

    public static void changeRootPermissions() {
        RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /rootfs /\nmount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\nexit");
    }

    public static char[] geteveryPermissions(String permissions) {
        int count = permissions.length();
        char[] c = new char[count];
        for (int i = 0; i < count; i++) {
            c[i] = permissions.charAt(i);
        }
        return c;
    }

    public static String getDefaultPermissions(String permissions) {
        int count = permissions.length();
        char[] cArr = new char[count];
        int[] num = new int[count];
        char[] c = geteveryPermissions(permissions);
        for (int i = 0; i < c.length; i++) {
            if (c[i] == 'r') {
                num[i] = 4;
            } else if (c[i] == 'w') {
                num[i] = 2;
            } else if (c[i] == 'x') {
                num[i] = 1;
            } else if (c[i] == 't') {
                num[i] = 0;
            } else {
                num[i] = 0;
            }
        }
        String user = String.valueOf(num[0] + num[1] + num[2]);
        String group = String.valueOf(num[3] + num[4] + num[5]);
        return String.valueOf(user) + group + String.valueOf(num[6] + num[7] + num[8]);
    }

    public static void copyFile(String oldPathFile, String newPathFile) throws Exception {
        int bytesum = 0;
        try {
            if (new File(oldPathFile).exists()) {
                InputStream inStream = new FileInputStream(oldPathFile);
                FileOutputStream fs = new FileOutputStream(newPathFile);
                byte[] buffer = new byte[1444];
                while (true) {
                    int byteread = inStream.read(buffer);
                    if (byteread == -1) {
                        inStream.close();
                        return;
                    }
                    bytesum += byteread;
                    System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public static void copyFolder(String sourceFolder, String destinationFolder) throws Exception {
        File temp;
        try {
            new File(destinationFolder).mkdirs();
            String[] file = new File(sourceFolder).list();
            for (int i = 0; i < file.length; i++) {
                if (sourceFolder.endsWith(File.separator)) {
                    temp = new File(String.valueOf(sourceFolder) + file[i]);
                } else {
                    temp = new File(String.valueOf(sourceFolder) + File.separator + file[i]);
                }
                if (temp.isFile()) {
                    FileInputStream input = new FileInputStream(temp);
                    FileOutputStream output = new FileOutputStream(String.valueOf(destinationFolder) + "/" + temp.getName().toString());
                    byte[] b = new byte[5120];
                    while (true) {
                        int len = input.read(b);
                        if (len == -1) {
                            break;
                        }
                        output.write(b, 0, len);
                    }
                    output.flush();
                    output.close();
                    input.close();
                }
                if (temp.isDirectory()) {
                    copyFolder(String.valueOf(sourceFolder) + "/" + file[i], String.valueOf(destinationFolder) + "/" + file[i]);
                }
            }
        } catch (Exception e) {
            throw new Exception("复制整个文件夹内容操作出错", e);
        }
    }

    public static ArrayList<Map<String, Object>> searchFile(String fileName, String filePath) {
        try {
            data = new ArrayList<>();
            for (File afile : new File(filePath).listFiles()) {
                item = new HashMap();
                String afileName = afile.getName().toLowerCase().trim();
                String afilePath = afile.getPath();
                if (afileName.indexOf(fileName) != -1) {
                    item.put("fileName", afileName);
                    item.put("fileAbility", afilePath);
                    item.put("CheckId", Integer.valueOf((int) R.drawable.icon_null));
                    if (afile.isDirectory()) {
                        item.put("fileIcon", Integer.valueOf((int) R.drawable.icon_folder));
                        data.add(item);
                        searchFileInFolder(fileName, afile);
                    } else {
                        item.put("fileIcon", Integer.valueOf(IconFormat.getIconFormat(afileName)));
                        data.add(item);
                    }
                }
                if (afile.isDirectory()) {
                    searchFileInFolder(fileName, afile);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public static void searchFileInFolder(String fileName, File file) {
        try {
            for (File afile : file.listFiles()) {
                item = new HashMap();
                String afileName = afile.getName().toLowerCase().trim();
                String afilePath = afile.getPath();
                if (afileName.indexOf(fileName) != -1) {
                    item.put("fileName", afileName);
                    item.put("fileAbility", afilePath);
                    item.put("CheckId", Integer.valueOf((int) R.drawable.icon_null));
                    if (afile.isDirectory()) {
                        item.put("fileIcon", Integer.valueOf((int) R.drawable.icon_folder));
                        data.add(item);
                        searchFileInFolder(fileName, afile);
                    } else {
                        item.put("fileIcon", Integer.valueOf(IconFormat.getIconFormat(afileName)));
                        data.add(item);
                    }
                }
                if (afile.isDirectory()) {
                    searchFileInFolder(fileName, afile);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
