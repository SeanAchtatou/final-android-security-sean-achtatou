package com.itfunz.itfunzsupertools;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class CpuControl extends Activity {
    private Button btnApply;
    private Button btnCancel;
    private Button btnDefault;
    /* access modifiers changed from: private */
    public CheckBox ckAuto;
    private EditText etRate1;
    private EditText etRate2;
    private EditText etRate3;
    private EditText etRate4;
    private EditText etRate5;
    private EditText etVsel1;
    private EditText etVsel2;
    private EditText etVsel3;
    private EditText etVsel4;
    private EditText etVsel5;
    StringBuilder res;
    String stRate1;
    String stRate2;
    String stRate3;
    String stRate4;
    String stRate5;
    String stVsel1;
    String stVsel2;
    String stVsel3;
    String stVsel4;
    String stVsel5;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.cpucontrol);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        this.etRate1 = (EditText) findViewById(R.id.rate1);
        this.etVsel1 = (EditText) findViewById(R.id.vsel1);
        this.etRate2 = (EditText) findViewById(R.id.rate2);
        this.etVsel2 = (EditText) findViewById(R.id.vsel2);
        this.etRate3 = (EditText) findViewById(R.id.rate3);
        this.etVsel3 = (EditText) findViewById(R.id.vsel3);
        this.etRate4 = (EditText) findViewById(R.id.rate4);
        this.etVsel4 = (EditText) findViewById(R.id.vsel4);
        this.etRate5 = (EditText) findViewById(R.id.rate5);
        this.etVsel5 = (EditText) findViewById(R.id.vsel5);
        this.btnApply = (Button) findViewById(R.id.apply);
        this.btnDefault = (Button) findViewById(R.id.returndefault);
        this.btnCancel = (Button) findViewById(R.id.space);
        this.ckAuto = (CheckBox) findViewById(R.id.autoStart);
        this.stRate1 = settings.getString("dfRate1", "125000");
        this.stVsel1 = settings.getString("dfVsel1", "32");
        this.stRate2 = settings.getString("dfRate2", "250000");
        this.stVsel2 = settings.getString("dfVsel2", "39");
        this.stRate3 = settings.getString("dfRate3", "500000");
        this.stVsel3 = settings.getString("dfVsel3", "50");
        this.stRate4 = settings.getString("dfRate4", "550000");
        this.stVsel4 = settings.getString("dfVsel4", "56");
        this.stRate5 = settings.getString("dfRate5", "600000");
        this.stVsel5 = settings.getString("dfVsel5", "62");
        this.etRate1.setText(this.stRate1);
        this.etVsel1.setText(this.stVsel1);
        this.etRate2.setText(this.stRate2);
        this.etVsel2.setText(this.stVsel2);
        this.etRate3.setText(this.stRate3);
        this.etVsel3.setText(this.stVsel3);
        this.etRate4.setText(this.stRate4);
        this.etVsel4.setText(this.stVsel4);
        this.etRate5.setText(this.stRate5);
        this.etVsel5.setText(this.stVsel5);
        this.btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.exit(0);
            }
        });
        this.btnDefault.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CpuControl.this.setCpuControl(3, "125000", "32", "250000", "39", "500000", "50", "550000", "56", "600000", "62");
            }
        });
        this.btnApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!CpuControl.this.getEtValue()) {
                    Toast.makeText(CpuControl.this, "您的数值设置存在问题，请重新设置！", 0).show();
                } else if (CpuControl.this.ckAuto.isChecked()) {
                    CpuControl.this.setCpuControl(2, CpuControl.this.stRate1, CpuControl.this.stVsel1, CpuControl.this.stRate2, CpuControl.this.stVsel2, CpuControl.this.stRate3, CpuControl.this.stVsel3, CpuControl.this.stRate4, CpuControl.this.stVsel4, CpuControl.this.stRate5, CpuControl.this.stVsel5);
                } else {
                    CpuControl.this.setCpuControl(1, CpuControl.this.stRate1, CpuControl.this.stVsel1, CpuControl.this.stRate2, CpuControl.this.stVsel2, CpuControl.this.stRate3, CpuControl.this.stVsel3, CpuControl.this.stRate4, CpuControl.this.stVsel4, CpuControl.this.stRate5, CpuControl.this.stVsel5);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void setCpuControl(int value, String Rate1, String Vsel1, String Rate2, String Vsel2, String Rate3, String Vsel3, String Rate4, String Vsel4, String Rate5, String Vsel5) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        this.stRate1 = settings.getString("dfRate1", "120000");
        this.stVsel1 = settings.getString("dfVsel1", "32");
        this.stRate2 = settings.getString("dfRate2", "250000");
        this.stVsel2 = settings.getString("dfVsel2", "39");
        this.stRate3 = settings.getString("dfRate3", "500000");
        this.stVsel3 = settings.getString("dfVsel3", "50");
        this.stRate4 = settings.getString("dfRate4", "550000");
        this.stVsel4 = settings.getString("dfVsel4", "56");
        this.stRate5 = settings.getString("dfRate5", "600000");
        this.stVsel5 = settings.getString("dfVsel5", "62");
        settings.edit().putString("dfRate1", Rate1).commit();
        settings.edit().putString("dfVsel1", Vsel1).commit();
        settings.edit().putString("dfRate2", Rate2).commit();
        settings.edit().putString("dfVsel2", Vsel2).commit();
        settings.edit().putString("dfRate3", Rate3).commit();
        settings.edit().putString("dfVsel3", Vsel3).commit();
        settings.edit().putString("dfRate4", Rate4).commit();
        settings.edit().putString("dfVsel4", Vsel4).commit();
        settings.edit().putString("dfRate5", Rate5).commit();
        settings.edit().putString("dfVsel5", Vsel5).commit();
        switch (value) {
            case 1:
                setConfig(Rate1, Vsel1, Rate2, Vsel2, Rate3, Vsel3, Rate4, Vsel4, Rate5, Vsel5);
                return;
            case 2:
                SetCPUControlAndFile(Rate1, Vsel1, Rate2, Vsel2, Rate3, Vsel3, Rate4, Vsel4, Rate5, Vsel5);
                FileService.createRootCommand();
                Toast.makeText(this, "设置成功！", 0).show();
                return;
            case 3:
                SetCPUControlAndFile(Rate1, Vsel1, Rate2, Vsel2, Rate3, Vsel3, Rate4, Vsel4, Rate5, Vsel5);
                FileService.createRootCommand();
                Toast.makeText(this, "设置成功！", 0).show();
                return;
            default:
                return;
        }
    }

    private void setConfig(String Rate1, String Vsel1, String Rate2, String Vsel2, String Rate3, String Vsel3, String Rate4, String Vsel4, String Rate5, String Vsel5) {
        RootScript.runRootCommand("insmod /data/data/com.itfunz.itfunzsupertools/files/overclock.ko omap2_clk_init_cpufreq_table_addr=0x$(busybox grep omap2_clk_init_cpufreq_table /proc/kallsyms|busybox sed '2,$d s/ .*//g')");
        RootScript.runRootCommand("echo 1 " + Rate1 + "000 " + Vsel1 + " > /proc/overclock/mpu_opps");
        RootScript.runRootCommand("echo 2 " + Rate2 + "000 " + Vsel2 + " > /proc/overclock/mpu_opps");
        RootScript.runRootCommand("echo 3 " + Rate3 + "000 " + Vsel3 + " > /proc/overclock/mpu_opps");
        RootScript.runRootCommand("echo 4 " + Rate4 + "000 " + Vsel4 + " > /proc/overclock/mpu_opps");
        RootScript.runRootCommand("echo 5 " + Rate5 + "000 " + Vsel5 + " > /proc/overclock/mpu_opps");
        RootScript.runRootCommand("echo 0 " + Rate1 + " > /proc/overclock/freq_table");
        RootScript.runRootCommand("echo 1 " + Rate2 + " > /proc/overclock/freq_table");
        RootScript.runRootCommand("echo 2 " + Rate3 + " > /proc/overclock/freq_table");
        RootScript.runRootCommand("echo 3 " + Rate5 + " > /proc/overclock/freq_table");
        RootScript.runRootCommand("echo " + Rate5 + "> /proc/overclock/max_rate");
        RootScript.runRootCommand("echo " + Vsel5 + "> /proc/overclock/max_vsel");
        Toast.makeText(this, "设置成功！", 0).show();
    }

    /* access modifiers changed from: private */
    public boolean getEtValue() {
        this.stRate1 = this.etRate1.getText().toString();
        this.stVsel1 = this.etVsel1.getText().toString();
        this.stRate2 = this.etRate2.getText().toString();
        this.stVsel2 = this.etVsel2.getText().toString();
        this.stRate3 = this.etRate3.getText().toString();
        this.stVsel3 = this.etVsel3.getText().toString();
        this.stRate4 = this.etRate4.getText().toString();
        this.stVsel4 = this.etVsel4.getText().toString();
        this.stRate5 = this.etRate5.getText().toString();
        this.stVsel5 = this.etVsel5.getText().toString();
        if (this.stRate1.equals("")) {
            this.stRate1 = "0";
        }
        if (this.stVsel1.equals("")) {
            this.stVsel1 = "0";
        }
        if (this.stRate2.equals("")) {
            this.stRate2 = "0";
        }
        if (this.stVsel2.equals("")) {
            this.stVsel2 = "0";
        }
        if (this.stRate3.equals("")) {
            this.stRate3 = "0";
        }
        if (this.stVsel3.equals("")) {
            this.stVsel3 = "0";
        }
        if (this.stRate4.equals("")) {
            this.stRate4 = "0";
        }
        if (this.stVsel4.equals("")) {
            this.stVsel4 = "0";
        }
        if (this.stRate5.equals("")) {
            this.stRate5 = "0";
        }
        if (this.stVsel5.equals("")) {
            this.stVsel5 = "0";
        }
        int Rate1 = Integer.valueOf(this.stRate1).intValue();
        int Vsel1 = Integer.valueOf(this.stVsel1).intValue();
        int Rate2 = Integer.valueOf(this.stRate2).intValue();
        int Vsel2 = Integer.valueOf(this.stVsel2).intValue();
        int Rate3 = Integer.valueOf(this.stRate3).intValue();
        int Vsel3 = Integer.valueOf(this.stVsel3).intValue();
        int Rate4 = Integer.valueOf(this.stRate4).intValue();
        int Vsel4 = Integer.valueOf(this.stVsel4).intValue();
        int Rate5 = Integer.valueOf(this.stRate5).intValue();
        int Vsel5 = Integer.valueOf(this.stVsel5).intValue();
        if (Rate1 < 125000 || Rate1 > 1200000 || Rate2 < 125000 || Rate2 > 1200000 || Rate3 < 125000 || Rate3 > 1200000 || Rate4 < 125000 || Rate4 > 1200000 || Rate5 < 125000 || Rate5 > 1200000 || Vsel1 < 16 || Vsel1 > 80 || Vsel2 < 16 || Vsel2 > 80 || Vsel3 < 16 || Vsel3 > 80 || Vsel4 < 16 || Vsel4 > 80 || Vsel5 < 16 || Vsel5 > 80) {
            return false;
        }
        return true;
    }

    private void SetCPUControlAndFile(String Rate1, String Vsel1, String Rate2, String Vsel2, String Rate3, String Vsel3, String Rate4, String Vsel4, String Rate5, String Vsel5) {
        safeMode();
        WriteFile("mot_boot_mode", "#!/system/bin/sh\nexport PATH=/system/bin:$PATH\nmot_boot_mode.bin\nif [ -f /system/lib/modules/ext2.ko ]\nthen\ninsmod /system/lib/modules/ext2.ko\nmount -t ext2 /dev/block/mmcblk0p2 /system/sdcard2\nfi\n#custom script\n/system/bin/sh script.sh\n");
        RootScript.runRootCommand("echo insmod /data/data/com.itfunz.itfunzsupertools/files/overclock.ko omap2_clk_init_cpufreq_table_addr=0x$(busybox grep omap2_clk_init_cpufreq_table /proc/kallsyms|busybox sed '2,$d s/ .*//g') >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
        RootScript.runRootCommand("echo 'echo " + Vsel5 + " > /proc/overclock/max_vsel' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
        RootScript.runRootCommand("echo 'echo " + Rate5 + " > /proc/overclock/max_rate' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
        RootScript.runRootCommand("echo 'echo 1 " + Rate1 + "000 " + Vsel1 + " > /proc/overclock/mpu_opps' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
        RootScript.runRootCommand("echo 'echo 2 " + Rate2 + "000 " + Vsel2 + " > /proc/overclock/mpu_opps' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
        RootScript.runRootCommand("echo 'echo 3 " + Rate3 + "000 " + Vsel3 + " > /proc/overclock/mpu_opps' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
        RootScript.runRootCommand("echo 'echo 4 " + Rate4 + "000 " + Vsel4 + " > /proc/overclock/mpu_opps' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
        RootScript.runRootCommand("echo 'echo 5 " + Rate5 + "000 " + Vsel5 + " > /proc/overclock/mpu_opps' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
        RootScript.runRootCommand("echo 'echo 0 " + Rate1 + " > /proc/overclock/freq_table' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
        RootScript.runRootCommand("echo 'echo 1 " + Rate2 + " > /proc/overclock/freq_table' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
        RootScript.runRootCommand("echo 'echo 2 " + Rate3 + " > /proc/overclock/freq_table' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
        RootScript.runRootCommand("echo 'echo 3 " + Rate5 + " > /proc/overclock/freq_table' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
    }

    private void WriteFile(String path, String value) {
        try {
            OutputStream os = openFileOutput(path, 0);
            OutputStreamWriter osw = new OutputStreamWriter(os);
            osw.write(value);
            osw.close();
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private void safeMode() {
        if (!new File("/system/bin/mot_boot_mode.bin").exists()) {
            RootScript.runScriptAsRoot("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\ncp /system/bin/mot_boot_mode /system/bin/mot_boot_mode.bin", this.res, 1000);
        }
    }
}
