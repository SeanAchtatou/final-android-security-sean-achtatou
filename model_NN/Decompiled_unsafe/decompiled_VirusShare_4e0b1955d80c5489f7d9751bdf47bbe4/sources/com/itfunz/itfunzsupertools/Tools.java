package com.itfunz.itfunzsupertools;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.itfunz.itfunzsupertools.command.ItfunzCheckSum;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.regex.Pattern;

public class Tools extends PreferenceActivity {
    public static final String Light_Lock = "Light_Lock";
    public static final String Light_Value = "Light_Value";
    /* access modifiers changed from: private */
    public int FinalStaticLight = 80;
    /* access modifiers changed from: private */
    public boolean bDisableWifi = false;
    String cmd = null;
    String cmd1 = null;
    StringBuilder res;
    StringBuilder res1;
    SharedPreferences settings;

    public PreferenceActivity getInstance() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        String GetVersion;
        super.onCreate(savedInstanceState);
        setTitle("ITFUNZ超级工具箱V3.9.7 Build 1022");
        startService(new Intent(this, ItfunzSuperToolsService.class));
        this.settings = PreferenceManager.getDefaultSharedPreferences(this);
        String PhoneVersion = Build.VERSION.RELEASE.trim().toString();
        String CheckVersion = getString(R.string.ROM_Version).trim().toString();
        int VersionLength = PhoneVersion.length();
        if (VersionLength <= 8) {
            GetVersion = "Original System";
        } else {
            GetVersion = PhoneVersion.substring(0, VersionLength - 4);
        }
        File file = new File("/system/bin/su");
        File file2 = new File("/system/xbin/su");
        File file3 = new File("/sbin/su");
        if (!file.exists() && !file2.exists() && !file3.exists()) {
            new AlertDialog.Builder(this).setTitle("权限检测").setMessage("您的手机尚未获取Root权限，这将导致工具箱部分功能无法正常使用，是否由工具箱协助您获取Root权限？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Tools.this, "正在获取Root权限中，Root后本程序会自动关闭，请重新开启并在弹出的提示框中选Allow。", 1).show();
                    new rootyourPhone().execute(new Boolean[0]);
                }
            }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Tools.this.finish();
                }
            }).setOnKeyListener(new DialogInterface.OnKeyListener() {
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode != 4) {
                        return false;
                    }
                    Tools.this.finish();
                    return false;
                }
            }).create().show();
        }
        if (CheckVersion.indexOf(GetVersion) == -1) {
            createSystemVersionDialog().show();
        }
        if (!RootScript.getCmdResult("busybox")) {
            ProgressDialog pdialog = ProgressDialog.show(this, "环境构造", "安装Busybox中,请稍候...");
            final Handler handler = new Handler();
            final ProgressDialog progressDialog = pdialog;
            new Thread() {
                public void run() {
                    super.run();
                    Tools.this.createFile("busybox");
                    RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\ncp /data/data/com.itfunz.itfunzsupertools/files/busybox /system/bin/busybox");
                    RootScript.runRootCommand("chmod 777 /system/bin/busybox\nmount -t yaffs2 -o ro,remount /dev/block/mtdblock6 /system\n");
                    Handler handler = handler;
                    final ProgressDialog progressDialog = progressDialog;
                    handler.post(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                }
            }.start();
        }
        new Handler().postDelayed(new Runnable() {
            public void run() {
                new checkNewVersionTask().execute(new Object[0]);
                if (Tools.this.settings.getBoolean("ad_filter_system", false)) {
                    new replaceHosts().execute(new Object[0]);
                }
            }
        }, 5000);
        if (ItfunzCheckSum.isIsFroyo()) {
            if (!new File("/data/data/com.itfunz.itfunzsupertools/files/overclock12.ko").exists()) {
                createFile("overclock12.ko");
            }
        } else if (!new File("/data/data/com.itfunz.itfunzsupertools/files/overclock.ko").exists()) {
            createFile("overclock.ko");
        }
        if (!new File("/data/data/com.itfunz.itfunzsupertools/files/createanddelete.sh").exists()) {
            WriteFile("createanddelete.sh", "#!/system/bin/sh\nmount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\n cat /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode > /system/bin/mot_boot_mode\nchmod 777 /system/bin/mot_boot_mode");
        }
        setPreferenceScreen(createPreferenceHierarchy());
        this.settings = PreferenceManager.getDefaultSharedPreferences(this);
        String pwd_password = this.settings.getString("tool_pwd", "");
        if (this.settings.getBoolean("tool_password", false)) {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(1);
            TextView textView = new TextView(this);
            textView.setText("您开启了工具箱密码保护功能，请输入六位保护码:");
            linearLayout.addView(textView);
            EditText editText = new EditText(this);
            linearLayout.addView(editText);
            final EditText editText2 = editText;
            final String str = pwd_password;
            final String str2 = pwd_password;
            new AlertDialog.Builder(this).setTitle("密码保护").setView(linearLayout).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (!editText2.getText().toString().trim().toLowerCase().equals(str)) {
                        Toast.makeText(Tools.this, "密码错误，请重新输入！", 0).show();
                        Tools.this.onCreate(null);
                    }
                }
            }).setNeutralButton("修改密码", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    LinearLayout ellayout = new LinearLayout(Tools.this);
                    ellayout.setOrientation(1);
                    TextView tvPassword = new TextView(Tools.this);
                    tvPassword.setText("请输入旧密码");
                    ellayout.addView(tvPassword);
                    final EditText etoldPassWord = new EditText(Tools.this);
                    ellayout.addView(etoldPassWord);
                    TextView tvNewPassword = new TextView(Tools.this);
                    tvNewPassword.setText("请输入新密码");
                    ellayout.addView(tvNewPassword);
                    final EditText etNewPassWord = new EditText(Tools.this);
                    ellayout.addView(etNewPassWord);
                    TextView tvRePassword = new TextView(Tools.this);
                    tvRePassword.setText("请再次输入新密码");
                    ellayout.addView(tvRePassword);
                    final EditText etReNewPassWord = new EditText(Tools.this);
                    ellayout.addView(etReNewPassWord);
                    AlertDialog.Builder view = new AlertDialog.Builder(Tools.this).setTitle("修改密码").setView(ellayout);
                    final String str = str2;
                    view.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String oldpwd = etoldPassWord.getText().toString().trim().toLowerCase();
                            String newPassword = etNewPassWord.getText().toString().trim().toLowerCase();
                            String newRePassword = etReNewPassWord.getText().toString().trim().toLowerCase();
                            if (oldpwd.equals(str)) {
                                if (!(newPassword.length() == 6) || !(newRePassword.length() == 6)) {
                                    Toast.makeText(Tools.this, "密码长度不正确，请重新输入！", 0).show();
                                    Tools.this.onCreate(null);
                                } else if (newPassword.equals(newRePassword)) {
                                    Tools.this.settings.edit().putString("tool_pwd", newPassword).commit();
                                    Toast.makeText(Tools.this, "设置成功！", 0).show();
                                } else {
                                    Toast.makeText(Tools.this, "两次输入的密码不一致，请重新输入！", 0).show();
                                    Tools.this.onCreate(null);
                                }
                            } else {
                                Toast.makeText(Tools.this, "旧密码错误，请重新输入！", 0).show();
                                Tools.this.onCreate(null);
                            }
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Tools.this.onCreate(null);
                        }
                    }).setOnKeyListener(new DialogInterface.OnKeyListener() {
                        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                            if (keyCode != 4) {
                                return false;
                            }
                            Tools.this.finish();
                            return false;
                        }
                    }).create().show();
                }
            }).setOnKeyListener(new DialogInterface.OnKeyListener() {
                public boolean onKey(DialogInterface arg0, int arg1, KeyEvent arg2) {
                    if (arg1 != 4) {
                        return false;
                    }
                    Tools.this.finish();
                    return false;
                }
            }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Tools.this.finish();
                }
            }).create().show();
        }
        if (this.settings.getBoolean("the_first", true)) {
            try {
                Runtime.getRuntime().exec("su");
            } catch (IOException e) {
                e.printStackTrace();
            }
            new AlertDialog.Builder(this).setTitle("友情提示").setMessage("MileStone用户您好，欢迎使用ITFUNZ超级工具箱，我们检测到您是第一次使用本程序，为了您能够有效的使用本工具箱强化手机功能，如果您安装有SuperUser类Root权限控制程序，请注意将工具箱列入白名单。如果您安装有进程管理器、开机启动项控制程序，请将工具箱的六个核心服务列入白名单。服务名称如下：\nItfunzSuperToolsService、HoldScreenLightService、CallledService、ItfunzSuperToolsWidgetService、ItfunzSuperToolsWidgetServiceMid、ItfunzSuperToolsWidgetServiceBig\n如果您没有安装，则可以忽略本文。\n软件意见和BUG反馈请发送邮件至Martincz@itfunz.com或前往http://www.itfunz.com发帖。最后感谢您对ITFUNZ的支持！").setPositiveButton("确定", (DialogInterface.OnClickListener) null).create().show();
            this.settings.edit().putBoolean("the_first", false).commit();
        }
        getPreferenceScreen().findPreference("cpuState").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                int MaxRateMhz;
                String resultMIPS = "";
                String resultMaxVsel = "";
                try {
                    resultMIPS = RootScript.run(new String[]{"/system/bin/cat", "/proc/cpuinfo"}, "/system/bin/").trim();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String MIPS = resultMIPS.substring(resultMIPS.indexOf("BogoMIPS") + 10, resultMIPS.indexOf("Features")).trim();
                String[] cmdMaxVsel = {"/system/bin/cat", "/proc/overclock/max_vsel"};
                try {
                    MaxRateMhz = Integer.valueOf(RootScript.run(new String[]{"/system/bin/cat", "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"}, "system/bin/").trim()).intValue() / 1000;
                    resultMaxVsel = RootScript.run(cmdMaxVsel, "system/bin/").trim();
                } catch (Exception e2) {
                    MaxRateMhz = 0;
                }
                if (!new File("/proc/overclock").exists() || MaxRateMhz == 0) {
                    new AlertDialog.Builder(Tools.this).setTitle("CPU性能状态").setMessage("实时频率：" + MIPS + "Mhz\n最高频率：" + MaxRateMhz + "Mhz\n您未设置超频").setPositiveButton("确定", (DialogInterface.OnClickListener) null).create().show();
                    return true;
                }
                new AlertDialog.Builder(Tools.this).setTitle("CPU性能状态").setMessage("实时频率：" + MIPS + "Mhz\n最高频率：" + MaxRateMhz + "Mhz\n最高电压：" + resultMaxVsel).setPositiveButton("确定", (DialogInterface.OnClickListener) null).create().show();
                return true;
            }
        });
        getPreferenceScreen().findPreference("cpuMHZ").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final String cpuConfig = newValue.toString();
                final int cpuValue = Integer.parseInt(cpuConfig);
                if (Tools.this.getExistsOtherTools()) {
                    new AlertDialog.Builder(Tools.this).setTitle("友情提示").setMessage("我们检测到目前PlusToolbox正在管理超频功能。点击“继续”，将把超频功能控制权移交给ITFUNZ超级工具箱。单击“取消”，将维持PlusToolbox的控制权不变并取消您的当前操作。").setPositiveButton("继续", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            Tools.this.safeMode();
                            if (ItfunzCheckSum.isIsFroyo()) {
                                switch (cpuValue) {
                                    case 600:
                                        Tools.this.MoveAndSetFile("58", "600000", "overclock12");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 700:
                                        Tools.this.MoveAndSetFile("58", "700000", "overclock12");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 800:
                                        Tools.this.MoveAndSetFile("58", "800000", "overclock12");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 900:
                                        Tools.this.MoveAndSetFile("58", "900000", "overclock12");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 1000:
                                        Tools.this.MoveAndSetFile("60", "1000000", "overclock12");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 1100:
                                        Tools.this.MoveAndSetFile("76", "1100000", "overclock12");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 1200:
                                        Tools.this.MoveAndSetFile("80", "1200000", "overclock12");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    default:
                                        Tools.this.MoveAndSetFile("56", "550000", "overclock12");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                }
                            } else {
                                switch (cpuValue) {
                                    case 600:
                                        Tools.this.MoveAndSetFile("58", "600000", "overclock");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 700:
                                        Tools.this.MoveAndSetFile("58", "700000", "overclock");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 800:
                                        Tools.this.MoveAndSetFile("58", "800000", "overclock");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 900:
                                        Tools.this.MoveAndSetFile("58", "900000", "overclock");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 1000:
                                        Tools.this.MoveAndSetFile("60", "1000000", "overclock");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 1100:
                                        Tools.this.MoveAndSetFile("76", "1100000", "overclock");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    case 1200:
                                        Tools.this.MoveAndSetFile("80", "1200000", "overclock");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                    default:
                                        Tools.this.MoveAndSetFile("56", "550000", "overclock");
                                        FileService.createRootCommand();
                                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                                        Tools.this.settings.edit().putString("cpuMHZ", cpuConfig).commit();
                                        Tools.this.onCreate(null);
                                        return;
                                }
                            }
                        }
                    }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create().show();
                    return false;
                }
                Tools.this.safeMode();
                if (!ItfunzCheckSum.isIsFroyo()) {
                    switch (cpuValue) {
                        case 600:
                            Tools.this.MoveAndSetFile("58", "600000", "overclock");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 700:
                            Tools.this.MoveAndSetFile("58", "700000", "overclock");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 800:
                            Tools.this.MoveAndSetFile("58", "800000", "overclock");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 900:
                            Tools.this.MoveAndSetFile("58", "900000", "overclock");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 1000:
                            Tools.this.MoveAndSetFile("60", "1000000", "overclock");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 1100:
                            Tools.this.MoveAndSetFile("76", "1100000", "overclock");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 1200:
                            Tools.this.MoveAndSetFile("80", "1200000", "overclock");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        default:
                            Tools.this.MoveAndSetFile("56", "550000", "overclock");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                    }
                } else {
                    switch (cpuValue) {
                        case 600:
                            Tools.this.MoveAndSetFile("58", "600000", "overclock12");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 700:
                            Tools.this.MoveAndSetFile("58", "700000", "overclock12");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 800:
                            Tools.this.MoveAndSetFile("58", "800000", "overclock12");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 900:
                            Tools.this.MoveAndSetFile("58", "900000", "overclock12");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 1000:
                            Tools.this.MoveAndSetFile("60", "1000000", "overclock12");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 1100:
                            Tools.this.MoveAndSetFile("76", "1100000", "overclock12");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        case 1200:
                            Tools.this.MoveAndSetFile("80", "1200000", "overclock12");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                        default:
                            Tools.this.MoveAndSetFile("56", "550000", "overclock12");
                            FileService.createRootCommand();
                            Toast.makeText(Tools.this, "超频成功！", 0).show();
                            break;
                    }
                }
                return true;
            }
        });
        getPreferenceScreen().findPreference("BaseBand").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String baseConfig = newValue.toString();
                if (baseConfig.equals("欧版港版基带")) {
                    Tools.this.createFile("UK_File_Audio");
                    Tools.this.createFile("UK_File_Audio2");
                    Tools.this.createFile("UK_File_GSM");
                    Tools.this.createFile("UK_File_Seem_Flex_Tables");
                    Tools.this.createFile("UK_generic_pds_init");
                    Tools.this.cmd = "mount -o remount,rw /dev/mtd/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/UK_File_Audio /system/etc/motorola/bp_nvm_default/File_Audio\ncp -f /data/data/com.itfunz.itfunzsupertools/files/UK_File_Audio2 /system/etc/motorola/bp_nvm_default/File_Audio2\ncp -f /data/data/com.itfunz.itfunzsupertools/files/UK_File_GSM /system/etc/motorola/bp_nvm_default/File_GSM\ncp -f /data/data/com.itfunz.itfunzsupertools/files/UK_File_Seem_Flex_Tables /system/etc/motorola/bp_nvm_default/File_Seem_Flex_Tables\ncp -f /data/data/com.itfunz.itfunzsupertools/files/UK_generic_pds_init /system/etc/motorola/bp_nvm_default/generic_pds_init\nmount -o remount,ro /dev/mtd/mtdblock6 /system\n";
                    Tools.this.res = new StringBuilder();
                    RootScript.runScriptAsRoot(Tools.this.cmd, Tools.this.res, 1000);
                } else if (baseConfig.equals("最新港版基带")) {
                    Tools.this.createFile("HK_File_Audio");
                    Tools.this.createFile("HK_File_Audio2");
                    Tools.this.createFile("HK_File_GSM");
                    Tools.this.createFile("HK_File_Seem_Flex_Tables");
                    Tools.this.createFile("HK_generic_pds_init");
                    Tools.this.cmd = "mount -o remount,rw /dev/mtd/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/HK_File_Audio /system/etc/motorola/bp_nvm_default/File_Audio\ncp -f /data/data/com.itfunz.itfunzsupertools/files/HK_File_Audio2 /system/etc/motorola/bp_nvm_default/File_Audio2\ncp -f /data/data/com.itfunz.itfunzsupertools/files/HK_File_GSM /system/etc/motorola/bp_nvm_default/File_GSM\ncp -f /data/data/com.itfunz.itfunzsupertools/files/HK_File_Seem_Flex_Tables /system/etc/motorola/bp_nvm_default/File_Seem_Flex_Tables\ncp -f /data/data/com.itfunz.itfunzsupertools/files/HK_generic_pds_init /system/etc/motorola/bp_nvm_default/generic_pds_init\nmount -o remount,ro /dev/mtd/mtdblock6 /system\n";
                    Tools.this.res = new StringBuilder();
                    RootScript.runScriptAsRoot(Tools.this.cmd, Tools.this.res, 1000);
                } else if (baseConfig.equals("最新欧版基带")) {
                    Tools.this.createFile("UK_New_File_Audio");
                    Tools.this.createFile("UK_New_File_Audio2");
                    Tools.this.createFile("UK_New_File_GSM");
                    Tools.this.createFile("UK_New_File_Seem_Flex_Tables");
                    Tools.this.createFile("UK_New_generic_pds_init");
                    Tools.this.cmd = "mount -o remount,rw /dev/mtd/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/UK_New_File_Audio /system/etc/motorola/bp_nvm_default/File_Audio\ncp -f /data/data/com.itfunz.itfunzsupertools/files/UK_New_File_Audio2 /system/etc/motorola/bp_nvm_default/File_Audio2\ncp -f /data/data/com.itfunz.itfunzsupertools/files/UK_New_File_GSM /system/etc/motorola/bp_nvm_default/File_GSM\ncp -f /data/data/com.itfunz.itfunzsupertools/files/UK_New_File_Seem_Flex_Tables /system/etc/motorola/bp_nvm_default/File_Seem_Flex_Tables\ncp -f /data/data/com.itfunz.itfunzsupertools/files/UK_New_generic_pds_init /system/etc/motorola/bp_nvm_default/generic_pds_init\nmount -o remount,ro /dev/mtd/mtdblock6 /system\n";
                    Tools.this.res = new StringBuilder();
                    RootScript.runScriptAsRoot(Tools.this.cmd, Tools.this.res, 1000);
                } else {
                    Tools.this.createFile("DE_File_Audio");
                    Tools.this.createFile("DE_File_Audio2");
                    Tools.this.createFile("DE_File_GSM");
                    Tools.this.createFile("DE_File_Seem_Flex_Tables");
                    Tools.this.createFile("DE_generic_pds_init");
                    Tools.this.cmd = "mount -o remount,rw /dev/mtd/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/DE_File_Audio /system/etc/motorola/bp_nvm_default/File_Audio\ncp -f /data/data/com.itfunz.itfunzsupertools/files/DE_File_Audio2 /system/etc/motorola/bp_nvm_default/File_Audio2\ncp -f /data/data/com.itfunz.itfunzsupertools/files/DE_File_GSM /system/etc/motorola/bp_nvm_default/File_GSM\ncp -f /data/data/com.itfunz.itfunzsupertools/files/DE_File_Seem_Flex_Tables /system/etc/motorola/bp_nvm_default/File_Seem_Flex_Tables\ncp -f /data/data/com.itfunz.itfunzsupertools/files/DE_generic_pds_init /system/etc/motorola/bp_nvm_default/generic_pds_init\nmount -o remount,ro /dev/mtd/mtdblock6 /system\n";
                    Tools.this.res = new StringBuilder();
                    RootScript.runScriptAsRoot(Tools.this.cmd, Tools.this.res, 1000);
                }
                Tools.this.onCreateDialog(2).show();
                return true;
            }
        });
        if (!ItfunzCheckSum.isIsFroyo()) {
            getPreferenceScreen().findPreference("Jit").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference arg0) {
                    Tools.this.onCreateDialog(1).show();
                    return true;
                }
            });
        }
        getPreferenceScreen().findPreference("Power").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference arg0) {
                Tools.this.startActivity(new Intent(Tools.this, ItfunzPowerManager.class));
                return true;
            }
        });
        getPreferenceScreen().findPreference("screenLock").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (Boolean.valueOf(newValue.toString()).booleanValue()) {
                    ItfunzSuperToolsService.mKeyguardLock.reenableKeyguard();
                    return true;
                }
                ItfunzSuperToolsService.mKeyguardLock.disableKeyguard();
                return true;
            }
        });
        getPreferenceScreen().findPreference("screen_sleep").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (newValue.toString().equals("电源键唤醒")) {
                    Tools.this.createPowerKeyFile();
                    RootScript.runScriptAsRoot("mount -o remount,rw /dev/mtd/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/key1.kl /system/usr/keylayout/qwerty.kl\ncp -f /data/data/com.itfunz.itfunzsupertools/files/key2.kl /system/usr/keylayout/sholesp0b-keypad.kl\ncp -f /data/data/com.itfunz.itfunzsupertools/files/key3.kl /system/usr/keylayout/sholesp1a-keypad.kl\ncp -f /data/data/com.itfunz.itfunzsupertools/files/key4.kl /system/usr/keylayout/sholesp1b-keypad.kl\ncp -f /data/data/com.itfunz.itfunzsupertools/files/key5.kl /system/usr/keylayout/sholesp2a-keypad.kl\n", Tools.this.res, 1000);
                } else {
                    Tools.this.createCameraKeyFile();
                    RootScript.runScriptAsRoot("mount -o remount,rw /dev/mtd/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/key1_.kl /system/usr/keylayout/qwerty.kl\ncp -f /data/data/com.itfunz.itfunzsupertools/files/key2_.kl /system/usr/keylayout/sholesp0b-keypad.kl\ncp -f /data/data/com.itfunz.itfunzsupertools/files/key3_.kl /system/usr/keylayout/sholesp1a-keypad.kl\ncp -f /data/data/com.itfunz.itfunzsupertools/files/key4_.kl /system/usr/keylayout/sholesp1b-keypad.kl\ncp -f /data/data/com.itfunz.itfunzsupertools/files/key5_.kl /system/usr/keylayout/sholesp2a-keypad.kl\n", Tools.this.res, 1000);
                }
                Tools.this.onCreateDialog(3).show();
                return true;
            }
        });
        getPreferenceScreen().findPreference("autopower").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent();
                intent.setClass(Tools.this, AutoPowerDown.class);
                Tools.this.startActivity(intent);
                return true;
            }
        });
        getPreferenceScreen().findPreference("screen_light").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Tools.this.onCreateDialog(4).show();
                return true;
            }
        });
        getPreferenceScreen().findPreference("led_light").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean ledenable = Boolean.valueOf(newValue.toString()).booleanValue();
                if (ItfunzCheckSum.isIsFroyo()) {
                    if (ledenable) {
                        FileService.SetSystemDefault("/sys/devices/platform/ld-button-backlight/leds/button-backlight/brightness", "255");
                        return true;
                    }
                    FileService.SetSystemConfig("/sys/devices/platform/ld-button-backlight/leds/button-backlight/brightness", "0");
                    return true;
                } else if (ledenable) {
                    FileService.SetSystemDefault("/sys/devices/platform/button-backlight/leds/button-backlight/brightness", "255");
                    return true;
                } else {
                    FileService.SetSystemConfig("/sys/devices/platform/button-backlight/leds/button-backlight/brightness", "0");
                    return true;
                }
            }
        });
        getPreferenceScreen().findPreference("led_miss_call").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Class<CallledService> cls = CallledService.class;
                if (newValue.toString().equals("关闭")) {
                    Class<CallledService> cls2 = CallledService.class;
                    Tools.this.stopService(new Intent(Tools.this, cls));
                    return true;
                }
                Class<CallledService> cls3 = CallledService.class;
                Tools.this.startService(new Intent(Tools.this, cls));
                return true;
            }
        });
        getPreferenceScreen().findPreference("about_program").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Intent intentApp = new Intent("android.intent.action.MAIN");
                intentApp.setComponent(new ComponentName("com.itfunz.itfunzsupertools", "com.itfunz.itfunzsupertools.simple.AboutProgram"));
                Tools.this.startActivity(intentApp);
                return true;
            }
        });
        getPreferenceScreen().findPreference("themeManager").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Tools.this.startActivity(new Intent(Tools.this, ThemeManager.class));
                return true;
            }
        });
        getPreferenceScreen().findPreference("fileManager").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Tools.this.startActivity(new Intent(Tools.this, ItfunzFileDialog.class));
                return true;
            }
        });
        getPreferenceScreen().findPreference("ad_filter_system").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (Boolean.valueOf(newValue.toString()).booleanValue()) {
                    new replaceHosts().execute(new Object[0]);
                    Toast.makeText(Tools.this, "广告过滤系统已开启。", 0).show();
                    new AlertDialog.Builder(Tools.this).setTitle("使用须知").setMessage("ITFUNZ广告过滤系统是Martincz打造的一套移动终端广告过滤系统。通过本系统配合超级工具箱可以实时过滤移动终端的推送广告，减少流量紧张用户因广告而产生的不必要的流量损失。\n但我们同时提倡大家在流量充裕的情况下不要进行过滤，尽可能通过点击广告支持开发者。\nITFUNZ移动平台广告过滤系统需要不断的丰富数据库才能为广大玩家提供最优质的服务，希望大家一起能参与广告链接的收集和提交工作，使我们的广告过滤更全面更有效。\n提交地址:http://ad.itfunz.com").setPositiveButton("我知道了", (DialogInterface.OnClickListener) null).create().show();
                    return true;
                }
                Tools.this.createFile("hosts");
                RootScript.runRootCommand("mount -o remount,rw /dev/mtd/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/hosts /etc/hosts\nchmod 777 /etc/hosts\n");
                Toast.makeText(Tools.this, "广告过滤系统已关闭，重启后生效。", 0).show();
                Tools.this.settings.edit().putInt("adFilterCount", 0).commit();
                return true;
            }
        });
        getPreferenceScreen().findPreference("google_service_control").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                if (Boolean.valueOf(newValue.toString()).booleanValue()) {
                    Tools.this.createFile("hosts-hack");
                    RootScript.runRootCommand("mount -o remount,rw /dev/mtd/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/hosts-hack /etc/hosts\nchmod 777 /etc/hosts\n");
                    Toast.makeText(Tools.this, "突破封锁成功，重启后生效。", 1).show();
                } else {
                    Tools.this.createFile("hosts");
                    RootScript.runRootCommand("mount -o remount,rw /dev/mtd/mtdblock6 /system\ncp -f /data/data/com.itfunz.itfunzsupertools/files/hosts /etc/hosts\nchmod 777 /etc/hosts\n");
                    Toast.makeText(Tools.this, "已关闭，重启后生效。", 1).show();
                }
                return true;
            }
        });
        getPreferenceScreen().findPreference("OneKeyScreenLock").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Tools.this.startActivity(new Intent(Tools.this, OneKeyLockScreen.class));
                return true;
            }
        });
        getPreferenceScreen().findPreference("guide_program").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Intent intentApp = new Intent("android.intent.action.MAIN");
                intentApp.setComponent(new ComponentName("com.itfunz.itfunzsupertools", "com.itfunz.itfunzsupertools.simple.GuideProgram"));
                Tools.this.startActivity(intentApp);
                return true;
            }
        });
        getPreferenceScreen().findPreference("OtherFunction_Config").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Tools.this.startActivity(new Intent(Tools.this, OtherFunction.class));
                return true;
            }
        });
        getPreferenceScreen().findPreference("simplify_program").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference arg0) {
                Tools.this.startActivity(new Intent(Tools.this, SimplifyProgram.class));
                return true;
            }
        });
        getPreferenceScreen().findPreference("dataMvApp").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(Tools.this).setTitle("程序搬家").setMessage("单击继续进入程序搬家界面，程序需要一些时间检索您手机中的所有程序信息，请耐心等待。").setPositiveButton("继续", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Tools.this.startActivity(new Intent(Tools.this, DataMvApp.class));
                    }
                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create().show();
                return true;
            }
        });
        getPreferenceScreen().findPreference("custom_cpu").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference arg0) {
                Tools.this.buildCustomCPUDialog().show();
                return true;
            }
        });
        if (!ItfunzCheckSum.isIsFroyo()) {
            getPreferenceScreen().findPreference("control_cpu").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    if (Tools.this.getExistsOtherTools()) {
                        new AlertDialog.Builder(Tools.this).setTitle("友情提示").setMessage("我们检测到目前PlusToolbox正在管理超频功能。点击“继续”并进行相应设置后，将把超频功能控制权移交给ITFUNZ超级工具箱。单击“取消”，将维持PlusToolbox的控制权不变并取消您的当前操作。").setPositiveButton("继续", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Tools.this.startActivity(new Intent(Tools.this, CpuControl.class));
                            }
                        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create().show();
                        return true;
                    }
                    Tools.this.startActivity(new Intent(Tools.this, CpuControl.class));
                    return true;
                }
            });
        }
        getPreferenceScreen().findPreference("tool_settings").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference arg0) {
                Tools.this.startActivity(new Intent(Tools.this, ItfunzSuperToolsSettings.class));
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public void MoveAndSetFile(String vsel, String rate, String version) {
        if (version.equals("overclock")) {
            WriteFile("mot_boot_mode", "#!/system/bin/sh\nexport PATH=/system/bin:$PATH\nmot_boot_mode.bin\nif [ -f /system/lib/modules/ext2.ko ]\nthen\ninsmod /system/lib/modules/ext2.ko\nmount -t ext2 /dev/block/mmcblk0p2 /system/sdcard2\nfi\n#custom script\n/system/bin/sh script.sh\n");
            RootScript.runRootCommand("echo insmod /data/data/com.itfunz.itfunzsupertools/files/overclock.ko omap2_clk_init_cpufreq_table_addr=0x$(busybox grep omap2_clk_init_cpufreq_table /proc/kallsyms|busybox sed '2,$d s/ .*//g') >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode\necho 'echo " + vsel + " > /proc/overclock/max_vsel' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode\necho 'echo " + rate + " > /proc/overclock/max_rate' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
            return;
        }
        WriteFile("mot_boot_mode", "#!/system/bin/sh\nexport PATH=/system/bin:$PATH\nmot_boot_mode.bin\nif [ -f /system/lib/modules/ext2.ko ]\nthen\ninsmod /system/lib/modules/ext2.ko\nmount -t ext2 /dev/block/mmcblk0p2 /system/sdcard2\nfi\n#custom script\n/system/bin/sh script.sh\n");
        RootScript.runRootCommand("echo insmod /data/data/com.itfunz.itfunzsupertools/files/overclock12.ko omap2_clk_init_cpufreq_table_addr=0xc004e4b0 >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode\necho 'echo " + vsel + " > /proc/overclock/max_vsel' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode\necho 'echo " + rate + " > /proc/overclock/max_rate' >> /data/data/com.itfunz.itfunzsupertools/files/mot_boot_mode");
    }

    private void WriteFile(String path, String value) {
        try {
            OutputStream os = openFileOutput(path, 2);
            OutputStreamWriter osw = new OutputStreamWriter(os);
            osw.write(value);
            osw.close();
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void safeMode() {
        if (!new File("/system/bin/mot_boot_mode.bin").exists()) {
            RootScript.runScriptAsRoot("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\ncp -f /system/bin/mot_boot_mode /system/bin/mot_boot_mode.bin", this.res, 1000);
        }
    }

    public void createFile(String fileName) {
        try {
            InputStream is = getAssets().open(fileName);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            FileOutputStream fos = openFileOutput(fileName, 2);
            fos.write(buffer);
            fos.close();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createPowerKeyFile() {
        if (!new File("/data/data/com.itfunz.itfunzsupertools/files/key1.kl").exists()) {
            createFile("key1.kl");
            createFile("key2.kl");
            createFile("key3.kl");
            createFile("key4.kl");
            createFile("key5.kl");
        }
    }

    public void createCameraKeyFile() {
        if (!new File("/data/data/com.itfunz.itfunzsupertools/files/key1_.kl").exists()) {
            createFile("key1_.kl");
            createFile("key2_.kl");
            createFile("key3_.kl");
            createFile("key4_.kl");
            createFile("key5_.kl");
        }
    }

    private PreferenceScreen createPreferenceHierarchy() {
        PreferenceScreen psLayout = getPreferenceManager().createPreferenceScreen(this);
        PreferenceCategory preferenceCategory = new PreferenceCategory(this);
        preferenceCategory.setTitle(R.string.title_cpu);
        psLayout.addPreference(preferenceCategory);
        PreferenceScreen psCPUstate = getPreferenceManager().createPreferenceScreen(this);
        psCPUstate.setTitle(R.string.cpu_state);
        psCPUstate.setKey("cpuState");
        psCPUstate.setSummary(R.string.cpu_state_summary);
        psLayout.addPreference(psCPUstate);
        ListPreference listPreference = new ListPreference(this);
        listPreference.setEntries(R.array.cpuMHZ);
        listPreference.setEntryValues(R.array.cpuMHZ);
        listPreference.setTitle(R.string.cpu_MHZ);
        listPreference.setDialogTitle(R.string.cpu_MHZ);
        listPreference.setKey("cpuMHZ");
        listPreference.setSummary(R.string.cpu_MHZ_summary);
        listPreference.setDefaultValue("550");
        psLayout.addPreference(listPreference);
        PreferenceScreen psCustomCPU = getPreferenceManager().createPreferenceScreen(this);
        psCustomCPU.setTitle(R.string.cpu_custom);
        psCustomCPU.setKey("custom_cpu");
        psCustomCPU.setSummary(R.string.cpu_custom_summary);
        psLayout.addPreference(psCustomCPU);
        if (!ItfunzCheckSum.isIsFroyo()) {
            PreferenceScreen psCPUControl = getPreferenceManager().createPreferenceScreen(this);
            psCPUControl.setTitle(R.string.cpu_control);
            psCPUControl.setKey("control_cpu");
            psCPUControl.setSummary(R.string.cpu_control_summary);
            psLayout.addPreference(psCPUControl);
        }
        PreferenceCategory preferenceCategory2 = new PreferenceCategory(this);
        preferenceCategory2.setTitle(R.string.title_theme);
        psLayout.addPreference(preferenceCategory2);
        PreferenceScreen psTheme = getPreferenceManager().createPreferenceScreen(this);
        psTheme.setTitle(R.string.theme_manager);
        psTheme.setKey("themeManager");
        psTheme.setSummary(R.string.theme_manager_summary);
        psLayout.addPreference(psTheme);
        PreferenceCategory preferenceCategory3 = new PreferenceCategory(this);
        preferenceCategory3.setTitle(R.string.title_File);
        psLayout.addPreference(preferenceCategory3);
        PreferenceScreen psFile = getPreferenceManager().createPreferenceScreen(this);
        psFile.setTitle(R.string.file_manager);
        psFile.setKey("fileManager");
        psFile.setSummary(R.string.file_manager_summary);
        psLayout.addPreference(psFile);
        PreferenceCategory preferenceCategory4 = new PreferenceCategory(this);
        preferenceCategory4.setTitle(R.string.title_BaseBand);
        psLayout.addPreference(preferenceCategory4);
        ListPreference listPreference2 = new ListPreference(this);
        listPreference2.setEntries(R.array.BaseBand);
        listPreference2.setEntryValues(R.array.BaseBand);
        listPreference2.setTitle(R.string.BaseBandChange);
        listPreference2.setDialogTitle(R.string.BaseBandChange);
        listPreference2.setKey("BaseBand");
        listPreference2.setSummary(R.string.BaseBandChangesummary);
        listPreference2.setDefaultValue("欧版港版基带");
        psLayout.addPreference(listPreference2);
        if (!ItfunzCheckSum.isIsFroyo()) {
            PreferenceCategory preferenceCategory5 = new PreferenceCategory(this);
            preferenceCategory5.setTitle(R.string.title_Jit);
            psLayout.addPreference(preferenceCategory5);
            PreferenceScreen psJit = getPreferenceManager().createPreferenceScreen(this);
            psJit.setTitle(R.string.jit);
            psJit.setKey("Jit");
            psJit.setSummary(R.string.jit_summary);
            psLayout.addPreference(psJit);
        }
        PreferenceCategory preferenceCategory6 = new PreferenceCategory(this);
        preferenceCategory6.setTitle(R.string.title_Call);
        psLayout.addPreference(preferenceCategory6);
        CheckBoxPreference checkBoxPreference = new CheckBoxPreference(this);
        checkBoxPreference.setKey("Call");
        checkBoxPreference.setTitle(R.string.CallVibrate);
        checkBoxPreference.setSummaryOn(R.string.call_on);
        checkBoxPreference.setSummaryOff(R.string.call_off);
        checkBoxPreference.setDefaultValue(null);
        psLayout.addPreference(checkBoxPreference);
        PreferenceCategory preferenceCategory7 = new PreferenceCategory(this);
        preferenceCategory7.setTitle(R.string.title_power);
        psLayout.addPreference(preferenceCategory7);
        PreferenceScreen psOneKeyScreenLock = getPreferenceManager().createPreferenceScreen(this);
        psOneKeyScreenLock.setTitle(R.string.app_OneKeyLockScreen);
        psOneKeyScreenLock.setKey("OneKeyScreenLock");
        psOneKeyScreenLock.setSummary(R.string.onekeyscreenlock_summary);
        psLayout.addPreference(psOneKeyScreenLock);
        PreferenceScreen psPowerManager = getPreferenceManager().createPreferenceScreen(this);
        psPowerManager.setTitle(R.string.powerManager);
        psPowerManager.setKey("Power");
        psPowerManager.setSummary(R.string.power_summary);
        psLayout.addPreference(psPowerManager);
        PreferenceScreen psAutoPowerdown = getPreferenceManager().createPreferenceScreen(this);
        psAutoPowerdown.setTitle(R.string.auto_PowerDown);
        psAutoPowerdown.setKey("autopower");
        psAutoPowerdown.setSummary(R.string.auto_PowerDown_summary);
        psLayout.addPreference(psAutoPowerdown);
        PreferenceScreen psScreenLight = getPreferenceManager().createPreferenceScreen(this);
        psScreenLight.setTitle(R.string.screen_light);
        psScreenLight.setKey("screen_light");
        psScreenLight.setSummary(R.string.screen_light_summary);
        psLayout.addPreference(psScreenLight);
        CheckBoxPreference checkBoxPreference2 = new CheckBoxPreference(this);
        checkBoxPreference2.setTitle(R.string.screen_led);
        checkBoxPreference2.setKey("led_light");
        checkBoxPreference2.setSummaryOn(R.string.screen_led_on);
        checkBoxPreference2.setSummaryOff(R.string.screen_led_off);
        checkBoxPreference2.setDefaultValue(1);
        psLayout.addPreference(checkBoxPreference2);
        ListPreference listPreference3 = new ListPreference(this);
        listPreference3.setEntries(R.array.LedColor);
        listPreference3.setEntryValues(R.array.LedColor);
        listPreference3.setTitle(R.string.led_miss_call);
        listPreference3.setDialogTitle(R.string.led_miss_call);
        listPreference3.setKey("led_miss_call");
        listPreference3.setSummary(R.string.led_miss_call_summary);
        listPreference3.setDefaultValue("关闭");
        psLayout.addPreference(listPreference3);
        PreferenceCategory preferenceCategory8 = new PreferenceCategory(this);
        preferenceCategory8.setTitle(R.string.title_Screen);
        psLayout.addPreference(preferenceCategory8);
        CheckBoxPreference checkBoxPreference3 = new CheckBoxPreference(this);
        checkBoxPreference3.setKey("screenLock");
        checkBoxPreference3.setTitle(R.string.screen_lock);
        checkBoxPreference3.setSummaryOn(R.string.screen_lock_on);
        checkBoxPreference3.setSummaryOff(R.string.screen_lock_off);
        checkBoxPreference3.setDefaultValue(1);
        psLayout.addPreference(checkBoxPreference3);
        ListPreference listPreference4 = new ListPreference(this);
        listPreference4.setEntries(R.array.ScreenKey);
        listPreference4.setEntryValues(R.array.ScreenKey);
        listPreference4.setTitle(R.string.screen_sleep);
        listPreference4.setDialogTitle(R.string.screen_sleep);
        listPreference4.setKey("screen_sleep");
        listPreference4.setSummary(R.string.screen_sleep_summary);
        listPreference4.setDefaultValue("电源键唤醒");
        psLayout.addPreference(listPreference4);
        PreferenceCategory preferenceCategory9 = new PreferenceCategory(this);
        preferenceCategory9.setTitle(R.string.title_web);
        psLayout.addPreference(preferenceCategory9);
        CheckBoxPreference checkBoxPreference4 = new CheckBoxPreference(this);
        checkBoxPreference4.setTitle(R.string.ad_filter_system);
        checkBoxPreference4.setKey("ad_filter_system");
        checkBoxPreference4.setDefaultValue(null);
        checkBoxPreference4.setSummary(R.string.ad_filter_system_summary);
        psLayout.addPreference(checkBoxPreference4);
        CheckBoxPreference checkBoxPreference5 = new CheckBoxPreference(this);
        checkBoxPreference5.setTitle(R.string.google_service_control);
        checkBoxPreference5.setKey("google_service_control");
        checkBoxPreference5.setSummaryOn(R.string.google_service_control_on);
        checkBoxPreference5.setSummaryOff(R.string.google_service_control_off);
        checkBoxPreference5.setDefaultValue(null);
        psLayout.addPreference(checkBoxPreference5);
        PreferenceCategory preferenceCategory10 = new PreferenceCategory(this);
        preferenceCategory10.setTitle(R.string.title_Other);
        psLayout.addPreference(preferenceCategory10);
        PreferenceScreen psOtherConfig = getPreferenceManager().createPreferenceScreen(this);
        psOtherConfig.setTitle(R.string.Other_Config);
        psOtherConfig.setKey("OtherFunction_Config");
        psOtherConfig.setSummary(R.string.Other_Config_summary);
        psLayout.addPreference(psOtherConfig);
        PreferenceCategory preferenceCategory11 = new PreferenceCategory(this);
        preferenceCategory11.setTitle(R.string.title_Program);
        psLayout.addPreference(preferenceCategory11);
        PreferenceScreen psProgramSimplify = getPreferenceManager().createPreferenceScreen(this);
        psProgramSimplify.setTitle(R.string.Simplify_Program);
        psProgramSimplify.setKey("simplify_program");
        psProgramSimplify.setSummary(R.string.Simplify_Program_summary);
        psLayout.addPreference(psProgramSimplify);
        PreferenceScreen psDataMvApp = getPreferenceManager().createPreferenceScreen(this);
        psDataMvApp.setTitle(R.string.dataMvApp);
        psDataMvApp.setKey("dataMvApp");
        psDataMvApp.setSummary(R.string.dataMvApp_summary);
        psLayout.addPreference(psDataMvApp);
        PreferenceScreen psToolSettings = getPreferenceManager().createPreferenceScreen(this);
        psToolSettings.setTitle((int) R.string.tools_settings);
        psToolSettings.setKey("tool_settings");
        psToolSettings.setSummary((int) R.string.tools_settings_summary);
        psLayout.addPreference(psToolSettings);
        PreferenceScreen psProgramGuide = getPreferenceManager().createPreferenceScreen(this);
        psProgramGuide.setTitle(R.string.guide_program);
        psProgramGuide.setKey("guide_program");
        psProgramGuide.setSummary(R.string.guide_program_summary);
        psLayout.addPreference(psProgramGuide);
        PreferenceScreen psAboutProgram = getPreferenceManager().createPreferenceScreen(this);
        psAboutProgram.setTitle(R.string.about_program);
        psAboutProgram.setKey("about_program");
        psAboutProgram.setSummary(R.string.about_program_summary);
        psLayout.addPreference(psAboutProgram);
        return psLayout;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return bulidDialogJit(this);
            case 2:
                return bulidDialogBaseband(this);
            case 3:
                return bulidDialogScreenSleepKey(this);
            case 4:
                return buildDialogScreenLight(this);
            default:
                return null;
        }
    }

    private Dialog bulidDialogJit(Context context) {
        final String[] jitFileBin = {"dalvikvm", "dexopt", "logcat"};
        final String[] jitFileLib = {"libcutils.so", "libdl.so", "libdvm.so", "liblog.so", "libm.so", "libnativehelper.so", "libz.so"};
        final String checksum = ItfunzCheckSum.getCheckSum("/system/bin/dalvikvm");
        return new AlertDialog.Builder(this).setTitle("JIT加速").setPositiveButton("启用", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (!checksum.equals("e93c")) {
                    Toast.makeText(Tools.this, "您已经启用了JIT，请勿重复启用。", 1).show();
                    return;
                }
                RootScript.runRootCommand("mount -o remount,rw /dev/mtd/mtdblock6 /system\n");
                for (int i = 0; i < jitFileBin.length; i++) {
                    Tools.this.createFile(jitFileBin[i]);
                    RootScript.runRootCommand("cp /system/bin/" + jitFileBin[i] + " /data/data/com.itfunz.itfunzsupertools/files/" + jitFileBin[i] + "_bak");
                    RootScript.runRootCommand("cp /data/data/com.itfunz.itfunzsupertools/files/" + jitFileBin[i] + " /system/bin/" + jitFileBin[i]);
                    RootScript.runRootCommand("chmod 777 /system/bin/" + jitFileBin[i]);
                }
                for (int i2 = 0; i2 < jitFileLib.length; i2++) {
                    Tools.this.createFile(jitFileLib[i2]);
                    RootScript.runRootCommand("cp /system/lib/" + jitFileLib[i2] + " /data/data/com.itfunz.itfunzsupertools/files/" + jitFileLib[i2] + "_bak");
                    RootScript.runRootCommand("cp /data/data/com.itfunz.itfunzsupertools/files/" + jitFileLib[i2] + " /system/lib/" + jitFileLib[i2]);
                    RootScript.runRootCommand("chmod 777 /system/lib/" + jitFileLib[i2]);
                }
                RootScript.runRootCommand("sync\nrm /data/dalvik-cache/*\nreboot");
            }
        }).setNegativeButton("禁用", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (checksum.equals("e93c")) {
                    Toast.makeText(Tools.this, "您当前没有启用JIT。", 1).show();
                    return;
                }
                RootScript.runRootCommand("mount -o remount,rw /dev/mtd/mtdblock6 /system\n");
                for (int i = 0; i < jitFileBin.length; i++) {
                    RootScript.runRootCommand("cp /data/data/com.itfunz.itfunzsupertools/files/" + jitFileBin[i] + "_bak /system/bin/" + jitFileBin[i]);
                    RootScript.runRootCommand("chmod 777 /system/bin/" + jitFileBin[i]);
                }
                for (int i2 = 0; i2 < jitFileLib.length; i2++) {
                    RootScript.runRootCommand("cp /data/data/com.itfunz.itfunzsupertools/files/" + jitFileLib[i2] + "_bak /system/lib/" + jitFileLib[i2]);
                    RootScript.runRootCommand("chmod 777 /system/lib/" + jitFileLib[i2]);
                }
                RootScript.runRootCommand("sync\nrm /data/dalvik-cache/*\nreboot");
            }
        }).setNeutralButton("状态检测", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (checksum.equals("e93c")) {
                    Toast.makeText(Tools.this, "您当前没有启用JIT。", 1).show();
                } else {
                    Toast.makeText(Tools.this, "您当前启用了JIT。", 1).show();
                }
            }
        }).setMessage("请选择您要执行的操作。\n启用JIT有时可能会导致系统运行不稳定，请谨慎使用。\n启用或禁用后会自动重启，请预先储存好尚未完成的工作。开启或禁用后首次启动速度较慢，请耐心等待。").create();
    }

    private Dialog bulidDialogBaseband(Context context) {
        return new AlertDialog.Builder(this).setTitle("基带修改").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RootScript.runScriptAsRoot("reboot", new StringBuilder(), 1000);
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).setMessage("更换成功，重新启动手机后生效，是否立即重启？").create();
    }

    private Dialog bulidDialogScreenSleepKey(Context context) {
        return new AlertDialog.Builder(this).setTitle("屏幕唤醒控制").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RootScript.runScriptAsRoot("reboot", new StringBuilder(), 1000);
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).setMessage("更换成功，重新启动手机后生效，是否立即重启？").create();
    }

    public boolean isNumeric(String str) {
        if (!Pattern.compile("[0-9]*").matcher(str).matches()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public Dialog buildCustomCPUDialog() {
        this.settings = PreferenceManager.getDefaultSharedPreferences(this);
        LinearLayout llayout = new LinearLayout(this);
        llayout.setOrientation(1);
        TextView tvRemark1 = new TextView(this);
        tvRemark1.setText("友情提示:\n在没有确定自己设置的参数安全前,请不要勾选开机自启选项,以免导致无法正常启动.");
        llayout.addView(tvRemark1);
        final EditText tvCpuRate = new EditText(this);
        TextView tvCpuRateRemark = new TextView(this);
        final EditText tvCpuVsel = new EditText(this);
        TextView tvCpuVselRemark = new TextView(this);
        tvCpuRateRemark.setText("请设定您要设置的频率(550000~1200000)");
        tvCpuVselRemark.setText("请设定您要设置的电压(32~80)");
        tvCpuRate.setText(this.settings.getString("CPU_Rate", "550000"));
        tvCpuVsel.setText(this.settings.getString("CPU_Vsel", "32"));
        llayout.addView(tvCpuRateRemark);
        llayout.addView(tvCpuRate);
        llayout.addView(tvCpuVselRemark);
        llayout.addView(tvCpuVsel);
        final CheckBox cbAutoStart = new CheckBox(this);
        cbAutoStart.setText("开机自启动(设置成功后对勾并不保持选定状态，以免下次设置时忘记取消而导致意外发生。)");
        llayout.addView(cbAutoStart);
        return new AlertDialog.Builder(this).setTitle("CPU超频专业版").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Tools.this.safeMode();
                final String Rate = tvCpuRate.getText().toString().trim();
                final String Vsel = tvCpuVsel.getText().toString().trim();
                if (Rate.equals("") && Vsel.equals("")) {
                    Toast.makeText(Tools.this, "电压或频率不能为空，请重新输入", 0).show();
                } else if (!Tools.this.isNumeric(Rate) || !Tools.this.isNumeric(Vsel)) {
                    Toast.makeText(Tools.this, "电压或频率必须为数字，请重新输入", 0).show();
                } else {
                    int RateNum = Integer.valueOf(Rate).intValue();
                    int VselNum = Integer.valueOf(Vsel).intValue();
                    if (RateNum < 550000 || RateNum > 1200000 || VselNum < 32 || VselNum > 80) {
                        Toast.makeText(Tools.this, "电压或频率不在有效范围之内，请重新输入", 0).show();
                    } else if (!cbAutoStart.isChecked()) {
                        RootScript.runRootCommand("insmod /data/data/com.itfunz.itfunzsupertools/files/overclock12.ko omap2_clk_init_cpufreq_table_addr=0x$(busybox grep omap2_clk_init_cpufreq_table /proc/kallsyms|busybox sed '2,$d s/ .*//g')\necho " + Vsel + " > /proc/overclock/max_vsel\necho " + Rate + " > /proc/overclock/max_rate");
                        Toast.makeText(Tools.this, "超频成功！", 0).show();
                        Tools.this.settings.edit().putString("CPU_Vsel", Vsel).putString("CPU_Rate", Rate).commit();
                    } else if (Tools.this.getExistsOtherTools()) {
                        new AlertDialog.Builder(Tools.this).setTitle("友情提示").setMessage("我们检测到目前PlusToolbox正在管理超频功能。点击“继续”，将把超频功能控制权移交给ITFUNZ超级工具箱。单击“取消”，将维持PlusToolbox的控制权不变并取消您的当前操作。").setPositiveButton("继续", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (ItfunzCheckSum.isIsFroyo()) {
                                    Tools.this.MoveAndSetFile(Vsel, Rate, "overclock12");
                                } else {
                                    Tools.this.MoveAndSetFile(Vsel, Rate, "overclock");
                                }
                                FileService.createRootCommand();
                                Toast.makeText(Tools.this, "超频成功，并已设为开机自启动！", 1).show();
                                Tools.this.settings.edit().putString("CPU_Vsel", Vsel).putString("CPU_Rate", Rate).commit();
                            }
                        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create().show();
                    } else {
                        if (ItfunzCheckSum.isIsFroyo()) {
                            Tools.this.MoveAndSetFile(Vsel, Rate, "overclock12");
                        } else {
                            Tools.this.MoveAndSetFile(Vsel, Rate, "overclock");
                        }
                        FileService.createRootCommand();
                        Toast.makeText(Tools.this, "超频成功，并已设为开机自启动！", 1).show();
                        Tools.this.settings.edit().putString("CPU_Vsel", Vsel).putString("CPU_Rate", Rate).commit();
                    }
                }
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).setView(llayout).create();
    }

    private Dialog buildDialogScreenLight(final Context context) {
        SharedPreferences light_setting = PreferenceManager.getDefaultSharedPreferences(context);
        int light_value = light_setting.getInt(Light_Value, 80);
        boolean light_lock = light_setting.getBoolean("Light_Lock", false);
        LinearLayout llayout = new LinearLayout(this);
        llayout.setOrientation(1);
        TextView tvRemark1 = new TextView(context);
        tvRemark1.setText("拖动滚动条以调整屏幕亮度");
        tvRemark1.setTextSize(18.0f);
        SeekBar sbLight = new SeekBar(context);
        sbLight.setMax(100);
        if (light_value == -1) {
            sbLight.setProgress(80);
        }
        sbLight.setProgress(light_value);
        sbLight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ContentResolver cr = Tools.this.getContentResolver();
                if (Settings.System.getInt(cr, "screen_brightness_mode", 1) == 1) {
                    Settings.System.putInt(cr, "screen_brightness_mode", 0);
                }
                if (progress < 2) {
                    progress = 2;
                }
                FileService.SetSystemDefault("/sys/class/leds/lcd-backlight/brightness", String.valueOf(progress));
                Tools.this.FinalStaticLight = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        final CheckBox cbSelect = new CheckBox(context);
        cbSelect.setText("如果您要长期设置此亮度，请在此打勾。这样可以防止系统自动修改，否则屏幕熄灭后会被系统修改为系统默认最低亮度。");
        cbSelect.setChecked(light_lock);
        llayout.addView(tvRemark1);
        llayout.addView(sbLight);
        llayout.addView(cbSelect);
        return new AlertDialog.Builder(this).setTitle("调整手机屏幕亮度").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Tools.this.settings = PreferenceManager.getDefaultSharedPreferences(context);
                Intent i = new Intent(Tools.this, HoldScreenLightService.class);
                if (cbSelect.isChecked()) {
                    Tools.this.settings.edit().putBoolean("Light_Lock", true).putInt(Tools.Light_Value, Tools.this.FinalStaticLight).commit();
                    FileService.SetSystemConfig("/sys/class/leds/lcd-backlight/brightness", String.valueOf(Tools.this.FinalStaticLight));
                    Tools.this.startService(i);
                    return;
                }
                Tools.this.settings.edit().putBoolean("Light_Lock", false).putInt(Tools.Light_Value, Tools.this.FinalStaticLight).commit();
                FileService.SetSystemDefault("/sys/class/leds/lcd-backlight/brightness", String.valueOf(Tools.this.FinalStaticLight));
                Tools.this.stopService(i);
            }
        }).setView(llayout).create();
    }

    private Dialog createSystemVersionDialog() {
        return new AlertDialog.Builder(this).setTitle("版本检测提示").setMessage("您可能在使用官方系统或其他第三方系统，为了获得更好的体验，建议您使用以下系统及其升级版本：\n" + getString(R.string.ROM_Version) + "。\n否则可能会出现兼容问题，如无法正常开机（可重刷ROM解决）。您也可以把正常使用本程序但未在上面列出的系统版本告诉我们，以便我们更新程序。亦可以将未能正常使用本程序的系统版本告诉我们，以便我们改进，谢谢！\n联系方式：www.itfunz.com\n电子邮件：martincz@itfunz.com").create();
    }

    public class checkNewVersionTask extends AsyncTask<Object, Object, Object> {
        public checkNewVersionTask() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(final Object result) {
            super.onPostExecute(result);
            if (!result.toString().equals("")) {
                Toast.makeText(Tools.this, result.toString(), 1).show();
                for (int i = 0; i < 3; i++) {
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            Toast.makeText(Tools.this, result.toString(), 1).show();
                        }
                    }, 3000);
                }
            }
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... params) {
            String result = "";
            Date dateNow = new Date();
            int date = dateNow.getDate();
            int month = dateNow.getMonth() + 1;
            int checkDate = Tools.this.settings.getInt("check_date", date);
            int checkMonth = Tools.this.settings.getInt("check_month", month);
            if (checkDate == date && checkMonth == month) {
                int checkCount = Tools.this.settings.getInt("check_count", 0);
                if (checkCount <= 5) {
                    try {
                        if (((ConnectivityManager) Tools.this.getSystemService("connectivity")).getActiveNetworkInfo().isAvailable()) {
                            String codeVersionString = ItfunzSuperToolsWebCheck.CheckNewVersionFromWeb("enrk9+/tqu)hpfrhx)bkm,on_`pes_pnhkn,mskdntlijn-tmi", "code");
                            if (!codeVersionString.equals("")) {
                                PackageInfo info = null;
                                try {
                                    info = Tools.this.getPackageManager().getPackageInfo(Tools.this.getPackageName(), 0);
                                } catch (PackageManager.NameNotFoundException e) {
                                    e.printStackTrace();
                                }
                                String CodeVersionCurrent = info.versionName;
                                String intNewVersion = ItfunzSuperToolsWebCheck.getCode(codeVersionString, CodeVersionCurrent);
                                if (!intNewVersion.equals("0")) {
                                    result = "本程序已更新,版本号：V" + intNewVersion + "\n您当前版本号：V" + CodeVersionCurrent + "\n更新内容：" + ItfunzSuperToolsWebCheck.CheckNewVersionFromWeb("enrk9+/tqu)hpfrhx)bkm,on_`pes_pnhkn,mskdntlijn-tmi", "content") + "\n您可以请前往www.itfunz.com获取最新版本，使用功能更丰富，更完善的新版工具箱。";
                                }
                                Tools.this.settings.edit().putInt("check_month", month).putInt("check_date", date).putInt("check_count", checkCount + 1).commit();
                            }
                        }
                    } catch (Exception e2) {
                        Log.i("UPDATE", e2.toString());
                    }
                }
            } else {
                Tools.this.settings.edit().putInt("check_month", month).putInt("check_date", date).putInt("check_count", 0).commit();
                new checkNewVersionTask().execute(new Object[0]);
            }
            return result;
        }
    }

    public boolean getExistsOtherTools() {
        File ToolsInData = new File("/data/data/com.fancy.plustoolbox");
        String Code = ItfunzCheckSum.getCheckSum("/system/bin/mot_boot_mode");
        File ToolsConfigInSystem = new File("/system/bin/boot_script/boot_overclock.sh");
        if (!ToolsInData.exists() || !Code.equals("295f") || !ToolsConfigInSystem.exists()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003c A[SYNTHETIC, Splitter:B:23:0x003c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void checkProcErrorMsg(java.io.InputStream r7) {
        /*
            r6 = this;
            java.lang.String r4 = "ROOT"
            r2 = 0
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0048 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0048 }
            r4.<init>(r7)     // Catch:{ IOException -> 0x0048 }
            r5 = 1024(0x400, float:1.435E-42)
            r3.<init>(r4, r5)     // Catch:{ IOException -> 0x0048 }
        L_0x000f:
            java.lang.String r1 = r3.readLine()     // Catch:{ IOException -> 0x0022, all -> 0x0045 }
            if (r1 != 0) goto L_0x001c
            if (r3 == 0) goto L_0x004b
            r3.close()     // Catch:{ IOException -> 0x0040 }
            r2 = r3
        L_0x001b:
            return
        L_0x001c:
            java.lang.String r4 = "ROOT"
            android.util.Log.d(r4, r1)     // Catch:{ IOException -> 0x0022, all -> 0x0045 }
            goto L_0x000f
        L_0x0022:
            r4 = move-exception
            r0 = r4
            r2 = r3
        L_0x0025:
            r0.printStackTrace()     // Catch:{ all -> 0x0039 }
            java.lang.String r4 = "ROOT"
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x0039 }
            android.util.Log.d(r4, r5)     // Catch:{ all -> 0x0039 }
            if (r2 == 0) goto L_0x001b
            r2.close()     // Catch:{ IOException -> 0x0037 }
            goto L_0x001b
        L_0x0037:
            r4 = move-exception
            goto L_0x001b
        L_0x0039:
            r4 = move-exception
        L_0x003a:
            if (r2 == 0) goto L_0x003f
            r2.close()     // Catch:{ IOException -> 0x0043 }
        L_0x003f:
            throw r4
        L_0x0040:
            r4 = move-exception
            r2 = r3
            goto L_0x001b
        L_0x0043:
            r5 = move-exception
            goto L_0x003f
        L_0x0045:
            r4 = move-exception
            r2 = r3
            goto L_0x003a
        L_0x0048:
            r4 = move-exception
            r0 = r4
            goto L_0x0025
        L_0x004b:
            r2 = r3
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.itfunz.itfunzsupertools.Tools.checkProcErrorMsg(java.io.InputStream):void");
    }

    public class rootyourPhone extends AsyncTask<Boolean, Void, Boolean> {
        public rootyourPhone() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Boolean... params) {
            File file = new File(Tools.this.getFilesDir(), "exploid");
            File file2 = new File(Tools.this.getFilesDir(), "install_kit.sh");
            File file3 = new File(Tools.this.getFilesDir(), "remount_data.sh");
            File file4 = new File(Tools.this.getFilesDir(), "remount_sys_ro.sh");
            File file5 = new File(Tools.this.getFilesDir(), "remount_sys_rw.sh");
            File file6 = new File(Tools.this.getFilesDir(), "su");
            File file7 = new File(Tools.this.getFilesDir(), "Superuser.apk");
            File file8 = new File(Tools.this.getFilesDir(), "tattoo_hack_g6561203.ko");
            File file9 = new File(Tools.this.getFilesDir(), "tattoo_hack_gf922713.ko");
            if (!file.exists()) {
                Tools.this.createFile("exploid");
            }
            if (!file2.exists()) {
                Tools.this.createFile("install_kit.sh");
            }
            if (!file3.exists()) {
                Tools.this.createFile("remount_data.sh");
            }
            if (!file4.exists()) {
                Tools.this.createFile("remount_sys_ro.sh");
            }
            if (!file5.exists()) {
                Tools.this.createFile("remount_sys_rw.sh");
            }
            if (!file6.exists()) {
                Tools.this.createFile("su");
            }
            if (!file7.exists()) {
                Tools.this.createFile("Superuser.apk");
            }
            if (!file8.exists()) {
                Tools.this.createFile("tattoo_hack_g6561203.ko");
            }
            if (!file9.exists()) {
                Tools.this.createFile("tattoo_hack_gf922713.ko");
            }
            if (!file.exists() || !file2.exists() || !file3.exists() || !file4.exists() || !file5.exists() || !file6.exists() || !file7.exists() || !file8.exists() || !file9.exists()) {
                new rootyourPhone().execute(new Boolean[0]);
                return false;
            }
            try {
                Process proc = Runtime.getRuntime().exec("chmod 770 " + file.getAbsolutePath());
                if (proc != null) {
                    Tools.this.checkProcErrorMsg(proc.getErrorStream());
                }
                Process proc2 = Runtime.getRuntime().exec(String.valueOf(file.getAbsolutePath()) + " /dev/block/mtdblock8 yaffs2");
                if (proc2 != null) {
                    Tools.this.checkProcErrorMsg(proc2.getInputStream());
                    Tools.this.checkProcErrorMsg(proc2.getErrorStream());
                }
                Thread.sleep(1000);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            if (result.booleanValue()) {
                WifiManager wifiManager = (WifiManager) Tools.this.getSystemService("wifi");
                int state = wifiManager.getWifiState();
                if (state == 3) {
                    wifiManager.setWifiEnabled(false);
                    Tools.this.bDisableWifi = false;
                } else if (state == 1) {
                    wifiManager.setWifiEnabled(true);
                    Tools.this.bDisableWifi = true;
                }
            }
            new installerTools().execute(new Void[0]);
        }
    }

    public class installerTools extends AsyncTask<Void, Void, Boolean> {
        public installerTools() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... params) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            File RootShell = new File("/data/local/tmp/rootshell");
            File InstallerTools = new File(Tools.this.getFilesDir(), "install_kit.sh");
            if (!RootShell.exists()) {
                new rootyourPhone().execute(new Boolean[0]);
            }
            if (!InstallerTools.exists()) {
                new rootyourPhone().execute(new Boolean[0]);
            }
            try {
                Runtime.getRuntime().exec("chmod 777 " + InstallerTools.getAbsolutePath());
                Runtime.getRuntime().exec(String.valueOf(RootShell.getAbsolutePath()) + " " + InstallerTools.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            WifiManager wifiManager = (WifiManager) Tools.this.getSystemService("wifi");
            if (Tools.this.bDisableWifi) {
                wifiManager.setWifiEnabled(false);
            } else {
                wifiManager.setWifiEnabled(true);
            }
            if (new File("/system/bin/su").exists()) {
                Toast.makeText(Tools.this, "Root成功！", 1).show();
                Tools.this.finish();
                return;
            }
            new rootyourPhone().execute(new Boolean[0]);
        }
    }

    public class replaceHosts extends AsyncTask<Object, Object, Object> {
        public replaceHosts() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            super.onPostExecute(result);
            int value = Integer.valueOf(result.toString()).intValue();
            if (value == 1) {
                Toast.makeText(Tools.this, "广告过滤数据同步成功，重启生效！", 0).show();
            } else if (value == 0) {
                Toast.makeText(Tools.this, "广告过滤数据同步失败，请检查网络后再试！", 0).show();
            }
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... arg0) {
            try {
                if (!((ConnectivityManager) Tools.this.getSystemService("connectivity")).getActiveNetworkInfo().isAvailable()) {
                    return 0;
                }
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Tools.this);
                int adFilterCount = settings.getInt("adFilterCount", 0);
                int newFilterCount = Integer.valueOf(ItfunzSuperToolsWebCheck.CheckNewVersionFromWeb("enrk9+/^^,dsbukt,^ni/_[q`.]dIcqoBkukn,slh", "LinksCount")).intValue();
                if (newFilterCount <= adFilterCount) {
                    return 2;
                }
                boolean result = ItfunzSuperToolsWebCheck.DownloadAdFilterHost();
                RootScript.runRootCommand("mount -o remount,rw /dev/mtd/mtdblock6 /system\nchmod 777 /tmp/hosts\ncat /tmp/hosts > /etc/hosts");
                if (!result) {
                    return 0;
                }
                settings.edit().putInt("adFilterCount", newFilterCount).commit();
                return 1;
            } catch (Exception e) {
                return 0;
            }
        }
    }
}
