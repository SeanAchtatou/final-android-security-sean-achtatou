package com.itfunz.itfunzsupertools;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ItfunzSuperToolsSettings extends PreferenceActivity {
    SharedPreferences appSettings;
    SharedPreferences settings;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ProgressDialog pdialog = ProgressDialog.show(this, "友情提示", "正在获取主题及控件配置信息中，请稍候...");
        final Handler handler = new Handler();
        new Thread() {
            public void run() {
                super.run();
                ItfunzSuperToolsSettings.this.setPreferenceScreen(ItfunzSuperToolsSettings.this.createPreferenceScreen());
                ItfunzSuperToolsSettings.this.settings = PreferenceManager.getDefaultSharedPreferences(ItfunzSuperToolsSettings.this);
                ItfunzSuperToolsSettings.this.appSettings = ItfunzSuperToolsSettings.this.getSharedPreferences("AppInfoPackage", 0);
                ItfunzSuperToolsSettings.this.getPreferenceManager().findPreference("tool_password").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        if (!ItfunzSuperToolsSettings.this.settings.getString("tool_pwd", "").equals("")) {
                            return true;
                        }
                        ItfunzSuperToolsSettings.this.createNewPasswordDialog().show();
                        return false;
                    }
                });
                ItfunzSuperToolsSettings.this.getPreferenceManager().findPreference("widget_settings_change_function_left_up").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference arg0, Object arg1) {
                        String ActivityName = String.valueOf(arg1);
                        ItfunzSuperToolsSettings.this.createProgramIcons(ItfunzSuperToolsSettings.this.appSettings.getString(ActivityName, ""), ActivityName, "/data/data/com.itfunz.itfunzsupertools/files/app_lf_up.png", "wdt_icons_lf_up").show();
                        Toast.makeText(ItfunzSuperToolsSettings.this, "更换成功！", 0).show();
                        return true;
                    }
                });
                ItfunzSuperToolsSettings.this.getPreferenceManager().findPreference("widget_settings_change_function_left_down").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference arg0, Object arg1) {
                        String ActivityName = String.valueOf(arg1);
                        ItfunzSuperToolsSettings.this.createProgramIcons(ItfunzSuperToolsSettings.this.appSettings.getString(ActivityName, ""), ActivityName, "/data/data/com.itfunz.itfunzsupertools/files/app_lf_down.png", "wdt_icons_lf_down").show();
                        Toast.makeText(ItfunzSuperToolsSettings.this, "更换成功！", 0).show();
                        return true;
                    }
                });
                ItfunzSuperToolsSettings.this.getPreferenceManager().findPreference("widget_settings_change_function_right_up").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference arg0, Object arg1) {
                        String ActivityName = String.valueOf(arg1);
                        ItfunzSuperToolsSettings.this.createProgramIcons(ItfunzSuperToolsSettings.this.appSettings.getString(ActivityName, ""), ActivityName, "/data/data/com.itfunz.itfunzsupertools/files/app_rh_up.png", "wdt_icons_rh_up").show();
                        Toast.makeText(ItfunzSuperToolsSettings.this, "更换成功！", 0).show();
                        return true;
                    }
                });
                ItfunzSuperToolsSettings.this.getPreferenceManager().findPreference("widget_settings_change_function_right_down").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference arg0, Object arg1) {
                        String ActivityName = String.valueOf(arg1);
                        ItfunzSuperToolsSettings.this.createProgramIcons(ItfunzSuperToolsSettings.this.appSettings.getString(ActivityName, ""), ActivityName, "/data/data/com.itfunz.itfunzsupertools/files/app_rh_down.png", "wdt_icons_rh_down").show();
                        Toast.makeText(ItfunzSuperToolsSettings.this, "更换成功！", 0).show();
                        return true;
                    }
                });
                ItfunzSuperToolsSettings.this.getPreferenceManager().findPreference("widget_settings_change_function_center").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference arg0, Object arg1) {
                        String ActivityName = String.valueOf(arg1);
                        ItfunzSuperToolsSettings.this.createProgramIcons(ItfunzSuperToolsSettings.this.appSettings.getString(ActivityName, ""), ActivityName, "/data/data/com.itfunz.itfunzsupertools/files/app_center.png", "wdt_icons_center").show();
                        Toast.makeText(ItfunzSuperToolsSettings.this, "更换成功！", 0).show();
                        return true;
                    }
                });
                ItfunzSuperToolsSettings.this.getPreferenceManager().findPreference("change_icons").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {
                        Intent intentApp = new Intent("android.intent.action.MAIN");
                        intentApp.setComponent(new ComponentName("com.itfunz.itfunzsupertools", "com.itfunz.itfunzsupertools.widget.ItfunzSuperToolsWidgetIconsDisplay"));
                        ItfunzSuperToolsSettings.this.startActivity(intentApp);
                        return false;
                    }
                });
                ItfunzSuperToolsSettings.this.getPreferenceManager().findPreference("change_theme").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference arg0, Object arg1) {
                        String Choose = arg1.toString().trim();
                        if (Choose.equals("默认主题")) {
                            ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_up", String.valueOf((int) R.drawable.wdt_app_power)).commit();
                            ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_up_push", String.valueOf((int) R.drawable.wdt_app_power_push)).commit();
                            ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_down", String.valueOf((int) R.drawable.wdt_app_call)).commit();
                            ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_down_push", String.valueOf((int) R.drawable.wdt_app_call_push)).commit();
                            ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_up", String.valueOf((int) R.drawable.wdt_app_explorer)).commit();
                            ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_up_push", String.valueOf((int) R.drawable.wdt_app_explorer_push)).commit();
                            ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_down", String.valueOf((int) R.drawable.wdt_app_mms)).commit();
                            ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_down_push", String.valueOf((int) R.drawable.wdt_app_mms_push)).commit();
                            ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_center", String.valueOf((int) R.drawable.wdt_app_lock)).commit();
                            ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_center_push", String.valueOf((int) R.drawable.wdt_app_lock_push)).commit();
                            Toast.makeText(ItfunzSuperToolsSettings.this, "更换成功！", 0).show();
                            ItfunzSuperToolsSettings.this.sendBroadcast(new Intent("itfunz.screen.refresh"));
                            return true;
                        } else if (Choose.equals("当前未挂载内存卡")) {
                            Toast.makeText(ItfunzSuperToolsSettings.this, "您当前未挂载内存卡，请在挂载后重试！", 0).show();
                            return false;
                        } else {
                            try {
                                File[] themeFilesSmall = ItfunzSuperToolsSettings.this.getThemeFiles("/sdcard/itfunzsupertools/widget_theme/" + Choose + "/229");
                                File[] themeFilesMiddle = ItfunzSuperToolsSettings.this.getThemeFiles("/sdcard/itfunzsupertools/widget_theme/" + Choose + "/342");
                                File[] themeFilesBig = ItfunzSuperToolsSettings.this.getThemeFiles("/sdcard/itfunzsupertools/widget_theme/" + Choose + "/422");
                                File[] themeFilesIcons = ItfunzSuperToolsSettings.this.getThemeFiles("/sdcard/itfunzsupertools/widget_theme/" + Choose + "/icons");
                                if (themeFilesSmall.length == 0 || themeFilesMiddle.length == 0 || themeFilesMiddle.length == 0) {
                                    Toast.makeText(ItfunzSuperToolsSettings.this, "主题文件可能遭到损坏、不完整或安装路径有误，无法使用，请重新安装！", 0).show();
                                    return false;
                                }
                                if (themeFilesIcons.length != 0) {
                                    ItfunzSuperToolsSettings.this.settings.edit().putBoolean("change_theme_icons", true).commit();
                                } else {
                                    ItfunzSuperToolsSettings.this.settings.edit().putBoolean("change_theme_icons", false).commit();
                                }
                                String[] themeFilesSmallName = {"wdt_layout_center.png", "wdt_layout_lf_down.png", "wdt_layout_lf_up.png", "wdt_layout_original.png", "wdt_layout_rh_down.png", "wdt_layout_rh_up.png"};
                                String[] themeFilesMiddleName = {"wdt_layout_center_mid_lf.png", "wdt_layout_center_mid_rh.png", "wdt_layout_lf_down_mid_lf.png", "wdt_layout_lf_down_mid_rh.png", "wdt_layout_lf_up_mid_lf.png", "wdt_layout_lf_up_mid_rh.png", "wdt_layout_original_mid_lf.png", "wdt_layout_original_mid_rh.png", "wdt_layout_rh_down_mid_lf.png", "wdt_layout_rh_down_mid_rh.png", "wdt_layout_rh_up_mid_lf.png", "wdt_layout_rh_up_mid_rh.png"};
                                String[] themeFilesBigName = {"wdt_layout_center_big_lf.png", "wdt_layout_center_big_rh.png", "wdt_layout_lf_down_big_lf.png", "wdt_layout_lf_down_big_rh.png", "wdt_layout_lf_up_big_lf.png", "wdt_layout_lf_up_big_rh.png", "wdt_layout_original_big_lf.png", "wdt_layout_original_big_rh.png", "wdt_layout_rh_down_big_lf.png", "wdt_layout_rh_down_big_rh.png", "wdt_layout_rh_up_big_lf.png", "wdt_layout_rh_up_big_rh.png"};
                                String[] themeFilesIconsName = {"wdt_app_call.png", "wdt_app_call_push.png", "wdt_app_explorer.png", "wdt_app_explorer_push.png", "wdt_app_lock.png", "wdt_app_lock_push.png", "wdt_app_mms.png", "wdt_app_mms_push.png", "wdt_app_power.png", "wdt_app_power_push.png"};
                                if (!ItfunzSuperToolsSettings.this.settings.getBoolean("change_theme_icons", false)) {
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_up", String.valueOf((int) R.drawable.wdt_app_power)).commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_up_push", String.valueOf((int) R.drawable.wdt_app_power_push)).commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_down", String.valueOf((int) R.drawable.wdt_app_call)).commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_down_push", String.valueOf((int) R.drawable.wdt_app_call_push)).commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_up", String.valueOf((int) R.drawable.wdt_app_explorer)).commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_up_push", String.valueOf((int) R.drawable.wdt_app_explorer_push)).commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_down", String.valueOf((int) R.drawable.wdt_app_mms)).commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_down_push", String.valueOf((int) R.drawable.wdt_app_mms_push)).commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_center", String.valueOf((int) R.drawable.wdt_app_lock)).commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_center_push", String.valueOf((int) R.drawable.wdt_app_lock_push)).commit();
                                } else if (!ItfunzSuperToolsSettings.this.getThemeFileState(themeFilesIcons, themeFilesIconsName)) {
                                    Toast.makeText(ItfunzSuperToolsSettings.this, "主题文件可能遭到损坏、不完整或安装路径有误，无法使用，请重新安装！", 0).show();
                                    return false;
                                } else {
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_up", "/sdcard/itfunzsupertools/widget_theme/" + Choose + "/icons/wdt_app_power.png").commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_up_push", "/sdcard/itfunzsupertools/widget_theme/" + Choose + "/icons/wdt_app_power_push.png").commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_down", "/sdcard/itfunzsupertools/widget_theme/" + Choose + "/icons/wdt_app_call.png").commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_lf_down_push", "/sdcard/itfunzsupertools/widget_theme/" + Choose + "/icons/wdt_app_call_push.png").commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_up", "/sdcard/itfunzsupertools/widget_theme/" + Choose + "/icons/wdt_app_explorer.png").commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_up_push", "/sdcard/itfunzsupertools/widget_theme/" + Choose + "/icons/wdt_app_explorer_push.png").commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_down", "/sdcard/itfunzsupertools/widget_theme/" + Choose + "/icons/wdt_app_mms.png").commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_rh_down_push", "/sdcard/itfunzsupertools/widget_theme/" + Choose + "/icons/wdt_app_mms_push.png").commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_center", "/sdcard/itfunzsupertools/widget_theme/" + Choose + "/icons/wdt_app_lock.png").commit();
                                    ItfunzSuperToolsSettings.this.settings.edit().putString("wdt_icons_center_push", "/sdcard/itfunzsupertools/widget_theme/" + Choose + "/icons/wdt_app_lock_push.png").commit();
                                }
                                boolean smallDir = ItfunzSuperToolsSettings.this.getThemeFileState(themeFilesSmall, themeFilesSmallName);
                                boolean middleDir = ItfunzSuperToolsSettings.this.getThemeFileState(themeFilesMiddle, themeFilesMiddleName);
                                boolean bigDir = ItfunzSuperToolsSettings.this.getThemeFileState(themeFilesBig, themeFilesBigName);
                                if (!smallDir || !middleDir || !bigDir) {
                                    Toast.makeText(ItfunzSuperToolsSettings.this, "主题文件可能遭到损坏、不完整或安装路径有误，无法使用，请重新安装！", 0).show();
                                    return false;
                                }
                                Toast.makeText(ItfunzSuperToolsSettings.this, "更换成功！", 0).show();
                                ItfunzSuperToolsSettings.this.sendBroadcast(new Intent("itfunz.screen.refresh"));
                                return true;
                            } catch (Exception e) {
                                Toast.makeText(ItfunzSuperToolsSettings.this, "主题文件可能遭到损坏、不完整或安装路径有误，无法使用，请重新安装！", 0).show();
                                return false;
                            }
                        }
                    }
                });
                Handler handler = handler;
                final ProgressDialog progressDialog = pdialog;
                handler.post(new Runnable() {
                    public void run() {
                        progressDialog.dismiss();
                    }
                });
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public boolean getThemeFileState(File[] files, String[] filesName) {
        int filesNameLength = filesName.length;
        int success = 0;
        if (fileLength < filesNameLength) {
            return false;
        }
        for (File name : files) {
            String fileThemeName = name.getName().toString();
            int index = 0;
            while (true) {
                if (index >= filesNameLength) {
                    break;
                } else if (fileThemeName.equals(filesName[index].toString())) {
                    success++;
                    break;
                } else {
                    index++;
                }
            }
        }
        if (success == filesNameLength) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public PreferenceScreen createPreferenceScreen() {
        CharSequence[] items;
        PreferenceScreen psLayout = getPreferenceManager().createPreferenceScreen(this);
        PreferenceCategory preferenceCategory = new PreferenceCategory(this);
        preferenceCategory.setTitle(R.string.title_tools_settings);
        psLayout.addPreference(preferenceCategory);
        CheckBoxPreference checkBoxPreference = new CheckBoxPreference(this);
        checkBoxPreference.setTitle(R.string.tool_password);
        checkBoxPreference.setKey("tool_password");
        checkBoxPreference.setSummaryOn(R.string.tool_password_on);
        checkBoxPreference.setSummaryOff(R.string.tool_password_off);
        checkBoxPreference.setDefaultValue(null);
        psLayout.addPreference(checkBoxPreference);
        PreferenceCategory preferenceCategory2 = new PreferenceCategory(this);
        preferenceCategory2.setTitle(R.string.title_widget_theme);
        psLayout.addPreference(preferenceCategory2);
        if (Environment.getExternalStorageState().equals("mounted")) {
            File[] themeFiles = getThemeFiles("/sdcard/itfunzsupertools/widget_theme");
            getThemeFiles("/sdcard/itfunzsupertools/.nomedia");
            int count = themeFiles.length;
            ArrayList<String> list = new ArrayList<>();
            list.add("默认主题");
            for (int i = 0; i < count; i++) {
                if (!themeFiles[i].getName().toString().equals(".nomedia")) {
                    list.add(themeFiles[i].getName());
                }
            }
            items = (String[]) list.toArray(new String[list.size()]);
        } else {
            items = new String[]{"当前未挂载内存卡"};
        }
        PreferenceScreen psWidgetIcons = getPreferenceManager().createPreferenceScreen(this);
        psWidgetIcons.setTitle(R.string.widget_settings_change_icons);
        psWidgetIcons.setKey("change_icons");
        psWidgetIcons.setSummary(R.string.widget_settings_change_icons_summary);
        psLayout.addPreference(psWidgetIcons);
        ListPreference listPreference = new ListPreference(this);
        listPreference.setTitle(R.string.widget_settings_change_theme);
        listPreference.setKey("change_theme");
        listPreference.setSummary(R.string.widget_settings_change_theme_summary);
        listPreference.setDialogTitle("选择一个已安装的控件主题");
        listPreference.setEntries(items);
        listPreference.setEntryValues(items);
        listPreference.setDefaultValue("默认主题");
        psLayout.addPreference(listPreference);
        PreferenceCategory preferenceCategory3 = new PreferenceCategory(this);
        preferenceCategory3.setTitle(R.string.title_widget_function);
        psLayout.addPreference(preferenceCategory3);
        PackageManager pm = getPackageManager();
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> packages = pm.queryIntentActivities(intent, 0);
        ArrayList<String> appString = new ArrayList<>();
        ArrayList<String> appStringName = new ArrayList<>();
        int packageCount = packages.size();
        SharedPreferences AppInfoPackage = getSharedPreferences("AppInfoPackage", 0);
        for (int i2 = 0; i2 < packageCount; i2++) {
            ResolveInfo info = packages.get(i2);
            String appLabelName = info.activityInfo.loadLabel(pm).toString();
            String appName = info.activityInfo.name;
            String appNamePackage = info.activityInfo.packageName;
            appString.add(appLabelName);
            appStringName.add(appName);
            AppInfoPackage.edit().putString(appName, appNamePackage).commit();
        }
        String[] strArr = new String[appString.size()];
        String[] strArr2 = new String[appStringName.size()];
        CharSequence[] appitems = (String[]) appString.toArray(strArr);
        CharSequence[] appitemsname = (String[]) appStringName.toArray(strArr2);
        ListPreference listPreference2 = new ListPreference(this);
        listPreference2.setTitle(R.string.widget_settings_change_function_left_up);
        listPreference2.setKey("widget_settings_change_function_left_up");
        listPreference2.setSummary(R.string.widget_settings_change_function_summary);
        listPreference2.setDialogTitle("选择一个功能应用于设置区域");
        listPreference2.setEntries(appitems);
        listPreference2.setEntryValues(appitemsname);
        listPreference2.setDefaultValue("com.itfunz.itfunzsupertools.ItfunzPowerManager");
        psLayout.addPreference(listPreference2);
        ListPreference listPreference3 = new ListPreference(this);
        listPreference3.setTitle(R.string.widget_settings_change_function_left_down);
        listPreference3.setKey("widget_settings_change_function_left_down");
        listPreference3.setSummary(R.string.widget_settings_change_function_summary);
        listPreference3.setDialogTitle("选择一个功能应用于设置区域");
        listPreference3.setEntries(appitems);
        listPreference3.setEntryValues(appitemsname);
        listPreference3.setDefaultValue("com.android.contacts.DialtactsActivity");
        psLayout.addPreference(listPreference3);
        ListPreference listPreference4 = new ListPreference(this);
        listPreference4.setTitle(R.string.widget_settings_change_function_right_up);
        listPreference4.setKey("widget_settings_change_function_right_up");
        listPreference4.setSummary(R.string.widget_settings_change_function_summary);
        listPreference4.setDialogTitle("选择一个功能应用于设置区域");
        listPreference4.setEntries(appitems);
        listPreference4.setEntryValues(appitemsname);
        listPreference4.setDefaultValue("com.itfunz.itfunzsupertools.ItfunzFileDialog");
        psLayout.addPreference(listPreference4);
        ListPreference listPreference5 = new ListPreference(this);
        listPreference5.setTitle(R.string.widget_settings_change_function_right_down);
        listPreference5.setKey("widget_settings_change_function_right_down");
        listPreference5.setSummary(R.string.widget_settings_change_function_summary);
        listPreference5.setDialogTitle("选择一个功能应用于设置区域");
        listPreference5.setEntries(appitems);
        listPreference5.setEntryValues(appitemsname);
        listPreference5.setDefaultValue("com.android.mms.ui.ConversationList");
        psLayout.addPreference(listPreference5);
        ListPreference listPreference6 = new ListPreference(this);
        listPreference6.setTitle(R.string.widget_settings_change_function_center);
        listPreference6.setKey("widget_settings_change_function_center");
        listPreference6.setSummary(R.string.widget_settings_change_function_summary);
        listPreference6.setDialogTitle("选择一个功能应用于设置区域");
        listPreference6.setEntries(appitems);
        listPreference6.setEntryValues(appitemsname);
        listPreference6.setDefaultValue("com.itfunz.itfunzsupertools.OneKeyLockScreen");
        psLayout.addPreference(listPreference6);
        return psLayout;
    }

    /* access modifiers changed from: private */
    public Dialog createNewPasswordDialog() {
        LinearLayout llayout = new LinearLayout(this);
        llayout.setOrientation(1);
        TextView tvMessage = new TextView(this);
        tvMessage.setText("请您为超级工具箱创建一个六位数的保护密码，此密码用于防止亲朋好友在使用您的手机时误修改超级工具箱设置而造成不可挽回的损失。");
        llayout.addView(tvMessage);
        final EditText etPassWord = new EditText(this);
        llayout.addView(etPassWord);
        TextView tvReMessage = new TextView(this);
        tvReMessage.setText("请再次输入密码");
        llayout.addView(tvReMessage);
        final EditText rePassWord = new EditText(this);
        llayout.addView(rePassWord);
        return new AlertDialog.Builder(this).setTitle("创建新密码").setView(llayout).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String password = etPassWord.getText().toString().trim().toLowerCase();
                String repassword = rePassWord.getText().toString().trim().toLowerCase();
                if (password.length() != 6 || repassword.length() != 6) {
                    Toast.makeText(ItfunzSuperToolsSettings.this, "密码长度不正确，请重新输入！", 0).show();
                } else if (password.equals(repassword)) {
                    ItfunzSuperToolsSettings.this.settings.edit().putString("tool_pwd", password).commit();
                    ItfunzSuperToolsSettings.this.settings.edit().putBoolean("tool_password", true).commit();
                    Toast.makeText(ItfunzSuperToolsSettings.this, "设置成功,重新启动工具箱生效！", 0).show();
                    ItfunzSuperToolsSettings.this.onCreate(null);
                } else {
                    Toast.makeText(ItfunzSuperToolsSettings.this, "两次输入的密码不一致，请重新输入！", 0).show();
                }
            }
        }).setNeutralButton("取消", (DialogInterface.OnClickListener) null).create();
    }

    /* access modifiers changed from: private */
    public File[] getThemeFiles(String path) {
        File fileSettingDir = new File(path);
        if (!fileSettingDir.exists()) {
            fileSettingDir.mkdirs();
        }
        return fileSettingDir.listFiles();
    }

    public Dialog createProgramIcons(String packName, String ActivityName, String path, String postion) {
        final String str = packName;
        final String str2 = ActivityName;
        final String str3 = path;
        final String str4 = postion;
        return new AlertDialog.Builder(this).setTitle("更换控件图标").setMessage("请选择您想要显示在魔幻控件上的图标样式:").setPositiveButton("维持现状", (DialogInterface.OnClickListener) null).setNegativeButton("程序默认", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                IOException e;
                FileNotFoundException e2;
                PackageManager.NameNotFoundException e3;
                try {
                    Bitmap bm = ItfunzSuperToolsSettings.drawableToBitmap(ItfunzSuperToolsSettings.zoomDrawable(ItfunzSuperToolsSettings.this.getPackageManager().getActivityIcon(new ComponentName(str, str2)), 120, 120));
                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(str3)));
                    try {
                        bm.compress(Bitmap.CompressFormat.PNG, 100, bos);
                        bos.flush();
                        bos.close();
                    } catch (PackageManager.NameNotFoundException e4) {
                        e3 = e4;
                        e3.printStackTrace();
                        RootScript.runRootCommand("chmod 777 " + str3);
                        ItfunzSuperToolsSettings.this.settings.edit().putString(str4, str3).commit();
                    } catch (FileNotFoundException e5) {
                        e2 = e5;
                        e2.printStackTrace();
                        RootScript.runRootCommand("chmod 777 " + str3);
                        ItfunzSuperToolsSettings.this.settings.edit().putString(str4, str3).commit();
                    } catch (IOException e6) {
                        e = e6;
                        e.printStackTrace();
                        RootScript.runRootCommand("chmod 777 " + str3);
                        ItfunzSuperToolsSettings.this.settings.edit().putString(str4, str3).commit();
                    }
                } catch (PackageManager.NameNotFoundException e7) {
                    e3 = e7;
                    e3.printStackTrace();
                    RootScript.runRootCommand("chmod 777 " + str3);
                    ItfunzSuperToolsSettings.this.settings.edit().putString(str4, str3).commit();
                } catch (FileNotFoundException e8) {
                    e2 = e8;
                    e2.printStackTrace();
                    RootScript.runRootCommand("chmod 777 " + str3);
                    ItfunzSuperToolsSettings.this.settings.edit().putString(str4, str3).commit();
                } catch (IOException e9) {
                    e = e9;
                    e.printStackTrace();
                    RootScript.runRootCommand("chmod 777 " + str3);
                    ItfunzSuperToolsSettings.this.settings.edit().putString(str4, str3).commit();
                }
                RootScript.runRootCommand("chmod 777 " + str3);
                ItfunzSuperToolsSettings.this.settings.edit().putString(str4, str3).commit();
            }
        }).setNeutralButton("图标库", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intentApp = new Intent("android.intent.action.MAIN");
                intentApp.setComponent(new ComponentName("com.itfunz.itfunzsupertools", "com.itfunz.itfunzsupertools.widget.ItfunzSuperToolsWidgetIconsDisplay"));
                ItfunzSuperToolsSettings.this.startActivity(intentApp);
            }
        }).create();
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Drawable zoomDrawable(Drawable drawable, int w, int h) {
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        Bitmap oldbmp = drawableToBitmap(drawable);
        Matrix matrix = new Matrix();
        matrix.postScale(((float) w) / ((float) width), ((float) h) / ((float) height));
        return new BitmapDrawable(Bitmap.createBitmap(oldbmp, 0, 0, width, height, matrix, true));
    }
}
