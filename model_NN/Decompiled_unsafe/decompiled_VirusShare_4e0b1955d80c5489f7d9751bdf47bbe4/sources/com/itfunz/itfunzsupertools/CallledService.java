package com.itfunz.itfunzsupertools;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.CallLog;
import android.util.Log;

public class CallledService extends Service {
    private static final int ID_NOTIFICATION = 1;
    SharedPreferences settings;

    public void onCreate() {
        super.onCreate();
        this.settings = PreferenceManager.getDefaultSharedPreferences(this);
        getContentResolver().registerContentObserver(CallLog.Calls.CONTENT_URI, false, new MissedCallContentObserver(this, new Handler()));
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public class MissedCallContentObserver extends ContentObserver {
        private static final String TAG = "MissedCallContentObserver";
        private Context ctx;

        public MissedCallContentObserver(Context context, Handler handler) {
            super(handler);
            this.ctx = context;
        }

        public void onChange(boolean selfChange) {
            ContentResolver contentResolver = this.ctx.getContentResolver();
            Uri uri = CallLog.Calls.CONTENT_URI;
            String[] strArr = new String[3];
            strArr[0] = "number";
            strArr[CallledService.ID_NOTIFICATION] = "type";
            strArr[2] = "new";
            Cursor csr = contentResolver.query(uri, strArr, null, null, "date DESC");
            if (csr != null) {
                if (csr.moveToFirst()) {
                    switch (csr.getInt(csr.getColumnIndex("type"))) {
                        case CallledService.ID_NOTIFICATION /*1*/:
                            Log.v(TAG, "incoming type");
                            break;
                        case 2:
                            Log.v(TAG, "outgoing type");
                            break;
                        case 3:
                            Log.v(TAG, "missed type");
                            if (csr.getInt(csr.getColumnIndex("new")) == CallledService.ID_NOTIFICATION) {
                                Log.v(TAG, "you have a missed call");
                                String ColorString = CallledService.this.settings.getString("led_miss_call", "关闭");
                                int ColorARGB = 0;
                                if (ColorString.equals("蓝色")) {
                                    ColorARGB = -16776961;
                                }
                                if (ColorString.equals("绿色")) {
                                    ColorARGB = -16711936;
                                }
                                if (ColorString.equals("红色")) {
                                    ColorARGB = -65536;
                                }
                                if (ColorString.equals("黄色")) {
                                    ColorARGB = -256;
                                }
                                if (ColorString.equals("橙色")) {
                                    ColorARGB = -40704;
                                }
                                Notification notification = new Notification();
                                notification.defaults |= 4;
                                notification.ledARGB = ColorARGB;
                                notification.ledOnMS = 300;
                                notification.ledOffMS = 1000;
                                notification.flags |= CallledService.ID_NOTIFICATION;
                                ((NotificationManager) CallledService.this.getSystemService("notification")).notify(CallledService.ID_NOTIFICATION, notification);
                                break;
                            }
                            break;
                    }
                }
                csr.close();
            }
        }

        public boolean deliverSelfNotifications() {
            return super.deliverSelfNotifications();
        }
    }
}
