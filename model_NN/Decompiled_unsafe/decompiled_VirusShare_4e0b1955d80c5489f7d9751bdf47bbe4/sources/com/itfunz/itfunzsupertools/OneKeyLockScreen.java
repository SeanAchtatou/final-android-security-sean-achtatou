package com.itfunz.itfunzsupertools;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.IOException;

public class OneKeyLockScreen extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        if (settings.getBoolean("the_first", true)) {
            try {
                Runtime.getRuntime().exec("su");
            } catch (IOException e) {
                e.printStackTrace();
            }
            settings.edit().putBoolean("the_first", false).commit();
        }
        RootScript.runRootCommand("sendevent /dev/input/event2 1 107 1\nsendevent /dev/input/event2 1 107 0\n");
        finish();
    }
}
