package com.itfunz.itfunzsupertools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ItfunzSuperToolsBroadcast extends BroadcastReceiver {
    public static final String Light_Lock = "Light_Lock";

    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, ItfunzSuperToolsService.class);
        i.setFlags(268435456);
        context.startService(i);
    }
}
