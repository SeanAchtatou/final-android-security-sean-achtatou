package com.itfunz.itfunzsupertools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class ItfunzPowerManager extends Activity {
    private ImageButton ibBootloader;
    private ImageButton ibPowerOff;
    private ImageButton ibReboot;
    private ImageButton ibRecovery;
    Timer timer = new Timer(true);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.powermanager);
        this.ibBootloader = (ImageButton) findViewById(R.id.bootloader);
        this.ibRecovery = (ImageButton) findViewById(R.id.recovery);
        this.ibReboot = (ImageButton) findViewById(R.id.reboot);
        this.ibPowerOff = (ImageButton) findViewById(R.id.poweroff);
        if (!new File("/system/bin/su").exists()) {
            createCheckRoot().show();
        }
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        if (settings.getBoolean("the_first", true)) {
            try {
                Runtime.getRuntime().exec("su");
            } catch (IOException e) {
                e.printStackTrace();
            }
            settings.edit().putBoolean("the_first", false).commit();
        }
        this.ibReboot.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ItfunzPowerManager.this.createWaiting(R.string.wait_remark2, ItfunzPowerManager.this.getString(R.string.title_reboot)).show();
                ItfunzPowerManager.this.timerStart("reboot");
            }
        });
        this.ibPowerOff.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ItfunzPowerManager.this.createWaiting(R.string.wait_remark2, ItfunzPowerManager.this.getString(R.string.title_shutdown)).show();
                ItfunzPowerManager.this.timerStart("reboot -p");
            }
        });
        this.ibRecovery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String pwd_password = settings.getString("tool_pwd", "");
                if (settings.getBoolean("tool_password", false)) {
                    LinearLayout llayout = new LinearLayout(ItfunzPowerManager.this);
                    llayout.setOrientation(1);
                    TextView tvMessage = new TextView(ItfunzPowerManager.this);
                    tvMessage.setText("您开启了工具箱密码保护功能，请输入六位保护码:");
                    llayout.addView(tvMessage);
                    final EditText etPassWord = new EditText(ItfunzPowerManager.this);
                    llayout.addView(etPassWord);
                    AlertDialog.Builder positiveButton = new AlertDialog.Builder(ItfunzPowerManager.this).setTitle("密码保护").setView(llayout).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (!etPassWord.getText().toString().trim().toLowerCase().equals(pwd_password)) {
                                Toast.makeText(ItfunzPowerManager.this, "密码错误，请重新输入！", 0).show();
                                return;
                            }
                            ItfunzPowerManager.this.createWaiting(R.string.wait_remark1, ItfunzPowerManager.this.getString(R.string.title_recovery)).show();
                            ItfunzPowerManager.this.timerStart("reboot recovery");
                        }
                    });
                    final SharedPreferences sharedPreferences = settings;
                    positiveButton.setNeutralButton("修改密码", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            LinearLayout ellayout = new LinearLayout(ItfunzPowerManager.this);
                            ellayout.setOrientation(1);
                            TextView tvPassword = new TextView(ItfunzPowerManager.this);
                            tvPassword.setText("请输入旧密码");
                            ellayout.addView(tvPassword);
                            final EditText etoldPassWord = new EditText(ItfunzPowerManager.this);
                            ellayout.addView(etoldPassWord);
                            TextView tvNewPassword = new TextView(ItfunzPowerManager.this);
                            tvNewPassword.setText("请输入新密码");
                            ellayout.addView(tvNewPassword);
                            final EditText etNewPassWord = new EditText(ItfunzPowerManager.this);
                            ellayout.addView(etNewPassWord);
                            TextView tvRePassword = new TextView(ItfunzPowerManager.this);
                            tvRePassword.setText("请再次输入新密码");
                            ellayout.addView(tvRePassword);
                            final EditText etReNewPassWord = new EditText(ItfunzPowerManager.this);
                            ellayout.addView(etReNewPassWord);
                            AlertDialog.Builder view = new AlertDialog.Builder(ItfunzPowerManager.this).setTitle("修改密码").setView(ellayout);
                            final String str = pwd_password;
                            final SharedPreferences sharedPreferences = sharedPreferences;
                            view.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String oldpwd = etoldPassWord.getText().toString().trim().toLowerCase();
                                    String newPassword = etNewPassWord.getText().toString().trim().toLowerCase();
                                    String newRePassword = etReNewPassWord.getText().toString().trim().toLowerCase();
                                    if (oldpwd.equals(str)) {
                                        if (!(newPassword.length() == 6) || !(newRePassword.length() == 6)) {
                                            Toast.makeText(ItfunzPowerManager.this, "密码长度不正确，请重新输入！", 0).show();
                                        } else if (newPassword.equals(newRePassword)) {
                                            sharedPreferences.edit().putString("tool_pwd", newPassword).commit();
                                            Toast.makeText(ItfunzPowerManager.this, "设置成功！", 0).show();
                                        } else {
                                            Toast.makeText(ItfunzPowerManager.this, "两次输入的密码不一致，请重新输入！", 0).show();
                                        }
                                    } else {
                                        Toast.makeText(ItfunzPowerManager.this, "旧密码错误，请重新输入！", 0).show();
                                    }
                                }
                            }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).setOnKeyListener(new DialogInterface.OnKeyListener() {
                                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                                    if (keyCode != 4) {
                                        return false;
                                    }
                                    ItfunzPowerManager.this.finish();
                                    return false;
                                }
                            }).create().show();
                        }
                    }).setOnKeyListener(new DialogInterface.OnKeyListener() {
                        public boolean onKey(DialogInterface arg0, int arg1, KeyEvent arg2) {
                            if (arg1 != 4) {
                                return false;
                            }
                            ItfunzPowerManager.this.finish();
                            return false;
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ItfunzPowerManager.this.finish();
                        }
                    }).create().show();
                    return;
                }
                ItfunzPowerManager.this.createWaiting(R.string.wait_remark1, ItfunzPowerManager.this.getString(R.string.title_recovery)).show();
                ItfunzPowerManager.this.timerStart("reboot recovery");
            }
        });
        this.ibBootloader.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ItfunzPowerManager.this.createWaiting(R.string.wait_remark1, ItfunzPowerManager.this.getString(R.string.title_bootloader)).show();
                ItfunzPowerManager.this.timerStart("reboot bootloader");
            }
        });
    }

    private Dialog createCheckRoot() {
        return new AlertDialog.Builder(this).setTitle((int) R.string.check_permission).setMessage((int) R.string.check_permission_content).setPositiveButton((int) R.string.btncancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        }).create();
    }

    /* access modifiers changed from: private */
    public ProgressDialog createWaiting(int remark, String title) {
        return ProgressDialog.show(this, title, getString(remark), true);
    }

    public void timerStart(final String command) {
        this.timer.schedule(new TimerTask() {
            public void run() {
                try {
                    Thread.sleep(3000);
                    RootScript.runScriptAsRoot(command, new StringBuilder(), 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ItfunzPowerManager.this.timer.cancel();
                System.exit(0);
            }
        }, 1);
    }
}
