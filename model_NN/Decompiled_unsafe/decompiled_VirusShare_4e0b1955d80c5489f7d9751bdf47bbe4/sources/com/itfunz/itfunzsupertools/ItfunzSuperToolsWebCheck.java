package com.itfunz.itfunzsupertools;

import android.util.Log;
import com.itfunz.itfunzsupertools.command.ItfunzSecurity;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class ItfunzSuperToolsWebCheck {
    public static InputStream getOpenConn(String path) {
        HttpURLConnection.setFollowRedirects(true);
        try {
            HttpURLConnection uc = (HttpURLConnection) new URL(path).openConnection();
            uc.setRequestMethod("GET");
            uc.setConnectTimeout(10000);
            uc.setReadTimeout(10000);
            uc.setUseCaches(false);
            uc.addRequestProperty("Accept", "*/*");
            uc.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE)");
            uc.connect();
            if (uc.getResponseCode() == 200) {
                return uc.getInputStream();
            }
            return null;
        } catch (MalformedURLException e) {
            Log.i("ERROR", e.toString());
            return null;
        } catch (Exception e2) {
            Log.i("ERROR", e2.toString());
            return null;
        }
    }

    public static boolean DownloadAdFilterHost() {
        InputStream is = getOpenConn(ItfunzSecurity.toOriginalKey("enrk9+/^^,dsbukt,^ni/_[q`.]dIcqo-pxq"));
        if (is == null) {
            Log.i("ERROR", "Can't Connection");
            return false;
        }
        try {
            RandomAccessFile file = new RandomAccessFile("/tmp/hosts", "rw");
            file.seek(0);
            while (1 != 0) {
                byte[] buffer = new byte[1024];
                int read = is.read(buffer);
                if (read == -1) {
                    break;
                }
                file.write(buffer, 0, read);
            }
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return true;
    }

    public static String CheckNewVersionFromWeb(String url, String content) {
        InputStream is = getOpenConn(ItfunzSecurity.toOriginalKey(url));
        if (is == null) {
            Log.i("ERROR", "Can't Connection");
            return "";
        }
        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Log.i("ERROR", ex.toString());
        }
        Document document = null;
        try {
            document = builder.parse(is);
            if (is != null) {
                is.close();
            }
        } catch (SAXException e) {
            Log.i("ERROR", e.toString());
        } catch (IOException e2) {
            Log.i("ERROR", e2.toString());
        }
        return ((Element) document.getDocumentElement().getElementsByTagName(content).item(0)).getChildNodes().item(0).getNodeValue();
    }

    /* JADX INFO: Multiple debug info for r0v3 java.lang.String: [D('SecondStr1' java.lang.String), D('FirstIndex1' int)] */
    /* JADX INFO: Multiple debug info for r2v5 int: [D('ThirdIndex2' int), D('ThirdStr1' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v11 java.lang.String: [D('FirstIndex2' int), D('SecondStr2' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v1 java.lang.String: [D('code2' java.lang.String), D('ThirdStr2' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v3 java.lang.String: [D('resultStr2' java.lang.String), D('ThirdStr2' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v5 int: [D('resultStr2' java.lang.String), D('result2' int)] */
    public static String getCode(String code1, String code2) {
        int FirstIndex1 = code1.indexOf(".");
        String FirstStr1 = code1.substring(0, FirstIndex1);
        int ThirdIndex1 = code1.lastIndexOf(".");
        int result1 = Integer.valueOf(String.valueOf(FirstStr1) + code1.substring(FirstIndex1 + 1, ThirdIndex1) + code1.substring(ThirdIndex1 + 1, code1.length())).intValue();
        int FirstIndex2 = code2.indexOf(".");
        String FirstStr2 = code2.substring(0, FirstIndex2);
        int ThirdIndex2 = code2.lastIndexOf(".");
        return result1 > Integer.valueOf(new StringBuilder(String.valueOf(FirstStr2)).append(code2.substring(FirstIndex2 + 1, ThirdIndex2)).append(code2.substring(ThirdIndex2 + 1, code2.length())).toString()).intValue() ? code1 : "0";
    }
}
