package com.itfunz.itfunzsupertools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.preference.PreferenceManager;

public class CallReceiver extends BroadcastReceiver {
    private static final String ACTION_PHONE_STATE_CHANGED = "android.intent.action.PHONE_STATE";
    private static final String IDLE = "IDLE";
    private static final String OFFHOOK = "OFFHOOK";
    private static String pre_state = "";

    private boolean getVibratePreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("Call", false);
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_PHONE_STATE_CHANGED)) {
            String state = intent.getStringExtra("state");
            if (pre_state.equals(OFFHOOK) && state.equals(IDLE) && getVibratePreference(context)) {
                ((Vibrator) context.getSystemService("vibrator")).vibrate(200);
            }
            pre_state = state;
        }
    }
}
