package com.itfunz.itfunzsupertools.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.itfunz.itfunzsupertools.R;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

public class ItfunzSuperToolsWidgetIconsDisplay extends Activity {
    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-1, -1);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new ReadIcons().execute(new Object[0]);
    }

    public class ImageAdapter extends BaseAdapter {
        private int[] AppIconId;
        private Context context;
        private int count;
        private String[] filePath;

        public int getCount() {
            return this.count;
        }

        public ImageAdapter(Context C, int[] AppId, int Count, String[] FilePath) {
            this.context = C;
            this.AppIconId = AppId;
            this.count = Count;
            this.filePath = FilePath;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageview;
            if (convertView == null) {
                imageview = new ImageView(this.context);
                imageview.setLayoutParams(new AbsListView.LayoutParams(72, 72));
                imageview.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageview.setPadding(5, 5, 5, 5);
            } else {
                imageview = (ImageView) convertView;
            }
            if (position < this.AppIconId.length) {
                imageview.setImageResource(this.AppIconId[position]);
            } else {
                imageview.setImageBitmap(BitmapFactory.decodeFile(this.filePath[position]));
            }
            return imageview;
        }
    }

    public class ReadIcons extends AsyncTask<Object, Object, Object> {
        int Allcount;

        public ReadIcons() {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... params) {
            int customFileCount;
            int allFileCount;
            int[] AppIconId = {R.drawable.wdt_app_transparent, R.drawable.wdt_app_adobe, R.drawable.wdt_app_bluetooth, R.drawable.wdt_app_book, R.drawable.wdt_app_call, R.drawable.wdt_app_camera, R.drawable.wdt_app_chrome, R.drawable.wdt_app_clock, R.drawable.wdt_app_contact, R.drawable.wdt_app_cpu, R.drawable.wdt_app_email, R.drawable.wdt_app_explorer, R.drawable.wdt_app_game, R.drawable.wdt_app_google_earth, R.drawable.wdt_app_home, R.drawable.wdt_app_image, R.drawable.wdt_app_internet, R.drawable.wdt_app_lock, R.drawable.wdt_app_map, R.drawable.wdt_app_mms, R.drawable.wdt_app_office, R.drawable.wdt_app_power, R.drawable.wdt_app_rss, R.drawable.wdt_app_search, R.drawable.wdt_app_video, R.drawable.wdt_app_wifi};
            File file = new File("/sdcard/itfunzsupertools/widget_icons");
            if (!file.exists()) {
                file.mkdirs();
            }
            File[] files = file.listFiles(new FileFilter() {
                public boolean accept(File arg0) {
                    String FileName = arg0.getName();
                    if (FileName.substring(FileName.lastIndexOf(".") + 1, FileName.length()).toLowerCase().equals("png")) {
                        return true;
                    }
                    return false;
                }
            });
            File file2 = new File("/sdcard/itfunzsupertools/widget_theme/" + PreferenceManager.getDefaultSharedPreferences(ItfunzSuperToolsWidgetIconsDisplay.this).getString("change_theme", "默认主题") + "/custom");
            if (!file2.exists()) {
                file.mkdirs();
            }
            File[] customIconsFiles = file2.listFiles(new FileFilter() {
                public boolean accept(File arg0) {
                    String FileName = arg0.getName();
                    if (FileName.substring(FileName.lastIndexOf(".") + 1, FileName.length()).toLowerCase().equals("png")) {
                        return true;
                    }
                    return false;
                }
            });
            try {
                customFileCount = customIconsFiles.length;
            } catch (Exception e) {
                customFileCount = 0;
            }
            try {
                allFileCount = files.length;
            } catch (Exception e2) {
                allFileCount = 0;
            }
            ArrayList<String> list = new ArrayList<>();
            int applength = AppIconId.length;
            int sdAndResIcons = applength + customFileCount;
            int count = sdAndResIcons + allFileCount;
            this.Allcount = count;
            int indexSD = 0;
            int indexCustom = 0;
            for (int i = 0; i < count; i++) {
                if (i < applength) {
                    list.add("");
                } else if (i < sdAndResIcons) {
                    list.add(customIconsFiles[indexSD].getPath());
                    indexSD++;
                } else {
                    list.add(files[indexCustom].getPath());
                    indexCustom++;
                }
            }
            return (String[]) list.toArray(new String[list.size()]);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ItfunzSuperToolsWidgetIconsDisplay.this);
            final int[] AppIconId = {R.drawable.wdt_app_transparent, R.drawable.wdt_app_adobe, R.drawable.wdt_app_bluetooth, R.drawable.wdt_app_book, R.drawable.wdt_app_call, R.drawable.wdt_app_camera, R.drawable.wdt_app_chrome, R.drawable.wdt_app_clock, R.drawable.wdt_app_contact, R.drawable.wdt_app_cpu, R.drawable.wdt_app_email, R.drawable.wdt_app_explorer, R.drawable.wdt_app_game, R.drawable.wdt_app_google_earth, R.drawable.wdt_app_home, R.drawable.wdt_app_image, R.drawable.wdt_app_internet, R.drawable.wdt_app_lock, R.drawable.wdt_app_map, R.drawable.wdt_app_mms, R.drawable.wdt_app_office, R.drawable.wdt_app_power, R.drawable.wdt_app_rss, R.drawable.wdt_app_search, R.drawable.wdt_app_video, R.drawable.wdt_app_wifi};
            int[] AppIconIdPush = {R.drawable.wdt_app_transparent, R.drawable.wdt_app_adobe_push, R.drawable.wdt_app_bluetooth_push, R.drawable.wdt_app_book_push, R.drawable.wdt_app_call_push, R.drawable.wdt_app_camera_push, R.drawable.wdt_app_chrome_push, R.drawable.wdt_app_clock_push, R.drawable.wdt_app_contact_push, R.drawable.wdt_app_cpu_push, R.drawable.wdt_app_email_push, R.drawable.wdt_app_explorer_push, R.drawable.wdt_app_game_push, R.drawable.wdt_app_google_earth_push, R.drawable.wdt_app_home_push, R.drawable.wdt_app_image_push, R.drawable.wdt_app_internet_push, R.drawable.wdt_app_lock_push, R.drawable.wdt_app_map_push, R.drawable.wdt_app_mms_push, R.drawable.wdt_app_office_push, R.drawable.wdt_app_power_push, R.drawable.wdt_app_rss_push, R.drawable.wdt_app_search_push, R.drawable.wdt_app_video_push, R.drawable.wdt_app_wifi_push};
            final String[] fileFinalPath = (String[]) result;
            ItfunzSuperToolsWidgetIconsDisplay.this.setTitle("请选择一个图标应用于控件图标样式");
            LinearLayout llayout = new LinearLayout(ItfunzSuperToolsWidgetIconsDisplay.this);
            llayout.setOrientation(1);
            llayout.setLayoutParams(ItfunzSuperToolsWidgetIconsDisplay.this.lp);
            GridView gvLayout = new GridView(ItfunzSuperToolsWidgetIconsDisplay.this);
            gvLayout.setNumColumns(6);
            gvLayout.setAdapter((ListAdapter) new ImageAdapter(ItfunzSuperToolsWidgetIconsDisplay.this, AppIconId, this.Allcount, fileFinalPath));
            final int[] iArr = AppIconIdPush;
            gvLayout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int postion, long arg3) {
                    final String iconsPathStr;
                    final String iconsPathStr_push;
                    if (postion < AppIconId.length) {
                        iconsPathStr = String.valueOf(AppIconId[postion]);
                        iconsPathStr_push = String.valueOf(iArr[postion]);
                    } else {
                        iconsPathStr = fileFinalPath[postion].toString();
                        iconsPathStr_push = "";
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(ItfunzSuperToolsWidgetIconsDisplay.this);
                    final SharedPreferences sharedPreferences = settings;
                    builder.setItems((int) R.array.wdt_range, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    sharedPreferences.edit().putString("wdt_icons_lf_up", iconsPathStr).commit();
                                    sharedPreferences.edit().putString("wdt_icons_lf_up_push", iconsPathStr_push).commit();
                                    Toast.makeText(ItfunzSuperToolsWidgetIconsDisplay.this, "新图标设置已成功应用！", 0).show();
                                    return;
                                case 1:
                                    sharedPreferences.edit().putString("wdt_icons_lf_down", iconsPathStr).commit();
                                    sharedPreferences.edit().putString("wdt_icons_lf_down_push", iconsPathStr_push).commit();
                                    Toast.makeText(ItfunzSuperToolsWidgetIconsDisplay.this, "新图标设置已成功应用！", 0).show();
                                    return;
                                case 2:
                                    sharedPreferences.edit().putString("wdt_icons_rh_up", iconsPathStr).commit();
                                    sharedPreferences.edit().putString("wdt_icons_rh_up_push", iconsPathStr_push).commit();
                                    Toast.makeText(ItfunzSuperToolsWidgetIconsDisplay.this, "新图标设置已成功应用！", 0).show();
                                    return;
                                case 3:
                                    sharedPreferences.edit().putString("wdt_icons_rh_down", iconsPathStr).commit();
                                    sharedPreferences.edit().putString("wdt_icons_rh_down_push", iconsPathStr_push).commit();
                                    Toast.makeText(ItfunzSuperToolsWidgetIconsDisplay.this, "新图标设置已成功应用！", 0).show();
                                    return;
                                case 4:
                                    sharedPreferences.edit().putString("wdt_icons_center", iconsPathStr).commit();
                                    sharedPreferences.edit().putString("wdt_icons_center_push", iconsPathStr_push).commit();
                                    Toast.makeText(ItfunzSuperToolsWidgetIconsDisplay.this, "新图标设置已成功应用！", 0).show();
                                    return;
                                default:
                                    return;
                            }
                        }
                    }).setTitle("应用选择的图标到：").create().show();
                }
            });
            llayout.addView(gvLayout);
            ItfunzSuperToolsWidgetIconsDisplay.this.setContentView(llayout);
        }
    }
}
