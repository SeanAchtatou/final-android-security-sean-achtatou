package com.itfunz.itfunzsupertools.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;
import com.itfunz.itfunzsupertools.R;
import com.itfunz.itfunzsupertools.command.ItfunzCheckSum;

public class ItfunzSuperToolsWidgetServiceMid extends Service {
    final String app_Center_Button = "itfunz.app.center";
    final String app_Left_Button_Down = "itfunz.app.left_down";
    final String app_Left_Button_Up = "itfunz.app.left_up";
    final String app_Right_Button_Down = "itfunz.app.right_down";
    final String app_Right_Button_Up = "itfunz.app.right_up";

    public void onStart(Intent intent, int startId) {
        RemoteViews updateViews;
        RemoteViews updateViews2;
        super.onStart(intent, startId);
        SharedPreferences Settings = PreferenceManager.getDefaultSharedPreferences(this);
        String theme_name = Settings.getString("change_theme", "默认主题");
        boolean theme_icons = Settings.getBoolean("change_theme_icons", false);
        boolean sdCardMount = Environment.getExternalStorageState().equals("mounted");
        String wdt_icons_lf_up = Settings.getString("wdt_icons_lf_up", String.valueOf((int) R.drawable.wdt_app_power));
        String wdt_icons_lf_down = Settings.getString("wdt_icons_lf_down", String.valueOf((int) R.drawable.wdt_app_call));
        String wdt_icons_rh_up = Settings.getString("wdt_icons_rh_up", String.valueOf((int) R.drawable.wdt_app_explorer));
        String wdt_icons_rh_down = Settings.getString("wdt_icons_rh_down", String.valueOf((int) R.drawable.wdt_app_mms));
        String wdt_icons_center = Settings.getString("wdt_icons_center", String.valueOf((int) R.drawable.wdt_app_lock));
        if (theme_name.equals("默认主题")) {
            updateViews2 = new RemoteViews("com.itfunz.itfunzsupertools", R.layout.toolwidgetmid);
            if (ItfunzCheckSum.isNumeric(wdt_icons_lf_up)) {
                updateViews2.setImageViewResource(R.id.wdt_lf_up_app, Integer.valueOf(wdt_icons_lf_up).intValue());
            } else if (!sdCardMount) {
                updateViews2.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power);
            } else {
                updateViews2.setImageViewBitmap(R.id.wdt_lf_up_app, BitmapFactory.decodeFile(wdt_icons_lf_up));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_lf_down)) {
                updateViews2.setImageViewResource(R.id.wdt_lf_down_app, Integer.valueOf(wdt_icons_lf_down).intValue());
            } else if (!sdCardMount) {
                updateViews2.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call);
            } else {
                updateViews2.setImageViewBitmap(R.id.wdt_lf_down_app, BitmapFactory.decodeFile(wdt_icons_lf_down));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_rh_up)) {
                updateViews2.setImageViewResource(R.id.wdt_rh_up_app, Integer.valueOf(wdt_icons_rh_up).intValue());
            } else if (!sdCardMount) {
                updateViews2.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer);
            } else {
                updateViews2.setImageViewBitmap(R.id.wdt_rh_up_app, BitmapFactory.decodeFile(wdt_icons_rh_up));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_rh_down)) {
                updateViews2.setImageViewResource(R.id.wdt_rh_down_app, Integer.valueOf(wdt_icons_rh_down).intValue());
            } else if (!sdCardMount) {
                updateViews2.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms);
            } else {
                updateViews2.setImageViewBitmap(R.id.wdt_rh_down_app, BitmapFactory.decodeFile(wdt_icons_rh_down));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_center)) {
                updateViews2.setImageViewResource(R.id.wdt_center_app, Integer.valueOf(wdt_icons_center).intValue());
            } else if (!sdCardMount) {
                updateViews2.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock);
            } else {
                updateViews2.setImageViewBitmap(R.id.wdt_center_app, BitmapFactory.decodeFile(wdt_icons_center));
            }
            updateViews2.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_mid_lf);
            updateViews2.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_mid_rh);
        } else if (!sdCardMount) {
            try {
                updateViews = new RemoteViews("com.itfunz.itfunzsupertools", R.layout.toolwidgetmid);
            } catch (Exception e) {
                updateViews2 = new RemoteViews("com.itfunz.itfunzsupertools", R.layout.toolwidgetmid);
                updateViews2.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power);
                updateViews2.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call);
                updateViews2.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer);
                updateViews2.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms);
                updateViews2.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock);
                updateViews2.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_mid_lf);
                updateViews2.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_mid_rh);
                Intent intent2 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
                intent2.setAction("itfunz.app.left_up");
                updateViews2.setOnClickPendingIntent(R.id.wdt_lf_up_app, PendingIntent.getBroadcast(this, 0, intent2, 0));
                Intent intent3 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
                intent3.setAction("itfunz.app.right_up");
                updateViews2.setOnClickPendingIntent(R.id.wdt_rh_up_app, PendingIntent.getBroadcast(this, 0, intent3, 0));
                Intent intent4 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
                intent4.setAction("itfunz.app.left_down");
                updateViews2.setOnClickPendingIntent(R.id.wdt_lf_down_app, PendingIntent.getBroadcast(this, 0, intent4, 0));
                Intent intent5 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
                intent5.setAction("itfunz.app.right_down");
                updateViews2.setOnClickPendingIntent(R.id.wdt_rh_down_app, PendingIntent.getBroadcast(this, 0, intent5, 0));
                Intent intent6 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
                intent6.setAction("itfunz.app.center");
                updateViews2.setOnClickPendingIntent(R.id.wdt_center_app, PendingIntent.getBroadcast(this, 0, intent6, 0));
                AppWidgetManager.getInstance(this).updateAppWidget(new ComponentName(this, ItfunzSuperToolsWidgetMid.class), updateViews2);
                long now = System.currentTimeMillis();
                ((AlarmManager) getSystemService("alarm")).set(0, now + 1800000, PendingIntent.getService(this, 0, intent, 0));
                stopSelf();
            }
            try {
                if (ItfunzCheckSum.isNumeric(wdt_icons_lf_up)) {
                    updateViews.setImageViewResource(R.id.wdt_lf_up_app, Integer.valueOf(wdt_icons_lf_up).intValue());
                } else {
                    updateViews.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power);
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_rh_up)) {
                    updateViews.setImageViewResource(R.id.wdt_rh_up_app, Integer.valueOf(wdt_icons_rh_up).intValue());
                } else {
                    updateViews.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer);
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_lf_down)) {
                    updateViews.setImageViewResource(R.id.wdt_lf_down_app, Integer.valueOf(wdt_icons_lf_down).intValue());
                } else {
                    updateViews.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call);
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_rh_down)) {
                    updateViews.setImageViewResource(R.id.wdt_rh_down_app, Integer.valueOf(wdt_icons_rh_down).intValue());
                } else {
                    updateViews.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms);
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_center)) {
                    updateViews.setImageViewResource(R.id.wdt_center_app, Integer.valueOf(wdt_icons_center).intValue());
                } else {
                    updateViews.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock);
                }
                updateViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_mid_lf);
                updateViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_mid_rh);
                updateViews2 = updateViews;
            } catch (Exception e2) {
                updateViews2 = new RemoteViews("com.itfunz.itfunzsupertools", R.layout.toolwidgetmid);
                updateViews2.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power);
                updateViews2.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call);
                updateViews2.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer);
                updateViews2.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms);
                updateViews2.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock);
                updateViews2.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_mid_lf);
                updateViews2.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_mid_rh);
                Intent intent22 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
                intent22.setAction("itfunz.app.left_up");
                updateViews2.setOnClickPendingIntent(R.id.wdt_lf_up_app, PendingIntent.getBroadcast(this, 0, intent22, 0));
                Intent intent32 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
                intent32.setAction("itfunz.app.right_up");
                updateViews2.setOnClickPendingIntent(R.id.wdt_rh_up_app, PendingIntent.getBroadcast(this, 0, intent32, 0));
                Intent intent42 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
                intent42.setAction("itfunz.app.left_down");
                updateViews2.setOnClickPendingIntent(R.id.wdt_lf_down_app, PendingIntent.getBroadcast(this, 0, intent42, 0));
                Intent intent52 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
                intent52.setAction("itfunz.app.right_down");
                updateViews2.setOnClickPendingIntent(R.id.wdt_rh_down_app, PendingIntent.getBroadcast(this, 0, intent52, 0));
                Intent intent62 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
                intent62.setAction("itfunz.app.center");
                updateViews2.setOnClickPendingIntent(R.id.wdt_center_app, PendingIntent.getBroadcast(this, 0, intent62, 0));
                AppWidgetManager.getInstance(this).updateAppWidget(new ComponentName(this, ItfunzSuperToolsWidgetMid.class), updateViews2);
                long now2 = System.currentTimeMillis();
                ((AlarmManager) getSystemService("alarm")).set(0, now2 + 1800000, PendingIntent.getService(this, 0, intent, 0));
                stopSelf();
            }
        } else {
            updateViews = new RemoteViews("com.itfunz.itfunzsupertools", R.layout.toolwidgetmid);
            if (theme_icons) {
                new refreshUI_ICON().execute(updateViews);
                updateViews2 = updateViews;
            } else {
                new refreshUI().execute(updateViews);
                updateViews2 = updateViews;
            }
        }
        Intent intent222 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
        intent222.setAction("itfunz.app.left_up");
        updateViews2.setOnClickPendingIntent(R.id.wdt_lf_up_app, PendingIntent.getBroadcast(this, 0, intent222, 0));
        Intent intent322 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
        intent322.setAction("itfunz.app.right_up");
        updateViews2.setOnClickPendingIntent(R.id.wdt_rh_up_app, PendingIntent.getBroadcast(this, 0, intent322, 0));
        Intent intent422 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
        intent422.setAction("itfunz.app.left_down");
        updateViews2.setOnClickPendingIntent(R.id.wdt_lf_down_app, PendingIntent.getBroadcast(this, 0, intent422, 0));
        Intent intent522 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
        intent522.setAction("itfunz.app.right_down");
        updateViews2.setOnClickPendingIntent(R.id.wdt_rh_down_app, PendingIntent.getBroadcast(this, 0, intent522, 0));
        Intent intent622 = new Intent(this, ItfunzSuperToolsWidgetMid.class);
        intent622.setAction("itfunz.app.center");
        updateViews2.setOnClickPendingIntent(R.id.wdt_center_app, PendingIntent.getBroadcast(this, 0, intent622, 0));
        AppWidgetManager.getInstance(this).updateAppWidget(new ComponentName(this, ItfunzSuperToolsWidgetMid.class), updateViews2);
        long now22 = System.currentTimeMillis();
        ((AlarmManager) getSystemService("alarm")).set(0, now22 + 1800000, PendingIntent.getService(this, 0, intent, 0));
        stopSelf();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void updateWidget(Context context, RemoteViews updateViews) {
        AppWidgetManager.getInstance(context).updateAppWidget(new ComponentName(context, ItfunzSuperToolsWidgetMid.class), updateViews);
    }

    public class refreshUI extends AsyncTask<RemoteViews, Integer, String> {
        public refreshUI() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(RemoteViews... params) {
            SharedPreferences Settings = PreferenceManager.getDefaultSharedPreferences(ItfunzSuperToolsWidgetServiceMid.this);
            Integer LayoutId = Integer.valueOf((int) R.layout.toolwidgetmid);
            String themePath = "/sdcard/itfunzsupertools/widget_theme/" + Settings.getString("change_theme", "默认主题") + "/342/";
            RemoteViews remoteViews = new RemoteViews("com.itfunz.itfunzsupertools", LayoutId.intValue());
            boolean sdCardMount = Environment.getExternalStorageState().equals("mounted");
            String wdt_icons_lf_up = Settings.getString("wdt_icons_lf_up", String.valueOf((int) R.drawable.wdt_app_power));
            String wdt_icons_lf_down = Settings.getString("wdt_icons_lf_down", String.valueOf((int) R.drawable.wdt_app_call));
            String wdt_icons_rh_up = Settings.getString("wdt_icons_rh_up", String.valueOf((int) R.drawable.wdt_app_explorer));
            String wdt_icons_rh_down = Settings.getString("wdt_icons_rh_down", String.valueOf((int) R.drawable.wdt_app_mms));
            String wdt_icons_center = Settings.getString("wdt_icons_center", String.valueOf((int) R.drawable.wdt_app_lock));
            if (ItfunzCheckSum.isNumeric(wdt_icons_lf_up)) {
                remoteViews.setImageViewResource(R.id.wdt_lf_up_app, Integer.valueOf(wdt_icons_lf_up).intValue());
            } else if (!sdCardMount) {
                remoteViews.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power);
            } else {
                remoteViews.setImageViewBitmap(R.id.wdt_lf_up_app, BitmapFactory.decodeFile(wdt_icons_lf_up));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_lf_down)) {
                remoteViews.setImageViewResource(R.id.wdt_lf_down_app, Integer.valueOf(wdt_icons_lf_down).intValue());
            } else if (!sdCardMount) {
                remoteViews.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call);
            } else {
                remoteViews.setImageViewBitmap(R.id.wdt_lf_down_app, BitmapFactory.decodeFile(wdt_icons_lf_down));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_rh_up)) {
                remoteViews.setImageViewResource(R.id.wdt_rh_up_app, Integer.valueOf(wdt_icons_rh_up).intValue());
            } else if (!sdCardMount) {
                remoteViews.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer);
            } else {
                remoteViews.setImageViewBitmap(R.id.wdt_rh_up_app, BitmapFactory.decodeFile(wdt_icons_rh_up));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_rh_down)) {
                remoteViews.setImageViewResource(R.id.wdt_rh_down_app, Integer.valueOf(wdt_icons_rh_down).intValue());
            } else if (!sdCardMount) {
                remoteViews.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms);
            } else {
                remoteViews.setImageViewBitmap(R.id.wdt_rh_down_app, BitmapFactory.decodeFile(wdt_icons_rh_down));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_center)) {
                remoteViews.setImageViewResource(R.id.wdt_center_app, Integer.valueOf(wdt_icons_center).intValue());
            } else if (!sdCardMount) {
                remoteViews.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock);
            } else {
                remoteViews.setImageViewBitmap(R.id.wdt_center_app, BitmapFactory.decodeFile(wdt_icons_center));
            }
            ItfunzSuperToolsWidgetServiceMid.this.updateWidget(ItfunzSuperToolsWidgetServiceMid.this, remoteViews);
            RemoteViews remoteViews2 = new RemoteViews("com.itfunz.itfunzsupertools", LayoutId.intValue());
            RemoteViews remoteViews3 = new RemoteViews("com.itfunz.itfunzsupertools", LayoutId.intValue());
            remoteViews2.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_original_mid_lf.png"));
            ItfunzSuperToolsWidgetServiceMid.this.updateWidget(ItfunzSuperToolsWidgetServiceMid.this, remoteViews2);
            remoteViews3.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_original_mid_rh.png"));
            ItfunzSuperToolsWidgetServiceMid.this.updateWidget(ItfunzSuperToolsWidgetServiceMid.this, remoteViews3);
            return null;
        }
    }

    public class refreshUI_ICON extends AsyncTask<RemoteViews, Integer, String> {
        public refreshUI_ICON() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(RemoteViews... params) {
            SharedPreferences Settings = PreferenceManager.getDefaultSharedPreferences(ItfunzSuperToolsWidgetServiceMid.this);
            boolean sdCardMount = Environment.getExternalStorageState().equals("mounted");
            String wdt_icons_lf_up = Settings.getString("wdt_icons_lf_up", String.valueOf((int) R.drawable.wdt_app_power));
            String wdt_icons_lf_down = Settings.getString("wdt_icons_lf_down", String.valueOf((int) R.drawable.wdt_app_call));
            String wdt_icons_rh_up = Settings.getString("wdt_icons_rh_up", String.valueOf((int) R.drawable.wdt_app_explorer));
            String wdt_icons_rh_down = Settings.getString("wdt_icons_rh_down", String.valueOf((int) R.drawable.wdt_app_mms));
            String wdt_icons_center = Settings.getString("wdt_icons_center", String.valueOf((int) R.drawable.wdt_app_lock));
            Integer LayoutId = Integer.valueOf((int) R.layout.toolwidgetmid);
            String themePath = "/sdcard/itfunzsupertools/widget_theme/" + Settings.getString("change_theme", "默认主题") + "/342/";
            RemoteViews remoteViews = new RemoteViews("com.itfunz.itfunzsupertools", LayoutId.intValue());
            RemoteViews remoteViews2 = new RemoteViews("com.itfunz.itfunzsupertools", LayoutId.intValue());
            RemoteViews remoteViews3 = new RemoteViews("com.itfunz.itfunzsupertools", LayoutId.intValue());
            if (ItfunzCheckSum.isNumeric(wdt_icons_lf_up)) {
                remoteViews.setImageViewResource(R.id.wdt_lf_up_app, Integer.valueOf(wdt_icons_lf_up).intValue());
            } else if (!sdCardMount) {
                remoteViews.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power);
            } else {
                remoteViews.setImageViewBitmap(R.id.wdt_lf_up_app, BitmapFactory.decodeFile(wdt_icons_lf_up));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_lf_down)) {
                remoteViews.setImageViewResource(R.id.wdt_lf_down_app, Integer.valueOf(wdt_icons_lf_down).intValue());
            } else if (!sdCardMount) {
                remoteViews.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call);
            } else {
                remoteViews.setImageViewBitmap(R.id.wdt_lf_down_app, BitmapFactory.decodeFile(wdt_icons_lf_down));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_rh_up)) {
                remoteViews.setImageViewResource(R.id.wdt_rh_up_app, Integer.valueOf(wdt_icons_rh_up).intValue());
            } else if (!sdCardMount) {
                remoteViews.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer);
            } else {
                remoteViews.setImageViewBitmap(R.id.wdt_rh_up_app, BitmapFactory.decodeFile(wdt_icons_rh_up));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_rh_down)) {
                remoteViews.setImageViewResource(R.id.wdt_rh_down_app, Integer.valueOf(wdt_icons_rh_down).intValue());
            } else if (!sdCardMount) {
                remoteViews.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms);
            } else {
                remoteViews.setImageViewBitmap(R.id.wdt_rh_down_app, BitmapFactory.decodeFile(wdt_icons_rh_down));
            }
            if (ItfunzCheckSum.isNumeric(wdt_icons_center)) {
                remoteViews.setImageViewResource(R.id.wdt_center_app, Integer.valueOf(wdt_icons_center).intValue());
            } else if (!sdCardMount) {
                remoteViews.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock);
            } else {
                remoteViews.setImageViewBitmap(R.id.wdt_center_app, BitmapFactory.decodeFile(wdt_icons_center));
            }
            ItfunzSuperToolsWidgetServiceMid.this.updateWidget(ItfunzSuperToolsWidgetServiceMid.this, remoteViews);
            remoteViews2.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_original_mid_lf.png"));
            ItfunzSuperToolsWidgetServiceMid.this.updateWidget(ItfunzSuperToolsWidgetServiceMid.this, remoteViews2);
            remoteViews3.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_original_mid_rh.png"));
            ItfunzSuperToolsWidgetServiceMid.this.updateWidget(ItfunzSuperToolsWidgetServiceMid.this, remoteViews3);
            return null;
        }
    }
}
