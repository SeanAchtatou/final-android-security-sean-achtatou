package com.itfunz.itfunzsupertools.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;
import com.itfunz.itfunzsupertools.R;
import com.itfunz.itfunzsupertools.command.ItfunzCheckSum;
import java.io.File;

public class ItfunzSuperToolsWidgetBig extends AppWidgetProvider {
    final String app_Center_Button = "itfunz.app.center";
    final String app_Left_Button_Down = "itfunz.app.left_down";
    final String app_Left_Button_Up = "itfunz.app.left_up";
    final String app_Right_Button_Down = "itfunz.app.right_down";
    final String app_Right_Button_Up = "itfunz.app.right_up";

    public void onDeleted(Context context, int[] appWidgetIds) {
        context.stopService(new Intent(context, ItfunzSuperToolsWidgetServiceBig.class));
        super.onDeleted(context, appWidgetIds);
    }

    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        SharedPreferences Settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences appSettings = context.getSharedPreferences("AppInfoPackage", 0);
        String theme_name = Settings.getString("change_theme", "默认主题");
        String action = intent.getAction();
        final RemoteViews mainViews = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
        RemoteViews remoteViews = new RemoteViews("com.itfunz.itfunzsupertools", R.layout.toolwidgetbig);
        final boolean sdCardMount = Environment.getExternalStorageState().equals("mounted");
        final String wdt_icons_lf_up = Settings.getString("wdt_icons_lf_up", String.valueOf((int) R.drawable.wdt_app_power));
        final String wdt_icons_lf_down = Settings.getString("wdt_icons_lf_down", String.valueOf((int) R.drawable.wdt_app_call));
        final String wdt_icons_rh_up = Settings.getString("wdt_icons_rh_up", String.valueOf((int) R.drawable.wdt_app_explorer));
        final String wdt_icons_rh_down = Settings.getString("wdt_icons_rh_down", String.valueOf((int) R.drawable.wdt_app_mms));
        final String wdt_icons_center = Settings.getString("wdt_icons_center", String.valueOf((int) R.drawable.wdt_app_lock));
        String wdt_icons_lf_up_push = Settings.getString("wdt_icons_lf_up_push", String.valueOf((int) R.drawable.wdt_app_power_push));
        String wdt_icons_lf_down_push = Settings.getString("wdt_icons_lf_down_push", String.valueOf((int) R.drawable.wdt_app_call_push));
        String wdt_icons_rh_up_push = Settings.getString("wdt_icons_rh_up_push", String.valueOf((int) R.drawable.wdt_app_explorer_push));
        String wdt_icons_rh_down_push = Settings.getString("wdt_icons_rh_down_push", String.valueOf((int) R.drawable.wdt_app_mms_push));
        String wdt_icons_center_push = Settings.getString("wdt_icons_center_push", String.valueOf((int) R.drawable.wdt_app_lock_push));
        if (action.equals("itfunz.screen.portrait") || action.equals("itfunz.screen.refresh") || action.equals("itfunz.screen.landscape")) {
            context.startService(new Intent(context, ItfunzSuperToolsWidgetServiceBig.class));
        }
        if (theme_name.equals("默认主题")) {
            if (action.equals("itfunz.app.left_up")) {
                String ActivityName = Settings.getString("widget_settings_change_function_left_up", "com.itfunz.itfunzsupertools.ItfunzPowerManager");
                String packName = appSettings.getString(ActivityName, "com.itfunz.itfunzsupertools");
                String wdt_icons_lf_up_push_str = String.valueOf(wdt_icons_lf_up.substring(0, wdt_icons_lf_up.length() - 4)) + "_push.png";
                if (!new File(wdt_icons_lf_up_push_str).exists()) {
                    wdt_icons_lf_up_push_str = wdt_icons_lf_up;
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_lf_up)) {
                    mainViews.setImageViewResource(R.id.wdt_lf_up_app, Integer.valueOf(wdt_icons_lf_up_push).intValue());
                } else if (!sdCardMount) {
                    mainViews.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power_push);
                } else {
                    mainViews.setImageViewBitmap(R.id.wdt_lf_up_app, BitmapFactory.decodeFile(wdt_icons_lf_up_push_str));
                }
                mainViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_lf_up_big_lf);
                mainViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_lf_up_big_rh);
                updateWidget(context, mainViews);
                final Context context2 = context;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (ItfunzCheckSum.isNumeric(wdt_icons_lf_up)) {
                            mainViews.setImageViewResource(R.id.wdt_lf_up_app, Integer.valueOf(wdt_icons_lf_up).intValue());
                        } else if (!sdCardMount) {
                            mainViews.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power);
                        } else {
                            mainViews.setImageViewBitmap(R.id.wdt_lf_up_app, BitmapFactory.decodeFile(wdt_icons_lf_up));
                        }
                        mainViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_big_lf);
                        mainViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_big_rh);
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context2, mainViews);
                    }
                }, 2000);
                Intent intent2 = new Intent("android.intent.action.MAIN");
                intent2.setComponent(new ComponentName(packName, ActivityName));
                intent2.setFlags(268435456);
                context.startActivity(intent2);
            } else if (action.equals("itfunz.app.right_up")) {
                String ActivityName2 = Settings.getString("widget_settings_change_function_right_up", "com.itfunz.itfunzsupertools.ItfunzFileDialog");
                String packName2 = appSettings.getString(ActivityName2, "com.itfunz.itfunzsupertools");
                String wdt_icons_rh_up_push_str = String.valueOf(wdt_icons_rh_up.substring(0, wdt_icons_rh_up.length() - 4)) + "_push.png";
                if (!new File(wdt_icons_rh_up_push_str).exists()) {
                    wdt_icons_rh_up_push_str = wdt_icons_rh_up;
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_rh_up)) {
                    mainViews.setImageViewResource(R.id.wdt_rh_up_app, Integer.valueOf(wdt_icons_rh_up_push).intValue());
                } else if (!sdCardMount) {
                    mainViews.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer_push);
                } else {
                    mainViews.setImageViewBitmap(R.id.wdt_rh_up_app, BitmapFactory.decodeFile(wdt_icons_rh_up_push_str));
                }
                mainViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_rh_up_big_lf);
                mainViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_rh_up_big_rh);
                updateWidget(context, mainViews);
                final RemoteViews remoteViews2 = mainViews;
                final boolean z = sdCardMount;
                final Context context3 = context;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (ItfunzCheckSum.isNumeric(wdt_icons_rh_up)) {
                            remoteViews2.setImageViewResource(R.id.wdt_rh_up_app, Integer.valueOf(wdt_icons_rh_up).intValue());
                        } else if (!z) {
                            remoteViews2.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer);
                        } else {
                            remoteViews2.setImageViewBitmap(R.id.wdt_rh_up_app, BitmapFactory.decodeFile(wdt_icons_rh_up));
                        }
                        remoteViews2.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_big_lf);
                        remoteViews2.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_big_rh);
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context3, remoteViews2);
                    }
                }, 2000);
                Intent intent3 = new Intent("android.intent.action.MAIN");
                intent3.setComponent(new ComponentName(packName2, ActivityName2));
                intent3.setFlags(268435456);
                context.startActivity(intent3);
            } else if (action.equals("itfunz.app.left_down")) {
                String ActivityName3 = Settings.getString("widget_settings_change_function_left_down", "com.android.contacts.DialtactsActivity");
                String packName3 = appSettings.getString(ActivityName3, "com.android.contacts");
                String wdt_icons_lf_down_push_str = String.valueOf(wdt_icons_lf_down.substring(0, wdt_icons_lf_down.length() - 4)) + "_push.png";
                if (!new File(wdt_icons_lf_down_push_str).exists()) {
                    wdt_icons_lf_down_push_str = wdt_icons_lf_down;
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_lf_down)) {
                    mainViews.setImageViewResource(R.id.wdt_lf_down_app, Integer.valueOf(wdt_icons_lf_down_push).intValue());
                } else if (!sdCardMount) {
                    mainViews.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call_push);
                } else {
                    mainViews.setImageViewBitmap(R.id.wdt_lf_down_app, BitmapFactory.decodeFile(wdt_icons_lf_down_push_str));
                }
                mainViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_lf_down_big_lf);
                mainViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_lf_down_big_rh);
                updateWidget(context, mainViews);
                final RemoteViews remoteViews3 = mainViews;
                final boolean z2 = sdCardMount;
                final Context context4 = context;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (ItfunzCheckSum.isNumeric(wdt_icons_lf_down)) {
                            remoteViews3.setImageViewResource(R.id.wdt_lf_down_app, Integer.valueOf(wdt_icons_lf_down).intValue());
                        } else if (!z2) {
                            remoteViews3.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call);
                        } else {
                            remoteViews3.setImageViewBitmap(R.id.wdt_lf_down_app, BitmapFactory.decodeFile(wdt_icons_lf_down));
                        }
                        remoteViews3.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_big_lf);
                        remoteViews3.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_big_rh);
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context4, remoteViews3);
                    }
                }, 2000);
                Intent intent4 = new Intent("android.intent.action.MAIN");
                intent4.setComponent(new ComponentName(packName3, ActivityName3));
                intent4.setFlags(268435456);
                context.startActivity(intent4);
            } else if (action.equals("itfunz.app.right_down")) {
                String ActivityName4 = Settings.getString("widget_settings_change_function_right_down", "com.android.mms.ui.ConversationList");
                String packName4 = appSettings.getString(ActivityName4, "com.android.mms");
                String wdt_icons_rh_down_push_str = String.valueOf(wdt_icons_rh_down.substring(0, wdt_icons_rh_down.length() - 4)) + "_push.png";
                if (!new File(wdt_icons_rh_down_push_str).exists()) {
                    wdt_icons_rh_down_push_str = wdt_icons_rh_down;
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_rh_down)) {
                    mainViews.setImageViewResource(R.id.wdt_rh_down_app, Integer.valueOf(wdt_icons_rh_down_push).intValue());
                } else if (!sdCardMount) {
                    mainViews.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms_push);
                } else {
                    mainViews.setImageViewBitmap(R.id.wdt_rh_down_app, BitmapFactory.decodeFile(wdt_icons_rh_down_push_str));
                }
                mainViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_rh_down_big_lf);
                mainViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_rh_down_big_rh);
                updateWidget(context, mainViews);
                final RemoteViews remoteViews4 = mainViews;
                final boolean z3 = sdCardMount;
                final Context context5 = context;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (ItfunzCheckSum.isNumeric(wdt_icons_rh_down)) {
                            remoteViews4.setImageViewResource(R.id.wdt_rh_down_app, Integer.valueOf(wdt_icons_rh_down).intValue());
                        } else if (!z3) {
                            remoteViews4.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms);
                        } else {
                            remoteViews4.setImageViewBitmap(R.id.wdt_rh_down_app, BitmapFactory.decodeFile(wdt_icons_rh_down));
                        }
                        remoteViews4.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_big_lf);
                        remoteViews4.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_big_rh);
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context5, remoteViews4);
                    }
                }, 2000);
                Intent intent5 = new Intent("android.intent.action.MAIN");
                intent5.setComponent(new ComponentName(packName4, ActivityName4));
                intent5.setFlags(268435456);
                context.startActivity(intent5);
            } else if (action.equals("itfunz.app.center")) {
                String ActivityName5 = Settings.getString("widget_settings_change_function_center", "com.itfunz.itfunzsupertools.OneKeyLockScreen");
                String packName5 = appSettings.getString(ActivityName5, "com.itfunz.itfunzsupertools");
                mainViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_center_big_lf);
                mainViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_center_big_rh);
                String wdt_icons_center_push_str = String.valueOf(wdt_icons_center.substring(0, wdt_icons_center.length() - 4)) + "_push.png";
                if (!new File(wdt_icons_center_push_str).exists()) {
                    wdt_icons_center_push_str = wdt_icons_center;
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_center)) {
                    mainViews.setImageViewResource(R.id.wdt_center_app, Integer.valueOf(wdt_icons_center_push).intValue());
                } else if (!sdCardMount) {
                    mainViews.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock_push);
                } else {
                    mainViews.setImageViewBitmap(R.id.wdt_center_app, BitmapFactory.decodeFile(wdt_icons_center_push_str));
                }
                updateWidget(context, mainViews);
                final RemoteViews remoteViews5 = mainViews;
                final boolean z4 = sdCardMount;
                final Context context6 = context;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        remoteViews5.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_big_lf);
                        remoteViews5.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_big_rh);
                        if (ItfunzCheckSum.isNumeric(wdt_icons_center)) {
                            remoteViews5.setImageViewResource(R.id.wdt_center_app, Integer.valueOf(wdt_icons_center).intValue());
                        } else if (!z4) {
                            remoteViews5.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock);
                        } else {
                            remoteViews5.setImageViewBitmap(R.id.wdt_center_app, BitmapFactory.decodeFile(wdt_icons_center));
                        }
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context6, remoteViews5);
                    }
                }, 2000);
                Intent intent6 = new Intent("android.intent.action.MAIN");
                intent6.setComponent(new ComponentName(packName5, ActivityName5));
                intent6.setFlags(268435456);
                context.startActivity(intent6);
            }
        } else if (sdCardMount) {
            final String themePath = "/sdcard/itfunzsupertools/widget_theme/" + theme_name + "/422/";
            if (action.equals("itfunz.app.left_up")) {
                String ActivityName6 = Settings.getString("widget_settings_change_function_left_up", "com.itfunz.itfunzsupertools.ItfunzPowerManager");
                String packName6 = appSettings.getString(ActivityName6, "com.itfunz.itfunzsupertools");
                String wdt_icons_lf_up_push_str2 = String.valueOf(wdt_icons_lf_up.substring(0, wdt_icons_lf_up.length() - 4)) + "_push.png";
                if (!new File(wdt_icons_lf_up_push_str2).exists()) {
                    wdt_icons_lf_up_push_str2 = wdt_icons_lf_up;
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_lf_up)) {
                    mainViews.setImageViewResource(R.id.wdt_lf_up_app, Integer.valueOf(wdt_icons_lf_up_push).intValue());
                } else if (!sdCardMount) {
                    mainViews.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power_push);
                } else {
                    mainViews.setImageViewBitmap(R.id.wdt_lf_up_app, BitmapFactory.decodeFile(wdt_icons_lf_up_push_str2));
                }
                mainViews.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_lf_up_big_lf.png"));
                remoteViews.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_lf_up_big_rh.png"));
                updateWidget(context, mainViews);
                updateWidget(context, remoteViews);
                final String str = wdt_icons_lf_up;
                final boolean z5 = sdCardMount;
                final Context context7 = context;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        RemoteViews mainViews = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
                        RemoteViews mainViews1 = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
                        RemoteViews mainViews2 = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
                        if (ItfunzCheckSum.isNumeric(str)) {
                            mainViews.setImageViewResource(R.id.wdt_lf_up_app, Integer.valueOf(str).intValue());
                        } else if (!z5) {
                            mainViews.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power);
                        } else {
                            mainViews.setImageViewBitmap(R.id.wdt_lf_up_app, BitmapFactory.decodeFile(str));
                        }
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context7, mainViews);
                        mainViews1.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_original_big_lf.png"));
                        mainViews2.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_original_big_rh.png"));
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context7, mainViews1);
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context7, mainViews2);
                    }
                }, 2000);
                Intent intent7 = new Intent("android.intent.action.MAIN");
                intent7.setComponent(new ComponentName(packName6, ActivityName6));
                intent7.setFlags(268435456);
                context.startActivity(intent7);
            } else if (action.equals("itfunz.app.right_up")) {
                String ActivityName7 = Settings.getString("widget_settings_change_function_right_up", "com.itfunz.itfunzsupertools.ItfunzFileDialog");
                String packName7 = appSettings.getString(ActivityName7, "com.itfunz.itfunzsupertools");
                String wdt_icons_rh_up_push_str2 = String.valueOf(wdt_icons_rh_up.substring(0, wdt_icons_rh_up.length() - 4)) + "_push.png";
                if (!new File(wdt_icons_rh_up_push_str2).exists()) {
                    wdt_icons_rh_up_push_str2 = wdt_icons_rh_up;
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_rh_up)) {
                    mainViews.setImageViewResource(R.id.wdt_rh_up_app, Integer.valueOf(wdt_icons_rh_up_push).intValue());
                } else if (!sdCardMount) {
                    mainViews.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer_push);
                } else {
                    mainViews.setImageViewBitmap(R.id.wdt_rh_up_app, BitmapFactory.decodeFile(wdt_icons_rh_up_push_str2));
                }
                mainViews.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_rh_up_big_lf.png"));
                remoteViews.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_rh_up_big_rh.png"));
                updateWidget(context, mainViews);
                updateWidget(context, remoteViews);
                final String str2 = wdt_icons_rh_up;
                final boolean z6 = sdCardMount;
                final Context context8 = context;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        RemoteViews mainViews = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
                        RemoteViews mainViews1 = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
                        if (ItfunzCheckSum.isNumeric(str2)) {
                            mainViews.setImageViewResource(R.id.wdt_rh_up_app, Integer.valueOf(str2).intValue());
                        } else if (!z6) {
                            mainViews.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer);
                        } else {
                            mainViews.setImageViewBitmap(R.id.wdt_rh_up_app, BitmapFactory.decodeFile(str2));
                        }
                        mainViews.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_original_big_lf.png"));
                        mainViews1.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_original_big_rh.png"));
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context8, mainViews);
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context8, mainViews1);
                    }
                }, 2000);
                Intent intent8 = new Intent("android.intent.action.MAIN");
                intent8.setComponent(new ComponentName(packName7, ActivityName7));
                intent8.setFlags(268435456);
                context.startActivity(intent8);
            } else if (action.equals("itfunz.app.left_down")) {
                String ActivityName8 = Settings.getString("widget_settings_change_function_left_down", "com.android.contacts.DialtactsActivity");
                String packName8 = appSettings.getString(ActivityName8, "com.android.contacts");
                String wdt_icons_lf_down_push_str2 = String.valueOf(wdt_icons_lf_down.substring(0, wdt_icons_lf_down.length() - 4)) + "_push.png";
                if (!new File(wdt_icons_lf_down_push_str2).exists()) {
                    wdt_icons_lf_down_push_str2 = wdt_icons_lf_down;
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_lf_down)) {
                    mainViews.setImageViewResource(R.id.wdt_lf_down_app, Integer.valueOf(wdt_icons_lf_down_push).intValue());
                } else if (!sdCardMount) {
                    mainViews.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call_push);
                } else {
                    mainViews.setImageViewBitmap(R.id.wdt_lf_down_app, BitmapFactory.decodeFile(wdt_icons_lf_down_push_str2));
                }
                mainViews.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_lf_down_big_lf.png"));
                remoteViews.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_lf_down_big_rh.png"));
                updateWidget(context, mainViews);
                updateWidget(context, remoteViews);
                final String str3 = wdt_icons_lf_down;
                final boolean z7 = sdCardMount;
                final Context context9 = context;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        RemoteViews mainViews = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
                        RemoteViews mainViews1 = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
                        if (ItfunzCheckSum.isNumeric(str3)) {
                            mainViews.setImageViewResource(R.id.wdt_lf_down_app, Integer.valueOf(str3).intValue());
                        } else if (!z7) {
                            mainViews.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call);
                        } else {
                            mainViews.setImageViewBitmap(R.id.wdt_lf_down_app, BitmapFactory.decodeFile(str3));
                        }
                        mainViews.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_original_big_lf.png"));
                        mainViews1.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_original_big_rh.png"));
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context9, mainViews);
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context9, mainViews1);
                    }
                }, 2000);
                Intent intent9 = new Intent("android.intent.action.MAIN");
                intent9.setComponent(new ComponentName(packName8, ActivityName8));
                intent9.setFlags(268435456);
                context.startActivity(intent9);
            } else if (action.equals("itfunz.app.right_down")) {
                String ActivityName9 = Settings.getString("widget_settings_change_function_right_down", "com.android.mms.ui.ConversationList");
                String packName9 = appSettings.getString(ActivityName9, "com.android.mms");
                String wdt_icons_rh_down_push_str2 = String.valueOf(wdt_icons_rh_down.substring(0, wdt_icons_rh_down.length() - 4)) + "_push.png";
                if (!new File(wdt_icons_rh_down_push_str2).exists()) {
                    wdt_icons_rh_down_push_str2 = wdt_icons_rh_down;
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_rh_down)) {
                    mainViews.setImageViewResource(R.id.wdt_rh_down_app, Integer.valueOf(wdt_icons_rh_down_push).intValue());
                } else if (!sdCardMount) {
                    mainViews.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms_push);
                } else {
                    mainViews.setImageViewBitmap(R.id.wdt_rh_down_app, BitmapFactory.decodeFile(wdt_icons_rh_down_push_str2));
                }
                mainViews.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_rh_down_big_lf.png"));
                remoteViews.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_rh_down_big_rh.png"));
                updateWidget(context, mainViews);
                updateWidget(context, remoteViews);
                final boolean z8 = sdCardMount;
                final String str4 = themePath;
                final Context context10 = context;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        RemoteViews mainViews = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
                        RemoteViews mainViews1 = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
                        if (ItfunzCheckSum.isNumeric(wdt_icons_rh_down)) {
                            mainViews.setImageViewResource(R.id.wdt_rh_down_app, Integer.valueOf(wdt_icons_rh_down).intValue());
                        } else if (!z8) {
                            mainViews.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms);
                        } else {
                            mainViews.setImageViewBitmap(R.id.wdt_rh_down_app, BitmapFactory.decodeFile(wdt_icons_rh_down));
                        }
                        mainViews.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(str4) + "wdt_layout_original_big_lf.png"));
                        mainViews1.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(str4) + "wdt_layout_original_big_rh.png"));
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context10, mainViews);
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context10, mainViews1);
                    }
                }, 2000);
                Intent intent10 = new Intent("android.intent.action.MAIN");
                intent10.setComponent(new ComponentName(packName9, ActivityName9));
                intent10.setFlags(268435456);
                context.startActivity(intent10);
            } else if (action.equals("itfunz.app.center")) {
                String ActivityName10 = Settings.getString("widget_settings_change_function_center", "com.itfunz.itfunzsupertools.OneKeyLockScreen");
                String packName10 = appSettings.getString(ActivityName10, "com.itfunz.itfunzsupertools");
                String wdt_icons_center_push_str2 = String.valueOf(wdt_icons_center.substring(0, wdt_icons_center.length() - 4)) + "_push.png";
                if (!new File(wdt_icons_center_push_str2).exists()) {
                    wdt_icons_center_push_str2 = wdt_icons_center;
                }
                if (ItfunzCheckSum.isNumeric(wdt_icons_center)) {
                    mainViews.setImageViewResource(R.id.wdt_center_app, Integer.valueOf(wdt_icons_center_push).intValue());
                } else if (!sdCardMount) {
                    mainViews.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock_push);
                } else {
                    mainViews.setImageViewBitmap(R.id.wdt_center_app, BitmapFactory.decodeFile(wdt_icons_center_push_str2));
                }
                mainViews.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_center_big_lf.png"));
                remoteViews.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(themePath) + "wdt_layout_center_big_rh.png"));
                updateWidget(context, mainViews);
                updateWidget(context, remoteViews);
                final boolean z9 = sdCardMount;
                final String str5 = themePath;
                final Context context11 = context;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        RemoteViews mainViews = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
                        RemoteViews mainViews1 = new RemoteViews("com.itfunz.itfunzsupertools", (int) R.layout.toolwidgetbig);
                        if (ItfunzCheckSum.isNumeric(wdt_icons_center)) {
                            mainViews.setImageViewResource(R.id.wdt_center_app, Integer.valueOf(wdt_icons_center).intValue());
                        } else if (!z9) {
                            mainViews.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock);
                        } else {
                            mainViews.setImageViewBitmap(R.id.wdt_center_app, BitmapFactory.decodeFile(wdt_icons_center));
                        }
                        mainViews.setImageViewBitmap(R.id.wdt_layout_lf, BitmapFactory.decodeFile(String.valueOf(str5) + "wdt_layout_original_big_lf.png"));
                        mainViews1.setImageViewBitmap(R.id.wdt_layout_rh, BitmapFactory.decodeFile(String.valueOf(str5) + "wdt_layout_original_big_rh.png"));
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context11, mainViews);
                        ItfunzSuperToolsWidgetBig.this.updateWidget(context11, mainViews1);
                    }
                }, 2000);
                Intent intent11 = new Intent("android.intent.action.MAIN");
                intent11.setComponent(new ComponentName(packName10, ActivityName10));
                intent11.setFlags(268435456);
                context.startActivity(intent11);
            }
        } else if (action.equals("itfunz.app.left_up")) {
            String ActivityName11 = Settings.getString("widget_settings_change_function_left_up", "com.itfunz.itfunzsupertools.ItfunzPowerManager");
            String packName11 = appSettings.getString(ActivityName11, "com.itfunz.itfunzsupertools");
            if (ItfunzCheckSum.isNumeric(wdt_icons_lf_up)) {
                mainViews.setImageViewResource(R.id.wdt_lf_up_app, Integer.valueOf(wdt_icons_lf_up_push).intValue());
            } else {
                mainViews.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power_push);
            }
            mainViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_lf_up_big_lf);
            mainViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_lf_up_big_rh);
            updateWidget(context, mainViews);
            final String str6 = wdt_icons_lf_up;
            final RemoteViews remoteViews6 = mainViews;
            final Context context12 = context;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (ItfunzCheckSum.isNumeric(str6)) {
                        remoteViews6.setImageViewResource(R.id.wdt_lf_up_app, Integer.valueOf(str6).intValue());
                    } else {
                        remoteViews6.setImageViewResource(R.id.wdt_lf_up_app, R.drawable.wdt_app_power);
                    }
                    remoteViews6.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_big_lf);
                    remoteViews6.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_big_rh);
                    ItfunzSuperToolsWidgetBig.this.updateWidget(context12, remoteViews6);
                }
            }, 2000);
            Intent intent12 = new Intent("android.intent.action.MAIN");
            intent12.setComponent(new ComponentName(packName11, ActivityName11));
            intent12.setFlags(268435456);
            context.startActivity(intent12);
        } else if (action.equals("itfunz.app.right_up")) {
            String ActivityName12 = Settings.getString("widget_settings_change_function_right_up", "com.itfunz.itfunzsupertools.ItfunzFileDialog");
            String packName12 = appSettings.getString(ActivityName12, "com.itfunz.itfunzsupertools");
            if (ItfunzCheckSum.isNumeric(wdt_icons_rh_up)) {
                mainViews.setImageViewResource(R.id.wdt_rh_up_app, Integer.valueOf(wdt_icons_rh_up_push).intValue());
            } else {
                mainViews.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer_push);
            }
            mainViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_rh_up_big_lf);
            mainViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_rh_up_big_rh);
            updateWidget(context, mainViews);
            final String str7 = wdt_icons_rh_up;
            final RemoteViews remoteViews7 = mainViews;
            final Context context13 = context;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (ItfunzCheckSum.isNumeric(str7)) {
                        remoteViews7.setImageViewResource(R.id.wdt_rh_up_app, Integer.valueOf(str7).intValue());
                    } else {
                        remoteViews7.setImageViewResource(R.id.wdt_rh_up_app, R.drawable.wdt_app_explorer);
                    }
                    remoteViews7.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_big_lf);
                    remoteViews7.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_big_rh);
                    ItfunzSuperToolsWidgetBig.this.updateWidget(context13, remoteViews7);
                }
            }, 2000);
            Intent intent13 = new Intent("android.intent.action.MAIN");
            intent13.setComponent(new ComponentName(packName12, ActivityName12));
            intent13.setFlags(268435456);
            context.startActivity(intent13);
        } else if (action.equals("itfunz.app.left_down")) {
            String ActivityName13 = Settings.getString("widget_settings_change_function_left_down", "com.android.contacts.DialtactsActivity");
            String packName13 = appSettings.getString(ActivityName13, "com.android.contacts");
            if (ItfunzCheckSum.isNumeric(wdt_icons_lf_down)) {
                mainViews.setImageViewResource(R.id.wdt_lf_down_app, Integer.valueOf(wdt_icons_lf_down_push).intValue());
            } else {
                mainViews.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call_push);
            }
            mainViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_lf_down_big_lf);
            mainViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_lf_down_big_rh);
            updateWidget(context, mainViews);
            final String str8 = wdt_icons_lf_down;
            final RemoteViews remoteViews8 = mainViews;
            final Context context14 = context;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (ItfunzCheckSum.isNumeric(str8)) {
                        remoteViews8.setImageViewResource(R.id.wdt_lf_down_app, Integer.valueOf(str8).intValue());
                    } else {
                        remoteViews8.setImageViewResource(R.id.wdt_lf_down_app, R.drawable.wdt_app_call);
                    }
                    remoteViews8.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_big_lf);
                    remoteViews8.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_big_rh);
                    ItfunzSuperToolsWidgetBig.this.updateWidget(context14, remoteViews8);
                }
            }, 2000);
            Intent intent14 = new Intent("android.intent.action.MAIN");
            intent14.setComponent(new ComponentName(packName13, ActivityName13));
            intent14.setFlags(268435456);
            context.startActivity(intent14);
        } else if (action.equals("itfunz.app.right_down")) {
            String ActivityName14 = Settings.getString("widget_settings_change_function_right_down", "com.android.mms.ui.ConversationList");
            String packName14 = appSettings.getString(ActivityName14, "com.android.mms");
            if (ItfunzCheckSum.isNumeric(wdt_icons_rh_down)) {
                mainViews.setImageViewResource(R.id.wdt_rh_down_app, Integer.valueOf(wdt_icons_rh_down_push).intValue());
            } else {
                mainViews.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms_push);
            }
            mainViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_rh_down_big_lf);
            mainViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_rh_down_big_rh);
            updateWidget(context, mainViews);
            final String str9 = wdt_icons_rh_down;
            final RemoteViews remoteViews9 = mainViews;
            final Context context15 = context;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (ItfunzCheckSum.isNumeric(str9)) {
                        remoteViews9.setImageViewResource(R.id.wdt_rh_down_app, Integer.valueOf(str9).intValue());
                    } else {
                        remoteViews9.setImageViewResource(R.id.wdt_rh_down_app, R.drawable.wdt_app_mms);
                    }
                    remoteViews9.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_big_lf);
                    remoteViews9.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_big_rh);
                    ItfunzSuperToolsWidgetBig.this.updateWidget(context15, remoteViews9);
                }
            }, 2000);
            Intent intent15 = new Intent("android.intent.action.MAIN");
            intent15.setComponent(new ComponentName(packName14, ActivityName14));
            intent15.setFlags(268435456);
            context.startActivity(intent15);
        } else if (action.equals("itfunz.app.center")) {
            String ActivityName15 = Settings.getString("widget_settings_change_function_center", "com.itfunz.itfunzsupertools.OneKeyLockScreen");
            String packName15 = appSettings.getString(ActivityName15, "com.itfunz.itfunzsupertools");
            mainViews.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_center_big_lf);
            mainViews.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_center_big_rh);
            if (ItfunzCheckSum.isNumeric(wdt_icons_center)) {
                mainViews.setImageViewResource(R.id.wdt_center_app, Integer.valueOf(wdt_icons_center_push).intValue());
            } else {
                mainViews.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock_push);
            }
            updateWidget(context, mainViews);
            final RemoteViews remoteViews10 = mainViews;
            final String str10 = wdt_icons_center;
            final Context context16 = context;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    remoteViews10.setImageViewResource(R.id.wdt_layout_lf, R.drawable.wdt_layout_original_big_lf);
                    remoteViews10.setImageViewResource(R.id.wdt_layout_rh, R.drawable.wdt_layout_original_big_rh);
                    if (ItfunzCheckSum.isNumeric(str10)) {
                        remoteViews10.setImageViewResource(R.id.wdt_center_app, Integer.valueOf(str10).intValue());
                    } else {
                        remoteViews10.setImageViewResource(R.id.wdt_center_app, R.drawable.wdt_app_lock);
                    }
                    ItfunzSuperToolsWidgetBig.this.updateWidget(context16, remoteViews10);
                }
            }, 2000);
            Intent intent16 = new Intent("android.intent.action.MAIN");
            intent16.setComponent(new ComponentName(packName15, ActivityName15));
            intent16.setFlags(268435456);
            context.startActivity(intent16);
        }
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        context.startService(new Intent(context, ItfunzSuperToolsWidgetServiceBig.class));
    }

    public void updateWidget(Context context, RemoteViews updateViews) {
        AppWidgetManager.getInstance(context).updateAppWidget(new ComponentName(context, ItfunzSuperToolsWidgetBig.class), updateViews);
    }
}
