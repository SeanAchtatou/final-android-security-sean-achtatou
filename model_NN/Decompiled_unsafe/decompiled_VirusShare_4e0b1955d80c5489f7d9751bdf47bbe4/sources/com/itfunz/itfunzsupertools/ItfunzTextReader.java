package com.itfunz.itfunzsupertools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.regex.Pattern;

public class ItfunzTextReader extends Activity {
    private static final String ACTION_VIEW = "android.intent.action.VIEW";
    String fontSize;
    LinearLayout layout;
    private LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -1);
    SharedPreferences settings;
    ScrollView svLayout;
    TextView tvContent;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        int spaceIndex;
        boolean isInSDCard;
        String code;
        InputStreamReader read;
        super.onCreate(savedInstanceState);
        this.settings = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            Intent intent = getIntent();
            String IntentAction = intent.getAction();
            this.svLayout = new ScrollView(this);
            this.svLayout.setLayoutParams(this.params);
            this.svLayout.setVerticalScrollBarEnabled(true);
            this.layout = new LinearLayout(this);
            this.layout.setLayoutParams(this.params);
            this.layout.setBackgroundColor(-1);
            this.tvContent = new TextView(this);
            this.layout.addView(this.tvContent);
            this.svLayout.addView(this.layout);
            this.svLayout.setBackgroundColor(-1);
            if (IntentAction.equals(ACTION_VIEW)) {
                String fileUriPath = intent.getDataString();
                String fileName = fileUriPath.substring(fileUriPath.lastIndexOf("/") + 1, fileUriPath.length());
                String filePath = fileUriPath.substring(fileUriPath.indexOf("file://") + 7, fileUriPath.length());
                String result = "";
                int length = filePath.length();
                int indexOf = filePath.indexOf("%20");
                String fileName2 = Utf8URLdecode(fileName);
                int spaceIndex2 = fileName2.indexOf("%20");
                int count = fileName2.length();
                while (spaceIndex != -1) {
                    fileName2 = new StringBuffer().append(fileName2.substring(0, spaceIndex)).append(" ").append(fileName2.substring(spaceIndex + 3, count)).toString();
                    count = fileName2.length();
                    spaceIndex2 = fileName2.indexOf("%20");
                }
                if (isUtf8Url(filePath)) {
                    filePath = Utf8URLdecode(filePath);
                    spaceIndex = filePath.indexOf("%20");
                    int count2 = filePath.length();
                    while (spaceIndex != -1) {
                        String head = filePath.substring(0, spaceIndex);
                        filePath = String.valueOf(head) + " " + filePath.substring(spaceIndex + 3, count2);
                        count2 = filePath.length();
                        spaceIndex = filePath.indexOf("%20");
                    }
                }
                if (spaceIndex != -1) {
                    int count3 = filePath.length();
                    int spaceIndex3 = filePath.indexOf("%20");
                    while (spaceIndex3 != -1) {
                        filePath = new StringBuffer().append(filePath.substring(0, spaceIndex3)).append(" ").append(filePath.substring(spaceIndex3 + 3, count3)).toString();
                        count3 = filePath.length();
                        spaceIndex3 = filePath.indexOf("%20");
                    }
                    filePath = Utf8URLdecode(filePath);
                }
                if (filePath.indexOf("/sdcard") != -1) {
                    isInSDCard = true;
                } else {
                    isInSDCard = false;
                }
                if (!isInSDCard) {
                    RootScript.runScriptAsRoot("ls -a -l " + filePath + " > /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetail.cfg", new StringBuilder(), 1000);
                    String permission = FileService.getFileContent(this, fileName2);
                    if (permission == null) {
                        RootScript.runRootCommand("cd \"" + filePath.substring(0, filePath.lastIndexOf("/")) + "\"" + "\nls -a -l -R > /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetailEn.cfg");
                        if (permission == null) {
                            permission = FileService.getFileContentEn(this, fileName2);
                        } else {
                            permission = FileService.getFileContentEn(this, fileName2);
                        }
                    }
                    result = FileService.getDefaultPermissions(permission);
                    FileService.changeRootPermissions();
                    RootScript.runRootCommand("chmod 777 " + filePath);
                }
                File file = new File(filePath);
                if ((file.length() / 1024) / 1024 > 10) {
                    createFileError().show();
                    return;
                }
                BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                switch ((bufferedInputStream.read() << 8) + bufferedInputStream.read()) {
                    case 61371:
                        code = "UTF-8";
                        break;
                    case 65279:
                        code = "Unicode big endian";
                        break;
                    case 65534:
                        code = "Unicode";
                        break;
                    default:
                        code = "ANSI";
                        break;
                }
                if (code == "ANSI") {
                    read = new InputStreamReader(new FileInputStream(file), "GBK");
                } else if (code == "Unicode") {
                    read = new InputStreamReader(new FileInputStream(file), "Unicode");
                } else {
                    read = new InputStreamReader(new FileInputStream(file), "UTF-8");
                }
                BufferedReader bufferedReader = new BufferedReader(read);
                StringBuffer buffer = new StringBuffer();
                while (true) {
                    String line = bufferedReader.readLine();
                    if (line == null) {
                        String content = buffer.toString();
                        if (!isInSDCard) {
                            RootScript.runRootCommand("chmod " + result + " " + filePath);
                            FileService.changeSimpleFilePermissions();
                        }
                        this.fontSize = this.settings.getString("ReaderFontSize", "13");
                        int fontSizenum = Integer.valueOf(this.fontSize).intValue();
                        this.tvContent.setText(content);
                        this.tvContent.setTextSize((float) fontSizenum);
                        this.tvContent.setBackgroundColor(-1);
                        this.tvContent.setTextColor(-16777216);
                        setTitle(fileName2);
                        setContentView(this.svLayout);
                        return;
                    }
                    buffer.append(line);
                    buffer.append("\n");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            createError().show();
        }
    }

    public Dialog createError() {
        return new AlertDialog.Builder(this).setTitle("错误").setMessage("不支持的文件格式").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        }).create();
    }

    public Dialog createFileError() {
        return new AlertDialog.Builder(this).setTitle("错误").setMessage("文件过大,内存溢出").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        }).create();
    }

    public String Utf8URLdecode(String text) {
        String result = "";
        if (text != null && text.length() > 0) {
            int p = text.toLowerCase().indexOf("%e");
            if (p == -1) {
                return text;
            }
            while (p != -1) {
                String result2 = String.valueOf(result) + text.substring(0, p);
                String text2 = text.substring(p, text.length());
                if (text2 == "" || text2.length() < 9) {
                    return result2;
                }
                result = String.valueOf(result2) + CodeToWord(text2.substring(0, 9));
                text = text2.substring(9, text2.length());
                p = text.toLowerCase().indexOf("%e");
            }
        }
        return String.valueOf(result) + text;
    }

    private String CodeToWord(String text) {
        try {
            return new String(new byte[]{(byte) (Integer.parseInt(text.substring(1, 3), 16) - 256), (byte) (Integer.parseInt(text.substring(4, 6), 16) - 256), (byte) (Integer.parseInt(text.substring(7, 9), 16) - 256)}, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public boolean isUtf8Url(String text) {
        int p = text.indexOf("%");
        if (p != -1 && text.length() - p > 9) {
            text = text.substring(p, p + 9);
        }
        return Utf8codeCheck(text);
    }

    private boolean Utf8codeCheck(String text) {
        String sign = "";
        if (text.toLowerCase().startsWith("%e")) {
            int i = 0;
            int p = 0;
            while (p != -1) {
                p = text.indexOf("%", p);
                if (p != -1) {
                    p++;
                }
                sign = String.valueOf(sign) + p;
                i++;
            }
        }
        return sign.equals("147-1");
    }

    public void finish() {
        super.finish();
        RootScript.runRootCommand("rm /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetailEn.cfg");
        RootScript.runRootCommand("rm /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetail.cfg");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.addSubMenu(0, 1, 1, "调整文字大小");
        menu.addSubMenu(0, 2, 2, "切换夜间模式");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                createFolderDialog().show();
                break;
            case 2:
                if (!item.getTitle().equals("切换夜间模式")) {
                    item.setTitle("切换夜间模式");
                    this.svLayout.setBackgroundColor(-1);
                    this.tvContent.setBackgroundColor(-1);
                    this.layout.setBackgroundColor(-1);
                    this.tvContent.setTextColor(-16777216);
                    break;
                } else {
                    item.setTitle("切换日间模式");
                    this.svLayout.setBackgroundColor(-16777216);
                    this.tvContent.setBackgroundColor(-16777216);
                    this.layout.setBackgroundColor(-16777216);
                    this.tvContent.setTextColor(-1);
                    break;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    public Dialog createFolderDialog() {
        this.fontSize = this.settings.getString("ReaderFontSize", "13");
        LinearLayout llayout = new LinearLayout(this);
        llayout.setOrientation(1);
        TextView tvTitle = new TextView(this);
        tvTitle.setText("调整文字的大小（1~100）");
        final EditText etSize = new EditText(this);
        etSize.setText(this.fontSize);
        llayout.addView(tvTitle);
        llayout.addView(etSize);
        return new AlertDialog.Builder(this).setTitle("调整文字大小").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String size = etSize.getText().toString().trim();
                if (size.equals("")) {
                    Toast.makeText(ItfunzTextReader.this, "您没有输入任何数字！", 0).show();
                } else if (ItfunzTextReader.this.isNumeric(size)) {
                    int num = Integer.valueOf(size).intValue();
                    if (num <= 0 || num > 100) {
                        Toast.makeText(ItfunzTextReader.this, "您输入数字不在有效范围，请重新输入！", 0).show();
                        return;
                    }
                    ItfunzTextReader.this.settings.edit().putString("ReaderFontSize", String.valueOf(num)).commit();
                    ItfunzTextReader.this.tvContent.setTextSize((float) num);
                } else {
                    Toast.makeText(ItfunzTextReader.this, "您输入的不是数字，请重新输入！", 0).show();
                }
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).setView(llayout).create();
    }

    public boolean isNumeric(String str) {
        if (!Pattern.compile("[0-9]*").matcher(str).matches()) {
            return false;
        }
        return true;
    }
}
