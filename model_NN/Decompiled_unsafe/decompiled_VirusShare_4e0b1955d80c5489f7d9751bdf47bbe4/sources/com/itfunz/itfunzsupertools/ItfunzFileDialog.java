package com.itfunz.itfunzsupertools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.itfunz.itfunzsupertools.command.IconFormat;
import com.itfunz.itfunzsupertools.command.InitFileMimeType;
import com.itfunz.itfunzsupertools.command.RootScript;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class ItfunzFileDialog extends Activity {
    private static final String DATABASE_NAME = "ItfunzSuperTools.db";
    private static final int DATEBASE_VERSION = 8;
    private static final String FILE_FORMAT = "File_Format";
    private static final String FILE_MIME_TYPE = "File_Mime_Type";
    private static final String FILE_SUB_TYPE = "File_Sub_Type";
    private static final String FILE_TABLE_NAME = "FileMimeType";
    public static final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
    String ClipboadeMode;
    String[] Clipboard;
    int[] ImageViewId;
    SimpleAdapter adapter;
    CheckBox cbgroupexec;
    CheckBox cbgroupread;
    CheckBox cbgroupwrite;
    CheckBox cbotherexec;
    CheckBox cbotherread;
    CheckBox cbotherwrite;
    CheckBox cbuserexec;
    CheckBox cbuserread;
    CheckBox cbuserwrite;
    ArrayList<Map<String, Object>> data;
    public String fileSeparator;
    public String fileStrPath;
    File[] files;
    ListView lvLayout;
    DatabaseHelper mOpenHelper = new DatabaseHelper(this);
    boolean mutiSelect = false;
    boolean mutiSelectAll = false;
    String[] permissions;
    public boolean readandwrite;
    String resultFile;
    boolean resultfordelete;
    int[] selectDirId;
    String[] selectDirPath;
    SharedPreferences settings;
    int[] state;
    int stateId = -1;
    String[] tempPath;
    String[] tempPermissions;
    public String themeName;
    public String themePath;
    public String upDirPath = "";

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, ItfunzFileDialog.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, (int) ItfunzFileDialog.DATEBASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    /* access modifiers changed from: private */
    public void CreateFileMimeTypeTable() {
        SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
        Log.i("ITFUNZ:DB", "CREATE TABLE FileMimeType (File_Format text not null, File_Mime_Type text not null, File_Sub_Type text not null );");
        try {
            db.execSQL("DROP TABLE IF EXISTS FileMimeType");
            db.execSQL("CREATE TABLE FileMimeType (File_Format text not null, File_Mime_Type text not null, File_Sub_Type text not null );");
            InitFileMimeTypeTableBase();
            db.close();
        } catch (SQLException e) {
            Log.i("ITFUNZ:DB", "failed");
        }
    }

    private void InitFileMimeTypeTableBase() {
        this.settings = PreferenceManager.getDefaultSharedPreferences(this);
        this.settings.edit().putInt("dbVersion", DATEBASE_VERSION).commit();
        SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
        try {
            for (String sql : InitFileMimeType.InitSql()) {
                db.execSQL(sql);
            }
        } catch (SQLException e) {
            Log.i("ITFUNZ:DB", "failed");
        }
    }

    /* access modifiers changed from: private */
    public Cursor getFileMimeTypeBase() {
        return this.mOpenHelper.getReadableDatabase().query(FILE_TABLE_NAME, new String[]{FILE_FORMAT, FILE_MIME_TYPE, FILE_SUB_TYPE}, null, null, null, null, null);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.settings = PreferenceManager.getDefaultSharedPreferences(this);
        if (this.settings.getBoolean("the_first", true)) {
            try {
                Runtime.getRuntime().exec("su");
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.settings.edit().putBoolean("the_first", false).commit();
        }
        this.readandwrite = this.settings.getBoolean("readandwrite", false);
        this.tempPath = new String[101];
        this.tempPermissions = new String[101];
        this.selectDirId = new int[101];
        this.selectDirPath = new String[101];
        this.Clipboard = new String[1000];
        if (!new File("/data/data/com.itfunz.itfunzsupertools/databases/ItfunzSuperTools.db").exists()) {
            final ProgressDialog pdialog = ProgressDialog.show(this, "文件管理器", "首次使用，配置文件关联中...");
            final Handler handler = new Handler();
            new Thread() {
                public void run() {
                    super.run();
                    ItfunzFileDialog.this.CreateFileMimeTypeTable();
                    Handler handler = handler;
                    final ProgressDialog progressDialog = pdialog;
                    handler.post(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                }
            }.start();
        } else if (this.settings.getInt("dbVersion", 1) < DATEBASE_VERSION) {
            final ProgressDialog pdialog2 = ProgressDialog.show(this, "文件管理器", "发现新数据，重新配置文件关联中...");
            final Handler handler2 = new Handler();
            new Thread() {
                public void run() {
                    super.run();
                    ItfunzFileDialog.this.CreateFileMimeTypeTable();
                    Handler handler = handler2;
                    final ProgressDialog progressDialog = pdialog2;
                    handler.post(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                }
            }.start();
        }
        String StartFilePath = this.settings.getString("firstDir", String.valueOf(File.separator) + Environment.getExternalStorageDirectory().getName());
        if (Environment.getExternalStorageState().equals("mounted")) {
            setTitle(StartFilePath);
            setContentView(createFileDirLayout(StartFilePath));
            return;
        }
        setTitle("/");
        setContentView(createFileDirLayout("/"));
    }

    public void onBackPressed() {
        File file = new File(this.fileStrPath);
        String strParentPath = file.getParent();
        String filePath = file.getPath();
        if (this.mutiSelect) {
            this.mutiSelect = false;
            this.mutiSelectAll = false;
            setContentView(createFileDirLayout(this.fileStrPath));
        } else if (strParentPath != null) {
            int i = 0;
            while (true) {
                if (i < 100) {
                    if (this.tempPath[i] != null && this.tempPath[i].equals(filePath)) {
                        RootScript.runRootCommand("chmod " + this.tempPermissions[i] + " " + "\"" + this.tempPath[i] + "\"");
                        this.tempPath[i] = null;
                        this.tempPermissions[i] = null;
                        break;
                    }
                    i++;
                } else {
                    break;
                }
            }
            setContentView(createFileDirLayout(strParentPath));
            setTitle(strParentPath);
            int i2 = 0;
            while (i2 <= 100) {
                if (this.selectDirPath[i2] == null || !this.selectDirPath[i2].equals(filePath)) {
                    i2++;
                } else {
                    this.lvLayout.setSelection(this.selectDirId[i2]);
                    this.selectDirId[i2] = -1;
                    this.selectDirPath[i2] = null;
                    return;
                }
            }
        } else {
            super.onBackPressed();
        }
    }

    private void SetFileData(String StartFilePath) {
        boolean filehide = this.settings.getBoolean("file_hide", true);
        this.data = new ArrayList<>();
        File file = new File(StartFilePath);
        if (filehide) {
            this.files = file.listFiles();
        } else {
            this.files = file.listFiles(new FileFilter() {
                public boolean accept(File arg0) {
                    if (arg0.getName().substring(0, 1).equals(".")) {
                        return false;
                    }
                    return true;
                }
            });
        }
        if (this.files != null) {
            Arrays.sort(this.files, new Comparator<Object>() {
                public int compare(Object arg0, Object arg1) {
                    return ((File) arg0).getName().toLowerCase().compareTo(((File) arg1).getName().toLowerCase());
                }
            });
        }
        int count = this.files.length;
        RootScript.runScriptAsRoot("cd \"" + StartFilePath + "\"" + "\nls -a -l > /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetail.cfg", new StringBuilder(), 1000);
        if (!new File("/data/data/com.itfunz.itfunzsupertools/files/tmpFileDetail.cfg").exists()) {
            this.upDirPath = StartFilePath.substring(0, StartFilePath.lastIndexOf("/"));
            RootScript.runRootCommand("cd \"" + this.upDirPath + "\"" + "\nls -a -l -R > /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetail.cfg");
        }
        this.permissions = new String[count];
        this.ImageViewId = new int[count];
        for (int i = 0; i < count; i++) {
            File fileIndex = this.files[i];
            String fileName = fileIndex.getName();
            if (filehide) {
                HashMap hashMap = new HashMap();
                if (fileIndex.isDirectory()) {
                    hashMap.put("fileIcon", Integer.valueOf((int) R.drawable.icon_folder));
                    this.ImageViewId[i] = R.drawable.icon_folder;
                } else {
                    hashMap.put("fileIcon", Integer.valueOf(IconFormat.getIconFormat(fileName)));
                    this.ImageViewId[i] = IconFormat.getIconFormat(fileName);
                }
                hashMap.put("fileName", fileName);
                String FileTime = DateFormat.format("yyyy/MM/dd kk:mm", fileIndex.lastModified()).toString();
                String permission = FileService.getFileContent(this, fileName);
                if (permission == null) {
                    this.upDirPath = StartFilePath.substring(0, StartFilePath.lastIndexOf("/"));
                    if (!new File("/data/data/com.itfunz.itfunzsupertools/files/tmpFileDetailEn.cfg").exists()) {
                        RootScript.runRootCommand("cd \"" + this.upDirPath + "\"" + "\nls -a -l -R > /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetailEn.cfg");
                    }
                    permission = FileService.getFileContentEn(this, fileName);
                }
                this.permissions[i] = permission;
                String FileSize = "\t大小:" + String.valueOf(this.files[i].length()) + "Byte(s)";
                if (FileSize.trim().equals("大小:0Byte(s)")) {
                    hashMap.put("fileAbility", String.valueOf(FileTime) + " " + permission);
                } else {
                    hashMap.put("fileAbility", String.valueOf(FileTime) + " " + permission + FileSize);
                }
                if (!this.mutiSelect) {
                    hashMap.put("CheckId", Integer.valueOf((int) R.drawable.icon_null));
                } else if (this.mutiSelectAll) {
                    hashMap.put("CheckId", Integer.valueOf((int) R.drawable.btn_check_buttonless_on));
                } else {
                    hashMap.put("CheckId", Integer.valueOf((int) R.drawable.btn_check_buttonless_off));
                }
                this.data.add(hashMap);
            } else if (!fileName.substring(0, 1).equals(".")) {
                HashMap hashMap2 = new HashMap();
                if (fileIndex.isDirectory()) {
                    hashMap2.put("fileIcon", Integer.valueOf((int) R.drawable.icon_folder));
                    this.ImageViewId[i] = R.drawable.icon_folder;
                } else {
                    hashMap2.put("fileIcon", Integer.valueOf(IconFormat.getIconFormat(fileName)));
                    this.ImageViewId[i] = IconFormat.getIconFormat(fileName);
                }
                hashMap2.put("fileName", fileName);
                String FileTime2 = DateFormat.format("yyyy/MM/dd kk:mm", fileIndex.lastModified()).toString();
                String permission2 = FileService.getFileContent(this, fileName);
                if (permission2 == null) {
                    this.upDirPath = StartFilePath.substring(0, StartFilePath.lastIndexOf("/"));
                    if (!new File("/data/data/com.itfunz.itfunzsupertools/files/tmpFileDetailEn.cfg").exists()) {
                        RootScript.runRootCommand("cd \"" + this.upDirPath + "\"" + "\nls -a -l -R > /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetailEn.cfg");
                    }
                    permission2 = FileService.getFileContentEn(this, fileName);
                }
                this.permissions[i] = permission2;
                String FileSize2 = "\t大小:" + String.valueOf(this.files[i].length()) + "Byte(s)";
                if (FileSize2.trim().equals("大小:0Byte(s)")) {
                    hashMap2.put("fileAbility", String.valueOf(FileTime2) + " " + permission2);
                } else {
                    hashMap2.put("fileAbility", String.valueOf(FileTime2) + " " + permission2 + FileSize2);
                }
                if (!this.mutiSelect) {
                    hashMap2.put("CheckId", Integer.valueOf((int) R.drawable.icon_null));
                } else if (this.mutiSelectAll) {
                    hashMap2.put("CheckId", Integer.valueOf((int) R.drawable.btn_check_buttonless_on));
                } else {
                    hashMap2.put("CheckId", Integer.valueOf((int) R.drawable.btn_check_buttonless_off));
                }
                this.data.add(hashMap2);
            }
        }
        this.mutiSelectAll = false;
        if (StartFilePath.indexOf(this.upDirPath) == -1) {
            RootScript.runRootCommand("rm /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetailEn.cfg");
        }
    }

    /* access modifiers changed from: private */
    public ListView createFileDirLayout(String StartFilePath) {
        SetFileData(StartFilePath);
        this.fileStrPath = StartFilePath;
        this.lvLayout = new ListView(this);
        this.lvLayout.setLayoutParams(layoutParams);
        this.lvLayout.setVerticalScrollBarEnabled(true);
        this.adapter = new SimpleAdapter(this, this.data, R.layout.filedetail, new String[]{"fileIcon", "fileName", "fileAbility", "CheckId"}, new int[]{R.id.FileIcon, R.id.FileName, R.id.FileAbility, R.id.CheckId});
        this.lvLayout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int itemId, long arg3) {
                File fileIndex = ItfunzFileDialog.this.files[itemId];
                String filePath = fileIndex.getPath();
                String fileName = fileIndex.getName();
                if (ItfunzFileDialog.this.mutiSelect) {
                    View view = ItfunzFileDialog.this.adapter.getView(itemId, ItfunzFileDialog.this.lvLayout.getChildAt(itemId), ItfunzFileDialog.this.lvLayout);
                    new TextView(ItfunzFileDialog.this);
                    String FileName = ((TextView) view.findViewById(R.id.FileName)).getText().toString();
                    new TextView(ItfunzFileDialog.this);
                    String FileAbility = ((TextView) view.findViewById(R.id.FileAbility)).getText().toString();
                    HashMap hashMap = new HashMap();
                    hashMap.put("fileIcon", Integer.valueOf(ItfunzFileDialog.this.ImageViewId[itemId]));
                    hashMap.put("fileName", FileName);
                    hashMap.put("fileAbility", FileAbility);
                    switch (ItfunzFileDialog.this.state[itemId]) {
                        case 0:
                            ItfunzFileDialog.this.state[itemId] = 1;
                            hashMap.put("CheckId", Integer.valueOf((int) R.drawable.btn_check_buttonless_on));
                            break;
                        case 1:
                            ItfunzFileDialog.this.state[itemId] = 0;
                            hashMap.put("CheckId", Integer.valueOf((int) R.drawable.btn_check_buttonless_off));
                            break;
                    }
                    ItfunzFileDialog.this.data.set(itemId, hashMap);
                    ItfunzFileDialog.this.adapter.notifyDataSetChanged();
                } else if (fileIndex.isDirectory()) {
                    int i = 0;
                    while (true) {
                        if (i > 100) {
                            break;
                        } else if (ItfunzFileDialog.this.selectDirPath[i] == null) {
                            ItfunzFileDialog.this.selectDirId[i] = itemId;
                            ItfunzFileDialog.this.selectDirPath[i] = filePath;
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (fileIndex.listFiles() != null) {
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(filePath));
                        ItfunzFileDialog.this.setTitle(filePath);
                    } else if (ItfunzFileDialog.this.settings.getBoolean("readandwrite", false)) {
                        String permissionResult = FileService.getDefaultPermissions(FileService.getFileContent(ItfunzFileDialog.this, fileName));
                        int i2 = 0;
                        while (true) {
                            if (i2 >= 100) {
                                break;
                            } else if (ItfunzFileDialog.this.tempPath[i2] == null) {
                                ItfunzFileDialog.this.tempPath[i2] = filePath;
                                ItfunzFileDialog.this.tempPermissions[i2] = permissionResult;
                                break;
                            } else {
                                i2++;
                            }
                        }
                        FileService.changeFileRootPermissions(filePath);
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(filePath));
                        ItfunzFileDialog.this.setTitle(filePath);
                    } else {
                        Toast.makeText(ItfunzFileDialog.this, "此处为只读目录，您没有权限访问，请开启读写模式", 0).show();
                    }
                } else {
                    String format = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();
                    if (format.equals("amt")) {
                        ItfunzFileDialog.this.themePath = filePath;
                        ItfunzFileDialog.this.themeName = fileName;
                        if (new File("/sdcard/motoskin").exists()) {
                            ItfunzFileDialog.this.createInstallThemeDialog().show();
                        } else {
                            Toast.makeText(ItfunzFileDialog.this, "您还没有初始化主题管理器，请先打开主题管理器", 0).show();
                        }
                    } else if (format.equals("sh")) {
                        ItfunzFileDialog.this.CreateRunScriptDialog(filePath).show();
                    } else if (fileName.equals("bootanimation.zip")) {
                        ItfunzFileDialog.this.CreatePowerLogoDialog(filePath).show();
                    } else {
                        Cursor cur = ItfunzFileDialog.this.getFileMimeTypeBase();
                        int count = cur.getCount();
                        boolean result = false;
                        int i3 = 0;
                        while (true) {
                            if (i3 >= count) {
                                break;
                            }
                            cur.moveToPosition(i3);
                            String FileFORMAT = cur.getString(cur.getColumnIndexOrThrow(ItfunzFileDialog.FILE_FORMAT)).toString();
                            String MiMeType = cur.getString(cur.getColumnIndexOrThrow(ItfunzFileDialog.FILE_MIME_TYPE)).toString();
                            String FileSUBTYPE = cur.getString(cur.getColumnIndexOrThrow(ItfunzFileDialog.FILE_SUB_TYPE)).toString();
                            if (format.equals(FileFORMAT)) {
                                Intent intent = new Intent("android.intent.action.VIEW");
                                intent.setDataAndType(Uri.fromFile(new File(filePath)), String.valueOf(MiMeType) + "/" + FileSUBTYPE);
                                ItfunzFileDialog.this.startActivity(intent);
                                result = true;
                                break;
                            }
                            i3++;
                        }
                        if (!result) {
                            Toast.makeText(ItfunzFileDialog.this, "暂时无法找到打开此格式的程序", 0).show();
                        }
                        cur.close();
                    }
                }
            }
        });
        this.lvLayout.setAdapter((ListAdapter) this.adapter);
        registerForContextMenu(this.lvLayout);
        return this.lvLayout;
    }

    public ScrollView createNullView(String StartFilePath) {
        this.fileStrPath = StartFilePath;
        ScrollView svLayout = new ScrollView(this);
        svLayout.setLayoutParams(layoutParams);
        svLayout.setVerticalScrollBarEnabled(true);
        LinearLayout llayout = new LinearLayout(this);
        llayout.setLayoutParams(layoutParams);
        llayout.setOrientation(1);
        svLayout.addView(llayout);
        return svLayout;
    }

    public ListView createBookMarkView() {
        this.fileStrPath = String.valueOf(this.fileStrPath) + "/BookMark";
        this.data = new ArrayList<>();
        Map<String, Object> item = new HashMap<>();
        item.put("FolderIcon", Integer.valueOf((int) R.drawable.icon_folder));
        item.put("FolderName", "根目录");
        this.data.add(item);
        Map<String, Object> item2 = new HashMap<>();
        item2.put("FolderIcon", Integer.valueOf((int) R.drawable.icon_folder));
        item2.put("FolderName", "内存卡");
        this.data.add(item2);
        Map<String, Object> item3 = new HashMap<>();
        item3.put("FolderIcon", Integer.valueOf((int) R.drawable.icon_folder));
        item3.put("FolderName", "系统内置程序目录");
        this.data.add(item3);
        Map<String, Object> item4 = new HashMap<>();
        item4.put("FolderIcon", Integer.valueOf((int) R.drawable.icon_folder));
        item4.put("FolderName", "用户安装程序目录");
        this.data.add(item4);
        ListView lvLayout2 = new ListView(this);
        lvLayout2.setLayoutParams(layoutParams);
        lvLayout2.setVerticalScrollBarEnabled(true);
        setTitle("书签");
        lvLayout2.setAdapter((ListAdapter) new SimpleAdapter(this, this.data, R.layout.folder, new String[]{"FolderIcon", "FolderName"}, new int[]{R.id.FolderIcon, R.id.FolderName}));
        lvLayout2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                switch (arg2) {
                    case 0:
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout("/"));
                        ItfunzFileDialog.this.setTitle("/");
                        return;
                    case 1:
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout("/sdcard"));
                        ItfunzFileDialog.this.setTitle("/sdcard");
                        return;
                    case 2:
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout("/system/app"));
                        ItfunzFileDialog.this.setTitle("/system/app");
                        return;
                    case 3:
                        if (ItfunzFileDialog.this.readandwrite) {
                            RootScript.runRootCommand("chmod 777 /data/");
                            ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout("/data/app"));
                            ItfunzFileDialog.this.setTitle("/data/app");
                            for (int i = 0; i < 100; i++) {
                                if (ItfunzFileDialog.this.tempPath[i] == null) {
                                    ItfunzFileDialog.this.tempPath[i] = "/data";
                                    ItfunzFileDialog.this.tempPermissions[i] = "771";
                                    return;
                                }
                            }
                            return;
                        }
                        Toast.makeText(ItfunzFileDialog.this, "此处为只读目录，您没有权限访问，请开启读写模式", 0).show();
                        return;
                    default:
                        return;
                }
            }
        });
        return lvLayout2;
    }

    public Dialog createInstallThemeDialog() {
        return new AlertDialog.Builder(this).setTitle("主题管理器").setMessage("您选择的是一个主题文件，是否要安装？（主题放在中文目录或含有空格目录会导致安装失败。）").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (new File("/sdcard/motoskin/" + ItfunzFileDialog.this.themeName.substring(0, ItfunzFileDialog.this.themeName.length() - 4)).exists()) {
                    Toast.makeText(ItfunzFileDialog.this, "您已经安装过该主题，不能重复安装", 1).show();
                    return;
                }
                final ProgressDialog pdialog = ProgressDialog.show(ItfunzFileDialog.this, "主题管理器", "安装中，请稍候...");
                final Handler handler = new Handler();
                new Thread() {
                    public void run() {
                        super.run();
                        RootScript.runRootCommand("busybox unzip " + ItfunzFileDialog.this.themePath + " -d /sdcard/motoskin/");
                        Handler handler = handler;
                        final ProgressDialog progressDialog = pdialog;
                        handler.post(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                                ItfunzFileDialog.this.startActivity(new Intent(ItfunzFileDialog.this, ThemeManager.class));
                            }
                        });
                    }
                }.start();
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create();
    }

    public Dialog createFolderDialog() {
        LinearLayout llayout = new LinearLayout(this);
        llayout.setOrientation(1);
        TextView tvTitle = new TextView(this);
        tvTitle.setText("名称:");
        final EditText etName = new EditText(this);
        llayout.addView(tvTitle);
        llayout.addView(etName);
        return new AlertDialog.Builder(this).setTitle("新建文件夹").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String FolderName = etName.getText().toString().trim();
                File file = new File(String.valueOf(ItfunzFileDialog.this.fileStrPath) + "/" + FolderName);
                if (file.exists()) {
                    Toast.makeText(ItfunzFileDialog.this, "文件夹已存在", 0).show();
                    return;
                }
                if (ItfunzFileDialog.this.readandwrite) {
                    FileService.changeFileRootPermissions(ItfunzFileDialog.this.fileStrPath);
                }
                file.mkdir();
                if (!file.exists()) {
                    Toast.makeText(ItfunzFileDialog.this, "该目录为只读目录，您没有权限在此创建文件夹", 0).show();
                    return;
                }
                ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                int count = ItfunzFileDialog.this.files.length;
                for (int i = 0; i < count; i++) {
                    if (ItfunzFileDialog.this.files[i].getName().equals(FolderName)) {
                        ItfunzFileDialog.this.stateId = i;
                        ItfunzFileDialog.this.lvLayout.setSelection(i);
                    }
                }
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).setView(llayout).create();
    }

    public Dialog CreateNoMediaFolder() {
        return new AlertDialog.Builder(this).setTitle("禁止被媒体检索").setMessage("单击确定，此目录的媒体文件将不会被内置播放器所检索到，重启生效，如需还原请删除本目录.nomedia文件夹既可。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                File file = new File(String.valueOf(ItfunzFileDialog.this.fileStrPath) + "/.nomedia");
                if (file.exists()) {
                    Toast.makeText(ItfunzFileDialog.this, "本文件夹已经被禁止检索了", 0).show();
                } else {
                    if (ItfunzFileDialog.this.readandwrite) {
                        FileService.changeFileRootPermissions(ItfunzFileDialog.this.fileStrPath);
                    }
                    file.mkdir();
                    if (!file.exists()) {
                        Toast.makeText(ItfunzFileDialog.this, "该目录为只读目录，您没有权限执行此操作", 0).show();
                    }
                }
                ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create();
    }

    public Dialog CreateToolsDialog() {
        return new AlertDialog.Builder(this).setTitle("工具").setItems((int) R.array.Tools, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        ItfunzFileDialog.this.CreateNoMediaFolder().show();
                        return;
                    case 1:
                        ItfunzFileDialog.this.startActivity(new Intent(ItfunzFileDialog.this, DiskSpace.class));
                        return;
                    case 2:
                        try {
                            int count = ItfunzFileDialog.this.state.length;
                            for (int index = 0; index < count; index++) {
                                if (ItfunzFileDialog.this.state[index] == 1) {
                                    String path = ItfunzFileDialog.this.files[index].getPath();
                                    if (path.toLowerCase().indexOf(".apk") != -1) {
                                        Intent intent = new Intent("android.intent.action.VIEW");
                                        intent.setDataAndType(Uri.fromFile(new File(path)), "application/vnd.android.package-archive");
                                        ItfunzFileDialog.this.startActivity(intent);
                                    } else {
                                        ItfunzFileDialog.this.mutiSelect = false;
                                        ItfunzFileDialog.this.mutiSelectAll = false;
                                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                                        return;
                                    }
                                }
                            }
                            ItfunzFileDialog.this.mutiSelect = false;
                            ItfunzFileDialog.this.mutiSelectAll = false;
                            ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                            return;
                        } catch (Exception e) {
                            Toast.makeText(ItfunzFileDialog.this, "您未选中任何APK程序", 0).show();
                            return;
                        }
                    default:
                        return;
                }
            }
        }).create();
    }

    public Dialog CreateOpenAsDialog(final String path) {
        return new AlertDialog.Builder(this).setTitle("打开方式").setItems((int) R.array.openAs, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent("android.intent.action.VIEW");
                switch (which) {
                    case 0:
                        intent.setDataAndType(Uri.fromFile(new File(path)), "audio/*");
                        ItfunzFileDialog.this.startActivity(intent);
                        return;
                    case 1:
                        intent.setDataAndType(Uri.fromFile(new File(path)), "video/*");
                        ItfunzFileDialog.this.startActivity(intent);
                        return;
                    case 2:
                        intent.setDataAndType(Uri.fromFile(new File(path)), "image/*");
                        ItfunzFileDialog.this.startActivity(intent);
                        return;
                    case 3:
                        intent.setDataAndType(Uri.fromFile(new File(path)), "text/*");
                        ItfunzFileDialog.this.startActivity(intent);
                        return;
                    case 4:
                        intent.setDataAndType(Uri.fromFile(new File(path)), "application/*");
                        ItfunzFileDialog.this.startActivity(intent);
                        return;
                    default:
                        return;
                }
            }
        }).create();
    }

    public Dialog CreateRunScriptDialog(final String path) {
        return new AlertDialog.Builder(this).setTitle("脚本文件").setMessage("这是一个可执行脚本文件，要执行哪个操作?").setPositiveButton("查看", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String apkPath = path;
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setDataAndType(Uri.fromFile(new File(apkPath)), "text/plain");
                ItfunzFileDialog.this.startActivity(intent);
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).setNeutralButton("执行", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RootScript.runRootCommand("/system/bin/sh " + path);
            }
        }).create();
    }

    public Dialog CreatePowerLogoDialog(final String path) {
        return new AlertDialog.Builder(this).setTitle("文件管理器").setMessage("这是一个开机动画文件，是否更换？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                final ProgressDialog pdialog = ProgressDialog.show(ItfunzFileDialog.this, "文件管理器", "更换中，请稍候...");
                final Handler handler = new Handler();
                final String str = path;
                new Thread() {
                    public void run() {
                        super.run();
                        FileService.changeRootPermissions();
                        FileService.changeFileRootPermissions("/data");
                        File fileInLocal = new File("/data/local/bootanimation.zip");
                        if (fileInLocal.exists()) {
                            fileInLocal.delete();
                        }
                        FileService.changeFileRootPermissions("/system/media");
                        try {
                            FileService.copyFile(str, "/system/media/bootanimation.zip");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        RootScript.runRootCommand("chmod 0644 /system/media/bootanimation.zip");
                        RootScript.runRootCommand("chmod 771 /data");
                        FileService.changeSimpleFilePermissions();
                        Handler handler = handler;
                        final ProgressDialog progressDialog = pdialog;
                        handler.post(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                                Toast.makeText(ItfunzFileDialog.this, "更换成功，重启后生效", 0).show();
                            }
                        });
                    }
                }.start();
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create();
    }

    public Dialog CreateDeleteFileDialog(String filename, final String filepath) {
        return new AlertDialog.Builder(this).setTitle("文件管理器").setMessage("您确认要删除" + filename + "?").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (ItfunzFileDialog.this.readandwrite) {
                    FileService.changeFileRootPermissions(filepath);
                }
                final ProgressDialog pdialog = ProgressDialog.show(ItfunzFileDialog.this, "正在删除", "请稍候...");
                final Handler handler = new Handler();
                final String str = filepath;
                new Thread() {
                    public void run() {
                        super.run();
                        ItfunzFileDialog.this.resultfordelete = FileService.delFolder(str);
                        Handler handler = handler;
                        final ProgressDialog progressDialog = pdialog;
                        handler.post(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                                if (ItfunzFileDialog.this.resultfordelete) {
                                    Toast.makeText(ItfunzFileDialog.this, "删除成功", 0).show();
                                } else {
                                    Toast.makeText(ItfunzFileDialog.this, "此处为只读目录，您没有权限删除", 0).show();
                                }
                                ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                                if (ItfunzFileDialog.this.stateId != -1) {
                                    ItfunzFileDialog.this.lvLayout.setSelection(ItfunzFileDialog.this.stateId);
                                    ItfunzFileDialog.this.stateId = -1;
                                }
                            }
                        });
                    }
                }.start();
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create();
    }

    public Dialog CreateDeleteAllFileDialog(final String[] filepath) {
        return new AlertDialog.Builder(this).setTitle("文件管理器").setMessage("您确认要删除这些文件?").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (ItfunzFileDialog.this.readandwrite) {
                    for (String changeFileRootPermissions : filepath) {
                        FileService.changeFileRootPermissions(changeFileRootPermissions);
                    }
                }
                final ProgressDialog pdialog = ProgressDialog.show(ItfunzFileDialog.this, "正在删除", "请稍候...");
                final Handler handler = new Handler();
                final String[] strArr = filepath;
                new Thread() {
                    public void run() {
                        super.run();
                        for (int i = 0; i < count; i++) {
                            ItfunzFileDialog.this.resultfordelete = FileService.delFolder(strArr[i]);
                        }
                        Handler handler = handler;
                        final ProgressDialog progressDialog = pdialog;
                        handler.post(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                                Toast.makeText(ItfunzFileDialog.this, "删除成功", 0).show();
                                ItfunzFileDialog.this.mutiSelect = false;
                                ItfunzFileDialog.this.mutiSelectAll = false;
                                ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                            }
                        });
                    }
                }.start();
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).create();
    }

    public Dialog CreatePermissionsDialog(String fileName, String permissions2, String path) {
        LinearLayout layout = (LinearLayout) ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.permissions, (ViewGroup) null);
        char userread = permissions2.charAt(0);
        char userwrite = permissions2.charAt(1);
        char userexec = permissions2.charAt(2);
        char groupread = permissions2.charAt(3);
        char groupwrite = permissions2.charAt(4);
        char groupexec = permissions2.charAt(5);
        char otherread = permissions2.charAt(6);
        char otherwrite = permissions2.charAt(7);
        char otherexec = permissions2.charAt(DATEBASE_VERSION);
        this.cbuserread = new CheckBox(this);
        this.cbuserwrite = new CheckBox(this);
        this.cbuserexec = new CheckBox(this);
        this.cbgroupread = new CheckBox(this);
        this.cbgroupwrite = new CheckBox(this);
        this.cbgroupexec = new CheckBox(this);
        this.cbotherread = new CheckBox(this);
        this.cbotherwrite = new CheckBox(this);
        this.cbotherexec = new CheckBox(this);
        this.cbuserread = (CheckBox) layout.findViewById(R.id.cbuserread);
        this.cbuserwrite = (CheckBox) layout.findViewById(R.id.cbuserwrite);
        this.cbuserexec = (CheckBox) layout.findViewById(R.id.cbuserexec);
        this.cbgroupread = (CheckBox) layout.findViewById(R.id.cbgroupread);
        this.cbgroupwrite = (CheckBox) layout.findViewById(R.id.cbgroupwrite);
        this.cbgroupexec = (CheckBox) layout.findViewById(R.id.cbgroupexec);
        this.cbotherread = (CheckBox) layout.findViewById(R.id.cbotherread);
        this.cbotherwrite = (CheckBox) layout.findViewById(R.id.cbotherwrite);
        this.cbotherexec = (CheckBox) layout.findViewById(R.id.cbotherexec);
        if (userread == 'r') {
            this.cbuserread.setChecked(true);
        }
        if (userwrite == 'w') {
            this.cbuserwrite.setChecked(true);
        }
        if (userexec == 'x') {
            this.cbuserexec.setChecked(true);
        }
        if (groupread == 'r') {
            this.cbgroupread.setChecked(true);
        }
        if (groupwrite == 'w') {
            this.cbgroupwrite.setChecked(true);
        }
        if (groupexec == 'x') {
            this.cbgroupexec.setChecked(true);
        }
        if (otherread == 'r') {
            this.cbotherread.setChecked(true);
        }
        if (otherwrite == 'w') {
            this.cbotherwrite.setChecked(true);
        }
        if (otherexec == 'x') {
            this.cbotherexec.setChecked(true);
        }
        final String str = permissions2;
        final String str2 = path;
        return new AlertDialog.Builder(this).setView(layout).setTitle(fileName).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int count = str.length();
                char[] cArr = new char[count];
                int[] num = new int[count];
                char[] c = FileService.geteveryPermissions(str);
                if (ItfunzFileDialog.this.cbuserread.isChecked()) {
                    c[0] = 'r';
                    num[0] = 4;
                } else {
                    c[0] = '-';
                    num[0] = 0;
                }
                if (ItfunzFileDialog.this.cbuserwrite.isChecked()) {
                    c[1] = 'w';
                    num[1] = 2;
                } else {
                    c[1] = '-';
                    num[1] = 0;
                }
                if (ItfunzFileDialog.this.cbuserexec.isChecked()) {
                    c[2] = 'x';
                    num[2] = 1;
                } else {
                    c[2] = '-';
                    num[2] = 0;
                }
                if (ItfunzFileDialog.this.cbgroupread.isChecked()) {
                    c[3] = 'r';
                    num[3] = 4;
                } else {
                    c[3] = '-';
                    num[3] = 0;
                }
                if (ItfunzFileDialog.this.cbgroupwrite.isChecked()) {
                    c[4] = 'w';
                    num[4] = 2;
                } else {
                    c[4] = '-';
                    num[4] = 0;
                }
                if (ItfunzFileDialog.this.cbgroupexec.isChecked()) {
                    c[5] = 'x';
                    num[5] = 1;
                } else {
                    c[5] = '-';
                    num[5] = 0;
                }
                if (ItfunzFileDialog.this.cbotherread.isChecked()) {
                    c[6] = 'r';
                    num[6] = 4;
                } else {
                    c[6] = '-';
                    num[6] = 0;
                }
                if (ItfunzFileDialog.this.cbotherwrite.isChecked()) {
                    c[7] = 'w';
                    num[7] = 2;
                } else {
                    c[7] = '-';
                    num[7] = 0;
                }
                if (ItfunzFileDialog.this.cbotherexec.isChecked()) {
                    c[ItfunzFileDialog.DATEBASE_VERSION] = 'x';
                    num[ItfunzFileDialog.DATEBASE_VERSION] = 1;
                } else if (c[ItfunzFileDialog.DATEBASE_VERSION] == 't') {
                    c[ItfunzFileDialog.DATEBASE_VERSION] = 't';
                    num[ItfunzFileDialog.DATEBASE_VERSION] = 0;
                } else {
                    c[ItfunzFileDialog.DATEBASE_VERSION] = '-';
                    num[ItfunzFileDialog.DATEBASE_VERSION] = 0;
                }
                RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system\nchmod " + String.valueOf(Integer.valueOf(num[0]).intValue() + Integer.valueOf(num[1]).intValue() + Integer.valueOf(num[2]).intValue()) + String.valueOf(Integer.valueOf(num[3]).intValue() + Integer.valueOf(num[4]).intValue() + Integer.valueOf(num[5]).intValue()) + String.valueOf(Integer.valueOf(num[6]).intValue() + Integer.valueOf(num[7]).intValue() + Integer.valueOf(num[ItfunzFileDialog.DATEBASE_VERSION]).intValue()) + " " + "\"" + str2 + "\"");
                ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
            }
        }).setNeutralButton("取消", (DialogInterface.OnClickListener) null).create();
    }

    public boolean onContextItemSelected(MenuItem item) {
        int id = (int) ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id;
        String filepath = this.files[id].getPath();
        String filename = this.files[id].getName();
        switch (item.getItemId()) {
            case 1:
                this.Clipboard = new String[1000];
                this.Clipboard[0] = filepath;
                this.ClipboadeMode = "copy";
                break;
            case 2:
                this.Clipboard = new String[1000];
                this.Clipboard[0] = filepath;
                this.ClipboadeMode = "cut";
                break;
            case 3:
                createRenameDialog(new File(filepath), id).show();
                break;
            case 4:
                CreateDeleteFileDialog(filename, filepath).show();
                break;
            case 5:
                CreatePermissionsDialog(filename, this.permissions[id], filepath).show();
                break;
            case 6:
                createMediaSelectDialog(filepath, filename).show();
                break;
            case 7:
                createChangeFontDialog(filepath).show();
                break;
            case DATEBASE_VERSION /*8*/:
                CreateOpenAsDialog(filepath).show();
                break;
        }
        return super.onContextItemSelected(item);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        int id = (int) ((AdapterView.AdapterContextMenuInfo) menuInfo).id;
        this.stateId = id;
        menu.setHeaderTitle("可选操作");
        menu.add(0, 1, 1, "复制");
        menu.add(0, 2, 2, "剪切");
        menu.add(0, 3, 3, "重命名");
        menu.add(0, 4, 4, "删除");
        menu.add(0, 5, 5, "权限");
        String fileName = this.files[id].getName();
        String fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
        if (fileType.equals("mp3") || fileType.equals("wma") || fileType.equals("mid") || fileType.equals("m4a") || fileType.equals("aac")) {
            menu.add(0, 6, 6, "设为铃声");
        }
        if (fileType.equals("ttf")) {
            menu.add(0, 7, 7, "更换字体");
        }
        if (!new File(this.files[id].getPath()).isDirectory()) {
            menu.add(0, (int) DATEBASE_VERSION, (int) DATEBASE_VERSION, "打开方式");
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.addSubMenu(0, 1, 1, "新建文件夹").setIcon((int) R.drawable.ic_menu_archive);
        menu.addSubMenu(0, 2, 2, "编辑菜单").setIcon((int) R.drawable.ic_menu_edit);
        if (this.settings.getBoolean("readandwrite", false)) {
            menu.addSubMenu(0, 3, 3, "只读模式").setIcon((int) R.drawable.ic_menu_read_only);
        } else {
            menu.addSubMenu(0, 3, 3, "读写模式").setIcon((int) R.drawable.ic_menu_read_write);
        }
        menu.addSubMenu(0, 4, 4, "书签").setIcon((int) R.drawable.ic_menu_bookmark);
        menu.addSubMenu(0, 5, 5, "工具").setIcon((int) R.drawable.ic_menu_tools);
        menu.addSubMenu(0, 6, 6, "搜索").setIcon((int) R.drawable.ic_menu_search);
        menu.addSubMenu(0, 7, 7, "首选项");
        menu.addSubMenu(0, (int) DATEBASE_VERSION, (int) DATEBASE_VERSION, "退出文件管理器");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                createFolderDialog().show();
                break;
            case 2:
                CreateEditDialog().show();
                break;
            case 3:
                if (!item.getTitle().equals("只读模式")) {
                    FileService.changeRootPermissions();
                    item.setIcon((int) R.drawable.ic_menu_read_only).setTitle("只读模式");
                    this.settings.edit().putBoolean("readandwrite", true).commit();
                    this.readandwrite = this.settings.getBoolean("readandwrite", false);
                    break;
                } else {
                    FileService.changeSimpleFilePermissions();
                    item.setIcon((int) R.drawable.ic_menu_read_write).setTitle("读写模式");
                    this.settings.edit().putBoolean("readandwrite", false).commit();
                    this.readandwrite = this.settings.getBoolean("readandwrite", false);
                    break;
                }
            case 4:
                setContentView(createBookMarkView());
                break;
            case 5:
                CreateToolsDialog().show();
                break;
            case 6:
                createSearchFileDialog().show();
                break;
            case 7:
                startActivity(new Intent(this, ItfunzFileDialogConfig.class));
                break;
            case DATEBASE_VERSION /*8*/:
                System.exit(0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void finish() {
        super.finish();
        RootScript.runRootCommand("rm /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetailEn.cfg");
        RootScript.runRootCommand("rm /data/data/com.itfunz.itfunzsupertools/files/tmpFileDetail.cfg");
    }

    public void FileCopyMethod(final ProgressDialog pdialog) {
        final Handler handler = new Handler();
        new Thread() {
            public void run() {
                super.run();
                int i = 0;
                while (i < 999 && ItfunzFileDialog.this.Clipboard[i] != null) {
                    File file = new File(ItfunzFileDialog.this.Clipboard[i]);
                    String fileName = file.getName();
                    String filePath = file.getParent();
                    if (file.isDirectory()) {
                        try {
                            String ActionPath = String.valueOf(ItfunzFileDialog.this.fileStrPath) + "/" + fileName;
                            if (ItfunzFileDialog.this.readandwrite) {
                                FileService.changeFileRootPermissions(filePath);
                                FileService.changeFileRootPermissions(ItfunzFileDialog.this.fileStrPath);
                                FileService.copyFolder(ItfunzFileDialog.this.Clipboard[i], ActionPath);
                            } else {
                                FileService.copyFolder(ItfunzFileDialog.this.Clipboard[i], ActionPath);
                            }
                            ItfunzFileDialog.this.resultFile = ActionPath;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            String ActionPath2 = String.valueOf(ItfunzFileDialog.this.fileStrPath) + "/" + fileName;
                            if (ItfunzFileDialog.this.readandwrite) {
                                FileService.changeFileRootPermissions(filePath);
                                FileService.changeFileRootPermissions(ItfunzFileDialog.this.fileStrPath);
                                FileService.copyFile(ItfunzFileDialog.this.Clipboard[i], ActionPath2);
                            } else {
                                FileService.copyFile(ItfunzFileDialog.this.Clipboard[i], ActionPath2);
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                    i++;
                }
                Handler handler = handler;
                final ProgressDialog progressDialog = pdialog;
                handler.post(new Runnable() {
                    public void run() {
                        progressDialog.dismiss();
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                    }
                });
            }
        }.start();
    }

    public void FileCutMethod(final ProgressDialog pdialog) {
        final Handler handler = new Handler();
        new Thread() {
            public void run() {
                super.run();
                int i = 0;
                while (i < 999 && ItfunzFileDialog.this.Clipboard[i] != null) {
                    File file = new File(ItfunzFileDialog.this.Clipboard[i]);
                    String fileName = file.getName();
                    String filePath = file.getParent();
                    if (file.isDirectory()) {
                        try {
                            String ActionPath = String.valueOf(ItfunzFileDialog.this.fileStrPath) + "/" + fileName;
                            if (ItfunzFileDialog.this.readandwrite) {
                                FileService.changeFileRootPermissions(filePath);
                                FileService.changeFileRootPermissions(ItfunzFileDialog.this.fileStrPath);
                                FileService.copyFolder(ItfunzFileDialog.this.Clipboard[i], ActionPath);
                                FileService.delFolder(ItfunzFileDialog.this.Clipboard[i]);
                            } else {
                                FileService.copyFolder(ItfunzFileDialog.this.Clipboard[i], ActionPath);
                                FileService.delFolder(ItfunzFileDialog.this.Clipboard[i]);
                            }
                            ItfunzFileDialog.this.resultFile = ActionPath;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            String ActionPath2 = String.valueOf(ItfunzFileDialog.this.fileStrPath) + "/" + fileName;
                            if (ItfunzFileDialog.this.readandwrite) {
                                FileService.changeFileRootPermissions(filePath);
                                FileService.changeFileRootPermissions(ItfunzFileDialog.this.fileStrPath);
                                FileService.copyFile(ItfunzFileDialog.this.Clipboard[i], ActionPath2);
                                FileService.delFolder(ItfunzFileDialog.this.Clipboard[i]);
                            } else {
                                FileService.copyFile(ItfunzFileDialog.this.Clipboard[i], ActionPath2);
                                FileService.delFolder(ItfunzFileDialog.this.Clipboard[i]);
                            }
                            ItfunzFileDialog.this.resultFile = ActionPath2;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                    i++;
                }
                Handler handler = handler;
                final ProgressDialog progressDialog = pdialog;
                handler.post(new Runnable() {
                    public void run() {
                        progressDialog.dismiss();
                        if (new File(ItfunzFileDialog.this.resultFile).exists()) {
                            Toast.makeText(ItfunzFileDialog.this, "操作已成功完成", 0).show();
                        } else {
                            Toast.makeText(ItfunzFileDialog.this, "目标目录可能为只读，请开启读写模式后再试", 0).show();
                        }
                        ItfunzFileDialog.this.Clipboard = new String[1000];
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                    }
                });
            }
        }.start();
    }

    public Dialog createRenameDialog(final File oldFile, int item) {
        String oldName = oldFile.getName();
        LinearLayout llayout = new LinearLayout(this);
        llayout.setOrientation(1);
        TextView tvTitle = new TextView(this);
        tvTitle.setText("名称:");
        final EditText etName = new EditText(this);
        llayout.addView(tvTitle);
        llayout.addView(etName);
        etName.setText(oldName);
        return new AlertDialog.Builder(this).setTitle("重命名").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                oldFile.renameTo(new File(String.valueOf(oldFile.getParent()) + "/" + etName.getText().toString().trim()));
                ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).setView(llayout).create();
    }

    public Dialog createSearchFileDialog() {
        LinearLayout llayout = new LinearLayout(this);
        llayout.setOrientation(1);
        TextView tvTitle = new TextView(this);
        tvTitle.setText("当前目录:" + this.fileStrPath);
        final EditText etName = new EditText(this);
        llayout.addView(tvTitle);
        llayout.addView(etName);
        return new AlertDialog.Builder(this).setTitle("文件搜索").setPositiveButton("搜索", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                final String FolderName = etName.getText().toString().trim();
                final Handler handler = new Handler();
                new AlertDialog.Builder(ItfunzFileDialog.this).setTitle("文件搜索").setMessage("为了不影响您的正常工作，我们将把搜索放置到后台运行，搜索完成后会自动显示。").setPositiveButton("确定", (DialogInterface.OnClickListener) null).create().show();
                new Thread() {
                    public void run() {
                        super.run();
                        ItfunzFileDialog.this.data = FileService.searchFile(FolderName, ItfunzFileDialog.this.fileStrPath);
                        handler.post(new Runnable() {
                            public void run() {
                                new AlertDialog.Builder(ItfunzFileDialog.this).setTitle("文件搜索").setMessage("搜索完毕。").setPositiveButton("确定", (DialogInterface.OnClickListener) null).create().show();
                                ItfunzFileDialog.this.setTitle("搜索结果");
                                ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createSearchFileDirLayout());
                            }
                        });
                    }
                }.start();
            }
        }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).setView(llayout).create();
    }

    public Dialog CreateEditDialog() {
        return new AlertDialog.Builder(this).setTitle("编辑菜单").setItems((int) R.array.Edit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int count = ItfunzFileDialog.this.lvLayout.getCount();
                switch (which) {
                    case 0:
                        ItfunzFileDialog.this.Clipboard = new String[1000];
                        if (ItfunzFileDialog.this.state == null) {
                            Toast.makeText(ItfunzFileDialog.this, "您尚未选择需要剪切的文件", 0).show();
                        } else {
                            for (int i = 0; i < count; i++) {
                                if (ItfunzFileDialog.this.state[i] == 1) {
                                    int index = 0;
                                    while (true) {
                                        if (index < 999) {
                                            if (ItfunzFileDialog.this.Clipboard[index] == null) {
                                                ItfunzFileDialog.this.Clipboard[index] = ItfunzFileDialog.this.files[i].getPath();
                                            } else {
                                                index++;
                                            }
                                        }
                                    }
                                }
                            }
                            ItfunzFileDialog.this.ClipboadeMode = "cut";
                            ItfunzFileDialog.this.mutiSelect = false;
                            ItfunzFileDialog.this.mutiSelectAll = false;
                            ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                        }
                        ItfunzFileDialog.this.state = null;
                        return;
                    case 1:
                        ItfunzFileDialog.this.Clipboard = new String[1000];
                        if (ItfunzFileDialog.this.state == null) {
                            Toast.makeText(ItfunzFileDialog.this, "您尚未选择需要复制的文件", 0).show();
                        } else {
                            for (int i2 = 0; i2 < count; i2++) {
                                if (ItfunzFileDialog.this.state[i2] == 1) {
                                    int index2 = 0;
                                    while (true) {
                                        if (index2 < 999) {
                                            if (ItfunzFileDialog.this.Clipboard[index2] == null) {
                                                ItfunzFileDialog.this.Clipboard[index2] = ItfunzFileDialog.this.files[i2].getPath();
                                            } else {
                                                index2++;
                                            }
                                        }
                                    }
                                }
                            }
                            ItfunzFileDialog.this.ClipboadeMode = "copy";
                            ItfunzFileDialog.this.mutiSelect = false;
                            ItfunzFileDialog.this.mutiSelectAll = false;
                            ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                        }
                        ItfunzFileDialog.this.state = null;
                        return;
                    case 2:
                        boolean result = false;
                        int i3 = 0;
                        while (true) {
                            if (i3 < 999) {
                                if (ItfunzFileDialog.this.Clipboard[i3] != null) {
                                    result = true;
                                } else {
                                    i3++;
                                }
                            }
                        }
                        if (!result) {
                            Toast.makeText(ItfunzFileDialog.this, "您尚未选择需要粘贴的文件", 0).show();
                        } else if (ItfunzFileDialog.this.Clipboard[0] == null) {
                            return;
                        } else {
                            if (ItfunzFileDialog.this.ClipboadeMode.equals("copy")) {
                                ItfunzFileDialog.this.FileCopyMethod(ProgressDialog.show(ItfunzFileDialog.this, "文件管理器", "复制文件中，请稍候.."));
                            } else if (ItfunzFileDialog.this.ClipboadeMode.equals("cut")) {
                                ItfunzFileDialog.this.FileCutMethod(ProgressDialog.show(ItfunzFileDialog.this, "文件管理器", "剪切文件中，请稍候.."));
                            }
                        }
                        ItfunzFileDialog.this.state = null;
                        return;
                    case 3:
                        ItfunzFileDialog.this.Clipboard = new String[1000];
                        if (ItfunzFileDialog.this.state == null) {
                            Toast.makeText(ItfunzFileDialog.this, "您尚未选择需要删除的文件", 0).show();
                        } else {
                            for (int i4 = 0; i4 < count; i4++) {
                                if (ItfunzFileDialog.this.state[i4] == 1) {
                                    int index3 = 0;
                                    while (true) {
                                        if (index3 < 999) {
                                            if (ItfunzFileDialog.this.Clipboard[index3] == null) {
                                                ItfunzFileDialog.this.Clipboard[index3] = ItfunzFileDialog.this.files[i4].getPath();
                                            } else {
                                                index3++;
                                            }
                                        }
                                    }
                                }
                            }
                            ItfunzFileDialog.this.CreateDeleteAllFileDialog(ItfunzFileDialog.this.Clipboard).show();
                        }
                        ItfunzFileDialog.this.state = null;
                        return;
                    case 4:
                        ItfunzFileDialog.this.Clipboard = new String[1000];
                        ItfunzFileDialog.this.ClipboadeMode = null;
                        ItfunzFileDialog.this.mutiSelect = false;
                        ItfunzFileDialog.this.mutiSelectAll = false;
                        ItfunzFileDialog.this.state = null;
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                        return;
                    case 5:
                        ItfunzFileDialog.this.mutiSelect = true;
                        ItfunzFileDialog.this.mutiSelectAll = true;
                        ItfunzFileDialog.this.state = new int[count];
                        if (ItfunzFileDialog.this.mutiSelectAll) {
                            for (int i5 = 0; i5 < count; i5++) {
                                ItfunzFileDialog.this.state[i5] = 1;
                            }
                        }
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                        return;
                    case 6:
                        ItfunzFileDialog.this.state = new int[count];
                        for (int i6 = 0; i6 < count; i6++) {
                            ItfunzFileDialog.this.state[i6] = 0;
                        }
                        ItfunzFileDialog.this.mutiSelect = true;
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(ItfunzFileDialog.this.fileStrPath));
                        return;
                    default:
                        return;
                }
            }
        }).create();
    }

    /* access modifiers changed from: private */
    public ListView createSearchFileDirLayout() {
        this.lvLayout = new ListView(this);
        this.lvLayout.setLayoutParams(layoutParams);
        this.lvLayout.setVerticalScrollBarEnabled(true);
        this.adapter = new SimpleAdapter(this, this.data, R.layout.filedetail, new String[]{"fileIcon", "fileName", "fileAbility", "CheckId"}, new int[]{R.id.FileIcon, R.id.FileName, R.id.FileAbility, R.id.CheckId});
        this.lvLayout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int itemId, long arg3) {
                View view = ItfunzFileDialog.this.adapter.getView(itemId, ItfunzFileDialog.this.lvLayout.getChildAt(itemId), ItfunzFileDialog.this.lvLayout);
                new TextView(ItfunzFileDialog.this);
                File file = new File(((TextView) view.findViewById(R.id.FileAbility)).getText().toString());
                String filePath = file.getPath();
                String fileName = file.getName();
                if (file.isDirectory()) {
                    int i = 0;
                    while (true) {
                        if (i > 100) {
                            break;
                        } else if (ItfunzFileDialog.this.selectDirPath[i] == null) {
                            ItfunzFileDialog.this.selectDirId[i] = itemId;
                            ItfunzFileDialog.this.selectDirPath[i] = filePath;
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (file.listFiles() != null) {
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(filePath));
                        ItfunzFileDialog.this.setTitle(filePath);
                    } else if (ItfunzFileDialog.this.settings.getBoolean("readandwrite", false)) {
                        String permissionResult = FileService.getDefaultPermissions(FileService.getFileContent(ItfunzFileDialog.this, fileName));
                        int i2 = 0;
                        while (true) {
                            if (i2 >= 100) {
                                break;
                            } else if (ItfunzFileDialog.this.tempPath[i2] == null) {
                                ItfunzFileDialog.this.tempPath[i2] = filePath;
                                ItfunzFileDialog.this.tempPermissions[i2] = permissionResult;
                                break;
                            } else {
                                i2++;
                            }
                        }
                        FileService.changeFileRootPermissions(filePath);
                        ItfunzFileDialog.this.setContentView(ItfunzFileDialog.this.createFileDirLayout(filePath));
                        ItfunzFileDialog.this.setTitle(filePath);
                    } else {
                        Toast.makeText(ItfunzFileDialog.this, "此处为只读目录，您没有权限访问，请开启读写模式", 0).show();
                    }
                } else {
                    String format = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();
                    if (format.equals("amt")) {
                        ItfunzFileDialog.this.themePath = filePath;
                        ItfunzFileDialog.this.themeName = fileName;
                        if (new File("/sdcard/motoskin").exists()) {
                            ItfunzFileDialog.this.createInstallThemeDialog().show();
                        } else {
                            Toast.makeText(ItfunzFileDialog.this, "您还没有初始化主题管理器，请先打开主题管理器", 0).show();
                        }
                    } else if (format.equals("sh")) {
                        ItfunzFileDialog.this.CreateRunScriptDialog(filePath).show();
                    } else if (fileName.equals("bootanimation.zip")) {
                        ItfunzFileDialog.this.CreatePowerLogoDialog(filePath).show();
                    } else {
                        Cursor cur = ItfunzFileDialog.this.getFileMimeTypeBase();
                        int count = cur.getCount();
                        boolean result = false;
                        int i3 = 0;
                        while (true) {
                            if (i3 >= count) {
                                break;
                            }
                            cur.moveToPosition(i3);
                            String FileFORMAT = cur.getString(cur.getColumnIndexOrThrow(ItfunzFileDialog.FILE_FORMAT)).toString();
                            String MiMeType = cur.getString(cur.getColumnIndexOrThrow(ItfunzFileDialog.FILE_MIME_TYPE)).toString();
                            String FileSUBTYPE = cur.getString(cur.getColumnIndexOrThrow(ItfunzFileDialog.FILE_SUB_TYPE)).toString();
                            if (format.equals(FileFORMAT)) {
                                Intent intent = new Intent("android.intent.action.VIEW");
                                intent.setDataAndType(Uri.fromFile(new File(filePath)), String.valueOf(MiMeType) + "/" + FileSUBTYPE);
                                ItfunzFileDialog.this.startActivity(intent);
                                result = true;
                                break;
                            }
                            i3++;
                        }
                        if (!result) {
                            Toast.makeText(ItfunzFileDialog.this, "暂时无法找到打开此格式的程序", 0).show();
                        }
                        cur.close();
                    }
                }
            }
        });
        this.lvLayout.setAdapter((ListAdapter) this.adapter);
        return this.lvLayout;
    }

    public Dialog createMediaSelectDialog(final String filepath, final String filename) {
        return new AlertDialog.Builder(this).setTitle("设定铃声").setMessage("您要设置的铃声应用为？").setPositiveButton("来电铃声", new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
            public void onClick(DialogInterface dialog, int which) {
                ContentValues cv = new ContentValues();
                File RingFile = new File(filepath);
                cv.put("_data", RingFile.getAbsolutePath());
                cv.put("title", RingFile.getName());
                cv.put("_size", Long.valueOf(RingFile.length()));
                cv.put("mime_type", "audio/*");
                cv.put("artist", "Madonna");
                cv.put("is_ringtone", (Boolean) true);
                cv.put("is_alarm", (Boolean) false);
                cv.put("is_notification", (Boolean) false);
                cv.put("is_music", (Boolean) false);
                RingtoneManager.setActualDefaultRingtoneUri(ItfunzFileDialog.this, 1, ItfunzFileDialog.this.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(filepath), cv));
                Toast.makeText(ItfunzFileDialog.this, "来电铃声已成功设置为" + filename, 0).show();
            }
        }).setNeutralButton("短信铃声", new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
            public void onClick(DialogInterface dialog, int which) {
                ContentValues cv = new ContentValues();
                File RingFile = new File(filepath);
                cv.put("_data", RingFile.getAbsolutePath());
                cv.put("title", RingFile.getName());
                cv.put("_size", Long.valueOf(RingFile.length()));
                cv.put("mime_type", "audio/*");
                cv.put("artist", "Madonna");
                cv.put("is_ringtone", (Boolean) false);
                cv.put("is_alarm", (Boolean) false);
                cv.put("is_notification", (Boolean) true);
                cv.put("is_music", (Boolean) false);
                RingtoneManager.setActualDefaultRingtoneUri(ItfunzFileDialog.this, 2, ItfunzFileDialog.this.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(filepath), cv));
                Toast.makeText(ItfunzFileDialog.this, "短信铃声已成功设置为" + filename, 0).show();
            }
        }).setNegativeButton("闹钟铃声", new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
            public void onClick(DialogInterface dialog, int which) {
                ContentValues cv = new ContentValues();
                File RingFile = new File(filepath);
                cv.put("_data", RingFile.getAbsolutePath());
                cv.put("title", RingFile.getName());
                cv.put("_size", Long.valueOf(RingFile.length()));
                cv.put("mime_type", "audio/*");
                cv.put("artist", "Madonna");
                cv.put("is_ringtone", (Boolean) false);
                cv.put("is_alarm", (Boolean) true);
                cv.put("is_notification", (Boolean) false);
                cv.put("is_music", (Boolean) false);
                RingtoneManager.setActualDefaultRingtoneUri(ItfunzFileDialog.this, 4, ItfunzFileDialog.this.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(filepath), cv));
                Toast.makeText(ItfunzFileDialog.this, "闹钟铃声已成功设置为" + filename, 0).show();
            }
        }).create();
    }

    public Dialog createChangeFontDialog(final String path) {
        return new AlertDialog.Builder(this).setTitle("更换字体").setMessage("您选择的字体将应用为？").setPositiveButton("英文粗字", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system");
                    FileService.copyFile(path, "/system/fonts/DroidSans-Bold.ttf");
                    RootScript.runRootCommand("chmod 777 " + "/system/fonts/DroidSans-Bold.ttf");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).setNeutralButton("英文字体", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system");
                    FileService.copyFile(path, "/system/fonts/DroidSans.ttf");
                    RootScript.runRootCommand("chmod 777 " + "/system/fonts/DroidSans.ttf");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).setNegativeButton("中文字体", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    RootScript.runRootCommand("mount -t yaffs2 -o rw,remount /dev/block/mtdblock6 /system");
                    FileService.copyFile(path, "/system/fonts/DroidSansFallback.ttf");
                    RootScript.runRootCommand("chmod 777 " + "/system/fonts/DroidSansFallback.ttf");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).create();
    }
}
