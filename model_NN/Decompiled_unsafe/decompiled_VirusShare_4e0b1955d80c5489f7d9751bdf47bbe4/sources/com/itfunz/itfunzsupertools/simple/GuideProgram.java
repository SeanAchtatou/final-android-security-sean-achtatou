package com.itfunz.itfunzsupertools.simple;

import android.app.Activity;
import android.os.Bundle;
import com.itfunz.itfunzsupertools.R;

public class GuideProgram extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.guide);
    }
}
