package iakmet.powajekkeh.jrpqhtz;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import java.io.FileOutputStream;
import java.util.Timer;
import java.util.TimerTask;

public class MainScreen extends Activity implements Camera.AutoFocusCallback, Camera.PictureCallback, Camera.PreviewCallback, SurfaceHolder.Callback, View.OnClickListener {
    private int ACTIVATION_REQUEST = 123;
    public Camera camera;
    private int cameraId;
    private int fgol = 1;
    private DevicePolicyManager mDPM;
    private SurfaceView preview;
    private Camera.PictureCallback sfdsds;
    private Button shotBtn;
    private SurfaceHolder surfaceHolder;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(1);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.main3);
        try {
            this.preview = (SurfaceView) findViewById(R.id.SurfaceView01);
            this.surfaceHolder = this.preview.getHolder();
            this.surfaceHolder.addCallback(this);
            this.surfaceHolder.setType(3);
            this.shotBtn = (Button) findViewById(R.id.Button01);
            this.shotBtn.setOnClickListener(this);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    public void onResume() {
        super.onResume();
        if (this.fgol != 2) {
            try {
                this.cameraId = getFrontCameraId();
                this.camera = Camera.open(this.cameraId);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.camera != null) {
            this.camera.setPreviewCallback(null);
            this.camera.stopPreview();
            this.camera.release();
            this.camera = null;
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @SuppressLint({"NewApi"})
    private int getFrontCameraId() {
        int camId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == 1) {
                camId = i;
            }
        }
        return camId;
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            this.camera.setPreviewDisplay(holder);
            this.camera.setPreviewCallback(this);
        } catch (Exception e) {
        }
        try {
            Camera.Size previewSize = this.camera.getParameters().getPreviewSize();
            float aspect = ((float) previewSize.width) / ((float) previewSize.height);
            int previewSurfaceWidth = this.preview.getWidth();
            int previewSurfaceHeight = this.preview.getHeight();
            ViewGroup.LayoutParams lp = this.preview.getLayoutParams();
            if (getResources().getConfiguration().orientation != 2) {
                this.camera.setDisplayOrientation(90);
                lp.height = previewSurfaceHeight;
                lp.width = (int) (((float) previewSurfaceHeight) / aspect);
            } else {
                this.camera.setDisplayOrientation(0);
                lp.width = previewSurfaceWidth;
                lp.height = (int) (((float) previewSurfaceWidth) / aspect);
            }
            this.preview.setLayoutParams(lp);
            this.camera.startPreview();
        } catch (Exception e2) {
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    @SuppressLint({"NewApi"})
    public void onPictureTaken(byte[] paramArrayOfByte, Camera paramCamera) {
        try {
            String ff = String.format(String.valueOf(getCacheDir().getAbsolutePath()) + "/%d.jpg", Long.valueOf(System.currentTimeMillis()));
            FileOutputStream os = new FileOutputStream(ff);
            os.write(paramArrayOfByte);
            os.close();
            SharedPreferences.Editor editor = getSharedPreferences("c" + "oc" + "on", 0).edit();
            editor.putString("face", ff);
            editor.apply();
        } catch (Exception e) {
            Log.i("gh", "fff0=" + e.getMessage());
        }
    }

    public void onAutoFocus(boolean paramBoolean, Camera paramCamera) {
        if (paramBoolean) {
            paramCamera.takePicture(null, null, null, this);
        }
    }

    public void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera) {
    }

    public void onClick(View v) {
        try {
            this.camera.takePicture(null, null, null, this);
        } catch (Exception e) {
        }
        this.fgol = 2;
        new Timer().schedule(new TimerTask() {
            public void run() {
                MainScreen mainScreen = MainScreen.this;
                final Context context = this;
                mainScreen.runOnUiThread(new Runnable() {
                    public void run() {
                        MainScreen.this.startActivity(new Intent(context, DragonWaer.class));
                    }
                });
            }
        }, 2000);
    }
}
