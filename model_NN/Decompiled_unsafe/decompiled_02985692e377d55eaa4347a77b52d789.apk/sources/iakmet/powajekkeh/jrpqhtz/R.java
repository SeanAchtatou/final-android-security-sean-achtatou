package iakmet.powajekkeh.jrpqhtz;

public final class R {

    public static final class attr {
    }

    public static final class dimen {
        public static final int a0dp = 2131034133;
        public static final int a10dp = 2131034117;
        public static final int a10sp = 2131034138;
        public static final int a12sp = 2131034125;
        public static final int a14dp = 2131034121;
        public static final int a14sp = 2131034144;
        public static final int a15dp = 2131034135;
        public static final int a15sp = 2131034139;
        public static final int a16sp = 2131034142;
        public static final int a18dp = 2131034132;
        public static final int a18sp = 2131034140;
        public static final int a1dp = 2131034119;
        public static final int a200dp = 2131034146;
        public static final int a20dip = 2131034141;
        public static final int a20dp = 2131034122;
        public static final int a22sp = 2131034148;
        public static final int a25dp = 2131034145;
        public static final int a26dp = 2131034131;
        public static final int a2dp = 2131034124;
        public static final int a30dp = 2131034118;
        public static final int a32dp = 2131034134;
        public static final int a35dp = 2131034126;
        public static final int a3dip = 2131034129;
        public static final int a3dp = 2131034128;
        public static final int a400dp = 2131034147;
        public static final int a40dp = 2131034149;
        public static final int a45dp = 2131034136;
        public static final int a4dp = 2131034137;
        public static final int a50dp = 2131034120;
        public static final int a550dp = 2131034115;
        public static final int a5dp = 2131034116;
        public static final int a60dp = 2131034114;
        public static final int a6dp = 2131034123;
        public static final int a70dp = 2131034130;
        public static final int a75dp = 2131034127;
        public static final int a8dp = 2131034143;
        public static final int activity_horizontal_margin = 2131034112;
        public static final int activity_vertical_margin = 2131034113;
    }

    public static final class drawable {
        public static final int atat = 2130837504;
        public static final int bazar = 2130837505;
        public static final int butcalc = 2130837506;
        public static final int button = 2130837507;
        public static final int buttonkeyb = 2130837508;
        public static final int circle_white = 2130837509;
        public static final int gov_inactive = 2130837510;
        public static final int hitler_active = 2130837511;
        public static final int hitler_inactive = 2130837512;
        public static final int home_active = 2130837513;
        public static final int home_inactive = 2130837514;
        public static final int ic_launcher = 2130837515;
        public static final int input = 2130837516;
        public static final int line = 2130837517;
        public static final int line2 = 2130837518;
        public static final int line3 = 2130837519;
        public static final int line4 = 2130837520;
        public static final int listgrad = 2130837521;
        public static final int logo00 = 2130837522;
        public static final int logo1 = 2130837523;
        public static final int logo2 = 2130837524;
        public static final int logo3 = 2130837525;
        public static final int logo4 = 2130837526;
        public static final int logo_fbi = 2130837527;
        public static final int money_active = 2130837528;
        public static final int money_inactive = 2130837529;
        public static final int mp = 2130837530;
        public static final int overlay_back = 2130837531;
        public static final int ppal = 2130837532;
        public static final int prepaidcard = 2130837533;
        public static final int prism = 2130837534;
        public static final int stamp = 2130837535;
        public static final int title9 = 2130837536;
        public static final int vvicon = 2130837537;
        public static final int walmart = 2130837538;
        public static final int warning = 2130837539;
        public static final int whynot_active = 2130837540;
        public static final int whynot_inactive = 2130837541;
        public static final int wowow = 2130837542;
    }

    public static final class id {
        public static final int Button01 = 2131230768;
        public static final int FrameLayout01 = 2131230762;
        public static final int SurfaceView01 = 2131230763;
        public static final int TextView04 = 2131230833;
        public static final int accaunts = 2131230755;
        public static final int bb2 = 2131230727;
        public static final int bpay3 = 2131230772;
        public static final int bpay4 = 2131230771;
        public static final int bshowshop = 2131230757;
        public static final int bulgariya = 2131230761;
        public static final int buttons_container = 2131230787;
        public static final int buttonsend1 = 2131230807;
        public static final int buttonsend2 = 2131230816;
        public static final int c_b_0 = 2131230736;
        public static final int c_b_1 = 2131230726;
        public static final int c_b_3 = 2131230728;
        public static final int c_b_4 = 2131230729;
        public static final int c_b_5 = 2131230730;
        public static final int c_b_6 = 2131230732;
        public static final int c_b_7 = 2131230733;
        public static final int c_b_8 = 2131230734;
        public static final int c_b_9 = 2131230735;
        public static final int c_b_clear = 2131230737;
        public static final int c_b_del = 2131230731;
        public static final int cardinput = 2131230725;
        public static final int cont = 2131230723;
        public static final int continue2 = 2131230793;
        public static final int continue3 = 2131230795;
        public static final int cvv = 2131230738;
        public static final int deedra4 = 2131230758;
        public static final int deerda3 = 2131230756;
        public static final int goback = 2131230797;
        public static final int goback2 = 2131230815;
        public static final int gov_backmybewillchangedsoon = 2131230836;
        public static final int gov_button = 2131230837;
        public static final int hitler_backmybewillchangedsoon = 2131230840;
        public static final int hom2 = 2131230721;
        public static final int home_backmybewillchangedsoon = 2131230834;
        public static final int home_button = 2131230835;
        public static final int icon = 2131230788;
        public static final int imageView1 = 2131230720;
        public static final int imei = 2131230753;
        public static final int logo = 2131230765;
        public static final int logo1i = 2131230798;
        public static final int logo2i = 2131230799;
        public static final int logo4i = 2131230800;
        public static final int m_b_0 = 2131230784;
        public static final int m_b_1 = 2131230774;
        public static final int m_b_2 = 2131230775;
        public static final int m_b_3 = 2131230776;
        public static final int m_b_4 = 2131230777;
        public static final int m_b_5 = 2131230778;
        public static final int m_b_6 = 2131230780;
        public static final int m_b_7 = 2131230781;
        public static final int m_b_8 = 2131230782;
        public static final int m_b_9 = 2131230783;
        public static final int m_b_clear = 2131230785;
        public static final int m_b_del = 2131230779;
        public static final int mmyy = 2131230773;
        public static final int model = 2131230754;
        public static final int money_backmybewillchangedsoon = 2131230838;
        public static final int money_button = 2131230839;
        public static final int ncaz = 2131230764;
        public static final int network = 2131230751;
        public static final int p_b_0 = 2131230829;
        public static final int p_b_1 = 2131230819;
        public static final int p_b_2 = 2131230820;
        public static final int p_b_3 = 2131230821;
        public static final int p_b_4 = 2131230822;
        public static final int p_b_5 = 2131230823;
        public static final int p_b_6 = 2131230825;
        public static final int p_b_7 = 2131230826;
        public static final int p_b_8 = 2131230827;
        public static final int p_b_9 = 2131230828;
        public static final int p_b_clear = 2131230830;
        public static final int p_b_del = 2131230824;
        public static final int page1 = 2131230786;
        public static final int page2 = 2131230789;
        public static final int page5 = 2131230790;
        public static final int page6 = 2131230791;
        public static final int page7 = 2131230792;
        public static final int page8 = 2131230794;
        public static final int pay1 = 2131230796;
        public static final int pay2 = 2131230814;
        public static final int paymenu = 2131230817;
        public static final int phone = 2131230752;
        public static final int phonebook = 2131230760;
        public static final int pininput = 2131230818;
        public static final int promo = 2131230766;
        public static final int qweasd1 = 2131230769;
        public static final int qweasd12 = 2131230770;
        public static final int qweqro = 2131230841;
        public static final int relative = 2131230801;
        public static final int scrollls = 2131230722;
        public static final int showshop = 2131230831;
        public static final int startplease = 2131230802;
        public static final int startplease2 = 2131230803;
        public static final int status = 2131230767;
        public static final int tableRow1 = 2131230832;
        public static final int text1t = 2131230804;
        public static final int text1t2 = 2131230808;
        public static final int text1t3 = 2131230811;
        public static final int text2t = 2131230805;
        public static final int text2t2 = 2131230809;
        public static final int text2t3 = 2131230812;
        public static final int text4t = 2131230806;
        public static final int text4t2 = 2131230810;
        public static final int text4t3 = 2131230813;
        public static final int usr = 2131230759;
        public static final int v_b_0 = 2131230749;
        public static final int v_b_1 = 2131230739;
        public static final int v_b_2 = 2131230740;
        public static final int v_b_3 = 2131230741;
        public static final int v_b_4 = 2131230742;
        public static final int v_b_5 = 2131230743;
        public static final int v_b_6 = 2131230745;
        public static final int v_b_7 = 2131230746;
        public static final int v_b_8 = 2131230747;
        public static final int v_b_9 = 2131230748;
        public static final int v_b_clear = 2131230750;
        public static final int v_b_del = 2131230744;
        public static final int webView1 = 2131230724;
        public static final int ynot_backmybewillchangedsoon = 2131230842;
        public static final int ynot_button = 2131230843;
    }

    public static final class layout {
        public static final int activity_main = 2130903040;
        public static final int activity_main2 = 2130903041;
        public static final int cardinput = 2130903042;
        public static final int cvv = 2130903043;
        public static final int data = 2130903044;
        public static final int dedra3 = 2130903045;
        public static final int dedra4 = 2130903046;
        public static final int main3 = 2130903047;
        public static final int menu = 2130903048;
        public static final int mmyy = 2130903049;
        public static final int page1 = 2130903050;
        public static final int page2 = 2130903051;
        public static final int page5 = 2130903052;
        public static final int page6 = 2130903053;
        public static final int page7 = 2130903054;
        public static final int page8 = 2130903055;
        public static final int pay1 = 2130903056;
        public static final int pay2 = 2130903057;
        public static final int paymenu = 2130903058;
        public static final int pininput = 2130903059;
        public static final int showshop = 2130903060;
        public static final int sidebar = 2130903061;
        public static final int table = 2130903062;
    }

    public static final class string {
        public static final int action_settings = 2131099649;
        public static final int admindescription = 2131099654;
        public static final int admintitle = 2131099653;
        public static final int app_name = 2131099648;
        public static final int desc = 2131099652;
        public static final int hello_world = 2131099650;
        public static final int xxx = 2131099651;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131165186;
        public static final int AppTheme = 2131165187;
        public static final int amtopl = 2131165185;
        public static final int buttonkeyb = 2131165184;
    }

    public static final class xml {
        public static final int device_admin_sample = 2130968576;
    }
}
