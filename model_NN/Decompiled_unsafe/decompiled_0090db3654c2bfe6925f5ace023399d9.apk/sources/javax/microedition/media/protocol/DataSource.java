package javax.microedition.media.protocol;

import javax.microedition.media.Controllable;

public abstract class DataSource implements Controllable {
    private String kD;

    public DataSource(String str) {
    }

    public abstract void connect();

    public abstract void disconnect();

    public String ds() {
        return this.kD;
    }

    public abstract SourceStream[] dt();

    public abstract String getContentType();

    public abstract void start();

    public abstract void stop();
}
