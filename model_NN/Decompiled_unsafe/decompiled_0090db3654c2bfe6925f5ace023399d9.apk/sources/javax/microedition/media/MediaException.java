package javax.microedition.media;

public class MediaException extends Exception {
    private static final long serialVersionUID = 1;

    public MediaException() {
    }

    public MediaException(String str) {
        super(str);
    }
}
