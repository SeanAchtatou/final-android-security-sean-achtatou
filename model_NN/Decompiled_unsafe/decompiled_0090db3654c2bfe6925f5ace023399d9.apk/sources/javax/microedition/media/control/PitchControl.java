package javax.microedition.media.control;

import javax.microedition.media.Control;

public interface PitchControl extends Control {
    int aJ(int i);

    int dd();

    int de();

    int getPitch();
}
