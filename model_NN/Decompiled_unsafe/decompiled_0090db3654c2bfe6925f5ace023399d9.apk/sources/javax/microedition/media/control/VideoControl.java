package javax.microedition.media.control;

public interface VideoControl extends GUIControl {
    public static final int USE_DIRECT_VIDEO = 1;

    void B(int i, int i2);

    void C(int i, int i2);

    byte[] S(String str);

    Object a(int i, Object obj);

    int dm();

    int dn();

    /* renamed from: do  reason: not valid java name */
    int m0do();

    int dp();

    int dq();

    int dr();

    void o(boolean z);

    void setVisible(boolean z);
}
