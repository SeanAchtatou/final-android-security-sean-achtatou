package javax.microedition.media.control;

public interface TempoControl extends RateControl {
    int aM(int i);

    int dl();
}
