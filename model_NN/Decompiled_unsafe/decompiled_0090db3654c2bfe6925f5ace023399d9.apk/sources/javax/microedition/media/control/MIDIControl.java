package javax.microedition.media.control;

import javax.microedition.media.Control;

public interface MIDIControl extends Control {
    public static final int CONTROL_CHANGE = 176;
    public static final int NOTE_ON = 144;

    String A(int i, int i2);

    int[] aG(int i);

    int aH(int i);

    int[] aI(int i);

    boolean db();

    int e(byte[] bArr, int i, int i2);

    int[] n(boolean z);

    void o(int i, int i2, int i3);

    String p(int i, int i2, int i3);

    void q(int i, int i2, int i3);

    void z(int i, int i2);
}
