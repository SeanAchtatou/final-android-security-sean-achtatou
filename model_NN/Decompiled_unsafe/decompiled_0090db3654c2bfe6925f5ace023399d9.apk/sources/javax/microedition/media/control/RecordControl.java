package javax.microedition.media.control;

import java.io.OutputStream;
import javax.microedition.media.Control;

public interface RecordControl extends Control {
    void R(String str);

    void a(OutputStream outputStream);

    int aL(int i);

    void commit();

    void di();

    void dj();

    String getContentType();

    void reset();
}
