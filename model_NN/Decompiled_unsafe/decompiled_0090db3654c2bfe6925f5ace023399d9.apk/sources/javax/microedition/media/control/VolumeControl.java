package javax.microedition.media.control;

import javax.microedition.media.Control;

public interface VolumeControl extends Control {
    int aN(int i);

    int getLevel();

    boolean isMuted();

    void p(boolean z);
}
