package javax.microedition.media.control;

import javax.microedition.media.Control;

public interface FramePositioningControl extends Control {
    int aD(int i);

    int aE(int i);

    long aF(int i);

    int d(long j);
}
