package javax.microedition.media;

import java.io.InputStream;
import javax.microedition.media.protocol.DataSource;
import org.meteoroid.core.g;
import org.meteoroid.plugin.device.MIDPDevice;

public class Manager {
    public static final String TONE_DEVICE_LOCATOR = "device://tone";
    public static final String[] kA = {"audio/x-wav", "audio/basic", "audio/mpeg", "audio/midi", "audio/x-tone-seq", "audio/amr"};
    public static final String[] kB = {"http://"};

    public static String[] N(String str) {
        return kA;
    }

    public static String[] O(String str) {
        return kB;
    }

    public static Player P(String str) {
        return new MIDPDevice.i(g.e(str, false));
    }

    public static Player a(InputStream inputStream, String str) {
        return new MIDPDevice.i(g.a(null, inputStream, str));
    }

    public static Player a(DataSource dataSource) {
        return new MIDPDevice.i(g.gT());
    }

    public static void n(int i, int i2, int i3) {
        throw new MediaException();
    }
}
