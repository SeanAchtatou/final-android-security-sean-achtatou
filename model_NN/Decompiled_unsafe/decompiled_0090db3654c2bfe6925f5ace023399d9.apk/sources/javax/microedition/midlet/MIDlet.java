package javax.microedition.midlet;

import android.os.Message;
import android.util.Log;
import com.a.a.c.c;
import org.meteoroid.core.a;
import org.meteoroid.core.h;
import org.meteoroid.plugin.device.MIDPDevice;

public abstract class MIDlet implements a.C0003a, h.a {
    public static final int MIDLET_PLATFORM_REQUEST = -2023686143;
    public static final int MIDLET_PLATFORM_REQUEST_FINISH = -2023686142;
    private boolean kE;
    private int kF;

    protected MIDlet() {
    }

    public final String T(String str) {
        return a.aL(str);
    }

    public final boolean U(String str) {
        h.j(MIDLET_PLATFORM_REQUEST, "MIDLET_PLATFORM_REQUEST");
        h.j(MIDLET_PLATFORM_REQUEST_FINISH, "MIDLET_PLATFORM_REQUEST_FINISH");
        h.e(MIDLET_PLATFORM_REQUEST, str);
        if (!this.kE) {
            return false;
        }
        throw new c();
    }

    public final int V(String str) {
        return -1;
    }

    public boolean a(Message message) {
        if (message.what == -2023686142) {
            if (message.obj != null) {
                this.kE = ((Boolean) message.obj).booleanValue();
            }
            return true;
        } else if (message.what == 47623) {
            onPause();
            dy();
            return true;
        } else if (message.what != 47622) {
            return false;
        } else {
            onStart();
            return true;
        }
    }

    public final void dA() {
        a.pause();
    }

    public final void dB() {
        a.resume();
    }

    public void dC() {
        this.kF = 0;
        h.a(this);
        a.start();
    }

    /* access modifiers changed from: protected */
    public abstract void dx();

    /* access modifiers changed from: protected */
    public abstract void dy();

    public final void dz() {
        Log.d(getClass().getSimpleName(), "MIDlet is called to destroy.");
        h.bP(MIDPDevice.MSG_MIDP_MIDLET_NOTIFYDESTROYED);
    }

    public int getState() {
        return this.kF;
    }

    public void onDestroy() {
        this.kF = 3;
        try {
            q(true);
        } catch (Exception e) {
            Log.w(getClass().getSimpleName(), e + " in MIDlet destroyApp");
        }
    }

    public void onPause() {
        this.kF = 2;
    }

    public void onResume() {
        this.kF = 1;
    }

    public void onStart() {
        this.kF = 1;
        try {
            dx();
        } catch (Exception e) {
            Log.w(getClass().getSimpleName(), e + " restart in MIDlet startApp");
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public abstract void q(boolean z);
}
