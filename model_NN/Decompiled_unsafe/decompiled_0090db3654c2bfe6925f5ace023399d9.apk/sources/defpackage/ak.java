package defpackage;

import com.a.a.e.o;
import com.a.a.e.p;
import com.a.a.i.k;
import java.util.Vector;

/* renamed from: ak  reason: default package */
public final class ak implements o {
    private static byte aj;
    public static boolean am = false;
    public static int b;
    private int[] X;
    private l Y = l.t();
    public int a;
    private j aH = this.aQ.aH;
    public boolean aK;
    boolean aM;
    private y aQ = y.I();
    private int aV;
    private int aW;
    private int aX;
    private int aY;
    private int aZ;
    public int aa;
    private int ab;
    private int ac;
    private int ad;
    private int ae;
    private p ah;
    private String[] ak;
    private String[] al;
    boolean an;
    boolean ao;
    public ai bE;
    public ai[] bF;
    boolean bR;
    boolean bS;
    private boolean bT;
    private boolean bU;
    private boolean bV;
    private int ba;
    private int bb = 12;
    private int bc;
    private int bd;
    private int bf = 5;
    private int bg;
    as bh;
    private aa bn = new aa();
    private k cZ;
    public int e;
    private as ec;
    int h;
    int i;
    public boolean k;
    private String o;
    private int p;
    public u vh;
    public d vi;
    an vj;
    e vk;
    private as vl;

    private void a(ai[] aiVarArr) {
        if (this.bF != null) {
            int length = this.bF.length + 1;
            for (int i2 = length / 2; i2 > 0; i2 /= 2) {
                for (int i3 = i2; i3 < length; i3++) {
                    int i4 = i3 - i2;
                    while (i4 >= i2) {
                        ai aiVar = aiVarArr[i4 - 1];
                        ai aiVar2 = aiVarArr[(i4 + i2) - 1];
                        if ((aiVar.ab < aiVar2.ab ? 65535 : aiVar.ab > aiVar2.ab ? (char) 1 : 0) > 0) {
                            ai aiVar3 = aiVarArr[i4 - 1];
                            aiVarArr[i4 - 1] = aiVarArr[(i4 + i2) - 1];
                            aiVarArr[(i4 + i2) - 1] = aiVar3;
                            i4 -= i2;
                        } else {
                            i4 = 0;
                        }
                    }
                }
            }
        }
    }

    public final void P() {
        this.o = null;
    }

    public final void a() {
        if (this.bF != null) {
            for (ai a2 : this.bF) {
                a2.a();
            }
        }
        y.I();
        if (!y.an) {
            this.bF = null;
        }
        if (this.vh != null) {
            this.vh.b();
        }
        y.I();
        if (!y.an) {
            this.vh = null;
        }
        if (this.ec != null) {
            this.ec.b();
            this.ec = null;
        }
        if (this.bh != null) {
            this.bh.b();
            this.bh = null;
        }
        this.ah = null;
        this.vi = null;
        System.gc();
        this.bn.a();
    }

    public final void a(String str) {
        this.ak = ah.f(str, ah.aP.A("哈") << 3);
        this.p = 0;
    }

    public final void b() {
        aj = 0;
        this.an = false;
        this.ao = false;
        this.bT = false;
        this.bU = false;
        if (!y.I().ao) {
            this.aQ.aH.a(this.e);
        }
        y.I().ao = false;
        this.vh.a(0, 0, 240, 320);
        this.vh.a(this.bE.p, this.bE.ab);
        this.vi = new d(this);
        this.ab = this.vh.b;
        this.ac = this.vh.h;
        this.h = this.bE.p;
        this.i = this.bE.ab;
        if (!this.aQ.bX && !this.aQ.bY) {
            a(this.aQ.s);
        }
        this.aQ.bX = false;
        this.aQ.bY = false;
        this.cZ = new k();
        a(this.bF);
        if (this.bF != null) {
            for (ai o2 : this.bF) {
                o2.o();
            }
        }
        if (this.e == 1 || this.e == 2 || this.e == 3 || this.e == 4 || this.e == 13 || this.e == 14 || this.e == 15) {
            this.vj = new an(this.vh.e, this.vh.a, 200, false, null, 240, 320);
            this.an = true;
        }
        if (this.e == 21 || this.e == 22 || this.e == 25 || this.e == 29 || this.e == 30 || this.e == 31 || this.e == 32 || this.e == 33) {
            this.vk = new e();
            this.ao = true;
        }
        if (this.e == 0) {
            this.aQ.t = "/music/piantou.mid";
            this.aQ.p = -1;
        } else if (this.e == 1 || this.e == 5 || this.e == 7 || this.e == 8 || this.e == 13 || this.e == 14 || this.e == 15) {
            this.aQ.t = "/music/map1.mid";
            this.aQ.p = -1;
        } else if (this.e == 6 || this.e == 9 || this.e == 10 || this.e == 11 || this.e == 12 || this.e == 34 || this.e == 35) {
            this.aQ.t = "/music/map2.mid";
            this.aQ.p = -1;
        } else {
            this.aQ.t = "/music/map3.mid";
            this.aQ.p = -1;
        }
        if (y.cc) {
            this.Y.a(this.aQ.t, this.aQ.p);
        }
        this.ah = ah.n("/ui/caidan.png");
        this.ec = new as("/sys/menuElements/shangcheng.ani", "/sys/menuElements/");
        this.bh = new as("/sys/menuElements/anniu.ani", "/sys/menuElements/");
        this.bn.b();
        this.aQ.cY = null;
        this.Y.k = true;
    }

    public final void b(int i2, int i3, int i4) {
        this.aY = i2;
        this.aZ = i3;
        this.ba = 6;
    }

    public final void b(o oVar) {
        oVar.translate(this.ab, this.ac);
        oVar.setColor(0);
        oVar.fillRect(0, 0, 240, 320);
        if (this.vh != null) {
            this.vh.b(oVar);
        }
        if (this.bF != null) {
            int length = this.bF.length;
            for (int i2 = 0; i2 < length; i2++) {
                if (y.bF.length > 1) {
                    int i3 = 0;
                    for (int i4 = 1; i4 < y.bF.length; i4++) {
                        if (this.bF[i2].ac != y.bF[i4].ac) {
                            i3++;
                        }
                    }
                    if (i3 != y.bF.length - 1) {
                    }
                }
                this.bF[i2].a(oVar, this.vh.p, this.vh.ab);
            }
        }
        if (this.vh != null) {
            this.vh.c(oVar);
        }
        if (this.an) {
            this.vj.a(oVar, this.vh.p, this.vh.ab);
        }
        if (this.ao) {
            this.vk.b(oVar);
        }
        if (this.ak != null) {
            String[] strArr = this.ak;
            int length2 = ((ah.e + 5) * strArr.length) + 20;
            int A = ah.aP.A(strArr[0]) + 40;
            ae.a(oVar, (this.vh.i - A) >> 1, (this.vh.aa - length2) >> 1, A, length2, 0, 12169636, 0, 12331540, aa.dK, 1, 1, 1, 1, 2, 1);
            oVar.setColor(16777215);
            for (int i5 = 0; i5 < strArr.length; i5++) {
                oVar.a(strArr[i5], this.vh.i >> 1, ((this.vh.aa - ((ah.e + 5) * strArr.length)) >> 1) + ((ah.e + 5) * i5), 17);
            }
        }
        oVar.translate(-this.ab, -this.ac);
        if (this.ak == null && !this.aH.k && !am) {
            oVar.a(this.ah, 0, 320, 36);
        }
        if (this.aK) {
            this.vl.a(oVar, 0, this.aV - this.vh.p, this.aW - this.vh.ab, false);
        }
        if (this.aa > 0 && this.aa < 6 && this.aa % 2 == 0) {
            l.a(oVar, 16777215);
        }
        if (this.vi != null) {
            this.vi.b(oVar);
        }
        if (this.aQ.cd) {
            oVar.setColor(0);
            if (this.bg < 30) {
                this.bg = this.bg + 5;
            } else {
                this.bg = 30;
                this.bV = true;
            }
            for (int i6 = 0; i6 < 8; i6++) {
                oVar.fillRect(i6 * 30, 0, this.bg, 320);
            }
            if (this.bV) {
                if (this.bc >= this.X.length) {
                    try {
                        Thread.sleep(1000);
                        this.bV = false;
                        this.bg = 0;
                        this.aQ.cd = false;
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                } else if (this.X[this.bc] >= ah.aP.A("哈") * this.bb) {
                    this.bc = this.bc + 1;
                } else {
                    int[] iArr = this.X;
                    int i7 = this.bc;
                    iArr[i7] = iArr[i7] + 30;
                }
                oVar.setColor(16777215);
                for (int i8 = 0; i8 < this.al.length; i8++) {
                    oVar.setClip(120 - (ah.aP.A("哈") * (this.bb >> 1)), (160 - ((this.al.length * ah.e) >> 1)) + ((ah.e + this.bf) * i8), this.X[i8], this.bd * (this.bc + 1));
                    oVar.a(this.al[i8], 120 - (ah.aP.A("哈") * (this.bb >> 1)), (160 - ((this.al.length * ah.e) >> 1)) + ((ah.e + this.bf) * i8), 20);
                }
            }
        }
        if (am) {
            this.bn.b(oVar);
        }
        if (k.k) {
            k.b(oVar);
        }
    }

    public final void c(String str, int i2, int i3, int i4) {
        int indexOf = str.indexOf("xml");
        if (indexOf != -1) {
            str = new StringBuffer().append(str.substring(0, indexOf)).append("ani").toString();
        }
        this.aV = i2;
        this.aW = i3;
        this.aX = i4;
        this.vl = new as(new StringBuffer().append("/role/").append(str).toString(), "/role/");
        this.vl.a(0, 1);
    }

    public final void c(Vector vector) {
        int size = vector.size() + y.bF.length;
        this.bF = new ai[size];
        for (int i2 = 0; i2 < y.bF.length; i2++) {
            y.bF[i2].ap = this;
            if (this.e == 0) {
                y.bF[0].t = "/role/r-zhongyin.ani";
                y.bF[0].aK = true;
                if (i2 != 0) {
                    y.bF[i2].aK = false;
                }
            } else {
                y.bF[0].t = "/role/r-zhongyin.ani";
                y.bF[i2].aK = true;
            }
            y.bF[i2].b();
        }
        for (int i3 = 0; i3 < size; i3++) {
            if (i3 < vector.size()) {
                this.bF[i3] = (ai) vector.elementAt(i3);
            } else {
                this.bF[i3] = y.bF[i3 - vector.size()];
            }
        }
        this.bE = y.bF[0];
        this.bE.an = true;
        int size2 = vector.size();
        for (int i4 = 0; i4 < size2; i4++) {
            this.bF[i4].ap = this;
            this.bF[i4].an = true;
            this.bF[i4].b();
        }
    }

    public final void d(Vector vector) {
        aq[] aqVarArr = new aq[vector.size()];
        int size = vector.size();
        for (int i2 = 0; i2 < size; i2++) {
            aqVarArr[i2] = (aq) vector.elementAt(i2);
        }
        this.vh.a(aqVarArr);
    }

    public final String[] kz() {
        return this.ak;
    }

    public final boolean l() {
        return this.aK;
    }

    public final void o() {
        if (k.k) {
            if (this.Y.h(131072)) {
                k.g(131072);
            }
            if (this.Y.h(262144)) {
                k.g(262144);
            }
            l.b();
        } else if (!am) {
            if (!this.aH.k) {
                if (this.Y.h(131072)) {
                    this.aQ.bX = true;
                    am = true;
                    this.bn.v();
                    l.b();
                    return;
                }
                if (!this.Y.h(262144)) {
                    if (this.Y.h(1)) {
                        if (this.e == 0 && this.aQ.bV && !this.bE.u(true)) {
                            this.aQ.bW = !this.aQ.bW;
                        }
                    }
                }
                l.b();
            }
            if (this.vi != null) {
                this.vi.b();
            }
            if (this.bE == null) {
                return;
            }
            if (this.Y.h(4096) || this.Y.h(4)) {
                this.bE.ds = 1;
                this.bE.b = 1;
            } else if (this.Y.h(8192) || this.Y.h(256)) {
                this.bE.ds = 2;
                this.bE.b = 1;
            } else if (this.Y.h(16384) || this.Y.h(16)) {
                this.bE.ds = 3;
                this.bE.b = 1;
            } else if (this.Y.h(k.MONDAY) || this.Y.h(64)) {
                this.bE.ds = 4;
                this.bE.b = 1;
            } else {
                this.bE.b = 0;
                if (this.Y.h(65536) || this.Y.h(32)) {
                    switch (this.bE.ds) {
                        case 1:
                            this.bE.aa = -4;
                            break;
                        case 2:
                            this.bE.aa = 4;
                            break;
                        case 3:
                            this.bE.i = -4;
                            break;
                        case 4:
                            this.bE.i = 4;
                            break;
                    }
                    this.bE.b(true);
                    this.bE.i = 0;
                    this.bE.aa = 0;
                }
            }
        } else if (b <= 0) {
            this.bn.o();
        } else {
            b--;
        }
    }

    public final void p(String str) {
        this.o = str;
        this.bg = 0;
        this.al = ah.b(this.o, "\r\n", ah.aP.A("哈") * this.bb);
        this.X = new int[this.al.length];
        this.bd = ah.e + this.bf;
    }

    public final void q() {
        if (am) {
            this.bn.q();
        } else if (this.ak != null) {
            int i2 = this.p;
            this.p = i2 + 1;
            if (i2 * this.Y.p() > 1500) {
                this.aQ.s = null;
                this.ak = null;
            }
        } else {
            if (!this.aH.k) {
                int length = this.vh.x().length;
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        break;
                    }
                    aq aqVar = this.vh.x()[i3];
                    if (aqVar.k && aqVar.aj == 1) {
                        this.aH.f(aqVar.i);
                        break;
                    }
                    i3++;
                }
            }
            this.aH.a();
            a(this.bF);
            if (this.vi != null) {
                this.vi.a();
            }
            if (this.vi != null && !this.vi.l() && !k.k && this.bF != null) {
                for (ai o2 : this.bF) {
                    o2.o();
                }
            }
            y.I();
            if (y.an) {
                this.aa++;
                if (this.aa > 8) {
                    this.aQ.d((byte) 5);
                    this.aa = 0;
                }
            }
            if (this.bS) {
                if (this.h < this.bE.p) {
                    this.ad += this.ba;
                    if (this.h + this.ad >= this.bE.p) {
                        this.ad = this.bE.p - this.h;
                    }
                } else if (this.h > this.bE.p) {
                    this.ad -= this.ba;
                    if (this.h + this.ad <= this.bE.p) {
                        this.ad = this.bE.p - this.h;
                    }
                }
                if (this.i < this.bE.ab) {
                    this.ae += this.ba;
                    if (this.i + this.ae >= this.bE.ab) {
                        this.ae = this.bE.ab - this.i;
                    }
                } else if (this.i > this.bE.ab) {
                    this.ae -= this.ba;
                    if (this.i + this.ae <= this.bE.ab) {
                        this.ae = this.bE.ab - this.i;
                    }
                }
                if (this.h + this.ad == this.bE.p && this.i + this.ae == this.bE.ab) {
                    this.ad = 0;
                    this.ae = 0;
                    this.bS = false;
                    return;
                }
                this.vh.a(this.h + this.ad, this.i + this.ae);
            }
            if (this.aM) {
                if (this.h < this.aY) {
                    this.ad += this.ba;
                    if (this.h + this.ad >= this.aY) {
                        this.ad = this.aY - this.h;
                    }
                } else if (this.h > this.aY) {
                    this.ad -= this.ba;
                    if (this.h + this.ad <= this.aY) {
                        this.ad = this.aY - this.h;
                    }
                }
                if (this.i < this.aZ) {
                    this.ae += this.ba;
                    if (this.i + this.ae >= this.aZ) {
                        this.ae = this.aZ - this.i;
                    }
                } else if (this.i > this.aZ) {
                    this.ae -= this.ba;
                    if (this.i + this.ae <= this.aZ) {
                        this.ae = this.aZ - this.i;
                    }
                }
                if (this.h + this.ad == this.aY && this.i + this.ae == this.aZ) {
                    this.ad = 0;
                    this.ae = 0;
                    this.h = this.aY;
                    this.i = this.aZ;
                    this.aM = false;
                }
                this.vh.a(this.h + this.ad, this.i + this.ae);
            }
            if (this.k) {
                if (this.a != -1) {
                    if (this.a > 0) {
                        this.a--;
                    } else {
                        this.k = false;
                    }
                }
                if (!this.k) {
                    this.ab = this.vh.b;
                    this.ac = this.vh.h;
                } else if (l.t().u() % 2 == 0) {
                    this.ab = this.vh.b + 3;
                    this.ac = this.vh.h + 3;
                } else {
                    this.ab = this.vh.b - 3;
                    this.ac = this.vh.h - 3;
                }
            }
            if (this.aK && this.aK) {
                if (this.aX == -1) {
                    if (!this.vl.h(0)) {
                        this.vl.f(0);
                    } else {
                        this.vl.a(0, 1);
                    }
                } else if (this.aX <= 0) {
                    this.aK = false;
                    this.vl = null;
                } else if (!this.vl.h(0)) {
                    this.vl.f(0);
                } else {
                    this.vl.a(0, 0);
                    this.aX--;
                }
            }
            if (this.ec != null && this.Y.u() % 4 == 0) {
                this.ec.f(0);
            }
        }
    }

    public final void v() {
        this.bE = y.bF[0];
        this.bE.an = true;
        int length = this.bF.length;
        for (int i2 = 0; i2 < length; i2++) {
            this.bF[i2].ap = this;
            this.bF[i2].an = true;
            this.bF[i2].b();
        }
    }
}
