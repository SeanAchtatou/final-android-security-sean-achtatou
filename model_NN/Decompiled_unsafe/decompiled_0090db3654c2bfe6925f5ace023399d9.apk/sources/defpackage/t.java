package defpackage;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Vector;
import javax.microedition.enhance.MIDPHelper;

/* renamed from: t  reason: default package */
public final class t {
    private static int[] X;
    private static byte[][] aG;
    private static boolean k = false;
    private static String o = "/event/";
    private int[] Z;
    private int a;
    private int[][] aR;
    private Vector aS;
    private Vector aT;
    private int[] aq;
    private Vector au;
    private int b;
    private int e;
    private int h;

    public t(int i) {
        if (!k) {
            a();
        }
        this.a = i;
        try {
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.b(getClass(), new StringBuffer().append(o).append("m").append(i).append("s").toString()));
            int readInt = dataInputStream.readInt();
            this.Z = new int[readInt];
            for (int i2 = 0; i2 < readInt; i2++) {
                this.Z[i2] = dataInputStream.readInt();
            }
            this.e = (readInt + 1) * 4;
            dataInputStream.close();
        } catch (IOException e2) {
        }
    }

    private static int a(int[] iArr, Vector vector, Vector vector2) {
        int size = vector.size();
        vector.addElement(new Integer(vector2.size()));
        vector.addElement(new Integer(iArr.length));
        for (int num : iArr) {
            vector2.addElement(new Integer(num));
        }
        return size;
    }

    private static int a(String[] strArr, Vector vector, Vector vector2) {
        int size = vector.size();
        vector.addElement(new Integer(vector2.size()));
        vector.addElement(new Integer(strArr.length));
        for (String addElement : strArr) {
            vector2.addElement(addElement);
        }
        return size;
    }

    private static void a() {
        try {
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.b(o.getClass(), new StringBuffer().append(o).append("cmd").toString()));
            int readInt = dataInputStream.readInt();
            X = new int[readInt];
            aG = new byte[readInt][];
            for (int i = 0; i < readInt; i++) {
                X[i] = dataInputStream.readInt();
                int readByte = dataInputStream.readByte();
                aG[i] = new byte[readByte];
                for (int i2 = 0; i2 < readByte; i2++) {
                    aG[i][i2] = dataInputStream.readByte();
                }
            }
            dataInputStream.close();
        } catch (Exception e2) {
            e2.printStackTrace();
            k = false;
        }
        k = true;
    }

    private static int[] a(DataInputStream dataInputStream) {
        int readInt = dataInputStream.readInt();
        int[] iArr = new int[readInt];
        for (int i = 0; i < readInt; i++) {
            iArr[i] = dataInputStream.readInt();
        }
        return iArr;
    }

    private static String[] b(DataInputStream dataInputStream) {
        int readInt = dataInputStream.readInt();
        String[] strArr = new String[readInt];
        for (int i = 0; i < readInt; i++) {
            strArr[i] = dataInputStream.readUTF();
        }
        return strArr;
    }

    private static byte[] l(int i) {
        for (int i2 = 0; i2 < X.length; i2++) {
            if (X[i2] == i) {
                return aG[i2];
            }
        }
        return new byte[0];
    }

    public final void b() {
        this.h = 0;
    }

    public final int e(byte b2) {
        return this.aq[b2];
    }

    public final int f(byte b2) {
        if (this.h == 0) {
            this.h = this.b + 1;
        }
        System.out.println(new StringBuffer().append("n").append(this.h).toString());
        this.b++;
        if (this.b < this.h + b2 && this.b < this.aq.length) {
            return this.aq[this.b];
        }
        this.b = (this.h + b2) - 1;
        return -1;
    }

    public final int f(int i) {
        this.au = new Vector();
        this.aS = new Vector();
        this.aT = new Vector();
        this.b = -1;
        try {
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.b(getClass(), new StringBuffer().append(o).append("m").append(this.a).append("s").toString()));
            dataInputStream.skip((long) (this.Z[i] + this.e));
            int readInt = dataInputStream.readInt();
            System.out.println(new StringBuffer().append("size").append(readInt).toString());
            this.aq = new int[readInt];
            this.aR = new int[readInt][];
            for (int i2 = 0; i2 < readInt; i2++) {
                this.aq[i2] = dataInputStream.readInt();
                byte[] l = l(this.aq[i2]);
                this.aR[i2] = new int[l.length];
                for (int i3 = 0; i3 < l.length; i3++) {
                    byte b2 = l[i3];
                    if (b2 == 1) {
                        this.aR[i2][i3] = dataInputStream.readInt();
                    } else if (b2 == 2) {
                        this.aR[i2][i3] = a(new String[]{dataInputStream.readUTF()}, this.au, this.aT);
                    } else if (b2 == 4) {
                        this.aR[i2][i3] = dataInputStream.readInt();
                    } else if (b2 == 5) {
                        this.aR[i2][i3] = a(a(dataInputStream), this.au, this.aS);
                    } else if (b2 == 3) {
                        this.aR[i2][i3] = a(b(dataInputStream), this.au, this.aT);
                    }
                }
            }
            dataInputStream.close();
            return readInt;
        } catch (IOException e2) {
            return -1;
        }
    }

    public final boolean h(int i) {
        return this.b < this.aq.length && i < this.aR[this.b].length && this.aR[this.b][i] != 0;
    }

    public final String i(int i) {
        if (this.b >= this.aq.length || i >= this.aR[this.b].length) {
            return "";
        }
        return (String) this.aT.elementAt(((Integer) this.au.elementAt(this.aR[this.b][i])).intValue());
    }

    public final int k(int i) {
        if (this.b >= this.aq.length || i >= this.aR[this.b].length) {
            return -1;
        }
        return this.aR[this.b][i];
    }

    public final int m(int i) {
        this.b = i;
        if (this.b < this.aq.length) {
            return this.aq[this.b];
        }
        return -1;
    }

    public final int p() {
        this.b++;
        if (this.b < this.aq.length) {
            return this.aq[this.b];
        }
        return -1;
    }

    public final int u() {
        return this.aq.length;
    }
}
