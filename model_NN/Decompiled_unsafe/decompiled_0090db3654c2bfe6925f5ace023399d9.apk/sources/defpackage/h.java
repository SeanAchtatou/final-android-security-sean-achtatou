package defpackage;

import com.a.a.e.p;

/* renamed from: h  reason: default package */
final class h {
    public byte aA;
    public byte[] aB;
    public short[][] aC;
    public short[] aD;
    public short[][] aE;
    public aq[][] aF;
    public byte[][] aG;
    public byte aj;
    public String[] ak;
    public byte[] at;
    public p[] aw;
    public short ay;
    public short az;

    h() {
    }

    public final p d(int i) {
        int length = this.aB.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                i2 = 0;
                break;
            } else if (this.aB[i2] == i) {
                break;
            } else {
                i2++;
            }
        }
        return this.aw[i2];
    }

    public final short[] e(int i) {
        int length = this.aC.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (this.aC[i2][0] == i) {
                return this.aC[i2];
            }
        }
        return null;
    }
}
