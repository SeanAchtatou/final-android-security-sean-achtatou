package defpackage;

import com.a.a.e.o;
import com.a.a.e.p;
import java.lang.reflect.Array;

/* renamed from: u  reason: default package */
public final class u {
    public int a;
    private byte[][] aG;
    private h aU;
    private int aV;
    private int aW;
    private int aX;
    private int aY;
    private int aZ;
    public int aa;
    public int ab;
    private int ac;
    private int ad;
    private int ae;
    public int b;
    private int ba;
    private int bb;
    private int bc;
    private int bd = 16711680;
    private aq[] be;
    private int bf;
    private int bg;
    public int e;
    public int h;
    public int i;
    public int p;

    public u(String str, String str2, boolean z) {
        f(str, str2);
    }

    private void a() {
        this.ac = (this.e / this.ba) + 2;
        this.ad = (this.a / this.bb) + 2;
        this.aG = (byte[][]) Array.newInstance(Byte.TYPE, this.ac, this.ad);
        for (int i2 = 0; i2 < this.aU.aG[0].length; i2 += 3) {
            this.aG[this.aU.aG[0][i2]][this.aU.aG[0][i2 + 1]] = this.aU.aG[0][i2 + 2];
        }
    }

    private void a(o oVar, int i2) {
        if (this.aU.aG != null) {
            byte[] bArr = this.aU.aG[i2];
            int i3 = 0;
            int length = bArr.length;
            while (true) {
                int i4 = i3;
                if (i4 < length) {
                    int i5 = bArr[i4] * this.ba;
                    int i6 = bArr[i4 + 1] * this.bb;
                    short[] e2 = this.aU.e(bArr[i4 + 2]);
                    short s = e2[2];
                    short s2 = e2[3];
                    short s3 = e2[4];
                    short s4 = e2[5];
                    byte O = ah.O(e2[6], e2[7]);
                    if (ah.c(i5, i6, s3, s4, this.p, this.ab, this.i, this.aa)) {
                        oVar.a(this.aU.d(e2[1]), s, s2, s3, s4, O, i5 - this.p, i6 - this.ab, 20);
                    }
                    i3 = i4 + 3;
                } else {
                    return;
                }
            }
        }
    }

    private void a(String str) {
        this.aU.aw = new p[this.aU.ak.length];
        for (int i2 = 0; i2 < this.aU.ak.length; i2++) {
            this.aU.aw[i2] = ah.n(new StringBuffer().append(str).append(this.aU.ak[i2]).toString());
        }
    }

    private void f(String str, String str2) {
        if (str == null) {
            throw new Exception("init map error");
        }
        this.aU = n.h(str);
        this.e = this.aU.ay;
        this.a = this.aU.az;
        this.ba = this.aU.aj;
        this.bb = this.aU.aA;
        int i2 = 0;
        while (true) {
            if (i2 >= this.aU.at.length) {
                break;
            } else if (this.aU.at[i2] == 2) {
                this.bc = i2;
                break;
            } else {
                i2++;
            }
        }
        a(str2);
        a();
        l.t();
        l.t();
        a(0, 0, 240, 320);
    }

    public final aq a(aq aqVar) {
        int length = this.aU.aF.length;
        for (int i2 = 0; i2 < length; i2++) {
            for (aq aqVar2 : this.aU.aF[i2]) {
                if (ah.c(aqVar.e, aqVar.a, aqVar.b, aqVar.h, aqVar2.e, aqVar2.a, aqVar2.b, aqVar2.h)) {
                    return aqVar2;
                }
            }
        }
        return null;
    }

    public final void a(int i2, int i3) {
        this.p = i2;
        this.ab = i3;
        int i4 = this.i;
        int i5 = this.aa;
        if (i2 <= (i4 >> 1)) {
            this.p = 0;
        } else if (i2 >= this.e - (i4 >> 1)) {
            this.p = this.e - i4;
        } else {
            this.p -= i4 >> 1;
        }
        if (i3 <= (i5 >> 1)) {
            this.ab = 0;
        } else if (i3 >= this.a - (i5 >> 1)) {
            this.ab = this.a - i5;
        } else {
            this.ab -= i5 >> 1;
        }
        this.aX = this.p / this.ba;
        this.aW = this.ab / this.bb;
        this.aY = this.aW + this.ae;
        this.aZ = this.aX + this.aV;
        if (this.aY > this.ad) {
            this.aY = this.ad;
        }
        if (this.aZ > this.ac) {
            this.aZ = this.ac;
        }
        this.bf = -(this.p % this.ba);
        this.bg = -(this.ab % this.bb);
    }

    public final void a(int i2, int i3, int i4, int i5) {
        if (this.e >= i4) {
            this.b = 0;
            this.i = i4;
        } else {
            this.b = (i4 - this.e) >> 1;
            this.i = this.e;
        }
        if (this.a >= i5) {
            this.h = 0;
            this.aa = i5;
        } else {
            this.h = (i5 - this.a) >> 1;
            this.aa = this.a;
        }
        this.aV = this.i / this.ba;
        this.ae = this.aa / this.bb;
        if (this.i != this.e) {
            this.aV += 2;
        }
        if (this.aa != this.a) {
            this.ae += 2;
        }
    }

    public final void a(aq[] aqVarArr) {
        this.be = aqVarArr;
    }

    public final void b() {
        this.aG = null;
        if (this.aU != null) {
            h hVar = this.aU;
            if (hVar.aG != null) {
                for (int i2 = 0; i2 < hVar.aG.length; i2++) {
                    hVar.aG[i2] = null;
                }
            }
            hVar.aG = null;
            hVar.at = null;
            hVar.aE = null;
            if (hVar.aw != null) {
                for (int i3 = 0; i3 < hVar.aw.length; i3++) {
                    hVar.aw[i3] = null;
                }
            }
            hVar.aw = null;
            if (hVar.ak != null) {
                for (int i4 = 0; i4 < hVar.ak.length; i4++) {
                    hVar.ak[i4] = null;
                }
            }
            hVar.ak = null;
            hVar.aC = null;
            if (hVar.aF != null) {
                for (int i5 = 0; i5 < hVar.aF.length; i5++) {
                    hVar.aF[i5] = null;
                }
            }
            hVar.aF = null;
        }
        this.aU = null;
    }

    public final void b(o oVar) {
        int bN = oVar.bN();
        int bO = oVar.bO();
        int bM = oVar.bM();
        int bL = oVar.bL();
        int i2 = this.bf;
        int i3 = this.bg;
        int i4 = this.aX;
        while (true) {
            int i5 = i4;
            int i6 = i3;
            if (i5 >= this.aZ) {
                break;
            }
            int i7 = i6;
            for (int i8 = this.aW; i8 < this.aY; i8++) {
                byte b2 = this.aG[i5][i8];
                if (b2 != 0) {
                    short[] e2 = this.aU.e(b2);
                    short s = e2[2];
                    short s2 = e2[3];
                    short s3 = e2[4];
                    short s4 = e2[5];
                    byte O = ah.O(e2[6], e2[7]);
                    p d = this.aU.d(e2[1]);
                    if (s3 == this.ba && s4 == this.bb) {
                        oVar.a(d, s, s2, s3, s4, O, i2, i7, 20);
                    }
                }
                i7 += this.bb;
            }
            i3 = this.bg;
            i2 += this.ba;
            i4 = i5 + 1;
        }
        int bN2 = oVar.bN();
        int bO2 = oVar.bO();
        int bM2 = oVar.bM();
        int bL2 = oVar.bL();
        short[] sArr = this.aU.aD;
        if (sArr != null) {
            int i9 = 0;
            int length = sArr.length;
            while (true) {
                int i10 = i9;
                if (i10 >= length) {
                    break;
                }
                int i11 = sArr[i10] * this.ba;
                int i12 = sArr[i10 + 1] * this.bb;
                short[] e3 = this.aU.e(sArr[i10 + 2]);
                short s5 = e3[2];
                short s6 = e3[3];
                short s7 = e3[4];
                short s8 = e3[5];
                byte O2 = ah.O(e3[6], e3[7]);
                if (ah.c(i11, i12, s7, s8, this.p, this.ab, this.i, this.aa)) {
                    oVar.a(this.aU.d(e3[1]), s5, s6, s7, s8, O2, i11 - this.p, i12 - this.ab, 20);
                }
                i9 = i10 + 3;
            }
            oVar.setClip(bN2, bO2, bM2, bL2);
        }
        for (int i13 = 1; i13 < this.bc; i13++) {
            a(oVar, i13);
        }
        oVar.setClip(bN, bO, bM, bL);
    }

    public final void c(o oVar) {
        int bN = oVar.bN();
        int bO = oVar.bO();
        int bM = oVar.bM();
        int bL = oVar.bL();
        int length = this.aU.aG.length;
        for (int i2 = this.bc; i2 < length; i2++) {
            a(oVar, i2);
        }
        oVar.setClip(bN, bO, bM, bL);
    }

    public final aq[] x() {
        return this.be;
    }
}
