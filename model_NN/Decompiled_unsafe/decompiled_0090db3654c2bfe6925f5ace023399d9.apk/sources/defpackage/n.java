package defpackage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.Vector;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.control.ToneControl;

/* renamed from: n  reason: default package */
final class n {
    private static byte[] at;
    private static Vector au;

    n() {
    }

    private static int a(int i, byte[] bArr) {
        byte[] bArr2 = new byte[2];
        int i2 = 0;
        int i3 = 0;
        while (bArr != null && i2 < bArr.length) {
            byte b = bArr[i2];
            if (i3 <= 1) {
                bArr2[i3] = b;
                i3++;
            } else {
                bArr2[0] = bArr2[1];
                bArr2[1] = b;
            }
            if (bArr2[0] == -1 && bArr2[1] == i) {
                return i2;
            }
            i2++;
        }
        return -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x007b A[SYNTHETIC, Splitter:B:20:0x007b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(defpackage.h r8) {
        /*
            r2 = 0
            r0 = 1
            byte[] r1 = defpackage.n.at     // Catch:{ all -> 0x0077 }
            int r0 = a(r0, r1)     // Catch:{ all -> 0x0077 }
            r1 = 12
            byte[] r3 = defpackage.n.at     // Catch:{ all -> 0x0077 }
            int r3 = a(r1, r3)     // Catch:{ all -> 0x0077 }
            if (r0 < 0) goto L_0x0042
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ all -> 0x0077 }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x0077 }
            byte[] r5 = defpackage.n.at     // Catch:{ all -> 0x0077 }
            int r6 = r0 + 1
            byte[] r7 = defpackage.n.at     // Catch:{ all -> 0x0077 }
            int r7 = r7.length     // Catch:{ all -> 0x0077 }
            int r0 = r7 - r0
            int r0 = r0 + -1
            r4.<init>(r5, r6, r0)     // Catch:{ all -> 0x0077 }
            r1.<init>(r4)     // Catch:{ all -> 0x0077 }
            short r0 = r1.readShort()     // Catch:{ all -> 0x0084 }
            r8.ay = r0     // Catch:{ all -> 0x0084 }
            short r0 = r1.readShort()     // Catch:{ all -> 0x0084 }
            r8.az = r0     // Catch:{ all -> 0x0084 }
            byte r0 = r1.readByte()     // Catch:{ all -> 0x0084 }
            r8.aj = r0     // Catch:{ all -> 0x0084 }
            byte r0 = r1.readByte()     // Catch:{ all -> 0x0084 }
            r8.aA = r0     // Catch:{ all -> 0x0084 }
            r1.close()     // Catch:{ all -> 0x0084 }
        L_0x0042:
            if (r3 < 0) goto L_0x0076
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ all -> 0x0077 }
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x0077 }
            byte[] r4 = defpackage.n.at     // Catch:{ all -> 0x0077 }
            int r5 = r3 + 1
            byte[] r6 = defpackage.n.at     // Catch:{ all -> 0x0077 }
            int r6 = r6.length     // Catch:{ all -> 0x0077 }
            int r3 = r6 - r3
            int r3 = r3 + -1
            r0.<init>(r4, r5, r3)     // Catch:{ all -> 0x0077 }
            r1.<init>(r0)     // Catch:{ all -> 0x0077 }
            byte r0 = r1.readByte()     // Catch:{ all -> 0x0084 }
            r2 = r0 & 255(0xff, float:3.57E-43)
            if (r2 <= 0) goto L_0x0073
            byte[] r0 = new byte[r2]     // Catch:{ all -> 0x0084 }
            r8.at = r0     // Catch:{ all -> 0x0084 }
            r0 = 0
        L_0x0066:
            if (r0 >= r2) goto L_0x0073
            byte[] r3 = r8.at     // Catch:{ all -> 0x0084 }
            byte r4 = r1.readByte()     // Catch:{ all -> 0x0084 }
            r3[r0] = r4     // Catch:{ all -> 0x0084 }
            int r0 = r0 + 1
            goto L_0x0066
        L_0x0073:
            r1.close()     // Catch:{ all -> 0x0084 }
        L_0x0076:
            return
        L_0x0077:
            r0 = move-exception
            r1 = r2
        L_0x0079:
            if (r1 == 0) goto L_0x007e
            r1.close()     // Catch:{ IOException -> 0x007f }
        L_0x007e:
            throw r0
        L_0x007f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x007e
        L_0x0084:
            r0 = move-exception
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.n.a(h):void");
    }

    private static void a(h hVar, Vector vector) {
        if (vector.size() != 0) {
            short[] sArr = new short[vector.size()];
            int length = sArr.length;
            for (int i = 0; i < length; i++) {
                sArr[i] = Short.parseShort((String) vector.elementAt(i));
            }
            hVar.aD = sArr;
        }
    }

    private static boolean a(h hVar, int i) {
        short[] e = hVar.e(i);
        return e[4] > hVar.aj || e[5] > hVar.aA;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0055 A[SYNTHETIC, Splitter:B:22:0x0055] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void b(defpackage.h r7) {
        /*
            r2 = 0
            r0 = 2
            byte[] r1 = defpackage.n.at     // Catch:{ all -> 0x0051 }
            int r0 = a(r0, r1)     // Catch:{ all -> 0x0051 }
            if (r0 < 0) goto L_0x0045
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ all -> 0x0051 }
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x0051 }
            byte[] r4 = defpackage.n.at     // Catch:{ all -> 0x0051 }
            int r5 = r0 + 1
            byte[] r6 = defpackage.n.at     // Catch:{ all -> 0x0051 }
            int r6 = r6.length     // Catch:{ all -> 0x0051 }
            int r0 = r6 - r0
            int r0 = r0 + -1
            r3.<init>(r4, r5, r0)     // Catch:{ all -> 0x0051 }
            r1.<init>(r3)     // Catch:{ all -> 0x0051 }
            byte r0 = r1.readByte()     // Catch:{ all -> 0x005e }
            r2 = r0 & 255(0xff, float:3.57E-43)
            byte[] r0 = new byte[r2]     // Catch:{ all -> 0x005e }
            r7.aB = r0     // Catch:{ all -> 0x005e }
            if (r2 <= 0) goto L_0x002f
            java.lang.String[] r0 = new java.lang.String[r2]     // Catch:{ all -> 0x005e }
            r7.ak = r0     // Catch:{ all -> 0x005e }
        L_0x002f:
            r0 = 0
        L_0x0030:
            if (r0 >= r2) goto L_0x0046
            byte[] r3 = r7.aB     // Catch:{ all -> 0x005e }
            byte r4 = r1.readByte()     // Catch:{ all -> 0x005e }
            r3[r0] = r4     // Catch:{ all -> 0x005e }
            java.lang.String[] r3 = r7.ak     // Catch:{ all -> 0x005e }
            java.lang.String r4 = r1.readUTF()     // Catch:{ all -> 0x005e }
            r3[r0] = r4     // Catch:{ all -> 0x005e }
            int r0 = r0 + 1
            goto L_0x0030
        L_0x0045:
            r1 = r2
        L_0x0046:
            if (r1 == 0) goto L_0x004b
            r1.close()     // Catch:{ IOException -> 0x004c }
        L_0x004b:
            return
        L_0x004c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004b
        L_0x0051:
            r0 = move-exception
            r1 = r2
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0058:
            throw r0
        L_0x0059:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0058
        L_0x005e:
            r0 = move-exception
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.n.b(h):void");
    }

    private static void c(h hVar) {
        try {
            int a = a(3, at);
            if (a >= 0) {
                DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(at, a + 1, (at.length - a) - 1));
                int readByte = dataInputStream.readByte() & ToneControl.SILENCE;
                hVar.aC = (short[][]) Array.newInstance(Short.TYPE, readByte, 8);
                for (int i = 0; i < readByte; i++) {
                    hVar.aC[i][0] = (short) dataInputStream.readByte();
                    hVar.aC[i][1] = (short) dataInputStream.readByte();
                    hVar.aC[i][2] = dataInputStream.readShort();
                    hVar.aC[i][3] = dataInputStream.readShort();
                    hVar.aC[i][4] = dataInputStream.readShort();
                    hVar.aC[i][5] = dataInputStream.readShort();
                    hVar.aC[i][6] = (short) dataInputStream.readByte();
                    hVar.aC[i][7] = (short) dataInputStream.readByte();
                }
            }
        } catch (IOException e) {
            throw new Exception("读取RES数据出错");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00d6 A[SYNTHETIC, Splitter:B:27:0x00d6] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void d(defpackage.h r12) {
        /*
            r3 = 0
            r2 = 0
            r0 = 7
            byte[] r1 = defpackage.n.at     // Catch:{ all -> 0x00d2 }
            int r0 = a(r0, r1)     // Catch:{ all -> 0x00d2 }
            if (r0 < 0) goto L_0x00e1
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ all -> 0x00d2 }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x00d2 }
            byte[] r5 = defpackage.n.at     // Catch:{ all -> 0x00d2 }
            int r6 = r0 + 1
            byte[] r7 = defpackage.n.at     // Catch:{ all -> 0x00d2 }
            int r7 = r7.length     // Catch:{ all -> 0x00d2 }
            int r0 = r7 - r0
            int r0 = r0 + -1
            r4.<init>(r5, r6, r0)     // Catch:{ all -> 0x00d2 }
            r1.<init>(r4)     // Catch:{ all -> 0x00d2 }
            byte r0 = r1.readByte()     // Catch:{ all -> 0x00df }
            r4 = r0 & 255(0xff, float:3.57E-43)
            byte[][] r0 = new byte[r4][]     // Catch:{ all -> 0x00df }
            r12.aG = r0     // Catch:{ all -> 0x00df }
            r2 = r3
        L_0x002b:
            if (r2 >= r4) goto L_0x00c2
            short r5 = r1.readShort()     // Catch:{ all -> 0x00df }
            byte[][] r0 = r12.aG     // Catch:{ all -> 0x00df }
            int r6 = r5 * 3
            byte[] r6 = new byte[r6]     // Catch:{ all -> 0x00df }
            r0[r2] = r6     // Catch:{ all -> 0x00df }
            r0 = r3
        L_0x003a:
            int r6 = r5 * 3
            if (r0 >= r6) goto L_0x00bd
            byte r6 = r1.readByte()     // Catch:{ all -> 0x00df }
            byte r7 = r1.readByte()     // Catch:{ all -> 0x00df }
            byte r8 = r1.readByte()     // Catch:{ all -> 0x00df }
            byte[][] r9 = r12.aG     // Catch:{ all -> 0x00df }
            r9 = r9[r2]     // Catch:{ all -> 0x00df }
            r9[r0] = r6     // Catch:{ all -> 0x00df }
            byte[][] r9 = r12.aG     // Catch:{ all -> 0x00df }
            r9 = r9[r2]     // Catch:{ all -> 0x00df }
            int r10 = r0 + 1
            r9[r10] = r7     // Catch:{ all -> 0x00df }
            byte[][] r9 = r12.aG     // Catch:{ all -> 0x00df }
            r9 = r9[r2]     // Catch:{ all -> 0x00df }
            int r10 = r0 + 2
            r9[r10] = r8     // Catch:{ all -> 0x00df }
            if (r2 != 0) goto L_0x00b9
            boolean r9 = a(r12, r8)     // Catch:{ all -> 0x00df }
            if (r9 == 0) goto L_0x00b9
            java.util.Vector r9 = defpackage.n.au     // Catch:{ all -> 0x00df }
            java.lang.StringBuffer r10 = new java.lang.StringBuffer     // Catch:{ all -> 0x00df }
            r10.<init>()     // Catch:{ all -> 0x00df }
            java.lang.String r11 = ""
            java.lang.StringBuffer r10 = r10.append(r11)     // Catch:{ all -> 0x00df }
            java.lang.StringBuffer r6 = r10.append(r6)     // Catch:{ all -> 0x00df }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x00df }
            r9.addElement(r6)     // Catch:{ all -> 0x00df }
            java.util.Vector r6 = defpackage.n.au     // Catch:{ all -> 0x00df }
            java.lang.StringBuffer r9 = new java.lang.StringBuffer     // Catch:{ all -> 0x00df }
            r9.<init>()     // Catch:{ all -> 0x00df }
            java.lang.String r10 = ""
            java.lang.StringBuffer r9 = r9.append(r10)     // Catch:{ all -> 0x00df }
            java.lang.StringBuffer r7 = r9.append(r7)     // Catch:{ all -> 0x00df }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x00df }
            r6.addElement(r7)     // Catch:{ all -> 0x00df }
            java.util.Vector r6 = defpackage.n.au     // Catch:{ all -> 0x00df }
            java.lang.StringBuffer r7 = new java.lang.StringBuffer     // Catch:{ all -> 0x00df }
            r7.<init>()     // Catch:{ all -> 0x00df }
            java.lang.String r9 = ""
            java.lang.StringBuffer r7 = r7.append(r9)     // Catch:{ all -> 0x00df }
            java.lang.StringBuffer r7 = r7.append(r8)     // Catch:{ all -> 0x00df }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x00df }
            r6.addElement(r7)     // Catch:{ all -> 0x00df }
            byte[][] r6 = r12.aG     // Catch:{ all -> 0x00df }
            r6 = r6[r2]     // Catch:{ all -> 0x00df }
            int r7 = r0 + 2
            r8 = 0
            r6[r7] = r8     // Catch:{ all -> 0x00df }
        L_0x00b9:
            int r0 = r0 + 3
            goto L_0x003a
        L_0x00bd:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x002b
        L_0x00c2:
            java.util.Vector r0 = defpackage.n.au     // Catch:{ all -> 0x00df }
            a(r12, r0)     // Catch:{ all -> 0x00df }
        L_0x00c7:
            if (r1 == 0) goto L_0x00cc
            r1.close()     // Catch:{ IOException -> 0x00cd }
        L_0x00cc:
            return
        L_0x00cd:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00cc
        L_0x00d2:
            r0 = move-exception
            r1 = r2
        L_0x00d4:
            if (r1 == 0) goto L_0x00d9
            r1.close()     // Catch:{ IOException -> 0x00da }
        L_0x00d9:
            throw r0
        L_0x00da:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00d9
        L_0x00df:
            r0 = move-exception
            goto L_0x00d4
        L_0x00e1:
            r1 = r2
            goto L_0x00c7
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.n.d(h):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0071 A[SYNTHETIC, Splitter:B:21:0x0071] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void e(defpackage.h r13) {
        /*
            r8 = 0
            r1 = 0
            r0 = 11
            byte[] r2 = defpackage.n.at     // Catch:{ all -> 0x006e }
            int r0 = a(r0, r2)     // Catch:{ all -> 0x006e }
            if (r0 < 0) goto L_0x0062
            java.io.DataInputStream r6 = new java.io.DataInputStream     // Catch:{ all -> 0x006e }
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x006e }
            byte[] r3 = defpackage.n.at     // Catch:{ all -> 0x006e }
            int r4 = r0 + 1
            byte[] r5 = defpackage.n.at     // Catch:{ all -> 0x006e }
            int r5 = r5.length     // Catch:{ all -> 0x006e }
            int r0 = r5 - r0
            int r0 = r0 + -1
            r2.<init>(r3, r4, r0)     // Catch:{ all -> 0x006e }
            r6.<init>(r2)     // Catch:{ all -> 0x006e }
            byte r0 = r6.readByte()     // Catch:{ all -> 0x007a }
            r10 = r0 & 255(0xff, float:3.57E-43)
            aq[][] r0 = new defpackage.aq[r10][]     // Catch:{ all -> 0x007a }
            r13.aF = r0     // Catch:{ all -> 0x007a }
            r9 = r8
        L_0x002c:
            if (r9 >= r10) goto L_0x0063
            short r11 = r6.readShort()     // Catch:{ all -> 0x007a }
            aq[][] r0 = r13.aF     // Catch:{ all -> 0x007a }
            aq[] r1 = new defpackage.aq[r11]     // Catch:{ all -> 0x007a }
            r0[r9] = r1     // Catch:{ all -> 0x007a }
            r7 = r8
        L_0x0039:
            if (r7 >= r11) goto L_0x005e
            aq[][] r0 = r13.aF     // Catch:{ all -> 0x007a }
            r12 = r0[r9]     // Catch:{ all -> 0x007a }
            aq r0 = new aq     // Catch:{ all -> 0x007a }
            short r1 = r6.readShort()     // Catch:{ all -> 0x007a }
            short r2 = r6.readShort()     // Catch:{ all -> 0x007a }
            short r3 = r6.readShort()     // Catch:{ all -> 0x007a }
            short r4 = r6.readShort()     // Catch:{ all -> 0x007a }
            byte r5 = r6.readByte()     // Catch:{ all -> 0x007a }
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x007a }
            r12[r7] = r0     // Catch:{ all -> 0x007a }
            int r0 = r7 + 1
            r7 = r0
            goto L_0x0039
        L_0x005e:
            int r0 = r9 + 1
            r9 = r0
            goto L_0x002c
        L_0x0062:
            r6 = r1
        L_0x0063:
            if (r6 == 0) goto L_0x0068
            r6.close()     // Catch:{ IOException -> 0x0069 }
        L_0x0068:
            return
        L_0x0069:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0068
        L_0x006e:
            r0 = move-exception
        L_0x006f:
            if (r1 == 0) goto L_0x0074
            r1.close()     // Catch:{ IOException -> 0x0075 }
        L_0x0074:
            throw r0
        L_0x0075:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0074
        L_0x007a:
            r0 = move-exception
            r1 = r6
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.n.e(h):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0068 A[SYNTHETIC, Splitter:B:22:0x0068] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void f(defpackage.h r10) {
        /*
            r2 = 0
            r1 = 0
            r0 = 9
            byte[] r3 = defpackage.n.at     // Catch:{ Exception -> 0x0058, all -> 0x0071 }
            int r0 = a(r0, r3)     // Catch:{ Exception -> 0x0058, all -> 0x0071 }
            if (r0 < 0) goto L_0x0057
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0058, all -> 0x0071 }
            byte[] r3 = defpackage.n.at     // Catch:{ Exception -> 0x0058, all -> 0x0071 }
            int r5 = r0 + 1
            byte[] r6 = defpackage.n.at     // Catch:{ Exception -> 0x0058, all -> 0x0071 }
            int r6 = r6.length     // Catch:{ Exception -> 0x0058, all -> 0x0071 }
            int r0 = r6 - r0
            int r0 = r0 + -1
            r4.<init>(r3, r5, r0)     // Catch:{ Exception -> 0x0058, all -> 0x0071 }
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0058, all -> 0x0071 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0058, all -> 0x0071 }
            byte r1 = r0.readByte()     // Catch:{ Exception -> 0x0073 }
            r5 = r1 & 255(0xff, float:3.57E-43)
            short[][] r1 = new short[r5][]     // Catch:{ Exception -> 0x0073 }
            r10.aE = r1     // Catch:{ Exception -> 0x0073 }
            r3 = r2
        L_0x002c:
            if (r3 >= r5) goto L_0x0051
            short r6 = r0.readShort()     // Catch:{ Exception -> 0x0073 }
            short[][] r1 = r10.aE     // Catch:{ Exception -> 0x0073 }
            int r7 = r6 * 3
            short[] r7 = new short[r7]     // Catch:{ Exception -> 0x0073 }
            r1[r3] = r7     // Catch:{ Exception -> 0x0073 }
            r1 = r2
        L_0x003b:
            int r7 = r6 * 3
            if (r1 >= r7) goto L_0x004d
            short[][] r7 = r10.aE     // Catch:{ Exception -> 0x0073 }
            r7 = r7[r3]     // Catch:{ Exception -> 0x0073 }
            byte r8 = r0.readByte()     // Catch:{ Exception -> 0x0073 }
            short r8 = (short) r8     // Catch:{ Exception -> 0x0073 }
            r7[r1] = r8     // Catch:{ Exception -> 0x0073 }
            int r1 = r1 + 1
            goto L_0x003b
        L_0x004d:
            int r1 = r3 + 1
            r3 = r1
            goto L_0x002c
        L_0x0051:
            r0.close()     // Catch:{ Exception -> 0x0073 }
            r4.close()     // Catch:{ Exception -> 0x0073 }
        L_0x0057:
            return
        L_0x0058:
            r0 = move-exception
            r0 = r1
        L_0x005a:
            java.lang.Exception r1 = new java.lang.Exception     // Catch:{ all -> 0x0062 }
            java.lang.String r2 = "读取区域层出错"
            r1.<init>(r2)     // Catch:{ all -> 0x0062 }
            throw r1     // Catch:{ all -> 0x0062 }
        L_0x0062:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0066:
            if (r1 == 0) goto L_0x006b
            r1.close()     // Catch:{ IOException -> 0x006c }
        L_0x006b:
            throw r0
        L_0x006c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006b
        L_0x0071:
            r0 = move-exception
            goto L_0x0066
        L_0x0073:
            r1 = move-exception
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.n.f(h):void");
    }

    public static h h(String str) {
        h hVar = new h();
        byte[] i = i(str);
        at = i;
        if (i == null) {
            throw new Exception("读取地图数据失败");
        }
        au = new Vector(15);
        a(hVar);
        b(hVar);
        c(hVar);
        d(hVar);
        f(hVar);
        e(hVar);
        at = null;
        au.removeAllElements();
        au = null;
        System.gc();
        return hVar;
    }

    private static byte[] i(String str) {
        byte[] bArr;
        IOException e;
        try {
            InputStream b = MIDPHelper.b("".getClass(), str);
            int available = b.available();
            if (available <= 0) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(256);
                byte[] bArr2 = new byte[256];
                while (b.read(bArr2) != -1) {
                    byteArrayOutputStream.write(bArr2);
                }
                bArr = byteArrayOutputStream.toByteArray();
            } else {
                byte[] bArr3 = new byte[available];
                try {
                    b.read(bArr3);
                    bArr = bArr3;
                } catch (IOException e2) {
                    IOException iOException = e2;
                    bArr = bArr3;
                    e = iOException;
                    e.printStackTrace();
                    return bArr;
                }
            }
            try {
                b.close();
            } catch (IOException e3) {
                e = e3;
            }
        } catch (IOException e4) {
            IOException iOException2 = e4;
            bArr = null;
            e = iOException2;
        }
        return bArr;
    }
}
