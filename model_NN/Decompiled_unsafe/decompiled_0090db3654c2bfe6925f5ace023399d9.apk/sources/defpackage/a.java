package defpackage;

import com.a.a.e.c;
import com.a.a.e.o;
import com.a.a.e.z;
import com.a.a.i.k;
import com.a.a.p.f;

/* renamed from: a  reason: default package */
final class a extends c {
    public static int[] X = {40, 30};
    private static int[] Z = new int[2];
    private static int a;
    private static int b;
    public static int e;
    private l Y;

    a(l lVar) {
        Z[0] = 0;
        Z[1] = 1;
        i(true);
        this.Y = lVar;
        d(getWidth(), getHeight());
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        l.a(oVar, 0);
        l lVar = this.Y;
        if (lVar.an) {
            oVar.setColor(0);
            oVar.fillRect(0, 0, 240, 320);
            oVar.setColor(z.CONSTRAINT_MASK);
            oVar.a("按任意键继续", 120, (int) f.OBEX_HTTP_OK, 17);
        } else if (lVar.aO != null && lVar.aO.kB() != null && lVar.k) {
            oVar.a(m.aP);
            lVar.aO.kB().b(oVar);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(int i) {
        if (i == -1) {
            e &= -4097;
        } else if (i == -2) {
            e &= -8193;
        } else if (i == -3) {
            e &= -16385;
        } else if (i == -4) {
            e &= -32769;
        } else if (i == -5) {
            e &= -65537;
        } else if (i == -6) {
            e &= -131073;
        } else if (i == -7) {
            e &= -262145;
        }
        switch (i) {
            case c.KEY_POUND /*35*/:
                e &= -2049;
                return;
            case 36:
            case 37:
            case 38:
            case 39:
            case com.a.a.b.c.BOOL /*40*/:
            case 41:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            default:
                return;
            case c.KEY_STAR /*42*/:
                e &= -1025;
                return;
            case 48:
                e &= -2;
                return;
            case c.KEY_NUM1 /*49*/:
                e &= -3;
                return;
            case 50:
                e &= -5;
                return;
            case c.KEY_NUM3 /*51*/:
                e &= -9;
                return;
            case c.KEY_NUM4 /*52*/:
                e &= -17;
                return;
            case c.KEY_NUM5 /*53*/:
                e &= -33;
                return;
            case c.KEY_NUM6 /*54*/:
                e &= -65;
                return;
            case c.KEY_NUM7 /*55*/:
                e &= -129;
                return;
            case 56:
                e &= -257;
                return;
            case c.KEY_NUM9 /*57*/:
                e &= -513;
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void b(int i, int i2) {
        if (!ah.a(i, i2, 0, a - X[1], X[0], X[1])) {
            if (!ah.a(i, i2, b - X[0], a - X[1], X[0], X[1])) {
                if (!ah.a(i, i2, b - 30, a - 100, 30, 30)) {
                    Z[0] = i;
                    Z[1] = i2;
                } else if (!m.o.equals("E680") && !m.o.equals("E6")) {
                    Z[0] = i;
                    Z[1] = i2;
                } else if (y.an && !k.k) {
                    e |= 262144;
                }
            } else if (!m.o.equals("E680") && !m.o.equals("E6")) {
                e |= 262144;
            } else if (!y.an) {
                e |= 262144;
            } else if (k.k) {
                e |= 262144;
            }
        } else if (!m.o.equals("E680") && !m.o.equals("E6")) {
            e |= 131072;
        } else if (!y.an) {
            e |= 131072;
        } else if (k.k) {
            e |= 131072;
        }
    }

    /* access modifiers changed from: protected */
    public final void c(int i, int i2) {
        if (ah.a(i, i2, 0, a - X[1], X[0], X[1])) {
            e &= -131073;
            return;
        }
        if (ah.a(i, i2, b - X[0], a - X[1], X[0], X[1])) {
            e &= -262145;
            return;
        }
        Z[0] = 0;
        Z[1] = 0;
    }

    /* access modifiers changed from: protected */
    public final void d(int i, int i2) {
        b = i;
        a = i2;
    }

    /* access modifiers changed from: protected */
    public final void i() {
        this.Y.v();
    }

    /* access modifiers changed from: protected */
    public final void j() {
        this.Y.q();
    }

    /* access modifiers changed from: protected */
    public final void keyPressed(int i) {
        if (i == -1) {
            e |= 4096;
        } else if (i == -2) {
            e |= 8192;
        } else if (i == -3) {
            e |= 16384;
        } else if (i == -4) {
            e |= k.MONDAY;
        } else if (i == -5) {
            e |= 65536;
        } else if (i == -6) {
            e |= 131072;
        } else if (i == -7) {
            e |= 262144;
        }
        switch (i) {
            case c.KEY_POUND /*35*/:
                e |= 2048;
                return;
            case 36:
            case 37:
            case 38:
            case 39:
            case com.a.a.b.c.BOOL /*40*/:
            case 41:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            default:
                return;
            case c.KEY_STAR /*42*/:
                e |= 1024;
                return;
            case 48:
                e |= 1;
                return;
            case c.KEY_NUM1 /*49*/:
                e |= 2;
                return;
            case 50:
                e |= 4;
                return;
            case c.KEY_NUM3 /*51*/:
                e |= 8;
                return;
            case c.KEY_NUM4 /*52*/:
                e |= 16;
                return;
            case c.KEY_NUM5 /*53*/:
                e |= 32;
                return;
            case c.KEY_NUM6 /*54*/:
                e |= 64;
                return;
            case c.KEY_NUM7 /*55*/:
                e |= 128;
                return;
            case 56:
                e |= 256;
                return;
            case c.KEY_NUM9 /*57*/:
                e |= 512;
                return;
        }
    }
}
