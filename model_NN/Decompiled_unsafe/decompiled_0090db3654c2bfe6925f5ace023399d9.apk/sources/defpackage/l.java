package defpackage;

import com.a.a.e.j;
import com.a.a.e.k;
import com.a.a.e.o;
import main.Main;

/* renamed from: l  reason: default package */
public final class l implements Runnable {
    private static l Y;
    private int a;
    private boolean aK;
    private Main aL;
    private boolean aM;
    private a aN = new a(this);
    public ar aO;
    private c af = c.k();
    public boolean am;
    boolean an;
    private volatile boolean ao;
    private int e = 50;
    public boolean k;

    private l() {
    }

    public static void a(int i, int i2) {
        a.X[0] = i;
        a.X[1] = i2;
    }

    public static void a(o oVar, int i) {
        oVar.setColor(i);
        oVar.setClip(0, 0, 240, 320);
        oVar.fillRect(0, 0, 240, 320);
    }

    public static void b() {
        a.e = 0;
    }

    public static l t() {
        if (Y == null) {
            Y = new l();
        }
        return Y;
    }

    public final void a() {
        this.ao = false;
    }

    public final void a(int i) {
        this.e = 60;
    }

    public final void a(String str, int i) {
        if (i == 0) {
            i = 1;
        } else if (i < 0) {
            i = -1;
        }
        this.af.a(str, i);
    }

    public final void a(Main main2) {
        this.aK = false;
        this.a = 0;
        this.aO = new ap();
        if (this.aO != null) {
            this.aO.b();
        }
        this.aL = main2;
        j.a(this.aL).a((k) this.aN);
        new Thread(this).start();
    }

    public final void b(boolean z) {
        if (z) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
        this.aM = z;
    }

    public final boolean h(int i) {
        return !this.aM && (a.e & i) != 0;
    }

    public final void o() {
        this.af.b();
    }

    public final int p() {
        return this.e;
    }

    public final void q() {
        this.an = true;
        a.e = 0;
        b(true);
        this.af.b();
    }

    public final void run() {
        this.ao = true;
        while (this.ao) {
            this.a++;
            long currentTimeMillis = System.currentTimeMillis();
            if (this.an) {
                if (!this.aM && a.e != 0) {
                    this.an = false;
                    if (y.I().cb) {
                        y.I();
                        if (y.cc) {
                            a(y.I().t, y.I().p);
                        }
                    }
                    a.e = 0;
                }
            } else if (!(this.aO == null || this.aO.kB() == null || !this.k)) {
                this.aO.kB().o();
                this.aO.kB().q();
            }
            this.aN.aX();
            this.aN.aY();
            long currentTimeMillis2 = ((long) this.e) - (System.currentTimeMillis() - currentTimeMillis);
            if (currentTimeMillis2 > 0) {
                try {
                    Thread.sleep(currentTimeMillis2);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        this.aO.a();
        this.aL.fq();
    }

    public final int u() {
        return this.a;
    }

    public final void v() {
        b(false);
        if (y.I().cg) {
            this.an = false;
            if (y.I().cb) {
                y.I();
                if (y.cc) {
                    a(y.I().t, y.I().p);
                }
            }
        }
    }
}
