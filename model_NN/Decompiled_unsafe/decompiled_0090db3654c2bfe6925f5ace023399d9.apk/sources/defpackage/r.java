package defpackage;

/* renamed from: r  reason: default package */
public final class r {
    public int[] X;
    public int[] Z;
    public String[] ak;
    public int[] aq;

    public final String i(int i) {
        return this.ak[this.aq[i]];
    }

    public final int[] j(int i) {
        int i2 = this.aq[i];
        int i3 = this.X[i2];
        int i4 = this.X[i2 + 1];
        int[] iArr = new int[i4];
        for (int i5 = 0; i5 < i4; i5++) {
            iArr[i5] = this.Z[i3 + i5];
        }
        return iArr;
    }
}
