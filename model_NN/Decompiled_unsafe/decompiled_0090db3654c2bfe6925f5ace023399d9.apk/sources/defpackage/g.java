package defpackage;

import com.a.a.e.o;
import com.a.a.e.p;
import com.a.a.e.z;
import java.io.IOException;

/* renamed from: g  reason: default package */
public final class g implements o {
    private l Y = l.t();
    private int a = 0;
    private p ah;
    private String[] ak;
    private p av;
    private p[] aw;
    private as[] ax;
    private int b = 0;
    private int e = 30;
    private int h = 0;
    private int i = 0;

    public final void a() {
        this.ak = null;
        this.ah = null;
        y.I();
        if (y.ce) {
            this.aw = null;
            for (as b2 : this.ax) {
                b2.b();
            }
            this.av = null;
        }
    }

    public final void b() {
        String ao = ah.ao("/txt/story1.txt");
        if (!y.ce) {
            this.ah = ah.n("/sys/menuElements/back1.ps");
            this.ak = ah.e(ao, "story1:", "story1End");
        } else {
            this.aw = new p[4];
            try {
                this.aw[0] = p.C("/ui/battlebackE1.png");
                this.aw[1] = p.C("/ui/battlebackE2.png");
                this.aw[2] = p.C("/ui/battlebackE3.png");
                this.aw[3] = p.C("/ui/battlebackE4.png");
                this.ah = p.C("/ui/battlebackE3.png");
                this.av = p.C("/ui/endword.png");
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.ax = new as[3];
            for (int i2 = 0; i2 < this.ax.length; i2++) {
                this.ax[i2] = new as("/effect/effect_huo.ani", "/effect/");
            }
            this.ak = ah.e(ao, "story2:", "story2End");
        }
        this.b = 320;
        this.h = 0;
        if (y.ce) {
            this.a = 150;
        } else {
            this.a = 0;
        }
        this.Y.k = true;
    }

    public final void b(o oVar) {
        if (y.ce) {
            oVar.setClip(-10, 0, 260, 320);
            this.i++;
            int width = 0 - this.aw[3].getWidth();
            for (int i2 = 0; i2 < 2; i2++) {
                oVar.a(this.aw[0], -10 - (this.aw[0].getWidth() * i2), 0, 20);
            }
            oVar.a(this.aw[1], -10, 27, 20);
            oVar.setClip(0, 219, 240, 101);
            ah.c(oVar, this.ah, 0, 320, 36);
            oVar.setClip(0, 0, 240, 320);
            if (this.i % 20 < 5) {
                oVar.a(this.aw[3], width + (this.i % ((this.aw[3].getWidth() + 240) + 20)), 56, 20);
            } else if (this.i % 20 < 10 || this.i % 20 >= 15) {
                oVar.a(this.aw[3], width + (this.i % ((this.aw[3].getWidth() + 240) + 20)), 57, 20);
            } else {
                oVar.a(this.aw[3], width + (this.i % ((this.aw[3].getWidth() + 240) + 20)), 58, 20);
            }
            oVar.a(this.aw[2], -10, 108, 20);
            ah.c(oVar, this.av, 120, 15, 17);
            for (int i3 = 0; i3 < this.ax.length; i3++) {
                this.ax[i3].f(0);
                switch (i3) {
                    case 0:
                        this.ax[i3].a(oVar, 0, 37, 137, false);
                        break;
                    case 1:
                        this.ax[i3].a(oVar, 0, 124, 137, false);
                        break;
                    case 2:
                        this.ax[i3].a(oVar, 0, 204, 137, false);
                        break;
                }
            }
        } else if (this.ah != null) {
            ah.c(oVar, this.ah, 120, 13, 17);
        }
        if (this.ak != null) {
            oVar.setClip(0, this.a, 240, Math.abs(this.a - this.b));
            for (int i4 = 0; i4 < this.ak.length; i4++) {
                oVar.setColor(16777215);
                oVar.a(this.ak[i4], 120, this.b + this.h + (this.e * i4), 17);
                oVar.setColor(12303291);
                oVar.a(this.ak[i4], 121, this.b + this.h + (this.e * i4), 17);
            }
        }
        oVar.setColor(z.CONSTRAINT_MASK);
        oVar.a("跳过", 235, 320, 40);
    }

    public final void o() {
        if (!this.Y.h(262144)) {
            return;
        }
        if (y.ce) {
            j.r().k = false;
            a();
            y.I().d((byte) 2);
            y.ce = false;
            return;
        }
        y.I().d((byte) 4);
    }

    public final void q() {
        this.h -= 2;
        if (Math.abs(this.h) > (this.ak.length * this.e) + 320) {
            y.I();
            if (y.ce) {
                j.r().k = false;
                y.I().a();
                y.I().d((byte) 2);
                y.ce = false;
                return;
            }
            y.I().d((byte) 4);
        }
    }
}
