package defpackage;

import com.a.a.e.o;
import com.a.a.e.p;
import com.a.a.e.z;
import com.a.a.i.k;
import com.a.a.k.b;
import com.a.a.p.f;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.IOException;
import javax.microedition.media.control.MIDIControl;
import javax.microedition.media.control.ToneControl;
import main.Main;

/* renamed from: v  reason: default package */
public final class v implements o {
    private l Y = l.t();
    private int a;
    private byte aA = 0;
    private y aQ = y.I();
    private p ah;
    private byte aj;
    private String[] ak;
    private String[] al;
    private boolean am;
    private boolean an;
    private boolean ao;
    private p av;
    private int b;
    private as bh;
    private p bi;
    private byte bj;
    private byte bk;
    private byte bl = 0;
    private byte bm = 30;
    private aa bn = new aa();
    private final int e = (ah.e + 3);
    private int h;
    private int i;
    private boolean k;
    private String o;
    private String r;
    private String s;
    private String t;
    private String u;

    private static void a(String str) {
        try {
            Main.fp().U(str);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void a() {
        this.bn.av = null;
        aa.dJ = null;
        aa.dK = null;
        this.bn.dL = null;
        this.bn.dM = null;
        this.bn.bi = null;
        this.bn.ah = null;
    }

    public final void b() {
        this.ah = ah.n("/sys/exit.png");
        this.k = true;
        this.aQ.cb = true;
        this.o = "/role/start.ani";
        this.r = "/role/";
        this.bj = 0;
        this.a = 0;
        this.ao = true;
        this.bj = 0;
        if (this.k) {
            this.bh = new as(this.o, this.r);
        }
        try {
            this.av = p.C("/ui/mainMenuWord.png");
            this.bn.ah = p.C("/ui/longwen.png");
            this.bi = p.C("/ui/menu1.png");
            this.bn.av = p.C("/ui/word.png");
            aa.dJ = p.C("/ui/kuang5.png");
            aa.dK = p.C("/ui/kuang6.png");
            this.bn.dL = p.C("/ui/kuang7.png");
            this.bn.dM = p.C("/ui/kuang8.png");
            this.bn.bi = p.C("/ui/kuang4.png");
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.bj = 20;
        this.bj = ToneControl.C4;
        String ao2 = ah.ao("/sys/m.ps");
        String ao3 = ah.ao("/txt/about.txt");
        this.al = ah.b(ah.d(ao3, "help:", "helpEnd"), "\r\n", 220);
        this.ak = ah.b(ah.d(ao3, "aboutUs:", "aboutUsEnd"), "\r\n", 220);
        this.s = ah.c(ao2, "addMenu");
        if (this.s == null || this.s.equals("")) {
            this.an = false;
        } else {
            this.an = true;
            this.t = ah.c(ao2, "addMenuLink");
        }
        this.bj = 100;
        this.h = this.bi.getWidth() + 39;
        this.aQ.t = "/music/piantou.mid";
        this.aQ.p = -1;
        if (y.cc) {
            this.Y.a(this.aQ.t, this.aQ.p);
        }
        if (this.Y.am) {
            this.aj = 3;
        } else {
            this.aj = 2;
            this.Y.am = true;
        }
        this.Y.k = true;
        System.gc();
    }

    public final void b(o oVar) {
        l.a(oVar, 0);
        oVar.setColor(16777215);
        switch (this.aj) {
            case 1:
                oVar.setColor(16777215);
                oVar.a("是否开启音乐？", 120, (int) f.OBEX_HTTP_OK, 33);
                oVar.setColor(z.CONSTRAINT_MASK);
                oVar.a("开", 2, 318, 36);
                oVar.a("关", 238, 318, 40);
                break;
            case 2:
                this.bh.a(oVar, 0, 120, f.OBEX_HTTP_OK, false);
                break;
            case 3:
                this.bh.a(oVar, 1, 120, f.OBEX_HTTP_OK, false);
                this.bi.getWidth();
                this.bi.getWidth();
                int height = (this.bi.getHeight() + 200) - 36;
                oVar.setColor(1579542);
                oVar.fillRect(79, height, 83, 36);
                oVar.setColor(12697272);
                oVar.fillRect(79, height + 1, 83, 34);
                oVar.setColor(1579542);
                oVar.fillRect(79, height + 2, 83, 32);
                oVar.setColor(5446417);
                oVar.fillRect(79, height + 3, 83, 30);
                aa.a(oVar, 83, height + 5, 74, 26, 15247695, 12331540, 1, 1, this.bn.ah, 2, 1, 0, this.bn.ah.getWidth(), 0, 0, this.bn.dM, 1, 1, 1, 1, 2, 0);
                oVar.setClip(0, height + 12, 240, this.av.getHeight() / 7);
                oVar.a(this.av, 120, (height + 12) - ((this.a * this.av.getHeight()) / 7), 17);
                oVar.setClip(0, 0, 240, 320);
                oVar.a(this.bi, 0, 0, this.bi.getWidth(), this.bi.getHeight(), 0, 120 - this.h, 200, 20);
                oVar.a(this.bi, 0, 0, this.bi.getWidth(), this.bi.getHeight(), 2, this.h + 120, 200, 24);
                break;
            case 5:
                ae.a(oVar, 0, 0, 240, 320, 0, 9209989, 0, 5446417, aa.dJ, 1, 0, 0, 1, 3, 1);
                this.i = 3;
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= this.i) {
                        break;
                    } else {
                        if (this.bl == i3) {
                            ae.a(oVar, 20, (i3 * 70) + this.bm + 30 + 10, 200, 60, 5592337, 11181090, 1, 1, this.bn.dM, 1, 1, 1, 1, 2, 0);
                        } else {
                            ae.a(oVar, 20, (i3 * 70) + this.bm + 30 + 10, 200, 60, 15247695, 12331540, 1, 1, this.bn.dM, 1, 1, 1, 1, 2, 0);
                        }
                        oVar.setColor(16777215);
                        if (!y.I().ca) {
                            oVar.a(new StringBuffer().append("存档").append(i3 + 1).append(":").toString(), 120, this.bm + 30 + 20 + (i3 * 70), 17);
                        } else if (y.I().at[i3] == 1) {
                            String stringBuffer = new StringBuffer().append(y.I().cS[i3]).append(" 等级 ").append((int) y.I().aD[i3]).toString();
                            String stringBuffer2 = new StringBuffer().append((int) y.I().cT[i3]).append("/").append((int) y.I().aB[i3]).append("/").append((int) y.I().cU[i3]).append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR).append((int) y.I().cV[i3]).append(":").append((int) y.I().cW[i3]).append(":").append((int) y.I().cX[i3]).toString();
                            oVar.a(stringBuffer, 120, this.bm + 30 + 20 + (i3 * 70), 17);
                            oVar.a(stringBuffer2, 120, this.bm + 30 + 20 + (i3 * 70) + ah.e, 17);
                        } else {
                            oVar.a(new StringBuffer().append("存档").append(i3 + 1).append(":").toString(), 120, this.bm + 30 + 20 + (i3 * 70), 17);
                            oVar.a("", 120, this.bm + 30 + 20 + (i3 * 70) + ah.e, 17);
                        }
                        i2 = i3 + 1;
                    }
                }
            case 6:
                ae.a(oVar, 0, 0, 240, 320, 0, 9209989, 0, 5446417, aa.dJ, 1, 0, 0, 1, 3, 1);
                ah.aP.getHeight();
                oVar.setColor(z.CONSTRAINT_MASK);
                ae.a(oVar, 34, 77, 172, f.OBEX_HTTP_ACCEPTED, 15247695, 12331540, 1, 1, this.bn.dM, 1, 1, 1, 1, 2, 0);
                oVar.setColor(16777215);
                oVar.a("声音设置", 120, 80, 17);
                oVar.a("开", 80, 145, 24);
                oVar.a("关", (int) f.OBEX_HTTP_OK, 145, 20);
                oVar.setClip(0, 0, 240, 320);
                aa.Q();
                if (this.aA != 0) {
                    aa.a(159, (int) MIDIControl.NOTE_ON, ah.aP.A("开") + 1, ah.e + 1, 65280, 0, oVar, aa.aa);
                    break;
                } else {
                    aa.a((80 - ah.aP.A("开")) - 1, (int) MIDIControl.NOTE_ON, ah.aP.A("开") + 1, ah.e + 1, 65280, 0, oVar, aa.aa);
                    break;
                }
            case 7:
                ae.a(oVar, 0, 0, 240, 320, 0, 9209989, 0, 5446417, aa.dJ, 1, 0, 0, 1, 3, 1);
                oVar.setColor(16777215);
                oVar.a("游戏帮助", 120, 50, 33);
                for (int i4 = 0; i4 < this.al.length; i4++) {
                    oVar.a(this.al[i4], 10, (this.e * i4) + 55, 20);
                }
                break;
            case 8:
                ae.a(oVar, 0, 0, 240, 320, 0, 9209989, 0, 5446417, aa.dJ, 1, 0, 0, 1, 3, 1);
                oVar.setColor(16777215);
                oVar.a("关于", 120, 50, 33);
                for (int i5 = 0; i5 < this.ak.length; i5++) {
                    oVar.a(this.ak[i5], 10, (this.e * i5) + 55, 20);
                }
                break;
            case 10:
                oVar.a(this.ah, 120, 160 - (this.ah.getHeight() / 2), 17);
                oVar.a("确认退出", 120, (int) f.OBEX_HTTP_OK, 17);
                oVar.a("是", 2, 318, 36);
                oVar.a("否", 238, 318, 40);
                break;
            case 11:
                oVar.a(this.ah, 120, 160 - (this.ah.getHeight() / 2), 17);
                oVar.a("更多精彩游戏", 120, 140, 17);
                oVar.a("尽在游戏平道", 120, ah.e + 140 + 3, 17);
                oVar.a("wap.xjoys.com", 120, (ah.e << 1) + 140 + 3, 17);
                oVar.a("是", 2, 318, 36);
                oVar.a("否", 238, 318, 40);
                break;
        }
        if (this.am) {
            ah.a(oVar, this.u, 120, f.OBEX_HTTP_OK, 16711680, 16777215, 17);
        }
    }

    public final void o() {
        switch (this.aj) {
            case 1:
                if (this.Y.h(131072)) {
                    y.cc = true;
                    this.Y.a(this.aQ.t, this.aQ.p);
                    this.aA = 0;
                    if (this.k) {
                        this.aj = 2;
                    } else {
                        this.aj = 3;
                    }
                } else if (this.Y.h(262144)) {
                    y.cc = false;
                    this.aA = 1;
                    if (this.k) {
                        this.aj = 2;
                    } else {
                        this.aj = 3;
                    }
                }
                l.b();
                break;
            case 2:
                if (this.Y.h(65536) || this.Y.h(32) || this.Y.h(131072)) {
                    if (y.cc) {
                        this.Y.a(this.aQ.t, this.aQ.p);
                    }
                    this.aj = 3;
                }
                l.b();
                break;
            case 3:
                if (this.Y.h(16384) || this.Y.h(16)) {
                    if (this.a > 0) {
                        this.a--;
                        if (!this.an && this.a == 2) {
                            this.a--;
                        }
                        if (!this.ao && this.a == 3) {
                            this.a--;
                        }
                    } else {
                        this.a = 6;
                    }
                    this.h = this.bi.getWidth();
                } else if (this.Y.h(k.MONDAY) || this.Y.h(64)) {
                    if (this.a < 6) {
                        this.a++;
                        if (!this.an && this.a == 2) {
                            this.a++;
                        }
                        if (!this.ao && this.a == 3) {
                            this.a++;
                        }
                    } else {
                        this.a = 0;
                    }
                    this.h = this.bi.getWidth();
                } else if (this.Y.h(65536) || this.Y.h(32) || this.Y.h(131072)) {
                    switch (this.a) {
                        case 0:
                            y.I().v();
                            this.bh.b();
                            this.bh = null;
                            this.aj = 4;
                            break;
                        case 1:
                            y.I().P();
                            y.I().v();
                            this.aj = 5;
                            break;
                        case 2:
                            this.aj = 9;
                            break;
                        case 3:
                            this.aj = 6;
                            break;
                        case 4:
                            this.aj = 7;
                            break;
                        case 5:
                            this.aj = 8;
                            break;
                        case 6:
                            this.aj = 10;
                            break;
                    }
                    this.h = this.bi.getWidth() + 39;
                }
                l.b();
                break;
            case 4:
            case 9:
                break;
            case 5:
                if (this.Y.h(262144)) {
                    this.aj = 3;
                    this.bl = 0;
                } else if (this.Y.h(4096) || this.Y.h(4)) {
                    if (this.bl > 0) {
                        this.bl = (byte) (this.bl - 1);
                    }
                } else if (this.Y.h(8192) || this.Y.h(256)) {
                    if (this.bl < this.i - 1) {
                        this.bl = (byte) (this.bl + 1);
                    }
                } else if (this.Y.h(65536) || this.Y.h(32)) {
                    if (y.I().at[this.bl] == 1) {
                        y.I().am = true;
                        y.I().D(this.bl);
                        y.I().Q();
                        y.I().d((byte) 4);
                        this.bh.b();
                        this.bh = null;
                    } else {
                        this.am = true;
                        this.u = "没有存档";
                        this.bl = 0;
                    }
                }
                l.b();
                break;
            case 6:
                if (this.Y.h(65536) || this.Y.h(32) || this.Y.h(131072)) {
                    if (this.aA == 0) {
                        y.cc = true;
                        this.Y.a(this.aQ.t, this.aQ.p);
                    } else {
                        this.Y.o();
                        y.cc = false;
                    }
                } else if (this.Y.h(262144)) {
                    this.aj = 3;
                } else if (this.Y.h(4096) || this.Y.h(4) || this.Y.h(16384) || this.Y.h(16)) {
                    this.aA = 0;
                } else if (this.Y.h(8192) || this.Y.h(256) || this.Y.h(k.MONDAY) || this.Y.h(64)) {
                    this.aA = 1;
                }
                l.b();
                break;
            case 7:
                if (this.Y.h(65536) || this.Y.h(32) || this.Y.h(131072)) {
                    l.b();
                    break;
                } else if (this.Y.h(262144)) {
                    this.aj = 3;
                    l.b();
                    break;
                } else if (this.Y.h(4096) || this.Y.h(4)) {
                    l.b();
                    break;
                } else if (this.Y.h(8192) || this.Y.h(256)) {
                }
                break;
            case 8:
                if (!this.Y.h(65536) && !this.Y.h(32) && !this.Y.h(131072) && this.Y.h(262144)) {
                    this.aj = 3;
                }
                l.b();
                break;
            case 10:
                if (this.Y.h(131072)) {
                    this.aj = b.BROKEN_CHAIN;
                }
                if (this.Y.h(262144)) {
                    this.aj = 3;
                    break;
                }
                break;
            case 11:
                if (this.Y.h(131072)) {
                    a(this.t);
                    this.Y.a();
                }
                if (this.Y.h(262144)) {
                    this.Y.a();
                    break;
                }
                break;
            default:
                return;
        }
        l.b();
    }

    public final void q() {
        if (this.b < 10) {
            this.b++;
        } else {
            this.b = 0;
            this.am = false;
        }
        switch (this.aj) {
            case 0:
                if (this.bk < this.bj) {
                    this.bk = (byte) (this.bk + 10);
                    if (this.bk > this.bj) {
                        this.bk = this.bj;
                        return;
                    }
                    return;
                }
                this.bk = 0;
                this.bj = 0;
                if (this.aQ.cb) {
                    this.aj = 1;
                } else if (this.k) {
                    this.aj = 2;
                } else {
                    this.aj = 3;
                }
                this.bj = 0;
                return;
            case 1:
                if (y.cc) {
                }
                return;
            case 2:
                if (this.bh.f(0) >= this.bh.k(0) - 1) {
                    if (y.cc) {
                        this.Y.a(this.aQ.t, this.aQ.p);
                    }
                    this.bh.a(1, 0);
                    this.aj = 3;
                    return;
                }
                return;
            case 3:
                this.bh.f(1);
                if (this.h < this.bi.getWidth() + 39) {
                    this.h += 2;
                    return;
                }
                return;
            case 4:
                this.aQ.d((byte) 3);
                return;
            case 5:
            case 6:
            case 7:
            case 8:
            case 10:
            default:
                return;
            case 9:
                a(this.t);
                this.Y.a();
                return;
        }
    }
}
