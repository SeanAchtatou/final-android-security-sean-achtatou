package defpackage;

import com.a.a.e.o;
import com.a.a.p.f;

/* renamed from: p  reason: default package */
public final class p implements o {
    private int[] X = {1};
    private l Y = l.t();
    private int[] Z;
    private int a = 0;
    private y aQ = y.I();
    private int aa;
    private com.a.a.e.p ah;
    private com.a.a.e.p[] aw;
    private as[] ax;
    private int b = 0;
    private int e;
    private int h = 0;
    private int i;
    private boolean k;

    public final void a() {
        if (this.ax != null) {
            for (as b2 : this.ax) {
                b2.b();
            }
        }
        this.aw = null;
    }

    public final void b() {
        this.ah = ah.n("/sys/bg.png");
        String ao = ah.ao("/sys/m.ps");
        String[] p = ah.p(ao, "logo");
        if (p == null) {
            y.I().d((byte) 2);
            return;
        }
        int[] o = ah.o(ao, "logoTime");
        while (o.length < p.length) {
            o = ah.b(o, o[0]);
        }
        this.Z = new int[o.length];
        for (int i2 = 0; i2 < p.length; i2++) {
            if (p != null) {
                if (p[i2].endsWith(".ps") || p[i2].endsWith(".png") || p[i2].endsWith(".jpg")) {
                    this.i++;
                } else if (p[i2].endsWith(".ani")) {
                    this.aa++;
                }
            }
        }
        if (this.i > 0) {
            this.aw = new com.a.a.e.p[this.i];
            this.i = 0;
        }
        if (this.aa > 0) {
            this.ax = new as[this.aa];
            this.aa = 0;
        }
        for (int i3 = 0; i3 < p.length; i3++) {
            if (p != null) {
                if (p[i3].endsWith(".ps") || p[i3].endsWith(".png") || p[i3].endsWith(".jpg")) {
                    try {
                        this.aw[this.i] = ah.n(p[i3]);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    this.Z[this.i] = o[i3];
                    this.i++;
                } else if (p[i3].endsWith(".ani")) {
                    if (this.ax[this.aa] == null) {
                        this.ax[this.aa] = new as(new StringBuffer().append("/sys").append(p[i3]).toString(), "/sys/");
                        this.ax[this.aa].b(true);
                    }
                    this.aa++;
                }
            }
        }
        l.t().k = true;
    }

    public final void b(o oVar) {
        l.t();
        l.a(oVar, 0);
        oVar.a(this.ah, 0, 0, 0);
        if (this.i > 0) {
            oVar.setColor(16777215);
            l.t();
            l.t();
            oVar.fillRect(0, 0, 240, 320);
            com.a.a.e.p pVar = this.aw[this.i - 1];
            l.t();
            l.t();
            oVar.a(pVar, 120, (int) f.OBEX_HTTP_OK, 3);
            if (!this.k) {
                this.h++;
                if (this.h > 20) {
                    this.e = 1;
                    oVar.e(0, 285, 240, 285);
                    oVar.a("是否开启音乐", 120, 290, 17);
                    oVar.a("开", 2, 318, 36);
                    oVar.a("关", 238, 318, 40);
                }
            } else if (this.Z[this.i - 1] > 0) {
                int[] iArr = this.Z;
                int i2 = this.i - 1;
                iArr[i2] = iArr[i2] - 1;
                this.i--;
            }
        } else if (this.aa > 0) {
            as asVar = this.ax[this.aa - 1];
            l.t();
            l.t();
            asVar.a(oVar, 0, 120, f.OBEX_HTTP_OK, false);
            this.b++;
            if (this.X[this.b % this.X.length] == 1) {
                this.ax[this.aa - 1].f(0);
                if (this.ax[this.aa - 1].h(0)) {
                    this.aa--;
                }
            }
        } else {
            y.I().d((byte) 2);
        }
    }

    public final void o() {
        if (this.e == 1) {
            if (this.Y.h(131072)) {
                this.k = true;
                y.cc = true;
                this.Y.a("/sys/sound1.mid", 1);
            }
            if (this.Y.h(262144)) {
                this.k = true;
                y.cc = false;
            }
        }
    }

    public final void q() {
    }
}
