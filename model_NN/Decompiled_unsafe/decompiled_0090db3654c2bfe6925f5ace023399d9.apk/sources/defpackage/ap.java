package defpackage;

/* renamed from: ap  reason: default package */
public final class ap implements ar {
    private l Y = l.t();
    private o vm;

    public final void a() {
        if (this.vm != null) {
            this.vm.a();
        }
    }

    public final void a(int i) {
        l.b();
        this.Y.b(false);
        if (this.vm != null) {
            this.Y.k = false;
            this.vm.a();
            this.vm = null;
            System.gc();
        }
        switch (i) {
            case 1:
                this.vm = new p();
                break;
            case 2:
                this.vm = new v();
                break;
            case 3:
                this.vm = new g();
                break;
            case 4:
                y.I().b();
                this.vm = y.I().ap;
                break;
            case 5:
                this.vm = new ae();
                break;
            case 6:
                this.vm = new aa();
                break;
            case 7:
                this.vm = new al();
                break;
        }
        this.vm.b();
    }

    public final void b() {
        this.Y.a(60);
        a(1);
    }

    public final o kB() {
        return this.vm;
    }
}
