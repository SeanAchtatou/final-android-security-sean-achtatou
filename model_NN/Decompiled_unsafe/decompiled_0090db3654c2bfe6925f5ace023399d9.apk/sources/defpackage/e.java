package defpackage;

import com.a.a.e.o;
import java.util.Random;

/* renamed from: e  reason: default package */
public final class e {
    private int[] X = new int[0];
    private int[] Z = new int[0];
    private int a = 0;
    private int[] aq = new int[0];
    private int[] ar = new int[0];
    private Random as = new Random();
    private int e = 0;

    private static int[] a(int[] iArr, int i, int i2) {
        int i3;
        int i4;
        int i5;
        int[] iArr2;
        int length = iArr.length;
        int[] iArr3 = new int[(length + i2)];
        System.arraycopy(iArr, 0, iArr3, 0, i);
        if (i2 > 0) {
            i5 = i + i2;
            i3 = i;
            i4 = length;
            iArr2 = iArr3;
        } else {
            i3 = i - i2;
            i4 = length + i2;
            i5 = i;
            iArr2 = iArr3;
        }
        System.arraycopy(iArr, i3, iArr2, i5, i4 - i);
        return iArr3;
    }

    private int e(int i, int i2) {
        return ((this.as.nextInt() >>> 1) % ((i2 - i) + 1)) + i;
    }

    public final void b(o oVar) {
        for (int length = this.aq.length - 1; length >= 0; length--) {
            if (this.aq[length] > 0) {
                this.aq[length] = -this.aq[length];
            } else {
                this.aq = a(this.aq, length, -1);
                this.ar = a(this.ar, length, -1);
            }
        }
        for (int length2 = this.X.length - 1; length2 >= 0; length2--) {
            int[] iArr = this.X;
            iArr[length2] = iArr[length2] - 30;
            int[] iArr2 = this.Z;
            iArr2[length2] = iArr2[length2] + 40;
            if (this.Z[length2] >= 300) {
                int length3 = this.aq.length;
                this.aq = a(this.aq, length3, 1);
                this.ar = a(this.ar, length3, 1);
                this.aq[length3] = this.X[length2];
                this.ar[length3] = e(3, 10) + 180;
                this.X = a(this.X, length2, -1);
                this.Z = a(this.Z, length2, -1);
            }
        }
        int length4 = this.X.length;
        this.X = a(this.X, length4, 4);
        this.Z = a(this.Z, length4, 4);
        for (int i = 0; i < 4; i++) {
            this.X[length4 + i] = e(0, 400);
            this.Z[length4 + i] = e(0, 30);
        }
        oVar.setColor(10526138);
        for (int length5 = this.X.length - 1; length5 >= 0; length5--) {
            oVar.e(this.X[length5] + 0, this.Z[length5] + 30, this.X[length5] + 0 + 5, (this.Z[length5] + 30) - 5);
        }
    }
}
