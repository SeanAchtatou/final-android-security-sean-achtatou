package defpackage;

/* renamed from: as  reason: default package */
public final class as {
    private int[] X = new int[this.vn.vt.length];
    private boolean k;
    private String o;
    private at vn;
    private boolean[] vo;

    public as(String str, String str2) {
        this.o = str;
        this.vn = ag.m(str, str2);
        for (int i = 0; i < this.X.length; i++) {
            this.X[i] = -1;
        }
        this.vo = new boolean[this.X.length];
    }

    public final void a(int i, int i2) {
        if (this.X[i] >= this.vn.vs[i]) {
            this.X[i] = 0;
        }
        this.X[i] = i2;
        this.vo[i] = false;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: short} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v36, resolved type: short} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v37, resolved type: short} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.a.a.e.o r13, int r14, int r15, int r16, boolean r17) {
        /*
            r12 = this;
            if (r14 < 0) goto L_0x0009
            at r0 = r12.vn
            short[] r0 = r0.vs
            int r0 = r0.length
            if (r14 <= r0) goto L_0x000a
        L_0x0009:
            return
        L_0x000a:
            int[] r0 = r12.X
            r0 = r0[r14]
            r1 = -1
            if (r0 != r1) goto L_0x0016
            int[] r0 = r12.X
            r1 = 0
            r0[r14] = r1
        L_0x0016:
            at r0 = r12.vn
            short[] r0 = r0.vt
            int r0 = r0.length
            if (r14 >= r0) goto L_0x0009
            at r0 = r12.vn
            byte[] r0 = r0.cU
            at r1 = r12.vn
            short[] r1 = r1.vt
            short r1 = r1[r14]
            int[] r2 = r12.X
            r2 = r2[r14]
            int r1 = r1 + r2
            byte r0 = r0[r1]
            r11 = r0 & 255(0xff, float:3.57E-43)
            r0 = 0
            r10 = r0
        L_0x0032:
            at r0 = r12.vn
            byte[] r0 = r0.at
            byte r0 = r0[r11]
            if (r10 >= r0) goto L_0x00e0
            at r0 = r12.vn
            short[] r0 = r0.cT
            short r0 = r0[r11]
            int r0 = r0 + r10
            if (r17 == 0) goto L_0x00b1
            at r1 = r12.vn
            short[] r1 = r1.vp
            int r2 = r0 * 3
            short r1 = r1[r2]
            int r1 = -r1
            int r7 = r1 + r15
            at r1 = r12.vn
            short[] r1 = r1.vp
            int r2 = r0 * 3
            int r2 = r2 + 1
            short r1 = r1[r2]
            int r8 = r1 + r16
        L_0x005a:
            at r1 = r12.vn
            boolean r1 = r1.k
            if (r1 == 0) goto L_0x00c8
            at r1 = r12.vn
            short[] r1 = r1.vp
            int r0 = r0 * 3
            int r0 = r0 + 2
            short r0 = r1[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r0 = r0 * 6
        L_0x006e:
            if (r17 == 0) goto L_0x00d5
            at r1 = r12.vn
            short[] r1 = r1.aD
            int r2 = r0 + 5
            short r1 = r1[r2]
            r6 = r1 ^ 2
            r9 = 24
        L_0x007c:
            at r1 = r12.vn
            com.a.a.e.p[] r1 = r1.aw
            at r2 = r12.vn
            short[] r2 = r2.aD
            short r2 = r2[r0]
            r1 = r1[r2]
            at r2 = r12.vn
            short[] r2 = r2.aD
            int r3 = r0 + 1
            short r2 = r2[r3]
            at r3 = r12.vn
            short[] r3 = r3.aD
            int r4 = r0 + 2
            short r3 = r3[r4]
            at r4 = r12.vn
            short[] r4 = r4.aD
            int r5 = r0 + 3
            short r4 = r4[r5]
            at r5 = r12.vn
            short[] r5 = r5.aD
            int r0 = r0 + 4
            short r5 = r5[r0]
            r0 = r13
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            int r0 = r10 + 1
            byte r0 = (byte) r0
            r10 = r0
            goto L_0x0032
        L_0x00b1:
            at r1 = r12.vn
            short[] r1 = r1.vp
            int r2 = r0 * 3
            short r1 = r1[r2]
            int r7 = r1 + r15
            at r1 = r12.vn
            short[] r1 = r1.vp
            int r2 = r0 * 3
            int r2 = r2 + 1
            short r1 = r1[r2]
            int r8 = r1 + r16
            goto L_0x005a
        L_0x00c8:
            at r1 = r12.vn
            short[] r1 = r1.vp
            int r0 = r0 * 3
            int r0 = r0 + 2
            short r0 = r1[r0]
            int r0 = r0 * 6
            goto L_0x006e
        L_0x00d5:
            at r1 = r12.vn
            short[] r1 = r1.aD
            int r2 = r0 + 5
            short r6 = r1[r2]
            r9 = 20
            goto L_0x007c
        L_0x00e0:
            boolean r0 = r12.k
            if (r0 == 0) goto L_0x0122
            r0 = 65280(0xff00, float:9.1477E-41)
            r13.setColor(r0)
            at r0 = r12.vn
            byte[] r0 = r0.aB
            byte r2 = r0[r11]
            at r0 = r12.vn
            short[] r0 = r0.vq
            short r1 = r0[r11]
            r0 = 0
        L_0x00f7:
            if (r0 >= r2) goto L_0x0122
            at r3 = r12.vn
            short[] r3 = r3.vr
            short r3 = r3[r1]
            int r3 = r3 + r15
            at r4 = r12.vn
            short[] r4 = r4.vr
            int r5 = r1 + 1
            short r4 = r4[r5]
            int r4 = r4 + r16
            at r5 = r12.vn
            short[] r5 = r5.vr
            int r6 = r1 + 2
            short r5 = r5[r6]
            at r6 = r12.vn
            short[] r6 = r6.vr
            int r7 = r1 + 3
            short r6 = r6[r7]
            r13.f(r3, r4, r5, r6)
            int r0 = r0 + 1
            int r1 = r1 + 5
            goto L_0x00f7
        L_0x0122:
            boolean[] r0 = r12.vo
            r1 = 1
            r0[r14] = r1
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.as.a(com.a.a.e.o, int, int, int, boolean):void");
    }

    public final void b() {
        ag.a(this.o);
        this.vn = null;
        this.o = null;
    }

    public final void b(boolean z) {
        this.k = true;
    }

    public final int f(int i) {
        if (this.vo[i]) {
            if (h(i)) {
                this.X[i] = -1;
                this.vo[i] = false;
            } else {
                int[] iArr = this.X;
                iArr[i] = iArr[i] + 1;
            }
        }
        return this.X[i];
    }

    public final boolean h(int i) {
        return this.X[i] == this.vn.vs[i] + -1;
    }

    public final int k(int i) {
        return this.vn.vs[i];
    }

    public final int m(int i) {
        return this.X[i];
    }
}
