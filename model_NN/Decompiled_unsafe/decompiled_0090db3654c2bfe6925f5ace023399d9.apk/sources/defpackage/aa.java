package defpackage;

import com.a.a.b.c;
import com.a.a.e.o;
import com.a.a.e.p;
import com.a.a.i.a;
import com.a.a.i.k;
import com.a.a.p.f;
import com.fasterxml.jackson.core.sym.CharsToNameCanonicalizer;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.IOException;
import java.lang.reflect.Array;
import javax.microedition.media.control.MIDIControl;

/* renamed from: aa  reason: default package */
public final class aa implements o {
    public static int[] X;
    private static l Y = l.t();
    public static int[] Z;
    public static int a;
    public static int aa = 0;
    private static int ab = 0;
    private static int ac = 0;
    private static String[] ak;
    private static String[] al;
    private static int[] aq;
    private static int[] ar;
    private static short ay = 0;
    private static short az = 0;
    public static int b;
    private static String[] cI;
    private static String[] cO;
    private static String[] cS;
    private static String[] cl;
    private static int[] cn;
    private static int[] co = new int[28600];
    private static String[] ct;
    public static p dJ;
    public static p dK;
    private static int dO;
    private static int dP = -1;
    private static int dQ = -1357836015;
    private static int dR = 0;
    private static int dc = 0;
    private static String[] dd;
    private static String[] de;
    private static int ds;
    private static int dt;
    private static int dw;
    private static int dx;
    public static int e = -1;
    public static int h;
    public static int i;
    private static int p = 0;
    private y aQ = y.I();
    private int aV = 7;
    private int aW = this.aV;
    private int aX;
    private int aY = 0;
    private int aZ;
    private int ad = 3;
    private int ae = this.ad;
    p ah;
    private boolean am;
    p av;
    private int ba;
    private int bb = 0;
    private int bc = 4;
    private int bd = this.bc;
    private int bf = 0;
    private int bg = 5;
    p bi;
    private k cZ;
    private int dA = 39;
    private int dB = 9;
    private p dC;
    private int dD = 26;
    private int dE = 9;
    private p dF;
    private int dG = 94;
    private int dH = 15;
    private p dI;
    p dL;
    p dM;
    private int dN;
    private int da = this.bg;
    private int db = 0;
    private p df;
    private p dg;
    private p dh;
    private p di;
    private p dj;
    private p dk;
    private p dl;
    private p dm;
    private p dn;

    /* renamed from: do  reason: not valid java name */
    private p f0do;
    private p dp;
    private p dq;
    private int dr = 0;
    private int du;
    private int dv;
    private int dy;
    private p dz;
    private boolean k;
    private int x = 0;
    private int y = 5;
    private int z = this.bg;

    private static void J() {
        for (int i2 = 0; i2 < y.cG.length; i2++) {
            boolean z2 = false;
            for (int i3 = 0; i3 < y.bF.length && !z2; i3++) {
                int i4 = 0;
                while (i4 < 9 && !z2) {
                    if (i4 == 4) {
                        i4++;
                    } else if (y.aR[i3][i4] == i2) {
                        z2 = true;
                    } else if (i3 == y.bF.length - 1 && i4 == 8) {
                        if (Z == null) {
                            int[] iArr = new int[1];
                            Z = iArr;
                            iArr[0] = i2;
                        } else {
                            int[] iArr2 = new int[(Z.length + 1)];
                            System.arraycopy(Z, 0, iArr2, 0, Z.length);
                            iArr2[iArr2.length - 1] = i2;
                            Z = iArr2;
                        }
                        z2 = true;
                    } else {
                        i4++;
                    }
                }
            }
        }
    }

    public static void P() {
        l.a(100, 70);
        ds = 0;
        dt = 0;
        dw = 0;
        dx = 0;
        r c = new f("/data/shop.bin").c(e);
        switch (e) {
            case 0:
                a = 16;
                b = 0;
                X = c.j(1);
                return;
            case 1:
                a = 17;
                b = 0;
                Z = c.j(2);
                return;
            case 2:
                a = 18;
                b = 0;
                ar = c.j(3);
                return;
            case 3:
                a = 16;
                b = 0;
                X = c.j(1);
                return;
            case 4:
                a = 17;
                b = 0;
                Z = c.j(2);
                return;
            case 5:
                a = 18;
                b = 0;
                ar = c.j(3);
                return;
            case 6:
                a = 3;
                b = 3;
                aq[0] = 13;
                aq[1] = 14;
                aq[2] = 15;
                return;
            default:
                return;
        }
    }

    public static void Q() {
        int i2 = dP + 1;
        dP = i2;
        if (i2 > 9999) {
            dP = 0;
        }
        if (dP % 2 == 0) {
            aa++;
        }
    }

    public static void R() {
        dR = 0;
        ay = 0;
        az = 0;
    }

    private void S() {
        this.bc = 4;
        this.bb = 0;
        this.bd = 0;
        this.bg = 5;
        this.bf = 0;
        this.da = 0;
    }

    private void a(int i2) {
        if (i2 >= 7) {
            this.aW = 7;
            return;
        }
        this.aW = i2;
        this.aV = i2;
    }

    public static void a(int i2, int i3, int i4, int i5, int i6, int i7, o oVar, int i8) {
        int abs = 2 - Math.abs((i8 % 5) - 2);
        int i9 = i2 - abs;
        int i10 = i3 - abs;
        int i11 = (abs * 2) + i4;
        int i12 = (abs * 2) + i5;
        oVar.setColor(65280);
        oVar.f(i9 + 1, i10 + 1, i11 - 2, i12 - 2);
        oVar.setColor(0);
        oVar.f(i9, i10, i11, i12);
    }

    private static void a(o oVar, int i2, int i3) {
        oVar.setClip(0, 0, 240, 320);
        oVar.setColor(16777215);
        switch (i3) {
            case 0:
                oVar.a("查看队伍信息", 120, i2, 17);
                return;
            case 1:
                oVar.a("查看任务列表", 120, i2, 17);
                return;
            case 2:
                oVar.a("进入系统菜单", 120, i2, 17);
                return;
            case 3:
                oVar.a("进入乐游商城", 120, i2, 17);
                return;
            case 10:
                oVar.a("查看人物状态", 120, i2, 17);
                return;
            case 11:
                oVar.a("更换武器装备", 120, i2, 17);
                return;
            case 12:
                oVar.a("查看包裹", 120, i2, 17);
                return;
            case 13:
                oVar.a("查看特技", 120, i2, 17);
                return;
            case 14:
                oVar.a("切换出战队员", 120, i2, 17);
                return;
            case c.INT_16 /*20*/:
                oVar.a("按上下键切换选框", 120, i2, 17);
                return;
            case 30:
                oVar.a("查看武器", 120, i2, 17);
                return;
            case 31:
                oVar.a("查看防具", 120, i2, 17);
                return;
            case 32:
                oVar.a("使用道具", 120, i2, 17);
                return;
            case 80:
                oVar.a("确定键切换出战队员", 120, i2, 17);
                return;
            default:
                return;
        }
    }

    public static void a(o oVar, int i2, int i3, int i4, int i5) {
        oVar.setColor(3091240);
        oVar.fillRect(i2, i3, 22, 22);
        oVar.setColor(1182469);
        oVar.fillRect(i2 + 1, i3, 21, 21);
        oVar.setColor(1709587);
        oVar.fillRect(i2 + 1, i3 + 1, 20, 20);
        oVar.setColor(1774344);
        oVar.fillRect(i2 + 2, i3 + 1, 19, 19);
        oVar.setColor(2563605);
        oVar.fillRect(i2 + 2, i3 + 2, 18, 18);
    }

    private void a(o oVar, int i2, int i3, int i4, int i5, int i6) {
        int i7;
        int i8;
        int i9;
        if (this.aQ.bY) {
            i7 = 1;
            i8 = e == 6 ? y.cP[i6][0] : ar[i6];
        } else {
            i7 = 0;
            i8 = y.cP[i6][0];
        }
        int i10 = y.cK[i8];
        String[][] strArr = (String[][]) Array.newInstance(String.class, 2, 5);
        switch (i8) {
            case 0:
                strArr[0] = ah.f(new StringBuffer().append("回复体力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 1:
                strArr[0] = ah.f(new StringBuffer().append("回复体力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 2:
                strArr[0] = ah.f(new StringBuffer().append("回复体力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 3:
                strArr[0] = ah.f(new StringBuffer().append("回复体力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 4:
                strArr[0] = ah.f(new StringBuffer().append("回复体力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 5:
                strArr[0] = ah.f(new StringBuffer().append("回复体力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 6:
                strArr[0] = ah.f(new StringBuffer().append("回复内力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 7:
                strArr[0] = ah.f(new StringBuffer().append("回复内力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 8:
                strArr[0] = ah.f(new StringBuffer().append("回复内力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 9:
                strArr[0] = ah.f(new StringBuffer().append("回复内力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 10:
                strArr[0] = ah.f(new StringBuffer().append("回复内力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 11:
                strArr[0] = ah.f(new StringBuffer().append("回复内力").append(i10).toString(), 120);
                i9 = 1;
                break;
            case 12:
                strArr[0] = ah.f(new StringBuffer().append("复活并回复").append(i10).append("％的体力和内力").toString(), 120);
                i9 = 1;
                break;
            case 13:
                strArr[0] = ah.f(new StringBuffer().append("复活并回复").append(i10).append("％的体力和内力").toString(), 120);
                i9 = 1;
                break;
            case 14:
                strArr[0] = ah.f(new StringBuffer().append("回复体力").append(i10).toString(), 120);
                strArr[1] = ah.f(new StringBuffer().append("回复内力").append(i10).toString(), 120);
                i9 = 2;
                break;
            case 15:
                strArr[0] = ah.f(new StringBuffer().append("回复体力").append(i10).toString(), 120);
                strArr[1] = ah.f(new StringBuffer().append("回复内力").append(i10).toString(), 120);
                i9 = 2;
                break;
            default:
                i9 = 0;
                break;
        }
        int i11 = 0;
        for (int i12 = 0; i12 < i9; i12++) {
            for (int i13 = 0; i13 < strArr[i12].length; i13++) {
                i11++;
            }
        }
        int i14 = (((i7 + 1) + i11) << 4) + 10;
        if (i2 > 120) {
            i2 = (i2 - 130) + 21;
        }
        if (i3 > 160) {
            i3 = (i3 - i14) - 21;
        }
        if (m.o.equals("N7370")) {
            oVar.setColor(0);
            oVar.fillRect((i2 - 5) + 10, (i3 - 5) + 10, 122, i14 - 8);
            oVar.setColor(10066329);
            oVar.fillRect(i2 - 5, i3 - 5, 130, i14);
            oVar.setColor(2236962);
            oVar.fillRect((i2 - 5) + 2, (i3 - 5) + 2, 126, i14 - 4);
        } else {
            oVar.a(co, 0, 130, i2 - 5, i3 - 5, 130, i14, true);
        }
        oVar.setClip(0, 0, 240, 320);
        oVar.setColor(15641122);
        oVar.a(y.cI[i8], i2, i3, 20);
        oVar.setColor(16777215);
        oVar.setColor(11250603);
        if (this.aQ.bY) {
            if (e == 6) {
                oVar.a(new StringBuffer().append("价格 ").append(y.cL[i8] / 4).toString(), i2, i3 + 16, 20);
            } else {
                oVar.a(new StringBuffer().append("价格 ").append(y.cL[i8]).toString(), i2, i3 + 16, 20);
            }
        }
        int i15 = 0;
        oVar.setColor(2020328);
        for (int i16 = 0; i16 < i9; i16++) {
            for (String a2 : strArr[i16]) {
                oVar.a(a2, i2, (((i7 + 1) + i15) << 4) + i3, 20);
                i15++;
            }
        }
    }

    private void a(o oVar, int i2, int i3, int i4, int i5, int i6, int i7) {
        int i8;
        int i9 = 0;
        if (this.aQ.bY) {
            if (e == 6) {
                i9 = i6 == 0 ? y.cE[i7][2] : y.cG[i7][2];
            }
            i8 = 1;
        } else if (i6 == 0) {
            i9 = y.cE[i7][2];
            i8 = 0;
        } else {
            i9 = y.cG[i7][2];
            i8 = 0;
        }
        String[][] strArr = new String[i9][];
        int i10 = 0;
        int i11 = 0;
        for (int i12 = i9; i12 > 0; i12--) {
            switch (i12) {
                case 1:
                    if (i6 != 0) {
                        i11 = y.cG[i7][3];
                        i10 = y.cG[i7][4];
                        break;
                    } else {
                        i11 = y.cE[i7][3];
                        i10 = y.cE[i7][4];
                        break;
                    }
                case 2:
                    if (i6 != 0) {
                        i11 = y.cG[i7][5];
                        i10 = y.cG[i7][6];
                        break;
                    } else {
                        i11 = y.cE[i7][5];
                        i10 = y.cE[i7][6];
                        break;
                    }
                case 3:
                    if (i6 != 0) {
                        i11 = y.cG[i7][7];
                        i10 = y.cG[i7][8];
                        break;
                    } else {
                        i11 = y.cE[i7][7];
                        i10 = y.cE[i7][8];
                        break;
                    }
            }
            switch (i11) {
                case 0:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("普通攻击的").append(i10).append("％转为体力").toString(), 120);
                    break;
                case 1:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("普通攻击的").append(i10).append("％转为内力").toString(), 120);
                    break;
                case 2:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("普通攻击").append(i10).append("％的溅射伤害").toString(), 120);
                    break;
                case 3:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("普通攻击后").append(i10).append("％再次攻击").toString(), 120);
                    break;
                case 4:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append(i10).append("％的概率造成2倍伤害").toString(), 120);
                    break;
                case 5:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append(i10).append("％的概率造成3倍伤害").toString(), 120);
                    break;
                case 6:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append(i10).append("％的概率造成4倍伤害").toString(), 120);
                    break;
                case 7:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append(i10).append("％的概率造成10倍伤害").toString(), 120);
                    break;
                case 8:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("反击伤害提高").append(i10).append("倍").toString(), 120);
                    break;
                case 9:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("体力在20％以下时，普通攻击").append(i10).append("倍").toString(), 120);
                    break;
                case 10:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("体力在100％时，普通攻击").append(i10).append("倍").toString(), 120);
                    break;
                case c.INT_16 /*20*/:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("攻击力＋").append(i10).toString(), 120);
                    break;
                case 21:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("防御＋").append(i10).toString(), 120);
                    break;
                case 22:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("敏捷＋").append(i10).toString(), 120);
                    break;
                case 23:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("智力＋").append(i10).toString(), 120);
                    break;
                case c.UUID /*24*/:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("技巧＋").append(i10).toString(), 120);
                    break;
                case 25:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("速度＋").append(i10).toString(), 120);
                    break;
                case 26:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("体力＋").append(i10).toString(), 120);
                    break;
                case 27:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("内力＋").append(i10).toString(), 120);
                    break;
                case 28:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("获得经验＋").append(i10).append("％").toString(), 120);
                    break;
                case 29:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("获得金钱＋").append(i10).append("％").toString(), 120);
                    break;
                case 30:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("行动后体力回复").append(i10).append("％").toString(), 120);
                    break;
                case 31:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("行动后内力回复").append(i10).append("％").toString(), 120);
                    break;
                case 32:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("战后体力回复").append(i10).append("％").toString(), 120);
                    break;
                case CharsToNameCanonicalizer.HASH_MULT /*33*/:
                    strArr[i12 - 1] = ah.f(new StringBuffer().append("战后内力回复").append(i10).append("％").toString(), 120);
                    break;
            }
        }
        int i13 = 0;
        for (String[] length : strArr) {
            int i14 = 0;
            while (i14 < length.length) {
                i14++;
                i13++;
            }
        }
        int i15 = i6 == 0 ? 3 : 2;
        int i16 = (((i15 + i13) + i8) << 4) + 10;
        if (i2 > 120) {
            i2 = (i2 - 130) + 21;
        }
        if (i3 > 160) {
            i3 = (i3 - i16) - 21;
        }
        if (m.o.equals("N7370")) {
            oVar.setColor(0);
            oVar.fillRect((i2 - 5) + 10, (i3 - 5) + 10, 122, i16 - 8);
            oVar.setColor(10066329);
            oVar.fillRect(i2 - 5, i3 - 5, 130, i16);
            oVar.setColor(2236962);
            oVar.fillRect((i2 - 5) + 2, (i3 - 5) + 2, 126, i16 - 4);
        } else {
            oVar.a(co, 0, 130, i2 - 5, i3 - 5, 130, i16, true);
        }
        int i17 = 0;
        oVar.setClip(0, 0, 240, 320);
        oVar.setColor(2020328);
        for (int i18 = 0; i18 < strArr.length; i18++) {
            int i19 = 0;
            while (i19 < strArr[i18].length) {
                oVar.a(strArr[i18][i19], i2, (((i15 + i17) + i8) << 4) + i3, 20);
                i19++;
                i17++;
            }
        }
        int i20 = 0;
        if (!this.aQ.bY) {
            i20 = i6 == 0 ? y.cE[i7][2] : y.cG[i7][2];
        } else if (e == 6) {
            i20 = i6 == 0 ? y.cE[i7][2] : y.cG[i7][2];
        }
        switch (i20) {
            case 0:
                oVar.setColor(16777215);
                break;
            case 1:
                oVar.setColor(65280);
                break;
            case 2:
                oVar.setColor(2020328);
                break;
            case 3:
                oVar.setColor(15602158);
                break;
            case 4:
                oVar.setColor(15641122);
                break;
        }
        if (i6 == 0) {
            int i21 = (!this.aQ.bY || e == 6) ? y.cE[i7][0] : i7;
            oVar.a(y.cl[i21], i2, i3, 20);
            oVar.setColor(11250603);
            if (this.aQ.bY) {
                if (e == 6) {
                    oVar.a(new StringBuffer().append("价格 ").append(y.cn[i21] / 4).toString(), i2, i3 + 16, 20);
                } else {
                    oVar.a(new StringBuffer().append("价格 ").append(y.cn[i21]).toString(), i2, i3 + 16, 20);
                }
            }
            oVar.setColor(12325666);
            switch (y.aq[i21]) {
                case 0:
                    oVar.a("(限仲颖装备)", i2, i3 + 16 + (i8 << 4), 20);
                    break;
                case 1:
                    oVar.a("(限女性装备)", i2, i3 + 16 + (i8 << 4), 20);
                    break;
                case 2:
                    oVar.a("(限关羽装备)", i2, i3 + 16 + (i8 << 4), 20);
                    break;
                case 3:
                    oVar.a("(限赵云装备)", i2, i3 + 16 + (i8 << 4), 20);
                    break;
                case 4:
                    oVar.a("(限吕布装备)", i2, i3 + 16 + (i8 << 4), 20);
                    break;
            }
            oVar.setColor(16777215);
            if (!this.aQ.bY) {
                oVar.a(new StringBuffer().append("攻击力 ").append(y.cE[i7][1]).toString(), i2, i3 + 32 + (i8 << 4), 20);
            } else if (e == 6) {
                oVar.a(new StringBuffer().append("攻击力 ").append(y.cE[i7][1]).toString(), i2, i3 + 32 + (i8 << 4), 20);
            } else {
                oVar.a(new StringBuffer().append("攻击力 ").append(y.ar[i7]).toString(), i2, i3 + 32 + (i8 << 4), 20);
            }
        } else {
            int i22 = (!this.aQ.bY || e == 6) ? y.cG[i7][0] : i7;
            oVar.a(y.ct[i22], i2, i3, 20);
            oVar.setColor(11250603);
            if (this.aQ.bY) {
                if (e == 6) {
                    oVar.a(new StringBuffer().append("价格 ").append(y.cx[i22] / 4).toString(), i2, i3 + 16, 20);
                } else {
                    oVar.a(new StringBuffer().append("价格 ").append(y.cx[i22]).toString(), i2, i3 + 16, 20);
                }
            }
            int i23 = (!this.aQ.bY || e == 6) ? y.cG[i7][1] : y.cv[i7];
            oVar.setColor(16777215);
            switch (y.cu[i22]) {
                case 0:
                default:
                    return;
                case 1:
                    oVar.a(new StringBuffer().append("智力 ").append(i23).toString(), i2, i3 + 16 + (i8 << 4), 20);
                    return;
                case 2:
                    oVar.a(new StringBuffer().append("防御 ").append(i23).toString(), i2, i3 + 16 + (i8 << 4), 20);
                    return;
                case 3:
                    oVar.a(new StringBuffer().append("速度 ").append(i23).toString(), i2, i3 + 16 + (i8 << 4), 20);
                    return;
                case 4:
                    oVar.a(new StringBuffer().append("体力 ").append(i23).toString(), i2, i3 + 16 + (i8 << 4), 20);
                    return;
                case 5:
                    oVar.a(new StringBuffer().append("敏捷 ").append(i23).toString(), i2, i3 + 16 + (i8 << 4), 20);
                    return;
                case 6:
                    oVar.a(new StringBuffer().append("技巧 ").append(i23).toString(), i2, i3 + 16 + (i8 << 4), 20);
                    return;
                case 7:
                    oVar.a(new StringBuffer().append("内力 ").append(i23).toString(), i2, i3 + 16 + (i8 << 4), 20);
                    return;
            }
        }
    }

    private static void a(o oVar, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        oVar.setClip(0, 0, 240, 320);
        oVar.setColor(15247695);
        oVar.fillRect(i2 + 1, i3, i4 - 2, i5);
        oVar.fillRect(i2, i3 + 1, i4, i5 - 2);
        oVar.setColor(0);
        oVar.fillRect(i2 + 1, i3 + 1, i4 - 2, i5 - 2);
        oVar.setColor(15247695);
        oVar.fillRect(i2 + 2, i3, i4 - 4, i5);
        oVar.fillRect(i2, i3 + 2, i4, i5 - 4);
        oVar.setColor(i8);
        oVar.fillRect(i2 + 3, i3 + 1, i4 - 6, i5 - 2);
        oVar.fillRect(i2 + 1, i3 + 3, i4 - 2, i5 - 6);
    }

    private static void a(o oVar, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        oVar.setClip(0, 0, 240, 320);
        oVar.setColor(0);
        oVar.fillRect(i2, i3, 5, i5);
        oVar.setColor(16777215);
        oVar.fillRect(i2 + 1, i3 + 1, 3, i5 - 2);
        oVar.setColor(11579568);
        if (i6 <= 0) {
            return;
        }
        if (i6 == 1) {
            oVar.fillRect(i2 + 1, i3 + 1, 3, i5 - 2);
        } else {
            oVar.fillRect(i2 + 1, i3 + 1 + ((((i7 + i8) - i9) * (i5 - 2)) / i6), 3, ((i5 - 2) / i6) + 1);
        }
    }

    public static void a(o oVar, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, p pVar, int i10, int i11, int i12, int i13, int i14, int i15, p pVar2, int i16, int i17, int i18, int i19, int i20, int i21) {
        oVar.setClip(0, 0, 240, 320);
        int i22 = 0;
        int i23 = i4 - 2;
        int i24 = i5 - 2;
        oVar.setColor(i6);
        oVar.fillRect(i2 + 1, i3, i23, i5);
        oVar.fillRect(i2, i3 + 1, i4, i24);
        oVar.setColor(i7);
        oVar.fillRect(i2 + 1, i3 + 1, i23, i24);
        if (i21 == 1) {
            oVar.setColor(i8);
            oVar.fillRect(i2 + 2, i3 + 2, i4 - 4, i5 - 4);
            oVar.setColor(i9);
            oVar.fillRect(i2 + 3, i3 + 3, i4 - 6, i5 - 6);
            i22 = 2;
            i23 = i4 - 6;
            i24 = i5 - 6;
        }
        oVar.setClip(i2 + i22 + 1, i3 + i22 + 1, i23, i24);
        int i25 = 0;
        while (true) {
            int i26 = i25;
            if (i26 >= i10) {
                break;
            }
            for (int i27 = 0; i27 < i11; i27++) {
                oVar.a(pVar, (((i2 + i22) + 1) + (i26 * i13)) - (i12 * i13), (((i3 + i22) + 1) + (i27 * i15)) - (i15 * 0), 20);
            }
            i25 = i26 + 1;
        }
        oVar.setClip(0, 0, 240, 320);
        oVar.a(pVar2, 0, 0, pVar2.getWidth(), pVar2.getHeight(), 0, i2, i3, 20);
        if (i17 == 1) {
            oVar.a(pVar2, 0, 0, pVar2.getWidth(), pVar2.getHeight(), 1, i2, (i3 + i5) - pVar2.getHeight(), 20);
        }
        if (i18 == 1) {
            oVar.a(pVar2, 0, 0, pVar2.getWidth(), pVar2.getHeight(), 2, i2 + i4, i3, 24);
        }
        if (i19 == 1) {
            oVar.a(pVar2, 0, 0, pVar2.getWidth(), pVar2.getHeight(), 3, i2 + i4, (i3 + i5) - pVar2.getHeight(), 24);
        }
    }

    public static void a(o oVar, String str, int i2, int i3, int i4, int i5) {
        oVar.setClip(i2 + 5, 0, i4, 320);
        oVar.setColor(16777215);
        oVar.a(str, i2 + 5 + dR, i3, 20);
        if (i2 + 5 + ah.aP.A(str) <= i4) {
            return;
        }
        if (ay < 20) {
            ay = (short) (ay + 1);
        } else if (ah.aP.A(str) - i4 >= Math.abs(dR)) {
            dR--;
        } else if (az < 20) {
            az = (short) (az + 1);
        } else {
            dR = 0;
            ay = 0;
            az = 0;
        }
    }

    private static void a(o oVar, String str, String str2, String str3, int i2, int i3) {
        int i4 = ah.e + 10;
        ae.a(oVar, 45, (320 - i4) >> 1, 150, i4, 0, 12169636, 0, 12331540, dK, 1, 1, 1, 1, 2, 1);
        oVar.setColor(16777215);
        oVar.a(str2, 70, (((320 - i4) >> 1) + i4) - 5, 36);
        oVar.a(str3, 170, (((320 - i4) >> 1) + i4) - 5, 40);
        int A = i3 == 0 ? 70 : (170 - ah.aP.A(str2)) - 1;
        oVar.setClip(0, 0, 240, 320);
        Q();
        a(A, (((((320 - i4) >> 1) + i4) - 5) - ah.e) - 1, ah.aP.A(str2) + 2, ah.e + 2, 65280, 0, oVar, aa);
    }

    private void b(o oVar, int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16 = i2 + i3;
        int i17 = 0;
        int i18 = 0;
        int i19 = 0;
        int i20 = 0;
        int i21 = 0;
        int i22 = 0;
        int i23 = 0;
        int i24 = 0;
        int i25 = 0;
        int i26 = 0;
        int i27 = 0;
        int i28 = 0;
        int i29 = i2;
        while (i29 < i16) {
            if (i29 <= i - 1) {
                int i30 = y.bF[(this.ae + i29) - this.ad].aW;
                int i31 = y.bF[(this.ae + i29) - this.ad].aX + y.bF[(this.ae + i29) - this.ad].dt;
                int i32 = y.bF[(this.ae + i29) - this.ad].aY;
                int i33 = y.bF[(this.ae + i29) - this.ad].aZ + y.bF[(this.ae + i29) - this.ad].du;
                int i34 = y.bF[(this.ae + i29) - this.ad].ae;
                int i35 = y.bF[(this.ae + i29) - this.ad].aV;
                i4 = y.bF[(this.ae + i29) - this.ad].ad;
                i5 = 104;
                i6 = (i34 * 88) / i35;
                i7 = 96;
                i8 = 105;
                i9 = i35;
                i10 = i34;
                i11 = 101;
                i12 = i33;
                i13 = i32;
                i14 = i31;
                i15 = i30;
            } else {
                i4 = i28;
                i5 = i27;
                i6 = i26;
                i7 = i25;
                i8 = i24;
                i9 = i23;
                i10 = i22;
                i11 = i21;
                i12 = i20;
                i13 = i19;
                i14 = i18;
                i15 = i17;
            }
            if (i29 > i - 1) {
                a(oVar, 34, ((i29 - i2) * 51) + 77, 172, 48, 15247695, 12331540, 1, 1, this.ah, 3, 1, 0, this.ah.getWidth(), 0, 0, this.dM, 1, 1, 1, 1, 2, 0);
            } else {
                if (dt == i29) {
                    a(oVar, 33, ((i29 - i2) * 51) + 76, 175, 51, 0, 12169636, 0, 12331540, this.dm, 1, 4, 0, 0, 0, this.dm.getHeight(), this.dL, 1, 1, 1, 1, 2, 1);
                } else {
                    ae.a(oVar, 34, ((i29 - i2) * 51) + 77, 172, 48, 15247695, 12331540, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                }
                a(oVar, 37, ((i29 - i2) * 51) + 79, 34, 30, 0, 12697272, 0, 15658734, this.dj, 1, 1, y.bF[(this.ae + i29) - this.ad].ac, 28, 0, 0, dK, 1, 0, 0, 0, 2, 1);
                b(oVar, 72, ((i29 - i2) * 51) + 80, 44, 13, 0, 792076, 5123890);
                b(oVar, a.UID, ((i29 - i2) * 51) + 80, 44, 13, 28, 5123890, 9596493);
                b(oVar, 39, ((i29 - i2) * 51) + a.PHOTO, 29, 13, 0, 792076, 5123890);
                b(oVar, 72, ((i29 - i2) * 51) + 94, 30, 14, 0, 792076, 4949774);
                b(oVar, 71, ((i29 - i2) * 51) + a.ORG, 72, 13, 15, 5123890, 15247695);
                b(oVar, MIDIControl.NOTE_ON, ((i29 - i2) * 51) + a.ORG, 60, 13, 15, 5123890, 15247695);
                b(oVar, 103, ((i29 - i2) * 51) + 101, 92, 5, 0, 0, 792076);
                ae.b(oVar, i5, i11 + ((i29 - i2) * 51), i6, 1, 13100944, 9488228, 7909707);
                ae.a(oVar, this.dk, this.f0do, i9, i10, i8, i7 + ((i29 - i2) * 51), 0);
                oVar.setClip(74, ((i29 - i2) * 51) + 82, this.dA, this.dB);
                oVar.a(this.dz, 74 - (y.bF[(this.ae + i29) - this.ad].ac * this.dA), ((i29 - i2) * 51) + 82, 20);
                oVar.setClip(a.URL, ((i29 - i2) * 51) + 82, this.dD, this.dE);
                oVar.a(this.av, 118 - (this.dD * 5), ((i29 - i2) * 51) + 82, 20);
                ae.a(oVar, this.dk, i4, 153, ((i29 - i2) * 51) + 83, 2);
                oVar.setClip(40, ((i29 - i2) * 51) + a.PUBLIC_KEY, this.dD, this.dE);
                if (y.bF[(this.ae + i29) - this.ad].aM) {
                    oVar.a(this.av, 40 - (this.dD * 2), ((i29 - i2) * 51) + a.PUBLIC_KEY, 20);
                } else {
                    oVar.a(this.av, 40 - (this.dD * 3), ((i29 - i2) * 51) + a.PUBLIC_KEY, 20);
                }
                oVar.setClip(75, ((i29 - i2) * 51) + 97, this.dD, this.dE);
                oVar.a(this.av, 75 - (this.dD * 4), ((i29 - i2) * 51) + 97, 20);
                oVar.setClip(73, ((i29 - i2) * 51) + a.ORG, this.dn.getWidth() / 2, this.dn.getHeight());
                oVar.a(this.dn, 73, ((i29 - i2) * 51) + a.ORG, 20);
                oVar.setClip(146, ((i29 - i2) * 51) + a.ORG, this.dn.getWidth() / 2, this.dn.getHeight());
                oVar.a(this.dn, 146 - (this.dn.getWidth() / 2), ((i29 - i2) * 51) + a.ORG, 20);
                ae.a(oVar, this.dk, this.f0do, i14, i15, 88, ((i29 - i2) * 51) + a.PUBLIC_KEY, 0);
                ae.a(oVar, this.dk, this.f0do, i12, i13, (int) f.OBEX_HTTP_CREATED, ((i29 - i2) * 51) + a.PUBLIC_KEY, 0);
            }
            i29++;
            i27 = i5;
            i26 = i6;
            i25 = i7;
            i24 = i8;
            i23 = i9;
            i22 = i10;
            i21 = i11;
            i20 = i12;
            i19 = i13;
            i18 = i14;
            i17 = i15;
            i28 = i4;
        }
    }

    public static void b(o oVar, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        oVar.setClip(0, 0, 240, 320);
        oVar.setColor(i7);
        oVar.fillRect(i2 + 1, i3, i4 - 2, i5);
        oVar.fillRect(i2, i3 + 1, i4, i5 - 2);
        oVar.setColor(i8);
        oVar.fillRect(i2 + i6 + 1, i3 + 1, (i4 - i6) - 2, i5 - 2);
    }

    private void c(o oVar) {
        int height = this.dI.getHeight();
        b(oVar, 124, 230, 60, 15, 21, 8415042, 10983781);
        oVar.setClip(124, 227, 21, height);
        oVar.a(this.dI, -65, 227, 20);
        ae.a(oVar, this.dk, y.aa, 147, 234, 0);
    }

    private static void c(o oVar, int i2, int i3) {
        int length;
        String[] strArr;
        String str = "";
        switch (i2) {
            case 1:
                str = "不能使用该物品";
                break;
            case 2:
                str = "至少一人出战";
                break;
            case 3:
                str = "最多三人出战";
                break;
            case 4:
                str = "金钱不足";
                break;
            case 5:
                str = "不能逃跑";
                break;
            case 6:
                str = "存盘成功";
                break;
            case 7:
                str = "体力已满";
                break;
            case 8:
                str = "内力已满";
                break;
            case 9:
                str = "体力内力已满";
                break;
        }
        if (i2 == 0) {
            length = ah.e + 10;
            strArr = null;
        } else {
            String[] f = ah.f(str, 190);
            length = ((ah.e + 3) * f.length) + 10;
            strArr = f;
        }
        ae.a(oVar, 20, (320 - length) >> 1, 200, length, 0, 12169636, 0, 12331540, dK, 1, 1, 1, 1, 2, 1);
        oVar.setColor(16777215);
        if (strArr != null) {
            for (int i4 = 0; i4 < strArr.length; i4++) {
                oVar.a(strArr[i4], 120, ((320 - length) >> 1) + ((ah.e + 3) * i4) + 5, 17);
            }
        }
    }

    private static int e(int i2, int i3) {
        return i2 % 8 == 0 ? i2 / 8 : (i2 / 8) + 1;
    }

    private static void e() {
        for (int i2 = 0; i2 < y.cE.length; i2++) {
            boolean z2 = false;
            int i3 = 0;
            while (i3 < y.bF.length && !z2) {
                if (y.aR[i3][4] == i2) {
                    z2 = true;
                } else if (i3 == y.bF.length - 1) {
                    if (X == null) {
                        int[] iArr = new int[1];
                        X = iArr;
                        iArr[0] = i2;
                    } else {
                        int[] iArr2 = new int[(X.length + 1)];
                        System.arraycopy(X, 0, iArr2, 0, X.length);
                        iArr2[iArr2.length - 1] = i2;
                        X = iArr2;
                    }
                    z2 = true;
                } else {
                    i3++;
                }
            }
        }
    }

    private void e(o oVar) {
        int i2;
        int i3;
        ae.a(oVar, 30, 49, 180, 18, 0, 12169636, 0, 12331540, this.bi, 1, 1, 1, 1, 2, 1);
        ae.a(oVar, 24, 67, 192, 182, 0, 9209989, 0, 5446417, dJ, 1, 0, 0, 1, 3, 1);
        ae.a(oVar, 24, 249, 192, 31, 0, 12169636, 0, 12331540, dK, 1, 1, 1, 1, 2, 1);
        ae.a(oVar, 48, 280, 43, 18, 920068, 12697272, 920068, 12331540, this.dL, 1, 1, 1, 1, 2, 1);
        ae.a(oVar, 149, 280, 43, 18, 920068, 12697272, 920068, 12331540, this.dL, 1, 1, 1, 1, 2, 1);
        ae.a(oVar, 3, 201, 68, 1, 920068);
        ae.a(oVar, 3, 36, 247, 1, 920068);
        oVar.setClip(56, 284, this.dD, this.dE);
        oVar.a(this.av, 56 - (this.dD * 0), 284, 20);
        oVar.setClip(157, 284, this.dD, this.dE);
        oVar.a(this.av, 157 - (this.dD * 1), 284, 20);
        int i4 = b;
        if (i4 == 1) {
            i2 = 0;
            i3 = 0;
        } else {
            i2 = 3;
            i3 = 3;
        }
        for (int i5 = 0; i5 < i4; i5++) {
            if (this.dr == 1) {
                if (ds == i5) {
                    a(oVar, ((i5 * 25) + 46) - i3, 41 - i3, (i3 * 2) + 23, (i3 * 2) + 23, 15247695, 0, 6439452);
                } else {
                    a(oVar, (i5 * 25) + 46, 41, 23, 23, 15247695, 0, 8415042);
                }
                oVar.setClip((i5 * 25) + 47, 42, 21, 21);
                oVar.a(this.dI, ((i5 * 25) + 47) - (aq[i5] * 21), 42, 20);
                oVar.setClip(0, 0, 240, 320);
            } else if (ds == i5) {
                a(oVar, (i5 * 25) + 46, 41 - i2, 23, 23, 15247695, 0, 6439452);
                oVar.setClip((i5 * 25) + 47, 42 - i2, 21, 21);
                oVar.a(this.dI, ((i5 * 25) + 47) - (aq[i5] * 21), 42 - i2, 20);
                oVar.setClip(0, 0, 240, 320);
            } else {
                a(oVar, (i5 * 25) + 46, 41, 23, 23, 15247695, 0, 8415042);
                oVar.setClip((i5 * 25) + 47, 42, 21, 21);
                oVar.a(this.dI, ((i5 * 25) + 47) - (aq[i5] * 21), 42, 20);
                oVar.setClip(0, 0, 240, 320);
            }
        }
        if (i4 > 0) {
            int i6 = dO + 1;
            dO = i6;
            if (i6 > 99999) {
                dO = 0;
            }
            oVar.a(this.dq, (35 - ae.k(dO)) + (ds * 25), 37 - ae.k(dO), 20);
        }
        this.dr = 0;
    }

    private static int f(int i2) {
        switch (i2) {
            case 0:
            case 2:
            case 5:
            case 8:
            default:
                return 0;
            case 1:
                return 27;
            case 3:
                return -32;
            case 4:
                return -32;
            case 6:
                return 27;
            case 7:
                return 27;
        }
    }

    private static void f(o oVar) {
        oVar.setColor(8415042);
        oVar.fillRect(35, 81, 169, 148);
        oVar.setColor(10983781);
        oVar.fillRect(36, 82, 167, 146);
        oVar.setColor(8415042);
        for (int i2 = 1; i2 < 8; i2++) {
            oVar.e((i2 * 21) + 35, 81, (i2 * 21) + 35, 228);
        }
        for (int i3 = 0; i3 < 7; i3++) {
            oVar.e(35, (i3 * 21) + 81, 203, (i3 * 21) + 81);
        }
    }

    private static int k(int i2) {
        switch (i2) {
            case 0:
            default:
                return 0;
            case 1:
                return 20;
            case 2:
                return 36;
            case 3:
                return 42;
            case 4:
                return 66;
            case 5:
                return 69;
            case 6:
                return 71;
            case 7:
                return 95;
            case 8:
                return a.UID;
        }
    }

    public final void a() {
        this.dh = null;
        this.dg = null;
        this.di = null;
        aq = null;
        this.dI = null;
        this.dj = null;
        this.df = null;
        this.dz = null;
        this.av = null;
        this.dC = null;
        this.dF = null;
        this.ah = null;
        this.dm = null;
        this.dk = null;
        this.dl = null;
        this.dq = null;
        this.dn = null;
        this.bi = null;
        dJ = null;
        dK = null;
        this.dL = null;
        this.dM = null;
        this.f0do = null;
        this.dp = null;
        co = null;
        cS = null;
        cO = null;
    }

    public final void b() {
        try {
            aq = new int[5];
            this.dh = p.C("/ui/equipicon.png");
            this.dg = p.C("/ui/weaponicon.png");
            this.dI = p.C("/ui/menuselecticon.png");
            this.di = p.C("/ui/itemicon.png");
            this.df = p.C("/ui/EquipRole.png");
            this.dF = p.C("/ui/menuword.png");
            this.dj = p.C("/ui/roleface.png");
            this.dz = p.C("/ui/rolename.png");
            this.dC = p.C("/ui/word2.png");
            this.av = p.C("/ui/word.png");
            this.ah = p.C("/ui/longwen.png");
            this.dm = p.C("/ui/jianbian.png");
            this.dk = p.C("/ui/number1.png");
            this.dl = p.C("/ui/number2.png");
            dJ = p.C("/ui/kuang5.png");
            dK = p.C("/ui/kuang6.png");
            this.dL = p.C("/ui/kuang7.png");
            this.dM = p.C("/ui/kuang8.png");
            this.bi = p.C("/ui/kuang4.png");
            this.dq = p.C("/ui/cursor.png");
            this.dn = p.C("/ui/mq.png");
            this.f0do = p.C("/ui/gang.png");
            this.dp = p.C("/ui/jia.png");
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        v();
        if (!m.o.equals("N7370")) {
            int[] iArr = new int[28600];
            co = iArr;
            int i2 = dQ;
            for (int i3 = 0; i3 < 28600; i3++) {
                iArr[i3] = i2;
            }
        }
        Y.k = true;
    }

    public final void b(o oVar) {
        int i2;
        int i3;
        int i4 = ((31 - ah.e) / 2) + 249;
        switch (a) {
            case 0:
                e(oVar);
                c(oVar);
                int length = y.bF.length;
                boolean z2 = false;
                int i5 = 0;
                int i6 = 0;
                while (i5 < 3) {
                    a(oVar, 34, (i5 * 51) + 77, 172, 48, 15247695, 12331540, 1, 1, this.ah, 3, 1, 0, this.ah.getWidth(), 0, 0, this.dM, 1, 1, 1, 1, 2, 0);
                    boolean z3 = z2;
                    while (!z3 && y.bF[i6].aM) {
                        int i7 = y.bF[i6].aW;
                        int i8 = y.bF[i6].aX + y.bF[(this.ae + i5) - this.ad].dt;
                        int i9 = y.bF[i6].aY;
                        int i10 = y.bF[i6].aZ + y.bF[(this.ae + i5) - this.ad].du;
                        int i11 = y.bF[i6].ae;
                        int i12 = y.bF[i6].aV;
                        int i13 = y.bF[i6].ad;
                        ae.a(oVar, 34, (i5 * 51) + 77, 172, 48, 15247695, 12331540, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                        a(oVar, 37, (i5 * 51) + 79, 34, 30, 0, 12697272, 0, 15658734, this.dj, 1, 1, y.bF[i6].ac, 28, 0, 0, dK, 1, 0, 0, 0, 2, 1);
                        b(oVar, 72, (i5 * 51) + 80, 44, 13, 0, 792076, 5123890);
                        b(oVar, a.UID, (i5 * 51) + 80, 44, 13, 28, 5123890, 9596493);
                        b(oVar, 39, (i5 * 51) + a.PHOTO, 29, 13, 0, 792076, 5123890);
                        b(oVar, 72, (i5 * 51) + 94, 30, 14, 0, 792076, 4949774);
                        b(oVar, 71, (i5 * 51) + a.ORG, 72, 13, 15, 5123890, 15247695);
                        b(oVar, MIDIControl.NOTE_ON, (i5 * 51) + a.ORG, 60, 13, 15, 5123890, 15247695);
                        b(oVar, 103, (i5 * 51) + 101, 92, 5, 0, 0, 792076);
                        ae.b(oVar, 104, (i5 * 51) + 101, (i11 * 88) / i12, 1, 13100944, 9488228, 7909707);
                        ae.a(oVar, this.dk, this.f0do, i12, i11, 105, (i5 * 51) + 96, 0);
                        oVar.setClip(74, (i5 * 51) + 82, this.dA, this.dB);
                        oVar.a(this.dz, 74 - (y.bF[i6].ac * this.dA), (i5 * 51) + 82, 20);
                        oVar.setClip(a.URL, (i5 * 51) + 82, this.dD, this.dE);
                        oVar.a(this.av, 118 - (this.dD * 5), (i5 * 51) + 82, 20);
                        ae.a(oVar, this.dk, i13, 153, (i5 * 51) + 83, 2);
                        oVar.setClip(40, (i5 * 51) + a.PUBLIC_KEY, this.dD, this.dE);
                        oVar.a(this.av, 40 - (this.dD * 2), (i5 * 51) + a.PUBLIC_KEY, 20);
                        oVar.setClip(75, (i5 * 51) + 97, this.dD, this.dE);
                        oVar.a(this.av, 75 - (this.dD * 4), (i5 * 51) + 97, 20);
                        oVar.setClip(73, (i5 * 51) + a.ORG, this.dn.getWidth() / 2, this.dn.getHeight());
                        oVar.a(this.dn, 73, (i5 * 51) + a.ORG, 20);
                        oVar.setClip(146, (i5 * 51) + a.ORG, this.dn.getWidth() / 2, this.dn.getHeight());
                        oVar.a(this.dn, 146 - (this.dn.getWidth() / 2), (i5 * 51) + a.ORG, 20);
                        ae.a(oVar, this.dk, this.f0do, i8, i7, 88, (i5 * 51) + a.PUBLIC_KEY, 0);
                        ae.a(oVar, this.dk, this.f0do, i10, i9, (int) f.OBEX_HTTP_CREATED, (i5 * 51) + a.PUBLIC_KEY, 0);
                        z3 = true;
                    }
                    if (i6 < length - 1) {
                        int i14 = i6 + 1;
                        if (z3) {
                            z2 = false;
                            i5++;
                            i6 = i14;
                        } else {
                            z2 = z3;
                            i6 = i14;
                        }
                    } else {
                        i5++;
                        z2 = z3;
                    }
                }
                a(oVar, i4, (a * 10) + ds);
                break;
            case 1:
                e(oVar);
                b(oVar, 0, 3);
                a(oVar, 208, 77, 5, 150, i, this.ae, dt, this.ad);
                a(oVar, i4, (a * 10) + ds);
                break;
            case 2:
                e(oVar);
                a(oVar, i4, (a * 10) + ds);
                ae.a(oVar, 34, 77, 172, f.OBEX_HTTP_ACCEPTED, 15247695, 6439452, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                oVar.a(this.df, ((240 - this.df.getWidth()) / 2) + 20, 84, 20);
                int width = ((240 - this.df.getWidth()) / 2) + 20 + 32;
                int i15 = (dt + this.ae) - this.ad;
                a(oVar, 37, 79, 34, 30, 0, 12697272, 0, 15658734, this.dj, 1, 1, y.bF[i15].ac, 28, 0, 0, dK, 1, 0, 0, 0, 2, 1);
                b(oVar, 72, 80, 44, 13, 0, 792076, 5123890);
                oVar.setClip(74, 82, this.dA, this.dB);
                oVar.a(this.dz, 74 - (y.bF[i15].ac * this.dA), 82, 20);
                oVar.setClip(0, 0, 240, 320);
                for (int i16 = 0; i16 < y.aR[0].length; i16++) {
                    if (y.aR[i15][i16] != -1) {
                        oVar.setClip(0, 0, 240, 320);
                        a(oVar, f(i16) + width, k(i16) + 84, 22, 22);
                        oVar.setClip(f(i16) + width + 1, k(i16) + 84 + 1, 20, 20);
                        if (i16 == 4) {
                            oVar.a(this.dg, ((f(i16) + width) + 1) - ((y.co[y.cE[y.aR[i15][i16]][0]] - 1) * 20), k(i16) + 84 + 1, 20);
                        } else {
                            oVar.a(this.dh, ((f(i16) + width) + 1) - ((y.cy[y.cG[y.aR[i15][i16]][0]] - 1) * 20), k(i16) + 84 + 1, 20);
                        }
                    }
                }
                oVar.setClip(0, 0, 240, 320);
                Q();
                a((f(this.ba) + width) - 1, (k(this.ba) + 84) - 1, 23, 23, 65280, 0, oVar, aa);
                int f = f(this.ba) + width + 5;
                int k2 = k(this.ba) + 84 + 22 + 5;
                if (y.aR[i15][this.ba] != -1) {
                    if (this.ba == 4) {
                        a(oVar, f, k2, 16, 130, 0, y.aR[i15][this.ba]);
                    } else {
                        a(oVar, f, k2, 16, 130, 1, y.aR[i15][this.ba]);
                    }
                }
                if (this.k) {
                    oVar.setClip(0, 0, 240, 320);
                    a(oVar, (String) null, "替换", "卸下", 150, this.aZ);
                    break;
                }
                break;
            case 3:
                e(oVar);
                a(oVar, i4, (a * 10) + ds);
                oVar.setClip(0, 0, 240, 320);
                int i17 = 0;
                while (true) {
                    int i18 = i17;
                    if (i18 >= 3) {
                        break;
                    } else {
                        ae.a(oVar, 34, (i18 * 54) + 77, 172, 51, 15247695, 12331540, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                        int i19 = 0;
                        oVar.setClip(0, 0, 240, 320);
                        oVar.setColor(16777215);
                        switch (i18) {
                            case 0:
                                oVar.a("武器", 120, (i18 * 54) + 76 + (((51 - (ah.e * 2)) - 3) / 2), 17);
                                if (y.cE != null) {
                                    i19 = y.cE.length;
                                }
                                ae.a(oVar, this.dk, this.f0do, 99, i19, 120, (i18 * 54) + 76 + (((51 - (ah.e * 2)) - 3) / 2) + ah.e + 3, 2);
                                break;
                            case 1:
                                oVar.a("防具", 120, (i18 * 54) + 76 + (((51 - (ah.e * 2)) - 3) / 2), 17);
                                if (y.cG != null) {
                                    i19 = y.cG.length;
                                }
                                ae.a(oVar, this.dk, this.f0do, 255, i19, 120, (i18 * 54) + 76 + (((51 - (ah.e * 2)) - 3) / 2) + ah.e + 3, 2);
                                break;
                            case 2:
                                oVar.a("道具", 120, (i18 * 54) + 76 + (((51 - (ah.e * 2)) - 3) / 2), 17);
                                if (y.cP != null) {
                                    i19 = y.cP.length;
                                }
                                ae.a(oVar, this.dk, this.f0do, 99, i19, 120, (i18 * 54) + 76 + (((51 - (ah.e * 2)) - 3) / 2) + ah.e + 3, 2);
                                break;
                        }
                        i17 = i18 + 1;
                    }
                }
                break;
            case 4:
                e(oVar);
                b(oVar, dt, 1);
                if (ak != null) {
                    a(oVar, al[this.bb - (this.bd - this.bc)], 24, 254, 182, 16777215);
                }
                b(oVar, 34, 130, 172, 101, 0, 15247695, 10983781);
                oVar.setColor(8415042);
                for (int i20 = 0; i20 < 4; i20++) {
                    oVar.fillRect(35, (i20 * 25) + 130 + 1, 170, 24);
                }
                oVar.setColor(6439452);
                oVar.fillRect(35, (this.bb * 25) + 130 + 1, 170, 24);
                ae.a(oVar, 34, 130, 172, 101, this.dM, 1, 1, 1, 1);
                if (ak != null) {
                    oVar.setColor(16777215);
                    for (int i21 = this.bd - this.bc; i21 < this.bd; i21++) {
                        oVar.a(ak[i21], 39, ((i21 - (this.bd - this.bc)) * 25) + 130 + 1, 20);
                    }
                }
                if (ak != null) {
                    ae.a(oVar, 207, 130, 5, 100, 0, 16777215, 11579568, ak.length, this.bd, this.bb, this.bc);
                    break;
                }
                break;
            case 5:
                e(oVar);
                int i22 = 0;
                while (true) {
                    int i23 = i22;
                    if (i23 >= 5) {
                        if (cl != null) {
                            ae.a(oVar, 208, 77, 5, 147, 0, 16777215, 11579568, cl.length, this.da, this.bf, this.bg);
                            break;
                        }
                    } else {
                        if (cl == null) {
                            a(oVar, 34, (i23 * 33) + 77, 172, 30, 15247695, 12331540, 1, 1, this.ah, 3, 1, 0, this.ah.getWidth(), 0, 0, this.dM, 1, 1, 1, 1, 2, 0);
                        } else if (i23 >= cl.length) {
                            a(oVar, 34, (i23 * 33) + 77, 172, 30, 15247695, 12331540, 1, 1, this.ah, 3, 1, 0, this.ah.getWidth(), 0, 0, this.dM, 1, 1, 1, 1, 2, 0);
                        } else {
                            if (this.bf == i23) {
                                a(oVar, 33, (i23 * 33) + 76, 175, 33, 0, 12169636, 0, 12331540, this.dm, 1, 3, 0, 0, 0, this.dm.getHeight(), this.dL, 1, 1, 1, 1, 2, 1);
                            } else {
                                ae.a(oVar, 34, (i23 * 33) + 77, 172, 30, 15247695, 12331540, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                            }
                            oVar.setColor(5191191);
                            oVar.a(cl[(this.da + i23) - this.bg], 120, (i23 * 33) + 77 + 6, 17);
                        }
                        i22 = i23 + 1;
                    }
                }
                break;
            case 6:
                e(oVar);
                int i24 = 0;
                while (true) {
                    int i25 = i24;
                    if (i25 >= 5) {
                        break;
                    } else {
                        if (this.du == i25) {
                            a(oVar, 33, (i25 * 33) + 76, 175, 33, 0, 12169636, 0, 12331540, this.dm, 1, 3, 0, 0, 0, this.dm.getHeight(), this.dL, 1, 1, 1, 1, 2, 1);
                        } else {
                            ae.a(oVar, 34, (i25 * 33) + 77, 172, 30, 15247695, 12331540, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                        }
                        oVar.setClip((240 - this.dG) / 2, (i25 * 33) + 77 + 8, this.dG, this.dH);
                        oVar.a(this.dF, 120, ((33 - this.dH) * i25) + 77 + 8, 17);
                        i24 = i25 + 1;
                    }
                }
            case 7:
                e(oVar);
                b(oVar, dt, 1);
                ae.a(oVar, 34, 130, 172, a.ORG, 15247695, 5057069, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                int i26 = 0;
                int i27 = 0;
                int i28 = 0;
                while (i28 < 6) {
                    b(oVar, 49, (i28 << 4) + 138, 120, 14, 38, 8415042, 10983781);
                    oVar.setClip(53, (i28 << 4) + 140, 30, 10);
                    oVar.a(this.dC, 53 - (i28 * 30), (i28 << 4) + 140, 20);
                    switch (i28) {
                        case 0:
                            i2 = y.bF[(dt + this.ae) - this.ad].ba;
                            i3 = y.bF[(dt + this.ae) - this.ad].dv;
                            break;
                        case 1:
                            i2 = y.bF[(dt + this.ae) - this.ad].bb;
                            i3 = y.bF[(dt + this.ae) - this.ad].dw;
                            break;
                        case 2:
                            i2 = y.bF[(dt + this.ae) - this.ad].bc;
                            i3 = y.bF[(dt + this.ae) - this.ad].dx;
                            break;
                        case 3:
                            i2 = y.bF[(dt + this.ae) - this.ad].bd;
                            i3 = y.bF[(dt + this.ae) - this.ad].dy;
                            break;
                        case 4:
                            i2 = y.bF[(dt + this.ae) - this.ad].bf;
                            i3 = y.bF[(dt + this.ae) - this.ad].dA;
                            break;
                        case 5:
                            i2 = y.bF[(dt + this.ae) - this.ad].bg;
                            i3 = y.bF[(dt + this.ae) - this.ad].dB;
                            break;
                        default:
                            i2 = i26;
                            i3 = i27;
                            break;
                    }
                    int f2 = (ae.f(i2) * 6) + 94;
                    ae.a(oVar, this.dk, i2, 89, (i28 << 4) + 142, 0);
                    oVar.setClip(0, 0, 240, 320);
                    if (i3 > 0) {
                        oVar.a(this.dp, f2, (i28 << 4) + 142, 20);
                        ae.a(oVar, this.dl, i3, f2 + 5 + 7, (i28 << 4) + 142, 0);
                    }
                    i28++;
                    i27 = i3;
                    i26 = i2;
                }
                break;
            case 8:
                e(oVar);
                a(oVar, i4, (a * 10) + ds);
                b(oVar, 0, 3);
                a(oVar, 208, 77, 5, 150, i, this.ae, dt, this.ad);
                break;
            case 9:
                e(oVar);
                ae.a(oVar, 34, 77, 172, f.OBEX_HTTP_ACCEPTED, 15247695, 6439452, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                oVar.setClip(0, 0, 240, 320);
                oVar.setColor(16777215);
                for (int i29 = 0; i29 < cI.length; i29++) {
                    oVar.a(cI[i29], 39, ((ah.e + 2) * i29) + 82, 20);
                }
                break;
            case 10:
                e(oVar);
                int i30 = 0;
                while (true) {
                    int i31 = i30;
                    if (i31 >= 3) {
                        break;
                    } else {
                        if (this.dv == i31) {
                            a(oVar, 33, (i31 * 54) + 76, 175, 54, 0, 12169636, 0, 12331540, this.dm, 1, 4, 0, 0, 0, this.dm.getHeight(), this.dL, 1, 1, 1, 1, 2, 1);
                        } else {
                            ae.a(oVar, 34, (i31 * 54) + 77, 172, 51, 15247695, 12331540, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                        }
                        oVar.setClip(0, 0, 240, 320);
                        oVar.setColor(16777215);
                        if (!y.I().ca) {
                            oVar.a(new StringBuffer().append("存档").append(i31 + 1).append(":").toString(), 120, (i31 * 54) + 76 + ((51 - ah.e) / 2), 17);
                        } else if (y.I().at[i31] == 1) {
                            String stringBuffer = new StringBuffer().append(y.I().cS[i31]).append(" 等级 ").append((int) y.I().aD[i31]).toString();
                            String stringBuffer2 = new StringBuffer().append((int) y.I().cT[i31]).append("/").append((int) y.I().aB[i31]).append("/").append((int) y.I().cU[i31]).append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR).append((int) y.I().cV[i31]).append(":").append((int) y.I().cW[i31]).append(":").append((int) y.I().cX[i31]).toString();
                            oVar.a(stringBuffer, 120, (i31 * 54) + 76 + (((51 - (ah.e * 2)) - 3) / 2) + 3, 17);
                            oVar.a(stringBuffer2, 120, (i31 * 54) + 76 + (((51 - (ah.e * 2)) - 3) / 2) + ah.e + 3, 17);
                        } else {
                            oVar.a(new StringBuffer().append("存档").append(i31 + 1).append(":").toString(), 120, (i31 * 54) + 76 + ((51 - ah.e) / 2), 17);
                        }
                        i30 = i31 + 1;
                    }
                }
            case 11:
                e(oVar);
                ae.a(oVar, 34, 77, 172, f.OBEX_HTTP_ACCEPTED, 15247695, 12331540, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                oVar.setColor(16777215);
                oVar.a("声音设置", 120, 80, 17);
                oVar.a("开", 80, 145, 24);
                oVar.a("关", (int) f.OBEX_HTTP_OK, 145, 20);
                oVar.setClip(0, 0, 240, 320);
                Q();
                if (ab != 0) {
                    a(159, (int) MIDIControl.NOTE_ON, ah.aP.A("开") + 1, ah.e + 1, 65280, 0, oVar, aa);
                    break;
                } else {
                    a((80 - ah.aP.A("开")) - 1, (int) MIDIControl.NOTE_ON, ah.aP.A("开") + 1, ah.e + 1, 65280, 0, oVar, aa);
                    break;
                }
            case 12:
                e(oVar);
                oVar.setClip(34, 77, 190, 170);
                break;
            case 13:
                e(oVar);
                ae.a(oVar, 34, 77, 172, f.OBEX_HTTP_ACCEPTED, 15247695, 12331540, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                oVar.setColor(16777215);
                oVar.a("游戏帮助", 120, 80, 17);
                oVar.setClip(0, ah.e + 77 + 3, 240, (162 - ah.e) - 10);
                for (int i32 = 0; i32 < cS.length; i32++) {
                    oVar.a(cS[i32], 37, ((i32 + 1 + dc) * (ah.e + 3)) + 77, 20);
                }
                break;
            case 14:
                e(oVar);
                ae.a(oVar, 34, 77, 172, f.OBEX_HTTP_ACCEPTED, 15247695, 12331540, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                oVar.setColor(16777215);
                oVar.a("关于", 120, 80, 17);
                oVar.setClip(0, ah.e + 77 + 3, 240, (162 - ah.e) - 10);
                for (int i33 = 0; i33 < cO.length; i33++) {
                    oVar.a(cO[i33], 37, ((i33 + 1 + dc) * (ah.e + 3)) + 77, 20);
                }
                break;
            case 15:
                e(oVar);
                break;
            case 16:
                e(oVar);
                if (this.aQ.bY) {
                    c(oVar);
                }
                oVar.setClip(0, 0, 240, 320);
                f(oVar);
                oVar.setColor(6439452);
                oVar.fillRect((dw * 21) + 35 + 1, (dx * 21) + 81 + 1, 20, 20);
                if (X != null) {
                    a(oVar, 207, 81, 5, MIDIControl.NOTE_ON, e(X.length, 8), this.aW, dx, this.aV);
                    int i34 = (this.aW - this.aV) << 3;
                    int length2 = 56 > X.length - ((this.aW - this.aV) << 3) ? X.length : ((this.aW - this.aV) << 3) + 56;
                    for (int i35 = i34; i35 < length2; i35++) {
                        int i36 = ((i35 % 8) * 21) + 35 + 1;
                        int i37 = (((i35 - i34) / 8) * 21) + 81 + 1;
                        oVar.setClip(i36, i37, 20, 20);
                        if (!this.aQ.bY || e == 6) {
                            oVar.a(this.dg, i36 - ((y.co[y.cE[X[i35]][0]] - 1) * 20), i37, 20);
                        } else {
                            oVar.a(this.dg, i36 - ((y.co[X[i35]] - 1) * 20), i37, 20);
                        }
                    }
                    int i38 = (dw * 21) + 35 + 1;
                    int i39 = (dx * 21) + 81 + 1;
                    oVar.setClip(0, 0, 240, 320);
                    Q();
                    a(i38 - 1, i39 - 1, 21, 21, 65280, 0, oVar, aa);
                    if (X == null) {
                        System.out.println("No Weapon!");
                    } else {
                        a(oVar, i38 + 5, i39 + 20 + 5, 16, 130, 0, X[(((dx + this.aW) - this.aV) << 3) + dw]);
                    }
                }
                if (this.aQ.bY && this.k) {
                    oVar.setClip(0, 0, 240, 320);
                    if (e != 6) {
                        a(oVar, (String) null, "购买", "取消", 150, this.aZ);
                        break;
                    } else {
                        a(oVar, (String) null, "卖出", "取消", 150, this.aZ);
                        break;
                    }
                }
                break;
            case 17:
                e(oVar);
                if (this.aQ.bY) {
                    c(oVar);
                }
                oVar.setClip(0, 0, 240, 320);
                f(oVar);
                oVar.setColor(6439452);
                oVar.fillRect((dw * 21) + 35 + 1, (dx * 21) + 81 + 1, 20, 20);
                if (Z != null) {
                    a(oVar, 207, 81, 5, MIDIControl.NOTE_ON, e(Z.length, 8), this.aW, dx, this.aV);
                    int i40 = (this.aW - this.aV) << 3;
                    int length3 = 56 > Z.length - ((this.aW - this.aV) << 3) ? Z.length : ((this.aW - this.aV) << 3) + 56;
                    for (int i41 = i40; i41 < length3; i41++) {
                        int i42 = ((i41 % 8) * 21) + 35 + 1;
                        int i43 = (((i41 - i40) / 8) * 21) + 81 + 1;
                        oVar.setClip(i42, i43, 20, 20);
                        if (!this.aQ.bY || e == 6) {
                            oVar.a(this.dh, i42 - ((y.cy[y.cG[Z[i41]][0]] - 1) * 20), i43, 20);
                        } else {
                            oVar.a(this.dh, i42 - ((y.cy[Z[i41]] - 1) * 20), i43, 20);
                        }
                    }
                    int i44 = (dw * 21) + 35 + 1;
                    int i45 = (dx * 21) + 81 + 1;
                    oVar.setClip(0, 0, 240, 320);
                    Q();
                    a(i44 - 1, i45 - 1, 21, 21, 65280, 0, oVar, aa);
                    if (Z == null) {
                        System.out.println("No Equip!");
                    } else {
                        a(oVar, i44 + 5, i45 + 20 + 5, 16, 130, 1, Z[(((dx + this.aW) - this.aV) << 3) + dw]);
                    }
                }
                if (this.aQ.bY && this.k) {
                    oVar.setClip(0, 0, 240, 320);
                    if (e != 6) {
                        a(oVar, (String) null, "购买", "取消", 150, this.aZ);
                        break;
                    } else {
                        a(oVar, (String) null, "卖出", "取消", 150, this.aZ);
                        break;
                    }
                }
                break;
            case 18:
                e(oVar);
                if (this.aQ.bY) {
                    c(oVar);
                }
                oVar.setClip(0, 0, 240, 320);
                f(oVar);
                oVar.setColor(6439452);
                oVar.fillRect((dw * 21) + 35 + 1, (dx * 21) + 81 + 1, 20, 20);
                if (this.aQ.bY) {
                    if (e == 6) {
                        if (y.cP == null) {
                            return;
                        }
                    } else if (ar == null) {
                        return;
                    }
                } else if (y.cP == null) {
                    return;
                }
                if (!(y.cP == null && ar == null)) {
                    int length4 = this.aQ.bY ? e == 6 ? y.cP.length : ar.length : y.cP.length;
                    a(oVar, 207, 81, 5, MIDIControl.NOTE_ON, e(length4, 8), this.aW, dx, this.aV);
                    int i46 = (this.aW - this.aV) << 3;
                    if (56 <= length4 - ((this.aW - this.aV) << 3)) {
                        length4 = ((this.aW - this.aV) << 3) + 56;
                    }
                    for (int i47 = i46; i47 < length4; i47++) {
                        int i48 = ((i47 % 8) * 21) + 35 + 1;
                        int i49 = (((i47 - i46) / 8) * 21) + 81 + 1;
                        oVar.setClip(i48, i49, 20, 20);
                        if (!this.aQ.bY || e == 6) {
                            oVar.a(this.di, i48 - ((y.cM[y.cP[i47][0]] - 1) * 20), i49, 20);
                        } else {
                            oVar.a(this.di, i48 - ((y.cM[ar[i47]] - 1) * 20), i49, 20);
                        }
                        oVar.setClip(0, 0, 240, 320);
                        if (!this.aQ.bY || e == 6) {
                            ae.a(oVar, this.dk, y.cP[i47][1], i48 + 20, i49 + 13, 1);
                        } else {
                            ae.a(oVar, this.dk, 1, i48 + 20, i49 + 13, 1);
                        }
                    }
                    int i50 = (dw * 21) + 35 + 1;
                    int i51 = (dx * 21) + 81 + 1;
                    oVar.setClip(0, 0, 240, 320);
                    Q();
                    a(i50 - 1, i51 - 1, 21, 21, 65280, 0, oVar, aa);
                    a(oVar, i50 + 5, i51 + 20 + 5, 16, 130, (((dx + this.aW) - this.aV) << 3) + dw);
                }
                if (this.k) {
                    oVar.setClip(0, 0, 240, 320);
                    if (this.aQ.bY) {
                        if (e != 6) {
                            a(oVar, (String) null, "购买", "取消", 150, this.aZ);
                            break;
                        } else {
                            a(oVar, (String) null, "卖出", "取消", 150, this.aZ);
                            break;
                        }
                    } else {
                        a(oVar, (String) null, "使用", "丢弃", 150, this.aZ);
                        break;
                    }
                }
                break;
            case 19:
                e(oVar);
                oVar.setClip(0, 0, 240, 320);
                f(oVar);
                oVar.setColor(6439452);
                oVar.fillRect((dw * 21) + 35 + 1, (dx * 21) + 81 + 1, 20, 20);
                if (cn != null) {
                    a(oVar, 207, 81, 5, MIDIControl.NOTE_ON, e(cn.length, 8), this.aW, dx, this.aV);
                    int i52 = this.ba == 4 ? 0 : 1;
                    int i53 = (this.aW - this.aV) << 3;
                    int length5 = 56 > cn.length - ((this.aW - this.aV) << 3) ? cn.length : ((this.aW - this.aV) << 3) + 56;
                    for (int i54 = i53; i54 < length5; i54++) {
                        int i55 = ((i54 % 8) * 21) + 35 + 1;
                        int i56 = (((i54 - i53) / 8) * 21) + 81 + 1;
                        oVar.setClip(i55, i56, 20, 20);
                        if (i52 == 0) {
                            oVar.a(this.dg, i55 - ((y.co[y.cE[cn[i54]][0]] - 1) * 20), i56, 20);
                        } else {
                            oVar.a(this.dh, i55 - ((y.cy[y.cG[cn[i54]][0]] - 1) * 20), i56, 20);
                        }
                    }
                    int i57 = (dw * 21) + 35 + 1;
                    int i58 = (dx * 21) + 81 + 1;
                    oVar.setClip(0, 0, 240, 320);
                    Q();
                    a(i57 - 1, i58 - 1, 21, 21, 65280, 0, oVar, aa);
                    a(oVar, i57 + 5, i58 + 20 + 5, 16, 130, i52, cn[(((dx + this.aW) - this.aV) << 3) + dw]);
                    break;
                }
                break;
            case c.INT_16 /*20*/:
                e(oVar);
                b(oVar, 0, 3);
                a(oVar, 208, 77, 5, 150, i, this.ae, dt, this.ad);
                break;
            case 21:
                if (!k.k) {
                    e(oVar);
                    int i59 = 0;
                    while (true) {
                        int i60 = i59;
                        if (i60 >= 5) {
                            if (dd != null) {
                                ae.a(oVar, 208, 77, 5, 147, 0, 16777215, 11579568, dd.length, this.z, this.x, this.y);
                                break;
                            }
                        } else {
                            if (dd == null) {
                                a(oVar, 34, (i60 * 33) + 77, 172, 30, 15247695, 12331540, 1, 1, this.ah, 3, 1, 0, this.ah.getWidth(), 0, 0, this.dM, 1, 1, 1, 1, 2, 0);
                            } else if (i60 >= dd.length) {
                                a(oVar, 34, (i60 * 33) + 77, 172, 30, 15247695, 12331540, 1, 1, this.ah, 3, 1, 0, this.ah.getWidth(), 0, 0, this.dM, 1, 1, 1, 1, 2, 0);
                            } else {
                                if (this.x == i60) {
                                    a(oVar, 33, (i60 * 33) + 76, 175, 33, 0, 12169636, 0, 12331540, this.dm, 1, 3, 0, 0, 0, this.dm.getHeight(), this.dL, 1, 1, 1, 1, 2, 1);
                                } else {
                                    ae.a(oVar, 34, (i60 * 33) + 77, 172, 30, 15247695, 12331540, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                                }
                                oVar.setColor(5191191);
                                oVar.a(dd[(this.z + i60) - this.y], 120, (i60 * 33) + 77 + 6, 17);
                            }
                            i59 = i60 + 1;
                        }
                    }
                } else {
                    k.b(oVar);
                    return;
                }
                break;
            case 22:
                if (!k.k) {
                    e(oVar);
                    int i61 = 0;
                    while (true) {
                        int i62 = i61;
                        if (i62 >= 5) {
                            break;
                        } else {
                            if (de == null) {
                                a(oVar, 34, (i62 * 33) + 77, 172, 30, 15247695, 12331540, 1, 1, this.ah, 3, 1, 0, this.ah.getWidth(), 0, 0, this.dM, 1, 1, 1, 1, 2, 0);
                            } else if (i62 >= de.length) {
                                a(oVar, 34, (i62 * 33) + 77, 172, 30, 15247695, 12331540, 1, 1, this.ah, 3, 1, 0, this.ah.getWidth(), 0, 0, this.dM, 1, 1, 1, 1, 2, 0);
                            } else {
                                if (this.db == i62) {
                                    a(oVar, 33, (i62 * 33) + 76, 175, 33, 0, 12169636, 0, 12331540, this.dm, 1, 3, 0, 0, 0, this.dm.getHeight(), this.dL, 1, 1, 1, 1, 2, 1);
                                } else {
                                    ae.a(oVar, 34, (i62 * 33) + 77, 172, 30, 15247695, 12331540, 1, 1, this.dM, 1, 1, 1, 1, 2, 0);
                                }
                                oVar.setColor(5191191);
                                oVar.a(de[i62], 120, (i62 * 33) + 77 + 6, 17);
                            }
                            i61 = i62 + 1;
                        }
                    }
                } else {
                    k.b(oVar);
                    return;
                }
        }
        if (this.aX != 0) {
            oVar.setClip(0, 0, 240, 320);
            c(oVar, this.aX, 200);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: y.a(int, boolean):void
     arg types: [int, int]
     candidates:
      y.a(int, int):void
      y.a(ai, java.io.DataOutputStream):void
      y.a(int, boolean):void */
    public final void o() {
        int i2 = 0;
        if (Y.h(4096) || Y.h(4)) {
            switch (a) {
                case 1:
                    if (this.ae > this.ad && dt == 0) {
                        this.ae--;
                    }
                    if (dt > 0) {
                        dt--;
                        break;
                    }
                    break;
                case 2:
                    if (!this.k) {
                        if (this.ba <= 0) {
                            this.ba = 8;
                            break;
                        } else {
                            this.ba--;
                            break;
                        }
                    }
                    break;
                case 4:
                    if (this.bd > this.bc && this.bb == 0) {
                        this.bd--;
                    }
                    if (this.bb > 0) {
                        this.bb--;
                        break;
                    }
                    break;
                case 5:
                    if (cl != null) {
                        if (this.da > this.bg && this.bf == 0) {
                            this.da--;
                        }
                        if (this.bf > 0) {
                            this.bf--;
                            break;
                        }
                    }
                    break;
                case 6:
                    if (this.du <= 0) {
                        this.du = 4;
                        break;
                    } else {
                        this.du--;
                        break;
                    }
                case 8:
                    if (this.aX == 0) {
                        if (this.ae > this.ad && dt == 0) {
                            this.ae--;
                        }
                        if (dt > 0) {
                            dt--;
                            break;
                        }
                    }
                    break;
                case 10:
                    if (!this.am) {
                        if (this.dv <= 0) {
                            this.dv = 2;
                            break;
                        } else {
                            this.dv--;
                            break;
                        }
                    } else {
                        return;
                    }
                case 12:
                    if (p < 0) {
                        p++;
                        break;
                    }
                    break;
                case 13:
                    if (dc < 0) {
                        dc++;
                        break;
                    }
                    break;
                case 14:
                    if (dc < 0) {
                        dc++;
                        break;
                    }
                    break;
                case 16:
                    if (!this.k && this.aX <= 0 && X != null) {
                        if (this.aW > this.aV && dx == 0) {
                            this.aW--;
                        }
                        if (dx > 0) {
                            dx--;
                            break;
                        }
                    }
                    break;
                case 17:
                    if (!this.k && this.aX <= 0 && Z != null) {
                        if (this.aW > this.aV && dx == 0) {
                            this.aW--;
                        }
                        if (dx > 0) {
                            dx--;
                            break;
                        }
                    }
                    break;
                case 18:
                    if (!this.k && this.aX <= 0) {
                        if (this.aW > this.aV && dx == 0) {
                            this.aW--;
                        }
                        if (dx > 0) {
                            dx--;
                            break;
                        }
                    }
                    break;
                case 19:
                    if (cn != null) {
                        if (this.aW > this.aV && dx == 0) {
                            this.aW--;
                        }
                        if (dx > 0) {
                            dx--;
                            break;
                        }
                    }
                    break;
                case c.INT_16 /*20*/:
                    if (this.aX <= 0) {
                        if (this.ae > this.ad && dt == 0) {
                            this.ae--;
                        }
                        if (dt > 0) {
                            dt--;
                            break;
                        }
                    }
                    break;
                case 21:
                    if (this.z > this.y && this.x == 0) {
                        this.z--;
                    }
                    if (this.x > 0) {
                        this.x--;
                        break;
                    }
                    break;
                case 22:
                    if (this.db > 0) {
                        this.db--;
                        break;
                    }
                    break;
            }
        } else if (Y.h(8192) || Y.h(256)) {
            switch (a) {
                case 1:
                    if (this.ae < i && dt == this.ad - 1) {
                        this.ae++;
                    }
                    if (dt < this.ad - 1) {
                        dt++;
                        break;
                    }
                    break;
                case 2:
                    if (!this.k) {
                        if (this.ba >= 8) {
                            this.ba = 0;
                            break;
                        } else {
                            this.ba++;
                            break;
                        }
                    }
                    break;
                case 4:
                    if (this.bd < ak.length && this.bb == this.bc - 1) {
                        this.bd++;
                    }
                    if (this.bb < this.bc - 1) {
                        this.bb++;
                        break;
                    }
                    break;
                case 5:
                    if (cl != null) {
                        if (this.da < cl.length && this.bf == this.bg - 1) {
                            this.da++;
                        }
                        if (this.bf < this.bg - 1) {
                            this.bf++;
                            break;
                        }
                    }
                    break;
                case 6:
                    if (this.du >= 4) {
                        this.du = 0;
                        break;
                    } else {
                        this.du++;
                        break;
                    }
                case 8:
                    if (this.aX == 0) {
                        if (this.ae < i && dt == this.ad - 1) {
                            this.ae++;
                        }
                        if (dt < this.ad - 1) {
                            dt++;
                            break;
                        }
                    }
                    break;
                case 10:
                    if (!this.am) {
                        if (this.dv >= 2) {
                            this.dv = 0;
                            break;
                        } else {
                            this.dv++;
                            break;
                        }
                    } else {
                        return;
                    }
                case 12:
                    p--;
                    break;
                case 13:
                    if (Math.abs(dc) < cS.length - 1) {
                        dc--;
                        break;
                    }
                    break;
                case 14:
                    if (Math.abs(dc) < cO.length - 1) {
                        dc--;
                        break;
                    }
                    break;
                case 16:
                    if (!this.k && this.aX <= 0 && X != null) {
                        int i3 = this.aW;
                        if (this.aW < e(X.length, 8) && dx == 6) {
                            this.aW++;
                        }
                        if (((((dx + 1) + i3) - this.aV) << 3) + dw + 1 > X.length) {
                            if (dx == 6 && this.aW > i3) {
                                dx--;
                                break;
                            }
                        } else if (dx < 6) {
                            dx++;
                            break;
                        }
                    }
                    break;
                case 17:
                    if (!this.k && this.aX <= 0 && Z != null) {
                        int i4 = this.aW;
                        if (this.aW < e(Z.length, 8) && dx == 6) {
                            this.aW++;
                        }
                        if (((((dx + 1) + i4) - this.aV) << 3) + dw + 1 > Z.length) {
                            if (dx == 6 && this.aW > i4) {
                                dx--;
                                break;
                            }
                        } else if (dx < 6) {
                            dx++;
                            break;
                        }
                    }
                    break;
                case 18:
                    if (!this.k && this.aX <= 0) {
                        if (this.aQ.bY) {
                            if (e != 6) {
                                i2 = ar.length;
                            } else if (y.cP != null) {
                                i2 = y.cP.length;
                            }
                        } else if (y.cP != null) {
                            i2 = y.cP.length;
                        }
                        if (y.cP != null) {
                            int i5 = this.aW;
                            if (this.aW < e(i2, 8) && dx == 6) {
                                this.aW++;
                            }
                            if (((((dx + 1) + i5) - this.aV) << 3) + dw + 1 > i2) {
                                if (dx == 6 && this.aW > i5) {
                                    dx--;
                                    break;
                                }
                            } else if (dx < 6) {
                                dx++;
                                break;
                            }
                        }
                    }
                    break;
                case 19:
                    if (cn != null) {
                        int i6 = this.aW;
                        if (this.aW < e(cn.length, 8) && dx == 6) {
                            this.aW++;
                        }
                        if (((((dx + 1) + i6) - this.aV) << 3) + dw + 1 > cn.length) {
                            if (dx == 6 && this.aW > i6) {
                                dx--;
                                break;
                            }
                        } else if (dx < 6) {
                            dx++;
                            break;
                        }
                    }
                    break;
                case c.INT_16 /*20*/:
                    if (this.aX <= 0) {
                        if (this.ae < i && dt == this.ad - 1) {
                            this.ae++;
                        }
                        if (dt < this.ad - 1) {
                            dt++;
                            break;
                        }
                    }
                    break;
                case 21:
                    if (this.z < dd.length && this.x == this.y - 1) {
                        this.z++;
                    }
                    if (this.x < this.y - 1) {
                        this.x++;
                        break;
                    }
                    break;
                case 22:
                    if (this.db < 4) {
                        this.db++;
                        break;
                    }
                    break;
            }
        } else if (Y.h(65536) || Y.h(131072) || Y.h(32)) {
            switch (a) {
                case 0:
                    switch (ds) {
                        case 0:
                            a = 1;
                            b = 5;
                            aq[0] = 11;
                            aq[1] = 2;
                            aq[2] = 1;
                            aq[3] = 3;
                            aq[4] = 12;
                            break;
                        case 1:
                            a = 5;
                            b = 1;
                            aq[0] = 11;
                            this.bf = 0;
                            this.bg = 5;
                            this.da = this.bg;
                            if (y.cQ != null) {
                                String[] strArr = cl;
                                if (strArr.length < this.bg) {
                                    this.da = strArr.length;
                                    this.bg = strArr.length;
                                    break;
                                } else {
                                    this.da = this.bg;
                                    break;
                                }
                            }
                            break;
                        case 2:
                            a = 6;
                            b = 1;
                            aq[0] = 11;
                            break;
                    }
                case 1:
                    switch (ds) {
                        case 0:
                            a = 7;
                            b = 0;
                            break;
                        case 1:
                            a = 2;
                            b = 1;
                            aq[0] = 11;
                            break;
                        case 2:
                            a = 3;
                            b = 3;
                            aq[0] = 13;
                            aq[1] = 14;
                            aq[2] = 15;
                            break;
                        case 3:
                            ac = 4;
                            break;
                        case 4:
                            a = 8;
                            b = 0;
                            break;
                    }
                case 2:
                    if (this.k) {
                        if (this.ba != 4) {
                            ac = 2;
                            break;
                        } else {
                            ac = 1;
                            break;
                        }
                    } else if (this.ba != 4) {
                        if (y.aR[(dt + this.ae) - this.ad][this.ba] == -1) {
                            ac = 2;
                            break;
                        } else {
                            this.k = true;
                            break;
                        }
                    } else if (y.aR[(dt + this.ae) - this.ad][this.ba] == -1) {
                        ac = 1;
                        break;
                    } else {
                        this.k = true;
                        break;
                    }
                case 3:
                    switch (ds) {
                        case 0:
                            ac = 1;
                            break;
                        case 1:
                            ac = 2;
                            break;
                        case 2:
                            ac = 3;
                            break;
                    }
                case 5:
                    if (cl != null) {
                        a = 9;
                        b = 0;
                        cI = ah.f(ct[(this.bf + this.da) - this.bg], f.OBEX_HTTP_ACCEPTED);
                        break;
                    }
                    break;
                case 6:
                    switch (this.du) {
                        case 0:
                            a = 10;
                            b = 0;
                            break;
                        case 1:
                            a = 11;
                            b = 0;
                            break;
                        case 2:
                            a = 13;
                            b = 0;
                            break;
                        case 3:
                            a = 14;
                            b = 0;
                            break;
                        case 4:
                            l.a(40, 30);
                            y.cE = null;
                            y.cG = null;
                            y.cP = null;
                            ak.am = false;
                            j.r().k = false;
                            this.aQ.a();
                            this.aQ.ap = null;
                            this.aQ.d((byte) 2);
                            break;
                    }
                case 8:
                    if (this.aX == 0) {
                        if (!y.bF[(this.ae - this.ad) + dt].aM) {
                            if (h >= 3) {
                                this.aX = 3;
                                break;
                            } else {
                                y.bF[(this.ae - this.ad) + dt].aM = true;
                                h++;
                                break;
                            }
                        } else if (h <= 1) {
                            this.aX = 2;
                            break;
                        } else {
                            y.bF[(this.ae - this.ad) + dt].aM = false;
                            h--;
                            break;
                        }
                    } else {
                        this.aX = 0;
                        this.aY = 0;
                        this.aZ = 0;
                        break;
                    }
                case 10:
                    if (this.aX <= 0) {
                        if (!this.am) {
                            this.aQ.g((byte) this.dv);
                            this.aQ.C(this.dv);
                            this.am = true;
                            break;
                        }
                    } else {
                        this.aX = 0;
                        this.aY = 0;
                        this.aZ = 0;
                        break;
                    }
                    break;
                case 11:
                    if (ab != 0) {
                        Y.o();
                        y.cc = false;
                        break;
                    } else {
                        y.cc = true;
                        Y.a(this.aQ.t, this.aQ.p);
                        break;
                    }
                case 16:
                    if (this.aQ.bY) {
                        if (!this.k) {
                            if (this.aX <= 0) {
                                if (X != null) {
                                    this.k = true;
                                    break;
                                }
                            } else {
                                this.aX = 0;
                                this.aY = 0;
                                this.aZ = 0;
                                break;
                            }
                        } else {
                            switch (this.aZ) {
                                case 0:
                                    int i7 = (((dx + this.aW) - this.aV) << 3) + dw;
                                    if (e != 6) {
                                        if (y.cn[X[i7]] > y.aa) {
                                            this.k = false;
                                            this.aX = 4;
                                            break;
                                        } else {
                                            y.aa -= y.cn[X[i7]];
                                            y.a(X[i7], true);
                                        }
                                    } else {
                                        y.aa += y.cn[y.cE[i7][0]] / 4;
                                        y.g(i7);
                                        if (X != null && (((dx + this.aW) - this.aV) << 3) + dw == X.length) {
                                            if (dw > 0) {
                                                dw--;
                                            } else {
                                                dx--;
                                                dw = 7;
                                            }
                                        }
                                        this.k = false;
                                        break;
                                    }
                                case 1:
                                    this.k = false;
                                    break;
                            }
                        }
                    }
                    break;
                case 17:
                    if (this.aQ.bY) {
                        if (!this.k) {
                            if (this.aX <= 0) {
                                if (Z != null) {
                                    this.k = true;
                                    break;
                                }
                            } else {
                                this.aX = 0;
                                this.aY = 0;
                                this.aZ = 0;
                                break;
                            }
                        } else {
                            switch (this.aZ) {
                                case 0:
                                    int i8 = (((dx + this.aW) - this.aV) << 3) + dw;
                                    if (e != 6) {
                                        if (y.cx[Z[i8]] > y.aa) {
                                            this.k = false;
                                            this.aX = 4;
                                            break;
                                        } else {
                                            y.aa -= y.cx[Z[i8]];
                                            y.b(Z[i8], true);
                                        }
                                    } else {
                                        y.aa += y.cx[y.cG[i8][0]] / 4;
                                        y.w(i8);
                                        if (Z != null && (((dx + this.aW) - this.aV) << 3) + dw == Z.length) {
                                            if (dw > 0) {
                                                dw--;
                                            } else {
                                                dx--;
                                                dw = 7;
                                            }
                                        }
                                        this.k = false;
                                        break;
                                    }
                                case 1:
                                    this.k = false;
                                    break;
                            }
                        }
                    }
                    break;
                case 18:
                    if (!this.k) {
                        if (this.aX <= 0) {
                            if (!this.aQ.bY) {
                                if (y.cP != null) {
                                    this.k = true;
                                    break;
                                }
                            } else if (e != 6) {
                                if (ar != null) {
                                    this.k = true;
                                    break;
                                }
                            } else if (y.cP != null) {
                                this.k = true;
                                break;
                            }
                        } else {
                            this.aX = 0;
                            this.aY = 0;
                            this.aZ = 0;
                            break;
                        }
                    } else if (!this.aQ.bY) {
                        int i9 = y.cP[(((dx + this.aW) - this.aV) << 3) + dw][0];
                        switch (this.aZ) {
                            case 0:
                                switch (y.cJ[y.cP[(((dx + this.aW) - this.aV) << 3) + dw][0]]) {
                                    case 0:
                                        this.dy = y.f(i9);
                                        a = 20;
                                        b = 0;
                                        break;
                                    case 1:
                                        this.dy = y.f(i9);
                                        a = 20;
                                        b = 0;
                                        break;
                                    case 2:
                                        this.k = false;
                                        this.aX = 1;
                                        break;
                                    case 3:
                                        this.dy = y.f(i9);
                                        a = 20;
                                        b = 0;
                                        break;
                                }
                                this.k = false;
                                break;
                            case 1:
                                y.x((((dx + this.aW) - this.aV) << 3) + dw);
                                if (y.cP != null && (((dx + this.aW) - this.aV) << 3) + dw == y.cP.length) {
                                    if (dw > 0) {
                                        dw--;
                                    } else {
                                        dx--;
                                        dw = 7;
                                    }
                                }
                                this.k = false;
                                break;
                        }
                    } else {
                        switch (this.aZ) {
                            case 0:
                                int i10 = (((dx + this.aW) - this.aV) << 3) + dw;
                                if (e != 6) {
                                    if (y.cL[ar[i10]] > y.aa) {
                                        this.k = false;
                                        this.aX = 4;
                                        break;
                                    } else {
                                        y.aa -= y.cL[ar[i10]];
                                        y.a(ar[i10], 1);
                                    }
                                } else {
                                    y.aa += y.cL[y.cP[i10][0]] / 4;
                                    y.i(y.cP[i10][0], 1);
                                    if (y.cP != null && (((dx + this.aW) - this.aV) << 3) + dw == y.cP.length) {
                                        if (dw > 0) {
                                            dw--;
                                        } else {
                                            dx--;
                                            dw = 7;
                                        }
                                    }
                                    this.k = false;
                                    break;
                                }
                            case 1:
                                this.k = false;
                                break;
                        }
                    }
                    break;
                case 19:
                    if (cn != null) {
                        ac = 1;
                        break;
                    }
                    break;
                case c.INT_16 /*20*/:
                    if (this.aX <= 0) {
                        if (this.dy <= 0) {
                            if (y.cP != null && (((dx + this.aW) - this.aV) << 3) + dw == y.cP.length) {
                                if (dw > 0) {
                                    dw--;
                                } else {
                                    dx--;
                                    dw = 7;
                                }
                            }
                            a = 18;
                            b = 1;
                            aq[0] = 11;
                            break;
                        } else {
                            y.b((dt + this.ae) - this.ad, y.cP[(((dx + this.aW) - this.aV) << 3) + dw][0], 1);
                            switch (y.ac) {
                                case 0:
                                    this.dy--;
                                    break;
                                case 1:
                                    this.aX = 7;
                                    break;
                                case 2:
                                    this.aX = 8;
                                    break;
                                case 3:
                                    this.aX = 9;
                                    break;
                            }
                        }
                    } else {
                        this.aX = 0;
                        this.aY = 0;
                        this.aZ = 0;
                        break;
                    }
                case 21:
                    switch ((this.x + this.z) - this.y) {
                        case 0:
                            a = 22;
                            b = 0;
                            break;
                        case 1:
                            this.cZ.a(1);
                            break;
                        case 2:
                            this.cZ.a(10);
                            break;
                        case 3:
                            this.cZ.a(7);
                            break;
                        case 4:
                            this.cZ.a(15);
                            break;
                    }
                case 22:
                    switch (this.db) {
                        case 0:
                            this.cZ.a(2);
                            break;
                        case 1:
                            this.cZ.a(3);
                            break;
                        case 2:
                            this.cZ.a(4);
                            break;
                        case 3:
                            this.cZ.a(5);
                            break;
                        case 4:
                            this.cZ.a(6);
                            break;
                    }
            }
            ds = 0;
        } else if (Y.h(262144)) {
            switch (a) {
                case 0:
                    l.a(40, 30);
                    ak.am = false;
                    break;
                case 1:
                    a = 0;
                    b = 4;
                    b = 3;
                    aq[0] = 0;
                    aq[1] = 4;
                    aq[2] = 5;
                    aq[3] = 6;
                    break;
                case 2:
                    if (!this.k) {
                        a = 1;
                        b = 5;
                        aq[0] = 11;
                        aq[1] = 2;
                        aq[2] = 1;
                        aq[3] = 3;
                        aq[4] = 12;
                        break;
                    } else {
                        this.k = false;
                        break;
                    }
                case 3:
                    if (!this.aQ.bY) {
                        a = 1;
                        b = 5;
                        aq[0] = 11;
                        aq[1] = 2;
                        aq[2] = 1;
                        aq[3] = 3;
                        aq[4] = 12;
                        break;
                    } else {
                        e = -1;
                        this.aQ.bX = false;
                        ak.am = false;
                        this.aQ.bY = false;
                        break;
                    }
                case 4:
                    a = 1;
                    b = 5;
                    aq[0] = 11;
                    aq[1] = 2;
                    aq[2] = 1;
                    aq[3] = 3;
                    aq[4] = 12;
                    S();
                    break;
                case 5:
                    a = 0;
                    b = 4;
                    b = 3;
                    aq[0] = 0;
                    aq[1] = 4;
                    aq[2] = 5;
                    aq[3] = 6;
                    S();
                    break;
                case 6:
                    a = 0;
                    b = 4;
                    b = 3;
                    aq[0] = 0;
                    aq[1] = 4;
                    aq[2] = 5;
                    aq[3] = 6;
                    this.du = 0;
                    break;
                case 7:
                    a = 1;
                    b = 5;
                    aq[0] = 11;
                    aq[1] = 2;
                    aq[2] = 1;
                    aq[3] = 3;
                    aq[4] = 12;
                    break;
                case 8:
                    a = 1;
                    b = 5;
                    aq[0] = 11;
                    aq[1] = 2;
                    aq[2] = 1;
                    aq[3] = 3;
                    aq[4] = 12;
                    break;
                case 9:
                    a = 5;
                    b = 1;
                    aq[0] = 11;
                    break;
                case 10:
                    a = 6;
                    b = 1;
                    aq[0] = 11;
                    this.dv = 0;
                    break;
                case 11:
                    a = 6;
                    b = 1;
                    aq[0] = 11;
                    break;
                case 12:
                    a = 6;
                    b = 1;
                    aq[0] = 11;
                    break;
                case 13:
                    a = 6;
                    b = 1;
                    aq[0] = 11;
                    dc = 0;
                    break;
                case 14:
                    a = 6;
                    b = 1;
                    aq[0] = 11;
                    dc = 0;
                    break;
                case 16:
                    if (!this.k) {
                        if (this.aX <= 0) {
                            if (!this.aQ.bY) {
                                a = 3;
                                b = 3;
                                aq[0] = 13;
                                aq[1] = 14;
                                aq[2] = 15;
                                X = null;
                            } else if (e == 6) {
                                a = 3;
                                b = 3;
                                aq[0] = 13;
                                aq[1] = 14;
                                aq[2] = 15;
                            } else {
                                e = -1;
                                this.aQ.bX = false;
                                ak.am = false;
                                this.aQ.bY = false;
                            }
                            X = null;
                            break;
                        } else {
                            this.aX = 0;
                            this.aY = 0;
                            this.aZ = 0;
                            break;
                        }
                    } else {
                        this.k = false;
                        this.aZ = 0;
                        break;
                    }
                case 17:
                    if (!this.k) {
                        if (this.aX <= 0) {
                            if (!this.aQ.bY || e == 6) {
                                a = 3;
                                b = 3;
                                aq[0] = 13;
                                aq[1] = 14;
                                aq[2] = 15;
                            } else {
                                e = -1;
                                this.aQ.bX = false;
                                ak.am = false;
                                this.aQ.bY = false;
                            }
                            Z = null;
                            break;
                        } else {
                            this.aX = 0;
                            this.aY = 0;
                            this.aZ = 0;
                            break;
                        }
                    } else {
                        this.k = false;
                        this.aZ = 0;
                        break;
                    }
                    break;
                case 18:
                    if (!this.k) {
                        if (this.aX <= 0) {
                            if (this.aQ.bY) {
                                if (e != 6) {
                                    e = -1;
                                    this.aQ.bX = false;
                                    ak.am = false;
                                    this.aQ.bY = false;
                                    ar = null;
                                    break;
                                } else {
                                    a = 3;
                                    b = 3;
                                    aq[0] = 13;
                                    aq[1] = 14;
                                    aq[2] = 15;
                                    break;
                                }
                            } else {
                                a = 3;
                                b = 3;
                                aq[0] = 13;
                                aq[1] = 14;
                                aq[2] = 15;
                                break;
                            }
                        } else {
                            this.aX = 0;
                            this.aY = 0;
                            this.aZ = 0;
                            break;
                        }
                    } else {
                        this.k = false;
                        this.aZ = 0;
                        break;
                    }
                case 19:
                    a = 2;
                    b = 1;
                    aq[0] = 11;
                    X = null;
                    Z = null;
                    cn = null;
                    break;
                case c.INT_16 /*20*/:
                    a = 18;
                    b = 1;
                    aq[0] = 11;
                    break;
                case 21:
                    if (!k.k) {
                        a = 0;
                        b = 4;
                        aq[0] = 0;
                        aq[1] = 4;
                        aq[2] = 5;
                        aq[3] = 6;
                        break;
                    }
                    break;
                case 22:
                    if (!k.k) {
                        a = 21;
                        b = 0;
                        break;
                    }
                    break;
            }
            ds = 0;
        } else if (Y.h(k.MONDAY) || Y.h(64)) {
            switch (a) {
                case 2:
                    if (this.k) {
                        if (this.aZ != 0) {
                            this.aZ = 0;
                            break;
                        } else {
                            this.aZ = 1;
                            break;
                        }
                    }
                    break;
                case 11:
                    if (ab != 0) {
                        ab = 0;
                        break;
                    } else {
                        ab = 1;
                        break;
                    }
                case 16:
                    if (!this.k) {
                        if (this.aX <= 0 && X != null && (((dx + this.aW) - this.aV) << 3) + dw < X.length - 1 && dw < 7) {
                            dw++;
                            break;
                        }
                    } else if (this.aZ != 0) {
                        this.aZ = 0;
                        break;
                    } else {
                        this.aZ = 1;
                        break;
                    }
                case 17:
                    if (!this.k) {
                        if (this.aX <= 0 && Z != null && (((dx + this.aW) - this.aV) << 3) + dw < Z.length - 1 && dw < 7) {
                            dw++;
                            break;
                        }
                    } else if (this.aZ != 0) {
                        this.aZ = 0;
                        break;
                    } else {
                        this.aZ = 1;
                        break;
                    }
                case 18:
                    if (!this.k) {
                        if (this.aX <= 0) {
                            if (!this.aQ.bY) {
                                if (y.cP != null && (((dx + this.aW) - this.aV) << 3) + dw < y.cP.length - 1 && dw < 7) {
                                    dw++;
                                    break;
                                }
                            } else if (e != 6) {
                                if ((((dx + this.aW) - this.aV) << 3) + dw < ar.length - 1 && dw < 7) {
                                    dw++;
                                    break;
                                }
                            } else if (y.cP != null && (((dx + this.aW) - this.aV) << 3) + dw < y.cP.length - 1 && dw < 7) {
                                dw++;
                                break;
                            }
                        }
                    } else if (this.aZ != 0) {
                        this.aZ = 0;
                        break;
                    } else {
                        this.aZ = 1;
                        break;
                    }
                    break;
                case 19:
                    if (cn != null && (((dx + this.aW) - this.aV) << 3) + dw < cn.length - 1 && dw < 7) {
                        dw++;
                        break;
                    }
            }
            this.dr = 1;
            if (ds < b - 1) {
                ds++;
            } else {
                ds = 0;
            }
        } else if (Y.h(16384) || Y.h(16)) {
            switch (a) {
                case 2:
                    if (this.k) {
                        if (this.aZ != 0) {
                            this.aZ = 0;
                            break;
                        } else {
                            this.aZ = 1;
                            break;
                        }
                    }
                    break;
                case 11:
                    if (ab != 0) {
                        ab = 0;
                        break;
                    } else {
                        ab = 1;
                        break;
                    }
                case 16:
                    if (!this.k) {
                        if (this.aX <= 0 && X != null && dw > 0) {
                            dw--;
                            break;
                        }
                    } else if (this.aZ != 0) {
                        this.aZ = 0;
                        break;
                    } else {
                        this.aZ = 1;
                        break;
                    }
                case 17:
                    if (!this.k) {
                        if (this.aX <= 0 && Z != null && dw > 0) {
                            dw--;
                            break;
                        }
                    } else if (this.aZ != 0) {
                        this.aZ = 0;
                        break;
                    } else {
                        this.aZ = 1;
                        break;
                    }
                case 18:
                    if (!this.k) {
                        if (this.aX <= 0 && y.cP != null && dw > 0) {
                            dw--;
                            break;
                        }
                    } else if (this.aZ != 0) {
                        this.aZ = 0;
                        break;
                    } else {
                        this.aZ = 1;
                        break;
                    }
                case 19:
                    if (cn != null && dw > 0) {
                        dw--;
                        break;
                    }
            }
            this.dr = 1;
            if (ds > 0) {
                ds--;
            } else if (ds == 0 && b != 0) {
                ds = b - 1;
            }
        }
        l.b();
    }

    public final void q() {
        int i2;
        int i3;
        if (!this.k && this.aX > 0) {
            if (this.aY < 20) {
                this.aY++;
            } else {
                this.aY = 0;
                this.aX = 0;
            }
        }
        switch (a) {
            case 0:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case c.INT_16 /*20*/:
            case 21:
            default:
                return;
            case 1:
                switch (ac) {
                    case 1:
                    case 2:
                    case 3:
                    default:
                        return;
                    case 4:
                        if (y.bF[(dt + this.ae) - this.ad].X != null) {
                            ak = new String[y.bF[(dt + this.ae) - this.ad].X.length];
                            al = new String[y.bF[(dt + this.ae) - this.ad].X.length];
                            for (int i4 = 0; i4 < ak.length; i4++) {
                                ak[i4] = y.ak[y.bF[(dt + this.ae) - this.ad].X[i4]];
                                al[i4] = y.al[y.bF[(dt + this.ae) - this.ad].X[i4]];
                            }
                            String[] strArr = ak;
                            if (strArr.length >= this.bc) {
                                this.bd = this.bc;
                            } else {
                                this.bd = strArr.length;
                                this.bc = strArr.length;
                            }
                        }
                        a = 4;
                        b = 0;
                        ac = 0;
                        return;
                }
            case 2:
                switch (ac) {
                    case 1:
                        switch (this.aZ) {
                            case 0:
                                i3 = 0;
                                break;
                            case 1:
                                i3 = y.aR[(dt + this.ae) - this.ad][this.ba];
                                y.aR[(dt + this.ae) - this.ad][this.ba] = -1;
                                break;
                            default:
                                i3 = 0;
                                break;
                        }
                        if (y.cE != null) {
                            e();
                            if (X != null) {
                                cn = X;
                                int length = cn.length;
                                switch (y.bF[(dt + this.ae) - this.ad].ac) {
                                    case 0:
                                        int i5 = 0;
                                        while (i5 < length) {
                                            if (y.aq[y.cE[cn[i5]][0]] != 0) {
                                                cn = ah.c(cn, i5);
                                                length--;
                                            } else {
                                                i5++;
                                            }
                                        }
                                        break;
                                    case 1:
                                        int i6 = 0;
                                        while (i6 < length) {
                                            if (y.aq[y.cE[cn[i6]][0]] != 1) {
                                                cn = ah.c(cn, i6);
                                                length--;
                                            } else {
                                                i6++;
                                            }
                                        }
                                        break;
                                    case 2:
                                        int i7 = 0;
                                        while (i7 < length) {
                                            if (y.aq[y.cE[cn[i7]][0]] != 2) {
                                                cn = ah.c(cn, i7);
                                                length--;
                                            } else {
                                                i7++;
                                            }
                                        }
                                        break;
                                    case 3:
                                        int i8 = 0;
                                        while (i8 < length) {
                                            if (y.aq[y.cE[cn[i8]][0]] != 3) {
                                                cn = ah.c(cn, i8);
                                                length--;
                                            } else {
                                                i8++;
                                            }
                                        }
                                        break;
                                    case 4:
                                        int i9 = 0;
                                        while (i9 < length) {
                                            if (y.aq[y.cE[cn[i9]][0]] != 4) {
                                                cn = ah.c(cn, i9);
                                                length--;
                                            } else {
                                                i9++;
                                            }
                                        }
                                        break;
                                    case 5:
                                        int i10 = 0;
                                        while (i10 < length) {
                                            if (y.aq[y.cE[cn[i10]][0]] != 1) {
                                                cn = ah.c(cn, i10);
                                                length--;
                                            } else {
                                                i10++;
                                            }
                                        }
                                        break;
                                }
                                if (cn != null) {
                                    a(e(cn.length, 8));
                                    switch (this.aZ) {
                                        case 1:
                                            int[] iArr = y.cE[i3];
                                            y.cE[i3] = y.cE[cn[0]];
                                            y.cE[cn[0]] = iArr;
                                            this.aQ.E((dt + this.ae) - this.ad);
                                            X = null;
                                            cn = null;
                                            break;
                                    }
                                }
                            }
                        }
                        switch (this.aZ) {
                            case 0:
                                dw = 0;
                                dx = 0;
                                a = 19;
                                b = 0;
                                break;
                        }
                        ac = 0;
                        this.k = false;
                        this.aZ = 0;
                        return;
                    case 2:
                        switch (this.aZ) {
                            case 0:
                                i2 = 0;
                                break;
                            case 1:
                                i2 = y.aR[(dt + this.ae) - this.ad][this.ba];
                                y.aR[(dt + this.ae) - this.ad][this.ba] = -1;
                                break;
                            default:
                                i2 = 0;
                                break;
                        }
                        if (y.cG != null) {
                            J();
                            if (Z != null) {
                                cn = Z;
                                int length2 = cn.length;
                                switch (this.ba) {
                                    case 0:
                                        int i11 = 0;
                                        while (i11 < length2) {
                                            if (y.cu[y.cG[cn[i11]][0]] != 1) {
                                                cn = ah.c(cn, i11);
                                                length2--;
                                            } else {
                                                i11++;
                                            }
                                        }
                                        break;
                                    case 1:
                                        int i12 = 0;
                                        while (i12 < length2) {
                                            if (y.cu[y.cG[cn[i12]][0]] != 5) {
                                                cn = ah.c(cn, i12);
                                                length2--;
                                            } else {
                                                i12++;
                                            }
                                        }
                                        break;
                                    case 2:
                                        int i13 = 0;
                                        while (i13 < length2) {
                                            if (y.cu[y.cG[cn[i13]][0]] != 2) {
                                                cn = ah.c(cn, i13);
                                                length2--;
                                            } else {
                                                i13++;
                                            }
                                        }
                                        break;
                                    case 3:
                                        int i14 = 0;
                                        while (i14 < length2) {
                                            if (y.cu[y.cG[cn[i14]][0]] != 4) {
                                                cn = ah.c(cn, i14);
                                                length2--;
                                            } else {
                                                i14++;
                                            }
                                        }
                                        break;
                                    case 5:
                                        int i15 = 0;
                                        while (i15 < length2) {
                                            if (y.cu[y.cG[cn[i15]][0]] != 7) {
                                                cn = ah.c(cn, i15);
                                                length2--;
                                            } else {
                                                i15++;
                                            }
                                        }
                                        break;
                                    case 6:
                                        int i16 = 0;
                                        while (i16 < length2) {
                                            if (y.cu[y.cG[cn[i16]][0]] != 6) {
                                                cn = ah.c(cn, i16);
                                                length2--;
                                            } else {
                                                i16++;
                                            }
                                        }
                                        break;
                                    case 7:
                                        int i17 = 0;
                                        while (i17 < length2) {
                                            if (y.cu[y.cG[cn[i17]][0]] != 4) {
                                                cn = ah.c(cn, i17);
                                                length2--;
                                            } else {
                                                i17++;
                                            }
                                        }
                                        break;
                                    case 8:
                                        int i18 = 0;
                                        while (i18 < length2) {
                                            if (y.cu[y.cG[cn[i18]][0]] != 3) {
                                                cn = ah.c(cn, i18);
                                                length2--;
                                            } else {
                                                i18++;
                                            }
                                        }
                                        break;
                                }
                                if (cn != null) {
                                    a(e(cn.length, 8));
                                    switch (this.aZ) {
                                        case 1:
                                            int[] iArr2 = y.cG[i2];
                                            y.cG[i2] = y.cG[cn[0]];
                                            y.cG[cn[0]] = iArr2;
                                            this.aQ.E((dt + this.ae) - this.ad);
                                            Z = null;
                                            cn = null;
                                            break;
                                    }
                                }
                            }
                        }
                        switch (this.aZ) {
                            case 0:
                                dw = 0;
                                dx = 0;
                                a = 19;
                                b = 0;
                                break;
                        }
                        ac = 0;
                        this.k = false;
                        this.aZ = 0;
                        return;
                    default:
                        return;
                }
            case 3:
                switch (ac) {
                    case 1:
                        if (y.cE != null) {
                            e();
                            if (X != null) {
                                a(e(X.length, 8));
                            }
                        }
                        dw = 0;
                        dx = 0;
                        a = 16;
                        b = 0;
                        ac = 0;
                        return;
                    case 2:
                        if (y.cG != null) {
                            J();
                            if (Z != null) {
                                a(e(Z.length, 8));
                            }
                        }
                        dw = 0;
                        dx = 0;
                        a = 17;
                        b = 0;
                        ac = 0;
                        return;
                    case 3:
                        if (y.cP != null) {
                            a(e(y.cP.length, 8));
                        }
                        dw = 0;
                        dx = 0;
                        a = 18;
                        b = 1;
                        aq[0] = 11;
                        ac = 0;
                        return;
                    default:
                        return;
                }
            case 10:
                if (!this.am) {
                    return;
                }
                if (this.dN < 5) {
                    this.dN++;
                    return;
                }
                this.am = false;
                this.dN = 0;
                y.I().P();
                this.aX = 6;
                return;
            case 19:
                switch (ac) {
                    case 1:
                        if (y.aR[(dt + this.ae) - this.ad][this.ba] == -1) {
                            y.aR[(dt + this.ae) - this.ad][this.ba] = cn[(((dx + this.aW) - this.aV) << 3) + dw];
                        } else {
                            int i19 = y.aR[(dt + this.ae) - this.ad][this.ba];
                            if (this.ba == 4) {
                                int[] iArr3 = y.cE[i19];
                                y.cE[i19] = y.cE[cn[(((dx + this.aW) - this.aV) << 3) + dw]];
                                y.cE[cn[(((dx + this.aW) - this.aV) << 3) + dw]] = iArr3;
                            } else {
                                int[] iArr4 = y.cG[i19];
                                y.cG[i19] = y.cG[cn[(((dx + this.aW) - this.aV) << 3) + dw]];
                                y.cG[cn[(((dx + this.aW) - this.aV) << 3) + dw]] = iArr4;
                            }
                        }
                        this.aQ.E((dt + this.ae) - this.ad);
                        a = 2;
                        b = 1;
                        aq[0] = 11;
                        X = null;
                        Z = null;
                        cn = null;
                        ac = 0;
                        return;
                    default:
                        return;
                }
        }
    }

    public final void v() {
        l.a(100, 70);
        a = 0;
        b = 4;
        b = 3;
        aq[0] = 0;
        aq[1] = 4;
        aq[2] = 5;
        i = y.bF.length;
        this.ad = 3;
        this.ae = this.ad;
        if (i >= this.ad) {
            this.ae = this.ad;
        } else {
            this.ae = i;
            this.ad = i;
        }
        this.cZ = new k();
        if (y.cQ != null) {
            cl = new String[y.cQ.length];
            ct = new String[y.cQ.length];
            for (int i2 = 0; i2 < y.cQ.length; i2++) {
                cl[i2] = y.cQ[i2][0];
                ct[i2] = y.cQ[i2][1];
            }
        } else {
            cl = null;
            ct = null;
        }
        ds = 0;
        dt = 0;
        dw = 0;
        dx = 0;
        this.aQ.R();
        if (y.cc) {
            ab = 0;
        } else {
            ab = 1;
        }
        String ao = ah.ao("/txt/about.txt");
        cS = ah.b(ah.d(ao, "help:", "helpEnd"), "\r\n", f.OBEX_HTTP_ACCEPTED);
        cO = ah.b(ah.d(ao, "aboutUs:", "aboutUsEnd"), "\r\n", f.OBEX_HTTP_ACCEPTED);
    }
}
