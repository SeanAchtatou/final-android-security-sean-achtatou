package defpackage;

import com.a.a.l.f;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Hashtable;
import java.util.Vector;

/* renamed from: am  reason: default package */
public final class am {
    private static final am br = new am();
    private Vector au = new Vector(2);
    private Hashtable cY = new Hashtable();
    private String o;

    private am() {
    }

    public static am kA() {
        return br;
    }

    public final DataOutputStream a(String str, boolean z) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.au.indexOf(str);
        this.cY.put(str, byteArrayOutputStream);
        return new DataOutputStream(byteArrayOutputStream);
    }

    public final void a(String str) {
        this.o = str;
        try {
            f d = f.d(str, false);
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(d.bk(1)));
            for (int readUnsignedByte = dataInputStream.readUnsignedByte(); readUnsignedByte > 0; readUnsignedByte--) {
                this.au.addElement(dataInputStream.readUTF());
            }
            dataInputStream.close();
            d.en();
        } catch (Exception e) {
        }
    }

    public final DataInputStream o(String str) {
        if (!this.au.contains(str)) {
            throw new Exception(new StringBuffer().append(str).append(" 记录不存在").toString());
        }
        f d = f.d(this.o, false);
        byte[] bk = d.bk(this.au.indexOf(str) + 2);
        d.en();
        return new DataInputStream(new ByteArrayInputStream(bk));
    }

    public final void p(String str) {
        if (this.cY.containsKey(str)) {
            ByteArrayOutputStream byteArrayOutputStream = (ByteArrayOutputStream) this.cY.get(str);
            this.cY.remove(str);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            f d = f.d(this.o, true);
            int indexOf = this.au.indexOf(str) + 2;
            if (indexOf < 2) {
                this.au.addElement(str);
                byteArrayOutputStream.reset();
                DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
                dataOutputStream.writeByte(this.au.size());
                for (int i = 0; i < this.au.size(); i++) {
                    dataOutputStream.writeUTF((String) this.au.elementAt(i));
                }
                dataOutputStream.close();
                byte[] byteArray2 = byteArrayOutputStream.toByteArray();
                if (d.eo() <= 0) {
                    d.f(byteArray2, 0, byteArray2.length);
                } else {
                    d.a(1, byteArray2, 0, byteArray2.length);
                }
                d.f(byteArray, 0, byteArray.length);
            } else {
                d.a(indexOf, byteArray, 0, byteArray.length);
            }
            d.en();
        }
    }
}
