package defpackage;

import com.a.a.e.l;
import com.a.a.e.o;
import java.util.Vector;

/* renamed from: au  reason: default package */
public final class au {
    private int a = 10;
    private l aP = l.k(0, 0, 8);
    private int aa;
    private Vector au = new Vector();
    private int b;
    private int e = 10;
    private int h;
    private int i = 16777215;
    private int p;

    public au(int i2, int i3, int i4, int i5) {
        this.b = i4;
        this.h = i5;
        this.aP = this.aP;
    }

    public final void a() {
        if (this.p < this.au.size()) {
            this.aa++;
            this.p++;
        }
    }

    public final synchronized void a(String str) {
        int i2;
        boolean z;
        if (this.au != null) {
            this.au.removeAllElements();
        }
        this.au = null;
        int i3 = this.b - this.e;
        l lVar = this.aP;
        Vector vector = new Vector();
        char[] charArray = str.toCharArray();
        StringBuffer stringBuffer = new StringBuffer(10);
        int length = charArray.length;
        int i4 = 0;
        boolean z2 = false;
        while (i4 < length) {
            char c = charArray[i4];
            if (c == 10 || c == 'n') {
                i2 = i4;
                z = true;
            } else {
                stringBuffer.append(c);
                if (lVar.A(stringBuffer.toString()) >= i3) {
                    stringBuffer.deleteCharAt(stringBuffer.length() - 1);
                    i4--;
                    z2 = true;
                }
                if (i4 == length - 1) {
                    i2 = i4;
                    z = true;
                } else {
                    int i5 = i4;
                    z = z2;
                    i2 = i5;
                }
            }
            if (z) {
                vector.addElement(stringBuffer.toString());
                stringBuffer = new StringBuffer(10);
                z = false;
            }
            int i6 = i2 + 1;
            z2 = z;
            i4 = i6;
        }
        if (vector.size() * this.aP.getHeight() >= this.h) {
            this.aa = 0;
            this.p = this.h / this.aP.getHeight();
        }
        this.au = vector;
    }

    public final void b() {
        if (this.aa > 0) {
            this.aa--;
            this.p--;
        }
    }

    public final void b(o oVar) {
        String[] strArr;
        oVar.setColor(this.i);
        oVar.a(this.aP);
        synchronized (this) {
            strArr = new String[this.au.size()];
            for (int i2 = 0; i2 < this.au.size(); i2++) {
                strArr[i2] = (String) this.au.elementAt(i2);
            }
        }
        if (strArr.length * this.aP.getHeight() >= this.h) {
            for (int i3 = this.aa; i3 < this.p; i3++) {
                oVar.a(strArr[i3], (this.e + this.b) >> 1, this.a + ((i3 - this.aa) * this.aP.getHeight()), 17);
            }
            int i4 = this.b >> 1;
            oVar.setColor(16777215);
            if (this.aa > 0) {
                oVar.f(i4, 1, i4 - 6, 7, i4 + 6, 7);
            }
            if (this.p != strArr.length) {
                oVar.f(i4 - 6, this.h + 1, i4 + 6, this.h + 1, i4, this.h + 7);
            }
        } else {
            for (int i5 = 0; i5 < strArr.length; i5++) {
                oVar.a(strArr[i5], (this.e + this.b) >> 1, this.a + ((this.h - (strArr.length * this.aP.getHeight())) / 2) + (this.aP.getHeight() * i5), 17);
            }
        }
        oVar.setClip(0, 0, 240, 320);
    }

    public final void e(l lVar) {
        this.aP = lVar;
    }
}
