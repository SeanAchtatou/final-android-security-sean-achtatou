package defpackage;

import com.a.a.e.p;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import javax.microedition.media.control.ToneControl;

/* renamed from: at  reason: default package */
final class at {
    byte[] aB;
    short[] aD;
    private String[] ak;
    byte[] at;
    p[] aw;
    short[] cT;
    byte[] cU;
    public boolean k;
    private String o;
    short[] vp;
    short[] vq;
    short[] vr;
    short[] vs;
    short[] vt;
    private z vu;

    private at() {
    }

    private static void a(at atVar) {
        int U = atVar.vu.U() * 6;
        boolean z = atVar.vu.T() == 1;
        atVar.aD = new short[U];
        if (U / 6 < 255) {
            atVar.k = true;
        } else {
            atVar.k = false;
        }
        for (short s = 0; s < U; s = (short) (s + 1)) {
            if (s % 6 != 1 && s % 6 != 2) {
                atVar.aD[s] = (short) (atVar.vu.T() & ToneControl.SILENCE);
            } else if (z) {
                atVar.aD[s] = atVar.vu.U();
            } else {
                atVar.aD[s] = (short) (atVar.vu.T() & ToneControl.SILENCE);
            }
        }
    }

    private static void b(at atVar) {
        int U = atVar.vu.U();
        atVar.at = new byte[U];
        atVar.cT = new short[U];
        atVar.aB = new byte[U];
        atVar.vq = new short[U];
        for (int i = 0; i < U; i++) {
            atVar.cT[i] = atVar.vu.U();
            atVar.at[i] = atVar.vu.T();
            atVar.vq[i] = atVar.vu.U();
            atVar.aB[i] = atVar.vu.T();
        }
        atVar.vp = new short[(atVar.vu.U() * 3)];
        for (int i2 = 0; i2 < atVar.vp.length; i2 += 3) {
            atVar.vp[i2] = atVar.vu.U();
            atVar.vp[i2 + 1] = atVar.vu.U();
            if (atVar.k) {
                atVar.vp[i2 + 2] = (short) atVar.vu.T();
            } else {
                atVar.vp[i2 + 2] = atVar.vu.U();
            }
        }
        atVar.vr = new short[(atVar.vu.U() * 5)];
        for (int i3 = 0; i3 < atVar.vr.length; i3 += 5) {
            atVar.vr[i3] = atVar.vu.U();
            atVar.vr[i3 + 1] = atVar.vu.U();
            atVar.vr[i3 + 2] = (short) (atVar.vu.T() & ToneControl.SILENCE);
            atVar.vr[i3 + 3] = (short) (atVar.vu.T() & ToneControl.SILENCE);
            atVar.vr[i3 + 4] = (short) atVar.vu.T();
        }
    }

    private static void c(at atVar) {
        int U = atVar.vu.U();
        atVar.vt = new short[U];
        atVar.vs = new short[U];
        for (int i = 0; i < U; i++) {
            atVar.vt[i] = atVar.vu.U();
            atVar.vs[i] = atVar.vu.U();
        }
        int U2 = atVar.vu.U();
        atVar.cU = new byte[U2];
        for (int i2 = 0; i2 < U2; i2++) {
            atVar.cU[i2] = atVar.vu.T();
        }
    }

    static at m(String str, String str2) {
        at atVar = new at();
        atVar.o = str2;
        atVar.vu = new z(str);
        byte T = atVar.vu.T();
        for (byte b = 0; b < T; b = (byte) (b + 1)) {
            byte T2 = atVar.vu.T();
            short U = atVar.vu.U();
            switch (T2) {
                case -96:
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(atVar.vu.at, atVar.vu.e, U);
                    DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream);
                    try {
                        int readByte = dataInputStream.readByte();
                        atVar.ak = new String[readByte];
                        atVar.aw = new p[readByte];
                        for (byte b2 = 0; b2 < readByte; b2 = (byte) (b2 + 1)) {
                            atVar.ak[b2] = dataInputStream.readUTF();
                        }
                        for (int i = 0; i < readByte; i++) {
                            atVar.aw[i] = ah.n(new StringBuffer().append(atVar.o).append(atVar.ak[i]).toString());
                        }
                        byteArrayInputStream.close();
                        dataInputStream.close();
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                        break;
                    }
                case -95:
                    a(atVar);
                    break;
                case -94:
                    b(atVar);
                    break;
                case -93:
                    c(atVar);
                    break;
                default:
                    atVar.vu.a((short) U);
                    break;
            }
        }
        atVar.vu.b();
        atVar.vu = null;
        return atVar;
    }
}
