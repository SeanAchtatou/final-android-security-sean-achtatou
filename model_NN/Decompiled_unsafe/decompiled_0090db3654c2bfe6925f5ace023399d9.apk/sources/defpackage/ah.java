package defpackage;

import com.a.a.e.l;
import com.a.a.e.o;
import com.a.a.e.p;
import com.a.a.p.f;
import com.umeng.common.b.e;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Random;
import java.util.Vector;
import javax.microedition.enhance.MIDPHelper;
import me.gall.sgp.sdk.service.BossService;

/* renamed from: ah  reason: default package */
public final class ah {
    public static l aP;
    public static int e;

    static {
        l k = l.k(0, 0, 8);
        aP = k;
        e = k.getHeight();
        new Random(System.currentTimeMillis());
    }

    public static byte O(int i, int i2) {
        if (i == 0 && i2 == 0) {
            return 0;
        }
        if (i == 1 && i2 == 0) {
            return 2;
        }
        if (i == 0 && i2 == 1) {
            return 1;
        }
        return (i == 1 && i2 == 1) ? (byte) 3 : 0;
    }

    public static void a(o oVar, String str, int i, int i2, int i3, int i4, int i5) {
        oVar.setColor(16777215);
        oVar.a(str, 119, 159, 17);
        oVar.a(str, 119, (int) f.OBEX_HTTP_CREATED, 17);
        oVar.a(str, 121, 159, 17);
        oVar.a(str, 121, (int) f.OBEX_HTTP_CREATED, 17);
        oVar.setColor(16711680);
        oVar.a(str, 120, (int) f.OBEX_HTTP_OK, 17);
    }

    public static boolean a(int i, int i2, int i3, int i4, int i5, int i6) {
        return c(i, i2, 0, 0, i3, i4, i5, i6);
    }

    public static String[] a(String[] strArr, int i) {
        if (strArr == null || strArr.length == 1) {
            return null;
        }
        while (i < strArr.length - 1) {
            strArr[i] = strArr[i + 1];
            i++;
        }
        String[] strArr2 = new String[(strArr.length - 1)];
        System.arraycopy(strArr, 0, strArr2, 0, strArr.length - 1);
        return strArr2;
    }

    public static int[][] a(int[][] iArr, int i) {
        if (iArr == null || iArr.length == 1) {
            return null;
        }
        while (i < iArr.length - 1) {
            iArr[i] = iArr[i + 1];
            i++;
        }
        int[][] iArr2 = new int[(iArr.length - 1)][];
        System.arraycopy(iArr, 0, iArr2, 0, iArr.length - 1);
        return iArr2;
    }

    public static String[][] a(String[][] strArr, int i) {
        if (strArr == null || strArr.length == 1) {
            return null;
        }
        while (i < strArr.length - 1) {
            strArr[i] = strArr[i + 1];
            i++;
        }
        String[][] strArr2 = new String[(strArr.length - 1)][];
        System.arraycopy(strArr, 0, strArr2, 0, strArr.length - 1);
        return strArr2;
    }

    public static String ao(String str) {
        String str2;
        System.out.println(new StringBuffer().append("读取").append(str).toString());
        InputStream b = MIDPHelper.b(new ah().getClass(), str);
        if (b == null) {
            System.out.println(new StringBuffer().append("文件").append(str).append("不存在").toString());
            return null;
        }
        byte[] bArr = new byte[256];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(256);
        while (true) {
            try {
                int read = b.read(bArr);
                if (read == -1) {
                    break;
                }
                if (str.toLowerCase().endsWith(".cfg")) {
                    for (int i = 7; i < read; i += 8) {
                        bArr[i] = (byte) (bArr[i] + 5);
                    }
                }
                byteArrayOutputStream.write(bArr, 0, read);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            b.close();
            byteArrayOutputStream.close();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        try {
            str2 = new String(byteArray, e.f);
        } catch (UnsupportedEncodingException e4) {
            e4.printStackTrace();
            str2 = null;
        }
        System.out.println(new StringBuffer().append("读取文件").append(str).append("完成").toString());
        return str2;
    }

    public static int[] b(int[] iArr, int i) {
        if (iArr == null) {
            return new int[]{i};
        }
        int[] iArr2 = new int[(iArr.length + 1)];
        System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
        iArr2[iArr2.length - 1] = i;
        return iArr2;
    }

    public static String[] b(String str, String str2, int i) {
        String[] n = n(str, str2);
        Vector vector = new Vector();
        int i2 = 0;
        while (n != null && i2 < n.length) {
            String[] f = f(n[i2], i);
            int i3 = 0;
            while (f != null && i3 < f.length) {
                vector.addElement(f[i3]);
                i3++;
            }
            i2++;
        }
        if (vector.size() <= 0) {
            return null;
        }
        String[] strArr = new String[vector.size()];
        vector.copyInto(strArr);
        return strArr;
    }

    public static String c(String str, String str2) {
        if (str == null || str2 == null || str.equals("") || str2.equals("")) {
            return null;
        }
        String[] n = n(str, "\r\n");
        int i = 0;
        while (n != null && i < n.length) {
            if (n[i] != null && !n[i].equals("") && n[i].startsWith(new StringBuffer().append(str2).append("=").toString())) {
                return n[i].substring(new StringBuffer().append(str2).append("=").toString().length());
            }
            i++;
        }
        return null;
    }

    public static final void c(o oVar, p pVar, int i, int i2, int i3) {
        if (pVar != null) {
            oVar.a(pVar, i, i2, i3);
        }
    }

    public static boolean c(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        return i5 < i + i3 && i6 < i2 + i4 && i5 + i7 > i && i6 + i8 > i2;
    }

    public static int[] c(int[] iArr, int i) {
        if (iArr == null || iArr.length == 1) {
            return null;
        }
        while (i < iArr.length - 1) {
            iArr[i] = iArr[i + 1];
            i++;
        }
        int[] iArr2 = new int[(iArr.length - 1)];
        System.arraycopy(iArr, 0, iArr2, 0, iArr.length - 1);
        return iArr2;
    }

    public static String d(String str, String str2, String str3) {
        if (str == null || str.equals("")) {
            return null;
        }
        int indexOf = str2 != null ? str.indexOf(str2) : 0;
        if (indexOf < 0) {
            return null;
        }
        if (str3 != null) {
            int indexOf2 = str.indexOf(str3, indexOf);
            if (indexOf2 >= 0) {
                return str2 != null ? str.substring(str2.length() + indexOf, indexOf2) : str.substring(0, indexOf2);
            }
            return null;
        }
        if (str2 != null) {
            str = str.substring(str2.length() + indexOf);
        }
        return str;
    }

    public static String[] e(String str, String str2, String str3) {
        String d = d(str, str2, str3);
        if (d == null) {
            return null;
        }
        return n(d, "\r\n");
    }

    public static String[] f(String str, int i) {
        String[] strArr;
        int i2;
        String[] strArr2;
        String[] strArr3;
        String[] strArr4 = null;
        if (i <= 0) {
            return null;
        }
        if (str == null || str.equals("")) {
            return new String[]{""};
        }
        StringBuffer stringBuffer = new StringBuffer("");
        int i3 = 0;
        while (i3 < str.length()) {
            stringBuffer.append(str.charAt(i3));
            if (aP.A(stringBuffer.toString()) > i) {
                stringBuffer.deleteCharAt(stringBuffer.length() - 1);
                if (strArr4 == null) {
                    strArr3 = new String[1];
                } else {
                    strArr3 = new String[(strArr4.length + 1)];
                    System.arraycopy(strArr4, 0, strArr3, 0, strArr4.length);
                }
                strArr3[strArr3.length - 1] = stringBuffer.toString();
                stringBuffer.delete(0, stringBuffer.length());
                int i4 = i3 - 1;
                strArr2 = strArr3;
                i2 = i4;
            } else {
                i2 = i3;
                strArr2 = strArr4;
            }
            strArr4 = strArr2;
            i3 = i2 + 1;
        }
        if (stringBuffer.length() > 0) {
            if (strArr4 == null) {
                strArr = new String[1];
            } else {
                strArr = new String[(strArr4.length + 1)];
                System.arraycopy(strArr4, 0, strArr, 0, strArr4.length);
            }
            strArr[strArr.length - 1] = stringBuffer.toString();
        } else {
            strArr = strArr4;
        }
        return strArr;
    }

    public static p n(String str) {
        if (str != null) {
            try {
                if (!str.equals("")) {
                    String stringBuffer = !str.startsWith("/") ? new StringBuffer().append("/").append(str).toString() : str;
                    if (str.toLowerCase().endsWith(".png")) {
                        return p.C(stringBuffer);
                    }
                    if (str.toLowerCase().endsWith(".ps")) {
                        z zVar = new z(stringBuffer);
                        p V = zVar.V();
                        zVar.b();
                        return V;
                    }
                    l.t();
                    new StringBuffer().append("不支持读取的文件格式").append(str).toString();
                    return null;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }
        return null;
    }

    private static String[] n(String str, String str2) {
        String[] strArr;
        String[] strArr2;
        String[] strArr3 = null;
        if (str == null || str2 == null) {
            return null;
        }
        int i = 0;
        while (i < str.length()) {
            int indexOf = str.indexOf(str2, i);
            if (indexOf != -1) {
                if (!str.substring(i, indexOf).equals("")) {
                    if (strArr3 == null) {
                        strArr2 = new String[1];
                    } else {
                        strArr2 = new String[(strArr3.length + 1)];
                        System.arraycopy(strArr3, 0, strArr2, 0, strArr3.length);
                    }
                    strArr2[strArr2.length - 1] = str.substring(i, indexOf);
                } else {
                    strArr2 = strArr3;
                }
                i = str2.length() + indexOf;
                strArr3 = strArr2;
            } else if (str.substring(i).equals("")) {
                return strArr3;
            } else {
                if (strArr3 == null) {
                    strArr = new String[1];
                } else {
                    strArr = new String[(strArr3.length + 1)];
                    System.arraycopy(strArr3, 0, strArr, 0, strArr3.length);
                }
                strArr[strArr.length - 1] = str.substring(i);
                return strArr;
            }
        }
        return strArr3;
    }

    public static int[] o(String str, String str2) {
        String[] n;
        String c = c(str, str2);
        if (c == null || c.equals("") || (n = n(c, BossService.ID_SEPARATOR)) == null) {
            return null;
        }
        int[] iArr = new int[n.length];
        for (int i = 0; i < n.length; i++) {
            iArr[i] = Integer.parseInt(n[i]);
        }
        return iArr;
    }

    public static String[] p(String str, String str2) {
        return n(c(str, str2), BossService.ID_SEPARATOR);
    }
}
