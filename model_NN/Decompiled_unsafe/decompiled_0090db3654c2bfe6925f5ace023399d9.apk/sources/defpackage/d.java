package defpackage;

import com.a.a.e.o;
import com.a.a.e.p;
import com.a.a.e.z;

/* renamed from: d  reason: default package */
public final class d {
    private int[] X;
    private int a = 10;
    private int aa = 0;
    private int ab;
    private int ac = 25;
    private p ah;
    private StringBuffer ai;
    private byte aj;
    private String[] ak;
    private String[] al;
    private boolean am;
    private boolean an;
    private boolean ao;
    private ak ap;
    private int b = 7;
    private int e;
    private int h;
    private int i;
    private boolean k;
    private String o = "";
    private int p;

    public d(ak akVar) {
        this.ap = akVar;
    }

    private void d(byte b2) {
        this.i = ah.e;
        if (this.i < 20) {
            this.i = 20;
        }
        this.h = (b2 + 1) * this.i;
    }

    private void o() {
        this.o = null;
        this.ah = null;
        this.ai = null;
        this.k = false;
        this.aa = 0;
        this.ao = false;
        this.p = 0;
        System.gc();
    }

    public final void a() {
        if (!this.k || !this.ao) {
            this.ap.bh.f(0);
            if (this.am) {
            }
            return;
        }
        this.ac--;
        if (this.ac == 0) {
            this.an = false;
            if (this.e <= 1) {
                o();
                System.out.println("5555555555555555555555555");
            } else {
                this.aa++;
            }
            this.e--;
            this.ac = 25;
        }
    }

    public final void a(int i2, String str, boolean z) {
        this.ai = new StringBuffer(str.length());
        this.aj = 2;
        if (i2 != -1) {
            r c = new f("/data/face.bin").c(i2);
            this.o = c.i(0);
            System.out.println(new StringBuffer().append("chatName:    ").append(this.o).toString());
            this.ao = z;
            try {
                this.ah = ah.n(new StringBuffer().append(y.o).append(c.i(1)).toString());
            } catch (Exception e2) {
                e2.printStackTrace();
                l.t();
            }
        }
        this.ai.append(str);
        if (this.ai != null) {
            this.ak = ah.f(this.ai.toString(), ((240 - (this.a << 1)) - ah.aP.A("哈")) - 10);
        }
        d(this.aj);
        int length = this.ak.length;
        byte b2 = this.aj;
        this.e = length % b2 == 0 ? (length - (length % b2)) / b2 : ((length - (length % b2)) / b2) + 1;
        this.k = true;
    }

    public final void a(String[] strArr, int i2, int i3, int[] iArr) {
        this.al = strArr;
        this.aj = (byte) i2;
        this.ab = i3;
        this.X = iArr;
        d(this.aj);
        this.am = true;
    }

    public final void b() {
        if (this.k) {
            this.an = false;
            if (l.t().h(65536) || l.t().h(32)) {
                if (this.e <= 1) {
                    o();
                } else {
                    this.aa++;
                }
                this.e--;
            }
            l.t();
            l.b();
        } else if (this.am) {
            this.an = false;
            if (l.t().h(65536) || l.t().h(32)) {
                this.an = true;
                this.ab = this.X[this.p];
                this.am = false;
            }
            if (l.t().h(4096) || l.t().h(4)) {
                if (this.p > 0) {
                    this.p--;
                } else {
                    this.p = this.aj - 1;
                }
            } else if (l.t().h(8192) || l.t().h(256)) {
                if (this.p < this.aj - 1) {
                    this.p++;
                } else {
                    this.p = 0;
                }
            }
            l.t();
            l.b();
        }
    }

    public final void b(o oVar) {
        if (this.k || this.am) {
            int i2 = (320 - this.h) - 18;
            ae.a(oVar, 0, i2, 240, this.h + 18, 0, 12169636, 0, 12331540, aa.dK, 1, 1, 1, 1, 2, 1);
            if (this.k) {
                if (this.ah != null) {
                    ah.c(oVar, this.ah, this.a, this.i + i2 + 5, 36);
                    oVar.setColor(z.CONSTRAINT_MASK);
                    oVar.a(new StringBuffer().append(this.o).append(":").toString(), this.ah.getWidth() + (this.a * 2), this.b + i2, 20);
                } else {
                    oVar.setColor(z.CONSTRAINT_MASK);
                    oVar.a(new StringBuffer().append(this.o).append(":").toString(), this.a, this.b + i2, 20);
                }
                oVar.setColor(16777215);
                for (int i3 = 0; i3 < this.aj; i3++) {
                    if ((this.aa * this.aj) + i3 < this.ak.length) {
                        oVar.a(this.ak[(this.aa * this.aj) + i3], this.a, this.b + i2 + (this.i * (i3 + 1)), 20);
                    }
                }
                this.ap.bh.a(oVar, 0, 220, i2 + this.h, false);
            }
            if (this.am) {
                for (int i4 = 0; i4 < this.aj; i4++) {
                    oVar.setColor(16777215);
                    oVar.a(this.al[i4], this.a + 40, this.b + i2 + (this.i * (i4 + 1)), 20);
                }
                oVar.setColor(11250449);
                oVar.a(this.al[this.p], this.a + 40, i2 + this.b + (this.i * (this.p + 1)), 20);
            }
        }
    }

    public final boolean l() {
        return this.k;
    }

    public final boolean m() {
        return this.am;
    }

    public final boolean n() {
        return this.an;
    }

    public final int p() {
        return this.ab;
    }
}
