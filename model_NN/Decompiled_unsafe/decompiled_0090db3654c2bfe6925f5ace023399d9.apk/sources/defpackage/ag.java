package defpackage;

import java.util.Hashtable;

/* renamed from: ag  reason: default package */
final class ag {
    private static Hashtable cY = new Hashtable();
    private static Hashtable nd = new Hashtable();

    ag() {
    }

    static void a(String str) {
        Integer num;
        if (str != null && (num = (Integer) nd.get(str)) != null) {
            Integer num2 = new Integer(num.intValue() - 1);
            if (num2.intValue() == 0) {
                nd.remove(str);
                cY.remove(str);
                return;
            }
            nd.put(str, num2);
        }
    }

    static at m(String str, String str2) {
        at atVar;
        if (cY.containsKey(str)) {
            atVar = (at) cY.get(str);
        } else {
            at m = at.m(str, str2);
            cY.put(str, m);
            atVar = m;
        }
        Integer num = (Integer) nd.get(atVar);
        nd.put(str, num == null ? new Integer(1) : new Integer(num.intValue() + 1));
        return atVar;
    }
}
