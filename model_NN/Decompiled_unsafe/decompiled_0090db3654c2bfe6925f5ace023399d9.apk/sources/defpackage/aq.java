package defpackage;

/* renamed from: aq  reason: default package */
public final class aq {
    public int a;
    public byte aj;
    public int b;
    public int e;
    public int h;
    public int i;
    public boolean k;

    public aq() {
    }

    public aq(int i2, int i3, int i4, int i5) {
        this.e = i2;
        this.a = i3;
        this.b = i4;
        this.h = i5;
        this.i = -1;
    }

    public aq(int i2, int i3, int i4, int i5, int i6) {
        this(i2, i3, i4, i5);
        this.i = i6;
    }
}
