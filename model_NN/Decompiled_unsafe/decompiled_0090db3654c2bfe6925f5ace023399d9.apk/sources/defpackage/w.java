package defpackage;

import com.a.a.e.l;
import com.a.a.e.o;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.enhance.MIDPHelper;

/* renamed from: w  reason: default package */
public final class w implements Runnable {
    private static String o;
    private String I;
    private String J;
    private int[] X;
    private int[] Z;
    private int a;
    private byte aA;
    private int aV;
    private int aa;
    private int ab;
    private int ac;
    private int ad;
    private int ae;
    private byte aj;
    private short ay;
    private int b;
    private byte bj;
    private Thread bo;
    private x bp;
    private aj[] bq;
    private am br;
    private au bs;
    private int e;
    private int h;
    private int i;
    private boolean k;
    private int p;
    private String r;
    private String s;
    private String t;
    private String u;
    private String v;
    private String w;

    public w(int i2, int i3, int i4, int i5, int i6, int i7, x xVar) {
        this.br = am.kA();
        this.t = "短信发送中";
        this.u = "发送失败\n请稍候再试";
        this.bj = 1;
        this.I = "确认";
        this.J = "返回";
        this.b = 131072;
        this.h = 262144;
        this.e = 4096;
        this.a = 8192;
        this.i = 240;
        this.aa = 320;
        this.X = null;
        this.X = new int[4];
        this.X[0] = 0;
        this.X[1] = this.aa - 30;
        this.X[2] = 30;
        this.X[3] = 30;
        this.Z = null;
        this.Z = new int[4];
        this.Z[0] = this.i - 30;
        this.Z[1] = this.aa - 30;
        this.Z[2] = 30;
        this.Z[3] = 30;
        this.aV = this.aa - 5;
        this.bs = null;
        l k2 = l.k(0, 0, 8);
        this.bs = new au(10, 10, this.i - 10, this.aV - k2.getHeight());
        this.bs.e(k2);
        this.bp = xVar;
        this.br.a("sms");
    }

    private void a(String str) {
        DataInputStream dataInputStream = new DataInputStream(MIDPHelper.b(getClass(), str));
        while (true) {
            try {
                int read = dataInputStream.read();
                if (read == -1) {
                    try {
                        break;
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                } else if (read == this.ad) {
                    dataInputStream.readInt();
                    this.aA = dataInputStream.readByte();
                    if (this.aA != 0) {
                        this.aA = 1;
                    }
                    dataInputStream.readUTF();
                    dataInputStream.readUTF();
                    this.r = dataInputStream.readUTF();
                    this.s = dataInputStream.readUTF();
                    this.ae = dataInputStream.readByte();
                    this.ab = dataInputStream.readByte();
                    if (this.aA != 0) {
                        this.ac = dataInputStream.readByte();
                        this.v = dataInputStream.readUTF();
                    }
                    this.w = dataInputStream.readUTF();
                } else {
                    dataInputStream.skip((long) dataInputStream.readInt());
                }
            } catch (Exception e3) {
                e3.printStackTrace();
                try {
                    dataInputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    dataInputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
                throw th;
            }
        }
        dataInputStream.close();
        try {
            DataInputStream o2 = this.br.o("cost");
            this.ay = o2.readShort();
            o2.close();
        } catch (Exception e6) {
            e6.printStackTrace();
        }
        if (this.aA != 0) {
            try {
                DataInputStream o3 = this.br.o("payTimes");
                this.bj = o3.readByte();
                o3.close();
            } catch (IOException e7) {
                e7.printStackTrace();
            } catch (Exception e8) {
                e8.printStackTrace();
            }
            StringBuffer stringBuffer = new StringBuffer(20);
            stringBuffer.append(new StringBuffer().append(this.r).append((int) this.bj).append(this.v).toString());
            this.bs.a(stringBuffer.toString());
            return;
        }
        this.bs.a(this.r);
    }

    public final void a(int i2) {
        if (i2 == this.e) {
            this.bs.b();
        } else if (i2 == this.a) {
            this.bs.a();
        } else {
            switch (this.aj) {
                case 1:
                    if (i2 == this.b) {
                        if (this.ae == 0 || this.ay < this.ae) {
                            this.bs.a(this.t);
                            if (this.bo == null) {
                                this.bo = new Thread(this);
                                this.bo.start();
                            }
                            this.bo = null;
                            this.aj = 2;
                            return;
                        }
                        this.bs.a(this.w);
                        this.aj = 4;
                        return;
                    } else if (i2 == this.h && this.bp != null) {
                        this.bp.o();
                        return;
                    } else {
                        return;
                    }
                case 2:
                case 3:
                default:
                    return;
                case 4:
                    if (i2 == this.b && this.bp != null) {
                        this.bp.a();
                        return;
                    }
                    return;
                case 5:
                    if (i2 == this.h && this.bp != null) {
                        this.bp.b();
                        return;
                    }
                    return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x009e A[SYNTHETIC, Splitter:B:36:0x009e] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00aa A[SYNTHETIC, Splitter:B:42:0x00aa] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r8, int r9) {
        /*
            r7 = this;
            r0 = 0
            r2 = 0
            r7.ad = r9
            java.lang.String r1 = "/mm2.ps"
            java.lang.Class r3 = r7.getClass()
            java.io.InputStream r3 = javax.microedition.enhance.MIDPHelper.b(r3, r1)
            if (r3 == 0) goto L_0x008c
            java.io.DataInputStream r1 = new java.io.DataInputStream
            r1.<init>(r3)
            int r3 = r1.readInt()     // Catch:{ IOException -> 0x0098 }
            r4 = 1024(0x400, float:1.435E-42)
            if (r3 >= r4) goto L_0x00e0
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0098 }
            r1.read(r3)     // Catch:{ IOException -> 0x0098 }
        L_0x0022:
            r1.close()     // Catch:{ IOException -> 0x0098 }
            if (r3 == 0) goto L_0x00de
            int r4 = r3.length     // Catch:{ IOException -> 0x00db, all -> 0x00d8 }
            r1 = r0
        L_0x0029:
            if (r1 >= r4) goto L_0x0036
            byte r5 = r3[r1]     // Catch:{ IOException -> 0x00db, all -> 0x00d8 }
            int r6 = r4 % 127
            r5 = r5 ^ r6
            byte r5 = (byte) r5     // Catch:{ IOException -> 0x00db, all -> 0x00d8 }
            r3[r1] = r5     // Catch:{ IOException -> 0x00db, all -> 0x00d8 }
            int r1 = r1 + 1
            goto L_0x0029
        L_0x0036:
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ IOException -> 0x00db, all -> 0x00d8 }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ IOException -> 0x00db, all -> 0x00d8 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x00db, all -> 0x00d8 }
            r1.<init>(r4)     // Catch:{ IOException -> 0x00db, all -> 0x00d8 }
            int r2 = r1.readInt()     // Catch:{ IOException -> 0x0098 }
            r3 = 0
            r7.bq = r3     // Catch:{ IOException -> 0x0098 }
            aj[] r3 = new defpackage.aj[r2]     // Catch:{ IOException -> 0x0098 }
            r7.bq = r3     // Catch:{ IOException -> 0x0098 }
        L_0x004b:
            if (r0 >= r2) goto L_0x0070
            aj[] r3 = r7.bq     // Catch:{ IOException -> 0x0098 }
            aj r4 = new aj     // Catch:{ IOException -> 0x0098 }
            r4.<init>()     // Catch:{ IOException -> 0x0098 }
            r3[r0] = r4     // Catch:{ IOException -> 0x0098 }
            r1.readUTF()     // Catch:{ IOException -> 0x0098 }
            aj[] r3 = r7.bq     // Catch:{ IOException -> 0x0098 }
            r3 = r3[r0]     // Catch:{ IOException -> 0x0098 }
            java.lang.String r4 = r1.readUTF()     // Catch:{ IOException -> 0x0098 }
            r3.o = r4     // Catch:{ IOException -> 0x0098 }
            aj[] r3 = r7.bq     // Catch:{ IOException -> 0x0098 }
            r3 = r3[r0]     // Catch:{ IOException -> 0x0098 }
            java.lang.String r4 = r1.readUTF()     // Catch:{ IOException -> 0x0098 }
            r3.r = r4     // Catch:{ IOException -> 0x0098 }
            int r0 = r0 + 1
            goto L_0x004b
        L_0x0070:
            java.lang.String r0 = r1.readUTF()     // Catch:{ IOException -> 0x0098 }
            defpackage.w.o = r0     // Catch:{ IOException -> 0x0098 }
        L_0x0076:
            if (r1 == 0) goto L_0x007b
            r1.close()     // Catch:{ IOException -> 0x0093 }
        L_0x007b:
            am r0 = r7.br     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r2 = "index"
            java.io.DataInputStream r1 = r0.o(r2)     // Catch:{ Exception -> 0x00b8 }
            byte r0 = r1.readByte()     // Catch:{ Exception -> 0x00b8 }
            r7.p = r0     // Catch:{ Exception -> 0x00b8 }
            r1.close()     // Catch:{ IOException -> 0x00b3 }
        L_0x008c:
            r7.a(r8)     // Catch:{ Exception -> 0x00cf }
            r0 = 1
            r7.aj = r0
            return
        L_0x0093:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x007b
        L_0x0098:
            r0 = move-exception
        L_0x0099:
            r0.printStackTrace()     // Catch:{ all -> 0x00a7 }
            if (r1 == 0) goto L_0x007b
            r1.close()     // Catch:{ IOException -> 0x00a2 }
            goto L_0x007b
        L_0x00a2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x007b
        L_0x00a7:
            r0 = move-exception
        L_0x00a8:
            if (r1 == 0) goto L_0x00ad
            r1.close()     // Catch:{ IOException -> 0x00ae }
        L_0x00ad:
            throw r0
        L_0x00ae:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00ad
        L_0x00b3:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x008c
        L_0x00b8:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00c5 }
            r1.close()     // Catch:{ IOException -> 0x00c0 }
            goto L_0x008c
        L_0x00c0:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x008c
        L_0x00c5:
            r0 = move-exception
            r1.close()     // Catch:{ IOException -> 0x00ca }
        L_0x00c9:
            throw r0
        L_0x00ca:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00c9
        L_0x00cf:
            r0 = move-exception
            java.lang.Exception r0 = new java.lang.Exception
            java.lang.String r1 = "短信信息初始化错误"
            r0.<init>(r1)
            throw r0
        L_0x00d8:
            r0 = move-exception
            r1 = r2
            goto L_0x00a8
        L_0x00db:
            r0 = move-exception
            r1 = r2
            goto L_0x0099
        L_0x00de:
            r1 = r2
            goto L_0x0076
        L_0x00e0:
            r3 = r2
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.w.a(java.lang.String, int):void");
    }

    public final void b(o oVar) {
        ae.a(oVar, 0, 0, 240, 320, 0, 9209989, 0, 5446417, null, 0, 0, 0, 0, 3, 1);
        this.bs.b(oVar);
        oVar.setColor(16777215);
        oVar.a(l.k(0, 0, 8));
        String str = null;
        String str2 = null;
        switch (this.aj) {
            case 1:
                if (!this.k) {
                    str = this.I;
                    str2 = this.J;
                    break;
                } else {
                    str = this.J;
                    str2 = this.I;
                    break;
                }
            case 3:
                if (!this.k) {
                    str = this.I;
                    str2 = this.J;
                    break;
                } else {
                    str = this.J;
                    str2 = this.I;
                    break;
                }
            case 4:
                if (!this.k) {
                    str = this.I;
                    break;
                } else {
                    str2 = this.I;
                    break;
                }
            case 5:
                if (!this.k) {
                    str2 = this.J;
                    break;
                } else {
                    str = this.J;
                    break;
                }
        }
        if (str != null) {
            oVar.a(str, 10, this.aV, 36);
        }
        if (str2 != null) {
            int i2 = this.aV;
            oVar.a(str2, this.i - 10, i2, 40);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ec A[SYNTHETIC, Splitter:B:39:0x00ec] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0100 A[SYNTHETIC, Splitter:B:47:0x0100] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0109 A[SYNTHETIC, Splitter:B:52:0x0109] */
    /* JADX WARNING: Removed duplicated region for block: B:85:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:86:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:36:0x00e0=Splitter:B:36:0x00e0, B:44:0x00f4=Splitter:B:44:0x00f4} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r8 = this;
            r7 = 5
            r6 = 4
            r5 = 1
            r4 = 0
            aj[] r0 = r8.bq
            if (r0 == 0) goto L_0x00b7
            aj[] r0 = r8.bq
            int r1 = r8.p
            r0 = r0[r1]
            java.lang.String r1 = r0.r
            aj[] r0 = r8.bq
            int r2 = r8.p
            r0 = r0[r2]
            java.lang.String r0 = r0.o
        L_0x0018:
            r1.trim()
            r0.trim()
            java.lang.String r2 = defpackage.w.o
            if (r2 == 0) goto L_0x0035
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r2 = defpackage.w.o
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
        L_0x0035:
            int r2 = r8.ad
            r3 = 10
            if (r2 >= r3) goto L_0x00bd
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r2 = ",0"
            java.lang.StringBuffer r0 = r0.append(r2)
            int r2 = r8.ad
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r2 = r0
        L_0x0055:
            java.lang.String r0 = "sms://"
            boolean r0 = r1.startsWith(r0)
            if (r0 != 0) goto L_0x0070
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r3 = "sms://"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
        L_0x0070:
            aj[] r0 = r8.bq
            if (r0 == 0) goto L_0x0083
            int r0 = r8.p
            int r0 = r0 + 1
            r8.p = r0
            int r0 = r8.p
            aj[] r3 = r8.bq
            int r3 = r3.length
            if (r0 != r3) goto L_0x0083
            r8.p = r4
        L_0x0083:
            am r0 = r8.br     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r3 = "index"
            r4 = 0
            java.io.DataOutputStream r0 = r0.a(r3, r4)     // Catch:{ Exception -> 0x00d9 }
            int r3 = r8.p     // Catch:{ Exception -> 0x00d9 }
            r0.writeByte(r3)     // Catch:{ Exception -> 0x00d9 }
            am r0 = r8.br     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r3 = "index"
            r0.p(r3)     // Catch:{ Exception -> 0x00d9 }
        L_0x0098:
            r3 = 0
            com.a.a.c.b r0 = com.a.a.c.d.q(r1)     // Catch:{ SecurityException -> 0x00de, Exception -> 0x00f2, all -> 0x0106 }
            com.a.a.q.c r0 = (com.a.a.q.c) r0     // Catch:{ SecurityException -> 0x00de, Exception -> 0x00f2, all -> 0x0106 }
            java.lang.String r1 = "text"
            com.a.a.q.b r1 = r0.af(r1)     // Catch:{ SecurityException -> 0x01d1, Exception -> 0x01ce }
            com.a.a.q.h r1 = (com.a.a.q.h) r1     // Catch:{ SecurityException -> 0x01d1, Exception -> 0x01ce }
            r1.an(r2)     // Catch:{ SecurityException -> 0x01d1, Exception -> 0x01ce }
            r0.a(r1)     // Catch:{ SecurityException -> 0x01d1, Exception -> 0x01ce }
            if (r0 == 0) goto L_0x00b2
            r0.close()     // Catch:{ Exception -> 0x01c3 }
        L_0x00b2:
            byte r0 = r8.aj
            if (r0 != r7) goto L_0x010d
        L_0x00b6:
            return
        L_0x00b7:
            java.lang.String r1 = "13472439275"
            java.lang.String r0 = ""
            goto L_0x0018
        L_0x00bd:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r2 = ","
            java.lang.StringBuffer r0 = r0.append(r2)
            int r2 = r8.ad
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r2 = r0
            goto L_0x0055
        L_0x00d9:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0098
        L_0x00de:
            r0 = move-exception
            r0 = r3
        L_0x00e0:
            au r1 = r8.bs     // Catch:{ all -> 0x01c9 }
            java.lang.String r2 = r8.u     // Catch:{ all -> 0x01c9 }
            r1.a(r2)     // Catch:{ all -> 0x01c9 }
            r1 = 5
            r8.aj = r1     // Catch:{ all -> 0x01c9 }
            if (r0 == 0) goto L_0x00b6
            r0.close()     // Catch:{ Exception -> 0x00f0 }
            goto L_0x00b6
        L_0x00f0:
            r0 = move-exception
            goto L_0x00b6
        L_0x00f2:
            r0 = move-exception
            r0 = r3
        L_0x00f4:
            au r1 = r8.bs     // Catch:{ all -> 0x01c9 }
            java.lang.String r2 = r8.u     // Catch:{ all -> 0x01c9 }
            r1.a(r2)     // Catch:{ all -> 0x01c9 }
            r1 = 5
            r8.aj = r1     // Catch:{ all -> 0x01c9 }
            if (r0 == 0) goto L_0x00b6
            r0.close()     // Catch:{ Exception -> 0x0104 }
            goto L_0x00b6
        L_0x0104:
            r0 = move-exception
            goto L_0x00b6
        L_0x0106:
            r0 = move-exception
        L_0x0107:
            if (r3 == 0) goto L_0x010c
            r3.close()     // Catch:{ Exception -> 0x01c6 }
        L_0x010c:
            throw r0
        L_0x010d:
            short r0 = r8.ay
            int r1 = r8.ab
            int r0 = r0 + r1
            short r0 = (short) r0
            r8.ay = r0
            am r0 = r8.br     // Catch:{ Exception -> 0x0163 }
            java.lang.String r1 = "index"
            r2 = 0
            java.io.DataOutputStream r0 = r0.a(r1, r2)     // Catch:{ Exception -> 0x0163 }
            int r1 = r8.p     // Catch:{ Exception -> 0x0163 }
            r0.writeByte(r1)     // Catch:{ Exception -> 0x0163 }
            am r0 = r8.br     // Catch:{ Exception -> 0x0163 }
            java.lang.String r1 = "index"
            r0.p(r1)     // Catch:{ Exception -> 0x0163 }
        L_0x012a:
            byte r0 = r8.aA
            if (r0 == 0) goto L_0x01b8
            byte r0 = r8.bj
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r8.bj = r0
            byte r0 = r8.bj
            int r1 = r8.ac
            if (r0 <= r1) goto L_0x0168
            r8.bj = r5
            au r0 = r8.bs
            java.lang.String r1 = r8.s
            r0.a(r1)
            r8.aj = r6
        L_0x0146:
            am r0 = r8.br     // Catch:{ IOException -> 0x015d, Exception -> 0x01b2 }
            java.lang.String r1 = "payTimes"
            r2 = 0
            java.io.DataOutputStream r0 = r0.a(r1, r2)     // Catch:{ IOException -> 0x015d, Exception -> 0x01b2 }
            byte r1 = r8.bj     // Catch:{ IOException -> 0x015d, Exception -> 0x01b2 }
            r0.writeByte(r1)     // Catch:{ IOException -> 0x015d, Exception -> 0x01b2 }
            am r0 = r8.br     // Catch:{ IOException -> 0x015d, Exception -> 0x01b2 }
            java.lang.String r1 = "payTimes"
            r0.p(r1)     // Catch:{ IOException -> 0x015d, Exception -> 0x01b2 }
            goto L_0x00b6
        L_0x015d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00b6
        L_0x0163:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x012a
        L_0x0168:
            int r0 = r8.ae
            if (r0 == 0) goto L_0x0181
            byte r0 = r8.bj
            int r1 = r8.ab
            int r0 = r0 * r1
            int r1 = r8.ae
            if (r0 <= r1) goto L_0x0181
            r8.bj = r5
            au r0 = r8.bs
            java.lang.String r1 = r8.s
            r0.a(r1)
            r8.aj = r6
            goto L_0x0146
        L_0x0181:
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r1 = 20
            r0.<init>(r1)
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r2 = r8.r
            java.lang.StringBuffer r1 = r1.append(r2)
            byte r2 = r8.bj
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = r8.v
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.append(r1)
            au r1 = r8.bs
            java.lang.String r0 = r0.toString()
            r1.a(r0)
            r8.aj = r5
            goto L_0x0146
        L_0x01b2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00b6
        L_0x01b8:
            au r0 = r8.bs
            java.lang.String r1 = r8.s
            r0.a(r1)
            r8.aj = r6
            goto L_0x00b6
        L_0x01c3:
            r0 = move-exception
            goto L_0x00b2
        L_0x01c6:
            r1 = move-exception
            goto L_0x010c
        L_0x01c9:
            r1 = move-exception
            r3 = r0
            r0 = r1
            goto L_0x0107
        L_0x01ce:
            r1 = move-exception
            goto L_0x00f4
        L_0x01d1:
            r1 = move-exception
            goto L_0x00e0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.w.run():void");
    }
}
