package defpackage;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Vector;
import javax.microedition.enhance.MIDPHelper;

/* renamed from: f  reason: default package */
public final class f {
    private int[] X;
    private int a;
    private byte aj;
    private byte[] at;
    private Vector au = new Vector();
    private int e;
    private boolean k = false;
    private String o;

    /* JADX WARNING: Removed duplicated region for block: B:20:0x006a A[SYNTHETIC, Splitter:B:20:0x006a] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0074 A[SYNTHETIC, Splitter:B:26:0x0074] */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public f(java.lang.String r6) {
        /*
            r5 = this;
            r0 = 0
            r5.<init>()
            r5.k = r0
            java.util.Vector r1 = new java.util.Vector
            r1.<init>()
            r5.au = r1
            r5.o = r6
            r2 = 0
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0063, all -> 0x0070 }
            java.lang.Class r3 = r5.getClass()     // Catch:{ Exception -> 0x0063, all -> 0x0070 }
            java.io.InputStream r3 = javax.microedition.enhance.MIDPHelper.b(r3, r6)     // Catch:{ Exception -> 0x0063, all -> 0x0070 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0063, all -> 0x0070 }
            byte r2 = r1.readByte()     // Catch:{ Exception -> 0x007e }
            r5.aj = r2     // Catch:{ Exception -> 0x007e }
            byte r2 = r5.aj     // Catch:{ Exception -> 0x007e }
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x007e }
            r5.at = r2     // Catch:{ Exception -> 0x007e }
            r2 = r0
        L_0x002a:
            byte r3 = r5.aj     // Catch:{ Exception -> 0x007e }
            if (r2 >= r3) goto L_0x0039
            byte[] r3 = r5.at     // Catch:{ Exception -> 0x007e }
            byte r4 = r1.readByte()     // Catch:{ Exception -> 0x007e }
            r3[r2] = r4     // Catch:{ Exception -> 0x007e }
            int r2 = r2 + 1
            goto L_0x002a
        L_0x0039:
            int r2 = r1.readInt()     // Catch:{ Exception -> 0x007e }
            r5.e = r2     // Catch:{ Exception -> 0x007e }
            int r2 = r5.e     // Catch:{ Exception -> 0x007e }
            int[] r2 = new int[r2]     // Catch:{ Exception -> 0x007e }
            r5.X = r2     // Catch:{ Exception -> 0x007e }
        L_0x0045:
            int r2 = r5.e     // Catch:{ Exception -> 0x007e }
            if (r0 >= r2) goto L_0x0054
            int[] r2 = r5.X     // Catch:{ Exception -> 0x007e }
            int r3 = r1.readInt()     // Catch:{ Exception -> 0x007e }
            r2[r0] = r3     // Catch:{ Exception -> 0x007e }
            int r0 = r0 + 1
            goto L_0x0045
        L_0x0054:
            byte r0 = r5.aj     // Catch:{ Exception -> 0x007e }
            int r0 = r0 + 5
            int r2 = r5.e     // Catch:{ Exception -> 0x007e }
            int r2 = r2 << 2
            int r0 = r0 + r2
            r5.a = r0     // Catch:{ Exception -> 0x007e }
            r1.close()     // Catch:{ Exception -> 0x0078 }
        L_0x0062:
            return
        L_0x0063:
            r0 = move-exception
            r1 = r2
        L_0x0065:
            r0.printStackTrace()     // Catch:{ all -> 0x007c }
            if (r1 == 0) goto L_0x0062
            r1.close()     // Catch:{ Exception -> 0x006e }
            goto L_0x0062
        L_0x006e:
            r0 = move-exception
            goto L_0x0062
        L_0x0070:
            r0 = move-exception
            r1 = r2
        L_0x0072:
            if (r1 == 0) goto L_0x0077
            r1.close()     // Catch:{ Exception -> 0x007a }
        L_0x0077:
            throw r0
        L_0x0078:
            r0 = move-exception
            goto L_0x0062
        L_0x007a:
            r1 = move-exception
            goto L_0x0077
        L_0x007c:
            r0 = move-exception
            goto L_0x0072
        L_0x007e:
            r0 = move-exception
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.f.<init>(java.lang.String):void");
    }

    private static int[] a(Vector vector) {
        int[] iArr = new int[vector.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= iArr.length) {
                return iArr;
            }
            iArr[i2] = ((Integer) vector.elementAt(i2)).intValue();
            i = i2 + 1;
        }
    }

    private static String[] b(Vector vector) {
        String[] strArr = new String[vector.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= strArr.length) {
                return strArr;
            }
            strArr[i2] = (String) vector.elementAt(i2);
            i = i2 + 1;
        }
    }

    public final void b() {
        for (int i = 0; i < this.e; i++) {
            this.au.addElement(c(i));
        }
        this.k = true;
    }

    public final r c(int i) {
        if (this.k) {
            return (r) this.au.elementAt(i);
        }
        r rVar = new r();
        Vector vector = new Vector();
        Vector vector2 = new Vector();
        Vector vector3 = new Vector();
        Vector vector4 = new Vector();
        Vector vector5 = new Vector();
        try {
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.b(getClass(), this.o));
            dataInputStream.skip((long) (this.a + this.X[i]));
            for (byte b = 0; b < this.aj; b++) {
                byte b2 = this.at[b];
                if (b2 == 1) {
                    vector.addElement(new Integer(dataInputStream.readInt()));
                }
                if (b2 == 2) {
                    vector.addElement(new Integer(vector2.size()));
                    vector2.addElement(dataInputStream.readUTF());
                }
                if (b2 == 3) {
                    vector.addElement(new Integer(vector3.size()));
                    int readInt = dataInputStream.readInt();
                    vector3.addElement(new Integer(vector2.size()));
                    vector3.addElement(new Integer(readInt));
                    for (int i2 = 0; i2 < readInt; i2++) {
                        vector2.addElement(dataInputStream.readUTF());
                    }
                }
                if (b2 == 4) {
                    vector.addElement(new Integer(dataInputStream.readInt()));
                }
                if (b2 == 5) {
                    vector.addElement(new Integer(vector4.size()));
                    int readInt2 = dataInputStream.readInt();
                    vector4.addElement(new Integer(vector5.size()));
                    vector4.addElement(new Integer(readInt2));
                    for (int i3 = 0; i3 < readInt2; i3++) {
                        vector5.addElement(new Integer(dataInputStream.readInt()));
                    }
                }
            }
            dataInputStream.close();
        } catch (IOException e2) {
        }
        rVar.aq = a(vector);
        a(vector3);
        rVar.ak = b(vector2);
        rVar.X = a(vector4);
        rVar.Z = a(vector5);
        return rVar;
    }

    public final int p() {
        return this.e;
    }
}
