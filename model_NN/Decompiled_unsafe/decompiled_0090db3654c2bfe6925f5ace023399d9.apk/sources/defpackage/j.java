package defpackage;

import com.a.a.p.c;
import com.fasterxml.jackson.core.sym.CharsToNameCanonicalizer;

/* renamed from: j  reason: default package */
public final class j {
    private static j aH = new j();
    public int a;
    private byte[] aB;
    private short[] aD = new short[128];
    private t aI;
    private int aa;
    private byte aj;
    public boolean am;
    private boolean an;
    private byte[] at;
    private int b;
    public int e;
    private int h;
    private int i;
    public boolean k;

    private j() {
    }

    public static j r() {
        return aH;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: y.a(int, boolean):void
     arg types: [int, int]
     candidates:
      y.a(int, int):void
      y.a(ai, java.io.DataOutputStream):void
      y.a(int, boolean):void */
    public final void a() {
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        if (this.k) {
            if (!this.am) {
                l.t();
                y I = y.I();
                switch (this.a) {
                    case 2:
                        y.I();
                        if (y.an) {
                            return;
                        }
                        break;
                    case 3:
                    case 23:
                    case CharsToNameCanonicalizer.HASH_MULT:
                    case c.TARGET:
                        return;
                    case 4:
                        if (I.ap.vi.l()) {
                            return;
                        }
                        break;
                    case 5:
                        y.I();
                        if (y.an) {
                            return;
                        }
                        break;
                    case 10:
                        if (I.ap.kz() != null) {
                            return;
                        }
                        break;
                    case 14:
                        if (y.I().aM) {
                            return;
                        }
                        break;
                    case 15:
                        if (y.I().bR) {
                            return;
                        }
                        break;
                    case 17:
                        if (I.ap.vi.n()) {
                            System.out.println("---------------------------------------->OK");
                            this.h = I.ap.vi.p();
                            break;
                        } else {
                            return;
                        }
                    case com.a.a.b.c.INT_16:
                        if (I.ap.kz() == null) {
                            this.am = true;
                            this.k = false;
                            y.ce = true;
                            I.d((byte) 3);
                            return;
                        }
                        return;
                    case 25:
                        if (!I.bY) {
                            this.am = true;
                            this.k = false;
                            return;
                        }
                        return;
                    case 31:
                        if (I.ap.l()) {
                            return;
                        }
                        break;
                    case 32:
                        if (I.ap.kz() != null) {
                            return;
                        }
                        break;
                    case 34:
                        if (I.bZ) {
                            return;
                        }
                        break;
                    case com.a.a.e.c.KEY_NUM1:
                        this.am = true;
                        return;
                    case 50:
                        if (y.I().bS) {
                            return;
                        }
                        break;
                    case com.a.a.e.c.KEY_NUM3:
                        if (y.I().bT) {
                            return;
                        }
                        break;
                    case com.a.a.e.c.KEY_NUM5:
                        if (this.i <= 0) {
                            this.i = 0;
                            break;
                        } else {
                            this.i--;
                            return;
                        }
                    case 60:
                        if (y.I().bU) {
                            return;
                        }
                        break;
                    case 64:
                        if (I.ap.aM) {
                            return;
                        }
                        break;
                    case 65:
                        if (!I.ap.bS) {
                            I.ap.bR = false;
                            break;
                        } else {
                            return;
                        }
                    case 67:
                        if (!I.cd) {
                            I.ap.P();
                            break;
                        } else {
                            return;
                        }
                    case 72:
                        if (!k.k) {
                            System.out.println("---------------------------------------->SMS");
                            if (!k.an) {
                                if (k.am) {
                                    k.am = false;
                                    switch (k.e) {
                                        case 11:
                                            while (i5 < y.bF.length) {
                                                y.bF[i5].ad += 3;
                                                y.bF[i5].aV = y.k(y.bF[i5].ad);
                                                y.bF[i5].aX = y.e(y.bF[i5].da, y.bF[i5].ad);
                                                y.bF[i5].aZ = y.e(y.bF[i5].x, y.bF[i5].ad);
                                                y.bF[i5].ba = y.e(y.bF[i5].y, y.bF[i5].ad);
                                                y.bF[i5].bb = y.e(y.bF[i5].z, y.bF[i5].ad);
                                                y.bF[i5].bc = y.e(y.bF[i5].db, y.bF[i5].ad);
                                                i5++;
                                            }
                                            break;
                                    }
                                }
                            } else {
                                k.an = false;
                                this.h = this.aa;
                            }
                            this.am = true;
                            return;
                        }
                        return;
                    case 73:
                        if (!k.k) {
                            System.out.println("---------------------------------------->SMS2");
                            if (k.an) {
                                k.an = false;
                                aH.k = false;
                                I.a();
                                I.ap = null;
                                I.d((byte) 2);
                                return;
                            } else if (k.am) {
                                y.ab = 0;
                                k.am = false;
                                y.cf = true;
                                I.q();
                                break;
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                }
                this.am = true;
                return;
            }
            l t = l.t();
            y I2 = y.I();
            if (this.b == this.aI.u()) {
                t.b(false);
                this.at = null;
                this.aB = null;
                this.k = false;
                this.am = true;
                return;
            }
            if (this.e >= this.aj && this.aj != 0) {
                this.an = false;
                this.am = true;
                this.b += this.aj;
                this.e = 0;
                this.aI.b();
            }
            if (this.h != 0) {
                this.a = this.aI.m(this.h);
                this.b = this.h;
                this.h = 0;
            } else if (this.an) {
                this.a = this.aI.f(this.aj);
            } else {
                this.a = this.aI.p();
            }
            this.am = false;
            if (!this.an) {
                this.b++;
            }
            System.out.println("");
            System.out.println("");
            System.out.println("");
            switch (this.a) {
                case -1:
                case 0:
                case 1:
                case 9:
                case 11:
                case 16:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case com.a.a.e.c.KEY_POUND:
                case 37:
                case 38:
                case 41:
                case com.a.a.e.c.KEY_STAR:
                case 43:
                case 44:
                case c.TARGET:
                case c.HTTP:
                default:
                    return;
                case 2:
                    y.I();
                    if (!y.an) {
                        I2.e = 1;
                        I2.b = aa.h;
                        int k2 = this.aI.k(0);
                        int[] iArr = new int[k2];
                        int[] iArr2 = new int[k2];
                        for (int i6 = 0; i6 < k2; i6++) {
                            iArr[i6] = this.aI.k(i6 + 1);
                        }
                        I2.X = new int[3];
                        I2.a = 0;
                        int i7 = 0;
                        for (int i8 = 0; i8 < y.bF.length; i8++) {
                            if (y.bF[i8].aM) {
                                I2.X[i7] = i8;
                                I2.a++;
                                y.bF[i8].aM = false;
                                i7++;
                            }
                        }
                        I2.h = 0;
                        int length = y.bF.length;
                        int i9 = 0;
                        int i10 = 0;
                        while (i10 < k2) {
                            int i11 = i9;
                            int i12 = 0;
                            while (i12 < length) {
                                if (y.bF[i12].ac == iArr[i10]) {
                                    y.bF[i12].aM = true;
                                    i10++;
                                    i12 = length;
                                }
                                int i13 = i12 + 1;
                                if (i13 == length) {
                                    iArr2[i11] = iArr[i10];
                                    i11++;
                                    i10++;
                                    i12 = i13;
                                } else {
                                    i12 = i13;
                                }
                            }
                            i9 = i11;
                        }
                        for (int i14 = 0; i14 < i9; i14++) {
                            I2.a(y.v(iArr2[i14]));
                            y.bF[y.bF.length - 1].aM = true;
                            I2.h++;
                        }
                        int k3 = this.aI.k(4);
                        I2.aK = this.aI.h(8);
                        y.bG = new b[k3];
                        while (i5 < k3) {
                            y.I();
                            b[] bVarArr = y.bG;
                            y.I();
                            bVarArr[i5] = y.B(this.aI.k(i5 + 5));
                            i5++;
                        }
                        y.an = true;
                        aa.h = I2.b;
                        return;
                    }
                    return;
                case 3:
                    int k4 = this.aI.k(1);
                    int k5 = this.aI.k(2);
                    int k6 = this.aI.k(3);
                    try {
                        I2.bE.p = k4;
                        I2.bE.ab = k5;
                        I2.bE.ds = k6;
                        I2.bE.q();
                        I2.S();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    this.am = true;
                    return;
                case 4:
                    t.b(false);
                    I2.ap.vi.a(this.aI.k(0), this.aI.i(1), this.aI.h(2));
                    return;
                case 5:
                    y.I();
                    if (!y.an) {
                        I2.e = 0;
                        int k7 = this.aI.k(0);
                        I2.aK = this.aI.h(4);
                        y.bG = new b[k7];
                        while (i5 < k7) {
                            y.I();
                            b[] bVarArr2 = y.bG;
                            y.I();
                            bVarArr2[i5] = y.B(this.aI.k(i5 + 1));
                            i5++;
                        }
                        y.an = true;
                        return;
                    }
                    return;
                case 6:
                    int k8 = this.aI.k(0);
                    try {
                        int length2 = I2.ap.bF.length;
                        int i15 = 0;
                        while (true) {
                            if (i15 < length2) {
                                ai aiVar = I2.ap.bF[i15];
                                if (aiVar.a == 0 || aiVar.a == 3 || aiVar.op.i != k8) {
                                    i15++;
                                } else {
                                    aiVar.k = false;
                                }
                            }
                        }
                        int length3 = I2.ap.vh.x().length;
                        while (i5 < length3) {
                            aq aqVar = I2.ap.vh.x()[i5];
                            if (aqVar.i == k8) {
                                aqVar.k = false;
                            }
                            i5++;
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    this.am = true;
                    return;
                case 7:
                    y.a(this.aI.k(0), this.aI.k(1));
                    this.am = true;
                    return;
                case 8:
                    y.b(this.aI.k(0), false);
                    this.am = true;
                    return;
                case 10:
                    I2.ap.a(this.aI.i(0));
                    return;
                case 12:
                    I2.o();
                    I2.ap.e = this.aI.k(0);
                    int k9 = this.aI.k(1);
                    int k10 = this.aI.k(2);
                    int k11 = this.aI.k(3);
                    I2.bE.p = k9;
                    I2.bE.ab = k10;
                    I2.bE.ds = k11;
                    I2.d((byte) 4);
                    I2.ap.k = false;
                    this.am = true;
                    this.k = false;
                    return;
                case 13:
                    int k12 = this.aI.k(0);
                    int k13 = this.aI.k(1);
                    int length4 = I2.ap.bF.length;
                    int i16 = 0;
                    while (true) {
                        if (i16 < length4) {
                            ai aiVar2 = I2.ap.bF[i16];
                            if (aiVar2.a == 0 || aiVar2.a == 3 || aiVar2.op.i != k12) {
                                i16++;
                            } else {
                                aiVar2.b = 0;
                                aiVar2.ds = k13;
                            }
                        }
                    }
                    this.am = true;
                    return;
                case 14:
                    int k14 = this.aI.k(0);
                    int k15 = this.aI.k(1);
                    int[] iArr3 = new int[k15];
                    int[] iArr4 = new int[k15];
                    for (int i17 = 0; i17 < k15; i17++) {
                        iArr3[i17] = this.aI.k((i17 << 1) + 2);
                        iArr4[i17] = this.aI.k((i17 << 1) + 3);
                    }
                    int length5 = I2.ap.bF.length;
                    while (true) {
                        if (i5 < length5) {
                            ai aiVar3 = I2.ap.bF[i5];
                            if (aiVar3.a == 0 || aiVar3.a == 3 || aiVar3.op.i != k14) {
                                y.I().aM = true;
                                i5++;
                            } else {
                                aiVar3.a = 4;
                                aiVar3.b(iArr3, iArr4, 4);
                            }
                        }
                    }
                    if (this.an) {
                        this.am = true;
                        return;
                    }
                    return;
                case 15:
                    int k16 = this.aI.k(0);
                    int k17 = this.aI.k(1);
                    int k18 = this.aI.k(2);
                    int k19 = this.aI.k(3);
                    int k20 = this.aI.k(4);
                    int length6 = I2.ap.bF.length;
                    while (true) {
                        if (i5 < length6) {
                            ai aiVar4 = I2.ap.bF[i5];
                            if (aiVar4.a == 0 || aiVar4.a == 3 || aiVar4.op.i != k16) {
                                i5++;
                            } else {
                                aiVar4.a = 4;
                                aiVar4.ao = true;
                                aiVar4.a(k17, k18, k20, k19);
                            }
                        }
                    }
                    y.I().bR = true;
                    if (this.an) {
                        this.am = true;
                        return;
                    }
                    return;
                case 17:
                    t.b(false);
                    int k21 = this.aI.k(0);
                    String[] strArr = new String[k21];
                    int[] iArr5 = new int[k21];
                    while (i5 < k21) {
                        strArr[i5] = this.aI.i((i5 << 1) + 1);
                        iArr5[i5] = this.aI.k((i5 << 1) + 2);
                        i5++;
                    }
                    I2.ap.vi.a(strArr, k21, this.h, iArr5);
                    return;
                case 18:
                    this.am = true;
                    return;
                case 19:
                    this.b = this.aI.u();
                    System.out.println(new StringBuffer().append("execCmdNum9:   ").append(this.b).toString());
                    this.am = true;
                    return;
                case com.a.a.b.c.INT_16:
                    I2.ap.a("故事就到这里...");
                    return;
                case 21:
                    I2.ap.k = true;
                    I2.ap.a = this.aI.k(0);
                    this.am = true;
                    return;
                case 22:
                    I2.ap.k = false;
                    this.am = true;
                    return;
                case 23:
                    ai v = y.v(this.aI.k(0));
                    v.aK = true;
                    I2.a(v);
                    aa.i = y.bF.length;
                    ai[] aiVarArr = new ai[(I2.ap.bF.length + 1)];
                    System.arraycopy(I2.ap.bF, 0, aiVarArr, 0, I2.ap.bF.length);
                    I2.ap.bF = aiVarArr;
                    I2.ap.bF[I2.ap.bF.length - 1] = y.bF[y.bF.length - 1];
                    this.am = true;
                    return;
                case com.a.a.b.c.UUID:
                    int k22 = this.aI.k(0);
                    int k23 = this.aI.k(1);
                    int i18 = 0;
                    while (true) {
                        if (i18 >= y.bF.length) {
                            i2 = 0;
                            i3 = 0;
                            i4 = 0;
                        } else if (y.bF[i18].ac == k22) {
                            i4 = y.bF[i18].p;
                            i3 = y.bF[i18].ab;
                            i2 = y.bF[i18].ds;
                        } else {
                            i18++;
                        }
                    }
                    int length7 = I2.ap.bF.length;
                    while (true) {
                        if (i5 < length7) {
                            ai aiVar5 = I2.ap.bF[i5];
                            if (aiVar5.op.i == k23) {
                                aiVar5.p = i4;
                                aiVar5.ab = i3;
                                aiVar5.ds = i2;
                                aiVar5.k = true;
                            } else {
                                i5++;
                            }
                        }
                    }
                    I2.G(k22);
                    this.am = true;
                    return;
                case 25:
                    aa.e = this.aI.k(0);
                    I2.bX = true;
                    ak.am = true;
                    I2.bY = true;
                    aa.P();
                    this.k = false;
                    return;
                case 31:
                    String i19 = this.aI.i(0);
                    int k24 = this.aI.k(1);
                    int k25 = this.aI.k(2);
                    int k26 = this.aI.k(3);
                    I2.ap.c(i19, k24, k25, k26);
                    I2.ap.aK = true;
                    if (k26 != -1) {
                        return;
                    }
                    break;
                case 32:
                    if (y.bF != null) {
                        while (i5 < y.bF.length) {
                            y.j(i5, 99999);
                            y.k(i5, 99999);
                            i5++;
                        }
                        return;
                    }
                    return;
                case CharsToNameCanonicalizer.HASH_MULT:
                    int k27 = this.aI.k(0);
                    int length8 = I2.ap.bF.length;
                    while (true) {
                        if (i5 < length8) {
                            ai aiVar6 = I2.ap.bF[i5];
                            if (aiVar6.a == 0 || aiVar6.a == 3 || aiVar6.op.i != k27) {
                                i5++;
                            } else {
                                aiVar6.k = true;
                            }
                        }
                    }
                    this.am = true;
                    return;
                case 34:
                    t.b(false);
                    I2.bZ = true;
                    return;
                case 36:
                    if (this.aI.k(0) == 1) {
                        int k28 = this.aI.k(1);
                        int k29 = this.aI.k(2);
                        int k30 = this.aI.k(3);
                        int k31 = this.aI.k(4);
                        switch (k29) {
                            case 1:
                                if (this.aD[k28] != this.aD[k30]) {
                                    this.h = k31;
                                    break;
                                }
                                break;
                        }
                    }
                    this.am = true;
                    return;
                case 39:
                    this.aD[this.aI.k(0)] = (short) this.aI.k(1);
                    break;
                case com.a.a.b.c.BOOL:
                    short[] sArr = this.aD;
                    int k32 = this.aI.k(0);
                    sArr[k32] = (short) (sArr[k32] + this.aI.k(1));
                    break;
                case 45:
                    if (this.aI.k(0) == 1) {
                        int k33 = this.aI.k(1);
                        int k34 = this.aI.k(2);
                        int k35 = this.aI.k(3);
                        int k36 = this.aI.k(4);
                        switch (k34) {
                            case 1:
                                if (this.aD[k33] != k35) {
                                    this.h = k36;
                                    break;
                                }
                                break;
                        }
                    }
                    this.am = true;
                    return;
                case 46:
                    if (this.aI.k(0) == 1) {
                        int k37 = this.aI.k(1);
                        int k38 = this.aI.k(2);
                        int k39 = this.aI.k(3);
                        int k40 = this.aI.k(4);
                        byte b2 = 0;
                        while (i5 < this.aB.length) {
                            if (this.aB[i5] == k40) {
                                b2 = this.at[i5];
                            }
                            i5++;
                        }
                        switch (k38) {
                            case 1:
                                if (this.aD[k37] != k39) {
                                    this.h = b2;
                                    break;
                                }
                                break;
                            case 2:
                                if (this.aD[k37] == k39) {
                                    this.h = b2;
                                    break;
                                }
                                break;
                            case 3:
                                if (this.aD[k37] >= k39) {
                                    this.h = b2;
                                    break;
                                }
                                break;
                            case 4:
                                if (this.aD[k37] > k39) {
                                    this.h = b2;
                                    break;
                                }
                                break;
                            case 5:
                                if (this.aD[k37] <= k39) {
                                    this.h = b2;
                                    break;
                                }
                                break;
                            case 6:
                                System.out.println(new StringBuffer().append("aaaaaaaaaa   ").append((int) this.aD[k37]).toString());
                                System.out.println(new StringBuffer().append("bbbbbbbbbb   ").append(k39).toString());
                                if (this.aD[k37] < k39) {
                                    this.h = b2;
                                    break;
                                }
                                break;
                        }
                    }
                    this.am = true;
                    return;
                case 47:
                    int k41 = this.aI.k(0);
                    int length9 = I2.ap.bF.length;
                    int i20 = 0;
                    while (true) {
                        if (i20 < length9) {
                            ai aiVar7 = I2.ap.bF[i20];
                            if (aiVar7.a == 0 || aiVar7.a == 3 || aiVar7.op.i != k41) {
                                i20++;
                            } else {
                                aiVar7.a = 5;
                                aiVar7.bh.a(0, 1);
                            }
                        }
                    }
                    this.am = true;
                    return;
                case 48:
                    this.am = true;
                    return;
                case com.a.a.e.c.KEY_NUM1:
                    this.an = true;
                    this.aj = (byte) this.aI.k(0);
                    return;
                case 50:
                    int k42 = this.aI.k(0);
                    int[] iArr6 = new int[k42];
                    int[] iArr7 = new int[k42];
                    while (i5 < k42) {
                        iArr6[i5] = this.aI.k((i5 << 1) + 1);
                        iArr7[i5] = this.aI.k((i5 << 1) + 2);
                        i5++;
                    }
                    I2.ap.bE.aA = 1;
                    I2.ap.bE.b = 1;
                    I2.ap.bE.b(iArr6, iArr7, 4);
                    y.I().bS = true;
                    return;
                case com.a.a.e.c.KEY_NUM3:
                    int k43 = this.aI.k(0);
                    int k44 = this.aI.k(1);
                    int k45 = this.aI.k(2);
                    int k46 = this.aI.k(3);
                    I2.ap.bE.aA = 2;
                    I2.ap.bE.a(k43, k44, k46, k45);
                    y.I().bT = true;
                    return;
                case com.a.a.e.c.KEY_NUM4:
                    I2.ap.bE.ds = this.aI.k(0);
                    return;
                case com.a.a.e.c.KEY_NUM5:
                    this.i = this.aI.k(0);
                    return;
                case com.a.a.e.c.KEY_NUM6:
                    this.am = true;
                    return;
                case com.a.a.e.c.KEY_NUM7:
                    this.am = true;
                    return;
                case 56:
                    this.am = true;
                    return;
                case com.a.a.e.c.KEY_NUM9:
                    this.am = true;
                    return;
                case 58:
                    break;
                case 59:
                    y.a(this.aI.k(0), false);
                    this.am = true;
                    return;
                case 60:
                    int k47 = this.aI.k(0);
                    int k48 = this.aI.k(1);
                    int k49 = this.aI.k(2);
                    int k50 = this.aI.k(3);
                    boolean h2 = this.aI.h(4);
                    I2.ap.bE.aA = 3;
                    ai aiVar8 = I2.ap.bE;
                    aiVar8.eB = k47;
                    aiVar8.eD = k48;
                    aiVar8.eA = k49;
                    aiVar8.bj = (byte) k50;
                    aiVar8.bT = h2;
                    y.I().bU = true;
                    return;
                case 61:
                    int k51 = this.aI.k(0);
                    System.out.println(new StringBuffer().append("GET missionID:   ").append(k51).toString());
                    y.I();
                    y.c(k51, false);
                    this.am = true;
                    return;
                case 62:
                    System.out.println(new StringBuffer().append("OVER missionID:   ").append(this.aI.k(0)).toString());
                    y.I();
                    y.a(this.aI.k(0));
                    this.am = true;
                    return;
                case 63:
                    int k52 = this.aI.k(1);
                    byte b3 = 0;
                    for (int i21 = 0; i21 < this.aB.length; i21++) {
                        if (this.aB[i21] == k52) {
                            b3 = this.at[i21];
                        }
                    }
                    y.I();
                    if (y.I().k) {
                        this.am = true;
                    } else {
                        this.h = b3;
                        this.am = true;
                    }
                    y.I().k = false;
                    return;
                case 64:
                    int k53 = this.aI.k(0);
                    int k54 = this.aI.k(1);
                    I2.ap.aM = true;
                    I2.ap.bR = true;
                    I2.ap.b(k53, k54, 6);
                    return;
                case 65:
                    I2.ap.bS = true;
                    return;
                case c.TYPE:
                    y.I().bV = true;
                    this.am = true;
                    return;
                case 67:
                    I2.ap.p(this.aI.i(0));
                    I2.cd = true;
                    return;
                case c.TIME_ISO_8601:
                    int k55 = this.aI.k(0);
                    if (k55 == 0) {
                        I2.ap.an = true;
                        I2.ap.vj = new an(240, 320, 200, false, null, 240, 320);
                    } else if (k55 == 1) {
                        I2.ap.ao = true;
                        I2.ap.vk = new e();
                    }
                    this.am = true;
                    return;
                case 69:
                    I2.ap.an = false;
                    I2.ap.ao = false;
                    this.am = true;
                    return;
                case 72:
                    this.am = true;
                    return;
                case 73:
                    this.am = true;
                    return;
            }
            this.am = true;
        }
    }

    public final void a(int i2) {
        this.aI = null;
        this.aI = new t(i2);
    }

    public final void b() {
        this.aD = null;
        this.aD = new short[128];
    }

    public final int f(int i2) {
        if (this.k) {
            return -1;
        }
        this.a = -1;
        int f = this.aI.f(i2);
        if (f <= 0) {
            return 0;
        }
        this.b = 0;
        this.k = true;
        this.am = true;
        l.t().b(true);
        l.t();
        l.b();
        if (this.at == null) {
            this.at = new byte[5];
            this.aB = new byte[5];
            for (int i3 = 0; i3 < 5; i3++) {
                this.at[i3] = -1;
                this.aB[i3] = -1;
            }
        }
        int i4 = 0;
        for (byte b2 = 0; b2 < f; b2 = (byte) (b2 + 1)) {
            if (this.aI.e(b2) == 54 || this.aI.e(b2) == 55 || this.aI.e(b2) == 56 || this.aI.e(b2) == 57 || this.aI.e(b2) == 58) {
                this.at[i4] = b2;
                this.aB[i4] = (byte) this.aI.e(b2);
                i4++;
            }
        }
        System.out.println(new StringBuffer().append("execCmdNum1:   ").append(this.b).toString());
        return f;
    }

    public final short[] s() {
        return this.aD;
    }
}
