package defpackage;

import com.a.a.b.c;
import com.a.a.e.p;
import com.fasterxml.jackson.core.sym.CharsToNameCanonicalizer;
import java.io.DataOutputStream;
import java.lang.reflect.Array;
import java.util.Hashtable;
import java.util.Random;
import java.util.Vector;
import me.gall.sgp.sdk.entity.app.StructuredData;

/* renamed from: y  reason: default package */
public final class y {
    public static int[] Z = new int[27];
    private static y aQ;
    public static int[][] aR = ((int[][]) Array.newInstance(Integer.TYPE, 6, 9));
    private static Vector aS = new Vector(20);
    private static int aV;
    public static int aa = 0;
    public static int ab = 1;
    public static int ac;
    public static String[] ak = new String[27];
    public static String[] al = new String[27];
    public static boolean an;
    public static int[] aq = new int[35];
    public static int[] ar = new int[35];
    private static Random as = new Random();
    private static Vector au = new Vector(20);
    public static ai[] bF;
    public static b[] bG;
    private static int[] cA = new int[70];
    public static int[] cB = new int[70];
    private static int[] cC = new int[39];
    private static int[] cD = new int[39];
    public static int[][] cE;
    private static int[][] cF;
    public static int[][] cG;
    private static int[] cH = new int[16];
    public static String[] cI = new String[16];
    public static int[] cJ = new int[16];
    public static int[] cK = new int[16];
    public static int[] cL = new int[16];
    public static int[] cM = new int[16];
    public static int[] cN = new int[16];
    public static String[] cO = new String[16];
    public static int[][] cP;
    public static String[][] cQ;
    private static int[] cR;
    public static boolean cc;
    public static boolean ce;
    public static boolean cf;
    private static int[][] ch = ((int[][]) Array.newInstance(Integer.TYPE, 6, 9));
    private static int[] ci = new int[27];
    private static int[] cj = new int[27];
    private static int[] ck = new int[35];
    public static String[] cl = new String[35];
    private static int[] cm = new int[35];
    public static int[] cn = new int[35];
    public static int[] co = new int[35];
    private static int[][] cp = ((int[][]) Array.newInstance(Integer.TYPE, 35, 50));
    private static int[] cq = new int[35];
    public static int[] cr = new int[35];
    private static int[] cs = new int[70];
    public static String[] ct = new String[70];
    public static int[] cu = new int[70];
    public static int[] cv = new int[70];
    private static int[] cw = new int[70];
    public static int[] cx = new int[70];
    public static int[] cy = new int[70];
    private static int[][] cz = ((int[][]) Array.newInstance(Integer.TYPE, 70, 50));
    public static String o = "/role/";
    public static String r = "/role/battle/";
    private String I = "content_record";
    private String J = "legame";
    private String V = "vars_record";
    private String W = "scene_record";
    public int[] X;
    private l Y = l.t();
    public int a;
    private byte aA = 20;
    public byte[] aB;
    public short[] aD;
    public j aH = j.r();
    public boolean aK;
    public boolean aM;
    private Vector aT;
    private int ad;
    private int ae;
    public byte aj = 0;
    public boolean am;
    public boolean ao;
    public ak ap;
    public byte[] at;
    public int b;
    public ai bE;
    private ab bH;
    private String bI = "actor_record";
    private String bJ = "removeActor_record";
    private String bK = "Weapon_record";
    private String bL = "Equip_record";
    private String bM = "Item_record";
    private String bN = "EquipInBody_record";
    private String bO = "EquipInBodySave_record";
    private String bP = "Drop_record";
    private String bQ = "haveMissionInHand_record";
    public boolean bR;
    public boolean bS;
    public boolean bT;
    public boolean bU;
    public boolean bV;
    public boolean bW;
    boolean bX;
    boolean bY;
    boolean bZ;
    public String[] cS;
    public short[] cT;
    public byte[] cU;
    public byte[] cV;
    public byte[] cW;
    public byte[] cX;
    public Hashtable cY;
    boolean ca;
    public boolean cb;
    boolean cd;
    public boolean cg;
    public int e;
    public int h;
    public int i;
    public boolean k;
    public int p;
    public String s;
    public String t;
    private String u = "smsinfo";
    private String v = "sms_record";
    private String w = "rmsinfo";

    y() {
        new Vector(10);
        this.ad = 0;
        this.at = new byte[]{0, 0, 0};
        this.cS = new String[]{null, null, null};
        this.aD = new short[]{1, 1, 1};
        this.cT = new short[]{1, 1, 1};
        this.aB = new byte[]{1, 1, 1};
        this.cU = new byte[]{1, 1, 1};
        this.cV = new byte[]{1, 1, 1};
        this.cW = new byte[]{1, 1, 1};
        this.cX = new byte[]{1, 1, 1};
        this.aT = new Vector();
    }

    private static void A(int i2) {
        if (bF[i2].aW == bF[i2].aX + bF[i2].dt && bF[i2].aY == bF[i2].aZ + bF[i2].du) {
            ac = 3;
        }
    }

    public static b B(int i2) {
        f fVar = new f("/data/enemy.bin");
        b bVar = new b();
        bVar.e = i2;
        r c = fVar.c(i2);
        bVar.a = c.aq[1];
        bVar.b = c.aq[2];
        bVar.h = c.aq[3];
        bVar.o = c.i(4);
        bVar.i = c.aq[13];
        bVar.aa = c.aq[9];
        bVar.p = c.aq[10];
        bVar.ac = c.aq[5];
        bVar.ab = c.aq[6];
        bVar.ad = c.aq[7];
        bVar.ae = c.aq[8];
        System.out.println(new StringBuffer().append("enemyDropDown:  ").append(bVar.ac).toString());
        int indexOf = bVar.o.indexOf("xml");
        if (indexOf != -1) {
            bVar.o = bVar.o.substring(0, indexOf);
            bVar.o = new StringBuffer().append(bVar.o).append("ani").toString();
        }
        bVar.o = new StringBuffer().append(r).append(bVar.o).toString();
        return bVar;
    }

    public static final synchronized y I() {
        y yVar;
        synchronized (y.class) {
            if (aQ == null) {
                aQ = new y();
            }
            yVar = aQ;
        }
        return yVar;
    }

    private static void J() {
        f fVar = new f("/data/Magic.bin");
        af afVar = new af();
        for (int i2 = 0; i2 < 27; i2++) {
            r c = fVar.c(i2);
            afVar.o = c.i(0);
            afVar.e = c.aq[1];
            afVar.a = c.aq[3];
            afVar.r = c.i(17);
            ci[i2] = i2;
            ak[i2] = afVar.o;
            cj[i2] = afVar.e;
            Z[i2] = afVar.a;
            al[i2] = afVar.r;
        }
    }

    private static void K() {
        f fVar = new f("/data/weapon.bin");
        ad adVar = new ad();
        for (int i2 = 0; i2 < 35; i2++) {
            r c = fVar.c(i2);
            adVar.o = c.i(0);
            adVar.e = c.aq[1];
            adVar.a = c.aq[2];
            adVar.b = c.aq[3];
            adVar.h = c.aq[4];
            adVar.i = c.aq[5];
            adVar.X = c.j(6);
            adVar.aa = c.aq[7];
            ck[i2] = i2;
            cl[i2] = adVar.o;
            aq[i2] = adVar.e;
            ar[i2] = adVar.a;
            cm[i2] = adVar.b;
            cn[i2] = adVar.h;
            co[i2] = adVar.i;
            cr[i2] = adVar.aa;
            for (int i3 = 0; i3 < adVar.X.length; i3++) {
                cp[i2][i3] = adVar.X[i3];
            }
            cq[i2] = adVar.X.length;
        }
    }

    private static void L() {
        f fVar = new f("/data/equip.bin");
        ac acVar = new ac();
        for (int i2 = 0; i2 < 70; i2++) {
            r c = fVar.c(i2);
            acVar.o = c.i(0);
            acVar.e = c.aq[1];
            acVar.a = c.aq[2];
            acVar.b = c.aq[3];
            acVar.h = c.aq[4];
            acVar.i = c.aq[5];
            acVar.X = c.j(6);
            acVar.aa = c.aq[7];
            cs[i2] = i2;
            ct[i2] = acVar.o;
            cu[i2] = acVar.e;
            cv[i2] = acVar.a;
            cw[i2] = acVar.b;
            cx[i2] = acVar.h;
            cy[i2] = acVar.i;
            cB[i2] = acVar.aa;
            for (int i3 = 0; i3 < acVar.X.length; i3++) {
                cz[i2][i3] = acVar.X[i3];
            }
            cA[i2] = acVar.X.length;
        }
    }

    private static void M() {
        f fVar = new f("/data/addaffect.bin");
        q qVar = new q();
        for (int i2 = 0; i2 < 39; i2++) {
            qVar.e = fVar.c(i2).aq[1];
            cC[i2] = i2;
            cD[i2] = qVar.e;
        }
    }

    private static void N() {
        f fVar = new f("/data/item.bin");
        s sVar = new s();
        for (int i2 = 0; i2 < 16; i2++) {
            r c = fVar.c(i2);
            sVar.o = c.i(0);
            sVar.e = c.aq[1];
            sVar.a = c.aq[2];
            sVar.b = c.aq[3];
            sVar.h = c.aq[4];
            sVar.i = c.aq[5];
            sVar.r = c.i(6);
            cH[i2] = i2;
            cI[i2] = sVar.o;
            cJ[i2] = sVar.e;
            cK[i2] = sVar.a;
            cL[i2] = sVar.b;
            cM[i2] = sVar.h;
            cN[i2] = sVar.i;
            cO[i2] = sVar.r;
        }
    }

    public static void a(int i2) {
        if (cQ != null) {
            int i3 = 0;
            while (true) {
                if (i3 >= cR.length) {
                    i3 = 0;
                    break;
                } else if (cR[i3] == i2) {
                    break;
                } else {
                    i3++;
                }
            }
            cQ = ah.a(cQ, i3);
            cR = ah.c(cR, i3);
        }
    }

    public static void a(int i2, int i3) {
        if (cP == null) {
            int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, 1, 2);
            cP = iArr;
            iArr[0][0] = i2;
            cP[0][1] = i3;
        } else if (cP.length < 99) {
            int i4 = 0;
            for (int i5 = 0; i5 < cP.length; i5++) {
                if (cP[i5][0] == i2) {
                    if (cP[i5][1] + i3 > 99) {
                        i4 = (cP[i5][1] + i3) - 99;
                        cP[i5][1] = 99;
                    } else {
                        i4 = -1;
                        int[] iArr2 = cP[i5];
                        iArr2[1] = iArr2[1] + i3;
                    }
                }
            }
            if (i4 != 0) {
                i3 = i4;
            }
            if (i3 > 0) {
                int[][] iArr3 = (int[][]) Array.newInstance(Integer.TYPE, cP.length + 1, 2);
                System.arraycopy(cP, 0, iArr3, 0, cP.length);
                iArr3[iArr3.length - 1][0] = i2;
                iArr3[iArr3.length - 1][1] = i3;
                cP = iArr3;
            }
        }
    }

    public static void a(int i2, boolean z) {
        if (cE == null) {
            int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, 1, 9);
            cE = iArr;
            iArr[0][0] = i2;
            int nextInt = as.nextInt() % ((ar[i2] / 10) + 1);
            if (z) {
                nextInt = 0;
            }
            cE[0][1] = nextInt + ar[i2];
            if (z) {
                cE[0][2] = 0;
                cE[0][3] = 0;
                cE[0][4] = 0;
                cE[0][5] = 0;
                cE[0][6] = 0;
                cE[0][7] = 0;
                cE[0][8] = 0;
                return;
            }
            int abs = Math.abs(as.nextInt() % 100);
            if (abs > 50) {
                cE[0][2] = 1;
            } else if (abs > 20) {
                cE[0][2] = 2;
            } else {
                cE[0][2] = 3;
            }
            int i3 = cE[0][2];
            for (int i4 = cq[i2]; i4 >= i3 && i3 > 0; i4--) {
                if (Math.abs(as.nextInt() % 100) < (i3 * 100) / i4) {
                    switch (i3) {
                        case 1:
                            cE[0][3] = cp[i2][i4 - 1];
                            cE[0][4] = Math.abs(as.nextInt() % ((cD[cp[i2][i4 - 1]] * 2) / cm[i2])) + 1;
                            break;
                        case 2:
                            cE[0][5] = cp[i2][i4 - 1];
                            cE[0][6] = Math.abs(as.nextInt() % ((cD[cp[i2][i4 - 1]] * 2) / cm[i2])) + 1;
                            break;
                        case 3:
                            cE[0][7] = cp[i2][i4 - 1];
                            cE[0][8] = Math.abs(as.nextInt() % ((cD[cp[i2][i4 - 1]] * 2) / cm[i2])) + 1;
                            break;
                    }
                    i3--;
                }
            }
        } else if (cE.length < 512) {
            int[][] iArr2 = (int[][]) Array.newInstance(Integer.TYPE, cE.length + 1, 9);
            System.arraycopy(cE, 0, iArr2, 0, cE.length);
            iArr2[iArr2.length - 1][0] = i2;
            iArr2[iArr2.length - 1][1] = (as.nextInt() % ((ar[i2] / 10) + 1)) + ar[i2];
            if (z) {
                iArr2[iArr2.length - 1][2] = 0;
                iArr2[iArr2.length - 1][3] = 0;
                iArr2[iArr2.length - 1][4] = 0;
                iArr2[iArr2.length - 1][5] = 0;
                iArr2[iArr2.length - 1][6] = 0;
                iArr2[iArr2.length - 1][7] = 0;
                iArr2[iArr2.length - 1][8] = 0;
            } else {
                int abs2 = Math.abs(as.nextInt() % 100);
                if (abs2 > 50) {
                    iArr2[iArr2.length - 1][2] = 1;
                } else if (abs2 > 20) {
                    iArr2[iArr2.length - 1][2] = 2;
                } else {
                    iArr2[iArr2.length - 1][2] = 3;
                }
                int i5 = iArr2[iArr2.length - 1][2];
                for (int i6 = cq[i2]; i6 >= i5 && i5 > 0; i6--) {
                    if (Math.abs(as.nextInt() % 100) < (i5 * 100) / i6) {
                        switch (i5) {
                            case 1:
                                iArr2[iArr2.length - 1][3] = cp[i2][i6 - 1];
                                iArr2[iArr2.length - 1][4] = Math.abs(as.nextInt() % ((cD[cp[i2][i6 - 1]] * 2) / cm[i2])) + 1;
                                break;
                            case 2:
                                iArr2[iArr2.length - 1][5] = cp[i2][i6 - 1];
                                iArr2[iArr2.length - 1][6] = Math.abs(as.nextInt() % ((cD[cp[i2][i6 - 1]] * 2) / cm[i2])) + 1;
                                break;
                            case 3:
                                iArr2[iArr2.length - 1][7] = cp[i2][i6 - 1];
                                iArr2[iArr2.length - 1][8] = Math.abs(as.nextInt() % ((cD[cp[i2][i6 - 1]] * 2) / cm[i2])) + 1;
                                break;
                        }
                        i5--;
                    }
                }
            }
            cE = iArr2;
        }
    }

    private static void a(ai aiVar, DataOutputStream dataOutputStream) {
        dataOutputStream.writeInt(aiVar.ac);
        dataOutputStream.writeInt(aiVar.p);
        dataOutputStream.writeInt(aiVar.ab);
        dataOutputStream.writeInt(aiVar.ds);
        dataOutputStream.writeInt(aiVar.e);
        dataOutputStream.writeInt(aiVar.ad);
        dataOutputStream.writeInt(aiVar.ae);
        dataOutputStream.writeInt(aiVar.aV);
        dataOutputStream.writeInt(aiVar.aW);
        dataOutputStream.writeInt(aiVar.aX);
        dataOutputStream.writeInt(aiVar.aY);
        dataOutputStream.writeInt(aiVar.aZ);
        dataOutputStream.writeInt(aiVar.ba);
        dataOutputStream.writeInt(aiVar.bb);
        dataOutputStream.writeInt(aiVar.bd);
        dataOutputStream.writeInt(aiVar.bc);
        dataOutputStream.writeInt(aiVar.bf);
        dataOutputStream.writeInt(aiVar.bg);
        dataOutputStream.writeBoolean(aiVar.aM);
    }

    public static void b(int i2, int i3, int i4) {
        ac = 0;
        switch (i3) {
            case 0:
                y(i2);
                j(i2, cK[i3]);
                break;
            case 1:
                y(i2);
                j(i2, cK[i3]);
                break;
            case 2:
                y(i2);
                j(i2, cK[i3]);
                break;
            case 3:
                y(i2);
                j(i2, cK[i3]);
                break;
            case 4:
                y(i2);
                j(i2, cK[i3]);
                break;
            case 5:
                y(i2);
                j(i2, cK[i3]);
                break;
            case 6:
                z(i2);
                k(i2, cK[i3]);
                break;
            case 7:
                z(i2);
                k(i2, cK[i3]);
                break;
            case 8:
                z(i2);
                k(i2, cK[i3]);
                break;
            case 9:
                z(i2);
                k(i2, cK[i3]);
                break;
            case 10:
                z(i2);
                k(i2, cK[i3]);
                break;
            case 11:
                z(i2);
                k(i2, cK[i3]);
                break;
            case 14:
                A(i2);
                j(i2, cK[i3]);
                k(i2, cK[i3]);
                break;
            case 15:
                A(i2);
                j(i2, cK[i3]);
                k(i2, cK[i3]);
                break;
        }
        if (ac == 0) {
            i(i3, 1);
        }
    }

    public static void b(int i2, boolean z) {
        if (cG == null) {
            int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, 1, 9);
            cG = iArr;
            iArr[0][0] = i2;
            cG[0][1] = (as.nextInt() % ((cv[i2] / 10) + 1)) + cv[i2];
            if (z) {
                cG[0][2] = 0;
                cG[0][3] = 0;
                cG[0][4] = 0;
                cG[0][5] = 0;
                cG[0][6] = 0;
                cG[0][7] = 0;
                cG[0][8] = 0;
                return;
            }
            int abs = Math.abs(as.nextInt() % 100);
            if (abs > 50) {
                cG[0][2] = 1;
            } else if (abs > 20) {
                cG[0][2] = 2;
            } else {
                cG[0][2] = 3;
            }
            int i3 = cG[0][2];
            for (int i4 = cA[i2]; i4 >= i3 && i3 > 0; i4--) {
                if (Math.abs(as.nextInt() % 100) < (i3 * 100) / i4) {
                    switch (i3) {
                        case 1:
                            cG[0][3] = cz[i2][i4 - 1];
                            cG[0][4] = Math.abs(as.nextInt() % (cD[cz[i2][i4 - 1]] / cw[i2])) + 1;
                            break;
                        case 2:
                            cG[0][5] = cz[i2][i4 - 1];
                            cG[0][6] = Math.abs(as.nextInt() % (cD[cz[i2][i4 - 1]] / cw[i2])) + 1;
                            break;
                        case 3:
                            cG[0][7] = cz[i2][i4 - 1];
                            cG[0][8] = Math.abs(as.nextInt() % (cD[cz[i2][i4 - 1]] / cw[i2])) + 1;
                            break;
                    }
                    i3--;
                }
            }
        } else if (cG.length < 512) {
            int[][] iArr2 = (int[][]) Array.newInstance(Integer.TYPE, cG.length + 1, 9);
            System.arraycopy(cG, 0, iArr2, 0, cG.length);
            iArr2[iArr2.length - 1][0] = i2;
            iArr2[iArr2.length - 1][1] = (as.nextInt() % ((cv[i2] / 10) + 1)) + cv[i2];
            if (z) {
                iArr2[iArr2.length - 1][2] = 0;
                iArr2[iArr2.length - 1][3] = 0;
                iArr2[iArr2.length - 1][4] = 0;
                iArr2[iArr2.length - 1][5] = 0;
                iArr2[iArr2.length - 1][6] = 0;
                iArr2[iArr2.length - 1][7] = 0;
                iArr2[iArr2.length - 1][8] = 0;
            } else {
                int abs2 = Math.abs(as.nextInt() % 100);
                if (abs2 > 50) {
                    iArr2[iArr2.length - 1][2] = 1;
                } else if (abs2 > 20) {
                    iArr2[iArr2.length - 1][2] = 2;
                } else {
                    iArr2[iArr2.length - 1][2] = 3;
                }
                int i5 = iArr2[iArr2.length - 1][2];
                for (int i6 = cA[i2]; i6 >= i5 && i5 > 0; i6--) {
                    if (Math.abs(as.nextInt() % 100) < (i5 * 100) / i6) {
                        switch (i5) {
                            case 1:
                                iArr2[iArr2.length - 1][3] = cz[i2][i6 - 1];
                                iArr2[iArr2.length - 1][4] = Math.abs(as.nextInt() % (cD[cz[i2][i6 - 1]] / cw[i2])) + 1;
                                break;
                            case 2:
                                iArr2[iArr2.length - 1][5] = cz[i2][i6 - 1];
                                iArr2[iArr2.length - 1][6] = Math.abs(as.nextInt() % (cD[cz[i2][i6 - 1]] / cw[i2])) + 1;
                                break;
                            case 3:
                                iArr2[iArr2.length - 1][7] = cz[i2][i6 - 1];
                                iArr2[iArr2.length - 1][8] = Math.abs(as.nextInt() % (cD[cz[i2][i6 - 1]] / cw[i2])) + 1;
                                break;
                        }
                        i5--;
                    }
                }
            }
            cG = iArr2;
        }
    }

    private static void c(int i2, int i3, int i4) {
        int i5 = 0;
        if (i2 == 0) {
            for (int i6 = 0; i6 < cE[aR[i3][i4]][2]; i6++) {
                switch (i6) {
                    case 0:
                        i5 = cE[aR[i3][i4]][3];
                        break;
                    case 1:
                        i5 = cE[aR[i3][i4]][5];
                        break;
                    case 2:
                        i5 = cE[aR[i3][i4]][7];
                        break;
                }
                switch (i5) {
                    case 0:
                        switch (i6) {
                            case 0:
                                bF[i3].dD += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dD += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dD += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 1:
                        switch (i6) {
                            case 0:
                                bF[i3].dE += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dE += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dE += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 2:
                        switch (i6) {
                            case 0:
                                bF[i3].dG += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dG += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dG += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 3:
                        switch (i6) {
                            case 0:
                                bF[i3].dH += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dH += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dH += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 4:
                        switch (i6) {
                            case 0:
                                bF[i3].dN += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dN += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dN += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 5:
                        switch (i6) {
                            case 0:
                                bF[i3].dO += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dO += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dO += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 6:
                        switch (i6) {
                            case 0:
                                bF[i3].dP += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dP += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dP += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 7:
                        switch (i6) {
                            case 0:
                                bF[i3].dQ += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dQ += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dQ += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 8:
                        switch (i6) {
                            case 0:
                                bF[i3].dR += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dR += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dR += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 9:
                        switch (i6) {
                            case 0:
                                bF[i3].eh += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].eh += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].eh += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 10:
                        switch (i6) {
                            case 0:
                                bF[i3].ej += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].ej += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].ej += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case c.INT_16:
                        switch (i6) {
                            case 0:
                                bF[i3].dv += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dv += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dv += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 21:
                        switch (i6) {
                            case 0:
                                bF[i3].dw += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dw += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dw += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 22:
                        switch (i6) {
                            case 0:
                                bF[i3].dy += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dy += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dy += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 23:
                        switch (i6) {
                            case 0:
                                bF[i3].dx += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dx += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dx += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case c.UUID:
                        switch (i6) {
                            case 0:
                                bF[i3].dA += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dA += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dA += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 25:
                        switch (i6) {
                            case 0:
                                bF[i3].dB += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dB += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dB += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 26:
                        switch (i6) {
                            case 0:
                                bF[i3].dt += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].dt += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].dt += cE[aR[i3][i4]][8];
                                continue;
                        }
                    case 27:
                        switch (i6) {
                            case 0:
                                bF[i3].du += cE[aR[i3][i4]][4];
                                continue;
                            case 1:
                                bF[i3].du += cE[aR[i3][i4]][6];
                                continue;
                            case 2:
                                bF[i3].du += cE[aR[i3][i4]][8];
                                continue;
                        }
                }
            }
            return;
        }
        for (int i7 = 0; i7 < cG[aR[i3][i4]][2]; i7++) {
            switch (i7) {
                case 0:
                    i5 = cG[aR[i3][i4]][3];
                    break;
                case 1:
                    i5 = cG[aR[i3][i4]][5];
                    break;
                case 2:
                    i5 = cG[aR[i3][i4]][7];
                    break;
            }
            switch (i5) {
                case c.INT_16:
                    switch (i7) {
                        case 0:
                            bF[i3].dv += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].dv += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].dv += cG[aR[i3][i4]][8];
                            continue;
                    }
                case 21:
                    switch (i7) {
                        case 0:
                            bF[i3].dw += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].dw += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].dw += cG[aR[i3][i4]][8];
                            continue;
                    }
                case 22:
                    switch (i7) {
                        case 0:
                            bF[i3].dy += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].dy += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].dy += cG[aR[i3][i4]][8];
                            continue;
                    }
                case 23:
                    switch (i7) {
                        case 0:
                            bF[i3].dx += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].dx += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].dx += cG[aR[i3][i4]][8];
                            continue;
                    }
                case c.UUID:
                    switch (i7) {
                        case 0:
                            bF[i3].dA += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].dA += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].dA += cG[aR[i3][i4]][8];
                            continue;
                    }
                case 25:
                    switch (i7) {
                        case 0:
                            bF[i3].dB += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].dB += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].dB += cG[aR[i3][i4]][8];
                            continue;
                    }
                case 26:
                    switch (i7) {
                        case 0:
                            bF[i3].dt += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].dt += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].dt += cG[aR[i3][i4]][8];
                            continue;
                    }
                case 27:
                    switch (i7) {
                        case 0:
                            bF[i3].du += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].du += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].du += cG[aR[i3][i4]][8];
                            continue;
                    }
                case 28:
                    switch (i7) {
                        case 0:
                            bF[i3].ek += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].ek += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].ek += cG[aR[i3][i4]][8];
                            continue;
                    }
                case 29:
                    switch (i7) {
                        case 0:
                            bF[i3].el += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].el += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].el += cG[aR[i3][i4]][8];
                            continue;
                    }
                case 30:
                    switch (i7) {
                        case 0:
                            bF[i3].em += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].em += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].em += cG[aR[i3][i4]][8];
                            continue;
                    }
                case 31:
                    switch (i7) {
                        case 0:
                            bF[i3].en += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].en += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].en += cG[aR[i3][i4]][8];
                            continue;
                    }
                case 32:
                    switch (i7) {
                        case 0:
                            bF[i3].eo += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].eo += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].eo += cG[aR[i3][i4]][8];
                            continue;
                    }
                case CharsToNameCanonicalizer.HASH_MULT:
                    switch (i7) {
                        case 0:
                            bF[i3].et += cG[aR[i3][i4]][4];
                            continue;
                        case 1:
                            bF[i3].et += cG[aR[i3][i4]][6];
                            continue;
                        case 2:
                            bF[i3].et += cG[aR[i3][i4]][8];
                            continue;
                    }
            }
        }
    }

    public static void c(int i2, boolean z) {
        f fVar = new f("/data/mission.bin");
        ao aoVar = new ao();
        r c = fVar.c(i2);
        aoVar.o = c.i(0);
        aoVar.r = c.i(1);
        if (!z) {
            cR = ah.b(cR, i2);
        }
        if (cQ == null) {
            String[][] strArr = (String[][]) Array.newInstance(String.class, 1, 2);
            cQ = strArr;
            strArr[0][0] = aoVar.o;
            cQ[0][1] = aoVar.r;
            return;
        }
        String[][] strArr2 = (String[][]) Array.newInstance(String.class, cQ.length + 1, 2);
        System.arraycopy(cQ, 0, strArr2, 0, cQ.length);
        strArr2[strArr2.length - 1][0] = aoVar.o;
        strArr2[strArr2.length - 1][1] = aoVar.r;
        cQ = strArr2;
    }

    public static int e(int i2, int i3) {
        return (((i3 - 1) * i2) / 10) + i2;
    }

    private void e() {
        f fVar = new f(new StringBuffer().append("/event/m").append(this.ap.e).toString());
        fVar.b();
        int p2 = fVar.p();
        for (int i2 = 0; i2 < p2; i2++) {
            r c = fVar.c(i2);
            int i3 = c.aq[1];
            int i4 = c.aq[2];
            int i5 = c.aq[3];
            int i6 = c.aq[4];
            String i7 = c.i(5);
            int i8 = c.aq[8];
            int i9 = c.aq[9];
            int i10 = c.aq[10];
            if (i7 == null || i7.length() == 0) {
                aq aqVar = new aq(i3, i4, i5, i6, i2);
                aqVar.k = true;
                aqVar.aj = (byte) c.aq[7];
                if (i8 != -1) {
                    switch (i9) {
                        case 1:
                            if (this.aH.s()[i8] != i10) {
                                aqVar.k = false;
                                break;
                            }
                            break;
                        case 2:
                            if (this.aH.s()[i8] == i10) {
                                aqVar.k = false;
                                break;
                            }
                            break;
                        case 3:
                            if (this.aH.s()[i8] >= i10) {
                                aqVar.k = false;
                                break;
                            }
                            break;
                        case 4:
                            if (this.aH.s()[i8] > i10) {
                                aqVar.k = false;
                                break;
                            }
                            break;
                        case 5:
                            if (this.aH.s()[i8] <= i10) {
                                aqVar.k = false;
                                break;
                            }
                            break;
                        case 6:
                            if (this.aH.s()[i8] < i10) {
                                aqVar.k = false;
                                break;
                            }
                            break;
                    }
                }
                au.addElement(aqVar);
            } else {
                ai aiVar = new ai();
                int indexOf = i7.indexOf("xml");
                if (indexOf != -1) {
                    i7 = new StringBuffer().append(i7.substring(0, indexOf)).append("ani").toString();
                    aiVar.aK = true;
                }
                String str = i7;
                if (str.indexOf("png") != -1) {
                    aiVar.aK = false;
                    aiVar.bR = true;
                }
                aiVar.p = i3;
                aiVar.ab = i4;
                aiVar.op = new aq((-i5) >> 1, -i6, i5, i6, i2);
                aiVar.b = 0;
                aiVar.a = 1;
                aiVar.am = true;
                aiVar.k = true;
                aiVar.aj = (byte) c.aq[7];
                aiVar.h = c.aq[6];
                if (aiVar.h == 3) {
                    aiVar.oq = new aq(-64, -64, 128, 128);
                }
                if (aiVar.h == 4) {
                    aiVar.oq = new aq(-32, -32, 64, 64);
                }
                if (aiVar.aK) {
                    aiVar.t = new StringBuffer().append(o).append(str).toString();
                    aiVar.u = o;
                } else {
                    aiVar.u = new StringBuffer().append(o).append(str).toString();
                }
                if (i8 != -1) {
                    switch (i9) {
                        case 1:
                            if (this.aH.s()[i8] != i10) {
                                aiVar.k = false;
                                break;
                            }
                            break;
                        case 2:
                            if (this.aH.s()[i8] == i10) {
                                aiVar.k = false;
                                break;
                            }
                            break;
                        case 3:
                            if (this.aH.s()[i8] >= i10) {
                                aiVar.k = false;
                                break;
                            }
                            break;
                        case 4:
                            if (this.aH.s()[i8] > i10) {
                                aiVar.k = false;
                                break;
                            }
                            break;
                        case 5:
                            if (this.aH.s()[i8] <= i10) {
                                aiVar.k = false;
                                break;
                            }
                            break;
                        case 6:
                            if (this.aH.s()[i8] < i10) {
                                aiVar.k = false;
                                break;
                            }
                            break;
                    }
                }
                aS.addElement(aiVar);
            }
        }
    }

    public static int f(int i2) {
        int i3 = 0;
        int i4 = 0;
        while (cP != null && i3 < cP.length) {
            if (cP[i3] != null && cP[i3][0] == i2) {
                i4 += cP[i3][1];
            }
            i3++;
        }
        return i4;
    }

    public static void g(int i2) {
        if (aa.X != null) {
            cE = ah.a(cE, aa.X[i2]);
            for (int i3 = 0; i3 < aa.X.length; i3++) {
                if (aa.X[i3] > aa.X[i2]) {
                    int[] iArr = aa.X;
                    iArr[i3] = iArr[i3] - 1;
                }
            }
            for (int i4 = 0; i4 < bF.length; i4++) {
                if (aR[i4][4] > aa.X[i2]) {
                    int[] iArr2 = aR[i4];
                    iArr2[4] = iArr2[4] - 1;
                }
            }
            aa.X = ah.c(aa.X, i2);
        }
    }

    public static void i(int i2, int i3) {
        if (cP != null) {
            int i4 = 0;
            int i5 = i3;
            while (i5 > 0 && i4 < cP.length) {
                if (cP[i4][0] == i2) {
                    if (cP[i4][1] >= i5) {
                        int[] iArr = cP[i4];
                        iArr[1] = iArr[1] - i5;
                        i5 = 0;
                    } else {
                        i5 -= cP[i4][1];
                        cP[i4][1] = 0;
                    }
                }
                i4++;
            }
            for (int length = cP.length - 1; length >= 0; length--) {
                if (cP[length][1] <= 0) {
                    cP = ah.a(cP, length);
                }
            }
        }
    }

    public static void j(int i2, int i3) {
        bF[i2].aW += i3;
        if (bF[i2].aW > bF[i2].aX + bF[i2].dt) {
            bF[i2].aW = bF[i2].aX + bF[i2].dt;
        }
    }

    public static int k(int i2) {
        return ((i2 + 1) * (i2 + 1)) << 2;
    }

    public static void k(int i2, int i3) {
        bF[i2].aY += i3;
        if (bF[i2].aY > bF[i2].aZ + bF[i2].du) {
            bF[i2].aY = bF[i2].aZ + bF[i2].du;
        }
    }

    public static ai v(int i2) {
        r c = new f("/data/role.bin").c(i2);
        ai aiVar = new ai();
        aiVar.ac = i2;
        aiVar.o = c.i(0);
        aiVar.r = c.i(1);
        aiVar.s = c.i(2);
        aiVar.t = c.i(3);
        int indexOf = aiVar.s.indexOf("xml");
        if (indexOf != -1) {
            aiVar.s = aiVar.s.substring(0, indexOf);
            aiVar.s = new StringBuffer().append(aiVar.s).append("ani").toString();
        }
        aiVar.s = new StringBuffer().append(r).append(aiVar.s).toString();
        int indexOf2 = aiVar.t.indexOf("xml");
        if (indexOf2 != -1) {
            aiVar.t = aiVar.t.substring(0, indexOf2);
            aiVar.t = new StringBuffer().append(aiVar.t).append("ani").toString();
        }
        aiVar.t = new StringBuffer().append(o).append(aiVar.t).toString();
        String str = aiVar.r;
        int indexOf3 = str.indexOf("png");
        if (indexOf3 != -1) {
            str = new StringBuffer().append(str.substring(0, indexOf3)).append("ps").toString();
        }
        aiVar.r = str;
        aiVar.ad = c.aq[4];
        aiVar.ae = c.aq[5];
        aiVar.aV = ((aiVar.ad + 1) * (aiVar.ad + 1)) << 2;
        aiVar.da = c.aq[6];
        aiVar.aW = e(aiVar.da, aiVar.ad);
        aiVar.aX = aiVar.aW;
        aiVar.x = c.aq[7];
        aiVar.aY = e(aiVar.x, aiVar.ad);
        aiVar.aZ = aiVar.aY;
        aiVar.y = c.aq[8];
        aiVar.z = c.aq[9];
        aiVar.db = c.aq[11];
        aiVar.bd = c.aq[10];
        aiVar.bf = c.aq[12];
        aiVar.bg = c.aq[18];
        aiVar.ba = e(aiVar.y, aiVar.ad);
        aiVar.bb = e(aiVar.z, aiVar.ad);
        aiVar.bc = e(aiVar.db, aiVar.ad);
        aiVar.dc = c.aq[14];
        aiVar.dr = c.aq[15];
        aiVar.ds = c.aq[16];
        aiVar.X = c.j(19);
        return aiVar;
    }

    public static void w(int i2) {
        if (aa.Z != null) {
            cG = ah.a(cG, aa.Z[i2]);
            for (int i3 = 0; i3 < aa.Z.length; i3++) {
                if (aa.Z[i3] > aa.Z[i2]) {
                    int[] iArr = aa.Z;
                    iArr[i3] = iArr[i3] - 1;
                }
            }
            for (int i4 = 0; i4 < bF.length; i4++) {
                for (int i5 = 0; i5 < 9; i5++) {
                    if (i5 != 4 && aR[i4][i5] > aa.Z[i2]) {
                        int[] iArr2 = aR[i4];
                        iArr2[i5] = iArr2[i5] - 1;
                    }
                }
            }
            aa.Z = ah.c(aa.Z, i2);
        }
    }

    public static void x(int i2) {
        if (cP != null) {
            for (int i3 = 0; i3 < cP.length; i3++) {
                if (i3 == i2) {
                    cP = ah.a(cP, i3);
                    return;
                }
            }
        }
    }

    private static void y(int i2) {
        if (bF[i2].aW == bF[i2].aX + bF[i2].dt) {
            ac = 1;
        }
    }

    private static void z(int i2) {
        if (bF[i2].aY == bF[i2].aZ + bF[i2].du) {
            ac = 2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:161:0x03da A[SYNTHETIC, Splitter:B:161:0x03da] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void C(int r7) {
        /*
            r6 = this;
            r2 = 0
            r3 = 0
            ab r0 = new ab     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r0.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r6.bH = r0     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r6.J     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r0.a(r1)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r1.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r4 = r6.V     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r0 = r3
        L_0x002b:
            r4 = 128(0x80, float:1.794E-43)
            if (r0 >= r4) goto L_0x003d
            j r4 = r6.aH     // Catch:{ Exception -> 0x03e5 }
            short[] r4 = r4.s()     // Catch:{ Exception -> 0x03e5 }
            short r4 = r4[r0]     // Catch:{ Exception -> 0x03e5 }
            r1.writeShort(r4)     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0 + 1
            goto L_0x002b
        L_0x003d:
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03e5 }
            r4.<init>()     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r5 = r6.V     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x03e5 }
            r0.p(r4)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r1.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r4 = r6.W     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            ak r0 = r6.ap     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.e     // Catch:{ Exception -> 0x03e5 }
            r1.writeShort(r0)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03e5 }
            r4.<init>()     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r5 = r6.W     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x03e5 }
            r0.p(r4)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r1.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r4 = r6.bI     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            ai[] r0 = defpackage.y.bF     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
            r1.writeShort(r0)     // Catch:{ Exception -> 0x03e5 }
            r0 = r3
        L_0x00af:
            ai[] r4 = defpackage.y.bF     // Catch:{ Exception -> 0x03e5 }
            int r4 = r4.length     // Catch:{ Exception -> 0x03e5 }
            if (r0 >= r4) goto L_0x00be
            ai[] r4 = defpackage.y.bF     // Catch:{ Exception -> 0x03e5 }
            r4 = r4[r0]     // Catch:{ Exception -> 0x03e5 }
            a(r4, r1)     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0 + 1
            goto L_0x00af
        L_0x00be:
            int r0 = defpackage.y.ab     // Catch:{ Exception -> 0x03e5 }
            r1.writeInt(r0)     // Catch:{ Exception -> 0x03e5 }
            int r0 = defpackage.y.aa     // Catch:{ Exception -> 0x03e5 }
            r1.writeInt(r0)     // Catch:{ Exception -> 0x03e5 }
            boolean r0 = r6.bV     // Catch:{ Exception -> 0x03e5 }
            r1.writeBoolean(r0)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03e5 }
            r4.<init>()     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r5 = r6.bI     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x03e5 }
            r0.p(r4)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r1.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r4 = r6.bJ     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.util.Vector r0 = r6.aT     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x03e5 }
            r1.writeShort(r0)     // Catch:{ Exception -> 0x03e5 }
            r4 = r3
        L_0x0109:
            java.util.Vector r0 = r6.aT     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x03e5 }
            if (r4 >= r0) goto L_0x0120
            java.util.Vector r0 = r6.aT     // Catch:{ Exception -> 0x03e5 }
            java.lang.Object r0 = r0.elementAt(r4)     // Catch:{ Exception -> 0x03e5 }
            ai r0 = (defpackage.ai) r0     // Catch:{ Exception -> 0x03e5 }
            a(r0, r1)     // Catch:{ Exception -> 0x03e5 }
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0109
        L_0x0120:
            int r0 = defpackage.aa.h     // Catch:{ Exception -> 0x03e5 }
            r1.writeInt(r0)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03e5 }
            r4.<init>()     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r5 = r6.bJ     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x03e5 }
            r0.p(r4)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r1.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r4 = r6.bK     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            int[][] r0 = defpackage.y.cE     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x017f
            int[][] r0 = defpackage.y.cE     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
        L_0x015e:
            r1.writeInt(r0)     // Catch:{ Exception -> 0x03e5 }
            r4 = r3
        L_0x0162:
            int[][] r0 = defpackage.y.cE     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x0185
            int[][] r0 = defpackage.y.cE     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
            if (r4 >= r0) goto L_0x0185
            r0 = r3
        L_0x016c:
            int[][] r5 = defpackage.y.cE     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            int r5 = r5.length     // Catch:{ Exception -> 0x03e5 }
            if (r0 >= r5) goto L_0x0181
            int[][] r5 = defpackage.y.cE     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r0]     // Catch:{ Exception -> 0x03e5 }
            r1.writeInt(r5)     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0 + 1
            goto L_0x016c
        L_0x017f:
            r0 = r3
            goto L_0x015e
        L_0x0181:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0162
        L_0x0185:
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03e5 }
            r4.<init>()     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r5 = r6.bK     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x03e5 }
            r0.p(r4)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r1.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r4 = r6.bL     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            int[][] r0 = defpackage.y.cG     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x01df
            int[][] r0 = defpackage.y.cG     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
        L_0x01be:
            r1.writeInt(r0)     // Catch:{ Exception -> 0x03e5 }
            r4 = r3
        L_0x01c2:
            int[][] r0 = defpackage.y.cG     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x01e5
            int[][] r0 = defpackage.y.cG     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
            if (r4 >= r0) goto L_0x01e5
            r0 = r3
        L_0x01cc:
            int[][] r5 = defpackage.y.cG     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            int r5 = r5.length     // Catch:{ Exception -> 0x03e5 }
            if (r0 >= r5) goto L_0x01e1
            int[][] r5 = defpackage.y.cG     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r0]     // Catch:{ Exception -> 0x03e5 }
            r1.writeInt(r5)     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0 + 1
            goto L_0x01cc
        L_0x01df:
            r0 = r3
            goto L_0x01be
        L_0x01e1:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x01c2
        L_0x01e5:
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03e5 }
            r4.<init>()     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r5 = r6.bL     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x03e5 }
            r0.p(r4)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r1.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r4 = r6.bM     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            int[][] r0 = defpackage.y.cP     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x023f
            int[][] r0 = defpackage.y.cP     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
        L_0x021e:
            r1.writeInt(r0)     // Catch:{ Exception -> 0x03e5 }
            r4 = r3
        L_0x0222:
            int[][] r0 = defpackage.y.cP     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x0245
            int[][] r0 = defpackage.y.cP     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
            if (r4 >= r0) goto L_0x0245
            r0 = r3
        L_0x022c:
            int[][] r5 = defpackage.y.cP     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            int r5 = r5.length     // Catch:{ Exception -> 0x03e5 }
            if (r0 >= r5) goto L_0x0241
            int[][] r5 = defpackage.y.cP     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r0]     // Catch:{ Exception -> 0x03e5 }
            r1.writeInt(r5)     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0 + 1
            goto L_0x022c
        L_0x023f:
            r0 = r3
            goto L_0x021e
        L_0x0241:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0222
        L_0x0245:
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03e5 }
            r4.<init>()     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r5 = r6.bM     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x03e5 }
            r0.p(r4)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r1.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r4 = r6.bP     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            int[][] r0 = defpackage.y.cF     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x029f
            int[][] r0 = defpackage.y.cF     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
        L_0x027e:
            r1.writeInt(r0)     // Catch:{ Exception -> 0x03e5 }
            r4 = r3
        L_0x0282:
            int[][] r0 = defpackage.y.cF     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x02a5
            int[][] r0 = defpackage.y.cF     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
            if (r4 >= r0) goto L_0x02a5
            r0 = r3
        L_0x028c:
            int[][] r5 = defpackage.y.cF     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            int r5 = r5.length     // Catch:{ Exception -> 0x03e5 }
            if (r0 >= r5) goto L_0x02a1
            int[][] r5 = defpackage.y.cF     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r0]     // Catch:{ Exception -> 0x03e5 }
            r1.writeInt(r5)     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0 + 1
            goto L_0x028c
        L_0x029f:
            r0 = r3
            goto L_0x027e
        L_0x02a1:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0282
        L_0x02a5:
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03e5 }
            r4.<init>()     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r5 = r6.bP     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x03e5 }
            r0.p(r4)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r1.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r4 = r6.bN     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            int[][] r0 = defpackage.y.aR     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x02ff
            int[][] r0 = defpackage.y.aR     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
        L_0x02de:
            r1.writeInt(r0)     // Catch:{ Exception -> 0x03e5 }
            r4 = r3
        L_0x02e2:
            int[][] r0 = defpackage.y.aR     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x0305
            int[][] r0 = defpackage.y.aR     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
            if (r4 >= r0) goto L_0x0305
            r0 = r3
        L_0x02ec:
            int[][] r5 = defpackage.y.aR     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            int r5 = r5.length     // Catch:{ Exception -> 0x03e5 }
            if (r0 >= r5) goto L_0x0301
            int[][] r5 = defpackage.y.aR     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r0]     // Catch:{ Exception -> 0x03e5 }
            r1.writeInt(r5)     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0 + 1
            goto L_0x02ec
        L_0x02ff:
            r0 = r3
            goto L_0x02de
        L_0x0301:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x02e2
        L_0x0305:
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03e5 }
            r4.<init>()     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r5 = r6.bN     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x03e5 }
            r0.p(r4)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r1.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r4 = r6.bO     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = r3
        L_0x0338:
            int[][] r0 = defpackage.y.ch     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x0359
            int[][] r0 = defpackage.y.ch     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
            if (r4 >= r0) goto L_0x0359
            r0 = r3
        L_0x0342:
            int[][] r5 = defpackage.y.ch     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            int r5 = r5.length     // Catch:{ Exception -> 0x03e5 }
            if (r0 >= r5) goto L_0x0355
            int[][] r5 = defpackage.y.ch     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r4]     // Catch:{ Exception -> 0x03e5 }
            r5 = r5[r0]     // Catch:{ Exception -> 0x03e5 }
            r1.writeInt(r5)     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0 + 1
            goto L_0x0342
        L_0x0355:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0338
        L_0x0359:
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03e5 }
            r4.<init>()     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r5 = r6.bO     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x03e5 }
            r0.p(r4)     // Catch:{ Exception -> 0x03e5 }
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r1.<init>()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r4 = r6.bQ     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            r4 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r4)     // Catch:{ Exception -> 0x03c6, all -> 0x03d6 }
            int[] r0 = defpackage.y.cR     // Catch:{ Exception -> 0x03e5 }
            if (r0 == 0) goto L_0x03a9
            int[] r0 = defpackage.y.cR     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0.length     // Catch:{ Exception -> 0x03e5 }
        L_0x0392:
            r1.writeInt(r0)     // Catch:{ Exception -> 0x03e5 }
            r0 = r3
        L_0x0396:
            int[] r3 = defpackage.y.cR     // Catch:{ Exception -> 0x03e5 }
            if (r3 == 0) goto L_0x03ab
            int[] r3 = defpackage.y.cR     // Catch:{ Exception -> 0x03e5 }
            int r3 = r3.length     // Catch:{ Exception -> 0x03e5 }
            if (r0 >= r3) goto L_0x03ab
            int[] r3 = defpackage.y.cR     // Catch:{ Exception -> 0x03e5 }
            r3 = r3[r0]     // Catch:{ Exception -> 0x03e5 }
            r1.writeInt(r3)     // Catch:{ Exception -> 0x03e5 }
            int r0 = r0 + 1
            goto L_0x0396
        L_0x03a9:
            r0 = r3
            goto L_0x0392
        L_0x03ab:
            ab r0 = r6.bH     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x03e5 }
            r3.<init>()     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r4 = r6.bQ     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r3 = r3.append(r4)     // Catch:{ Exception -> 0x03e5 }
            java.lang.StringBuffer r3 = r3.append(r7)     // Catch:{ Exception -> 0x03e5 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x03e5 }
            r0.p(r3)     // Catch:{ Exception -> 0x03e5 }
        L_0x03c3:
            r6.bH = r2
            return
        L_0x03c6:
            r0 = move-exception
            r1 = r2
        L_0x03c8:
            r0.printStackTrace()     // Catch:{ all -> 0x03e3 }
            if (r1 == 0) goto L_0x03c3
            r1.close()     // Catch:{ Exception -> 0x03d1 }
            goto L_0x03c3
        L_0x03d1:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x03c3
        L_0x03d6:
            r0 = move-exception
            r1 = r2
        L_0x03d8:
            if (r1 == 0) goto L_0x03dd
            r1.close()     // Catch:{ Exception -> 0x03de }
        L_0x03dd:
            throw r0
        L_0x03de:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x03dd
        L_0x03e3:
            r0 = move-exception
            goto L_0x03d8
        L_0x03e5:
            r0 = move-exception
            goto L_0x03c8
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.y.C(int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:161:0x0440 A[SYNTHETIC, Splitter:B:161:0x0440] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void D(int r8) {
        /*
            r7 = this;
            r2 = 0
            r3 = 0
            r0 = 1
            defpackage.y.aV = r0
            ab r0 = new ab     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r0.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r7.bH = r0     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r7.J     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r0.a(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r1.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r4 = r7.V     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r8)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r0 = r3
        L_0x002d:
            r4 = 128(0x80, float:1.794E-43)
            if (r0 >= r4) goto L_0x0040
            j r4 = r7.aH     // Catch:{ Exception -> 0x044b }
            short[] r4 = r4.s()     // Catch:{ Exception -> 0x044b }
            short r5 = r1.readShort()     // Catch:{ Exception -> 0x044b }
            r4[r0] = r5     // Catch:{ Exception -> 0x044b }
            int r0 = r0 + 1
            goto L_0x002d
        L_0x0040:
            r1.close()     // Catch:{ Exception -> 0x044b }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r1.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r4 = r7.W     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r8)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            short r0 = r1.readShort()     // Catch:{ Exception -> 0x044b }
            r7.ae = r0     // Catch:{ Exception -> 0x044b }
            r1.close()     // Catch:{ Exception -> 0x044b }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r1.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r4 = r7.bI     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r8)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            short r0 = r1.readShort()     // Catch:{ Exception -> 0x044b }
            byte r4 = (byte) r0     // Catch:{ Exception -> 0x044b }
            r0 = 0
            defpackage.y.bF = r0     // Catch:{ Exception -> 0x044b }
            r0 = r3
        L_0x0087:
            if (r0 >= r4) goto L_0x010a
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            ai r6 = v(r5)     // Catch:{ Exception -> 0x044b }
            if (r5 != 0) goto L_0x0095
            r7.bE = r6     // Catch:{ Exception -> 0x044b }
        L_0x0095:
            r6.ac = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.p = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.ab = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.ds = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.e = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.ad = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.ae = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.aV = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.aW = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.aX = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.aY = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.aZ = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.ba = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.bb = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.bd = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.bc = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.bf = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.bg = r5     // Catch:{ Exception -> 0x044b }
            boolean r5 = r1.readBoolean()     // Catch:{ Exception -> 0x044b }
            r6.aM = r5     // Catch:{ Exception -> 0x044b }
            r7.a(r6)     // Catch:{ Exception -> 0x044b }
            int r0 = r0 + 1
            goto L_0x0087
        L_0x010a:
            int r0 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            defpackage.y.ab = r0     // Catch:{ Exception -> 0x044b }
            int r0 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            defpackage.y.aa = r0     // Catch:{ Exception -> 0x044b }
            boolean r0 = r1.readBoolean()     // Catch:{ Exception -> 0x044b }
            r7.bV = r0     // Catch:{ Exception -> 0x044b }
            r1.close()     // Catch:{ Exception -> 0x044b }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r1.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r4 = r7.bJ     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r8)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            short r0 = r1.readShort()     // Catch:{ Exception -> 0x044b }
            byte r4 = (byte) r0     // Catch:{ Exception -> 0x044b }
            java.util.Vector r0 = r7.aT     // Catch:{ Exception -> 0x044b }
            r0.removeAllElements()     // Catch:{ Exception -> 0x044b }
            r0 = r3
        L_0x0143:
            if (r0 >= r4) goto L_0x01c8
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            ai r6 = v(r5)     // Catch:{ Exception -> 0x044b }
            if (r5 != 0) goto L_0x0151
            r7.bE = r6     // Catch:{ Exception -> 0x044b }
        L_0x0151:
            r6.ac = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.p = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.ab = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.ds = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.e = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.ad = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.ae = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.aV = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.aW = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.aX = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.aY = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.aZ = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.ba = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.bb = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.bd = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.bc = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.bf = r5     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r6.bg = r5     // Catch:{ Exception -> 0x044b }
            boolean r5 = r1.readBoolean()     // Catch:{ Exception -> 0x044b }
            r6.aM = r5     // Catch:{ Exception -> 0x044b }
            java.util.Vector r5 = r7.aT     // Catch:{ Exception -> 0x044b }
            r5.addElement(r6)     // Catch:{ Exception -> 0x044b }
            int r0 = r0 + 1
            goto L_0x0143
        L_0x01c8:
            int r0 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            defpackage.aa.h = r0     // Catch:{ Exception -> 0x044b }
            r0 = 0
            defpackage.aa.h = r0     // Catch:{ Exception -> 0x044b }
            r0 = r3
        L_0x01d2:
            ai[] r4 = defpackage.y.bF     // Catch:{ Exception -> 0x044b }
            int r4 = r4.length     // Catch:{ Exception -> 0x044b }
            if (r0 >= r4) goto L_0x01e8
            ai[] r4 = defpackage.y.bF     // Catch:{ Exception -> 0x044b }
            r4 = r4[r0]     // Catch:{ Exception -> 0x044b }
            boolean r4 = r4.aM     // Catch:{ Exception -> 0x044b }
            if (r4 == 0) goto L_0x01e5
            int r4 = defpackage.aa.h     // Catch:{ Exception -> 0x044b }
            int r4 = r4 + 1
            defpackage.aa.h = r4     // Catch:{ Exception -> 0x044b }
        L_0x01e5:
            int r0 = r0 + 1
            goto L_0x01d2
        L_0x01e8:
            r1.close()     // Catch:{ Exception -> 0x044b }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r1.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r4 = r7.bK     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r8)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            int r0 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            if (r0 != 0) goto L_0x02cb
            r0 = 0
            defpackage.y.cE = r0     // Catch:{ Exception -> 0x044b }
        L_0x020d:
            r1.close()     // Catch:{ Exception -> 0x044b }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r1.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r4 = r7.bL     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r8)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            int r0 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            if (r0 != 0) goto L_0x02fe
            r0 = 0
            defpackage.y.cG = r0     // Catch:{ Exception -> 0x044b }
        L_0x0232:
            r1.close()     // Catch:{ Exception -> 0x044b }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r1.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r4 = r7.bM     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r8)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            int r0 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            if (r0 != 0) goto L_0x0331
            r0 = 0
            defpackage.y.cP = r0     // Catch:{ Exception -> 0x044b }
        L_0x0257:
            r1.close()     // Catch:{ Exception -> 0x044b }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r1.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r4 = r7.bP     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r8)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            int r0 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            if (r0 != 0) goto L_0x0363
            r0 = 0
            defpackage.y.cF = r0     // Catch:{ Exception -> 0x044b }
        L_0x027c:
            r1.close()     // Catch:{ Exception -> 0x044b }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r1.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r4 = r7.bN     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r8)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            int r0 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r4 = 9
            int[] r0 = new int[]{r0, r4}     // Catch:{ Exception -> 0x044b }
            java.lang.Class r4 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x044b }
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r4, r0)     // Catch:{ Exception -> 0x044b }
            int[][] r0 = (int[][]) r0     // Catch:{ Exception -> 0x044b }
            defpackage.y.aR = r0     // Catch:{ Exception -> 0x044b }
            r4 = r3
        L_0x02ad:
            int[][] r0 = defpackage.y.aR     // Catch:{ Exception -> 0x044b }
            if (r0 == 0) goto L_0x039b
            int[][] r0 = defpackage.y.aR     // Catch:{ Exception -> 0x044b }
            int r0 = r0.length     // Catch:{ Exception -> 0x044b }
            if (r4 >= r0) goto L_0x039b
            r0 = r3
        L_0x02b7:
            int[][] r5 = defpackage.y.aR     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r5 = r5.length     // Catch:{ Exception -> 0x044b }
            if (r0 >= r5) goto L_0x0396
            int[][] r5 = defpackage.y.aR     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r6 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r5[r0] = r6     // Catch:{ Exception -> 0x044b }
            int r0 = r0 + 1
            goto L_0x02b7
        L_0x02cb:
            r4 = 9
            int[] r0 = new int[]{r0, r4}     // Catch:{ Exception -> 0x044b }
            java.lang.Class r4 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x044b }
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r4, r0)     // Catch:{ Exception -> 0x044b }
            int[][] r0 = (int[][]) r0     // Catch:{ Exception -> 0x044b }
            defpackage.y.cE = r0     // Catch:{ Exception -> 0x044b }
            r4 = r3
        L_0x02dc:
            int[][] r0 = defpackage.y.cE     // Catch:{ Exception -> 0x044b }
            if (r0 == 0) goto L_0x020d
            int[][] r0 = defpackage.y.cE     // Catch:{ Exception -> 0x044b }
            int r0 = r0.length     // Catch:{ Exception -> 0x044b }
            if (r4 >= r0) goto L_0x020d
            r0 = r3
        L_0x02e6:
            int[][] r5 = defpackage.y.cE     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r5 = r5.length     // Catch:{ Exception -> 0x044b }
            if (r0 >= r5) goto L_0x02fa
            int[][] r5 = defpackage.y.cE     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r6 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r5[r0] = r6     // Catch:{ Exception -> 0x044b }
            int r0 = r0 + 1
            goto L_0x02e6
        L_0x02fa:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x02dc
        L_0x02fe:
            r4 = 9
            int[] r0 = new int[]{r0, r4}     // Catch:{ Exception -> 0x044b }
            java.lang.Class r4 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x044b }
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r4, r0)     // Catch:{ Exception -> 0x044b }
            int[][] r0 = (int[][]) r0     // Catch:{ Exception -> 0x044b }
            defpackage.y.cG = r0     // Catch:{ Exception -> 0x044b }
            r4 = r3
        L_0x030f:
            int[][] r0 = defpackage.y.cG     // Catch:{ Exception -> 0x044b }
            if (r0 == 0) goto L_0x0232
            int[][] r0 = defpackage.y.cG     // Catch:{ Exception -> 0x044b }
            int r0 = r0.length     // Catch:{ Exception -> 0x044b }
            if (r4 >= r0) goto L_0x0232
            r0 = r3
        L_0x0319:
            int[][] r5 = defpackage.y.cG     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r5 = r5.length     // Catch:{ Exception -> 0x044b }
            if (r0 >= r5) goto L_0x032d
            int[][] r5 = defpackage.y.cG     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r6 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r5[r0] = r6     // Catch:{ Exception -> 0x044b }
            int r0 = r0 + 1
            goto L_0x0319
        L_0x032d:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x030f
        L_0x0331:
            r4 = 2
            int[] r0 = new int[]{r0, r4}     // Catch:{ Exception -> 0x044b }
            java.lang.Class r4 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x044b }
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r4, r0)     // Catch:{ Exception -> 0x044b }
            int[][] r0 = (int[][]) r0     // Catch:{ Exception -> 0x044b }
            defpackage.y.cP = r0     // Catch:{ Exception -> 0x044b }
            r4 = r3
        L_0x0341:
            int[][] r0 = defpackage.y.cP     // Catch:{ Exception -> 0x044b }
            if (r0 == 0) goto L_0x0257
            int[][] r0 = defpackage.y.cP     // Catch:{ Exception -> 0x044b }
            int r0 = r0.length     // Catch:{ Exception -> 0x044b }
            if (r4 >= r0) goto L_0x0257
            r0 = r3
        L_0x034b:
            int[][] r5 = defpackage.y.cP     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r5 = r5.length     // Catch:{ Exception -> 0x044b }
            if (r0 >= r5) goto L_0x035f
            int[][] r5 = defpackage.y.cP     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r6 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r5[r0] = r6     // Catch:{ Exception -> 0x044b }
            int r0 = r0 + 1
            goto L_0x034b
        L_0x035f:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0341
        L_0x0363:
            r4 = 9
            int[] r0 = new int[]{r0, r4}     // Catch:{ Exception -> 0x044b }
            java.lang.Class r4 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x044b }
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r4, r0)     // Catch:{ Exception -> 0x044b }
            int[][] r0 = (int[][]) r0     // Catch:{ Exception -> 0x044b }
            defpackage.y.cF = r0     // Catch:{ Exception -> 0x044b }
            r4 = r3
        L_0x0374:
            int[][] r0 = defpackage.y.cF     // Catch:{ Exception -> 0x044b }
            if (r0 == 0) goto L_0x027c
            int[][] r0 = defpackage.y.cF     // Catch:{ Exception -> 0x044b }
            int r0 = r0.length     // Catch:{ Exception -> 0x044b }
            if (r4 >= r0) goto L_0x027c
            r0 = r3
        L_0x037e:
            int[][] r5 = defpackage.y.cF     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r5 = r5.length     // Catch:{ Exception -> 0x044b }
            if (r0 >= r5) goto L_0x0392
            int[][] r5 = defpackage.y.cF     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r6 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r5[r0] = r6     // Catch:{ Exception -> 0x044b }
            int r0 = r0 + 1
            goto L_0x037e
        L_0x0392:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0374
        L_0x0396:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x02ad
        L_0x039b:
            r1.close()     // Catch:{ Exception -> 0x044b }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r1.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r4 = r7.bO     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r8)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r0 = 6
            r4 = 9
            int[] r0 = new int[]{r0, r4}     // Catch:{ Exception -> 0x044b }
            java.lang.Class r4 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x044b }
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r4, r0)     // Catch:{ Exception -> 0x044b }
            int[][] r0 = (int[][]) r0     // Catch:{ Exception -> 0x044b }
            defpackage.y.ch = r0     // Catch:{ Exception -> 0x044b }
            r4 = r3
        L_0x03c9:
            int[][] r0 = defpackage.y.ch     // Catch:{ Exception -> 0x044b }
            if (r0 == 0) goto L_0x03eb
            int[][] r0 = defpackage.y.ch     // Catch:{ Exception -> 0x044b }
            int r0 = r0.length     // Catch:{ Exception -> 0x044b }
            if (r4 >= r0) goto L_0x03eb
            r0 = r3
        L_0x03d3:
            int[][] r5 = defpackage.y.ch     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r5 = r5.length     // Catch:{ Exception -> 0x044b }
            if (r0 >= r5) goto L_0x03e7
            int[][] r5 = defpackage.y.ch     // Catch:{ Exception -> 0x044b }
            r5 = r5[r4]     // Catch:{ Exception -> 0x044b }
            int r6 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r5[r0] = r6     // Catch:{ Exception -> 0x044b }
            int r0 = r0 + 1
            goto L_0x03d3
        L_0x03e7:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x03c9
        L_0x03eb:
            r1.close()     // Catch:{ Exception -> 0x044b }
            ab r0 = r7.bH     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            r1.<init>()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r4 = r7.bQ     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.StringBuffer r1 = r1.append(r8)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x042c, all -> 0x043c }
            int r0 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            int[] r0 = new int[r0]     // Catch:{ Exception -> 0x044b }
            defpackage.y.cR = r0     // Catch:{ Exception -> 0x044b }
            r0 = r3
        L_0x0410:
            int[] r4 = defpackage.y.cR     // Catch:{ Exception -> 0x044b }
            if (r4 == 0) goto L_0x0424
            int[] r4 = defpackage.y.cR     // Catch:{ Exception -> 0x044b }
            int r4 = r4.length     // Catch:{ Exception -> 0x044b }
            if (r0 >= r4) goto L_0x0424
            int[] r4 = defpackage.y.cR     // Catch:{ Exception -> 0x044b }
            int r5 = r1.readInt()     // Catch:{ Exception -> 0x044b }
            r4[r0] = r5     // Catch:{ Exception -> 0x044b }
            int r0 = r0 + 1
            goto L_0x0410
        L_0x0424:
            r1.close()     // Catch:{ Exception -> 0x044b }
        L_0x0427:
            r7.bH = r2
            defpackage.y.aV = r3
            return
        L_0x042c:
            r0 = move-exception
            r1 = r2
        L_0x042e:
            r0.printStackTrace()     // Catch:{ all -> 0x0449 }
            if (r1 == 0) goto L_0x0427
            r1.close()     // Catch:{ Exception -> 0x0437 }
            goto L_0x0427
        L_0x0437:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0427
        L_0x043c:
            r0 = move-exception
            r1 = r2
        L_0x043e:
            if (r1 == 0) goto L_0x0443
            r1.close()     // Catch:{ Exception -> 0x0444 }
        L_0x0443:
            throw r0
        L_0x0444:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0443
        L_0x0449:
            r0 = move-exception
            goto L_0x043e
        L_0x044b:
            r0 = move-exception
            goto L_0x042e
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.y.D(int):void");
    }

    public final void E(int i2) {
        bF[i2].dt = 0;
        bF[i2].du = 0;
        bF[i2].dv = 0;
        bF[i2].dw = 0;
        bF[i2].dx = 0;
        bF[i2].dy = 0;
        bF[i2].dA = 0;
        bF[i2].dB = 0;
        bF[i2].dD = 0;
        bF[i2].dE = 0;
        bF[i2].dG = 0;
        bF[i2].dH = 0;
        bF[i2].dN = 0;
        bF[i2].dO = 0;
        bF[i2].dP = 0;
        bF[i2].dQ = 0;
        bF[i2].dR = 0;
        bF[i2].eh = 0;
        bF[i2].ej = 0;
        bF[i2].ek = 0;
        bF[i2].el = 0;
        bF[i2].em = 0;
        bF[i2].en = 0;
        bF[i2].eo = 0;
        bF[i2].et = 0;
        for (int i3 = 0; i3 < 9; i3++) {
            if (aR[i2][i3] != -1) {
                switch (i3) {
                    case 0:
                        bF[i2].dx += cG[aR[i2][i3]][1];
                        if (cG[aR[i2][i3]][2] > 0) {
                            c(1, i2, i3);
                            break;
                        } else {
                            continue;
                        }
                    case 1:
                        bF[i2].dy += cG[aR[i2][i3]][1];
                        if (cG[aR[i2][i3]][2] > 0) {
                            c(1, i2, i3);
                            break;
                        } else {
                            continue;
                        }
                    case 2:
                        bF[i2].dw += cG[aR[i2][i3]][1];
                        if (cG[aR[i2][i3]][2] > 0) {
                            c(1, i2, i3);
                            break;
                        } else {
                            continue;
                        }
                    case 3:
                        bF[i2].dt += cG[aR[i2][i3]][1];
                        if (cG[aR[i2][i3]][2] > 0) {
                            c(1, i2, i3);
                            break;
                        } else {
                            continue;
                        }
                    case 4:
                        bF[i2].dv += cE[aR[i2][i3]][1];
                        if (cE[aR[i2][i3]][2] > 0) {
                            c(0, i2, i3);
                            break;
                        } else {
                            continue;
                        }
                    case 5:
                        bF[i2].du += cG[aR[i2][i3]][1];
                        if (cG[aR[i2][i3]][2] > 0) {
                            c(1, i2, i3);
                            break;
                        } else {
                            continue;
                        }
                    case 6:
                        bF[i2].dA += cG[aR[i2][i3]][1];
                        if (cG[aR[i2][i3]][2] > 0) {
                            c(1, i2, i3);
                            break;
                        } else {
                            continue;
                        }
                    case 7:
                        bF[i2].dt += cG[aR[i2][i3]][1];
                        if (cG[aR[i2][i3]][2] > 0) {
                            c(1, i2, i3);
                            break;
                        } else {
                            continue;
                        }
                    case 8:
                        bF[i2].dB += cG[aR[i2][i3]][1];
                        if (cG[aR[i2][i3]][2] > 0) {
                            c(1, i2, i3);
                            break;
                        } else {
                            continue;
                        }
                }
            }
        }
        if (bF[i2].aW > bF[i2].aX + bF[i2].dt) {
            bF[i2].aW = bF[i2].aX + bF[i2].dt;
        }
        if (bF[i2].aY > bF[i2].aZ + bF[i2].du) {
            bF[i2].aY = bF[i2].aZ + bF[i2].du;
        }
    }

    public final void F(int i2) {
        byte b2 = (byte) (this.aA / 4);
        if (((byte) (this.aA % 4)) > 0) {
            b2 = (byte) (b2 + 1);
        }
        this.aj = b2;
    }

    public final void G(int i2) {
        for (int i3 = 0; i3 < bF.length; i3++) {
            if (bF[i3].ac == i2) {
                if (bF[i3].aM) {
                    int i4 = aa.h - 1;
                    aa.h = i4;
                    if (i4 <= 0) {
                        bF[0].aM = true;
                        aa.h++;
                    }
                }
                int length = this.ap.bF.length;
                boolean z = false;
                for (int i5 = 0; i5 < length; i5++) {
                    if (!z && this.ap.bF[i5] == bF[i3]) {
                        z = true;
                    }
                    if (z && i5 < this.ap.bF.length - 1) {
                        this.ap.bF[i5] = this.ap.bF[i5 + 1];
                    }
                }
                this.aT.addElement(bF[i3]);
                b(bF[i3]);
                ai[] aiVarArr = new ai[(this.ap.bF.length - 1)];
                System.arraycopy(this.ap.bF, 0, aiVarArr, 0, this.ap.bF.length - 1);
                this.ap.bF = aiVarArr;
                return;
            }
        }
    }

    public final i O() {
        if (this.ad > 16) {
            this.ad = 0;
        }
        System.out.println("infoID");
        f fVar = new f("/data/text.bin");
        i iVar = new i();
        iVar.o = fVar.c(this.ad).i(0);
        this.ad++;
        return iVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x01d4 A[SYNTHETIC, Splitter:B:20:0x01d4] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x01e8 A[SYNTHETIC, Splitter:B:27:0x01e8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void P() {
        /*
            r7 = this;
            r2 = 0
            r6 = 1
            r0 = 0
            ab r1 = new ab     // Catch:{ Exception -> 0x01cd, all -> 0x01e4 }
            r1.<init>()     // Catch:{ Exception -> 0x01cd, all -> 0x01e4 }
            r7.bH = r1     // Catch:{ Exception -> 0x01cd, all -> 0x01e4 }
            ab r1 = r7.bH     // Catch:{ Exception -> 0x01cd, all -> 0x01e4 }
            java.lang.String r3 = r7.w     // Catch:{ Exception -> 0x01cd, all -> 0x01e4 }
            r1.a(r3)     // Catch:{ Exception -> 0x01cd, all -> 0x01e4 }
            ab r1 = r7.bH     // Catch:{ Exception -> 0x01cd, all -> 0x01e4 }
            java.lang.String r3 = r7.I     // Catch:{ Exception -> 0x01cd, all -> 0x01e4 }
            java.io.DataInputStream r1 = r1.o(r3)     // Catch:{ Exception -> 0x01cd, all -> 0x01e4 }
            r3 = 1
            r7.ca = r3     // Catch:{ Exception -> 0x01fa }
        L_0x001c:
            r3 = 3
            if (r0 >= r3) goto L_0x01c7
            byte[] r3 = r7.at     // Catch:{ Exception -> 0x01fa }
            byte r4 = r1.readByte()     // Catch:{ Exception -> 0x01fa }
            r3[r0] = r4     // Catch:{ Exception -> 0x01fa }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01fa }
            r4.<init>()     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "loadRmsInfo flag["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            byte[] r5 = r7.at     // Catch:{ Exception -> 0x01fa }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01fa }
            r3.println(r4)     // Catch:{ Exception -> 0x01fa }
            byte[] r3 = r7.at     // Catch:{ Exception -> 0x01fa }
            byte r3 = r3[r0]     // Catch:{ Exception -> 0x01fa }
            if (r3 != r6) goto L_0x01c3
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01fa }
            r4.<init>()     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "names["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.String[] r5 = r7.cS     // Catch:{ Exception -> 0x01fa }
            r5 = r5[r0]     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01fa }
            r3.println(r4)     // Catch:{ Exception -> 0x01fa }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01fa }
            r4.<init>()     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "levels["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            short[] r5 = r7.aD     // Catch:{ Exception -> 0x01fa }
            short r5 = r5[r0]     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01fa }
            r3.println(r4)     // Catch:{ Exception -> 0x01fa }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01fa }
            r4.<init>()     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "years["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            short[] r5 = r7.cT     // Catch:{ Exception -> 0x01fa }
            short r5 = r5[r0]     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01fa }
            r3.println(r4)     // Catch:{ Exception -> 0x01fa }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01fa }
            r4.<init>()     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "months["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            byte[] r5 = r7.aB     // Catch:{ Exception -> 0x01fa }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01fa }
            r3.println(r4)     // Catch:{ Exception -> 0x01fa }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01fa }
            r4.<init>()     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "days["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            byte[] r5 = r7.cU     // Catch:{ Exception -> 0x01fa }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01fa }
            r3.println(r4)     // Catch:{ Exception -> 0x01fa }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01fa }
            r4.<init>()     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "hours["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            byte[] r5 = r7.cV     // Catch:{ Exception -> 0x01fa }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01fa }
            r3.println(r4)     // Catch:{ Exception -> 0x01fa }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01fa }
            r4.<init>()     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "minutes["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            byte[] r5 = r7.cW     // Catch:{ Exception -> 0x01fa }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01fa }
            r3.println(r4)     // Catch:{ Exception -> 0x01fa }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01fa }
            r4.<init>()     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "seconds["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            byte[] r5 = r7.cX     // Catch:{ Exception -> 0x01fa }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x01fa }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x01fa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01fa }
            r3.println(r4)     // Catch:{ Exception -> 0x01fa }
            java.lang.String[] r3 = r7.cS     // Catch:{ Exception -> 0x01fa }
            java.lang.String r4 = r1.readUTF()     // Catch:{ Exception -> 0x01fa }
            r3[r0] = r4     // Catch:{ Exception -> 0x01fa }
            short[] r3 = r7.aD     // Catch:{ Exception -> 0x01fa }
            short r4 = r1.readShort()     // Catch:{ Exception -> 0x01fa }
            r3[r0] = r4     // Catch:{ Exception -> 0x01fa }
            short[] r3 = r7.cT     // Catch:{ Exception -> 0x01fa }
            short r4 = r1.readShort()     // Catch:{ Exception -> 0x01fa }
            r3[r0] = r4     // Catch:{ Exception -> 0x01fa }
            byte[] r3 = r7.aB     // Catch:{ Exception -> 0x01fa }
            byte r4 = r1.readByte()     // Catch:{ Exception -> 0x01fa }
            r3[r0] = r4     // Catch:{ Exception -> 0x01fa }
            byte[] r3 = r7.cU     // Catch:{ Exception -> 0x01fa }
            byte r4 = r1.readByte()     // Catch:{ Exception -> 0x01fa }
            r3[r0] = r4     // Catch:{ Exception -> 0x01fa }
            byte[] r3 = r7.cV     // Catch:{ Exception -> 0x01fa }
            byte r4 = r1.readByte()     // Catch:{ Exception -> 0x01fa }
            r3[r0] = r4     // Catch:{ Exception -> 0x01fa }
            byte[] r3 = r7.cW     // Catch:{ Exception -> 0x01fa }
            byte r4 = r1.readByte()     // Catch:{ Exception -> 0x01fa }
            r3[r0] = r4     // Catch:{ Exception -> 0x01fa }
            byte[] r3 = r7.cX     // Catch:{ Exception -> 0x01fa }
            byte r4 = r1.readByte()     // Catch:{ Exception -> 0x01fa }
            r3[r0] = r4     // Catch:{ Exception -> 0x01fa }
        L_0x01c3:
            int r0 = r0 + 1
            goto L_0x001c
        L_0x01c7:
            r1.close()     // Catch:{ Exception -> 0x01fa }
        L_0x01ca:
            r7.bH = r2
            return
        L_0x01cd:
            r0 = move-exception
            r1 = r2
        L_0x01cf:
            r0.printStackTrace()     // Catch:{ all -> 0x01f8 }
            if (r1 == 0) goto L_0x01ca
            y r0 = I()     // Catch:{ Exception -> 0x01df }
            r3 = 0
            r0.am = r3     // Catch:{ Exception -> 0x01df }
            r1.close()     // Catch:{ Exception -> 0x01df }
            goto L_0x01ca
        L_0x01df:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01ca
        L_0x01e4:
            r0 = move-exception
            r1 = r2
        L_0x01e6:
            if (r1 == 0) goto L_0x01f2
            y r2 = I()     // Catch:{ Exception -> 0x01f3 }
            r3 = 0
            r2.am = r3     // Catch:{ Exception -> 0x01f3 }
            r1.close()     // Catch:{ Exception -> 0x01f3 }
        L_0x01f2:
            throw r0
        L_0x01f3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01f2
        L_0x01f8:
            r0 = move-exception
            goto L_0x01e6
        L_0x01fa:
            r0 = move-exception
            goto L_0x01cf
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.y.P():void");
    }

    public final void Q() {
        K();
        L();
        N();
        J();
        M();
        if (cR != null) {
            cQ = null;
            for (int c : cR) {
                c(c, true);
            }
        }
        R();
    }

    public final void R() {
        for (int i2 = 0; i2 < bF.length; i2++) {
            bF[i2].dt = 0;
            bF[i2].du = 0;
            bF[i2].dv = 0;
            bF[i2].dw = 0;
            bF[i2].dx = 0;
            bF[i2].dy = 0;
            bF[i2].dA = 0;
            bF[i2].dB = 0;
            bF[i2].dD = 0;
            bF[i2].dE = 0;
            bF[i2].dG = 0;
            bF[i2].dH = 0;
            bF[i2].dN = 0;
            bF[i2].dO = 0;
            bF[i2].dP = 0;
            bF[i2].dQ = 0;
            bF[i2].dR = 0;
            bF[i2].eh = 0;
            bF[i2].ej = 0;
            bF[i2].ek = 0;
            bF[i2].el = 0;
            bF[i2].em = 0;
            bF[i2].en = 0;
            bF[i2].eo = 0;
            bF[i2].et = 0;
            for (int i3 = 0; i3 < 9; i3++) {
                if (aR[i2][i3] != -1) {
                    switch (i3) {
                        case 0:
                            bF[i2].dx += cG[aR[i2][i3]][1];
                            if (cG[aR[i2][i3]][2] <= 0) {
                                break;
                            } else {
                                c(1, i2, i3);
                                break;
                            }
                        case 1:
                            bF[i2].dy += cG[aR[i2][i3]][1];
                            if (cG[aR[i2][i3]][2] <= 0) {
                                break;
                            } else {
                                c(1, i2, i3);
                                break;
                            }
                        case 2:
                            bF[i2].dw += cG[aR[i2][i3]][1];
                            if (cG[aR[i2][i3]][2] <= 0) {
                                break;
                            } else {
                                c(1, i2, i3);
                                break;
                            }
                        case 3:
                            bF[i2].dt += cG[aR[i2][i3]][1];
                            if (cG[aR[i2][i3]][2] <= 0) {
                                break;
                            } else {
                                c(1, i2, i3);
                                break;
                            }
                        case 4:
                            bF[i2].dv += cE[aR[i2][i3]][1];
                            if (cE[aR[i2][i3]][2] <= 0) {
                                break;
                            } else {
                                c(0, i2, i3);
                                break;
                            }
                        case 5:
                            bF[i2].du += cG[aR[i2][i3]][1];
                            if (cG[aR[i2][i3]][2] <= 0) {
                                break;
                            } else {
                                c(1, i2, i3);
                                break;
                            }
                        case 6:
                            bF[i2].dA += cG[aR[i2][i3]][1];
                            if (cG[aR[i2][i3]][2] <= 0) {
                                break;
                            } else {
                                c(1, i2, i3);
                                break;
                            }
                        case 7:
                            bF[i2].dt += cG[aR[i2][i3]][1];
                            if (cG[aR[i2][i3]][2] <= 0) {
                                break;
                            } else {
                                c(1, i2, i3);
                                break;
                            }
                        case 8:
                            bF[i2].dB += cG[aR[i2][i3]][1];
                            if (cG[aR[i2][i3]][2] <= 0) {
                                break;
                            } else {
                                c(1, i2, i3);
                                break;
                            }
                    }
                }
            }
            if (bF[i2].aW > bF[i2].aX + bF[i2].dt) {
                bF[i2].aW = bF[i2].aX + bF[i2].dt;
            }
            if (bF[i2].aY > bF[i2].aZ + bF[i2].du) {
                bF[i2].aY = bF[i2].aZ + bF[i2].du;
            }
        }
    }

    public final void S() {
        int[][] iArr = bF[0].aR;
        int i2 = 0;
        int i3 = 0;
        while (bF != null && i2 < bF.length) {
            if (i2 != 0) {
                bF[i2].p = iArr[(i3 + 1) * this.aj][0];
                bF[i2].ab = iArr[(i3 + 1) * this.aj][1];
                bF[i2].ds = iArr[(i3 + 1) * this.aj][2];
                i3++;
            }
            i2++;
        }
    }

    public final void a() {
        aQ = null;
        this.ap = null;
        au.removeAllElements();
        aS.removeAllElements();
        bF = null;
        this.aH.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: y.a(int, boolean):void
     arg types: [int, int]
     candidates:
      y.a(int, int):void
      y.a(ai, java.io.DataOutputStream):void
      y.a(int, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0182  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0229  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x023c  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0256  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0262  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x026a A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(defpackage.ai r14) {
        /*
            r13 = this;
            r12 = 3
            r11 = 4
            r10 = 9
            r3 = 1
            r4 = 0
            ai[] r0 = defpackage.y.bF
            if (r0 != 0) goto L_0x009e
            ai[] r0 = new defpackage.ai[r3]
            defpackage.y.bF = r0
        L_0x000e:
            int r0 = defpackage.y.aV
            if (r0 != 0) goto L_0x00bd
            int r0 = defpackage.aa.h
            if (r0 != r12) goto L_0x00b1
            r2 = r3
        L_0x0017:
            r1 = r4
        L_0x0018:
            java.util.Vector r0 = r13.aT
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x026a
            java.util.Vector r0 = r13.aT
            java.lang.Object r0 = r0.elementAt(r1)
            ai r0 = (defpackage.ai) r0
            int r0 = r0.ac
            int r5 = r14.ac
            if (r0 != r5) goto L_0x0251
            java.util.Vector r0 = r13.aT
            java.lang.Object r0 = r0.elementAt(r1)
            ai r0 = (defpackage.ai) r0
            java.util.Vector r5 = r13.aT
            r5.removeElementAt(r1)
            r5 = r4
        L_0x003c:
            if (r5 >= r10) goto L_0x017e
            int[][] r1 = defpackage.y.ch
            int r6 = r0.ac
            r1 = r1[r6]
            r1 = r1[r5]
            if (r1 < 0) goto L_0x0179
            if (r5 != r11) goto L_0x00f5
            int[][] r1 = defpackage.y.cE
            if (r1 != 0) goto L_0x00c0
            int[] r1 = new int[]{r3, r10}
            java.lang.Class r6 = java.lang.Integer.TYPE
            java.lang.Object r1 = java.lang.reflect.Array.newInstance(r6, r1)
            int[][] r1 = (int[][]) r1
            defpackage.y.cE = r1
            int[][] r6 = defpackage.y.cF
            int[][] r7 = defpackage.y.ch
            int r8 = r0.ac
            r7 = r7[r8]
            r7 = r7[r11]
            r6 = r6[r7]
            r1[r4] = r6
        L_0x006a:
            int[][] r1 = defpackage.y.aR
            ai[] r6 = defpackage.y.bF
            int r6 = r6.length
            int r6 = r6 + -1
            r1 = r1[r6]
            int[][] r6 = defpackage.y.cE
            int r6 = r6.length
            int r6 = r6 + -1
            r1[r11] = r6
        L_0x007a:
            r6 = r4
        L_0x007b:
            r1 = 6
            if (r6 >= r1) goto L_0x0160
            r1 = r4
        L_0x007f:
            if (r1 >= r10) goto L_0x015b
            int[][] r7 = defpackage.y.ch
            r7 = r7[r6]
            r7 = r7[r1]
            int[][] r8 = defpackage.y.ch
            int r9 = r0.ac
            r8 = r8[r9]
            r8 = r8[r5]
            if (r7 <= r8) goto L_0x009b
            int[][] r7 = defpackage.y.ch
            r7 = r7[r6]
            r8 = r7[r1]
            int r8 = r8 + -1
            r7[r1] = r8
        L_0x009b:
            int r1 = r1 + 1
            goto L_0x007f
        L_0x009e:
            ai[] r0 = defpackage.y.bF
            int r0 = r0.length
            int r0 = r0 + 1
            ai[] r0 = new defpackage.ai[r0]
            ai[] r1 = defpackage.y.bF
            ai[] r2 = defpackage.y.bF
            int r2 = r2.length
            java.lang.System.arraycopy(r1, r4, r0, r4, r2)
            defpackage.y.bF = r0
            goto L_0x000e
        L_0x00b1:
            int r0 = defpackage.aa.h
            if (r0 >= r12) goto L_0x00bd
            r14.aM = r3
            int r0 = defpackage.aa.h
            int r0 = r0 + 1
            defpackage.aa.h = r0
        L_0x00bd:
            r2 = r4
            goto L_0x0017
        L_0x00c0:
            int[][] r1 = defpackage.y.cE
            int r1 = r1.length
            r6 = 512(0x200, float:7.175E-43)
            if (r1 >= r6) goto L_0x006a
            int[][] r1 = defpackage.y.cE
            int r1 = r1.length
            int r1 = r1 + 1
            int[] r1 = new int[]{r1, r10}
            java.lang.Class r6 = java.lang.Integer.TYPE
            java.lang.Object r1 = java.lang.reflect.Array.newInstance(r6, r1)
            int[][] r1 = (int[][]) r1
            int[][] r6 = defpackage.y.cE
            int[][] r7 = defpackage.y.cE
            int r7 = r7.length
            java.lang.System.arraycopy(r6, r4, r1, r4, r7)
            int r6 = r1.length
            int r6 = r6 + -1
            int[][] r7 = defpackage.y.cF
            int[][] r8 = defpackage.y.ch
            int r9 = r0.ac
            r8 = r8[r9]
            r8 = r8[r11]
            r7 = r7[r8]
            r1[r6] = r7
            defpackage.y.cE = r1
            goto L_0x006a
        L_0x00f5:
            int[][] r1 = defpackage.y.cG
            if (r1 != 0) goto L_0x0127
            int[] r1 = new int[]{r3, r10}
            java.lang.Class r6 = java.lang.Integer.TYPE
            java.lang.Object r1 = java.lang.reflect.Array.newInstance(r6, r1)
            int[][] r1 = (int[][]) r1
            defpackage.y.cG = r1
            int[][] r6 = defpackage.y.cF
            int[][] r7 = defpackage.y.ch
            int r8 = r0.ac
            r7 = r7[r8]
            r7 = r7[r5]
            r6 = r6[r7]
            r1[r4] = r6
        L_0x0115:
            int[][] r1 = defpackage.y.aR
            ai[] r6 = defpackage.y.bF
            int r6 = r6.length
            int r6 = r6 + -1
            r1 = r1[r6]
            int[][] r6 = defpackage.y.cG
            int r6 = r6.length
            int r6 = r6 + -1
            r1[r5] = r6
            goto L_0x007a
        L_0x0127:
            int[][] r1 = defpackage.y.cG
            int r1 = r1.length
            r6 = 512(0x200, float:7.175E-43)
            if (r1 >= r6) goto L_0x0115
            int[][] r1 = defpackage.y.cG
            int r1 = r1.length
            int r1 = r1 + 1
            int[] r1 = new int[]{r1, r10}
            java.lang.Class r6 = java.lang.Integer.TYPE
            java.lang.Object r1 = java.lang.reflect.Array.newInstance(r6, r1)
            int[][] r1 = (int[][]) r1
            int[][] r6 = defpackage.y.cG
            int[][] r7 = defpackage.y.cG
            int r7 = r7.length
            java.lang.System.arraycopy(r6, r4, r1, r4, r7)
            int r6 = r1.length
            int r6 = r6 + -1
            int[][] r7 = defpackage.y.cF
            int[][] r8 = defpackage.y.ch
            int r9 = r0.ac
            r8 = r8[r9]
            r8 = r8[r5]
            r7 = r7[r8]
            r1[r6] = r7
            defpackage.y.cG = r1
            goto L_0x0115
        L_0x015b:
            int r1 = r6 + 1
            r6 = r1
            goto L_0x007b
        L_0x0160:
            int[][] r1 = defpackage.y.cF
            int[][] r6 = defpackage.y.ch
            int r7 = r0.ac
            r6 = r6[r7]
            r6 = r6[r5]
            int[][] r1 = defpackage.ah.a(r1, r6)
            defpackage.y.cF = r1
            int[][] r1 = defpackage.y.ch
            int r6 = r0.ac
            r1 = r1[r6]
            r6 = -1
            r1[r5] = r6
        L_0x0179:
            int r1 = r5 + 1
            r5 = r1
            goto L_0x003c
        L_0x017e:
            r14 = r0
            r0 = r3
        L_0x0180:
            if (r0 != 0) goto L_0x01ad
            int r1 = r14.dc
            a(r1, r3)
            int[][] r1 = defpackage.y.aR
            ai[] r5 = defpackage.y.bF
            int r5 = r5.length
            int r5 = r5 + -1
            r1 = r1[r5]
            int[][] r5 = defpackage.y.cE
            int r5 = r5.length
            int r5 = r5 + -1
            r1[r11] = r5
            int r1 = r14.dr
            b(r1, r3)
            int[][] r1 = defpackage.y.aR
            ai[] r5 = defpackage.y.bF
            int r5 = r5.length
            int r5 = r5 + -1
            r1 = r1[r5]
            r5 = 2
            int[][] r6 = defpackage.y.cG
            int r6 = r6.length
            int r6 = r6 + -1
            r1[r5] = r6
        L_0x01ad:
            ai[] r1 = defpackage.y.bF
            ai[] r5 = defpackage.y.bF
            int r5 = r5.length
            int r5 = r5 + -1
            r1[r5] = r14
            ai[] r1 = defpackage.y.bF
            ai[] r5 = defpackage.y.bF
            int r5 = r5.length
            int r5 = r5 + -1
            r1 = r1[r5]
            ak r5 = r13.ap
            r1.ap = r5
            ai[] r1 = defpackage.y.bF
            ai[] r5 = defpackage.y.bF
            int r5 = r5.length
            int r5 = r5 + -1
            r1 = r1[r5]
            r1.aK = r3
            if (r2 != r3) goto L_0x01ea
            if (r0 != r3) goto L_0x01ea
            ai[] r0 = defpackage.y.bF
            ai[] r1 = defpackage.y.bF
            int r1 = r1.length
            int r1 = r1 + -1
            r0 = r0[r1]
            boolean r0 = r0.aM
            if (r0 == 0) goto L_0x01ea
            ai[] r0 = defpackage.y.bF
            ai[] r1 = defpackage.y.bF
            int r1 = r1.length
            int r1 = r1 + -1
            r0 = r0[r1]
            r0.aM = r4
        L_0x01ea:
            ai[] r0 = defpackage.y.bF
            ai[] r1 = defpackage.y.bF
            int r1 = r1.length
            int r1 = r1 + -1
            r0 = r0[r1]
            r0.k = r3
            ai[] r0 = defpackage.y.bF
            ai[] r1 = defpackage.y.bF
            int r1 = r1.length
            int r1 = r1 + -1
            r0 = r0[r1]
            r0.am = r3
            ai[] r0 = defpackage.y.bF
            ai[] r1 = defpackage.y.bF
            int r1 = r1.length
            int r1 = r1 + -1
            r0 = r0[r1]
            r0.b = r4
            ai[] r0 = defpackage.y.bF
            ai[] r1 = defpackage.y.bF
            int r1 = r1.length
            int r1 = r1 + -1
            r0 = r0[r1]
            java.lang.String r1 = defpackage.y.o
            r0.u = r1
            ai[] r0 = defpackage.y.bF
            ai[] r1 = defpackage.y.bF
            int r1 = r1.length
            int r1 = r1 + -1
            r0 = r0[r1]
            r0.b()
            ai[] r0 = defpackage.y.bF
            int r0 = r0.length
            if (r0 <= r3) goto L_0x0256
            ai[] r0 = defpackage.y.bF
            ai[] r1 = defpackage.y.bF
            int r1 = r1.length
            int r1 = r1 + -1
            r0 = r0[r1]
            r0.a = r12
        L_0x0234:
            ai[] r0 = defpackage.y.bF
            r0 = r0[r4]
            int[][] r0 = r0.aR
            if (r0 != 0) goto L_0x0262
            ai[] r0 = defpackage.y.bF
            r0 = r0[r4]
            r0.q()
        L_0x0243:
            boolean r0 = r13.am
            if (r0 != 0) goto L_0x024d
            boolean r0 = r14.bS
            if (r0 != 0) goto L_0x024d
            r14.bS = r3
        L_0x024d:
            r13.S()
            return
        L_0x0251:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0018
        L_0x0256:
            ai[] r0 = defpackage.y.bF
            ai[] r1 = defpackage.y.bF
            int r1 = r1.length
            int r1 = r1 + -1
            r0 = r0[r1]
            r0.a = r4
            goto L_0x0234
        L_0x0262:
            ai[] r0 = defpackage.y.bF
            r0 = r0[r4]
            r0.v()
            goto L_0x0243
        L_0x026a:
            r0 = r4
            goto L_0x0180
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.y.a(ai):void");
    }

    public final void b() {
        if (this.am) {
            if (this.ap != null) {
                o();
                this.ap = null;
            }
            au.removeAllElements();
            aS.removeAllElements();
            if (this.ap == null) {
                this.ap = new ak();
                this.ap.e = this.ae;
            }
            this.am = false;
        } else if (this.ap == null) {
            this.ap = new ak();
            aa.h = 0;
            K();
            L();
            N();
            J();
            M();
            for (int i2 = 0; i2 < aR.length; i2++) {
                for (int i3 = 0; i3 < aR[0].length; i3++) {
                    aR[i2][i3] = -1;
                    ch[i2][i3] = -1;
                }
            }
            this.bE = v(0);
            if (this.bE != null) {
                this.bE.b = 0;
                if (bF == null) {
                    a(this.bE);
                }
            }
            R();
            r c = new f("/data/startmap.bin").c(0);
            this.ap.e = c.aq[0];
            int i4 = c.aq[3];
            int i5 = c.aq[4];
            int i6 = c.aq[5];
            this.bE.p = i4;
            this.bE.ab = i5;
            this.bE.ds = i6;
            this.bE.k = true;
            this.bE.am = true;
            this.bE.b = 0;
            this.bE.a = 0;
            this.bE.u = o;
        }
        int i7 = this.ap.e;
        r c2 = new f("/data/map.bin").c(i7);
        this.s = c2.i(0);
        this.i = c2.aq[2];
        String i8 = c2.i(1);
        System.out.println(i8);
        int indexOf = i8.indexOf("xml");
        if (indexOf != -1) {
            i8 = new StringBuffer().append(i8.substring(0, indexOf)).append(StructuredData.TYPE_OF_MAP).toString();
        }
        System.out.println(i8);
        try {
            this.ap.vh = new u(new StringBuffer().append("/map/").append(i8).toString(), "/map/", false);
            this.ap.e = i7;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (au.isEmpty()) {
            e();
        }
        System.out.println(new StringBuffer().append("battleOver:      ").append(this.ao).toString());
        if (this.ao) {
            this.ap.v();
        } else if (aS != null) {
            this.ap.c(aS);
        }
        if (au != null) {
            this.ap.d(au);
        }
        if (!this.bX) {
            bF[0].q();
            S();
        }
        this.Y.k = true;
        System.gc();
    }

    public final void b(ai aiVar) {
        int i2 = aiVar.ac;
        int i3 = 0;
        boolean z = false;
        int i4 = 0;
        while (bF != null && i3 < bF.length) {
            if (bF[i3] == aiVar) {
                z = true;
                i4 = i3;
            }
            if (z && i3 < bF.length - 1) {
                bF[i3] = bF[i3 + 1];
            }
            i3++;
        }
        ai[] aiVarArr = new ai[(bF.length - 1)];
        System.arraycopy(bF, 0, aiVarArr, 0, bF.length - 1);
        bF = aiVarArr;
        aiVarArr[0].v();
        for (int i5 = 0; i5 < 9; i5++) {
            if (aR[i4][i5] >= 0) {
                if (i5 == 4) {
                    if (cF == null) {
                        int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, 1, 9);
                        cF = iArr;
                        iArr[0] = cE[aR[i4][i5]];
                    } else if (cF.length < 512) {
                        int[][] iArr2 = (int[][]) Array.newInstance(Integer.TYPE, cF.length + 1, 9);
                        System.arraycopy(cF, 0, iArr2, 0, cF.length);
                        iArr2[iArr2.length - 1] = cE[aR[i4][i5]];
                        cF = iArr2;
                    }
                    ch[i2][i5] = cF.length - 1;
                    cE = ah.a(cE, aR[i4][i5]);
                    for (int i6 = 0; i6 < 6; i6++) {
                        if (aR[i6][4] > aR[i4][i5]) {
                            int[] iArr3 = aR[i6];
                            iArr3[4] = iArr3[4] - 1;
                        }
                    }
                } else {
                    if (cF == null) {
                        int[][] iArr4 = (int[][]) Array.newInstance(Integer.TYPE, 1, 9);
                        cF = iArr4;
                        iArr4[0] = cG[aR[i4][i5]];
                    } else if (cF.length < 512) {
                        int[][] iArr5 = (int[][]) Array.newInstance(Integer.TYPE, cF.length + 1, 9);
                        System.arraycopy(cF, 0, iArr5, 0, cF.length);
                        iArr5[iArr5.length - 1] = cG[aR[i4][i5]];
                        cF = iArr5;
                    }
                    ch[i2][i5] = cF.length - 1;
                    cG = ah.a(cG, aR[i4][i5]);
                    for (int i7 = 0; i7 < 6; i7++) {
                        for (int i8 = 0; i8 < 9; i8++) {
                            if (i8 != 4 && aR[i7][i8] > aR[i4][i5]) {
                                int[] iArr6 = aR[i7];
                                iArr6[i8] = iArr6[i8] - 1;
                            }
                        }
                    }
                }
                aR[i4][i5] = -1;
            }
        }
    }

    public final void d(byte b2) {
        if (cc) {
            this.Y.o();
        }
        this.Y.aO.a(7);
        ((al) this.Y.aO.kB()).aj = b2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x030f A[SYNTHETIC, Splitter:B:25:0x030f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void g(byte r11) {
        /*
            r10 = this;
            r2 = 0
            r9 = 1
            r0 = 0
            byte[] r1 = r10.at
            r1[r11] = r9
            java.io.PrintStream r1 = java.lang.System.out
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r4 = "createRmsInfo flag[rmsNum]="
            java.lang.StringBuffer r3 = r3.append(r4)
            byte[] r4 = r10.at
            byte r4 = r4[r11]
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.println(r3)
            java.lang.String[] r1 = r10.cS
            ai[] r3 = defpackage.y.bF
            r3 = r3[r0]
            java.lang.String r3 = r3.o
            r1[r11] = r3
            short[] r1 = r10.aD
            ai[] r3 = defpackage.y.bF
            r3 = r3[r0]
            int r3 = r3.ad
            short r3 = (short) r3
            r1[r11] = r3
            java.lang.String r1 = "GMT+8"
            java.util.TimeZone r1 = java.util.TimeZone.getTimeZone(r1)
            java.util.Calendar r1 = java.util.Calendar.getInstance(r1)
            java.io.PrintStream r3 = java.lang.System.out
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.String r5 = "calendar.getTime="
            java.lang.StringBuffer r4 = r4.append(r5)
            java.util.Date r5 = r1.getTime()
            java.lang.StringBuffer r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.println(r4)
            int r3 = r1.get(r9)
            r4 = 2
            int r4 = r1.get(r4)
            r5 = 5
            int r5 = r1.get(r5)
            r6 = 11
            int r6 = r1.get(r6)
            r7 = 12
            int r7 = r1.get(r7)
            r8 = 13
            int r1 = r1.get(r8)
            short[] r8 = r10.cT
            short r3 = (short) r3
            r8[r11] = r3
            byte[] r3 = r10.aB
            int r4 = r4 + 1
            byte r4 = (byte) r4
            r3[r11] = r4
            byte[] r3 = r10.cU
            byte r4 = (byte) r5
            r3[r11] = r4
            byte[] r3 = r10.cV
            byte r4 = (byte) r6
            r3[r11] = r4
            byte[] r3 = r10.cW
            byte r4 = (byte) r7
            r3[r11] = r4
            byte[] r3 = r10.cX
            byte r1 = (byte) r1
            r3[r11] = r1
            java.io.PrintStream r1 = java.lang.System.out
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r4 = "names[rmsNum]="
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String[] r4 = r10.cS
            r4 = r4[r11]
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.println(r3)
            java.io.PrintStream r1 = java.lang.System.out
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r4 = "levels[rmsNum]="
            java.lang.StringBuffer r3 = r3.append(r4)
            short[] r4 = r10.aD
            short r4 = r4[r11]
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.println(r3)
            java.io.PrintStream r1 = java.lang.System.out
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r4 = "生成时间="
            java.lang.StringBuffer r3 = r3.append(r4)
            short[] r4 = r10.cT
            short r4 = r4[r11]
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "/"
            java.lang.StringBuffer r3 = r3.append(r4)
            byte[] r4 = r10.aB
            byte r4 = r4[r11]
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = "/"
            java.lang.StringBuffer r3 = r3.append(r4)
            byte[] r4 = r10.cU
            byte r4 = r4[r11]
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = " "
            java.lang.StringBuffer r3 = r3.append(r4)
            byte[] r4 = r10.cV
            byte r4 = r4[r11]
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ":"
            java.lang.StringBuffer r3 = r3.append(r4)
            byte[] r4 = r10.cW
            byte r4 = r4[r11]
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = ":"
            java.lang.StringBuffer r3 = r3.append(r4)
            byte[] r4 = r10.cX
            byte r4 = r4[r11]
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.println(r3)
            ab r1 = new ab     // Catch:{ Exception -> 0x02fb, all -> 0x030b }
            r1.<init>()     // Catch:{ Exception -> 0x02fb, all -> 0x030b }
            r10.bH = r1     // Catch:{ Exception -> 0x02fb, all -> 0x030b }
            ab r1 = r10.bH     // Catch:{ Exception -> 0x02fb, all -> 0x030b }
            java.lang.String r3 = r10.w     // Catch:{ Exception -> 0x02fb, all -> 0x030b }
            r1.a(r3)     // Catch:{ Exception -> 0x02fb, all -> 0x030b }
            ab r1 = r10.bH     // Catch:{ Exception -> 0x02fb, all -> 0x030b }
            java.lang.String r3 = r10.I     // Catch:{ Exception -> 0x02fb, all -> 0x030b }
            r4 = 0
            java.io.DataOutputStream r1 = r1.a(r3, r4)     // Catch:{ Exception -> 0x02fb, all -> 0x030b }
        L_0x014f:
            r3 = 3
            if (r0 >= r3) goto L_0x02f1
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x031a }
            r4.<init>()     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "saveRmsInfo flag["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            byte[] r5 = r10.at     // Catch:{ Exception -> 0x031a }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x031a }
            r3.println(r4)     // Catch:{ Exception -> 0x031a }
            byte[] r3 = r10.at     // Catch:{ Exception -> 0x031a }
            byte r3 = r3[r0]     // Catch:{ Exception -> 0x031a }
            r1.writeByte(r3)     // Catch:{ Exception -> 0x031a }
            byte[] r3 = r10.at     // Catch:{ Exception -> 0x031a }
            byte r3 = r3[r0]     // Catch:{ Exception -> 0x031a }
            if (r3 != r9) goto L_0x02ed
            java.lang.String[] r3 = r10.cS     // Catch:{ Exception -> 0x031a }
            r3 = r3[r0]     // Catch:{ Exception -> 0x031a }
            r1.writeUTF(r3)     // Catch:{ Exception -> 0x031a }
            short[] r3 = r10.aD     // Catch:{ Exception -> 0x031a }
            short r3 = r3[r0]     // Catch:{ Exception -> 0x031a }
            r1.writeShort(r3)     // Catch:{ Exception -> 0x031a }
            short[] r3 = r10.cT     // Catch:{ Exception -> 0x031a }
            short r3 = r3[r0]     // Catch:{ Exception -> 0x031a }
            r1.writeShort(r3)     // Catch:{ Exception -> 0x031a }
            byte[] r3 = r10.aB     // Catch:{ Exception -> 0x031a }
            byte r3 = r3[r0]     // Catch:{ Exception -> 0x031a }
            r1.writeByte(r3)     // Catch:{ Exception -> 0x031a }
            byte[] r3 = r10.cU     // Catch:{ Exception -> 0x031a }
            byte r3 = r3[r0]     // Catch:{ Exception -> 0x031a }
            r1.writeByte(r3)     // Catch:{ Exception -> 0x031a }
            byte[] r3 = r10.cV     // Catch:{ Exception -> 0x031a }
            byte r3 = r3[r0]     // Catch:{ Exception -> 0x031a }
            r1.writeByte(r3)     // Catch:{ Exception -> 0x031a }
            byte[] r3 = r10.cW     // Catch:{ Exception -> 0x031a }
            byte r3 = r3[r0]     // Catch:{ Exception -> 0x031a }
            r1.writeByte(r3)     // Catch:{ Exception -> 0x031a }
            byte[] r3 = r10.cX     // Catch:{ Exception -> 0x031a }
            byte r3 = r3[r0]     // Catch:{ Exception -> 0x031a }
            r1.writeByte(r3)     // Catch:{ Exception -> 0x031a }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x031a }
            r4.<init>()     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "names["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.String[] r5 = r10.cS     // Catch:{ Exception -> 0x031a }
            r5 = r5[r0]     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x031a }
            r3.println(r4)     // Catch:{ Exception -> 0x031a }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x031a }
            r4.<init>()     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "levels["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            short[] r5 = r10.aD     // Catch:{ Exception -> 0x031a }
            short r5 = r5[r0]     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x031a }
            r3.println(r4)     // Catch:{ Exception -> 0x031a }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x031a }
            r4.<init>()     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "years["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            short[] r5 = r10.cT     // Catch:{ Exception -> 0x031a }
            short r5 = r5[r0]     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x031a }
            r3.println(r4)     // Catch:{ Exception -> 0x031a }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x031a }
            r4.<init>()     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "months["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            byte[] r5 = r10.aB     // Catch:{ Exception -> 0x031a }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x031a }
            r3.println(r4)     // Catch:{ Exception -> 0x031a }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x031a }
            r4.<init>()     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "days["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            byte[] r5 = r10.cU     // Catch:{ Exception -> 0x031a }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x031a }
            r3.println(r4)     // Catch:{ Exception -> 0x031a }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x031a }
            r4.<init>()     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "hours["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            byte[] r5 = r10.cV     // Catch:{ Exception -> 0x031a }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x031a }
            r3.println(r4)     // Catch:{ Exception -> 0x031a }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x031a }
            r4.<init>()     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "minutes["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            byte[] r5 = r10.cW     // Catch:{ Exception -> 0x031a }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x031a }
            r3.println(r4)     // Catch:{ Exception -> 0x031a }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x031a }
            r4.<init>()     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "seconds["
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r0)     // Catch:{ Exception -> 0x031a }
            java.lang.String r5 = "]="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            byte[] r5 = r10.cX     // Catch:{ Exception -> 0x031a }
            byte r5 = r5[r0]     // Catch:{ Exception -> 0x031a }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Exception -> 0x031a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x031a }
            r3.println(r4)     // Catch:{ Exception -> 0x031a }
        L_0x02ed:
            int r0 = r0 + 1
            goto L_0x014f
        L_0x02f1:
            ab r0 = r10.bH     // Catch:{ Exception -> 0x031a }
            java.lang.String r3 = r10.I     // Catch:{ Exception -> 0x031a }
            r0.p(r3)     // Catch:{ Exception -> 0x031a }
        L_0x02f8:
            r10.bH = r2
            return
        L_0x02fb:
            r0 = move-exception
            r1 = r2
        L_0x02fd:
            r0.printStackTrace()     // Catch:{ all -> 0x0318 }
            if (r1 == 0) goto L_0x02f8
            r1.close()     // Catch:{ Exception -> 0x0306 }
            goto L_0x02f8
        L_0x0306:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x02f8
        L_0x030b:
            r0 = move-exception
            r1 = r2
        L_0x030d:
            if (r1 == 0) goto L_0x0312
            r1.close()     // Catch:{ Exception -> 0x0313 }
        L_0x0312:
            throw r0
        L_0x0313:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0312
        L_0x0318:
            r0 = move-exception
            goto L_0x030d
        L_0x031a:
            r0 = move-exception
            goto L_0x02fd
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.y.g(byte):void");
    }

    public final p n(String str) {
        if (this.cY == null) {
            this.cY = new Hashtable();
        }
        p pVar = null;
        if (this.cY.containsKey(str)) {
            return (p) this.cY.get(str);
        }
        try {
            pVar = ah.n(str);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.cY.put(str, pVar);
        return pVar;
    }

    public final void o() {
        this.ap.a();
        int size = aS.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((ai) aS.elementAt(i2)).a();
        }
        aS.removeAllElements();
        au.removeAllElements();
        System.gc();
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x003a A[SYNTHETIC, Splitter:B:18:0x003a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void q() {
        /*
            r4 = this;
            r2 = 0
            ab r0 = new ab     // Catch:{ Exception -> 0x0027, all -> 0x0037 }
            r0.<init>()     // Catch:{ Exception -> 0x0027, all -> 0x0037 }
            r4.bH = r0     // Catch:{ Exception -> 0x0027, all -> 0x0037 }
            ab r0 = r4.bH     // Catch:{ Exception -> 0x0027, all -> 0x0037 }
            java.lang.String r1 = r4.u     // Catch:{ Exception -> 0x0027, all -> 0x0037 }
            r0.a(r1)     // Catch:{ Exception -> 0x0027, all -> 0x0037 }
            ab r0 = r4.bH     // Catch:{ Exception -> 0x0027, all -> 0x0037 }
            java.lang.String r1 = r4.v     // Catch:{ Exception -> 0x0027, all -> 0x0037 }
            r3 = 0
            java.io.DataOutputStream r1 = r0.a(r1, r3)     // Catch:{ Exception -> 0x0027, all -> 0x0037 }
            boolean r0 = defpackage.y.cf     // Catch:{ Exception -> 0x0046 }
            r1.writeBoolean(r0)     // Catch:{ Exception -> 0x0046 }
            ab r0 = r4.bH     // Catch:{ Exception -> 0x0046 }
            java.lang.String r3 = r4.v     // Catch:{ Exception -> 0x0046 }
            r0.p(r3)     // Catch:{ Exception -> 0x0046 }
        L_0x0024:
            r4.bH = r2
            return
        L_0x0027:
            r0 = move-exception
            r1 = r2
        L_0x0029:
            r0.printStackTrace()     // Catch:{ all -> 0x0043 }
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ Exception -> 0x0032 }
            goto L_0x0024
        L_0x0032:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0024
        L_0x0037:
            r0 = move-exception
        L_0x0038:
            if (r2 == 0) goto L_0x003d
            r2.close()     // Catch:{ Exception -> 0x003e }
        L_0x003d:
            throw r0
        L_0x003e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003d
        L_0x0043:
            r0 = move-exception
            r2 = r1
            goto L_0x0038
        L_0x0046:
            r0 = move-exception
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.y.q():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0040 A[SYNTHETIC, Splitter:B:18:0x0040] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void v() {
        /*
            r4 = this;
            r2 = 0
            ab r0 = new ab     // Catch:{ Exception -> 0x0026, all -> 0x003d }
            r0.<init>()     // Catch:{ Exception -> 0x0026, all -> 0x003d }
            r4.bH = r0     // Catch:{ Exception -> 0x0026, all -> 0x003d }
            ab r0 = r4.bH     // Catch:{ Exception -> 0x0026, all -> 0x003d }
            java.lang.String r1 = r4.u     // Catch:{ Exception -> 0x0026, all -> 0x003d }
            r0.a(r1)     // Catch:{ Exception -> 0x0026, all -> 0x003d }
            ab r0 = r4.bH     // Catch:{ Exception -> 0x0026, all -> 0x003d }
            java.lang.String r1 = r4.v     // Catch:{ Exception -> 0x0026, all -> 0x003d }
            java.io.DataInputStream r1 = r0.o(r1)     // Catch:{ Exception -> 0x0026, all -> 0x003d }
            I()     // Catch:{ Exception -> 0x0053 }
            boolean r0 = r1.readBoolean()     // Catch:{ Exception -> 0x0053 }
            defpackage.y.cf = r0     // Catch:{ Exception -> 0x0053 }
            r1.close()     // Catch:{ Exception -> 0x0053 }
        L_0x0023:
            r4.bH = r2
            return
        L_0x0026:
            r0 = move-exception
            r1 = r2
        L_0x0028:
            r0.printStackTrace()     // Catch:{ all -> 0x0050 }
            if (r1 == 0) goto L_0x0023
            y r0 = I()     // Catch:{ Exception -> 0x0038 }
            r3 = 0
            r0.am = r3     // Catch:{ Exception -> 0x0038 }
            r1.close()     // Catch:{ Exception -> 0x0038 }
            goto L_0x0023
        L_0x0038:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0023
        L_0x003d:
            r0 = move-exception
        L_0x003e:
            if (r2 == 0) goto L_0x004a
            y r1 = I()     // Catch:{ Exception -> 0x004b }
            r3 = 0
            r1.am = r3     // Catch:{ Exception -> 0x004b }
            r2.close()     // Catch:{ Exception -> 0x004b }
        L_0x004a:
            throw r0
        L_0x004b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x0050:
            r0 = move-exception
            r2 = r1
            goto L_0x003e
        L_0x0053:
            r0 = move-exception
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.y.v():void");
    }
}
