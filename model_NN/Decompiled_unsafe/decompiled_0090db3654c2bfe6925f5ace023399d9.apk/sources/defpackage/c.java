package defpackage;

import java.io.IOException;
import java.io.InputStream;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;

/* renamed from: c  reason: default package */
final class c {
    private static final c af = new c();
    private Player ag;
    private int e = 1;

    private c() {
    }

    private void a() {
        if (this.ag != null) {
            this.ag.cZ();
            this.ag.close();
            this.ag = null;
        }
    }

    public static c k() {
        return af;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, int i) {
        a();
        if (str != null) {
            this.e = i;
            try {
                InputStream b = MIDPHelper.b(getClass(), str);
                this.ag = Manager.a(b, "audio/midi");
                this.ag.cX();
                this.ag.cY();
                this.ag.aC(this.e);
                b.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (MediaException e3) {
                e3.printStackTrace();
            }
            if (this.ag != null) {
                try {
                    this.ag.start();
                } catch (MediaException e4) {
                    e4.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        a();
        this.e = 1;
    }
}
