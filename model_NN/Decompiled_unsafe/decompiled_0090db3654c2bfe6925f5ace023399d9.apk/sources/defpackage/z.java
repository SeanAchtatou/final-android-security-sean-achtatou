package defpackage;

import com.a.a.e.p;
import com.a.a.k.b;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.control.ToneControl;

/* renamed from: z  reason: default package */
public final class z {
    private static final byte[] aB = {-119, 80, 78, 71, b.UNSUPPORTED_PUBLIC_KEY_TYPE, 10, 26, 10, 0, 0, 0, b.UNSUPPORTED_PUBLIC_KEY_TYPE, 73, 72, 68, 82};
    private int a;
    public byte[] at;
    public int e;

    public z() {
        this.e = 0;
        this.a = 0;
    }

    public z(String str) {
        String str2 = null;
        try {
            str2 = !str.startsWith("/") ? new StringBuffer().append("/").append(str).toString() : str;
            InputStream b = MIDPHelper.b(getClass(), str2);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(256);
            byte[] bArr = new byte[256];
            while (true) {
                int read = b.read(bArr);
                this.a = read;
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, this.a);
                } else {
                    this.at = byteArrayOutputStream.toByteArray();
                    b.close();
                    byteArrayOutputStream.close();
                    this.a = this.at.length;
                    this.e = 0;
                    return;
                }
            }
        } catch (Exception e2) {
            l.t();
            new StringBuffer().append("读取文件：").append(str2).append("出错").toString();
        }
    }

    public final byte T() {
        byte[] bArr = this.at;
        int i = this.e;
        this.e = i + 1;
        return bArr[i];
    }

    public final short U() {
        byte[] bArr = this.at;
        int i = this.e;
        this.e = i + 1;
        byte[] bArr2 = this.at;
        int i2 = this.e;
        this.e = i2 + 1;
        return (short) (((bArr[i] & ToneControl.SILENCE) << 8) + (bArr2[i2] & ToneControl.SILENCE));
    }

    public final p V() {
        this.a = this.at.length;
        this.e = 0;
        for (int i = 7; i < this.a; i += 8) {
            byte[] bArr = this.at;
            bArr[i] = (byte) (bArr[i] + 5);
        }
        this.a = this.at.length + 16;
        byte[] bArr2 = new byte[this.a];
        for (int i2 = 16; i2 < this.a; i2++) {
            bArr2[i2] = T();
        }
        for (int i3 = 0; i3 < 16; i3++) {
            bArr2[i3] = aB[i3];
        }
        this.at = null;
        this.at = bArr2;
        byte[] bArr3 = this.at;
        return p.d(this.at, 0, this.a);
    }

    public final void a(int i) {
        this.e += i;
        if (this.e >= this.a) {
            this.e = 0;
        }
    }

    public final void b() {
        this.at = null;
        this.a = 0;
        this.e = 0;
    }
}
