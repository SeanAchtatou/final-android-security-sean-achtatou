package defpackage;

import com.a.a.e.o;
import com.a.a.e.p;
import java.lang.reflect.Array;
import java.util.Random;

/* renamed from: ai  reason: default package */
public final class ai {
    private static int eF;
    private static int eP;
    public int[] X;
    private int[] Z;
    public int a;
    byte aA;
    private j aH = y.I().aH;
    public boolean aK;
    public boolean aM;
    public int[][] aR;
    public int aV;
    public int aW;
    public int aX;
    public int aY;
    public int aZ;
    public int aa;
    public int ab;
    public int ac;
    public int ad;
    public int ae;
    private p ah;
    public byte aj;
    public boolean am;
    public boolean an;
    public boolean ao;
    public ak ap;
    private int[] aq;
    private Random as;
    public int b;
    public boolean bR;
    public boolean bS;
    boolean bT;
    private boolean bU = true;
    private boolean bV = true;
    public int ba;
    public int bb;
    public int bc;
    public int bd;
    public int bf;
    public int bg;
    public as bh;
    byte bj;
    public int dA;
    public int dB;
    public int dD;
    public int dE;
    public int dG;
    public int dH;
    public int dN;
    public int dO;
    public int dP;
    public int dQ;
    public int dR;
    public int da;
    public int db;
    public int dc;
    public int dr;
    public int ds;
    public int dt;
    public int du;
    public int dv;
    public int dw;
    public int dx;
    public int dy;
    public int e;
    int eA;
    int eB;
    int eD;
    private int eE;
    public int eh;
    public int ej;
    public int ek;
    public int el;
    public int em;
    public int en;
    public int eo;
    public int et;
    private int fA;
    private int fc = 1;
    private int fh = 10;
    private int fk = -1;
    private int fl = -1;
    private int ft = 0;
    private int fu;
    private int fv = 1;
    private int fw;
    private int fx;
    private int fy;
    private int fz;
    public int h;
    public int i;
    public boolean k;
    public String o;
    public aq op;
    public aq oq;
    private aq or;
    public int p;
    public String r;
    public String s;
    public String t;
    String u;
    public int x;
    public int y;
    public int z;

    private int a(int i2, ai aiVar) {
        int f = this.aH.f(i2);
        if (f > 0) {
            this.b = 0;
            if (aiVar != null) {
                aiVar.ao = true;
                aiVar.fc = 0;
            }
        }
        return f;
    }

    private int e(int i2, int i3) {
        if (this.as == null) {
            this.as = new Random();
        }
        return (Math.abs(this.as.nextInt()) % i3) + 1;
    }

    private boolean l() {
        if (!this.am || !this.k) {
            return false;
        }
        switch (this.ds) {
            case 1:
                int i2 = this.ab + this.op.a;
                if (this.aa + i2 < 0) {
                    this.aa = -i2;
                    return true;
                }
                break;
            case 2:
                int i3 = this.ab;
                if (this.aa + i3 > this.ap.vh.a) {
                    this.aa = this.ap.vh.a - i3;
                    return true;
                }
                break;
            case 3:
                int i4 = this.p + this.op.e;
                if (this.i + i4 < 0) {
                    this.i = -i4;
                    return true;
                }
                break;
            case 4:
                int i5 = this.p - this.op.e;
                if (this.i + i5 >= this.ap.vh.e) {
                    this.i = this.ap.vh.e - i5;
                    return true;
                }
                break;
        }
        return false;
    }

    private boolean m() {
        if (this.ap.e != 0 || !y.I().bV || !y.I().bW) {
            return false;
        }
        switch (this.ds) {
            case 1:
                this.e = 9;
                break;
            case 2:
                this.e = 8;
                break;
            case 3:
                this.e = 10;
                break;
            case 4:
                this.e = 11;
                break;
        }
        return true;
    }

    public final void a() {
        if (this.bh != null) {
            this.bh.b();
        }
        this.bh = null;
        if (this.ah != null) {
            this.ah = null;
        }
    }

    public final void a(int i2, int i3, int i4, int i5) {
        this.fw = i2;
        this.fx = i3;
        this.fy = i4;
        this.eA = i5;
        this.fz = 2;
    }

    public final void a(o oVar, int i2, int i3) {
        if (this.aK) {
            if (this.bh != null && this.k && this.am) {
                this.bh.a(oVar, this.e, this.p - i2, this.ab - i3, false);
                if (this.an) {
                    oVar.setColor(65280);
                }
            }
        } else if (this.ah != null && this.k && this.am) {
            ah.c(oVar, this.ah, this.p - i2, this.ab - i3, 33);
        }
    }

    public final void b() {
        if (this.aK) {
            this.bh = new as(this.t, this.u);
        } else if (this.bR) {
            this.ah = y.I().n(this.u);
        }
        if (this.a == 0 || this.a == 3) {
            this.op = new aq(-7, -7, 14, 1);
        }
    }

    public final void b(boolean z2) {
        if (this.ap.e != 0 || !y.I().bV || !y.I().bW) {
            for (ai aiVar : this.ap.bF) {
                if (aiVar.a != 0 && aiVar.a != 3 && aiVar.am && aiVar.k) {
                    if (ah.c(this.p + this.op.e + this.i, this.ab + this.op.a + this.aa, this.op.b, this.op.h, aiVar.p + aiVar.op.e, aiVar.ab + aiVar.op.a, aiVar.op.b, aiVar.op.h)) {
                        this.i = 0;
                        this.aa = 0;
                        if (!this.aH.k && aiVar.aj != 1) {
                            if (!z2) {
                                if (aiVar.aj != 3) {
                                    if (aiVar.aj == 2 && (aiVar.h == 2 || aiVar.h == 3 || aiVar.h == 4)) {
                                        switch (this.ds) {
                                            case 1:
                                                if (this.ab > aiVar.ab && aiVar.p < this.p + (this.op.b >> 1) && aiVar.p + aiVar.op.b > this.p + (this.op.b >> 1)) {
                                                    aiVar.fv = 2;
                                                    break;
                                                }
                                            case 2:
                                                if (this.ab < aiVar.ab && aiVar.p < this.p + (this.op.b >> 1) && aiVar.p + aiVar.op.b > this.p + (this.op.b >> 1)) {
                                                    aiVar.fv = 1;
                                                    break;
                                                }
                                            case 3:
                                                if (this.p > aiVar.p && aiVar.ab < this.ab + (this.op.h >> 1) && aiVar.ab + aiVar.op.h > this.ab + (this.op.h >> 1)) {
                                                    aiVar.fv = 4;
                                                    break;
                                                }
                                            case 4:
                                                if (this.p < aiVar.p && aiVar.ab < this.ab + (this.op.h >> 1) && aiVar.ab + aiVar.op.h > this.ab + (this.op.h >> 1)) {
                                                    aiVar.fv = 3;
                                                    break;
                                                }
                                        }
                                    }
                                } else {
                                    continue;
                                }
                            } else if (!(aiVar.aj == 2 || aiVar.h == 2 || aiVar.h == 3 || aiVar.h == 4)) {
                            }
                            int i2 = aiVar.op.i;
                            if (i2 >= 0 && this.fk != i2) {
                                this.fk = i2;
                                if (a(i2, aiVar) <= 0) {
                                    aiVar.fc = 1;
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    aiVar.fc = 1;
                    aiVar.ao = false;
                }
            }
            this.fk = -1;
        }
    }

    public final void b(int[] iArr, int[] iArr2, int i2) {
        this.Z = iArr;
        this.aq = iArr2;
        this.eA = 4;
        this.fz = 1;
    }

    /* JADX WARN: Type inference failed for: r11v0, types: [ai] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final void o() {
        /*
            r11 = this;
            j r0 = r11.aH
            boolean r0 = r0.k
            if (r0 != 0) goto L_0x000e
            boolean r0 = r11.am
            if (r0 == 0) goto L_0x000e
            boolean r0 = r11.k
            if (r0 != 0) goto L_0x00ef
        L_0x000e:
            int r0 = r11.a
            if (r0 != 0) goto L_0x059b
            ak r0 = r11.ap
            int r0 = r0.e
            if (r0 != 0) goto L_0x0028
            y r0 = defpackage.y.I()
            boolean r0 = r0.bV
            if (r0 == 0) goto L_0x0028
            y r0 = defpackage.y.I()
            boolean r0 = r0.bW
            if (r0 != 0) goto L_0x0034
        L_0x0028:
            byte r0 = r11.aA
            if (r0 != 0) goto L_0x0034
            boolean r0 = r11.am
            if (r0 == 0) goto L_0x0034
            boolean r0 = r11.k
            if (r0 != 0) goto L_0x01ed
        L_0x0034:
            byte r0 = r11.aA
            r1 = 1
            if (r0 == r1) goto L_0x0043
            byte r0 = r11.aA
            r1 = 2
            if (r0 == r1) goto L_0x0043
            byte r0 = r11.aA
            r1 = 3
            if (r0 != r1) goto L_0x0046
        L_0x0043:
            r0 = 1
            r11.b = r0
        L_0x0046:
            ak r0 = r11.ap
            int r0 = r0.e
            if (r0 != 0) goto L_0x0249
            y r0 = defpackage.y.I()
            boolean r0 = r0.bV
            if (r0 == 0) goto L_0x0249
            y r0 = defpackage.y.I()
            boolean r0 = r0.bW
            if (r0 == 0) goto L_0x0249
            r0 = 3
            r11.eE = r0
        L_0x005f:
            int r0 = r11.b
            r1 = 1
            if (r0 != r1) goto L_0x0557
            byte r0 = r11.aA
            r1 = 1
            if (r0 != r1) goto L_0x024e
            int[] r0 = r11.Z
            int r1 = r11.fA
            r0 = r0[r1]
            r11.ds = r0
        L_0x0071:
            int r0 = r11.ds
            switch(r0) {
                case 1: goto L_0x02d4;
                case 2: goto L_0x0339;
                case 3: goto L_0x039e;
                case 4: goto L_0x0403;
                default: goto L_0x0076;
            }
        L_0x0076:
            byte r0 = r11.aA
            switch(r0) {
                case 0: goto L_0x0468;
                case 1: goto L_0x0475;
                case 2: goto L_0x049d;
                case 3: goto L_0x04bc;
                default: goto L_0x007b;
            }
        L_0x007b:
            ak r0 = r11.ap
            ai r0 = r0.bE
            byte r0 = r0.aA
            if (r0 != 0) goto L_0x00a1
            defpackage.y.I()
            ai[] r0 = defpackage.y.bF
            defpackage.y.I()
            r1 = 0
            r0 = r0[r1]
            int r0 = r0.i
            if (r0 != 0) goto L_0x00a1
            defpackage.y.I()
            ai[] r0 = defpackage.y.bF
            defpackage.y.I()
            r1 = 0
            r0 = r0[r1]
            int r0 = r0.aa
            if (r0 == 0) goto L_0x0500
        L_0x00a1:
            defpackage.y.I()
            ai[] r0 = defpackage.y.bF
            defpackage.y.I()
            r1 = 0
            r0 = r0[r1]
            int r0 = r0.b
            r1 = 1
            if (r0 != r1) goto L_0x0500
            int[][] r0 = r11.aR
            if (r0 == 0) goto L_0x0500
            int[][] r0 = r11.aR
            int r0 = r0.length
            int r0 = r0 + -1
        L_0x00ba:
            if (r0 <= 0) goto L_0x04db
            int[][] r1 = r11.aR
            r1 = r1[r0]
            r2 = 0
            int[][] r3 = r11.aR
            int r4 = r0 + -1
            r3 = r3[r4]
            r4 = 0
            r3 = r3[r4]
            r1[r2] = r3
            int[][] r1 = r11.aR
            r1 = r1[r0]
            r2 = 1
            int[][] r3 = r11.aR
            int r4 = r0 + -1
            r3 = r3[r4]
            r4 = 1
            r3 = r3[r4]
            r1[r2] = r3
            int[][] r1 = r11.aR
            r1 = r1[r0]
            r2 = 2
            int[][] r3 = r11.aR
            int r4 = r0 + -1
            r3 = r3[r4]
            r4 = 2
            r3 = r3[r4]
            r1[r2] = r3
            int r0 = r0 + -1
            goto L_0x00ba
        L_0x00ef:
            byte r0 = r11.aj
            r1 = 1
            if (r0 != r1) goto L_0x000e
            int r0 = r11.h
            r1 = 2
            if (r0 == r1) goto L_0x0103
            int r0 = r11.h
            r1 = 3
            if (r0 == r1) goto L_0x0103
            int r0 = r11.h
            r1 = 4
            if (r0 != r1) goto L_0x01c5
        L_0x0103:
            int r0 = r11.ds
            switch(r0) {
                case 1: goto L_0x011f;
                case 2: goto L_0x0148;
                case 3: goto L_0x0171;
                case 4: goto L_0x019b;
                default: goto L_0x0108;
            }
        L_0x0108:
            aq r0 = r11.op
            int r0 = r0.i
            if (r0 < 0) goto L_0x000e
            int r1 = r11.fk
            if (r1 == r0) goto L_0x000e
            r11.fk = r0
            int r0 = r11.a(r0, r11)
            if (r0 > 0) goto L_0x000e
            r0 = 1
            r11.fc = r0
            goto L_0x000e
        L_0x011f:
            int r0 = r11.ab
            int r1 = r11.ab
            if (r0 <= r1) goto L_0x0108
            int r0 = r11.p
            int r1 = r11.p
            aq r2 = r11.op
            int r2 = r2.b
            int r2 = r2 >> 1
            int r1 = r1 + r2
            if (r0 >= r1) goto L_0x0108
            int r0 = r11.p
            aq r1 = r11.op
            int r1 = r1.b
            int r0 = r0 + r1
            int r1 = r11.p
            aq r2 = r11.op
            int r2 = r2.b
            int r2 = r2 >> 1
            int r1 = r1 + r2
            if (r0 <= r1) goto L_0x0108
            r0 = 2
            r11.fv = r0
            goto L_0x0108
        L_0x0148:
            int r0 = r11.ab
            int r1 = r11.ab
            if (r0 >= r1) goto L_0x0108
            int r0 = r11.p
            int r1 = r11.p
            aq r2 = r11.op
            int r2 = r2.b
            int r2 = r2 >> 1
            int r1 = r1 + r2
            if (r0 >= r1) goto L_0x0108
            int r0 = r11.p
            aq r1 = r11.op
            int r1 = r1.b
            int r0 = r0 + r1
            int r1 = r11.p
            aq r2 = r11.op
            int r2 = r2.b
            int r2 = r2 >> 1
            int r1 = r1 + r2
            if (r0 <= r1) goto L_0x0108
            r0 = 1
            r11.fv = r0
            goto L_0x0108
        L_0x0171:
            int r0 = r11.p
            int r1 = r11.p
            if (r0 <= r1) goto L_0x0108
            int r0 = r11.ab
            int r1 = r11.ab
            aq r2 = r11.op
            int r2 = r2.h
            int r2 = r2 >> 1
            int r1 = r1 + r2
            if (r0 >= r1) goto L_0x0108
            int r0 = r11.ab
            aq r1 = r11.op
            int r1 = r1.h
            int r0 = r0 + r1
            int r1 = r11.ab
            aq r2 = r11.op
            int r2 = r2.h
            int r2 = r2 >> 1
            int r1 = r1 + r2
            if (r0 <= r1) goto L_0x0108
            r0 = 4
            r11.fv = r0
            goto L_0x0108
        L_0x019b:
            int r0 = r11.p
            int r1 = r11.p
            if (r0 >= r1) goto L_0x0108
            int r0 = r11.ab
            int r1 = r11.ab
            aq r2 = r11.op
            int r2 = r2.h
            int r2 = r2 >> 1
            int r1 = r1 + r2
            if (r0 >= r1) goto L_0x0108
            int r0 = r11.ab
            aq r1 = r11.op
            int r1 = r1.h
            int r0 = r0 + r1
            int r1 = r11.ab
            aq r2 = r11.op
            int r2 = r2.h
            int r2 = r2 >> 1
            int r1 = r1 + r2
            if (r0 <= r1) goto L_0x0108
            r0 = 3
            r11.fv = r0
            goto L_0x0108
        L_0x01c5:
            int r0 = r11.h
            if (r0 == 0) goto L_0x01ce
            int r0 = r11.h
            r1 = 1
            if (r0 != r1) goto L_0x0108
        L_0x01ce:
            int r0 = r11.b
            if (r0 != 0) goto L_0x0108
            int r0 = r11.ds
            switch(r0) {
                case 1: goto L_0x01d9;
                case 2: goto L_0x01de;
                case 3: goto L_0x01e3;
                case 4: goto L_0x01e8;
                default: goto L_0x01d7;
            }
        L_0x01d7:
            goto L_0x0108
        L_0x01d9:
            r0 = 1
            r11.e = r0
            goto L_0x0108
        L_0x01de:
            r0 = 0
            r11.e = r0
            goto L_0x0108
        L_0x01e3:
            r0 = 2
            r11.e = r0
            goto L_0x0108
        L_0x01e8:
            r0 = 3
            r11.e = r0
            goto L_0x0108
        L_0x01ed:
            j r0 = r11.aH
            boolean r0 = r0.k
            if (r0 != 0) goto L_0x0034
            r0 = 0
            ak r1 = r11.ap
            u r1 = r1.vh
            aq[] r1 = r1.x()
            int r9 = r1.length
            r8 = r0
        L_0x01fe:
            if (r8 >= r9) goto L_0x0244
            ak r0 = r11.ap
            u r0 = r0.vh
            aq[] r0 = r0.x()
            r10 = r0[r8]
            boolean r0 = r10.k
            if (r0 == 0) goto L_0x0240
            int r0 = r11.p
            aq r1 = r11.op
            int r1 = r1.e
            int r0 = r0 + r1
            int r1 = r11.ab
            aq r2 = r11.op
            int r2 = r2.a
            int r1 = r1 + r2
            aq r2 = r11.op
            int r2 = r2.b
            aq r3 = r11.op
            int r3 = r3.h
            int r4 = r10.e
            int r5 = r10.a
            int r6 = r10.b
            int r7 = r10.h
            boolean r0 = defpackage.ah.c(r0, r1, r2, r3, r4, r5, r6, r7)
            if (r0 == 0) goto L_0x0240
            int r0 = r10.i
            int r1 = r11.fk
            if (r1 == r0) goto L_0x0034
            r11.fk = r0
            r1 = 0
            r11.a(r0, r1)
            goto L_0x0034
        L_0x0240:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x01fe
        L_0x0244:
            r0 = -1
            r11.fk = r0
            goto L_0x0034
        L_0x0249:
            r0 = 0
            r11.eE = r0
            goto L_0x005f
        L_0x024e:
            byte r0 = r11.aA
            r1 = 2
            if (r0 != r1) goto L_0x02c9
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 >= r1) goto L_0x026d
            r0 = 4
            r11.ds = r0
            int r0 = r11.p
            int r1 = r11.eA
            int r0 = r0 + r1
            r11.p = r0
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 < r1) goto L_0x026d
            int r0 = r11.fw
            r11.p = r0
        L_0x026d:
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 != r1) goto L_0x028d
            int r0 = r11.ab
            int r1 = r11.fx
            if (r0 >= r1) goto L_0x028d
            r0 = 2
            r11.ds = r0
            int r0 = r11.ab
            int r1 = r11.eA
            int r0 = r0 + r1
            r11.ab = r0
            int r0 = r11.ab
            int r1 = r11.fx
            if (r0 < r1) goto L_0x028d
            int r0 = r11.fx
            r11.ab = r0
        L_0x028d:
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 <= r1) goto L_0x02a7
            r0 = 3
            r11.ds = r0
            int r0 = r11.p
            int r1 = r11.eA
            int r0 = r0 - r1
            r11.p = r0
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 > r1) goto L_0x02a7
            int r0 = r11.fw
            r11.p = r0
        L_0x02a7:
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 != r1) goto L_0x0071
            int r0 = r11.ab
            int r1 = r11.fx
            if (r0 <= r1) goto L_0x0071
            r0 = 1
            r11.ds = r0
            int r0 = r11.ab
            int r1 = r11.eA
            int r0 = r0 - r1
            r11.ab = r0
            int r0 = r11.ab
            int r1 = r11.fx
            if (r0 > r1) goto L_0x0071
            int r0 = r11.fx
            r11.ab = r0
            goto L_0x0071
        L_0x02c9:
            byte r0 = r11.aA
            r1 = 3
            if (r0 != r1) goto L_0x0071
            int r0 = r11.eB
            r11.ds = r0
            goto L_0x0071
        L_0x02d4:
            r0 = 5
            r11.e = r0
            byte r0 = r11.aA
            switch(r0) {
                case 0: goto L_0x02de;
                case 1: goto L_0x02e6;
                case 2: goto L_0x030c;
                case 3: goto L_0x0311;
                default: goto L_0x02dc;
            }
        L_0x02dc:
            goto L_0x0076
        L_0x02de:
            int r0 = r11.eE
            int r0 = -4 - r0
            r11.aa = r0
            goto L_0x0076
        L_0x02e6:
            int r0 = r11.ab
            int[] r1 = r11.aq
            int r2 = r11.fA
            r1 = r1[r2]
            if (r0 <= r1) goto L_0x02fc
            r0 = 1
            r11.b = r0
            int r0 = r11.ab
            int r1 = r11.eA
            int r0 = r0 - r1
            r11.ab = r0
            goto L_0x0076
        L_0x02fc:
            int[] r0 = r11.aq
            int r1 = r11.fA
            r0 = r0[r1]
            r11.ab = r0
            int r0 = r11.fA
            int r0 = r0 + 1
            r11.fA = r0
            goto L_0x0076
        L_0x030c:
            r0 = -4
            r11.aa = r0
            goto L_0x0076
        L_0x0311:
            boolean r0 = r11.bT
            if (r0 != 0) goto L_0x0318
            r0 = 1
            r11.e = r0
        L_0x0318:
            byte r0 = r11.bj
            switch(r0) {
                case 1: goto L_0x031f;
                case 2: goto L_0x0326;
                case 3: goto L_0x032c;
                case 4: goto L_0x0333;
                default: goto L_0x031d;
            }
        L_0x031d:
            goto L_0x0076
        L_0x031f:
            int r0 = r11.eA
            int r0 = -r0
            r11.aa = r0
            goto L_0x0076
        L_0x0326:
            int r0 = r11.eA
            r11.aa = r0
            goto L_0x0076
        L_0x032c:
            int r0 = r11.eA
            int r0 = -r0
            r11.i = r0
            goto L_0x0076
        L_0x0333:
            int r0 = r11.eA
            r11.i = r0
            goto L_0x0076
        L_0x0339:
            r0 = 4
            r11.e = r0
            byte r0 = r11.aA
            switch(r0) {
                case 0: goto L_0x0343;
                case 1: goto L_0x034b;
                case 2: goto L_0x0371;
                case 3: goto L_0x0376;
                default: goto L_0x0341;
            }
        L_0x0341:
            goto L_0x0076
        L_0x0343:
            int r0 = r11.eE
            int r0 = r0 + 4
            r11.aa = r0
            goto L_0x0076
        L_0x034b:
            int r0 = r11.ab
            int[] r1 = r11.aq
            int r2 = r11.fA
            r1 = r1[r2]
            if (r0 >= r1) goto L_0x0361
            r0 = 1
            r11.b = r0
            int r0 = r11.ab
            int r1 = r11.eA
            int r0 = r0 + r1
            r11.ab = r0
            goto L_0x0076
        L_0x0361:
            int[] r0 = r11.aq
            int r1 = r11.fA
            r0 = r0[r1]
            r11.ab = r0
            int r0 = r11.fA
            int r0 = r0 + 1
            r11.fA = r0
            goto L_0x0076
        L_0x0371:
            r0 = 4
            r11.aa = r0
            goto L_0x0076
        L_0x0376:
            boolean r0 = r11.bT
            if (r0 != 0) goto L_0x037d
            r0 = 0
            r11.e = r0
        L_0x037d:
            byte r0 = r11.bj
            switch(r0) {
                case 1: goto L_0x0384;
                case 2: goto L_0x038b;
                case 3: goto L_0x0391;
                case 4: goto L_0x0398;
                default: goto L_0x0382;
            }
        L_0x0382:
            goto L_0x0076
        L_0x0384:
            int r0 = r11.eA
            int r0 = -r0
            r11.aa = r0
            goto L_0x0076
        L_0x038b:
            int r0 = r11.eA
            r11.aa = r0
            goto L_0x0076
        L_0x0391:
            int r0 = r11.eA
            int r0 = -r0
            r11.i = r0
            goto L_0x0076
        L_0x0398:
            int r0 = r11.eA
            r11.i = r0
            goto L_0x0076
        L_0x039e:
            r0 = 6
            r11.e = r0
            byte r0 = r11.aA
            switch(r0) {
                case 0: goto L_0x03a8;
                case 1: goto L_0x03b0;
                case 2: goto L_0x03d6;
                case 3: goto L_0x03db;
                default: goto L_0x03a6;
            }
        L_0x03a6:
            goto L_0x0076
        L_0x03a8:
            int r0 = r11.eE
            int r0 = -4 - r0
            r11.i = r0
            goto L_0x0076
        L_0x03b0:
            int r0 = r11.p
            int[] r1 = r11.aq
            int r2 = r11.fA
            r1 = r1[r2]
            if (r0 <= r1) goto L_0x03c6
            r0 = 1
            r11.b = r0
            int r0 = r11.p
            int r1 = r11.eA
            int r0 = r0 - r1
            r11.p = r0
            goto L_0x0076
        L_0x03c6:
            int[] r0 = r11.aq
            int r1 = r11.fA
            r0 = r0[r1]
            r11.p = r0
            int r0 = r11.fA
            int r0 = r0 + 1
            r11.fA = r0
            goto L_0x0076
        L_0x03d6:
            r0 = -4
            r11.i = r0
            goto L_0x0076
        L_0x03db:
            boolean r0 = r11.bT
            if (r0 != 0) goto L_0x03e2
            r0 = 2
            r11.e = r0
        L_0x03e2:
            byte r0 = r11.bj
            switch(r0) {
                case 1: goto L_0x03e9;
                case 2: goto L_0x03f0;
                case 3: goto L_0x03f6;
                case 4: goto L_0x03fd;
                default: goto L_0x03e7;
            }
        L_0x03e7:
            goto L_0x0076
        L_0x03e9:
            int r0 = r11.eA
            int r0 = -r0
            r11.aa = r0
            goto L_0x0076
        L_0x03f0:
            int r0 = r11.eA
            r11.aa = r0
            goto L_0x0076
        L_0x03f6:
            int r0 = r11.eA
            int r0 = -r0
            r11.i = r0
            goto L_0x0076
        L_0x03fd:
            int r0 = r11.eA
            r11.i = r0
            goto L_0x0076
        L_0x0403:
            r0 = 7
            r11.e = r0
            byte r0 = r11.aA
            switch(r0) {
                case 0: goto L_0x040d;
                case 1: goto L_0x0415;
                case 2: goto L_0x043b;
                case 3: goto L_0x0440;
                default: goto L_0x040b;
            }
        L_0x040b:
            goto L_0x0076
        L_0x040d:
            int r0 = r11.eE
            int r0 = r0 + 4
            r11.i = r0
            goto L_0x0076
        L_0x0415:
            int r0 = r11.p
            int[] r1 = r11.aq
            int r2 = r11.fA
            r1 = r1[r2]
            if (r0 >= r1) goto L_0x042b
            r0 = 1
            r11.b = r0
            int r0 = r11.p
            int r1 = r11.eA
            int r0 = r0 + r1
            r11.p = r0
            goto L_0x0076
        L_0x042b:
            int[] r0 = r11.aq
            int r1 = r11.fA
            r0 = r0[r1]
            r11.p = r0
            int r0 = r11.fA
            int r0 = r0 + 1
            r11.fA = r0
            goto L_0x0076
        L_0x043b:
            r0 = 4
            r11.i = r0
            goto L_0x0076
        L_0x0440:
            boolean r0 = r11.bT
            if (r0 != 0) goto L_0x0447
            r0 = 3
            r11.e = r0
        L_0x0447:
            byte r0 = r11.bj
            switch(r0) {
                case 1: goto L_0x044e;
                case 2: goto L_0x0455;
                case 3: goto L_0x045b;
                case 4: goto L_0x0462;
                default: goto L_0x044c;
            }
        L_0x044c:
            goto L_0x0076
        L_0x044e:
            int r0 = r11.eA
            int r0 = -r0
            r11.aa = r0
            goto L_0x0076
        L_0x0455:
            int r0 = r11.eA
            r11.aa = r0
            goto L_0x0076
        L_0x045b:
            int r0 = r11.eA
            int r0 = -r0
            r11.i = r0
            goto L_0x0076
        L_0x0462:
            int r0 = r11.eA
            r11.i = r0
            goto L_0x0076
        L_0x0468:
            r11.l()
            r0 = 0
            r11.b(r0)
            r0 = 1
            r11.u(r0)
            goto L_0x007b
        L_0x0475:
            int r0 = r11.fA
            int[] r1 = r11.Z
            int r1 = r1.length
            int r1 = r1 + -1
            if (r0 <= r1) goto L_0x049d
            r0 = 0
            r11.b = r0
            j r0 = r11.aH
            int r1 = r0.e
            int r1 = r1 + 1
            r0.e = r1
            r0 = 0
            r11.fA = r0
            j r0 = r11.aH
            r1 = 1
            r0.am = r1
            r0 = 0
            r11.aA = r0
            y r0 = defpackage.y.I()
            r1 = 0
            r0.bS = r1
            goto L_0x007b
        L_0x049d:
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 != r1) goto L_0x007b
            int r0 = r11.ab
            int r1 = r11.fx
            if (r0 != r1) goto L_0x007b
            r0 = 0
            r11.b = r0
            int r0 = r11.fy
            r11.ds = r0
            r0 = 0
            r11.aA = r0
            y r0 = defpackage.y.I()
            r1 = 0
            r0.bT = r1
            goto L_0x007b
        L_0x04bc:
            int r0 = r11.fu
            int r1 = r11.eA
            int r0 = r0 + r1
            r11.fu = r0
            int r0 = r11.fu
            int r1 = r11.eD
            if (r0 < r1) goto L_0x007b
            r0 = 0
            r11.fu = r0
            r0 = 0
            r11.eD = r0
            r0 = 0
            r11.aA = r0
            y r0 = defpackage.y.I()
            r1 = 0
            r0.bU = r1
            goto L_0x007b
        L_0x04db:
            int[][] r0 = r11.aR
            r1 = 0
            r0 = r0[r1]
            r1 = 0
            int r2 = r11.p
            r0[r1] = r2
            int[][] r0 = r11.aR
            r1 = 0
            r0 = r0[r1]
            r1 = 1
            int r2 = r11.ab
            r0[r1] = r2
            int[][] r0 = r11.aR
            r1 = 0
            r0 = r0[r1]
            r1 = 2
            int r2 = r11.ds
            r0[r1] = r2
            y r0 = defpackage.y.I()
            r0.S()
        L_0x0500:
            int r0 = r11.i
            if (r0 != 0) goto L_0x0508
            int r0 = r11.aa
            if (r0 == 0) goto L_0x051c
        L_0x0508:
            int r0 = r11.p
            int r1 = r11.i
            int r0 = r0 + r1
            r11.p = r0
            int r0 = r11.ab
            int r1 = r11.aa
            int r0 = r0 + r1
            r11.ab = r0
            r0 = 0
            r11.i = r0
            r0 = 0
            r11.aa = r0
        L_0x051c:
            r11.m()
            as r0 = r11.bh
            if (r0 == 0) goto L_0x0533
            byte r0 = r11.aA
            r1 = 3
            if (r0 != r1) goto L_0x052c
            boolean r0 = r11.bT
            if (r0 == 0) goto L_0x0533
        L_0x052c:
            as r0 = r11.bh
            int r1 = r11.e
            r0.f(r1)
        L_0x0533:
            ak r0 = r11.ap
            boolean r0 = r0.aM
            if (r0 != 0) goto L_0x0556
            ak r0 = r11.ap
            boolean r0 = r0.bR
            if (r0 != 0) goto L_0x0556
            ak r0 = r11.ap
            int r1 = r11.p
            r0.h = r1
            ak r0 = r11.ap
            int r1 = r11.ab
            r0.i = r1
            ak r0 = r11.ap
            u r0 = r0.vh
            int r1 = r11.p
            int r2 = r11.ab
            r0.a(r1, r2)
        L_0x0556:
            return
        L_0x0557:
            int r0 = r11.b
            if (r0 != 0) goto L_0x0533
            int r0 = r11.ds
            switch(r0) {
                case 1: goto L_0x0572;
                case 2: goto L_0x0579;
                case 3: goto L_0x0580;
                case 4: goto L_0x0587;
                default: goto L_0x0560;
            }
        L_0x0560:
            boolean r0 = r11.m()
            if (r0 == 0) goto L_0x058e
            as r0 = r11.bh
            if (r0 == 0) goto L_0x0533
            as r0 = r11.bh
            int r1 = r11.e
            r0.f(r1)
            goto L_0x0533
        L_0x0572:
            r0 = 1
            r11.e = r0
            r0 = -4
            r11.aa = r0
            goto L_0x0560
        L_0x0579:
            r0 = 0
            r11.e = r0
            r0 = 4
            r11.aa = r0
            goto L_0x0560
        L_0x0580:
            r0 = 2
            r11.e = r0
            r0 = -4
            r11.i = r0
            goto L_0x0560
        L_0x0587:
            r0 = 3
            r11.e = r0
            r0 = 4
            r11.i = r0
            goto L_0x0560
        L_0x058e:
            as r0 = r11.bh
            if (r0 == 0) goto L_0x0533
            as r0 = r11.bh
            int r1 = r11.e
            r2 = 0
            r0.a(r1, r2)
            goto L_0x0533
        L_0x059b:
            int r0 = r11.a
            r1 = 3
            if (r0 != r1) goto L_0x0609
            ak r0 = r11.ap
            ai r0 = r0.bE
            int r0 = r0.b
            r11.b = r0
            ak r0 = r11.ap
            ai r0 = r0.bE
            byte r0 = r0.aA
            if (r0 == 0) goto L_0x05c4
            ak r0 = r11.ap
            ai r0 = r0.bE
            byte r0 = r0.aA
            r1 = 3
            if (r0 != r1) goto L_0x05db
            ak r0 = r11.ap
            ai r0 = r0.bE
            boolean r0 = r0.bT
            if (r0 != 0) goto L_0x05db
            r0 = 0
            r11.b = r0
        L_0x05c4:
            int r0 = r11.b
            r1 = 1
            if (r0 != r1) goto L_0x05ef
            int r0 = r11.ds
            switch(r0) {
                case 1: goto L_0x05df;
                case 2: goto L_0x05e3;
                case 3: goto L_0x05e7;
                case 4: goto L_0x05eb;
                default: goto L_0x05ce;
            }
        L_0x05ce:
            as r0 = r11.bh
            if (r0 == 0) goto L_0x0556
            as r0 = r11.bh
            int r1 = r11.e
            r0.f(r1)
            goto L_0x0556
        L_0x05db:
            r0 = 1
            r11.b = r0
            goto L_0x05c4
        L_0x05df:
            r0 = 5
            r11.e = r0
            goto L_0x05ce
        L_0x05e3:
            r0 = 4
            r11.e = r0
            goto L_0x05ce
        L_0x05e7:
            r0 = 6
            r11.e = r0
            goto L_0x05ce
        L_0x05eb:
            r0 = 7
            r11.e = r0
            goto L_0x05ce
        L_0x05ef:
            int r0 = r11.b
            if (r0 != 0) goto L_0x05ce
            int r0 = r11.ds
            switch(r0) {
                case 1: goto L_0x05f9;
                case 2: goto L_0x05fd;
                case 3: goto L_0x0601;
                case 4: goto L_0x0605;
                default: goto L_0x05f8;
            }
        L_0x05f8:
            goto L_0x05ce
        L_0x05f9:
            r0 = 1
            r11.e = r0
            goto L_0x05ce
        L_0x05fd:
            r0 = 0
            r11.e = r0
            goto L_0x05ce
        L_0x0601:
            r0 = 2
            r11.e = r0
            goto L_0x05ce
        L_0x0605:
            r0 = 3
            r11.e = r0
            goto L_0x05ce
        L_0x0609:
            int r0 = r11.a
            r1 = 5
            if (r0 == r1) goto L_0x0556
            int r0 = r11.a
            r1 = 4
            if (r0 != r1) goto L_0x07fc
            int r0 = r11.fz
            if (r11 == 0) goto L_0x0556
            switch(r0) {
                case 1: goto L_0x0631;
                case 2: goto L_0x070e;
                default: goto L_0x061a;
            }
        L_0x061a:
            int r0 = r11.b
            r1 = 1
            if (r0 != r1) goto L_0x07dd
            int r0 = r11.ds
            switch(r0) {
                case 1: goto L_0x07c9;
                case 2: goto L_0x07ce;
                case 3: goto L_0x07d3;
                case 4: goto L_0x07d8;
                default: goto L_0x0624;
            }
        L_0x0624:
            as r0 = r11.bh
            if (r0 == 0) goto L_0x0556
            as r0 = r11.bh
            int r1 = r11.e
            r0.f(r1)
            goto L_0x0556
        L_0x0631:
            int[] r0 = r11.Z
            int r1 = r11.fA
            r0 = r0[r1]
            r11.ds = r0
            int r0 = r11.ds
            switch(r0) {
                case 1: goto L_0x066b;
                case 2: goto L_0x068f;
                case 3: goto L_0x06b3;
                case 4: goto L_0x06d9;
                default: goto L_0x063e;
            }
        L_0x063e:
            int r0 = r11.fA
            int[] r1 = r11.Z
            int r1 = r1.length
            int r1 = r1 + -1
            if (r0 <= r1) goto L_0x061a
            r0 = 0
            r11.b = r0
            j r0 = r11.aH
            int r1 = r0.e
            int r1 = r1 + 1
            r0.e = r1
            r0 = 1
            r11.a = r0
            r0 = 0
            r11.ao = r0
            int r0 = r11.h
            r1 = 2
            if (r0 != r1) goto L_0x06ff
            r0 = 1
            r11.fc = r0
        L_0x0660:
            r0 = 0
            r11.fA = r0
            y r0 = defpackage.y.I()
            r1 = 0
            r0.aM = r1
            goto L_0x061a
        L_0x066b:
            int r0 = r11.ab
            int[] r1 = r11.aq
            int r2 = r11.fA
            r1 = r1[r2]
            if (r0 <= r1) goto L_0x0680
            r0 = 1
            r11.b = r0
            int r0 = r11.ab
            int r1 = r11.eA
            int r0 = r0 - r1
            r11.ab = r0
            goto L_0x063e
        L_0x0680:
            int[] r0 = r11.aq
            int r1 = r11.fA
            r0 = r0[r1]
            r11.ab = r0
            int r0 = r11.fA
            int r0 = r0 + 1
            r11.fA = r0
            goto L_0x063e
        L_0x068f:
            int r0 = r11.ab
            int[] r1 = r11.aq
            int r2 = r11.fA
            r1 = r1[r2]
            if (r0 >= r1) goto L_0x06a4
            r0 = 1
            r11.b = r0
            int r0 = r11.ab
            int r1 = r11.eA
            int r0 = r0 + r1
            r11.ab = r0
            goto L_0x063e
        L_0x06a4:
            int[] r0 = r11.aq
            int r1 = r11.fA
            r0 = r0[r1]
            r11.ab = r0
            int r0 = r11.fA
            int r0 = r0 + 1
            r11.fA = r0
            goto L_0x063e
        L_0x06b3:
            int r0 = r11.p
            int[] r1 = r11.aq
            int r2 = r11.fA
            r1 = r1[r2]
            if (r0 <= r1) goto L_0x06c9
            r0 = 1
            r11.b = r0
            int r0 = r11.p
            int r1 = r11.eA
            int r0 = r0 - r1
            r11.p = r0
            goto L_0x063e
        L_0x06c9:
            int[] r0 = r11.aq
            int r1 = r11.fA
            r0 = r0[r1]
            r11.p = r0
            int r0 = r11.fA
            int r0 = r0 + 1
            r11.fA = r0
            goto L_0x063e
        L_0x06d9:
            int r0 = r11.p
            int[] r1 = r11.aq
            int r2 = r11.fA
            r1 = r1[r2]
            if (r0 >= r1) goto L_0x06ef
            r0 = 1
            r11.b = r0
            int r0 = r11.p
            int r1 = r11.eA
            int r0 = r0 + r1
            r11.p = r0
            goto L_0x063e
        L_0x06ef:
            int[] r0 = r11.aq
            int r1 = r11.fA
            r0 = r0[r1]
            r11.p = r0
            int r0 = r11.fA
            int r0 = r0 + 1
            r11.fA = r0
            goto L_0x063e
        L_0x06ff:
            r0 = 0
            r11.fc = r0
            int[] r0 = r11.Z
            int r1 = r11.fA
            int r1 = r1 + -1
            r0 = r0[r1]
            r11.ds = r0
            goto L_0x0660
        L_0x070e:
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 != r1) goto L_0x0747
            int r0 = r11.ab
            int r1 = r11.fx
            if (r0 != r1) goto L_0x0747
            r0 = 0
            r11.b = r0
            int r0 = r11.fy
            r11.ds = r0
            j r0 = r11.aH
            int r1 = r0.e
            int r1 = r1 + 1
            r0.e = r1
            r0 = 1
            r11.a = r0
            r0 = 0
            r11.ao = r0
            int r0 = r11.h
            r1 = 2
            if (r0 != r1) goto L_0x0743
            r0 = 1
            r11.fc = r0
        L_0x0737:
            r0 = 0
            r11.fA = r0
            y r0 = defpackage.y.I()
            r1 = 0
            r0.bR = r1
            goto L_0x061a
        L_0x0743:
            r0 = 0
            r11.fc = r0
            goto L_0x0737
        L_0x0747:
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 >= r1) goto L_0x0764
            r0 = 1
            r11.b = r0
            r0 = 4
            r11.ds = r0
            int r0 = r11.p
            int r1 = r11.eA
            int r0 = r0 + r1
            r11.p = r0
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 < r1) goto L_0x0764
            int r0 = r11.fw
            r11.p = r0
        L_0x0764:
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 != r1) goto L_0x0787
            int r0 = r11.ab
            int r1 = r11.fx
            if (r0 >= r1) goto L_0x0787
            r0 = 1
            r11.b = r0
            r0 = 2
            r11.ds = r0
            int r0 = r11.ab
            int r1 = r11.eA
            int r0 = r0 + r1
            r11.ab = r0
            int r0 = r11.ab
            int r1 = r11.fx
            if (r0 < r1) goto L_0x0787
            int r0 = r11.fx
            r11.ab = r0
        L_0x0787:
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 <= r1) goto L_0x07a4
            r0 = 1
            r11.b = r0
            r0 = 3
            r11.ds = r0
            int r0 = r11.p
            int r1 = r11.eA
            int r0 = r0 - r1
            r11.p = r0
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 > r1) goto L_0x07a4
            int r0 = r11.fw
            r11.p = r0
        L_0x07a4:
            int r0 = r11.p
            int r1 = r11.fw
            if (r0 != r1) goto L_0x061a
            int r0 = r11.ab
            int r1 = r11.fx
            if (r0 <= r1) goto L_0x061a
            r0 = 1
            r11.b = r0
            r0 = 1
            r11.ds = r0
            int r0 = r11.ab
            int r1 = r11.eA
            int r0 = r0 - r1
            r11.ab = r0
            int r0 = r11.ab
            int r1 = r11.fx
            if (r0 > r1) goto L_0x061a
            int r0 = r11.fx
            r11.ab = r0
            goto L_0x061a
        L_0x07c9:
            r0 = 5
            r11.e = r0
            goto L_0x0624
        L_0x07ce:
            r0 = 4
            r11.e = r0
            goto L_0x0624
        L_0x07d3:
            r0 = 6
            r11.e = r0
            goto L_0x0624
        L_0x07d8:
            r0 = 7
            r11.e = r0
            goto L_0x0624
        L_0x07dd:
            int r0 = r11.b
            if (r0 != 0) goto L_0x0624
            int r0 = r11.ds
            switch(r0) {
                case 1: goto L_0x07e8;
                case 2: goto L_0x07ed;
                case 3: goto L_0x07f2;
                case 4: goto L_0x07f7;
                default: goto L_0x07e6;
            }
        L_0x07e6:
            goto L_0x0624
        L_0x07e8:
            r0 = 1
            r11.e = r0
            goto L_0x0624
        L_0x07ed:
            r0 = 0
            r11.e = r0
            goto L_0x0624
        L_0x07f2:
            r0 = 2
            r11.e = r0
            goto L_0x0624
        L_0x07f7:
            r0 = 3
            r11.e = r0
            goto L_0x0624
        L_0x07fc:
            int r0 = r11.a
            r1 = 5
            if (r0 == r1) goto L_0x0556
            int r0 = r11.a
            r1 = 4
            if (r0 == r1) goto L_0x08c7
            int r0 = r11.h
            if (r0 != 0) goto L_0x0817
            j r0 = r11.aH
            int r0 = r0.a
            r1 = 13
            if (r0 != r1) goto L_0x0556
            j r0 = r11.aH
            r1 = -1
            r0.a = r1
        L_0x0817:
            int r0 = r11.h
            r1 = 1
            if (r0 != r1) goto L_0x08e8
            r0 = 0
            r11.b = r0
            r0 = 0
            r11.e = r0
        L_0x0822:
            int r0 = r11.b
            r1 = 1
            if (r0 != r1) goto L_0x0a84
            int r0 = r11.ds
            switch(r0) {
                case 1: goto L_0x0a5e;
                case 2: goto L_0x0a68;
                case 3: goto L_0x0a71;
                case 4: goto L_0x0a7b;
                default: goto L_0x082c;
            }
        L_0x082c:
            boolean r0 = r11.ao
            if (r0 != 0) goto L_0x085a
            int r0 = r11.b
            if (r0 == 0) goto L_0x084b
            l r0 = defpackage.l.t()
            int r0 = r0.u()
            int r0 = r0 % 80
            if (r0 != 0) goto L_0x084b
            r0 = 1
            r1 = 4
            int r0 = r11.e(r0, r1)
            r11.fv = r0
            r0 = 0
            r11.bU = r0
        L_0x084b:
            boolean r0 = r11.bU
            if (r0 != 0) goto L_0x085a
            int r0 = r11.fh
            int r1 = r0 + -1
            r11.fh = r1
            if (r0 <= 0) goto L_0x0aa3
            r0 = 0
            r11.fc = r0
        L_0x085a:
            int r0 = r11.h
            r1 = 3
            if (r0 == r1) goto L_0x0864
            int r0 = r11.h
            r1 = 4
            if (r0 != r1) goto L_0x0893
        L_0x0864:
            y r0 = defpackage.y.I()
            ak r0 = r0.ap
            boolean r0 = r0.k
            r1 = 1
            if (r0 == r1) goto L_0x0893
            ak r0 = r11.ap
            int r0 = r0.e
            if (r0 != 0) goto L_0x0885
            y r0 = defpackage.y.I()
            boolean r0 = r0.bV
            if (r0 == 0) goto L_0x0885
            y r0 = defpackage.y.I()
            boolean r0 = r0.bW
            if (r0 != 0) goto L_0x0893
        L_0x0885:
            boolean r0 = r11.am
            if (r0 == 0) goto L_0x0893
            boolean r0 = r11.k
            if (r0 == 0) goto L_0x0893
            j r0 = r11.aH
            boolean r0 = r0.k
            if (r0 == 0) goto L_0x0aaf
        L_0x0893:
            r0 = 1
            boolean r0 = r11.u(r0)
            if (r0 != 0) goto L_0x08a0
            boolean r0 = r11.l()
            if (r0 == 0) goto L_0x08ab
        L_0x08a0:
            r0 = 1
            r1 = 4
            int r0 = r11.e(r0, r1)
            r11.fv = r0
            r0 = 0
            r11.bU = r0
        L_0x08ab:
            int r0 = r11.i
            if (r0 != 0) goto L_0x08b3
            int r0 = r11.aa
            if (r0 == 0) goto L_0x08c7
        L_0x08b3:
            int r0 = r11.p
            int r1 = r11.i
            int r0 = r0 + r1
            r11.p = r0
            int r0 = r11.ab
            int r1 = r11.aa
            int r0 = r0 + r1
            r11.ab = r0
            r0 = 0
            r11.i = r0
            r0 = 0
            r11.aa = r0
        L_0x08c7:
            int r0 = r11.h
            r1 = 1
            if (r0 == r1) goto L_0x08d1
            int r0 = r11.h
            r1 = 4
            if (r0 != r1) goto L_0x08d4
        L_0x08d1:
            r0 = 0
            r11.e = r0
        L_0x08d4:
            as r0 = r11.bh
            if (r0 == 0) goto L_0x0556
            defpackage.y.I()
            boolean r0 = defpackage.y.an
            if (r0 != 0) goto L_0x0556
            as r0 = r11.bh
            int r1 = r11.e
            r0.f(r1)
            goto L_0x0556
        L_0x08e8:
            int r0 = r11.h
            r1 = 2
            if (r0 != r1) goto L_0x0a37
            boolean r0 = r11.am
            if (r0 == 0) goto L_0x08f5
            boolean r0 = r11.k
            if (r0 != 0) goto L_0x0902
        L_0x08f5:
            r0 = 2
            defpackage.ai.eF = r0
            int r0 = r11.fc
            r11.b = r0
            int r0 = r11.fv
            r11.ds = r0
            goto L_0x0822
        L_0x0902:
            ak r0 = r11.ap
            int r0 = r0.e
            if (r0 != 0) goto L_0x0918
            y r0 = defpackage.y.I()
            boolean r0 = r0.bV
            if (r0 == 0) goto L_0x0918
            y r0 = defpackage.y.I()
            boolean r0 = r0.bW
            if (r0 != 0) goto L_0x08f5
        L_0x0918:
            j r0 = r11.aH
            boolean r0 = r0.k
            if (r0 != 0) goto L_0x0a25
            boolean r0 = r11.bV
            if (r0 == 0) goto L_0x0a25
            r0 = 0
            r8 = r0
        L_0x0924:
            ai[] r0 = defpackage.y.bF
            int r0 = r0.length
            if (r8 >= r0) goto L_0x0a25
            int r0 = r11.p
            aq r1 = r11.op
            int r1 = r1.e
            int r0 = r0 + r1
            int r1 = r11.i
            int r0 = r0 + r1
            int r1 = r11.ab
            aq r2 = r11.op
            int r2 = r2.a
            int r1 = r1 + r2
            int r2 = r11.aa
            int r1 = r1 + r2
            aq r2 = r11.op
            int r2 = r2.b
            aq r3 = r11.op
            int r3 = r3.h
            ai[] r4 = defpackage.y.bF
            r4 = r4[r8]
            int r4 = r4.p
            ai[] r5 = defpackage.y.bF
            r5 = r5[r8]
            aq r5 = r5.op
            int r5 = r5.e
            int r4 = r4 + r5
            ai[] r5 = defpackage.y.bF
            r5 = r5[r8]
            int r5 = r5.ab
            ai[] r6 = defpackage.y.bF
            r6 = r6[r8]
            aq r6 = r6.op
            int r6 = r6.a
            int r5 = r5 + r6
            ai[] r6 = defpackage.y.bF
            r6 = r6[r8]
            aq r6 = r6.op
            int r6 = r6.b
            ai[] r7 = defpackage.y.bF
            r7 = r7[r8]
            aq r7 = r7.op
            int r7 = r7.h
            boolean r0 = defpackage.ah.c(r0, r1, r2, r3, r4, r5, r6, r7)
            if (r0 == 0) goto L_0x0a1c
            int r0 = r11.fh
            int r1 = r0 + -1
            r11.fh = r1
            if (r0 > 0) goto L_0x0a21
            ak r0 = r11.ap
            d r0 = r0.vi
            boolean r0 = r0.l()
            if (r0 == 0) goto L_0x0995
            ak r0 = r11.ap
            d r0 = r0.vi
            boolean r0 = r0.m()
            if (r0 != 0) goto L_0x0a21
        L_0x0995:
            r0 = 10
            r11.ft = r0
            r0 = 1
            r1 = 3
            int r0 = r11.e(r0, r1)
            ai[] r1 = defpackage.y.bF
            r1 = r1[r8]
            int r1 = r1.p
            int r2 = r11.p
            if (r1 <= r2) goto L_0x09bb
            r1 = 1
            if (r0 != r1) goto L_0x09af
            r1 = 1
            r11.fv = r1
        L_0x09af:
            r1 = 2
            if (r0 != r1) goto L_0x09b5
            r1 = 3
            r11.fv = r1
        L_0x09b5:
            r1 = 3
            if (r0 != r1) goto L_0x09bb
            r1 = 4
            r11.fv = r1
        L_0x09bb:
            ai[] r1 = defpackage.y.bF
            r1 = r1[r8]
            int r1 = r1.p
            int r2 = r11.p
            if (r1 >= r2) goto L_0x09d7
            r1 = 1
            if (r0 != r1) goto L_0x09cb
            r1 = 2
            r11.fv = r1
        L_0x09cb:
            r1 = 2
            if (r0 != r1) goto L_0x09d1
            r1 = 3
            r11.fv = r1
        L_0x09d1:
            r1 = 3
            if (r0 != r1) goto L_0x09d7
            r1 = 4
            r11.fv = r1
        L_0x09d7:
            ai[] r1 = defpackage.y.bF
            r1 = r1[r8]
            int r1 = r1.ab
            int r2 = r11.ab
            if (r1 >= r2) goto L_0x09f3
            r1 = 1
            if (r0 != r1) goto L_0x09e7
            r1 = 1
            r11.fv = r1
        L_0x09e7:
            r1 = 2
            if (r0 != r1) goto L_0x09ed
            r1 = 4
            r11.fv = r1
        L_0x09ed:
            r1 = 3
            if (r0 != r1) goto L_0x09f3
            r1 = 2
            r11.fv = r1
        L_0x09f3:
            ai[] r1 = defpackage.y.bF
            r1 = r1[r8]
            int r1 = r1.ab
            int r2 = r11.ab
            if (r1 <= r2) goto L_0x0a0f
            r1 = 1
            if (r0 != r1) goto L_0x0a03
            r1 = 3
            r11.fv = r1
        L_0x0a03:
            r1 = 2
            if (r0 != r1) goto L_0x0a09
            r1 = 2
            r11.fv = r1
        L_0x0a09:
            r1 = 3
            if (r0 != r1) goto L_0x0a0f
            r0 = 1
            r11.fv = r0
        L_0x0a0f:
            r0 = 1
            r11.fc = r0
            r0 = 10
            r11.fh = r0
        L_0x0a16:
            r0 = 0
            r11.i = r0
            r0 = 0
            r11.aa = r0
        L_0x0a1c:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x0924
        L_0x0a21:
            r0 = 0
            r11.fc = r0
            goto L_0x0a16
        L_0x0a25:
            int r0 = r11.ft
            int r1 = r0 + -1
            r11.ft = r1
            if (r0 <= 0) goto L_0x0a32
            r0 = 0
            r11.bV = r0
            goto L_0x08f5
        L_0x0a32:
            r0 = 1
            r11.bV = r0
            goto L_0x08f5
        L_0x0a37:
            int r0 = r11.h
            r1 = 3
            if (r0 != r1) goto L_0x0a49
            r0 = 2
            defpackage.ai.eF = r0
            int r0 = r11.fc
            r11.b = r0
            int r0 = r11.fv
            r11.ds = r0
            goto L_0x0822
        L_0x0a49:
            int r0 = r11.h
            r1 = 4
            if (r0 != r1) goto L_0x0822
            r0 = 0
            r11.e = r0
            r0 = 2
            defpackage.ai.eF = r0
            int r0 = r11.fc
            r11.b = r0
            int r0 = r11.fv
            r11.ds = r0
            goto L_0x0822
        L_0x0a5e:
            r0 = 5
            r11.e = r0
            int r0 = defpackage.ai.eF
            int r0 = -r0
            r11.aa = r0
            goto L_0x082c
        L_0x0a68:
            r0 = 4
            r11.e = r0
            int r0 = defpackage.ai.eF
            r11.aa = r0
            goto L_0x082c
        L_0x0a71:
            r0 = 6
            r11.e = r0
            int r0 = defpackage.ai.eF
            int r0 = -r0
            r11.i = r0
            goto L_0x082c
        L_0x0a7b:
            r0 = 7
            r11.e = r0
            int r0 = defpackage.ai.eF
            r11.i = r0
            goto L_0x082c
        L_0x0a84:
            int r0 = r11.b
            if (r0 != 0) goto L_0x082c
            int r0 = r11.ds
            switch(r0) {
                case 1: goto L_0x0a8f;
                case 2: goto L_0x0a94;
                case 3: goto L_0x0a99;
                case 4: goto L_0x0a9e;
                default: goto L_0x0a8d;
            }
        L_0x0a8d:
            goto L_0x082c
        L_0x0a8f:
            r0 = 1
            r11.e = r0
            goto L_0x082c
        L_0x0a94:
            r0 = 0
            r11.e = r0
            goto L_0x082c
        L_0x0a99:
            r0 = 2
            r11.e = r0
            goto L_0x082c
        L_0x0a9e:
            r0 = 3
            r11.e = r0
            goto L_0x082c
        L_0x0aa3:
            r0 = 1
            r11.fc = r0
            r0 = 1
            r11.bU = r0
            r0 = 10
            r11.fh = r0
            goto L_0x085a
        L_0x0aaf:
            ak r0 = r11.ap
            ai r8 = r0.bE
            int r0 = r11.p
            aq r1 = r11.oq
            int r1 = r1.e
            int r0 = r0 + r1
            int r1 = r11.i
            int r0 = r0 + r1
            int r1 = r11.ab
            aq r2 = r11.oq
            int r2 = r2.a
            int r1 = r1 + r2
            int r2 = r11.aa
            int r1 = r1 + r2
            aq r2 = r11.oq
            int r2 = r2.b
            aq r3 = r11.oq
            int r3 = r3.h
            int r4 = r8.p
            aq r5 = r8.op
            int r5 = r5.e
            int r4 = r4 + r5
            int r5 = r8.ab
            aq r6 = r8.op
            int r6 = r6.a
            int r5 = r5 + r6
            aq r6 = r8.op
            int r6 = r6.b
            aq r7 = r8.op
            int r7 = r7.h
            boolean r0 = defpackage.ah.c(r0, r1, r2, r3, r4, r5, r6, r7)
            if (r0 == 0) goto L_0x0c84
            r0 = 0
            defpackage.ai.eP = r0
            int r0 = r11.p
            aq r1 = r11.op
            int r1 = r1.e
            int r0 = r0 + r1
            int r1 = r11.i
            int r0 = r0 + r1
            int r1 = r11.ab
            aq r2 = r11.op
            int r2 = r2.a
            int r1 = r1 + r2
            int r2 = r11.aa
            int r1 = r1 + r2
            aq r2 = r11.op
            int r2 = r2.b
            aq r3 = r11.op
            int r3 = r3.h
            int r4 = r8.p
            aq r5 = r8.op
            int r5 = r5.e
            int r4 = r4 + r5
            int r5 = r8.ab
            aq r6 = r8.op
            int r6 = r6.a
            int r5 = r5 + r6
            aq r6 = r8.op
            int r6 = r6.b
            aq r7 = r8.op
            int r7 = r7.h
            boolean r0 = defpackage.ah.c(r0, r1, r2, r3, r4, r5, r6, r7)
            if (r0 == 0) goto L_0x0b5b
            r0 = 0
            r11.b = r0
            r0 = 0
            r11.i = r0
            r0 = 0
            r11.aa = r0
            r0 = 0
            r8.b = r0
            r0 = 0
            r8.i = r0
            r0 = 0
            r8.aa = r0
            ak r0 = r11.ap
            int r0 = r0.aa
            r1 = 7
            if (r0 != r1) goto L_0x0b42
            r0 = 0
            r11.k = r0
        L_0x0b42:
            l r0 = defpackage.l.t()
            r1 = 1
            r0.b(r1)
            aq r0 = r11.op
            int r0 = r0.i
            if (r0 < 0) goto L_0x0893
            int r1 = r11.fl
            if (r1 == r0) goto L_0x0893
            r11.fl = r0
            r11.a(r0, r11)
            goto L_0x0893
        L_0x0b5b:
            int r0 = r8.p
            int r1 = r11.p
            if (r0 <= r1) goto L_0x0bef
            int r0 = r8.ab
            int r1 = r11.ab
            if (r0 <= r1) goto L_0x0baf
            int r0 = r8.p
            int r1 = r11.p
            int r0 = r0 - r1
            int r1 = r8.ab
            int r2 = r11.ab
            int r1 = r1 - r2
            if (r0 <= r1) goto L_0x0b93
            int r0 = r8.p
            int r1 = r11.p
            if (r0 != r1) goto L_0x0b81
            r0 = 2
            r11.fv = r0
        L_0x0b7c:
            r0 = -1
            r11.fl = r0
            goto L_0x0893
        L_0x0b81:
            r0 = 1
            boolean r0 = r11.u(r0)
            if (r0 == 0) goto L_0x0b8f
            r0 = 2
            r11.fv = r0
            r0 = 4
            r11.e = r0
            goto L_0x0b7c
        L_0x0b8f:
            r0 = 4
            r11.fv = r0
            goto L_0x0b7c
        L_0x0b93:
            int r0 = r8.ab
            int r1 = r11.ab
            if (r0 != r1) goto L_0x0b9d
            r0 = 4
            r11.fv = r0
            goto L_0x0b7c
        L_0x0b9d:
            r0 = 1
            boolean r0 = r11.u(r0)
            if (r0 == 0) goto L_0x0bab
            r0 = 4
            r11.fv = r0
            r0 = 7
            r11.e = r0
            goto L_0x0b7c
        L_0x0bab:
            r0 = 2
            r11.fv = r0
            goto L_0x0b7c
        L_0x0baf:
            int r0 = r8.p
            int r1 = r11.p
            int r0 = r0 - r1
            int r1 = r11.ab
            int r2 = r8.ab
            int r1 = r1 - r2
            if (r0 <= r1) goto L_0x0bd3
            int r0 = r8.p
            int r1 = r11.p
            if (r0 == r1) goto L_0x0c7f
            r0 = 1
            boolean r0 = r11.u(r0)
            if (r0 == 0) goto L_0x0bcf
            r0 = 1
            r11.fv = r0
            r0 = 5
            r11.e = r0
            goto L_0x0b7c
        L_0x0bcf:
            r0 = 4
            r11.fv = r0
            goto L_0x0b7c
        L_0x0bd3:
            int r0 = r8.ab
            int r1 = r11.ab
            if (r0 != r1) goto L_0x0bdd
            r0 = 4
            r11.fv = r0
            goto L_0x0b7c
        L_0x0bdd:
            r0 = 1
            boolean r0 = r11.u(r0)
            if (r0 == 0) goto L_0x0beb
            r0 = 4
            r11.fv = r0
            r0 = 7
            r11.e = r0
            goto L_0x0b7c
        L_0x0beb:
            r0 = 1
            r11.fv = r0
            goto L_0x0b7c
        L_0x0bef:
            int r0 = r8.ab
            int r1 = r11.ab
            if (r0 <= r1) goto L_0x0c3f
            int r0 = r11.p
            int r1 = r8.p
            int r0 = r0 - r1
            int r1 = r8.ab
            int r2 = r11.ab
            int r1 = r1 - r2
            if (r0 <= r1) goto L_0x0c20
            int r0 = r8.p
            int r1 = r11.p
            if (r0 != r1) goto L_0x0c0c
            r0 = 2
            r11.fv = r0
            goto L_0x0b7c
        L_0x0c0c:
            r0 = 1
            boolean r0 = r11.u(r0)
            if (r0 == 0) goto L_0x0c1b
            r0 = 2
            r11.fv = r0
            r0 = 4
            r11.e = r0
            goto L_0x0b7c
        L_0x0c1b:
            r0 = 3
            r11.fv = r0
            goto L_0x0b7c
        L_0x0c20:
            int r0 = r8.ab
            int r1 = r11.ab
            if (r0 != r1) goto L_0x0c2b
            r0 = 3
            r11.fv = r0
            goto L_0x0b7c
        L_0x0c2b:
            r0 = 1
            boolean r0 = r11.u(r0)
            if (r0 == 0) goto L_0x0c3a
            r0 = 3
            r11.fv = r0
            r0 = 6
            r11.e = r0
            goto L_0x0b7c
        L_0x0c3a:
            r0 = 2
            r11.fv = r0
            goto L_0x0b7c
        L_0x0c3f:
            int r0 = r8.p
            int r1 = r11.p
            int r0 = r0 - r1
            int r1 = r11.ab
            int r2 = r8.ab
            int r1 = r1 - r2
            if (r0 <= r1) goto L_0x0c65
            int r0 = r8.p
            int r1 = r11.p
            if (r0 == r1) goto L_0x0c7f
            r0 = 1
            boolean r0 = r11.u(r0)
            if (r0 == 0) goto L_0x0c60
            r0 = 1
            r11.fv = r0
            r0 = 5
            r11.e = r0
            goto L_0x0b7c
        L_0x0c60:
            r0 = 3
            r11.fv = r0
            goto L_0x0b7c
        L_0x0c65:
            int r0 = r8.ab
            int r1 = r11.ab
            if (r0 != r1) goto L_0x0c70
            r0 = 3
            r11.fv = r0
            goto L_0x0b7c
        L_0x0c70:
            r0 = 1
            boolean r0 = r11.u(r0)
            if (r0 == 0) goto L_0x0c7f
            r0 = 3
            r11.fv = r0
            r0 = 6
            r11.e = r0
            goto L_0x0b7c
        L_0x0c7f:
            r0 = 1
            r11.fv = r0
            goto L_0x0b7c
        L_0x0c84:
            r0 = 0
            defpackage.ai.eP = r0
            goto L_0x0893
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ai.o():void");
    }

    public final void q() {
        this.aR = null;
        y.I().F(4);
        y.I();
        this.aR = (int[][]) Array.newInstance(Integer.TYPE, ((y.bF.length - 1) * y.I().aj) + 1, 3);
        this.aR[0][0] = this.p;
        this.aR[0][1] = this.ab;
        this.aR[0][2] = this.ds;
        for (int i2 = 1; i2 < this.aR.length; i2++) {
            this.aR[i2][0] = this.aR[i2 - 1][0];
            this.aR[i2][1] = this.aR[i2 - 1][1];
            this.aR[i2][2] = this.aR[i2 - 1][2];
            switch (this.ds) {
                case 1:
                    this.aR[i2][1] = (short) (this.aR[i2 - 1][1] + 4);
                    break;
                case 2:
                    this.aR[i2][1] = (short) (this.aR[i2 - 1][1] - 4);
                    break;
                case 3:
                    this.aR[i2][0] = (short) (this.aR[i2 - 1][0] + 4);
                    break;
                case 4:
                    this.aR[i2][0] = (short) (this.aR[i2 - 1][0] - 4);
                    break;
            }
        }
    }

    public final boolean u(boolean z2) {
        if (!this.am || !this.k) {
            return false;
        }
        if (this.or == null) {
            this.or = new aq();
        }
        aq aqVar = this.or;
        int i2 = this.p + this.op.e + this.i;
        int i3 = this.ab + this.op.a + this.aa;
        int i4 = this.op.b;
        int i5 = this.op.h;
        aqVar.e = i2;
        aqVar.a = i3;
        aqVar.b = i4;
        aqVar.h = i5;
        aqVar.i = -1;
        if (this.ap.vh.a(this.or) == null) {
            return false;
        }
        if (this.ap.e == 0 && y.I().bV && y.I().bW) {
            return true;
        }
        this.i = 0;
        this.aa = 0;
        return true;
    }

    public final void v() {
        y.I().F(4);
        y.I();
        int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, ((y.bF.length - 1) * y.I().aj) + 1, 3);
        iArr[0][0] = this.aR[0][0];
        iArr[0][1] = this.aR[0][1];
        iArr[0][2] = this.aR[0][2];
        for (int i2 = 1; i2 < iArr.length; i2++) {
            iArr[i2][0] = this.p;
            iArr[i2][1] = this.ab;
            iArr[i2][2] = this.ds;
            switch (this.ds) {
                case 1:
                    iArr[i2][1] = (short) (iArr[i2 - 1][1] + 4);
                    break;
                case 2:
                    iArr[i2][1] = (short) (iArr[i2 - 1][1] - 4);
                    break;
                case 3:
                    iArr[i2][0] = (short) (iArr[i2 - 1][0] + 4);
                    break;
                case 4:
                    iArr[i2][0] = (short) (iArr[i2 - 1][0] - 4);
                    break;
            }
        }
        this.aR = null;
        this.aR = iArr;
    }
}
