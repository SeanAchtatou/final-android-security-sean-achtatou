package defpackage;

import com.a.a.e.o;
import com.a.a.e.p;
import java.util.Random;

/* renamed from: an  reason: default package */
public final class an {
    private int[] X = new int[200];
    private int[] Z = new int[200];
    private int a = 320;
    private int[] aq = new int[200];
    private int[] ar = new int[200];
    private Random as = new Random();
    private p[] aw = null;
    private p[] dS;
    private int e = 240;
    private boolean k = false;

    public an(int i, int i2, int i3, boolean z, p[] pVarArr, int i4, int i5) {
    }

    private int e(int i, int i2) {
        return ((this.as.nextInt() >>> 1) % ((i2 - i) + 1)) + i;
    }

    public final void a(o oVar, int i, int i2) {
        for (int i3 = 0; i3 < this.X.length; i3++) {
            if (this.ar[i3] <= 0) {
                this.X[i3] = e(i - (this.e >> 1), this.e + i + (this.e >> 1));
                this.Z[i3] = e(i2 - (this.a >> 1), this.a + i2 + (this.a >> 1));
                this.aq[i3] = e(1, 3);
                if (this.k) {
                    this.dS[i3] = this.aw[e(0, this.aw.length - 1)];
                }
                int[] iArr = this.ar;
                iArr[i3] = iArr[i3] + 1;
            } else if (this.X[i3] < i - (this.e >> 1) || this.X[i3] > this.e + i + (this.e >> 1) || this.Z[i3] < i2 - (this.a >> 1) || this.Z[i3] > this.a + i2 + (this.a >> 1)) {
                this.ar[i3] = 0;
            } else {
                int[] iArr2 = this.Z;
                iArr2[i3] = iArr2[i3] + this.aq[i3];
                if (this.Z[i3] % 80 < 30) {
                    int[] iArr3 = this.X;
                    iArr3[i3] = iArr3[i3] + 2;
                } else if (this.k) {
                    if (this.Z[i3] % 80 >= 30 && this.Z[i3] % 80 <= 60) {
                        int[] iArr4 = this.X;
                        iArr4[i3] = iArr4[i3] - 2;
                    } else if (this.Z[i3] % 80 <= 60 || this.Z[i3] % 80 >= 90) {
                        int[] iArr5 = this.X;
                        iArr5[i3] = iArr5[i3] + 1;
                    } else {
                        int[] iArr6 = this.X;
                        iArr6[i3] = iArr6[i3] - 1;
                    }
                } else if (this.Z[i3] % 80 >= 30 && this.Z[i3] % 80 <= 40) {
                    int[] iArr7 = this.X;
                    iArr7[i3] = iArr7[i3] - 2;
                } else if (this.Z[i3] % 80 <= 40 || this.Z[i3] % 80 >= 60) {
                    int[] iArr8 = this.X;
                    iArr8[i3] = iArr8[i3] + 1;
                } else {
                    int[] iArr9 = this.X;
                    iArr9[i3] = iArr9[i3] - 1;
                }
            }
        }
        oVar.setColor(16777215);
        for (int i4 = 0; i4 < this.X.length; i4++) {
            if (this.X[i4] > 0 && this.X[i4] - i < 240) {
                if (this.k) {
                    oVar.a(this.dS[i4], this.X[i4] - i, this.Z[i4] - i2, 0);
                } else if (i4 % 5 < 2) {
                    oVar.fillRect(this.X[i4] - i, this.Z[i4] - i2, 3, 1);
                    oVar.fillRect((this.X[i4] - i) + 1, (this.Z[i4] - i2) - 1, 1, 3);
                } else {
                    oVar.fillRect(this.X[i4] - i, this.Z[i4] - i2, 4, 2);
                    oVar.fillRect((this.X[i4] - i) + 1, (this.Z[i4] - i2) - 1, 2, 4);
                }
            }
        }
    }
}
