package me.gall.sgp.android.common;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.UUID;

public class DeviceInfo {
    private static final String LOCAL_UID = "LOCAL_UID";
    public static final String LOG_TAG = DeviceInfo.class.getSimpleName();

    public static String getDeviceICCID(Context context) {
        String str = null;
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = getTelephonyManager(context).getSimSerialNumber();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "iccid=" + str);
        }
        return str == null ? "" : str;
    }

    private static String getDeviceIDHash(Context context) {
        String str;
        try {
            str = Settings.Secure.getString(context.getContentResolver(), "android_id");
        } catch (Exception e) {
            try {
                Class<?> cls = Class.forName("android.os.SystemProperties");
                str = (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
            } catch (Exception e2) {
                str = null;
            }
        }
        Log.d(LOG_TAG, "androidId=" + str);
        if (str == null || "9774d56d682e549c".equals(str)) {
            return str;
        }
        return null;
    }

    public static String getDeviceIMEI(Context context) {
        String str = null;
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = getTelephonyManager(context).getDeviceId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "imei=" + str);
        }
        return str == null ? "" : str;
    }

    public static final String getDeviceIMSI(Context context) {
        String str;
        if (isDualSim()) {
            try {
                str = getDeviceIMSI(context, getReadySim(context));
            } catch (Exception e) {
                e.printStackTrace();
                str = null;
            }
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            try {
                if (telephonyManager.getSimState() == 5 || telephonyManager.getSimState() == 0) {
                    Log.d(LOG_TAG, "Sim state ready.");
                    str = telephonyManager.getSubscriberId();
                } else {
                    throw new Exception("Sim card is not ready yet.");
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                Log.e(LOG_TAG, "Couldn't get the operator.");
                str = null;
            }
        }
        if (str == null) {
            str = "";
        }
        Log.d(LOG_TAG, "imsi=" + str);
        return str;
    }

    public static String getDeviceIMSI(Context context, int i) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        Class<TelephonyManager> cls = TelephonyManager.class;
        try {
            Object invoke = cls.getMethod("getSimStateGemini", Integer.TYPE).invoke(telephonyManager, new Integer(i));
            Object invoke2 = TelephonyManager.class.getMethod("getSubscriberIdGemini", Integer.TYPE).invoke(telephonyManager, new Integer(i));
            if (invoke.toString().equals("5")) {
                Log.d(LOG_TAG, "IMSI of sim " + i + " is " + ((String) null));
                return invoke2.toString();
            }
            Log.d(LOG_TAG, "Sim " + i + " is not ready.");
            return null;
        } catch (Exception e) {
            Class<TelephonyManager> cls2 = TelephonyManager.class;
            try {
                Object invoke3 = cls2.getMethod("getSimState", Integer.TYPE).invoke(telephonyManager, new Integer(i));
                Object invoke4 = TelephonyManager.class.getMethod("getSubscriberId", Integer.TYPE).invoke(telephonyManager, new Integer(i));
                if (invoke3.toString().equals("5")) {
                    Log.d(LOG_TAG, "IMSI of sim " + i + " is " + ((String) null));
                    return invoke4.toString();
                }
                Log.d(LOG_TAG, "Sim " + i + " is not ready.");
                return null;
            } catch (Exception e2) {
                Log.d(LOG_TAG, "It may not be a dual sim device." + e);
                return null;
            }
        }
    }

    public static String getDevicePhonenumber(Context context) {
        String str = null;
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = getTelephonyManager(context).getLine1Number();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "phoneNumber=" + str);
        }
        return str == null ? "" : str;
    }

    public static String getLocalGenerateUUID(Context context) {
        if (!Configuration.getSetting(context).contains(LOCAL_UID)) {
            Configuration.getSetting(context).edit().putString(LOCAL_UID, UUID.randomUUID().toString()).commit();
        }
        return Configuration.getSetting(context).getString(LOCAL_UID, "");
    }

    public static String getMacAddress(Context context) {
        String str = null;
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == 0) {
            try {
                str = getWifiManager(context).getConnectionInfo().getMacAddress();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "mac=" + str);
        }
        return str == null ? "" : str;
    }

    public static String getOSVersion() {
        return "android-" + Build.VERSION.SDK_INT;
    }

    public static int getReadySim(Context context) {
        boolean isSimReady = isSimReady(context, 0);
        boolean isSimReady2 = isSimReady(context, 1);
        if (isSimReady) {
            return 0;
        }
        if (isSimReady2) {
            return 1;
        }
        throw new IllegalStateException("None sim is ready.");
    }

    public static final String getStandardDeviceIMSI(Context context) {
        String str;
        try {
            str = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "Couldn't get the operator.");
            str = null;
        }
        Log.d(LOG_TAG, "imsi=" + str);
        return str;
    }

    private static TelephonyManager getTelephonyManager(Context context) {
        return (TelephonyManager) context.getSystemService("phone");
    }

    public static WifiManager getWifiManager(Context context) {
        return (WifiManager) context.getSystemService("wifi");
    }

    public static boolean isDualSim() {
        Class<TelephonyManager> cls = TelephonyManager.class;
        try {
            cls.getMethod("getSimStateGemini", Integer.TYPE);
            Log.d(LOG_TAG, "It is a mediatek device.");
            return true;
        } catch (Exception e) {
            Class<TelephonyManager> cls2 = TelephonyManager.class;
            try {
                cls2.getMethod("getSimState", Integer.TYPE);
                Log.d(LOG_TAG, "It is a mstar device.");
                return true;
            } catch (Exception e2) {
                return false;
            }
        }
    }

    public static boolean isSimReady(Context context, int i) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        Class<TelephonyManager> cls = TelephonyManager.class;
        try {
            return cls.getMethod("getSimStateGemini", Integer.TYPE).invoke(telephonyManager, new Integer(i)).toString().equals("5");
        } catch (Exception e) {
            Class<TelephonyManager> cls2 = TelephonyManager.class;
            try {
                return cls2.getMethod("getSimState", Integer.TYPE).invoke(telephonyManager, new Integer(i)).toString().equals("5");
            } catch (Exception e2) {
                e2.printStackTrace();
                return false;
            }
        }
    }
}
