package me.gall.sgp.android.common;

import android.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class AsyncTaskExecutor {
    public static final String LOG_TAG = AsyncTaskExecutor.class.getSimpleName();
    private static ExecutorService executorService;

    public interface AsyncCallback<T> {
        void call(T t);

        void exception(Throwable th);
    }

    public static void executeTask(Runnable runnable) {
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
        if (executorService.isShutdown() || executorService.isTerminated()) {
            Log.e(LOG_TAG, "ExecutorService is stopped.");
            executorService = Executors.newSingleThreadExecutor();
        }
        executorService.execute(runnable);
    }

    public static void finishTask() {
        if (executorService != null) {
            executorService.shutdown();
        }
        executorService = null;
    }

    public static <T> Future<T> submitTask(Callable<T> callable) {
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
        if (executorService.isShutdown() || executorService.isTerminated()) {
            Log.e(LOG_TAG, "ExecutorService is stopped.");
            executorService = Executors.newSingleThreadExecutor();
        }
        return executorService.submit(callable);
    }
}
