package me.gall.sgp.android.common;

import com.umeng.common.b.e;
import java.security.MessageDigest;
import javax.microedition.media.control.ToneControl;

public class EncryptUtil {
    public static String encryptString(String str, String str2) {
        MessageDigest instance = MessageDigest.getInstance(str2);
        instance.reset();
        instance.update(str.getBytes(e.f));
        byte[] digest = instance.digest();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            if (Integer.toHexString(digest[i] & ToneControl.SILENCE).length() == 1) {
                stringBuffer.append("0").append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
            } else {
                stringBuffer.append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
            }
        }
        return stringBuffer.toString();
    }
}
