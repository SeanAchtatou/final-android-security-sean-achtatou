package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class CampaignDetail implements Serializable {
    private static final long serialVersionUID = 1;
    private Campaign campaign;
    private Integer campaignId;
    private String description;
    private Integer id;
    private String script;

    public Campaign getCampaign() {
        return this.campaign;
    }

    public Integer getCampaignId() {
        return this.campaignId;
    }

    public String getDescription() {
        return this.description;
    }

    public Integer getId() {
        return this.id;
    }

    public String getScript() {
        return this.script;
    }

    public void setCampaign(Campaign campaign2) {
        this.campaign = campaign2;
    }

    public void setCampaignId(Integer num) {
        this.campaignId = num;
    }

    public void setDescription(String str) {
        this.description = str;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setScript(String str) {
        this.script = str;
    }
}
