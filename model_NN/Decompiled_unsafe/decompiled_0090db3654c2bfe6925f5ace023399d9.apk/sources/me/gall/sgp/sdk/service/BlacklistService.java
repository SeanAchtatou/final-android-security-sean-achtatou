package me.gall.sgp.sdk.service;

public interface BlacklistService {
    boolean addPlayerIntoBlacklist(String str, String str2);

    boolean isInBlacklist(String str, String str2);
}
