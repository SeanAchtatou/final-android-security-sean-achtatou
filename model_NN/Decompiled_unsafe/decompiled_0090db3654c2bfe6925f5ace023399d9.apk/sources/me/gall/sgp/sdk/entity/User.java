package me.gall.sgp.sdk.entity;

public class User {
    public static final int AUTO = 2;
    public static final int MANUAL = 0;
    public static final int PHONENUMBER = 1;
    private long createTime;
    private String email;
    private String iccid;
    private String imei;
    private long lastLoginTime;
    private String mac;
    private String nickName;
    private String password;
    private String phoneNumber;
    private int registryType;
    private long updateTime;
    private String userName;
    private String userid;

    public static int getAuto() {
        return 2;
    }

    public static int getManual() {
        return 0;
    }

    public static int getPhonenumber() {
        return 1;
    }

    public long getCreateTime() {
        return this.createTime;
    }

    public String getEmail() {
        return this.email;
    }

    public String getIccid() {
        return this.iccid;
    }

    public String getImei() {
        return this.imei;
    }

    public long getLastLoginTime() {
        return this.lastLoginTime;
    }

    public String getMac() {
        return this.mac;
    }

    public String getNickName() {
        return this.nickName;
    }

    public String getPassword() {
        return this.password;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public int getRegistryType() {
        return this.registryType;
    }

    public long getUpdateTime() {
        return this.updateTime;
    }

    public String getUserName() {
        return this.userName;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setCreateTime(long j) {
        this.createTime = j;
    }

    public void setEmail(String str) {
        this.email = str;
    }

    public void setIccid(String str) {
        this.iccid = str;
    }

    public void setImei(String str) {
        this.imei = str;
    }

    public void setLastLoginTime(long j) {
        this.lastLoginTime = j;
    }

    public void setMac(String str) {
        this.mac = str;
    }

    public void setNickName(String str) {
        this.nickName = str;
    }

    public void setPassword(String str) {
        this.password = str;
    }

    public void setPhoneNumber(String str) {
        this.phoneNumber = str;
    }

    public void setRegistryType(int i) {
        this.registryType = i;
    }

    public void setUpdateTime(long j) {
        this.updateTime = j;
    }

    public void setUserName(String str) {
        this.userName = str;
    }

    public void setUserid(String str) {
        this.userid = str;
    }
}
