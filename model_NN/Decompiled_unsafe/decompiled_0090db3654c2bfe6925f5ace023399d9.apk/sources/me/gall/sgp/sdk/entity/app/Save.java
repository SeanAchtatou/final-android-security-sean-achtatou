package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class Save implements Serializable {
    public static final int ALLOW_DOWN = 1;
    public static final int DENY_DOWN = 0;
    private static final long serialVersionUID = 1;
    private String content;
    private Integer downFlag;
    private Integer id;
    private Long lastUploadTime;
    private String playerId;

    public String getContent() {
        return this.content;
    }

    public Integer getDownFlag() {
        return this.downFlag;
    }

    public Integer getId() {
        return this.id;
    }

    public Long getLastUploadTime() {
        return this.lastUploadTime;
    }

    public String getPlayerId() {
        return this.playerId;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public void setDownFlag(Integer num) {
        this.downFlag = num;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setLastUploadTime(Long l) {
        this.lastUploadTime = l;
    }

    public void setPlayerId(String str) {
        this.playerId = str;
    }
}
