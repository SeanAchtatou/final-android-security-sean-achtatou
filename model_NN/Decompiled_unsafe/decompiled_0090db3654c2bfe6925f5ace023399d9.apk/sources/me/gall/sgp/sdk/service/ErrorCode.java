package me.gall.sgp.sdk.service;

public interface ErrorCode {
    public static final int BAD_CELLPHONENUMBER = 212;
    public static final int BAD_ICCID = 209;
    public static final int BAD_IMEI = 208;
    public static final int BAD_MAC = 210;
    public static final int IMEI_ICCID_MAC_NULL = 211;
    public static final int PASSWORD_BAD_CHAR = 204;
    public static final int PASSWORD_BAD_LENGTH = 205;
    public static final int PASSWORD_NOT_MATCH = 207;
    public static final int USERNAME_BAD_CHAR = 201;
    public static final int USERNAME_BAD_LENGTH = 202;
    public static final int USERNAME_EXISTS = 203;
    public static final int USERNAME_NOT_EXISTS = 206;
}
