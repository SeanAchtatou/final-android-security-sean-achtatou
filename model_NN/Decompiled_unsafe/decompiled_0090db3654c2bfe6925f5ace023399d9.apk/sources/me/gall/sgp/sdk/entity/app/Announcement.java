package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class Announcement implements Serializable {
    public static final int ACTIVITY = 1;
    public static final int BULLETIN = 2;
    private static final long serialVersionUID = -3955717059272742240L;
    private String content;
    private Long endTime;
    private Long startTime;
    private Integer state;
    private String title;
    private Integer type;
    private String uuid;
    private Integer version;

    public String getContent() {
        return this.content;
    }

    public Long getEndTime() {
        return this.endTime;
    }

    public Long getStartTime() {
        return this.startTime;
    }

    public Integer getState() {
        return this.state;
    }

    public String getTitle() {
        return this.title;
    }

    public Integer getType() {
        return this.type;
    }

    public String getUuid() {
        return this.uuid;
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public void setEndTime(Long l) {
        this.endTime = l;
    }

    public void setStartTime(Long l) {
        this.startTime = l;
    }

    public void setState(Integer num) {
        this.state = num;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setType(Integer num) {
        this.type = num;
    }

    public void setUuid(String str) {
        this.uuid = str;
    }

    public void setVersion(Integer num) {
        this.version = num;
    }
}
