package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class CheckinBox implements Serializable {
    public static final int DELAY = 2;
    public static final int NOMARL = 1;
    private static final long serialVersionUID = 5737176704153953327L;
    private String appId;
    private String checkinBoxId;
    private Integer id;
    private Integer maxCheckinTimes;
    private String name;
    private String reward;
    private String serverId;
    private Integer type;

    public String getAppId() {
        return this.appId;
    }

    public String getCheckinBoxId() {
        return this.checkinBoxId;
    }

    public Integer getId() {
        return this.id;
    }

    public Integer getMaxCheckinTimes() {
        return this.maxCheckinTimes;
    }

    public String getName() {
        return this.name;
    }

    public String getReward() {
        return this.reward;
    }

    public String getServerId() {
        return this.serverId;
    }

    public Integer getType() {
        return this.type;
    }

    public void setAppId(String str) {
        this.appId = str;
    }

    public void setCheckinBoxId(String str) {
        this.checkinBoxId = str;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setMaxCheckinTimes(Integer num) {
        this.maxCheckinTimes = num;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setReward(String str) {
        this.reward = str;
    }

    public void setServerId(String str) {
        this.serverId = str;
    }

    public void setType(Integer num) {
        this.type = num;
    }
}
