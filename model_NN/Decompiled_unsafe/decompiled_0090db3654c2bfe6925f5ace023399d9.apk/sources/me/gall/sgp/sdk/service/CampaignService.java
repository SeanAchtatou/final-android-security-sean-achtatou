package me.gall.sgp.sdk.service;

import me.gall.sgp.sdk.entity.app.Campaign;
import me.gall.sgp.sdk.entity.app.CampaignDetail;

public interface CampaignService {
    Campaign[] getAvailableCampaigns();

    Campaign[] getByTimeZone(long j, long j2);

    Campaign getCampaignById(int i);

    CampaignDetail getCampaignDetaiByCId(int i);

    CampaignDetail getCampaignDetaiById(int i);

    int getCampaignProgress(int i, String str);

    int updateProgress(int i, String str, int i2);
}
