package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class GachaBox implements Serializable {
    private static final long serialVersionUID = 3235858924174654406L;
    private String description;
    private Long endValidTime;
    private Integer id;
    private String img;
    private Lottery[] lotteries;
    private String name;
    private Double price;
    private Long startValidTime;
    private Integer timeLimit;
    private String timeLimitErrorMsg;
    private Integer totalLimit;
    private String totalLimitErrorMsg;
    private String type;

    public String getDescription() {
        return this.description;
    }

    public Long getEndValidTime() {
        return this.endValidTime;
    }

    public Integer getId() {
        return this.id;
    }

    public String getImg() {
        return this.img;
    }

    public Lottery[] getLotteries() {
        return this.lotteries;
    }

    public String getName() {
        return this.name;
    }

    public Double getPrice() {
        return this.price;
    }

    public Long getStartValidTime() {
        return this.startValidTime;
    }

    public Integer getTimeLimit() {
        return this.timeLimit;
    }

    public String getTimeLimitErrorMsg() {
        return this.timeLimitErrorMsg;
    }

    public Integer getTotalLimit() {
        return this.totalLimit;
    }

    public String getTotalLimitErrorMsg() {
        return this.totalLimitErrorMsg;
    }

    public String getType() {
        return this.type;
    }

    public void setDescription(String str) {
        this.description = str;
    }

    public void setEndValidTime(Long l) {
        this.endValidTime = l;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setImg(String str) {
        this.img = str;
    }

    public void setLotteries(Lottery[] lotteryArr) {
        this.lotteries = lotteryArr;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setPrice(Double d) {
        this.price = d;
    }

    public void setStartValidTime(Long l) {
        this.startValidTime = l;
    }

    public void setTimeLimit(Integer num) {
        this.timeLimit = num;
    }

    public void setTimeLimitErrorMsg(String str) {
        this.timeLimitErrorMsg = str;
    }

    public void setTotalLimit(Integer num) {
        this.totalLimit = num;
    }

    public void setTotalLimitErrorMsg(String str) {
        this.totalLimitErrorMsg = str;
    }

    public void setType(String str) {
        this.type = str;
    }
}
