package me.gall.sgp.sdk.service;

import me.gall.sgp.sdk.entity.app.Announcement;

public interface AnnouncementService {
    Announcement getAnnounceByType(int i);
}
