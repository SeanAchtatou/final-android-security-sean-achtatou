package me.gall.sgp.sdk.service;

import me.gall.sgp.sdk.entity.app.Mail;
import me.gall.sgp.sdk.entity.app.SgpPlayer;

public interface FriendshipService {
    void acceptInvite(String str, String str2);

    void acceptInvite(String str, String str2, Mail mail);

    void acceptInvite(String str, String[] strArr);

    void acceptInvite(String str, String[] strArr, Mail mail);

    SgpPlayer[] getInvite(String str);

    SgpPlayer[] getMyFriends(int i, int i2, String str);

    SgpPlayer[] getNotConfirm(String str);

    void invite(String str, String str2);

    void invite(String str, String[] strArr);

    void unfriend(String str, String str2);

    void unfriend(String str, String[] strArr);
}
