package me.gall.sgp.sdk.service;

import me.gall.sgp.sdk.entity.app.Mail;

public interface MailService {
    void delete(int i);

    void delete(int[] iArr);

    void read(int i);

    void read(int[] iArr);

    Mail[] receive(int i, int i2, String str, int i3);

    Mail[] receiveUnread(long j, String str);

    Mail send(Mail mail);
}
