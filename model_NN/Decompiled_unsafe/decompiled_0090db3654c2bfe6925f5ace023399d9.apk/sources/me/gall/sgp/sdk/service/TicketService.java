package me.gall.sgp.sdk.service;

import me.gall.sgp.sdk.entity.app.Ticket;

public interface TicketService {
    Ticket[] getTicketsBySenderPlayerId(String str, int i, int i2, int i3);

    boolean sendTicket(Ticket ticket);
}
