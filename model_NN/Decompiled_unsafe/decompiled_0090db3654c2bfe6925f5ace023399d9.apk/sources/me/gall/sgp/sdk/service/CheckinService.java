package me.gall.sgp.sdk.service;

import me.gall.sgp.sdk.entity.app.CheckinBox;

public interface CheckinService {
    int accumlateCount(String str, String str2);

    int checkin(String str, String str2);

    int countinuousCount(String str, String str2);

    CheckinBox getCheckinboardByChekinboardId(String str);

    long getLastCheckinTime(String str, String str2);

    String getRewardByChekinboardId(String str);

    int setCheckinTimes(String str, String str2, int i);
}
