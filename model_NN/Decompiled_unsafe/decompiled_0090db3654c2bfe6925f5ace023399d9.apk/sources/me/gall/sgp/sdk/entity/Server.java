package me.gall.sgp.sdk.entity;

public class Server {
    public static final int GOOD = 1;
    public static final int HEAVY = 3;
    public static final int LIGHT = 0;
    public static final int MAINTAIN = 4;
    public static final int ORDINARY = 2;
    private String address;
    private String id;
    private boolean isNew = false;
    private boolean isRecommend = false;
    private String name;
    private int state = 4;

    public String getAddress() {
        return this.address;
    }

    public String getId() {
        return this.id;
    }

    public boolean getIsNew() {
        return this.isNew;
    }

    public boolean getIsRecommend() {
        return this.isRecommend;
    }

    public String getName() {
        return this.name;
    }

    public int getState() {
        return this.state;
    }

    public void setAddress(String str) {
        this.address = str;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setIsNew(boolean z) {
        this.isNew = z;
    }

    public void setIsRecommend(boolean z) {
        this.isRecommend = z;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setState(int i) {
        this.state = i;
    }
}
