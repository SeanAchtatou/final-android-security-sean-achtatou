package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class Lottery implements Serializable {
    private static final long serialVersionUID = 3296398631379705432L;
    private Integer id;
    private String name;
    private String prize;
    private Integer quality;
    private Integer relatedGachaBox;
    private Integer weight;

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getPrize() {
        return this.prize;
    }

    public Integer getQuality() {
        return this.quality;
    }

    public Integer getRelatedGachaBox() {
        return this.relatedGachaBox;
    }

    public Integer getWeight() {
        return this.weight;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setPrize(String str) {
        this.prize = str;
    }

    public void setQuality(Integer num) {
        this.quality = num;
    }

    public void setRelatedGachaBox(Integer num) {
        this.relatedGachaBox = num;
    }

    public void setWeight(Integer num) {
        this.weight = num;
    }
}
