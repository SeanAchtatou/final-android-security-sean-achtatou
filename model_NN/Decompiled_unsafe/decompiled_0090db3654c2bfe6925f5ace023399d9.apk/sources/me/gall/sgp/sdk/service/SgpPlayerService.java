package me.gall.sgp.sdk.service;

import me.gall.sgp.sdk.entity.app.Save;
import me.gall.sgp.sdk.entity.app.SgpPlayer;

public interface SgpPlayerService {
    SgpPlayer create(SgpPlayer sgpPlayer);

    Save downloadSave(String str);

    SgpPlayer[] getByLastLoginTime(long j, int i, int i2);

    SgpPlayer[] getByName(String str, int i, int i2);

    SgpPlayer[] getByUserId(String str);

    SgpPlayer getOneByUserId(String str);

    SgpPlayer getSgpPlayerByCustomId(String str);

    SgpPlayer update(SgpPlayer sgpPlayer);

    Save uploadSave(Save save);
}
