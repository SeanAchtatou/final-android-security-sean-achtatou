package me.gall.sgp.sdk.service;

import me.gall.sgp.sdk.entity.app.Boss;
import me.gall.sgp.sdk.entity.app.SgpPlayer;

public interface BossService {
    public static final String ID_SEPARATOR = ",";

    int attack(int i, int i2, String str);

    Boss getByBossId(int i);

    Boss[] getByBossIds(String str);

    Boss[] getByBossIds(int[] iArr);

    int getCurrentHP(int i, String str);

    SgpPlayer getLastAttackPlayer(int i);
}
