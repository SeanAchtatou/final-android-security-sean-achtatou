package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class Friendship implements Serializable {
    public static final int FRIEND = 1;
    public static final int UN_CONFIRM = 0;
    private static final long serialVersionUID = 1;
    private Integer id;
    private String receiveId;
    private String sendId;
    private int status;

    public Integer getId() {
        return this.id;
    }

    public String getReceiveId() {
        return this.receiveId;
    }

    public String getSendId() {
        return this.sendId;
    }

    public Integer getStatus() {
        return Integer.valueOf(this.status);
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setReceiveId(String str) {
        this.receiveId = str;
    }

    public void setSendId(String str) {
        this.sendId = str;
    }

    public void setStatus(Integer num) {
        this.status = num.intValue();
    }
}
