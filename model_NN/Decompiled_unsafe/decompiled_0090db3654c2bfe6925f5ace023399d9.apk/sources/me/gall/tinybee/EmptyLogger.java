package me.gall.tinybee;

import android.content.Context;
import java.util.HashMap;
import java.util.Map;
import me.gall.tinybee.Logger;

public class EmptyLogger implements Logger {
    public void finish() {
    }

    public Context getContext() {
        return null;
    }

    public boolean hasOfflineData() {
        return false;
    }

    public void init() {
    }

    public boolean isSandboxMode() {
        return false;
    }

    public void onPause(Context context) {
    }

    public void onResume(Context context) {
    }

    public void send(String str) {
    }

    public void send(String str, Map<String, String> map) {
    }

    public void sendException(HashMap<String, String> hashMap) {
    }

    public void setOnlineParamCallback(Logger.OnlineParamCallback onlineParamCallback) {
    }

    public void syncOfflineData() {
    }
}
