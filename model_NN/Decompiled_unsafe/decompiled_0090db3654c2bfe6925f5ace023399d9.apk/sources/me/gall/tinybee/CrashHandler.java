package me.gall.tinybee;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import java.lang.Thread;
import java.util.HashMap;

public class CrashHandler implements Thread.UncaughtExceptionHandler {
    public static final String TAG = "CrashHandler";
    private Context context;

    public CrashHandler(Context context2) {
        this.context = context2;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        Log.e(TAG, "uncaughtException");
        StackTraceElement[] stackTrace = th.getStackTrace();
        StringBuffer stringBuffer = new StringBuffer();
        for (StackTraceElement stackTraceElement : stackTrace) {
            stringBuffer.append(stackTraceElement.toString()).append(10);
        }
        HashMap hashMap = new HashMap();
        hashMap.put("EXCEPTION", stringBuffer.toString());
        hashMap.put("MODLEL", Build.MODEL);
        hashMap.put("OSVERSION", String.valueOf(Build.VERSION.SDK_INT));
        hashMap.put("TIMSTAMP", new StringBuilder(String.valueOf(System.currentTimeMillis())).toString());
        hashMap.put("PACKAGENAME", this.context.getPackageName());
        try {
            PackageManager packageManager = this.context.getApplicationContext().getPackageManager();
            hashMap.put("APPNAME", (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(this.context.getPackageName(), 0)));
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, "APPNAME not found ");
        }
        LoggerManager.getCurrentLogger().sendException(hashMap);
    }
}
