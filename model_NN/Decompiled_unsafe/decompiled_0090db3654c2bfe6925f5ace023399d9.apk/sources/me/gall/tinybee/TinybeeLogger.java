package me.gall.tinybee;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.a.a.h.b;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import me.gall.sgp.sdk.entity.app.StructuredData;
import me.gall.tinybee.Logger;
import me.gall.tinybee.LoggerManager;
import org.json.JSONException;
import org.json.JSONObject;

public class TinybeeLogger implements Logger {
    public static final String TAG = "TinybeeLogger";
    private static final String TINYBEE_RUNTIMES = "_TB_RUNTIMES";
    private static final String TINYBEE_UUIDS = "_TB_UUIDS";
    public static final int URL_TYPE_APK_DOWNLOAD = 0;
    public static final int URL_TYPE_WEB_PAGE = 1;
    private static final String UUID = "UUID";
    public static final int VIEW_DIALOG = 2;
    public static final int VIEW_FULLSCREEN = 1;
    public static final int VIEW_NORMAL = 0;
    private static final String _TB_crash = "_TB_crash";
    private static final String _TB_launch = "_TB_launch";
    /* access modifiers changed from: private */
    public static boolean mUpdateFromOpenToClose = false;
    /* access modifiers changed from: private */
    public static boolean networkError;
    /* access modifiers changed from: private */
    public static int retry;
    /* access modifiers changed from: private */
    public LoggerManager.LoggerConfiguration conf;
    private Context context;
    private ExecutorService executorService;
    private boolean inited = false;

    public static final class Config {
        private static final String HOST = "http://tinybee.savenumber.com/";
        private static final String HOST_TEST = "http://test.gall.me/tinybee/";
        protected static String network = "";

        protected static boolean checkCallingPermission(String str, Context context) {
            return Binder.getCallingPid() == Process.myPid() || context.checkCallingPermission(str) == 0;
        }

        static String getAnalysisURL(boolean z) {
            return String.valueOf(getHost(z)) + "analysis/";
        }

        protected static String getAndroidID(Context context) {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if ("9774d56d682e549c".equals(string)) {
                return string;
            }
            try {
                Class<?> cls = Class.forName("android.os.SystemProperties");
                return (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
            } catch (Exception e) {
                return string;
            }
        }

        public static String getDataConnectionNetworkInfo(Context context) {
            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
                Log.d(TinybeeLogger.TAG, "ACCESS_NETWORK_STATE permission denied.");
                return null;
            }
            try {
                return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0).getExtraInfo();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        protected static String getHost(boolean z) {
            return z ? HOST_TEST : HOST;
        }

        protected static String getIMEI(Context context) {
            try {
                return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected static String getIMSI(Context context) {
            try {
                return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        static String getLiveParamsURL(boolean z) {
            return String.valueOf(getHost(z)) + "customparameters/";
        }

        protected static String getMAC(Context context) {
            try {
                WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                return (wifiManager == null ? null : wifiManager.getConnectionInfo()).getMacAddress();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected static String getManufacturer() {
            return Build.MANUFACTURER;
        }

        protected static String getModel() {
            return Build.MODEL;
        }

        protected static String getNetwork(Context context) {
            if (!isConnect(context)) {
                return "NONETWORK";
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            try {
                if (activeNetworkInfo.getTypeName().toUpperCase().indexOf("MOBILE") != -1) {
                    network = activeNetworkInfo.getExtraInfo();
                } else if (activeNetworkInfo.getTypeName().toUpperCase().indexOf("WIFI") != -1) {
                    network = "WIFI";
                }
            } catch (Exception e) {
                network = "NONETWORK";
            }
            return network;
        }

        protected static String getNumber(Context context) {
            try {
                return ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        protected static String getOs() {
            return "android";
        }

        protected static String getOsVerSion() {
            return Build.VERSION.RELEASE;
        }

        protected static String getResolution(Context context) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            return String.valueOf(displayMetrics.widthPixels) + "*" + displayMetrics.heightPixels;
        }

        private static TelephonyManager getTelephonyManager(Context context) {
            return (TelephonyManager) context.getSystemService("phone");
        }

        protected static long getTimeStamp() {
            return System.currentTimeMillis();
        }

        static String getUpdateInfoURL(boolean z) {
            return String.valueOf(getHost(z)) + "update/info";
        }

        public static final boolean isConnect(Context context) {
            return isWifiConnect(context) || isDataConnect(context);
        }

        public static boolean isDataConnect(Context context) {
            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
                Log.d(TinybeeLogger.TAG, "ACCESS_NETWORK_STATE permission denied.");
                return true;
            }
            try {
                NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0);
                if (networkInfo.isConnectedOrConnecting() || getTelephonyManager(context).getDataState() == 2) {
                    Log.d(TinybeeLogger.TAG, "mobile is connected. Type:" + networkInfo.getTypeName() + " APN:" + networkInfo.getExtraInfo());
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        public static boolean isWifiConnect(Context context) {
            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != 0) {
                Log.d(TinybeeLogger.TAG, "ACCESS_WIFI_STATE permission denied.");
                return true;
            }
            try {
                return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isConnected();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        protected static final String replace(String str, String str2, String str3) {
            try {
                return str.replace(str2, str3);
            } catch (Exception e) {
                Log.e(TinybeeLogger.TAG, "Config.replace()出错:" + e.getMessage());
                return str;
            }
        }

        public static void setMobileDataEnabled(Context context, boolean z) {
            Method declaredMethod;
            Log.d(TinybeeLogger.TAG, "SDK_INT is " + Build.VERSION.SDK_INT);
            if (Build.VERSION.SDK_INT >= 9) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                Field declaredField = Class.forName(connectivityManager.getClass().getName()).getDeclaredField("mService");
                declaredField.setAccessible(true);
                Object obj = declaredField.get(connectivityManager);
                Method declaredMethod2 = Class.forName(obj.getClass().getName()).getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
                declaredMethod2.setAccessible(true);
                declaredMethod2.invoke(obj, Boolean.valueOf(z));
            } else {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (telephonyManager.getDataState() == 2 && z) {
                    Log.d(TinybeeLogger.TAG, "Already enable. No need to turn on data connection.");
                } else if (telephonyManager.getDataState() != 2 && !z) {
                    Log.d(TinybeeLogger.TAG, "Already disable. No need to turn off data connection.");
                }
                Method declaredMethod3 = Class.forName(telephonyManager.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
                declaredMethod3.setAccessible(true);
                Object invoke = declaredMethod3.invoke(telephonyManager, new Object[0]);
                Class<?> cls = Class.forName(invoke.getClass().getName());
                if (!z) {
                    declaredMethod = cls.getDeclaredMethod("disableDataConnectivity", new Class[0]);
                    Log.d(TinybeeLogger.TAG, "Ready to disable data connection state.");
                } else {
                    declaredMethod = cls.getDeclaredMethod("enableDataConnectivity", new Class[0]);
                    Log.d(TinybeeLogger.TAG, "Ready to enable data connection state.");
                }
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(invoke, new Object[0]);
                Log.d(TinybeeLogger.TAG, "Success change data connection state.");
            }
            SystemClock.sleep(2000);
        }

        public static void setWifiEnabled(Context context, boolean z) {
            ((WifiManager) context.getSystemService("wifi")).setWifiEnabled(z);
            SystemClock.sleep(2000);
        }

        static boolean testConnection() {
            return false;
        }
    }

    static class EventTask implements Runnable {
        public static final String APPID = "APPID";
        public static final String TIMESTAMP = "TIMESTAMP";
        private LoggerManager.LoggerConfiguration conf;
        protected Context context;
        private String eventId;
        /* access modifiers changed from: private */
        public JSONObject params;
        /* access modifiers changed from: private */
        public long timestamp;

        public EventTask(Context context2, LoggerManager.LoggerConfiguration loggerConfiguration, String str) {
            if (context2 == null || loggerConfiguration == null) {
                throw new IllegalArgumentException("Context or appId could not be null.");
            }
            this.context = context2;
            this.conf = loggerConfiguration;
            this.eventId = str;
            this.params = new JSONObject();
        }

        public void addParam(String str, String str2) {
            if (str == null || str2 == null) {
                Log.w(TinybeeLogger.TAG, String.valueOf(str) + " must not be null.");
                return;
            }
            try {
                this.params.put(str, str2);
            } catch (JSONException e) {
                Log.e(TinybeeLogger.TAG, e.getMessage());
                e.printStackTrace();
            }
        }

        public synchronized void appendOfflineData(Context context2) {
            SharedPreferences sharedPreferences = context2.getApplicationContext().getSharedPreferences("_TB_" + this.conf.getAppId(), 0);
            int i = sharedPreferences.getInt("_TB_Length", 0);
            Log.d(TinybeeLogger.TAG, "save length:" + i);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            String str = null;
            if (this.params.length() > 0) {
                str = this.params.toString();
            }
            long timeStamp = Config.getTimeStamp();
            Log.d(TinybeeLogger.TAG, "save()  event:" + this.eventId + "   value:" + str + "   timestamp:" + timeStamp);
            edit.putString("_TB_KEY_" + i + "event", this.eventId);
            edit.putString("_TB_KEY_" + i + StructuredData.TYPE_OF_VALUE, str);
            edit.putString("_TB_KEY_" + i + "timeStamp", String.valueOf(timeStamp));
            edit.putInt("_TB_Length", i + 1);
            edit.commit();
        }

        public boolean catchErrorCode(int i) {
            return false;
        }

        public void consumeContent(String str) {
        }

        public LoggerManager.LoggerConfiguration getConf() {
            return this.conf;
        }

        public String getEventId() {
            return this.eventId;
        }

        public long getTimestamp() {
            return this.timestamp;
        }

        public boolean isNetworkError() {
            return TinybeeLogger.networkError;
        }

        public String read(InputStream inputStream, String str) {
            byte[] bArr = new byte[1024];
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    byteArrayOutputStream.flush();
                    byteArrayOutputStream.close();
                    String str2 = new String(byteArrayOutputStream.toByteArray(), str);
                    Log.d(TinybeeLogger.TAG, "Data:" + str2);
                    return str2;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:26:0x0193  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x01b2  */
        /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void registerForUUID() {
            /*
                r6 = this;
                r1 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x01b8 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x01b8 }
                boolean r3 = r3.isSandboxMode()     // Catch:{ Exception -> 0x01b8 }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getAnalysisURL(r3)     // Catch:{ Exception -> 0x01b8 }
                java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x01b8 }
                r2.<init>(r3)     // Catch:{ Exception -> 0x01b8 }
                java.lang.String r3 = "_TB_register"
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x01b8 }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01b8 }
                r0.<init>(r2)     // Catch:{ Exception -> 0x01b8 }
                java.lang.String r2 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
                java.lang.String r4 = "Register:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x01b8 }
                java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x01b8 }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x01b8 }
                android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x01b8 }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x01b8 }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x01b8 }
                r1 = 1
                r0.setDoOutput(r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1 = 1
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r1 = "POST"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1 = 0
                r0.setUseCaches(r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r1 = "Content-Type"
                java.lang.String r2 = "application/json"
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.<init>()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "APPID"
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = r3.getAppId()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "MODEL"
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getModel()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "MANUFACTURER"
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getManufacturer()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "OS"
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getOs()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "RESOLUTION"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getResolution(r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "NETWORK"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getNetwork(r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "IMEI"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getIMEI(r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "MAC"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getMAC(r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "CHANNELID"
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = r3.getChannelId()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "CHANNELNAME"
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = r3.getChannelName()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "ANDROID_ID"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getAndroidID(r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "IMSI"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getIMSI(r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "PHONENUM"
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getNumber(r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r4 = "CONTENT:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r4 = r1.toString()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.io.OutputStream r2 = r0.getOutputStream()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = "UTF-8"
                byte[] r1 = r1.getBytes(r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r2.write(r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r2.flush()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r2.close()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r4 = "ResponseCode:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r2 = 200(0xc8, float:2.8E-43)
                if (r1 == r2) goto L_0x013b
                r2 = 201(0xc9, float:2.82E-43)
                if (r1 == r2) goto L_0x013b
                r2 = 304(0x130, float:4.26E-43)
                if (r1 != r2) goto L_0x0197
            L_0x013b:
                org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = "UTF-8"
                java.lang.String r2 = r6.read(r2, r3)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r1.<init>(r2)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "UUID"
                java.lang.String r1 = r1.getString(r2)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                if (r1 == 0) goto L_0x017d
                int r2 = r1.length()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                if (r2 <= 0) goto L_0x017d
                android.content.Context r2 = r6.context     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r3 = r3.getAppId()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                me.gall.tinybee.TinybeeLogger.saveUUID(r2, r3, r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r4 = "Get uuid:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                android.util.Log.d(r2, r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                if (r0 == 0) goto L_0x017c
                r0.disconnect()
            L_0x017c:
                return
            L_0x017d:
                java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r2 = "Failed to get uuid."
                r1.<init>(r2)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                throw r1     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
            L_0x0185:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x0189:
                r0.printStackTrace()     // Catch:{ all -> 0x01b6 }
                r0 = 3
                r2 = 0
                r6.retry(r0, r2)     // Catch:{ all -> 0x01b6 }
                if (r1 == 0) goto L_0x017c
                r1.disconnect()
                goto L_0x017c
            L_0x0197:
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r4 = "Invalid response:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                r2.<init>(r1)     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
                throw r2     // Catch:{ Exception -> 0x0185, all -> 0x01ac }
            L_0x01ac:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x01b0:
                if (r1 == 0) goto L_0x01b5
                r1.disconnect()
            L_0x01b5:
                throw r0
            L_0x01b6:
                r0 = move-exception
                goto L_0x01b0
            L_0x01b8:
                r0 = move-exception
                goto L_0x0189
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.tinybee.TinybeeLogger.EventTask.registerForUUID():void");
        }

        public void retry(int i, boolean z) {
            TinybeeLogger.retry = TinybeeLogger.retry + 1;
            Log.d(TinybeeLogger.TAG, "Retry count:" + TinybeeLogger.retry);
            if (TinybeeLogger.retry > i || TinybeeLogger.networkError) {
                TinybeeLogger.networkError = true;
                if (z) {
                    Log.w(TinybeeLogger.TAG, "Network fatal error. Save right now.");
                    appendOfflineData(this.context);
                    return;
                }
                return;
            }
            run();
        }

        public void run() {
            if (!Config.isConnect(this.context) || TinybeeLogger.networkError) {
                Log.d(TinybeeLogger.TAG, "Network disabled. Save event[" + this.eventId + "].");
                appendOfflineData(this.context);
                return;
            }
            uploadEvent();
        }

        public void setEventId(String str) {
            this.eventId = str;
        }

        public void setTimestamp(long j) {
            this.timestamp = j;
        }

        /* JADX WARNING: Removed duplicated region for block: B:40:0x0150  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void uploadEvent() {
            /*
                r6 = this;
                android.content.Context r0 = r6.context
                me.gall.tinybee.LoggerManager$LoggerConfiguration r1 = r6.conf
                java.lang.String r1 = r1.getAppId()
                java.lang.String r0 = me.gall.tinybee.TinybeeLogger.getUUID(r0, r1)
                if (r0 != 0) goto L_0x0018
                java.lang.String r0 = "TinybeeLogger"
                java.lang.String r1 = "Null uuid. Register for uuid at first."
                android.util.Log.d(r0, r1)
                r6.registerForUUID()
            L_0x0018:
                r1 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0171 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0171 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x0171 }
                boolean r3 = r3.isSandboxMode()     // Catch:{ Exception -> 0x0171 }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getAnalysisURL(r3)     // Catch:{ Exception -> 0x0171 }
                java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0171 }
                r2.<init>(r3)     // Catch:{ Exception -> 0x0171 }
                java.lang.String r3 = r6.eventId     // Catch:{ Exception -> 0x0171 }
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0171 }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0171 }
                r0.<init>(r2)     // Catch:{ Exception -> 0x0171 }
                java.lang.String r2 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0171 }
                java.lang.String r4 = "POST:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x0171 }
                java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x0171 }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0171 }
                android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x0171 }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0171 }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0171 }
                r1 = 1
                r0.setDoOutput(r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r1 = 1
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r1 = "POST"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r1 = 0
                r0.setUseCaches(r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r1 = "Content-Type"
                java.lang.String r2 = "application/json"
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r1 = "UUID"
                android.content.Context r2 = r6.context     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r3 = r3.getAppId()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r2 = me.gall.tinybee.TinybeeLogger.getUUID(r2, r3)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                long r1 = r6.timestamp     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r3 = 0
                int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
                if (r1 == 0) goto L_0x0091
                java.lang.String r1 = "TIMESTAMP"
                long r2 = r6.timestamp     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
            L_0x0091:
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                org.json.JSONObject r1 = r6.params     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r2 = "APPID"
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.conf     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r3 = r3.getAppId()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r1.put(r2, r3)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                org.json.JSONObject r1 = r6.params     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                int r1 = r1.length()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                if (r1 <= 0) goto L_0x00de
                java.lang.String r1 = "TinybeeLogger"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r3 = "CONTENT:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                org.json.JSONObject r3 = r6.params     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                org.json.JSONObject r2 = r6.params     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r3 = "UTF-8"
                byte[] r2 = r2.getBytes(r3)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r1.write(r2)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r1.flush()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r1.close()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
            L_0x00de:
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r2 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r4 = "ResponseCode:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r2 = 200(0xc8, float:2.8E-43)
                if (r1 == r2) goto L_0x00fe
                r2 = 201(0xc9, float:2.82E-43)
                if (r1 != r2) goto L_0x0111
            L_0x00fe:
                java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r2 = "UTF-8"
                java.lang.String r1 = r6.read(r1, r2)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r6.consumeContent(r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
            L_0x010b:
                if (r0 == 0) goto L_0x0110
                r0.disconnect()
            L_0x0110:
                return
            L_0x0111:
                r2 = 304(0x130, float:4.26E-43)
                if (r1 != r2) goto L_0x012f
                java.lang.String r1 = "TinybeeLogger"
                java.lang.String r2 = "Resource not modified."
                android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                goto L_0x010b
            L_0x011d:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x0121:
                r0.printStackTrace()     // Catch:{ all -> 0x016f }
                r0 = 3
                r2 = 1
                r6.retry(r0, r2)     // Catch:{ all -> 0x016f }
                if (r1 == 0) goto L_0x0110
                r1.disconnect()
                goto L_0x0110
            L_0x012f:
                boolean r2 = r6.catchErrorCode(r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                if (r2 != 0) goto L_0x0154
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r4 = "Invalid response:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                r2.<init>(r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                throw r2     // Catch:{ Exception -> 0x011d, all -> 0x014a }
            L_0x014a:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x014e:
                if (r1 == 0) goto L_0x0153
                r1.disconnect()
            L_0x0153:
                throw r0
            L_0x0154:
                java.lang.String r2 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r4 = "ResponseCode:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r3 = " has been caught."
                java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                android.util.Log.d(r2, r1)     // Catch:{ Exception -> 0x011d, all -> 0x014a }
                goto L_0x010b
            L_0x016f:
                r0 = move-exception
                goto L_0x014e
            L_0x0171:
                r0 = move-exception
                goto L_0x0121
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.tinybee.TinybeeLogger.EventTask.uploadEvent():void");
        }
    }

    class RequestLiveParamsTask extends EventTask {
        private static final String TB_LIVE_PARAMS = "_TB_LIVE_PARAMS_";
        private Logger.OnlineParamCallback callback;

        public RequestLiveParamsTask(Context context, LoggerManager.LoggerConfiguration loggerConfiguration, Logger.OnlineParamCallback onlineParamCallback) {
            super(context, loggerConfiguration, null);
            this.callback = onlineParamCallback;
        }

        private String getLastTimestamp() {
            return this.context.getApplicationContext().getSharedPreferences("_TB_" + getConf().getAppId(), 0).getString("_TB_LAST_LIVEPARAM_REQUEST_TIMESTAMP", "0");
        }

        private Map<String, String> getLiveParams() {
            return this.context.getApplicationContext().getSharedPreferences(TB_LIVE_PARAMS + getConf().getAppId(), 0).getAll();
        }

        private void updateLastTimestamp(String str) {
            this.context.getApplicationContext().getSharedPreferences("_TB_" + getConf().getAppId(), 0).edit().putString("_TB_LAST_LIVEPARAM_REQUEST_TIMESTAMP", str).commit();
        }

        private void updateLiveParams(JSONObject jSONObject) {
            SharedPreferences.Editor edit = this.context.getApplicationContext().getSharedPreferences(TB_LIVE_PARAMS + getConf().getAppId(), 0).edit();
            edit.clear();
            edit.commit();
            try {
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    edit.putString(next, jSONObject.getString(next));
                }
            } catch (Exception e) {
                Log.d(TinybeeLogger.TAG, "Live params persist error." + e);
            }
            edit.commit();
        }

        public void appendOfflineData(Context context) {
            Log.d(TinybeeLogger.TAG, "Request live params task need not save.");
        }

        /* JADX WARNING: Removed duplicated region for block: B:24:0x00f3  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0112  */
        /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void requestLiveParams() {
            /*
                r6 = this;
                r1 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0118 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0118 }
                me.gall.tinybee.TinybeeLogger r3 = me.gall.tinybee.TinybeeLogger.this     // Catch:{ Exception -> 0x0118 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r3.conf     // Catch:{ Exception -> 0x0118 }
                boolean r3 = r3.isSandboxMode()     // Catch:{ Exception -> 0x0118 }
                java.lang.String r3 = me.gall.tinybee.TinybeeLogger.Config.getLiveParamsURL(r3)     // Catch:{ Exception -> 0x0118 }
                java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0118 }
                r2.<init>(r3)     // Catch:{ Exception -> 0x0118 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r3 = r6.getConf()     // Catch:{ Exception -> 0x0118 }
                java.lang.String r3 = r3.getAppId()     // Catch:{ Exception -> 0x0118 }
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0118 }
                java.lang.String r3 = "?v=2"
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0118 }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0118 }
                r0.<init>(r2)     // Catch:{ Exception -> 0x0118 }
                java.lang.String r2 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0118 }
                java.lang.String r4 = "GET:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x0118 }
                java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x0118 }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0118 }
                android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x0118 }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0118 }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0118 }
                r1 = 1
                r0.setDoOutput(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                r1 = 1
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r1 = "GET"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                r1 = 0
                r0.setUseCaches(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r1 = r6.getLastTimestamp()     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r2 = "TIMESTAMP"
                r0.setRequestProperty(r2, r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r2 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r4 = "TIMESTAMP:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                android.util.Log.d(r2, r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r2 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r4 = "ResponseCode:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                r2 = 200(0xc8, float:2.8E-43)
                if (r1 != r2) goto L_0x00cc
                org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r3 = "UTF-8"
                java.lang.String r2 = r6.read(r2, r3)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                r1.<init>(r2)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                r6.updateLiveParams(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                long r1 = me.gall.tinybee.TinybeeLogger.Config.getTimeStamp()     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                r6.updateLastTimestamp(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                me.gall.tinybee.Logger$OnlineParamCallback r1 = r6.callback     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                if (r1 == 0) goto L_0x00c6
                me.gall.tinybee.Logger$OnlineParamCallback r1 = r6.callback     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.util.Map r2 = r6.getLiveParams()     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                r1.requestComplete(r2)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
            L_0x00c6:
                if (r0 == 0) goto L_0x00cb
                r0.disconnect()
            L_0x00cb:
                return
            L_0x00cc:
                r2 = 304(0x130, float:4.26E-43)
                if (r1 != r2) goto L_0x00f7
                java.lang.String r1 = "TinybeeLogger"
                java.lang.String r2 = "Resource not modified."
                android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                me.gall.tinybee.Logger$OnlineParamCallback r1 = r6.callback     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                if (r1 == 0) goto L_0x00c6
                me.gall.tinybee.Logger$OnlineParamCallback r1 = r6.callback     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.util.Map r2 = r6.getLiveParams()     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                r1.requestComplete(r2)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                goto L_0x00c6
            L_0x00e5:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x00e9:
                r0.printStackTrace()     // Catch:{ all -> 0x0116 }
                r0 = 3
                r2 = 1
                r6.retry(r0, r2)     // Catch:{ all -> 0x0116 }
                if (r1 == 0) goto L_0x00cb
                r1.disconnect()
                goto L_0x00cb
            L_0x00f7:
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r4 = "Invalid response:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                r2.<init>(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
                throw r2     // Catch:{ Exception -> 0x00e5, all -> 0x010c }
            L_0x010c:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x0110:
                if (r1 == 0) goto L_0x0115
                r1.disconnect()
            L_0x0115:
                throw r0
            L_0x0116:
                r0 = move-exception
                goto L_0x0110
            L_0x0118:
                r0 = move-exception
                goto L_0x00e9
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.tinybee.TinybeeLogger.RequestLiveParamsTask.requestLiveParams():void");
        }

        public void run() {
            if (!Config.isConnect(this.context) || isNetworkError()) {
                Log.d(TinybeeLogger.TAG, "Network disabled. Return last saved params.");
                if (this.callback != null) {
                    this.callback.requestComplete(getLiveParams());
                    return;
                }
                return;
            }
            requestLiveParams();
        }
    }

    static class RequestUpdateTask extends EventTask {
        public RequestUpdateTask(Context context, LoggerManager.LoggerConfiguration loggerConfiguration) {
            super(context, loggerConfiguration, null);
        }

        private String getLastTimestamp() {
            return this.context.getApplicationContext().getSharedPreferences("_TB_" + getConf().getAppId(), 0).getString("_TB_LAST_UPDATE_TIMESTAMP", "0");
        }

        private void updateLastTimestamp(String str) {
            this.context.getApplicationContext().getSharedPreferences("_TB_" + getConf().getAppId(), 0).edit().putString("_TB_LAST_UPDATE_TIMESTAMP", str).commit();
        }

        public void appendOfflineData(Context context) {
            Log.d(TinybeeLogger.TAG, "Update info task need not save.");
        }

        public void consumeContent(String str) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                TinybeeLogger.saveUpdateInfo(this.context, getConf().getAppId(), jSONObject.getString("TITLE"), jSONObject.getInt("VIEW"), jSONObject.getString("CONTENT"), jSONObject.getInt("URL_TYPE"), jSONObject.getString("URL"), jSONObject.getInt("VERSION"), jSONObject.getBoolean("FORCE"), jSONObject.has("DELAY") ? jSONObject.getInt("DELAY") : 0);
                TinybeeLogger.updateIfNeed(this.context, getConf());
            } catch (JSONException e) {
                Log.w(TinybeeLogger.TAG, "Invalid update info:" + e);
            }
        }

        public void run() {
            Log.w(TinybeeLogger.TAG, "run");
            if (!Config.isConnect(this.context) || isNetworkError()) {
                Log.d(TinybeeLogger.TAG, "Network disabled.");
                return;
            }
            Log.w(TinybeeLogger.TAG, "updateInfo");
            updateInfo();
        }

        /* JADX WARNING: Removed duplicated region for block: B:33:0x0136  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void updateInfo() {
            /*
                r6 = this;
                r1 = 0
                java.lang.String r2 = "1"
                android.content.Context r0 = r6.context     // Catch:{ Exception -> 0x00d8 }
                android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ Exception -> 0x00d8 }
                android.content.Context r3 = r6.context     // Catch:{ Exception -> 0x00d8 }
                java.lang.String r3 = r3.getPackageName()     // Catch:{ Exception -> 0x00d8 }
                r4 = 0
                android.content.pm.PackageInfo r0 = r0.getPackageInfo(r3, r4)     // Catch:{ Exception -> 0x00d8 }
                int r0 = r0.versionCode     // Catch:{ Exception -> 0x00d8 }
                java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x00d8 }
            L_0x001a:
                java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x0151 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0151 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r4 = r6.getConf()     // Catch:{ Exception -> 0x0151 }
                boolean r4 = r4.isSandboxMode()     // Catch:{ Exception -> 0x0151 }
                java.lang.String r4 = me.gall.tinybee.TinybeeLogger.Config.getUpdateInfoURL(r4)     // Catch:{ Exception -> 0x0151 }
                java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0151 }
                r3.<init>(r4)     // Catch:{ Exception -> 0x0151 }
                java.lang.String r4 = "?appid="
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0151 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r4 = r6.getConf()     // Catch:{ Exception -> 0x0151 }
                java.lang.String r4 = r4.getAppId()     // Catch:{ Exception -> 0x0151 }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0151 }
                java.lang.String r4 = "&timestamp="
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0151 }
                java.lang.String r4 = r6.getLastTimestamp()     // Catch:{ Exception -> 0x0151 }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0151 }
                java.lang.String r4 = "&packagename="
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0151 }
                android.content.Context r4 = r6.context     // Catch:{ Exception -> 0x0151 }
                java.lang.String r4 = r4.getPackageName()     // Catch:{ Exception -> 0x0151 }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0151 }
                java.lang.String r4 = "&versioncode="
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0151 }
                java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x0151 }
                java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0151 }
                r2.<init>(r0)     // Catch:{ Exception -> 0x0151 }
                java.lang.String r0 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0151 }
                java.lang.String r4 = "GET:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x0151 }
                java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ Exception -> 0x0151 }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0151 }
                android.util.Log.d(r0, r3)     // Catch:{ Exception -> 0x0151 }
                java.net.URLConnection r0 = r2.openConnection()     // Catch:{ Exception -> 0x0151 }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0151 }
                r1 = 1
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r1 = "GET"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                r1 = 0
                r0.setUseCaches(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r2 = "TinybeeLogger"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r4 = "ResponseCode:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                r2 = 200(0xc8, float:2.8E-43)
                if (r1 != r2) goto L_0x00df
                long r1 = me.gall.tinybee.TinybeeLogger.Config.getTimeStamp()     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                r6.updateLastTimestamp(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r2 = "UTF-8"
                java.lang.String r1 = r6.read(r1, r2)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                r6.consumeContent(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
            L_0x00d2:
                if (r0 == 0) goto L_0x00d7
                r0.disconnect()
            L_0x00d7:
                return
            L_0x00d8:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ Exception -> 0x0151 }
                r0 = r2
                goto L_0x001a
            L_0x00df:
                r2 = 304(0x130, float:4.26E-43)
                if (r1 != r2) goto L_0x0108
                long r1 = me.gall.tinybee.TinybeeLogger.Config.getTimeStamp()     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                r6.updateLastTimestamp(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r1 = "TinybeeLogger"
                java.lang.String r2 = "Resource not modified."
                android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                goto L_0x00d2
            L_0x00f6:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x00fa:
                r0.printStackTrace()     // Catch:{ all -> 0x014f }
                r0 = 3
                r2 = 1
                r6.retry(r0, r2)     // Catch:{ all -> 0x014f }
                if (r1 == 0) goto L_0x00d7
                r1.disconnect()
                goto L_0x00d7
            L_0x0108:
                r2 = 404(0x194, float:5.66E-43)
                if (r1 != r2) goto L_0x013a
                long r1 = me.gall.tinybee.TinybeeLogger.Config.getTimeStamp()     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                r6.updateLastTimestamp(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r1 = "TinybeeLogger"
                java.lang.String r2 = "Update info not exist or disabled."
                android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                android.content.Context r1 = r6.context     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                me.gall.tinybee.LoggerManager$LoggerConfiguration r2 = r6.getConf()     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r2 = r2.getAppId()     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                me.gall.tinybee.TinybeeLogger.clearUpdateInfo(r1, r2)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                r1 = 1
                me.gall.tinybee.TinybeeLogger.mUpdateFromOpenToClose = r1     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                goto L_0x00d2
            L_0x0130:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x0134:
                if (r1 == 0) goto L_0x0139
                r1.disconnect()
            L_0x0139:
                throw r0
            L_0x013a:
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r4 = "Invalid response:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                r2.<init>(r1)     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
                throw r2     // Catch:{ Exception -> 0x00f6, all -> 0x0130 }
            L_0x014f:
                r0 = move-exception
                goto L_0x0134
            L_0x0151:
                r0 = move-exception
                goto L_0x00fa
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.tinybee.TinybeeLogger.RequestUpdateTask.updateInfo():void");
        }
    }

    protected TinybeeLogger(Context context2, LoggerManager.LoggerConfiguration loggerConfiguration) {
        if (loggerConfiguration == null) {
            throw new NullPointerException("LoggerConfiguration must not be null.");
        }
        this.context = context2;
        this.conf = loggerConfiguration;
        if (this.executorService == null) {
            this.executorService = Executors.newSingleThreadExecutor();
        }
    }

    /* access modifiers changed from: private */
    public static void clearUpdateInfo(Context context2, String str) {
        SharedPreferences.Editor edit = context2.getApplicationContext().getSharedPreferences("_TB_UPDATEINFO_" + str, 0).edit();
        edit.clear();
        edit.commit();
    }

    private void executeTask(Runnable runnable) {
        if (this.executorService.isShutdown() || this.executorService.isTerminated()) {
            Log.d(TAG, "Executor already shut down. Init a new executor.");
            this.executorService = null;
            this.executorService = Executors.newSingleThreadExecutor();
        }
        this.executorService.execute(runnable);
    }

    private static void fillIntent(Intent intent, Context context2, LoggerManager.LoggerConfiguration loggerConfiguration) {
        SharedPreferences sharedPreferences = context2.getApplicationContext().getSharedPreferences("_TB_UPDATEINFO_" + loggerConfiguration.getAppId(), 0);
        intent.putExtra("Title", sharedPreferences.getString("Title", "Untitled"));
        intent.putExtra("View", sharedPreferences.getInt("View", 0));
        intent.putExtra("Content", sharedPreferences.getString("Content", ""));
        intent.putExtra("URL_Type", sharedPreferences.getInt("URL_Type", 1));
        intent.putExtra("URL", sharedPreferences.getString("URL", "http://gall.me"));
        intent.putExtra("Version", sharedPreferences.getInt("Version", 1));
        intent.putExtra("Force", sharedPreferences.getBoolean("Force", true));
        intent.putExtra("AppId", loggerConfiguration.getAppId());
        intent.putExtra("ChannelId", loggerConfiguration.getChannelId());
        intent.putExtra("ChannelName", loggerConfiguration.getChannelName());
        intent.putExtra("SandboxMode", loggerConfiguration.isSandboxMode());
    }

    /* access modifiers changed from: private */
    public static String getUUID(Context context2, String str) {
        return context2.getApplicationContext().getSharedPreferences(TINYBEE_UUIDS, 0).getString(str, null);
    }

    private synchronized List<EventTask> retrieveOfflineTasks(Context context2, String str) {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList();
            SharedPreferences sharedPreferences = context2.getApplicationContext().getSharedPreferences("_TB_" + str, 0);
            int i = sharedPreferences.getInt("_TB_Length", 0);
            Log.d(TAG, "load() length = " + i);
            for (int i2 = 0; i2 < i; i2++) {
                String string = sharedPreferences.getString("_TB_KEY_" + i2 + "event", "");
                String string2 = sharedPreferences.getString("_TB_KEY_" + i2 + StructuredData.TYPE_OF_VALUE, null);
                String string3 = sharedPreferences.getString("_TB_KEY_" + i2 + "timeStamp", "0");
                Log.d(TAG, "load()  event:" + string + "   value:" + string2 + "   timestamp:" + string3);
                if (string != null) {
                    EventTask eventTask = new EventTask(context2, this.conf, string);
                    if (string2 != null) {
                        try {
                            eventTask.params = new JSONObject(string2);
                        } catch (JSONException e) {
                            Log.d(TAG, "invalid content = " + string2);
                        }
                    }
                    eventTask.timestamp = Long.parseLong(string3);
                    arrayList.add(eventTask);
                }
            }
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.clear();
            edit.commit();
        }
        return arrayList;
    }

    private synchronized void saveAtOnce() {
        if (!this.executorService.isShutdown()) {
            List<Runnable> shutdownNow = this.executorService.shutdownNow();
            Log.d(TAG, "There are " + shutdownNow.size() + " tasks running. Append offline data immediately.");
            if (!shutdownNow.isEmpty()) {
                Iterator<Runnable> it = shutdownNow.iterator();
                while (it.hasNext()) {
                    ((EventTask) it.next()).appendOfflineData(this.context);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static void saveUUID(Context context2, String str, String str2) {
        context2.getApplicationContext().getSharedPreferences(TINYBEE_UUIDS, 0).edit().putString(str, str2).commit();
    }

    /* access modifiers changed from: private */
    public static void saveUpdateInfo(Context context2, String str, String str2, int i, String str3, int i2, String str4, int i3, boolean z, int i4) {
        SharedPreferences.Editor edit = context2.getApplicationContext().getSharedPreferences("_TB_UPDATEINFO_" + str, 0).edit();
        edit.clear();
        edit.commit();
        edit.putString("Title", str2);
        edit.putInt("View", i);
        edit.putString("Content", str3);
        edit.putInt("URL_Type", i2);
        edit.putString("URL", str4);
        edit.putInt("Version", i3);
        edit.putBoolean("Force", z);
        edit.putInt("Delay", i4);
        edit.commit();
    }

    /* access modifiers changed from: private */
    public static void showUpdateAcitivity(Context context2, LoggerManager.LoggerConfiguration loggerConfiguration) {
        Intent intent = new Intent();
        fillIntent(intent, context2, loggerConfiguration);
        intent.setFlags(603979776);
        intent.setClass(context2, TinybeeUpdateActivity.class);
        context2.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public static void updateIfNeed(final Context context2, final LoggerManager.LoggerConfiguration loggerConfiguration) {
        int i;
        int i2 = context2.getApplicationContext().getSharedPreferences("_TB_UPDATEINFO_" + loggerConfiguration.getAppId(), 0).getInt("Version", 1);
        try {
            i = context2.getPackageManager().getPackageInfo(context2.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            i = 1;
        }
        if (i2 > i) {
            int i3 = context2.getApplicationContext().getSharedPreferences("_TB_UPDATEINFO_" + loggerConfiguration.getAppId(), 0).getInt("Delay", 0);
            Log.d(TAG, "Delay:" + i3);
            if (i3 > 0) {
                new Timer().schedule(new TimerTask() {
                    public void run() {
                        if (!TinybeeLogger.mUpdateFromOpenToClose) {
                            TinybeeLogger.showUpdateAcitivity(context2, loggerConfiguration);
                        }
                    }
                }, (long) (i3 * b.DEFAULT_MINIMAL_LOCATION_UPDATES));
            } else {
                showUpdateAcitivity(context2, loggerConfiguration);
            }
        } else {
            Log.d(TAG, "No need to update." + i2 + "<=" + i);
        }
    }

    public void finish() {
        LoggerManager.clear();
        long j = this.context.getApplicationContext().getSharedPreferences(TINYBEE_RUNTIMES, 0).getLong(this.conf.getAppId(), 0);
        if (j == 0) {
            Log.w(TAG, "Invalid start time 0. May init not called");
            return;
        }
        long timeStamp = Config.getTimeStamp() - j;
        EventTask eventTask = new EventTask(this.context, this.conf, "_TB_runtime");
        eventTask.addParam("RUNTIME", String.valueOf(timeStamp));
        eventTask.appendOfflineData(this.context);
    }

    public Context getContext() {
        return this.context;
    }

    public boolean hasOfflineData() {
        return this.context.getApplicationContext().getSharedPreferences(new StringBuilder("_TB_").append(this.conf.getAppId()).toString(), 0).getInt("_TB_Length", 0) > 0;
    }

    public void init() {
        if (!this.inited) {
            this.inited = true;
            updateIfNeed(this.context, this.conf);
            Log.d(TAG, "SandboxMode is " + isSandboxMode());
            launch();
            if (hasOfflineData()) {
                syncOfflineData();
            }
            this.context.getApplicationContext().getSharedPreferences(TINYBEE_RUNTIMES, 0).edit().putLong(this.conf.getAppId(), Config.getTimeStamp()).commit();
        }
    }

    public boolean isSandboxMode() {
        return this.context.getApplicationContext().getSharedPreferences("_TB_" + this.conf.getAppId(), 0).getBoolean("_TB_SANDBOX", false);
    }

    /* access modifiers changed from: protected */
    public void launch() {
        Log.d(TAG, "launch");
        EventTask eventTask = new EventTask(this.context, this.conf, _TB_launch);
        eventTask.addParam("NETWORK", Config.getNetwork(this.context));
        eventTask.addParam("IMSI", Config.getIMSI(this.context));
        eventTask.addParam("CHANNELNAME", this.conf.getChannelName());
        eventTask.addParam("CHANNELID", this.conf.getChannelId());
        try {
            eventTask.addParam("VERSIONSCODE", String.valueOf(this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionCode));
            eventTask.addParam("VERSIONSNAME", String.valueOf(this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, "VERSIONSCODE or VERSIONSNAME not found ");
        }
        eventTask.addParam("PACKAGENAME", this.context.getPackageName());
        executeTask(eventTask);
        mUpdateFromOpenToClose = false;
        executeTask(new RequestUpdateTask(this.context, this.conf));
    }

    public void onPause(Context context2) {
        saveAtOnce();
    }

    public void onResume(Context context2) {
        if (hasOfflineData()) {
            syncOfflineData();
        }
    }

    public void send(String str) {
        send(str, null);
    }

    public void send(String str, Map<String, String> map) {
        EventTask eventTask = new EventTask(this.context, this.conf, str);
        if (map != null) {
            for (String next : map.keySet()) {
                if (map.containsKey(next)) {
                    eventTask.addParam(next, map.get(next));
                }
            }
        }
        eventTask.addParam("channelId", LoggerManager.getCurrentCid());
        eventTask.addParam("channelName", LoggerManager.getCurrentChannelName());
        executeTask(eventTask);
    }

    public void sendException(HashMap<String, String> hashMap) {
        EventTask eventTask = new EventTask(this.context, this.conf, _TB_crash);
        for (Map.Entry next : hashMap.entrySet()) {
            eventTask.addParam(next.getKey().toString(), next.getValue().toString());
            Log.i(next.getKey().toString(), next.getValue().toString());
        }
        eventTask.addParam(EventTask.APPID, this.conf.getAppId());
        executeTask(eventTask);
    }

    public void setOnlineParamCallback(Logger.OnlineParamCallback onlineParamCallback) {
        if (onlineParamCallback != null) {
            updateOnlineParam(onlineParamCallback);
        }
    }

    public void syncOfflineData() {
        if (Config.isConnect(this.context)) {
            for (EventTask next : retrieveOfflineTasks(this.context, this.conf.getAppId())) {
                Log.d(TAG, "Start to sync " + next.getEventId());
                executeTask(next);
            }
            return;
        }
        Log.d(TAG, "No need to sync offline data in none network enviroment.");
    }

    public void updateOnlineParam(Logger.OnlineParamCallback onlineParamCallback) {
        try {
            executeTask(new RequestLiveParamsTask(this.context, this.conf, onlineParamCallback));
        } catch (Exception e) {
            Log.e(TAG, "初始化或更新在线参数异常:" + e.getMessage());
        }
    }
}
