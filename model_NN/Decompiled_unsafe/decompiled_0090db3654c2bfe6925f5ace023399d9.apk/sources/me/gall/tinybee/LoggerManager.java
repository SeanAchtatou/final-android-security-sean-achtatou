package me.gall.tinybee;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Process;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import me.gall.tinybee.TinybeeLogger;

public class LoggerManager {
    private static final String LOGGER_TYPE_TINYBEE = "tinybee";
    private static final String TAG = "LoggerManager";
    private static final String _TB_LOGGER_KEYS = "_TB_LOGGER_KEYS_";
    private static String channelId = "";
    private static String channelName = "";
    private static CrashHandler crashHandler;
    private static boolean isTestMode = false;
    private static HashMap<String, Logger> loggers = new HashMap<>();
    private static Logger mCurrentLogger;

    static final class LoggerConfiguration {
        private String appId;
        private String channelId;
        private String channelName;
        private String packageName;
        private boolean sandboxMode;
        private String type;

        public LoggerConfiguration(String str, String str2, String str3, String str4, String str5, boolean z) {
            this.type = str;
            this.appId = str2;
            this.channelId = str3;
            this.channelName = str4;
            this.packageName = str5;
            this.sandboxMode = z;
        }

        public String getAppId() {
            return this.appId;
        }

        public String getChannelId() {
            return this.channelId;
        }

        public String getChannelName() {
            return this.channelName;
        }

        public String getPackageName() {
            return this.packageName;
        }

        public String getType() {
            return this.type;
        }

        public boolean isSandboxMode() {
            return this.sandboxMode;
        }

        public void setAppId(String str) {
            this.appId = str;
        }

        public void setChannelId(String str) {
            this.channelId = str;
        }

        public void setChannelName(String str) {
            this.channelName = str;
        }

        public void setPackageName(String str) {
            this.packageName = str;
        }

        public void setSandboxMode(boolean z) {
            this.sandboxMode = z;
        }

        public void setType(String str) {
            this.type = str;
        }
    }

    public static class NetworkStateChangeReceiver extends BroadcastReceiver {
        protected static final String CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";
        protected static final String WIFI_STATE_CHANGED = "android.net.wifi.WIFI_STATE_CHANGED";
        protected static final String WIFI_STATE_CHANGED_2 = "android.net.wifi.supplicant.CONNECTION_CHANGE";

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (!action.equals(CONNECTIVITY_CHANGE) && !action.equals(WIFI_STATE_CHANGED) && !action.equals(WIFI_STATE_CHANGED_2)) {
                return;
            }
            if (TinybeeLogger.Config.isConnect(context)) {
                List<Logger> access$0 = LoggerManager.listAllLoggers(context);
                Log.d(LoggerManager.TAG, String.valueOf(access$0.size()) + " loggers wait to sync.");
                for (Logger logger : access$0) {
                    if (logger.hasOfflineData()) {
                        logger.syncOfflineData();
                    }
                }
                Log.d(LoggerManager.TAG, "Sync offline data complete.");
                return;
            }
            Log.d(LoggerManager.TAG, "None available connection.");
        }
    }

    public static void clear() {
        Log.d(TAG, "");
        if (loggers != null && !loggers.isEmpty()) {
            loggers.clear();
        }
    }

    @Deprecated
    public static void enableTestMode(boolean z) {
        isTestMode = z;
    }

    static void forceFinish() {
        Log.w(TAG, "Force finish.");
        for (Logger finish : loggers.values()) {
            finish.finish();
        }
        for (Logger next : loggers.values()) {
            if (next.getContext() != null) {
                try {
                    ((Activity) next.getContext()).finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        System.gc();
        Process.killProcess(Process.myPid());
        System.exit(0);
    }

    private static String generateLoggerKey(String str, String str2, String str3) {
        return String.valueOf(str) + "_TB_" + str2 + "_TB_" + str3;
    }

    public static String getCurrentChannelName() {
        return channelName;
    }

    public static String getCurrentCid() {
        return channelId;
    }

    public static Logger getCurrentLogger() {
        return mCurrentLogger;
    }

    public static Logger getLogger(Context context) {
        boolean z;
        Exception e;
        String str = null;
        String str2 = "";
        String str3 = "";
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            str = applicationInfo.metaData.getString("TB_APP_ID");
            str2 = applicationInfo.metaData.getString("TB_CHANNEL_ID");
            str3 = applicationInfo.metaData.getString("TB_CHANNEL_NAME");
            z = applicationInfo.metaData.getBoolean("TB_SANDBOX_MODE");
            if (str == null) {
                try {
                    throw new Exception("TB_APP_ID must be valid.");
                } catch (Exception e2) {
                    e = e2;
                    Log.w(TAG, e.getMessage());
                    return getLogger(context, str, str2, str3, z);
                }
            }
        } catch (Exception e3) {
            Exception exc = e3;
            z = false;
            e = exc;
        }
        return getLogger(context, str, str2, str3, z);
    }

    public static Logger getLogger(Context context, String str) {
        return getLogger(context, str, "", "", false);
    }

    public static Logger getLogger(Context context, String str, String str2) {
        return getLogger(context, str, str2, "", false);
    }

    public static Logger getLogger(Context context, String str, String str2, String str3) {
        return getLogger(context, str, str2, str3, false);
    }

    public static Logger getLogger(Context context, String str, String str2, String str3, boolean z) {
        String str4;
        String str5;
        Log.d(TAG, "getLogger");
        String generateLoggerKey = generateLoggerKey(str, str2, str3);
        if (!loggers.containsKey(generateLoggerKey)) {
            Log.d(TAG, "getLogger,no log instance with key:" + generateLoggerKey);
            if (str == null || str.equals("")) {
                throw new NullPointerException("APPID can not be null");
            }
            str5 = str2 == null ? "" : str2;
            str4 = str3 == null ? "" : str3;
            LoggerConfiguration loggerConfiguration = new LoggerConfiguration(LOGGER_TYPE_TINYBEE, str, str5, str4, context.getPackageName(), z);
            try {
                TinybeeLogger tinybeeLogger = new TinybeeLogger(context, loggerConfiguration);
                Log.d(TAG, "Logger " + str + " is done.");
                saveLoggerConfiguration(context, loggerConfiguration);
                tinybeeLogger.init();
                loggers.put(generateLoggerKey, tinybeeLogger);
            } catch (Exception e) {
                Log.e(TAG, "Logger could not create because of " + e.getMessage());
                new EmptyLogger();
            }
        } else {
            str4 = str3;
            str5 = str2;
        }
        channelId = str5;
        channelName = str4;
        if (crashHandler == null) {
            crashHandler = new CrashHandler(context);
        }
        Thread.setDefaultUncaughtExceptionHandler(crashHandler);
        Log.i("11111", "222222");
        mCurrentLogger = loggers.get(generateLoggerKey);
        return loggers.get(generateLoggerKey);
    }

    @Deprecated
    public static boolean isTestMode() {
        return isTestMode;
    }

    /* access modifiers changed from: private */
    public static List<Logger> listAllLoggers(Context context) {
        ArrayList arrayList = new ArrayList();
        for (String next : context.getApplicationContext().getSharedPreferences(_TB_LOGGER_KEYS, 0).getAll().keySet()) {
            SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(_TB_LOGGER_KEYS + next, 0);
            if (next != null) {
                String string = sharedPreferences.getString("CHANNELID", "");
                String string2 = sharedPreferences.getString("CHANNELNAME", "");
                arrayList.add(new TinybeeLogger(context, new LoggerConfiguration(sharedPreferences.getString("TYPE", LOGGER_TYPE_TINYBEE), next, string, string2, context.getPackageName(), sharedPreferences.getBoolean("SANDBOXMODE", false))));
            }
        }
        return arrayList;
    }

    private static void saveLoggerConfiguration(Context context, LoggerConfiguration loggerConfiguration) {
        if (!context.getApplicationContext().getSharedPreferences(_TB_LOGGER_KEYS, 0).contains(loggerConfiguration.getAppId())) {
            SharedPreferences.Editor edit = context.getApplicationContext().getSharedPreferences(_TB_LOGGER_KEYS, 0).edit();
            edit.putString(loggerConfiguration.getAppId(), _TB_LOGGER_KEYS + loggerConfiguration.getAppId());
            edit.commit();
            SharedPreferences.Editor edit2 = context.getApplicationContext().getSharedPreferences(_TB_LOGGER_KEYS + loggerConfiguration.getAppId(), 0).edit();
            edit2.putString("CHANNELID", loggerConfiguration.getChannelId());
            edit2.putString("CHANNELNAME", loggerConfiguration.getChannelName());
            edit2.putString("TYPE", loggerConfiguration.getType());
            edit2.putBoolean("SANDBOXMODE", loggerConfiguration.isSandboxMode());
            edit2.commit();
        }
    }
}
