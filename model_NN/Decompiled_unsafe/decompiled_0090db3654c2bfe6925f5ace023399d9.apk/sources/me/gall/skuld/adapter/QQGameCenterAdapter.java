package me.gall.skuld.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.tencent.webnet.PreSMSReturn;
import com.tencent.webnet.WebNetEvent;
import com.tencent.webnet.WebNetInterface;
import java.io.FileNotFoundException;
import java.util.Map;
import me.gall.skuld.SNSPlatformManager;
import me.gall.skuld.adapter.SNSPlatformAdapter;
import me.gall.skuld.util.DataMapper;
import org.opensocial.models.AppData;
import org.opensocial.models.skuld.Achievement;
import org.opensocial.models.skuld.Application;
import org.opensocial.models.skuld.Billing;
import org.opensocial.models.skuld.Challenge;
import org.opensocial.models.skuld.Lobby;
import org.opensocial.models.skuld.Player;
import org.opensocial.models.skuld.Score;

public class QQGameCenterAdapter extends a implements WebNetEvent {
    /* access modifiers changed from: private */
    public static SNSPlatformAdapter.AsyncResultCallback<Billing> nj;
    private boolean ni;
    private Billing nk;

    public static class SMSPaymentActivity extends Activity implements Handler.Callback {
        private static final int HIDE_PROCESSING_DIALOG = 2;
        private static final String LOG = "SMSPaymentActivity";
        private static final int SHOW_PROCESSING_DIALOG = 1;
        /* access modifiers changed from: private */
        public Handler handler;
        /* access modifiers changed from: private */
        public String nn;
        private ProgressDialog np;
        private ViewGroup nq;

        /* access modifiers changed from: private */
        public void fI() {
            this.handler.sendEmptyMessage(1);
        }

        private void fJ() {
            this.handler.sendEmptyMessage(2);
        }

        /* access modifiers changed from: private */
        public void fK() {
            fJ();
            finish();
        }

        public static final View findViewByName(Context context, View view, String str) {
            return view.findViewById(getViewIdentifier(context, str));
        }

        public static final int getIdentifier(Context context, String str, String str2) {
            int identifier = context.getResources().getIdentifier(str, str2, context.getPackageName());
            if (identifier != 0) {
                return identifier;
            }
            throw new FileNotFoundException(String.valueOf(str) + " is not found in res/" + str2 + "/.");
        }

        public static final int getLayoutIdentifier(Context context, String str) {
            return getIdentifier(context, str, "layout");
        }

        public static final int getViewIdentifier(Context context, String str) {
            try {
                return getIdentifier(context, str, "id");
            } catch (FileNotFoundException e) {
                throw new NullPointerException(String.valueOf(str) + " is not found.");
            }
        }

        public static final View inflateView(Context context, String str) {
            return ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(getLayoutIdentifier(context, str), (ViewGroup) null);
        }

        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (getWindow().isActive()) {
                        if (this.np == null) {
                            this.np = new ProgressDialog(this);
                            this.np.setMessage("请稍候");
                            this.np.setCancelable(false);
                        }
                        if (!this.np.isShowing()) {
                            this.np.show();
                            break;
                        }
                    }
                    break;
                case 2:
                    if (this.np != null && this.np.isShowing()) {
                        this.np.hide();
                        break;
                    }
            }
            return false;
        }

        public void onBackPressed() {
            fK();
        }

        /* access modifiers changed from: protected */
        public void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            getWindow().requestFeature(1);
            Intent intent = getIntent();
            this.handler = new Handler(this);
            String stringExtra = intent.hasExtra("TEXT") ? intent.getStringExtra("TEXT") : null;
            this.nn = intent.getStringExtra("BILLINGID");
            try {
                this.nq = (ViewGroup) inflateView(this, "skuld_layout_main");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            setContentView(this.nq);
            Log.d(LOG, "TEXT:" + stringExtra);
            ((TextView) findViewByName(this, this.nq, "skuld_view_content")).setText(stringExtra);
            ((Button) findViewByName(this, this.nq, "skuld_view_confirm")).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    SMSPaymentActivity.this.fI();
                    String str = "SMS_TECENT_BILLING_MARK:" + System.currentTimeMillis();
                    Log.d(SMSPaymentActivity.LOG, "Sending :" + SMSPaymentActivity.this.nn + " mark:" + str);
                    WebNetInterface.SMSBillingPoint(Integer.parseInt(SMSPaymentActivity.this.nn), str);
                    SMSPaymentActivity.this.handler.postDelayed(new Runnable() {
                        public void run() {
                            SMSPaymentActivity.this.fK();
                        }
                    }, 5000);
                }
            });
            ((Button) findViewByName(this, this.nq, "skuld_view_cancel")).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (QQGameCenterAdapter.nj != null) {
                        QQGameCenterAdapter.nj.aq("用户取消计费");
                    }
                    SMSPaymentActivity.this.fK();
                }
            });
        }

        public void onDestroy() {
            if (this.np != null) {
                this.np.dismiss();
            }
            this.np = null;
            super.onDestroy();
        }
    }

    private String bs(int i) {
        String str;
        switch (i) {
            case -107:
                str = "Error_Type_Sys";
                break;
            case -106:
                str = "Error_Type_Rec";
                break;
            case -105:
                str = "Error_Type_Send";
                break;
            case -104:
                str = "Error_Type_Connect";
                break;
            case -103:
                str = "FUNCTION_NA";
                break;
            case -102:
                str = "NOT_LOGIN";
                break;
            case -101:
                str = "NET_NOT_AVAILABLE";
                break;
            case 1031:
                str = "BillingPoint_Event_Error";
                break;
            case 1041:
                str = "SendSMS_Event_Error";
                break;
            case 1042:
                str = "SendSMS_Event_Generic_Failure";
                break;
            case 1043:
                str = "SendSMS_Event_Radio_Off";
                break;
            case 1044:
                str = "SendSMS_Event_NULL_PDU";
                break;
            case 1045:
                str = "SendSMS_Event_INIT_ERROR";
                break;
            default:
                str = "Unknown error";
                break;
        }
        Log.w(getName(), str);
        return str;
    }

    public /* bridge */ /* synthetic */ AppData a(Player player) {
        return super.a(player);
    }

    public /* bridge */ /* synthetic */ void a(String str, Bitmap bitmap) {
        super.a(str, bitmap);
    }

    public /* bridge */ /* synthetic */ void a(DataMapper dataMapper) {
        super.a(dataMapper);
    }

    public /* bridge */ /* synthetic */ void a(AppData appData) {
        super.a(appData);
    }

    public /* bridge */ /* synthetic */ void a(AppData appData, SNSPlatformAdapter.AsyncResultCallback asyncResultCallback) {
        super.a(appData, asyncResultCallback);
    }

    public /* bridge */ /* synthetic */ void a(Achievement achievement) {
        super.a(achievement);
    }

    public void a(Achievement achievement, SNSPlatformAdapter.AsyncResultCallback<Achievement> asyncResultCallback) {
        WebNetInterface.UpdateAchievement(Integer.parseInt(achievement.getId()));
    }

    public /* bridge */ /* synthetic */ void a(Billing billing) {
        super.a(billing);
    }

    public void a(Billing billing, SNSPlatformAdapter.AsyncResultCallback<Billing> asyncResultCallback) {
        Log.d(getName(), "=========Start Billing========");
        if (billing.type == 1) {
            Log.d(getName(), "=========It is a SMS Billing========");
            Log.d(getName(), "Get the sms desp.");
            nj = asyncResultCallback;
            this.nk = billing;
            PreSMSReturn PreSMSBillingPoint = WebNetInterface.PreSMSBillingPoint(Integer.parseInt(billing.getId()));
            if (PreSMSBillingPoint.m_bSuccess) {
                Log.d(getName(), "Pre SMS Billing:" + PreSMSBillingPoint.m_contents);
                Intent intent = new Intent();
                intent.setClass(SNSPlatformManager.fs(), SMSPaymentActivity.class);
                intent.putExtra("TEXT", PreSMSBillingPoint.m_contents);
                intent.putExtra("BILLINGID", billing.getId());
                SNSPlatformManager.fs().startActivity(intent);
                return;
            }
            Log.w(getName(), "Fail Pre SMS Billing." + PreSMSBillingPoint.m_contents);
            asyncResultCallback.aq("Could not get fee description.");
            return;
        }
        Log.d(getName(), "Not support update normal billing.");
        Log.d(getName(), "Normal Billing:" + billing.getId());
    }

    public /* bridge */ /* synthetic */ void a(Challenge challenge) {
        super.a(challenge);
    }

    public /* bridge */ /* synthetic */ void a(Challenge challenge, SNSPlatformAdapter.AsyncResultCallback asyncResultCallback) {
        super.a(challenge, asyncResultCallback);
    }

    public /* bridge */ /* synthetic */ void a(Score score) {
        super.a(score);
    }

    public void a(Score score, SNSPlatformAdapter.AsyncResultCallback<Score> asyncResultCallback) {
        WebNetInterface.UpdateScore(Integer.parseInt(score.getId()), score.score);
    }

    public /* bridge */ /* synthetic */ Player[] a(Lobby lobby, int i) {
        return super.a(lobby, i);
    }

    public /* bridge */ /* synthetic */ void ap(String str) {
        super.ap(str);
    }

    public /* bridge */ /* synthetic */ Player b(Player player) {
        return super.b(player);
    }

    public /* bridge */ /* synthetic */ void b(DataMapper dataMapper) {
        super.b(dataMapper);
    }

    public void bq(final int i) {
        SNSPlatformManager.fs().runOnUiThread(new Runnable() {
            public void run() {
                if (i == 0) {
                    WebNetInterface.StartWeb(SNSPlatformManager.fs());
                } else {
                    WebNetInterface.StartWeb(SNSPlatformManager.fs(), i);
                }
            }
        });
    }

    public void br(int i) {
    }

    public void c(Map<String, String> map) {
        super.c(map);
        WebNetInterface.Init(SNSPlatformManager.fs(), this);
    }

    public /* bridge */ /* synthetic */ void c(DataMapper dataMapper) {
        super.c((DataMapper<String>) dataMapper);
    }

    public boolean e(int i, String str) {
        Log.d(getName(), "Result mark:" + str);
        if (i != 1040 || this.nk == null) {
            bs(i);
            nj.aq(bs(i));
            return false;
        }
        nj.f(this.nk);
        return false;
    }

    public boolean f(int i, String str) {
        return false;
    }

    public /* bridge */ /* synthetic */ DataMapper fA() {
        return super.fA();
    }

    public /* bridge */ /* synthetic */ DataMapper fB() {
        return super.fB();
    }

    public /* bridge */ /* synthetic */ DataMapper fC() {
        return super.fC();
    }

    public /* bridge */ /* synthetic */ Application fD() {
        return super.fD();
    }

    public /* bridge */ /* synthetic */ Lobby fE() {
        return super.fE();
    }

    public /* bridge */ /* synthetic */ boolean fF() {
        return super.fF();
    }

    public /* bridge */ /* synthetic */ String fG() {
        return super.fG();
    }

    public Bitmap fu() {
        return null;
    }

    public Bitmap fv() {
        return null;
    }

    public String fw() {
        return null;
    }

    public String fx() {
        return null;
    }

    public void fy() {
    }

    public /* bridge */ /* synthetic */ String fz() {
        return super.fz();
    }

    public /* bridge */ /* synthetic */ String getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ String getKey() {
        return super.getKey();
    }

    public String getName() {
        return getClass().getName();
    }

    public void onDestroy() {
        if (this.ni) {
            WebNetInterface.Destroy();
        }
    }

    public void onPause() {
    }

    public void onResume() {
        WebNetInterface.SetCurActivity(SNSPlatformManager.fs());
    }

    public /* bridge */ /* synthetic */ void setId(String str) {
        super.setId(str);
    }

    public /* bridge */ /* synthetic */ void setKey(String str) {
        super.setKey(str);
    }

    public /* bridge */ /* synthetic */ Player t(boolean z) {
        return super.t(z);
    }
}
