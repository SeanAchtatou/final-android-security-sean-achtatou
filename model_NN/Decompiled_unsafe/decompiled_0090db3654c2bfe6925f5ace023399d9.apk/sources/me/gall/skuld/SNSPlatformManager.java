package me.gall.skuld;

import android.app.Activity;
import android.util.Log;
import java.util.Map;
import me.gall.skuld.adapter.SNSPlatformAdapter;
import me.gall.skuld.util.Configuration;

public final class SNSPlatformManager {
    public static final String META_TAG_SKULD_ADAPTER_NAME = "SKULD_ADAPTER_NAME";
    private static final String TAG = "SNSPlatformManager";
    private static SNSPlatformAdapter ne;
    private static Activity nf;
    private static String ng;
    private static boolean nh;

    public static void a(Activity activity, String str, Map<String, String> map) {
        nf = activity;
        ng = str;
        try {
            b(map);
            nh = true;
            Log.i(TAG, "Skuld is initilized. Everything has been prepared.");
        } catch (Exception e) {
            Log.e(TAG, "The SDK is not initilized because of " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void b(Map<String, String> map) {
        Class<?> cls = Class.forName("me.gall.skuld.adapter." + Configuration.getManifestMetaValue(nf, META_TAG_SKULD_ADAPTER_NAME) + "Adapter");
        Log.i(META_TAG_SKULD_ADAPTER_NAME, cls.getName());
        ne = (SNSPlatformAdapter) cls.newInstance();
        ne.c(map);
        Log.i(TAG, "Skuld is created.");
    }

    public static SNSPlatformAdapter fr() {
        if (nh) {
            return ne;
        }
        throw new NullPointerException("SNSPlatformAdapter has not initilized. May be SKULD_ADAPTER_NAME in the manifest is wrong spelled or check the log for detail.");
    }

    public static Activity fs() {
        return nf;
    }

    public static String ft() {
        return ng;
    }

    public static final void onDestroy() {
        if (nh) {
            ne.onDestroy();
            Log.i(TAG, "Skuld is destroyed.");
            return;
        }
        Log.w(TAG, "Skuld is not initilized yet.");
    }

    public static final void onPause() {
        if (nh) {
            ne.onPause();
            Log.i(TAG, "Skuld is paused.");
            return;
        }
        Log.w(TAG, "Skuld is not initilized yet.");
    }

    public static final void onResume() {
        if (nh) {
            ne.onResume();
            Log.i(TAG, "Skuld is resumed.");
            return;
        }
        Log.w(TAG, "Skuld is not initilized yet.");
    }
}
