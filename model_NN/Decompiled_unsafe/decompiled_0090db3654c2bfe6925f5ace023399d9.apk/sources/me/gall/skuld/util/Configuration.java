package me.gall.skuld.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Configuration {
    private static final String LOG_TAG = "skuld:Configuration";

    public static String a(Activity activity) {
        try {
            return ((TelephonyManager) activity.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                StringBuffer stringBuffer = new StringBuffer("android_id");
                stringBuffer.append("Skuld");
                return w(instance.digest(stringBuffer.toString().getBytes()));
            } catch (NoSuchAlgorithmException e2) {
                return "00000000000000000000000000000000";
            }
        }
    }

    public static void a(Activity activity, String str, String str2) {
        SharedPreferences.Editor edit = activity.getPreferences(0).edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public static String b(Activity activity, String str, String str2) {
        return activity.getPreferences(0).getString(str, str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0072, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0073, code lost:
        r1 = r0;
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0023, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0072 A[ExcHandler: ClassCastException (r0v10 'e' java.lang.ClassCastException A[CUSTOM_DECLARE]), Splitter:B:3:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0022 A[ExcHandler: NameNotFoundException (r0v7 'e' android.content.pm.PackageManager$NameNotFoundException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getManifestMetaValue(android.content.Context r5, java.lang.String r6) {
        /*
            r1 = 0
            android.content.pm.PackageManager r0 = r5.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0022, ClassCastException -> 0x0047 }
            java.lang.String r2 = r5.getPackageName()     // Catch:{ NameNotFoundException -> 0x0022, ClassCastException -> 0x0047 }
            r3 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r2 = r0.getApplicationInfo(r2, r3)     // Catch:{ NameNotFoundException -> 0x0022, ClassCastException -> 0x0047 }
            android.os.Bundle r0 = r2.metaData     // Catch:{ NameNotFoundException -> 0x0022, ClassCastException -> 0x0072 }
            java.lang.String r0 = r0.getString(r6)     // Catch:{ NameNotFoundException -> 0x0022, ClassCastException -> 0x0072 }
            if (r0 != 0) goto L_0x0021
            android.os.Bundle r1 = r2.metaData     // Catch:{ NameNotFoundException -> 0x0076, ClassCastException -> 0x0072 }
            int r1 = r1.getInt(r6)     // Catch:{ NameNotFoundException -> 0x0076, ClassCastException -> 0x0072 }
            java.lang.String r0 = java.lang.String.valueOf(r1)     // Catch:{ NameNotFoundException -> 0x0076, ClassCastException -> 0x0072 }
        L_0x0021:
            return r0
        L_0x0022:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x0026:
            java.lang.String r2 = "skuld:Configuration"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r1 = r1.getMessage()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r3.<init>(r1)
            java.lang.String r1 = " unknown meta key or not exist:"
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r1 = r1.toString()
            android.util.Log.w(r2, r1)
            goto L_0x0021
        L_0x0047:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x004b:
            android.os.Bundle r0 = r0.metaData
            int r0 = r0.getInt(r6)
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r2 = "skuld:Configuration"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r1 = r1.getMessage()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r3.<init>(r1)
            java.lang.String r1 = " int to String error"
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r2, r1)
            goto L_0x0021
        L_0x0072:
            r0 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x004b
        L_0x0076:
            r1 = move-exception
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: me.gall.skuld.util.Configuration.getManifestMetaValue(android.content.Context, java.lang.String):java.lang.String");
    }

    public static int h(Context context, String str) {
        String str2 = null;
        try {
            str2 = getManifestMetaValue(context, str);
            return Integer.parseInt(str2);
        } catch (Exception e) {
            Log.w(LOG_TAG, String.valueOf(e.getMessage()) + " wrong trans:" + str + " with " + str2);
            return 0;
        }
    }

    public static String w(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            byte b2 = (b >>> 4) & 15;
            int i = 0;
            while (true) {
                if (b2 < 0 || b2 > 9) {
                    stringBuffer.append((char) ((b2 - 10) + 97));
                } else {
                    stringBuffer.append((char) (b2 + 48));
                }
                byte b3 = b & 15;
                int i2 = i + 1;
                if (i >= 1) {
                    break;
                }
                i = i2;
                b2 = b3;
            }
        }
        return stringBuffer.toString();
    }
}
