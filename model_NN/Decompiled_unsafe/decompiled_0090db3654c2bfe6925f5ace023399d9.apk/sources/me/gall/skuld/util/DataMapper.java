package me.gall.skuld.util;

import java.util.HashMap;

public class DataMapper<V> extends HashMap<Integer, V> {
    private static final long serialVersionUID = 1;

    public DataMapper<V> b(V[] vArr) {
        if (vArr == null || vArr.length <= 0) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < vArr.length; i++) {
            put(Integer.valueOf(i + 1), vArr[i]);
        }
        return this;
    }

    public V bt(int i) {
        if (i <= size() && i > 0) {
            return get(Integer.valueOf(i));
        }
        throw new ArrayIndexOutOfBoundsException();
    }
}
