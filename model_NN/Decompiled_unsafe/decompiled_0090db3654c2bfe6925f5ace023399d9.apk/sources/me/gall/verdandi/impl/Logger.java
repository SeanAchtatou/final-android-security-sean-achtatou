package me.gall.verdandi.impl;

import me.gall.verdandi.ILogger;
import org.meteoroid.core.h;
import org.meteoroid.core.l;

public class Logger implements ILogger {
    public void a(String str, String[] strArr, String[] strArr2) {
        String[] strArr3 = new String[(strArr.length + strArr2.length + 1)];
        strArr3[0] = str;
        for (int i = 1; i < strArr3.length; i += 2) {
            strArr3[i] = strArr[i / 2];
            strArr3[i + 1] = strArr2[i / 2];
        }
        h.d(l.MSG_SYSTEM_LOG_EVENT_BY_APPENDER, strArr3);
    }

    public void f(String str, String str2, String str3) {
        if (str2 == null || str3 == null) {
            h.d(l.MSG_SYSTEM_LOG_APPENDER, str);
            return;
        }
        h.d(l.MSG_SYSTEM_LOG_APPENDER, new String[]{str, str2, str3});
    }

    public void g(String str, String str2, String str3) {
        h.d(l.MSG_SYSTEM_LOG_EVENT_BY_APPENDER, new String[]{str, str2, str3});
    }

    public void send(String str) {
        h.d(l.MSG_SYSTEM_LOG_EVENT_BY_APPENDER, new String[]{str});
    }
}
