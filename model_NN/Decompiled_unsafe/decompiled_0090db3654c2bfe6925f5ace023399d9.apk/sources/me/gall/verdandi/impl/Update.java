package me.gall.verdandi.impl;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import me.gall.verdandi.APIBridge;
import me.gall.verdandi.APINotAvailableException;
import me.gall.verdandi.IUpdate;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class Update extends Activity implements IUpdate {
    String oe;
    String of;
    ProgressDialog og;
    Handler oh = new Handler();
    IntentFilter oi;
    a oj;
    boolean ok = false;

    class a extends BroadcastReceiver {
        a() {
        }

        public void onReceive(Context context, Intent intent) {
            File file = new File(Environment.getExternalStorageDirectory(), Update.this.of);
            if (file.exists()) {
                file.delete();
            }
            Update.this.unregisterReceiver(Update.this.oj);
        }
    }

    public Update(String str) {
        this.oe = str;
        start();
    }

    private void c(final Intent intent) {
        this.oh.post(new Runnable() {
            public void run() {
                AlertDialog.Builder message = new AlertDialog.Builder(Update.this).setTitle("安装完成").setMessage("是否现在运行");
                final Intent intent = intent;
                message.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Update.this.startActivity(intent);
                        Update.this.finish();
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Update.this.finish();
                    }
                }).create().show();
            }
        });
    }

    private void gL() {
        this.oh.post(new Runnable() {
            public void run() {
                Update.this.og.cancel();
                new AlertDialog.Builder(Update.this).setTitle("下载完成").setMessage("是否安装新的应用").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Update.this.gJ();
                        Update.this.gK();
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Update.this.gK();
                    }
                }).create().show();
            }
        });
    }

    public boolean gH() {
        return this.oe.startsWith("http://") || this.oe.startsWith("https://");
    }

    public void gI() {
        this.og.show();
        if (gH()) {
            try {
                if (APIBridge.getDeviceUtil().fR()) {
                    new Thread() {
                        public void run() {
                            try {
                                InputStream content = new DefaultHttpClient().execute(new HttpGet(Update.this.oe)).getEntity().getContent();
                                if (content == null) {
                                    throw new RuntimeException("isStream is null");
                                }
                                FileOutputStream fileOutputStream = new FileOutputStream(new File(Environment.getExternalStorageDirectory(), Update.this.of));
                                byte[] bArr = new byte[1024];
                                while (true) {
                                    int read = content.read(bArr);
                                    if (read <= 0) {
                                        content.close();
                                        fileOutputStream.close();
                                        Update.this.ok = true;
                                        return;
                                    }
                                    fileOutputStream.write(bArr, 0, read);
                                }
                            } catch (ClientProtocolException e) {
                                e.printStackTrace();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                            }
                        }
                    }.start();
                }
            } catch (APINotAvailableException e) {
                e.printStackTrace();
            }
        } else if (new File(Environment.getExternalStorageDirectory(), this.of).exists()) {
            this.ok = true;
        }
    }

    public void gJ() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory(), this.of)), "application/vnd.android.package-archive");
        c(intent);
    }

    public void gK() {
        this.oj = new a();
        this.oi = new IntentFilter();
        this.oi.addAction("android.intent.action.UPDATE_DEL_BROADCAST");
        registerReceiver(this.oj, this.oi);
    }

    public void start() {
        this.og = new ProgressDialog(this);
        this.og.setMessage("请稍后...");
        this.og.setProgressStyle(0);
        gI();
        while (!this.ok) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        gL();
    }
}
