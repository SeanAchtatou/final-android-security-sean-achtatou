package me.gall.verdandi.impl;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import com.a.a.i.k;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.umeng.common.b.e;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.microedition.media.PlayerListener;
import javax.microedition.media.control.MetaDataControl;
import javax.microedition.media.control.ToneControl;
import me.gall.skuld.SNSPlatformManager;
import me.gall.skuld.adapter.FeatureNotSupportException;
import me.gall.skuld.adapter.SNSPlatformAdapter;
import me.gall.verdandi.ISocial;
import me.gall.verdandi.NotSuchTargetException;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.core.m;
import org.meteoroid.core.n;
import org.meteoroid.plugin.vd.CommandButton;
import org.meteoroid.plugin.vd.DefaultVirtualDevice;
import org.meteoroid.plugin.vd.ScreenWidget;
import org.opensocial.models.skuld.Achievement;
import org.opensocial.models.skuld.Billing;
import org.opensocial.models.skuld.Score;

public final class Social implements DialogInterface.OnClickListener, ISocial, h.a {
    private static final String LOG = "Social";
    private SNSPlatformAdapter nW;
    private int nX = 1;
    private List<String> nY = new ArrayList();
    private Map<String, String> nZ = new HashMap();
    private AlarmManager oa = null;

    public static class AlarmReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            Intent intent2;
            PackageManager.NameNotFoundException e;
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            int intValue = ((Integer) intent.getExtras().get("iconID")).intValue();
            CharSequence charSequence = (CharSequence) intent.getExtras().get(MetaDataControl.TITLE_KEY);
            CharSequence charSequence2 = (CharSequence) intent.getExtras().get("content");
            String str = (String) intent.getExtras().get("url");
            Notification notification = new Notification();
            notification.icon = intValue;
            if (str != null) {
                intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
            } else {
                Intent intent3 = new Intent();
                try {
                    PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    Intent intent4 = new Intent("android.intent.action.MAIN", (Uri) null);
                    intent4.addCategory("android.intent.category.LAUNCHER");
                    intent4.setPackage(packageInfo.packageName);
                    ResolveInfo next = context.getPackageManager().queryIntentActivities(intent4, 0).iterator().next();
                    if (next != null) {
                        String str2 = next.activityInfo.packageName;
                        String str3 = next.activityInfo.name;
                        intent2 = new Intent("android.intent.action.MAIN");
                        try {
                            intent2.addCategory("android.intent.category.LAUNCHER");
                            intent2.setComponent(new ComponentName(str2, str3));
                        } catch (PackageManager.NameNotFoundException e2) {
                            e = e2;
                        }
                    } else {
                        intent2 = intent3;
                    }
                } catch (PackageManager.NameNotFoundException e3) {
                    PackageManager.NameNotFoundException nameNotFoundException = e3;
                    intent2 = intent3;
                    e = nameNotFoundException;
                    e.printStackTrace();
                    notification.setLatestEventInfo(context, charSequence, charSequence2, PendingIntent.getActivity(context, 0, intent2, k.NOVEMBER));
                    notificationManager.notify(1, notification);
                }
            }
            notification.setLatestEventInfo(context, charSequence, charSequence2, PendingIntent.getActivity(context, 0, intent2, k.NOVEMBER));
            notificationManager.notify(1, notification);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String[], android.content.DialogInterface$OnClickListener, boolean):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], java.lang.String[], me.gall.verdandi.impl.Social$1, int]
     candidates:
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, android.view.View, boolean, android.content.DialogInterface$OnCancelListener):void
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String[], android.content.DialogInterface$OnClickListener, boolean):void */
    private void a(String str, Uri uri) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("image/*");
        List<ResolveInfo> queryIntentActivities = l.getActivity().getPackageManager().queryIntentActivities(intent, 0);
        if (queryIntentActivities.isEmpty()) {
            l.i("手机内没有安装可用的分享组件", 0);
            throw new NotSuchTargetException("手机内没有安装可用的分享组件");
        } else if ((this.nX & 1) != 0) {
            if (uri != null) {
                intent.setType("image/*");
                intent.putExtra("android.intent.extra.STREAM", uri);
            }
            if (str != null) {
                intent.putExtra("android.intent.extra.TEXT", str);
                intent.putExtra("sms_body", str);
            }
            intent.setFlags(k.DECEMBER);
            l.getActivity().startActivity(Intent.createChooser(intent, "请选择分享方式"));
        } else {
            final ArrayList arrayList = new ArrayList();
            for (ResolveInfo resolveInfo : queryIntentActivities) {
                ActivityInfo activityInfo = resolveInfo.activityInfo;
                for (int i = 0; i < this.nY.size(); i++) {
                    if (activityInfo.packageName.toLowerCase().contains(this.nY.get(i))) {
                        Intent intent2 = new Intent("android.intent.action.SEND");
                        intent2.setType("image/*");
                        if (uri != null) {
                            intent2.putExtra("android.intent.extra.STREAM", uri);
                        }
                        if (str != null) {
                            intent2.putExtra("android.intent.extra.TEXT", str);
                            intent2.putExtra("sms_body", str);
                        }
                        intent2.setFlags(k.DECEMBER);
                        intent2.setPackage(activityInfo.packageName);
                        arrayList.add(intent2);
                        Log.d(LOG, String.valueOf(activityInfo.packageName) + " has added into share targets.");
                    }
                }
            }
            Log.d(LOG, "There are " + arrayList.size() + " targets in all.");
            if (arrayList.isEmpty()) {
                l.i("找不到指定的分享组件", 0);
                throw new NotSuchTargetException("找不到指定的分享组件");
            } else if (arrayList.size() == 1) {
                l.getActivity().startActivity((Intent) arrayList.get(0));
            } else {
                try {
                    String[] strArr = new String[arrayList.size()];
                    for (int i2 = 0; i2 < strArr.length; i2++) {
                        strArr[i2] = this.nZ.get(((Intent) arrayList.get(i2)).getPackage());
                    }
                    m.a("请选择分享方式", (String) null, strArr, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            l.getActivity().startActivity((Intent) arrayList.get(i));
                        }
                    }, false);
                } catch (Exception e) {
                    l.i("分享错误", 0);
                    throw new NotSuchTargetException("分享错误");
                }
            }
        }
    }

    private SNSPlatformAdapter fr() {
        if (this.nW == null) {
            SNSPlatformManager.a(l.getActivity(), l.fz(), new HashMap());
            this.nW = SNSPlatformManager.fr();
            h.a(this);
        }
        return this.nW;
    }

    private Bitmap gD() {
        Bitmap ic;
        ScreenWidget iT = ((DefaultVirtualDevice) n.pJ).iT();
        if (iT == null || (ic = iT.ic()) == null) {
            return null;
        }
        return ic;
    }

    private void gE() {
        this.nY.clear();
        this.nZ.clear();
        if ((this.nX & 2) != 0) {
            this.nY.add("com.sina.weibo");
            this.nZ.put("com.sina.weibo", "新浪微博");
        }
        if ((this.nX & 4) != 0) {
            this.nY.add("com.uc.browser");
            this.nY.add("com.UCMobile");
            this.nZ.put("com.uc.browser", "UC浏览器");
            this.nZ.put("com.UCMobile", "UC浏览器");
        }
        if ((this.nX & 16) != 0) {
            this.nY.add("com.tencent.microblog");
            this.nY.add("com.tencent.WBlog");
            this.nZ.put("com.tencent.microblog", "腾讯微博");
            this.nZ.put("com.tencent.WBlog", "腾讯微博");
        }
        if ((this.nX & 8) != 0) {
            this.nY.add("com.tencent.mm");
            this.nZ.put("com.tencent.mm", "微信");
        }
        if ((this.nX & 32) != 0) {
            this.nY.add("com.renren.mobile");
            this.nZ.put("com.renren.mobile", "人人网");
        }
        if ((this.nX & 64) != 0) {
            this.nY.add("com.kaixin001.activity");
            this.nZ.put("com.kaixin001.activity", "开心网");
        }
        if ((this.nX & 128) != 0) {
            this.nY.add("com.facebook.katana");
            this.nZ.put("com.facebook.katana", "Facebook");
        }
        if ((this.nX & 256) != 0) {
            this.nY.add("com.twitter.android");
            this.nZ.put("com.twitter.android", "Twitter");
        }
        if ((this.nX & 512) != 0) {
            this.nY.add("com.google.android.apps.plus");
            this.nZ.put("com.google.android.apps.plus", "Google+");
        }
    }

    private int gG() {
        int i = Calendar.getInstance().get(7) - 1;
        if (i == 1) {
            return 0;
        }
        return 1 - i;
    }

    public Vector Q(int i, int i2) {
        return null;
    }

    public void a(String str, String str2, int i, final ISocial.BillingResultListener billingResultListener) {
        Billing billing = new Billing();
        billing.setType(1);
        billing.setId(str2);
        billing.k((float) i);
        try {
            fr().a(billing, new SNSPlatformAdapter.AsyncResultCallback<Billing>() {
                public void aq(String str) {
                    if (billingResultListener != null) {
                        Log.d(Social.LOG, "BillingResultListener:" + str);
                        h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"BillingFail", PlayerListener.ERROR, str});
                        billingResultListener.billingFail();
                    }
                }

                /* renamed from: b */
                public void f(Billing billing) {
                    if (billingResultListener != null) {
                        h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"BillingSuccess", "id", billing.getId()});
                        billingResultListener.billingOK(billing.getId());
                    }
                }

                /* renamed from: c */
                public void e(Billing billing) {
                    Log.d(Social.LOG, "BillingResultListener:option");
                }
            });
        } catch (FeatureNotSupportException e) {
            e.printStackTrace();
            if (billingResultListener != null) {
                Log.d(LOG, "BillingResultListener:" + e);
                billingResultListener.billingFail();
            }
        }
    }

    public void a(String str, String str2, String str3, String str4, long j) {
        a(str, str2, str3, str4, j, 0);
    }

    public void a(String str, String str2, String str3, String str4, long j, int i) {
        long currentTimeMillis = System.currentTimeMillis();
        if (str4 != null) {
            try {
                currentTimeMillis = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str4).getTime();
            } catch (ParseException e) {
                try {
                    String[] split = str4.split("-");
                    int parseInt = Integer.parseInt(split[0]);
                    String str5 = split[1];
                    currentTimeMillis = i(parseInt, str5) <= System.currentTimeMillis() ? h(parseInt, str5) : i(parseInt, str5);
                } catch (Exception e2) {
                    Log.e("formateTimeStr", "Convert exception!");
                    throw new IllegalArgumentException();
                }
            }
        }
        Intent intent = new Intent(l.getActivity(), AlarmReceiver.class);
        intent.putExtra("iconID", l.hH());
        intent.putExtra(MetaDataControl.TITLE_KEY, str);
        intent.putExtra("content", str2);
        intent.putExtra("url", str3);
        PendingIntent broadcast = PendingIntent.getBroadcast(l.getActivity(), i, intent, k.NOVEMBER);
        this.oa = (AlarmManager) l.getActivity().getSystemService("alarm");
        Log.d(LOG, "starttime:" + currentTimeMillis + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + new SimpleDateFormat().format(new Date(currentTimeMillis)) + " timeInterval:" + j);
        if (j == -1) {
            this.oa.set(0, currentTimeMillis, broadcast);
        } else {
            this.oa.setRepeating(0, currentTimeMillis, 1000 * j, broadcast);
        }
    }

    public boolean a(Message message) {
        if (message.what == 40961) {
            SNSPlatformManager.onResume();
            return false;
        } else if (message.what == 40960) {
            SNSPlatformManager.onPause();
            return false;
        } else if (message.what != 40968) {
            return false;
        } else {
            SNSPlatformManager.onDestroy();
            return false;
        }
    }

    public void aG(String str) {
        h.d(CommandButton.MSG_CHECKIN, str);
    }

    public void aH(String str) {
        Log.d(LOG, "Upload achievementId:" + str);
        Achievement achievement = new Achievement();
        achievement.setId(str);
        try {
            fr().a(achievement);
        } catch (FeatureNotSupportException e) {
            e.printStackTrace();
        }
    }

    public void aI(String str) {
        a(str, null);
    }

    public void aJ(String str) {
        Uri uri = null;
        Bitmap gD = gD();
        if (gD != null) {
            uri = Uri.parse(MediaStore.Images.Media.insertImage(l.getActivity().getContentResolver(), gD, (String) null, (String) null));
        }
        a(str, uri);
    }

    public boolean bD(int i) {
        return false;
    }

    public Vector bE(int i) {
        return null;
    }

    public void bF(int i) {
        this.nX = i;
        gE();
    }

    public void bG(int i) {
        fr().br(i);
    }

    public void bH(int i) {
        this.oa = (AlarmManager) l.getActivity().getSystemService("alarm");
        this.oa.cancel(PendingIntent.getBroadcast(l.getActivity(), i, new Intent(l.getActivity(), AlarmReceiver.class), k.NOVEMBER));
    }

    public void bq(int i) {
        try {
            fr().bq(i);
        } catch (FeatureNotSupportException e) {
            e.printStackTrace();
        }
    }

    public void d(String str, String str2, int i) {
        a(str, str2, i, null);
    }

    public String encryptString(String str, String str2) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str2.toUpperCase());
            instance.reset();
            instance.update(str.getBytes(e.f));
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < digest.length; i++) {
                if (Integer.toHexString(digest[i] & ToneControl.SILENCE).length() == 1) {
                    stringBuffer.append("0").append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
                } else {
                    stringBuffer.append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
                }
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public String fw() {
        return fr().fw();
    }

    public String fx() {
        return fr().fx();
    }

    public void fy() {
        fr().fy();
    }

    public Vector gA() {
        return null;
    }

    public Vector gB() {
        return null;
    }

    public Vector gC() {
        return null;
    }

    public void gF() {
        fr().fy();
    }

    public Vector gv() {
        return null;
    }

    public Vector gw() {
        return null;
    }

    public Vector gx() {
        return null;
    }

    public Vector gy() {
        return null;
    }

    public Vector gz() {
        return null;
    }

    public long h(int i, String str) {
        int gG = gG();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.add(5, ((gG + 7) + i) - 1);
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(gregorianCalendar.getTime())) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + str).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void h(String str, int i) {
        Log.d(LOG, "Upload score:" + i + " in leadboard:" + str);
        Score score = new Score();
        score.cn(i);
        int i2 = 0;
        try {
            i2 = Integer.parseInt(str);
        } catch (Exception e) {
            Log.e(LOG, "uploadScore()  leadboardId  转int出错" + e.getMessage());
        }
        score.cp(i2);
        try {
            fr().a(score);
        } catch (FeatureNotSupportException e2) {
            e2.printStackTrace();
        }
    }

    public long i(int i, String str) {
        int gG = gG();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.add(5, (gG + i) - 1);
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(gregorianCalendar.getTime())) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + str).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void i(String str, String str2, String str3) {
        a(str, str2, str3, null, -1);
    }

    public void launch() {
        try {
            fr().bq(0);
        } catch (FeatureNotSupportException e) {
            e.printStackTrace();
        }
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == -1) {
            launch();
        }
    }

    public Vector u(int i, int i2, int i3) {
        return null;
    }

    public void y(String str, String str2) {
        boolean z;
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        if (str2 != null) {
            Iterator<ResolveInfo> it = l.getActivity().getPackageManager().queryIntentActivities(intent, 0).iterator();
            while (true) {
                if (it.hasNext()) {
                    ActivityInfo activityInfo = it.next().activityInfo;
                    if (activityInfo.packageName.toLowerCase().contains(str2)) {
                        intent.setPackage(activityInfo.packageName);
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!z) {
                throw new NotSuchTargetException("找不到目标客户端" + str2);
            }
        }
        l.getActivity().startActivity(intent);
    }
}
