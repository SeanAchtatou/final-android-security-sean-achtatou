package me.gall.verdandi.impl;

import com.a.a.m.m;
import com.googlecode.jsonrpc4j.Base64;
import java.util.HashMap;
import java.util.List;
import me.gall.sgp.android.core.SGPManager;
import me.gall.sgp.sdk.entity.Server;
import me.gall.sgp.sdk.entity.User;
import me.gall.sgp.sdk.entity.app.Announcement;
import me.gall.sgp.sdk.entity.app.Campaign;
import me.gall.sgp.sdk.entity.app.CampaignDetail;
import me.gall.sgp.sdk.entity.app.CheckinBox;
import me.gall.sgp.sdk.entity.app.GachaBox;
import me.gall.sgp.sdk.entity.app.LeaderBoardScore;
import me.gall.sgp.sdk.entity.app.Lottery;
import me.gall.sgp.sdk.entity.app.Mail;
import me.gall.sgp.sdk.entity.app.Save;
import me.gall.sgp.sdk.entity.app.SgpPlayer;
import me.gall.sgp.sdk.entity.app.Ticket;
import me.gall.sgp.sdk.service.RouterService;
import me.gall.verdandi.ISGP;
import org.meteoroid.core.l;

public class SGP implements ISGP {
    private SGPManager nV;

    public int a(int i, String str, int i2) {
        return this.nV.getCampaignService().updateProgress(i, str, i2);
    }

    public Object a(Class cls) {
        return this.nV.getService(cls);
    }

    public Server a(String str, long j, String str2) {
        return a(str, j, str2, (String) null);
    }

    public Server a(String str, long j, String str2, String str3) {
        HashMap hashMap = new HashMap();
        if (str != null) {
            hashMap.put(RouterService.USER_ID, str);
        }
        if (j != 0) {
            hashMap.put(RouterService.CREATE_TIME, String.valueOf(j));
        }
        if (str2 != null) {
            hashMap.put(m.PROP_VERSION, str2);
        }
        if (str3 != null) {
            hashMap.put("channelId", str3);
        }
        try {
            return this.nV.updateRouting(hashMap);
        } catch (Throwable th) {
            throw new Exception(th);
        }
    }

    public LeaderBoardScore a(String str, SgpPlayer sgpPlayer) {
        return this.nV.getLeaderBoardService().getLeaderBoardScoreByLeaderIdAndPlayerId(str, sgpPlayer.getId());
    }

    public Save a(SgpPlayer sgpPlayer, Save save) {
        if (sgpPlayer == null || sgpPlayer.getId() == null) {
            throw new Exception("Call create/get to get a player at first.");
        }
        save.setPlayerId(sgpPlayer.getId());
        return this.nV.getSgpPlayerService().uploadSave(save);
    }

    public SgpPlayer a(SgpPlayer sgpPlayer) {
        if (this.nV.getCurrentServer() == null || this.nV.getCurrentUser() == null) {
            throw new Exception("Call route to get server instance or register/login to get a user at first.");
        }
        sgpPlayer.setUserId(this.nV.getCurrentUser().getUserid());
        sgpPlayer.setServerId(this.nV.getCurrentServer().getId());
        return this.nV.getSgpPlayerService().create(sgpPlayer);
    }

    public void a(String str, String str2, Mail mail) {
        this.nV.getFriendshipService().acceptInvite(str, str2, mail);
    }

    public void a(String str, SgpPlayer sgpPlayer, int i) {
        this.nV.getLeaderBoardService().submitLeaderBoardScore(str, sgpPlayer.getId(), i);
    }

    public void a(String str, String[] strArr) {
        this.nV.getFriendshipService().acceptInvite(str, strArr);
    }

    public void a(String str, String[] strArr, Mail mail) {
        this.nV.getFriendshipService().acceptInvite(str, strArr, mail);
    }

    public void a(Mail mail) {
        if (mail.getFromId() == null || mail.getToId() == null) {
            throw new Exception("发送者和接受者不能为空");
        }
        this.nV.getMailService().send(mail);
    }

    public void a(Ticket ticket) {
        this.nV.getTicketService().sendTicket(ticket);
    }

    public void a(Mail[] mailArr) {
        int[] iArr = new int[mailArr.length];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = mailArr[i].getId().intValue();
        }
        this.nV.getMailService().read(iArr);
    }

    public Campaign[] a(long j, long j2) {
        return this.nV.getCampaignService().getByTimeZone(j, j2);
    }

    public Mail[] a(SgpPlayer sgpPlayer, int i, int i2, int i3) {
        return this.nV.getMailService().receive(i, i2, sgpPlayer.getId(), i3);
    }

    public Mail[] a(SgpPlayer sgpPlayer, long j) {
        return this.nV.getMailService().receiveUnread(j, sgpPlayer.getId());
    }

    public SgpPlayer[] a(long j, int i, int i2) {
        return this.nV.getSgpPlayerService().getByLastLoginTime(j, i, i2);
    }

    public SgpPlayer[] aA(String str) {
        return this.nV.getFriendshipService().getInvite(str);
    }

    public SgpPlayer[] aB(String str) {
        return this.nV.getFriendshipService().getNotConfirm(str);
    }

    public CheckinBox aC(String str) {
        return this.nV.getCheckinService().getCheckinboardByChekinboardId(str);
    }

    public String aD(String str) {
        return this.nV.getCheckinService().getRewardByChekinboardId(str);
    }

    public String[] aE(String str) {
        return (String[]) this.nV.getStructuredDataService().getSetValue(str).toArray();
    }

    public SgpPlayer aF(String str) {
        return this.nV.getSgpPlayerService().getSgpPlayerByCustomId(str);
    }

    public void addListValue(String str, String str2) {
        this.nV.getStructuredDataService().addListValue(str, str2);
    }

    public boolean addPlayerIntoBlacklist(String str, String str2) {
        return this.nV.getBlacklistService().addPlayerIntoBlacklist(str, str2);
    }

    public void addSetValue(String str, String str2) {
        this.nV.getStructuredDataService().addSetValue(str, str2);
    }

    public byte[] az(String str) {
        return Base64.decode(str);
    }

    public int b(int i, String str, int i2) {
        return this.nV.getBossService().attack(i, i2, str);
    }

    public Save b(SgpPlayer sgpPlayer) {
        if (sgpPlayer != null && sgpPlayer.getId() != null) {
            return this.nV.getSgpPlayerService().downloadSave(sgpPlayer.getId());
        }
        throw new Exception("Call create/get to get a player at first.");
    }

    public void b(String str, SgpPlayer sgpPlayer, int i) {
        this.nV.getLeaderBoardService().addUpLeaderBoardScore(str, sgpPlayer.getId(), i);
    }

    public void b(String str, String[] strArr) {
        this.nV.getFriendshipService().unfriend(str, strArr);
    }

    public void b(Mail mail) {
        this.nV.getMailService().read(mail.getId().intValue());
    }

    public void b(Mail[] mailArr) {
        int[] iArr = new int[mailArr.length];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = mailArr[i].getId().intValue();
        }
        this.nV.getMailService().delete(iArr);
    }

    public Ticket[] b(String str, int i, int i2, int i3) {
        return this.nV.getTicketService().getTicketsBySenderPlayerId(str, i, i2, i3);
    }

    public CampaignDetail bA(int i) {
        return this.nV.getCampaignService().getCampaignDetaiByCId(i);
    }

    public SgpPlayer bB(int i) {
        return this.nV.getBossService().getLastAttackPlayer(i);
    }

    public Lottery[] bC(int i) {
        return this.nV.getGachaBoxService().getLotteriesByGachaBoxId(Integer.valueOf(i));
    }

    public Announcement bz(int i) {
        return this.nV.getAnnouncementService().getAnnounceByType(i);
    }

    public int c(String str, String str2, int i) {
        return this.nV.getCheckinService().setCheckinTimes(str, str2, i);
    }

    public SgpPlayer c(SgpPlayer sgpPlayer) {
        return this.nV.getSgpPlayerService().update(sgpPlayer);
    }

    public void c(String str, boolean z, boolean z2) {
        this.nV = SGPManager.getInstance(l.getActivity(), str, z2);
    }

    public void c(Mail mail) {
        this.nV.getMailService().delete(mail.getId().intValue());
    }

    public LeaderBoardScore[] c(String str, int i, int i2) {
        List<LeaderBoardScore> topLeaderBoardScoreByLeaderId = this.nV.getLeaderBoardService().getTopLeaderBoardScoreByLeaderId(str, i, i2);
        if (topLeaderBoardScoreByLeaderId == null || topLeaderBoardScoreByLeaderId.isEmpty()) {
            return null;
        }
        LeaderBoardScore[] leaderBoardScoreArr = new LeaderBoardScore[topLeaderBoardScoreByLeaderId.size()];
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= leaderBoardScoreArr.length) {
                return leaderBoardScoreArr;
            }
            leaderBoardScoreArr[i4] = topLeaderBoardScoreByLeaderId.get(i4);
            i3 = i4 + 1;
        }
    }

    public SgpPlayer[] d(String str, int i, int i2) {
        return this.nV.getSgpPlayerService().getByName(str, i, i2);
    }

    public void destroy() {
        this.nV.destroy();
        this.nV = null;
    }

    public String draw(String str, int i, int i2) {
        try {
            return this.nV.getGachaBoxService().draw(str, i, i2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw new Exception(th);
        }
    }

    public String[] draw(String str, int i, int[] iArr) {
        try {
            return this.nV.getGachaBoxService().draw(str, i, iArr);
        } catch (Throwable th) {
            th.printStackTrace();
            throw new Exception(th);
        }
    }

    public SgpPlayer[] e(String str, int i, int i2) {
        return this.nV.getFriendshipService().getMyFriends(i, i2, str);
    }

    public int g(int i, String str) {
        return this.nV.getBossService().getCurrentHP(i, str);
    }

    public LeaderBoardScore[] g(String str, int i) {
        return c(str, 0, i);
    }

    public Campaign[] getAvailableCampaigns() {
        return this.nV.getCampaignService().getAvailableCampaigns();
    }

    public GachaBox[] getAvailableGachaBox() {
        return this.nV.getGachaBoxService().getAvailableGachaBox();
    }

    public int getCampaignProgress(int i, String str) {
        return this.nV.getCampaignService().getCampaignProgress(i, str);
    }

    public long getCurrentTimestamp() {
        return this.nV.getRouterService().getCurrentTimestamp();
    }

    public GachaBox getGachaBoxByName(String str) {
        return this.nV.getGachaBoxService().getGachaBoxByName(str);
    }

    public String getHashValue(String str, String str2) {
        return this.nV.getStructuredDataService().getHashValue(str, str2);
    }

    public long getLastCheckinTime(String str, String str2) {
        return this.nV.getCheckinService().getLastCheckinTime(str, str2);
    }

    public String[] getListByIndex(String str, long j, long j2) {
        return this.nV.getStructuredDataService().getListByIndex(str, j, j2);
    }

    public String[] getListValue(String str) {
        return this.nV.getStructuredDataService().getListValue(str);
    }

    public SgpPlayer getPlayer() {
        if (this.nV.getCurrentUser() != null) {
            return this.nV.getSgpPlayerService().getOneByUserId(this.nV.getCurrentUser().getUserid());
        }
        throw new Exception("Call register/login to get a user at first.");
    }

    public String getValue(String str) {
        return this.nV.getStructuredDataService().getValue(str);
    }

    public Server[] gq() {
        return this.nV.getRouterService().getServerList(this.nV.getAppId());
    }

    public User gr() {
        try {
            return this.nV.quickLogin();
        } catch (Throwable th) {
            throw new Exception(th);
        }
    }

    public User gs() {
        return this.nV.getCurrentUser();
    }

    public Server gt() {
        return this.nV.getCurrentServer();
    }

    public SgpPlayer[] gu() {
        if (this.nV.getCurrentUser() != null) {
            return this.nV.getSgpPlayerService().getByUserId(this.nV.getCurrentUser().getUserid());
        }
        throw new Exception("Call register/login to get a user at first.");
    }

    public User h(String str, String str2, String str3) {
        User user = new User();
        try {
            user.setUserid(str);
            user.setUserName(str2);
            user.setPassword(str3);
            User updateUser = this.nV.getUserService().updateUser(user);
            this.nV.setCurrentUser(updateUser);
            return updateUser;
        } catch (Throwable th) {
            throw new Exception(th);
        }
    }

    public void invite(String str, String str2) {
        this.nV.getFriendshipService().invite(str, str2);
    }

    public void invite(String str, String[] strArr) {
        this.nV.getFriendshipService().invite(str, strArr);
    }

    public User login(String str, String str2) {
        try {
            return this.nV.login(str, str2);
        } catch (Throwable th) {
            throw new Exception(th);
        }
    }

    public void q(String str, String str2) {
        this.nV.getFriendshipService().acceptInvite(str, str2);
    }

    public int r(String str, String str2) {
        return this.nV.getCheckinService().checkin(str, str2);
    }

    public User register(String str, String str2) {
        try {
            return this.nV.signup(str, str2);
        } catch (Throwable th) {
            throw new Exception(th);
        }
    }

    public int s(String str, String str2) {
        return this.nV.getCheckinService().accumlateCount(str, str2);
    }

    public void saveOrUpdateHashValue(String str, String str2, String str3) {
        this.nV.getStructuredDataService().saveOrUpdateHashValue(str, str2, str3);
    }

    public void saveOrUpdateValue(String str, String str2) {
        this.nV.getStructuredDataService().saveOrUpdateValue(str, str2);
    }

    public int t(String str, String str2) {
        return this.nV.getCheckinService().countinuousCount(str, str2);
    }

    public boolean u(String str, String str2) {
        return this.nV.getStructuredDataService().containtSet(str, str2);
    }

    public void v(String str, String str2) {
        this.nV.getStructuredDataService().removeFromSet(str, str2);
    }

    public boolean w(String str, String str2) {
        return this.nV.getBlacklistService().isInBlacklist(str, str2);
    }

    public String x(byte[] bArr) {
        return Base64.encodeBytes(bArr);
    }

    public void x(String str, String str2) {
        this.nV.getFriendshipService().unfriend(str, str2);
    }
}
