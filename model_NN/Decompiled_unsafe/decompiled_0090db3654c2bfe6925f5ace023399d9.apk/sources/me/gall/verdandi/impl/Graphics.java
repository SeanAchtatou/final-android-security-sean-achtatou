package me.gall.verdandi.impl;

import android.graphics.Bitmap;
import com.a.a.e.l;
import com.a.a.e.o;
import com.a.a.e.p;
import java.io.InputStream;
import me.gall.verdandi.IGraphics;
import org.meteoroid.plugin.device.MIDPDevice;

public class Graphics implements IGraphics {
    public p P(int i, int i2) {
        return s(i, i2, 0);
    }

    public p a(InputStream inputStream, int i) {
        return null;
    }

    public void a(o oVar, p pVar, int i, int i2, int i3) {
    }

    public void a(o oVar, p pVar, int i, int i2, int i3, int i4, boolean z) {
    }

    public void a(p pVar) {
    }

    public p ay(String str) {
        return null;
    }

    public void b(o oVar, p pVar, int i, int i2) {
    }

    public void b(o oVar, p pVar, int i, int i2, int i3) {
    }

    public void b(o oVar, p pVar, int i, int i2, int i3, int i4) {
    }

    public p s(int i, int i2, int i3) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        createBitmap.eraseColor(i3);
        p pVar = new p(createBitmap);
        pVar.iO = true;
        return pVar;
    }

    public l t(int i, int i2, int i3) {
        return MIDPDevice.d.v(i, i2, i3);
    }
}
