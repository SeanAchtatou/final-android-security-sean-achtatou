package me.gall.verdandi.impl;

import android.content.pm.PackageManager;
import me.gall.verdandi.IApplication;
import org.meteoroid.core.l;

public class Application implements IApplication {
    public boolean as(String str) {
        return l.as(str);
    }

    public boolean at(String str) {
        return l.at(str);
    }

    public int fO() {
        try {
            return l.getActivity().getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 1;
        }
    }

    public String fP() {
        try {
            return l.getActivity().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String fz() {
        return l.fz();
    }

    public String getPackageName() {
        return l.getActivity().getPackageName();
    }
}
