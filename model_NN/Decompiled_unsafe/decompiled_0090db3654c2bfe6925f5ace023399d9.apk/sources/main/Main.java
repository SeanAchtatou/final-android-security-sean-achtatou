package main;

import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.a;

public final class Main extends MIDlet {
    private static Main aL;
    private l Y;

    public Main() {
        aL = this;
    }

    public static Main fp() {
        return aL;
    }

    /* access modifiers changed from: protected */
    public final void dx() {
        if (this.Y == null) {
            this.Y = l.t();
            Runtime.getRuntime().totalMemory();
            this.Y.a(this);
            return;
        }
        this.Y.v();
    }

    /* access modifiers changed from: protected */
    public final void dy() {
        this.Y.q();
    }

    public final void fq() {
        try {
            q(true);
        } catch (a e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final void q(boolean z) {
        dz();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.Y = null;
    }
}
