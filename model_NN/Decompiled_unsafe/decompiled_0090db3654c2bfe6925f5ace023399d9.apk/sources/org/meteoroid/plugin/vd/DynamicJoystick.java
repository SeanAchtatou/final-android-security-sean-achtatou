package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.util.AttributeSet;

public class DynamicJoystick extends Joystick {
    private int sY;
    private int sZ;

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.sY = this.centerX;
        this.sZ = this.centerY;
    }

    public boolean l(int i, int i2, int i3, int i4) {
        if (this.state == 1 && i == 0 && Math.sqrt(Math.pow((double) (i2 - this.sY), 2.0d) + Math.pow((double) (i3 - this.sZ), 2.0d)) <= ((double) this.sN)) {
            this.id = i4;
            this.centerX = i2;
            this.centerY = i3;
            this.state = 0;
        }
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.sN) || sqrt < ((double) this.sO)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 1:
                if (this.id != i4) {
                    return true;
                }
                release();
                return true;
            case 2:
                if (this.id != i4) {
                    return true;
                }
                this.state = 0;
                this.tb = i2;
                this.tc = i3;
                VirtualKey a = a(a((float) i2, (float) i3), this.mode);
                if (this.sP != a) {
                    if (this.sP != null && this.sP.state == 0) {
                        this.sP.state = 1;
                        VirtualKey.b(this.sP);
                    }
                    this.sP = a;
                }
                this.sP.state = 0;
                VirtualKey.b(this.sP);
                return true;
            default:
                return true;
        }
    }

    public void onDraw(Canvas canvas) {
        if (this.state != 1) {
            super.onDraw(canvas);
        }
    }
}
