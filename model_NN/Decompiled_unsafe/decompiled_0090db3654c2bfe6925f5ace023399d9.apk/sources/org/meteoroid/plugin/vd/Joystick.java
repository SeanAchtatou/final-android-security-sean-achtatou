package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import com.a.a.r.c;
import com.a.a.s.e;

public class Joystick extends AbstractRoundWidget {
    public static final int EIGHT_DIR_CONTROL = 8;
    public static final int EIGHT_DIR_CONTROL_NUM = 10;
    public static final int FOUR_DIR_CONTROL = 4;
    public static final int FOUR_DIR_CONTROL_NUM = 6;
    private static final VirtualKey[] sQ = new VirtualKey[5];
    private static final VirtualKey[] ta = new VirtualKey[9];
    private boolean ju = true;
    int mode;
    int tb;
    int tc;
    Bitmap[] td;
    Bitmap[] te;

    /* access modifiers changed from: package-private */
    public VirtualKey a(float f, int i) {
        return i == 8 ? ta[(int) ((((double) f) + 22.5d) / 45.0d)] : sQ[(int) ((45.0f + f) / 90.0f)];
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.td = e.bt(attributeSet.getAttributeValue(str, "stick"));
        this.te = e.bt(attributeSet.getAttributeValue(str, "base"));
        this.mode = attributeSet.getAttributeIntValue(str, "mode", 4);
    }

    public void a(c cVar) {
        super.a(cVar);
        if (this.mode == 8) {
            ta[0] = new VirtualKey();
            ta[0].ty = "RIGHT";
            ta[1] = new VirtualKey();
            ta[1].ty = "NUM_3";
            ta[2] = new VirtualKey();
            ta[2].ty = "UP";
            ta[3] = new VirtualKey();
            ta[3].ty = "NUM_1";
            ta[4] = new VirtualKey();
            ta[4].ty = "LEFT";
            ta[5] = new VirtualKey();
            ta[5].ty = "NUM_7";
            ta[6] = new VirtualKey();
            ta[6].ty = "DOWN";
            ta[7] = new VirtualKey();
            ta[7].ty = "NUM_9";
            ta[8] = ta[0];
        } else if (this.mode == 10) {
            ta[0] = new VirtualKey();
            ta[0].ty = "NUM_6";
            ta[1] = new VirtualKey();
            ta[1].ty = "NUM_3";
            ta[2] = new VirtualKey();
            ta[2].ty = "NUM_2";
            ta[3] = new VirtualKey();
            ta[3].ty = "NUM_1";
            ta[4] = new VirtualKey();
            ta[4].ty = "NUM_4";
            ta[5] = new VirtualKey();
            ta[5].ty = "NUM_7";
            ta[6] = new VirtualKey();
            ta[6].ty = "NUM_8";
            ta[7] = new VirtualKey();
            ta[7].ty = "NUM_9";
            ta[8] = ta[0];
        } else if (this.mode == 6) {
            sQ[0] = new VirtualKey();
            sQ[0].ty = "NUM_6";
            sQ[1] = new VirtualKey();
            sQ[1].ty = "NUM_2";
            sQ[2] = new VirtualKey();
            sQ[2].ty = "NUM_4";
            sQ[3] = new VirtualKey();
            sQ[3].ty = "NUM_8";
            sQ[4] = sQ[0];
        } else {
            sQ[0] = new VirtualKey();
            sQ[0].ty = "RIGHT";
            sQ[1] = new VirtualKey();
            sQ[1].ty = "UP";
            sQ[2] = new VirtualKey();
            sQ[2].ty = "LEFT";
            sQ[3] = new VirtualKey();
            sQ[3].ty = "DOWN";
            sQ[4] = sQ[0];
        }
        reset();
    }

    public String getName() {
        return "Joystick";
    }

    public boolean ia() {
        return (this.te == null && this.td == null) ? false : true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean l(int i, int i2, int i3, int i4) {
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.sN) || sqrt < ((double) this.sO)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                this.id = i4;
                break;
            case 1:
                if (this.id != i4) {
                    return true;
                }
                release();
                return true;
            case 2:
                break;
            default:
                return true;
        }
        if (this.id != i4) {
            return true;
        }
        this.state = 0;
        this.tb = i2;
        this.tc = i3;
        VirtualKey a = a(a((float) i2, (float) i3), this.mode);
        if (this.sP != a) {
            if (this.sP != null && this.sP.state == 0) {
                this.sP.state = 1;
                VirtualKey.b(this.sP);
            }
            this.sP = a;
        }
        this.sP.state = 0;
        VirtualKey.b(this.sP);
        return true;
    }

    public void onDraw(Canvas canvas) {
        Paint paint = null;
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.sK = 0;
            this.ju = true;
        }
        if (this.ju) {
            if (this.sJ > 0 && this.state == 1) {
                this.sK++;
                this.hq.setAlpha(255 - ((this.sK * 255) / this.sJ));
                if (this.sK >= this.sJ) {
                    this.sK = 0;
                    this.ju = false;
                }
            }
            if (a(this.te)) {
                canvas.drawBitmap(this.te[this.state], (float) (this.centerX - (this.te[this.state].getWidth() / 2)), (float) (this.centerY - (this.te[this.state].getHeight() / 2)), (this.sJ == -1 || this.state != 1) ? null : this.hq);
            }
            if (a(this.td)) {
                Bitmap bitmap = this.td[this.state];
                float width = (float) (this.tb - (this.td[this.state].getWidth() / 2));
                float height = (float) (this.tc - (this.td[this.state].getHeight() / 2));
                if (this.sJ != -1 && this.state == 1) {
                    paint = this.hq;
                }
                canvas.drawBitmap(bitmap, width, height, paint);
            }
        }
    }

    public final void reset() {
        this.tb = this.centerX;
        this.tc = this.centerY;
        this.state = 1;
    }

    public void setVisible(boolean z) {
        this.ju = z;
    }
}
