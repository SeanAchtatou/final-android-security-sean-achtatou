package org.meteoroid.plugin.vd;

import org.meteoroid.core.l;

public class CallOptionMenu extends SimpleButton {
    private boolean lq;

    public String getName() {
        return "CallOptionMenu";
    }

    public void onClick() {
        if (this.lq) {
            l.getActivity().closeOptionsMenu();
        } else {
            l.getActivity().openOptionsMenu();
        }
        this.lq = !this.lq;
    }
}
