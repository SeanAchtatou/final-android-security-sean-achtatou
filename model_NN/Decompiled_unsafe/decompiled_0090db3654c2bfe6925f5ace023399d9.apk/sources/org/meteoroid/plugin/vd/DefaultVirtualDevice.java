package org.meteoroid.plugin.vd;

import android.util.Log;
import android.view.Display;
import com.a.a.r.c;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Properties;
import org.meteoroid.core.e;
import org.meteoroid.core.f;
import org.meteoroid.core.h;
import org.meteoroid.core.l;

public class DefaultVirtualDevice implements c {
    public static final int WIDGET_TYPE_ARCADE_JOYSTICK = 11;
    public static final int WIDGET_TYPE_BACKGROUND = 0;
    public static final int WIDGET_TYPE_CALL_OPTIONMENU = 12;
    public static final int WIDGET_TYPE_CHECKIN = 13;
    public static final int WIDGET_TYPE_DEVICE_SCREEN = 2;
    public static final int WIDGET_TYPE_DYNAMICJOYSTICK = 15;
    public static final int WIDGET_TYPE_EXIT_BUTTON = 6;
    public static final int WIDGET_TYPE_HIDE_VD = 10;
    public static final int WIDGET_TYPE_INTELEGENCE_BG = 7;
    public static final int WIDGET_TYPE_JOYSTICK = 3;
    public static final int WIDGET_TYPE_MUTE_SWITCHER = 4;
    public static final int WIDGET_TYPE_SENSOR_SWITCHER = 5;
    public static final int WIDGET_TYPE_SNSBUTTON = 14;
    public static final int WIDGET_TYPE_STEERINGWHEEL = 9;
    public static final int WIDGET_TYPE_URL_BUTTON = 8;
    public static final int WIDGET_TYPE_VIRTUAL_BUTTON = 1;
    public static final String[] sW = {"Background", "VirtualKey", "ScreenWidget", "Joystick", "MuteSwitcher", "SensorSwitcher", "CommandButton", "IntellegenceBackground", "URLButton", "SteeringWheel", "HideVirtualDeviceSwitcher", "ArcadeJoyStick", "CallOptionMenu", "CheckinButton", "SNSButton", "DynamicJoystick"};
    private final LinkedHashSet<c.a> sU = new LinkedHashSet<>();
    private ScreenWidget sV;
    private int sX;

    public static final String iU() {
        Display defaultDisplay = l.getActivity().getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        int min = Math.min(width, height);
        int max = Math.max(width, height);
        Log.d(c.LOG_TAG, "Screen full width is " + min + "px and height is " + max + "px.");
        if (min == 480 && max == 854) {
            return "fwvga";
        }
        if (min == 480 && max == 960) {
            return "uwvga";
        }
        if (min == 800 && max == 960) {
            return "dualwvga";
        }
        if (min == 480 && max > 640 && max <= 800) {
            return "wvga";
        }
        if (min == 360 && max == 640) {
            return "nhd";
        }
        if (min == 320 && max >= 460 && max <= 500) {
            return "hvga";
        }
        if (min == 240 && max == 320) {
            return "qvga";
        }
        if (min == 240 && max <= 400 && max > 320) {
            return "wqvga";
        }
        if (min == 480 && max == 640) {
            return "vga";
        }
        if (min == 600 && max == 800) {
            return "svga";
        }
        if (min <= 600 && min > 540 && max <= 1024 && max > 960) {
            return "wsvga";
        }
        if (min <= 768 && min > 700 && max == 1024) {
            return "xga";
        }
        if (min == 640 && max <= 960 && max >= 800) {
            return "retina";
        }
        if (min == 540 && max <= 960 && max >= 800) {
            return "qhd";
        }
        if (min <= 800 && min > 720 && max <= 1280 && max >= 1100) {
            return "wxga";
        }
        if (min <= 720 && min > 640 && max <= 1280 && max >= 1100) {
            return "720hd";
        }
        if (min <= 768 && min >= 640 && max == 1366) {
            return "hd";
        }
        if (min <= 1080 && min >= 900 && max >= 1760) {
            return "1080hd";
        }
        if (min == 320 && max == 320) {
            return "gear";
        }
        Log.w(c.LOG_TAG, "Unkown screen resolution:" + min + "x" + max);
        return null;
    }

    public void a(c.a aVar) {
        if (!this.sU.contains(aVar)) {
            aVar.a(this);
            if (aVar instanceof ScreenWidget) {
                ScreenWidget screenWidget = (ScreenWidget) aVar;
                screenWidget.iY();
                a(screenWidget);
            }
            this.sU.add(aVar);
            if (aVar.ia()) {
                e.a(aVar);
            }
            if (aVar.isTouchable()) {
                f.a(aVar);
            }
        }
    }

    public void a(Properties properties) {
        if (properties.containsKey("widget.orientation")) {
            String property = properties.getProperty("widget.orientation");
            this.sX = 2;
            if (property.equals("landscape")) {
                this.sX = 0;
                Log.d(c.LOG_TAG, "Device orientation is force to landscape.");
            } else if (property.equals("portrait")) {
                this.sX = 1;
                Log.d(c.LOG_TAG, "Device orientation is force to portrait.");
            } else if (property.equals("auto")) {
                this.sX = 4;
                Log.d(c.LOG_TAG, "Device orientation is auto change by sensor.");
            } else {
                Log.w(c.LOG_TAG, "Orientation not specificed. It will be decided by user. ");
            }
            l.bS(this.sX);
        }
        if (properties.containsKey("widget.num")) {
            int parseInt = Integer.parseInt(properties.getProperty("widget.num"));
            a aVar = new a(properties);
            int i = 1;
            while (i <= parseInt) {
                try {
                    if (properties.containsKey("widget." + i + ".type")) {
                        String property2 = properties.getProperty("widget." + i + ".type");
                        try {
                            int parseInt2 = Integer.parseInt(property2);
                            if (parseInt2 < 0 || parseInt2 >= sW.length) {
                                Log.w(c.LOG_TAG, "Unknown widget type:" + parseInt2);
                                i++;
                            } else {
                                property2 = sW[parseInt2];
                                c.a bl = bl(property2);
                                bl.a(aVar, "widget." + i + ".");
                                a(bl);
                                i++;
                            }
                        } catch (Exception e) {
                            Log.d(c.LOG_TAG, "Type is not a number. Use a string type as widget name.");
                        }
                    } else {
                        Log.d(c.LOG_TAG, "Widget " + i + " not exist! Checkout it is not missing.");
                        i++;
                    }
                } catch (Exception e2) {
                    Log.w(c.LOG_TAG, "Init widget[" + i + "] error." + e2);
                    e2.printStackTrace();
                }
            }
        }
        properties.clear();
        System.gc();
    }

    public void a(ScreenWidget screenWidget) {
        if (this.sV != null) {
            b(this.sV);
        }
        this.sV = screenWidget;
    }

    public void b(c.a aVar) {
        this.sU.remove(aVar);
        e.b(aVar);
        f.b(aVar);
    }

    public void bc(String str) {
        onDestroy();
        com.a.a.s.e.jd();
        if (str == null) {
            onCreate();
            return;
        }
        Properties properties = new Properties();
        try {
            properties.load(l.aV(str + File.separator + "res" + File.separator + "raw" + File.separator + "vd_" + iU() + ".properties"));
        } catch (IOException e) {
            Log.e(c.LOG_TAG, "Error in reloading vd:" + str);
        }
        com.a.a.s.e.bn(str + File.separator + "res" + File.separator + "drawable-nodpi");
        a(properties);
        com.a.a.s.e.bn(null);
    }

    public c.a bl(String str) {
        if (str.equals(sW[2])) {
            Log.d(c.LOG_TAG, "Construct the device screen widget.");
            return (ScreenWidget) org.meteoroid.core.c.ou;
        }
        Log.d(c.LOG_TAG, "Construct a [" + str + "] widget.");
        return (c.a) Class.forName("org.meteoroid.plugin.vd." + str).newInstance();
    }

    public int getOrientation() {
        return this.sX;
    }

    public Properties iR() {
        Properties properties = new Properties();
        properties.load(l.getActivity().getResources().openRawResource(com.a.a.s.e.bq("vd_" + iU())));
        return properties;
    }

    public LinkedHashSet<c.a> iS() {
        return this.sU;
    }

    public ScreenWidget iT() {
        return this.sV;
    }

    public boolean isVisible() {
        return true;
    }

    public void onCreate() {
        h.j(VirtualKey.MSG_VIRTUAL_KEY_EVENT, "MSG_VIRTUAL_BUTTON_EVENT");
        try {
            a(iR());
        } catch (IOException e) {
            Log.e(c.LOG_TAG, "Oooooooops, Fail to load virtual device by resolution. Maybe the resolution is odd or missing." + e);
        }
    }

    public void onDestroy() {
        Iterator<c.a> it = this.sU.iterator();
        while (it.hasNext()) {
            c.a next = it.next();
            e.b(next);
            f.b(next);
        }
        this.sU.clear();
    }
}
