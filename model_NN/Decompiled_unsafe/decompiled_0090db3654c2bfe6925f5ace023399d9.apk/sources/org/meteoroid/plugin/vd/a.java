package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import java.util.Properties;
import me.gall.sgp.sdk.service.BossService;

public class a implements AttributeSet {
    private Properties tA;

    public a(Properties properties) {
        this.tA = properties;
    }

    public boolean getAttributeBooleanValue(int i, boolean z) {
        return false;
    }

    public boolean getAttributeBooleanValue(String str, String str2, boolean z) {
        return this.tA.containsKey(new StringBuilder().append(str).append(str2).toString()) ? Boolean.parseBoolean(this.tA.getProperty(str + str2)) : z;
    }

    public int getAttributeCount() {
        return this.tA.size();
    }

    public float getAttributeFloatValue(int i, float f) {
        return 0.0f;
    }

    public float getAttributeFloatValue(String str, String str2, float f) {
        return this.tA.containsKey(new StringBuilder().append(str).append(str2).toString()) ? Float.parseFloat(this.tA.getProperty(str + str2)) : f;
    }

    public int getAttributeIntValue(int i, int i2) {
        return 0;
    }

    public int getAttributeIntValue(String str, String str2, int i) {
        return this.tA.containsKey(new StringBuilder().append(str).append(str2).toString()) ? Integer.parseInt(this.tA.getProperty(str + str2)) : i;
    }

    public int getAttributeListValue(int i, String[] strArr, int i2) {
        return 0;
    }

    public int getAttributeListValue(String str, String str2, String[] strArr, int i) {
        return this.tA.containsKey(new StringBuilder().append(str).append(str2).toString()) ? this.tA.getProperty(str + str2).split(BossService.ID_SEPARATOR).length : i;
    }

    public String getAttributeName(int i) {
        return null;
    }

    public int getAttributeNameResource(int i) {
        return 0;
    }

    public int getAttributeResourceValue(int i, int i2) {
        return 0;
    }

    public int getAttributeResourceValue(String str, String str2, int i) {
        return 0;
    }

    public int getAttributeUnsignedIntValue(int i, int i2) {
        return 0;
    }

    public int getAttributeUnsignedIntValue(String str, String str2, int i) {
        return 0;
    }

    public String getAttributeValue(int i) {
        return null;
    }

    public String getAttributeValue(String str, String str2) {
        return this.tA.getProperty(str + str2);
    }

    public String getClassAttribute() {
        return null;
    }

    public String getIdAttribute() {
        return null;
    }

    public int getIdAttributeResourceValue(int i) {
        return 0;
    }

    public String getPositionDescription() {
        return null;
    }

    public int getStyleAttribute() {
        return 0;
    }
}
