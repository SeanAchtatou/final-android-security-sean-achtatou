package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import com.a.a.r.c;

public abstract class ScreenWidget implements c.a {
    private final Rect tf = new Rect();
    public final Paint tg = new Paint();
    public Rect th;
    private float ti;
    private float tj;
    private boolean tk = false;
    private boolean tl;

    public void C(boolean z) {
        this.tg.setFilterBitmap(z);
        this.tk = true;
    }

    public abstract void a(int i, float f, float f2, int i2);

    public final void a(Rect rect) {
        this.tf.left = rect.left;
        this.tf.top = rect.top;
        this.tf.right = rect.right;
        this.tf.bottom = rect.bottom;
    }

    public Rect iV() {
        return this.tf;
    }

    public final float iW() {
        return this.ti;
    }

    public final float iX() {
        return this.tj;
    }

    public final void iY() {
        if (ic() != null) {
            this.ti = ((float) this.tf.width()) / ((float) ic().getWidth());
            this.tj = ((float) this.tf.height()) / ((float) ic().getHeight());
            if (!this.tk) {
                if (this.ti == 1.0f && this.tj == 1.0f) {
                    this.tg.setFilterBitmap(false);
                } else {
                    this.tg.setFilterBitmap(true);
                }
            }
            Log.d(c.LOG_TAG, "Scale Screen to " + this.tf.width() + "x" + this.tf.height());
        }
    }

    public boolean ia() {
        return true;
    }

    public abstract Bitmap ic();

    public boolean isTouchable() {
        return this.tl;
    }

    public boolean k(int i, int i2, int i3, int i4) {
        if (!iV().contains(i2, i3) || !this.tl) {
            return false;
        }
        a(i, ((float) (i2 - iV().left)) / this.ti, ((float) (i3 - iV().top)) / this.tj, i4);
        return false;
    }

    public void setTouchable(boolean z) {
        this.tl = z;
    }
}
