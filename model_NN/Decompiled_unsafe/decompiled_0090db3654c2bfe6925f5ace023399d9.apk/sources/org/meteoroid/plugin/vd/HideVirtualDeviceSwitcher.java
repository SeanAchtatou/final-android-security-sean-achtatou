package org.meteoroid.plugin.vd;

import com.a.a.r.c;
import java.util.Iterator;
import org.meteoroid.core.n;

public class HideVirtualDeviceSwitcher extends BooleanSwitcher {
    public String getName() {
        return "HideVirtualDeviceSwitcher";
    }

    public void iP() {
        DefaultVirtualDevice defaultVirtualDevice = (DefaultVirtualDevice) n.pJ;
        Iterator<c.a> it = defaultVirtualDevice.iS().iterator();
        while (it.hasNext()) {
            c.a next = it.next();
            if (next != defaultVirtualDevice.iT()) {
                next.setVisible(false);
            }
        }
    }

    public void iQ() {
        DefaultVirtualDevice defaultVirtualDevice = (DefaultVirtualDevice) n.pJ;
        Iterator<c.a> it = defaultVirtualDevice.iS().iterator();
        while (it.hasNext()) {
            c.a next = it.next();
            if (next != defaultVirtualDevice.iT()) {
                next.setVisible(true);
            }
        }
    }

    public void setVisible(boolean z) {
    }
}
