package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import me.gall.sgp.sdk.entity.app.StructuredData;
import me.gall.sgp.sdk.service.BossService;
import org.meteoroid.core.h;
import org.meteoroid.core.l;

public final class URLButton extends SimpleButton {
    private int index;
    public String[] tx;

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.tx = attributeSet.getAttributeValue(str, StructuredData.TYPE_OF_VALUE).split(BossService.ID_SEPARATOR);
    }

    public String getName() {
        return "URLButton";
    }

    public void onClick() {
        if (this.tx != null && this.tx[this.index] != null) {
            h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"URLClick", l.fz() + "=" + this.tx[this.index]});
            l.ba(this.tx[this.index]);
        }
    }
}
