package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.a.a.r.c;
import com.a.a.s.e;

public class Background implements c.a {
    int color = -16777216;
    private boolean ju = true;
    private c pJ;
    private Paint qH;
    Rect rect;
    Bitmap sR;

    public void a(AttributeSet attributeSet, String str) {
        this.rect = e.br(attributeSet.getAttributeValue(str, "rect"));
        String attributeValue = attributeSet.getAttributeValue(str, "bitmap");
        if (attributeValue != null) {
            this.sR = e.bs(attributeValue);
        } else {
            this.color = Integer.valueOf(attributeSet.getAttributeValue(str, "color"), 16).intValue();
        }
    }

    public void a(c cVar) {
        this.pJ = cVar;
        if (this.sR == null) {
            this.qH = new Paint();
            this.qH.setAntiAlias(true);
            this.qH.setStyle(Paint.Style.FILL);
        }
    }

    public boolean gP() {
        return true;
    }

    public String getName() {
        return "Background";
    }

    public c iM() {
        return this.pJ;
    }

    public boolean ia() {
        return true;
    }

    public boolean isTouchable() {
        return false;
    }

    public boolean k(int i, int i2, int i3, int i4) {
        return false;
    }

    public void onDraw(Canvas canvas) {
        if (this.ju) {
            if (this.sR == null) {
                this.qH.setColor(this.color);
                canvas.drawRect(this.rect, this.qH);
                return;
            }
            canvas.drawBitmap(this.sR, (Rect) null, this.rect, (Paint) null);
        }
    }

    public void setVisible(boolean z) {
        this.ju = z;
    }
}
