package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import com.a.a.r.c;
import com.a.a.s.e;

public class SteeringWheel extends AbstractRoundWidget {
    private static final int ANTICLOCKWISE = 2;
    private static final int CLOCKWISE = 1;
    private static final int LEFT = 0;
    private static final int RIGHT = 1;
    private static final int UNKNOWN = 0;
    private static final VirtualKey[] ts = new VirtualKey[2];
    private final Matrix iP = new Matrix();
    private int orientation = 0;
    private float tt = -1.0f;
    private float tu = 0.0f;
    public int tv = 135;
    public int tw = 5;

    private final float b(float f, float f2) {
        float f3 = f2 - f;
        if (this.orientation == 0) {
            if (Math.abs(f3) > 180.0f) {
                if (f3 > 0.0f) {
                    this.orientation = 1;
                } else if (f3 < 0.0f) {
                    this.orientation = 2;
                }
            } else if (f2 > f) {
                this.orientation = 2;
            } else if (f2 < f) {
                this.orientation = 1;
            }
        }
        return (this.orientation != 1 || f3 <= 0.0f) ? (this.orientation != 2 || f3 >= 0.0f) ? f3 : f3 + 360.0f : f3 - 360.0f;
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.tv = attributeSet.getAttributeIntValue(str, "angleMax", 135);
        this.tw = attributeSet.getAttributeIntValue(str, "angleMin", 5);
        this.sH = e.bt(attributeSet.getAttributeValue(str, "bitmap"));
    }

    public void a(c cVar) {
        super.a(cVar);
        ts[0] = new VirtualKey();
        ts[0].ty = "LEFT";
        ts[1] = new VirtualKey();
        ts[1].ty = "RIGHT";
        reset();
    }

    public boolean gP() {
        return true;
    }

    public String getName() {
        return "SteeringWheel";
    }

    public final boolean l(int i, int i2, int i3, int i4) {
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        VirtualKey virtualKey = null;
        Thread.yield();
        if (sqrt > ((double) this.sN) || sqrt < ((double) this.sO)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                if (this.tt == -1.0f) {
                    this.tt = a((float) i2, (float) i3);
                }
                this.id = i4;
                break;
            case 1:
                if (this.id == i4) {
                    release();
                    break;
                }
                break;
            case 2:
                if (this.id == i4) {
                    this.state = 0;
                    this.tu = b(this.tt, a((float) i2, (float) i3));
                    if (this.tu > ((float) this.tv)) {
                        this.tu = (float) this.tv;
                    } else if (this.tu < ((float) (this.tv * -1))) {
                        this.tu = (float) (this.tv * -1);
                    }
                    if (this.tu >= ((float) this.tw) && this.tu <= ((float) this.tv)) {
                        virtualKey = ts[0];
                    } else if (this.tu > ((float) (this.tw * -1)) || this.tu < ((float) (this.tv * -1))) {
                        if (this.tu < ((float) this.tw) && this.tu > ((float) (this.tw * -1))) {
                            this.orientation = 0;
                            break;
                        }
                    } else {
                        virtualKey = ts[1];
                    }
                    if (this.sP != virtualKey) {
                        if (this.sP != null && this.sP.state == 0) {
                            this.sP.state = 1;
                            VirtualKey.b(this.sP);
                        }
                        this.sP = virtualKey;
                    }
                    if (this.sP != null) {
                        this.sP.state = 0;
                        VirtualKey.b(this.sP);
                        break;
                    }
                }
                break;
        }
        return true;
    }

    public void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.sK = 0;
            this.ju = true;
        }
        if (this.ju) {
            if (this.sJ > 0 && this.state == 1) {
                this.sK++;
                this.hq.setAlpha(255 - ((this.sK * 255) / this.sJ));
                if (this.sK >= this.sJ) {
                    this.sK = 0;
                    this.ju = false;
                }
            }
            if (this.state == 1) {
                if (this.tu > 0.0f) {
                    this.tu -= 15.0f;
                    this.tu = this.tu < 0.0f ? 0.0f : this.tu;
                } else if (this.tu < 0.0f) {
                    this.tu += 15.0f;
                    this.tu = this.tu > 0.0f ? 0.0f : this.tu;
                }
            }
            if (this.sH != null) {
                canvas.save();
                if (this.tu != 0.0f) {
                    this.iP.setRotate(this.tu * -1.0f, (float) (this.sH[this.state].getWidth() >> 1), (float) (this.sH[this.state].getHeight() >> 1));
                }
                canvas.translate((float) (this.centerX - (this.sH[this.state].getWidth() >> 1)), (float) (this.centerY - (this.sH[this.state].getHeight() >> 1)));
                canvas.drawBitmap(this.sH[this.state], this.iP, null);
                canvas.restore();
                this.iP.reset();
            }
        }
    }

    public final void reset() {
        this.state = 1;
        this.tt = -1.0f;
        this.orientation = 0;
    }
}
