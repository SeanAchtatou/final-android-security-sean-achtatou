package org.meteoroid.plugin.vd;

import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import me.gall.sgp.sdk.entity.app.StructuredData;
import org.meteoroid.core.h;
import org.meteoroid.core.i;
import org.meteoroid.core.l;
import org.meteoroid.plugin.device.MIDPDevice;
import org.meteoroid.plugin.feature.AbstractAdvertisement;
import org.meteoroid.plugin.feature.AbstractDownloadAndInstall;

public final class CommandButton extends SimpleButton {
    public static final int MSG_APPLOTTERY = 74710;
    public static final int MSG_CHECKIN = -2004318072;
    public static final int MSG_DISABLE_AD = 9520139;
    public static final int MSG_REQUEST_RELOAD_SKIN = 952848;
    public static final int MSG_SOCIAL_PLATFORM_LAUNCH = 1013249;
    public static final int MSG_TIME_UP = 9520138;
    private String sS;

    private String bk(String str) {
        int indexOf;
        int indexOf2 = str.indexOf(40);
        if (indexOf2 == -1 || (indexOf = str.indexOf(41, indexOf2)) == -1) {
            return null;
        }
        return str.substring(indexOf2 + 1, indexOf);
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.sS = attributeSet.getAttributeValue(str, StructuredData.TYPE_OF_VALUE).trim();
        h.j(MSG_APPLOTTERY, "APPLOTTERY");
        h.j(MSG_CHECKIN, "CHECKIN");
        h.j(MSG_SOCIAL_PLATFORM_LAUNCH, "MSG_SOCIAL_PLATFORM_LAUNCH");
        h.j(MSG_REQUEST_RELOAD_SKIN, "MSG_REQUEST_RELOAD_SKIN");
        h.j(MSG_TIME_UP, "MSG_TIME_UP");
        h.j(MSG_DISABLE_AD, "MSG_DISABLE_AD");
    }

    public String getName() {
        return "CommandButton";
    }

    public void onClick() {
        if (this.sS.startsWith("CMD_EXIT")) {
            l.hk();
        } else if (this.sS.equalsIgnoreCase("CMD_ABOUT")) {
            h.bP(i.MSG_OPTIONMENU_ABOUT);
        } else if (this.sS.equalsIgnoreCase("CMD_MENU")) {
            l.getHandler().post(new Runnable() {
                public void run() {
                    l.getActivity().openOptionsMenu();
                }
            });
        } else if (this.sS.startsWith("CMD_CHECKIN")) {
            h.bP(MSG_CHECKIN);
        } else if (this.sS.startsWith("CMD_APPLOTTERY")) {
            h.bP(MSG_APPLOTTERY);
        } else if (this.sS.startsWith("CMD_SNS")) {
            h.bP(MSG_SOCIAL_PLATFORM_LAUNCH);
        } else if (this.sS.startsWith("CMD_RELOADSKIN")) {
            h.bP(MSG_REQUEST_RELOAD_SKIN);
        } else if (this.sS.startsWith("CMD_TIMEUP")) {
            h.bP(MSG_TIME_UP);
        } else if (this.sS.startsWith("CMD_INSTALLAPP")) {
            AnonymousClass2 r1 = new AbstractDownloadAndInstall() {
                public void bZ(int i) {
                }

                public String bf() {
                    return getName();
                }

                public String getName() {
                    return "AbstractDownloadAndInstall";
                }

                public void iz() {
                }
            };
            r1.be(bk(this.sS));
            r1.rS = true;
            if (r1.iq().isEmpty()) {
                l.i("您已经安装了该应用", 0);
            } else {
                r1.a(r1.iq().get(0));
            }
        } else if (!this.sS.startsWith("CMD_DISABLE_AD")) {
            Log.w(getName(), "NULL command:" + this.sS);
        } else if (AbstractAdvertisement.A(l.getActivity())) {
            Log.d(getName(), "Already disabled.");
            l.i("广告功能已经关闭", 0);
        } else {
            h.a(new h.a() {
                public boolean a(Message message) {
                    if (message.what != 61698) {
                        return false;
                    }
                    h.bP(CommandButton.MSG_DISABLE_AD);
                    l.i("广告功能关闭成功", 0);
                    return true;
                }
            });
            h.bP(MIDPDevice.j.MSG_GCF_SMS_CONNECTION_SEND);
        }
    }
}
