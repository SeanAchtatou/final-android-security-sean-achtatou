package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.a.a.r.c;
import com.a.a.s.e;
import me.gall.sgp.sdk.service.BossService;

public abstract class AbstractButton implements c.a {
    public static final int PRESSED = 0;
    public static final int RELEASED = 1;
    public static final int UNAVAILABLE_POINTER_ID = -1;
    private int count;
    int delay;
    Paint hq = new Paint();
    int id = -1;
    boolean ju = true;
    private c pJ;
    int rC;
    Rect sF;
    Rect sG;
    Bitmap[] sH;
    boolean sI;
    int sJ;
    int sK;
    boolean sL = true;
    private int sM;
    int state = 1;

    public void a(AttributeSet attributeSet, String str) {
        this.sH = e.bt(attributeSet.getAttributeValue(str, "bitmap"));
        String attributeValue = attributeSet.getAttributeValue(str, "touch");
        if (attributeValue != null) {
            this.sF = e.br(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            this.sG = e.br(attributeValue2);
        }
        String attributeValue3 = attributeSet.getAttributeValue(str, "fade");
        if (attributeValue3 != null) {
            String[] split = attributeValue3.split(BossService.ID_SEPARATOR);
            if (split.length >= 1) {
                this.sJ = Integer.parseInt(split[0]);
            } else {
                this.sJ = -1;
            }
            if (split.length >= 2) {
                this.delay = Integer.parseInt(split[1]);
            }
        }
        if (this.sH != null && this.sH.length > 2) {
            this.sI = true;
            this.rC = attributeSet.getAttributeIntValue(str, "interval", 40);
        }
    }

    public void a(c cVar) {
        this.pJ = cVar;
        if (this.sF == null) {
            this.sF = this.sG;
        }
    }

    public boolean a(Bitmap[] bitmapArr) {
        return bitmapArr != null && this.state >= 0 && this.state <= bitmapArr.length + -1 && bitmapArr[this.state] != null;
    }

    public boolean gP() {
        return this.sL;
    }

    public int iL() {
        return this.state;
    }

    public c iM() {
        return this.pJ;
    }

    public boolean ia() {
        return this.sH != null;
    }

    public boolean isTouchable() {
        return true;
    }

    public boolean k(int i, int i2, int i3, int i4) {
        return l(i, i2, i3, i4);
    }

    public abstract boolean l(int i, int i2, int i3, int i4);

    public void onDraw(Canvas canvas) {
        if (this.sI) {
            this.count++;
            if (this.count >= this.rC) {
                this.count = 0;
                this.sM++;
                if (this.sH != null && this.sM >= this.sH.length) {
                    this.sM = 0;
                }
            }
        } else if (this.delay > 0) {
            this.delay--;
            return;
        } else {
            if (this.state == 0) {
                this.sK = 0;
                this.ju = true;
            }
            if (this.ju) {
                if (this.sJ > 0 && this.state == 1) {
                    this.sK++;
                    this.hq.setAlpha(255 - ((this.sK * 255) / this.sJ));
                    if (this.sK >= this.sJ) {
                        this.sK = 0;
                        this.ju = false;
                    }
                }
                this.sM = this.state;
            } else {
                return;
            }
        }
        if (a(this.sH)) {
            canvas.drawBitmap(this.sH[this.sM], (Rect) null, this.sG, (this.sJ == -1 || this.state != 1) ? null : this.hq);
        }
    }

    public void setVisible(boolean z) {
        this.ju = z;
    }
}
