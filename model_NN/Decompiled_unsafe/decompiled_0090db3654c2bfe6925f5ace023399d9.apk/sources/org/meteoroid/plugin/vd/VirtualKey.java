package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import org.meteoroid.core.h;

public class VirtualKey extends AbstractButton {
    public static final int MSG_VIRTUAL_KEY_EVENT = 7833601;
    public String ty;
    private boolean tz = true;

    public static final synchronized void b(VirtualKey virtualKey) {
        synchronized (VirtualKey.class) {
            h.b(h.c(MSG_VIRTUAL_KEY_EVENT, virtualKey));
        }
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.ty = attributeSet.getAttributeValue(str, "keyname");
        this.tz = attributeSet.getAttributeBooleanValue(str, "clickThrough", true);
    }

    public String getName() {
        return ja();
    }

    public String ja() {
        return this.ty;
    }

    public boolean l(int i, int i2, int i3, int i4) {
        if (this.sF.contains(i2, i3)) {
            switch (i) {
                case 0:
                case 2:
                    this.id = i4;
                    this.state = 0;
                    b(this);
                    break;
                case 1:
                    this.id = -1;
                    this.state = 1;
                    b(this);
                    break;
            }
            if (!this.tz) {
                return true;
            }
        } else if (this.state == 0 && this.id == i4) {
            this.id = -1;
            this.state = 1;
            b(this);
        }
        return false;
    }
}
