package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import com.a.a.r.c;
import com.a.a.s.e;

public class ArcadeJoyStick extends AbstractRoundWidget {
    public static final int CENTER = 0;
    public static final int DOWN = 2;
    public static final int EIGHT_DIR_CONTROL = 8;
    public static final int FOUR_DIR_CONTROL = 4;
    public static final int LEFT = 3;
    public static final int RIGHT = 4;
    public static final int UP = 1;
    private static final VirtualKey[] sQ = new VirtualKey[5];
    private boolean ju = true;

    private final VirtualKey i(float f) {
        return sQ[(int) ((45.0f + f) / 90.0f)];
    }

    private final int j(float f) {
        if (f >= 45.0f && f < 135.0f) {
            return 1;
        }
        if (f >= 135.0f && f < 225.0f) {
            return 3;
        }
        if (f < 225.0f || f >= 315.0f) {
            return (f >= 315.0f || f < 45.0f) ? 4 : 0;
        }
        return 2;
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.sH = e.bt(attributeSet.getAttributeValue(str, "bitmap"));
    }

    public void a(c cVar) {
        super.a(cVar);
        sQ[0] = new VirtualKey();
        sQ[0].ty = "RIGHT";
        sQ[1] = new VirtualKey();
        sQ[1].ty = "UP";
        sQ[2] = new VirtualKey();
        sQ[2].ty = "LEFT";
        sQ[3] = new VirtualKey();
        sQ[3].ty = "DOWN";
        sQ[4] = sQ[0];
        reset();
    }

    public String getName() {
        return "ArcadeJoystick";
    }

    public boolean ia() {
        return this.sH != null;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean l(int i, int i2, int i3, int i4) {
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.sN) || sqrt < ((double) this.sO)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                this.id = i4;
                break;
            case 1:
                if (this.id != i4) {
                    return true;
                }
                release();
                return true;
            case 2:
                break;
            default:
                return true;
        }
        if (this.id != i4) {
            return true;
        }
        float a = a((float) i2, (float) i3);
        this.state = j(a);
        VirtualKey i5 = i(a);
        if (this.sP != i5) {
            if (this.sP != null && this.sP.state == 0) {
                this.sP.state = 1;
                VirtualKey.b(this.sP);
            }
            this.sP = i5;
        }
        this.sP.state = 0;
        VirtualKey.b(this.sP);
        return true;
    }

    public void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.sK = 0;
            this.ju = true;
        }
        if (this.ju) {
            if (this.sJ > 0 && this.state == 1) {
                this.sK++;
                this.hq.setAlpha(255 - ((this.sK * 255) / this.sJ));
                if (this.sK >= this.sJ) {
                    this.sK = 0;
                    this.ju = false;
                }
            }
            if (a(this.sH)) {
                canvas.drawBitmap(this.sH[this.state], (float) (this.centerX - (this.sH[this.state].getWidth() / 2)), (float) (this.centerY - (this.sH[this.state].getHeight() / 2)), (Paint) null);
            }
        }
    }

    public final void reset() {
        this.state = 0;
    }

    public void setVisible(boolean z) {
        this.ju = z;
    }
}
