package org.meteoroid.plugin.vd;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.AttributeSet;
import android.util.Log;
import com.a.a.r.c;
import com.a.a.s.e;
import me.gall.sgp.sdk.entity.app.StructuredData;
import me.gall.sgp.sdk.service.BossService;
import org.meteoroid.core.f;
import org.meteoroid.core.n;

public final class SensorSwitcher extends BooleanSwitcher implements SensorEventListener {
    private static final int DOWN = 1;
    private static final int LEFT = 2;
    private static final int RIGHT = 3;
    private static final int UP = 0;
    private static final VirtualKey[] sQ = new VirtualKey[4];
    private int sX;
    private int[] tm;
    private int tn = 2;
    private int to = 2;
    private VirtualKey tp;
    private final int[] tq = new int[3];
    private int tr = 0;
    private int x;
    private int y;
    private int z;

    public void a(AttributeSet attributeSet, String str) {
        this.sH = e.bt(attributeSet.getAttributeValue(str, "bitmap"));
        String attributeValue = attributeSet.getAttributeValue(str, "touch");
        if (attributeValue != null) {
            this.sF = e.br(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            this.sG = e.br(attributeValue2);
        }
        this.state = 1;
        this.sJ = attributeSet.getAttributeIntValue(str, "fade", -1);
        String attributeValue3 = attributeSet.getAttributeValue(str, StructuredData.TYPE_OF_VALUE);
        if (attributeValue3 != null) {
            String[] split = attributeValue3.split(BossService.ID_SEPARATOR);
            if (split.length >= 1) {
                try {
                    if (Integer.parseInt(split[0]) == 0) {
                        iP();
                    }
                } catch (NumberFormatException e) {
                    if (Boolean.parseBoolean(split[0])) {
                        iP();
                    }
                }
            }
            if (split.length >= 3) {
                this.tn = Integer.parseInt(split[1]);
                this.to = Integer.parseInt(split[2]);
            }
            if (split.length >= 6) {
                this.tm = new int[3];
                this.tm[0] = Integer.parseInt(split[3]);
                this.tm[1] = Integer.parseInt(split[4]);
                this.tm[2] = Integer.parseInt(split[5]);
            }
        }
    }

    public void a(c cVar) {
        super.a(cVar);
        sQ[0] = new VirtualKey();
        sQ[0].ty = "UP";
        sQ[0].state = 1;
        sQ[1] = new VirtualKey();
        sQ[1].ty = "DOWN";
        sQ[1].state = 1;
        sQ[2] = new VirtualKey();
        sQ[2].ty = "LEFT";
        sQ[2].state = 1;
        sQ[3] = new VirtualKey();
        sQ[3].ty = "RIGHT";
        sQ[3].state = 1;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.tr = 0;
        this.tq[0] = 0;
        this.tq[1] = 0;
        this.tq[2] = 0;
        iZ();
    }

    public synchronized void a(VirtualKey virtualKey) {
        if (virtualKey != this.tp) {
            iZ();
        }
        if (virtualKey != null && virtualKey.state == 1) {
            virtualKey.state = 0;
            VirtualKey.b(virtualKey);
            this.tp = virtualKey;
        }
        if (virtualKey != null) {
            Log.d(getName(), "sensor vb:[" + virtualKey.ty + "|" + virtualKey.state + "]");
        }
    }

    public String getName() {
        return "SensorSwitcher";
    }

    public void iP() {
        f.a((SensorEventListener) this);
        Log.w(getName(), "Switch on.");
    }

    public void iQ() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.tr = 0;
        this.tq[0] = 0;
        this.tq[1] = 0;
        this.tq[2] = 0;
        iZ();
        f.b((SensorEventListener) this);
        Log.w(getName(), "Switch off.");
    }

    public void iZ() {
        if (this.tp != null && this.tp.state == 0) {
            this.tp.state = 1;
            VirtualKey.b(this.tp);
            this.tp = null;
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        Log.d(getName(), "sensor:[" + ((int) sensorEvent.values[0]) + "|" + ((int) sensorEvent.values[1]) + "|" + ((int) sensorEvent.values[2]) + "]");
        this.sX = n.pJ.getOrientation();
        if (this.sX == 1) {
            this.x = -((int) sensorEvent.values[0]);
            this.y = (int) sensorEvent.values[1];
        } else if (this.sX == 0) {
            this.x = (int) sensorEvent.values[1];
            this.y = (int) sensorEvent.values[0];
        } else {
            Log.w(getName(), "deviceOrientation:" + this.sX);
            return;
        }
        this.z = (int) sensorEvent.values[2];
        if (this.tr < 1) {
            if (this.tm != null) {
                this.tq[0] = this.tm[0];
                this.tq[1] = this.tm[1];
                this.tq[2] = this.tm[2];
            } else {
                this.tq[0] = this.x;
                this.tq[1] = this.y;
                this.tq[2] = this.z;
            }
            Log.d(getName(), "sensor_init:[" + this.tq[0] + "|" + this.tq[1] + "|" + this.tq[2] + "]");
            this.tr++;
            return;
        }
        int i = this.x - this.tq[0];
        int i2 = this.y - this.tq[1];
        int i3 = this.z - this.tq[2];
        if (Math.abs(i) < this.tn && Math.abs(i2) < this.to && Math.abs(i3) < this.to) {
            iZ();
        } else if (Math.abs(i) >= this.tn) {
            if (i < 0) {
                a(sQ[2]);
            } else {
                a(sQ[3]);
            }
        } else if (Math.abs(i2) >= this.to) {
            if (i2 > 0) {
                a(sQ[1]);
            } else {
                a(sQ[0]);
            }
            a(this.tp);
        } else if (Math.abs(i3) < this.to) {
        } else {
            if (i3 > 0) {
                a(sQ[0]);
            } else {
                a(sQ[1]);
            }
        }
    }
}
