package org.meteoroid.plugin.device;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.Message;
import android.os.StatFs;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.a.a.e.o;
import com.a.a.e.p;
import com.a.a.e.z;
import com.fasterxml.jackson.core.sym.CharsToNameCanonicalizer;
import g31fhsgzqxzl.gf.R;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.TimerTask;
import java.util.UUID;
import java.util.Vector;
import java.util.regex.Pattern;
import javax.microedition.media.Control;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.PlayerListener;
import javax.microedition.media.control.VolumeControl;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.core.f;
import org.meteoroid.core.g;
import org.meteoroid.core.h;
import org.meteoroid.core.n;
import org.meteoroid.plugin.vd.ScreenWidget;
import org.meteoroid.plugin.vd.VirtualKey;

public class MIDPDevice extends ScreenWidget implements com.a.a.r.a, f.a, f.C0004f, h.a {
    public static final int ASTERISK = 11;
    public static final int AUTO_DETECT = -1;
    public static final int DOWN = 1;
    public static final int FIRE = 4;
    public static final int GAME_A = 5;
    public static final int GAME_B = 6;
    public static final int GAME_C = 7;
    public static final int GAME_D = 8;
    public static final int LEFT = 2;
    public static final int MSG_MIDP_COMMAND_EVENT = 44034;
    public static final int MSG_MIDP_DISPLAY_CALL_SERIALLY = 44035;
    public static final int MSG_MIDP_MIDLET_NOTIFYDESTROYED = 44036;
    public static final int MUTE_SWITCH = 2;
    public static final int POUND = 12;
    public static final int POWER = 0;
    public static final int RIGHT = 3;
    public static final int SENSOR_SWITCH = 1;
    public static final int SOFT1 = 9;
    public static final int SOFT2 = 10;
    public static final int UP = 0;
    public static final int URL = 8;
    public static final String[] pR = {"UP", "DOWN", "LEFT", "RIGHT", "SELECT", "GAME_A", "GAME_B", "GAME_C", "GAME_D", "SOFT1", "SOFT2", "ASTERISK", "POUND", "UP_LEFT", "UP_RIGHT", "DOWN_LEFT", "DOWN_RIGHT"};
    private static final HashMap<String, Integer> pZ = new HashMap<>(pR.length);
    private static final Properties qh = new Properties();
    public static String qr = "";
    public UUID gw;
    private int height = -1;
    private boolean pS;
    private boolean pT;
    private boolean pU;
    private boolean pV;
    private boolean pW;
    private boolean pX;
    private boolean pY;
    private Bitmap qa;
    private Bitmap qb;
    private Bitmap qc;
    private Canvas qd;
    private e qe;
    private e qf;
    private e qg;
    /* access modifiers changed from: private */
    public volatile int qi = -1;
    /* access modifiers changed from: private */
    public volatile int qj = -1;
    private boolean qk = false;
    private int ql;
    private int qm;
    private int qn;
    private int qo;
    private int qp;
    public g qq;
    public h qs;
    public boolean qt = false;
    public String qu = "";
    private int width = -1;

    private final class a extends TimerTask {
        private a() {
        }

        public void run() {
            if (MIDPDevice.this.qi == -1 && MIDPDevice.this.qj != -1) {
                MIDPDevice.this.T(1, MIDPDevice.this.bT(MIDPDevice.this.qj));
                int unused = MIDPDevice.this.qj = -1;
            }
        }
    }

    public static final class b implements com.a.a.d.b {
        File file;
        InputStream qA;
        OutputStream qB;
        DataInputStream qy;
        DataOutputStream qz;
        String url;

        private class a extends OutputStream {
            private RandomAccessFile qC;

            public a(RandomAccessFile randomAccessFile) {
                this.qC = randomAccessFile;
            }

            public void close() {
                this.qC.close();
            }

            public void flush() {
            }

            public void write(int i) {
                this.qC.writeByte(i);
            }

            public void write(byte[] bArr) {
                this.qC.write(bArr);
            }

            public void write(byte[] bArr, int i, int i2) {
                this.qC.write(bArr, i, i2);
            }
        }

        public b(String str, int i) {
            this.url = str;
            String substring = str.substring("file://".length());
            if (Environment.getExternalStorageState().equals("mounted")) {
                this.file = new File(Environment.getExternalStorageDirectory() + substring);
                return;
            }
            throw new IOException("SD card not ready.");
        }

        public long aF() {
            return 0;
        }

        public long aG() {
            StatFs statFs = new StatFs(getPath());
            return (long) (statFs.getBlockSize() * statFs.getAvailableBlocks());
        }

        public long aH() {
            return 0;
        }

        public long aI() {
            return this.file.length();
        }

        public Enumeration<String> aJ() {
            String[] list = this.file.list();
            Vector vector = new Vector(list.length);
            for (String addElement : list) {
                vector.addElement(addElement);
            }
            return vector.elements();
        }

        public void aK() {
            this.file.mkdir();
        }

        public DataInputStream ak() {
            this.qy = new DataInputStream(al());
            return this.qy;
        }

        public InputStream al() {
            this.qA = new FileInputStream(this.file);
            return this.qA;
        }

        public DataOutputStream am() {
            this.qz = new DataOutputStream(an());
            return this.qz;
        }

        public OutputStream an() {
            this.qB = new FileOutputStream(this.file);
            return this.qB;
        }

        public OutputStream b(long j) {
            RandomAccessFile randomAccessFile = new RandomAccessFile(this.file, "rw");
            randomAccessFile.seek(j);
            this.qB = new a(randomAccessFile);
            return this.qB;
        }

        public Enumeration<String> b(String str, boolean z) {
            return null;
        }

        public boolean canRead() {
            return this.file.canRead();
        }

        public boolean canWrite() {
            return this.file.canWrite();
        }

        public void close() {
            if (this.qz != null) {
                this.qz.close();
                this.qz = null;
            }
            if (this.qy != null) {
                this.qy.close();
                this.qy = null;
            }
            if (this.qB != null) {
                this.qB.close();
                this.qB = null;
            }
            if (this.qA != null) {
                this.qA.close();
                this.qA = null;
            }
        }

        public void create() {
            this.file.createNewFile();
        }

        public void delete() {
            this.file.delete();
        }

        public long e(boolean z) {
            return 0;
        }

        public boolean exists() {
            return this.file.exists();
        }

        public void f(boolean z) {
        }

        public void g(boolean z) {
        }

        public String getName() {
            String name = this.file.getName();
            int lastIndexOf = name.lastIndexOf(47);
            return lastIndexOf == -1 ? name : name.substring(lastIndexOf);
        }

        public String getPath() {
            String path = this.file.getPath();
            int lastIndexOf = path.lastIndexOf(47);
            return lastIndexOf == -1 ? path : path.substring(0, lastIndexOf);
        }

        public String getURL() {
            return this.url;
        }

        public void h(boolean z) {
        }

        public boolean isDirectory() {
            return this.file.isDirectory();
        }

        public boolean isHidden() {
            return isHidden();
        }

        public boolean isOpen() {
            return (this.qy == null && this.qz == null && this.qA == null && this.qB == null) ? false : true;
        }

        public long lastModified() {
            return this.file.lastModified();
        }

        public void truncate(long j) {
        }

        public void y(String str) {
            this.file.renameTo(new File(getPath(), str));
        }

        public void z(String str) {
        }
    }

    public static final class c {
        public final Paint hq = new Paint(1);
        public Paint.FontMetricsInt qE;

        public c(Typeface typeface, int i, boolean z) {
            this.hq.setTypeface(typeface);
            this.hq.setTextSize((float) i);
            this.hq.setUnderlineText(z);
            this.qE = this.hq.getFontMetricsInt();
        }

        public int A(String str) {
            return (int) this.hq.measureText(str);
        }

        public int a(char c) {
            return (int) this.hq.measureText(String.valueOf(c));
        }

        public int a(char[] cArr, int i, int i2) {
            return (int) this.hq.measureText(cArr, i, i2);
        }

        public int b(String str, int i, int i2) {
            return (int) this.hq.measureText(str, i, i + i2);
        }

        public int bH() {
            return -this.qE.ascent;
        }

        public int getHeight() {
            return this.hq.getFontMetricsInt(this.qE);
        }
    }

    public static final class d {
        public static int SIZE_LARGE = 16;
        public static int SIZE_MEDIUM = 14;
        public static int SIZE_SMALL = 12;
        private static final HashMap<com.a.a.e.l, c> qF = new HashMap<>();

        public static int a(com.a.a.e.l lVar, char c) {
            return b(lVar).a(c);
        }

        public static int a(com.a.a.e.l lVar, String str) {
            return b(lVar).A(str);
        }

        public static int a(com.a.a.e.l lVar, String str, int i, int i2) {
            return b(lVar).b(str, i, i2);
        }

        public static int a(com.a.a.e.l lVar, char[] cArr, int i, int i2) {
            return b(lVar).a(cArr, i, i2);
        }

        public static c b(com.a.a.e.l lVar) {
            boolean z = true;
            int i = 0;
            c cVar = qF.get(lVar);
            if (cVar != null) {
                return cVar;
            }
            Typeface typeface = Typeface.SANS_SERIF;
            if (lVar.bE() == 0) {
                typeface = Typeface.SANS_SERIF;
            } else if (lVar.bE() == 32) {
                typeface = Typeface.MONOSPACE;
            } else if (lVar.bE() == 64) {
                typeface = Typeface.SANS_SERIF;
            }
            if ((lVar.getStyle() & 0) != 0) {
            }
            int i2 = (lVar.getStyle() & 1) != 0 ? 1 : 0;
            if ((lVar.getStyle() & 2) != 0) {
                i2 |= 2;
            }
            if ((lVar.getStyle() & 4) == 0) {
                z = false;
            }
            if (lVar.getSize() == 8) {
                i = SIZE_SMALL;
            } else if (lVar.getSize() == 0) {
                i = SIZE_MEDIUM;
            } else if (lVar.getSize() == 16) {
                i = SIZE_LARGE;
            }
            c cVar2 = new c(Typeface.create(typeface, i2), i, z);
            qF.put(lVar, cVar2);
            return cVar2;
        }

        public static int c(com.a.a.e.l lVar) {
            return b(lVar).bH();
        }

        public static int d(com.a.a.e.l lVar) {
            return b(lVar).getHeight();
        }

        public static com.a.a.e.l v(int i, int i2, int i3) {
            boolean z = true;
            com.a.a.e.l lVar = new com.a.a.e.l(i, i2, i3);
            Typeface typeface = Typeface.SANS_SERIF;
            if (lVar.bE() == 0) {
                typeface = Typeface.SANS_SERIF;
            } else if (lVar.bE() == 32) {
                typeface = Typeface.MONOSPACE;
            } else if (lVar.bE() == 64) {
                typeface = Typeface.SANS_SERIF;
            }
            if ((lVar.getStyle() & 0) != 0) {
            }
            int i4 = (lVar.getStyle() & 1) != 0 ? 1 : 0;
            if ((lVar.getStyle() & 2) != 0) {
                i4 |= 2;
            }
            if ((lVar.getStyle() & 4) == 0) {
                z = false;
            }
            qF.put(lVar, new c(Typeface.create(typeface, i4), i3, z));
            return lVar;
        }

        public void init() {
            qF.clear();
        }
    }

    public static class e extends o {
        private static final DashPathEffect qJ = new DashPathEffect(new float[]{5.0f, 5.0f}, 0.0f);
        private com.a.a.e.l jf;
        private Paint qG;
        private Paint qH;
        private c qI;
        private Canvas qK;
        private int qL;
        private Matrix qM;
        private Bitmap qN;
        private int qO;
        private int qP;
        private int qQ;
        private int qR;
        private Bitmap qS;
        private int[] qT;

        private e() {
            this.qG = new Paint();
            this.qH = new Paint();
            this.qL = 0;
            this.qM = new Matrix();
            this.qO = 0;
            this.qP = 0;
            this.qQ = 0;
            this.qR = 0;
            this.qG.setAntiAlias(false);
            this.qG.setStyle(Paint.Style.STROKE);
            this.qH.setAntiAlias(false);
            this.qH.setStyle(Paint.Style.FILL);
        }

        public e(Bitmap bitmap) {
            this();
            this.qN = bitmap;
            this.qK = new Canvas(bitmap);
            b(this.qK);
        }

        public e(Canvas canvas) {
            this();
            b(canvas);
        }

        public void a(com.a.a.e.l lVar) {
            if (lVar == null) {
                lVar = com.a.a.e.l.bD();
            }
            this.jf = lVar;
            this.qI = d.b(lVar);
        }

        public void a(p pVar, int i, int i2, int i3) {
            if (pVar != null && this.qQ > 0 && this.qR > 0 && pVar.iN != null) {
                if (i3 == 0) {
                    i3 = 20;
                }
                if ((i3 & 8) != 0) {
                    i -= pVar.width;
                } else if ((i3 & 1) != 0) {
                    i -= pVar.width / 2;
                }
                if ((i3 & 32) != 0) {
                    i2 -= pVar.height;
                } else if ((i3 & 2) != 0) {
                    i2 -= pVar.height / 2;
                }
                this.qK.drawBitmap(pVar.iN, (float) i, (float) i2, (Paint) null);
            }
        }

        public void a(p pVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            int i9;
            int i10;
            int i11;
            boolean z = true;
            if (pVar != null && pVar.iN != null && this.qQ > 0 && this.qR > 0) {
                if (i < 0 || i2 < 0) {
                    throw new IllegalArgumentException("Area out of Image: x" + i + " y:" + i2);
                } else if (!pVar.isMutable() || pVar.bX() != this) {
                    if (i + i3 > pVar.getWidth()) {
                        i3 = pVar.getWidth() - i;
                    }
                    int height = i2 + i4 > pVar.getHeight() ? pVar.getHeight() - i2 : i4;
                    if (i3 > 0 && height > 0) {
                        this.qM.reset();
                        switch (i5) {
                            case 0:
                                i9 = i3;
                                i3 = height;
                                i10 = -i;
                                i11 = -i2;
                                break;
                            case 1:
                                this.qM.preScale(-1.0f, 1.0f);
                                this.qM.preRotate(-180.0f);
                                int i12 = height + i2;
                                i9 = i3;
                                i3 = height;
                                i10 = -i;
                                i11 = i12;
                                break;
                            case 2:
                                this.qM.preScale(-1.0f, 1.0f);
                                int i13 = i3 + i;
                                i9 = i3;
                                i3 = height;
                                i10 = i13;
                                i11 = -i2;
                                break;
                            case 3:
                                this.qM.preRotate(180.0f);
                                int i14 = i3 + i;
                                int i15 = height + i2;
                                i9 = i3;
                                i3 = height;
                                i10 = i14;
                                i11 = i15;
                                break;
                            case 4:
                                this.qM.preScale(-1.0f, 1.0f);
                                this.qM.preRotate(-270.0f);
                                i9 = height;
                                i10 = -i2;
                                i11 = -i;
                                break;
                            case 5:
                                this.qM.preRotate(90.0f);
                                i9 = height;
                                i10 = height + i2;
                                i11 = -i;
                                break;
                            case 6:
                                this.qM.preRotate(270.0f);
                                i9 = height;
                                i10 = -i2;
                                i11 = i3 + i;
                                break;
                            case 7:
                                this.qM.preScale(-1.0f, 1.0f);
                                this.qM.preRotate(-90.0f);
                                i9 = height;
                                i10 = height + i2;
                                i11 = i3 + i;
                                break;
                            default:
                                throw new IllegalArgumentException("Bad transform");
                        }
                        boolean z2 = false;
                        if (i8 == 0) {
                            i8 = 20;
                        }
                        if ((i8 & 64) != 0) {
                            z2 = true;
                        }
                        if ((i8 & 16) != 0) {
                            if ((i8 & 34) != 0) {
                                z2 = true;
                            }
                        } else if ((i8 & 32) != 0) {
                            if ((i8 & 2) != 0) {
                                z2 = true;
                            } else {
                                i7 -= i3;
                            }
                        } else if ((i8 & 2) != 0) {
                            i7 -= (i3 - 1) >>> 1;
                        } else {
                            z2 = true;
                        }
                        if ((i8 & 4) != 0) {
                            if ((i8 & 9) == 0) {
                                z = z2;
                            }
                        } else if ((i8 & 8) != 0) {
                            if ((i8 & 1) == 0) {
                                i6 -= i9;
                                z = z2;
                            }
                        } else if ((i8 & 1) != 0) {
                            i6 -= (i9 - 1) >>> 1;
                            z = z2;
                        }
                        if (z) {
                            throw new IllegalArgumentException("Bad Anchor");
                        }
                        this.qK.save();
                        this.qK.clipRect(i6, i7, i6 + i9, i7 + i3);
                        this.qK.translate((float) (i6 + i10), (float) (i11 + i7));
                        this.qK.drawBitmap(pVar.iN, this.qM, null);
                        this.qK.restore();
                    }
                } else {
                    throw new IllegalArgumentException("Image is source and target");
                }
            }
        }

        public void a(String str, int i, int i2, int i3) {
            if (this.qQ > 0 && this.qR > 0) {
                if (i3 == 0) {
                    i3 = 20;
                }
                if ((i3 & 16) != 0) {
                    i2 -= this.qI.qE.top;
                } else if ((i3 & 32) != 0) {
                    i2 -= this.qI.qE.bottom;
                } else if ((i3 & 2) != 0) {
                    i2 += ((this.qI.qE.descent - this.qI.qE.ascent) / 2) - this.qI.qE.descent;
                }
                if ((i3 & 1) != 0) {
                    this.qI.hq.setTextAlign(Paint.Align.CENTER);
                } else if ((i3 & 8) != 0) {
                    this.qI.hq.setTextAlign(Paint.Align.RIGHT);
                } else if ((i3 & 4) != 0) {
                    this.qI.hq.setTextAlign(Paint.Align.LEFT);
                }
                this.qI.hq.setColor(this.qG.getColor());
                this.qK.drawText(str, (float) i, (float) i2, this.qI.hq);
            }
        }

        public void a(String str, int i, int i2, int i3, int i4, int i5) {
            if (this.qQ > 0 && this.qR > 0) {
                if (i5 == 0) {
                    i5 = 20;
                }
                if ((i5 & 16) != 0) {
                    i4 -= this.qI.qE.top;
                } else if ((i5 & 32) != 0) {
                    i4 -= this.qI.qE.bottom;
                } else if ((i5 & 2) != 0) {
                    i4 += ((this.qI.qE.descent - this.qI.qE.ascent) / 2) - this.qI.qE.descent;
                }
                if ((i5 & 1) != 0) {
                    this.qI.hq.setTextAlign(Paint.Align.CENTER);
                } else if ((i5 & 8) != 0) {
                    this.qI.hq.setTextAlign(Paint.Align.RIGHT);
                } else if ((i5 & 4) != 0) {
                    this.qI.hq.setTextAlign(Paint.Align.LEFT);
                }
                this.qI.hq.setColor(this.qG.getColor());
                String str2 = str;
                int i6 = i;
                this.qK.drawText(str2, i6, i2 + i, (float) i3, (float) i4, this.qI.hq);
            }
        }

        public void a(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
            int[] iArr2;
            if (iArr != null && this.qQ > 0 && this.qR > 0 && i5 > 0 && i6 > 0) {
                int i7 = i2 < i5 ? i5 : i2;
                if (iArr.length - i < i5 * i6) {
                    iArr2 = new int[((i5 * i6) + i)];
                    int i8 = i;
                    while (i8 < iArr2.length) {
                        System.arraycopy(iArr, 0, iArr2, i8, i5);
                        i8 += i5;
                    }
                } else {
                    iArr2 = iArr;
                }
                this.qK.drawBitmap(iArr2, i, i7, i3, i4, i5, i6, z, (Paint) null);
            }
        }

        public void ak(int i) {
            if (i == 0 || i == 1) {
                this.qL = i;
                if (i == 0) {
                    this.qG.setPathEffect(null);
                    this.qH.setPathEffect(null);
                    return;
                }
                this.qG.setPathEffect(qJ);
                this.qH.setPathEffect(qJ);
                return;
            }
            throw new IllegalArgumentException();
        }

        public int al(int i) {
            return i;
        }

        public void b(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.qQ > 0 && this.qR > 0 && i3 > 0 && i4 > 0) {
                this.qK.drawArc(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, true, this.qG);
            }
        }

        public void b(Canvas canvas) {
            this.qK = canvas;
            a(com.a.a.e.l.bD());
            translate(-bU(), -bV());
            setColor(0);
            if (this.qN != null) {
                setClip(0, 0, this.qN.getWidth(), this.qN.getHeight());
            } else {
                setClip(0, 0, org.meteoroid.core.c.ou.getWidth(), org.meteoroid.core.c.ou.getHeight());
            }
        }

        public int bL() {
            return this.qR;
        }

        public int bM() {
            return this.qQ;
        }

        public int bN() {
            return this.qO;
        }

        public int bO() {
            return this.qP;
        }

        public com.a.a.e.l bP() {
            return this.jf;
        }

        public int bT() {
            return this.qL;
        }

        public void bW() {
            b(this.qK);
        }

        public void c(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.qQ > 0 && this.qR > 0 && i3 > 0 && i4 > 0) {
                this.qK.drawRoundRect(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, this.qG);
            }
        }

        public void c(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
            boolean z = true;
            if (this.qQ > 0 && this.qR > 0 && i3 > 0 && i4 > 0 && this.qN != null) {
                Log.d("copyArea", "src:" + i + "|" + i2 + "|" + i3 + "|" + i4 + " dest:" + i5 + "|" + i6);
                if (i7 == 0) {
                    i7 = 20;
                }
                boolean z2 = (i7 & 64) != 0;
                if ((i7 & 16) != 0) {
                    if ((i7 & 34) != 0) {
                        z2 = true;
                    }
                } else if ((i7 & 32) != 0) {
                    if ((i7 & 2) != 0) {
                        z2 = true;
                    } else {
                        i6 -= i4 - 1;
                    }
                } else if ((i7 & 2) != 0) {
                    i6 -= (i4 - 1) >>> 1;
                } else {
                    z2 = true;
                }
                if ((i7 & 4) != 0) {
                    if ((i7 & 9) == 0) {
                        z = z2;
                    }
                } else if ((i7 & 8) != 0) {
                    if ((i7 & 1) == 0) {
                        i5 -= i3 - 1;
                        z = z2;
                    }
                } else if ((i7 & 1) != 0) {
                    i5 -= (i3 - 1) >>> 1;
                    z = z2;
                }
                if (z) {
                    throw new IllegalArgumentException("Bad Anchor");
                }
                if (this.qS != null && this.qS.getWidth() == i3 && this.qS.getHeight() == i4) {
                    if (this.qT == null || !(this.qT == null || this.qT.length == i3 * i4)) {
                        this.qT = new int[(i3 * i4)];
                    }
                    this.qN.getPixels(this.qT, 0, i3, i, i2, i3, i4);
                    this.qS.setPixels(this.qT, 0, i3, 0, 0, i3, i4);
                } else {
                    this.qS = Bitmap.createBitmap(this.qN, i, i2, i3, i4);
                }
                this.qK.drawBitmap(this.qS, (float) i5, (float) i6, this.qG);
            }
        }

        public void c(Bitmap bitmap) {
            this.qK.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        }

        public void d(int i, int i2, int i3, int i4) {
            if (i3 < 0 || i4 < 0) {
                i4 = 0;
                i3 = 0;
            }
            this.qK.clipRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), Region.Op.INTERSECT);
            Rect clipBounds = this.qK.getClipBounds();
            this.qO = clipBounds.left;
            this.qP = clipBounds.top;
            this.qQ = clipBounds.width();
            this.qR = clipBounds.height();
        }

        public void d(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.qQ > 0 && this.qR > 0 && i3 > 0 && i4 > 0) {
                this.qK.drawArc(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, true, this.qH);
            }
        }

        public void e(int i, int i2, int i3, int i4) {
            if (this.qQ > 0 && this.qR > 0) {
                if (i > i3) {
                    i++;
                } else {
                    i3++;
                }
                if (i2 > i4) {
                    i2++;
                } else {
                    i4++;
                }
                this.qK.drawLine((float) i, (float) i2, (float) i3, (float) i4, this.qG);
            }
        }

        public void e(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.qQ > 0 && this.qR > 0 && i3 > 0 && i4 > 0) {
                this.qK.drawRoundRect(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, this.qH);
            }
        }

        public void f(int i, int i2, int i3, int i4) {
            if (this.qQ > 0 && this.qR > 0) {
                if (i3 == 0 && i4 == 0) {
                    this.qK.drawPoint((float) i, (float) i2, this.qH);
                } else {
                    this.qK.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.qG);
                }
            }
        }

        public void f(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.qQ > 0 && this.qR > 0) {
                Path path = new Path();
                path.moveTo((float) i, (float) i2);
                path.lineTo((float) i3, (float) i4);
                path.lineTo((float) i5, (float) i6);
                path.lineTo((float) i, (float) i2);
                this.qK.drawPath(path, this.qH);
            }
        }

        public void fillRect(int i, int i2, int i3, int i4) {
            if (this.qQ > 0 && this.qR > 0 && i3 > 0 && i4 > 0) {
                this.qK.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.qH);
            }
        }

        public Bitmap getBitmap() {
            return this.qN;
        }

        public Canvas ig() {
            return this.qK;
        }

        public void setClip(int i, int i2, int i3, int i4) {
            this.qO = i;
            this.qP = i2;
            this.qQ = i3;
            this.qR = i4;
            if (i3 > 0 && i4 > 0) {
                this.qK.clipRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), Region.Op.REPLACE);
            }
        }

        public void setColor(int i) {
            this.qG.setColor(-16777216 | i);
            this.qH.setColor(-16777216 | i);
            super.setColor(i);
        }

        public void translate(int i, int i2) {
            if (i != 0 || i2 != 0) {
                super.translate(i, i2);
                this.qK.translate((float) i, (float) i2);
                setClip(bN() - i, bO() - i2, bM(), bL());
            }
        }
    }

    public static final class f implements com.a.a.c.h {
        private HttpURLConnection qU;
        private URL url;

        public f(String str, int i, boolean z) {
            try {
                if (!hh()) {
                    throw new IOException("No avaliable connection.");
                }
                this.url = new URL(str);
                this.qU = (HttpURLConnection) this.url.openConnection();
                this.qU.setDoInput(true);
                if (i != 1) {
                    this.qU.setDoOutput(true);
                }
                if (z) {
                    this.qU.setConnectTimeout(10000);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }

        private final boolean hh() {
            NetworkInfo activeNetworkInfo;
            try {
                ConnectivityManager hu = org.meteoroid.core.l.hu();
                return hu != null && (activeNetworkInfo = hu.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
            } catch (Exception e) {
                Log.v(PlayerListener.ERROR, e.toString());
            }
        }

        public DataInputStream ak() {
            return new DataInputStream(al());
        }

        public InputStream al() {
            return this.qU.getInputStream();
        }

        public DataOutputStream am() {
            return new DataOutputStream(an());
        }

        public OutputStream an() {
            return this.qU.getOutputStream();
        }

        public void close() {
            Log.d("HTTPConnection", "Connection " + this.url.toString() + " disconnected.");
            this.qU.disconnect();
        }

        public long getDate() {
            return this.qU.getDate();
        }

        public String getEncoding() {
            return this.qU.getContentEncoding();
        }

        public long getExpiration() {
            return this.qU.getExpiration();
        }

        public String getFile() {
            return this.url.getFile();
        }

        public String getHeaderField(int i) {
            return this.qU.getHeaderField(i);
        }

        public String getHeaderField(String str) {
            return this.qU.getHeaderField(str);
        }

        public long getHeaderFieldDate(String str, long j) {
            return this.qU.getHeaderFieldDate(str, j);
        }

        public int getHeaderFieldInt(String str, int i) {
            return this.qU.getHeaderFieldInt(str, i);
        }

        public String getHeaderFieldKey(int i) {
            return this.qU.getHeaderFieldKey(i);
        }

        public String getHost() {
            return this.url.getHost();
        }

        public long getLastModified() {
            return this.qU.getLastModified();
        }

        public long getLength() {
            return (long) this.qU.getContentLength();
        }

        public int getPort() {
            return this.url.getPort();
        }

        public String getProtocol() {
            return this.url.getProtocol();
        }

        public String getQuery() {
            return this.url.getQuery();
        }

        public String getRef() {
            return this.url.getRef();
        }

        public String getRequestMethod() {
            return this.qU.getRequestMethod();
        }

        public String getRequestProperty(String str) {
            return this.qU.getRequestProperty(str);
        }

        public int getResponseCode() {
            Log.d("HTTPConnection", "Connection " + this.url.toString() + " established.");
            String requestProperty = this.qU.getRequestProperty("X-Online-Host");
            if (requestProperty != null) {
                Log.d("HTTPConnection", "Connection X-Online-Host is" + requestProperty + ".");
            }
            this.qU.connect();
            int responseCode = this.qU.getResponseCode();
            Log.d("HTTPConnection", "Connection " + this.url.toString() + " response " + responseCode);
            return responseCode;
        }

        public String getResponseMessage() {
            return this.qU.getResponseMessage();
        }

        public String getType() {
            return this.qU.getContentType();
        }

        public String getURL() {
            return this.url.getPath();
        }

        public void setRequestMethod(String str) {
            this.qU.setRequestMethod(str);
        }

        public void setRequestProperty(String str, String str2) {
            this.qU.setRequestProperty(str, str2);
        }
    }

    public static final class g implements com.a.a.b.g {
        public BluetoothDevice he = null;
        BluetoothAdapter qV;
        BluetoothSocket qW;
        com.a.a.b.j qX;

        /* JADX WARNING: Removed duplicated region for block: B:15:0x009e A[LOOP:1: B:15:0x009e->B:34:0x0166, LOOP_START, PHI: r2 r4 
          PHI: (r2v15 int) = (r2v14 int), (r2v16 int) binds: [B:14:0x009c, B:34:0x0166] A[DONT_GENERATE, DONT_INLINE]
          PHI: (r4v4 android.bluetooth.BluetoothSocket) = (r4v3 android.bluetooth.BluetoothSocket), (r4v5 android.bluetooth.BluetoothSocket) binds: [B:14:0x009c, B:34:0x0166] A[DONT_GENERATE, DONT_INLINE]] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0131  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public g(java.lang.String r8) {
            /*
                r7 = this;
                r1 = 0
                r3 = 0
                r7.<init>()
                r7.he = r1
                java.lang.String r0 = "MIDPDevice"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r4 = "open StreamConnection   name = "
                java.lang.StringBuilder r2 = r2.append(r4)
                java.lang.StringBuilder r2 = r2.append(r8)
                java.lang.String r2 = r2.toString()
                android.util.Log.d(r0, r2)
                android.bluetooth.BluetoothAdapter r0 = android.bluetooth.BluetoothAdapter.getDefaultAdapter()
                r7.qV = r0
                java.lang.String r0 = "MACadd"
                int r0 = r8.lastIndexOf(r0)
                java.lang.String r2 = "MACadd"
                int r2 = r2.length()
                int r2 = r2 + r0
                java.lang.String r0 = ""
                r4 = r0
                r0 = r2
            L_0x0036:
                int r5 = r2 + 17
                if (r0 >= r5) goto L_0x0052
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.StringBuilder r4 = r5.append(r4)
                char r5 = r8.charAt(r0)
                java.lang.StringBuilder r4 = r4.append(r5)
                java.lang.String r4 = r4.toString()
                int r0 = r0 + 1
                goto L_0x0036
            L_0x0052:
                com.a.a.r.a r0 = org.meteoroid.core.c.ou
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0
                com.a.a.b.e r2 = com.a.a.b.e.gv
                java.util.UUID r2 = r2.gw
                r0.gw = r2
                java.lang.String r0 = "MIDPDevice"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x016e }
                r2.<init>()     // Catch:{ Exception -> 0x016e }
                java.lang.String r5 = "ADD = "
                java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x016e }
                java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x016e }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x016e }
                android.util.Log.d(r0, r2)     // Catch:{ Exception -> 0x016e }
                java.lang.String r0 = "MIDPDevice"
                java.lang.String r2 = "先直接连获取到的远程设备.如果连不上再循环连已经搜索到的设备"
                android.util.Log.d(r0, r2)     // Catch:{ Exception -> 0x016e }
                android.bluetooth.BluetoothAdapter r0 = r7.qV     // Catch:{ Exception -> 0x016e }
                android.bluetooth.BluetoothDevice r2 = r0.getRemoteDevice(r4)     // Catch:{ Exception -> 0x016e }
                com.a.a.r.a r0 = org.meteoroid.core.c.ou     // Catch:{ Exception -> 0x016e }
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0     // Catch:{ Exception -> 0x016e }
                java.util.UUID r0 = r0.gw     // Catch:{ Exception -> 0x016e }
                android.bluetooth.BluetoothSocket r1 = r2.createRfcommSocketToServiceRecord(r0)     // Catch:{ Exception -> 0x016e }
                if (r1 != 0) goto L_0x0148
                java.lang.String r0 = "MIDPDevice"
                java.lang.String r2 = "BluetoothSocket null"
                android.util.Log.d(r0, r2)     // Catch:{ Exception -> 0x009a }
                java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ Exception -> 0x009a }
                r0.<init>()     // Catch:{ Exception -> 0x009a }
                throw r0     // Catch:{ Exception -> 0x009a }
            L_0x009a:
                r0 = move-exception
                r0 = r1
            L_0x009c:
                r2 = r3
                r4 = r0
            L_0x009e:
                com.a.a.b.e r0 = com.a.a.b.e.gv
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.gF
                int r0 = r0.size()
                if (r2 >= r0) goto L_0x0175
                com.a.a.r.a r0 = org.meteoroid.core.c.ou     // Catch:{ IOException -> 0x014c }
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0     // Catch:{ IOException -> 0x014c }
                java.util.UUID r0 = r0.gw     // Catch:{ IOException -> 0x014c }
                if (r0 == 0) goto L_0x0172
                java.lang.String r0 = "MIDPDevice"
                java.lang.String r1 = "开始连接"
                android.util.Log.d(r0, r1)     // Catch:{ IOException -> 0x014c }
                java.lang.String r1 = "MIDPDevice"
                com.a.a.b.e r0 = com.a.a.b.e.gv     // Catch:{ IOException -> 0x014c }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.gF     // Catch:{ IOException -> 0x014c }
                java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ IOException -> 0x014c }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x014c }
                java.lang.String r0 = r0.getName()     // Catch:{ IOException -> 0x014c }
                android.util.Log.d(r1, r0)     // Catch:{ IOException -> 0x014c }
                java.lang.String r1 = "MIDPDevice"
                com.a.a.b.e r0 = com.a.a.b.e.gv     // Catch:{ IOException -> 0x014c }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.gF     // Catch:{ IOException -> 0x014c }
                java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ IOException -> 0x014c }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x014c }
                java.lang.String r0 = r0.getAddress()     // Catch:{ IOException -> 0x014c }
                android.util.Log.d(r1, r0)     // Catch:{ IOException -> 0x014c }
                com.a.a.b.e r0 = com.a.a.b.e.gv     // Catch:{ IOException -> 0x014c }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.gF     // Catch:{ IOException -> 0x014c }
                java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ IOException -> 0x014c }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x014c }
                com.a.a.r.a r1 = org.meteoroid.core.c.ou     // Catch:{ IOException -> 0x014c }
                org.meteoroid.plugin.device.MIDPDevice r1 = (org.meteoroid.plugin.device.MIDPDevice) r1     // Catch:{ IOException -> 0x014c }
                java.util.UUID r1 = r1.gw     // Catch:{ IOException -> 0x014c }
                android.bluetooth.BluetoothSocket r1 = r0.createRfcommSocketToServiceRecord(r1)     // Catch:{ IOException -> 0x014c }
            L_0x00f1:
                if (r1 == 0) goto L_0x0166
                com.a.a.b.e r0 = com.a.a.b.e.gv     // Catch:{ IOException -> 0x016c }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.gF     // Catch:{ IOException -> 0x016c }
                java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ IOException -> 0x016c }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x016c }
                r7.he = r0     // Catch:{ IOException -> 0x016c }
                r1.connect()     // Catch:{ IOException -> 0x016c }
            L_0x0102:
                java.lang.String r0 = "MIDPDevice"
                java.lang.String r2 = "取消搜索"
                android.util.Log.d(r0, r2)
                r7.qW = r1
                java.lang.String r0 = "MIDPDevice"
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "远程设备描述内容 = "
                java.lang.StringBuilder r1 = r1.append(r2)
                android.bluetooth.BluetoothSocket r2 = r7.qW
                android.bluetooth.BluetoothDevice r2 = r2.getRemoteDevice()
                int r2 = r2.describeContents()
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r1 = r1.toString()
                android.util.Log.d(r0, r1)
                com.a.a.b.j r0 = r7.qX
                if (r0 != 0) goto L_0x013a
                com.a.a.b.j r0 = new com.a.a.b.j
                android.bluetooth.BluetoothSocket r1 = r7.qW
                r0.<init>(r1, r3)
                r7.qX = r0
            L_0x013a:
                com.a.a.r.a r0 = org.meteoroid.core.c.ou
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0
                r0.qq = r7
                java.lang.String r0 = "MIDPDevice"
                java.lang.String r1 = "连接成功"
                android.util.Log.d(r0, r1)
                return
            L_0x0148:
                r1.connect()     // Catch:{ Exception -> 0x009a }
                goto L_0x0102
            L_0x014c:
                r0 = move-exception
                r1 = r4
            L_0x014e:
                java.lang.String r4 = "MIDPDevice"
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "error = "
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.StringBuilder r0 = r5.append(r0)
                java.lang.String r0 = r0.toString()
                android.util.Log.e(r4, r0)
            L_0x0166:
                int r0 = r2 + 1
                r2 = r0
                r4 = r1
                goto L_0x009e
            L_0x016c:
                r0 = move-exception
                goto L_0x014e
            L_0x016e:
                r0 = move-exception
                r0 = r1
                goto L_0x009c
            L_0x0172:
                r1 = r4
                goto L_0x00f1
            L_0x0175:
                r1 = r4
                goto L_0x0102
            */
            throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.device.MIDPDevice.g.<init>(java.lang.String):void");
        }

        public int Z() {
            return this.qX.Z();
        }

        public int aa() {
            return this.qX.aa();
        }

        public DataInputStream ak() {
            return this.qX.ak();
        }

        public InputStream al() {
            return this.qX.al();
        }

        public DataOutputStream am() {
            return this.qX.am();
        }

        public OutputStream an() {
            return this.qX.an();
        }

        public void close() {
        }

        public void p(byte[] bArr) {
            this.qX.p(bArr);
        }

        public int q(byte[] bArr) {
            return this.qX.q(bArr);
        }

        public boolean ready() {
            return this.qX.ready();
        }
    }

    public static final class h implements com.a.a.b.h {
        private static final String LOG_TAG = "L2CapConnectionNotifier";
        public static final int STATE_CONNECTED = 3;
        public static final int STATE_CONNECTING = 2;
        public static final int STATE_LISTEN = 1;
        public static final int STATE_NONE = 0;
        public static int rb;
        public BluetoothDevice he;
        public BluetoothSocket qY;
        public final BluetoothServerSocket qZ;
        BluetoothAdapter ra;
        public boolean rc = false;
        com.a.a.b.j rd;

        public h(String str) {
            BluetoothServerSocket bluetoothServerSocket;
            Log.d(LOG_TAG, "open StreamConnectionNotifier");
            this.ra = BluetoothAdapter.getDefaultAdapter();
            ((MIDPDevice) org.meteoroid.core.c.ou).qu = this.ra.getName();
            int indexOf = str.indexOf(";name=");
            if (indexOf != -1) {
                int length = indexOf + ";name=".length();
                String substring = str.substring(length, str.length());
                int indexOf2 = substring.indexOf(59);
                Log.d(LOG_TAG, "nameindex2 = " + indexOf2);
                if (indexOf2 != -1) {
                    MIDPDevice.qr = substring.substring(0, indexOf2);
                } else {
                    MIDPDevice.qr = str.substring(length, str.length());
                }
            }
            Log.d("MIDPDevice", "ServerName = " + MIDPDevice.qr);
            try {
                ((MIDPDevice) org.meteoroid.core.c.ou).gw = UUID.fromString("11111111-2222-3333-4444-555555555555");
                if (MIDPDevice.qr != "") {
                    this.ra.setName(MIDPDevice.qr);
                }
                ((MIDPDevice) org.meteoroid.core.c.ou).qt = true;
                bluetoothServerSocket = this.ra.listenUsingRfcommWithServiceRecord("MIDPBlueTooth", ((MIDPDevice) org.meteoroid.core.c.ou).gw);
            } catch (IOException e) {
                Log.e(LOG_TAG, "listen() failed", e);
                bluetoothServerSocket = null;
            }
            this.qZ = bluetoothServerSocket;
            rb = 1;
            ((MIDPDevice) org.meteoroid.core.c.ou).qs = this;
        }

        /* renamed from: ab */
        public com.a.a.b.g aE() {
            rb = 1;
            new Thread() {
                public void run() {
                    while (h.rb != 3) {
                        try {
                            Log.d(h.LOG_TAG, "等待连接  " + h.rb);
                            BluetoothSocket accept = h.this.qZ.accept();
                            if (accept != null) {
                                synchronized (((MIDPDevice) org.meteoroid.core.c.ou).gw) {
                                    switch (h.rb) {
                                        case 0:
                                        case 3:
                                            try {
                                                Log.d(h.LOG_TAG, "STATE_CONNECTED");
                                                accept.close();
                                                h.rb = 1;
                                                break;
                                            } catch (IOException e) {
                                                Log.d(h.LOG_TAG, "Could not close unwanted socket");
                                                break;
                                            }
                                        case 1:
                                        case 2:
                                            try {
                                                h.this.qY = accept;
                                                h.this.he = h.this.qY.getRemoteDevice();
                                                h.this.rd = new com.a.a.b.j(h.this.qY, true);
                                                h.rb = 3;
                                                Log.d(h.LOG_TAG, "监听成功.连接成功");
                                                break;
                                            } catch (Exception e2) {
                                                Log.e(h.LOG_TAG, "服务器 连接失败 = " + e2);
                                                break;
                                            }
                                    }
                                    Log.d(h.LOG_TAG, "notify        ");
                                    ((MIDPDevice) org.meteoroid.core.c.ou).gw.notify();
                                }
                            }
                        } catch (Exception e3) {
                            Log.d(h.LOG_TAG, "accept() failed", e3);
                            return;
                        }
                    }
                    return;
                }
            }.start();
            synchronized (((MIDPDevice) org.meteoroid.core.c.ou).gw) {
                try {
                    Log.d(LOG_TAG, "wait");
                    ((MIDPDevice) org.meteoroid.core.c.ou).gw.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return this.rd;
        }

        public void close() {
        }
    }

    public static final class i implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, Player {
        private static final String LOG_TAG = "MIDP player";
        private g.a rf;
        private HashSet<PlayerListener> rg;
        private int rh = 1;
        private int ri;
        private boolean rj;
        private m rk;
        private int state = 100;

        public i(g.a aVar) {
            this.rf = aVar;
            aVar.pa = org.meteoroid.core.g.gV();
            aVar.pc.setOnCompletionListener(this);
            aVar.pc.setOnPreparedListener(this);
            this.rg = new HashSet<>();
        }

        private final void b(String str, Object obj) {
            Iterator<PlayerListener> it = this.rg.iterator();
            while (it.hasNext()) {
                PlayerListener next = it.next();
                if (next != null) {
                    next.a(this, str, obj);
                }
            }
        }

        public Control M(String str) {
            if (str.indexOf("VolumeControl") == -1) {
                return null;
            }
            if (this.rk == null) {
                this.rk = new m(this.rf);
            }
            return this.rk;
        }

        public void a(PlayerListener playerListener) {
            Log.d(LOG_TAG, "Add a PlayerListener.");
            if (!this.rg.contains(playerListener)) {
                this.rg.add(playerListener);
            }
        }

        public void aC(int i) {
            Log.d(LOG_TAG, "setLoopCount " + i + ".");
            this.rh = i;
        }

        public void b(PlayerListener playerListener) {
            this.rg.remove(playerListener);
        }

        public long c(long j) {
            Log.d(LOG_TAG, "setMediaTime " + j + ".");
            int duration = this.rf.pc.getDuration();
            if (((long) duration) < j) {
                j = (long) duration;
            }
            try {
                this.rf.pc.seekTo((int) j);
                return j;
            } catch (IllegalStateException e) {
                throw new MediaException();
            }
        }

        public Control[] cW() {
            if (this.rk == null) {
                this.rk = new m(this.rf);
            }
            return new Control[]{this.rk};
        }

        public void cX() {
            try {
                if (this.state == 100) {
                    Log.d(LOG_TAG, "realize.");
                    this.rf.pc.reset();
                    this.rf.pc.setDataSource(this.rf.oY);
                    this.state = 200;
                }
            } catch (Exception e) {
                Log.w(LOG_TAG, e);
                throw new MediaException();
            }
        }

        public void cY() {
            cX();
            if (this.state == 200) {
                Log.d(LOG_TAG, "prefetch.");
                try {
                    this.rf.pc.prepare();
                    this.state = 300;
                } catch (Exception e) {
                    b(PlayerListener.ERROR, e.getMessage());
                    throw new MediaException();
                }
            }
        }

        public void cZ() {
            this.state = 100;
            Log.d(LOG_TAG, "deallocate.");
        }

        public void close() {
            cZ();
            this.state = 0;
            this.rf.recycle();
            if (this.rf.oY != null) {
                File file = new File(this.rf.oY);
                if (!file.exists() || !file.delete()) {
                    Log.d(LOG_TAG, "Temp file not exist or could not delete.");
                } else {
                    Log.d(LOG_TAG, "Clean the temp file.");
                }
            }
            org.meteoroid.core.g.a(this.rf);
            b(PlayerListener.CLOSED, null);
            Log.d(LOG_TAG, "close.");
        }

        public long da() {
            return (long) (this.rf.pc.getCurrentPosition() * com.a.a.h.b.DEFAULT_MINIMAL_LOCATION_UPDATES);
        }

        public String getContentType() {
            return this.rf.type;
        }

        public long getDuration() {
            return (long) this.rf.pc.getDuration();
        }

        public int getState() {
            return this.state;
        }

        public void onCompletion(MediaPlayer mediaPlayer) {
            Log.d(LOG_TAG, "playedCount:" + this.ri + " loopCount:" + this.rh);
            if (mediaPlayer == this.rf.pc) {
                this.ri++;
                if (this.ri < this.rh) {
                    try {
                        this.state = 100;
                        start();
                    } catch (MediaException e) {
                        e.printStackTrace();
                    }
                } else if (this.rg.isEmpty()) {
                    try {
                        stop();
                    } catch (MediaException e2) {
                        e2.printStackTrace();
                    }
                } else {
                    b(PlayerListener.END_OF_MEDIA, null);
                }
            }
        }

        public void onPrepared(MediaPlayer mediaPlayer) {
            Log.d(LOG_TAG, "onPrepared:" + this.ri + " loopCount:" + this.rh);
        }

        public void setContentType(String str) {
            this.rf.type = str;
        }

        public void start() {
            cX();
            cY();
            if (this.state == 300) {
                try {
                    if (this.rj && !this.rf.pc.isPlaying()) {
                        this.rf.pc.prepare();
                        this.rj = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    this.state = 100;
                    cX();
                    cY();
                }
                try {
                    Log.d(LOG_TAG, "start.");
                    if (this.rh == -1) {
                        this.rf.pc.setLooping(true);
                    } else {
                        this.rf.pc.setLooping(false);
                    }
                    this.rf.pc.start();
                    this.rf.oZ = true;
                    this.state = 400;
                    b(PlayerListener.STARTED, null);
                } catch (Exception e2) {
                    b(PlayerListener.ERROR, e2.getMessage());
                    throw new MediaException();
                }
            }
        }

        public void stop() {
            if (this.state == 400) {
                Log.d(LOG_TAG, "stop.");
                try {
                    if (this.rf.pc.isPlaying()) {
                        this.rf.pc.stop();
                    }
                    this.rf.oZ = false;
                    this.ri = 0;
                    this.state = 300;
                    this.rj = true;
                    b(PlayerListener.STOPPED, null);
                } catch (Exception e) {
                    b(PlayerListener.ERROR, e.getMessage());
                    e.printStackTrace();
                    throw new MediaException();
                } catch (Throwable th) {
                    this.state = 300;
                    this.rj = true;
                    b(PlayerListener.STOPPED, null);
                    throw th;
                }
            }
        }
    }

    public static final class j implements com.a.a.q.c, h.a {
        public static final int MSG_GCF_SMS_CONNECTION_RECEIVE = 15391747;
        public static final int MSG_GCF_SMS_CONNECTION_SEND = 15391744;
        public static final int MSG_GCF_SMS_CONNECTION_SEND_COMPLETE = 15391745;
        public static final int MSG_GCF_SMS_CONNECTION_SEND_FAIL = 15391746;
        private static final int NOT_SENT = 0;
        private static final int SENT_FAIL = 2;
        private static final int SENT_OK = 1;
        private String address;
        private int rl = 0;
        private com.a.a.q.d rm;
        private com.a.a.q.b rn;

        public static final class a implements com.a.a.q.a {
            private byte[] bA;
            private String ro;

            public byte[] fe() {
                return this.bA;
            }

            public String getAddress() {
                return this.ro;
            }

            public Date getTimestamp() {
                return new Date();
            }

            public void setAddress(String str) {
                this.ro = str;
            }

            public void v(byte[] bArr) {
                this.bA = bArr;
            }
        }

        public static final class b implements com.a.a.q.h {
            private String address;
            private String jd;

            public void an(String str) {
                this.jd = str;
            }

            public String fo() {
                return this.jd;
            }

            public String getAddress() {
                return this.address;
            }

            public Date getTimestamp() {
                return new Date();
            }

            public void setAddress(String str) {
                this.address = str;
            }
        }

        public j(String str) {
            org.meteoroid.core.h.j(MSG_GCF_SMS_CONNECTION_SEND, "MSG_GCF_SMS_CONNECTION_SEND");
            org.meteoroid.core.h.j(MSG_GCF_SMS_CONNECTION_SEND_COMPLETE, "MSG_GCF_SMS_CONNECTION_SEND_COMPLETE");
            org.meteoroid.core.h.j(MSG_GCF_SMS_CONNECTION_SEND_FAIL, "MSG_GCF_SMS_CONNECTION_SEND_FAIL");
            org.meteoroid.core.h.j(MSG_GCF_SMS_CONNECTION_RECEIVE, "MSG_GCF_SMS_CONNECTION_RECEIVE");
            this.address = str;
            org.meteoroid.core.h.a(this);
        }

        public void a(com.a.a.q.b bVar) {
            org.meteoroid.core.h.a(this);
            org.meteoroid.core.h.b(org.meteoroid.core.h.c(MSG_GCF_SMS_CONNECTION_SEND, bVar));
            synchronized (this) {
                try {
                    this.rl = 0;
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            org.meteoroid.core.h.b(this);
            if (this.rl != 1) {
                throw new InterruptedIOException("Fail to send.");
            }
        }

        public void a(com.a.a.q.d dVar) {
            this.rm = dVar;
        }

        public boolean a(Message message) {
            if (message.what == 15391745) {
                this.rl = 1;
                synchronized (this) {
                    notifyAll();
                }
                return true;
            } else if (message.what == 15391746) {
                this.rl = 2;
                synchronized (this) {
                    notifyAll();
                }
                return true;
            } else if (message.what != 15391747) {
                return false;
            } else {
                this.rn = (com.a.a.q.b) message.obj;
                if (this.rm != null) {
                    this.rm.a(this);
                }
                synchronized (this) {
                    notifyAll();
                }
                return true;
            }
        }

        public com.a.a.q.b af(String str) {
            return j(str, null);
        }

        public int b(com.a.a.q.b bVar) {
            return 1;
        }

        public void close() {
            org.meteoroid.core.h.b(this);
        }

        public com.a.a.q.b ff() {
            synchronized (this) {
                try {
                    wait(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (this.rn != null) {
                return this.rn;
            }
            throw new InterruptedIOException();
        }

        public com.a.a.q.b j(String str, String str2) {
            com.a.a.q.b kVar;
            if (str.equals(com.a.a.q.c.TEXT_MESSAGE)) {
                kVar = new b();
            } else if (str.equals(com.a.a.q.c.BINARY_MESSAGE)) {
                kVar = new a();
            } else if (str.equals(com.a.a.q.c.MULTIPART_MESSAGE)) {
                kVar = new k();
            } else {
                throw new IllegalArgumentException(str);
            }
            if (str2 != null) {
                kVar.setAddress(str2);
            }
            if (this.address != null) {
                kVar.setAddress(this.address);
            }
            return kVar;
        }
    }

    public static final class k implements com.a.a.q.f {
        private ArrayList<com.a.a.q.e> rp = new ArrayList<>();
        private String rq;
        private ArrayList<String> rr = new ArrayList<>();
        private ArrayList<String> rs = new ArrayList<>();
        private ArrayList<String> rt = new ArrayList<>();
        private HashMap<String, String> ru = new HashMap<>();
        private String rv;
        private String rw;

        public boolean a(com.a.a.q.e eVar) {
            return this.rp.remove(eVar);
        }

        public String[] ag(String str) {
            if (str.equals("cc")) {
                return (String[]) this.rs.toArray();
            }
            if (str.equals("bcc")) {
                return (String[]) this.rt.toArray();
            }
            if (str.equals("to")) {
                return (String[]) this.rr.toArray();
            }
            throw new IllegalArgumentException();
        }

        public boolean ah(String str) {
            com.a.a.q.e eVar;
            Iterator<com.a.a.q.e> it = this.rp.iterator();
            while (true) {
                if (!it.hasNext()) {
                    eVar = null;
                    break;
                }
                eVar = it.next();
                if (eVar.fi().equals(str)) {
                    break;
                }
            }
            if (eVar != null) {
                return a(eVar);
            }
            return false;
        }

        public boolean ai(String str) {
            com.a.a.q.e eVar;
            Iterator<com.a.a.q.e> it = this.rp.iterator();
            while (true) {
                if (!it.hasNext()) {
                    eVar = null;
                    break;
                }
                eVar = it.next();
                if (eVar.fj().equals(str)) {
                    break;
                }
            }
            if (eVar != null) {
                return a(eVar);
            }
            return false;
        }

        public String aj(String str) {
            return this.ru.get(str);
        }

        public com.a.a.q.e ak(String str) {
            Iterator<com.a.a.q.e> it = this.rp.iterator();
            while (it.hasNext()) {
                com.a.a.q.e next = it.next();
                if (next.fi().equals(str)) {
                    return next;
                }
            }
            return null;
        }

        public void al(String str) {
            if (str.equals("cc")) {
                this.rs.clear();
            } else if (str.equals("bcc")) {
                this.rt.clear();
            } else if (str.equals("to")) {
                this.rr.clear();
            } else {
                throw new IllegalArgumentException();
            }
        }

        public void am(String str) {
            this.rv = str;
        }

        public void b(com.a.a.q.e eVar) {
            this.rp.add(eVar);
        }

        public com.a.a.q.e[] fl() {
            return (com.a.a.q.e[]) this.rp.toArray();
        }

        public String fm() {
            return this.rv;
        }

        public void fn() {
            this.rs.clear();
            this.rt.clear();
            this.rr.clear();
        }

        public String getAddress() {
            return this.rq != null ? this.rq : this.rr.get(0);
        }

        public String getSubject() {
            return this.rw;
        }

        public Date getTimestamp() {
            return new Date();
        }

        public boolean k(String str, String str2) {
            if (str.equals("cc")) {
                this.rs.add(str2);
            } else if (str.equals("bcc")) {
                this.rt.add(str2);
            } else if (str.equals("to")) {
                this.rr.add(str2);
            } else {
                throw new IllegalArgumentException();
            }
            return true;
        }

        public boolean l(String str, String str2) {
            if (str.equals("cc")) {
                return this.rs.remove(str2);
            }
            if (str.equals("bcc")) {
                return this.rt.remove(str2);
            }
            if (str.equals("to")) {
                return this.rr.remove(str2);
            }
            throw new IllegalArgumentException();
        }

        public void setAddress(String str) {
            this.rq = str;
        }

        public void setHeader(String str, String str2) {
            this.ru.put(str, str2);
        }

        public void setSubject(String str) {
            this.rw = str;
        }
    }

    public static final class l implements com.a.a.c.p {
        private int mode;
        private Socket rx;

        public l(String str, int i, boolean z) {
            this.mode = i;
            try {
                if (!hh()) {
                    throw new IOException("No avaliable connection.");
                }
                URI uri = new URI(str);
                this.rx = new Socket(uri.getHost(), uri.getPort());
                Log.d("SocketConnection", "Connection " + uri.toString() + " established.");
            } catch (Exception e) {
                throw new IOException("URISyntaxException");
            }
        }

        private final boolean hh() {
            NetworkInfo activeNetworkInfo;
            try {
                ConnectivityManager hu = org.meteoroid.core.l.hu();
                return hu != null && (activeNetworkInfo = hu.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
            } catch (Exception e) {
                Log.v(PlayerListener.ERROR, e.toString());
            }
        }

        public void a(byte b, int i) {
            boolean z = false;
            switch (b) {
                case 0:
                    Socket socket = this.rx;
                    if (i != 0) {
                        z = true;
                    }
                    socket.setTcpNoDelay(z);
                    return;
                case 1:
                    Socket socket2 = this.rx;
                    if (i != 0) {
                        z = true;
                    }
                    socket2.setSoLinger(z, i);
                    return;
                case 2:
                    Socket socket3 = this.rx;
                    if (i != 0) {
                        z = true;
                    }
                    socket3.setKeepAlive(z);
                    return;
                case 3:
                    this.rx.setReceiveBufferSize(i);
                    return;
                case 4:
                    this.rx.setSendBufferSize(i);
                    return;
                default:
                    return;
            }
        }

        public String aD() {
            return this.rx.getLocalAddress().getHostAddress();
        }

        public DataInputStream ak() {
            return new DataInputStream(al());
        }

        public InputStream al() {
            if (this.mode == 2) {
                throw new IOException("Connection is write only.");
            }
            Log.d("SocketConnection", "Connection " + this.rx.getInetAddress().getHostAddress() + " openInputStream.");
            return this.rx.getInputStream();
        }

        public DataOutputStream am() {
            return new DataOutputStream(an());
        }

        public OutputStream an() {
            if (this.mode == 1) {
                throw new IOException("Connection is read only.");
            }
            Log.d("SocketConnection", "Connection " + this.rx.getInetAddress().getHostAddress() + " openOutputStream.");
            return this.rx.getOutputStream();
        }

        public void close() {
            this.rx.close();
            Log.d("SocketConnection", "Connection " + this.rx.getInetAddress().getHostAddress() + " close.");
            this.rx = null;
            System.gc();
        }

        public String getAddress() {
            return this.rx.getInetAddress().getHostAddress();
        }

        public int getLocalPort() {
            return this.rx.getLocalPort();
        }

        public int getPort() {
            return this.rx.getPort();
        }

        public int h(byte b) {
            switch (b) {
                case 0:
                    return this.rx.getTcpNoDelay() ? 1 : 0;
                case 1:
                    return this.rx.getSoLinger();
                case 2:
                    return !this.rx.getKeepAlive() ? 0 : 1;
                case 3:
                    return this.rx.getReceiveBufferSize();
                case 4:
                    return this.rx.getSendBufferSize();
                default:
                    throw new IllegalArgumentException("Invalid socket option");
            }
        }
    }

    public static final class m implements VolumeControl {
        private g.a rf;

        public m(g.a aVar) {
            this.rf = aVar;
        }

        public int aN(int i) {
            int i2 = 100;
            int i3 = i < 0 ? 0 : i;
            if (i3 <= 100) {
                i2 = i3;
            }
            this.rf.pa = i2;
            org.meteoroid.core.g.bJ(i2);
            Log.d("MIDPVolume", "Volume is set to " + i2);
            return i2;
        }

        public int getLevel() {
            return this.rf.pa;
        }

        public boolean isMuted() {
            return this.rf.pb;
        }

        public void p(boolean z) {
            this.rf.pb = z;
            org.meteoroid.core.g.v(z);
            Log.d("MIDPVolume", "Volume mute is set to " + z);
        }
    }

    private boolean R(int i2, int i3) {
        if (!this.pY) {
            return true;
        }
        if (this.qn == 0) {
            this.qn = getWidth() / 60;
        }
        if (this.qo == 0) {
            this.qo = getHeight() / 60;
        }
        return Math.abs(i2 - this.ql) >= this.qn || Math.abs(i3 - this.qm) >= this.qo;
    }

    private final void S(int i2, int i3) {
        int bV = bV(i3);
        if (bV == -1) {
            return;
        }
        if (i2 == 0) {
            this.qp = (1 << bV) | this.qp;
            return;
        }
        this.qp = (1 << bV) ^ this.qp;
    }

    /* access modifiers changed from: private */
    public final int bT(int i2) {
        switch (i2) {
            case 4:
                if (org.meteoroid.core.l.fS() == null || !org.meteoroid.core.l.fS().startsWith("R800")) {
                    return 0;
                }
                return pZ.get("NUM_5").intValue();
            case 7:
            case com.a.a.i.a.TITLE:
                return pZ.get("NUM_0").intValue();
            case 8:
            case com.a.a.e.c.KEY_NUM3:
                return pZ.get("NUM_1").intValue();
            case 9:
            case CharsToNameCanonicalizer.HASH_MULT:
                return pZ.get("NUM_2").intValue();
            case 10:
            case 46:
                return pZ.get("NUM_3").intValue();
            case 11:
            case 47:
                return pZ.get("NUM_4").intValue();
            case 12:
            case 32:
                return pZ.get("NUM_5").intValue();
            case 13:
            case 34:
                return pZ.get("NUM_6").intValue();
            case 14:
            case com.a.a.e.c.KEY_NUM6:
            case 99:
                return pZ.get("NUM_7").intValue();
            case 15:
            case com.a.a.e.c.KEY_NUM4:
                return pZ.get("NUM_8").intValue();
            case 16:
            case 31:
            case 100:
                return pZ.get("NUM_9").intValue();
            case 17:
            case 61:
            case 102:
                return pZ.get(pR[11]).intValue();
            case 18:
            case com.a.a.e.c.KEY_NUM7:
            case 103:
                return pZ.get(pR[12]).intValue();
            case 19:
                return pZ.get(pR[0]).intValue();
            case com.a.a.b.c.INT_16:
                return pZ.get(pR[1]).intValue();
            case 21:
                return pZ.get(pR[2]).intValue();
            case 22:
                return pZ.get(pR[3]).intValue();
            case 23:
                return pZ.get(pR[4]).intValue();
            case 44:
            case 86:
            case 88:
            case 93:
            case 108:
            case 186:
                return bU(2);
            case 45:
            case 85:
            case 87:
            case 92:
            case com.a.a.i.a.ORG:
            case 183:
                return bU(1);
            case com.a.a.p.c.TYPE:
                return pZ.get(pR[9]).intValue();
            default:
                return 0;
        }
    }

    private final int bV(int i2) {
        if (i2 == bU(1)) {
            return 17;
        }
        if (i2 == bU(2)) {
            return 18;
        }
        switch (R(i2)) {
            case 1:
                return 12;
            case 2:
                return 13;
            case 5:
                return 14;
            case 6:
                return 15;
            case 8:
                return 16;
            case com.a.a.e.c.KEY_POUND:
                return 11;
            case com.a.a.e.c.KEY_STAR:
                return 10;
            case 48:
                return 0;
            case com.a.a.e.c.KEY_NUM1:
                return 1;
            case 50:
                return 2;
            case com.a.a.e.c.KEY_NUM3:
                return 3;
            case com.a.a.e.c.KEY_NUM4:
                return 4;
            case com.a.a.e.c.KEY_NUM5:
                return 5;
            case com.a.a.e.c.KEY_NUM6:
                return 6;
            case com.a.a.e.c.KEY_NUM7:
                return 7;
            case 56:
                return 8;
            case com.a.a.e.c.KEY_NUM9:
                return 9;
            default:
                return -1;
        }
    }

    private final void ie() {
        final HashMap hashMap = new HashMap();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(org.meteoroid.core.l.getActivity().getAssets().open(com.a.a.s.e.bo(new String(com.a.a.s.a.decode("TUVUQS1JTkYvTUFOSUZFU1QuTUY=")))), com.umeng.common.b.e.f));
            Log.d(getName(), "Start to analyze META-INF/MANIFEST.MF.");
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                readLine.trim();
                int indexOf = readLine.indexOf(58);
                if (indexOf > -1) {
                    String trim = readLine.substring(0, indexOf).trim();
                    String trim2 = readLine.substring(indexOf + 1).trim();
                    if (Pattern.compile("\\bMIDlet-\\d").matcher(readLine).find()) {
                        int indexOf2 = trim2.indexOf(44);
                        int lastIndexOf = trim2.lastIndexOf(44);
                        if (indexOf2 <= -1 || lastIndexOf <= -1) {
                            Log.w(getName(), "The midlet " + trim + ":" + trim2 + " where p1=" + indexOf2 + " p2=" + lastIndexOf);
                        } else {
                            String trim3 = trim2.substring(0, indexOf2).trim();
                            trim2.substring(indexOf2 + 1, lastIndexOf).trim();
                            hashMap.put(trim3, trim2.substring(lastIndexOf + 1).trim());
                            Log.d(getName(), "The midlet " + readLine + " has added.");
                        }
                    } else {
                        org.meteoroid.core.a.z(trim, trim2);
                    }
                }
            }
            if (hashMap.isEmpty()) {
                Log.w(getName(), "No midlets found in MANIFEST.MF.");
                org.meteoroid.core.l.i(org.meteoroid.core.l.getString(R.string.no_midlet_found), 1);
            } else if (hashMap.size() == 1) {
                org.meteoroid.core.a.aK((String) hashMap.values().toArray()[0]);
            } else {
                final String[] strArr = new String[hashMap.keySet().size()];
                hashMap.keySet().toArray(strArr);
                final AlertDialog.Builder builder = new AlertDialog.Builder(org.meteoroid.core.l.getActivity());
                builder.setItems(strArr, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        org.meteoroid.core.a.aK((String) hashMap.get(strArr[i]));
                    }
                });
                builder.setCancelable(false);
                org.meteoroid.core.l.getHandler().post(new Runnable() {
                    public void run() {
                        builder.create().show();
                    }
                });
            }
        } catch (IOException e2) {
            Log.w(getName(), "MANIFEST.MF may not exist or invalid.");
            org.meteoroid.core.l.i(org.meteoroid.core.l.getString(R.string.no_midlet_found), 1);
        }
    }

    public final int R(int i2) {
        if (pZ.containsKey(pR[0]) && i2 == pZ.get(pR[0]).intValue()) {
            return 1;
        }
        if (pZ.containsKey(pR[1]) && i2 == pZ.get(pR[1]).intValue()) {
            return 6;
        }
        if (pZ.containsKey(pR[2]) && i2 == pZ.get(pR[2]).intValue()) {
            return 2;
        }
        if (pZ.containsKey(pR[3]) && i2 == pZ.get(pR[3]).intValue()) {
            return 5;
        }
        if (pZ.containsKey(pR[4]) && i2 == pZ.get(pR[4]).intValue()) {
            return 8;
        }
        if (pZ.containsKey(pR[5]) && i2 == pZ.get(pR[5]).intValue()) {
            return 9;
        }
        if (pZ.containsKey(pR[6]) && i2 == pZ.get(pR[6]).intValue()) {
            return 10;
        }
        if (pZ.containsKey(pR[7]) && i2 == pZ.get(pR[7]).intValue()) {
            return 11;
        }
        if (pZ.containsKey(pR[12]) && i2 == pZ.get(pR[12]).intValue()) {
            return 35;
        }
        if (pZ.containsKey(pR[11]) && i2 == pZ.get(pR[11]).intValue()) {
            return 42;
        }
        if (pZ.containsKey("NUM_0") && i2 == pZ.get("NUM_0").intValue()) {
            return 48;
        }
        if (pZ.containsKey("NUM_1") && i2 == pZ.get("NUM_1").intValue()) {
            return 49;
        }
        if (pZ.containsKey("NUM_2") && i2 == pZ.get("NUM_2").intValue()) {
            return 50;
        }
        if (pZ.containsKey("NUM_3") && i2 == pZ.get("NUM_3").intValue()) {
            return 51;
        }
        if (pZ.containsKey("NUM_4") && i2 == pZ.get("NUM_4").intValue()) {
            return 52;
        }
        if (pZ.containsKey("NUM_5") && i2 == pZ.get("NUM_5").intValue()) {
            return 53;
        }
        if (pZ.containsKey("NUM_6") && i2 == pZ.get("NUM_6").intValue()) {
            return 54;
        }
        if (pZ.containsKey("NUM_7") && i2 == pZ.get("NUM_7").intValue()) {
            return 55;
        }
        if (!pZ.containsKey("NUM_8") || i2 != pZ.get("NUM_8").intValue()) {
            return (!pZ.containsKey("NUM_9") || i2 != pZ.get("NUM_9").intValue()) ? 0 : 57;
        }
        return 56;
    }

    public final int S(int i2) {
        return (i2 < 0 || i2 > 8) ? i2 : i2 == 1 ? pZ.get(pR[0]).intValue() : i2 == 6 ? pZ.get(pR[1]).intValue() : i2 == 2 ? pZ.get(pR[2]).intValue() : i2 == 5 ? pZ.get(pR[3]).intValue() : i2 == 8 ? pZ.get(pR[4]).intValue() : i2 == 9 ? pZ.get(pR[5]).intValue() : i2 == 10 ? pZ.get(pR[6]).intValue() : i2 == 11 ? pZ.get(pR[7]).intValue() : i2 == 12 ? pZ.get(pR[8]).intValue() : i2;
    }

    public final String T(int i2) {
        if (i2 == pZ.get(pR[0]).intValue()) {
            return pR[0];
        }
        if (i2 == pZ.get(pR[1]).intValue()) {
            return pR[1];
        }
        if (i2 == pZ.get(pR[2]).intValue()) {
            return pR[2];
        }
        if (i2 == pZ.get(pR[3]).intValue()) {
            return pR[3];
        }
        if (i2 == pZ.get(pR[4]).intValue()) {
            return pR[4];
        }
        switch (i2) {
            case com.a.a.e.c.KEY_POUND:
                return "KEY_POUND";
            case 36:
            case 37:
            case 38:
            case 39:
            case com.a.a.b.c.BOOL:
            case 41:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            default:
                return "";
            case com.a.a.e.c.KEY_STAR:
                return "KEY_STAR";
            case 48:
                return "KEY_NUM0";
            case com.a.a.e.c.KEY_NUM1:
                return "KEY_NUM1";
            case 50:
                return "KEY_NUM2";
            case com.a.a.e.c.KEY_NUM3:
                return "KEY_NUM3";
            case com.a.a.e.c.KEY_NUM4:
                return "KEY_NUM4";
            case com.a.a.e.c.KEY_NUM5:
                return "KEY_NUM5";
            case com.a.a.e.c.KEY_NUM6:
                return "KEY_NUM6";
            case com.a.a.e.c.KEY_NUM7:
                return "KEY_NUM7";
            case 56:
                return "KEY_NUM8";
            case com.a.a.e.c.KEY_NUM9:
                return "KEY_NUM9";
        }
    }

    public final boolean T(int i2, int i3) {
        if (i3 == 0) {
            return false;
        }
        switch (i2) {
            case 0:
                if (!bW(i3)) {
                    com.a.a.e.j.a((MIDlet) null).keyPressed(i3);
                    Log.d(getName(), "Dispatch key event type: DOWN [" + i3 + "]");
                } else if (this.pT) {
                    com.a.a.e.j.a((MIDlet) null).aa(i3);
                    Log.d(getName(), "Dispatch key event type: REPEAT [" + i3 + "]");
                }
                S(i2, i3);
                return true;
            case 1:
                if (bW(i3)) {
                    com.a.a.e.j.a((MIDlet) null).b(i3);
                    Log.d(getName(), "Dispatch key event type: UP [" + i3 + "]");
                    S(i2, i3);
                }
                return true;
            default:
                Log.d(getName(), "Unkown key event type:" + i2 + "[" + i3 + "]");
                return false;
        }
    }

    public final Matrix a(int i2, Matrix matrix) {
        switch (i2) {
            case 0:
                break;
            case 1:
                matrix.preScale(-1.0f, 1.0f);
                matrix.preRotate(-180.0f);
                break;
            case 2:
                matrix.preScale(-1.0f, 1.0f);
                break;
            case 3:
                matrix.preRotate(180.0f);
                break;
            case 4:
                matrix.preScale(-1.0f, 1.0f);
                matrix.preRotate(-270.0f);
                break;
            case 5:
                matrix.preRotate(90.0f);
                break;
            case 6:
                matrix.preRotate(270.0f);
                break;
            case 7:
                matrix.preScale(-1.0f, 1.0f);
                matrix.preRotate(-90.0f);
                break;
            default:
                throw new IllegalArgumentException("Bad transform");
        }
        return matrix;
    }

    public com.a.a.c.b a(String str, int i2, boolean z) {
        Log.d(getName(), "Connector open " + str + ".");
        String trim = str.trim();
        if (trim.startsWith("http:")) {
            return new f(trim, i2, z);
        }
        if (trim.startsWith("sms:")) {
            return new j(trim);
        }
        if (trim.startsWith("socket:")) {
            return new l(trim, i2, z);
        }
        if (trim.startsWith("file:")) {
            return new b(trim, i2);
        }
        if (trim.indexOf("MACadd") != -1) {
            return new g(trim);
        }
        if (trim.startsWith("btl2cap://") || trim.startsWith("btspp://")) {
            return new h(trim);
        }
        throw new IOException("Unkown protocol:" + trim);
    }

    public void a(int i2, float f2, float f3, int i3) {
        if (this.pX || i3 == 0) {
            org.meteoroid.core.h.b(org.meteoroid.core.h.c(com.a.a.r.a.MSG_DEVICE_TOUCH_EVENT, new int[]{i2, (int) f2, (int) f3, i3}));
        }
    }

    public void a(AttributeSet attributeSet, String str) {
        setTouchable(attributeSet.getAttributeBooleanValue(str, "touchable", false));
        this.pU = isTouchable();
        C(attributeSet.getAttributeBooleanValue(str, "filter", true));
        String attributeValue = attributeSet.getAttributeValue(str, "origrect");
        if (attributeValue != null) {
            this.th = com.a.a.s.e.br(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            a(com.a.a.s.e.br(attributeValue2));
        }
    }

    public void a(com.a.a.r.c cVar) {
        if (!n.pL) {
            if (this.pW) {
                this.qa = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.qb = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.qf = b(this.qa);
                this.qg = b(this.qb);
                this.qe = this.qf;
                this.qc = this.qb;
            } else {
                this.qa = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.qb = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.qc = this.qb;
                this.qd = new Canvas(this.qc);
                this.qe = b(this.qa);
            }
        }
        iY();
        if (this.pS) {
            n.hW();
        }
        Log.d(getName(), "Buffer and graphics are ready .");
        com.a.a.e.j.a((MIDlet) null).d(getWidth(), getHeight());
    }

    public void a(Properties properties) {
        try {
            if (properties.containsKey("screen.width")) {
                this.width = Integer.parseInt(properties.getProperty("screen.width"));
                Log.d(getName(), "Set screen width " + this.width);
            }
            if (properties.containsKey("screen.height")) {
                this.height = Integer.parseInt(properties.getProperty("screen.height"));
                Log.d(getName(), "Set screen height " + this.height);
            }
            if (properties.containsKey("font.size.large")) {
                d.SIZE_LARGE = Integer.parseInt(properties.getProperty("font.size.large"));
                Log.d(getName(), "Set font.size.large " + d.SIZE_LARGE);
            }
            if (properties.containsKey("font.size.medium")) {
                d.SIZE_MEDIUM = Integer.parseInt(properties.getProperty("font.size.medium"));
                Log.d(getName(), "Set font.size.medium " + d.SIZE_MEDIUM);
            }
            if (properties.containsKey("font.size.small")) {
                d.SIZE_SMALL = Integer.parseInt(properties.getProperty("font.size.small"));
                Log.d(getName(), "Set font.size.small " + d.SIZE_SMALL);
            }
            for (int i2 = 0; i2 < 10; i2++) {
                if (properties.containsKey("key." + i2)) {
                    pZ.put("NUM_" + i2, Integer.valueOf(Integer.parseInt(properties.getProperty("key." + i2).trim())));
                }
            }
            for (int i3 = 0; i3 < pR.length; i3++) {
                if (properties.containsKey("key." + pR[i3])) {
                    pZ.put(pR[i3], Integer.valueOf(Integer.parseInt(properties.getProperty("key." + pR[i3]).trim())));
                }
            }
            for (Object next : properties.keySet()) {
                System.setProperty((String) next, properties.getProperty((String) next));
            }
        } catch (Exception e2) {
            Log.w(getName(), "Device property exception. " + e2);
        }
        this.pT = f("hasRepeatEvents", false);
        this.pV = f("hasPointerMotionEvents", true);
        this.pW = f("isDoubleBuffered", true);
        this.pS = f("isPositiveUpdate", false);
        this.pX = f("enableMultiTouch", false);
        this.pY = f("fixTouchPosition", false);
    }

    public boolean a(Message message) {
        if (message.what == 47872) {
            ie();
            return true;
        } else if (message.what == 44036) {
            org.meteoroid.core.h.bP(org.meteoroid.core.l.MSG_SYSTEM_EXIT);
            return true;
        } else if (message.what != -2023686143) {
            return e(message);
        } else {
            org.meteoroid.core.l.aZ((String) message.obj);
            org.meteoroid.core.h.e(MIDlet.MIDLET_PLATFORM_REQUEST_FINISH, Boolean.FALSE);
            return true;
        }
    }

    public boolean aT() {
        return this.pU;
    }

    public boolean aU() {
        return this.pV;
    }

    public boolean aV() {
        return this.pT;
    }

    public boolean aW() {
        return this.pW;
    }

    public e b(Bitmap bitmap) {
        return new e(new Canvas(bitmap));
    }

    public boolean b(KeyEvent keyEvent) {
        if (!this.qk) {
            org.meteoroid.core.l.hx().schedule(new a(), 500, 700);
            this.qk = true;
        }
        Log.d(getName(), "Native key event:" + keyEvent.getAction() + "[" + keyEvent.getKeyCode() + "]");
        if (keyEvent.getAction() == 1) {
            this.qj = keyEvent.getKeyCode();
            this.qi = -1;
        } else if (keyEvent.getAction() == 0) {
            if (this.qi == -1 && this.qj == -1) {
                T(0, bT(keyEvent.getKeyCode()));
            }
            this.qi = keyEvent.getKeyCode();
        }
        return false;
    }

    public int bU(int i2) {
        if (i2 == 1) {
            return pZ.get(pR[9]).intValue();
        }
        if (i2 == 2) {
            return pZ.get(pR[10]).intValue();
        }
        return 0;
    }

    public final boolean bW(int i2) {
        return ((1 << bV(i2)) & this.qp) != 0;
    }

    public final o bX() {
        return this.qe;
    }

    public String bd(String str) {
        Log.d(getName(), "Get device property:" + str);
        return qh.getProperty(str);
    }

    public int bf(String str) {
        return pZ.containsKey(str) ? pZ.get(str).intValue() : z.CONSTRAINT_MASK;
    }

    public boolean e(Message message) {
        switch (message.what) {
            case MSG_MIDP_COMMAND_EVENT /*44034*/:
                return f((com.a.a.e.f) message.obj);
            case com.a.a.r.a.MSG_DEVICE_TOUCH_EVENT:
                return f((int[]) message.obj);
            case VirtualKey.MSG_VIRTUAL_KEY_EVENT /*7833601*/:
                return T(((VirtualKey) message.obj).iL(), bf(((VirtualKey) message.obj).ja()));
            default:
                return false;
        }
    }

    public boolean f(com.a.a.e.f fVar) {
        com.a.a.e.j.a((MIDlet) null).a(fVar);
        return true;
    }

    public boolean f(String str, boolean z) {
        String bd = bd(str);
        return bd == null ? z : Boolean.parseBoolean(bd);
    }

    public boolean f(int[] iArr) {
        Log.d(getName(), "action[" + iArr[0] + "]" + "x:" + iArr[1] + " y:" + iArr[2]);
        switch (iArr[0]) {
            case 0:
                if (!this.pU) {
                    return true;
                }
                com.a.a.e.j.a((MIDlet) null).b(iArr[1], iArr[2]);
                this.ql = iArr[1];
                this.qm = iArr[2];
                return true;
            case 1:
                if (!this.pU) {
                    return true;
                }
                com.a.a.e.j.a((MIDlet) null).c(iArr[1], iArr[2]);
                return true;
            case 2:
                if (!this.pU || !this.pV || !R(iArr[1], iArr[2])) {
                    return true;
                }
                com.a.a.e.j.a((MIDlet) null).n(iArr[1], iArr[2]);
                return true;
            default:
                return false;
        }
    }

    public boolean gP() {
        return true;
    }

    public int getHeight() {
        return this.height == -1 ? iV().height() : this.height;
    }

    public String getName() {
        return "MIDPDevice";
    }

    public int getWidth() {
        return this.width == -1 ? iV().width() : this.width;
    }

    public void ib() {
        if (this.pW) {
            if (this.qc == this.qa) {
                this.qc = this.qb;
                this.qe = this.qf;
            } else {
                this.qc = this.qa;
                this.qe = this.qg;
            }
        } else if (this.qa != null && !this.qa.isRecycled()) {
            this.qd.drawBitmap(this.qa, 0.0f, 0.0f, (Paint) null);
        }
        if (this.qe != null) {
            this.qe.bW();
        }
        if (this.pS) {
            unlock();
        } else {
            n.hY();
        }
    }

    public final Bitmap ic() {
        return this.qc;
    }

    /* renamed from: if  reason: not valid java name */
    public final int m1if() {
        return this.qp;
    }

    public int j(String str, int i2) {
        String bd = bd(str);
        return bd == null ? i2 : Integer.parseInt(bd);
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void lock() {
        /*
            r2 = this;
            monitor-enter(r2)
            r0 = 100
            r2.wait(r0)     // Catch:{ InterruptedException -> 0x0008, all -> 0x000a }
        L_0x0006:
            monitor-exit(r2)
            return
        L_0x0008:
            r0 = move-exception
            goto L_0x0006
        L_0x000a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.device.MIDPDevice.lock():void");
    }

    public void onCreate() {
        org.meteoroid.core.h.j(com.a.a.r.a.MSG_DEVICE_TOUCH_EVENT, "MSG_DEVICE_TOUCH_EVENT");
        org.meteoroid.core.h.j(MSG_MIDP_COMMAND_EVENT, "MSG_MIDP_COMMAND_EVENT");
        org.meteoroid.core.h.j(MSG_MIDP_DISPLAY_CALL_SERIALLY, "MSG_MIDP_DISPLAY_CALL_SERIALLY");
        org.meteoroid.core.h.j(com.a.a.r.a.MSG_DEVICE_REQUEST_REFRESH, "MSG_DEVICE_REQUEST_REFRESH");
        org.meteoroid.core.h.a(this);
        qh.clear();
        try {
            qh.load(org.meteoroid.core.l.getActivity().getResources().openRawResource(com.a.a.s.e.bq(com.a.a.m.m.CONTEXT_TYPE_DEVICE)));
        } catch (IOException e2) {
            Log.e(getName(), "device.properties not exist or valid." + e2);
        }
        a(qh);
        org.meteoroid.core.f.a((f.a) this);
        org.meteoroid.core.f.a((f.C0004f) this);
        System.gc();
    }

    public void onDestroy() {
        if (this.qa != null) {
            this.qa.recycle();
        }
        this.qa = null;
        if (this.qb != null) {
            this.qb.recycle();
        }
        this.qb = null;
        if (this.qc != null) {
            this.qc.recycle();
        }
        this.qc = null;
        this.qe = null;
        if (this.qt) {
        }
    }

    public void onDraw(Canvas canvas) {
        if (this.qc != null && !this.qc.isRecycled()) {
            canvas.drawBitmap(this.qc, this.th, iV(), this.tg);
        }
        if (this.pS) {
            lock();
        }
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        Log.d(getName(), "Trackball event.");
        return false;
    }

    public void setVisible(boolean z) {
    }

    public synchronized void unlock() {
        notify();
    }
}
