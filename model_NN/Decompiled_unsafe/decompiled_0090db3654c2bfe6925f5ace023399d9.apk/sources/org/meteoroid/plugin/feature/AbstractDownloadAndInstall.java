package org.meteoroid.plugin.feature;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;
import com.a.a.s.e;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.meteoroid.core.h;
import org.meteoroid.core.k;
import org.meteoroid.core.l;
import org.meteoroid.core.m;
import org.meteoroid.plugin.feature.AbstractPaymentManager;

public abstract class AbstractDownloadAndInstall extends BroadcastReceiver implements h.a, AbstractPaymentManager.Payment {
    public static final int INSTALL_FAIL = 3;
    public static final int INSTALL_NOT_START = 0;
    public static final int INSTALL_RUNNING = 1;
    public static final int INSTALL_SUCCESS = 2;
    private static final String fileName = "target.apk";
    private a[] rM;
    private ArrayList<a> rN;
    private a rO;
    private boolean rP;
    private int rQ = 0;
    private boolean rR;
    public boolean rS;
    /* access modifiers changed from: private */
    public String rT;
    /* access modifiers changed from: private */
    public String rU;
    public com.a.a.s.b ry;

    public class a {
        public String packageName;
        public String rV;
        public boolean rW;
        public String rX;
        public String rY;

        public a() {
        }
    }

    private final class b extends AsyncTask<URL, Integer, Long> {
        private b() {
        }

        /* access modifiers changed from: protected */
        public Long doInBackground(URL... urlArr) {
            File file;
            try {
                if (!new File(AbstractDownloadAndInstall.this.rU).exists()) {
                    File file2 = new File(AbstractDownloadAndInstall.this.rT);
                    file2.mkdirs();
                    if (!l.hh()) {
                        return 0L;
                    }
                    HttpURLConnection httpURLConnection = (HttpURLConnection) urlArr[0].openConnection();
                    httpURLConnection.setRequestMethod(com.a.a.c.h.GET);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.connect();
                    File file3 = new File(file2, AbstractDownloadAndInstall.fileName);
                    try {
                        InputStream inputStream = httpURLConnection.getInputStream();
                        AbstractDownloadAndInstall.this.e(inputStream);
                        inputStream.close();
                    } catch (IOException e) {
                        file = file3;
                        Log.e(AbstractDownloadAndInstall.this.getName(), "Fail to download the file.");
                        if (file != null && file.exists()) {
                            file.delete();
                        }
                        return -1L;
                    }
                }
                return 100L;
            } catch (IOException e2) {
                file = null;
                Log.e(AbstractDownloadAndInstall.this.getName(), "Fail to download the file.");
                file.delete();
                return -1L;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Long l) {
            if (l.longValue() == 100) {
                m.hK();
                AbstractDownloadAndInstall.this.installApk();
            } else if (l.longValue() == -1) {
                m.hK();
                if (!AbstractDownloadAndInstall.this.rS) {
                    h.b(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
                }
            } else {
                Log.d(AbstractDownloadAndInstall.this.getName(), "On Post Execute result..." + l);
            }
        }
    }

    /* access modifiers changed from: private */
    public void e(InputStream inputStream) {
        FileOutputStream openFileOutput = l.getActivity().openFileOutput(fileName, 1);
        byte[] bArr = new byte[102400];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                openFileOutput.write(bArr, 0, read);
            } else {
                openFileOutput.flush();
                openFileOutput.close();
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void installApk() {
        if (this.rU == null) {
            this.rT = l.getActivity().getFilesDir().getAbsolutePath() + File.separator;
            this.rU = this.rT + fileName;
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(this.rU)), "application/vnd.android.package-archive");
        l.getActivity().startActivity(intent);
    }

    private void is() {
        InputStream aV = l.aV(e.bo(this.rO.rV));
        e(aV);
        aV.close();
        installApk();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.core.m.a(java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [?[OBJECT, ARRAY], java.lang.String, int, int]
     candidates:
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, boolean, boolean):void */
    public void a(a aVar) {
        if (aVar.rW) {
            l.i("不能重复安装同一个应用程序", 0);
            if (!this.rS) {
                h.b(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
                return;
            }
            return;
        }
        this.rO = aVar;
        this.rQ = 1;
        bZ(this.rQ);
        h.b(h.c(l.MSG_SYSTEM_LOG_EVENT, new String[]{"StartInstallApp", l.fz() + "=" + aVar.packageName}));
        if (aVar.rV.startsWith("market:") || aVar.rV.startsWith("http:") || aVar.rV.startsWith("https:")) {
            l.ba(aVar.rV);
        } else if (aVar.rV.startsWith("download:")) {
            String str = "http:" + aVar.rV.substring("download:".length());
            m.a((String) null, "正在下载", false, true);
            try {
                new b().execute(new URL(str));
            } catch (MalformedURLException e) {
                m.hK();
                b(e);
            }
        } else {
            try {
                is();
            } catch (Exception e2) {
                b(e2);
            }
        }
    }

    public boolean a(Message message) {
        if (message.what == 61697) {
            ((AbstractPaymentManager) message.obj).a(this);
        }
        if (message.what != 40961 || this.rQ != 1) {
            return false;
        }
        b((Exception) null);
        return false;
    }

    public void b(Exception exc) {
        this.rQ = 3;
        bZ(this.rQ);
        if (exc != null) {
            Log.w(getName(), "Invalid package:" + this.rO.packageName + exc);
        }
        if (!this.rS) {
            h.b(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
        }
    }

    public void bY(int i) {
        this.rQ = i;
    }

    public abstract void bZ(int i);

    public void be(String str) {
        this.rN = new ArrayList<>();
        this.rT = l.getActivity().getFilesDir().getAbsolutePath() + File.separator;
        this.rU = this.rT + fileName;
        this.ry = new com.a.a.s.b(str);
        String bm = this.ry.bm("DOWNLOAD");
        if (bm != null) {
            String[] split = bm.split("\\;");
            this.rM = new a[split.length];
            for (int i = 0; i < split.length; i++) {
                this.rM[i] = new a();
                this.rM[i].rV = split[i];
                Log.d(getName(), "app[" + i + "] target is " + this.rM[i].rV);
            }
        } else {
            Log.e(getName(), "No app target url !!!!!!!");
        }
        String bm2 = this.ry.bm("PACKAGE");
        if (bm2 != null) {
            String[] split2 = bm2.split("\\;");
            for (int i2 = 0; i2 < split2.length; i2++) {
                this.rM[i2].packageName = split2[i2];
                this.rM[i2].rW = k.bQ(0).getSharedPreferences().getBoolean(this.rM[i2].packageName, false);
                Log.d(getName(), "app[" + i2 + "] packageName is " + this.rM[i2].packageName + " and install state is " + this.rM[i2].rW);
            }
        }
        String bm3 = this.ry.bm("IMAGE");
        if (bm3 != null) {
            String[] split3 = bm3.split("\\;");
            for (int i3 = 0; i3 < split3.length; i3++) {
                this.rM[i3].rX = split3[i3];
                Log.d(getName(), "app[" + i3 + "] image is " + this.rM[i3].rX);
            }
        }
        String bm4 = this.ry.bm("THUMB");
        if (bm4 != null) {
            String[] split4 = bm4.split("\\;");
            for (int i4 = 0; i4 < split4.length; i4++) {
                this.rM[i4].rY = split4[i4];
                Log.d(getName(), "app[" + i4 + "] thumb is " + this.rM[i4].rY);
            }
        }
        String bm5 = this.ry.bm("FORCELAUNCH");
        if (bm5 != null) {
            this.rP = Boolean.parseBoolean(bm5);
        }
        String bm6 = this.ry.bm("SKIPCHECK");
        if (bm6 != null) {
            this.rR = Boolean.parseBoolean(bm6);
        }
        for (int i5 = 0; i5 < this.rM.length; i5++) {
            if (!this.rM[i5].rW) {
                this.rN.add(this.rM[i5]);
            }
        }
        h.a(this);
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme(com.umeng.common.a.c);
        l.getActivity().registerReceiver(this, intentFilter);
    }

    public List<a> iq() {
        ir();
        return this.rN;
    }

    public void ir() {
        if (this.rR) {
            Log.d(getName(), "Skip Check App is enable !!!");
            return;
        }
        for (int i = 0; i < this.rM.length; i++) {
            if (l.as(this.rM[i].packageName)) {
                this.rN.remove(this.rM[i]);
            }
        }
        Log.d(getName(), "Available apps are " + this.rN.size());
    }

    public int it() {
        return this.rQ;
    }

    public void onDestroy() {
        l.getActivity().unregisterReceiver(this);
    }

    public void onReceive(Context context, Intent intent) {
        Log.d(getName(), "The package has installed." + intent.getDataString());
        if (this.rO != null && this.rO.packageName != null && intent.getDataString().indexOf(this.rO.packageName) != -1) {
            this.rQ = 2;
            bZ(this.rQ);
            k.bQ(0).getEditor().putBoolean(this.rO.packageName, true).commit();
            this.rO.rW = true;
            if (iq().isEmpty()) {
                h.b(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_NO_MORE, this));
            }
            h.b(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_SUCCESS, this));
            h.b(h.c(l.MSG_SYSTEM_LOG_EVENT, new String[]{"InstallAppSuccess", this.rO.packageName + "=" + l.fz()}));
            if (this.rP) {
                l.at(this.rO.packageName);
            }
        }
    }
}
