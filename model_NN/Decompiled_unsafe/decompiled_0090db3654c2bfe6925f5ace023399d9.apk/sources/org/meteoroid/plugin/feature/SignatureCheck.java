package org.meteoroid.plugin.feature;

import android.content.DialogInterface;
import android.os.Message;
import android.util.Log;
import com.a.a.r.b;
import com.a.a.s.a;
import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.core.m;

public class SignatureCheck implements b, h.a {
    public static final int MSG_SIGNATURE_ERROR = 1717986918;
    private boolean sr = false;
    private boolean ss = false;
    private boolean st = true;
    private boolean su = true;
    private String sv;

    public boolean a(Message message) {
        if (message.what == 47872) {
            if (!this.sr) {
                m.a("警告", "您正在使用破解版产品，该产品可能会侵害您的利益，请打客服电话010-68948836举报。", "退出", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        l.hf();
                    }
                });
                Log.d(getName(), "Invalid signature:" + this.sv);
            }
            if (!this.ss) {
                h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"SignatureCheckError", "应用名称", l.fz(), "sign", this.sv});
                return true;
            } else if (this.st) {
                h.bP(MSG_SIGNATURE_ERROR);
                return true;
            }
        } else if (message.what == 1717986918 && this.su) {
            l.hf();
            return true;
        }
        return false;
    }

    public void be(String str) {
        String bm = new com.a.a.s.b(str).bm("SIGN");
        try {
            this.sv = a.encodeBytes(MessageDigest.getInstance("SHA-1").digest(l.getActivity().getPackageManager().getPackageInfo(l.getActivity().getPackageName(), 64).signatures[0].toByteArray()));
            String[] split = bm.split(";");
            for (String equals : split) {
                if (equals.equals(this.sv)) {
                    this.sr = true;
                    this.ss = true;
                    this.st = false;
                    this.su = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        h.a(this);
    }

    public String getName() {
        return "SignatureCheck";
    }

    public void onDestroy() {
    }

    public void y(byte[] bArr) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            X509Certificate x509Certificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(bArr));
            int version = x509Certificate.getVersion();
            String obj = x509Certificate.getPublicKey().toString();
            String bigInteger = x509Certificate.getSerialNumber().toString();
            String format = simpleDateFormat.format(x509Certificate.getNotBefore());
            String format2 = simpleDateFormat.format(x509Certificate.getNotAfter());
            String name = x509Certificate.getIssuerDN().getName();
            Log.d(getName(), "证书版本 = " + version + "\r\n公钥 = " + obj + "\r\n序列号 = " + bigInteger + "\r\n 有效期 = " + format + "\r\n失效日期 = " + format2 + "\r\n作者 = " + name + "\r\n算法名称" + x509Certificate.getSigAlgName());
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }
}
