package org.meteoroid.plugin.feature;

import android.os.Message;
import com.a.a.r.b;
import org.meteoroid.core.h;
import org.meteoroid.core.i;

public abstract class AbstractOptionMenuItemFeature implements b, h.a, i.c {
    private String label = "Unknown";
    private com.a.a.s.b ry;

    public void E(String str) {
        this.label = str;
    }

    public boolean a(Message message) {
        if (message.what != 47872) {
            return false;
        }
        i.a(this);
        h.b(this);
        return false;
    }

    public void be(String str) {
        this.ry = new com.a.a.s.b(str);
        String bm = this.ry.bm("LABEL");
        if (bm != null) {
            this.label = bm;
        }
        h.a(this);
    }

    public String bf() {
        return this.label;
    }

    public int getId() {
        return 143858403;
    }

    public String getName() {
        return bf();
    }

    public String getValue(String str) {
        return this.ry.bm(str);
    }
}
