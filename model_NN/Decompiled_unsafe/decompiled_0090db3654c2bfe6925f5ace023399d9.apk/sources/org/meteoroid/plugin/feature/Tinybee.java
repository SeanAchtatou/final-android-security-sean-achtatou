package org.meteoroid.plugin.feature;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Message;
import android.util.Log;
import com.a.a.r.b;
import java.util.HashMap;
import java.util.Map;
import me.gall.sgp.sdk.entity.app.StructuredData;
import me.gall.sgp.sdk.service.BossService;
import me.gall.tinybee.Logger;
import me.gall.tinybee.LoggerManager;
import org.meteoroid.core.h;
import org.meteoroid.core.k;
import org.meteoroid.core.l;

public class Tinybee implements b, Logger.OnlineParamCallback, h.a {
    private static final long DISABLE = 0;
    private String appId;
    private String channelId = "";
    private String channelName = "";
    private Logger sA;
    /* access modifiers changed from: private */
    public Logger sB;
    private long sC = 0;
    private String sx;
    private String sy;
    private String sz;

    private void iK() {
        new Thread() {
            public void run() {
                String hD;
                StringBuffer stringBuffer = new StringBuffer();
                try {
                    hD = l.ho().getDeviceId();
                } catch (Exception e) {
                    Exception exc = e;
                    hD = l.hD();
                    exc.printStackTrace();
                }
                stringBuffer.append(hD + "=");
                stringBuffer.append(l.gd() + "=");
                stringBuffer.append(l.fS() + "=");
                for (ApplicationInfo next : l.getActivity().getPackageManager().getInstalledApplications(128)) {
                    if (!next.packageName.startsWith("com.google") && !next.packageName.startsWith("com.android")) {
                        stringBuffer.append(next.packageName + BossService.ID_SEPARATOR);
                    }
                }
                HashMap hashMap = new HashMap();
                hashMap.put(StructuredData.TYPE_OF_VALUE, stringBuffer.toString());
                Tinybee.this.sB.send("CollectAppInfo", hashMap);
            }
        }.start();
    }

    public boolean a(Message message) {
        int i = 1;
        if (message.what == 40960) {
            if (this.sB != null) {
                this.sB.onPause((Context) message.obj);
            }
            if (this.sA != null) {
                this.sA.onPause((Context) message.obj);
            }
        } else if (message.what == 40961) {
            if (this.sB != null) {
                this.sB.onResume((Context) message.obj);
            }
            if (this.sA != null) {
                this.sA.onResume((Context) message.obj);
            }
        } else if (message.what == 47886) {
            if (message.obj != null) {
                if (message.obj instanceof String) {
                    this.sx = (String) message.obj;
                    this.sA = LoggerManager.getLogger(l.getActivity(), this.sx);
                } else if (message.obj instanceof String[]) {
                    String[] strArr = (String[]) message.obj;
                    if (strArr.length == 3) {
                        this.sx = strArr[0];
                        this.sy = strArr[1];
                        this.sz = strArr[2];
                    }
                    this.sA = LoggerManager.getLogger(l.getActivity(), this.sx, this.sy, this.sz);
                }
            }
        } else if (message.what == 47887) {
            if (message.obj instanceof String[]) {
                String[] strArr2 = (String[]) message.obj;
                if (strArr2.length == 1) {
                    this.sB.send(strArr2[0]);
                } else if (strArr2.length == 2) {
                    HashMap hashMap = new HashMap();
                    hashMap.put(StructuredData.TYPE_OF_VALUE, strArr2[1]);
                    this.sB.send(strArr2[0], hashMap);
                } else if (strArr2.length > 2) {
                    HashMap hashMap2 = new HashMap();
                    while (i < strArr2.length) {
                        if (i + 1 < strArr2.length) {
                            hashMap2.put(strArr2[i], strArr2[i + 1]);
                        }
                        i += 2;
                    }
                    this.sB.send(strArr2[0], hashMap2);
                }
            }
        } else if (message.what == 47902 && (message.obj instanceof String[]) && this.sA != null) {
            String[] strArr3 = (String[]) message.obj;
            if (strArr3.length == 1) {
                this.sA.send(strArr3[0]);
            } else if (strArr3.length == 2) {
                HashMap hashMap3 = new HashMap();
                hashMap3.put(StructuredData.TYPE_OF_VALUE, strArr3[1]);
                this.sA.send(strArr3[0], hashMap3);
            } else if (strArr3.length > 2) {
                HashMap hashMap4 = new HashMap();
                while (i < strArr3.length) {
                    if (i + 1 < strArr3.length) {
                        hashMap4.put(strArr3[i], strArr3[i + 1]);
                    }
                    i += 2;
                }
                this.sA.send(strArr3[0], hashMap4);
            }
        }
        return false;
    }

    public void be(String str) {
        boolean z = false;
        com.a.a.s.b bVar = new com.a.a.s.b(str);
        String bm = bVar.bm("COLLECTINFO");
        if (bm != null) {
            this.sC = Long.parseLong(bm) * 1000 * 60 * 60 * 24;
        }
        if (this.sC != 0) {
            long currentTimeMillis = System.currentTimeMillis();
            SharedPreferences sharedPreferences = k.bQ(0).getSharedPreferences();
            long j = sharedPreferences.getLong("LastDay", 0);
            if (j == 0 || currentTimeMillis - j >= this.sC) {
                sharedPreferences.edit().putLong("LastDay", currentTimeMillis).commit();
                iK();
                Log.d(getName(), "Time to collect.");
            }
        }
        String bm2 = bVar.bm("APP_ID");
        if (bm2 != null) {
            this.appId = bm2;
        }
        String bm3 = bVar.bm("CHANNEL_ID");
        if (bm3 != null) {
            this.channelId = bm3;
        }
        String bm4 = bVar.bm("CHANNEL_NAME");
        if (bm4 != null) {
            this.channelName = bm4;
        }
        String bm5 = bVar.bm("TEST");
        if (bm5 != null) {
            z = Boolean.parseBoolean(bm5);
        }
        this.sB = LoggerManager.getLogger(l.getActivity(), this.appId, this.channelId, this.channelName, z);
        this.sB.setOnlineParamCallback(this);
        h.a(this);
    }

    public String getName() {
        return getClass().getSimpleName();
    }

    public void onDestroy() {
        if (this.sB != null) {
            this.sB.finish();
        }
        if (this.sA != null) {
            this.sA.finish();
        }
    }

    public void requestComplete(Map<String, String> map) {
        Log.d(getName(), "requestComplete");
        if (map != null && !map.isEmpty()) {
            Log.d(getName(), "requestComplete online param size=" + map.size());
            h.d(l.MSG_SYSTEM_ONLINE_PARAM, map);
        }
    }

    public void requestError() {
        Log.d(getName(), "requestError");
    }
}
