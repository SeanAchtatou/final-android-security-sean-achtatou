package org.meteoroid.plugin.feature;

import android.content.DialogInterface;
import android.os.Message;
import android.util.Log;
import com.a.a.h.b;
import com.a.a.s.d;
import java.util.TimerTask;
import org.meteoroid.core.a;
import org.meteoroid.core.h;
import org.meteoroid.core.k;
import org.meteoroid.core.l;
import org.meteoroid.plugin.feature.AbstractPaymentManager;

public abstract class AbstractTrialActivation extends AbstractPaymentManager {
    public static final int LAUNCH_LIMIT = 1;
    public static final int SYSTEM_MSG_TRAILACTIVATION = -1742057216;
    public static final int TIME_LIMIT = 0;
    private int mode = 0;
    private boolean rz;
    private int sj = 60000;
    private int sk = 0;
    private int sl;
    private boolean sm;
    private boolean sn;
    private int so;
    private boolean sp = false;

    private void iH() {
        l.hx().schedule(new TimerTask() {
            public void run() {
                k.bQ(0).getEditor().putBoolean("trial", true).commit();
                if (!AbstractTrialActivation.this.iG()) {
                    h.bP(AbstractTrialActivation.SYSTEM_MSG_TRAILACTIVATION);
                }
            }
        }, (long) this.sj);
    }

    public void A(boolean z) {
        k.bQ(0).getEditor().putBoolean("actived", z).commit();
    }

    public boolean a(Message message) {
        if (this.rz) {
            return super.a(message);
        }
        if (message.what == 47872 && !this.sp) {
            this.sp = true;
            Log.d(getName(), "IsTrialEnded is " + iF());
            if (iF()) {
                Log.d(getName(), "IgnoreBlockAtStart is " + this.so);
                if (this.so != 0) {
                    this.sj = this.so;
                    if (this.mode == 0) {
                        try {
                            iH();
                            Log.d(getName(), "startActivationCounter...in " + this.sj);
                            return false;
                        } catch (Exception e) {
                            Log.w(getName(), e);
                            return false;
                        }
                    } else if (this.mode != 1) {
                        return false;
                    } else {
                        Log.w(getName(), "IgnoreBlockAtStart would cause activation based on launch time not work.");
                        l.hf();
                        return false;
                    }
                } else {
                    Log.d(getName(), "HasActived is " + iG());
                    if (!iG()) {
                        h.bP(SYSTEM_MSG_TRAILACTIVATION);
                        return true;
                    }
                }
            } else if (this.mode == 0) {
                try {
                    iH();
                    Log.d(getName(), "startActivationCounter...in " + this.sj);
                } catch (Exception e2) {
                    Log.w(getName(), e2);
                }
            }
        } else if (message.what == -1742057216) {
            iI();
            return true;
        } else if (message.what == 61700) {
            k.bQ(0).getEditor().putBoolean("nomore", true).commit();
            this.sn = true;
        } else if (message.what == 61698) {
            Log.d(getName(), "Payment:" + ((AbstractPaymentManager.Payment) message.obj).bf() + " is successed.");
            fJ();
            iJ();
            if (a.os == null) {
                h.b(h.c(l.MSG_SYSTEM_INIT_COMPLETE, null));
            }
            return true;
        } else if (message.what == 61699) {
            Log.d(getName(), "Payment:" + ((AbstractPaymentManager.Payment) message.obj).bf() + " is failed.");
            fJ();
            fail();
            return true;
        }
        return super.a(message);
    }

    public void be(String str) {
        boolean z = true;
        super.be(str);
        h.j(SYSTEM_MSG_TRAILACTIVATION, "SYSTEM_MSG_TRAILACTIVATION");
        String bh = bh("EXPIRED");
        if (bh != null) {
            this.sj = Integer.parseInt(bh) * b.DEFAULT_MINIMAL_LOCATION_UPDATES;
            this.mode = 0;
        }
        String bh2 = bh("LAUNCH");
        if (bh2 != null) {
            this.sk = Integer.parseInt(bh2);
            this.mode = 1;
        }
        String bh3 = bh("MULTI");
        if (bh3 != null) {
            this.sm = Boolean.parseBoolean(bh3);
        }
        String bh4 = bh("IGNORE");
        if (bh4 != null) {
            this.so = Integer.parseInt(bh4) * b.DEFAULT_MINIMAL_LOCATION_UPDATES;
        }
        if (this.mode == 1) {
            this.sl = k.bQ(0).getSharedPreferences().getInt("launch", 0);
            k.bQ(0).getEditor().putInt("launch", this.sl + 1).commit();
            Log.d(getName(), "CurrentLaunchTime is " + this.sl);
        }
        this.sn = k.bQ(0).getSharedPreferences().getBoolean("nomore", false);
        Log.d(getName(), "NoMoreActivation is " + this.sn);
        String bh5 = bh("START");
        if (bh5 == null) {
            return;
        }
        if (bh5.length() == 8) {
            if (d.w(Integer.parseInt(bh5.substring(0, 4)), Integer.parseInt(bh5.substring(4, 6)), Integer.parseInt(bh5.substring(6, 8)))) {
                z = false;
            }
            this.rz = z;
            return;
        }
        Log.w(getName(), "Not valid start date:" + bh5);
    }

    public abstract void cancel();

    public void fail() {
        A(false);
        l.resume();
    }

    public boolean iF() {
        return this.mode == 0 ? k.bQ(0).getSharedPreferences().getBoolean("trial", false) : this.mode != 1 || this.sl >= this.sk;
    }

    public boolean iG() {
        return this.sm ? this.sn : k.bQ(0).getSharedPreferences().getBoolean("actived", false);
    }

    public abstract void iI();

    public void iJ() {
        A(true);
        if (this.sm) {
            if (this.mode == 0) {
                k.bQ(0).getEditor().putBoolean("trial", false).commit();
            } else if (this.mode == 1) {
                k.bQ(0).getEditor().putInt("launch", 0).commit();
                this.sl = 0;
            }
        }
        l.resume();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i >= iu().size() || i < 0) {
            Log.w(getName(), "Selected an unknown payment." + i);
            fail();
            return;
        }
        ix();
        Log.d(getName(), "Paying with " + iu().get(i).bf());
        iu().get(i).iz();
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
