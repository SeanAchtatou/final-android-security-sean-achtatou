package org.meteoroid.plugin.feature;

import android.content.DialogInterface;
import android.os.Message;
import android.util.Log;
import com.a.a.r.b;
import g31fhsgzqxzl.gf.R;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.meteoroid.core.d;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.core.m;

public abstract class AbstractPaymentManager implements DialogInterface.OnClickListener, b, h.a {
    private com.a.a.s.b ry;
    private Set<String> sa;
    private boolean sb = false;
    private LinkedList<Payment> sc;

    public interface Payment extends b {
        public static final int MSG_PAYMENT_FAIL = 61699;
        public static final int MSG_PAYMENT_NO_MORE = 61700;
        public static final int MSG_PAYMENT_QUERY = 61697;
        public static final int MSG_PAYMENT_REQUEST = 61696;
        public static final int MSG_PAYMENT_SUCCESS = 61698;

        String bf();

        void iz();
    }

    public void a(Payment payment) {
        if (!this.sc.contains(payment)) {
            Log.d(getName(), payment.bf() + " has added into availiable payments.");
            this.sc.add(payment);
        }
    }

    public boolean a(Message message) {
        if (message.what != 47876) {
            return false;
        }
        iv();
        return false;
    }

    public void b(Message message, String str) {
        Iterator<Payment> it = this.sc.iterator();
        while (it.hasNext()) {
            if (it.next().getClass().getSimpleName().equalsIgnoreCase(str)) {
                h.a(message, str);
            }
        }
    }

    public void be(String str) {
        this.ry = new com.a.a.s.b(str);
        String bm = this.ry.bm("EXCLUDE");
        this.sa = new HashSet();
        if (bm != null) {
            this.sa.addAll(Arrays.asList(bm.split(";")));
        }
        this.sc = new LinkedList<>();
        h.j(Payment.MSG_PAYMENT_REQUEST, "MSG_PAYMENT_REQUEST");
        h.j(Payment.MSG_PAYMENT_QUERY, "MSG_PAYMENT_QUERY");
        h.j(Payment.MSG_PAYMENT_SUCCESS, "MSG_PAYMENT_SUCCESS");
        h.j(Payment.MSG_PAYMENT_FAIL, "MSG_PAYMENT_FAIL");
        h.a(this);
    }

    public String bh(String str) {
        return this.ry.bm(str);
    }

    public void bi(String str) {
        Iterator<Payment> it = this.sc.iterator();
        while (it.hasNext()) {
            Payment next = it.next();
            if (next.getClass().getSimpleName().equalsIgnoreCase(str)) {
                next.iz();
            }
        }
    }

    public void bj(String str) {
        if (this.sa == null) {
            this.sa = new HashSet();
        }
        this.sa.add(str);
    }

    public void fJ() {
        this.sb = false;
        m.hK();
    }

    public List<Payment> iu() {
        return this.sc;
    }

    public void iv() {
        boolean z;
        Iterator<b> it = d.gM().iterator();
        while (it.hasNext()) {
            b next = it.next();
            if (next instanceof Payment) {
                Iterator<String> it2 = this.sa.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        z = false;
                        break;
                    }
                    if (next.getClass().getSimpleName().equalsIgnoreCase(it2.next())) {
                        z = true;
                        break;
                    }
                }
                if (!z) {
                    a((Payment) next);
                } else {
                    Log.d(getName(), next.getClass().getSimpleName() + " is excluded.");
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String[], android.content.DialogInterface$OnClickListener, boolean):void
     arg types: [?[OBJECT, ARRAY], ?[OBJECT, ARRAY], java.lang.String[], org.meteoroid.plugin.feature.AbstractPaymentManager, int]
     candidates:
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, android.view.View, boolean, android.content.DialogInterface$OnCancelListener):void
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String[], android.content.DialogInterface$OnClickListener, boolean):void */
    public void iw() {
        Log.d(getName(), "There is " + this.sc.size() + " payment(s) in all.");
        if (this.sc.size() == 1) {
            ix();
            this.sc.get(0).iz();
        } else if (this.sc.size() > 1) {
            String[] strArr = new String[this.sc.size()];
            for (int i = 0; i < strArr.length; i++) {
                strArr[i] = this.sc.get(i).bf();
            }
            m.a((String) null, (String) null, strArr, (DialogInterface.OnClickListener) this, false);
        } else {
            l.i("没有可用的计费插件，计费插件初始化失败", 0);
            l.hf();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.core.m.a(java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [?[OBJECT, ARRAY], java.lang.String, int, int]
     candidates:
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, boolean, boolean):void */
    public void ix() {
        this.sb = true;
        m.a((String) null, l.getString(R.string.activating), false, true);
    }

    public boolean iy() {
        return this.sb;
    }

    public void onDestroy() {
        this.sc.clear();
        this.sc = null;
    }
}
