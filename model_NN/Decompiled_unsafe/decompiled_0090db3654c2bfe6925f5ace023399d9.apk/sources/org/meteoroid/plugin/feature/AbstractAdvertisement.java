package org.meteoroid.plugin.feature;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.a.a.r.b;
import com.a.a.s.d;
import java.util.Map;
import java.util.TimerTask;
import org.meteoroid.core.f;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.core.m;

public abstract class AbstractAdvertisement extends TimerTask implements b, f.d, h.a {
    private static final String ADVERTISEMENTS = "_AB_ADVERTISEMENTS";
    public static final int ALWAYS_SHOW_THE_ADVERTISMENT = 0;
    public static final int MSG_ADVERTISEMENT_CLICK = -2128412415;
    public static final int MSG_ADVERTISEMENT_SHOW = -2128412416;
    private static final String STATUS = "STATUS";
    private int count;
    private int duration = 20;
    private boolean rA = false;
    private String rB;
    private int rC = 60;
    /* access modifiers changed from: private */
    public RelativeLayout rD;
    private boolean rE;
    private boolean rF = false;
    private boolean rG = false;
    private boolean rH = false;
    private int rI = com.a.a.h.b.DEFAULT_MINIMAL_LOCATION_UPDATES;
    private boolean rJ = true;
    private int rK;
    private com.a.a.s.b ry;
    private boolean rz = false;

    public static boolean A(Context context) {
        return context.getSharedPreferences(ADVERTISEMENTS, 0).getBoolean(STATUS, false);
    }

    public static void a(Context context, boolean z) {
        context.getSharedPreferences(ADVERTISEMENTS, 0).edit().putBoolean(STATUS, z).commit();
    }

    public void U(int i, int i2) {
        a(im(), ii(), i, i2);
    }

    public void a(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (this.count >= this.rI && this.rJ && ij() && isShown()) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            motionEvent.setLocation((float) (im().getRight() - 20), (float) (im().getTop() + 20));
            if (action == 0 && im().dispatchTouchEvent(motionEvent)) {
                Log.d(getName(), "Fake click down.");
                this.rK++;
            } else if (this.rK != 0 && action == 1 && im().dispatchTouchEvent(motionEvent)) {
                Log.d(getName(), "Fake click up.");
                if (this.rK >= 2) {
                    this.count = 0;
                    this.rJ = false;
                    this.rK = 0;
                }
            }
            motionEvent.setLocation(x, y);
        }
        if (action == 0) {
            this.count++;
        }
    }

    public void a(View view, String str, int i, int i2) {
        a(view, str, new RelativeLayout.LayoutParams(i, i2));
    }

    public void a(View view, String str, RelativeLayout.LayoutParams layoutParams) {
        if (this.rD == null) {
            this.rD = new RelativeLayout(l.getActivity());
            this.rD.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        }
        if (!(str == null || str == null)) {
            if (str.equalsIgnoreCase("bottom")) {
                layoutParams.addRule(12);
            } else if (str.equalsIgnoreCase("top")) {
                layoutParams.addRule(10);
            }
        }
        this.rD.addView(view, layoutParams);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.plugin.feature.AbstractAdvertisement.a(android.content.Context, boolean):void
     arg types: [android.app.Activity, int]
     candidates:
      org.meteoroid.plugin.feature.AbstractAdvertisement.a(android.view.View, android.view.MotionEvent):void
      org.meteoroid.core.f.d.a(android.view.View, android.view.MotionEvent):void
      org.meteoroid.plugin.feature.AbstractAdvertisement.a(android.content.Context, boolean):void */
    public boolean a(Message message) {
        if (!this.rz && !this.rA) {
            if (message.what == 23041) {
                if (this.rD != null) {
                    l.getHandler().post(new Runnable() {
                        public void run() {
                            if (AbstractAdvertisement.this.rD.getParent() != null) {
                                m.hP().removeView(AbstractAdvertisement.this.rD);
                            }
                            m.hP().addView(AbstractAdvertisement.this.rD);
                            AbstractAdvertisement.this.l(0);
                        }
                    });
                }
            } else if (message.what == 47885) {
                Map map = (Map) message.obj;
                if (map.containsKey("AdSwitch")) {
                    this.rH = Boolean.parseBoolean((String) map.get("AdSwitch"));
                    Log.d(getName(), "AdSwitch:" + this.rH);
                    if (this.rH) {
                        in();
                    }
                }
            } else if (message.what == 9520139) {
                a((Context) l.getActivity(), true);
                in();
            }
        }
        return false;
    }

    public void bX(int i) {
        this.rC = i;
    }

    public void be(String str) {
        if (A(l.getActivity())) {
            Log.d(getName(), "Advertisement is disabled.");
            return;
        }
        this.ry = new com.a.a.s.b(str);
        String bh = bh("DURATION");
        if (bh != null) {
            this.duration = Integer.parseInt(bh);
        }
        String bh2 = bh("INTERVAL");
        if (bh2 != null) {
            this.rC = Integer.parseInt(bh2);
        }
        String bh3 = bh("TEST");
        if (bh3 != null) {
            this.rG = Boolean.parseBoolean(bh3);
        }
        String bh4 = bh("ALIGN");
        if (bh4 != null) {
            this.rB = bh4;
        }
        String bh5 = bh("FAKECLICK");
        if (bh5 != null) {
            this.rI = Integer.parseInt(bh5);
            f.a(this);
        }
        String bh6 = bh("PACKAGE");
        if (bh6 == null || !l.as(bh6)) {
            String bh7 = bh("START");
            if (bh7 != null) {
                if (bh7.length() == 8) {
                    this.rz = !d.w(Integer.parseInt(bh7.substring(0, 4)), Integer.parseInt(bh7.substring(4, 6)), Integer.parseInt(bh7.substring(6, 8)));
                } else {
                    Log.w(getName(), "Not valid start date:" + bh7);
                }
            }
            String bh8 = bh("END");
            if (bh8 != null) {
                if (bh8.length() == 8) {
                    this.rA = d.w(Integer.parseInt(bh8.substring(0, 4)), Integer.parseInt(bh8.substring(4, 6)), Integer.parseInt(bh8.substring(6, 8)));
                } else {
                    Log.w(getName(), "Not valid end date:" + bh8);
                }
            }
            h.a(this);
            return;
        }
        Log.e(getName(), "The depended package [" + bh6 + "] has already been installed. So disable the feature:" + getName());
    }

    public void bg(String str) {
        this.rB = str;
    }

    public String bh(String str) {
        return this.ry.bm(str);
    }

    public int getDuration() {
        return this.duration;
    }

    public int getInterval() {
        return this.rC;
    }

    public String getName() {
        return getClass().getSimpleName();
    }

    public void ih() {
        U(-1, -2);
    }

    public String ii() {
        return this.rB;
    }

    public abstract boolean ij();

    public void ik() {
        if (ij() && !this.rF && !this.rH) {
            im().setVisibility(0);
            this.rF = true;
            Log.d(getName(), "showSimpleAd[" + getName() + "]");
            h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"AdvertisementShow", l.fz() + "=" + getName()});
        }
    }

    public void il() {
        if (ij() && this.rF && this.duration != 0) {
            im().setVisibility(8);
            this.rF = false;
            Log.d(getName(), "hideSimpleAd[" + getName() + "]");
        }
    }

    public abstract View im();

    public void in() {
        Log.d(getName(), "stopSimpleAd[" + getName() + "]");
        cancel();
        Log.d(getName(), "Ad is showing. Force hide it.");
        l.getHandler().post(new Runnable() {
            public void run() {
                AbstractAdvertisement.this.im().setVisibility(8);
            }
        });
    }

    public boolean io() {
        return this.rH;
    }

    public void ip() {
        this.rJ = true;
    }

    public boolean isShown() {
        return this.rF;
    }

    public boolean isTestMode() {
        return this.rG;
    }

    public void l(long j) {
        if (!this.rE) {
            l.hx().schedule(this, j * 1000, (long) (this.rC * com.a.a.h.b.DEFAULT_MINIMAL_LOCATION_UPDATES));
            Log.d(getName(), "startSimpleAd[" + getName() + "]");
            this.rE = true;
        }
    }

    public void onDestroy() {
        in();
        h.b(this);
    }

    public void run() {
        if (!this.rz && !this.rA && !this.rH && !A(l.getActivity())) {
            Handler handler = l.getHandler();
            handler.post(new Runnable() {
                public void run() {
                    AbstractAdvertisement.this.ik();
                }
            });
            if (getDuration() != 0) {
                handler.postDelayed(new Runnable() {
                    public void run() {
                        AbstractAdvertisement.this.il();
                    }
                }, (long) (this.duration * com.a.a.h.b.DEFAULT_MINIMAL_LOCATION_UPDATES));
            }
        }
    }

    public void setDuration(int i) {
        this.duration = i;
    }

    public void z(boolean z) {
        this.rF = z;
    }
}
