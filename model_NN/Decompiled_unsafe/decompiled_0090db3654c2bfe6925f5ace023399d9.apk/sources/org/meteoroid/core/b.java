package org.meteoroid.core;

import android.app.Activity;
import android.util.Log;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public final class b {
    private static final String DATA_STORE_FILE = ".dat";
    private static final String LOG_TAG = "DataManager";

    @Deprecated
    public static List<String> aM(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String aN = aN(str);
        String[] fileList = l.getActivity().fileList();
        ArrayList arrayList = new ArrayList();
        if (fileList != null && fileList.length > 0) {
            for (int i = 0; i < fileList.length; i++) {
                if (fileList[i].endsWith(DATA_STORE_FILE) && (!(aN == null || fileList[i].indexOf(aN) == -1) || aN == null)) {
                    arrayList.add(fileList[i].substring(0, fileList[i].length() - DATA_STORE_FILE.length()));
                    Log.d(LOG_TAG, "Find data file:" + fileList[i]);
                }
            }
        }
        return arrayList;
    }

    private static String aN(String str) {
        return str.replace(File.separator, "_");
    }

    @Deprecated
    public static boolean aO(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String aN = aN(str);
        for (String equals : aM(aN)) {
            if (equals.equals(aN)) {
                Log.d(LOG_TAG, "Find data:" + aN);
                return true;
            }
        }
        Log.d(LOG_TAG, "Find no data:" + aN);
        return false;
    }

    @Deprecated
    public static OutputStream aP(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String aN = aN(str);
        Log.d(LOG_TAG, "Update data:" + aN);
        return l.getActivity().openFileOutput(aN + DATA_STORE_FILE, 1);
    }

    @Deprecated
    public static InputStream aQ(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String aN = aN(str);
        Log.d(LOG_TAG, "Read data:" + aN);
        return l.getActivity().openFileInput(aN + DATA_STORE_FILE);
    }

    @Deprecated
    public static boolean aR(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String aN = aN(str);
        Log.d(LOG_TAG, "Delete data:" + aN);
        return l.getActivity().deleteFile(aN + DATA_STORE_FILE);
    }

    protected static void d(Activity activity) {
    }

    protected static void onDestroy() {
    }
}
