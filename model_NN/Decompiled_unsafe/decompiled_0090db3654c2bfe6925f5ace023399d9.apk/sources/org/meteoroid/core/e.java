package org.meteoroid.core;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class e implements SurfaceHolder.Callback {
    public static final String LOG_TAG = "GraphicsManager";
    private static final Rect oA = new Rect(-1, -1, -1, -1);
    private static final ConcurrentLinkedQueue<a> oC = new ConcurrentLinkedQueue<>();
    private static double oD;
    public static SurfaceView ow;
    public static final e ox = new e();
    private static boolean oy = false;
    private static SurfaceHolder oz;
    private boolean oB;

    public interface a {
        boolean gP();

        void onDraw(Canvas canvas);
    }

    public static double a(double d, double d2) {
        return d2 > 0.0d ? d * d2 : d;
    }

    public static int a(int i, double d) {
        return (int) a((double) i, d);
    }

    public static Bitmap a(int i, int i2, boolean z, int i3) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        createBitmap.eraseColor(i3);
        return createBitmap;
    }

    public static final void a(a aVar) {
        oC.add(aVar);
    }

    public static Bitmap b(InputStream inputStream, int i) {
        if (inputStream == null) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = i;
        try {
            return BitmapFactory.decodeStream(inputStream, oA, options);
        } catch (Exception e) {
            Log.w(LOG_TAG, "createBitmap with InputStream error." + e);
            return null;
        }
    }

    public static Bitmap b(int[] iArr, int i, int i2, boolean z) {
        Log.d(LOG_TAG, "createRGBImage." + i + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + i2);
        if (iArr == null) {
            return null;
        }
        try {
            return Bitmap.createBitmap(iArr, i, i2, Bitmap.Config.ARGB_8888);
        } catch (Exception e) {
            Log.w(LOG_TAG, "createBitmap with rgb error." + e);
            return null;
        }
    }

    public static final void b(a aVar) {
        oC.remove(aVar);
    }

    public static Bitmap d(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        try {
            return BitmapFactory.decodeStream(inputStream);
        } catch (Exception e) {
            Log.w(LOG_TAG, "createBitmap with InputStream error." + e);
            throw new IOException();
        }
    }

    protected static void d(Activity activity) {
        e(activity);
    }

    private static void e(Activity activity) {
        char c = 2;
        ow = new SurfaceView(activity);
        ow.setId(268049792);
        ow.setFocusable(true);
        ow.setFocusableInTouchMode(true);
        ow.setLongClickable(true);
        ow.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        oz = ow.getHolder();
        f.a(ow);
        oz.addCallback(ox);
        try {
            oz.setType(1);
            c = 1;
        } catch (Exception e) {
            try {
                oz.setType(2);
            } catch (Exception e2) {
                oz.setType(0);
            }
        }
        oz.setFormat(1);
        switch (c) {
            case 1:
                Log.d(LOG_TAG, "Hardware surface");
                return;
            case 2:
                Log.d(LOG_TAG, "GPU surface");
                return;
            default:
                Log.d(LOG_TAG, "No hardware acceleration available");
                return;
        }
    }

    public static double f(Activity activity) {
        if (oD == -1.0d) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            oD = (double) displayMetrics.density;
        }
        return oD;
    }

    public static final void gN() {
        oC.clear();
    }

    protected static final void gO() {
        if (!ox.oB) {
            synchronized (ow) {
                try {
                    ow.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        if (oz == null) {
            oz = ow.getHolder();
        }
        synchronized (oz) {
            Canvas lockCanvas = oz.lockCanvas();
            if (lockCanvas != null) {
                lockCanvas.drawColor(-16777216);
                Iterator<a> it = oC.iterator();
                while (it.hasNext()) {
                    it.next().onDraw(lockCanvas);
                }
                oz.unlockCanvasAndPost(lockCanvas);
            }
        }
    }

    public static Bitmap h(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            return null;
        }
        if (i < 0 || i2 < 0 || bArr.length < 0) {
            throw new IllegalArgumentException();
        } else if (i + i2 > bArr.length) {
            throw new ArrayIndexOutOfBoundsException();
        } else {
            try {
                return BitmapFactory.decodeByteArray(bArr, i, i2);
            } catch (Exception e) {
                Log.w(LOG_TAG, "createBitmap with imageData error." + e);
                throw new IOException();
            }
        }
    }

    protected static void onDestroy() {
        ox.oB = false;
        oz.removeCallback(ox);
        f.b(ow);
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        ox.oB = true;
        synchronized (ow) {
            ow.notifyAll();
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        ox.oB = true;
        synchronized (ow) {
            ow.notifyAll();
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        ox.oB = false;
    }
}
