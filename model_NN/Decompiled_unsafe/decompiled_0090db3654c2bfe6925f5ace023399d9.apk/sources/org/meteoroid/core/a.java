package org.meteoroid.core;

import android.app.Activity;
import android.os.Message;
import android.util.Log;
import java.util.HashMap;
import org.meteoroid.core.h;

public final class a {
    public static final String LOG_TAG = "ApplicationManager";
    public static final int MSG_APPLICATION_LAUNCH = 47617;
    public static final int MSG_APPLICATION_NEED_PAUSE = 47623;
    public static final int MSG_APPLICATION_NEED_START = 47622;
    public static final int MSG_APPLICATION_PAUSE = 47619;
    public static final int MSG_APPLICATION_QUIT = 47621;
    public static final int MSG_APPLICATION_RESUME = 47620;
    public static final int MSG_APPLICATION_START = 47618;
    public static C0003a os;
    private static final HashMap<String, String> ot = new HashMap<>();

    /* renamed from: org.meteoroid.core.a$a  reason: collision with other inner class name */
    public interface C0003a {
        public static final int EXIT = 3;
        public static final int INIT = 0;
        public static final int PAUSE = 2;
        public static final int RUN = 1;

        void dC();

        int getState();

        void onDestroy();

        void onPause();

        void onResume();

        void onStart();
    }

    public static void a(C0003a aVar) {
        if (aVar == null) {
            Log.e(LOG_TAG, "Launch a null application.");
            return;
        }
        os = aVar;
        try {
            os.dC();
            Log.d(LOG_TAG, "The application has successfully launched.");
        } catch (Exception e) {
            Log.w(LOG_TAG, "The application failed to launch.");
            e.printStackTrace();
        }
    }

    public static void aK(String str) {
        if (str != null) {
            try {
                os = (C0003a) Class.forName(str).newInstance();
                h.b(h.c(MSG_APPLICATION_LAUNCH, os));
            } catch (Exception e) {
                e.printStackTrace();
                Log.w(LOG_TAG, e.toString());
            }
            if (os != null) {
                os.dC();
                Log.d(LOG_TAG, "The application has successfully launched.");
                return;
            }
            Log.w(LOG_TAG, "The application failed to launch.");
            return;
        }
        Log.w(LOG_TAG, "No available application could launch.");
    }

    public static String aL(String str) {
        Log.d(LOG_TAG, "Get Application Property:" + str);
        return ot.get(str);
    }

    public static boolean bv() {
        boolean z = true;
        if (os == null) {
            return false;
        }
        if (os.getState() != 1) {
            z = false;
        }
        return z;
    }

    protected static void d(Activity activity) {
        h.j(MSG_APPLICATION_LAUNCH, "MSG_APPLICATION_LAUNCH");
        h.j(MSG_APPLICATION_START, "MSG_APPLICATION_START");
        h.j(MSG_APPLICATION_PAUSE, "MSG_APPLICATION_PAUSE");
        h.j(MSG_APPLICATION_RESUME, "MSG_APPLICATION_RESUME");
        h.j(MSG_APPLICATION_QUIT, "MSG_APPLICATION_QUIT");
        h.j(MSG_APPLICATION_NEED_START, "MSG_APPLICATION_NEED_START");
        h.j(MSG_APPLICATION_NEED_PAUSE, "MSG_APPLICATION_NEED_PAUSE");
        h.a(new h.a() {
            public boolean a(Message message) {
                if (message.what == 47873) {
                    a.pause();
                    return false;
                } else if (message.what != 47874) {
                    return false;
                } else {
                    a.resume();
                    return false;
                }
            }
        });
    }

    protected static void onDestroy() {
        quit();
    }

    public static void pause() {
        if (os == null) {
            return;
        }
        if (os.getState() == 1) {
            os.onPause();
            h.b(h.c(MSG_APPLICATION_PAUSE, os));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + os.getState());
    }

    public static void quit() {
        if (os != null && os.getState() != 3) {
            os.onDestroy();
            h.b(h.c(MSG_APPLICATION_QUIT, os));
        }
    }

    public static void resume() {
        if (os == null) {
            return;
        }
        if (os.getState() == 2) {
            os.onResume();
            h.b(h.c(MSG_APPLICATION_RESUME, os));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + os.getState());
    }

    public static void start() {
        if (os == null) {
            return;
        }
        if (os.getState() == 0) {
            os.onStart();
            h.b(h.c(MSG_APPLICATION_START, os));
            Log.d(LOG_TAG, "The application has successfully started.");
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + os.getState());
    }

    public static void z(String str, String str2) {
        ot.put(str, str2);
    }
}
