package org.meteoroid.core;

import android.app.Activity;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingQueue;

public final class h implements Runnable {
    public static final String LOG_TAG = "MessageDispatchManager";
    public static final int MSG_ARG2_DONT_RECYCLE_ME = -13295;
    public static final int MSG_ARG_NONE = 0;
    private static final LinkedBlockingQueue<Message> pe = new LinkedBlockingQueue<>();
    private static final CopyOnWriteArraySet<a> pf = new CopyOnWriteArraySet<>();
    private static final h pg = new h();
    private static final SparseArray<String> ph = new SparseArray<>();
    private static final ArrayList<a> pj = new ArrayList<>();
    private boolean pi;

    public interface a {
        boolean a(Message message);
    }

    public static Message a(int i, Object obj, int i2, int i3) {
        Message obtain = Message.obtain();
        obtain.what = i;
        obtain.obj = obj;
        obtain.arg1 = i2;
        obtain.arg2 = i3;
        return obtain;
    }

    public static Message a(int i, Object obj, boolean z) {
        return a(i, obj, 0, z ? MSG_ARG2_DONT_RECYCLE_ME : 0);
    }

    public static synchronized void a(Message message, String str) {
        synchronized (h.class) {
            Iterator<a> it = pf.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                a next = it.next();
                if (next.getClass().getSimpleName().equalsIgnoreCase(str)) {
                    if (next.a(message)) {
                        Log.d(LOG_TAG, "The message [" + ph.get(message.what) + "] has been direct consumed by [" + next.getClass().getName() + "].");
                        break;
                    }
                    Log.d(LOG_TAG, "The message [" + ph.get(message.what) + "] has been skipped by [" + next.getClass().getName() + "].");
                }
            }
        }
    }

    public static void a(a aVar) {
        if (!pf.contains(aVar)) {
            pf.add(aVar);
        }
    }

    public static void b(Message message) {
        pe.add(message);
    }

    public static void b(a aVar) {
        pj.add(aVar);
    }

    public static void bP(int i) {
        d(i, null);
    }

    public static Message c(int i, Object obj) {
        return a(i, obj, 0, 0);
    }

    public static boolean c(Message message) {
        return pe.contains(message);
    }

    public static void d(int i, Object obj) {
        b(c(i, obj));
    }

    protected static void d(Activity activity) {
        new Thread(pg).start();
    }

    private static final void d(Message message) {
        boolean z;
        Iterator<a> it = pf.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            a next = it.next();
            if (next.a(message)) {
                Log.d(LOG_TAG, "The message [" + ph.get(message.what) + "] has been consumed by [" + next.getClass().getName() + "]. And " + pe.size() + " messages are waiting.");
                z = true;
                break;
            }
        }
        if (!z) {
            Log.d(LOG_TAG, "There is a message [" + ph.get(message.what, Integer.toHexString(message.what)) + "] which no consumer could deal with it. " + pe.size() + " messages left.");
        }
        if (message.arg2 != -13295) {
            message.recycle();
        }
    }

    public static synchronized void e(int i, Object obj) {
        synchronized (h.class) {
            d(c(i, obj));
        }
    }

    public static final void hb() {
        pe.clear();
    }

    public static void j(int i, String str) {
        ph.append(i, str);
    }

    protected static void onDestroy() {
        pg.pi = true;
        pe.clear();
    }

    public void run() {
        while (!this.pi) {
            while (true) {
                Message poll = pe.poll();
                if (poll == null) {
                    break;
                }
                d(poll);
            }
            if (!pj.isEmpty()) {
                Iterator<a> it = pj.iterator();
                while (it.hasNext()) {
                    pf.remove(it.next());
                }
                pj.clear();
            }
            Thread.yield();
        }
    }
}
