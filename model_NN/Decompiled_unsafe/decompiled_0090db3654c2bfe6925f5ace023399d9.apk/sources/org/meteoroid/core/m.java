package org.meteoroid.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import org.meteoroid.core.h;

public final class m {
    public static final String LOG_TAG = "ViewManager";
    public static final int MSG_VIEW_CHANGED = 23041;
    private static final int ROOT_VIEW_ID = 792998026;
    /* access modifiers changed from: private */
    public static ProgressDialog np;
    /* access modifiers changed from: private */
    public static FrameLayout pA;
    public static boolean pB = false;
    /* access modifiers changed from: private */
    public static AlertDialog pC;
    /* access modifiers changed from: private */
    public static a py;
    /* access modifiers changed from: private */
    public static a pz;

    public interface a {
        void aM();

        void aN();

        boolean aZ();

        View getView();
    }

    private static void a(final AlertDialog.Builder builder) {
        if (builder != null) {
            l.getHandler().post(new Runnable() {
                public void run() {
                    AlertDialog unused = m.pC = builder.create();
                    m.pC.show();
                }
            });
        }
    }

    public static void a(String str, String str2, View view, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, str2, view, null, null, null, null, null, null, null, null, z, onCancelListener);
    }

    private static void a(String str, String str2, View view, String[] strArr, DialogInterface.OnClickListener onClickListener, String str3, DialogInterface.OnClickListener onClickListener2, String str4, DialogInterface.OnClickListener onClickListener3, String str5, DialogInterface.OnClickListener onClickListener4, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(l.getActivity());
        if (str != null) {
            builder.setTitle(str);
        }
        if (str2 != null) {
            builder.setMessage(str2);
        }
        if (!(strArr == null || onClickListener == null)) {
            builder.setItems(strArr, onClickListener);
        }
        if (view != null) {
            builder.setView(view);
        }
        if (str3 != null) {
            builder.setPositiveButton(str3, onClickListener2);
        }
        if (str4 != null) {
            builder.setNegativeButton(str4, onClickListener3);
        }
        if (str5 != null) {
            builder.setNeutralButton(str5, onClickListener4);
        }
        builder.setCancelable(z);
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        a(builder);
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener) {
        a(str, str2, str3, onClickListener, null, null, null, null, false, null);
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2) {
        a(str, str2, str3, onClickListener, str4, onClickListener2, null, null, false, null);
    }

    @Deprecated
    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, String str5, DialogInterface.OnClickListener onClickListener3, boolean z) {
        a(str, str2, str3, onClickListener, str4, onClickListener2, str5, onClickListener3, z, null);
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, String str5, DialogInterface.OnClickListener onClickListener3, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, str2, null, null, null, str3, onClickListener, str4, onClickListener2, str5, onClickListener3, z, onCancelListener);
    }

    @Deprecated
    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, boolean z) {
        a(str, str2, str3, onClickListener, str4, onClickListener2, null, null, z, null);
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, str2, str3, onClickListener, str4, onClickListener2, null, null, z, onCancelListener);
    }

    public static void a(final String str, final String str2, final boolean z, final boolean z2) {
        l.getHandler().post(new Runnable() {
            public void run() {
                if (m.np == null) {
                    ProgressDialog unused = m.np = new ProgressDialog(l.getActivity());
                }
                if (str != null) {
                    m.np.setTitle(str);
                }
                if (str2 != null) {
                    m.np.setMessage(str2);
                }
                m.np.setCancelable(z);
                m.np.setIndeterminate(z2);
                if (!m.np.isShowing()) {
                    m.np.show();
                }
            }
        });
    }

    public static void a(String str, String str2, String[] strArr, DialogInterface.OnClickListener onClickListener, boolean z) {
        a(str, str2, null, strArr, onClickListener, null, null, null, null, null, null, z, null);
    }

    public static void a(final a aVar) {
        if (aVar == null || aVar == py || aVar.getView() == null) {
            Log.d(LOG_TAG, "Change view not effected.");
        } else {
            l.getHandler().post(new Runnable() {
                public void run() {
                    Log.d(m.LOG_TAG, "Change view start.");
                    if (m.pA.getParent() == null) {
                        l.getActivity().setContentView(m.pA);
                    }
                    if (!(m.py == null || m.py.getView() == null)) {
                        m.pA.removeView(m.py.getView());
                    }
                    if (m.py != null) {
                        m.py.aN();
                        a unused = m.pz = m.py;
                    }
                    m.pA.addView(aVar.getView());
                    if (aVar != null) {
                        aVar.aM();
                    }
                    a unused2 = m.py = aVar;
                    m.pA.invalidate();
                    m.py.getView().requestFocus();
                    h.d(m.MSG_VIEW_CHANGED, m.py);
                    Log.d(m.LOG_TAG, "Change view end.");
                }
            });
        }
    }

    protected static boolean aZ() {
        if (py != null) {
            return py.aZ() || pB;
        }
        return false;
    }

    public static void b(a aVar) {
        pz = py;
        py = aVar;
    }

    protected static void d(Activity activity) {
        h.j(MSG_VIEW_CHANGED, "MSG_VIEW_CHANGED");
        pA = new FrameLayout(activity);
        pA.setBackgroundColor(-16777216);
        pA.setId(ROOT_VIEW_ID);
        pA.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        pA.requestFocus();
        h.a(new h.a() {
            public boolean a(Message message) {
                if (message.what == 47873) {
                    if (m.py == null) {
                        return false;
                    }
                    m.py.aN();
                    return false;
                } else if (message.what != 47874 || m.py == null) {
                    return false;
                } else {
                    m.py.aM();
                    return false;
                }
            }
        });
    }

    public static void hK() {
        l.getHandler().post(new Runnable() {
            public void run() {
                if (m.np != null && m.np.isShowing()) {
                    m.np.dismiss();
                }
            }
        });
    }

    public static final void hL() {
        l.getHandler().post(new Runnable() {
            public void run() {
                l.getActivity().setContentView(m.pA);
                m.pA.invalidate();
                Log.d(m.LOG_TAG, "restore root view end.");
            }
        });
    }

    public static final void hM() {
        a(pz);
    }

    public static a hN() {
        return py;
    }

    public static void hO() {
        l.getHandler().post(new Runnable() {
            public void run() {
                if (m.pC != null && m.pC.isShowing()) {
                    m.pC.dismiss();
                }
            }
        });
    }

    public static FrameLayout hP() {
        return pA;
    }

    public static void j(long j) {
        l.getHandler().postDelayed(new Runnable() {
            public void run() {
                if (m.np != null && m.np.isShowing()) {
                    m.np.dismiss();
                }
            }
        }, j);
    }

    public static void k(long j) {
        l.getHandler().postDelayed(new Runnable() {
            public void run() {
                if (m.pC != null && m.pC.isShowing()) {
                    m.pC.dismiss();
                }
            }
        }, j);
    }

    protected static void onDestroy() {
        pz = null;
        py = null;
    }
}
