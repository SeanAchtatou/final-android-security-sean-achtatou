package org.meteoroid.core;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Message;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.meteoroid.core.h;

public final class g {
    public static final String LOG_TAG = "MediaManager";
    public static final String MEDIA_TEMP_FILE = "temp";
    public static final int VOLUME_CONTROL_BY_CLIP = -1;
    public static final int VOLUME_CONTROL_BY_DEVICE = -2;
    private static final g oM = new g();
    private static int oN = -1;
    private static String oO;
    private static int oP = 15;
    /* access modifiers changed from: private */
    public static int oQ;
    private static float oR;
    private static AudioManager oS;
    /* access modifiers changed from: private */
    public static final ConcurrentLinkedQueue<a> oT = new ConcurrentLinkedQueue<>();
    private static int oU = 0;
    private static int oV;
    private static int oW = 0;
    /* access modifiers changed from: private */
    public static int oX = 0;

    public class a {
        public String name;
        public String oY;
        public boolean oZ;
        public int pa;
        public boolean pb;
        public MediaPlayer pc;
        public String type;

        public a() {
        }

        public void recycle() {
            if (this.name != null) {
                g.oT.remove(this.name);
            }
            if (this.pc != null) {
                this.pc.release();
            }
            this.pc = null;
        }
    }

    public static a a(String str, InputStream inputStream, String str2) {
        if (inputStream == null) {
            return null;
        }
        oU++;
        g gVar = oM;
        gVar.getClass();
        a aVar = new a();
        String str3 = "";
        if (str2.indexOf("mid") != -1) {
            str3 = ".mid";
        } else if (str2.indexOf("mpeg") != -1) {
            str3 = ".mp3";
        } else if (str2.indexOf("amr") != -1) {
            str3 = ".amr";
        }
        aVar.name = str;
        aVar.type = str2;
        aVar.oY = oU + str3;
        FileOutputStream openFileOutput = l.getActivity().openFileOutput(aVar.oY, 1);
        byte[] bArr = new byte[256];
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                openFileOutput.flush();
                openFileOutput.close();
                aVar.pc = new MediaPlayer();
                aVar.oY = oO + aVar.oY;
                oT.add(aVar);
                Log.d(LOG_TAG, "Create a media clip " + str + " [" + str2 + "].");
                return aVar;
            }
            openFileOutput.write(bArr, 0, read);
        }
    }

    public static void a(a aVar) {
        if (oT.contains(aVar)) {
            oT.remove(aVar.name);
        }
    }

    public static void bI(int i) {
        oN = i;
    }

    public static void bJ(int i) {
        if (gS() == -1) {
            bL(i);
        } else if (gS() == -2) {
            if (i == 0) {
                oV = gV();
                bL(0);
            } else if (oV != 0) {
                bL(oV);
                oV = 0;
            }
            Log.d(LOG_TAG, "Failed to set volume because the globe volume mode is control by device." + oV);
        }
    }

    /* access modifiers changed from: private */
    public static void bK(int i) {
        oS.setStreamVolume(3, i, 16);
    }

    /* access modifiers changed from: private */
    public static void bL(int i) {
        Log.d(LOG_TAG, "Set device volume to " + i);
        oS.setStreamVolume(3, (int) (((float) i) * oR), 16);
    }

    protected static void d(Activity activity) {
        oO = activity.getFilesDir().getAbsolutePath() + File.separator;
        oS = (AudioManager) activity.getSystemService("audio");
        oP = oS.getStreamMaxVolume(3);
        Log.d(LOG_TAG, "Max volume is" + oP);
        oR = ((float) oP) / 100.0f;
        Log.d(LOG_TAG, "VOLUME_TRANS_RATIO is" + oR);
        oQ = gV();
        Log.d(LOG_TAG, "Init volume is" + oQ);
        h.a(new h.a() {
            public boolean a(Message message) {
                if (message.what == 47873) {
                    Iterator it = g.oT.iterator();
                    while (it.hasNext()) {
                        a aVar = (a) it.next();
                        try {
                            if (aVar.pc != null && aVar.pc.isPlaying() && aVar.oZ) {
                                aVar.pc.pause();
                                Log.d(g.LOG_TAG, "force to pause:" + aVar.name);
                            }
                        } catch (IllegalStateException e) {
                            Log.w(g.LOG_TAG, "error to pause:" + aVar.name);
                        }
                    }
                    int unused = g.oX = g.gU();
                    Log.d(g.LOG_TAG, "Pause volume is" + g.oX);
                    g.bL(g.oQ);
                    return false;
                } else if (message.what != 47874) {
                    return false;
                } else {
                    g.bK(g.oX);
                    Iterator it2 = g.oT.iterator();
                    while (it2.hasNext()) {
                        a aVar2 = (a) it2.next();
                        try {
                            if (aVar2.pc != null && aVar2.oZ && !aVar2.pc.isPlaying()) {
                                aVar2.pc.start();
                                Log.d(g.LOG_TAG, "force to resume:" + aVar2.name);
                            }
                        } catch (IllegalStateException e2) {
                            Log.w(g.LOG_TAG, "error to resume:" + aVar2.name);
                        }
                    }
                    return false;
                }
            }
        });
        if (gS() == -2) {
            bL(70);
        }
    }

    public static a e(String str, boolean z) {
        if (str == null) {
            return null;
        }
        String str2 = new String(str);
        if (!z) {
            if (str2.startsWith("/")) {
                str2 = str.substring(1);
            }
            str2 = "file:///android_asset/abresource" + str2;
        }
        g gVar = oM;
        gVar.getClass();
        a aVar = new a();
        String str3 = "";
        if (str.endsWith(".mid") || str.endsWith(".midi")) {
            str3 = "audio/midi";
        } else if (str.endsWith(".wav")) {
            str3 = "audio/x-wav";
        } else if (str.endsWith(".mpeg") || str.endsWith(".mp3")) {
            str3 = "audio/mpeg";
        } else if (str.endsWith(".amr")) {
            str3 = "audio/amr";
        }
        aVar.name = str;
        aVar.type = str3;
        aVar.oY = str2;
        aVar.pc = new MediaPlayer();
        Log.d(LOG_TAG, "Create a media clip " + str + " [" + str3 + "].");
        oT.add(aVar);
        return aVar;
    }

    public static int gS() {
        return oN;
    }

    public static a gT() {
        g gVar = oM;
        gVar.getClass();
        a aVar = new a();
        aVar.pc = new MediaPlayer();
        return aVar;
    }

    /* access modifiers changed from: private */
    public static int gU() {
        return oS.getStreamVolume(3);
    }

    public static int gV() {
        Log.d(LOG_TAG, "Current volume is:" + oS.getStreamVolume(3));
        return (int) (((float) oS.getStreamVolume(3)) / oR);
    }

    public static boolean gW() {
        return gV() == 0;
    }

    protected static void onDestroy() {
        oT.clear();
        bL(oQ);
    }

    public static void v(boolean z) {
        Log.d(LOG_TAG, "Mute works:" + z);
        if (z) {
            oW = gV();
            bJ(0);
        } else if (oW != 0) {
            bJ(oW);
        }
    }
}
