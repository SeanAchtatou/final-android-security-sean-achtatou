package org.meteoroid.core;

import android.app.Activity;
import android.util.Log;
import com.a.a.r.b;
import java.util.Iterator;
import java.util.LinkedHashSet;

public final class d {
    public static final String LOG_TAG = "FeatureManager";
    private static LinkedHashSet<b> ov = new LinkedHashSet<>();

    public static b A(String str, String str2) {
        Exception e;
        b bVar;
        try {
            bVar = (b) Class.forName(str).newInstance();
            try {
                bVar.be(str2);
                ov.add(bVar);
                Log.d(LOG_TAG, bVar.getName() + " has added.");
            } catch (Exception e2) {
                e = e2;
                Log.w(LOG_TAG, e);
                return bVar;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            bVar = null;
            e = exc;
            Log.w(LOG_TAG, e);
            return bVar;
        }
        return bVar;
    }

    public static boolean a(b bVar) {
        return ov.remove(bVar);
    }

    protected static void d(Activity activity) {
    }

    public static LinkedHashSet<b> gM() {
        return ov;
    }

    protected static void onDestroy() {
        if (!ov.isEmpty()) {
            Iterator<b> it = ov.iterator();
            while (it.hasNext()) {
                b next = it.next();
                next.onDestroy();
                Log.d(LOG_TAG, next.getName() + " has destroyed.");
            }
        }
    }
}
