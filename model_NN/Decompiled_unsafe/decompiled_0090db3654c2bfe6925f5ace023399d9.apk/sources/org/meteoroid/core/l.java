package org.meteoroid.core;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.os.Vibrator;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import com.a.a.i.k;
import com.a.a.m.m;
import com.a.a.s.e;
import g31fhsgzqxzl.gf.R;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import org.meteoroid.core.h;

public final class l {
    public static final String LOG_TAG = "SystemManager";
    public static final int MSG_SYSTEM_ACTIVITY_RESULT = 47880;
    public static final int MSG_SYSTEM_DEVICE_INIT_COMPLETE = 47878;
    public static final int MSG_SYSTEM_EXIT = 47875;
    public static final int MSG_SYSTEM_FEATURE_ADDED_COMPLETE = 47876;
    public static final int MSG_SYSTEM_FUNCTION_REQUEST = 47882;
    public static final int MSG_SYSTEM_GRAPHICS_INIT_COMPLETE = 47877;
    public static final int MSG_SYSTEM_INIT_COMPLETE = 47872;
    public static final int MSG_SYSTEM_LOG_APPENDER = 47886;
    public static final int MSG_SYSTEM_LOG_EVENT = 47887;
    public static final int MSG_SYSTEM_LOG_EVENT_BY_APPENDER = 47902;
    public static final int MSG_SYSTEM_NOTIFY_EXIT = 47881;
    public static final int MSG_SYSTEM_ONLINE_PARAM = 47885;
    public static final int MSG_SYSTEM_ON_PAUSE = 47873;
    public static final int MSG_SYSTEM_ON_RESUME = 47874;
    public static final int MSG_SYSTEM_VD_INIT_COMPLETE = 47879;
    private static Handler handler;
    /* access modifiers changed from: private */
    public static Activity pq;
    private static int pr = 0;
    public static boolean ps = false;
    /* access modifiers changed from: private */
    public static boolean pt = false;
    private static final Timer pu = new Timer();

    public static final InputStream aV(String str) {
        InputStream inputStream = null;
        try {
            inputStream = pq.getAssets().open(str);
        } catch (Exception e) {
            Log.w(LOG_TAG, "Can't load resource:" + str + " is not exist.");
            if (ps) {
                throw new IOException();
            }
        }
        Log.d(LOG_TAG, "Load assert " + str + (inputStream != null ? " success." : " failed."));
        return inputStream;
    }

    public static final byte[] aW(String str) {
        InputStream aV = aV(str);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = aV.read(bArr);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static String aX(String str) {
        try {
            return pq.getPackageManager().getApplicationInfo(pq.getPackageName(), 128).metaData.getString(str);
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(LOG_TAG, e.getMessage() + " unknown meta key or not exist:" + str);
            return null;
        }
    }

    public static int aY(String str) {
        String str2 = null;
        try {
            str2 = aX(str);
            return Integer.parseInt(str2);
        } catch (Exception e) {
            Log.w(LOG_TAG, e.getMessage() + " wrong trans:" + str + " with " + str2);
            return 0;
        }
    }

    public static boolean aZ(String str) {
        h.b(h.c(MSG_SYSTEM_FUNCTION_REQUEST, str));
        return true;
    }

    public static boolean as(String str) {
        try {
            return pq.getPackageManager().getApplicationInfo(str, 0) != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean at(String str) {
        PackageManager packageManager = pq.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(str, 0);
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setPackage(packageInfo.packageName);
            ResolveInfo next = packageManager.queryIntentActivities(intent, 0).iterator().next();
            if (next != null) {
                String str2 = next.activityInfo.name;
                Intent intent2 = new Intent("android.intent.action.MAIN");
                intent2.addCategory("android.intent.category.LAUNCHER");
                intent2.setComponent(new ComponentName(str, str2));
                pq.startActivity(intent2);
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void bR(int i) {
        ((Vibrator) pq.getSystemService("vibrator")).vibrate((long) i);
    }

    public static void bS(int i) {
        pq.setRequestedOrientation(i);
    }

    public static boolean ba(final String str) {
        if (str.startsWith("http://") || str.startsWith("market://")) {
            getHandler().post(new Runnable() {
                public void run() {
                    l.pq.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                }
            });
            return true;
        } else if (str.startsWith("tel:")) {
            getHandler().post(new Runnable() {
                public void run() {
                    l.pq.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(str)));
                }
            });
            return true;
        } else {
            Log.w(LOG_TAG, "Not supported " + str);
            return false;
        }
    }

    protected static void d(Activity activity) {
        pq = activity;
        h.j(MSG_SYSTEM_INIT_COMPLETE, "MSG_SYSTEM_INIT_COMPLETE");
        h.j(MSG_SYSTEM_ON_PAUSE, "MSG_SYSTEM_ON_PAUSE");
        h.j(MSG_SYSTEM_FEATURE_ADDED_COMPLETE, "MSG_SYSTEM_FEATURE_ADDED_COMPLETE");
        h.j(MSG_SYSTEM_GRAPHICS_INIT_COMPLETE, "MSG_SYSTEM_GRAPHICS_INIT_COMPLETE");
        h.j(MSG_SYSTEM_DEVICE_INIT_COMPLETE, "MSG_SYSTEM_DEVICE_INIT_COMPLETE");
        h.j(MSG_SYSTEM_VD_INIT_COMPLETE, "MSG_SYSTEM_VD_INIT_COMPLETE");
        h.j(MSG_SYSTEM_ON_PAUSE, "MSG_SYSTEM_ON_PAUSE");
        h.j(MSG_SYSTEM_ON_RESUME, "MSG_SYSTEM_ON_RESUME");
        h.j(MSG_SYSTEM_FUNCTION_REQUEST, "MSG_SYSTEM_FUNCTION_REQUEST");
        h.j(MSG_SYSTEM_EXIT, "MSG_SYSTEM_EXIT");
        h.j(MSG_SYSTEM_ACTIVITY_RESULT, "MSG_SYSTEM_ACTIVITY_RESULT");
        h.j(MSG_SYSTEM_LOG_EVENT, "MSG_SYSTEM_LOG_EVENT");
        h.j(MSG_SYSTEM_NOTIFY_EXIT, "MSG_SYSTEM_NOTIFY_EXIT");
        Properties properties = new Properties();
        try {
            properties.load(activity.getResources().openRawResource(e.bq("globe")));
            handler = new Handler();
            hv();
            if (properties.containsKey("ThrowIOExceptions")) {
                ps = Boolean.parseBoolean(properties.getProperty("ThrowIOExceptions"));
            }
            if (properties.containsKey("DontQuit")) {
                m.pB = Boolean.parseBoolean(properties.getProperty("DontQuit"));
            }
            if (!properties.containsKey("DisableWakeLock")) {
                activity.getWindow().setFlags(128, 128);
            }
            h.d(activity);
            e.d(activity);
            d.d(activity);
            if (properties.containsKey("feature")) {
                String[] split = properties.getProperty("feature").split("\\}");
                for (int i = 0; i < split.length; i++) {
                    int indexOf = split[i].indexOf("{");
                    if (indexOf != -1) {
                        d.A(split[i].trim().substring(0, indexOf), split[i].trim().substring(indexOf + 1));
                        Log.d(LOG_TAG, split[i] + " has been added.");
                    } else {
                        Log.w(LOG_TAG, "Failed to create feature:" + split[i]);
                    }
                }
            }
            h.bP(MSG_SYSTEM_FEATURE_ADDED_COMPLETE);
            a.d(activity);
            f.d(activity);
            if (properties.containsKey("VolumeMode")) {
                g.bI(Integer.parseInt(properties.getProperty("VolumeMode")));
            }
            g.d(activity);
            m.d(activity);
            i.d(activity);
            c.aS(properties.getProperty(m.CONTEXT_TYPE_DEVICE));
            h.bP(MSG_SYSTEM_DEVICE_INIT_COMPLETE);
            h.bP(MSG_SYSTEM_VD_INIT_COMPLETE);
            if (properties.containsKey("AdaptiveVirtualDevice")) {
                n.pK = Boolean.parseBoolean(properties.getProperty("AdaptiveVirtualDevice"));
            }
            n.bb(properties.getProperty("virtualdevice"));
            properties.clear();
            System.gc();
            h.a(new h.a() {
                public boolean a(Message message) {
                    if (message.what == 47875) {
                        l.gF();
                        return true;
                    } else if (message.what == 47881) {
                        if (l.pt) {
                            return true;
                        }
                        m.a(l.getString(R.string.alert), l.getString(R.string.exit_dialog), l.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                l.hf();
                                boolean unused = l.pt = false;
                            }
                        }, l.getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                l.resume();
                                boolean unused = l.pt = false;
                            }
                        }, true, new DialogInterface.OnCancelListener() {
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                                l.resume();
                                boolean unused = l.pt = false;
                            }
                        });
                        boolean unused = l.pt = true;
                        l.pause();
                        return true;
                    } else if (message.what == 47882) {
                        return l.ba((String) message.obj);
                    } else {
                        return false;
                    }
                }
            });
            h.b(h.c(MSG_SYSTEM_LOG_EVENT, new String[]{"Launch", fz()}));
        } catch (Exception e) {
            Log.e(LOG_TAG, "Load globe.properties error." + e);
        }
    }

    public static String fS() {
        return Build.MODEL;
    }

    public static int fV() {
        return pq.getWindowManager().getDefaultDisplay().getWidth();
    }

    public static int fW() {
        return pq.getWindowManager().getDefaultDisplay().getHeight();
    }

    public static String fY() {
        String str = null;
        if (pq.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = ho().getDeviceId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "imei=" + str);
        }
        return str;
    }

    public static String fz() {
        return getString(R.string.app_name);
    }

    protected static void gF() {
        h.hb();
        onDestroy();
        pq.finish();
        System.gc();
        System.exit(0);
        Process.killProcess(Process.myPid());
    }

    public static String gd() {
        return Build.MANUFACTURER;
    }

    public static Activity getActivity() {
        return pq;
    }

    public static Handler getHandler() {
        return handler;
    }

    public static String getMacAddress() {
        String str = null;
        if (pq.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == 0) {
            try {
                str = hp().getConnectionInfo().getMacAddress();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "mac=" + str);
        }
        return str;
    }

    public static String getString(int i) {
        return pq.getString(i);
    }

    public static String hA() {
        String hB = hB();
        if (hB == null || hB.trim().equals("")) {
            Log.d(LOG_TAG, "phoneNumber is empty.Try imei.");
            hB = fY();
        }
        if (hB == null || hB.trim().equals("")) {
            Log.d(LOG_TAG, "imei is empty. Try iccid.");
            hB = hC();
        }
        if (hB == null || hB.trim().equals("")) {
            Log.d(LOG_TAG, "iccid is empty.Try mac.");
            hB = getMacAddress();
        }
        if (hB == null || hB.trim().equals("")) {
            Log.d(LOG_TAG, "mac is empty.Try android_id.");
            hB = hD();
        }
        Log.d(LOG_TAG, "uniqueID is " + hB);
        return hB;
    }

    public static String hB() {
        String str = null;
        if (pq.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = ho().getLine1Number();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "phoneNumber=" + str);
        }
        return str;
    }

    public static String hC() {
        String str = null;
        if (pq.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = ho().getSimSerialNumber();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "iccid=" + str);
        }
        return str;
    }

    public static String hD() {
        String str;
        String string = Settings.Secure.getString(pq.getContentResolver(), "android_id");
        if (!"9774d56d682e549c".equals(string)) {
            try {
                Class<?> cls = Class.forName("android.os.SystemProperties");
                str = (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
            } catch (Exception e) {
                str = string;
            }
        } else {
            str = string;
        }
        Log.d(LOG_TAG, "androidId=" + str);
        return str;
    }

    public static Location hE() {
        if (pq.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            return ((LocationManager) pq.getSystemService(m.PROP_LOCATION)).getLastKnownLocation("gps");
        }
        if (pq.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            return ((LocationManager) pq.getSystemService(m.PROP_LOCATION)).getLastKnownLocation("network");
        }
        Log.e(LOG_TAG, "Get Location failed for permission not properly config.");
        return null;
    }

    public static void hF() {
        pq.startActivity(new Intent("android.settings.WIFI_SETTINGS"));
    }

    public static void hG() {
        pq.startActivity(new Intent("android.settings.WIRELESS_SETTINGS"));
    }

    public static int hH() {
        return R.drawable.icon;
    }

    public static void he() {
        pr = 0;
        h.b(h.c(MSG_SYSTEM_ON_RESUME, null));
    }

    public static void hf() {
        h.b(h.c(MSG_SYSTEM_EXIT, null));
    }

    public static String hg() {
        TelephonyManager ho = ho();
        if (pq.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
            Log.e(LOG_TAG, "Get CarrierIMSI failed for permission not properly config.");
            return null;
        } else if (ho.getSimState() == 5) {
            Log.d(LOG_TAG, "Sim state ready.");
            return ho.getSubscriberId();
        } else {
            throw new Exception("Sim card is not ready yet.");
        }
    }

    public static final boolean hh() {
        return hi() || hj();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (hu().getNetworkInfo(1).isConnected() == true) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final boolean hi() {
        /*
            r0 = 1
            android.app.Activity r1 = org.meteoroid.core.l.pq
            java.lang.String r2 = "android.permission.ACCESS_NETWORK_STATE"
            int r1 = r1.checkCallingOrSelfPermission(r2)
            if (r1 == 0) goto L_0x000c
        L_0x000b:
            return r0
        L_0x000c:
            r1 = 0
            android.net.ConnectivityManager r2 = hu()     // Catch:{ Exception -> 0x0035 }
            r3 = 1
            android.net.NetworkInfo r2 = r2.getNetworkInfo(r3)     // Catch:{ Exception -> 0x0035 }
            boolean r2 = r2.isConnected()     // Catch:{ Exception -> 0x0035 }
            if (r2 != r0) goto L_0x003b
        L_0x001c:
            java.lang.String r1 = "SystemManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Wifi state: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r1, r2)
            goto L_0x000b
        L_0x0035:
            r0 = move-exception
            java.lang.String r2 = "SystemManager"
            android.util.Log.w(r2, r0)
        L_0x003b:
            r0 = r1
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.core.l.hi():boolean");
    }

    public static final boolean hj() {
        boolean z = true;
        if (pq.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0) {
            try {
                if (!hu().getNetworkInfo(0).isConnectedOrConnecting()) {
                    if (ho().getDataState() != 2) {
                        z = false;
                    }
                }
            } catch (Exception e) {
                Log.w(LOG_TAG, e);
                z = false;
            }
            Log.d(LOG_TAG, "DataConnect state: " + z);
        }
        return z;
    }

    public static void hk() {
        h.bP(MSG_SYSTEM_NOTIFY_EXIT);
    }

    public static AudioManager hl() {
        return (AudioManager) pq.getSystemService("audio");
    }

    public static AudioManager hm() {
        return (AudioManager) pq.getSystemService("alarm");
    }

    public static ActivityManager hn() {
        return (ActivityManager) pq.getSystemService("activity");
    }

    public static TelephonyManager ho() {
        return (TelephonyManager) pq.getSystemService("phone");
    }

    public static WifiManager hp() {
        return (WifiManager) pq.getSystemService("wifi");
    }

    public static NotificationManager hq() {
        return (NotificationManager) pq.getSystemService("notification");
    }

    public static InputMethodManager hr() {
        return (InputMethodManager) pq.getSystemService("input_method");
    }

    public static void hs() {
        Intent launchIntentForPackage = pq.getBaseContext().getPackageManager().getLaunchIntentForPackage(pq.getBaseContext().getPackageName());
        launchIntentForPackage.addFlags(k.OCTOBER);
        pq.startActivity(launchIntentForPackage);
    }

    public static LocationManager ht() {
        return (LocationManager) pq.getSystemService(m.PROP_LOCATION);
    }

    public static ConnectivityManager hu() {
        return (ConnectivityManager) pq.getSystemService("connectivity");
    }

    public static void hv() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) pq.getSystemService("activity")).getRunningAppProcesses();
        int myPid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.pid != myPid && next.importance > 300) {
                Process.killProcess(next.pid);
                Log.d(LOG_TAG, "Kill background process:" + next.processName + " pid:" + next.pid);
            }
        }
    }

    public static int hw() {
        return pq.getResources().getConfiguration().orientation;
    }

    public static Timer hx() {
        return pu;
    }

    public static String hy() {
        if (pq.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            Log.d(LOG_TAG, "ACCESS_NETWORK_STATE permission denied.");
            return null;
        }
        try {
            return hu().getNetworkInfo(0).getExtraInfo();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int hz() {
        try {
            return Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (NumberFormatException e) {
            return 4;
        }
    }

    public static void i(final String str, final int i) {
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(l.pq, str, i).show();
            }
        });
    }

    protected static void onDestroy() {
        pu.cancel();
        pu.purge();
        a.onDestroy();
        i.onDestroy();
        e.onDestroy();
        f.onDestroy();
        g.onDestroy();
        m.onDestroy();
        c.onDestroy();
        n.onDestroy();
        d.onDestroy();
        h.onDestroy();
    }

    public static void pause() {
        pr++;
        if (pr == 1) {
            h.b(h.c(MSG_SYSTEM_ON_PAUSE, null));
        } else {
            Log.w(LOG_TAG, "The system has already paused." + pr);
        }
    }

    public static void resume() {
        pr--;
        if (pr == 0) {
            h.b(h.c(MSG_SYSTEM_ON_RESUME, null));
        } else {
            Log.w(LOG_TAG, "The system do not need resumed." + pr);
        }
        if (pr <= 0) {
            pr = 0;
        }
    }

    public static void w(boolean z) {
        if (pq.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == 0 && pq.checkCallingOrSelfPermission("android.permission.CHANGE_WIFI_STATE") == 0) {
            hp().setWifiEnabled(z);
        } else {
            Log.w(LOG_TAG, "Not enough permission in change wifi.");
        }
    }

    public static void x(boolean z) {
        Method declaredMethod;
        if ((pq.checkCallingOrSelfPermission("android.permission.MODIFY_PHONE_STATE") == 0 || pq.checkCallingOrSelfPermission("android.permission.CHANGE_NETWORK_STATE") == 0) && pq.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                if (Build.VERSION.SDK_INT >= 9) {
                    ConnectivityManager hu = hu();
                    Field declaredField = Class.forName(hu.getClass().getName()).getDeclaredField("mService");
                    declaredField.setAccessible(true);
                    Object obj = declaredField.get(hu);
                    Method declaredMethod2 = Class.forName(obj.getClass().getName()).getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
                    declaredMethod2.setAccessible(true);
                    declaredMethod2.invoke(obj, Boolean.valueOf(z));
                    return;
                }
                TelephonyManager ho = ho();
                if (ho.getDataState() == 2 && z) {
                    Log.d(LOG_TAG, "Already enable. No need to turn on data connection.");
                } else if (ho.getDataState() != 2 && !z) {
                    Log.d(LOG_TAG, "Already disable. No need to turn off data connection.");
                }
                Method declaredMethod3 = Class.forName(ho.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
                declaredMethod3.setAccessible(true);
                Object invoke = declaredMethod3.invoke(ho, new Object[0]);
                Class<?> cls = Class.forName(invoke.getClass().getName());
                if (!z) {
                    declaredMethod = cls.getDeclaredMethod("disableDataConnectivity", new Class[0]);
                    Log.d(LOG_TAG, "Ready to disable data connection state.");
                } else {
                    declaredMethod = cls.getDeclaredMethod("enableDataConnectivity", new Class[0]);
                    Log.d(LOG_TAG, "Ready to enable data connection state.");
                }
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(invoke, new Object[0]);
                Log.d(LOG_TAG, "Success change data connection state.");
            } catch (Exception e) {
                Log.w(LOG_TAG, "Error in change data connection:" + e);
            }
        } else {
            Log.w(LOG_TAG, "Not enough permission in change data connection.");
        }
    }
}
