package org.meteoroid.core;

import android.util.Log;
import com.a.a.r.a;

public final class c {
    private static final String LOG_TAG = "DeviceManager";
    public static a ou;

    protected static a aS(String str) {
        try {
            ou = (a) Class.forName(str).newInstance();
            ou.onCreate();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to create device. " + e);
            e.printStackTrace();
        }
        return ou;
    }

    protected static final void onDestroy() {
        if (ou != null) {
            ou.onDestroy();
        }
    }
}
