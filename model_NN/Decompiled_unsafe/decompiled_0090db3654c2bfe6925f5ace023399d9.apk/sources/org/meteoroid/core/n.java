package org.meteoroid.core;

import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import com.a.a.r.c;
import org.meteoroid.core.h;

public final class n {
    public static final String LOG_TAG = "VDManager";
    public static final int MSG_SYSTEM_VD_RELOAD = 6913;
    public static final int MSG_SYSTEM_VD_RELOAD_COMPLETE = 6914;
    private static int orientation;
    public static c pJ;
    public static boolean pK = false;
    public static boolean pL;
    private static int pM;
    private static int pN;
    /* access modifiers changed from: private */
    public static boolean pO;
    /* access modifiers changed from: private */
    public static boolean pP;
    private static final Thread pQ = new Thread() {
        public final void run() {
            while (n.pO) {
                n.hY();
                SystemClock.sleep(200);
            }
        }
    };

    protected static c bb(String str) {
        try {
            pJ = (c) Class.forName(str).newInstance();
            pJ.onCreate();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to create virtual device. " + e);
            e.printStackTrace();
        }
        h.j(MSG_SYSTEM_VD_RELOAD, "MSG_SYSTEM_VD_RELOAD");
        h.j(MSG_SYSTEM_VD_RELOAD_COMPLETE, "MSG_SYSTEM_VD_RELOAD_COMPLETE");
        h.a(new h.a() {
            public boolean a(Message message) {
                if (message.what == 40965 && n.pK && n.hV()) {
                    Log.d(n.LOG_TAG, "The vd need auto reload !");
                    l.pause();
                    h.bP(n.MSG_SYSTEM_VD_RELOAD);
                } else if (message.what == 6913) {
                    n.pL = true;
                    SystemClock.sleep(100);
                    while (n.pP) {
                        Thread.yield();
                    }
                    n.bc((String) message.obj);
                    h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"ReloadSkin", l.fz() + "=" + ((String) message.obj)});
                    h.bP(n.MSG_SYSTEM_VD_RELOAD_COMPLETE);
                    return true;
                } else if (message.what == 6914) {
                    SystemClock.sleep(100);
                    l.resume();
                    n.pL = false;
                }
                return false;
            }
        });
        hU();
        return pJ;
    }

    protected static void bc(String str) {
        pJ.bc(str);
        hU();
    }

    private static final void hU() {
        pM = l.fV();
        pN = l.fW();
        orientation = l.hw();
    }

    public static final boolean hV() {
        return (pM == l.fV() && pN == l.fW() && orientation == l.hw()) ? false : true;
    }

    public static void hW() {
        if (!pO && !pQ.isAlive()) {
            pO = true;
            pQ.start();
        }
    }

    public static void hX() {
        pO = false;
    }

    public static void hY() {
        if (!pL) {
            pP = true;
            e.gO();
            pP = false;
        }
    }

    protected static void onDestroy() {
        if (pJ != null) {
            pJ.onDestroy();
        }
    }
}
