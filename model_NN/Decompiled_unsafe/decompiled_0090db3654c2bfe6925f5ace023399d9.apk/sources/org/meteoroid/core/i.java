package org.meteoroid.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import g31fhsgzqxzl.gf.R;
import java.util.ArrayList;
import java.util.Iterator;
import org.meteoroid.core.h;

public class i {
    public static final int MSG_OPTIONMENU_ABOUT = 60080;
    public static final int OPTION_MENU_ITEM_ABOUT = 64160;
    public static final int OPTION_MENU_ITEM_EXIT = 64161;
    private static final ArrayList<c> pl = new ArrayList<>();
    private static int[] pm;

    private static final class a implements h.a, c {
        private a() {
        }

        public boolean a(Message message) {
            if (message.what != 60080) {
                return false;
            }
            l.getHandler().post(new Runnable() {
                public void run() {
                    a.this.bi();
                }
            });
            return true;
        }

        public String bf() {
            return l.getString(R.string.about);
        }

        public void bi() {
            AlertDialog.Builder builder = new AlertDialog.Builder(l.getActivity());
            builder.setTitle((int) R.string.about);
            builder.setMessage((int) R.string.about_dialog);
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    dialogInterface.dismiss();
                    l.resume();
                }
            });
            builder.setNegativeButton((int) R.string.close, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    l.resume();
                }
            });
            builder.create().show();
            l.pause();
        }

        public int getId() {
            return i.OPTION_MENU_ITEM_ABOUT;
        }
    }

    private static final class b implements c {
        private b() {
        }

        public String bf() {
            return l.getString(R.string.exit);
        }

        public void bi() {
            l.hk();
        }

        public int getId() {
            return i.OPTION_MENU_ITEM_EXIT;
        }
    }

    public interface c {
        String bf();

        void bi();

        int getId();
    }

    public static void a(MenuItem menuItem) {
        pl.get(menuItem.getItemId()).bi();
    }

    public static void a(c cVar) {
        if (!pl.contains(cVar)) {
            pl.add(0, cVar);
        }
    }

    public static void b(c cVar) {
        pl.remove(cVar);
    }

    protected static void d(Activity activity) {
        String string = l.getString(R.string.about_dialog);
        if (string != null && string.length() > 0) {
            a aVar = new a();
            a(aVar);
            h.a(aVar);
        }
        a(new b());
    }

    public static void e(int[] iArr) {
        pm = iArr;
    }

    public static boolean isEmpty() {
        return pl.isEmpty();
    }

    protected static void onDestroy() {
        pl.clear();
    }

    public static void onPrepareOptionsMenu(Menu menu) {
        boolean z;
        int i;
        menu.clear();
        Iterator<c> it = pl.iterator();
        int i2 = 0;
        int i3 = 0;
        while (it.hasNext()) {
            c next = it.next();
            if (pm != null) {
                z = false;
                for (int i4 : pm) {
                    if (i4 == next.getId()) {
                        z = true;
                    }
                }
            } else {
                z = false;
            }
            if (!z) {
                menu.add(131072, i3, i2, next.bf());
                i = i2 + 1;
            } else {
                i = i2;
            }
            i3++;
            i2 = i;
        }
    }

    public static int size() {
        return pl.size();
    }
}
