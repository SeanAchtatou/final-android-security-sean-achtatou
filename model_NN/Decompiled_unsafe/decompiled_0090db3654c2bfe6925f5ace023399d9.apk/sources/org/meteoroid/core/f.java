package org.meteoroid.core;

import android.app.Activity;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.util.Iterator;
import java.util.LinkedHashSet;

public final class f extends GestureDetector.SimpleOnGestureListener implements View.OnKeyListener, View.OnTouchListener {
    public static final String LOG_TAG = "InputManager";
    private static final f oE = new f();
    private static final LinkedHashSet<a> oG = new LinkedHashSet<>();
    private static final LinkedHashSet<b> oH = new LinkedHashSet<>();
    private static final LinkedHashSet<C0004f> oI = new LinkedHashSet<>();
    /* access modifiers changed from: private */
    public static final LinkedHashSet<e> oJ = new LinkedHashSet<>();
    /* access modifiers changed from: private */
    public static final LinkedHashSet<d> oK = new LinkedHashSet<>();
    private SensorManager mI;
    /* access modifiers changed from: private */
    public GestureDetector oF;

    public interface a {
        boolean b(KeyEvent keyEvent);
    }

    public interface b {
        boolean k(int i, int i2, int i3, int i4);
    }

    private final class c implements View.OnTouchListener {
        private c() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            SystemClock.sleep(34);
            if (!f.oJ.isEmpty()) {
                f.this.oF.onTouchEvent(motionEvent);
            }
            if (!f.oK.isEmpty()) {
                Iterator it = f.oK.iterator();
                while (it.hasNext()) {
                    ((d) it.next()).a(view, motionEvent);
                }
            }
            int action = motionEvent.getAction();
            int i = action & 255;
            int i2 = action >> 8;
            if (i == 6) {
                return f.k(1, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
            }
            if (i == 5) {
                return f.k(0, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
            }
            int pointerCount = motionEvent.getPointerCount();
            boolean z = false;
            for (int i3 = 0; i3 < pointerCount; i3++) {
                if (f.k(action, (int) motionEvent.getX(i3), (int) motionEvent.getY(i3), i3)) {
                    z = true;
                }
            }
            return z;
        }
    }

    public interface d {
        void a(View view, MotionEvent motionEvent);
    }

    public interface e {
        public static final int GESTURE_SLIDE_DOWN = 2;
        public static final int GESTURE_SLIDE_LEFT = 3;
        public static final int GESTURE_SLIDE_RIGHT = 4;
        public static final int GESTURE_SLIDE_UP = 1;

        boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2);
    }

    /* renamed from: org.meteoroid.core.f$f  reason: collision with other inner class name */
    public interface C0004f {
        boolean onTrackballEvent(MotionEvent motionEvent);
    }

    public static final void a(SensorEventListener sensorEventListener) {
        oE.mI.registerListener(sensorEventListener, oE.mI.getDefaultSensor(1), 3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x0015  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void a(android.view.KeyEvent r2) {
        /*
            java.util.LinkedHashSet<org.meteoroid.core.f$a> r0 = org.meteoroid.core.f.oG
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            java.util.LinkedHashSet<org.meteoroid.core.f$a> r0 = org.meteoroid.core.f.oG
            java.util.Iterator r1 = r0.iterator()
        L_0x000f:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0008
            java.lang.Object r0 = r1.next()
            org.meteoroid.core.f$a r0 = (org.meteoroid.core.f.a) r0
            boolean r0 = r0.b(r2)
            if (r0 == 0) goto L_0x000f
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.core.f.a(android.view.KeyEvent):void");
    }

    protected static void a(View view) {
        View.OnTouchListener onTouchListener;
        if (!m.pB) {
            view.setOnKeyListener(oE);
        }
        if (l.hz() >= 5) {
            f fVar = oE;
            fVar.getClass();
            onTouchListener = new c();
        } else {
            onTouchListener = oE;
        }
        view.setOnTouchListener(onTouchListener);
    }

    public static final void a(a aVar) {
        oG.add(aVar);
    }

    public static final void a(b bVar) {
        oH.add(bVar);
    }

    public static final void a(d dVar) {
        oK.add(dVar);
    }

    public static final void a(e eVar) {
        oJ.add(eVar);
    }

    public static final void a(C0004f fVar) {
        oI.add(fVar);
    }

    public static final void b(SensorEventListener sensorEventListener) {
        oE.mI.unregisterListener(sensorEventListener);
    }

    protected static void b(View view) {
        view.setOnKeyListener(null);
        view.setOnTouchListener(null);
    }

    public static final void b(a aVar) {
        oG.remove(aVar);
    }

    public static final void b(b bVar) {
        oH.remove(bVar);
    }

    public static final void b(d dVar) {
        oK.remove(dVar);
    }

    public static final void b(e eVar) {
        oJ.remove(eVar);
    }

    public static final void b(C0004f fVar) {
        oI.remove(fVar);
    }

    protected static void d(Activity activity) {
        oE.oF = new GestureDetector(activity, oE);
        oE.mI = (SensorManager) activity.getSystemService("sensor");
    }

    public static final boolean k(int i, int i2, int i3, int i4) {
        Iterator<b> it = oH.iterator();
        while (it.hasNext()) {
            if (it.next().k(i, i2, i3, i4)) {
                return true;
            }
        }
        return false;
    }

    protected static void onDestroy() {
        oJ.clear();
        oH.clear();
        oI.clear();
        oG.clear();
    }

    public static boolean onTrackballEvent(MotionEvent motionEvent) {
        if (oI.isEmpty()) {
            return false;
        }
        Iterator<C0004f> it = oI.iterator();
        while (it.hasNext()) {
            if (it.next().onTrackballEvent(motionEvent)) {
                return true;
            }
        }
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        Iterator<e> it = oJ.iterator();
        while (it.hasNext()) {
            if (it.next().onFling(motionEvent, motionEvent2, f, f2)) {
                return true;
            }
        }
        return false;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (oG.isEmpty()) {
            return false;
        }
        Iterator<a> it = oG.iterator();
        while (it.hasNext()) {
            if (it.next().b(keyEvent)) {
                return true;
            }
        }
        return false;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        SystemClock.sleep(34);
        if (!oJ.isEmpty()) {
            this.oF.onTouchEvent(motionEvent);
        }
        if (!oK.isEmpty()) {
            Iterator<d> it = oK.iterator();
            while (it.hasNext()) {
                it.next().a(view, motionEvent);
            }
        }
        return k(motionEvent.getAction(), (int) motionEvent.getX(), (int) motionEvent.getY(), 0);
    }
}
