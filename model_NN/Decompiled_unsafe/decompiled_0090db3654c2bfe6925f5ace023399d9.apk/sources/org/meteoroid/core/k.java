package org.meteoroid.core;

import android.content.SharedPreferences;
import android.util.Log;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public final class k {

    public static final class a {
        public a(String str) {
        }
    }

    public static final class b {
        private static final String DATA_STORE_FILE = ".dat";
        private static final String LOG_TAG = "FilePersistence";
        private boolean po;

        public b(boolean z) {
            this.po = z;
        }

        private String aN(String str) {
            return str.replace(File.separator, "_");
        }

        public List<String> aM(String str) {
            String aN = aN(str);
            String[] fileList = l.getActivity().fileList();
            ArrayList arrayList = new ArrayList();
            if (fileList != null && fileList.length > 0) {
                for (int i = 0; i < fileList.length; i++) {
                    if (fileList[i].endsWith(DATA_STORE_FILE) && (!(aN == null || fileList[i].indexOf(aN) == -1) || aN == null)) {
                        arrayList.add(fileList[i].substring(0, fileList[i].length() - DATA_STORE_FILE.length()));
                        Log.d(LOG_TAG, "Find data file:" + fileList[i]);
                    }
                }
            }
            return arrayList;
        }

        public boolean aO(String str) {
            String aN = aN(str);
            for (String equals : aM(aN)) {
                if (equals.equals(aN)) {
                    return true;
                }
            }
            return false;
        }

        public OutputStream aP(String str) {
            String aN = aN(str);
            Log.d(LOG_TAG, "Update data:" + aN);
            return l.getActivity().openFileOutput(aN + DATA_STORE_FILE, 1);
        }

        public InputStream aQ(String str) {
            String aN = aN(str);
            Log.d(LOG_TAG, "Read data:" + aN);
            return l.getActivity().openFileInput(aN + DATA_STORE_FILE);
        }

        public boolean aR(String str) {
            String aN = aN(str);
            Log.d(LOG_TAG, "Delete data:" + aN);
            return l.getActivity().deleteFile(aN + DATA_STORE_FILE);
        }

        public String aU(String str) {
            return l.getActivity().getFilesDir().getAbsolutePath() + File.separator + aN(str) + DATA_STORE_FILE;
        }
    }

    public static final class c {
        private SharedPreferences pp;

        private c(int i) {
            this.pp = l.getActivity().getPreferences(i);
        }

        public SharedPreferences.Editor getEditor() {
            return this.pp.edit();
        }

        public SharedPreferences getSharedPreferences() {
            return this.pp;
        }
    }

    public static a aT(String str) {
        return new a(str);
    }

    public static c bQ(int i) {
        return new c(i);
    }

    public static b hc() {
        return new b(true);
    }

    public static b hd() {
        return new b(false);
    }
}
