package org.opensocial.services;

import org.opensocial.models.Group;
import org.opensocial.providers.Provider;

public abstract class GroupsRestService extends RestService {
    public GroupsRestService(Provider provider) {
        super(provider);
        cg("groups/{id}");
    }

    public abstract Group E(String str, String str2);

    public abstract boolean F(String str, String str2);

    public abstract Group a(String str, String str2, Group group);

    public abstract Group[] cd(String str);
}
