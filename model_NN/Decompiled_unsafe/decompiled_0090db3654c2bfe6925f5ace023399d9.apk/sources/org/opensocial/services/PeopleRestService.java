package org.opensocial.services;

import org.opensocial.models.Person;
import org.opensocial.providers.Provider;

public abstract class PeopleRestService extends RestService {
    public PeopleRestService(Provider provider) {
        super(provider);
        cg("people/{id}/{groupId}");
    }

    public abstract Person G(String str, String str2);

    public abstract Person[] H(String str, String str2);

    public Person ce(String str) {
        return G(str, Service.SELF);
    }

    public Person[] cf(String str) {
        return H(str, Service.FRIENDS);
    }
}
