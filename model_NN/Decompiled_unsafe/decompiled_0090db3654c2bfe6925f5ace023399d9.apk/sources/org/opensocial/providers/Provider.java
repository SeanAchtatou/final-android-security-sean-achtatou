package org.opensocial.providers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Provider implements Serializable {
    private static final long serialVersionUID = 1;
    private String kC;
    private String name;
    private String uY;
    private String uZ;
    private String va;
    private String vb;
    private String vc;
    private Map<String, String> vd;
    private boolean ve = true;
    private String version;

    public void D(String str, String str2) {
        if (this.vd == null) {
            this.vd = new HashMap();
        }
        this.vd.put(str, str2);
    }

    public void G(boolean z) {
        this.ve = z;
    }

    public void bX(String str) {
        this.version = str;
    }

    public void bY(String str) {
        this.uY = str;
    }

    public void bZ(String str) {
        this.uZ = str;
    }

    public void ca(String str) {
        this.va = str;
    }

    public void cb(String str) {
        this.vb = str;
    }

    public void cc(String str) {
        this.vc = str;
    }

    public void d(Map<String, String> map) {
        this.vd = map;
    }

    public String getContentType() {
        return this.kC == null ? "application/json" : this.kC;
    }

    public String getName() {
        return this.name;
    }

    public String getVersion() {
        return this.version == null ? "0.8" : this.version;
    }

    public String kq() {
        return this.uY;
    }

    public String kr() {
        return this.uZ;
    }

    public String ks() {
        return this.va;
    }

    public String kt() {
        return this.vb;
    }

    public String ku() {
        return this.vc;
    }

    public Map<String, String> kv() {
        return this.vd;
    }

    public boolean kw() {
        return this.ve;
    }

    public void setContentType(String str) {
        this.kC = str;
    }

    public void setName(String str) {
        this.name = str;
    }
}
