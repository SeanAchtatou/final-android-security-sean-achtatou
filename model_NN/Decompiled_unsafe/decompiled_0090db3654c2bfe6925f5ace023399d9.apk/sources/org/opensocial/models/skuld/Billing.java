package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public final class Billing extends Model {
    public static final int BILLING_TYPE_DEFAULT = 0;
    public static final int BILLING_TYPE_OTHER = 4;
    public static final int BILLING_TYPE_SMS = 1;
    public static final int BILLING_TYPE_WAP = 2;
    public static final int BILLING_TYPE_WEB = 3;
    public int type;
    public float uI;
    private String uJ;

    public void bS(String str) {
        this.uJ = str;
    }

    public int getType() {
        return this.type;
    }

    public void k(float f) {
        this.uI = f;
    }

    public float ka() {
        return this.uI;
    }

    public String kb() {
        return this.uJ;
    }

    public void setType(int i) {
        this.type = i;
    }
}
