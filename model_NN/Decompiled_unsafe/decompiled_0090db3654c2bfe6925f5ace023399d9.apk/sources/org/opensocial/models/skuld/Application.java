package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public class Application extends Model {
    private String name;
    private int uA;
    private int uB;
    private String uC;
    private String uD;
    private String uE;
    private String uF;
    private String uG;
    private long uH;
    private String uv;
    private String uw;
    private String ux;
    private long uy;
    private String uz;
    private int versionCode;
    private String versionName;

    public void bI(String str) {
        this.uv = str;
    }

    public void bJ(String str) {
        this.uw = str;
    }

    public void bK(String str) {
        this.ux = str;
    }

    public void bL(String str) {
        this.uz = str;
    }

    public void bM(String str) {
        this.versionName = str;
    }

    public void bN(String str) {
        this.uC = str;
    }

    public void bO(String str) {
        this.uD = str;
    }

    public void bP(String str) {
        this.uE = str;
    }

    public void bQ(String str) {
        this.uF = str;
    }

    public void bR(String str) {
        this.uG = str;
    }

    public void cf(int i) {
        this.versionCode = i;
    }

    public void cg(int i) {
        this.uA = i;
    }

    public void ch(int i) {
        this.uB = i;
    }

    public int fO() {
        return this.versionCode;
    }

    public String fP() {
        return this.versionName;
    }

    public String getName() {
        return this.name;
    }

    public String jN() {
        return this.uv;
    }

    public String jO() {
        return this.uw;
    }

    public String jP() {
        return this.ux;
    }

    public long jQ() {
        return this.uy;
    }

    public String jR() {
        return this.uz;
    }

    public int jS() {
        return this.uA;
    }

    public int jT() {
        return this.uB;
    }

    public String jU() {
        return this.uC;
    }

    public String jV() {
        return this.uD;
    }

    public String jW() {
        return this.uE;
    }

    public String jX() {
        return this.uF;
    }

    public String jY() {
        return this.uG;
    }

    public long jZ() {
        return this.uH;
    }

    public void o(long j) {
        this.uy = j;
    }

    public void p(long j) {
        this.uH = j;
    }

    public void setName(String str) {
        this.name = str;
    }
}
