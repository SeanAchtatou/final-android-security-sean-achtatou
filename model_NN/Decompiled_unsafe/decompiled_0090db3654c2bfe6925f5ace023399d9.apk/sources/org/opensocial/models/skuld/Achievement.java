package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public final class Achievement extends Model {
    public boolean us;
    public boolean ut;
    public int uu;

    public void E(boolean z) {
        this.us = z;
    }

    public void F(boolean z) {
        this.ut = z;
    }

    public void ce(int i) {
        this.uu = i;
    }

    public boolean jK() {
        return this.us;
    }

    public boolean jL() {
        return this.ut;
    }

    public int jM() {
        return this.uu;
    }
}
