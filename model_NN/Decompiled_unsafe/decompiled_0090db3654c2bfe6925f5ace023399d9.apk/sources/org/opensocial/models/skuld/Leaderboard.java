package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public final class Leaderboard extends Model {
    public int uN;
    public int uO;
    public String uP;
    public int uQ;

    public void bV(String str) {
        this.uP = str;
    }

    public void ci(int i) {
        this.uN = i;
    }

    public void cj(int i) {
        this.uO = i;
    }

    public void ck(int i) {
        this.uQ = i;
    }

    public int kf() {
        return this.uN;
    }

    public int kg() {
        return this.uO;
    }

    public String kh() {
        return this.uP;
    }

    public int ki() {
        return this.uQ;
    }
}
