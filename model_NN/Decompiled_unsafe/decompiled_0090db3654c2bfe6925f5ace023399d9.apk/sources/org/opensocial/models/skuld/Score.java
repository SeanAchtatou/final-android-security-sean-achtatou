package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public final class Score extends Model {
    public int score;
    private String uJ;
    public int uW;
    public int uX;

    public void bS(String str) {
        this.uJ = str;
    }

    public void cn(int i) {
        this.score = i;
    }

    public void co(int i) {
        this.uW = i;
    }

    public void cp(int i) {
        this.uX = i;
    }

    public String kb() {
        return this.uJ;
    }

    public int kn() {
        return this.score;
    }

    public int ko() {
        return this.uW;
    }

    public int kp() {
        return this.uX;
    }
}
