package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public final class Challenge extends Model {
    public String uK;
    public long uL;
    public String uM;

    public void bT(String str) {
        this.uK = str;
    }

    public void bU(String str) {
        this.uM = str;
    }

    public String kc() {
        return this.uK;
    }

    public long kd() {
        return this.uL;
    }

    public String ke() {
        return this.uM;
    }

    public void q(long j) {
        this.uL = j;
    }
}
