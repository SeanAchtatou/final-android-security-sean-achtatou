package org.opensocial.models;

import java.util.Date;

public class Person extends Model {
    public static final String GENDER_FEMALE = "female";
    public static final String GENDER_MALE = "male";
    public static final String GENDER_UNDISCLOSED = "undisclosed";
    public static final String NETWORK_PRESENCE_AWAY = "AWAY";
    public static final String NETWORK_PRESENCE_CHAT = "CHAT";
    public static final String NETWORK_PRESENCE_DND = "DND";
    public static final String NETWORK_PRESENCE_OFFLINE = "_OFFLINE";
    public static final String NETWORK_PRESENCE_ONLINE = "ONLINE";
    public static final String NETWORK_PRESENCE_XA = "XA";
    private boolean connected;
    private String oY;
    private String tN;
    private String[] tO;
    private String tP = GENDER_UNDISCLOSED;
    private Date tQ;
    private String[] tR;
    private String tS;
    private String tT;
    private Account[] tU;
    private Address[] tV;
    private Name[] tW;
    private AppData[] tX;
    private String tY;
    private String[] tZ;
    private Name ua;
    private Name ub;
    private String uc;
    private Organization[] ud;
    private String[] ue;
    private Name uf;
    private String ug;
    private String uh;
    private Date ui;
    private String[] uj;
    private String uk;
    private String[] ul;
    private String um;
    private Date un;
    private String[] uo;
    private DateUTCOffset up;

    public void D(boolean z) {
        this.connected = z;
    }

    public void a(DateUTCOffset dateUTCOffset) {
        this.up = dateUTCOffset;
    }

    public void a(Name name) {
        this.ua = name;
    }

    public void a(Account[] accountArr) {
        this.tU = accountArr;
    }

    public void a(Address[] addressArr) {
        this.tV = addressArr;
    }

    public void a(AppData[] appDataArr) {
        this.tX = appDataArr;
    }

    public void a(Name[] nameArr) {
        this.tW = nameArr;
    }

    public void a(Organization[] organizationArr) {
        this.ud = organizationArr;
    }

    public void b(Date date) {
        this.tQ = date;
    }

    public void b(Name name) {
        this.ub = name;
    }

    public void bA(String str) {
        this.tY = str;
    }

    public void bB(String str) {
        this.oY = str;
    }

    public void bC(String str) {
        this.uc = str;
    }

    public void bD(String str) {
        this.ug = str;
    }

    public void bE(String str) {
        this.uh = str;
    }

    public void bF(String str) {
        this.uk = str;
    }

    public void bG(String str) {
        this.um = str;
    }

    public void bw(String str) {
        this.tN = str;
    }

    public void bx(String str) {
        this.tP = str;
    }

    public void by(String str) {
        this.tS = str;
    }

    public void bz(String str) {
        this.tT = str;
    }

    public void c(Date date) {
        this.ui = date;
    }

    public void c(Name name) {
        this.uf = name;
    }

    public void c(String[] strArr) {
        this.tO = strArr;
    }

    public void d(Date date) {
        this.un = date;
    }

    public void d(String[] strArr) {
        this.tR = strArr;
    }

    public void e(String[] strArr) {
        this.tZ = strArr;
    }

    public void f(String[] strArr) {
        this.ue = strArr;
    }

    public void g(String[] strArr) {
        this.uj = strArr;
    }

    public String getDisplayName() {
        return this.tN;
    }

    public String getLocation() {
        return this.oY;
    }

    public void h(String[] strArr) {
        this.ul = strArr;
    }

    public void i(String[] strArr) {
        this.uo = strArr;
    }

    public boolean isConnected() {
        return this.connected;
    }

    public Date jA() {
        return this.ui;
    }

    public String[] jB() {
        return this.uj;
    }

    public String jC() {
        return this.uk;
    }

    public String[] jD() {
        return this.ul;
    }

    public String jE() {
        return this.um;
    }

    public Date jF() {
        return this.un;
    }

    public String[] jG() {
        return this.uo;
    }

    public DateUTCOffset jH() {
        return this.up;
    }

    public String[] jg() {
        return this.tO;
    }

    public String jh() {
        return this.tP;
    }

    public Date ji() {
        return this.tQ;
    }

    public String[] jj() {
        return this.tR;
    }

    public String jk() {
        return this.tS;
    }

    public String jl() {
        return this.tT;
    }

    public Account[] jm() {
        return this.tU;
    }

    public Address[] jn() {
        return this.tV;
    }

    public Name[] jo() {
        return this.tW;
    }

    public AppData[] jp() {
        return this.tX;
    }

    public String jq() {
        return this.tY;
    }

    public String[] jr() {
        return this.tZ;
    }

    public Name js() {
        return this.ua;
    }

    public Name jt() {
        return this.ub;
    }

    public String ju() {
        return this.uc;
    }

    public Organization[] jv() {
        return this.ud;
    }

    public String[] jw() {
        return this.ue;
    }

    public Name jx() {
        return this.uf;
    }

    public String jy() {
        return this.ug;
    }

    public String jz() {
        return this.uh;
    }
}
