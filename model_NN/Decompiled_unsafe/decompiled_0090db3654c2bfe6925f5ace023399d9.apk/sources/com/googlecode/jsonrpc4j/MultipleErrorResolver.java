package com.googlecode.jsonrpc4j;

import com.fasterxml.jackson.databind.JsonNode;
import com.googlecode.jsonrpc4j.ErrorResolver;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

public class MultipleErrorResolver implements ErrorResolver {
    private List<ErrorResolver> resolvers = new LinkedList();

    public MultipleErrorResolver(ErrorResolver... errorResolverArr) {
        for (ErrorResolver add : errorResolverArr) {
            this.resolvers.add(add);
        }
    }

    public void addErrorResolver(ErrorResolver errorResolver) {
        this.resolvers.add(errorResolver);
    }

    public ErrorResolver.JsonError resolveError(Throwable th, Method method, List<JsonNode> list) {
        for (ErrorResolver resolveError : this.resolvers) {
            ErrorResolver.JsonError resolveError2 = resolveError.resolveError(th, method, list);
            if (resolveError2 != null) {
                return resolveError2;
            }
        }
        return null;
    }
}
