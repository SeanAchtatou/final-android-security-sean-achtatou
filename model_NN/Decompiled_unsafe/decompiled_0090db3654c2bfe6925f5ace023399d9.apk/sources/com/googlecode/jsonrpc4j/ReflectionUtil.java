package com.googlecode.jsonrpc4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class ReflectionUtil {
    private static Map<Method, List<Annotation>> methodAnnotationCache = new HashMap();
    private static Map<String, Set<Method>> methodCache = new HashMap();
    private static Map<Method, List<List<Annotation>>> methodParamAnnotationCache = new HashMap();
    private static Map<Method, List<Class<?>>> parameterTypeCache = new HashMap();

    public static Set<Method> findMethods(Class<?>[] clsArr, String str) {
        StringBuilder sb = new StringBuilder();
        for (Class<?> name : clsArr) {
            sb.append(name.getName()).append("::");
        }
        String sb2 = sb.append(str).toString();
        if (methodCache.containsKey(sb2)) {
            return methodCache.get(sb2);
        }
        HashSet hashSet = new HashSet();
        for (Class<?> methods : clsArr) {
            for (Method method : methods.getMethods()) {
                if (method.getName().equals(str)) {
                    hashSet.add(method);
                }
            }
        }
        Set<Method> unmodifiableSet = Collections.unmodifiableSet(hashSet);
        methodCache.put(sb2, unmodifiableSet);
        return unmodifiableSet;
    }

    public static <T extends Annotation> T getAnnotation(Method method, Class<T> cls) {
        for (Annotation next : getAnnotations(method)) {
            if (cls.isInstance(next)) {
                return (Annotation) cls.cast(next);
            }
        }
        return null;
    }

    public static List<Annotation> getAnnotations(Method method) {
        if (methodAnnotationCache.containsKey(method)) {
            return methodAnnotationCache.get(method);
        }
        ArrayList arrayList = new ArrayList();
        for (Annotation add : method.getAnnotations()) {
            arrayList.add(add);
        }
        List<Annotation> unmodifiableList = Collections.unmodifiableList(arrayList);
        methodAnnotationCache.put(method, unmodifiableList);
        return unmodifiableList;
    }

    public static <T extends Annotation> List<T> getAnnotations(Method method, Class<T> cls) {
        ArrayList arrayList = new ArrayList();
        for (Annotation next : getAnnotations(method)) {
            if (cls.isInstance(next)) {
                arrayList.add(cls.cast(next));
            }
        }
        return arrayList;
    }

    public static List<List<Annotation>> getParameterAnnotations(Method method) {
        if (methodParamAnnotationCache.containsKey(method)) {
            return methodParamAnnotationCache.get(method);
        }
        ArrayList arrayList = new ArrayList();
        for (Annotation[] annotationArr : method.getParameterAnnotations()) {
            ArrayList arrayList2 = new ArrayList();
            for (Annotation add : r4[r2]) {
                arrayList2.add(add);
            }
            arrayList.add(arrayList2);
        }
        List<List<Annotation>> unmodifiableList = Collections.unmodifiableList(arrayList);
        methodParamAnnotationCache.put(method, unmodifiableList);
        return unmodifiableList;
    }

    public static <T extends Annotation> List<List<T>> getParameterAnnotations(Method method, Class<T> cls) {
        ArrayList arrayList = new ArrayList();
        for (List<Annotation> it : getParameterAnnotations(method)) {
            ArrayList arrayList2 = new ArrayList();
            for (Annotation annotation : it) {
                if (cls.isInstance(annotation)) {
                    arrayList2.add(cls.cast(annotation));
                }
            }
            arrayList.add(arrayList2);
        }
        return arrayList;
    }

    public static List<Class<?>> getParameterTypes(Method method) {
        if (parameterTypeCache.containsKey(method)) {
            return parameterTypeCache.get(method);
        }
        ArrayList arrayList = new ArrayList();
        for (Class<?> add : method.getParameterTypes()) {
            arrayList.add(add);
        }
        List<Class<?>> unmodifiableList = Collections.unmodifiableList(arrayList);
        parameterTypeCache.put(method, unmodifiableList);
        return unmodifiableList;
    }

    public static Object parseArguments(Method method, Object[] objArr, boolean z) {
        boolean z2;
        if (!z) {
            return objArr;
        }
        HashMap hashMap = new HashMap();
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        for (int i = 0; i < parameterAnnotations.length; i++) {
            Annotation[] annotationArr = parameterAnnotations[i];
            int length = annotationArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    z2 = false;
                    break;
                }
                Annotation annotation = annotationArr[i2];
                if (JsonRpcParam.class.isInstance(annotation)) {
                    hashMap.put(((JsonRpcParam) annotation).value(), objArr[i]);
                    z2 = true;
                    break;
                }
                i2++;
            }
            if (!z2) {
                throw new RuntimeException("useNamedParams is enabled and a JsonRpcParam annotation was not found at parameter index " + i + " on method " + method.getName());
            }
        }
        return hashMap;
    }
}
