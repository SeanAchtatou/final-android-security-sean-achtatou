package com.googlecode.jsonrpc4j;

import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.LinkedList;
import java.util.List;

public class MultipleExceptionResolver implements ExceptionResolver {
    private List<ExceptionResolver> resolvers = new LinkedList();

    public MultipleExceptionResolver(ExceptionResolver... exceptionResolverArr) {
        for (ExceptionResolver add : exceptionResolverArr) {
            this.resolvers.add(add);
        }
    }

    public void addExceptionResolver(ExceptionResolver exceptionResolver) {
        this.resolvers.add(exceptionResolver);
    }

    public Throwable resolveException(ObjectNode objectNode) {
        for (ExceptionResolver resolveException : this.resolvers) {
            Throwable resolveException2 = resolveException.resolveException(objectNode);
            if (resolveException2 != null) {
                return resolveException2;
            }
        }
        return null;
    }
}
