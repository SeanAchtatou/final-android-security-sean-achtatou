package com.googlecode.jsonrpc4j.spring;

import com.googlecode.jsonrpc4j.StreamServer;
import java.net.InetAddress;
import javax.net.ServerSocketFactory;
import org.springframework.beans.factory.DisposableBean;

public class CompositeJsonStreamServiceExporter extends AbstractCompositeJsonServiceExporter implements DisposableBean {
    public static final int DEFAULT_BACKLOG = 0;
    public static final String DEFAULT_HOSTNAME = "0.0.0.0";
    public static final int DEFAULT_MAX_CLIENT_ERRORS = 5;
    public static final int DEFAULT_MAX_THREADS = 50;
    public static final int DEFAULT_PORT = 10420;
    private int backlog = 0;
    private String hostName = "0.0.0.0";
    private int maxClientErrors = 5;
    private int maxThreads = 50;
    private int port = 10420;
    private ServerSocketFactory serverSocketFactory;
    private StreamServer streamServer;

    public void destroy() {
        this.streamServer.stop();
    }

    /* access modifiers changed from: protected */
    public void exportService() {
        if (this.streamServer == null) {
            if (this.serverSocketFactory == null) {
                this.serverSocketFactory = ServerSocketFactory.getDefault();
            }
            this.streamServer = new StreamServer(getJsonRpcServer(), this.maxThreads, this.serverSocketFactory.createServerSocket(this.port, this.backlog, InetAddress.getByName(this.hostName)));
            this.streamServer.setMaxClientErrors(this.maxClientErrors);
        }
        this.streamServer.start();
    }

    public void setBacklog(int i) {
        this.backlog = i;
    }

    public void setHostName(String str) {
        this.hostName = str;
    }

    public void setMaxClientErrors(int i) {
        this.maxClientErrors = i;
    }

    public void setMaxThreads(int i) {
        this.maxThreads = i;
    }

    public void setPort(int i) {
        this.port = i;
    }

    public void setServerSocketFactory(ServerSocketFactory serverSocketFactory2) {
        this.serverSocketFactory = serverSocketFactory2;
    }

    public void setStreamServer(StreamServer streamServer2) {
        this.streamServer = streamServer2;
    }
}
