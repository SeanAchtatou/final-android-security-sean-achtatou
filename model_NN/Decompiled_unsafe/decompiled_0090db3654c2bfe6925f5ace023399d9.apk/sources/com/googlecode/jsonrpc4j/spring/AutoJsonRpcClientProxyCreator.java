package com.googlecode.jsonrpc4j.spring;

import com.googlecode.jsonrpc4j.JsonRpcService;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import me.gall.sgp.sdk.entity.app.StructuredData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.SimpleMetadataReaderFactory;
import org.springframework.util.ClassUtils;

public class AutoJsonRpcClientProxyCreator implements BeanFactoryPostProcessor, ApplicationContextAware {
    private static final Log LOG = LogFactory.getLog(AutoJsonRpcClientProxyCreator.class);
    private ApplicationContext applicationContext;
    private URL baseUrl;
    private String scanPackage;

    private String appendBasePath(String str) {
        try {
            return new URL(this.baseUrl, str).toString();
        } catch (MalformedURLException e) {
            throw new RuntimeException(String.format("Cannot combine URLs '%s' and '%s' to valid URL.", this.baseUrl, str), e);
        }
    }

    private void registerJsonProxyBean(DefaultListableBeanFactory defaultListableBeanFactory, String str, String str2, boolean z) {
        defaultListableBeanFactory.registerBeanDefinition(str + "-clientProxy", BeanDefinitionBuilder.rootBeanDefinition(JsonProxyFactoryBean.class).addPropertyValue("serviceUrl", appendBasePath(str2)).addPropertyValue("serviceInterface", str).addPropertyValue("useNamedParams", Boolean.valueOf(z)).getBeanDefinition());
    }

    private String resolvePackageToScan() {
        return "classpath:" + ClassUtils.convertClassNameToResourcePath(this.scanPackage) + "/**/*.class";
    }

    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) {
        SimpleMetadataReaderFactory simpleMetadataReaderFactory = new SimpleMetadataReaderFactory(this.applicationContext);
        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) configurableListableBeanFactory;
        String resolvePackageToScan = resolvePackageToScan();
        LOG.debug(String.format("Scanning '%s' for JSON-RPC service interfaces.", resolvePackageToScan));
        try {
            for (Resource resource : this.applicationContext.getResources(resolvePackageToScan)) {
                if (resource.isReadable()) {
                    MetadataReader metadataReader = simpleMetadataReaderFactory.getMetadataReader(resource);
                    ClassMetadata classMetadata = metadataReader.getClassMetadata();
                    AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
                    String name = JsonRpcService.class.getName();
                    if (annotationMetadata.isAnnotated(name)) {
                        String className = classMetadata.getClassName();
                        String str = (String) annotationMetadata.getAnnotationAttributes(name).get(StructuredData.TYPE_OF_VALUE);
                        boolean booleanValue = ((Boolean) annotationMetadata.getAnnotationAttributes(name).get("useNamedParams")).booleanValue();
                        LOG.debug(String.format("Found JSON-RPC service to proxy [%s] on path '%s'.", className, str));
                        registerJsonProxyBean(defaultListableBeanFactory, className, str, booleanValue);
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(String.format("Cannot scan package '%s' for classes.", resolvePackageToScan), e);
        }
    }

    public void setApplicationContext(ApplicationContext applicationContext2) {
        this.applicationContext = applicationContext2;
    }

    public void setBaseUrl(URL url) {
        this.baseUrl = url;
    }

    public void setScanPackage(String str) {
        this.scanPackage = str;
    }
}
