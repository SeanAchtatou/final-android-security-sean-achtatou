package com.googlecode.jsonrpc4j;

import com.a.a.c.h;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.googlecode.jsonrpc4j.ErrorResolver;
import com.umeng.common.b.e;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.media.PlayerListener;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JsonRpcServer {
    public static final ErrorResolver DEFAULT_ERRROR_RESOLVER = new MultipleErrorResolver(AnnotationsErrorResolver.INSTANCE, DefaultErrorResolver.INSTANCE);
    public static final String JSONRPC_RESPONSE_CONTENT_TYPE = "application/json-rpc";
    private static final Logger LOGGER = Logger.getLogger(JsonRpcServer.class.getName());
    private static Class<?> WEBPARAM_ANNOTATION_CLASS;
    /* access modifiers changed from: private */
    public static Method WEBPARAM_NAME_METHOD;
    private boolean allowExtraParams;
    private boolean allowLessParams;
    private boolean backwardsComaptible;
    private ErrorResolver errorResolver;
    private Level exceptionLogLevel;
    private Object handler;
    private ObjectMapper mapper;
    private Class<?> remoteInterface;
    private boolean rethrowExceptions;

    private static class MethodAndArgs {
        /* access modifiers changed from: private */
        public List<JsonNode> arguments;
        /* access modifiers changed from: private */
        public Method method;

        private MethodAndArgs() {
            this.method = null;
            this.arguments = new ArrayList();
        }
    }

    static {
        try {
            WEBPARAM_ANNOTATION_CLASS = JsonRpcServer.class.getClassLoader().loadClass("javax.jws.WebParam");
            WEBPARAM_NAME_METHOD = WEBPARAM_ANNOTATION_CLASS.getMethod("name", new Class[0]);
        } catch (Exception e) {
        }
    }

    public JsonRpcServer(ObjectMapper objectMapper, Object obj) {
        this(objectMapper, obj, null);
    }

    public JsonRpcServer(ObjectMapper objectMapper, Object obj, Class<?> cls) {
        this.backwardsComaptible = true;
        this.rethrowExceptions = false;
        this.allowExtraParams = false;
        this.allowLessParams = false;
        this.errorResolver = null;
        this.exceptionLogLevel = Level.WARNING;
        this.mapper = objectMapper;
        this.handler = obj;
        this.remoteInterface = cls;
    }

    public JsonRpcServer(Object obj) {
        this(new ObjectMapper(), obj, null);
    }

    public JsonRpcServer(Object obj, Class<?> cls) {
        this(new ObjectMapper(), obj, null);
    }

    protected static InputStream createInputStream(String str, String str2, String str3) {
        return new ByteArrayInputStream(("{ " + "\"id\": \"" + str2 + "\", " + "\"method\": \"" + str + "\", " + "\"params\": " + URLDecoder.decode(new String(Base64.decode(str3)), e.f) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + "}").getBytes());
    }

    private MethodAndArgs findBestMethodByParamsNode(Set<Method> set, JsonNode jsonNode) {
        if (jsonNode == null || jsonNode.isNull()) {
            return findBestMethodUsingParamIndexes(set, 0, null);
        }
        if (jsonNode.isArray()) {
            return findBestMethodUsingParamIndexes(set, jsonNode.size(), ArrayNode.class.cast(jsonNode));
        }
        if (jsonNode.isObject()) {
            HashSet hashSet = new HashSet();
            Iterator<String> fieldNames = jsonNode.fieldNames();
            while (fieldNames.hasNext()) {
                hashSet.add(fieldNames.next());
            }
            return findBestMethodUsingParamNames(set, hashSet, ObjectNode.class.cast(jsonNode));
        }
        throw new IllegalArgumentException("Unknown params node type: " + jsonNode.toString());
    }

    private MethodAndArgs findBestMethodUsingParamIndexes(Set<Method> set, int i, ArrayNode arrayNode) {
        Method method;
        int i2;
        Method method2;
        int size = (arrayNode == null || arrayNode.isNull()) ? 0 : arrayNode.size();
        HashSet<Method> hashSet = new HashSet<>();
        int i3 = Integer.MAX_VALUE;
        for (Method next : set) {
            int length = next.getParameterTypes().length - i;
            if (Math.abs(length) <= Math.abs(i3) && ((this.allowExtraParams || length >= 0) && (this.allowLessParams || length <= 0))) {
                if (Math.abs(length) < Math.abs(i3)) {
                    hashSet.clear();
                }
                hashSet.add(next);
                i3 = length;
            }
        }
        if (hashSet.isEmpty()) {
            return null;
        }
        if (hashSet.size() == 1 || size == 0) {
            method = (Method) hashSet.iterator().next();
        } else {
            int i4 = -1;
            method = null;
            for (Method method3 : hashSet) {
                List<Class<?>> parameterTypes = ReflectionUtil.getParameterTypes(method3);
                int i5 = 0;
                int i6 = 0;
                while (i5 < parameterTypes.size() && i5 < size) {
                    if (isMatchingType(arrayNode.get(i5), parameterTypes.get(i5))) {
                        i6++;
                    }
                    i5++;
                }
                if (i6 > i4) {
                    method2 = method3;
                    i2 = i6;
                } else {
                    i2 = i4;
                    method2 = method;
                }
                i4 = i2;
                method = method2;
            }
        }
        MethodAndArgs methodAndArgs = new MethodAndArgs();
        Method unused = methodAndArgs.method = method;
        int length2 = method.getParameterTypes().length;
        for (int i7 = 0; i7 < length2; i7++) {
            if (i7 < size) {
                methodAndArgs.arguments.add(arrayNode.get(i7));
            } else {
                methodAndArgs.arguments.add(NullNode.getInstance());
            }
        }
        return methodAndArgs;
    }

    private MethodAndArgs findBestMethodUsingParamNames(Set<Method> set, Set<String> set2, ObjectNode objectNode) {
        int i;
        int i2;
        Method method;
        ArrayList arrayList;
        int i3 = -1;
        int i4 = -1;
        Method method2 = null;
        ArrayList arrayList2 = null;
        for (Method next : set) {
            List<Class<?>> parameterTypes = ReflectionUtil.getParameterTypes(next);
            if ((this.allowExtraParams || set2.size() <= parameterTypes.size()) && (this.allowLessParams || set2.size() >= parameterTypes.size())) {
                ArrayList arrayList3 = new ArrayList();
                for (List list : ReflectionUtil.getParameterAnnotations(next, JsonRpcParamName.class)) {
                    if (list.size() > 0) {
                        final JsonRpcParamName jsonRpcParamName = (JsonRpcParamName) list.get(0);
                        arrayList3.add(new JsonRpcParam() {
                            public Class<? extends Annotation> annotationType() {
                                return JsonRpcParam.class;
                            }

                            public String value() {
                                return jsonRpcParamName.value();
                            }
                        });
                    } else {
                        list.add(null);
                    }
                }
                for (List list2 : WEBPARAM_ANNOTATION_CLASS != null ? ReflectionUtil.getParameterAnnotations(next, WEBPARAM_ANNOTATION_CLASS) : new ArrayList()) {
                    if (list2.size() > 0) {
                        final Annotation annotation = (Annotation) list2.get(0);
                        arrayList3.add(new JsonRpcParam() {
                            public Class<? extends Annotation> annotationType() {
                                return JsonRpcParam.class;
                            }

                            public String value() {
                                try {
                                    return (String) JsonRpcServer.WEBPARAM_NAME_METHOD.invoke(annotation, new Object[0]);
                                } catch (Exception e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });
                    } else {
                        list2.add(null);
                    }
                }
                for (List list3 : ReflectionUtil.getParameterAnnotations(next, JsonRpcParam.class)) {
                    if (list3.size() > 0) {
                        arrayList3.add(list3.get(0));
                    } else {
                        list3.add(null);
                    }
                }
                int i5 = 0;
                int i6 = 0;
                int i7 = 0;
                while (true) {
                    int i8 = i7;
                    if (i8 >= arrayList3.size()) {
                        break;
                    }
                    JsonRpcParam jsonRpcParam = (JsonRpcParam) arrayList3.get(i8);
                    if (jsonRpcParam != null) {
                        String value = jsonRpcParam.value();
                        boolean contains = set2.contains(value);
                        if (contains && isMatchingType(objectNode.get(value), parameterTypes.get(i8))) {
                            i5++;
                            i6++;
                        } else if (contains) {
                            i6++;
                        }
                    }
                    i7 = i8 + 1;
                }
                if ((this.allowExtraParams || i6 <= parameterTypes.size()) && (this.allowLessParams || i6 >= parameterTypes.size())) {
                    if (i6 > i3 || (i6 == i3 && i5 > i4)) {
                        method = next;
                        i2 = i5;
                        arrayList = arrayList3;
                        i = i6;
                    } else {
                        arrayList = arrayList2;
                        method = method2;
                        i = i3;
                        i2 = i4;
                    }
                    method2 = method;
                    i4 = i2;
                    i3 = i;
                    arrayList2 = arrayList;
                }
            }
        }
        if (method2 == null) {
            return null;
        }
        MethodAndArgs methodAndArgs = new MethodAndArgs();
        Method unused = methodAndArgs.method = method2;
        int length = method2.getParameterTypes().length;
        for (int i9 = 0; i9 < length; i9++) {
            JsonRpcParam jsonRpcParam2 = (JsonRpcParam) arrayList2.get(i9);
            if (jsonRpcParam2 != null) {
                if (set2.contains(jsonRpcParam2.value())) {
                    methodAndArgs.arguments.add(objectNode.get(jsonRpcParam2.value()));
                }
            }
            methodAndArgs.arguments.add(NullNode.getInstance());
        }
        return methodAndArgs;
    }

    private boolean isMatchingType(JsonNode jsonNode, Class<?> cls) {
        boolean z = true;
        if (jsonNode.isNull()) {
            return true;
        }
        if (jsonNode.isTextual()) {
            return String.class.isAssignableFrom(cls);
        }
        if (jsonNode.isNumber()) {
            return Number.class.isAssignableFrom(cls) || Short.TYPE.isAssignableFrom(cls) || Integer.TYPE.isAssignableFrom(cls) || Long.TYPE.isAssignableFrom(cls) || Float.TYPE.isAssignableFrom(cls) || Double.TYPE.isAssignableFrom(cls);
        }
        if (!jsonNode.isArray() || !cls.isArray()) {
            if (jsonNode.isArray()) {
                return cls.isArray() || Collection.class.isAssignableFrom(cls);
            }
            if (jsonNode.isBinary()) {
                return byte[].class.isAssignableFrom(cls) || Byte[].class.isAssignableFrom(cls) || char[].class.isAssignableFrom(cls) || Character[].class.isAssignableFrom(cls);
            }
            if (jsonNode.isBoolean()) {
                return Boolean.TYPE.isAssignableFrom(cls) || Boolean.class.isAssignableFrom(cls);
            }
            if (!jsonNode.isObject() && !jsonNode.isPojo()) {
                return false;
            }
            if (cls.isPrimitive() || String.class.isAssignableFrom(cls) || Number.class.isAssignableFrom(cls) || Boolean.class.isAssignableFrom(cls)) {
                z = false;
            }
            return z;
        } else if (jsonNode.size() > 0) {
            return isMatchingType(jsonNode.get(0), cls.getComponentType());
        } else {
            return false;
        }
    }

    private Object parseId(JsonNode jsonNode) {
        if (jsonNode == null || jsonNode.isNull()) {
            return null;
        }
        if (jsonNode.isDouble()) {
            return Double.valueOf(jsonNode.asDouble());
        }
        if (jsonNode.isFloatingPointNumber()) {
            return Double.valueOf(jsonNode.asDouble());
        }
        if (jsonNode.isInt()) {
            return Integer.valueOf(jsonNode.asInt());
        }
        if (jsonNode.isIntegralNumber()) {
            return Integer.valueOf(jsonNode.asInt());
        }
        if (jsonNode.isLong()) {
            return Long.valueOf(jsonNode.asLong());
        }
        if (jsonNode.isTextual()) {
            return jsonNode.asText();
        }
        throw new IllegalArgumentException("Unknown id type");
    }

    private void writeAndFlushValue(OutputStream outputStream, Object obj) {
        this.mapper.writeValue(new NoCloseOutputStream(outputStream), obj);
        outputStream.flush();
    }

    /* access modifiers changed from: protected */
    public ObjectNode createErrorResponse(String str, Object obj, int i, String str2, Object obj2) {
        ObjectNode createObjectNode = this.mapper.createObjectNode();
        ObjectNode createObjectNode2 = this.mapper.createObjectNode();
        createObjectNode2.put("code", i);
        createObjectNode2.put("message", str2);
        if (obj2 != null) {
            createObjectNode2.put("data", this.mapper.valueToTree(obj2));
        }
        createObjectNode.put("jsonrpc", str);
        if (Integer.class.isInstance(obj)) {
            createObjectNode.put("id", Integer.class.cast(obj).intValue());
        } else if (Long.class.isInstance(obj)) {
            createObjectNode.put("id", Long.class.cast(obj).longValue());
        } else if (Float.class.isInstance(obj)) {
            createObjectNode.put("id", Float.class.cast(obj).floatValue());
        } else if (Double.class.isInstance(obj)) {
            createObjectNode.put("id", Double.class.cast(obj).doubleValue());
        } else if (BigDecimal.class.isInstance(obj)) {
            createObjectNode.put("id", BigDecimal.class.cast(obj));
        } else {
            createObjectNode.put("id", String.class.cast(obj));
        }
        createObjectNode.put(PlayerListener.ERROR, createObjectNode2);
        return createObjectNode;
    }

    /* access modifiers changed from: protected */
    public ObjectNode createSuccessResponse(String str, Object obj, JsonNode jsonNode) {
        ObjectNode createObjectNode = this.mapper.createObjectNode();
        createObjectNode.put("jsonrpc", str);
        if (Integer.class.isInstance(obj)) {
            createObjectNode.put("id", Integer.class.cast(obj).intValue());
        } else if (Long.class.isInstance(obj)) {
            createObjectNode.put("id", Long.class.cast(obj).longValue());
        } else if (Float.class.isInstance(obj)) {
            createObjectNode.put("id", Float.class.cast(obj).floatValue());
        } else if (Double.class.isInstance(obj)) {
            createObjectNode.put("id", Double.class.cast(obj).doubleValue());
        } else if (BigDecimal.class.isInstance(obj)) {
            createObjectNode.put("id", BigDecimal.class.cast(obj));
        } else {
            createObjectNode.put("id", String.class.cast(obj));
        }
        createObjectNode.put("result", jsonNode);
        return createObjectNode;
    }

    /* access modifiers changed from: protected */
    public Object getHandler(String str) {
        return this.handler;
    }

    /* access modifiers changed from: protected */
    public Class<?>[] getHandlerInterfaces(String str) {
        if (this.remoteInterface != null) {
            return new Class[]{this.remoteInterface};
        } else if (Proxy.isProxyClass(this.handler.getClass())) {
            return this.handler.getClass().getInterfaces();
        } else {
            return new Class[]{this.handler.getClass()};
        }
    }

    /* access modifiers changed from: protected */
    public String getMethodName(JsonNode jsonNode) {
        if (jsonNode == null || jsonNode.isNull()) {
            return null;
        }
        return jsonNode.asText();
    }

    /* access modifiers changed from: protected */
    public String getServiceName(JsonNode jsonNode) {
        return null;
    }

    public void handle(InputStream inputStream, OutputStream outputStream) {
        try {
            handleNode(this.mapper.readTree(new NoCloseInputStream(inputStream)), outputStream);
        } catch (JsonParseException e) {
            writeAndFlushValue(outputStream, createErrorResponse("jsonrpc", "null", -32700, "Parse error", null));
        }
    }

    public void handle(ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
        InputStream createInputStream;
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Handing ResourceRequest " + resourceRequest.getMethod());
        }
        resourceResponse.setContentType(JSONRPC_RESPONSE_CONTENT_TYPE);
        OutputStream portletOutputStream = resourceResponse.getPortletOutputStream();
        if (resourceRequest.getMethod().equals(h.POST)) {
            createInputStream = resourceRequest.getPortletInputStream();
        } else if (resourceRequest.getMethod().equals(h.GET)) {
            createInputStream = createInputStream(resourceRequest.getParameter("method"), resourceRequest.getParameter("id"), resourceRequest.getParameter("params"));
        } else {
            throw new IOException("Invalid request method, only POST and GET is supported");
        }
        handle(createInputStream, portletOutputStream);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.googlecode.jsonrpc4j.JsonRpcServer.handle(java.io.InputStream, java.io.OutputStream):void
     arg types: [javax.servlet.ServletInputStream, javax.servlet.ServletOutputStream]
     candidates:
      com.googlecode.jsonrpc4j.JsonRpcServer.handle(javax.portlet.ResourceRequest, javax.portlet.ResourceResponse):void
      com.googlecode.jsonrpc4j.JsonRpcServer.handle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse):void
      com.googlecode.jsonrpc4j.JsonRpcServer.handle(java.io.InputStream, java.io.OutputStream):void */
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        ServletInputStream createInputStream;
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Handing HttpServletRequest " + httpServletRequest.getMethod());
        }
        httpServletResponse.setContentType(JSONRPC_RESPONSE_CONTENT_TYPE);
        ServletOutputStream outputStream = httpServletResponse.getOutputStream();
        if (httpServletRequest.getMethod().equals(h.POST)) {
            createInputStream = httpServletRequest.getInputStream();
        } else if (httpServletRequest.getMethod().equals(h.GET)) {
            createInputStream = createInputStream(httpServletRequest.getParameter("method"), httpServletRequest.getParameter("id"), httpServletRequest.getParameter("params"));
        } else {
            throw new IOException("Invalid request method, only POST and GET is supported");
        }
        handle((InputStream) createInputStream, (OutputStream) outputStream);
    }

    public void handleArray(ArrayNode arrayNode, OutputStream outputStream) {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Handing " + arrayNode.size() + " requests");
        }
        outputStream.write(91);
        for (int i = 0; i < arrayNode.size(); i++) {
            handleNode(arrayNode.get(i), outputStream);
            if (i != arrayNode.size() - 1) {
                outputStream.write(44);
            }
        }
        outputStream.write(93);
    }

    public void handleNode(JsonNode jsonNode, OutputStream outputStream) {
        if (jsonNode.isObject()) {
            handleObject(ObjectNode.class.cast(jsonNode), outputStream);
        } else if (jsonNode.isArray()) {
            handleArray(ArrayNode.class.cast(jsonNode), outputStream);
        } else {
            writeAndFlushValue(outputStream, createErrorResponse("2.0", "null", -32600, "Invalid Request", null));
        }
    }

    public void handleObject(ObjectNode objectNode, OutputStream outputStream) {
        Throwable th;
        JsonNode jsonNode;
        ErrorResolver.JsonError jsonError = null;
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Request: " + objectNode.toString());
        }
        if ((this.backwardsComaptible || objectNode.has("jsonrpc")) && objectNode.has("method")) {
            JsonNode jsonNode2 = objectNode.get("jsonrpc");
            JsonNode jsonNode3 = objectNode.get("method");
            JsonNode jsonNode4 = objectNode.get("id");
            JsonNode jsonNode5 = objectNode.get("params");
            String asText = (jsonNode2 == null || jsonNode2.isNull()) ? "2.0" : jsonNode2.asText();
            String methodName = getMethodName(jsonNode3);
            String serviceName = getServiceName(jsonNode3);
            Object parseId = parseId(jsonNode4);
            HashSet hashSet = new HashSet();
            hashSet.addAll(ReflectionUtil.findMethods(getHandlerInterfaces(serviceName), methodName));
            if (hashSet.isEmpty()) {
                writeAndFlushValue(outputStream, createErrorResponse(asText, parseId, -32601, "Method not found", null));
                return;
            }
            MethodAndArgs findBestMethodByParamsNode = findBestMethodByParamsNode(hashSet, jsonNode5);
            if (findBestMethodByParamsNode == null) {
                writeAndFlushValue(outputStream, createErrorResponse(asText, parseId, -32602, "Invalid method parameters", null));
                return;
            }
            try {
                JsonNode invoke = invoke(getHandler(serviceName), findBestMethodByParamsNode.method, findBestMethodByParamsNode.arguments);
                th = null;
                jsonNode = invoke;
            } catch (Throwable th2) {
                th = th2;
                jsonNode = null;
            }
            if (parseId != null) {
                if (th != null) {
                    Throwable targetException = InvocationTargetException.class.isInstance(th) ? InvocationTargetException.class.cast(th).getTargetException() : th;
                    jsonError = this.errorResolver != null ? this.errorResolver.resolveError(targetException, findBestMethodByParamsNode.method, findBestMethodByParamsNode.arguments) : DEFAULT_ERRROR_RESOLVER.resolveError(targetException, findBestMethodByParamsNode.method, findBestMethodByParamsNode.arguments);
                    if (jsonError == null) {
                        jsonError = new ErrorResolver.JsonError(0, targetException.getMessage(), targetException.getClass().getName());
                    }
                }
                writeAndFlushValue(outputStream, jsonError != null ? createErrorResponse(asText, parseId, jsonError.getCode(), jsonError.getMessage(), jsonError.getData()) : createSuccessResponse(asText, parseId, jsonNode));
            }
            if (th != null) {
                if (LOGGER.isLoggable(this.exceptionLogLevel)) {
                    LOGGER.log(this.exceptionLogLevel, "Error in JSON-RPC Service", th);
                }
                if (this.rethrowExceptions) {
                    throw new RuntimeException(th);
                }
                return;
            }
            return;
        }
        writeAndFlushValue(outputStream, createErrorResponse("2.0", "null", -32600, "Invalid Request", null));
    }

    /* access modifiers changed from: protected */
    public JsonNode invoke(Object obj, Method method, List<JsonNode> list) {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Invoking method: " + method.getName());
        }
        Object[] objArr = new Object[list.size()];
        Type[] genericParameterTypes = method.getGenericParameterTypes();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= genericParameterTypes.length) {
                break;
            }
            objArr[i2] = this.mapper.readValue(this.mapper.treeAsTokens(list.get(i2)), TypeFactory.defaultInstance().constructType(genericParameterTypes[i2]));
            i = i2 + 1;
        }
        Object invoke = method.invoke(obj, objArr);
        if (method.getGenericReturnType() != null) {
            return this.mapper.valueToTree(invoke);
        }
        return null;
    }

    public void setAllowExtraParams(boolean z) {
        this.allowExtraParams = z;
    }

    public void setAllowLessParams(boolean z) {
        this.allowLessParams = z;
    }

    public void setBackwardsComaptible(boolean z) {
        this.backwardsComaptible = z;
    }

    public void setErrorResolver(ErrorResolver errorResolver2) {
        this.errorResolver = errorResolver2;
    }

    public void setExceptionLogLevel(Level level) {
        this.exceptionLogLevel = level;
    }

    public void setRethrowExceptions(boolean z) {
        this.rethrowExceptions = z;
    }
}
