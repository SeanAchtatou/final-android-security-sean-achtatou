package com.googlecode.jsonrpc4j;

import com.a.a.c.h;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

public class JsonRpcHttpClient extends JsonRpcClient {
    private Proxy connectionProxy;
    private int connectionTimeoutMillis;
    private Map<String, String> headers;
    private HostnameVerifier hostNameVerifier;
    private int readTimeoutMillis;
    private URL serviceUrl;
    private SSLContext sslContext;

    public JsonRpcHttpClient(ObjectMapper objectMapper, URL url, Map<String, String> map) {
        super(objectMapper);
        this.connectionProxy = Proxy.NO_PROXY;
        this.connectionTimeoutMillis = 60000;
        this.readTimeoutMillis = 120000;
        this.sslContext = null;
        this.hostNameVerifier = null;
        this.headers = new HashMap();
        this.serviceUrl = url;
        this.headers.putAll(map);
    }

    public JsonRpcHttpClient(URL url) {
        this(new ObjectMapper(), url, new HashMap());
    }

    public JsonRpcHttpClient(URL url, Map<String, String> map) {
        this(new ObjectMapper(), url, map);
    }

    public Proxy getConnectionProxy() {
        return this.connectionProxy;
    }

    public int getConnectionTimeoutMillis() {
        return this.connectionTimeoutMillis;
    }

    public Map<String, String> getHeaders() {
        return Collections.unmodifiableMap(this.headers);
    }

    public int getReadTimeoutMillis() {
        return this.readTimeoutMillis;
    }

    public URL getServiceUrl() {
        return this.serviceUrl;
    }

    public <T> T invoke(String str, Object obj, Class<T> cls) {
        return invoke(str, obj, (Type) Type.class.cast(cls));
    }

    public <T> T invoke(String str, Object obj, Class<T> cls, Map<String, String> map) {
        return invoke(str, obj, (Type) Type.class.cast(cls), map);
    }

    public Object invoke(String str, Object obj, Type type) {
        return invoke(str, obj, type, new HashMap());
    }

    /* JADX INFO: finally extract failed */
    public Object invoke(String str, Object obj, Type type, Map<String, String> map) {
        HttpURLConnection prepareConnection = prepareConnection(map);
        prepareConnection.connect();
        OutputStream outputStream = prepareConnection.getOutputStream();
        try {
            super.invoke(str, obj, outputStream);
            outputStream.close();
            InputStream inputStream = prepareConnection.getInputStream();
            try {
                return super.readResponse(type, inputStream);
            } finally {
                inputStream.close();
            }
        } catch (Throwable th) {
            outputStream.close();
            throw th;
        }
    }

    public void invoke(String str, Object obj) {
        invoke(str, obj, (Class) null, (Map<String, String>) new HashMap());
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection prepareConnection(Map<String, String> map) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) this.serviceUrl.openConnection(this.connectionProxy);
        httpURLConnection.setConnectTimeout(this.connectionTimeoutMillis);
        httpURLConnection.setReadTimeout(this.readTimeoutMillis);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setDefaultUseCaches(false);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setRequestMethod(h.POST);
        if (HttpsURLConnection.class.isInstance(httpURLConnection)) {
            HttpsURLConnection cast = HttpsURLConnection.class.cast(httpURLConnection);
            if (this.hostNameVerifier != null) {
                cast.setHostnameVerifier(this.hostNameVerifier);
            }
            if (this.sslContext != null) {
                cast.setSSLSocketFactory(this.sslContext.getSocketFactory());
            }
        }
        for (Map.Entry next : this.headers.entrySet()) {
            httpURLConnection.setRequestProperty((String) next.getKey(), (String) next.getValue());
        }
        for (Map.Entry next2 : map.entrySet()) {
            httpURLConnection.setRequestProperty((String) next2.getKey(), (String) next2.getValue());
        }
        httpURLConnection.setRequestProperty("Content-Type", JsonRpcServer.JSONRPC_RESPONSE_CONTENT_TYPE);
        return httpURLConnection;
    }

    public void setConnectionProxy(Proxy proxy) {
        this.connectionProxy = proxy;
    }

    public void setConnectionTimeoutMillis(int i) {
        this.connectionTimeoutMillis = i;
    }

    public void setHeaders(Map<String, String> map) {
        this.headers.clear();
        this.headers.putAll(map);
    }

    public void setHostNameVerifier(HostnameVerifier hostnameVerifier) {
        this.hostNameVerifier = hostnameVerifier;
    }

    public void setReadTimeoutMillis(int i) {
        this.readTimeoutMillis = i;
    }

    public void setServiceUrl(URL url) {
        this.serviceUrl = url;
    }

    public void setSslContext(SSLContext sSLContext) {
        this.sslContext = sSLContext;
    }
}
