package com.googlecode.jsonrpc4j;

import com.fasterxml.jackson.databind.JsonNode;
import java.lang.reflect.Method;
import java.util.List;

public interface ErrorResolver {

    public static class JsonError {
        private int code;
        private Object data;
        private String message;

        public JsonError(int i, String str, Object obj) {
            this.code = i;
            this.message = str;
            this.data = obj;
        }

        /* access modifiers changed from: protected */
        public int getCode() {
            return this.code;
        }

        /* access modifiers changed from: protected */
        public Object getData() {
            return this.data;
        }

        /* access modifiers changed from: protected */
        public String getMessage() {
            return this.message;
        }
    }

    JsonError resolveError(Throwable th, Method method, List<JsonNode> list);
}
