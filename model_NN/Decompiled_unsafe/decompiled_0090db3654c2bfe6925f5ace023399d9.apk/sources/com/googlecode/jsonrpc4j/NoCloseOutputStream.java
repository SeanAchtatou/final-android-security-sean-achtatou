package com.googlecode.jsonrpc4j;

import java.io.OutputStream;

public class NoCloseOutputStream extends OutputStream {
    private boolean closeAttempted = false;
    private OutputStream ops;

    public NoCloseOutputStream(OutputStream outputStream) {
        this.ops = outputStream;
    }

    public void close() {
        this.closeAttempted = true;
    }

    public void flush() {
        this.ops.flush();
    }

    public boolean wasCloseAttempted() {
        return this.closeAttempted;
    }

    public void write(int i) {
        this.ops.write(i);
    }

    public void write(byte[] bArr) {
        this.ops.write(bArr);
    }

    public void write(byte[] bArr, int i, int i2) {
        this.ops.write(bArr, i, i2);
    }
}
