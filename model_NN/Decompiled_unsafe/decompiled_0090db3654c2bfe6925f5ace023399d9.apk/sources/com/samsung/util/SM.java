package com.samsung.util;

public class SM {
    private String bB;
    private String bC;
    private final int bD;
    private String message;

    public SM() {
        this.bD = 80;
        this.bB = "";
        this.bC = "";
        this.message = "";
    }

    public SM(String str, String str2, String str3) {
        this.bD = 80;
        k(str);
        l(str2);
        setData(str3);
    }

    private boolean m(String str) {
        int length;
        if (str == null || (length = str.length()) > 20) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if ((charAt < '0' || charAt > '9') && (charAt != '+' || i != 0)) {
                return false;
            }
        }
        return true;
    }

    public String G() {
        return this.bB;
    }

    public String H() {
        return this.bC;
    }

    public String getData() {
        return this.message;
    }

    public void k(String str) {
        if (!m(str)) {
            throw new IllegalArgumentException("Invalid DestAddress");
        }
        this.bB = str;
    }

    public void l(String str) {
        this.bC = str;
    }

    public void setData(String str) {
        if (str == null || str.length() <= 80) {
            this.message = str;
            return;
        }
        throw new IllegalArgumentException("Invalid textMessage size");
    }
}
