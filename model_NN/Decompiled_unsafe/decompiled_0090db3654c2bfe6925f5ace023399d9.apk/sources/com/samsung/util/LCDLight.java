package com.samsung.util;

public final class LCDLight {
    public static boolean E() {
        return true;
    }

    public static void F() {
        if (!E()) {
            throw new IllegalStateException("off: device does not support LCDLight");
        }
    }

    public static void u(int i) {
        if (!E()) {
            throw new IllegalStateException("on: device does not support LCDLight");
        } else if (i < 0) {
            throw new IllegalArgumentException("invalid duration");
        }
    }
}
