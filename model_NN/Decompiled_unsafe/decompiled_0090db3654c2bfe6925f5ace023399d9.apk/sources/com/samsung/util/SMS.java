package com.samsung.util;

public class SMS {
    public static boolean E() {
        return true;
    }

    public static void a(SM sm) {
        if (sm == null) {
            throw new NullPointerException("SM is null");
        } else if (!E()) {
            throw new IllegalStateException("send: device does not support SMS");
        } else if (sm.getData() == null) {
            throw new NullPointerException("SM data is null");
        }
    }
}
