package com.umeng.analytics;

import android.content.Context;
import android.content.SharedPreferences;
import com.umeng.common.Log;
import com.umeng.common.b;
import javax.microedition.media.PlayerListener;

public class ReportPolicy {
    public static final int BATCH_AT_LAUNCH = 1;
    public static final int BATCH_BY_INTERVAL = 6;
    public static final int DAILY = 4;
    public static final int REALTIME = 0;
    public static final int WIFIONLY = 5;
    static final int a = 2;
    static final int b = 3;
    private static long c = 10000;
    private int d = -1;
    private long e = -1;
    private long f = -1;

    static void a(Context context, int i) {
        if (i < 0 || i > 6) {
            Log.b("MobclickAgent", "Illegal value of report policy");
        } else {
            h.b(context).edit().putInt("umeng_local_report_policy", i).commit();
        }
    }

    static void b(Context context, int i, long j) {
        if (i < 0 || i > 6) {
            Log.b("MobclickAgent", "Illegal value of report policy");
            return;
        }
        if (i == 6 && j < c) {
            j = c;
            Log.b("MobclickAgent", String.format("Report interval can't be less than %dms, reset to %dms", Long.valueOf(c), Long.valueOf(c)));
        }
        h.b(context).edit().putInt("umeng_local_report_policy", i).putLong("report_interval", j).commit();
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        if (this.d == 4) {
            SharedPreferences.Editor edit = h.d(context).edit();
            edit.putString(b.c(), "true");
            edit.commit();
        } else if (this.d == 6) {
            this.e = System.currentTimeMillis();
            h.b(context).edit().putLong("last_report_time", this.e).commit();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, int i, long j) {
        if (i < 0 || i > 6) {
            Log.b("MobclickAgent", "Illegal value of report policy");
            return;
        }
        this.d = i;
        this.f = j;
        SharedPreferences.Editor putInt = h.b(context).edit().putInt("umeng_net_report_policy", i);
        if (j < c) {
            j = c;
        }
        putInt.putLong("report_interval", j).commit();
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        if (this.d == 0) {
            return true;
        }
        return this.d == 6 && System.currentTimeMillis() - this.e > this.f;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, Context context) {
        if (!b.a(context, "android.permission.ACCESS_NETWORK_STATE") || !b.m(context)) {
            return false;
        }
        if ("flush".equals(str) || PlayerListener.ERROR.equals(str)) {
            return true;
        }
        switch (this.d) {
            case 0:
                return true;
            case 1:
                if (str == "launch") {
                    return true;
                }
                break;
            case 2:
                if (str == "terminate") {
                    return true;
                }
                break;
            case 3:
            default:
                this.d = b(context);
                return true;
            case 4:
                return !h.d(context).getString(b.c(), "false").equals("true") && str.equals("launch");
            case 5:
                return b.k(context);
            case 6:
                if (System.currentTimeMillis() - this.e > this.f) {
                    return true;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public int b(Context context) {
        SharedPreferences b2 = h.b(context);
        return b2.getInt("umeng_net_report_policy", -1) != -1 ? b2.getInt("umeng_net_report_policy", 1) : b2.getInt("umeng_local_report_policy", 1);
    }

    /* access modifiers changed from: package-private */
    public void c(Context context) {
        if (this.d == -1) {
            this.d = b(context);
        }
        if (this.d == 6 && this.f == -1) {
            SharedPreferences b2 = h.b(context);
            this.f = b2.getLong("report_interval", c);
            this.e = b2.getLong("last_report_time", -1);
        }
    }
}
