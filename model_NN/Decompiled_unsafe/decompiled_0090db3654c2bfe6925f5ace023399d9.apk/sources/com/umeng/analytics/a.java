package com.umeng.analytics;

import android.content.Context;
import android.text.TextUtils;
import com.a.a.m.m;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.umeng.common.Log;
import com.umeng.common.b;
import com.umeng.common.b.g;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import javax.microedition.media.PlayerListener;
import javax.microedition.media.control.MetaDataControl;
import org.json.JSONArray;
import org.json.JSONObject;

class a implements Thread.UncaughtExceptionHandler {
    private static final String d = "com_umeng__crash.cache";
    private Thread.UncaughtExceptionHandler a;
    private g b;
    private Context c;

    private void a(Throwable th) {
        if (th == null) {
            Log.e("MobclickAgent", "Exception is null in handleException");
            return;
        }
        this.b.f(this.c);
        a(this.c, th);
    }

    public void a(Context context) {
        if (Thread.getDefaultUncaughtExceptionHandler() == this) {
            Log.a("MobclickAgent", "can't call onError more than once!");
            return;
        }
        this.c = context.getApplicationContext();
        this.a = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str) {
        if (context != null && !TextUtils.isEmpty(str)) {
            try {
                String a2 = g.a();
                String str2 = a2.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)[0];
                String str3 = a2.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)[1];
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(MetaDataControl.DATE_KEY, str2);
                jSONObject.put("time", str3);
                jSONObject.put("context", str);
                jSONObject.put(com.umeng.common.a.b, PlayerListener.ERROR);
                jSONObject.put(m.PROP_VERSION, b.d(context));
                JSONArray b2 = b(context);
                if (b2 == null) {
                    b2 = new JSONArray();
                }
                b2.put(jSONObject);
                FileOutputStream openFileOutput = context.openFileOutput(d, 0);
                openFileOutput.write(b2.toString().getBytes());
                openFileOutput.flush();
                openFileOutput.close();
            } catch (Exception e) {
                Log.b("MobclickAgent", "an error occured while writing report file...", e);
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Throwable th) {
        if (context != null && th != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
                cause.printStackTrace(printWriter);
            }
            String obj = stringWriter.toString();
            printWriter.close();
            a(context, obj);
        }
    }

    public void a(g gVar) {
        this.b = gVar;
    }

    /* access modifiers changed from: package-private */
    public JSONArray b(Context context) {
        JSONArray jSONArray = null;
        if (context != null) {
            File file = new File(context.getFilesDir(), d);
            try {
                if (file.exists()) {
                    FileInputStream openFileInput = context.openFileInput(d);
                    StringBuffer stringBuffer = new StringBuffer();
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = openFileInput.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        stringBuffer.append(new String(bArr, 0, read));
                    }
                    openFileInput.close();
                    if (stringBuffer.length() != 0) {
                        jSONArray = new JSONArray(stringBuffer.toString());
                    }
                    try {
                        file.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return jSONArray;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        a(th);
        if (this.a != null) {
            this.a.uncaughtException(thread, th);
        }
    }
}
