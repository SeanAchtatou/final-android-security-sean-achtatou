package com.umeng.common.b;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.microedition.media.control.ToneControl;

public class g {
    public static final String a = System.getProperty("line.separator");
    private static final String b = "helper";

    public static String a() {
        return a(new Date());
    }

    public static String a(Context context, long j) {
        return j < 1000 ? String.valueOf((int) j) + "B" : j < 1000000 ? String.valueOf(Math.round(((double) ((float) j)) / 1000.0d)) + "K" : j < 1000000000 ? String.valueOf(new DecimalFormat("#0.0").format(((double) ((float) j)) / 1000000.0d)) + "M" : String.valueOf(new DecimalFormat("#0.00").format(((double) ((float) j)) / 1.0E9d)) + "G";
    }

    public static String a(String str) {
        if (str == null) {
            return null;
        }
        try {
            byte[] bytes = str.getBytes();
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(bytes);
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < digest.length; i++) {
                stringBuffer.append(String.format("%02X", Byte.valueOf(digest[i])));
            }
            return stringBuffer.toString();
        } catch (Exception e) {
            return str.replaceAll("[^[a-z][A-Z][0-9][.][_]]", "");
        }
    }

    public static String a(Date date) {
        return date == null ? "" : new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    public static void a(Context context, String str) {
        context.startActivity(context.getPackageManager().getLaunchIntentForPackage(str));
    }

    public static String b(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                stringBuffer.append(Integer.toHexString(b2 & ToneControl.SILENCE));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.i(b, "getMD5 error", e);
            return "";
        }
    }

    public static boolean b(Context context, String str) {
        try {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean c(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean d(String str) {
        if (c(str)) {
            return false;
        }
        String lowerCase = str.trim().toLowerCase();
        return lowerCase.startsWith("http://") || lowerCase.startsWith("https://");
    }
}
