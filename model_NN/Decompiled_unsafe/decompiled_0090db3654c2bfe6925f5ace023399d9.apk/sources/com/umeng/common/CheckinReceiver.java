package com.umeng.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import com.umeng.analytics.MobclickAgent;
import java.util.Calendar;
import java.util.HashMap;

public class CheckinReceiver extends BroadcastReceiver {
    public static final String UMENG_CONFIGURATIONS = "UMENG_CONFIGURATIONS";

    private void checkin(Context context, int i, int i2) {
        context.getApplicationContext().getSharedPreferences("UMENG_CONFIGURATIONS", 0).edit().putBoolean("CHECKIN@" + i + "-" + i2, true).commit();
    }

    private TelephonyManager getTelephonyManager(Context context) {
        return (TelephonyManager) context.getSystemService("phone");
    }

    private boolean hasChecked(Context context, int i, int i2) {
        return context.getApplicationContext().getSharedPreferences("UMENG_CONFIGURATIONS", 0).contains("CHECKIN@" + i + "-" + i2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x0013 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getDeviceICCID(android.content.Context r3) {
        /*
            r2 = this;
            r1 = 0
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            int r0 = r3.checkCallingOrSelfPermission(r0)
            if (r0 != 0) goto L_0x001a
            android.telephony.TelephonyManager r0 = r2.getTelephonyManager(r3)     // Catch:{ Exception -> 0x0016 }
            java.lang.String r0 = r0.getSimSerialNumber()     // Catch:{ Exception -> 0x0016 }
        L_0x0011:
            if (r0 != 0) goto L_0x0015
            java.lang.String r0 = ""
        L_0x0015:
            return r0
        L_0x0016:
            r0 = move-exception
            r0.printStackTrace()
        L_0x001a:
            r0 = r1
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.common.CheckinReceiver.getDeviceICCID(android.content.Context):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x0013 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getDeviceIMEI(android.content.Context r3) {
        /*
            r2 = this;
            r1 = 0
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            int r0 = r3.checkCallingOrSelfPermission(r0)
            if (r0 != 0) goto L_0x001a
            android.telephony.TelephonyManager r0 = r2.getTelephonyManager(r3)     // Catch:{ Exception -> 0x0016 }
            java.lang.String r0 = r0.getDeviceId()     // Catch:{ Exception -> 0x0016 }
        L_0x0011:
            if (r0 != 0) goto L_0x0015
            java.lang.String r0 = ""
        L_0x0015:
            return r0
        L_0x0016:
            r0 = move-exception
            r0.printStackTrace()
        L_0x001a:
            r0 = r1
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.common.CheckinReceiver.getDeviceIMEI(android.content.Context):java.lang.String");
    }

    public final String getDeviceIMSI(Context context) {
        String str;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        try {
            if (telephonyManager.getSimState() == 5 || telephonyManager.getSimState() == 0) {
                str = telephonyManager.getSubscriberId();
                return str == null ? "" : str;
            }
            throw new Exception("Sim card is not ready yet.");
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x0013 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getDevicePhonenumber(android.content.Context r3) {
        /*
            r2 = this;
            r1 = 0
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            int r0 = r3.checkCallingOrSelfPermission(r0)
            if (r0 != 0) goto L_0x001a
            android.telephony.TelephonyManager r0 = r2.getTelephonyManager(r3)     // Catch:{ Exception -> 0x0016 }
            java.lang.String r0 = r0.getLine1Number()     // Catch:{ Exception -> 0x0016 }
        L_0x0011:
            if (r0 != 0) goto L_0x0015
            java.lang.String r0 = ""
        L_0x0015:
            return r0
        L_0x0016:
            r0 = move-exception
            r0.printStackTrace()
        L_0x001a:
            r0 = r1
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.common.CheckinReceiver.getDevicePhonenumber(android.content.Context):java.lang.String");
    }

    public final boolean isConnectedOrConnecting(Context context) {
        return isWifiConnect(context) || isMobileConnect(context);
    }

    public boolean isMobileConnect(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            return true;
        }
        try {
            NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0);
            if (networkInfo.isConnectedOrConnecting() && networkInfo.isAvailable() && getTelephonyManager(context).getDataState() == 2) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isWifiConnect(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != 0) {
            return true;
        }
        try {
            return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isConnectedOrConnecting();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void onReceive(Context context, Intent intent) {
        MobclickAgent.onResume(context);
        Calendar instance = Calendar.getInstance();
        int i = instance.get(1);
        int i2 = instance.get(6);
        if (!hasChecked(context, i, i2)) {
            try {
                HashMap hashMap = new HashMap();
                hashMap.put("PHONENUMBER", getDevicePhonenumber(context));
                hashMap.put("IMEI", getDeviceIMEI(context));
                hashMap.put("IMSI", getDeviceIMSI(context));
                hashMap.put("ICCID", getDeviceICCID(context));
                MobclickAgent.onEvent(context, "DailyCheckin", hashMap);
                checkin(context, i, i2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        MobclickAgent.onPause(context);
    }
}
