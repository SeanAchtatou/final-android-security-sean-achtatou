package com.umeng.common.net;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.umeng.common.Log;
import com.umeng.common.b.g;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class c {
    /* access modifiers changed from: private */
    public static final String a = c.class.getName();
    private static final String b = "umeng_download_task_list";
    private static final String c = "UMENG_DATA";
    private static final String d = "cp";
    private static final String e = "url";
    private static final String f = "progress";
    private static final String g = "last_modified";
    private static final String h = "extra";
    private static Context i = null;
    private static final String j = "yyyy-MM-dd HH:mm:ss";
    private a k;

    class a extends SQLiteOpenHelper {
        private static final int b = 2;
        private static final String c = "CREATE TABLE umeng_download_task_list (cp TEXT, url TEXT, progress INTEGER, extra TEXT, last_modified TEXT, UNIQUE (cp,url) ON CONFLICT ABORT);";

        a(Context context) {
            super(context, c.c, (SQLiteDatabase.CursorFactory) null, 2);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            Log.c(c.a, c);
            sQLiteDatabase.execSQL(c);
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    private static class b {
        public static final c a = new c(null);

        private b() {
        }
    }

    private c() {
        this.k = new a(i);
    }

    /* synthetic */ c(c cVar) {
        this();
    }

    public static c a(Context context) {
        if (i == null && context == null) {
            throw new NullPointerException();
        }
        if (i == null) {
            i = context;
        }
        return b.a;
    }

    public List<String> a(String str) {
        SQLiteDatabase readableDatabase = this.k.getReadableDatabase();
        String[] strArr = {e};
        Cursor query = readableDatabase.query(b, strArr, "cp=?", new String[]{str}, null, null, null, "1");
        ArrayList arrayList = new ArrayList();
        query.moveToFirst();
        while (!query.isAfterLast()) {
            arrayList.add(query.getString(0));
            query.moveToNext();
        }
        query.close();
        return arrayList;
    }

    public void a(int i2) {
        try {
            Date date = new Date(new Date().getTime() - ((long) (i2 * com.a.a.h.b.DEFAULT_MINIMAL_LOCATION_UPDATES)));
            this.k.getWritableDatabase().execSQL(" DELETE FROM umeng_download_task_list WHERE strftime('yyyy-MM-dd HH:mm:ss', last_modified)<=strftime('yyyy-MM-dd HH:mm:ss', '" + new SimpleDateFormat(j).format(date) + "')");
            Log.c(a, "clearOverdueTasks(" + i2 + ")" + " remove all tasks before " + new SimpleDateFormat(j).format(date));
        } catch (Exception e2) {
            Log.b(a, e2.getMessage());
        }
    }

    public void a(String str, String str2, int i2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(f, Integer.valueOf(i2));
        contentValues.put(g, g.a());
        this.k.getWritableDatabase().update(b, contentValues, "cp=? and url=?", new String[]{str, str2});
        Log.c(a, "updateProgress(" + str + ", " + str2 + ", " + i2 + ")");
    }

    public void a(String str, String str2, String str3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(h, str3);
        contentValues.put(g, g.a());
        this.k.getWritableDatabase().update(b, contentValues, "cp=? and url=?", new String[]{str, str2});
        Log.c(a, "updateExtra(" + str + ", " + str2 + ", " + str3 + ")");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean a(String str, String str2) {
        Exception e2;
        boolean z;
        ContentValues contentValues = new ContentValues();
        contentValues.put(d, str);
        contentValues.put(e, str2);
        contentValues.put(f, (Integer) 0);
        contentValues.put(g, g.a());
        try {
            Cursor query = this.k.getReadableDatabase().query(b, new String[]{f}, "cp=? and url=?", new String[]{str, str2}, null, null, null, "1");
            if (query.getCount() > 0) {
                Log.c(a, "insert(" + str + ", " + str2 + "): " + " already exists in the db. Insert is cancelled.");
                z = false;
            } else {
                long insert = this.k.getWritableDatabase().insert(b, null, contentValues);
                boolean z2 = insert != -1;
                try {
                    Log.c(a, "insert(" + str + ", " + str2 + "): " + "rowid=" + insert);
                    z = z2;
                } catch (Exception e3) {
                    Exception exc = e3;
                    z = z2;
                    e2 = exc;
                    Log.c(a, "insert(" + str + ", " + str2 + "): " + e2.getMessage(), e2);
                    return z;
                }
            }
            try {
                query.close();
            } catch (Exception e4) {
                e2 = e4;
                Log.c(a, "insert(" + str + ", " + str2 + "): " + e2.getMessage(), e2);
                return z;
            }
        } catch (Exception e5) {
            e2 = e5;
            z = false;
            Log.c(a, "insert(" + str + ", " + str2 + "): " + e2.getMessage(), e2);
            return z;
        }
        return z;
    }

    public int b(String str, String str2) {
        int i2;
        Cursor query = this.k.getReadableDatabase().query(b, new String[]{f}, "cp=? and url=?", new String[]{str, str2}, null, null, null, "1");
        if (query.getCount() > 0) {
            query.moveToFirst();
            i2 = query.getInt(0);
        } else {
            i2 = -1;
        }
        query.close();
        return i2;
    }

    public String c(String str, String str2) {
        String str3 = null;
        SQLiteDatabase readableDatabase = this.k.getReadableDatabase();
        String[] strArr = {h};
        Cursor query = readableDatabase.query(b, strArr, "cp=? and url=?", new String[]{str, str2}, null, null, null, "1");
        if (query.getCount() > 0) {
            query.moveToFirst();
            str3 = query.getString(0);
        }
        query.close();
        return str3;
    }

    public Date d(String str, String str2) {
        Date date = null;
        SQLiteDatabase readableDatabase = this.k.getReadableDatabase();
        String[] strArr = {g};
        Cursor query = readableDatabase.query(b, strArr, "cp=? and url=?", new String[]{str, str2}, null, null, null, null);
        if (query.getCount() > 0) {
            query.moveToFirst();
            String string = query.getString(0);
            Log.c(a, "getLastModified(" + str + ", " + str2 + "): " + string);
            try {
                date = new SimpleDateFormat(j).parse(string);
            } catch (Exception e2) {
                Log.c(a, e2.getMessage());
            }
        }
        query.close();
        return date;
    }

    public void e(String str, String str2) {
        this.k.getWritableDatabase().delete(b, "cp=? and url=?", new String[]{str, str2});
        Log.c(a, "delete(" + str + ", " + str2 + ")");
    }

    public void finalize() {
        this.k.close();
    }
}
