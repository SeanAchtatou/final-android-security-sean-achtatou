package com.umeng.common.net;

import android.app.Notification;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.umeng.common.Log;
import com.umeng.common.net.DownloadingService;
import com.umeng.common.net.a;

class e implements DownloadingService.a {
    final /* synthetic */ DownloadingService a;

    e(DownloadingService downloadingService) {
        this.a = downloadingService;
    }

    public void a(int i) {
        int i2 = 0;
        if (DownloadingService.r.containsKey(Integer.valueOf(i))) {
            DownloadingService.d dVar = (DownloadingService.d) DownloadingService.r.get(Integer.valueOf(i));
            long[] jArr = dVar.f;
            if (jArr != null && jArr[1] > 0 && (i2 = (int) ((((float) jArr[0]) / ((float) jArr[1])) * 100.0f)) > 100) {
                i2 = 99;
            }
            Notification a2 = this.a.a(dVar.e, i, i2);
            dVar.b = a2;
            this.a.k.notify(i, a2);
        }
    }

    public void a(int i, int i2) {
        if (DownloadingService.r.containsKey(Integer.valueOf(i))) {
            DownloadingService.d dVar = (DownloadingService.d) DownloadingService.r.get(Integer.valueOf(i));
            a.C0002a aVar = dVar.e;
            Notification notification = dVar.b;
            notification.contentView.setProgressBar(com.umeng.common.a.a.c(this.a.m), 100, i2, false);
            notification.contentView.setTextViewText(com.umeng.common.a.a.b(this.a.m), String.valueOf(String.valueOf(i2)) + "%");
            this.a.k.notify(i, notification);
            Log.c(DownloadingService.j, String.format("%3$10s Notification: mNotificationId = %1$15s\t|\tprogress = %2$15s", Integer.valueOf(i), Integer.valueOf(i2), aVar.b));
        }
    }

    public void a(int i, Exception exc) {
        if (DownloadingService.r.containsKey(Integer.valueOf(i))) {
            DownloadingService.d dVar = (DownloadingService.d) DownloadingService.r.get(Integer.valueOf(i));
            a.C0002a aVar = dVar.e;
            Notification notification = dVar.b;
            notification.contentView.setTextViewText(com.umeng.common.a.a.d(this.a.m), String.valueOf(aVar.b) + com.umeng.common.a.i);
            this.a.k.notify(i, notification);
            this.a.a(i);
        }
    }

    public void a(int i, String str) {
        DownloadingService.d dVar;
        if (DownloadingService.r.containsKey(Integer.valueOf(i)) && (dVar = (DownloadingService.d) DownloadingService.r.get(Integer.valueOf(i))) != null) {
            a.C0002a aVar = dVar.e;
            dVar.b.contentView.setTextViewText(com.umeng.common.a.a.b(this.a.m), String.valueOf(String.valueOf(100)) + "%");
            c.a(this.a.m).a(aVar.a, aVar.c, 100);
            Bundle bundle = new Bundle();
            bundle.putString("filename", str);
            Message obtain = Message.obtain();
            obtain.what = 5;
            obtain.arg1 = 1;
            obtain.obj = aVar;
            obtain.arg2 = i;
            obtain.setData(bundle);
            this.a.n.sendMessage(obtain);
            Message obtain2 = Message.obtain();
            obtain2.what = 5;
            obtain2.arg1 = 1;
            obtain2.setData(bundle);
            try {
                if (DownloadingService.q.get(aVar) != null) {
                    ((Messenger) DownloadingService.q.get(aVar)).send(obtain2);
                }
                this.a.a(i);
            } catch (RemoteException e) {
                this.a.a(i);
            }
        }
    }
}
