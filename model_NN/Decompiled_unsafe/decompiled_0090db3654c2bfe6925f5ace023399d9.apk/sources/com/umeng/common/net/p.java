package com.umeng.common.net;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.animation.Animation;
import android.widget.ImageView;
import com.umeng.common.Log;
import com.umeng.common.b.g;
import com.umeng.common.net.o;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Stack;
import java.util.WeakHashMap;

public class p {
    public static boolean a = false;
    /* access modifiers changed from: private */
    public static final String b = p.class.getName();
    private static final long c = 104857600;
    private static final long d = 10485760;
    private static final Map<ImageView, String> e = Collections.synchronizedMap(new WeakHashMap());

    public interface a {
        void a(o.a aVar);

        void a(b bVar);
    }

    public enum b {
        BIND_FORM_CACHE,
        BIND_FROM_NET
    }

    static class c extends AsyncTask<Object, Integer, Drawable> {
        private Context a;
        private String b;
        private ImageView c;
        private b d;
        private boolean e;
        private a f;
        private Animation g;
        private boolean h;
        private File i;

        public c(Context context, ImageView imageView, String str, b bVar, File file, boolean z, a aVar, Animation animation, boolean z2) {
            this.i = file;
            this.a = context;
            this.b = str;
            this.f = aVar;
            this.d = bVar;
            this.e = z;
            this.g = animation;
            this.c = imageView;
            this.h = z2;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Drawable doInBackground(Object... objArr) {
            if (p.a) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            if (this.i == null || !this.i.exists()) {
                try {
                    p.a(this.a, this.b);
                    File b2 = p.b(this.a, this.b);
                    Drawable a2 = (b2 == null || !b2.exists()) ? null : p.c(b2.getAbsolutePath());
                    Log.c(p.b, "get drawable from net else file.");
                    return a2;
                } catch (Exception e3) {
                    Log.e(p.b, e3.toString(), e3);
                    return null;
                }
            } else {
                Drawable a3 = p.c(this.i.getAbsolutePath());
                if (a3 == null) {
                    this.i.delete();
                }
                Log.c(p.b, "get drawable from cacheFile.");
                return a3;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Drawable drawable) {
            p.b(this.a, this.c, drawable, this.e, this.f, this.g, this.h, this.b);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            if (this.f != null) {
                this.f.a(this.d);
            }
        }
    }

    private static Bitmap a(Bitmap bitmap) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            RectF rectF = new RectF(rect);
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(-12434878);
            canvas.drawRoundRect(rectF, (float) (bitmap.getWidth() / 6), (float) (bitmap.getHeight() / 6), paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            bitmap.recycle();
            return createBitmap;
        } catch (OutOfMemoryError e2) {
            Log.e(b, "Cant`t create round corner bitmap. [OutOfMemoryError] ");
            return null;
        }
    }

    public static String a(Context context, String str) {
        File file;
        String canonicalPath;
        long j;
        if (g.c(str)) {
            return null;
        }
        try {
            String str2 = String.valueOf(b(str)) + ".tmp";
            if (com.umeng.common.b.b()) {
                canonicalPath = Environment.getExternalStorageDirectory().getCanonicalPath();
                j = c;
            } else {
                canonicalPath = context.getCacheDir().getCanonicalPath();
                j = d;
            }
            File file2 = new File(String.valueOf(canonicalPath) + com.umeng.common.a.a);
            if (file2.exists()) {
                if (b(file2.getCanonicalFile()) > j) {
                    new Thread(new q(file2)).start();
                }
            } else if (!file2.mkdirs()) {
                Log.b(b, "Failed to create directory" + file2.getAbsolutePath() + ". Check permission. Make sure WRITE_EXTERNAL_STORAGE is added in your Manifest.xml");
            }
            file = new File(file2, str2);
            try {
                file.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                InputStream inputStream = (InputStream) new URL(str).openConnection().getContent();
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        fileOutputStream.flush();
                        inputStream.close();
                        fileOutputStream.close();
                        File file3 = new File(file.getParent(), file.getName().replace(".tmp", ""));
                        file.renameTo(file3);
                        Log.a(b, "download img[" + str + "]  to " + file3.getCanonicalPath());
                        return file3.getCanonicalPath();
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Exception e3) {
            e = e3;
            file = null;
            Log.a(b, String.valueOf(e.getStackTrace().toString()) + "\t url:\t" + g.a + str);
            if (file != null && file.exists()) {
                file.deleteOnExit();
            }
            return null;
        }
    }

    public static void a(Context context, ImageView imageView, String str, boolean z) {
        a(context, imageView, str, z, null, null, false);
    }

    public static void a(Context context, ImageView imageView, String str, boolean z, a aVar) {
        a(context, imageView, str, z, aVar, null, false);
    }

    public static void a(Context context, ImageView imageView, String str, boolean z, a aVar, Animation animation) {
        a(context, imageView, str, z, aVar, null, false);
    }

    public static void a(Context context, ImageView imageView, String str, boolean z, a aVar, Animation animation, boolean z2) {
        if (imageView != null) {
            e.put(imageView, str);
            try {
                File b2 = b(context, str);
                if (b2 == null || !b2.exists() || a) {
                    new c(context, imageView, str, b.BIND_FROM_NET, null, z, aVar, animation, z2).execute(new Object[0]);
                    return;
                }
                if (aVar != null) {
                    aVar.a(b.BIND_FORM_CACHE);
                }
                Drawable c2 = c(b2.getAbsolutePath());
                if (c2 == null) {
                    b2.delete();
                }
                b(context, imageView, c2, z, aVar, animation, z2, str);
            } catch (Exception e2) {
                Log.b(b, "", e2);
                if (aVar != null) {
                    aVar.a(o.a.FAIL);
                }
            }
        }
    }

    private static boolean a(ImageView imageView, String str) {
        String str2 = e.get(imageView);
        return str2 != null && !str2.equals(str);
    }

    private static long b(File file) {
        long length;
        long j = 0;
        if (file == null || !file.exists() || !file.isDirectory()) {
            return 0;
        }
        Stack stack = new Stack();
        stack.clear();
        stack.push(file);
        while (true) {
            long j2 = j;
            if (stack.isEmpty()) {
                return j2;
            }
            File[] listFiles = ((File) stack.pop()).listFiles();
            j = j2;
            int i = 0;
            while (i < listFiles.length) {
                if (listFiles[i].isDirectory()) {
                    stack.push(listFiles[i]);
                    length = j;
                } else {
                    length = listFiles[i].length() + j;
                }
                i++;
                j = length;
            }
        }
    }

    protected static File b(Context context, String str) {
        File file = new File(new File(String.valueOf(com.umeng.common.b.b() ? Environment.getExternalStorageDirectory().getCanonicalPath() : context.getCacheDir().getCanonicalPath()) + com.umeng.common.a.a), b(str));
        if (file.exists()) {
            return file;
        }
        return null;
    }

    private static String b(String str) {
        int lastIndexOf = str.lastIndexOf(".");
        String str2 = "";
        if (lastIndexOf >= 0) {
            str2 = str.substring(lastIndexOf);
        }
        return String.valueOf(g.a(str)) + str2;
    }

    /* access modifiers changed from: private */
    public static synchronized void b(Context context, ImageView imageView, Drawable drawable, boolean z, a aVar, Animation animation, boolean z2, String str) {
        synchronized (p.class) {
            if (z2 && drawable != null) {
                try {
                    drawable = new BitmapDrawable(a(((BitmapDrawable) drawable).getBitmap()));
                } catch (Exception e2) {
                    Log.b(b, "bind failed", e2);
                    if (aVar != null) {
                        aVar.a(o.a.FAIL);
                    }
                }
            }
            if (drawable == null || imageView == null) {
                if (aVar != null) {
                    aVar.a(o.a.FAIL);
                }
                Log.e(b, "bind drawable failed. drawable [" + drawable + "]  imageView[+" + imageView + "+]");
            } else if (!a(imageView, str)) {
                if (z) {
                    imageView.setBackgroundDrawable(drawable);
                } else {
                    imageView.setImageDrawable(drawable);
                }
                if (animation != null) {
                    imageView.startAnimation(animation);
                }
                if (aVar != null) {
                    aVar.a(o.a.SUCCESS);
                }
            } else if (aVar != null) {
                aVar.a(o.a.FAIL);
            }
        }
    }

    /* access modifiers changed from: private */
    public static Drawable c(String str) {
        try {
            return Drawable.createFromPath(str);
        } catch (OutOfMemoryError e2) {
            Log.e(b, "Resutil fetchImage OutOfMemoryError:" + e2.toString());
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static void c(File file) {
        if (file != null && file.exists() && file.canWrite() && file.isDirectory()) {
            File[] listFiles = file.listFiles();
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].isDirectory()) {
                    c(listFiles[i]);
                } else if (new Date().getTime() - listFiles[i].lastModified() > 1800) {
                    listFiles[i].delete();
                }
            }
        }
    }
}
