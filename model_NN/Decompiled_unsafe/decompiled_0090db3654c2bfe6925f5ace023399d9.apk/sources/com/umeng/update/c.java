package com.umeng.update;

import android.content.Context;
import android.content.DialogInterface;

class c implements DialogInterface.OnClickListener {
    private final /* synthetic */ Context a;
    private final /* synthetic */ UpdateResponse b;

    c(Context context, UpdateResponse updateResponse) {
        this.a = context;
        this.b = updateResponse;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        UmengUpdateAgent.j.a(this.a, this.b.path);
    }
}
