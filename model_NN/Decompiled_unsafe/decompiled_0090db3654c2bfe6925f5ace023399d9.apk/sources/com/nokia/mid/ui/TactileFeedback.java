package com.nokia.mid.ui;

public class TactileFeedback {
    public static final int FEEDBACK_STYLE_BASIC = 1;
    public static final int FEEDBACK_STYLE_SENSITIVE = 2;

    public boolean D() {
        return true;
    }

    public void a(Object obj, int i) {
    }

    public void a(Object obj, int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public void b(Object obj, int i) {
    }

    public void c(Object obj) {
    }

    public void s(int i) {
    }
}
