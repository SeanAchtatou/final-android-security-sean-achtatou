package com.nokia.mid.sound;

import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;

public class Sound {
    public static final int FORMAT_TONE = 1;
    public static final int FORMAT_WAV = 5;
    public static final int SOUND_PLAYING = 0;
    public static final int SOUND_STOPPED = 1;
    public static final int SOUND_UNINITIALIZED = 3;
    private int bt = 100;
    private SoundListener bu;
    Player bv;
    private int state = 3;

    public Sound(int i, long j) {
    }

    public Sound(byte[] bArr, int i) {
        a(bArr, i);
    }

    public static int p(int i) {
        return 1;
    }

    public static int[] z() {
        return new int[]{1, 5};
    }

    public void a(SoundListener soundListener) {
        this.bu = soundListener;
    }

    public void a(byte[] bArr, int i) {
        try {
            if (this.bv != null) {
                this.bv.stop();
            }
            this.bv = null;
            this.bv = Manager.a(bArr.length < 3 ? MIDPHelper.b(getClass(), "/short.mp3") : MIDPHelper.b(getClass(), "/long.mp3"), "audio/mpeg");
            this.bv.cX();
            this.bv.cY();
            this.state = 1;
            if (this.bu != null) {
                this.bu.a(this, this.state);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getState() {
        return this.state;
    }

    public void n(int i) {
        if (i == 0) {
            this.bv.aC(-1);
        } else {
            this.bv.aC(i);
        }
        try {
            this.bv.start();
        } catch (MediaException e) {
            e.printStackTrace();
        }
        this.state = 0;
        if (this.bu != null) {
            this.bu.a(this, this.state);
        }
    }

    public void o(int i) {
        this.bt = i;
    }

    public void release() {
        try {
            if (this.bv != null) {
                this.bv.stop();
            }
            this.bv = null;
            this.state = 3;
            if (this.bu != null) {
                this.bu.a(this, this.state);
            }
        } catch (MediaException e) {
            e.printStackTrace();
        }
    }

    public void resume() {
        try {
            this.bv.stop();
            this.bv.start();
        } catch (MediaException e) {
            e.printStackTrace();
        }
        this.state = 0;
        if (this.bu != null) {
            this.bu.a(this, this.state);
        }
    }

    public void stop() {
        try {
            if (this.bv != null) {
                this.bv.stop();
            }
            this.state = 1;
            if (this.bu != null) {
                this.bu.a(this, this.state);
            }
        } catch (MediaException e) {
            e.printStackTrace();
        }
    }

    public int y() {
        return this.bt;
    }
}
