package com.fasterxml.jackson.core.io;

import com.a.a.c.p;
import java.io.CharConversionException;
import java.io.InputStream;
import javax.microedition.media.control.ToneControl;

public final class UTF32Reader extends BaseReader {
    final boolean mBigEndian;
    int mByteCount = 0;
    int mCharCount = 0;
    char mSurrogate = 0;

    public UTF32Reader(IOContext iOContext, InputStream inputStream, byte[] bArr, int i, int i2, boolean z) {
        super(iOContext, inputStream, bArr, i, i2);
        this.mBigEndian = z;
    }

    private boolean loadMore(int i) {
        this.mByteCount += this._length - i;
        if (i > 0) {
            if (this._ptr > 0) {
                for (int i2 = 0; i2 < i; i2++) {
                    this._buffer[i2] = this._buffer[this._ptr + i2];
                }
                this._ptr = 0;
            }
            this._length = i;
        } else {
            this._ptr = 0;
            int read = this._in.read(this._buffer);
            if (read < 1) {
                this._length = 0;
                if (read < 0) {
                    freeBuffers();
                    return false;
                }
                reportStrangeStream();
            }
            this._length = read;
        }
        while (this._length < 4) {
            int read2 = this._in.read(this._buffer, this._length, this._buffer.length - this._length);
            if (read2 < 1) {
                if (read2 < 0) {
                    freeBuffers();
                    reportUnexpectedEOF(this._length, 4);
                }
                reportStrangeStream();
            }
            this._length = read2 + this._length;
        }
        return true;
    }

    private void reportInvalid(int i, int i2, String str) {
        throw new CharConversionException("Invalid UTF-32 character 0x" + Integer.toHexString(i) + str + " at char #" + (this.mCharCount + i2) + ", byte #" + ((this.mByteCount + this._ptr) - 1) + ")");
    }

    private void reportUnexpectedEOF(int i, int i2) {
        throw new CharConversionException("Unexpected EOF in the middle of a 4-byte UTF-32 char: got " + i + ", needed " + i2 + ", at char #" + this.mCharCount + ", byte #" + (this.mByteCount + i) + ")");
    }

    public /* bridge */ /* synthetic */ void close() {
        super.close();
    }

    public /* bridge */ /* synthetic */ int read() {
        return super.read();
    }

    public int read(char[] cArr, int i, int i2) {
        int i3;
        int i4;
        byte b;
        if (this._buffer == null) {
            return -1;
        }
        if (i2 < 1) {
            return i2;
        }
        if (i < 0 || i + i2 > cArr.length) {
            reportBounds(cArr, i, i2);
        }
        int i5 = i2 + i;
        if (this.mSurrogate != 0) {
            i3 = i + 1;
            cArr[i] = this.mSurrogate;
            this.mSurrogate = 0;
        } else {
            int i6 = this._length - this._ptr;
            if (i6 < 4 && !loadMore(i6)) {
                return -1;
            }
            i3 = i;
        }
        while (true) {
            if (i3 >= i5) {
                i4 = i3;
                break;
            }
            int i7 = this._ptr;
            if (this.mBigEndian) {
                b = (this._buffer[i7 + 3] & ToneControl.SILENCE) | (this._buffer[i7] << 24) | ((this._buffer[i7 + 1] & ToneControl.SILENCE) << 16) | ((this._buffer[i7 + 2] & ToneControl.SILENCE) << 8);
            } else {
                b = (this._buffer[i7 + 3] << 24) | (this._buffer[i7] & ToneControl.SILENCE) | ((this._buffer[i7 + 1] & ToneControl.SILENCE) << 8) | ((this._buffer[i7 + 2] & ToneControl.SILENCE) << 16);
            }
            this._ptr += 4;
            if (b > 65535) {
                if (b > 1114111) {
                    reportInvalid(b, i3 - i, "(above " + Integer.toHexString(1114111) + ") ");
                }
                int i8 = b - p.DELAY;
                i4 = i3 + 1;
                cArr[i3] = (char) (55296 + (i8 >> 10));
                b = (i8 & ToneControl.SILENCE) | p.DELAY;
                if (i4 >= i5) {
                    this.mSurrogate = (char) b;
                    break;
                }
            } else {
                i4 = i3;
            }
            i3 = i4 + 1;
            cArr[i4] = (char) b;
            if (this._ptr >= this._length) {
                i4 = i3;
                break;
            }
        }
        int i9 = i4 - i;
        this.mCharCount += i9;
        return i9;
    }
}
