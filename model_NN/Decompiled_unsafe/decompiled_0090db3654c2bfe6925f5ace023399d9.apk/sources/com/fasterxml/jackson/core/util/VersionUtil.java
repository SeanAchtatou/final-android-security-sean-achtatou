package com.fasterxml.jackson.core.util;

import com.a.a.m.m;
import com.fasterxml.jackson.core.Version;
import com.umeng.common.b.e;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.regex.Pattern;

public class VersionUtil {
    public static final String VERSION_FILE = "VERSION.txt";
    private static final Pattern VERSION_SEPARATOR = Pattern.compile("[-_./;:]");
    private final Version _version;

    protected VersionUtil() {
        Version version = null;
        try {
            version = versionFor(getClass());
        } catch (Exception e) {
            System.err.println("ERROR: Failed to load Version information for bundle (via " + getClass().getName() + ").");
        }
        this._version = version == null ? Version.unknownVersion() : version;
    }

    public static Version mavenVersionFor(ClassLoader classLoader, String str, String str2) {
        InputStream resourceAsStream = classLoader.getResourceAsStream("META-INF/maven/" + str.replaceAll("\\.", "/") + "/" + str2 + "/pom.properties");
        if (resourceAsStream != null) {
            try {
                Properties properties = new Properties();
                properties.load(resourceAsStream);
                Version parseVersion = parseVersion(properties.getProperty(m.PROP_VERSION), properties.getProperty("groupId"), properties.getProperty("artifactId"));
                try {
                    resourceAsStream.close();
                    return parseVersion;
                } catch (IOException e) {
                    return parseVersion;
                }
            } catch (IOException e2) {
                try {
                    resourceAsStream.close();
                } catch (IOException e3) {
                }
            } catch (Throwable th) {
                try {
                    resourceAsStream.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        }
        return Version.unknownVersion();
    }

    @Deprecated
    public static Version parseVersion(String str) {
        return parseVersion(str, null, null);
    }

    public static Version parseVersion(String str, String str2, String str3) {
        String str4 = null;
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            return null;
        }
        String[] split = VERSION_SEPARATOR.split(trim);
        int parseVersionPart = parseVersionPart(split[0]);
        int parseVersionPart2 = split.length > 1 ? parseVersionPart(split[1]) : 0;
        int parseVersionPart3 = split.length > 2 ? parseVersionPart(split[2]) : 0;
        if (split.length > 3) {
            str4 = split[3];
        }
        return new Version(parseVersionPart, parseVersionPart2, parseVersionPart3, str4, str2, str3);
    }

    protected static int parseVersionPart(String str) {
        String str2 = str.toString();
        int length = str2.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str2.charAt(i2);
            if (charAt > '9' || charAt < '0') {
                break;
            }
            i = (i * 10) + (charAt - '0');
        }
        return i;
    }

    public static Version versionFor(Class<?> cls) {
        InputStream resourceAsStream;
        String str;
        String str2;
        Version version = null;
        try {
            resourceAsStream = cls.getResourceAsStream(VERSION_FILE);
            if (resourceAsStream != null) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream, e.f));
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    String readLine2 = bufferedReader.readLine();
                    if (readLine2 != null) {
                        str2 = readLine2.trim();
                        str = bufferedReader.readLine();
                        if (str != null) {
                            str = str.trim();
                        }
                    } else {
                        str2 = readLine2;
                        str = null;
                    }
                } else {
                    str = null;
                    str2 = null;
                }
                version = parseVersion(readLine, str2, str);
                resourceAsStream.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        } catch (IOException e3) {
        } catch (Throwable th) {
            resourceAsStream.close();
            throw th;
        }
        return version == null ? Version.unknownVersion() : version;
    }

    public Version version() {
        return this._version;
    }
}
