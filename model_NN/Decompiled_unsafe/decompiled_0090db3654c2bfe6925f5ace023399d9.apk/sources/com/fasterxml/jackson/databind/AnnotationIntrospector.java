package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.Versioned;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.NoClass;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import com.fasterxml.jackson.databind.introspect.NopAnnotationIntrospector;
import com.fasterxml.jackson.databind.introspect.ObjectIdInfo;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.jsontype.TypeResolverBuilder;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class AnnotationIntrospector implements Versioned {

    public static class Pair extends AnnotationIntrospector {
        protected final AnnotationIntrospector _primary;
        protected final AnnotationIntrospector _secondary;

        public Pair(AnnotationIntrospector annotationIntrospector, AnnotationIntrospector annotationIntrospector2) {
            this._primary = annotationIntrospector;
            this._secondary = annotationIntrospector2;
        }

        public static AnnotationIntrospector create(AnnotationIntrospector annotationIntrospector, AnnotationIntrospector annotationIntrospector2) {
            return annotationIntrospector == null ? annotationIntrospector2 : annotationIntrospector2 == null ? annotationIntrospector : new Pair(annotationIntrospector, annotationIntrospector2);
        }

        public Collection<AnnotationIntrospector> allIntrospectors() {
            return allIntrospectors(new ArrayList());
        }

        public Collection<AnnotationIntrospector> allIntrospectors(Collection<AnnotationIntrospector> collection) {
            this._primary.allIntrospectors(collection);
            this._secondary.allIntrospectors(collection);
            return collection;
        }

        public VisibilityChecker<?> findAutoDetectVisibility(AnnotatedClass annotatedClass, VisibilityChecker<?> visibilityChecker) {
            return this._primary.findAutoDetectVisibility(annotatedClass, this._secondary.findAutoDetectVisibility(annotatedClass, visibilityChecker));
        }

        public Object findContentDeserializer(Annotated annotated) {
            Object findContentDeserializer = this._primary.findContentDeserializer(annotated);
            return (findContentDeserializer == null || findContentDeserializer == JsonDeserializer.None.class || findContentDeserializer == NoClass.class) ? this._secondary.findContentDeserializer(annotated) : findContentDeserializer;
        }

        public Object findContentSerializer(Annotated annotated) {
            Object findContentSerializer = this._primary.findContentSerializer(annotated);
            return (findContentSerializer == null || findContentSerializer == JsonSerializer.None.class || findContentSerializer == NoClass.class) ? this._secondary.findContentSerializer(annotated) : findContentSerializer;
        }

        public Class<?> findDeserializationContentType(Annotated annotated, JavaType javaType) {
            Class<?> findDeserializationContentType = this._primary.findDeserializationContentType(annotated, javaType);
            return findDeserializationContentType == null ? this._secondary.findDeserializationContentType(annotated, javaType) : findDeserializationContentType;
        }

        public Class<?> findDeserializationKeyType(Annotated annotated, JavaType javaType) {
            Class<?> findDeserializationKeyType = this._primary.findDeserializationKeyType(annotated, javaType);
            return findDeserializationKeyType == null ? this._secondary.findDeserializationKeyType(annotated, javaType) : findDeserializationKeyType;
        }

        public String findDeserializationName(AnnotatedField annotatedField) {
            String findDeserializationName;
            String findDeserializationName2 = this._primary.findDeserializationName(annotatedField);
            return findDeserializationName2 == null ? this._secondary.findDeserializationName(annotatedField) : (findDeserializationName2.length() != 0 || (findDeserializationName = this._secondary.findDeserializationName(annotatedField)) == null) ? findDeserializationName2 : findDeserializationName;
        }

        public String findDeserializationName(AnnotatedMethod annotatedMethod) {
            String findDeserializationName;
            String findDeserializationName2 = this._primary.findDeserializationName(annotatedMethod);
            return findDeserializationName2 == null ? this._secondary.findDeserializationName(annotatedMethod) : (findDeserializationName2.length() != 0 || (findDeserializationName = this._secondary.findDeserializationName(annotatedMethod)) == null) ? findDeserializationName2 : findDeserializationName;
        }

        public String findDeserializationName(AnnotatedParameter annotatedParameter) {
            String findDeserializationName = this._primary.findDeserializationName(annotatedParameter);
            return findDeserializationName == null ? this._secondary.findDeserializationName(annotatedParameter) : findDeserializationName;
        }

        public Class<?> findDeserializationType(Annotated annotated, JavaType javaType) {
            Class<?> findDeserializationType = this._primary.findDeserializationType(annotated, javaType);
            return findDeserializationType == null ? this._secondary.findDeserializationType(annotated, javaType) : findDeserializationType;
        }

        public Object findDeserializer(Annotated annotated) {
            Object findDeserializer = this._primary.findDeserializer(annotated);
            return findDeserializer == null ? this._secondary.findDeserializer(annotated) : findDeserializer;
        }

        public String findEnumValue(Enum<?> enumR) {
            String findEnumValue = this._primary.findEnumValue(enumR);
            return findEnumValue == null ? this._secondary.findEnumValue(enumR) : findEnumValue;
        }

        public Object findFilterId(AnnotatedClass annotatedClass) {
            Object findFilterId = this._primary.findFilterId(annotatedClass);
            return findFilterId == null ? this._secondary.findFilterId(annotatedClass) : findFilterId;
        }

        public JsonFormat.Value findFormat(AnnotatedMember annotatedMember) {
            JsonFormat.Value findFormat = this._primary.findFormat(annotatedMember);
            return findFormat == null ? this._secondary.findFormat(annotatedMember) : findFormat;
        }

        public Boolean findIgnoreUnknownProperties(AnnotatedClass annotatedClass) {
            Boolean findIgnoreUnknownProperties = this._primary.findIgnoreUnknownProperties(annotatedClass);
            return findIgnoreUnknownProperties == null ? this._secondary.findIgnoreUnknownProperties(annotatedClass) : findIgnoreUnknownProperties;
        }

        public Object findInjectableValueId(AnnotatedMember annotatedMember) {
            Object findInjectableValueId = this._primary.findInjectableValueId(annotatedMember);
            return findInjectableValueId == null ? this._secondary.findInjectableValueId(annotatedMember) : findInjectableValueId;
        }

        public Object findKeyDeserializer(Annotated annotated) {
            Object findKeyDeserializer = this._primary.findKeyDeserializer(annotated);
            return (findKeyDeserializer == null || findKeyDeserializer == KeyDeserializer.None.class || findKeyDeserializer == NoClass.class) ? this._secondary.findKeyDeserializer(annotated) : findKeyDeserializer;
        }

        public Object findKeySerializer(Annotated annotated) {
            Object findKeySerializer = this._primary.findKeySerializer(annotated);
            return (findKeySerializer == null || findKeySerializer == JsonSerializer.None.class || findKeySerializer == NoClass.class) ? this._secondary.findKeySerializer(annotated) : findKeySerializer;
        }

        public ObjectIdInfo findObjectIdInfo(Annotated annotated) {
            ObjectIdInfo findObjectIdInfo = this._primary.findObjectIdInfo(annotated);
            return findObjectIdInfo == null ? this._secondary.findObjectIdInfo(annotated) : findObjectIdInfo;
        }

        public Class<?> findPOJOBuilder(AnnotatedClass annotatedClass) {
            Class<?> findPOJOBuilder = this._primary.findPOJOBuilder(annotatedClass);
            return findPOJOBuilder == null ? this._secondary.findPOJOBuilder(annotatedClass) : findPOJOBuilder;
        }

        public JsonPOJOBuilder.Value findPOJOBuilderConfig(AnnotatedClass annotatedClass) {
            JsonPOJOBuilder.Value findPOJOBuilderConfig = this._primary.findPOJOBuilderConfig(annotatedClass);
            return findPOJOBuilderConfig == null ? this._secondary.findPOJOBuilderConfig(annotatedClass) : findPOJOBuilderConfig;
        }

        public String[] findPropertiesToIgnore(Annotated annotated) {
            String[] findPropertiesToIgnore = this._primary.findPropertiesToIgnore(annotated);
            return findPropertiesToIgnore == null ? this._secondary.findPropertiesToIgnore(annotated) : findPropertiesToIgnore;
        }

        public TypeResolverBuilder<?> findPropertyContentTypeResolver(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
            TypeResolverBuilder<?> findPropertyContentTypeResolver = this._primary.findPropertyContentTypeResolver(mapperConfig, annotatedMember, javaType);
            return findPropertyContentTypeResolver == null ? this._secondary.findPropertyContentTypeResolver(mapperConfig, annotatedMember, javaType) : findPropertyContentTypeResolver;
        }

        public TypeResolverBuilder<?> findPropertyTypeResolver(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
            TypeResolverBuilder<?> findPropertyTypeResolver = this._primary.findPropertyTypeResolver(mapperConfig, annotatedMember, javaType);
            return findPropertyTypeResolver == null ? this._secondary.findPropertyTypeResolver(mapperConfig, annotatedMember, javaType) : findPropertyTypeResolver;
        }

        public ReferenceProperty findReferenceType(AnnotatedMember annotatedMember) {
            ReferenceProperty findReferenceType = this._primary.findReferenceType(annotatedMember);
            return findReferenceType == null ? this._secondary.findReferenceType(annotatedMember) : findReferenceType;
        }

        public String findRootName(AnnotatedClass annotatedClass) {
            String findRootName;
            String findRootName2 = this._primary.findRootName(annotatedClass);
            return findRootName2 == null ? this._secondary.findRootName(annotatedClass) : (findRootName2.length() > 0 || (findRootName = this._secondary.findRootName(annotatedClass)) == null) ? findRootName2 : findRootName;
        }

        public Class<?> findSerializationContentType(Annotated annotated, JavaType javaType) {
            Class<?> findSerializationContentType = this._primary.findSerializationContentType(annotated, javaType);
            return findSerializationContentType == null ? this._secondary.findSerializationContentType(annotated, javaType) : findSerializationContentType;
        }

        public JsonInclude.Include findSerializationInclusion(Annotated annotated, JsonInclude.Include include) {
            return this._primary.findSerializationInclusion(annotated, this._secondary.findSerializationInclusion(annotated, include));
        }

        public Class<?> findSerializationKeyType(Annotated annotated, JavaType javaType) {
            Class<?> findSerializationKeyType = this._primary.findSerializationKeyType(annotated, javaType);
            return findSerializationKeyType == null ? this._secondary.findSerializationKeyType(annotated, javaType) : findSerializationKeyType;
        }

        public String findSerializationName(AnnotatedField annotatedField) {
            String findSerializationName;
            String findSerializationName2 = this._primary.findSerializationName(annotatedField);
            return findSerializationName2 == null ? this._secondary.findSerializationName(annotatedField) : (findSerializationName2.length() != 0 || (findSerializationName = this._secondary.findSerializationName(annotatedField)) == null) ? findSerializationName2 : findSerializationName;
        }

        public String findSerializationName(AnnotatedMethod annotatedMethod) {
            String findSerializationName;
            String findSerializationName2 = this._primary.findSerializationName(annotatedMethod);
            return findSerializationName2 == null ? this._secondary.findSerializationName(annotatedMethod) : (findSerializationName2.length() != 0 || (findSerializationName = this._secondary.findSerializationName(annotatedMethod)) == null) ? findSerializationName2 : findSerializationName;
        }

        public String[] findSerializationPropertyOrder(AnnotatedClass annotatedClass) {
            String[] findSerializationPropertyOrder = this._primary.findSerializationPropertyOrder(annotatedClass);
            return findSerializationPropertyOrder == null ? this._secondary.findSerializationPropertyOrder(annotatedClass) : findSerializationPropertyOrder;
        }

        public Boolean findSerializationSortAlphabetically(AnnotatedClass annotatedClass) {
            Boolean findSerializationSortAlphabetically = this._primary.findSerializationSortAlphabetically(annotatedClass);
            return findSerializationSortAlphabetically == null ? this._secondary.findSerializationSortAlphabetically(annotatedClass) : findSerializationSortAlphabetically;
        }

        public Class<?> findSerializationType(Annotated annotated) {
            Class<?> findSerializationType = this._primary.findSerializationType(annotated);
            return findSerializationType == null ? this._secondary.findSerializationType(annotated) : findSerializationType;
        }

        public JsonSerialize.Typing findSerializationTyping(Annotated annotated) {
            JsonSerialize.Typing findSerializationTyping = this._primary.findSerializationTyping(annotated);
            return findSerializationTyping == null ? this._secondary.findSerializationTyping(annotated) : findSerializationTyping;
        }

        public Object findSerializer(Annotated annotated) {
            Object findSerializer = this._primary.findSerializer(annotated);
            return findSerializer == null ? this._secondary.findSerializer(annotated) : findSerializer;
        }

        public List<NamedType> findSubtypes(Annotated annotated) {
            List<NamedType> findSubtypes = this._primary.findSubtypes(annotated);
            List<NamedType> findSubtypes2 = this._secondary.findSubtypes(annotated);
            if (findSubtypes == null || findSubtypes.isEmpty()) {
                return findSubtypes2;
            }
            if (findSubtypes2 == null || findSubtypes2.isEmpty()) {
                return findSubtypes;
            }
            ArrayList arrayList = new ArrayList(findSubtypes.size() + findSubtypes2.size());
            arrayList.addAll(findSubtypes);
            arrayList.addAll(findSubtypes2);
            return arrayList;
        }

        public String findTypeName(AnnotatedClass annotatedClass) {
            String findTypeName = this._primary.findTypeName(annotatedClass);
            return (findTypeName == null || findTypeName.length() == 0) ? this._secondary.findTypeName(annotatedClass) : findTypeName;
        }

        public TypeResolverBuilder<?> findTypeResolver(MapperConfig<?> mapperConfig, AnnotatedClass annotatedClass, JavaType javaType) {
            TypeResolverBuilder<?> findTypeResolver = this._primary.findTypeResolver(mapperConfig, annotatedClass, javaType);
            return findTypeResolver == null ? this._secondary.findTypeResolver(mapperConfig, annotatedClass, javaType) : findTypeResolver;
        }

        public NameTransformer findUnwrappingNameTransformer(AnnotatedMember annotatedMember) {
            NameTransformer findUnwrappingNameTransformer = this._primary.findUnwrappingNameTransformer(annotatedMember);
            return findUnwrappingNameTransformer == null ? this._secondary.findUnwrappingNameTransformer(annotatedMember) : findUnwrappingNameTransformer;
        }

        public Object findValueInstantiator(AnnotatedClass annotatedClass) {
            Object findValueInstantiator = this._primary.findValueInstantiator(annotatedClass);
            return findValueInstantiator == null ? this._secondary.findValueInstantiator(annotatedClass) : findValueInstantiator;
        }

        public Class<?>[] findViews(Annotated annotated) {
            Class<?>[] findViews = this._primary.findViews(annotated);
            return findViews == null ? this._secondary.findViews(annotated) : findViews;
        }

        public boolean hasAnyGetterAnnotation(AnnotatedMethod annotatedMethod) {
            return this._primary.hasAnyGetterAnnotation(annotatedMethod) || this._secondary.hasAnyGetterAnnotation(annotatedMethod);
        }

        public boolean hasAnySetterAnnotation(AnnotatedMethod annotatedMethod) {
            return this._primary.hasAnySetterAnnotation(annotatedMethod) || this._secondary.hasAnySetterAnnotation(annotatedMethod);
        }

        public boolean hasAsValueAnnotation(AnnotatedMethod annotatedMethod) {
            return this._primary.hasAsValueAnnotation(annotatedMethod) || this._secondary.hasAsValueAnnotation(annotatedMethod);
        }

        public boolean hasCreatorAnnotation(Annotated annotated) {
            return this._primary.hasCreatorAnnotation(annotated) || this._secondary.hasCreatorAnnotation(annotated);
        }

        public boolean hasIgnoreMarker(AnnotatedMember annotatedMember) {
            return this._primary.hasIgnoreMarker(annotatedMember) || this._secondary.hasIgnoreMarker(annotatedMember);
        }

        public Boolean hasRequiredMarker(AnnotatedMember annotatedMember) {
            Boolean hasRequiredMarker = this._primary.hasRequiredMarker(annotatedMember);
            return hasRequiredMarker == null ? this._secondary.hasRequiredMarker(annotatedMember) : hasRequiredMarker;
        }

        public boolean isAnnotationBundle(Annotation annotation) {
            return this._primary.isAnnotationBundle(annotation) || this._secondary.isAnnotationBundle(annotation);
        }

        public boolean isHandled(Annotation annotation) {
            return this._primary.isHandled(annotation) || this._secondary.isHandled(annotation);
        }

        public Boolean isIgnorableType(AnnotatedClass annotatedClass) {
            Boolean isIgnorableType = this._primary.isIgnorableType(annotatedClass);
            return isIgnorableType == null ? this._secondary.isIgnorableType(annotatedClass) : isIgnorableType;
        }

        public Boolean isTypeId(AnnotatedMember annotatedMember) {
            Boolean isTypeId = this._primary.isTypeId(annotatedMember);
            return isTypeId == null ? this._secondary.isTypeId(annotatedMember) : isTypeId;
        }

        public Version version() {
            return this._primary.version();
        }
    }

    public static class ReferenceProperty {
        private final String _name;
        private final Type _type;

        public enum Type {
            MANAGED_REFERENCE,
            BACK_REFERENCE
        }

        public ReferenceProperty(Type type, String str) {
            this._type = type;
            this._name = str;
        }

        public static ReferenceProperty back(String str) {
            return new ReferenceProperty(Type.BACK_REFERENCE, str);
        }

        public static ReferenceProperty managed(String str) {
            return new ReferenceProperty(Type.MANAGED_REFERENCE, str);
        }

        public String getName() {
            return this._name;
        }

        public Type getType() {
            return this._type;
        }

        public boolean isBackReference() {
            return this._type == Type.BACK_REFERENCE;
        }

        public boolean isManagedReference() {
            return this._type == Type.MANAGED_REFERENCE;
        }
    }

    public static AnnotationIntrospector nopInstance() {
        return NopAnnotationIntrospector.instance;
    }

    public static AnnotationIntrospector pair(AnnotationIntrospector annotationIntrospector, AnnotationIntrospector annotationIntrospector2) {
        return new Pair(annotationIntrospector, annotationIntrospector2);
    }

    public Collection<AnnotationIntrospector> allIntrospectors() {
        return Collections.singletonList(this);
    }

    public Collection<AnnotationIntrospector> allIntrospectors(Collection<AnnotationIntrospector> collection) {
        collection.add(this);
        return collection;
    }

    public VisibilityChecker<?> findAutoDetectVisibility(AnnotatedClass annotatedClass, VisibilityChecker<?> visibilityChecker) {
        return visibilityChecker;
    }

    public Object findContentDeserializer(Annotated annotated) {
        return null;
    }

    public Object findContentSerializer(Annotated annotated) {
        return null;
    }

    public Class<?> findDeserializationContentType(Annotated annotated, JavaType javaType) {
        return null;
    }

    public Class<?> findDeserializationKeyType(Annotated annotated, JavaType javaType) {
        return null;
    }

    public String findDeserializationName(AnnotatedField annotatedField) {
        return null;
    }

    public String findDeserializationName(AnnotatedMethod annotatedMethod) {
        return null;
    }

    public String findDeserializationName(AnnotatedParameter annotatedParameter) {
        return null;
    }

    public Class<?> findDeserializationType(Annotated annotated, JavaType javaType) {
        return null;
    }

    public Object findDeserializer(Annotated annotated) {
        return null;
    }

    public String findEnumValue(Enum<?> enumR) {
        return null;
    }

    public Object findFilterId(AnnotatedClass annotatedClass) {
        return null;
    }

    public JsonFormat.Value findFormat(AnnotatedMember annotatedMember) {
        return null;
    }

    public Boolean findIgnoreUnknownProperties(AnnotatedClass annotatedClass) {
        return null;
    }

    public Object findInjectableValueId(AnnotatedMember annotatedMember) {
        return null;
    }

    public Object findKeyDeserializer(Annotated annotated) {
        return null;
    }

    public Object findKeySerializer(Annotated annotated) {
        return null;
    }

    public ObjectIdInfo findObjectIdInfo(Annotated annotated) {
        return null;
    }

    public Class<?> findPOJOBuilder(AnnotatedClass annotatedClass) {
        return null;
    }

    public JsonPOJOBuilder.Value findPOJOBuilderConfig(AnnotatedClass annotatedClass) {
        return null;
    }

    public String[] findPropertiesToIgnore(Annotated annotated) {
        return null;
    }

    public TypeResolverBuilder<?> findPropertyContentTypeResolver(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
        return null;
    }

    public TypeResolverBuilder<?> findPropertyTypeResolver(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, JavaType javaType) {
        return null;
    }

    public ReferenceProperty findReferenceType(AnnotatedMember annotatedMember) {
        return null;
    }

    public String findRootName(AnnotatedClass annotatedClass) {
        return null;
    }

    public Class<?> findSerializationContentType(Annotated annotated, JavaType javaType) {
        return null;
    }

    public JsonInclude.Include findSerializationInclusion(Annotated annotated, JsonInclude.Include include) {
        return include;
    }

    public Class<?> findSerializationKeyType(Annotated annotated, JavaType javaType) {
        return null;
    }

    public String findSerializationName(AnnotatedField annotatedField) {
        return null;
    }

    public String findSerializationName(AnnotatedMethod annotatedMethod) {
        return null;
    }

    public String[] findSerializationPropertyOrder(AnnotatedClass annotatedClass) {
        return null;
    }

    public Boolean findSerializationSortAlphabetically(AnnotatedClass annotatedClass) {
        return null;
    }

    public Class<?> findSerializationType(Annotated annotated) {
        return null;
    }

    public JsonSerialize.Typing findSerializationTyping(Annotated annotated) {
        return null;
    }

    public Object findSerializer(Annotated annotated) {
        return null;
    }

    public List<NamedType> findSubtypes(Annotated annotated) {
        return null;
    }

    public String findTypeName(AnnotatedClass annotatedClass) {
        return null;
    }

    public TypeResolverBuilder<?> findTypeResolver(MapperConfig<?> mapperConfig, AnnotatedClass annotatedClass, JavaType javaType) {
        return null;
    }

    public NameTransformer findUnwrappingNameTransformer(AnnotatedMember annotatedMember) {
        return null;
    }

    public Object findValueInstantiator(AnnotatedClass annotatedClass) {
        return null;
    }

    public Class<?>[] findViews(Annotated annotated) {
        return null;
    }

    public boolean hasAnyGetterAnnotation(AnnotatedMethod annotatedMethod) {
        return false;
    }

    public boolean hasAnySetterAnnotation(AnnotatedMethod annotatedMethod) {
        return false;
    }

    public boolean hasAsValueAnnotation(AnnotatedMethod annotatedMethod) {
        return false;
    }

    public boolean hasCreatorAnnotation(Annotated annotated) {
        return false;
    }

    public boolean hasIgnoreMarker(AnnotatedMember annotatedMember) {
        return false;
    }

    public Boolean hasRequiredMarker(AnnotatedMember annotatedMember) {
        return null;
    }

    public boolean isAnnotationBundle(Annotation annotation) {
        return false;
    }

    public boolean isHandled(Annotation annotation) {
        return false;
    }

    public Boolean isIgnorableType(AnnotatedClass annotatedClass) {
        return null;
    }

    public Boolean isTypeId(AnnotatedMember annotatedMember) {
        return null;
    }

    public abstract Version version();
}
