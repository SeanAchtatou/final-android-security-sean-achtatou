package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.cfg.DatabindVersion;

public abstract class NopAnnotationIntrospector extends AnnotationIntrospector {
    public static final NopAnnotationIntrospector instance = new NopAnnotationIntrospector() {
        public Version version() {
            return DatabindVersion.instance.version();
        }
    };

    public Version version() {
        return Version.unknownVersion();
    }
}
