package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

public final class MissingNode extends BaseJsonNode {
    private static final MissingNode instance = new MissingNode();

    private MissingNode() {
    }

    public static MissingNode getInstance() {
        return instance;
    }

    public String asText() {
        return "";
    }

    public JsonToken asToken() {
        return JsonToken.NOT_AVAILABLE;
    }

    public <T extends JsonNode> T deepCopy() {
        return this;
    }

    public boolean equals(Object obj) {
        return obj == this;
    }

    public boolean isMissingNode() {
        return true;
    }

    public JsonNode path(int i) {
        return this;
    }

    public JsonNode path(String str) {
        return this;
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        jsonGenerator.writeNull();
    }

    public void serializeWithType(JsonGenerator jsonGenerator, SerializerProvider serializerProvider, TypeSerializer typeSerializer) {
        jsonGenerator.writeNull();
    }

    public String toString() {
        return "";
    }
}
