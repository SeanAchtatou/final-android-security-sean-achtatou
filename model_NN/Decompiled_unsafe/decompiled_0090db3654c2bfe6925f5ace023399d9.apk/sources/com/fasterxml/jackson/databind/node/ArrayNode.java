package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.node.ContainerNode;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ArrayNode extends ContainerNode<ArrayNode> {
    protected ArrayList<JsonNode> _children;

    public ArrayNode(JsonNodeFactory jsonNodeFactory) {
        super(jsonNodeFactory);
    }

    protected ArrayNode(JsonNodeFactory jsonNodeFactory, ArrayList<JsonNode> arrayList) {
        super(jsonNodeFactory);
        this._children = arrayList;
    }

    private ArrayNode _add(JsonNode jsonNode) {
        if (this._children == null) {
            this._children = new ArrayList<>();
        }
        this._children.add(jsonNode);
        return this;
    }

    private ArrayNode _insert(int i, JsonNode jsonNode) {
        if (this._children == null) {
            this._children = new ArrayList<>();
            this._children.add(jsonNode);
        } else if (i < 0) {
            this._children.add(0, jsonNode);
        } else if (i >= this._children.size()) {
            this._children.add(jsonNode);
        } else {
            this._children.add(i, jsonNode);
        }
        return this;
    }

    private boolean _sameChildren(ArrayList<JsonNode> arrayList) {
        int size = arrayList.size();
        if (size() != size) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (!this._children.get(i).equals(arrayList.get(i))) {
                return false;
            }
        }
        return true;
    }

    public JsonNode _set(int i, JsonNode jsonNode) {
        if (this._children != null && i >= 0 && i < this._children.size()) {
            return this._children.set(i, jsonNode);
        }
        throw new IndexOutOfBoundsException("Illegal index " + i + ", array size " + size());
    }

    public ArrayNode add(double d) {
        return _add(numberNode(d));
    }

    public ArrayNode add(float f) {
        return _add(numberNode(f));
    }

    public ArrayNode add(int i) {
        _add(numberNode(i));
        return this;
    }

    public ArrayNode add(long j) {
        return _add(numberNode(j));
    }

    public ArrayNode add(JsonNode jsonNode) {
        if (jsonNode == null) {
            jsonNode = nullNode();
        }
        _add(jsonNode);
        return this;
    }

    public ArrayNode add(Boolean bool) {
        return bool == null ? addNull() : _add(booleanNode(bool.booleanValue()));
    }

    public ArrayNode add(Double d) {
        return d == null ? addNull() : _add(numberNode(d.doubleValue()));
    }

    public ArrayNode add(Float f) {
        return f == null ? addNull() : _add(numberNode(f.floatValue()));
    }

    public ArrayNode add(Integer num) {
        return num == null ? addNull() : _add(numberNode(num.intValue()));
    }

    public ArrayNode add(Long l) {
        return l == null ? addNull() : _add(numberNode(l.longValue()));
    }

    public ArrayNode add(String str) {
        return str == null ? addNull() : _add(textNode(str));
    }

    public ArrayNode add(BigDecimal bigDecimal) {
        return bigDecimal == null ? addNull() : _add(numberNode(bigDecimal));
    }

    public ArrayNode add(boolean z) {
        return _add(booleanNode(z));
    }

    public ArrayNode add(byte[] bArr) {
        return bArr == null ? addNull() : _add(binaryNode(bArr));
    }

    public ArrayNode addAll(ArrayNode arrayNode) {
        int size = arrayNode.size();
        if (size > 0) {
            if (this._children == null) {
                this._children = new ArrayList<>(size + 2);
            }
            arrayNode.addContentsTo(this._children);
        }
        return this;
    }

    public ArrayNode addAll(Collection<JsonNode> collection) {
        if (collection.size() > 0) {
            if (this._children == null) {
                this._children = new ArrayList<>(collection);
            } else {
                this._children.addAll(collection);
            }
        }
        return this;
    }

    public ArrayNode addArray() {
        ArrayNode arrayNode = arrayNode();
        _add(arrayNode);
        return arrayNode;
    }

    /* access modifiers changed from: protected */
    public void addContentsTo(List<JsonNode> list) {
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                list.add(it.next());
            }
        }
    }

    public ArrayNode addNull() {
        _add(nullNode());
        return this;
    }

    public ObjectNode addObject() {
        ObjectNode objectNode = objectNode();
        _add(objectNode);
        return objectNode;
    }

    public ArrayNode addPOJO(Object obj) {
        if (obj == null) {
            addNull();
        } else {
            _add(POJONode(obj));
        }
        return this;
    }

    public JsonToken asToken() {
        return JsonToken.START_ARRAY;
    }

    public ArrayNode deepCopy() {
        if (this._children == null) {
            return new ArrayNode(this._nodeFactory);
        }
        int size = this._children.size();
        ArrayList arrayList = new ArrayList(Math.max(4, size));
        for (int i = 0; i < size; i++) {
            arrayList.add(this._children.get(i).deepCopy());
        }
        return new ArrayNode(this._nodeFactory, arrayList);
    }

    public Iterator<JsonNode> elements() {
        return this._children == null ? ContainerNode.NoNodesIterator.instance() : this._children.iterator();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ArrayNode arrayNode = (ArrayNode) obj;
        return (this._children == null || this._children.size() == 0) ? arrayNode.size() == 0 : arrayNode._sameChildren(this._children);
    }

    public ObjectNode findParent(String str) {
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                JsonNode findParent = it.next().findParent(str);
                if (findParent != null) {
                    return (ObjectNode) findParent;
                }
            }
        }
        return null;
    }

    public List<JsonNode> findParents(String str, List<JsonNode> list) {
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                list = it.next().findParents(str, list);
            }
        }
        return list;
    }

    public JsonNode findValue(String str) {
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                JsonNode findValue = it.next().findValue(str);
                if (findValue != null) {
                    return findValue;
                }
            }
        }
        return null;
    }

    public List<JsonNode> findValues(String str, List<JsonNode> list) {
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                list = it.next().findValues(str, list);
            }
        }
        return list;
    }

    public List<String> findValuesAsText(String str, List<String> list) {
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                list = it.next().findValuesAsText(str, list);
            }
        }
        return list;
    }

    public JsonNode get(int i) {
        if (i < 0 || this._children == null || i >= this._children.size()) {
            return null;
        }
        return this._children.get(i);
    }

    public JsonNode get(String str) {
        return null;
    }

    public int hashCode() {
        if (this._children == null) {
            return 1;
        }
        int size = this._children.size();
        Iterator<JsonNode> it = this._children.iterator();
        while (true) {
            int i = size;
            if (!it.hasNext()) {
                return i;
            }
            JsonNode next = it.next();
            size = next != null ? next.hashCode() ^ i : i;
        }
    }

    public ArrayNode insert(int i, double d) {
        return _insert(i, numberNode(d));
    }

    public ArrayNode insert(int i, float f) {
        return _insert(i, numberNode(f));
    }

    public ArrayNode insert(int i, int i2) {
        _insert(i, numberNode(i2));
        return this;
    }

    public ArrayNode insert(int i, long j) {
        return _insert(i, numberNode(j));
    }

    public ArrayNode insert(int i, JsonNode jsonNode) {
        if (jsonNode == null) {
            jsonNode = nullNode();
        }
        _insert(i, jsonNode);
        return this;
    }

    public ArrayNode insert(int i, Boolean bool) {
        return bool == null ? insertNull(i) : _insert(i, booleanNode(bool.booleanValue()));
    }

    public ArrayNode insert(int i, Double d) {
        return d == null ? insertNull(i) : _insert(i, numberNode(d.doubleValue()));
    }

    public ArrayNode insert(int i, Float f) {
        return f == null ? insertNull(i) : _insert(i, numberNode(f.floatValue()));
    }

    public ArrayNode insert(int i, Integer num) {
        if (num == null) {
            insertNull(i);
        } else {
            _insert(i, numberNode(num.intValue()));
        }
        return this;
    }

    public ArrayNode insert(int i, Long l) {
        return l == null ? insertNull(i) : _insert(i, numberNode(l.longValue()));
    }

    public ArrayNode insert(int i, String str) {
        return str == null ? insertNull(i) : _insert(i, textNode(str));
    }

    public ArrayNode insert(int i, BigDecimal bigDecimal) {
        return bigDecimal == null ? insertNull(i) : _insert(i, numberNode(bigDecimal));
    }

    public ArrayNode insert(int i, boolean z) {
        return _insert(i, booleanNode(z));
    }

    public ArrayNode insert(int i, byte[] bArr) {
        return bArr == null ? insertNull(i) : _insert(i, binaryNode(bArr));
    }

    public ArrayNode insertArray(int i) {
        ArrayNode arrayNode = arrayNode();
        _insert(i, arrayNode);
        return arrayNode;
    }

    public ArrayNode insertNull(int i) {
        _insert(i, nullNode());
        return this;
    }

    public ObjectNode insertObject(int i) {
        ObjectNode objectNode = objectNode();
        _insert(i, objectNode);
        return objectNode;
    }

    public ArrayNode insertPOJO(int i, Object obj) {
        return obj == null ? insertNull(i) : _insert(i, POJONode(obj));
    }

    public boolean isArray() {
        return true;
    }

    public JsonNode path(int i) {
        return (i < 0 || this._children == null || i >= this._children.size()) ? MissingNode.getInstance() : this._children.get(i);
    }

    public JsonNode path(String str) {
        return MissingNode.getInstance();
    }

    public JsonNode remove(int i) {
        if (i < 0 || this._children == null || i >= this._children.size()) {
            return null;
        }
        return this._children.remove(i);
    }

    public ArrayNode removeAll() {
        this._children = null;
        return this;
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        jsonGenerator.writeStartArray();
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                ((BaseJsonNode) it.next()).serialize(jsonGenerator, serializerProvider);
            }
        }
        jsonGenerator.writeEndArray();
    }

    public void serializeWithType(JsonGenerator jsonGenerator, SerializerProvider serializerProvider, TypeSerializer typeSerializer) {
        typeSerializer.writeTypePrefixForArray(this, jsonGenerator);
        if (this._children != null) {
            Iterator<JsonNode> it = this._children.iterator();
            while (it.hasNext()) {
                ((BaseJsonNode) it.next()).serialize(jsonGenerator, serializerProvider);
            }
        }
        typeSerializer.writeTypeSuffixForArray(this, jsonGenerator);
    }

    public JsonNode set(int i, JsonNode jsonNode) {
        if (jsonNode == null) {
            jsonNode = nullNode();
        }
        return _set(i, jsonNode);
    }

    public int size() {
        if (this._children == null) {
            return 0;
        }
        return this._children.size();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((size() << 4) + 16);
        sb.append('[');
        if (this._children != null) {
            int size = this._children.size();
            for (int i = 0; i < size; i++) {
                if (i > 0) {
                    sb.append(',');
                }
                sb.append(this._children.get(i).toString());
            }
        }
        sb.append(']');
        return sb.toString();
    }
}
