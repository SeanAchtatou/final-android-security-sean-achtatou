package com.fasterxml.jackson.databind.util;

import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import me.gall.sgp.sdk.entity.app.StructuredData;

public class BeanUtil {
    protected static boolean isCglibGetCallbacks(AnnotatedMethod annotatedMethod) {
        Package packageR;
        Class<?> rawType = annotatedMethod.getRawType();
        if (rawType == null || !rawType.isArray() || (packageR = rawType.getComponentType().getPackage()) == null) {
            return false;
        }
        String name = packageR.getName();
        return name.startsWith("net.sf.cglib") || name.startsWith("org.hibernate.repackage.cglib");
    }

    protected static boolean isGroovyMetaClassGetter(AnnotatedMethod annotatedMethod) {
        Package packageR;
        Class<?> rawType = annotatedMethod.getRawType();
        return rawType != null && !rawType.isArray() && (packageR = rawType.getPackage()) != null && packageR.getName().startsWith("groovy.lang");
    }

    protected static boolean isGroovyMetaClassSetter(AnnotatedMethod annotatedMethod) {
        Package packageR = annotatedMethod.getRawParameterType(0).getPackage();
        return packageR != null && packageR.getName().startsWith("groovy.lang");
    }

    protected static String manglePropertyName(String str) {
        StringBuilder sb = null;
        int length = str.length();
        if (length == 0) {
            return null;
        }
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            char lowerCase = Character.toLowerCase(charAt);
            if (charAt == lowerCase) {
                break;
            }
            if (sb == null) {
                sb = new StringBuilder(str);
            }
            sb.setCharAt(i, lowerCase);
        }
        return sb != null ? sb.toString() : str;
    }

    public static String okNameForGetter(AnnotatedMethod annotatedMethod) {
        String name = annotatedMethod.getName();
        String okNameForIsGetter = okNameForIsGetter(annotatedMethod, name);
        return okNameForIsGetter == null ? okNameForRegularGetter(annotatedMethod, name) : okNameForIsGetter;
    }

    public static String okNameForIsGetter(AnnotatedMethod annotatedMethod, String str) {
        if (!str.startsWith("is")) {
            return null;
        }
        Class<?> rawType = annotatedMethod.getRawType();
        if (rawType == Boolean.class || rawType == Boolean.TYPE) {
            return manglePropertyName(str.substring(2));
        }
        return null;
    }

    public static String okNameForMutator(AnnotatedMethod annotatedMethod, String str) {
        String name = annotatedMethod.getName();
        if (name.startsWith(str)) {
            return manglePropertyName(name.substring(str.length()));
        }
        return null;
    }

    public static String okNameForRegularGetter(AnnotatedMethod annotatedMethod, String str) {
        if (!str.startsWith("get")) {
            return null;
        }
        if ("getCallbacks".equals(str)) {
            if (isCglibGetCallbacks(annotatedMethod)) {
                return null;
            }
        } else if ("getMetaClass".equals(str) && isGroovyMetaClassGetter(annotatedMethod)) {
            return null;
        }
        return manglePropertyName(str.substring(3));
    }

    public static String okNameForSetter(AnnotatedMethod annotatedMethod) {
        String okNameForMutator = okNameForMutator(annotatedMethod, StructuredData.TYPE_OF_SET);
        if (okNameForMutator == null) {
            return null;
        }
        if (!"metaClass".equals(okNameForMutator) || !isGroovyMetaClassSetter(annotatedMethod)) {
            return okNameForMutator;
        }
        return null;
    }
}
