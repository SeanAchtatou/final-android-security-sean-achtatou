package com.a.a.e;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.IOException;
import java.io.InputStream;
import javax.microedition.enhance.MIDPHelper;
import org.meteoroid.core.c;
import org.meteoroid.core.e;
import org.meteoroid.plugin.device.MIDPDevice;

public class p {
    private static final Matrix iP = new Matrix();
    private o bx;
    public int height;
    public Bitmap iN;
    public boolean iO;
    public int width;

    public p(Bitmap bitmap) {
        this(bitmap, false);
    }

    private p(Bitmap bitmap, boolean z) {
        this.iN = bitmap;
        if (bitmap != null) {
            this.width = bitmap.getWidth();
            this.height = bitmap.getHeight();
        }
        this.iO = z;
    }

    public static p C(String str) {
        Log.d("CreateImage", str);
        return b(MIDPHelper.b(p.class, str));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static p a(p pVar, int i, int i2, int i3, int i4, int i5) {
        Log.d("CreateImage", "From a clip of another Image.");
        if (pVar == null) {
            throw new NullPointerException();
        }
        iP.reset();
        iP.setTranslate((float) i, (float) i2);
        ((MIDPDevice) c.ou).a(i5, iP);
        Bitmap createBitmap = Bitmap.createBitmap(pVar.iN, i, i2, i3, i4, iP, true);
        if (createBitmap == null) {
            throw new IllegalArgumentException("Area out of Image");
        }
        p pVar2 = new p(createBitmap);
        pVar2.iO = false;
        return pVar2;
    }

    public static p a(int[] iArr, int i, int i2, boolean z) {
        Log.d("CreateImage", "From rgb array:" + iArr.length);
        Bitmap b = e.b(iArr, i, i2, z);
        if (b == null) {
            throw new IllegalArgumentException();
        }
        p pVar = new p(b);
        pVar.iO = false;
        return pVar;
    }

    public static p b(InputStream inputStream) {
        Log.d("CreateImage", "From stream.");
        Bitmap d = e.d(inputStream);
        if (d == null) {
            throw new IOException();
        }
        p pVar = new p(d);
        pVar.iO = false;
        return pVar;
    }

    public static p d(byte[] bArr, int i, int i2) {
        Log.d("CreateImage", "With byte array:" + bArr.length + " offset:" + i + " length:" + i2);
        return new p(e.h(bArr, i, i2));
    }

    public static p e(p pVar) {
        Log.d("CreateImage", "Clone another Image.");
        return new p(Bitmap.createBitmap(pVar.iN));
    }

    public static p o(int i, int i2) {
        Log.d("CreateImage", "width:" + i + " height:" + i2);
        return new p(e.a(i, i2, false, -16777216), true);
    }

    public void a(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6) {
        int i7;
        if (Math.abs(i2) < i5) {
            i7 = (i2 > 0 ? 1 : -1) * i5;
        } else {
            i7 = i2;
        }
        this.iN.getPixels(iArr, i, i7, i3, i4, i5, i6);
        for (int i8 = 0; i8 < iArr.length; i8++) {
            if (iArr[i8] == -16777216) {
                iArr[i8] = -1;
            }
        }
    }

    public o bX() {
        if (this.iO) {
            if (this.bx == null) {
                this.bx = new MIDPDevice.e(this.iN);
                Log.d(getClass().getName(), "Create a MutableImage Graphics." + getWidth() + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + getHeight());
            }
            this.bx.bW();
            return this.bx;
        }
        throw new IllegalStateException("Image is immutable");
    }

    public Bitmap getBitmap() {
        return this.iN;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public boolean isMutable() {
        return this.iO;
    }
}
