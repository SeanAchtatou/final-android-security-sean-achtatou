package com.a.a.e;

import android.view.View;
import java.util.ArrayList;

public abstract class r {
    public static final int BUTTON = 2;
    public static final int HYPERLINK = 1;
    public static final int LAYOUT_2 = 16384;
    public static final int LAYOUT_BOTTOM = 32;
    public static final int LAYOUT_CENTER = 3;
    public static final int LAYOUT_DEFAULT = 0;
    public static final int LAYOUT_EXPAND = 2048;
    public static final int LAYOUT_LEFT = 1;
    public static final int LAYOUT_NEWLINE_AFTER = 512;
    public static final int LAYOUT_NEWLINE_BEFORE = 256;
    public static final int LAYOUT_RIGHT = 2;
    public static final int LAYOUT_SHRINK = 1024;
    public static final int LAYOUT_TOP = 16;
    public static final int LAYOUT_VCENTER = 48;
    public static final int LAYOUT_VEXPAND = 8192;
    public static final int LAYOUT_VSHRINK = 4096;
    public static final int OUTOFITEM = Integer.MAX_VALUE;
    public static final int PLAIN = 0;
    private v iS = null;
    protected f iT;
    private s iU;
    private ArrayList<f> iq = new ArrayList<>();
    private String label;
    private int layout;

    public r(String str) {
        p(-1, -1);
        this.label = str;
    }

    public void E(String str) {
        this.label = str;
    }

    public void a(s sVar) {
        this.iU = sVar;
    }

    /* access modifiers changed from: protected */
    public void a(v vVar) {
        this.iS = vVar;
    }

    public void an(int i) {
        if (((i & 1024) == 0 || (i & 2048) == 0) && ((i & 4096) == 0 || (i & 8192) == 0)) {
            this.layout = i;
            invalidate();
            return;
        }
        throw new IllegalArgumentException("Bad combination of layout policies");
    }

    public void b(f fVar) {
        boolean z = false;
        if (fVar == null) {
            throw new NullPointerException();
        } else if (!this.iq.contains(fVar)) {
            int i = 0;
            while (true) {
                if (i >= this.iq.size()) {
                    break;
                } else if (fVar.getPriority() < this.iq.get(i).getPriority()) {
                    this.iq.add(i, fVar);
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!z) {
                this.iq.add(fVar);
            }
            invalidate();
        }
    }

    public String bf() {
        return this.label;
    }

    public void c(f fVar) {
        this.iq.remove(fVar);
        if (this.iT == fVar) {
            this.iT = null;
        }
        invalidate();
    }

    public int ca() {
        return this.layout;
    }

    public int cb() {
        return getHeight();
    }

    public int cc() {
        return getWidth();
    }

    public void cd() {
        v ce = ce();
        if (ce != null && (ce instanceof m)) {
            ((m) ce).bI().c(this);
        }
    }

    /* access modifiers changed from: protected */
    public v ce() {
        return this.iS;
    }

    public int cf() {
        return getHeight();
    }

    public int cg() {
        return getWidth();
    }

    public s ch() {
        return this.iU;
    }

    public void d(f fVar) {
        this.iT = fVar;
        if (fVar == null) {
            invalidate();
        } else if (this.iq.contains(fVar)) {
            b(fVar);
        } else {
            invalidate();
        }
    }

    public int getHeight() {
        if (getView() != null) {
            return getView().getHeight();
        }
        return 0;
    }

    public int getMinimumHeight() {
        return getHeight();
    }

    public int getMinimumWidth() {
        return getWidth();
    }

    public abstract View getView();

    public int getWidth() {
        if (getView() != null) {
            return getView().getWidth();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void i() {
    }

    /* access modifiers changed from: package-private */
    public final void invalidate() {
        if (this.iS != null) {
            this.iS.getView().postInvalidate();
        }
    }

    public boolean isFocusable() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void j() {
    }

    public void p(int i, int i2) {
        if (i < -1 || i2 < -1) {
            throw new IllegalArgumentException();
        }
        invalidate();
    }
}
