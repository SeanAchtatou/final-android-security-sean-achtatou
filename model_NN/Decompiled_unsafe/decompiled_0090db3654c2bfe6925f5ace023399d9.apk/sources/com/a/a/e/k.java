package com.a.a.e;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import org.meteoroid.core.c;
import org.meteoroid.core.i;
import org.meteoroid.core.l;
import org.meteoroid.core.m;

public abstract class k implements m.a {
    public static final int ALERT = 5;
    public static final int CANVAS = 0;
    public static final int FORM = 2;
    public static final int GAMECANVAS = 1;
    public static final int LIST = 3;
    public static final int TEXTBOX = 4;
    private int height = -1;
    protected j in = null;
    public boolean io = false;
    private aa ip;
    private ArrayList<f> iq = new ArrayList<>();
    private g ir = null;
    private boolean isVisible;
    private String title;
    private int width = -1;

    k() {
    }

    private void bC() {
        this.width = c.ou.getWidth();
        this.height = c.ou.getHeight();
    }

    private void by() {
        if (this.isVisible) {
            l.getHandler().post(new Runnable() {
                public void run() {
                    k.this.aN();
                    k.this.aM();
                }
            });
        }
    }

    public void a(aa aaVar) {
        this.ip = aaVar;
    }

    public void a(g gVar) {
        this.ir = gVar;
    }

    public void aM() {
        Iterator<f> it = this.iq.iterator();
        while (it.hasNext()) {
            i.a(it.next());
        }
        if (!this.isVisible) {
            try {
                i();
                Log.d("Displayable", getClass().getSimpleName() + " shownotify called.");
            } catch (Exception e) {
                Log.w("Displayable", e);
            }
            this.isVisible = true;
        }
    }

    public void aN() {
        Iterator<f> it = this.iq.iterator();
        while (it.hasNext()) {
            i.b(it.next());
        }
        if (this.isVisible) {
            try {
                j();
                Log.d("Displayable", getClass().getSimpleName() + " hidenotify called.");
            } catch (Exception e) {
                Log.w("Displayable", e);
            }
            this.isVisible = false;
        }
    }

    public abstract int aS();

    public boolean aZ() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void aa(int i) {
    }

    /* access modifiers changed from: protected */
    public void b(int i) {
    }

    /* access modifiers changed from: protected */
    public void b(int i, int i2) {
    }

    public void b(f fVar) {
        boolean z = false;
        Log.d("Displayable", fVar.bf() + " has added.");
        int priority = fVar.getPriority();
        int i = 0;
        while (true) {
            if (i >= this.iq.size()) {
                break;
            } else if (priority <= this.iq.get(i).getPriority()) {
                this.iq.add(i, fVar);
                by();
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            this.iq.add(fVar);
            by();
        }
    }

    public aa bA() {
        return this.ip;
    }

    public g bB() {
        return this.ir;
    }

    public ArrayList<f> bz() {
        return this.iq;
    }

    /* access modifiers changed from: protected */
    public void c(int i, int i2) {
    }

    public void c(f fVar) {
        this.iq.remove(fVar);
        by();
    }

    /* access modifiers changed from: protected */
    public void d(int i, int i2) {
    }

    public int getHeight() {
        if (this.height == -1) {
            bC();
        }
        return this.height;
    }

    public String getTitle() {
        return this.title;
    }

    public int getWidth() {
        if (this.width == -1) {
            bC();
        }
        return this.width;
    }

    /* access modifiers changed from: protected */
    public void i() {
    }

    public boolean isShown() {
        return this.isVisible;
    }

    /* access modifiers changed from: protected */
    public void j() {
    }

    /* access modifiers changed from: protected */
    public void keyPressed(int i) {
    }

    /* access modifiers changed from: protected */
    public void n(int i, int i2) {
    }

    public void setTitle(String str) {
        this.title = str;
    }
}
