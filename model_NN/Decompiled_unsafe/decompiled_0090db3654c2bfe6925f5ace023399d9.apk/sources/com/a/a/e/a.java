package com.a.a.e;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fasterxml.jackson.core.util.BufferRecycler;
import java.util.Iterator;
import org.meteoroid.core.l;

public class a extends v {
    public static final int FOREVER = -2;
    protected static Context context = null;
    public static final f hr = new f("", 4, 0);
    private Activity hA;
    private String hs;
    private p ht;
    private b hu;
    private int hv;
    private n hw;
    private TextView hx;
    private LinearLayout hy;
    private ImageView hz;

    public a(String str) {
        this(str, null, null, b.hC);
    }

    public a(String str, String str2, p pVar, b bVar) {
        super(str);
        this.hs = null;
        this.ht = null;
        this.hA = l.getActivity();
        this.hy = new LinearLayout(this.hA);
        this.hy.setOrientation(1);
        this.hx = new TextView(this.hA);
        this.hz = new ImageView(this.hA);
        if (str2 != null) {
            setString(str2);
        }
        if (pVar != null) {
            c(pVar);
        }
        if (bVar != null) {
            a(bVar);
        }
        this.hy.addView(this.hx, new ViewGroup.LayoutParams(-2, -2));
        this.hy.addView(this.hz, new ViewGroup.LayoutParams(-2, -2));
    }

    public void a(b bVar) {
        this.hu = bVar;
    }

    public void a(n nVar) {
        if (nVar == null && this.hw != null) {
            this.hy.removeView(this.hw.getView());
        }
        this.hw = nVar;
        if (nVar != null) {
            this.hy.addView(nVar.getView(), new ViewGroup.LayoutParams(-2, -2));
        }
    }

    public void aM() {
        super.aM();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = a.this.bz().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Add command " + next.bf());
                    ((ViewGroup) a.this.getView()).addView(next.bh());
                }
                a.this.getView().requestLayout();
            }
        });
    }

    public void aN() {
        super.aN();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = a.this.bz().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Remove command " + next.bf());
                    ((ViewGroup) a.this.getView()).removeView(next.bh());
                }
                a.this.getView().requestLayout();
            }
        });
    }

    public p aO() {
        return this.ht;
    }

    public b aP() {
        return this.hu;
    }

    public int aQ() {
        return BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN;
    }

    public n aR() {
        return this.hw;
    }

    public int aS() {
        return 5;
    }

    public void c(p pVar) {
        this.ht = pVar;
        this.hz.setImageBitmap(pVar.getBitmap());
    }

    public String getString() {
        return this.hs;
    }

    public int getTimeout() {
        return this.hv;
    }

    public View getView() {
        return this.hy;
    }

    public void setString(String str) {
        this.hs = str;
        this.hx.setText(str);
    }

    public void setTimeout(int i) {
        this.hv = i;
    }
}
