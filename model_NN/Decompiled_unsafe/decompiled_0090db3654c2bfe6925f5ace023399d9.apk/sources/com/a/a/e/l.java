package com.a.a.e;

import java.util.Hashtable;
import org.meteoroid.plugin.device.MIDPDevice;

public final class l {
    public static final int FACE_MONOSPACE = 32;
    public static final int FACE_PROPORTIONAL = 64;
    public static final int FACE_SYSTEM = 0;
    public static final int FONT_INPUT_TEXT = 1;
    public static final int FONT_STATIC_TEXT = 0;
    public static final int SIZE_LARGE = 16;
    public static final int SIZE_MEDIUM = 0;
    public static final int SIZE_SMALL = 8;
    public static final int STYLE_BOLD = 1;
    public static final int STYLE_ITALIC = 2;
    public static final int STYLE_PLAIN = 0;
    public static final int STYLE_UNDERLINED = 4;
    private static final l it = new l(0, 0, 0);
    private static l[] iu = {it, it};
    private static final Hashtable<Integer, l> iv = new Hashtable<>();
    private int iw;
    private int ix;
    private int size;

    public l(int i, int i2, int i3) {
        this.iw = i;
        this.ix = i2;
        this.size = i3;
    }

    public static l U(int i) {
        if (i == 1 || i == 0) {
            return iu[i];
        }
        throw new IllegalArgumentException("Bad specifier");
    }

    public static l bD() {
        return it;
    }

    public static l k(int i, int i2, int i3) {
        Integer num = new Integer(i2 + i3 + i);
        l lVar = iv.get(num);
        if (lVar != null) {
            return lVar;
        }
        l lVar2 = new l(i, i2, i3);
        iv.put(num, lVar2);
        return lVar2;
    }

    public int A(String str) {
        return MIDPDevice.d.a(this, str);
    }

    public int a(char c) {
        return MIDPDevice.d.a(this, c);
    }

    public int a(char[] cArr, int i, int i2) {
        return MIDPDevice.d.a(this, cArr, i, i2);
    }

    public int b(String str, int i, int i2) {
        return MIDPDevice.d.a(this, str, i, i2);
    }

    public int bE() {
        return this.iw;
    }

    public boolean bF() {
        return this.ix == 0;
    }

    public boolean bG() {
        return (this.ix & 4) != 0;
    }

    public int bH() {
        return MIDPDevice.d.c(this);
    }

    public int getHeight() {
        return MIDPDevice.d.d(this);
    }

    public int getSize() {
        return this.size;
    }

    public int getStyle() {
        return this.ix;
    }

    public int hashCode() {
        return this.ix + this.size + this.iw;
    }

    public boolean isBold() {
        return (this.ix & 1) != 0;
    }

    public boolean isItalic() {
        return (this.ix & 2) != 0;
    }
}
