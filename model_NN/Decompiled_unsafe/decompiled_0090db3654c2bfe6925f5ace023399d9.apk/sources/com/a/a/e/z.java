package com.a.a.e;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.meteoroid.core.l;

public class z extends r {
    public static final int ANY = 0;
    public static final int CONSTRAINT_MASK = 65535;
    public static final int DECIMAL = 5;
    public static final int EMAILADDR = 1;
    public static final int INITIAL_CAPS_SENTENCE = 2097152;
    public static final int INITIAL_CAPS_WORD = 1048576;
    public static final int NON_PREDICTIVE = 524288;
    public static final int NUMERIC = 2;
    public static final int PASSWORD = 65536;
    public static final int PHONENUMBER = 3;
    public static final int SENSITIVE = 262144;
    public static final int UNEDITABLE = 131072;
    public static final int URL = 4;
    /* access modifiers changed from: private */
    public TextView hx;
    /* access modifiers changed from: private */
    public LinearLayout hy;
    private int ji;
    private int jj;
    /* access modifiers changed from: private */
    public EditText jk;
    private int jp;

    public z(final String str, final String str2, int i, final int i2) {
        super(str);
        this.ji = i;
        this.jj = i2;
        l.getHandler().post(new Runnable() {
            public void run() {
                Activity activity = l.getActivity();
                LinearLayout unused = z.this.hy = new LinearLayout(activity);
                z.this.hy.setOrientation(1);
                z.this.hy.setBackgroundColor(-16777216);
                TextView unused2 = z.this.hx = new TextView(activity);
                z.this.E(str);
                z.this.hy.addView(z.this.hx, new ViewGroup.LayoutParams(-1, -2));
                EditText unused3 = z.this.jk = new EditText(activity);
                z.this.setString(str2);
                switch (i2) {
                    case 0:
                        z.this.jk.setSingleLine(true);
                        z.this.jk.setInputType(1);
                        break;
                    case 1:
                        z.this.jk.setSingleLine(true);
                        z.this.jk.setInputType(48);
                        break;
                    case 2:
                        z.this.jk.setSingleLine(true);
                        z.this.jk.setInputType(2);
                        break;
                    case 3:
                        z.this.jk.setSingleLine(true);
                        z.this.jk.setInputType(3);
                        break;
                    case 4:
                        z.this.jk.setSingleLine(true);
                        z.this.jk.setInputType(16);
                        break;
                    case 5:
                        z.this.jk.setSingleLine(true);
                        z.this.jk.setInputType(12290);
                        break;
                    case 65536:
                        z.this.jk.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        z.this.jk.setTypeface(Typeface.MONOSPACE);
                        break;
                }
                z.this.hy.addView(z.this.jk, new ViewGroup.LayoutParams(-1, -2));
                synchronized (z.this) {
                    z.this.notify();
                }
            }
        });
        synchronized (this) {
            try {
                wait(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void E(final String str) {
        super.E(str);
        l.getHandler().post(new Runnable() {
            public void run() {
                z.this.hx.setText(str);
            }
        });
    }

    public void F(String str) {
    }

    public int a(char[] cArr) {
        return getString().toCharArray().length;
    }

    public void a(char[] cArr, int i, int i2, int i3) {
        e(new String(cArr, i, i2), i3);
    }

    public int ap(int i) {
        this.jk.setFilters(new InputFilter[]{new InputFilter.LengthFilter(i)});
        this.ji = i;
        return i;
    }

    public void aq(int i) {
        this.jj = i;
    }

    public void b(char[] cArr, int i, int i2) {
        setString(new String(cArr, i, i2));
    }

    /* renamed from: bo */
    public LinearLayout getView() {
        return this.hy;
    }

    public int cj() {
        return this.jj;
    }

    public int ck() {
        return this.jp;
    }

    public void e(String str, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getString().substring(0, i));
        stringBuffer.append(str);
        stringBuffer.append(getString().substring(i + 1));
        setString(stringBuffer.toString());
    }

    public int getMaxSize() {
        return this.ji;
    }

    public String getString() {
        return this.jk.getText().toString();
    }

    /* access modifiers changed from: protected */
    public void i() {
        this.jk.clearFocus();
    }

    /* access modifiers changed from: protected */
    public void j() {
        l.hr().hideSoftInputFromWindow(this.jk.getWindowToken(), 2);
        Log.d("TextBox", "Force close soft input.");
    }

    public void r(int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer(getString());
        stringBuffer.delete(i, i + i2);
        setString(stringBuffer.toString());
    }

    public void setString(final String str) {
        l.getHandler().post(new Runnable() {
            public void run() {
                z.this.jk.setText(str);
            }
        });
    }

    public int size() {
        return getString().length();
    }
}
