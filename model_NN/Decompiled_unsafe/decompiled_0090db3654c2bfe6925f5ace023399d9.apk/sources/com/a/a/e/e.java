package com.a.a.e;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import org.meteoroid.core.l;

public class e extends r implements d {
    /* access modifiers changed from: private */
    public ArrayAdapter<String> hH;
    private ArrayAdapter<p> hI;
    private LinearLayout hJ;
    /* access modifiers changed from: private */
    public ListView hK;
    private Spinner hL;
    private int hM;

    public e(String str, int i) {
        this(str, i, null, null);
    }

    public e(String str, int i, String[] strArr, p[] pVarArr) {
        super(str);
        this.hM = i;
        Activity activity = l.getActivity();
        this.hJ = new LinearLayout(activity);
        this.hJ.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        this.hJ.setOrientation(1);
        strArr = strArr == null ? new String[0] : strArr;
        pVarArr = pVarArr == null ? new p[0] : pVarArr;
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(strArr));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(Arrays.asList(pVarArr));
        this.hH = new ArrayAdapter<>(activity, Y(i), arrayList);
        this.hH.setNotifyOnChange(true);
        this.hI = new ArrayAdapter<>(activity, Y(i), arrayList2);
        TextView textView = new TextView(activity);
        textView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        textView.setText(str);
        this.hJ.addView(textView);
        if (i == 4) {
            this.hL = new Spinner(activity);
            this.hL.setAdapter((SpinnerAdapter) this.hH);
            this.hJ.addView(this.hL);
            return;
        }
        this.hK = new ListView(activity);
        this.hK.setAdapter((ListAdapter) this.hH);
        this.hK.setBackgroundColor(-16777216);
        this.hK.setChoiceMode(Z(i));
        this.hJ.addView(this.hK);
        bd();
    }

    private int Y(int i) {
        switch (i) {
            case 1:
                return 17367055;
            case 2:
                return 17367056;
            case 3:
            default:
                return 17367043;
            case 4:
                return 17367048;
        }
    }

    private int Z(int i) {
        switch (i) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
            default:
                return 0;
        }
    }

    private void bd() {
        if (this.hK != null && this.hH != null) {
            l.getHandler().post(new Runnable() {
                public void run() {
                    try {
                        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(e.this.hK.getWidth(), Integer.MIN_VALUE);
                        int i = 0;
                        for (int i2 = 0; i2 < e.this.hH.getCount(); i2++) {
                            View view = e.this.hH.getView(i2, null, e.this.hK);
                            view.measure(makeMeasureSpec, 0);
                            i += view.getMeasuredHeight();
                        }
                        ViewGroup.LayoutParams layoutParams = e.this.hK.getLayoutParams();
                        layoutParams.height = i + (e.this.hK.getDividerHeight() * (e.this.hH.getCount() - 1));
                        e.this.hK.setLayoutParams(layoutParams);
                        e.this.hK.requestLayout();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public l U(int i) {
        return l.bD();
    }

    public p V(int i) {
        return this.hI.getItem(i);
    }

    public boolean W(int i) {
        return this.hM == 4 ? i == this.hL.getSelectedItemPosition() : this.hK.isItemChecked(i);
    }

    public void X(int i) {
    }

    public int a(String str, p pVar) {
        if (str != null) {
            this.hH.add(str);
        }
        if (pVar != null) {
            this.hI.add(pVar);
        }
        if (this.hM != 4) {
            bd();
        }
        return this.hH.getCount();
    }

    public int a(boolean[] zArr) {
        if (zArr.length > this.hH.getCount()) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < zArr.length; i++) {
            zArr[i] = this.hK.isItemChecked(i);
        }
        return zArr.length;
    }

    public void a(int i, l lVar) {
    }

    public void a(int i, String str, p pVar) {
        this.hH.insert(str, i);
        this.hI.insert(pVar, i);
        if (this.hM != 4) {
            bd();
        }
    }

    public void b(int i, String str, p pVar) {
        a(i, str, pVar);
        delete(i + 1);
    }

    public void b(boolean[] zArr) {
        if (zArr.length > this.hH.getCount()) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < zArr.length; i++) {
            this.hK.setItemChecked(i, zArr[i]);
        }
    }

    public void ba() {
        this.hH.clear();
        this.hI.clear();
        if (this.hM != 4) {
            bd();
        }
    }

    public int bb() {
        return 0;
    }

    public int bc() {
        if (this.hM == 2) {
            return -1;
        }
        if (this.hM == 4) {
            return this.hL.getSelectedItemPosition();
        }
        for (int i = 0; i < this.hK.getCount(); i++) {
            if (this.hK.isItemChecked(i)) {
                return i;
            }
        }
        return -1;
    }

    public void delete(int i) {
        this.hH.remove(this.hH.getItem(i));
        this.hI.remove(this.hI.getItem(i));
        if (this.hM != 4) {
            bd();
        }
    }

    public void e(int i, boolean z) {
        if (this.hM == 4) {
            this.hL.setSelection(i);
        } else {
            this.hK.setItemChecked(i, z);
        }
    }

    public String getString(int i) {
        return this.hH.getItem(i);
    }

    public View getView() {
        return this.hJ;
    }

    public int size() {
        return this.hH.getCount();
    }
}
