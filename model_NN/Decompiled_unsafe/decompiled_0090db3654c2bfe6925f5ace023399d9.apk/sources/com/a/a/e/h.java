package com.a.a.e;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;
import org.meteoroid.core.c;
import org.meteoroid.core.l;
import org.meteoroid.plugin.device.MIDPDevice;

public abstract class h extends r implements View.OnFocusChangeListener {
    protected static final int KEY_PRESS = 4;
    protected static final int KEY_RELEASE = 8;
    protected static final int KEY_REPEAT = 16;
    protected static final int NONE = 0;
    protected static final int POINTER_DRAG = 128;
    protected static final int POINTER_PRESS = 32;
    protected static final int POINTER_RELEASE = 64;
    protected static final int TRAVERSE_HORIZONTAL = 1;
    protected static final int TRAVERSE_VERTICAL = 2;
    private o bx;
    private a hQ = new a(l.getActivity());

    class a extends View {
        public a(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            h.this.a(canvas);
        }
    }

    public h(String str) {
        super(str);
        this.hQ.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.hQ.setOnFocusChangeListener(this);
    }

    public int R(int i) {
        if (i > 0) {
            return ((MIDPDevice) c.ou).R(i);
        }
        return 0;
    }

    public void a(Canvas canvas) {
        if (this.bx == null) {
            this.bx = new MIDPDevice.e(canvas);
        }
        d(this.bx, canvas.getWidth(), canvas.getHeight());
    }

    /* access modifiers changed from: protected */
    public boolean a(int i, int i2, int i3, int[] iArr) {
        return true;
    }

    /* access modifiers changed from: protected */
    public final void aX() {
        this.hQ.postInvalidate();
    }

    /* access modifiers changed from: protected */
    public void aa(int i) {
    }

    /* access modifiers changed from: protected */
    public abstract int ab(int i);

    /* access modifiers changed from: protected */
    public abstract int ac(int i);

    /* access modifiers changed from: protected */
    public void b(int i) {
    }

    /* access modifiers changed from: protected */
    public void b(int i, int i2) {
    }

    /* renamed from: bj */
    public a getView() {
        this.hQ.setMinimumHeight(bn());
        this.hQ.setMinimumWidth(bm());
        return this.hQ;
    }

    /* access modifiers changed from: protected */
    public final int bk() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void bl() {
    }

    /* access modifiers changed from: protected */
    public abstract int bm();

    /* access modifiers changed from: protected */
    public abstract int bn();

    /* access modifiers changed from: protected */
    public void c(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public final void c(int i, int i2, int i3, int i4) {
        this.hQ.postInvalidate(i, i2, i + i3, i2 + i4);
    }

    /* access modifiers changed from: protected */
    public abstract void d(o oVar, int i, int i2);

    /* access modifiers changed from: protected */
    public void keyPressed(int i) {
    }

    /* access modifiers changed from: protected */
    public void n(int i, int i2) {
    }

    public void onFocusChange(View view, boolean z) {
        if (view == this.hQ && !z) {
            bl();
        }
    }
}
