package com.a.a.e;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import org.meteoroid.core.l;

public class n extends r {
    public static final int CONTINUOUS_IDLE = 0;
    public static final int CONTINUOUS_RUNNING = 2;
    public static final int INCREMENTAL_IDLE = 1;
    public static final int INCREMENTAL_UPDATING = 3;
    public static final int INDEFINITE = -1;
    private TextView hx;
    private LinearLayout hy;
    private boolean iF;
    private int iG;
    private ProgressBar iH;
    private SeekBar iI;
    private int value;

    public n(String str, boolean z, int i, int i2) {
        super(str);
        this.iF = z;
        this.iG = i;
        Activity activity = l.getActivity();
        this.hy = new LinearLayout(activity);
        this.hy.setOrientation(1);
        this.hx = new TextView(activity);
        this.hx.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.hx.setTextAppearance(this.hx.getContext(), 16973890);
        if (z) {
            this.iI = new SeekBar(activity, null, 16842875);
            if (i == 0) {
                this.iI.setMax(getView().getWidth());
            } else {
                this.iI.setMax(i);
            }
            this.iI.setProgress(i2);
            this.hy.addView(this.iI, new ViewGroup.LayoutParams(-1, -2));
        } else if (i != -1) {
            this.iH = new ProgressBar(activity, null, 16842872);
            this.iH.setMinimumWidth(1);
            if (i == 0) {
                this.iH.setMax(getView().getWidth());
            } else {
                this.iH.setMax(i);
            }
            this.iH.incrementProgressBy(1);
            this.iH.setProgress(i2);
            this.iH.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (n.this.ch() != null && n.this.iT != null) {
                        n.this.ch().a(n.this.iT, n.this);
                    }
                }
            });
            this.hy.addView(this.iH, new ViewGroup.LayoutParams(-1, -2));
        } else if (i2 == 1) {
            this.iH = new ProgressBar(activity, null, 16842874);
            this.iH.setIndeterminate(true);
            this.hy.addView(this.iH, new ViewGroup.LayoutParams(-2, -2));
        } else if (i2 == 2) {
            this.iH = new ProgressBar(activity);
            this.hy.addView(this.iH, new ViewGroup.LayoutParams(-2, -2));
        }
    }

    public boolean bJ() {
        return this.iF;
    }

    /* renamed from: bo */
    public LinearLayout getView() {
        return this.hy;
    }

    public int getMaxValue() {
        return this.iG;
    }

    public int getValue() {
        return this.value;
    }

    public void setMaxValue(int i) {
        this.iG = i;
    }

    public void setValue(int i) {
        this.value = i;
        if (this.iH != null) {
            this.iH.setProgress(i);
        }
        if (this.iI != null) {
            this.iI.setProgress(i);
        }
        this.hy.postInvalidate();
    }
}
