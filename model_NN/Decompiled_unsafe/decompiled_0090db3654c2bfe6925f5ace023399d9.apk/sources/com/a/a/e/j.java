package com.a.a.e;

import android.content.DialogInterface;
import android.os.Message;
import android.util.Log;
import com.a.a.r.a;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.core.c;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.core.m;
import org.meteoroid.plugin.device.MIDPDevice;

public final class j implements h.a {
    public static final int ALERT = 3;
    public static final int CHOICE_GROUP_ELEMENT = 2;
    public static final int COLOR_BACKGROUND = 0;
    public static final int COLOR_BORDER = 4;
    public static final int COLOR_FOREGROUND = 1;
    public static final int COLOR_HIGHLIGHTED_BACKGROUND = 2;
    public static final int COLOR_HIGHLIGHTED_BORDER = 5;
    public static final int COLOR_HIGHLIGHTED_FOREGROUND = 3;
    public static final int LIST_ELEMENT = 1;
    public static final String LOG_TAG = "Display";
    private static k ie = null;

    /* renamed from: if  reason: not valid java name */
    public static final j f2if = new j();
    private static MIDlet ig;
    private static volatile int ik;
    private Message ih = h.a(MIDPDevice.MSG_MIDP_DISPLAY_CALL_SERIALLY, null, 0, h.MSG_ARG2_DONT_RECYCLE_ME);
    private boolean ii;
    private volatile boolean ij = true;

    private j() {
        h.a(this);
    }

    public static j a(MIDlet mIDlet) {
        if (mIDlet != null) {
            ig = mIDlet;
        }
        return f2if;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.core.m.a(java.lang.String, java.lang.String, android.view.View, boolean, android.content.DialogInterface$OnCancelListener):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], android.view.View, int, ?[OBJECT, ARRAY]]
     candidates:
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String[], android.content.DialogInterface$OnClickListener, boolean):void
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, android.view.View, boolean, android.content.DialogInterface$OnCancelListener):void */
    private final void a(final a aVar) {
        int timeout = aVar.getTimeout();
        if (!aVar.bz().isEmpty() || aVar.bB() == null) {
            m.a(aVar.getTitle(), (String) null, aVar.getView(), false, (DialogInterface.OnCancelListener) null);
        } else {
            m.a(aVar.getTitle(), aVar.getString(), "关闭", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    aVar.bB().a(a.hr, aVar);
                }
            });
        }
        if (timeout != -2) {
            if (timeout <= 0) {
                timeout = aVar.aQ();
            }
            m.k((long) timeout);
        }
    }

    private final void a(c cVar) {
        this.ij = false;
        try {
            cVar.a(bu().bX());
        } catch (Exception e) {
            Log.w(LOG_TAG, "Exception in paint!" + e);
            e.printStackTrace();
        }
        bw();
        this.ij = true;
    }

    private final void b(c cVar) {
        h.bP(a.MSG_DEVICE_REQUEST_REFRESH);
    }

    private final void b(Runnable runnable) {
        this.ih.obj = runnable;
        if (!h.c(this.ih)) {
            h.b(this.ih);
        }
    }

    private static final MIDPDevice bu() {
        return (MIDPDevice) c.ou;
    }

    private final boolean bv() {
        return org.meteoroid.core.a.bv();
    }

    private final boolean bx() {
        return ie != null && (ie.aS() == 0 || ie.aS() == 1);
    }

    public void a(a aVar, k kVar) {
        a(kVar);
        a(aVar);
    }

    public void a(f fVar) {
        if (ie != null) {
            ie.bB().a(fVar, ie);
        }
    }

    public void a(k kVar) {
        if (kVar != ie && kVar != null) {
            if (kVar.aS() != 5) {
                if (ie != null && ie.aS() == 5) {
                    m.hO();
                }
                if (ie != null && ie.isShown()) {
                    ie.in = null;
                }
                if (ie == null || !((ie.aS() == 0 || ie.aS() == 1) && (kVar.aS() == 0 || kVar.aS() == 1))) {
                    m.a(kVar);
                    return;
                }
                ie.aN();
                ie.in = f2if;
                m.b(kVar);
                ie = kVar;
                ie.aM();
                h.d(m.MSG_VIEW_CHANGED, kVar);
                Log.d(LOG_TAG, "Fast switch canvas end.");
                return;
            }
            a((a) kVar);
        }
    }

    public void a(r rVar) {
        a(rVar.ce());
    }

    public void a(Runnable runnable) {
        b(runnable);
    }

    public boolean a(Message message) {
        if ((message.what == 44287 || message.what == 40965) && bx()) {
            if (this.ij) {
                Log.d(LOG_TAG, "Repaint later now.");
                a((c) ie);
            } else {
                if (ik > 0) {
                    ik--;
                }
                Log.d(LOG_TAG, "Still not ready for repaint. Try later for " + ik);
            }
        } else if (message.what == 23041) {
            if (message.obj != null && (message.obj instanceof k)) {
                ie = (k) message.obj;
                ie.in = this;
                this.ii = true;
                if (ie.aS() == 0) {
                    c((c) ie);
                }
            }
        } else if (message.what == 44035) {
            Log.d(LOG_TAG, "MIDP_DISPLAY_CALL_SERIALLY");
            if (message.obj != null) {
                ((Runnable) message.obj).run();
            }
            return true;
        }
        return false;
    }

    public final void aY() {
        Thread.yield();
    }

    public final void aa(int i) {
        if (!this.ii && ie != null && bv()) {
            ie.aa(i);
        }
    }

    public boolean ae(int i) {
        return false;
    }

    public int af(int i) {
        return c.ou.j("BestImageWidth", 15);
    }

    public int ag(int i) {
        return c.ou.j("BestImageHeight", 15);
    }

    public final boolean ah(int i) {
        l.bR(i);
        return true;
    }

    public final void b(int i) {
        if (!this.ii && ie != null && bv()) {
            if (ie.aS() == 1) {
                ((com.a.a.f.a) ie).s(1, i);
            }
            ie.b(i);
        }
    }

    public final synchronized void b(int i, int i2) {
        if (this.ii) {
            this.ii = false;
        }
        if (ie != null && bv()) {
            try {
                ie.b(i, i2);
            } catch (Exception e) {
            }
        }
    }

    public int bq() {
        return c.ou.j("alphaLevelCount", 255);
    }

    public int br() {
        return c.ou.j("colorCount", z.CONSTRAINT_MASK);
    }

    public k bs() {
        return ie;
    }

    public boolean bt() {
        return c.ou.f("isColor", true);
    }

    public final void bw() {
        bu().ib();
    }

    public final synchronized void c(int i, int i2) {
        if (!this.ii) {
            if (ie != null && bv()) {
                try {
                    ie.c(i, i2);
                } catch (Exception e) {
                }
            }
        }
    }

    public final void c(c cVar) {
        if (!bx() || cVar != ie) {
            Log.d(LOG_TAG, "Not a valid canvas. Skip repaint.");
        } else if (this.ij) {
            ik = 0;
            a(cVar);
        } else if (ik <= 1) {
            b(cVar);
            ik++;
        }
    }

    public void d(int i, int i2) {
        if (ie != null) {
            ie.d(i, i2);
        }
    }

    public int getColor(int i) {
        switch (i) {
            case 0:
            case 3:
            case 5:
                return 16777215;
            case 1:
            case 2:
            case 4:
            default:
                return 0;
        }
    }

    public final void h(o oVar) {
        if (bx()) {
            ((c) ie).a(oVar);
        }
    }

    public int j(boolean z) {
        return z ? 1 : 0;
    }

    public final void keyPressed(int i) {
        if (this.ii) {
            this.ii = false;
        }
        if (ie != null && bv()) {
            if (!ie.bz().isEmpty()) {
                if (i == ((MIDPDevice) c.ou).bU(1)) {
                    a(ie.bz().get(0));
                } else if (ie.bz().size() > 1 && i == ((MIDPDevice) c.ou).bU(2)) {
                    a(ie.bz().get(1));
                }
            }
            if (ie.aS() == 1) {
                ((com.a.a.f.a) ie).s(0, i);
            }
            ie.keyPressed(i);
        }
    }

    public final synchronized void n(int i, int i2) {
        if (!this.ii) {
            if (ie != null && bv()) {
                try {
                    ie.n(i, i2);
                } catch (Exception e) {
                }
            }
        }
    }
}
