package com.a.a.e;

import android.util.Log;
import android.view.View;
import org.meteoroid.core.e;
import org.meteoroid.plugin.device.MIDPDevice;

public abstract class c extends k {
    public static final int DOWN = 6;
    public static final int FIRE = 8;
    public static final int GAME_A = 9;
    public static final int GAME_B = 10;
    public static final int GAME_C = 11;
    public static final int GAME_D = 12;
    public static final int KEY_NUM0 = 48;
    public static final int KEY_NUM1 = 49;
    public static final int KEY_NUM2 = 50;
    public static final int KEY_NUM3 = 51;
    public static final int KEY_NUM4 = 52;
    public static final int KEY_NUM5 = 53;
    public static final int KEY_NUM6 = 54;
    public static final int KEY_NUM7 = 55;
    public static final int KEY_NUM8 = 56;
    public static final int KEY_NUM9 = 57;
    public static final int KEY_POUND = 35;
    public static final int KEY_STAR = 42;
    public static final int LEFT = 2;
    public static final int RIGHT = 5;
    public static final int UP = 1;

    public int R(int i) {
        return ((MIDPDevice) org.meteoroid.core.c.ou).R(i);
    }

    public int S(int i) {
        return ((MIDPDevice) org.meteoroid.core.c.ou).S(i);
    }

    public String T(int i) {
        return ((MIDPDevice) org.meteoroid.core.c.ou).T(i);
    }

    /* access modifiers changed from: protected */
    public abstract void a(o oVar);

    public void aM() {
        super.aM();
        if (aS() == 0) {
            aX();
        }
    }

    public int aS() {
        return 0;
    }

    public boolean aT() {
        return ((MIDPDevice) org.meteoroid.core.c.ou).aT();
    }

    public boolean aU() {
        return ((MIDPDevice) org.meteoroid.core.c.ou).aU();
    }

    public boolean aV() {
        return ((MIDPDevice) org.meteoroid.core.c.ou).aV();
    }

    public boolean aW() {
        return ((MIDPDevice) org.meteoroid.core.c.ou).aW();
    }

    public final void aX() {
        if (this.in != null) {
            this.in.c(this);
        }
    }

    public final void aY() {
        if (this.in != null) {
            this.in.aY();
        }
    }

    public boolean aZ() {
        return false;
    }

    public final void c(int i, int i2, int i3, int i4) {
        Log.d("midpcanvas", "repaint x:" + i + " y:" + i2 + " w:" + i3 + " h:" + i4);
        aX();
    }

    public View getView() {
        return e.ow;
    }

    public void i(boolean z) {
    }
}
