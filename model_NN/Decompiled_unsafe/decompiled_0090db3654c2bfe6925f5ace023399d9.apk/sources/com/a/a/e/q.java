package com.a.a.e;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.meteoroid.core.l;

public class q extends r {
    private p ht;
    private TextView hx;
    private LinearLayout hy;
    private ImageView hz;
    private String iQ;
    private int iR;
    private int layout;

    public q(String str, p pVar, int i, String str2) {
        this(str, pVar, i, str2, 0);
    }

    public q(String str, p pVar, int i, String str2, int i2) {
        super(str);
        Activity activity = l.getActivity();
        this.hy = new LinearLayout(activity);
        this.hy.setOrientation(1);
        this.hx = new TextView(activity);
        this.hy.addView(this.hx, new ViewGroup.LayoutParams(-1, -2));
        this.hz = new ImageView(activity);
        this.hy.addView(this.hz, new ViewGroup.LayoutParams(-1, -2));
        if (pVar != null) {
            c(pVar);
        }
        an(i);
        if (str2 != null) {
            D(str2);
        }
        am(i2);
    }

    public void D(String str) {
        this.iQ = str;
        this.hx.setText(str);
        this.hx.postInvalidate();
    }

    public p aO() {
        return this.ht;
    }

    public void am(int i) {
        this.iR = i;
    }

    public void an(int i) {
        this.layout = i;
    }

    public String bY() {
        return this.iQ;
    }

    public int bZ() {
        return this.iR;
    }

    /* renamed from: bo */
    public LinearLayout getView() {
        return this.hy;
    }

    public void c(p pVar) {
        this.ht = pVar;
        this.hz.setImageBitmap(pVar.iN);
        this.hz.postInvalidate();
    }

    public int ca() {
        return this.layout;
    }
}
