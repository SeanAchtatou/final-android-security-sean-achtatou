package com.a.a.e;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.meteoroid.core.l;

public class m extends v {
    private ScrollView iA;
    /* access modifiers changed from: private */
    public LinearLayout iB;
    /* access modifiers changed from: private */
    public List<r> iy;
    private t iz;

    public m(String str) {
        this(str, null);
    }

    public m(String str, r[] rVarArr) {
        super(str);
        this.iB = new LinearLayout(l.getActivity());
        this.iB.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.iB.setOrientation(1);
        this.iy = new ArrayList();
        if (rVarArr != null) {
            for (r b : rVarArr) {
                b(b);
            }
        }
        this.iA = new ScrollView(l.getActivity());
        this.iA.addView(this.iB);
        this.iA.setVerticalScrollBarEnabled(true);
        this.iA.setVerticalFadingEdgeEnabled(false);
        this.iA.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    }

    public int B(String str) {
        if (str != null) {
            return b(new x("", str));
        }
        throw new NullPointerException();
    }

    public void a(final int i, final r rVar) {
        this.iy.add(i, rVar);
        l.getHandler().post(new Runnable() {
            public void run() {
                m.this.iB.addView(rVar.getView(), i, new ViewGroup.LayoutParams(-1, -2));
            }
        });
    }

    public void a(t tVar) {
        this.iz = tVar;
    }

    public void aM() {
        super.aM();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = m.this.bz().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Add command " + next.bf());
                    m.this.iB.addView(next.bh());
                }
                m.this.getView().requestLayout();
            }
        });
    }

    public void aN() {
        super.aN();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = m.this.bz().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Remove command " + next.bf());
                    m.this.iB.removeView(next.bh());
                }
                m.this.getView().requestLayout();
            }
        });
    }

    public int aS() {
        return 2;
    }

    public r ai(int i) {
        return this.iy.get(i);
    }

    public int b(final r rVar) {
        if (this.iy.add(rVar)) {
            l.getHandler().post(new Runnable() {
                public void run() {
                    m.this.iB.addView(rVar.getView(), new ViewGroup.LayoutParams(-1, -2));
                }
            });
            return this.iy.size() - 1;
        }
        throw new IllegalStateException();
    }

    public void b(final int i, final r rVar) {
        this.iy.set(i, rVar);
        l.getHandler().post(new Runnable() {
            public void run() {
                m.this.iB.addView(rVar.getView(), i, new ViewGroup.LayoutParams(-1, -2));
            }
        });
        this.iB.removeViewAt(i + 1);
    }

    public t bI() {
        return this.iz;
    }

    public void ba() {
        this.iy.clear();
        l.getHandler().post(new Runnable() {
            public void run() {
                m.this.iB.removeAllViews();
            }
        });
    }

    public int d(p pVar) {
        if (pVar != null) {
            return b(new q("", pVar, 0, null));
        }
        throw new NullPointerException();
    }

    public void delete(final int i) {
        this.iy.remove(i);
        l.getHandler().post(new Runnable() {
            public void run() {
                m.this.iB.removeViewAt(i);
            }
        });
    }

    public View getView() {
        return this.iA;
    }

    /* access modifiers changed from: protected */
    public void i() {
        l.getHandler().post(new Runnable() {
            public void run() {
                for (r i : m.this.iy) {
                    i.i();
                }
                m.this.iB.clearFocus();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void j() {
        l.getHandler().post(new Runnable() {
            public void run() {
                for (r j : m.this.iy) {
                    j.j();
                }
                m.this.iB.clearFocus();
            }
        });
    }

    public int size() {
        return this.iy.size();
    }
}
