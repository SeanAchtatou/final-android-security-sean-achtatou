package com.a.a.e;

import android.graphics.Typeface;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Iterator;
import org.meteoroid.core.l;

public class y extends v {
    /* access modifiers changed from: private */
    public TextView hx;
    /* access modifiers changed from: private */
    public LinearLayout hy;
    private v iS = null;
    private int ji;
    private int jj;
    /* access modifiers changed from: private */
    public EditText jk;

    public y(String str, final String str2, final int i, final int i2) {
        super(str);
        l.getHandler().post(new Runnable() {
            public void run() {
                LinearLayout unused = y.this.hy = new LinearLayout(l.getActivity());
                y.this.hy.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                y.this.hy.setOrientation(1);
                TextView unused2 = y.this.hx = new TextView(l.getActivity());
                y.this.hx.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
                EditText unused3 = y.this.jk = new EditText(l.getActivity());
                y.this.jk.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
                y.this.hy.addView(y.this.hx);
                y.this.hy.addView(y.this.jk);
                switch (i2) {
                    case 0:
                        y.this.jk.setSingleLine(true);
                        y.this.jk.setInputType(1);
                        break;
                    case 1:
                        y.this.jk.setSingleLine(true);
                        y.this.jk.setInputType(48);
                        break;
                    case 2:
                        y.this.jk.setSingleLine(true);
                        y.this.jk.setInputType(2);
                        break;
                    case 3:
                        y.this.jk.setSingleLine(true);
                        y.this.jk.setInputType(3);
                        break;
                    case 4:
                        y.this.jk.setSingleLine(true);
                        y.this.jk.setInputType(16);
                        break;
                    case 5:
                        y.this.jk.setSingleLine(true);
                        y.this.jk.setInputType(12290);
                        break;
                    case 65536:
                        y.this.jk.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        y.this.jk.setTypeface(Typeface.MONOSPACE);
                        break;
                }
                y.this.ap(i);
                y.this.jk.setText(str2 == null ? "" : str2);
                synchronized (y.this) {
                    y.this.notify();
                }
            }
        });
        synchronized (this) {
            try {
                wait(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void F(String str) {
    }

    public int a(char[] cArr) {
        return getString().toCharArray().length;
    }

    public void a(char[] cArr, int i, int i2, int i3) {
        e(new String(cArr, i, i2), i3);
    }

    public void aM() {
        super.aM();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = y.this.bz().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Add command " + next.bf());
                    y.this.getView().addView(next.bh());
                }
                y.this.getView().requestLayout();
            }
        });
    }

    public void aN() {
        super.aN();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = y.this.bz().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Remove command " + next.bf());
                    y.this.getView().removeView(next.bh());
                }
                y.this.getView().requestLayout();
            }
        });
    }

    public int aS() {
        return 4;
    }

    public int ap(int i) {
        this.jk.setFilters(new InputFilter[]{new InputFilter.LengthFilter(i)});
        this.ji = i;
        return i;
    }

    public void aq(int i) {
        this.jj = i;
    }

    public void b(char[] cArr, int i, int i2) {
        setString(new String(cArr, i, i2));
    }

    /* renamed from: bo */
    public LinearLayout getView() {
        return this.hy;
    }

    public int cj() {
        return this.jj;
    }

    public int ck() {
        return 0;
    }

    public void e(String str, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getString().substring(0, i));
        stringBuffer.append(str);
        stringBuffer.append(getString().substring(i + 1));
        setString(stringBuffer.toString());
    }

    public int getMaxSize() {
        return this.ji;
    }

    public String getString() {
        return this.jk.getText().toString();
    }

    /* access modifiers changed from: protected */
    public void i() {
        this.jk.clearFocus();
    }

    public void invalidate() {
        if (this.iS != null) {
            getView().postInvalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void j() {
        l.hr().hideSoftInputFromWindow(this.jk.getWindowToken(), 2);
        Log.d("TextBox", "Force close soft input.");
    }

    public void r(int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer(getString());
        stringBuffer.delete(i, i + i2);
        setString(stringBuffer.toString());
    }

    public void setString(final String str) {
        l.getHandler().post(new Runnable() {
            public void run() {
                y.this.jk.setText(str);
            }
        });
    }

    public int size() {
        return getString().length();
    }
}
