package com.a.a.e;

import android.app.Activity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.meteoroid.core.l;

public class u extends v implements AdapterView.OnItemClickListener, d {
    public static f iV;
    /* access modifiers changed from: private */
    public ArrayAdapter<String> hH;
    /* access modifiers changed from: private */
    public ArrayAdapter<p> hI;
    /* access modifiers changed from: private */
    public ListView hK;
    private int hM;
    private LinearLayout hy;
    private int iW;

    public u(String str, int i) {
        this(str, i, null, null);
    }

    public u(String str, int i, String[] strArr, p[] pVarArr) {
        super(str);
        this.hM = i;
        strArr = strArr == null ? new String[0] : strArr;
        pVarArr = pVarArr == null ? new p[0] : pVarArr;
        iV = new f("", 1, 0);
        Activity activity = l.getActivity();
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(strArr));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(Arrays.asList(pVarArr));
        this.hH = new ArrayAdapter<>(activity, Y(i), arrayList);
        this.hH.setNotifyOnChange(true);
        this.hI = new ArrayAdapter<>(activity, Y(i), arrayList2);
        this.hK = new ListView(activity);
        this.hK.setAdapter((ListAdapter) this.hH);
        this.hK.setBackgroundColor(-16777216);
        this.hK.setChoiceMode(Z(i));
        this.hK.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                return keyEvent.getAction() == 0 && i == 4;
            }
        });
        this.hK.setOnItemClickListener(this);
        this.hy = new LinearLayout(l.getActivity());
        this.hy.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.hy.setOrientation(1);
        this.hy.addView(this.hK);
    }

    private int Y(int i) {
        switch (i) {
            case 1:
                return 17367055;
            case 2:
                return 17367056;
            case 3:
            default:
                return 17367043;
        }
    }

    private int Z(int i) {
        switch (i) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 0;
            default:
                throw new IllegalArgumentException("Invalid listType.");
        }
    }

    public l U(int i) {
        return l.bD();
    }

    public p V(int i) {
        return this.hI.getItem(i);
    }

    public boolean W(int i) {
        if (this.hM == 3 && i == this.iW) {
            return true;
        }
        return this.hK.isItemChecked(i);
    }

    public void X(int i) {
    }

    public int a(final String str, final p pVar) {
        l.getHandler().post(new Runnable() {
            public void run() {
                if (str != null) {
                    u.this.hH.add(str);
                }
                if (pVar != null) {
                    u.this.hI.add(pVar);
                }
            }
        });
        return this.hH.getCount() + 1;
    }

    public int a(boolean[] zArr) {
        if (zArr.length > this.hH.getCount()) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < zArr.length; i++) {
            zArr[i] = this.hK.isItemChecked(i);
        }
        return zArr.length;
    }

    public void a(int i, l lVar) {
    }

    public void a(final int i, final String str, final p pVar) {
        l.getHandler().post(new Runnable() {
            public void run() {
                u.this.hH.insert(str, i);
                u.this.hI.insert(pVar, i);
            }
        });
    }

    public void aM() {
        super.aM();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = u.this.bz().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Add command " + next.bf());
                    u.this.getView().addView(next.bh());
                }
                u.this.getView().requestLayout();
            }
        });
    }

    public void aN() {
        super.aN();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = u.this.bz().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Remove command " + next.bf());
                    u.this.getView().removeView(next.bh());
                }
                u.this.getView().requestLayout();
            }
        });
    }

    public int aS() {
        return 3;
    }

    public void b(final int i, final String str, final p pVar) {
        l.getHandler().post(new Runnable() {
            public void run() {
                u.this.a(i, str, pVar);
                u.this.delete(i + 1);
            }
        });
    }

    public void b(final boolean[] zArr) {
        if (zArr.length > this.hH.getCount()) {
            throw new IllegalArgumentException();
        }
        l.getHandler().post(new Runnable() {
            public void run() {
                for (int i = 0; i < zArr.length; i++) {
                    u.this.hK.setItemChecked(i, zArr[i]);
                }
            }
        });
    }

    public void ba() {
        l.getHandler().post(new Runnable() {
            public void run() {
                u.this.hH.clear();
                u.this.hI.clear();
            }
        });
    }

    public int bb() {
        return 0;
    }

    public int bc() {
        if (this.hM == 2) {
            return -1;
        }
        if (this.hM == 3) {
            return this.iW;
        }
        for (int i = 0; i < this.hK.getCount(); i++) {
            if (this.hK.isItemChecked(i)) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: bo */
    public LinearLayout getView() {
        return this.hy;
    }

    public void delete(final int i) {
        l.getHandler().post(new Runnable() {
            public void run() {
                u.this.hH.remove(u.this.hH.getItem(i));
                u.this.hI.remove(u.this.hI.getItem(i));
            }
        });
    }

    public void e(final int i, final boolean z) {
        l.getHandler().post(new Runnable() {
            public void run() {
                u.this.hK.setItemChecked(i, z);
            }
        });
    }

    public void e(f fVar) {
        iV = fVar;
    }

    public String getString(int i) {
        return this.hH.getItem(i);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (this.hM == 3) {
            this.iW = i;
            bB().a(iV, this);
        }
    }

    public int size() {
        return this.hH.getCount();
    }
}
