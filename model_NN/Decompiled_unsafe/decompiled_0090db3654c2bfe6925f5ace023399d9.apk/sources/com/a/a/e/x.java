package com.a.a.e;

import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.meteoroid.core.l;

public class x extends r {
    private final Activity hA;
    private Button hP;
    private TextView hx;
    private LinearLayout hy;
    /* access modifiers changed from: private */
    public f iT;
    private String jd;
    private int je;
    private l jf;
    private TextView jg;
    private String url;

    public x(String str, String str2) {
        this(str, str2, 0);
    }

    public x(String str, String str2, int i) {
        super(str);
        this.hA = l.getActivity();
        this.iT = new f("", 8, 0);
        this.hy = new LinearLayout(this.hA);
        this.hy.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.hy.setOrientation(1);
        this.hx = new TextView(this.hA);
        this.hP = new Button(this.hA);
        this.jg = new TextView(this.hA);
        if (str != null) {
            this.jg.setText(str);
            this.jg.setTextSize(20.0f);
            this.hy.addView(this.jg, new ViewGroup.LayoutParams(-2, -2));
        }
        switch (i) {
            case 0:
                if (str2 != null) {
                    this.hx.setText(str2);
                }
                this.hx.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (x.this.iT != null && x.this.ch() != null) {
                            x.this.ch().a(x.this.iT, x.this);
                        }
                    }
                });
                this.hy.addView(this.hx, new ViewGroup.LayoutParams(-1, -2));
                this.hy.postInvalidate();
                break;
            case 1:
                if (str2 != null) {
                    this.url = "<a href=tel:" + str2 + ">" + str2 + "</a>";
                }
                this.hx.setText(Html.fromHtml(this.url));
                this.hx.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (x.this.iT != null && x.this.ch() != null) {
                            x.this.ch().a(x.this.iT, x.this);
                        }
                    }
                });
                this.hy.addView(this.hx, new ViewGroup.LayoutParams(-1, -2));
                this.hy.postInvalidate();
                break;
            case 2:
                this.hP.setText(str2);
                this.hy.addView(this.hP, new ViewGroup.LayoutParams(-2, -2));
                this.hP.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (x.this.iT != null && x.this.ch() != null) {
                            x.this.ch().a(x.this.iT, x.this);
                        }
                    }
                });
                this.hy.postInvalidate();
                break;
        }
        ao(i);
    }

    public void a(l lVar) {
        this.jf = lVar;
    }

    public void ao(int i) {
        this.je = i;
    }

    public l bP() {
        return this.jf;
    }

    /* renamed from: bo */
    public LinearLayout getView() {
        return this.hy;
    }

    public int ci() {
        return this.je;
    }

    public void d(f fVar) {
        this.iT = fVar;
    }

    public String getText() {
        return this.jd;
    }

    public void setText(String str) {
        this.jd = str;
        this.hx.setText(str);
        this.hx.postInvalidate();
    }
}
