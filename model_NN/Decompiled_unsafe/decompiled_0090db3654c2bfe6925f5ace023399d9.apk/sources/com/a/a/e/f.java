package com.a.a.e;

import android.view.View;
import android.widget.Button;
import org.meteoroid.core.h;
import org.meteoroid.core.i;
import org.meteoroid.core.l;
import org.meteoroid.plugin.device.MIDPDevice;

public class f implements View.OnClickListener, i.c {
    public static final int BACK = 2;
    public static final int CANCEL = 3;
    public static final int EXIT = 7;
    public static final int HELP = 5;
    public static final int ITEM = 8;
    public static final int OK = 4;
    public static final int SCREEN = 1;
    public static final int STOP = 6;
    private int hO;
    private Button hP;
    private String label;
    private int priority;

    public f(String str, int i, int i2) {
        this.label = str;
        this.hO = i;
        this.priority = i2;
        this.hP = new Button(l.getActivity());
        this.hP.setText(str);
        this.hP.setOnClickListener(this);
    }

    public f(String str, String str2, int i, int i2) {
        this(str, i, i2);
    }

    public int be() {
        return this.hO;
    }

    public String bf() {
        return this.label;
    }

    public String bg() {
        return this.label;
    }

    public Button bh() {
        return this.hP;
    }

    public void bi() {
        h.b(h.c(MIDPDevice.MSG_MIDP_COMMAND_EVENT, this));
    }

    public int getId() {
        return this.priority;
    }

    public int getPriority() {
        return this.priority;
    }

    public void onClick(View view) {
        h.b(h.c(MIDPDevice.MSG_MIDP_COMMAND_EVENT, this));
    }
}
