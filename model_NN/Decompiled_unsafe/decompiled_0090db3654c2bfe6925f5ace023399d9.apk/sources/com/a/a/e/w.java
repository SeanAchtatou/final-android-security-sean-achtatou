package com.a.a.e;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import org.meteoroid.core.l;

public class w extends r {
    private LinearLayout hy;

    public w(int i, int i2) {
        super("");
        this.hy = new LinearLayout(l.getActivity());
        q(i, i2);
    }

    public w(String str) {
        this(10, 10);
    }

    public View getView() {
        return this.hy;
    }

    public void q(int i, int i2) {
        if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException();
        }
        this.hy.setLayoutParams(new ViewGroup.LayoutParams(i, i2));
    }
}
