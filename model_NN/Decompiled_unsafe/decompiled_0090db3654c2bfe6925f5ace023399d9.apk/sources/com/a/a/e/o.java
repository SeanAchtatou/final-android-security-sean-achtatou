package com.a.a.e;

public class o {
    public static final int BASELINE = 64;
    public static final int BOTTOM = 32;
    public static final int DOTTED = 1;
    public static final int HCENTER = 1;
    public static final int LEFT = 4;
    public static final int RIGHT = 8;
    public static final int SOLID = 0;
    public static final int TOP = 16;
    public static final int VCENTER = 2;
    private static final StringBuffer iM = new StringBuffer();
    private int color;
    private int iK = 0;
    private int iL = 0;

    public void a(char c, int i, int i2, int i3) {
        iM.delete(0, iM.length());
        iM.append(c);
        a(iM.toString(), i, i2, i3);
    }

    public void a(l lVar) {
    }

    public void a(p pVar, int i, int i2, int i3) {
    }

    public void a(p pVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
    }

    public void a(String str, int i, int i2, int i3) {
    }

    public void a(String str, int i, int i2, int i3, int i4, int i5) {
    }

    public void a(char[] cArr, int i, int i2, int i3, int i4, int i5) {
        iM.delete(0, iM.length());
        iM.append(cArr, i, i2);
        a(iM.toString(), i3, i4, i5);
    }

    public void a(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
    }

    public void aj(int i) {
        if (i < 0 || i > 255) {
            throw new IllegalArgumentException();
        }
        l(i, i, i);
    }

    public void ak(int i) {
    }

    public int al(int i) {
        return -1;
    }

    public void b(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public int bK() {
        return getColor() & 255;
    }

    public int bL() {
        return -1;
    }

    public int bM() {
        return -1;
    }

    public int bN() {
        return -1;
    }

    public int bO() {
        return -1;
    }

    public l bP() {
        return null;
    }

    public int bQ() {
        return ((bS() + bR()) + bK()) / 3;
    }

    public int bR() {
        return (getColor() >> 8) & 255;
    }

    public int bS() {
        return (getColor() >> 16) & 255;
    }

    public int bT() {
        return -1;
    }

    public int bU() {
        return this.iK;
    }

    public int bV() {
        return this.iL;
    }

    public void bW() {
    }

    public void c(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public void c(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
    }

    public void d(int i, int i2, int i3, int i4) {
    }

    public void d(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public void e(int i, int i2, int i3, int i4) {
    }

    public void e(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public void f(int i, int i2, int i3, int i4) {
    }

    public void f(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public void fillRect(int i, int i2, int i3, int i4) {
    }

    public int getColor() {
        return this.color;
    }

    public void l(int i, int i2, int i3) {
        setColor((i2 << 8) + i3 + (i << 16));
    }

    public void setClip(int i, int i2, int i3, int i4) {
    }

    public void setColor(int i) {
        this.color = i;
    }

    public void translate(int i, int i2) {
        this.iK += i;
        this.iL += i2;
    }
}
