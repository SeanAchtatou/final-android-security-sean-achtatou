package com.a.a.e;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import org.meteoroid.core.l;

public class i extends r {
    public static final int DATE = 1;
    public static final int DATE_TIME = 3;
    public static final int TIME = 2;
    private Calendar calendar;
    private Date hS;
    private DatePicker hT;
    private TimePicker hU;
    /* access modifiers changed from: private */
    public int hV;
    /* access modifiers changed from: private */
    public int hW;
    /* access modifiers changed from: private */
    public int hX;
    /* access modifiers changed from: private */
    public int hY;
    /* access modifiers changed from: private */
    public int hZ;
    /* access modifiers changed from: private */
    public TextView hx;
    private LinearLayout hy;
    DatePicker.OnDateChangedListener ia = new DatePicker.OnDateChangedListener() {
        public void onDateChanged(DatePicker datePicker, int i, int i2, int i3) {
            int unused = i.this.hV = i;
            int unused2 = i.this.hW = i2;
            int unused3 = i.this.hX = i3;
            i.this.hx.setText(i.this.label + i.this.hV + "/" + (i.this.hW + 1) + "/" + i.this.hX);
        }
    };
    TimePicker.OnTimeChangedListener ib = new TimePicker.OnTimeChangedListener() {
        public void onTimeChanged(TimePicker timePicker, int i, int i2) {
            int unused = i.this.hY = i;
            int unused2 = i.this.hZ = i2;
            i.this.hx.setText(i.this.label + i.this.hY + ":" + i.this.hZ);
        }
    };
    /* access modifiers changed from: private */
    public String label;
    private int mode;

    public i(String str, int i) {
        super(str);
        d(str, i);
    }

    public i(String str, int i, TimeZone timeZone) {
        super(str);
        d(str, i);
    }

    public void ad(int i) {
        this.mode = i;
    }

    /* renamed from: bo */
    public LinearLayout getView() {
        return this.hy;
    }

    public int bp() {
        return this.mode;
    }

    public void d(String str, int i) {
        this.label = str;
        this.mode = i;
        Activity activity = l.getActivity();
        this.calendar = Calendar.getInstance();
        this.hV = this.calendar.get(1);
        this.hW = this.calendar.get(2);
        this.hX = this.calendar.get(5);
        this.hY = this.calendar.get(10);
        this.hZ = this.calendar.get(12);
        this.hy = new LinearLayout(activity);
        this.hy.setBackgroundColor(-16777216);
        this.hy.setOrientation(1);
        this.hx = new TextView(activity);
        this.hx.setText(str);
        this.hy.addView(this.hx, new ViewGroup.LayoutParams(-2, -2));
        this.hT = new DatePicker(activity);
        this.hU = new TimePicker(activity);
        this.hU.setOnTimeChangedListener(this.ib);
        this.hT.init(this.hV, this.hW, this.hX, this.ia);
        switch (i) {
            case 1:
                this.hy.addView(this.hT, new ViewGroup.LayoutParams(-2, -2));
                return;
            case 2:
                this.hy.addView(this.hU, new ViewGroup.LayoutParams(-2, -2));
                return;
            case 3:
                this.hy.addView(this.hT, new ViewGroup.LayoutParams(-2, -2));
                this.hy.addView(this.hU, new ViewGroup.LayoutParams(-2, -2));
                return;
            default:
                return;
        }
    }

    public Date getDate() {
        return this.hS;
    }

    public void setDate(Date date) {
        this.hS = date;
    }
}
