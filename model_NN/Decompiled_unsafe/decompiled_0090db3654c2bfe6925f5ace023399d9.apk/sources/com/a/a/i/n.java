package com.a.a.i;

public class n extends RuntimeException {
    private static final long serialVersionUID = 1;
    private int kG = -1;

    public n() {
    }

    public n(String str) {
        super(str);
    }

    public n(String str, int i) {
        super(str);
        this.kG = i;
    }

    public int getField() {
        return this.kG;
    }
}
