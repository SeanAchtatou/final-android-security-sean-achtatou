package com.a.a.i;

public class h extends Exception {
    public static final int FEATURE_NOT_SUPPORTED = 0;
    public static final int GENERAL_ERROR = 1;
    public static final int LIST_CLOSED = 2;
    public static final int LIST_NOT_ACCESSIBLE = 3;
    public static final int MAX_CATEGORIES_EXCEEDED = 4;
    public static final int UNSUPPORTED_VERSION = 5;
    public static final int UPDATE_ERROR = 6;
    private static final long serialVersionUID = 1;
    private int kI = 1;

    public h() {
    }

    public h(String str) {
        super(str);
    }

    public h(String str, int i) {
        super(str);
        this.kI = i;
    }

    public int dH() {
        return this.kI;
    }
}
