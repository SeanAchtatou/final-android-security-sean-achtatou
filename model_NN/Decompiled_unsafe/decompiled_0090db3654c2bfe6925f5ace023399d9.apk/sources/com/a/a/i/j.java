package com.a.a.i;

import java.util.Enumeration;

public interface j {
    public static final String UNCATEGORIZED = null;

    boolean K(int i, int i2);

    boolean L(int i, int i2);

    String M(int i, int i2);

    Enumeration<i> Y(String str);

    Enumeration<i> Z(String str);

    Enumeration<i> a(i iVar);

    boolean aT(int i);

    int[] aU(int i);

    int[] aV(int i);

    int aW(int i);

    String aX(int i);

    String aY(int i);

    int aZ(int i);

    boolean aa(String str);

    void addCategory(String str);

    int ba(int i);

    void c(String str, boolean z);

    void close();

    String[] dL();

    int dM();

    Enumeration<i> dN();

    int[] dO();

    String getName();

    void h(String str, String str2);
}
