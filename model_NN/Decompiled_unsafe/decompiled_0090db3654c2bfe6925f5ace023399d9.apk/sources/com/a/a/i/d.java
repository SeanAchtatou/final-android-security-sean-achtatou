package com.a.a.i;

import java.util.Enumeration;

public interface d extends j {
    public static final int ENDING = 1;
    public static final int OCCURRING = 2;
    public static final int STARTING = 0;

    c a(c cVar);

    Enumeration<c> a(int i, long j, long j2, boolean z);

    int[] aP(int i);

    void b(c cVar);

    c dF();
}
