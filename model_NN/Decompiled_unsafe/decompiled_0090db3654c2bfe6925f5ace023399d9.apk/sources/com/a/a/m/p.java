package com.a.a.m;

import java.util.HashMap;

public class p {
    public static final HashMap<String, p> lN = new HashMap<>();
    private static String[] lO = {"m/s^2", "Celsius", "degree"};
    private String lM;

    private p() {
    }

    public static p ad(String str) {
        if (lN.isEmpty()) {
            for (int i = 0; i < lO.length; i++) {
                p pVar = new p();
                pVar.lM = lO[i];
                lN.put(lO[i], pVar);
            }
        }
        return lN.get(str);
    }

    public String toString() {
        return this.lM;
    }
}
