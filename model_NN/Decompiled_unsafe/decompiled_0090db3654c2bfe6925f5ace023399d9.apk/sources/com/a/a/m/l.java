package com.a.a.m;

import com.a.a.c.b;

public interface l extends b {
    public static final int STATE_CLOSED = 4;
    public static final int STATE_LISTENING = 2;
    public static final int STATE_OPENED = 1;

    a a(b bVar);

    void a(g gVar, int i);

    void a(g gVar, int i, long j, boolean z, boolean z2, boolean z3);

    e[] a(int i, long j, boolean z, boolean z2, boolean z3);

    e[] bn(int i);

    String bo(int i);

    int[] eK();

    m eL();

    void eM();

    int getState();
}
