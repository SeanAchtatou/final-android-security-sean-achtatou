package com.a.a.o;

import android.util.Log;
import com.a.a.c.b;
import com.a.a.m.a;
import com.a.a.m.e;
import com.a.a.m.g;
import com.a.a.m.l;
import com.a.a.m.m;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class f implements l {
    private g lL = new g();
    private c lQ = new c();
    private a lR;
    private int[] mA;
    private String mB;
    private int mC;
    private long mD;
    private boolean mE;
    private boolean mF;
    private boolean mG;
    /* access modifiers changed from: private */
    public e mc = new e();
    private e[] mt;
    private g mu;
    private m mv;
    private b mw;
    private a mx = new a();
    private BufferedReader my = new BufferedReader(this.mz);
    private Reader mz = new Reader() {
        public void close() {
        }

        public int read(char[] cArr, int i, int i2) {
            String[] strArr = f.this.mc.strings;
            StringBuffer stringBuffer = new StringBuffer();
            for (int i3 = 0; i3 < f.this.mc.strings.length; i3++) {
                stringBuffer = stringBuffer.append(strArr[i3]);
                i2 = stringBuffer.length();
                for (int i4 = 0; i4 < i2; i4++) {
                    cArr[i4] = stringBuffer.charAt(i4);
                }
            }
            return i2;
        }
    };
    private int state;

    public f() {
        try {
            this.mC = this.my.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public a a(com.a.a.m.b bVar) {
        this.lR = this.mx;
        return this.lR;
    }

    public void a(g gVar, int i) {
        this.mu = gVar;
        this.mC = i;
        this.mu.a(this, this.mt, true);
    }

    public void a(g gVar, int i, long j, boolean z, boolean z2, boolean z3) {
        this.mu = gVar;
        this.mC = i;
        this.mD = j;
        this.mE = z;
        this.mF = z2;
        this.mG = z3;
        this.mu.a(this, this.mt, true);
    }

    public e[] a(int i, long j, boolean z, boolean z2, boolean z3) {
        this.mC = i;
        this.mD = j;
        this.mE = z;
        this.mF = z2;
        this.mG = z3;
        for (int i2 = 0; i2 < this.mt.length; i2++) {
            this.mt[i2] = this.lQ;
            System.currentTimeMillis();
        }
        return this.mt;
    }

    public e[] bn(int i) {
        this.mC = i;
        for (int i2 = 0; i2 < this.mt.length; i2++) {
            this.mt[i2] = this.lQ;
        }
        return this.mt;
    }

    public String bo(int i) {
        for (int i2 : this.mA) {
            this.mB = m.PROP_IS_REPORTING_ERRORS;
            Log.i("Get Error Text", this.mB + "**********");
        }
        return this.mB;
    }

    public void close() {
        this.mw.close();
    }

    public int[] eK() {
        return this.mA;
    }

    public m eL() {
        this.mv = this.lL;
        return this.mv;
    }

    public void eM() {
        this.mu.a(this, this.mt, false);
    }

    public int getState() {
        switch (this.state) {
            case 1:
                a(this.mu, this.mC);
                a(this.mu, this.mC, this.mD, this.mE, this.mF, this.mG);
                break;
            case 2:
                eM();
                break;
            case 4:
                try {
                    close();
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
        }
        return this.state;
    }
}
