package com.a.a.o;

import android.hardware.SensorManager;
import com.a.a.m.b;
import com.a.a.m.m;
import java.util.HashMap;
import org.meteoroid.core.l;

public class g implements m {
    private int ji;
    private c lQ = new c();
    private SensorManager mI = ((SensorManager) l.getActivity().getSystemService("sensor"));
    private b[] mJ = new b[3];
    private b mK = new b();
    private int mL;
    private String mM;
    private String mN;
    private String mO;
    private HashMap<String, String> mP = new HashMap<>();
    private HashMap<String, String> mQ = new HashMap<>();
    private HashMap<String, String> mR = new HashMap<>();
    private String[] mS = {m.CONTEXT_TYPE_USER, m.CONTEXT_TYPE_DEVICE, m.CONTEXT_TYPE_DEVICE};
    private String[] mT = {"Accelerometer", "Thermometer", "Orientation"};
    private int[] mU = {1, 7, 3};
    private String[] mV = {"acc01", "tem01", "ori01"};
    private e mc = new e();
    private String md;
    private String[] mh = new String[4];
    private int mo = this.mc.mo;
    private String[] strings = new String[3];
    private String url;

    public g() {
        this.mI.getDefaultSensor(this.mo);
        this.strings = this.mc.strings;
        this.mh = this.mK.mh;
    }

    public b[] eN() {
        for (int i = 0; i < this.mJ.length; i++) {
            this.mJ[i] = this.mK;
        }
        return this.mJ;
    }

    public int eO() {
        this.mM = eP();
        if (this.mM.equals(m.CONTEXT_TYPE_AMBIENT)) {
            this.mL = 2;
        }
        if (this.mM.equals(m.CONTEXT_TYPE_DEVICE)) {
            this.mL = 4;
        }
        if (this.mM.equals(m.CONTEXT_TYPE_USER)) {
            this.mL = 1;
        }
        if (this.mM.equals(m.CONTEXT_TYPE_VEHICLE)) {
            this.mL = 8;
        }
        return this.mL;
    }

    public String eP() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.mh.length) {
                return this.mM;
            }
            this.mP.put(this.mh[i2], this.mS[i2]);
            if (this.md == eR() && this.mP.containsKey(this.md)) {
                this.mM = this.mP.get(this.md);
            }
            i = i2 + 1;
        }
    }

    public String[] eQ() {
        return this.strings;
    }

    public String eR() {
        for (int i = 0; i < this.mU.length; i++) {
            if (this.mo == this.mU[i]) {
                this.md = this.mh[i];
                this.mK.md = this.md;
            }
        }
        return this.md;
    }

    public boolean eS() {
        this.mL = eO();
        return this.mL != 1;
    }

    public boolean eT() {
        this.mL = eO();
        return this.mL != 1;
    }

    public String getDescription() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.mh.length) {
                return "Simple Gui " + this.mN + " Sensor";
            }
            this.mQ.put(this.mh[i2], this.mT[i2]);
            if (this.md == eR() && this.mQ.containsKey(this.md)) {
                this.mN = this.mQ.get(this.md);
            }
            i = i2 + 1;
        }
    }

    public int getMaxBufferSize() {
        this.ji = this.lQ.toString().length();
        for (int i = 0; i < this.ji; i++) {
            this.lQ.isValid(i);
            this.lQ.bl(i);
            this.lQ.bm(i);
        }
        return this.ji;
    }

    public String getModel() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.mh.length) {
                return this.mO;
            }
            this.mR.put(this.mh[i2], this.mV[i2]);
            if (this.md == eR() && this.mR.containsKey(this.md)) {
                this.mO = this.mR.get(this.md);
            }
            i = i2 + 1;
        }
    }

    public Object getProperty(String str) {
        String str2 = null;
        for (int i = 0; i < this.strings.length; i++) {
            str2 = this.strings[i];
        }
        return str2;
    }

    public String getUrl() {
        this.url = "sensor:" + eR() + ";contextType=" + eP() + ";model=" + getModel();
        return this.url;
    }

    public boolean isAvailable() {
        return true;
    }
}
