package com.a.a.o;

import android.util.Log;
import com.a.a.m.i;
import com.a.a.m.p;
import java.util.HashMap;

public class b implements com.a.a.m.b {
    private int accuracy;
    private int gm;
    private double lB;
    private double lC;
    private double lD;
    private String lM;
    private String[] lO;
    private c lQ;
    private p lY;
    private i[] lZ;
    private i ma;
    private d mb;
    private e mc;
    public String md;

    /* renamed from: me  reason: collision with root package name */
    private int f3me;
    private double mf;
    private HashMap<String, String> mg;
    public String[] mh;
    private int[] mi;
    private String name;
    private String[] strings;

    public b() {
        this.lZ = new i[1];
        this.strings = new String[3];
        this.mg = new HashMap<>();
        this.mh = new String[]{"acceleration", "temperature", "orientation"};
        this.lO = new String[]{"m/s^2", "Celsius", "degree"};
        this.mi = new int[]{0, -1, -2, -3};
        this.mb = new d();
        this.lB = this.mb.lB;
        this.lC = this.mb.lC;
        this.ma = new i(this.lB, this.lC, eX());
        this.mc = new e();
        this.strings = this.mc.strings;
        this.lQ = new c();
    }

    public double eX() {
        for (int i = 0; i < this.mh.length; i++) {
            this.mg.put(this.lO[i], this.mh[i]);
            if (this.mg.containsKey(this.lO[i])) {
                String str = this.mg.get(this.lO[i]);
                if (str == this.mh[0]) {
                    if (this.mf >= -19.61d && this.mf <= 19.61d) {
                        this.lD = 0.01d;
                    } else if (this.mf >= -58.84d && this.mf <= 58.84d) {
                        this.lD = 0.03d;
                    }
                } else if (str == this.mh[1]) {
                    this.lD = 1.0d;
                } else if (str == this.mh[2]) {
                    this.lD = 1.0d;
                }
            }
        }
        return this.lD;
    }

    public i[] eu() {
        for (int i = 0; i < this.lZ.length; i++) {
            this.lZ[i] = this.ma;
        }
        return this.lZ;
    }

    public int ev() {
        if (getAccuracy() == -1.0f) {
            this.lQ.mm = 0.0f;
        } else if (this.lQ.mm > 0.0f) {
            this.f3me = this.mi[0];
        } else if (this.lQ.mm == ((float) (((double) this.lQ.mm) * 0.1d))) {
            this.f3me = this.mi[1];
        } else if (this.lQ.mm == ((float) (((double) this.lQ.mm) * 0.01d))) {
            this.f3me = this.mi[2];
        } else if (this.lQ.mm == ((float) (((double) this.lQ.mm) * 0.001d))) {
            this.f3me = this.mi[3];
        }
        return this.f3me;
    }

    public p ew() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.lO.length) {
                return this.lY;
            }
            this.mg.put(this.mh[i2], this.lO[i2]);
            if (this.mg.containsKey(this.mh[i2]) && this.md == this.mh[i2]) {
                this.lM = this.mg.get(this.mh[i2]);
                try {
                    this.lY = p.ad(this.lM);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e2) {
                    e2.printStackTrace();
                }
            }
            i = i2 + 1;
        }
    }

    public float getAccuracy() {
        this.accuracy = this.mc.accuracy;
        Log.i("getAccuracy", this.accuracy + "******");
        return (float) this.accuracy;
    }

    public int getDataType() {
        if (this.lB == this.lQ.ex()[0]) {
            this.gm = 1;
        } else if (this.lB == ((double) this.lQ.ey()[0])) {
            this.gm = 2;
        } else {
            this.gm = 4;
        }
        return this.gm;
    }

    public String getName() {
        for (String str : this.strings) {
            this.name = str;
            Log.i("Get Name", this.name + "******");
        }
        return this.name;
    }
}
