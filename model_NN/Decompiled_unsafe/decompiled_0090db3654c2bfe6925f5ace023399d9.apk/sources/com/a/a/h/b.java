package com.a.a.h;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import com.a.a.g.c;
import com.a.a.g.g;
import com.a.a.g.h;
import com.a.a.g.i;
import com.a.a.g.j;
import org.meteoroid.core.l;

public class b extends j {
    public static final int ACCURACY_THRESHOLD = 50;
    public static final float DEFAULT_MINIMAL_LOCATION_DISTANCE = 1.0f;
    public static final int DEFAULT_MINIMAL_LOCATION_UPDATES = 1000;
    public static final int DEFAULT_POWER_REQIREMENT = 2;
    private static LocationManager kv;
    private String kf;
    private a kw;
    private a kx;
    private int state = 3;

    private final class a extends Thread {
        private Looper ky;

        public a(String str) {
            super(str);
        }

        public Looper getLooper() {
            while (this.ky == null) {
                try {
                    Thread.sleep(30);
                    System.out.println("Waited for 30ms for the looper to prepare.");
                } catch (InterruptedException e) {
                }
            }
            return this.ky;
        }

        public void run() {
            Looper.prepare();
            this.ky = Looper.myLooper();
            Looper.loop();
        }
    }

    private b(String str) {
        this.kf = str;
        this.kx = new a("LocationUpdateThread");
        this.kx.start();
    }

    private String aB(int i) {
        switch (i) {
            case 1:
                return "Available";
            case 2:
                return "Temporarily Unavailable";
            case 3:
                return "Out of Service";
            default:
                return "Unknown State '" + i + "'";
        }
    }

    public static b b(c cVar) {
        int i;
        int i2 = 2;
        if (kv == null) {
            kv = l.ht();
        }
        if (cVar == null) {
            cVar = new c();
        }
        String cD = cVar.cD();
        if (cD == null) {
            switch (cVar.cy()) {
                case 0:
                    i = 2;
                    break;
                case 1:
                    i = 1;
                    break;
                case 2:
                    i = 2;
                    break;
                case 3:
                    i = 3;
                    break;
                default:
                    throw new IllegalArgumentException("The power consumption must be one of Critiera.NO_REQUIREMENT, Criteria.POWER_USAGE_HIGH, Criteria.POWER_USAGE_MEDIUM or Criteria.POWER_USAGE_LOW.");
            }
            boolean isAltitudeRequired = cVar.isAltitudeRequired();
            boolean cB = cVar.cB();
            boolean cB2 = cVar.cB();
            boolean cz = cVar.cz();
            if (cVar.getHorizontalAccuracy() < 50) {
                i2 = 1;
            }
            Criteria criteria = new Criteria();
            criteria.setAccuracy(i2);
            criteria.setSpeedRequired(cB2);
            criteria.setAltitudeRequired(isAltitudeRequired);
            criteria.setBearingRequired(cB);
            criteria.setCostAllowed(cz);
            criteria.setPowerRequirement(i);
            cD = kv.getBestProvider(criteria, true);
        }
        System.out.println("getAndroidLocationProvider: Best provider for criteria is '" + cD + "'");
        if (cD != null) {
            return new b(cD);
        }
        if (!kv.getProviders(true).isEmpty()) {
            return null;
        }
        throw new h("No enabled LocationProvider found. Enable an Location Provider and try again.");
    }

    private void cU() {
        kv.requestLocationUpdates(this.kf, 0, 0.0f, this.kw, this.kx.getLooper());
    }

    public void a(i iVar, int i, int i2, int i3) {
        System.out.println("Setting a location listener:" + iVar);
        if (this.kw == null) {
            this.kw = new a(this);
        }
        if (iVar == null) {
            kv.removeUpdates(this.kw);
            return;
        }
        this.kw.a(iVar);
        cU();
    }

    public g aA(int i) {
        Location lastKnownLocation = kv.getLastKnownLocation(this.kf);
        if (lastKnownLocation == null) {
            System.out.println("getLocation():null received from LocationManager.getLastKnownLocation.");
            return null;
        }
        g gVar = new g(lastKnownLocation);
        j.a(gVar);
        System.out.println("getLocation():" + gVar);
        return gVar;
    }

    public String cV() {
        return this.kf;
    }

    public int getState() {
        return this.state;
    }

    public void reset() {
        if (this.kw != null) {
            kv.removeUpdates(this.kw);
        }
    }

    /* access modifiers changed from: package-private */
    public void setState(int i) {
        this.state = i;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("AndroidLocationProvider{provider='");
        stringBuffer.append(this.kf);
        stringBuffer.append("',state='");
        stringBuffer.append(aB(getState()));
        stringBuffer.append("'}");
        return stringBuffer.toString();
    }
}
