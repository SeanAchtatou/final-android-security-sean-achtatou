package com.a.a.h;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import com.a.a.g.g;
import com.a.a.g.i;
import com.a.a.g.j;

public class a implements LocationListener {
    private i kt;
    private b ku;

    public a(b bVar) {
        this.ku = bVar;
    }

    public void a(i iVar) {
        this.kt = iVar;
    }

    public i cT() {
        return this.kt;
    }

    public void onLocationChanged(Location location) {
        System.out.println("onLocationChanged:" + location);
        if (this.kt == null) {
            System.out.println("onLocationChanged:No listener to inform about location change. Doing nothing.");
        } else if (location == null) {
            System.out.println("onLocationChanged:location is null.");
        } else {
            g gVar = new g(location);
            j.a(gVar);
            this.kt.a(this.ku, gVar);
        }
    }

    public void onProviderDisabled(String str) {
        System.out.println("onProviderDisabled:" + str);
        onStatusChanged(str, 0, null);
    }

    public void onProviderEnabled(String str) {
        System.out.println("onProviderEnabled:" + str);
        onStatusChanged(str, 1, null);
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
        String str2;
        int i2;
        switch (i) {
            case 0:
                str2 = "Out of Service";
                i2 = 3;
                break;
            case 1:
                str2 = "Temporarily Unavailable";
                i2 = 2;
                break;
            case 2:
                str2 = "Available";
                i2 = 1;
                break;
            default:
                throw new IllegalArgumentException("LocationProvider.onStatusChanged: Status '" + i + "' not recognized.");
        }
        System.out.println("onStatusChanged:provider:" + str + ".Status:" + str2);
        this.kt.a(this.ku, i2);
        this.ku.setState(i2);
    }
}
