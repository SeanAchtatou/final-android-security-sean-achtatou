package com.a.a.r;

import android.util.AttributeSet;
import org.meteoroid.core.e;
import org.meteoroid.core.f;

public interface c {
    public static final String LOG_TAG = "VirtualDevice";

    public interface a extends e.a, f.b {
        void a(AttributeSet attributeSet, String str);

        void a(c cVar);

        String getName();

        boolean ia();

        boolean isTouchable();

        void setVisible(boolean z);
    }

    void bc(String str);

    int getOrientation();

    boolean isVisible();

    void onCreate();

    void onDestroy();
}
