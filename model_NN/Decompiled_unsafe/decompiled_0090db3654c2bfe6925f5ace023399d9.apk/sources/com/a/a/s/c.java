package com.a.a.s;

import java.util.Vector;

public abstract class c<T> {
    private int tC = 0;
    private T tD;
    private Vector<T> tE = new Vector<>();
    private int tF;
    private int tG;

    public c(int i, int i2) {
        this.tF = i;
        this.tG = i2;
    }

    public T getObject() {
        if (this.tE.size() > this.tF) {
            this.tD = this.tE.firstElement();
        } else if (this.tC <= this.tG) {
            this.tD = jb();
            this.tC++;
        } else {
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
                this.tD = this.tE.firstElement();
            }
        }
        return this.tD;
    }

    public void i(T t) {
        this.tE.addElement(t);
        synchronized (this) {
            notifyAll();
        }
    }

    public abstract T jb();
}
