package com.a.a.s;

import android.util.Log;
import java.util.HashMap;
import me.gall.sgp.sdk.service.BossService;

public class b {
    private HashMap<String, String> tB = new HashMap<>();

    public b(String str) {
        if (str != null && str.length() > 0) {
            String[] split = str.split(BossService.ID_SEPARATOR);
            for (int i = 0; i < split.length; i++) {
                int indexOf = split[i].indexOf(":");
                if (indexOf != -1) {
                    String trim = split[i].substring(0, indexOf).trim();
                    String substring = split[i].substring(indexOf + 1);
                    this.tB.put(trim, substring);
                    Log.d("Configuration", trim + ":" + substring);
                } else {
                    Log.w("Configuration", "Unkown args for configuration." + split[i]);
                }
            }
        }
    }

    public String bm(String str) {
        return this.tB.get(str);
    }
}
