package com.a.a.s;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.util.Log;
import g31fhsgzqxzl.gf.R;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import me.gall.sgp.sdk.service.BossService;
import org.meteoroid.core.l;

public final class e {
    public static final String LOG_TAG = "ResourceUtils";
    private static String tI;
    private static final HashMap<String, Bitmap> tJ = new HashMap<>();

    public static int a(Context context, float f) {
        return (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    private static Uri a(Resources resources, String str, int i) {
        String resourcePackageName = resources.getResourcePackageName(i);
        String resourceTypeName = resources.getResourceTypeName(i);
        String resourceEntryName = resources.getResourceEntryName(i);
        Log.d(LOG_TAG, "resPkg=" + resourcePackageName + ",type=" + resourceTypeName + ",name=" + resourceEntryName);
        return a(str, resourcePackageName, resourceTypeName, resourceEntryName);
    }

    private static Uri a(String str, String str2, String str3, String str4) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("android.resource");
        builder.encodedAuthority(str);
        builder.appendEncodedPath(str3);
        if (!str.equals(str2)) {
            builder.appendEncodedPath(str2 + ":" + str4);
        } else {
            builder.appendEncodedPath(str4);
        }
        return builder.build();
    }

    public static int b(Context context, float f) {
        return (int) ((f / context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static Uri b(Context context, int i) {
        try {
            return a(context.getResources(), context.getPackageName(), i);
        } catch (Resources.NotFoundException e) {
            return null;
        }
    }

    public static void bn(String str) {
        if (str != null) {
            tI = str + File.separator;
        } else {
            tI = null;
        }
    }

    public static final String bo(String str) {
        return "abresource" + File.separator + str;
    }

    private static int bp(String str) {
        try {
            return c(R.drawable.class, str);
        } catch (Exception e) {
            throw new IOException("Fail to get resource id:" + str);
        }
    }

    public static int bq(String str) {
        try {
            return c(R.raw.class, str);
        } catch (Exception e) {
            throw new IOException("Fail to get raw id:" + str);
        }
    }

    public static Rect br(String str) {
        if (str == null) {
            Log.w(LOG_TAG, "Rect string couldn't be null.");
            return null;
        }
        String[] split = str.split(BossService.ID_SEPARATOR);
        if (split.length != 4) {
            throw new IllegalArgumentException("Invalid rect string:" + str);
        }
        int parseInt = Integer.parseInt(split[0].trim());
        int parseInt2 = Integer.parseInt(split[1].trim());
        return new Rect(parseInt, parseInt2, Integer.parseInt(split[2].trim()) + parseInt, Integer.parseInt(split[3].trim()) + parseInt2);
    }

    public static Bitmap bs(String str) {
        if (str == null) {
            Log.e(LOG_TAG, "Bitmap string couldn't be null.");
            return null;
        } else if (str.trim().length() == 0) {
            Log.d(LOG_TAG, "Empty bitmap will be null.");
            return null;
        } else if (tJ.containsKey(str)) {
            Log.d(LOG_TAG, "Wow, " + str + " hit the bitmap cache.");
            return tJ.get(str);
        } else if (jc()) {
            Bitmap d = org.meteoroid.core.e.d(l.aV(tI + str + ".png"));
            tJ.put(str, d);
            return d;
        } else {
            try {
                Bitmap decodeResource = BitmapFactory.decodeResource(l.getActivity().getResources(), bp(str.trim()));
                tJ.put(str, decodeResource);
                return decodeResource;
            } catch (Exception e) {
                throw new IOException("Fail to load bitmap " + str + ":" + e.toString());
            }
        }
    }

    public static Bitmap[] bt(String str) {
        if (str == null) {
            Log.e(LOG_TAG, "Bitmap string couldn't be null.");
            return null;
        }
        String[] split = str.split(BossService.ID_SEPARATOR);
        if (split.length <= 0) {
            return null;
        }
        Bitmap[] bitmapArr = new Bitmap[split.length];
        for (int i = 0; i < split.length; i++) {
            bitmapArr[i] = bs(split[i].trim());
        }
        return bitmapArr;
    }

    public static final String bu(String str) {
        int lastIndexOf = str.lastIndexOf(File.separator);
        return lastIndexOf == -1 ? "" : str.substring(0, lastIndexOf + 1);
    }

    public static final String bv(String str) {
        int lastIndexOf = str.lastIndexOf(File.separator);
        return lastIndexOf == -1 ? str : str.substring(lastIndexOf + 1);
    }

    private static int c(Class<?> cls, String str) {
        return cls.getField(str).getInt(null);
    }

    public static String d(InputStream inputStream, String str) {
        byte[] bArr = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.close();
                return new String(byteArrayOutputStream.toByteArray(), str);
            }
        }
    }

    private static boolean jc() {
        return tI != null;
    }

    public static void jd() {
        tJ.clear();
    }

    public static String w(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            byte b2 = (b >>> 4) & 15;
            int i = 0;
            while (true) {
                if (b2 < 0 || b2 > 9) {
                    stringBuffer.append((char) ((b2 - 10) + 97));
                } else {
                    stringBuffer.append((char) (b2 + 48));
                }
                byte b3 = b & 15;
                int i2 = i + 1;
                if (i >= 1) {
                    break;
                }
                i = i2;
                b2 = b3;
            }
        }
        return stringBuffer.toString();
    }
}
