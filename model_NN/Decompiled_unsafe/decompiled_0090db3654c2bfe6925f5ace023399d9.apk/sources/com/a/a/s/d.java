package com.a.a.s;

import android.text.format.Time;
import android.util.Log;
import android.webkit.WebView;
import org.meteoroid.core.l;

public final class d {
    public static final void a(WebView webView) {
        webView.clearFocus();
    }

    public static final void a(final WebView webView, long j) {
        l.getHandler().postDelayed(new Runnable() {
            public void run() {
                if (webView != null && webView.isShown()) {
                    webView.requestFocus(130);
                }
                l.getActivity().getWindow().setSoftInputMode(18);
            }
        }, j);
    }

    public static final boolean w(int i, int i2, int i3) {
        Time time = new Time("GMT+8");
        time.setToNow();
        Log.d("isDateExpired?", "Current date is " + time.year + "-" + (time.month + 1) + "-" + time.monthDay + " and target time is " + i + "-" + i2 + "-" + i3);
        if (i > time.year || ((i == time.year && i2 > time.month + 1) || (i == time.year && i2 == time.month + 1 && i3 > time.monthDay))) {
            Log.d("isDateExpired?", "Not reach the date.");
            return false;
        }
        Log.d("isDateExpired?", "Yeah, the date is expired.");
        return true;
    }
}
