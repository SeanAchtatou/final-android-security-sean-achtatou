package com.a.a.g;

import com.a.a.h.b;

public abstract class j {
    public static final int AVAILABLE = 1;
    public static final int OUT_OF_SERVICE = 3;
    public static final int TEMPORARILY_UNAVAILABLE = 2;
    private static g km;

    protected j() {
    }

    public static j a(c cVar) {
        return b.b(cVar);
    }

    public static void a(g gVar) {
        km = gVar;
    }

    public static void a(l lVar) {
    }

    public static void a(l lVar, b bVar, float f) {
    }

    public static g cL() {
        if (km != null) {
            System.out.println("getLastKnownLocation:lastKnownLocation:" + km);
            return km;
        }
        System.out.println("getLastKnownLocation:no last known location. Neither getLocation() nor a LocationListener was ever called.");
        return null;
    }

    public abstract void a(i iVar, int i, int i2, int i3);

    public abstract g aA(int i);

    public abstract int getState();

    public abstract void reset();
}
