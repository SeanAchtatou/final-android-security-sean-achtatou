package com.a.a.g;

import android.location.Location;

public class g {
    public static final int MTA_ASSISTED = 262144;
    public static final int MTA_UNASSISTED = 524288;
    public static final int MTE_ANGLEOFARRIVAL = 32;
    public static final int MTE_CELLID = 8;
    public static final int MTE_SATELLITE = 1;
    public static final int MTE_SHORTRANGE = 16;
    public static final int MTE_TIMEDIFFERENCE = 2;
    public static final int MTE_TIMEOFARRIVAL = 4;
    public static final int MTY_NETWORKBASED = 131072;
    public static final int MTY_TERMINALBASED = 65536;
    private m kg;
    private float kk;
    private float kl;
    private long timestamp;

    public g(Location location) {
        float accuracy = location.getAccuracy();
        this.kk = location.getSpeed();
        this.kl = location.getBearing();
        this.timestamp = location.getTime();
        this.kg = new m(location.getLatitude(), location.getLongitude(), (float) location.getAltitude(), accuracy, accuracy);
    }

    public String L(String str) {
        return null;
    }

    public m cE() {
        return this.kg;
    }

    public a cF() {
        return null;
    }

    public float cJ() {
        return this.kl;
    }

    public int cK() {
        return 0;
    }

    public float getSpeed() {
        return this.kk;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public boolean isValid() {
        return true;
    }

    public String toString() {
        return this.kg.toString() + ".Speed:" + this.kk + ".Time:" + this.timestamp + ".Course:" + this.kl + ".";
    }
}
