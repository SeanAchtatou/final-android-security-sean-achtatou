package com.a.a.g;

public class m extends b {
    private float kr;
    private float ks;

    public m(double d, double d2, float f, float f2, float f3) {
        super(d, d2, f);
        this.kr = f2;
        this.ks = f3;
    }

    public float cR() {
        return this.kr;
    }

    public float cS() {
        return this.ks;
    }

    public void f(float f) {
        this.kr = f;
    }

    public void g(float f) {
        this.ks = f;
    }

    public String toString() {
        return "Lat:" + this.jU + ".Log:" + this.jV + ".Alt:" + this.jW + ".HAcc:" + this.kr + ".VAcc:" + this.ks + ".";
    }
}
