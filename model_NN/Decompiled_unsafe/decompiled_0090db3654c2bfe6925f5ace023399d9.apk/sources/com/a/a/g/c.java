package com.a.a.g;

public class c {
    public static final int NO_REQUIREMENT = 0;
    public static final int POWER_USAGE_HIGH = 3;
    public static final int POWER_USAGE_LOW = 1;
    public static final int POWER_USAGE_MEDIUM = 2;
    private int jX = 2;
    private int jY = 1;
    private int jZ = 1;
    private int ka;
    private boolean kb = true;
    private boolean kc = true;
    private boolean kd;
    private boolean ke;
    private String kf;

    public void G(String str) {
        this.kf = str;
    }

    public void ay(int i) {
        this.ka = i;
    }

    public void az(int i) {
        this.jX = i;
    }

    public int cA() {
        return this.ka;
    }

    public boolean cB() {
        return this.kc;
    }

    public boolean cC() {
        return this.ke;
    }

    public String cD() {
        return this.kf;
    }

    public int cy() {
        return this.jX;
    }

    public boolean cz() {
        return this.kb;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        if (this.ke != cVar.ke) {
            return false;
        }
        if (this.kd != cVar.kd) {
            return false;
        }
        if (this.kb != cVar.kb) {
            return false;
        }
        if (this.jZ != cVar.jZ) {
            return false;
        }
        if (this.jX != cVar.jX) {
            return false;
        }
        if (this.ka != cVar.ka) {
            return false;
        }
        if (this.kc != cVar.kc) {
            return false;
        }
        return this.jY == cVar.jY;
    }

    public int getHorizontalAccuracy() {
        return this.jZ;
    }

    public int getVerticalAccuracy() {
        return this.jY;
    }

    public int hashCode() {
        int i = 1231;
        int i2 = ((((((((this.kb ? 1231 : 1237) + (((this.kd ? 1231 : 1237) + (((this.ke ? 1231 : 1237) + 31) * 31)) * 31)) * 31) + this.jZ) * 31) + this.jX) * 31) + this.ka) * 31;
        if (!this.kc) {
            i = 1237;
        }
        return ((i2 + i) * 31) + this.jY;
    }

    public boolean isAltitudeRequired() {
        return this.kd;
    }

    public void k(boolean z) {
        this.kc = z;
    }

    public void l(boolean z) {
        this.ke = z;
    }

    public void setAltitudeRequired(boolean z) {
        this.kd = z;
    }

    public void setCostAllowed(boolean z) {
        this.kb = z;
    }

    public void setHorizontalAccuracy(int i) {
        this.jZ = i;
    }

    public void setVerticalAccuracy(int i) {
        this.jY = i;
    }
}
