package com.a.a.f;

import com.a.a.e.o;
import com.a.a.e.p;

public class d extends b {
    public static final int TRANS_MIRROR = 2;
    public static final int TRANS_MIRROR_ROT180 = 1;
    public static final int TRANS_MIRROR_ROT270 = 4;
    public static final int TRANS_MIRROR_ROT90 = 7;
    public static final int TRANS_NONE = 0;
    public static final int TRANS_ROT180 = 3;
    public static final int TRANS_ROT270 = 6;
    public static final int TRANS_ROT90 = 5;
    private int jA;
    private int[] jB;
    private int jC;
    private int jD;
    private int jE;
    private int jF;
    private int jG;
    private p jH;
    private int jI;
    private int jJ;
    private int jK;
    private int jL;
    private int[] jM;
    private int[] jN;

    public d(p pVar) {
        this(pVar, pVar.getWidth(), pVar.getHeight());
    }

    public d(p pVar, int i, int i2) {
        super(0, 0, i, i2, true);
        if (pVar.getWidth() % i == 0 && pVar.getHeight() % i2 == 0) {
            this.jH = pVar;
            this.jE = pVar.getWidth() / i;
            this.jF = pVar.getHeight() / i2;
            this.jJ = 0;
            this.jI = 0;
            this.jK = i;
            this.jL = i2;
            return;
        }
        throw new IllegalArgumentException();
    }

    public d(d dVar) {
        super(dVar.getX(), dVar.getY(), dVar.getWidth(), dVar.getHeight(), dVar.isVisible());
        this.jA = dVar.jA;
        this.jB = dVar.jB;
        this.jC = dVar.jC;
        this.jD = dVar.jD;
        this.jE = dVar.jE;
        this.jF = dVar.jF;
        this.jG = dVar.jG;
        this.jH = dVar.jH;
        this.jI = dVar.jI;
        this.jJ = dVar.jJ;
        this.jK = dVar.jK;
        this.jL = dVar.jL;
    }

    private synchronized boolean a(Object obj, int i, int i2) {
        boolean z;
        int i3;
        int i4;
        int x;
        int width;
        int i5;
        boolean z2;
        d dVar;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10 = 0;
        int i11 = 0;
        int i12 = 0;
        int i13 = 0;
        int i14 = 0;
        int i15 = 0;
        boolean z3 = true;
        d dVar2 = this;
        int i16 = i2;
        int i17 = i;
        while (true) {
            if (z3) {
                int i18 = dVar2.jI;
                int i19 = dVar2.jJ;
                int i20 = dVar2.jK;
                int i21 = dVar2.jL;
                if (i20 == 0 || i21 == 0) {
                    z = false;
                } else {
                    switch (dVar2.jG) {
                        case 0:
                            x = dVar2.getX() + i18;
                            width = i19 + dVar2.getY();
                            i5 = i20;
                            break;
                        case 1:
                            x = dVar2.getX() + i18;
                            width = (dVar2.getY() + ((dVar2.getHeight() - i19) - 1)) - i21;
                            i5 = i20;
                            break;
                        case 2:
                            x = (((dVar2.getWidth() - i18) - 1) + dVar2.getX()) - i20;
                            width = i19 + dVar2.getY();
                            i5 = i20;
                            break;
                        case 3:
                            x = (((dVar2.getWidth() - i18) - 1) + dVar2.getX()) - i20;
                            width = (dVar2.getY() + ((dVar2.getHeight() - i19) - 1)) - i21;
                            i5 = i20;
                            break;
                        case 4:
                            x = dVar2.getX() + i19;
                            width = dVar2.getY() + i18;
                            i5 = i21;
                            i21 = i20;
                            break;
                        case 5:
                            x = (((dVar2.getHeight() - i19) - 1) + dVar2.getX()) - i21;
                            width = dVar2.getY() + i18;
                            i5 = i21;
                            i21 = i20;
                            break;
                        case 6:
                            x = dVar2.getX() + i19;
                            width = (((dVar2.getWidth() - i18) - 1) + dVar2.getY()) - i20;
                            i5 = i21;
                            i21 = i20;
                            break;
                        case 7:
                            x = (((dVar2.getHeight() - i19) - 1) + dVar2.getX()) - i21;
                            width = (((dVar2.getWidth() - i18) - 1) + dVar2.getY()) - i20;
                            i5 = i21;
                            i21 = i20;
                            break;
                        default:
                            z = false;
                            break;
                    }
                    if (obj == dVar2) {
                        z2 = false;
                        dVar = dVar2;
                        i6 = i5;
                        i7 = i11;
                        i5 = i12;
                        i8 = i21;
                        i21 = i13;
                        i9 = i10;
                    } else if (obj instanceof d) {
                        i8 = i15;
                        i9 = x;
                        i6 = i14;
                        x = i17;
                        i7 = width;
                        width = i16;
                        z2 = z3;
                        dVar = obj;
                    } else if (obj instanceof e) {
                        e eVar = obj;
                        int x2 = eVar.getX();
                        int y = eVar.getY();
                        i6 = eVar.getWidth();
                        dVar = dVar2;
                        i8 = eVar.getHeight();
                        z2 = false;
                        int i22 = width;
                        width = y;
                        i7 = i22;
                        int i23 = x;
                        x = x2;
                        i9 = i23;
                    } else {
                        p pVar = obj;
                        i6 = pVar.getWidth();
                        i7 = width;
                        i9 = x;
                        width = i16;
                        x = i17;
                        d dVar3 = dVar2;
                        i8 = pVar.getHeight();
                        z2 = false;
                        dVar = dVar3;
                    }
                    i12 = i5;
                    i11 = i7;
                    i10 = i9;
                    i16 = width;
                    i17 = x;
                    i14 = i6;
                    i13 = i21;
                    i15 = i8;
                    dVar2 = dVar;
                    z3 = z2;
                }
            } else if (i10 > i17 && i10 >= i17 + i14) {
                z = false;
            } else if (i10 < i17 && i10 + i12 <= i17) {
                z = false;
            } else if (i11 > i16 && i11 >= i16 + i15) {
                z = false;
            } else if (i11 < i16 && i11 + i13 <= i16) {
                z = false;
            } else if (obj instanceof e) {
                e eVar2 = (e) obj;
                if (i17 > i10) {
                    i3 = (i17 + i14 < i10 + i12 ? i17 + i14 : i10 + i12) - i17;
                    i10 = i17;
                } else {
                    i3 = (i10 + i12 < i17 + i14 ? i10 + i12 : i17 + i14) - i10;
                }
                if (i16 > i11) {
                    i4 = (i16 + i15 < i11 + i13 ? i16 + i15 : i11 + i13) - i16;
                    i11 = i16;
                } else {
                    i4 = (i11 + i13 < i16 + i15 ? i11 + i13 : i16 + i15) - i11;
                }
                int cv = eVar2.cv();
                int cw = eVar2.cw();
                int i24 = (i10 - i17) / cv;
                int i25 = (i11 - i16) / cw;
                int i26 = ((i3 + (i10 - i17)) - 1) / cv;
                int i27 = ((i4 + (i11 - i16)) - 1) / cw;
                int i28 = i25;
                while (true) {
                    if (i28 <= i27) {
                        int i29 = i24;
                        while (i29 <= i26) {
                            int y2 = eVar2.y(i29, i28);
                            if (y2 < 0) {
                                y2 = eVar2.aw(y2);
                            }
                            if (y2 != 0) {
                                z = true;
                            } else {
                                i29++;
                            }
                        }
                        i28++;
                    } else {
                        z = false;
                    }
                }
            } else {
                z = true;
            }
        }
        return z;
    }

    /* JADX WARN: Type inference failed for: r7v39 */
    /* JADX WARN: Type inference failed for: r7v41 */
    /* JADX WARN: Type inference failed for: r7v42 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean b(java.lang.Object r42, int r43, int r44) {
        /*
            r41 = this;
            monitor-enter(r41)
            r13 = 1
            r11 = 0
            r9 = 0
            r7 = 0
            r2 = 0
            r5 = 0
            r3 = 0
            r12 = r41
            r18 = r44
            r19 = r43
        L_0x000e:
            if (r13 == 0) goto L_0x01a5
            int r4 = r12.jI     // Catch:{ all -> 0x05a0 }
            int r6 = r12.getWidth()     // Catch:{ all -> 0x05a0 }
            if (r4 >= r6) goto L_0x002e
            int r4 = r12.jI     // Catch:{ all -> 0x05a0 }
            int r6 = r12.jK     // Catch:{ all -> 0x05a0 }
            int r4 = r4 + r6
            if (r4 <= 0) goto L_0x002e
            int r4 = r12.jJ     // Catch:{ all -> 0x05a0 }
            int r6 = r12.getHeight()     // Catch:{ all -> 0x05a0 }
            if (r4 >= r6) goto L_0x002e
            int r4 = r12.jJ     // Catch:{ all -> 0x05a0 }
            int r6 = r12.jL     // Catch:{ all -> 0x05a0 }
            int r4 = r4 + r6
            if (r4 > 0) goto L_0x0031
        L_0x002e:
            r2 = 0
        L_0x002f:
            monitor-exit(r41)
            return r2
        L_0x0031:
            int r4 = r12.jI     // Catch:{ all -> 0x05a0 }
            if (r4 < 0) goto L_0x0069
            int r4 = r12.jI     // Catch:{ all -> 0x05a0 }
            r14 = r4
        L_0x0038:
            int r4 = r12.jJ     // Catch:{ all -> 0x05a0 }
            if (r4 < 0) goto L_0x006c
            int r4 = r12.jJ     // Catch:{ all -> 0x05a0 }
            r8 = r4
        L_0x003f:
            int r4 = r12.jI     // Catch:{ all -> 0x05a0 }
            int r6 = r12.jK     // Catch:{ all -> 0x05a0 }
            int r4 = r4 + r6
            int r6 = r12.getWidth()     // Catch:{ all -> 0x05a0 }
            if (r4 >= r6) goto L_0x006f
            int r4 = r12.jI     // Catch:{ all -> 0x05a0 }
            int r6 = r12.jK     // Catch:{ all -> 0x05a0 }
            int r4 = r4 + r6
            int r6 = r4 - r14
        L_0x0051:
            int r4 = r12.jJ     // Catch:{ all -> 0x05a0 }
            int r10 = r12.jL     // Catch:{ all -> 0x05a0 }
            int r4 = r4 + r10
            int r10 = r12.getHeight()     // Catch:{ all -> 0x05a0 }
            if (r4 >= r10) goto L_0x0076
            int r4 = r12.jJ     // Catch:{ all -> 0x05a0 }
            int r10 = r12.jL     // Catch:{ all -> 0x05a0 }
            int r4 = r4 + r10
            int r4 = r4 - r8
        L_0x0062:
            int r10 = r12.jG     // Catch:{ all -> 0x05a0 }
            switch(r10) {
                case 0: goto L_0x007c;
                case 1: goto L_0x00b0;
                case 2: goto L_0x00c4;
                case 3: goto L_0x00d8;
                case 4: goto L_0x00f5;
                case 5: goto L_0x0105;
                case 6: goto L_0x013e;
                case 7: goto L_0x011e;
                default: goto L_0x0067;
            }     // Catch:{ all -> 0x05a0 }
        L_0x0067:
            r2 = 0
            goto L_0x002f
        L_0x0069:
            r4 = 0
            r14 = r4
            goto L_0x0038
        L_0x006c:
            r4 = 0
            r8 = r4
            goto L_0x003f
        L_0x006f:
            int r4 = r12.getWidth()     // Catch:{ all -> 0x05a0 }
            int r6 = r4 - r14
            goto L_0x0051
        L_0x0076:
            int r4 = r12.getHeight()     // Catch:{ all -> 0x05a0 }
            int r4 = r4 - r8
            goto L_0x0062
        L_0x007c:
            int r10 = r12.getX()     // Catch:{ all -> 0x05a0 }
            int r10 = r10 + r14
            int r14 = r12.getY()     // Catch:{ all -> 0x05a0 }
            int r8 = r8 + r14
        L_0x0086:
            r0 = r42
            if (r0 == r12) goto L_0x0195
            r0 = r42
            boolean r2 = r0 instanceof com.a.a.f.d     // Catch:{ all -> 0x05a0 }
            if (r2 == 0) goto L_0x0156
            r0 = r42
            com.a.a.f.d r0 = (com.a.a.f.d) r0     // Catch:{ all -> 0x05a0 }
            r2 = r0
            r7 = r10
            r9 = r13
            r11 = r19
            r10 = r18
            r40 = r2
            r2 = r3
            r3 = r5
            r5 = r8
            r8 = r40
        L_0x00a2:
            r12 = r8
            r13 = r9
            r18 = r10
            r19 = r11
            r9 = r5
            r11 = r7
            r5 = r3
            r7 = r6
            r3 = r2
            r2 = r4
            goto L_0x000e
        L_0x00b0:
            int r10 = r12.getX()     // Catch:{ all -> 0x05a0 }
            int r10 = r10 + r14
            int r14 = r12.getY()     // Catch:{ all -> 0x05a0 }
            int r15 = r12.getHeight()     // Catch:{ all -> 0x05a0 }
            int r8 = r15 - r8
            int r8 = r8 + -1
            int r8 = r8 + r14
            int r8 = r8 - r4
            goto L_0x0086
        L_0x00c4:
            int r10 = r12.getX()     // Catch:{ all -> 0x05a0 }
            int r15 = r12.getWidth()     // Catch:{ all -> 0x05a0 }
            int r14 = r15 - r14
            int r14 = r14 + -1
            int r10 = r10 + r14
            int r10 = r10 - r6
            int r14 = r12.getY()     // Catch:{ all -> 0x05a0 }
            int r8 = r8 + r14
            goto L_0x0086
        L_0x00d8:
            int r10 = r12.getX()     // Catch:{ all -> 0x05a0 }
            int r15 = r12.getWidth()     // Catch:{ all -> 0x05a0 }
            int r14 = r15 - r14
            int r14 = r14 + -1
            int r10 = r10 + r14
            int r10 = r10 - r6
            int r14 = r12.getY()     // Catch:{ all -> 0x05a0 }
            int r15 = r12.getHeight()     // Catch:{ all -> 0x05a0 }
            int r8 = r15 - r8
            int r8 = r8 + -1
            int r8 = r8 + r14
            int r8 = r8 - r4
            goto L_0x0086
        L_0x00f5:
            int r10 = r12.getX()     // Catch:{ all -> 0x05a0 }
            int r10 = r10 + r8
            int r8 = r12.getY()     // Catch:{ all -> 0x05a0 }
            int r8 = r8 + r14
            r40 = r6
            r6 = r4
            r4 = r40
            goto L_0x0086
        L_0x0105:
            int r10 = r12.getX()     // Catch:{ all -> 0x05a0 }
            int r15 = r12.getHeight()     // Catch:{ all -> 0x05a0 }
            int r8 = r15 - r8
            int r8 = r8 + r10
            int r10 = r8 - r4
            int r8 = r12.getY()     // Catch:{ all -> 0x05a0 }
            int r8 = r8 + r14
            r40 = r6
            r6 = r4
            r4 = r40
            goto L_0x0086
        L_0x011e:
            int r10 = r12.getX()     // Catch:{ all -> 0x05a0 }
            int r15 = r12.getHeight()     // Catch:{ all -> 0x05a0 }
            int r8 = r15 - r8
            int r8 = r8 + r10
            int r10 = r8 - r4
            int r8 = r12.getY()     // Catch:{ all -> 0x05a0 }
            int r15 = r12.getWidth()     // Catch:{ all -> 0x05a0 }
            int r14 = r15 - r14
            int r8 = r8 + r14
            int r8 = r8 - r6
            r40 = r6
            r6 = r4
            r4 = r40
            goto L_0x0086
        L_0x013e:
            int r10 = r12.getX()     // Catch:{ all -> 0x05a0 }
            int r10 = r10 + r8
            int r8 = r12.getY()     // Catch:{ all -> 0x05a0 }
            int r15 = r12.getWidth()     // Catch:{ all -> 0x05a0 }
            int r14 = r15 - r14
            int r8 = r8 + r14
            int r8 = r8 - r6
            r40 = r6
            r6 = r4
            r4 = r40
            goto L_0x0086
        L_0x0156:
            r0 = r42
            boolean r2 = r0 instanceof com.a.a.f.e     // Catch:{ all -> 0x05a0 }
            if (r2 == 0) goto L_0x017d
            r5 = 0
            r0 = r42
            com.a.a.f.e r0 = (com.a.a.f.e) r0     // Catch:{ all -> 0x05a0 }
            r2 = r0
            int r9 = r2.getX()     // Catch:{ all -> 0x05a0 }
            int r7 = r2.getY()     // Catch:{ all -> 0x05a0 }
            int r3 = r2.getWidth()     // Catch:{ all -> 0x05a0 }
            int r2 = r2.getHeight()     // Catch:{ all -> 0x05a0 }
            r11 = r9
            r9 = r5
            r5 = r8
            r8 = r12
            r40 = r7
            r7 = r10
            r10 = r40
            goto L_0x00a2
        L_0x017d:
            r5 = 0
            r0 = r42
            com.a.a.e.p r0 = (com.a.a.e.p) r0     // Catch:{ all -> 0x05a0 }
            r2 = r0
            int r3 = r2.getWidth()     // Catch:{ all -> 0x05a0 }
            int r2 = r2.getHeight()     // Catch:{ all -> 0x05a0 }
            r7 = r10
            r9 = r5
            r11 = r19
            r5 = r8
            r10 = r18
            r8 = r12
            goto L_0x00a2
        L_0x0195:
            r3 = 0
            r5 = r9
            r9 = r3
            r3 = r6
            r6 = r7
            r7 = r11
            r11 = r10
            r10 = r8
            r8 = r12
            r40 = r2
            r2 = r4
            r4 = r40
            goto L_0x00a2
        L_0x01a5:
            r0 = r19
            if (r11 <= r0) goto L_0x01b0
            int r4 = r19 + r5
            if (r11 < r4) goto L_0x01b0
            r2 = 0
            goto L_0x002f
        L_0x01b0:
            r0 = r19
            if (r11 >= r0) goto L_0x01bd
            int r4 = r11 + r7
            r0 = r19
            if (r4 > r0) goto L_0x01bd
            r2 = 0
            goto L_0x002f
        L_0x01bd:
            r0 = r18
            if (r9 <= r0) goto L_0x01c8
            int r4 = r18 + r3
            if (r9 < r4) goto L_0x01c8
            r2 = 0
            goto L_0x002f
        L_0x01c8:
            r0 = r18
            if (r9 >= r0) goto L_0x01d5
            int r4 = r9 + r2
            r0 = r18
            if (r4 > r0) goto L_0x01d5
            r2 = 0
            goto L_0x002f
        L_0x01d5:
            r0 = r19
            if (r0 <= r11) goto L_0x024d
            int r4 = r19 + r5
            int r6 = r11 + r7
            if (r4 >= r6) goto L_0x024a
            int r4 = r19 + r5
        L_0x01e1:
            int r5 = r4 - r19
            r34 = r19
        L_0x01e5:
            r0 = r18
            if (r0 <= r9) goto L_0x025f
            int r4 = r18 + r3
            int r6 = r9 + r2
            if (r4 >= r6) goto L_0x025d
            int r2 = r18 + r3
        L_0x01f1:
            int r9 = r2 - r18
            r33 = r18
        L_0x01f5:
            r28 = 0
            r27 = 0
            r17 = 0
            r16 = 0
            r15 = 0
            r14 = 0
            r0 = r41
            int[] r2 = r0.jB     // Catch:{ all -> 0x05a0 }
            if (r2 != 0) goto L_0x026e
            r0 = r41
            int r2 = r0.jA     // Catch:{ all -> 0x05a0 }
        L_0x0209:
            int r23 = r41.getWidth()     // Catch:{ all -> 0x05a0 }
            int r22 = r41.getHeight()     // Catch:{ all -> 0x05a0 }
            r0 = r41
            int r3 = r0.jE     // Catch:{ all -> 0x05a0 }
            int r3 = r2 % r3
            int r21 = r23 * r3
            r0 = r41
            int r3 = r0.jE     // Catch:{ all -> 0x05a0 }
            int r2 = r2 / r3
            int r20 = r22 * r2
            r0 = r41
            int[] r2 = r0.jM     // Catch:{ all -> 0x05a0 }
            if (r2 != 0) goto L_0x0236
            int r2 = r23 * r22
            int[] r2 = new int[r2]     // Catch:{ all -> 0x05a0 }
            r0 = r41
            r0.jM = r2     // Catch:{ all -> 0x05a0 }
            int r2 = r23 * r22
            int[] r2 = new int[r2]     // Catch:{ all -> 0x05a0 }
            r0 = r41
            r0.jN = r2     // Catch:{ all -> 0x05a0 }
        L_0x0236:
            r30 = 1
            r0 = r41
            int[] r3 = r0.jM     // Catch:{ all -> 0x05a0 }
            r29 = r41
        L_0x023e:
            if (r30 == 0) goto L_0x0570
            r0 = r29
            int r2 = r0.jG     // Catch:{ all -> 0x05a0 }
            switch(r2) {
                case 0: goto L_0x0279;
                case 1: goto L_0x0333;
                case 2: goto L_0x030f;
                case 3: goto L_0x02e5;
                case 4: goto L_0x03b5;
                case 5: goto L_0x035c;
                case 6: goto L_0x03db;
                case 7: goto L_0x0387;
                default: goto L_0x0247;
            }     // Catch:{ all -> 0x05a0 }
        L_0x0247:
            r2 = 0
            goto L_0x002f
        L_0x024a:
            int r4 = r11 + r7
            goto L_0x01e1
        L_0x024d:
            int r4 = r11 + r7
            int r6 = r19 + r5
            if (r4 >= r6) goto L_0x025a
            int r4 = r11 + r7
        L_0x0255:
            int r5 = r4 - r11
            r34 = r11
            goto L_0x01e5
        L_0x025a:
            int r4 = r19 + r5
            goto L_0x0255
        L_0x025d:
            int r2 = r2 + r9
            goto L_0x01f1
        L_0x025f:
            int r4 = r9 + r2
            int r6 = r18 + r3
            if (r4 >= r6) goto L_0x026b
            int r2 = r2 + r9
        L_0x0266:
            int r2 = r2 - r9
            r33 = r9
            r9 = r2
            goto L_0x01f5
        L_0x026b:
            int r2 = r18 + r3
            goto L_0x0266
        L_0x026e:
            r0 = r41
            int[] r2 = r0.jB     // Catch:{ all -> 0x05a0 }
            r0 = r41
            int r3 = r0.jA     // Catch:{ all -> 0x05a0 }
            r2 = r2[r3]     // Catch:{ all -> 0x05a0 }
            goto L_0x0209
        L_0x0279:
            r0 = r29
            com.a.a.e.p r2 = r0.jH     // Catch:{ all -> 0x05a0 }
            r4 = 0
            int r6 = r21 + r34
            int r7 = r29.getX()     // Catch:{ all -> 0x05a0 }
            int r6 = r6 - r7
            int r7 = r20 + r33
            int r8 = r29.getY()     // Catch:{ all -> 0x05a0 }
            int r7 = r7 - r8
            r8 = r5
            r2.a(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x05a0 }
            r24 = 0
            r26 = 1
            r25 = 0
        L_0x0296:
            r0 = r42
            r1 = r29
            if (r0 == r1) goto L_0x0556
            r0 = r42
            boolean r2 = r0 instanceof com.a.a.f.d     // Catch:{ all -> 0x05a0 }
            if (r2 == 0) goto L_0x040c
            r0 = r42
            com.a.a.f.d r0 = (com.a.a.f.d) r0     // Catch:{ all -> 0x05a0 }
            r2 = r0
            r0 = r41
            int[] r3 = r0.jN     // Catch:{ all -> 0x05a0 }
            int[] r4 = r2.jB     // Catch:{ all -> 0x05a0 }
            if (r4 != 0) goto L_0x0404
            int r4 = r2.jA     // Catch:{ all -> 0x05a0 }
        L_0x02b1:
            int r8 = r2.getWidth()     // Catch:{ all -> 0x05a0 }
            int r7 = r2.getHeight()     // Catch:{ all -> 0x05a0 }
            int r6 = r2.jE     // Catch:{ all -> 0x05a0 }
            int r6 = r4 % r6
            int r6 = r6 * r8
            int r10 = r2.jE     // Catch:{ all -> 0x05a0 }
            int r4 = r4 / r10
            int r4 = r4 * r7
            r10 = r15
            r11 = r16
            r12 = r2
            r13 = r30
            r2 = r4
            r4 = r6
            r6 = r7
            r7 = r8
            r8 = r14
        L_0x02cd:
            r20 = r2
            r21 = r4
            r22 = r6
            r23 = r7
            r14 = r8
            r15 = r10
            r16 = r11
            r17 = r24
            r27 = r25
            r28 = r26
            r29 = r12
            r30 = r13
            goto L_0x023e
        L_0x02e5:
            r0 = r29
            com.a.a.e.p r2 = r0.jH     // Catch:{ all -> 0x05a0 }
            r4 = 0
            int r6 = r21 + r23
            int r7 = r29.getX()     // Catch:{ all -> 0x05a0 }
            int r7 = r34 - r7
            int r6 = r6 - r7
            int r6 = r6 - r5
            int r6 = r6 + -1
            int r7 = r20 + r22
            int r8 = r29.getY()     // Catch:{ all -> 0x05a0 }
            int r8 = r33 - r8
            int r7 = r7 - r8
            int r7 = r7 - r9
            int r7 = r7 + -1
            r8 = r5
            r2.a(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x05a0 }
            int r2 = r9 * r5
            int r24 = r2 + -1
            r26 = -1
            r25 = 0
            goto L_0x0296
        L_0x030f:
            r0 = r29
            com.a.a.e.p r2 = r0.jH     // Catch:{ all -> 0x05a0 }
            r4 = 0
            int r6 = r21 + r23
            int r7 = r29.getX()     // Catch:{ all -> 0x05a0 }
            int r7 = r34 - r7
            int r6 = r6 - r7
            int r6 = r6 - r5
            int r6 = r6 + -1
            int r7 = r20 + r33
            int r8 = r29.getY()     // Catch:{ all -> 0x05a0 }
            int r7 = r7 - r8
            r8 = r5
            r2.a(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x05a0 }
            int r24 = r5 + -1
            r26 = -1
            int r25 = r5 << 1
            goto L_0x0296
        L_0x0333:
            r0 = r29
            com.a.a.e.p r2 = r0.jH     // Catch:{ all -> 0x05a0 }
            r4 = 0
            int r6 = r21 + r34
            int r7 = r29.getX()     // Catch:{ all -> 0x05a0 }
            int r6 = r6 - r7
            int r7 = r20 + r22
            int r8 = r29.getY()     // Catch:{ all -> 0x05a0 }
            int r8 = r33 - r8
            int r7 = r7 - r8
            int r7 = r7 - r9
            int r7 = r7 + -1
            r8 = r5
            r2.a(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x05a0 }
            int r2 = r9 + -1
            int r24 = r2 * r5
            r26 = 1
            int r2 = r5 << 1
            int r0 = -r2
            r25 = r0
            goto L_0x0296
        L_0x035c:
            r0 = r29
            com.a.a.e.p r6 = r0.jH     // Catch:{ all -> 0x05a0 }
            r8 = 0
            int r2 = r21 + r33
            int r4 = r29.getY()     // Catch:{ all -> 0x05a0 }
            int r10 = r2 - r4
            int r2 = r20 + r22
            int r4 = r29.getX()     // Catch:{ all -> 0x05a0 }
            int r4 = r34 - r4
            int r2 = r2 - r4
            int r11 = r2 - r5
            r7 = r3
            r12 = r9
            r13 = r5
            r6.a(r7, r8, r9, r10, r11, r12, r13)     // Catch:{ all -> 0x05a0 }
            int r2 = r5 + -1
            int r24 = r2 * r9
            int r0 = -r9
            r26 = r0
            int r2 = r9 * r5
            int r25 = r2 + 1
            goto L_0x0296
        L_0x0387:
            r0 = r29
            com.a.a.e.p r6 = r0.jH     // Catch:{ all -> 0x05a0 }
            r8 = 0
            int r2 = r21 + r23
            int r4 = r29.getY()     // Catch:{ all -> 0x05a0 }
            int r4 = r33 - r4
            int r2 = r2 - r4
            int r10 = r2 - r9
            int r2 = r20 + r22
            int r4 = r29.getX()     // Catch:{ all -> 0x05a0 }
            int r4 = r34 - r4
            int r2 = r2 - r4
            int r11 = r2 - r5
            r7 = r3
            r12 = r9
            r13 = r5
            r6.a(r7, r8, r9, r10, r11, r12, r13)     // Catch:{ all -> 0x05a0 }
            int r2 = r9 * r5
            int r24 = r2 + -1
            int r0 = -r9
            r26 = r0
            int r2 = r9 * r5
            int r25 = r2 + -1
            goto L_0x0296
        L_0x03b5:
            r0 = r29
            com.a.a.e.p r6 = r0.jH     // Catch:{ all -> 0x05a0 }
            r8 = 0
            int r2 = r21 + r33
            int r4 = r29.getY()     // Catch:{ all -> 0x05a0 }
            int r10 = r2 - r4
            int r2 = r20 + r34
            int r4 = r29.getX()     // Catch:{ all -> 0x05a0 }
            int r11 = r2 - r4
            r7 = r3
            r12 = r9
            r13 = r5
            r6.a(r7, r8, r9, r10, r11, r12, r13)     // Catch:{ all -> 0x05a0 }
            r24 = 0
            int r2 = r9 * r5
            int r2 = -r2
            int r25 = r2 + 1
            r26 = r9
            goto L_0x0296
        L_0x03db:
            r0 = r29
            com.a.a.e.p r6 = r0.jH     // Catch:{ all -> 0x05a0 }
            r8 = 0
            int r2 = r21 + r23
            int r4 = r29.getY()     // Catch:{ all -> 0x05a0 }
            int r4 = r33 - r4
            int r2 = r2 - r4
            int r10 = r2 - r9
            int r2 = r20 + r34
            int r4 = r29.getX()     // Catch:{ all -> 0x05a0 }
            int r11 = r2 - r4
            r7 = r3
            r12 = r9
            r13 = r5
            r6.a(r7, r8, r9, r10, r11, r12, r13)     // Catch:{ all -> 0x05a0 }
            int r24 = r9 + -1
            int r2 = r9 * r5
            int r2 = -r2
            int r25 = r2 + -1
            r26 = r9
            goto L_0x0296
        L_0x0404:
            int[] r4 = r2.jB     // Catch:{ all -> 0x05a0 }
            int r6 = r2.jA     // Catch:{ all -> 0x05a0 }
            r4 = r4[r6]     // Catch:{ all -> 0x05a0 }
            goto L_0x02b1
        L_0x040c:
            r0 = r42
            boolean r2 = r0 instanceof com.a.a.f.e     // Catch:{ all -> 0x05a0 }
            if (r2 == 0) goto L_0x052c
            r8 = 0
            r0 = r42
            com.a.a.f.e r0 = (com.a.a.f.e) r0     // Catch:{ all -> 0x05a0 }
            r2 = r0
            com.a.a.e.p r10 = r2.jH     // Catch:{ all -> 0x05a0 }
            r4 = 0
            r7 = 1
            r6 = 0
            int r35 = r2.cv()     // Catch:{ all -> 0x05a0 }
            int r36 = r2.cw()     // Catch:{ all -> 0x05a0 }
            int r11 = r34 - r19
            int r30 = r11 / r35
            int r11 = r33 - r18
            int r32 = r11 / r36
            int r11 = r34 - r19
            int r11 = r11 + r5
            int r11 = r11 + -1
            int r37 = r11 / r35
            int r11 = r33 - r18
            int r11 = r11 + r9
            int r11 = r11 + -1
            int r38 = r11 / r36
            r31 = r32
        L_0x043d:
            r0 = r31
            r1 = r38
            if (r0 > r1) goto L_0x051c
            r28 = r30
        L_0x0445:
            r0 = r28
            r1 = r37
            if (r0 > r1) goto L_0x0516
            r0 = r28
            r1 = r31
            int r11 = r2.y(r0, r1)     // Catch:{ all -> 0x05a0 }
            if (r11 >= 0) goto L_0x05a3
            int r11 = r2.aw(r11)     // Catch:{ all -> 0x05a0 }
            r14 = r11
        L_0x045a:
            r0 = r28
            r1 = r30
            if (r0 != r1) goto L_0x04c5
            int r11 = r34 - r19
            int r13 = r11 % r35
        L_0x0464:
            r0 = r31
            r1 = r32
            if (r0 != r1) goto L_0x04c7
            int r11 = r33 - r18
            int r17 = r11 % r36
        L_0x046e:
            r0 = r28
            r1 = r37
            if (r0 != r1) goto L_0x04ca
            int r11 = r34 + r5
            int r11 = r11 - r19
            int r11 = r11 + -1
            int r11 = r11 % r35
            r16 = r11
        L_0x047e:
            r0 = r31
            r1 = r38
            if (r0 != r1) goto L_0x04cf
            int r11 = r33 + r9
            int r11 = r11 - r18
            int r11 = r11 + -1
            int r11 = r11 % r36
            r27 = r11
        L_0x048e:
            int r11 = r31 - r32
            int r11 = r11 * r36
            int r11 = r11 * r5
            int r12 = r28 - r30
            int r12 = r12 * r35
            int r12 = r12 + r11
            r0 = r28
            r1 = r30
            if (r0 != r1) goto L_0x04d4
            r11 = 0
        L_0x049f:
            int r12 = r12 - r11
            r0 = r31
            r1 = r32
            if (r0 != r1) goto L_0x04d9
            r11 = 0
        L_0x04a7:
            int r11 = r11 * r5
            int r12 = r12 - r11
            if (r14 != 0) goto L_0x04ea
            r14 = r17
            r11 = r12
        L_0x04ae:
            r0 = r27
            if (r14 > r0) goto L_0x0510
            r12 = r11
            r11 = r13
        L_0x04b4:
            r0 = r16
            if (r11 > r0) goto L_0x04de
            r0 = r41
            int[] r15 = r0.jN     // Catch:{ all -> 0x05a0 }
            r17 = 0
            r15[r12] = r17     // Catch:{ all -> 0x05a0 }
            int r11 = r11 + 1
            int r12 = r12 + 1
            goto L_0x04b4
        L_0x04c5:
            r13 = 0
            goto L_0x0464
        L_0x04c7:
            r17 = 0
            goto L_0x046e
        L_0x04ca:
            int r11 = r35 + -1
            r16 = r11
            goto L_0x047e
        L_0x04cf:
            int r11 = r36 + -1
            r27 = r11
            goto L_0x048e
        L_0x04d4:
            int r11 = r34 - r19
            int r11 = r11 % r35
            goto L_0x049f
        L_0x04d9:
            int r11 = r33 - r18
            int r11 = r11 % r36
            goto L_0x04a7
        L_0x04de:
            int r11 = r14 + 1
            int r14 = r16 - r13
            int r14 = r14 + 1
            int r14 = r5 - r14
            int r12 = r12 + r14
            r14 = r11
            r11 = r12
            goto L_0x04ae
        L_0x04ea:
            int r11 = r14 + -1
            int r14 = r10.getWidth()     // Catch:{ all -> 0x05a0 }
            int r15 = r2.cv()     // Catch:{ all -> 0x05a0 }
            int r14 = r14 / r15
            int r15 = r11 % r14
            int r15 = r15 * r35
            int r11 = r11 / r14
            int r39 = r11 * r36
            r0 = r41
            int[] r11 = r0.jN     // Catch:{ all -> 0x05a0 }
            int r14 = r15 + r13
            int r15 = r39 + r17
            int r13 = r16 - r13
            int r16 = r13 + 1
            int r13 = r27 - r17
            int r17 = r13 + 1
            r13 = r5
            r10.a(r11, r12, r13, r14, r15, r16, r17)     // Catch:{ all -> 0x05a0 }
        L_0x0510:
            int r11 = r28 + 1
            r28 = r11
            goto L_0x0445
        L_0x0516:
            int r11 = r31 + 1
            r31 = r11
            goto L_0x043d
        L_0x051c:
            r2 = r20
            r10 = r6
            r11 = r7
            r12 = r29
            r13 = r8
            r6 = r22
            r7 = r23
            r8 = r4
            r4 = r21
            goto L_0x02cd
        L_0x052c:
            r7 = 0
            r0 = r42
            com.a.a.e.p r0 = (com.a.a.e.p) r0     // Catch:{ all -> 0x05a0 }
            r10 = r0
            r0 = r41
            int[] r11 = r0.jN     // Catch:{ all -> 0x05a0 }
            r12 = 0
            int r14 = r34 - r19
            int r15 = r33 - r18
            r13 = r5
            r16 = r5
            r17 = r9
            r10.a(r11, r12, r13, r14, r15, r16, r17)     // Catch:{ all -> 0x05a0 }
            r2 = 0
            r6 = 1
            r4 = 0
            r8 = r2
            r10 = r4
            r11 = r6
            r12 = r29
            r13 = r7
            r4 = r21
            r6 = r22
            r7 = r23
            r2 = r20
            goto L_0x02cd
        L_0x0556:
            r2 = 0
            r4 = r21
            r6 = r22
            r7 = r23
            r8 = r24
            r10 = r25
            r11 = r26
            r12 = r29
            r13 = r2
            r25 = r27
            r26 = r28
            r2 = r20
            r24 = r17
            goto L_0x02cd
        L_0x0570:
            r2 = 0
            r6 = r2
            r3 = r14
            r4 = r17
        L_0x0575:
            if (r6 >= r9) goto L_0x059d
            r2 = 0
        L_0x0578:
            if (r2 >= r5) goto L_0x0596
            r0 = r41
            int[] r7 = r0.jM     // Catch:{ all -> 0x05a0 }
            r7 = r7[r4]     // Catch:{ all -> 0x05a0 }
            r0 = r41
            int[] r8 = r0.jN     // Catch:{ all -> 0x05a0 }
            r8 = r8[r3]     // Catch:{ all -> 0x05a0 }
            r7 = r7 & r8
            int r7 = r7 >> 24
            r8 = -1
            if (r7 != r8) goto L_0x058f
            r2 = 1
            goto L_0x002f
        L_0x058f:
            int r2 = r2 + 1
            int r4 = r4 + r28
            int r3 = r3 + r16
            goto L_0x0578
        L_0x0596:
            int r2 = r6 + 1
            int r4 = r4 + r27
            int r3 = r3 + r15
            r6 = r2
            goto L_0x0575
        L_0x059d:
            r2 = 0
            goto L_0x002f
        L_0x05a0:
            r2 = move-exception
            monitor-exit(r41)
            throw r2
        L_0x05a3:
            r14 = r11
            goto L_0x045a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.f.d.b(java.lang.Object, int, int):boolean");
    }

    public final void a(o oVar) {
        if (isVisible()) {
            int i = this.jB == null ? this.jA : this.jB[this.jA];
            int width = getWidth();
            int height = getHeight();
            oVar.a(this.jH, width * (i % this.jE), height * (i / this.jE), width, height, this.jG, getX(), getY(), 20);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.a.a.e.p r10, int r11, int r12) {
        /*
            r9 = this;
            r0 = 0
            monitor-enter(r9)
            int r2 = r9.getWidth()     // Catch:{ all -> 0x0029 }
            int r3 = r9.getHeight()     // Catch:{ all -> 0x0029 }
            int r4 = r10.getWidth()     // Catch:{ all -> 0x0029 }
            int r5 = r10.getHeight()     // Catch:{ all -> 0x0029 }
            r9.setSize(r11, r12)     // Catch:{ all -> 0x0029 }
            int r1 = r10.getWidth()     // Catch:{ all -> 0x0029 }
            int r1 = r1 % r11
            if (r1 != 0) goto L_0x0023
            int r1 = r10.getHeight()     // Catch:{ all -> 0x0029 }
            int r1 = r1 % r12
            if (r1 == 0) goto L_0x002c
        L_0x0023:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0029 }
            r0.<init>()     // Catch:{ all -> 0x0029 }
            throw r0     // Catch:{ all -> 0x0029 }
        L_0x0029:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0029 }
            throw r0
        L_0x002c:
            r9.jH = r10     // Catch:{ all -> 0x0029 }
            int r1 = r9.jE     // Catch:{ all -> 0x0029 }
            int r6 = r9.jF     // Catch:{ all -> 0x0029 }
            int r1 = r1 * r6
            int r6 = r10.getWidth()     // Catch:{ all -> 0x0029 }
            int r6 = r6 / r11
            r9.jE = r6     // Catch:{ all -> 0x0029 }
            int r6 = r10.getHeight()     // Catch:{ all -> 0x0029 }
            int r6 = r6 / r12
            r9.jF = r6     // Catch:{ all -> 0x0029 }
            int r6 = r9.jF     // Catch:{ all -> 0x0029 }
            int r7 = r9.jE     // Catch:{ all -> 0x0029 }
            int r6 = r6 * r7
            if (r6 >= r1) goto L_0x004e
            r1 = 0
            r9.jB = r1     // Catch:{ all -> 0x0029 }
            r1 = 0
            r9.jA = r1     // Catch:{ all -> 0x0029 }
        L_0x004e:
            int r1 = r9.getWidth()     // Catch:{ all -> 0x0029 }
            if (r11 != r1) goto L_0x005a
            int r1 = r9.getHeight()     // Catch:{ all -> 0x0029 }
            if (r12 == r1) goto L_0x0076
        L_0x005a:
            r1 = 0
            r6 = 0
            r9.i(r1, r6, r11, r12)     // Catch:{ all -> 0x0029 }
            r1 = 0
            r9.jN = r1     // Catch:{ all -> 0x0029 }
            r9.jM = r1     // Catch:{ all -> 0x0029 }
            int r1 = r9.jG     // Catch:{ all -> 0x0029 }
            if (r1 == 0) goto L_0x0076
            int r1 = r9.jG     // Catch:{ all -> 0x0029 }
            switch(r1) {
                case 1: goto L_0x006f;
                case 2: goto L_0x0078;
                case 3: goto L_0x007b;
                case 4: goto L_0x0081;
                case 5: goto L_0x0084;
                case 6: goto L_0x0089;
                case 7: goto L_0x008b;
                default: goto L_0x006d;
            }     // Catch:{ all -> 0x0029 }
        L_0x006d:
            monitor-exit(r9)     // Catch:{ all -> 0x0029 }
        L_0x006e:
            return
        L_0x006f:
            int r1 = r4 - r2
            int r0 = r5 - r3
        L_0x0073:
            r9.t(r1, r0)     // Catch:{ all -> 0x0029 }
        L_0x0076:
            monitor-exit(r9)     // Catch:{ all -> 0x0029 }
            goto L_0x006e
        L_0x0078:
            int r1 = r4 - r2
            goto L_0x0073
        L_0x007b:
            int r1 = r5 - r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0073
        L_0x0081:
            int r1 = r5 - r3
            goto L_0x0073
        L_0x0084:
            int r1 = r5 - r3
            int r0 = r4 - r2
            goto L_0x0073
        L_0x0089:
            r1 = r0
            goto L_0x0073
        L_0x008b:
            int r1 = r4 - r2
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.f.d.a(com.a.a.e.p, int, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.f.d.a(java.lang.Object, int, int):boolean
     arg types: [com.a.a.e.p, int, int]
     candidates:
      com.a.a.f.d.a(com.a.a.e.p, int, int):void
      com.a.a.f.d.a(java.lang.Object, int, int):boolean */
    public final boolean a(p pVar, int i, int i2, boolean z) {
        if (pVar == null) {
            throw new IllegalArgumentException();
        } else if (!isVisible()) {
            return false;
        } else {
            return z ? b(pVar, i, i2) : a((Object) pVar, i, i2);
        }
    }

    public final boolean a(d dVar, boolean z) {
        if (dVar == null) {
            throw new NullPointerException();
        } else if (!dVar.isVisible() || !isVisible()) {
            return false;
        } else {
            return z ? b(dVar, 0, 0) : a(dVar, 0, 0);
        }
    }

    public final boolean a(e eVar, boolean z) {
        if (eVar == null) {
            throw new NullPointerException();
        } else if (isVisible() && eVar.isVisible() && isVisible()) {
            return z ? b(eVar, 0, 0) : a(eVar, 0, 0);
        } else {
            return false;
        }
    }

    public void at(int i) {
        int length = this.jB == null ? this.jF * this.jE : this.jB.length;
        if (i < 0 || i >= length) {
            throw new IndexOutOfBoundsException();
        }
        this.jA = i;
    }

    public void au(int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        if (this.jG != i) {
            int width = getWidth();
            int height = getHeight();
            int i6 = this.jG;
            switch (i) {
                case 0:
                    int i7 = this.jC;
                    i2 = this.jD;
                    i3 = i7;
                    break;
                case 1:
                    int i8 = width - this.jC;
                    i2 = height - this.jD;
                    i3 = i8;
                    break;
                case 2:
                    int i9 = width - this.jC;
                    i2 = this.jD;
                    i3 = i9;
                    break;
                case 3:
                    int i10 = this.jC;
                    i2 = height - this.jD;
                    i3 = i10;
                    break;
                case 4:
                    int i11 = height - this.jD;
                    i2 = this.jC;
                    i3 = i11;
                    break;
                case 5:
                    int i12 = height - this.jD;
                    i2 = width - this.jC;
                    i3 = i12;
                    break;
                case 6:
                    int i13 = this.jD;
                    i2 = this.jC;
                    i3 = i13;
                    break;
                case 7:
                    int i14 = this.jD;
                    i2 = width - this.jC;
                    i3 = i14;
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            switch (i6) {
                case 0:
                    i4 = this.jC;
                    i5 = this.jD;
                    break;
                case 1:
                    i4 = width - this.jC;
                    i5 = height - this.jD;
                    break;
                case 2:
                    i4 = width - this.jC;
                    i5 = this.jD;
                    break;
                case 3:
                    i4 = this.jC;
                    i5 = height - this.jD;
                    break;
                case 4:
                    i4 = height - this.jD;
                    i5 = this.jC;
                    break;
                case 5:
                    i4 = height - this.jD;
                    i5 = width - this.jC;
                    break;
                case 6:
                    i4 = this.jD;
                    i5 = this.jC;
                    break;
                case 7:
                    i4 = this.jD;
                    i5 = width - this.jC;
                    break;
                default:
                    return;
            }
            t(i4 - i3, i5 - i2);
            this.jG = i;
        }
    }

    public int cm() {
        return getX() + this.jC;
    }

    public int cn() {
        return getY() + this.jD;
    }

    public final int co() {
        return this.jA;
    }

    public int cp() {
        return this.jB == null ? this.jF * this.jE : this.jB.length;
    }

    public void cq() {
        if (this.jA == (this.jB == null ? this.jF * this.jE : this.jB.length) - 1) {
            this.jA = 0;
        } else {
            this.jA++;
        }
    }

    public void cr() {
        if (this.jA == 0) {
            this.jA = (this.jB == null ? this.jF * this.jE : this.jB.length) - 1;
        } else {
            this.jA--;
        }
    }

    public int cs() {
        return this.jE * this.jF;
    }

    public void d(int[] iArr) {
        if (iArr == null) {
            this.jB = null;
            return;
        }
        int i = (this.jF * this.jE) - 1;
        if (r3 == 0) {
            throw new IllegalArgumentException();
        }
        for (int i2 : iArr) {
            if (i2 > i || i2 < 0) {
                throw new ArrayIndexOutOfBoundsException();
            }
        }
        this.jB = iArr;
        this.jA = 0;
    }

    public void i(int i, int i2, int i3, int i4) {
        if (i3 < 0 || i4 < 0) {
            throw new IllegalArgumentException();
        }
        this.jI = i;
        this.jJ = i2;
        this.jK = i3;
        this.jL = i4;
    }

    public void v(int i, int i2) {
        this.jC = i;
        this.jD = i2;
    }

    public void w(int i, int i2) {
        int i3;
        int i4;
        int width = getWidth();
        int height = getHeight();
        switch (this.jG) {
            case 0:
                i3 = this.jC;
                i4 = this.jD;
                break;
            case 1:
                i3 = width - this.jC;
                i4 = height - this.jD;
                break;
            case 2:
                i3 = width - this.jC;
                i4 = this.jD;
                break;
            case 3:
                i3 = this.jC;
                i4 = height - this.jD;
                break;
            case 4:
                i3 = height - this.jD;
                i4 = this.jC;
                break;
            case 5:
                i3 = height - this.jD;
                i4 = width - this.jC;
                break;
            case 6:
                i3 = this.jD;
                i4 = this.jC;
                break;
            case 7:
                i3 = this.jD;
                i4 = width - this.jC;
                break;
            default:
                return;
        }
        u(i - i3, i2 - i4);
    }
}
