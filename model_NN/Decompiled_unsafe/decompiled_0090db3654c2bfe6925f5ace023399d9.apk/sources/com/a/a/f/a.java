package com.a.a.f;

import com.a.a.e.c;
import com.a.a.e.o;
import org.meteoroid.plugin.device.MIDPDevice;

public abstract class a extends c {
    public static final int DOWN_PRESSED = 64;
    public static final int FIRE_PRESSED = 256;
    public static final int GAME_A_PRESSED = 512;
    public static final int GAME_B_PRESSED = 1024;
    public static final int GAME_C_PRESSED = 2048;
    public static final int GAME_D_PRESSED = 4096;
    public static final int LEFT_PRESSED = 4;
    public static final int RIGHT_PRESSED = 32;
    public static final int UP_PRESSED = 2;
    private int jt;

    protected a(boolean z) {
    }

    private static final boolean ar(int i) {
        return i == 6 || i == 1 || i == 2 || i == 5 || i == 8 || i == 9 || i == 10 || i == 11 || i == 12;
    }

    public void a(o oVar) {
    }

    public int aS() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public o bX() {
        return ((MIDPDevice) org.meteoroid.core.c.ou).bX();
    }

    public void bw() {
        if (this.in != null) {
            this.in.bw();
        }
    }

    public int cl() {
        return this.jt;
    }

    public void g(int i, int i2, int i3, int i4) {
        if (this.in != null) {
            this.in.bw();
        }
    }

    public void s(int i, int i2) {
        int R = R(i2);
        if (i == 0 && ar(R)) {
            this.jt = (1 << R) | this.jt;
        } else if (i == 1 && ar(R)) {
            this.jt = (1 << R) ^ this.jt;
        }
    }
}
