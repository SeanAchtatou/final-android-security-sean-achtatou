package com.a.a.f;

import com.a.a.e.o;

public abstract class b {
    private int height;
    private boolean ju;
    private int width;
    private int x;
    private int y;

    b(int i, int i2, int i3, int i4, boolean z) {
        setSize(i3, i4);
        u(i, i2);
        setVisible(z);
    }

    public abstract void a(o oVar);

    public final int getHeight() {
        return this.height;
    }

    public final int getWidth() {
        return this.width;
    }

    public final int getX() {
        return this.x;
    }

    public final int getY() {
        return this.y;
    }

    public final boolean isVisible() {
        return this.ju;
    }

    /* access modifiers changed from: package-private */
    public void setSize(int i, int i2) {
        if (i < 1 || i2 < 1) {
            throw new IllegalArgumentException();
        }
        this.width = i;
        this.height = i2;
    }

    public void setVisible(boolean z) {
        this.ju = z;
    }

    public void t(int i, int i2) {
        synchronized (this) {
            this.x += i;
            this.y += i2;
        }
    }

    public void u(int i, int i2) {
        synchronized (this) {
            this.x = i;
            this.y = i2;
        }
    }
}
