package com.a.a.f;

import com.a.a.e.o;
import com.a.a.e.p;
import java.lang.reflect.Array;

public class e extends b {
    private final int jE;
    private final int jF;
    p jH;
    private int jO;
    private int jP;
    private int jQ;
    private int[][] jR;
    int[] jS;
    int jT;

    public e(int i, int i2, p pVar, int i3, int i4) {
        super(0, 0, i * i3, i2 * i4, true);
        if (pVar == null) {
            throw new NullPointerException();
        } else if (i <= 0 || i2 <= 0 || i4 <= 0 || i3 <= 0) {
            throw new IllegalArgumentException();
        } else if (pVar.getWidth() % i3 == 0 && pVar.getHeight() % i4 == 0) {
            this.jH = pVar;
            this.jE = i;
            this.jF = i2;
            this.jP = i3;
            this.jO = i4;
            this.jQ = (pVar.getWidth() / i3) * (pVar.getHeight() / i4);
            this.jR = (int[][]) Array.newInstance(Integer.TYPE, i2, i);
            this.jS = new int[5];
            this.jT = 0;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public final void a(o oVar) {
        synchronized (this) {
            if (isVisible()) {
                int x = getX();
                int y = getY();
                int i = 0;
                int ct = ct();
                int cu = cu();
                int cv = cv();
                int cw = cw();
                int width = this.jH.getWidth() / cv;
                while (true) {
                    int i2 = i;
                    if (i2 < cu) {
                        int i3 = x;
                        for (int i4 = 0; i4 < ct; i4++) {
                            int y2 = y(i4, i2);
                            if (y2 < 0) {
                                y2 = aw(y2);
                            }
                            if (y2 != 0) {
                                int i5 = y2 - 1;
                                oVar.a(this.jH, cv * (i5 % width), (i5 / width) * cw, cv, cw, 0, i3, y, 20);
                            }
                            i3 += cv;
                        }
                        i = i2 + 1;
                        y += cw;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public int av(int i) {
        int i2;
        synchronized (this) {
            if (i >= 0) {
                if (i <= this.jQ) {
                    if (this.jT == this.jS.length) {
                        int[] iArr = new int[(this.jT + 6)];
                        System.arraycopy(this.jS, 0, iArr, 0, this.jT);
                        this.jS = iArr;
                    }
                    this.jS[this.jT] = i;
                    this.jT++;
                    i2 = -this.jT;
                }
            }
            throw new IndexOutOfBoundsException();
        }
        return i2;
    }

    public int aw(int i) {
        int i2;
        synchronized (this) {
            int i3 = (-i) - 1;
            if (i3 >= 0) {
                if (i3 < this.jT) {
                    i2 = this.jS[i3];
                }
            }
            throw new IndexOutOfBoundsException();
        }
        return i2;
    }

    public void b(int i, int i2, int i3, int i4, int i5) {
        synchronized (this) {
            if (i3 < 0 || i4 < 0) {
                throw new IllegalArgumentException();
            }
            if (i2 >= 0 && i >= 0) {
                if (i + i3 <= this.jE && i2 + i4 <= this.jF) {
                    if ((-i5) - 1 >= this.jT || i5 > this.jQ) {
                        throw new IndexOutOfBoundsException();
                    }
                    int i6 = i2 + i4;
                    int i7 = i + i3;
                    while (i2 < i6) {
                        for (int i8 = i; i8 < i7; i8++) {
                            this.jR[i2][i8] = i5;
                        }
                        i2++;
                    }
                }
            }
            throw new IndexOutOfBoundsException();
        }
    }

    public void b(p pVar, int i, int i2) {
        synchronized (this) {
            if (this.jH == null) {
                throw new NullPointerException();
            } else if (i2 <= 0 || i <= 0) {
                throw new IllegalArgumentException();
            } else if (this.jH.getWidth() % i == 0 && this.jH.getHeight() % i2 == 0) {
                int width = (this.jH.getWidth() / cv()) * (this.jH.getHeight() / cw());
                setSize(this.jE * i, this.jF * i2);
                this.jP = i;
                this.jO = i2;
                if (width >= this.jQ) {
                    this.jQ = width;
                    return;
                }
                this.jQ = width;
                this.jS = new int[5];
                this.jT = 0;
                b(0, 0, ct(), cu(), 0);
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    public final int ct() {
        return this.jE;
    }

    public final int cu() {
        return this.jF;
    }

    public final int cv() {
        return this.jP;
    }

    public final int cw() {
        return this.jO;
    }

    public void m(int i, int i2, int i3) {
        synchronized (this) {
            if ((-i3) - 1 >= this.jT || i3 > this.jQ) {
                throw new IndexOutOfBoundsException();
            }
            this.jR[i2][i] = i3;
        }
    }

    public void x(int i, int i2) {
        synchronized (this) {
            int i3 = (-i) - 1;
            if (i3 >= 0) {
                if (i3 < this.jT) {
                    if (i2 >= 0) {
                        if (i2 <= this.jQ) {
                            this.jS[i3] = i2;
                        }
                    }
                    throw new IndexOutOfBoundsException();
                }
            }
            throw new IndexOutOfBoundsException();
        }
    }

    public int y(int i, int i2) {
        return this.jR[i2][i];
    }
}
