package com.a.a.f;

import android.util.Log;
import com.a.a.e.o;
import com.a.a.e.r;
import java.util.Vector;

public class c {
    private Vector<b> jv = new Vector<>();
    private int jw = 0;
    private int jx = 0;
    private int jy = r.OUTOFITEM;
    private int jz = r.OUTOFITEM;

    public c() {
        Log.d("LayerManager", "LayerManager is created.");
    }

    public void a(b bVar) {
        this.jv.add(bVar);
    }

    public void a(b bVar, int i) {
        this.jv.insertElementAt(bVar, i);
    }

    public b as(int i) {
        return this.jv.get(i);
    }

    public void b(b bVar) {
        this.jv.remove(bVar);
    }

    public void d(o oVar, int i, int i2) {
        int bN = oVar.bN();
        int bO = oVar.bO();
        int bM = oVar.bM();
        int bL = oVar.bL();
        oVar.translate(i - this.jw, i2 - this.jx);
        oVar.d(this.jw, this.jx, this.jy, this.jz);
        int size = getSize();
        while (true) {
            size--;
            if (size >= 0) {
                b as = as(size);
                if (as.isVisible()) {
                    as.a(oVar);
                }
            } else {
                oVar.translate((-i) + this.jw, (-i2) + this.jx);
                oVar.setClip(bN, bO, bM, bL);
                return;
            }
        }
    }

    public int getSize() {
        return this.jv.size();
    }

    public void h(int i, int i2, int i3, int i4) {
        if (i3 < 0 || i4 < 0) {
            throw new IllegalArgumentException();
        }
        this.jw = i;
        this.jx = i2;
        this.jy = i3;
        this.jz = i4;
    }
}
