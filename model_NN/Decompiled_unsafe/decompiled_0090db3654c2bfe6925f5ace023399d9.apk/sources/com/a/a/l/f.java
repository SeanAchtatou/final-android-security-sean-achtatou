package com.a.a.l;

import android.util.Log;
import com.a.a.i.k;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.microedition.media.control.ToneControl;
import org.meteoroid.core.b;

public final class f {
    public static final int AUTHMODE_ANY = 1;
    public static final int AUTHMODE_PRIVATE = 0;
    public static final String LOG_TAG = "RMS";
    private static final String PREFIX = "rms_";
    /* access modifiers changed from: private */
    public HashMap<Integer, byte[]> lk = new HashMap<>();
    private int ll;
    private long lm;
    private HashSet<e> ln = new HashSet<>();
    private int lo;
    private boolean lp;
    private boolean lq;
    private String name;
    private int size;
    private int version;

    private static final class a implements c {
        private int index;
        private f lr;
        private d ls;
        /* access modifiers changed from: private */
        public b lt;
        private boolean lu;
        private ArrayList<Integer> lv;
        private final Comparator<Map.Entry<Integer, byte[]>> lw = new Comparator<Map.Entry<Integer, byte[]>>() {
            /* renamed from: a */
            public int compare(Map.Entry<Integer, byte[]> entry, Map.Entry<Integer, byte[]> entry2) {
                int a = a.this.lt.a(entry.getValue(), entry2.getValue());
                if (a == 0) {
                    return 0;
                }
                return a == 1 ? 1 : -1;
            }
        };
        private e lx = new e() {
            public void a(f fVar, int i) {
                a.this.eh();
            }

            public void b(f fVar, int i) {
                a.this.eh();
            }

            public void c(f fVar, int i) {
                a.this.eh();
            }
        };

        public a(f fVar, d dVar, b bVar, boolean z) {
            this.lr = fVar;
            this.ls = dVar;
            this.lt = bVar;
            this.lu = z;
            this.lv = new ArrayList<>();
            s(z);
            eh();
            reset();
        }

        public void destroy() {
            Log.d("RecordEnumerationImpl", "destroy");
            this.lv.clear();
            this.lv = null;
        }

        public int ea() {
            Log.d("RecordEnumerationImpl", "numRecords:" + this.lv.size());
            return this.lv.size();
        }

        public byte[] eb() {
            Log.d("RecordEnumerationImpl", "nextRecord");
            boolean unused = this.lr.isOpen();
            try {
                byte[] bk = this.lr.bk(this.lv.get(this.index).intValue());
                this.index++;
                return bk;
            } catch (Exception e) {
                throw new a();
            }
        }

        public int ec() {
            try {
                int intValue = this.lv.get(this.index).intValue();
                this.index++;
                Log.d("RecordEnumerationImpl", "nextRecordId:" + intValue);
                return intValue;
            } catch (Exception e) {
                throw new a();
            }
        }

        public byte[] ed() {
            Log.d("RecordEnumerationImpl", "previousRecord");
            boolean unused = this.lr.isOpen();
            try {
                this.index--;
                return this.lr.bk(this.lv.get(this.index).intValue());
            } catch (Exception e) {
                throw new a();
            }
        }

        public int ee() {
            Log.d("RecordEnumerationImpl", "previousRecordId");
            try {
                this.index--;
                return this.lv.get(this.index).intValue();
            } catch (Exception e) {
                throw new a();
            }
        }

        public boolean ef() {
            Log.d("RecordEnumerationImpl", "hasNextElement:" + this.index + "[" + this.lv.size() + "]");
            try {
                boolean unused = this.lr.isOpen();
                return this.index <= this.lv.size() + -1;
            } catch (j e) {
                return false;
            }
        }

        public boolean eg() {
            Log.d("RecordEnumerationImpl", "hasPreviousElement" + this.index + "[" + this.lv.size() + "]");
            try {
                boolean unused = this.lr.isOpen();
                return this.index > 0;
            } catch (j e) {
                return false;
            }
        }

        public void eh() {
            Log.d("RecordEnumerationImpl", "rebuild");
            synchronized (this) {
                this.lv.clear();
                ArrayList arrayList = new ArrayList();
                for (Map.Entry entry : this.lr.lk.entrySet()) {
                    if (this.ls == null || (this.ls != null && this.ls.s((byte[]) entry.getValue()))) {
                        arrayList.add(entry);
                    }
                }
                if (this.lt != null) {
                    Collections.sort(arrayList, this.lw);
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    Map.Entry entry2 = (Map.Entry) it.next();
                    this.lv.add(entry2.getKey());
                    Log.d("RecordEnumerationImpl", "add:" + entry2.getKey());
                }
            }
        }

        public boolean ei() {
            Log.d("RecordEnumerationImpl", "isKeptUpdated");
            return this.lu;
        }

        public void reset() {
            Log.d("RecordEnumerationImpl", "reset");
            this.index = 0;
        }

        public void s(boolean z) {
            Log.d("RecordEnumerationImpl", "keepUpdated" + z);
            if (z) {
                this.lr.a(this.lx);
            } else {
                this.lr.b(this.lx);
            }
            this.lu = z;
        }
    }

    private f(String str) {
        this.name = str;
    }

    public static f a(String str, boolean z, int i, boolean z2) {
        return d(str, z);
    }

    private synchronized void a(DataOutputStream dataOutputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
        this.version++;
        dataOutputStream2.writeInt(this.version);
        dataOutputStream2.writeLong(System.currentTimeMillis());
        dataOutputStream2.writeInt(this.ll);
        dataOutputStream2.writeInt(this.lk.size());
        for (Map.Entry next : this.lk.entrySet()) {
            dataOutputStream2.writeInt(((Integer) next.getKey()).intValue());
            byte[] bArr = (byte[]) next.getValue();
            dataOutputStream2.writeInt(bArr.length);
            dataOutputStream2.write(bArr);
        }
        byteArrayOutputStream.flush();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String b = b(byteArray, "MD5");
        Log.d(LOG_TAG, "Generate sign:" + b);
        dataOutputStream.write(byteArray);
        dataOutputStream.writeUTF(b);
        dataOutputStream.flush();
    }

    public static void ab(String str) {
        Log.d(LOG_TAG, "Del " + str);
        if (!b.aO(PREFIX + str)) {
            throw new i();
        } else if (!b.aR(PREFIX + str)) {
            throw new g();
        }
    }

    private String b(byte[] bArr, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str.toUpperCase());
            instance.reset();
            instance.update(bArr);
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < digest.length; i++) {
                if (Integer.toHexString(digest[i] & ToneControl.SILENCE).length() == 1) {
                    stringBuffer.append("0").append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
                } else {
                    stringBuffer.append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
                }
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static f c(String str, String str2, String str3) {
        return d(str, false);
    }

    private synchronized void c(DataInputStream dataInputStream) {
        synchronized (this) {
            DataInputStream dataInputStream2 = new DataInputStream(dataInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = dataInputStream2.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            DataInputStream dataInputStream3 = new DataInputStream(new ByteArrayInputStream(byteArray));
            this.version = dataInputStream3.readInt();
            this.lm = dataInputStream3.readLong();
            this.ll = dataInputStream3.readInt();
            int readInt = dataInputStream3.readInt();
            for (int i = 0; i < readInt; i++) {
                int readInt2 = dataInputStream3.readInt();
                byte[] bArr2 = new byte[dataInputStream3.readInt()];
                dataInputStream3.read(bArr2);
                this.lk.put(Integer.valueOf(readInt2), bArr2);
            }
            try {
                String readUTF = dataInputStream3.readUTF();
                dataInputStream3.close();
                byte[] bArr3 = new byte[((byteArray.length - readUTF.getBytes().length) - 2)];
                System.arraycopy(byteArray, 0, bArr3, 0, bArr3.length);
                if (!readUTF.equals(b(bArr3, "MD5"))) {
                    Log.w(LOG_TAG, "Verify sign fail:" + readUTF);
                    throw new IllegalArgumentException("Invalid sign. The file has been modified.");
                }
            } catch (IOException e) {
                Log.d(LOG_TAG, "No need to verify the rms.");
            } catch (Exception e2) {
                throw new IOException(e2.getMessage());
            }
            ej();
        }
    }

    public static f d(String str, boolean z) {
        Log.d(LOG_TAG, "openRecordStore:" + str);
        f fVar = new f(str);
        if (b.aO(PREFIX + str)) {
            Log.d(LOG_TAG, str + " exist.");
            try {
                DataInputStream dataInputStream = new DataInputStream(b.aQ(PREFIX + str));
                fVar.c(dataInputStream);
                dataInputStream.close();
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
                throw new g();
            }
        } else if (!z) {
            Log.d(LOG_TAG, str + " not exist and not necessary to create.");
            throw new i();
        } else {
            Log.d(LOG_TAG, str + " not exist and necessary to create.");
            try {
                fVar.em();
            } catch (Exception e2) {
                Log.e(LOG_TAG, e2.getMessage());
                throw new g();
            }
        }
        fVar.lq = true;
        return fVar;
    }

    private void ej() {
        this.size = 0;
        for (byte[] length : this.lk.values()) {
            this.size = length.length + this.size;
        }
        Log.d(LOG_TAG, "calcTheSizeOfRecords:" + this.size);
    }

    private synchronized void ek() {
        if (b.aO(PREFIX + this.name)) {
            try {
                DataInputStream dataInputStream = new DataInputStream(b.aQ(PREFIX + this.name));
                int readInt = dataInputStream.readInt();
                Log.d(LOG_TAG, "Current version:" + this.version + " newest version:" + readInt);
                dataInputStream.close();
                if (readInt > this.version) {
                    Log.d(LOG_TAG, "Records are out of date.Refereshing...");
                    c(new DataInputStream(b.aQ(PREFIX + this.name)));
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
        return;
    }

    public static String[] el() {
        Log.d(LOG_TAG, "listRecordStores");
        List<String> aM = b.aM(PREFIX);
        if (aM.isEmpty()) {
            return null;
        }
        String[] strArr = new String[aM.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < strArr.length) {
                strArr[i2] = aM.get(i2).substring(PREFIX.length());
                i = i2 + 1;
            } else {
                aM.clear();
                return strArr;
            }
        }
    }

    private synchronized void em() {
        DataOutputStream dataOutputStream = new DataOutputStream(b.aP(PREFIX + this.name));
        a(dataOutputStream);
        dataOutputStream.flush();
        dataOutputStream.close();
        ej();
    }

    /* access modifiers changed from: private */
    public boolean isOpen() {
        if (this.lq) {
            return this.lq;
        }
        Log.e(LOG_TAG, "RecordStoreNotOpenException");
        throw new j();
    }

    public int a(int i, byte[] bArr, int i2) {
        isOpen();
        ek();
        byte[] bk = bk(i);
        int min = Math.min(bk.length, bArr.length - i2);
        System.arraycopy(bk, 0, bArr, i2, min);
        Log.d(LOG_TAG, "getRecordwithoffset" + i + " offset:" + i2 + " length:" + min);
        return min;
    }

    public c a(d dVar, b bVar, boolean z) {
        boolean z2 = true;
        isOpen();
        StringBuilder append = new StringBuilder().append("enumerateRecords: filter:").append(dVar != null).append(" comparator:");
        if (bVar == null) {
            z2 = false;
        }
        Log.d(LOG_TAG, append.append(z2).append(" keepUpdated:").append(z).toString());
        return new a(this, dVar, bVar, z);
    }

    public void a(int i, byte[] bArr, int i2, int i3) {
        isOpen();
        Log.d(LOG_TAG, "setRecord:" + i + " data:" + bArr.length);
        if (this.lk.containsKey(Integer.valueOf(i))) {
            this.lk.remove(Integer.valueOf(i));
            byte[] bArr2 = new byte[i3];
            System.arraycopy(bArr, i2, bArr2, 0, i3);
            this.lk.put(Integer.valueOf(i), bArr2);
            try {
                em();
            } catch (Exception e) {
            }
            Iterator<e> it = this.ln.iterator();
            while (it.hasNext()) {
                it.next().b(this, i);
            }
            return;
        }
        throw new a();
    }

    public void a(e eVar) {
        if (!this.ln.contains(eVar)) {
            this.ln.add(eVar);
            Log.d(LOG_TAG, "addRecordListener");
        }
    }

    public void b(e eVar) {
        if (this.ln.remove(eVar)) {
            Log.d(LOG_TAG, "removeRecordListener");
        }
    }

    public void bi(int i) {
        isOpen();
        Log.d(LOG_TAG, "deleteRecord" + i);
        if (this.lk.containsKey(Integer.valueOf(i))) {
            this.lk.remove(Integer.valueOf(i));
            try {
                em();
                Iterator<e> it = this.ln.iterator();
                while (it.hasNext()) {
                    it.next().c(this, i);
                }
            } catch (Exception e) {
                throw new g();
            }
        } else {
            throw new a();
        }
    }

    public int bj(int i) {
        isOpen();
        ek();
        Log.d(LOG_TAG, "getRecordSize:" + i);
        if (this.lk.containsKey(Integer.valueOf(i))) {
            return this.lk.get(Integer.valueOf(i)).length;
        }
        throw new a();
    }

    public byte[] bk(int i) {
        isOpen();
        ek();
        if (this.lk.containsKey(Integer.valueOf(i))) {
            Log.d(LOG_TAG, "getRecord" + i + " length:" + this.lk.get(Integer.valueOf(i)).length);
            return this.lk.get(Integer.valueOf(i));
        }
        throw new a("recordId=" + i);
    }

    public void en() {
        isOpen();
        ek();
        Log.d(LOG_TAG, "closeRecordStore" + this.name + " at version:" + this.version);
        this.ln.clear();
        this.ln = null;
        this.lk.clear();
        this.lq = false;
        this.lk = null;
        this.name = null;
        System.gc();
    }

    public int eo() {
        isOpen();
        ek();
        Log.d(LOG_TAG, "getNumRecords" + this.lk.size());
        return this.lk.size();
    }

    public int ep() {
        isOpen();
        return k.JULY - this.size;
    }

    public int eq() {
        isOpen();
        ek();
        Log.d(LOG_TAG, "getNextRecordID");
        return this.ll + 1;
    }

    public int f(byte[] bArr, int i, int i2) {
        isOpen();
        this.ll++;
        Log.d(LOG_TAG, "addRecord:" + this.ll + " offset:" + i + " numBytes:" + i2);
        byte[] bArr2 = new byte[i2];
        if (i2 != 0) {
            System.arraycopy(bArr, i, bArr2, 0, i2);
        }
        this.lk.put(Integer.valueOf(this.ll), bArr2);
        try {
            em();
            Iterator<e> it = this.ln.iterator();
            while (it.hasNext()) {
                it.next().a(this, this.ll);
            }
            return this.ll;
        } catch (Exception e) {
            throw new g();
        }
    }

    public void g(int i, boolean z) {
        this.lo = i;
        this.lp = z;
    }

    public long getLastModified() {
        isOpen();
        ek();
        return this.lm;
    }

    public String getName() {
        isOpen();
        return this.name;
    }

    public int getSize() {
        isOpen();
        ek();
        return this.size;
    }

    public int getVersion() {
        isOpen();
        ek();
        return this.version;
    }
}
