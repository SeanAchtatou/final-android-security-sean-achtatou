package com.a.a.b;

import android.bluetooth.BluetoothClass;
import android.util.Log;

public class d {
    private int gr;
    private int gs;
    private int gt;
    BluetoothClass gu;

    public d(int i) {
        Log.e("DeviceClass", "record = " + i);
    }

    protected d(BluetoothClass bluetoothClass) {
        this.gu = bluetoothClass;
    }

    public int X() {
        Log.e("DeviceClass", "getServiceClasses");
        return this.gr;
    }

    public int Y() {
        Log.e("DeviceClass", "getMinorDeviceClass = " + this.gu.getDeviceClass());
        if (this.gu.getDeviceClass() == 516 || this.gu.getDeviceClass() == 524) {
            Log.e("DeviceClass", "getMinorDeviceClass return 4");
            return 4;
        } else if (this.gu.getDeviceClass() == 268) {
            return 12;
        } else {
            Log.e("DeviceClass", "getMinorDeviceClass 不是已知类型,实际类型是" + this.gu.getDeviceClass());
            return 4;
        }
    }

    public int getMajorDeviceClass() {
        Log.e("DeviceClass", "getMajorDeviceClass = " + this.gu.getMajorDeviceClass());
        return this.gu.getMajorDeviceClass();
    }
}
