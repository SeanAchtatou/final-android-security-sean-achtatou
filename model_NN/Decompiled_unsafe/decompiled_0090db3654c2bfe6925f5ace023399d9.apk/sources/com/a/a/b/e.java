package com.a.a.b;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.a.a.b.i;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.Vector;
import org.meteoroid.core.l;

public class e {
    public static final int CACHED = 0;
    public static final int GIAC = 10390323;
    public static final int LIAC = 10390272;
    public static final int NOT_DISCOVERABLE = 0;
    public static final int PREKNOWN = 1;
    protected static f gE;
    public static final e gv = new e();
    boolean gA = false;
    int gB;
    ArrayList<a> gC = new ArrayList<>();
    public Thread gD = new Thread() {
        public void run() {
            int i = 0;
            Log.d("DISCOVERYAGENT", "start");
            while (true) {
                int i2 = i;
                if (i2 < e.this.gC.size()) {
                    try {
                        BluetoothDevice bluetoothDevice = e.this.gC.get(i2).gH.he;
                        Log.d("DiscoveryAgent", "server name = " + bluetoothDevice.getName());
                        e.gE.a(e.this.gC.get(i2).index, new k[]{new k(bluetoothDevice, o.gw.toString())});
                    } catch (Exception e) {
                        Log.d("DISCOVERYAGENT", "搜索服务器error = ", e);
                    }
                    e.gE.m(e.this.gC.get(i2).index, 1);
                    i = i2 + 1;
                } else {
                    Log.d("DiscoveryAgent", "搜索服务完成");
                    return;
                }
            }
        }
    };
    public Vector<BluetoothDevice> gF = new Vector<>();
    public UUID gw;
    protected final BroadcastReceiver gx = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.bluetooth.device.action.FOUND".equals(intent.getAction())) {
                Log.d("DiscoveryAgent", "发现设备");
                BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                l lVar = new l(bluetoothDevice);
                d dVar = new d(bluetoothDevice.getBluetoothClass());
                Log.d("DiscoveryAgent", "name = " + bluetoothDevice.getName());
                Log.d("DiscoveryAgent", "add = " + bluetoothDevice.getAddress());
                if (bluetoothDevice.getBluetoothClass().getMajorDeviceClass() == 512 && bluetoothDevice.getBluetoothClass().getDeviceClass() == 524) {
                    Log.d("DiscoveryAgent", "这是一个android手机设备.添加到vector");
                    e.this.gF.add(bluetoothDevice);
                }
                e.this.gz.a(lVar, dVar);
            }
        }
    };
    protected final BroadcastReceiver gy = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(intent.getAction())) {
                e.this.gz.L(0);
                l.getActivity().unregisterReceiver(e.this.gx);
                l.getActivity().unregisterReceiver(e.this.gy);
                Log.d("DiscoveryAgent", "设备搜索结束");
            }
        }
    };
    f gz;
    int index = 0;

    protected class a {
        l gH;
        int index;

        protected a() {
        }
    }

    public l[] J(int i) {
        Log.d("DiscoveryAgent", "retrieveDevices");
        Set<BluetoothDevice> bondedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
        l[] lVarArr = new l[bondedDevices.size()];
        int i2 = 0;
        Iterator<BluetoothDevice> it = bondedDevices.iterator();
        while (true) {
            int i3 = i2;
            if (it.hasNext()) {
                lVarArr[i3] = new l(it.next());
                i2 = i3 + 1;
            } else {
                Log.d("DiscoveryAgent", "devicies.size = " + lVarArr.length);
                return lVarArr;
            }
        }
    }

    public boolean K(int i) {
        i.a aVar = i.gK;
        return i.a.gN.cancelDiscovery();
    }

    public synchronized int a(int[] iArr, o[] oVarArr, l lVar, f fVar) {
        a aVar;
        Log.d("DiscoveryAgent", "开始搜索服务器");
        if (this.gw == null) {
            for (int i = 0; i < oVarArr.length; i++) {
                o oVar = oVarArr[i];
                if (o.gw != null) {
                    o oVar2 = oVarArr[i];
                    this.gw = o.gw;
                }
            }
        }
        aVar = new a();
        int i2 = this.index;
        this.index = i2 + 1;
        aVar.index = i2;
        aVar.gH = lVar;
        gE = fVar;
        this.gC.add(aVar);
        Log.d("DiscoveryAgent", "add server = " + this.index);
        new Thread() {
            public void run() {
                Log.d("DISCOVERYAGENT", "start");
                try {
                    BluetoothDevice bluetoothDevice = e.this.gC.get(0).gH.he;
                    Log.d("DiscoveryAgent", "server name = " + bluetoothDevice.getName());
                    e.gE.a(e.this.gC.get(0).index, new k[]{new k(bluetoothDevice, o.gw.toString())});
                } catch (Exception e) {
                    Log.d("DISCOVERYAGENT", "搜索服务器error = ", e);
                }
                e.gE.m(e.this.gC.get(0).index, 1);
                e.this.gC.remove(0);
                Log.d("DiscoveryAgent", "搜索服务完成");
            }
        }.start();
        return aVar.index;
    }

    public String a(UUID uuid, int i, boolean z) {
        Log.d("DiscoveryAgent", "!!!!!!!!!!!!!!!!!!!!!!!1selectService");
        return null;
    }

    public boolean a(int i, f fVar) {
        this.gz = fVar;
        this.gB = i;
        Log.d("DiscoveryAgent", "开始搜索设备");
        i.a aVar = i.gK;
        this.gA = i.a.gN.startDiscovery();
        l.getActivity().registerReceiver(this.gx, new IntentFilter("android.bluetooth.device.action.FOUND"));
        l.getActivity().registerReceiver(this.gy, new IntentFilter("android.bluetooth.adapter.action.DISCOVERY_FINISHED"));
        return this.gA;
    }

    public boolean a(f fVar) {
        fVar.L(2);
        i.a aVar = i.gK;
        return i.a.gN.cancelDiscovery();
    }
}
