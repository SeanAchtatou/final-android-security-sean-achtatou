package com.a.a.b;

import android.util.Log;
import java.util.UUID;

public class o {
    protected static UUID gw = null;
    public long hl;
    public String hm;
    public boolean hn;

    public static class a extends o {
        public a(long j) {
            super(j);
        }

        public a(String str, boolean z) {
            super(str, z);
        }
    }

    public o(long j) {
        this.hl = j;
        if (gw == null) {
            gw = UUID.fromString("11111111-2222-3333-4444-555555555555");
        }
    }

    public o(String str, boolean z) {
        this.hm = toString(str);
        gw = UUID.fromString("11111111-2222-3333-4444-555555555555");
        this.hn = z;
        Log.e("UUID", "Create UUID = " + gw.toString());
    }

    private String toString(String str) {
        String[] strArr = new String[5];
        strArr[0] = "";
        for (int i = 1; i < strArr.length; i++) {
            strArr[i] = "-";
        }
        for (int i2 = 0; i2 < 8; i2++) {
            strArr[0] = strArr[0] + str.charAt(i2);
        }
        for (int i3 = 8; i3 < 12; i3++) {
            strArr[1] = strArr[1] + str.charAt(i3);
        }
        for (int i4 = 12; i4 < 16; i4++) {
            strArr[2] = strArr[2] + str.charAt(i4);
        }
        for (int i5 = 16; i5 < 20; i5++) {
            strArr[3] = strArr[3] + str.charAt(i5);
        }
        for (int i6 = 20; i6 < 32; i6++) {
            strArr[4] = strArr[4] + str.charAt(i6);
        }
        return strArr[0] + strArr[1] + strArr[2] + strArr[3] + strArr[4];
    }

    public boolean equals(Object obj) {
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        return gw.toString();
    }
}
