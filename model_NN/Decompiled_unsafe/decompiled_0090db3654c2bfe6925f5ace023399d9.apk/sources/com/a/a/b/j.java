package com.a.a.b;

import android.bluetooth.BluetoothSocket;
import android.util.Log;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

public class j implements g {
    byte[] bA;
    protected c gS;
    protected d gT;
    protected b gU;
    protected a gV;
    public BluetoothSocket gW;
    protected boolean gX;
    protected boolean gY = false;
    boolean gZ;
    Vector<byte[]> ha = new Vector<>();
    boolean hb = false;

    public static class a extends DataInputStream {
        public a(InputStream inputStream) {
            super(inputStream);
        }

        public void close() {
        }
    }

    public static class b extends DataOutputStream {
        public b(OutputStream outputStream) {
            super(outputStream);
        }

        public void close() {
        }
    }

    public static class c extends InputStream {
        InputStream hc;

        public c(InputStream inputStream) {
            this.hc = inputStream;
        }

        public void close() {
        }

        public int read() {
            return this.hc.read();
        }
    }

    public static class d extends OutputStream {
        OutputStream hd;

        public d(OutputStream outputStream) {
            this.hd = outputStream;
        }

        public void close() {
        }

        public void write(int i) {
            this.hd.write(i);
        }
    }

    public j(BluetoothSocket bluetoothSocket, boolean z) {
        this.gW = bluetoothSocket;
        this.gX = z;
        Log.d("MyL2CAPConnection", "socket Create Completed  is server = " + z);
    }

    public int Z() {
        Log.d("MyL2CAPConnection", "getTransmitMTU");
        return 48;
    }

    public int aa() {
        Log.d("MyL2CAPConnection", "getReceiveMTU");
        return 48;
    }

    public DataInputStream ak() {
        Log.d("MyL2CAPConnection", "openDataInputStream   " + this.gX);
        if (this.gV == null) {
            this.gV = new a(this.gW.getInputStream());
        }
        return this.gV;
    }

    public InputStream al() {
        Log.d("MyL2CAPConnection", "openInputStream   " + this.gX);
        if (this.gS == null) {
            this.gS = new c(this.gW.getInputStream());
        }
        return this.gS;
    }

    public DataOutputStream am() {
        if (this.gU == null) {
            this.gU = new b(this.gW.getOutputStream());
        }
        Log.d("MyL2CAPConnection", "openDataOutputStream   " + this.gX);
        return this.gU;
    }

    public OutputStream an() {
        Log.d("MyL2CAPConnection", "openOutputStream   " + this.gX);
        if (this.gT == null) {
            this.gT = new d(this.gW.getOutputStream());
        }
        return this.gT;
    }

    public void close() {
        Log.d("MyL2CAP", "close");
    }

    public void p(byte[] bArr) {
        if (this.gT == null) {
            this.gT = new d(this.gW.getOutputStream());
        }
        this.gT.write(bArr.length);
        this.gT.write(bArr);
        this.gT.flush();
    }

    public int q(byte[] bArr) {
        if (this.gS == null) {
            this.gS = new c(this.gW.getInputStream());
        }
        int read = this.gS.read();
        if (read == 0) {
            this.bA = new byte[256];
        } else {
            this.bA = new byte[read];
        }
        int read2 = this.gS.read(this.bA) + 0;
        if (this.bA.length > bArr.length) {
            System.arraycopy(this.bA, 0, bArr, 0, bArr.length);
        } else {
            System.arraycopy(this.bA, 0, bArr, 0, this.bA.length);
        }
        return read;
    }

    public boolean ready() {
        return true;
    }
}
