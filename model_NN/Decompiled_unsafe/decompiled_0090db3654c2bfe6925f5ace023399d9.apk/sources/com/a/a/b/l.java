package com.a.a.b;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.util.Log;
import com.a.a.c.b;
import org.meteoroid.core.c;
import org.meteoroid.plugin.device.MIDPDevice;

public class l {
    protected BluetoothDevice he;
    protected boolean hk;

    protected l(BluetoothDevice bluetoothDevice) {
        this.he = bluetoothDevice;
        Log.d("RemoteDevice", "新建远程设备");
    }

    protected l(String str) {
        Log.d("RemoteDevice", "address");
        this.he = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(str);
    }

    public static l b(b bVar) {
        Log.d("RemoteDevice", "getRemoteDevice");
        if (((MIDPDevice) c.ou).qq != null) {
            return new l(((MIDPDevice) c.ou).qq.he);
        }
        if (((MIDPDevice) c.ou).qs != null) {
            return new l(((MIDPDevice) c.ou).qs.he);
        }
        Log.d("RemoteDevice", "getRemoteDevice    device  null");
        return null;
    }

    public boolean a(b bVar, boolean z) {
        Log.d("RemoteDevice", "encrypt");
        return false;
    }

    public final String ah() {
        Log.d("RemoteDevice", "getBluetoothAddress");
        return this.he.getAddress();
    }

    public boolean aq() {
        Log.d("RemoteDevice", "isTrustedDevice");
        return false;
    }

    public boolean ar() {
        Log.d("RemoteDevice", "authenticate");
        return false;
    }

    public boolean at() {
        Log.d("RemoteDevice", "isAuthenticated");
        return false;
    }

    public boolean au() {
        Log.d("RemoteDevice", "isEncrypted");
        return false;
    }

    public String c(boolean z) {
        Log.d("RemoteDevice", "getFriendlyName" + this.he.getName());
        return this.he.getName();
    }

    public boolean c(b bVar) {
        Log.d("RemoteDevice", "authorize");
        return false;
    }

    public boolean d(b bVar) {
        Log.d("RemoteDevice", "isAuthorized");
        return false;
    }

    public boolean equals(Object obj) {
        Log.d("RemoteDevice", "equals");
        return false;
    }

    /* access modifiers changed from: protected */
    public int getBondState() {
        return this.he.getBondState();
    }

    public int hashCode() {
        Log.d("RemoteDevice", "hashCode");
        return 0;
    }
}
