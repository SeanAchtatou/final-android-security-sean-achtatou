package com.a.a.b;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

public class c {
    public static final int BOOL = 40;
    public static final int DATALT = 56;
    public static final int DATSEQ = 48;
    public static final int INT_1 = 16;
    public static final int INT_16 = 20;
    public static final int INT_2 = 17;
    public static final int INT_4 = 18;
    public static final int INT_8 = 19;
    public static final int NULL = 0;
    public static final int STRING = 32;
    public static final int URL = 64;
    public static final int UUID = 24;
    public static final int U_INT_1 = 8;
    public static final int U_INT_16 = 12;
    public static final int U_INT_2 = 9;
    public static final int U_INT_4 = 10;
    public static final int U_INT_8 = 11;
    private int gm;
    private long gn;
    private boolean go;
    BluetoothDevice gp;
    private String gq = "";
    private int size;
    private Object value;

    public c(int i) {
        Log.d("DataElement", "Value Type = " + i);
    }

    public c(int i, long j) {
        Log.d("DataElement", "!!!!!valueType = " + i + "    value = " + j);
    }

    protected c(int i, BluetoothDevice bluetoothDevice) {
        this.gm = i;
        this.gp = bluetoothDevice;
    }

    public c(int i, Object obj) {
        Log.d("DataElement", "valueType = " + i + "    value = " + obj);
    }

    protected c(String str, BluetoothDevice bluetoothDevice) {
        this.gq = str;
        this.gp = bluetoothDevice;
    }

    public c(boolean z) {
        Log.d("DataElement", "bool = " + z);
    }

    public boolean W() {
        Log.d("DataElement", "getBoolean");
        return this.go;
    }

    public void a(c cVar) {
        Log.d("DataElement", "addElement = ");
    }

    public void a(c cVar, int i) {
        Log.d("DataElement", "insertElementAt");
    }

    public boolean b(c cVar) {
        Log.d("DataElement", "removeElement");
        return false;
    }

    public int getDataType() {
        Log.d("DataElement", "getDataType   32");
        return 32;
    }

    public long getLong() {
        Log.d("DataElement", "getLong");
        return this.gn;
    }

    public int getSize() {
        Log.d("DataElement", "getSize");
        return this.size;
    }

    public Object getValue() {
        if (this.gq != "") {
            Log.d("DataElement", "getValue  256  = " + this.gq);
            return this.gq;
        }
        Log.d("DataElement", "getValue = bd = " + this.gp + "  name = " + this.gp.getName());
        return this.gp.getName();
    }
}
