package com.a.a.b;

import android.bluetooth.BluetoothDevice;
import android.util.Log;
import java.util.Hashtable;

public class k implements m {
    BluetoothDevice he;
    String hf;
    c hg;
    Hashtable<Integer, c> hh = new Hashtable<>();
    public int hi = 0;
    public c hj;

    public k(BluetoothDevice bluetoothDevice, String str) {
        Log.d("MyServiceRecord", "new MyServiceRecord");
        this.he = bluetoothDevice;
        this.hf = str;
    }

    public k(String str) {
        this.hf = str;
        Log.d("MyServiceRecord", "new MyServiceRecord 2");
    }

    public c N(int i) {
        Log.d("MyServiceRecord", "getAttributeValue ID = " + i);
        if (i == this.hi) {
            return this.hj;
        }
        if (i != 256) {
            return new c(32, this.he);
        }
        if (this.hg != null) {
            return this.hg;
        }
        Log.d("MyServiceRecord", "getAttributeValue ID = 256  " + this.he.getName());
        return new c(this.he.getName(), this.he);
    }

    public void O(int i) {
        Log.d("MyServiceRecord", "setDeviceServiceClasses");
    }

    public boolean a(int i, c cVar) {
        Log.d("MyServiceRecord", "setAttributeValue");
        this.hi = i;
        this.hj = cVar;
        return true;
    }

    public int[] ao() {
        Log.d("MyServiceRecord", "getAttributeIDs");
        return new int[]{this.hi};
    }

    public l ap() {
        Log.d("MyServiceRecord", "getHostDevice");
        return new l(this.he);
    }

    public boolean c(int[] iArr) {
        Log.d("MyServiceRecord", "populateRecord");
        return false;
    }

    public String d(int i, boolean z) {
        Log.d("MyServiceRecord", this.hf);
        String str = this.hf.startsWith("btspp://") ? "btspp://" : this.hf.startsWith("btl2cap://") ? "btl2cap://" : "btl2cap://";
        if (this.he != null) {
            Log.d("MyServiceRecord", "getConnectionURL=" + str + "0000000AAAAA:1master=false;encrypt=false;authenticate=false" + "MACadd" + this.he.getAddress());
            return str + "0000000AAAAA:1master=false;encrypt=false;authenticate=false" + "MACadd" + this.he.getAddress();
        }
        Log.d("MyServiceRecord", "Device Null");
        return str + "0000000AAAAA:1master=false;encrypt=false;authenticate=false";
    }
}
