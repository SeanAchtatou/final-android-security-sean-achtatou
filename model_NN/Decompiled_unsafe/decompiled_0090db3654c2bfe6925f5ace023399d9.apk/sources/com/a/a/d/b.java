package com.a.a.d;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

public interface b extends com.a.a.c.b {
    long aF();

    long aG();

    long aH();

    long aI();

    Enumeration<String> aJ();

    void aK();

    DataInputStream ak();

    InputStream al();

    DataOutputStream am();

    OutputStream an();

    OutputStream b(long j);

    Enumeration<String> b(String str, boolean z);

    boolean canRead();

    boolean canWrite();

    void create();

    void delete();

    long e(boolean z);

    boolean exists();

    void f(boolean z);

    void g(boolean z);

    String getName();

    String getPath();

    String getURL();

    void h(boolean z);

    boolean isDirectory();

    boolean isHidden();

    boolean isOpen();

    long lastModified();

    void truncate(long j);

    void y(String str);

    void z(String str);
}
