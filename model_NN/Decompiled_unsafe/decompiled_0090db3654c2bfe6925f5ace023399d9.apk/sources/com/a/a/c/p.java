package com.a.a.c;

public interface p extends q {
    public static final byte DELAY = 0;
    public static final byte KEEPALIVE = 2;
    public static final byte LINGER = 1;
    public static final byte RCVBUF = 3;
    public static final byte SNDBUF = 4;

    void a(byte b, int i);

    String aD();

    String getAddress();

    int getLocalPort();

    int getPort();

    int h(byte b);
}
