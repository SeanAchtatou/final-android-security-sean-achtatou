package com.a.a.c;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.meteoroid.core.c;
import org.meteoroid.plugin.device.MIDPDevice;

public class d {
    public static final int READ = 1;
    public static final int READ_WRITE = 3;
    public static final int WRITE = 2;

    public static b a(String str, int i, boolean z) {
        return ((MIDPDevice) c.ou).a(str, i, z);
    }

    public static b c(String str, int i) {
        return a(str, i, true);
    }

    public static b q(String str) {
        return a(str, 3, true);
    }

    public static DataInputStream r(String str) {
        return ((j) q(str)).ak();
    }

    public static DataOutputStream s(String str) {
        return ((k) q(str)).am();
    }

    public static InputStream t(String str) {
        return ((j) q(str)).al();
    }

    public static OutputStream u(String str) {
        return ((k) q(str)).an();
    }
}
