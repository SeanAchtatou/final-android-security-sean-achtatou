package com.a.a.q;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class e {
    private byte[] mZ;
    private String mimeType;
    private String na;
    private String nb;
    private String nc;

    public e(InputStream inputStream, String str, String str2, String str3, String str4) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read();
            if (read != -1) {
                byteArrayOutputStream.write(read);
            } else {
                byteArrayOutputStream.flush();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                g(byteArray, 0, byteArray.length);
                this.na = str2;
                this.mimeType = str;
                this.nb = str3;
                this.nc = str4;
                return;
            }
        }
    }

    public e(byte[] bArr, int i, int i2, String str, String str2, String str3, String str4) {
        g(bArr, i, i2);
        this.na = str2;
        this.mimeType = str;
        this.nb = str3;
        this.nc = str4;
    }

    public e(byte[] bArr, String str, String str2, String str3, String str4) {
        this(bArr, 0, bArr.length, str, str2, str3, str4);
    }

    private void g(byte[] bArr, int i, int i2) {
        this.mZ = new byte[i2];
        System.arraycopy(this.mZ, i, this.mZ, 0, i2);
    }

    public byte[] fg() {
        return this.mZ;
    }

    public InputStream fh() {
        return new ByteArrayInputStream(this.mZ);
    }

    public String fi() {
        return this.na;
    }

    public String fj() {
        return this.nb;
    }

    public String fk() {
        return this.mimeType;
    }

    public String getEncoding() {
        return this.nc;
    }

    public int getLength() {
        return this.mZ.length;
    }
}
