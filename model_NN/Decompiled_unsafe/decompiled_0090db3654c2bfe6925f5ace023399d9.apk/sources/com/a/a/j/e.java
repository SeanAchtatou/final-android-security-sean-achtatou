package com.a.a.j;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.Contacts;
import com.umeng.common.a;

public class e {
    public static f a(ContentResolver contentResolver, g gVar, Cursor cursor) {
        int i = cursor.getInt(cursor.getColumnIndex("_id"));
        f fVar = new f((long) i, gVar);
        a(cursor, fVar);
        b(contentResolver, i, fVar);
        b(cursor, fVar);
        a(contentResolver, i, fVar);
        c(cursor, fVar);
        fVar.r(false);
        return fVar;
    }

    private static void a(ContentResolver contentResolver, int i, f fVar) {
        ContentResolver contentResolver2 = contentResolver;
        Cursor query = contentResolver2.query(Contacts.Phones.CONTENT_URI, null, "person == " + i, null, null);
        int columnIndex = query.getColumnIndex("number");
        int columnIndex2 = query.getColumnIndex(a.b);
        while (query.moveToNext()) {
            Integer bg = bg(query.getInt(columnIndex2));
            fVar.b(com.a.a.i.a.TEL, bg.intValue(), query.getString(columnIndex));
        }
        query.close();
    }

    private static void a(Cursor cursor, f fVar) {
        String string = cursor.getString(cursor.getColumnIndex("name"));
        String[] strArr = new String[g.kU.ld];
        strArr[2] = string;
        fVar.a(106, 0, strArr);
    }

    private static void b(ContentResolver contentResolver, int i, f fVar) {
        ContentResolver contentResolver2 = contentResolver;
        Cursor query = contentResolver2.query(Contacts.ContactMethods.CONTENT_URI, null, "person == " + i + " AND " + "kind" + " == " + 2, null, null);
        int columnIndex = query.getColumnIndex("data");
        int columnIndex2 = query.getColumnIndex(a.b);
        while (query.moveToNext()) {
            Integer bf = bf(query.getInt(columnIndex2));
            String string = query.getString(columnIndex);
            String[] strArr = new String[g.kT.ld];
            strArr[1] = string;
            fVar.a(100, bf.intValue(), strArr);
        }
        query.close();
    }

    private static void b(Cursor cursor, f fVar) {
        String string = cursor.getString(cursor.getColumnIndex("display_name"));
        if (string != null) {
            fVar.b(105, 0, string);
        }
    }

    private static Integer bf(int i) {
        switch (i) {
            case 0:
                return new Integer(32);
            case 1:
                return new Integer(8);
            case 2:
                return new Integer(512);
            case 3:
                return new Integer(32);
            default:
                return new Integer(0);
        }
    }

    private static Integer bg(int i) {
        switch (i) {
            case 1:
                return new Integer(8);
            case 2:
                return new Integer(16);
            case 3:
                return new Integer(512);
            case 4:
                return new Integer(516);
            case 5:
                return new Integer(12);
            case 6:
                return new Integer(64);
            case 7:
                return new Integer(32);
            default:
                return new Integer(0);
        }
    }

    private static void c(Cursor cursor, f fVar) {
        int columnIndex = cursor.getColumnIndex("notes");
        while (cursor.moveToNext()) {
            String string = cursor.getString(columnIndex);
            if (string != null) {
                fVar.b(108, 0, string);
            }
        }
    }
}
