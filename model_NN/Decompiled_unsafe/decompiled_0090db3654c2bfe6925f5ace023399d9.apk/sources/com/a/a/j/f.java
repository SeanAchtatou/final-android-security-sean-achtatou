package com.a.a.j;

import com.a.a.i.a;
import com.a.a.i.j;
import com.a.a.i.n;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class f implements a {
    private final long id;
    private final g kQ;
    private final ArrayList<h> kR;
    private boolean kS;

    f(long j, g gVar) {
        this.id = j;
        this.kQ = gVar;
        this.kR = new ArrayList<>();
    }

    f(g gVar) {
        this(-1, gVar);
    }

    private h N(int i, int i2) {
        i bc = this.kQ.bc(i);
        if (bc == null) {
            throw new n("The field with id '" + i + "' is not supported.");
        } else if (bc.type != i2) {
            throw new IllegalArgumentException("The field with metadata '" + bc + "' is not of type '" + i2 + "'.");
        } else {
            Iterator<h> it = this.kR.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (next.la.equals(bc)) {
                    return next;
                }
            }
            h hVar = new h(bc);
            this.kR.add(hVar);
            return hVar;
        }
    }

    private h bh(int i) {
        i bc = this.kQ.bc(i);
        if (bc == null) {
            throw new n("The field with id '" + i + "' is not supported.");
        }
        Iterator<h> it = this.kR.iterator();
        while (it.hasNext()) {
            h next = it.next();
            if (next.la.equals(bc)) {
                return next;
            }
        }
        h hVar = new h(bc);
        this.kR.add(hVar);
        return hVar;
    }

    public byte[] E(int i, int i2) {
        h N = N(i, 0);
        if (i2 < N.lb.size()) {
            return (byte[]) N.lb.get(i2);
        }
        throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index < numberOfValues'.");
    }

    public long F(int i, int i2) {
        h N = N(i, 2);
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index >= 0'");
        } else if (i2 < N.lb.size()) {
            return ((Date) N.lb.get(i2)).getTime();
        } else {
            throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index < numberOfValues'.");
        }
    }

    public boolean G(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index >= 0'");
        }
        h N = N(i, 0);
        if (i2 < N.lb.size()) {
            return ((Boolean) N.lb.get(i2)).booleanValue();
        }
        throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index < numberOfValues'.");
    }

    public String[] H(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The parameter 'index' violates contraint 'index >= 0'");
        }
        h N = N(i, 5);
        if (i2 < N.lb.size()) {
            return (String[]) N.lb.get(i2);
        }
        throw new IndexOutOfBoundsException("The parameter 'index' violates contraint 'index < numberOfValuesInField'");
    }

    public void I(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        }
        h bh = bh(i);
        if (i2 >= bh.lb.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        }
        bh.lb.remove(i2);
        bh.lc.remove(i2);
    }

    public int J(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The index '" + i2 + "' must not be < 0.");
        }
        h bh = bh(i);
        int size = bh.lc.size() - 1;
        if (i2 <= size) {
            return bh.lc.get(i2).intValue();
        }
        throw new IndexOutOfBoundsException("The index '" + i2 + "' is larger then the last valid index of '" + size + "'");
    }

    public void W(String str) {
        throw new UnsupportedOperationException();
    }

    public void X(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
    }

    public void a(int i, int i2, int i3, long j) {
        h N = N(i, 2);
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        } else if (i2 >= N.lb.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        } else {
            N.lb.set(i2, new Date(j));
            N.lc.set(i2, new Integer(i3));
            this.kS = true;
        }
    }

    public void a(int i, int i2, int i3, String str) {
        h N = N(i, 4);
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        } else if (i2 >= N.lb.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        } else {
            N.lb.set(i2, str);
            N.lc.set(i2, new Integer(i3));
            this.kS = true;
        }
    }

    public void a(int i, int i2, int i3, boolean z) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        }
        h N = N(i, 1);
        if (i2 >= N.lb.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        }
        N.lb.set(i2, new Boolean(z));
        N.lc.set(i2, new Integer(i3));
        this.kS = true;
    }

    public void a(int i, int i2, int i3, byte[] bArr, int i4, int i5) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        }
        h N = N(i, 0);
        int size = N.lb.size();
        if (i2 >= size) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        } else if (bArr == null) {
            throw new NullPointerException("Parameter 'value' must not be null.");
        } else if (bArr.length == 0) {
            throw new IllegalArgumentException("Array value of parameter 'value' must not have zero elements.");
        } else if (i4 < 0) {
            throw new IllegalArgumentException("Parameter 'offset' must not have a negative value.");
        } else if (i4 >= size) {
            throw new IllegalArgumentException("Parameter 'offset' must not be larger then number of values which is '" + size + "'");
        } else if (i5 <= 0) {
            throw new IllegalArgumentException("Parameter 'length' must not have a negative value.");
        } else if (i5 > size) {
            throw new IllegalArgumentException("Parameter 'length' must not have a value which exceeds the number of values in the field which is '" + size + "'");
        } else if (i4 + i5 > size) {
            throw new IllegalArgumentException("The sum of 'offset' and 'length' must not exceed the number of values whish is '" + size + "'");
        } else {
            byte[] bArr2 = new byte[i5];
            System.arraycopy(bArr, i4, bArr2, 0, i5);
            N.lb.set(i2, bArr2);
            N.lc.set(i2, new Integer(i3));
            this.kS = true;
        }
    }

    public void a(int i, int i2, int i3, String[] strArr) {
        h N = N(i, 5);
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        } else if (i2 >= N.lb.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        } else {
            N.lb.set(i2, strArr);
            N.lc.set(i2, new Integer(i3));
            this.kS = true;
        }
    }

    public void a(int i, int i2, long j) {
        h N = N(i, 2);
        N.lb.add(new Date(j));
        N.lc.add(new Integer(i2));
        this.kS = true;
    }

    public void a(int i, int i2, boolean z) {
        h N = N(i, 1);
        N.lb.add(new Boolean(z));
        N.lc.add(new Integer(i2));
        this.kS = true;
    }

    public void a(int i, int i2, byte[] bArr, int i3, int i4) {
        if (bArr == null) {
            throw new NullPointerException("The parameter value is null.");
        } else if (i4 < 1) {
            throw new IllegalArgumentException("The parameter 'length' violates contraint 'length > 0'");
        } else if (i4 > bArr.length) {
            throw new IllegalArgumentException("The parameter 'length' violates contraint 'length <= value.length'");
        } else if (i3 < 0) {
            throw new IllegalArgumentException("The parameter 'offset' violates contraint 'offset > 0'");
        } else if (i3 >= bArr.length) {
            throw new IllegalArgumentException("The parameter 'offset' violates contraint 'offset < value.length'");
        } else {
            h N = N(i, 0);
            byte[] bArr2 = new byte[i4];
            System.arraycopy(bArr, i3, bArr2, 0, i4);
            N.lb.add(bArr2);
            N.lc.add(new Integer(i2));
            this.kS = true;
        }
    }

    public void a(int i, int i2, String[] strArr) {
        if (strArr == null) {
            throw new NullPointerException("Parameter 'value' must not be null.");
        }
        h N = N(i, 5);
        N.lb.add(strArr);
        N.lc.add(new Integer(i2));
        this.kS = true;
    }

    public int aO(int i) {
        return this.kQ.bc(i).lf;
    }

    public int aS(int i) {
        return bh(i).lb.size();
    }

    public void b(int i, int i2, String str) {
        if (str == null) {
            throw new NullPointerException("Parameter 'value' must not be null.");
        }
        h N = N(i, 4);
        N.lb.add(str);
        N.lc.add(new Integer(i2));
        this.kS = true;
    }

    public void commit() {
        if (this.kS) {
            this.kQ.a(this);
        }
    }

    public j dI() {
        return this.kQ;
    }

    public boolean dJ() {
        return this.kS;
    }

    public int[] dK() {
        int i;
        int[] iArr = new int[this.kR.size()];
        int i2 = 0;
        Iterator<h> it = this.kR.iterator();
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                break;
            }
            iArr[i] = it.next().la.le;
            i2 = i + 1;
        }
        int[] iArr2 = new int[i];
        while (true) {
            i--;
            if (i < 0) {
                return iArr2;
            }
            iArr2[i] = iArr[i];
        }
    }

    public String[] dL() {
        return new String[0];
    }

    public int dM() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean dU() {
        return this.id == -1;
    }

    /* access modifiers changed from: package-private */
    public long getId() {
        return this.id;
    }

    public int getInt(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index >= 0'");
        }
        h N = N(i, 3);
        if (i2 < N.lb.size()) {
            return ((Integer) N.lb.get(i2)).intValue();
        }
        throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index < numberOfValues'.");
    }

    public String getString(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index >= 0'");
        }
        h N = N(i, 4);
        if (i2 < N.lb.size()) {
            return (String) N.lb.get(i2);
        }
        throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index < numberOfValues'.");
    }

    public void j(int i, int i2, int i3, int i4) {
        h N = N(i, 3);
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        } else if (i2 >= N.lb.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        } else {
            N.lb.set(i2, new Integer(i4));
            N.lc.set(i2, new Integer(i3));
            this.kS = true;
        }
    }

    public void r(int i, int i2, int i3) {
        h N = N(i, 3);
        N.lb.add(new Integer(i3));
        N.lc.add(new Integer(i2));
        this.kS = true;
    }

    /* access modifiers changed from: package-private */
    public void r(boolean z) {
        this.kS = z;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Contact (" + this.id + ")");
        stringBuffer.append("\n");
        Iterator<h> it = this.kR.iterator();
        while (it.hasNext()) {
            stringBuffer.append(it.next());
            stringBuffer.append("\n");
        }
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }
}
