package com.a.a.j;

import java.util.ArrayList;
import java.util.Arrays;

public class h {
    protected final i la;
    protected ArrayList<Object> lb = new ArrayList<>();
    protected ArrayList<Integer> lc = new ArrayList<>();

    public h(i iVar) {
        this.la = iVar;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.la);
        stringBuffer.append("\n");
        int size = this.lb.size();
        for (int i = 0; i < size; i++) {
            Object obj = this.lb.get(i);
            if (obj instanceof Object[]) {
                obj = Arrays.toString((Object[]) obj);
            }
            stringBuffer.append("Value[" + i + "]:" + obj + ".");
            stringBuffer.append("Attr[" + i + "]:" + this.lc.get(i) + ".");
            stringBuffer.append("\n");
        }
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }
}
