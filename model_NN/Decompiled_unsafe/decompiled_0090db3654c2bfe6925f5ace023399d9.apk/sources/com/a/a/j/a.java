package com.a.a.j;

import com.a.a.i.j;
import com.a.a.i.n;

public abstract class a implements j {
    private i[] kK;
    private final int mode;
    private final String name;

    public a(String str, int i) {
        this.name = str;
        if (i == 1 || i == 2 || i == 3) {
            this.mode = i;
            return;
        }
        throw new IllegalArgumentException("The mode '" + i + "' is not supported.");
    }

    private i f(int i, boolean z) {
        for (i iVar : this.kK) {
            if (i == iVar.le) {
                return iVar;
            }
        }
        if (!z) {
            return null;
        }
        throw new n("The field with id '" + i + "' is not supported.");
    }

    public boolean K(int i, int i2) {
        i bc = bc(i);
        for (int i3 : bc.lh) {
            if (i2 == i3) {
                return true;
            }
        }
        return false;
    }

    public boolean L(int i, int i2) {
        i bc = bc(i);
        if (bc.type != 5) {
            throw new IllegalArgumentException("The field with id '" + i + "' is not of type 'PIMItem.STRING_ARRAY'.");
        }
        for (int i3 : bc.lg) {
            if (i2 == i3) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(i[] iVarArr) {
        this.kK = iVarArr;
    }

    public boolean aT(int i) {
        return f(i, false) != null;
    }

    public int[] aU(int i) {
        return bc(i).lh;
    }

    public int[] aV(int i) {
        return bc(i).lg;
    }

    public int aW(int i) {
        return bc(i).type;
    }

    public String aX(int i) {
        return bc(i).label;
    }

    public String aY(int i) {
        switch (i) {
            case 0:
                break;
            default:
                throw new IllegalArgumentException("Attribute '" + i + "' is not valid.");
        }
        return "None";
    }

    public int aZ(int i) {
        return -1;
    }

    public int ba(int i) {
        return bc(i).ld;
    }

    public i bc(int i) {
        return f(i, true);
    }

    public int[] dO() {
        int[] iArr = new int[this.kK.length];
        for (int i = 0; i < this.kK.length; i++) {
            iArr[i] = this.kK[i].le;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void dR() {
        if (this.mode == 2) {
            throw new SecurityException("The list is only writeable.");
        }
    }

    /* access modifiers changed from: protected */
    public void dS() {
        if (this.mode == 1) {
            throw new SecurityException("The list is only readable.");
        }
    }

    public String getName() {
        return this.name;
    }
}
