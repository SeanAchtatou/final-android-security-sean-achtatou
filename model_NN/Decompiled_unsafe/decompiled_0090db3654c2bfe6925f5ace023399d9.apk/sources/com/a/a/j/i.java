package com.a.a.j;

class i {
    public static final int DEFAULT_NUMBER_OF_ARRAYELEMENTS = 0;
    public static final int DEFAULT_PREFERRED_INDEX = -1;
    protected final String label;
    protected final int ld;
    protected final int le;
    protected final int lf;
    protected final int[] lg;
    protected final int[] lh;
    protected final int type;

    public i(int i, int i2, String str, int[] iArr) {
        this.le = i;
        this.type = i2;
        this.label = str;
        this.lh = iArr;
        this.ld = 0;
        this.lf = -1;
        this.lg = new int[0];
    }

    public i(int i, String str, int i2, int i3, int[] iArr, int[] iArr2) {
        this.le = i;
        this.type = 5;
        this.label = str;
        this.ld = i2;
        this.lf = i3;
        this.lg = iArr;
        this.lh = iArr2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.le == ((i) obj).le;
    }

    public int hashCode() {
        return this.le + 31;
    }

    public String toString() {
        return "FieldInfo:Id:" + this.le + ".Type:" + this.type + ".Label:" + this.label + ".ArrayElements:" + this.ld + ".";
    }
}
