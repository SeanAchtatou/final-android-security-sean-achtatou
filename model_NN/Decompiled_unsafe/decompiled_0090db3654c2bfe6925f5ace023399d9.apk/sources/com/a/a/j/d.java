package com.a.a.j;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.Contacts;
import com.a.a.i.i;
import java.util.Enumeration;
import org.meteoroid.core.l;

public class d implements Enumeration<i> {
    private final int count = this.kO.getCount();
    private ContentResolver kN = l.getActivity().getContentResolver();
    private Cursor kO = this.kN.query(Contacts.People.CONTENT_URI, null, null, null, null);
    private final c kP;
    private int position = -1;

    public d(c cVar) {
        this.kP = cVar;
    }

    /* renamed from: dT */
    public i nextElement() {
        if (this.kO.isClosed()) {
            return null;
        }
        this.position++;
        this.kO.moveToPosition(this.position);
        return this.kP.a(this.kO);
    }

    public boolean hasMoreElements() {
        return this.position < this.count + -1;
    }
}
