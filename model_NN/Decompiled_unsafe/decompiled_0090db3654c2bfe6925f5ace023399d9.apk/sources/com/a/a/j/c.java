package com.a.a.j;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import com.a.a.i.i;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.umeng.common.a;
import java.util.Enumeration;
import org.meteoroid.core.l;

public class c {
    private final g kM;
    private final ContentResolver kN = l.getActivity().getContentResolver();

    public c(g gVar) {
        this.kM = gVar;
    }

    private int bd(int i) {
        if ((i & 8) == 8) {
            return 1;
        }
        if ((i & 512) == 512) {
            return 2;
        }
        if ((i & 32) == 32) {
        }
        return 3;
    }

    private int be(int i) {
        if ((i & 16) == 16) {
            return 2;
        }
        if ((i & 4) == 4) {
            if ((i & 8) == 8) {
                return 5;
            }
            if ((i & 512) == 512) {
            }
            return 4;
        } else if ((i & 8) == 8) {
            return 1;
        } else {
            if ((i & 512) == 512) {
                return 3;
            }
            if ((i & 64) == 64) {
                return 6;
            }
            return (i & 32) == 32 ? 7 : 7;
        }
    }

    public Enumeration<i> Y(String str) {
        throw new UnsupportedOperationException();
    }

    public Enumeration<i> Z(String str) {
        throw new UnsupportedOperationException();
    }

    public f a(Cursor cursor) {
        return e.a(this.kN, this.kM, cursor);
    }

    public void a(f fVar) {
        boolean z;
        boolean z2;
        ContentValues contentValues = new ContentValues();
        this.kN.delete(Contacts.People.CONTENT_URI, null, null);
        Uri insert = this.kN.insert(Contacts.People.CONTENT_URI, contentValues);
        Contacts.People.addToMyContactsGroup(this.kN, ContentUris.parseId(insert));
        contentValues.clear();
        StringBuffer stringBuffer = new StringBuffer();
        String[] H = fVar.H(106, 0);
        if (H[3] != null) {
            stringBuffer.append(H[3]);
            z = true;
        } else {
            z = false;
        }
        if (H[1] != null) {
            if (z) {
                stringBuffer.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            }
            stringBuffer.append(H[1]);
            z = true;
        }
        if (H[0] != null) {
            if (z) {
                stringBuffer.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            }
            stringBuffer.append(H[0]);
            z = true;
        }
        if (H[4] != null) {
            if (z) {
                stringBuffer.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            }
            stringBuffer.append(H[4]);
            z = true;
        }
        if (H[2] != null) {
            if (z) {
                stringBuffer.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            }
            stringBuffer.append("(");
            stringBuffer.append(H[2]);
            stringBuffer.append(")");
        }
        contentValues.put("name", stringBuffer.toString());
        if (fVar.aS(108) > 0) {
            contentValues.put("notes", fVar.getString(108, 0));
        }
        this.kN.update(insert, contentValues, null, null);
        int aS = fVar.aS(100);
        for (int i = 0; i < aS; i++) {
            Uri withAppendedPath = Uri.withAppendedPath(insert, "contact_methods");
            contentValues.clear();
            contentValues.put("kind", new Integer(2));
            contentValues.put(a.b, new Integer(bd(fVar.J(100, i))));
            String[] H2 = fVar.H(100, i);
            StringBuffer stringBuffer2 = new StringBuffer();
            if (H2[0] != null) {
                stringBuffer2.append("PoBox ");
                stringBuffer2.append(H2[0]);
                z2 = true;
            } else {
                z2 = false;
            }
            if (H2[2] != null) {
                if (z2) {
                    stringBuffer2.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                }
                stringBuffer2.append(H2[2]);
                z2 = true;
            }
            if (H2[5] != null) {
                if (z2) {
                    stringBuffer2.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                }
                stringBuffer2.append(H2[5]);
                z2 = true;
            }
            if (H2[3] != null) {
                if (z2) {
                    stringBuffer2.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                }
                stringBuffer2.append(H2[3]);
                z2 = true;
            }
            if (H2[6] != null) {
                if (z2) {
                    stringBuffer2.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                }
                stringBuffer2.append(H2[6]);
                z2 = true;
            }
            if (H2[1] != null) {
                if (z2) {
                    stringBuffer2.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                }
                stringBuffer2.append(H2[1]);
            }
            contentValues.put("data", stringBuffer2.toString());
            this.kN.insert(withAppendedPath, contentValues);
        }
        int aS2 = fVar.aS(103);
        for (int i2 = 0; i2 < aS2; i2++) {
            Uri withAppendedPath2 = Uri.withAppendedPath(insert, "contact_methods");
            contentValues.clear();
            StringBuffer stringBuffer3 = new StringBuffer();
            stringBuffer3.append(fVar.getString(103, i2));
            contentValues.put("data", stringBuffer3.toString());
            contentValues.put("kind", new Integer(1));
            contentValues.put(a.b, new Integer(bd(fVar.J(103, i2))));
            this.kN.insert(withAppendedPath2, contentValues);
        }
        int aS3 = fVar.aS(com.a.a.i.a.TEL);
        for (int i3 = 0; i3 < aS3; i3++) {
            Uri withAppendedPath3 = Uri.withAppendedPath(insert, "phones");
            contentValues.clear();
            contentValues.put("number", fVar.getString(com.a.a.i.a.TEL, i3));
            contentValues.put(a.b, new Integer(be(fVar.J(com.a.a.i.a.TEL, i3))));
            this.kN.insert(withAppendedPath3, contentValues);
        }
    }

    public void b(f fVar) {
        this.kN.delete(ContentUris.withAppendedId(Contacts.People.CONTENT_URI, fVar.getId()), null, null);
    }

    public com.a.a.i.a c(f fVar) {
        throw new UnsupportedOperationException();
    }

    public Enumeration<i> d(f fVar) {
        throw new UnsupportedOperationException();
    }

    public Enumeration<i> dN() {
        return new d(this);
    }
}
