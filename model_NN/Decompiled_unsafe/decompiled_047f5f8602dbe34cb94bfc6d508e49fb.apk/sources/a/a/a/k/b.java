package a.a.a.k;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Deprecated
public class b extends a implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final Map f180a = new ConcurrentHashMap();

    public e a(String str, Object obj) {
        if (str != null) {
            if (obj != null) {
                this.f180a.put(str, obj);
            } else {
                this.f180a.remove(str);
            }
        }
        return this;
    }

    public Object a(String str) {
        return this.f180a.get(str);
    }

    public void a(e eVar) {
        for (Map.Entry entry : this.f180a.entrySet()) {
            eVar.a((String) entry.getKey(), entry.getValue());
        }
    }

    public Object clone() {
        b bVar = (b) super.clone();
        a(bVar);
        return bVar;
    }
}
