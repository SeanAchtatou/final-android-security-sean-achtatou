package a.a.a.m;

import a.a.a.ab;
import a.a.a.ac;
import a.a.a.k;
import a.a.a.l;
import a.a.a.n.a;
import a.a.a.q;
import a.a.a.r;
import a.a.a.v;

public class j implements r {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f189a;

    public j() {
        this(false);
    }

    public j(boolean z) {
        this.f189a = z;
    }

    public void a(q qVar, e eVar) {
        a.a(qVar, "HTTP request");
        if (qVar instanceof l) {
            if (this.f189a) {
                qVar.d("Transfer-Encoding");
                qVar.d("Content-Length");
            } else if (qVar.a("Transfer-Encoding")) {
                throw new ab("Transfer-encoding header already present");
            } else if (qVar.a("Content-Length")) {
                throw new ab("Content-Length header already present");
            }
            ac b = qVar.g().b();
            k b2 = ((l) qVar).b();
            if (b2 == null) {
                qVar.a("Content-Length", "0");
                return;
            }
            if (!b2.b() && b2.c() >= 0) {
                qVar.a("Content-Length", Long.toString(b2.c()));
            } else if (b.c(v.b)) {
                throw new ab("Chunked transfer encoding not allowed for " + b);
            } else {
                qVar.a("Transfer-Encoding", "chunked");
            }
            if (b2.d() != null && !qVar.a("Content-Type")) {
                qVar.a(b2.d());
            }
            if (b2.e() != null && !qVar.a("Content-Encoding")) {
                qVar.a(b2.e());
            }
        }
    }
}
