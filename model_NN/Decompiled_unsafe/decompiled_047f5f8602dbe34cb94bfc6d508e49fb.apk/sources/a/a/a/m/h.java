package a.a.a.m;

import a.a.a.i;
import a.a.a.m;
import a.a.a.n.a;
import a.a.a.q;
import a.a.a.s;
import java.io.IOException;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private final int f187a;

    public h() {
        this(3000);
    }

    public h(int i) {
        this.f187a = a.a(i, "Wait for continue time");
    }

    private static void a(i iVar) {
        try {
            iVar.close();
        } catch (IOException e) {
        }
    }

    public s a(q qVar, i iVar, e eVar) {
        a.a(qVar, "HTTP request");
        a.a(iVar, "Client connection");
        a.a(eVar, "HTTP context");
        try {
            s b = b(qVar, iVar, eVar);
            return b == null ? c(qVar, iVar, eVar) : b;
        } catch (IOException e) {
            a(iVar);
            throw e;
        } catch (m e2) {
            a(iVar);
            throw e2;
        } catch (RuntimeException e3) {
            a(iVar);
            throw e3;
        }
    }

    public void a(q qVar, g gVar, e eVar) {
        a.a(qVar, "HTTP request");
        a.a(gVar, "HTTP processor");
        a.a(eVar, "HTTP context");
        eVar.a("http.request", qVar);
        gVar.a(qVar, eVar);
    }

    public void a(s sVar, g gVar, e eVar) {
        a.a(sVar, "HTTP response");
        a.a(gVar, "HTTP processor");
        a.a(eVar, "HTTP context");
        eVar.a("http.response", sVar);
        gVar.a(sVar, eVar);
    }

    /* access modifiers changed from: protected */
    public boolean a(q qVar, s sVar) {
        int b;
        return ("HEAD".equalsIgnoreCase(qVar.g().a()) || (b = sVar.a().b()) < 200 || b == 204 || b == 304 || b == 205) ? false : true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0086  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a.a.a.s b(a.a.a.q r6, a.a.a.i r7, a.a.a.m.e r8) {
        /*
            r5 = this;
            r1 = 0
            java.lang.String r0 = "HTTP request"
            a.a.a.n.a.a(r6, r0)
            java.lang.String r0 = "Client connection"
            a.a.a.n.a.a(r7, r0)
            java.lang.String r0 = "HTTP context"
            a.a.a.n.a.a(r8, r0)
            java.lang.String r0 = "http.connection"
            r8.a(r0, r7)
            java.lang.String r0 = "http.request_sent"
            java.lang.Boolean r2 = java.lang.Boolean.FALSE
            r8.a(r0, r2)
            r7.a(r6)
            boolean r0 = r6 instanceof a.a.a.l
            if (r0 == 0) goto L_0x009d
            r2 = 1
            a.a.a.ae r0 = r6.g()
            a.a.a.ac r3 = r0.b()
            r0 = r6
            a.a.a.l r0 = (a.a.a.l) r0
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x009a
            a.a.a.v r0 = a.a.a.v.b
            boolean r0 = r3.c(r0)
            if (r0 != 0) goto L_0x009a
            r7.b()
            int r0 = r5.f187a
            boolean r0 = r7.a(r0)
            if (r0 == 0) goto L_0x009a
            a.a.a.s r3 = r7.a()
            boolean r0 = r5.a(r6, r3)
            if (r0 == 0) goto L_0x0055
            r7.a(r3)
        L_0x0055:
            a.a.a.af r0 = r3.a()
            int r0 = r0.b()
            r4 = 200(0xc8, float:2.8E-43)
            if (r0 >= r4) goto L_0x0096
            r4 = 100
            if (r0 == r4) goto L_0x0082
            a.a.a.ab r0 = new a.a.a.ab
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unexpected response: "
            java.lang.StringBuilder r1 = r1.append(r2)
            a.a.a.af r2 = r3.a()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0082:
            r0 = r1
            r1 = r2
        L_0x0084:
            if (r1 == 0) goto L_0x008b
            a.a.a.l r6 = (a.a.a.l) r6
            r7.a(r6)
        L_0x008b:
            r7.b()
            java.lang.String r1 = "http.request_sent"
            java.lang.Boolean r2 = java.lang.Boolean.TRUE
            r8.a(r1, r2)
            return r0
        L_0x0096:
            r0 = 0
            r1 = r0
            r0 = r3
            goto L_0x0084
        L_0x009a:
            r0 = r1
            r1 = r2
            goto L_0x0084
        L_0x009d:
            r0 = r1
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.m.h.b(a.a.a.q, a.a.a.i, a.a.a.m.e):a.a.a.s");
    }

    /* access modifiers changed from: protected */
    public s c(q qVar, i iVar, e eVar) {
        a.a(qVar, "HTTP request");
        a.a(iVar, "Client connection");
        a.a(eVar, "HTTP context");
        s sVar = null;
        int i = 0;
        while (true) {
            if (sVar != null && i >= 200) {
                return sVar;
            }
            sVar = iVar.a();
            if (a(qVar, sVar)) {
                iVar.a(sVar);
            }
            i = sVar.a().b();
        }
    }
}
