package a.a.a.m;

import a.a.a.n.a;

@Deprecated
public final class c implements e {

    /* renamed from: a  reason: collision with root package name */
    private final e f184a;
    private final e b;

    public c(e eVar, e eVar2) {
        this.f184a = (e) a.a(eVar, "HTTP context");
        this.b = eVar2;
    }

    public Object a(String str) {
        Object a2 = this.f184a.a(str);
        return a2 == null ? this.b.a(str) : a2;
    }

    public void a(String str, Object obj) {
        this.f184a.a(str, obj);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[local: ").append(this.f184a);
        sb.append("defaults: ").append(this.b);
        sb.append("]");
        return sb.toString();
    }
}
