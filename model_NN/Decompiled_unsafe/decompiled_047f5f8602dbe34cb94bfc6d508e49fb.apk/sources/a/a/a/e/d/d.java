package a.a.a.e.d;

import a.a.a.e.e.a;
import a.a.a.e.e.b;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import javax.naming.InvalidNameException;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import org.apache.commons.logging.Log;

public final class d implements HostnameVerifier {

    /* renamed from: a  reason: collision with root package name */
    private final Log f42a;
    private final b b;

    static String a(String str) {
        if (str == null) {
            return null;
        }
        try {
            List rdns = new LdapName(str).getRdns();
            for (int size = rdns.size() - 1; size >= 0; size--) {
                Attribute attribute = ((Rdn) rdns.get(size)).toAttributes().get("cn");
                if (attribute != null) {
                    try {
                        Object obj = attribute.get();
                        if (obj != null) {
                            return obj.toString();
                        }
                    } catch (NoSuchElementException | NamingException e) {
                    }
                }
            }
            return null;
        } catch (InvalidNameException e2) {
            throw new SSLException(str + " is not a valid X500 distinguished name");
        }
    }

    static List a(X509Certificate x509Certificate, int i) {
        Collection<List<?>> collection;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        try {
            collection = x509Certificate.getSubjectAlternativeNames();
        } catch (CertificateParsingException e) {
            collection = null;
        }
        if (collection != null) {
            for (List next : collection) {
                if (((Integer) next.get(0)).intValue() == i) {
                    String str = (String) next.get(1);
                    arrayList = arrayList2 == null ? new ArrayList() : arrayList2;
                    arrayList.add(str);
                } else {
                    arrayList = arrayList2;
                }
                arrayList2 = arrayList;
            }
        }
        return arrayList2;
    }

    static void a(String str, String str2, b bVar) {
        if (!b(str, str2, bVar)) {
            throw new SSLException("Certificate for <" + str + "> doesn't match " + "common name of the certificate subject: " + str2);
        }
    }

    static void a(String str, List list) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                throw new SSLException("Certificate for <" + str + "> doesn't match any " + "of the subject alternative names: " + list);
            } else if (!str.equals((String) list.get(i2))) {
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    static void a(String str, List list, b bVar) {
        String lowerCase = str.toLowerCase(Locale.ROOT);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                throw new SSLException("Certificate for <" + str + "> doesn't match any " + "of the subject alternative names: " + list);
            } else if (!b(lowerCase, ((String) list.get(i2)).toLowerCase(Locale.ROOT), bVar)) {
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    static boolean a(String str, String str2) {
        if (str2 != null && str.endsWith(str2)) {
            return str.length() == str2.length() || str.charAt((str.length() - str2.length()) + -1) == '.';
        }
        return false;
    }

    private static boolean a(String str, String str2, b bVar, boolean z) {
        if (bVar != null && str.contains(".") && !a(str, bVar.a(str2))) {
            return false;
        }
        int indexOf = str2.indexOf(42);
        if (indexOf == -1) {
            return str.equalsIgnoreCase(str2);
        }
        String substring = str2.substring(0, indexOf);
        String substring2 = str2.substring(indexOf + 1);
        if (!substring.isEmpty() && !str.startsWith(substring)) {
            return false;
        }
        if (substring2.isEmpty() || str.endsWith(substring2)) {
            return !z || !str.substring(substring.length(), str.length() - substring2.length()).contains(".");
        }
        return false;
    }

    static String b(String str) {
        if (str == null) {
            return str;
        }
        try {
            return InetAddress.getByName(str).getHostAddress();
        } catch (UnknownHostException e) {
            return str;
        }
    }

    static void b(String str, List list) {
        String b2 = b(str);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                throw new SSLException("Certificate for <" + str + "> doesn't match any " + "of the subject alternative names: " + list);
            } else if (!b2.equals(b((String) list.get(i2)))) {
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    static boolean b(String str, String str2, b bVar) {
        return a(str, str2, bVar, true);
    }

    public final void a(String str, X509Certificate x509Certificate) {
        boolean a2 = a.a(str);
        boolean d = a.d(str);
        List a3 = a(x509Certificate, (a2 || d) ? 7 : 2);
        if (a3 == null || a3.isEmpty()) {
            String a4 = a(x509Certificate.getSubjectX500Principal().getName("RFC2253"));
            if (a4 == null) {
                throw new SSLException("Certificate subject for <" + str + "> doesn't contain " + "a common name and does not have alternative names");
            }
            a(str, a4, this.b);
        } else if (a2) {
            a(str, a3);
        } else if (d) {
            b(str, a3);
        } else {
            a(str, a3, this.b);
        }
    }

    public final boolean verify(String str, SSLSession sSLSession) {
        try {
            a(str, (X509Certificate) sSLSession.getPeerCertificates()[0]);
            return true;
        } catch (SSLException e) {
            if (this.f42a.isDebugEnabled()) {
                this.f42a.debug(e.getMessage(), e);
            }
            return false;
        }
    }
}
