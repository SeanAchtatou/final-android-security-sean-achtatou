package a.a.a.e.d;

import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Deprecated
public abstract class a implements i {

    /* renamed from: a  reason: collision with root package name */
    static final String[] f41a = {"ac", "co", "com", "ed", "edu", "go", "gouv", "gov", "info", "lg", "ne", "net", "or", "org"};
    private final Log b = LogFactory.getLog(getClass());

    static {
        Arrays.sort(f41a);
    }

    public static int a(String str) {
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (str.charAt(i2) == '.') {
                i++;
            }
        }
        return i;
    }

    private static boolean a(String str, String str2, boolean z) {
        boolean endsWith;
        boolean z2 = true;
        if (str == null) {
            return false;
        }
        String lowerCase = str.toLowerCase(Locale.ROOT);
        String lowerCase2 = str2.toLowerCase(Locale.ROOT);
        String[] split = lowerCase2.split("\\.");
        if (!(split.length >= 3 && split[0].endsWith("*") && (!z || a(split)))) {
            return lowerCase.equals(lowerCase2);
        }
        String str3 = split[0];
        if (str3.length() > 1) {
            String substring = str3.substring(0, str3.length() - 1);
            endsWith = lowerCase.startsWith(substring) && lowerCase.substring(substring.length()).endsWith(lowerCase2.substring(str3.length()));
        } else {
            endsWith = lowerCase.endsWith(lowerCase2.substring(1));
        }
        if (!endsWith || (z && a(lowerCase) != a(lowerCase2))) {
            z2 = false;
        }
        return z2;
    }

    private static boolean a(String[] strArr) {
        return (strArr.length == 3 && strArr[2].length() == 2 && Arrays.binarySearch(f41a, strArr[1]) >= 0) ? false : true;
    }

    public final void a(String str, X509Certificate x509Certificate) {
        List a2 = d.a(x509Certificate, (a.a.a.e.e.a.a(str) || a.a.a.e.e.a.d(str)) ? 7 : 2);
        String a3 = d.a(x509Certificate.getSubjectX500Principal().getName("RFC2253"));
        a(str, a3 != null ? new String[]{a3} : null, (a2 == null || a2.isEmpty()) ? null : (String[]) a2.toArray(new String[a2.size()]));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public final void a(String str, SSLSocket sSLSocket) {
        a.a.a.n.a.a((Object) str, "Host");
        SSLSession session = sSLSocket.getSession();
        if (session == null) {
            sSLSocket.getInputStream().available();
            session = sSLSocket.getSession();
            if (session == null) {
                sSLSocket.startHandshake();
                session = sSLSocket.getSession();
            }
        }
        a(str, (X509Certificate) session.getPeerCertificates()[0]);
    }

    public final void a(String str, String[] strArr, String[] strArr2, boolean z) {
        String str2 = (strArr == null || strArr.length <= 0) ? null : strArr[0];
        List<String> asList = (strArr2 == null || strArr2.length <= 0) ? null : Arrays.asList(strArr2);
        String b2 = a.a.a.e.e.a.d(str) ? d.b(str.toLowerCase(Locale.ROOT)) : str;
        if (asList != null) {
            for (String str3 : asList) {
                if (a.a.a.e.e.a.d(str3)) {
                    str3 = d.b(str3);
                }
                if (a(b2, str3, z)) {
                    return;
                }
            }
            throw new SSLException("Certificate for <" + str + "> doesn't match any " + "of the subject alternative names: " + asList);
        } else if (str2 != null) {
            if (!a(b2, a.a.a.e.e.a.d(str2) ? d.b(str2) : str2, z)) {
                throw new SSLException("Certificate for <" + str + "> doesn't match " + "common name of the certificate subject: " + str2);
            }
        } else {
            throw new SSLException("Certificate subject for <" + str + "> doesn't contain " + "a common name and does not have alternative names");
        }
    }

    public final boolean verify(String str, SSLSession sSLSession) {
        try {
            a(str, (X509Certificate) sSLSession.getPeerCertificates()[0]);
            return true;
        } catch (SSLException e) {
            if (this.b.isDebugEnabled()) {
                this.b.debug(e.getMessage(), e);
            }
            return false;
        }
    }
}
