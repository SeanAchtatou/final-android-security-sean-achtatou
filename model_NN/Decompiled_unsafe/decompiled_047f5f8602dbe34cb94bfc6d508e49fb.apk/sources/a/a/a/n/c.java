package a.a.a.n;

import java.io.Serializable;

public final class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f193a;
    private int b;

    public c(int i) {
        a.b(i, "Buffer capacity");
        this.f193a = new byte[i];
    }

    private void c(int i) {
        byte[] bArr = new byte[Math.max(this.f193a.length << 1, i)];
        System.arraycopy(this.f193a, 0, bArr, 0, this.b);
        this.f193a = bArr;
    }

    public void a() {
        this.b = 0;
    }

    public void a(int i) {
        int i2 = this.b + 1;
        if (i2 > this.f193a.length) {
            c(i2);
        }
        this.f193a[this.b] = (byte) i;
        this.b = i2;
    }

    public void a(d dVar, int i, int i2) {
        if (dVar != null) {
            a(dVar.b(), i, i2);
        }
    }

    public void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i < 0 || i > bArr.length || i2 < 0 || i + i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + bArr.length);
            } else if (i2 != 0) {
                int i3 = this.b + i2;
                if (i3 > this.f193a.length) {
                    c(i3);
                }
                System.arraycopy(bArr, i, this.f193a, this.b, i2);
                this.b = i3;
            }
        }
    }

    public void a(char[] cArr, int i, int i2) {
        if (cArr != null) {
            if (i < 0 || i > cArr.length || i2 < 0 || i + i2 < 0 || i + i2 > cArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + cArr.length);
            } else if (i2 != 0) {
                int i3 = this.b;
                int i4 = i3 + i2;
                if (i4 > this.f193a.length) {
                    c(i4);
                }
                while (i3 < i4) {
                    this.f193a[i3] = (byte) cArr[i];
                    i++;
                    i3++;
                }
                this.b = i4;
            }
        }
    }

    public int b(int i) {
        return this.f193a[i];
    }

    public byte[] b() {
        byte[] bArr = new byte[this.b];
        if (this.b > 0) {
            System.arraycopy(this.f193a, 0, bArr, 0, this.b);
        }
        return bArr;
    }

    public int c() {
        return this.f193a.length;
    }

    public int d() {
        return this.b;
    }

    public byte[] e() {
        return this.f193a;
    }

    public boolean f() {
        return this.b == 0;
    }

    public boolean g() {
        return this.b == this.f193a.length;
    }
}
