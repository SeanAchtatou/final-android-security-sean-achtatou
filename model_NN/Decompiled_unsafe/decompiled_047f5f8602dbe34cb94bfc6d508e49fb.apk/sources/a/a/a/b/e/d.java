package a.a.a.b.e;

import a.a.a.a.a;
import a.a.a.a.c;
import a.a.a.a.i;
import a.a.a.a.j;
import a.a.a.a.m;
import a.a.a.a.n;
import a.a.a.e;
import a.a.a.n.b;
import a.a.a.q;
import a.a.a.r;
import java.util.Queue;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Deprecated
abstract class d implements r {

    /* renamed from: a  reason: collision with root package name */
    final Log f19a = LogFactory.getLog(getClass());

    private e a(c cVar, n nVar, q qVar, a.a.a.m.e eVar) {
        b.a(cVar, "Auth scheme");
        return cVar instanceof m ? ((m) cVar).a(nVar, qVar, eVar) : cVar.a(nVar, qVar);
    }

    private void a(c cVar) {
        b.a(cVar, "Auth scheme");
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar, q qVar, a.a.a.m.e eVar) {
        c c = iVar.c();
        n d = iVar.d();
        switch (e.f20a[iVar.b().ordinal()]) {
            case 1:
                return;
            case 2:
                a(c);
                if (c.c()) {
                    return;
                }
                break;
            case 3:
                Queue e = iVar.e();
                if (e == null) {
                    a(c);
                    break;
                } else {
                    while (!e.isEmpty()) {
                        a aVar = (a) e.remove();
                        c a2 = aVar.a();
                        n b = aVar.b();
                        iVar.a(a2, b);
                        if (this.f19a.isDebugEnabled()) {
                            this.f19a.debug("Generating response to an authentication challenge using " + a2.a() + " scheme");
                        }
                        try {
                            qVar.a(a(a2, b, qVar, eVar));
                            return;
                        } catch (j e2) {
                            if (this.f19a.isWarnEnabled()) {
                                this.f19a.warn(a2 + " authentication error: " + e2.getMessage());
                            }
                        }
                    }
                    return;
                }
        }
        if (c != null) {
            try {
                qVar.a(a(c, d, qVar, eVar));
            } catch (j e3) {
                if (this.f19a.isErrorEnabled()) {
                    this.f19a.error(c + " authentication error: " + e3.getMessage());
                }
            }
        }
    }
}
