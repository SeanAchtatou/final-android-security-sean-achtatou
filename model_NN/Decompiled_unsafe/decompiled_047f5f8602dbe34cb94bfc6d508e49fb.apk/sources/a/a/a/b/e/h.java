package a.a.a.b.e;

import a.a.a.a.i;
import a.a.a.e.n;
import a.a.a.m.e;
import a.a.a.n.a;
import a.a.a.q;

@Deprecated
public class h extends d {
    public void a(q qVar, e eVar) {
        a.a(qVar, "HTTP request");
        a.a(eVar, "HTTP context");
        if (!qVar.a("Proxy-Authorization")) {
            n nVar = (n) eVar.a("http.connection");
            if (nVar == null) {
                this.f19a.debug("HTTP connection not set in the context");
            } else if (!nVar.j().e()) {
                i iVar = (i) eVar.a("http.auth.proxy-scope");
                if (iVar == null) {
                    this.f19a.debug("Proxy auth state not set in the context");
                    return;
                }
                if (this.f19a.isDebugEnabled()) {
                    this.f19a.debug("Proxy auth state: " + iVar.b());
                }
                a(iVar, qVar, eVar);
            }
        }
    }
}
