package a.a.a.b.f;

import a.a.a.c;
import a.a.a.g.e;
import a.a.a.j.l;
import a.a.a.j.u;
import a.a.a.j.v;
import a.a.a.k;
import a.a.a.m.d;
import a.a.a.n.a;
import a.a.a.y;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static final BitSet f27a = new BitSet(256);
    private static final BitSet b = new BitSet(256);
    private static final BitSet c = new BitSet(256);
    private static final BitSet d = new BitSet(256);
    private static final BitSet e = new BitSet(256);
    private static final BitSet f = new BitSet(256);
    private static final BitSet g = new BitSet(256);

    static {
        for (int i = 97; i <= 122; i++) {
            f27a.set(i);
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            f27a.set(i2);
        }
        for (int i3 = 48; i3 <= 57; i3++) {
            f27a.set(i3);
        }
        f27a.set(95);
        f27a.set(45);
        f27a.set(46);
        f27a.set(42);
        g.or(f27a);
        f27a.set(33);
        f27a.set(126);
        f27a.set(39);
        f27a.set(40);
        f27a.set(41);
        b.set(44);
        b.set(59);
        b.set(58);
        b.set(36);
        b.set(38);
        b.set(43);
        b.set(61);
        c.or(f27a);
        c.or(b);
        d.or(f27a);
        d.set(47);
        d.set(59);
        d.set(58);
        d.set(64);
        d.set(38);
        d.set(61);
        d.set(43);
        d.set(36);
        d.set(44);
        f.set(59);
        f.set(47);
        f.set(63);
        f.set(58);
        f.set(64);
        f.set(38);
        f.set(61);
        f.set(43);
        f.set(36);
        f.set(44);
        f.set(91);
        f.set(93);
        e.or(f);
        e.or(f27a);
    }

    public static String a(Iterable iterable, char c2, Charset charset) {
        StringBuilder sb = new StringBuilder();
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            y yVar = (y) it.next();
            String f2 = f(yVar.a(), charset);
            String f3 = f(yVar.b(), charset);
            if (sb.length() > 0) {
                sb.append(c2);
            }
            sb.append(f2);
            if (f3 != null) {
                sb.append("=");
                sb.append(f3);
            }
        }
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.b.f.g.a(java.lang.Iterable, char, java.nio.charset.Charset):java.lang.String
     arg types: [java.lang.Iterable, int, java.nio.charset.Charset]
     candidates:
      a.a.a.b.f.g.a(java.lang.String, java.nio.charset.Charset, boolean):java.lang.String
      a.a.a.b.f.g.a(a.a.a.n.d, java.nio.charset.Charset, char[]):java.util.List
      a.a.a.b.f.g.a(java.lang.Iterable, char, java.nio.charset.Charset):java.lang.String */
    public static String a(Iterable iterable, Charset charset) {
        return a(iterable, '&', charset);
    }

    private static String a(String str, Charset charset, BitSet bitSet, boolean z) {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        ByteBuffer encode = charset.encode(str);
        while (encode.hasRemaining()) {
            byte b2 = encode.get() & 255;
            if (bitSet.get(b2)) {
                sb.append((char) b2);
            } else if (!z || b2 != 32) {
                sb.append("%");
                char upperCase = Character.toUpperCase(Character.forDigit((b2 >> 4) & 15, 16));
                char upperCase2 = Character.toUpperCase(Character.forDigit(b2 & 15, 16));
                sb.append(upperCase);
                sb.append(upperCase2);
            } else {
                sb.append('+');
            }
        }
        return sb.toString();
    }

    private static String a(String str, Charset charset, boolean z) {
        if (str == null) {
            return null;
        }
        ByteBuffer allocate = ByteBuffer.allocate(str.length());
        CharBuffer wrap = CharBuffer.wrap(str);
        while (wrap.hasRemaining()) {
            char c2 = wrap.get();
            if (c2 == '%' && wrap.remaining() >= 2) {
                char c3 = wrap.get();
                char c4 = wrap.get();
                int digit = Character.digit(c3, 16);
                int digit2 = Character.digit(c4, 16);
                if (digit == -1 || digit2 == -1) {
                    allocate.put((byte) 37);
                    allocate.put((byte) c3);
                    allocate.put((byte) c4);
                } else {
                    allocate.put((byte) ((digit << 4) + digit2));
                }
            } else if (!z || c2 != '+') {
                allocate.put((byte) c2);
            } else {
                allocate.put((byte) 32);
            }
        }
        allocate.flip();
        return charset.decode(allocate).toString();
    }

    /* JADX INFO: finally extract failed */
    public static List a(k kVar) {
        int i = 1024;
        e a2 = e.a(kVar);
        if (a2 == null || !a2.a().equalsIgnoreCase("application/x-www-form-urlencoded")) {
            return Collections.emptyList();
        }
        long c2 = kVar.c();
        a.a(c2 <= 2147483647L, "HTTP entity is too large");
        Charset b2 = a2.b() != null ? a2.b() : d.f185a;
        InputStream f2 = kVar.f();
        if (f2 == null) {
            return Collections.emptyList();
        }
        try {
            if (c2 > 0) {
                i = (int) c2;
            }
            a.a.a.n.d dVar = new a.a.a.n.d(i);
            InputStreamReader inputStreamReader = new InputStreamReader(f2, b2);
            char[] cArr = new char[1024];
            while (true) {
                int read = inputStreamReader.read(cArr);
                if (read == -1) {
                    break;
                }
                dVar.a(cArr, 0, read);
            }
            f2.close();
            if (dVar.length() == 0) {
                return Collections.emptyList();
            }
            return a(dVar, b2, '&');
        } catch (Throwable th) {
            f2.close();
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [a.a.a.n.d, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public static List a(a.a.a.n.d dVar, Charset charset, char... cArr) {
        a.a((Object) dVar, "Char array buffer");
        v vVar = v.f179a;
        BitSet bitSet = new BitSet();
        for (char c2 : cArr) {
            bitSet.set(c2);
        }
        u uVar = new u(0, dVar.length());
        ArrayList arrayList = new ArrayList();
        while (!uVar.c()) {
            bitSet.set(61);
            String a2 = vVar.a(dVar, uVar, bitSet);
            String str = null;
            if (!uVar.c()) {
                char charAt = dVar.charAt(uVar.b());
                uVar.a(uVar.b() + 1);
                if (charAt == '=') {
                    bitSet.clear(61);
                    str = vVar.b(dVar, uVar, bitSet);
                    if (!uVar.c()) {
                        uVar.a(uVar.b() + 1);
                    }
                }
            }
            if (!a2.isEmpty()) {
                arrayList.add(new l(e(a2, charset), e(str, charset)));
            }
        }
        return arrayList;
    }

    public static List a(String str, Charset charset) {
        a.a.a.n.d dVar = new a.a.a.n.d(str.length());
        dVar.a(str);
        return a(dVar, charset, '&', ';');
    }

    static String b(String str, Charset charset) {
        return a(str, charset, c, false);
    }

    static String c(String str, Charset charset) {
        return a(str, charset, e, false);
    }

    static String d(String str, Charset charset) {
        return a(str, charset, d, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.b.f.g.a(java.lang.String, java.nio.charset.Charset, boolean):java.lang.String
     arg types: [java.lang.String, java.nio.charset.Charset, int]
     candidates:
      a.a.a.b.f.g.a(java.lang.Iterable, char, java.nio.charset.Charset):java.lang.String
      a.a.a.b.f.g.a(a.a.a.n.d, java.nio.charset.Charset, char[]):java.util.List
      a.a.a.b.f.g.a(java.lang.String, java.nio.charset.Charset, boolean):java.lang.String */
    private static String e(String str, Charset charset) {
        if (str == null) {
            return null;
        }
        if (charset == null) {
            charset = c.f28a;
        }
        return a(str, charset, true);
    }

    private static String f(String str, Charset charset) {
        if (str == null) {
            return null;
        }
        if (charset == null) {
            charset = c.f28a;
        }
        return a(str, charset, g, true);
    }
}
