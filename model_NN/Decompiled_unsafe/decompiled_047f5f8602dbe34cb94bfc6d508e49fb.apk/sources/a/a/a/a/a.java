package a.a.a.a;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final c f0a;
    private final n b;

    public a(c cVar, n nVar) {
        a.a.a.n.a.a(cVar, "Auth scheme");
        a.a.a.n.a.a(nVar, "User credentials");
        this.f0a = cVar;
        this.b = nVar;
    }

    public c a() {
        return this.f0a;
    }

    public n b() {
        return this.b;
    }

    public String toString() {
        return this.f0a.toString();
    }
}
