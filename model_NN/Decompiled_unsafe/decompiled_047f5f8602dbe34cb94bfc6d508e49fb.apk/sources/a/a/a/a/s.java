package a.a.a.a;

import a.a.a.n.h;
import java.io.Serializable;
import java.security.Principal;

public class s implements Serializable, Principal {

    /* renamed from: a  reason: collision with root package name */
    private final String f10a;
    private final String b;
    private final String c;

    public String a() {
        return this.b;
    }

    public String b() {
        return this.f10a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof s) {
            s sVar = (s) obj;
            return h.a(this.f10a, sVar.f10a) && h.a(this.b, sVar.b);
        }
    }

    public String getName() {
        return this.c;
    }

    public int hashCode() {
        return h.a(h.a(17, this.f10a), this.b);
    }

    public String toString() {
        return this.c;
    }
}
