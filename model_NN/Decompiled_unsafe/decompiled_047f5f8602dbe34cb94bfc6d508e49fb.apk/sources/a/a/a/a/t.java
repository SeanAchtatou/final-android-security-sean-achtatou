package a.a.a.a;

import a.a.a.n.a;
import a.a.a.n.h;
import java.io.Serializable;
import java.security.Principal;

public class t implements n, Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final k f11a;
    private final String b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public t(String str) {
        a.a((Object) str, "Username:password string");
        int indexOf = str.indexOf(58);
        if (indexOf >= 0) {
            this.f11a = new k(str.substring(0, indexOf));
            this.b = str.substring(indexOf + 1);
            return;
        }
        this.f11a = new k(str);
        this.b = null;
    }

    public Principal a() {
        return this.f11a;
    }

    public String b() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof t) && h.a(this.f11a, ((t) obj).f11a);
    }

    public int hashCode() {
        return this.f11a.hashCode();
    }

    public String toString() {
        return this.f11a.toString();
    }
}
