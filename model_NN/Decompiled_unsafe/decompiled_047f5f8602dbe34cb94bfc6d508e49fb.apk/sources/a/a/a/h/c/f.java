package a.a.a.h.c;

import a.a.a.ab;
import a.a.a.h.f.a;
import a.a.a.j.u;
import a.a.a.k.e;
import a.a.a.n.d;
import a.a.a.s;
import a.a.a.t;
import a.a.a.z;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class f extends a {
    private final Log b = LogFactory.getLog(getClass());
    private final t c;
    private final d d;

    @Deprecated
    public f(a.a.a.i.f fVar, a.a.a.j.t tVar, t tVar2, e eVar) {
        super(fVar, tVar, eVar);
        a.a.a.n.a.a(tVar2, "Response factory");
        this.c = tVar2;
        this.d = new d(128);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public s b(a.a.a.i.f fVar) {
        int i = 0;
        while (true) {
            this.d.a();
            int a2 = fVar.a(this.d);
            if (a2 == -1 && i == 0) {
                throw new z("The target server failed to respond");
            }
            u uVar = new u(0, this.d.length());
            if (this.f150a.b(this.d, uVar)) {
                return this.c.a(this.f150a.c(this.d, uVar), null);
            } else if (a2 != -1 && !a(this.d, i)) {
                if (this.b.isDebugEnabled()) {
                    this.b.debug("Garbage in response: " + this.d.toString());
                }
                i++;
            }
        }
        throw new ab("The server failed to respond with a valid HTTP response");
    }

    /* access modifiers changed from: protected */
    public boolean a(d dVar, int i) {
        return false;
    }
}
