package a.a.a.h.a;

import a.a.a.a.n;
import a.a.a.e;
import a.a.a.q;
import org.ietf.jgss.Oid;

public class l extends f {
    public l() {
    }

    public l(boolean z, boolean z2) {
        super(z, z2);
    }

    public e a(n nVar, q qVar, a.a.a.m.e eVar) {
        return super.a(nVar, qVar, eVar);
    }

    public String a() {
        return "Kerberos";
    }

    /* access modifiers changed from: protected */
    public byte[] a(byte[] bArr, String str) {
        return super.a(bArr, str);
    }

    /* access modifiers changed from: protected */
    public byte[] a(byte[] bArr, String str, n nVar) {
        return a(bArr, new Oid("1.2.840.113554.1.2.2"), str, nVar);
    }

    public String b() {
        return null;
    }

    public boolean c() {
        return true;
    }
}
