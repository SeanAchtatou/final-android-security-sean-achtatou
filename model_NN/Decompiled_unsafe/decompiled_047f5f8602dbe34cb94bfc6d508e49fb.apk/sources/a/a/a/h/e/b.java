package a.a.a.h.e;

import a.a.a.g.d;
import a.a.a.h.f.f;
import a.a.a.h.f.h;
import a.a.a.h.f.n;
import a.a.a.i.g;
import a.a.a.k;
import a.a.a.n.a;
import a.a.a.p;
import java.io.OutputStream;

@Deprecated
public class b {

    /* renamed from: a  reason: collision with root package name */
    private final d f146a;

    public b(d dVar) {
        this.f146a = (d) a.a(dVar, "Content length strategy");
    }

    /* access modifiers changed from: protected */
    public OutputStream a(g gVar, p pVar) {
        long a2 = this.f146a.a(pVar);
        return a2 == -2 ? new f(gVar) : a2 == -1 ? new n(gVar) : new h(gVar, a2);
    }

    public void a(g gVar, p pVar, k kVar) {
        a.a(gVar, "Session output buffer");
        a.a(pVar, "HTTP message");
        a.a(kVar, "HTTP entity");
        OutputStream a2 = a(gVar, pVar);
        kVar.a(a2);
        a2.close();
    }
}
