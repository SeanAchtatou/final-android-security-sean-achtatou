package a.a.a.h.b;

import a.a.a.ab;
import a.a.a.ac;
import a.a.a.ae;
import a.a.a.b.c.l;
import a.a.a.j.a;
import a.a.a.j.m;
import a.a.a.k.f;
import a.a.a.q;
import java.net.URI;
import java.net.URISyntaxException;

@Deprecated
public class v extends a implements l {
    private final q c;
    private URI d;
    private String e;
    private ac f;
    private int g;

    public v(q qVar) {
        a.a.a.n.a.a(qVar, "HTTP request");
        this.c = qVar;
        a(qVar.f());
        a(qVar.d());
        if (qVar instanceof l) {
            this.d = ((l) qVar).i();
            this.e = ((l) qVar).a_();
            this.f = null;
        } else {
            ae g2 = qVar.g();
            try {
                this.d = new URI(g2.c());
                this.e = g2.a();
                this.f = qVar.c();
            } catch (URISyntaxException e2) {
                throw new ab("Invalid request URI: " + g2.c(), e2);
            }
        }
        this.g = 0;
    }

    public void a(URI uri) {
        this.d = uri;
    }

    public String a_() {
        return this.e;
    }

    public ac c() {
        if (this.f == null) {
            this.f = f.b(f());
        }
        return this.f;
    }

    public ae g() {
        ac c2 = c();
        String str = null;
        if (this.d != null) {
            str = this.d.toASCIIString();
        }
        if (str == null || str.isEmpty()) {
            str = "/";
        }
        return new m(a_(), str, c2);
    }

    public boolean h() {
        return false;
    }

    public URI i() {
        return this.d;
    }

    public boolean j() {
        return true;
    }

    public void k() {
        this.f163a.a();
        a(this.c.d());
    }

    public q l() {
        return this.c;
    }

    public int m() {
        return this.g;
    }

    public void n() {
        this.g++;
    }
}
