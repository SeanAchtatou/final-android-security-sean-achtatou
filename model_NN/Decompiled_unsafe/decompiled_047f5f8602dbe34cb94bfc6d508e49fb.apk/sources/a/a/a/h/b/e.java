package a.a.a.h.b;

import a.a.a.b.h;
import a.a.a.f.c;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public class e implements h, Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final TreeSet f99a = new TreeSet(new a.a.a.f.e());

    public synchronized List a() {
        return new ArrayList(this.f99a);
    }

    public synchronized void a(c cVar) {
        if (cVar != null) {
            this.f99a.remove(cVar);
            if (!cVar.a(new Date())) {
                this.f99a.add(cVar);
            }
        }
    }

    public synchronized boolean a(Date date) {
        boolean z = false;
        synchronized (this) {
            if (date != null) {
                Iterator it = this.f99a.iterator();
                boolean z2 = false;
                while (it.hasNext()) {
                    if (((c) it.next()).a(date)) {
                        it.remove();
                        z2 = true;
                    }
                }
                z = z2;
            }
        }
        return z;
    }

    public synchronized String toString() {
        return this.f99a.toString();
    }
}
