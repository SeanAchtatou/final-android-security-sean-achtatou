package a.a.a.h.f;

import a.a.a.i.a;
import a.a.a.i.g;
import a.a.a.k.e;
import a.a.a.n.c;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;

@Deprecated
public abstract class d implements a, g {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f153a = {13, 10};
    private OutputStream b;
    private c c;
    private Charset d;
    private boolean e;
    private int f;
    private l g;
    private CodingErrorAction h;
    private CodingErrorAction i;
    private CharsetEncoder j;
    private ByteBuffer k;

    private void a(CharBuffer charBuffer) {
        if (charBuffer.hasRemaining()) {
            if (this.j == null) {
                this.j = this.d.newEncoder();
                this.j.onMalformedInput(this.h);
                this.j.onUnmappableCharacter(this.i);
            }
            if (this.k == null) {
                this.k = ByteBuffer.allocate(1024);
            }
            this.j.reset();
            while (charBuffer.hasRemaining()) {
                a(this.j.encode(charBuffer, this.k, true));
            }
            a(this.j.flush(this.k));
            this.k.clear();
        }
    }

    private void a(CoderResult coderResult) {
        if (coderResult.isError()) {
            coderResult.throwException();
        }
        this.k.flip();
        while (this.k.hasRemaining()) {
            a(this.k.get());
        }
        this.k.compact();
    }

    public void a() {
        d();
        this.b.flush();
    }

    public void a(int i2) {
        if (this.c.g()) {
            d();
        }
        this.c.a(i2);
    }

    public void a(a.a.a.n.d dVar) {
        int i2 = 0;
        if (dVar != null) {
            if (this.e) {
                int length = dVar.length();
                while (length > 0) {
                    int min = Math.min(this.c.c() - this.c.d(), length);
                    if (min > 0) {
                        this.c.a(dVar, i2, min);
                    }
                    if (this.c.g()) {
                        d();
                    }
                    i2 += min;
                    length -= min;
                }
            } else {
                a(CharBuffer.wrap(dVar.b(), 0, dVar.length()));
            }
            a(f153a);
        }
    }

    /* access modifiers changed from: protected */
    public void a(OutputStream outputStream, int i2, e eVar) {
        a.a.a.n.a.a(outputStream, "Input stream");
        a.a.a.n.a.b(i2, "Buffer size");
        a.a.a.n.a.a(eVar, "HTTP parameters");
        this.b = outputStream;
        this.c = new c(i2);
        String str = (String) eVar.a("http.protocol.element-charset");
        this.d = str != null ? Charset.forName(str) : a.a.a.c.b;
        this.e = this.d.equals(a.a.a.c.b);
        this.j = null;
        this.f = eVar.a("http.connection.min-chunk-limit", 512);
        this.g = c();
        CodingErrorAction codingErrorAction = (CodingErrorAction) eVar.a("http.malformed.input.action");
        if (codingErrorAction == null) {
            codingErrorAction = CodingErrorAction.REPORT;
        }
        this.h = codingErrorAction;
        CodingErrorAction codingErrorAction2 = (CodingErrorAction) eVar.a("http.unmappable.input.action");
        if (codingErrorAction2 == null) {
            codingErrorAction2 = CodingErrorAction.REPORT;
        }
        this.i = codingErrorAction2;
    }

    public void a(String str) {
        if (str != null) {
            if (str.length() > 0) {
                if (this.e) {
                    for (int i2 = 0; i2 < str.length(); i2++) {
                        a(str.charAt(i2));
                    }
                } else {
                    a(CharBuffer.wrap(str));
                }
            }
            a(f153a);
        }
    }

    public void a(byte[] bArr) {
        if (bArr != null) {
            a(bArr, 0, bArr.length);
        }
    }

    public void a(byte[] bArr, int i2, int i3) {
        if (bArr != null) {
            if (i3 > this.f || i3 > this.c.c()) {
                d();
                this.b.write(bArr, i2, i3);
                this.g.a((long) i3);
                return;
            }
            if (i3 > this.c.c() - this.c.d()) {
                d();
            }
            this.c.a(bArr, i2, i3);
        }
    }

    public a.a.a.i.e b() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public l c() {
        return new l();
    }

    /* access modifiers changed from: protected */
    public void d() {
        int d2 = this.c.d();
        if (d2 > 0) {
            this.b.write(this.c.e(), 0, d2);
            this.c.a();
            this.g.a((long) d2);
        }
    }

    public int e() {
        return this.c.d();
    }
}
