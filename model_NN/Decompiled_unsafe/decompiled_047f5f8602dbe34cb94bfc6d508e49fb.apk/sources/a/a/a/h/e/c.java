package a.a.a.h.e;

import a.a.a.aa;
import a.a.a.ab;
import a.a.a.e;
import a.a.a.f;
import a.a.a.g.d;
import a.a.a.n.a;
import a.a.a.p;

public class c implements d {

    /* renamed from: a  reason: collision with root package name */
    public static final c f147a = new c();
    private final int b;

    public c() {
        this(-1);
    }

    public c(int i) {
        this.b = i;
    }

    public long a(p pVar) {
        long j;
        a.a(pVar, "HTTP message");
        e c = pVar.c("Transfer-Encoding");
        if (c != null) {
            try {
                f[] e = c.e();
                int length = e.length;
                return (!"identity".equalsIgnoreCase(c.d()) && length > 0 && "chunked".equalsIgnoreCase(e[length + -1].a())) ? -2 : -1;
            } catch (aa e2) {
                throw new ab("Invalid Transfer-Encoding header value: " + c, e2);
            }
        } else if (pVar.c("Content-Length") == null) {
            return (long) this.b;
        } else {
            e[] b2 = pVar.b("Content-Length");
            int length2 = b2.length - 1;
            while (true) {
                if (length2 < 0) {
                    j = -1;
                    break;
                }
                try {
                    j = Long.parseLong(b2[length2].d());
                    break;
                } catch (NumberFormatException e3) {
                    length2--;
                }
            }
            if (j >= 0) {
                return j;
            }
            return -1;
        }
    }
}
