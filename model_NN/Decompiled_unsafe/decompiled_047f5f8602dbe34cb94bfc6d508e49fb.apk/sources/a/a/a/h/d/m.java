package a.a.a.h.d;

import a.a.a.e;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.n;
import a.a.a.j.p;
import a.a.a.j.u;
import a.a.a.n.a;
import a.a.a.n.d;
import a.a.a.y;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class m extends r {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f138a = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"};

    public m() {
        this(null, p.SECURITYLEVEL_DEFAULT);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public m(java.lang.String[] r5, a.a.a.h.d.p r6) {
        /*
            r4 = this;
            r0 = 7
            a.a.a.f.b[] r1 = new a.a.a.f.b[r0]
            r0 = 0
            a.a.a.h.d.q r2 = new a.a.a.h.d.q
            r2.<init>()
            r1[r0] = r2
            r0 = 1
            a.a.a.h.d.f r2 = new a.a.a.h.d.f
            r2.<init>()
            r1[r0] = r2
            r2 = 2
            a.a.a.h.d.p r0 = a.a.a.h.d.p.SECURITYLEVEL_IE_MEDIUM
            if (r6 != r0) goto L_0x004b
            a.a.a.h.d.n r0 = new a.a.a.h.d.n
            r0.<init>()
        L_0x001d:
            r1[r2] = r0
            r0 = 3
            a.a.a.h.d.h r2 = new a.a.a.h.d.h
            r2.<init>()
            r1[r0] = r2
            r0 = 4
            a.a.a.h.d.j r2 = new a.a.a.h.d.j
            r2.<init>()
            r1[r0] = r2
            r0 = 5
            a.a.a.h.d.e r2 = new a.a.a.h.d.e
            r2.<init>()
            r1[r0] = r2
            r2 = 6
            a.a.a.h.d.g r3 = new a.a.a.h.d.g
            if (r5 == 0) goto L_0x0051
            java.lang.Object r0 = r5.clone()
            java.lang.String[] r0 = (java.lang.String[]) r0
        L_0x0042:
            r3.<init>(r0)
            r1[r2] = r3
            r4.<init>(r1)
            return
        L_0x004b:
            a.a.a.h.d.i r0 = new a.a.a.h.d.i
            r0.<init>()
            goto L_0x001d
        L_0x0051:
            java.lang.String[] r0 = a.a.a.h.d.m.f138a
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.h.d.m.<init>(java.lang.String[], a.a.a.h.d.p):void");
    }

    private static boolean b(String str) {
        return str != null && str.startsWith("\"") && str.endsWith("\"");
    }

    public int a() {
        return 0;
    }

    public List a(e eVar, f fVar) {
        d dVar;
        u uVar;
        a.a(eVar, "Header");
        a.a(fVar, "Cookie origin");
        if (!eVar.c().equalsIgnoreCase("Set-Cookie")) {
            throw new n("Unrecognized cookie header '" + eVar.toString() + "'");
        }
        a.a.a.f[] e = eVar.e();
        boolean z = false;
        boolean z2 = false;
        for (a.a.a.f fVar2 : e) {
            if (fVar2.a("version") != null) {
                z2 = true;
            }
            if (fVar2.a("expires") != null) {
                z = true;
            }
        }
        if (!z && z2) {
            return a(e, fVar);
        }
        w wVar = w.f142a;
        if (eVar instanceof a.a.a.d) {
            dVar = ((a.a.a.d) eVar).a();
            uVar = new u(((a.a.a.d) eVar).b(), dVar.length());
        } else {
            String d = eVar.d();
            if (d == null) {
                throw new n("Header value is null");
            }
            dVar = new d(d.length());
            dVar.a(d);
            uVar = new u(0, dVar.length());
        }
        a.a.a.f a2 = wVar.a(dVar, uVar);
        String a3 = a2.a();
        String b = a2.b();
        if (a3 == null || a3.isEmpty()) {
            throw new n("Cookie name may not be empty");
        }
        c cVar = new c(a3, b);
        cVar.e(a(fVar));
        cVar.d(b(fVar));
        y[] c = a2.c();
        for (int length = c.length - 1; length >= 0; length--) {
            y yVar = c[length];
            String lowerCase = yVar.a().toLowerCase(Locale.ROOT);
            cVar.a(lowerCase, yVar.b());
            a.a.a.f.d a4 = a(lowerCase);
            if (a4 != null) {
                a4.a(cVar, yVar.b());
            }
        }
        if (z) {
            cVar.a(0);
        }
        return Collections.singletonList(cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
     arg types: [java.util.List, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.e.a(a.a.a.n.d, a.a.a.f, boolean):a.a.a.n.d
     arg types: [a.a.a.n.d, a.a.a.j.c, int]
     candidates:
      a.a.a.j.e.a(a.a.a.n.d, a.a.a.y, boolean):a.a.a.n.d
      a.a.a.j.e.a(a.a.a.n.d, a.a.a.y[], boolean):a.a.a.n.d
      a.a.a.j.e.a(a.a.a.n.d, java.lang.String, boolean):void
      a.a.a.j.e.a(a.a.a.n.d, a.a.a.f, boolean):a.a.a.n.d */
    public List a(List list) {
        a.a((Collection) list, "List of cookies");
        d dVar = new d(list.size() * 20);
        dVar.a("Cookie");
        dVar.a(": ");
        for (int i = 0; i < list.size(); i++) {
            c cVar = (c) list.get(i);
            if (i > 0) {
                dVar.a("; ");
            }
            String a2 = cVar.a();
            String b = cVar.b();
            if (cVar.h() <= 0 || b(b)) {
                dVar.a(a2);
                dVar.a("=");
                if (b != null) {
                    dVar.a(b);
                }
            } else {
                a.a.a.j.e.b.a(dVar, (a.a.a.f) new a.a.a.j.c(a2, b), false);
            }
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new p(dVar));
        return arrayList;
    }

    public e b() {
        return null;
    }

    public String toString() {
        return "compatibility";
    }
}
