package a.a.a.h.f;

import a.a.a.i.a;
import a.a.a.i.f;
import a.a.a.k.e;
import a.a.a.n.d;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;

@Deprecated
public abstract class c implements a, f {

    /* renamed from: a  reason: collision with root package name */
    private InputStream f152a;
    private byte[] b;
    private a.a.a.n.c c;
    private Charset d;
    private boolean e;
    private int f;
    private int g;
    private l h;
    private CodingErrorAction i;
    private CodingErrorAction j;
    private int k;
    private int l;
    private CharsetDecoder m;
    private CharBuffer n;

    private int a(d dVar, int i2) {
        int i3 = this.k;
        this.k = i2 + 1;
        if (i2 > i3 && this.b[i2 - 1] == 13) {
            i2--;
        }
        int i4 = i2 - i3;
        if (!this.e) {
            return a(dVar, ByteBuffer.wrap(this.b, i3, i4));
        }
        dVar.a(this.b, i3, i4);
        return i4;
    }

    private int a(d dVar, ByteBuffer byteBuffer) {
        int i2 = 0;
        if (!byteBuffer.hasRemaining()) {
            return 0;
        }
        if (this.m == null) {
            this.m = this.d.newDecoder();
            this.m.onMalformedInput(this.i);
            this.m.onUnmappableCharacter(this.j);
        }
        if (this.n == null) {
            this.n = CharBuffer.allocate(1024);
        }
        this.m.reset();
        while (byteBuffer.hasRemaining()) {
            i2 += a(this.m.decode(byteBuffer, this.n, true), dVar, byteBuffer);
        }
        int a2 = i2 + a(this.m.flush(this.n), dVar, byteBuffer);
        this.n.clear();
        return a2;
    }

    private int a(CoderResult coderResult, d dVar, ByteBuffer byteBuffer) {
        if (coderResult.isError()) {
            coderResult.throwException();
        }
        this.n.flip();
        int remaining = this.n.remaining();
        while (this.n.hasRemaining()) {
            dVar.a(this.n.get());
        }
        this.n.compact();
        return remaining;
    }

    private int b(d dVar) {
        int d2 = this.c.d();
        if (d2 > 0) {
            if (this.c.b(d2 - 1) == 10) {
                d2--;
            }
            if (d2 > 0 && this.c.b(d2 - 1) == 13) {
                d2--;
            }
        }
        if (this.e) {
            dVar.a(this.c, 0, d2);
        } else {
            d2 = a(dVar, ByteBuffer.wrap(this.c.e(), 0, d2));
        }
        this.c.a();
        return d2;
    }

    private int c() {
        for (int i2 = this.k; i2 < this.l; i2++) {
            if (this.b[i2] == 10) {
                return i2;
            }
        }
        return -1;
    }

    public int a() {
        while (!g()) {
            if (f() == -1) {
                return -1;
            }
        }
        byte[] bArr = this.b;
        int i2 = this.k;
        this.k = i2 + 1;
        return bArr[i2] & 255;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [a.a.a.n.d, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public int a(d dVar) {
        a.a.a.n.a.a((Object) dVar, "Char array buffer");
        boolean z = true;
        int i2 = 0;
        while (z) {
            int c2 = c();
            if (c2 == -1) {
                if (g()) {
                    this.c.a(this.b, this.k, this.l - this.k);
                    this.k = this.l;
                }
                i2 = f();
                if (i2 == -1) {
                    z = false;
                }
            } else if (this.c.f()) {
                return a(dVar, c2);
            } else {
                this.c.a(this.b, this.k, (c2 + 1) - this.k);
                this.k = c2 + 1;
                z = false;
            }
            if (this.f > 0 && this.c.d() >= this.f) {
                throw new IOException("Maximum line length limit exceeded");
            }
        }
        if (i2 != -1 || !this.c.f()) {
            return b(dVar);
        }
        return -1;
    }

    public int a(byte[] bArr, int i2, int i3) {
        if (bArr == null) {
            return 0;
        }
        if (g()) {
            int min = Math.min(i3, this.l - this.k);
            System.arraycopy(this.b, this.k, bArr, i2, min);
            this.k += min;
            return min;
        } else if (i3 > this.g) {
            int read = this.f152a.read(bArr, i2, i3);
            if (read <= 0) {
                return read;
            }
            this.h.a((long) read);
            return read;
        } else {
            while (!g()) {
                if (f() == -1) {
                    return -1;
                }
            }
            int min2 = Math.min(i3, this.l - this.k);
            System.arraycopy(this.b, this.k, bArr, i2, min2);
            this.k += min2;
            return min2;
        }
    }

    /* access modifiers changed from: protected */
    public void a(InputStream inputStream, int i2, e eVar) {
        a.a.a.n.a.a(inputStream, "Input stream");
        a.a.a.n.a.b(i2, "Buffer size");
        a.a.a.n.a.a(eVar, "HTTP parameters");
        this.f152a = inputStream;
        this.b = new byte[i2];
        this.k = 0;
        this.l = 0;
        this.c = new a.a.a.n.c(i2);
        String str = (String) eVar.a("http.protocol.element-charset");
        this.d = str != null ? Charset.forName(str) : a.a.a.c.b;
        this.e = this.d.equals(a.a.a.c.b);
        this.m = null;
        this.f = eVar.a("http.connection.max-line-length", -1);
        this.g = eVar.a("http.connection.min-chunk-limit", 512);
        this.h = d();
        CodingErrorAction codingErrorAction = (CodingErrorAction) eVar.a("http.malformed.input.action");
        if (codingErrorAction == null) {
            codingErrorAction = CodingErrorAction.REPORT;
        }
        this.i = codingErrorAction;
        CodingErrorAction codingErrorAction2 = (CodingErrorAction) eVar.a("http.unmappable.input.action");
        if (codingErrorAction2 == null) {
            codingErrorAction2 = CodingErrorAction.REPORT;
        }
        this.j = codingErrorAction2;
    }

    public a.a.a.i.e b() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public l d() {
        return new l();
    }

    public int e() {
        return this.l - this.k;
    }

    /* access modifiers changed from: protected */
    public int f() {
        if (this.k > 0) {
            int i2 = this.l - this.k;
            if (i2 > 0) {
                System.arraycopy(this.b, this.k, this.b, 0, i2);
            }
            this.k = 0;
            this.l = i2;
        }
        int i3 = this.l;
        int read = this.f152a.read(this.b, i3, this.b.length - i3);
        if (read == -1) {
            return -1;
        }
        this.l = i3 + read;
        this.h.a((long) read);
        return read;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return this.k < this.l;
    }
}
