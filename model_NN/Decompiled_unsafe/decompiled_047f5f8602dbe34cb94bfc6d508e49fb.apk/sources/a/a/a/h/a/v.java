package a.a.a.h.a;

import java.io.UnsupportedEncodingException;

class v extends t {

    /* renamed from: a  reason: collision with root package name */
    protected byte[] f90a = new byte[8];
    protected String b;
    protected byte[] c;
    protected int d;

    v(String str) {
        super(str, 2);
        a(this.f90a, 24);
        this.d = a(20);
        if ((this.d & 1) == 0) {
            throw new o("NTLM type 2 message indicates no support for Unicode. Flags are: " + Integer.toString(this.d));
        }
        this.b = null;
        if (a() >= 20) {
            byte[] b2 = b(12);
            if (b2.length != 0) {
                try {
                    this.b = new String(b2, "UnicodeLittleUnmarked");
                } catch (UnsupportedEncodingException e) {
                    throw new o(e.getMessage(), e);
                }
            }
        }
        this.c = null;
        if (a() >= 48) {
            byte[] b3 = b(40);
            if (b3.length != 0) {
                this.c = b3;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public byte[] c() {
        return this.f90a;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public byte[] e() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.d;
    }
}
