package a.a.a.h.c;

import a.a.a.e.r;
import a.a.a.e.s;
import a.a.a.n;
import a.a.a.n.a;

public class h implements r {

    /* renamed from: a  reason: collision with root package name */
    public static final h f122a = new h();

    public int a(n nVar) {
        a.a(nVar, "HTTP host");
        int b = nVar.b();
        if (b > 0) {
            return b;
        }
        String c = nVar.c();
        if (c.equalsIgnoreCase("http")) {
            return 80;
        }
        if (c.equalsIgnoreCase("https")) {
            return 443;
        }
        throw new s(c + " protocol is not supported");
    }
}
