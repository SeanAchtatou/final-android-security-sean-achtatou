package a.a.a.h.a;

import a.a.a.a.c;
import a.a.a.a.d;
import a.a.a.a.e;

public class m implements d, e {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f83a;
    private final boolean b;

    public m() {
        this(true, true);
    }

    public m(boolean z, boolean z2) {
        this.f83a = z;
        this.b = z2;
    }

    public c a(a.a.a.k.e eVar) {
        return new l(this.f83a, this.b);
    }

    public c a(a.a.a.m.e eVar) {
        return new l(this.f83a, this.b);
    }
}
