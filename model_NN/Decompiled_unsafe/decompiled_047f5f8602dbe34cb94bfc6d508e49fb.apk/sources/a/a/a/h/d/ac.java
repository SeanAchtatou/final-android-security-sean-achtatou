package a.a.a.h.d;

import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.h;
import a.a.a.f.n;
import a.a.a.f.o;
import a.a.a.n.a;

public class ac extends a implements b {
    public String a() {
        return "version";
    }

    public void a(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        if (cVar.h() < 0) {
            throw new h("Cookie version may not be negative");
        }
    }

    public void a(o oVar, String str) {
        a.a(oVar, "Cookie");
        if (str == null) {
            throw new n("Missing value for version attribute");
        } else if (str.trim().isEmpty()) {
            throw new n("Blank value for version attribute");
        } else {
            try {
                oVar.a(Integer.parseInt(str));
            } catch (NumberFormatException e) {
                throw new n("Invalid version: " + e.getMessage());
            }
        }
    }
}
