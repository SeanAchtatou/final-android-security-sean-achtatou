package a.a.a.h.d;

import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.h;
import a.a.a.f.n;
import a.a.a.f.o;
import a.a.a.n.a;
import java.util.Locale;

public class af implements b {
    public String a() {
        return "domain";
    }

    public void a(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        String lowerCase = fVar.a().toLowerCase(Locale.ROOT);
        if (cVar.d() == null) {
            throw new h("Invalid cookie state: domain not specified");
        }
        String lowerCase2 = cVar.d().toLowerCase(Locale.ROOT);
        if (!(cVar instanceof a.a.a.f.a) || !((a.a.a.f.a) cVar).b("domain")) {
            if (!cVar.d().equals(lowerCase)) {
                throw new h("Illegal domain attribute: \"" + cVar.d() + "\"." + "Domain of origin: \"" + lowerCase + "\"");
            }
        } else if (!lowerCase2.startsWith(".")) {
            throw new h("Domain attribute \"" + cVar.d() + "\" violates RFC 2109: domain must start with a dot");
        } else {
            int indexOf = lowerCase2.indexOf(46, 1);
            if ((indexOf < 0 || indexOf == lowerCase2.length() - 1) && !lowerCase2.equals(".local")) {
                throw new h("Domain attribute \"" + cVar.d() + "\" violates RFC 2965: the value contains no embedded dots " + "and the value is not .local");
            } else if (!a(lowerCase, lowerCase2)) {
                throw new h("Domain attribute \"" + cVar.d() + "\" violates RFC 2965: effective host name does not " + "domain-match domain attribute.");
            } else if (lowerCase.substring(0, lowerCase.length() - lowerCase2.length()).indexOf(46) != -1) {
                throw new h("Domain attribute \"" + cVar.d() + "\" violates RFC 2965: " + "effective host minus domain may not contain any dots");
            }
        }
    }

    public void a(o oVar, String str) {
        a.a(oVar, "Cookie");
        if (str == null) {
            throw new n("Missing value for domain attribute");
        } else if (str.trim().isEmpty()) {
            throw new n("Blank value for domain attribute");
        } else {
            String lowerCase = str.toLowerCase(Locale.ROOT);
            if (!str.startsWith(".")) {
                lowerCase = '.' + lowerCase;
            }
            oVar.d(lowerCase);
        }
    }

    public boolean a(String str, String str2) {
        return str.equals(str2) || (str2.startsWith(".") && str.endsWith(str2));
    }

    public boolean b(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        String lowerCase = fVar.a().toLowerCase(Locale.ROOT);
        String d = cVar.d();
        return a(lowerCase, d) && lowerCase.substring(0, lowerCase.length() - d.length()).indexOf(46) == -1;
    }
}
