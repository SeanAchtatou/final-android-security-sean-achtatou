package a.a.a.h.c;

import a.a.a.e.b;
import a.a.a.e.b.h;
import a.a.a.e.d;
import a.a.a.e.o;
import a.a.a.e.q;
import a.a.a.m.e;
import a.a.a.n;
import a.a.a.n.a;
import a.a.a.s;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

@Deprecated
class l implements o {

    /* renamed from: a  reason: collision with root package name */
    private final b f126a;
    private final d b;
    private volatile i c;
    private volatile boolean d = false;
    private volatile long e = Long.MAX_VALUE;

    l(b bVar, d dVar, i iVar) {
        a.a(bVar, "Connection manager");
        a.a(dVar, "Connection operator");
        a.a(iVar, "HTTP pool entry");
        this.f126a = bVar;
        this.b = dVar;
        this.c = iVar;
    }

    private q r() {
        i iVar = this.c;
        if (iVar == null) {
            return null;
        }
        return (q) iVar.g();
    }

    private q s() {
        i iVar = this.c;
        if (iVar != null) {
            return (q) iVar.g();
        }
        throw new c();
    }

    private i t() {
        i iVar = this.c;
        if (iVar != null) {
            return iVar;
        }
        throw new c();
    }

    public s a() {
        return s().a();
    }

    public void a(long j, TimeUnit timeUnit) {
        if (j > 0) {
            this.e = timeUnit.toMillis(j);
        } else {
            this.e = -1;
        }
    }

    public void a(a.a.a.e.b.b bVar, e eVar, a.a.a.k.e eVar2) {
        q qVar;
        a.a(bVar, "Route");
        a.a(eVar2, "HTTP parameters");
        synchronized (this) {
            if (this.c == null) {
                throw new c();
            }
            h a2 = this.c.a();
            a.a.a.n.b.a(a2, "Route tracker");
            a.a.a.n.b.a(!a2.i(), "Connection already open");
            qVar = (q) this.c.g();
        }
        n d2 = bVar.d();
        this.b.a(qVar, d2 != null ? d2 : bVar.a(), bVar.b(), eVar, eVar2);
        synchronized (this) {
            if (this.c == null) {
                throw new InterruptedIOException();
            }
            h a3 = this.c.a();
            if (d2 == null) {
                a3.a(qVar.h());
            } else {
                a3.a(d2, qVar.h());
            }
        }
    }

    public void a(a.a.a.l lVar) {
        s().a(lVar);
    }

    public void a(e eVar, a.a.a.k.e eVar2) {
        n a2;
        q qVar;
        a.a(eVar2, "HTTP parameters");
        synchronized (this) {
            if (this.c == null) {
                throw new c();
            }
            h a3 = this.c.a();
            a.a.a.n.b.a(a3, "Route tracker");
            a.a.a.n.b.a(a3.i(), "Connection not open");
            a.a.a.n.b.a(a3.e(), "Protocol layering without a tunnel not supported");
            a.a.a.n.b.a(!a3.f(), "Multiple protocol layering not supported");
            a2 = a3.a();
            qVar = (q) this.c.g();
        }
        this.b.a(qVar, a2, eVar, eVar2);
        synchronized (this) {
            if (this.c == null) {
                throw new InterruptedIOException();
            }
            this.c.a().c(qVar.h());
        }
    }

    public void a(n nVar, boolean z, a.a.a.k.e eVar) {
        q qVar;
        a.a(nVar, "Next proxy");
        a.a(eVar, "HTTP parameters");
        synchronized (this) {
            if (this.c == null) {
                throw new c();
            }
            h a2 = this.c.a();
            a.a.a.n.b.a(a2, "Route tracker");
            a.a.a.n.b.a(a2.i(), "Connection not open");
            qVar = (q) this.c.g();
        }
        qVar.a(null, nVar, z, eVar);
        synchronized (this) {
            if (this.c == null) {
                throw new InterruptedIOException();
            }
            this.c.a().b(nVar, z);
        }
    }

    public void a(a.a.a.q qVar) {
        s().a(qVar);
    }

    public void a(s sVar) {
        s().a(sVar);
    }

    public void a(Object obj) {
        t().a(obj);
    }

    public void a(boolean z, a.a.a.k.e eVar) {
        n a2;
        q qVar;
        a.a(eVar, "HTTP parameters");
        synchronized (this) {
            if (this.c == null) {
                throw new c();
            }
            h a3 = this.c.a();
            a.a.a.n.b.a(a3, "Route tracker");
            a.a.a.n.b.a(a3.i(), "Connection not open");
            a.a.a.n.b.a(!a3.e(), "Connection is already tunnelled");
            a2 = a3.a();
            qVar = (q) this.c.g();
        }
        qVar.a(null, a2, z, eVar);
        synchronized (this) {
            if (this.c == null) {
                throw new InterruptedIOException();
            }
            this.c.a().b(z);
        }
    }

    public boolean a(int i) {
        return s().a(i);
    }

    public void b() {
        s().b();
    }

    public void b(int i) {
        s().b(i);
    }

    public boolean c() {
        q r = r();
        if (r != null) {
            return r.c();
        }
        return false;
    }

    public void close() {
        i iVar = this.c;
        if (iVar != null) {
            iVar.a().h();
            ((q) iVar.g()).close();
        }
    }

    public boolean d() {
        q r = r();
        if (r != null) {
            return r.d();
        }
        return true;
    }

    public void e() {
        i iVar = this.c;
        if (iVar != null) {
            iVar.a().h();
            ((q) iVar.g()).e();
        }
    }

    public InetAddress f() {
        return s().f();
    }

    public int g() {
        return s().g();
    }

    public void h() {
        synchronized (this) {
            if (this.c != null) {
                this.f126a.a(this, this.e, TimeUnit.MILLISECONDS);
                this.c = null;
            }
        }
    }

    public void i() {
        synchronized (this) {
            if (this.c != null) {
                this.d = false;
                try {
                    ((q) this.c.g()).e();
                } catch (IOException e2) {
                }
                this.f126a.a(this, this.e, TimeUnit.MILLISECONDS);
                this.c = null;
            }
        }
    }

    public a.a.a.e.b.b j() {
        return t().c();
    }

    public void k() {
        this.d = true;
    }

    public void l() {
        this.d = false;
    }

    public SSLSession m() {
        Socket i = s().i();
        if (i instanceof SSLSocket) {
            return ((SSLSocket) i).getSession();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public i n() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public i o() {
        i iVar = this.c;
        this.c = null;
        return iVar;
    }

    public b p() {
        return this.f126a;
    }

    public boolean q() {
        return this.d;
    }
}
