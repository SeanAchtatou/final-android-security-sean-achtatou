package a.a.a.h.d;

import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.h;
import a.a.a.f.n;
import a.a.a.f.o;
import a.a.a.f.p;
import a.a.a.n.a;
import java.util.StringTokenizer;

public class ag implements b {
    private static boolean a(int i, int[] iArr) {
        for (int i2 : iArr) {
            if (i == i2) {
                return true;
            }
        }
        return false;
    }

    private static int[] a(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, ",");
        int[] iArr = new int[stringTokenizer.countTokens()];
        int i = 0;
        while (stringTokenizer.hasMoreTokens()) {
            try {
                iArr[i] = Integer.parseInt(stringTokenizer.nextToken().trim());
                if (iArr[i] < 0) {
                    throw new n("Invalid Port attribute.");
                }
                i++;
            } catch (NumberFormatException e) {
                throw new n("Invalid Port attribute: " + e.getMessage());
            }
        }
        return iArr;
    }

    public String a() {
        return "port";
    }

    public void a(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        int c = fVar.c();
        if ((cVar instanceof a.a.a.f.a) && ((a.a.a.f.a) cVar).b("port") && !a(c, cVar.f())) {
            throw new h("Port attribute violates RFC 2965: Request port not found in cookie's port list.");
        }
    }

    public void a(o oVar, String str) {
        a.a(oVar, "Cookie");
        if (oVar instanceof p) {
            p pVar = (p) oVar;
            if (str != null && !str.trim().isEmpty()) {
                pVar.a(a(str));
            }
        }
    }

    public boolean b(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        int c = fVar.c();
        if ((cVar instanceof a.a.a.f.a) && ((a.a.a.f.a) cVar).b("port")) {
            if (cVar.f() == null) {
                return false;
            }
            if (!a(c, cVar.f())) {
                return false;
            }
        }
        return true;
    }
}
