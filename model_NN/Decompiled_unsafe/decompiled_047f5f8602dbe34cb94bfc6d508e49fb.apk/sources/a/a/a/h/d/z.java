package a.a.a.h.d;

import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.h;
import a.a.a.f.n;
import a.a.a.f.o;
import a.a.a.n.a;
import java.util.Locale;

public class z implements b {
    public String a() {
        return "domain";
    }

    public void a(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        String a2 = fVar.a();
        String d = cVar.d();
        if (d == null) {
            throw new h("Cookie domain may not be null");
        } else if (d.equals(a2)) {
        } else {
            if (d.indexOf(46) == -1) {
                throw new h("Domain attribute \"" + d + "\" does not match the host \"" + a2 + "\"");
            } else if (!d.startsWith(".")) {
                throw new h("Domain attribute \"" + d + "\" violates RFC 2109: domain must start with a dot");
            } else {
                int indexOf = d.indexOf(46, 1);
                if (indexOf < 0 || indexOf == d.length() - 1) {
                    throw new h("Domain attribute \"" + d + "\" violates RFC 2109: domain must contain an embedded dot");
                }
                String lowerCase = a2.toLowerCase(Locale.ROOT);
                if (!lowerCase.endsWith(d)) {
                    throw new h("Illegal domain attribute \"" + d + "\". Domain of origin: \"" + lowerCase + "\"");
                } else if (lowerCase.substring(0, lowerCase.length() - d.length()).indexOf(46) != -1) {
                    throw new h("Domain attribute \"" + d + "\" violates RFC 2109: host minus domain may not contain any dots");
                }
            }
        }
    }

    public void a(o oVar, String str) {
        a.a(oVar, "Cookie");
        if (str == null) {
            throw new n("Missing value for domain attribute");
        } else if (str.trim().isEmpty()) {
            throw new n("Blank value for domain attribute");
        } else {
            oVar.d(str);
        }
    }

    public boolean b(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        String a2 = fVar.a();
        String d = cVar.d();
        if (d == null) {
            return false;
        }
        return a2.equals(d) || (d.startsWith(".") && a2.endsWith(d));
    }
}
