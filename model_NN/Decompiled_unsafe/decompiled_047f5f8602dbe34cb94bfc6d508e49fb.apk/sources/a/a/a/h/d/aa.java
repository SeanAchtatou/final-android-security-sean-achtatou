package a.a.a.h.d;

import a.a.a.e;
import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.g;
import a.a.a.f.h;
import a.a.a.f.n;
import a.a.a.j.p;
import a.a.a.n.a;
import a.a.a.n.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class aa extends r {

    /* renamed from: a  reason: collision with root package name */
    static final String[] f130a = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy"};
    private final boolean b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.h.d.aa.<init>(java.lang.String[], boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      a.a.a.h.d.aa.<init>(boolean, a.a.a.f.b[]):void
      a.a.a.h.d.aa.<init>(java.lang.String[], boolean):void */
    public aa() {
        this((String[]) null, false);
    }

    protected aa(boolean z, b... bVarArr) {
        super(bVarArr);
        this.b = z;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public aa(java.lang.String[] r5, boolean r6) {
        /*
            r4 = this;
            r0 = 7
            a.a.a.f.b[] r1 = new a.a.a.f.b[r0]
            r0 = 0
            a.a.a.h.d.ac r2 = new a.a.a.h.d.ac
            r2.<init>()
            r1[r0] = r2
            r0 = 1
            a.a.a.h.d.i r2 = new a.a.a.h.d.i
            r2.<init>()
            r1[r0] = r2
            r0 = 2
            a.a.a.h.d.z r2 = new a.a.a.h.d.z
            r2.<init>()
            r1[r0] = r2
            r0 = 3
            a.a.a.h.d.h r2 = new a.a.a.h.d.h
            r2.<init>()
            r1[r0] = r2
            r0 = 4
            a.a.a.h.d.j r2 = new a.a.a.h.d.j
            r2.<init>()
            r1[r0] = r2
            r0 = 5
            a.a.a.h.d.e r2 = new a.a.a.h.d.e
            r2.<init>()
            r1[r0] = r2
            r2 = 6
            a.a.a.h.d.g r3 = new a.a.a.h.d.g
            if (r5 == 0) goto L_0x0049
            java.lang.Object r0 = r5.clone()
            java.lang.String[] r0 = (java.lang.String[]) r0
        L_0x003e:
            r3.<init>(r0)
            r1[r2] = r3
            r4.<init>(r1)
            r4.b = r6
            return
        L_0x0049:
            java.lang.String[] r0 = a.a.a.h.d.aa.f130a
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.h.d.aa.<init>(java.lang.String[], boolean):void");
    }

    private List b(List list) {
        int i;
        int i2 = Integer.MAX_VALUE;
        Iterator it = list.iterator();
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                break;
            }
            c cVar = (c) it.next();
            i2 = cVar.h() < i ? cVar.h() : i;
        }
        d dVar = new d(list.size() * 40);
        dVar.a("Cookie");
        dVar.a(": ");
        dVar.a("$Version=");
        dVar.a(Integer.toString(i));
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            dVar.a("; ");
            a(dVar, (c) it2.next(), i);
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new p(dVar));
        return arrayList;
    }

    private List c(List list) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            int h = cVar.h();
            d dVar = new d(40);
            dVar.a("Cookie: ");
            dVar.a("$Version=");
            dVar.a(Integer.toString(h));
            dVar.a("; ");
            a(dVar, cVar, h);
            arrayList.add(new p(dVar));
        }
        return arrayList;
    }

    public int a() {
        return 1;
    }

    public List a(e eVar, f fVar) {
        a.a(eVar, "Header");
        a.a(fVar, "Cookie origin");
        if (eVar.c().equalsIgnoreCase("Set-Cookie")) {
            return a(eVar.e(), fVar);
        }
        throw new n("Unrecognized cookie header '" + eVar.toString() + "'");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
     arg types: [java.util.List, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection */
    public List a(List list) {
        a.a((Collection) list, "List of cookies");
        if (list.size() > 1) {
            ArrayList arrayList = new ArrayList(list);
            Collections.sort(arrayList, g.f50a);
            list = arrayList;
        }
        return this.b ? b(list) : c(list);
    }

    public void a(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        String a2 = cVar.a();
        if (a2.indexOf(32) != -1) {
            throw new h("Cookie name may not contain blanks");
        } else if (a2.startsWith("$")) {
            throw new h("Cookie name may not start with $");
        } else {
            super.a(cVar, fVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a(d dVar, c cVar, int i) {
        a(dVar, cVar.a(), cVar.b(), i);
        if (cVar.e() != null && (cVar instanceof a.a.a.f.a) && ((a.a.a.f.a) cVar).b("path")) {
            dVar.a("; ");
            a(dVar, "$Path", cVar.e(), i);
        }
        if (cVar.d() != null && (cVar instanceof a.a.a.f.a) && ((a.a.a.f.a) cVar).b("domain")) {
            dVar.a("; ");
            a(dVar, "$Domain", cVar.d(), i);
        }
    }

    /* access modifiers changed from: protected */
    public void a(d dVar, String str, String str2, int i) {
        dVar.a(str);
        dVar.a("=");
        if (str2 == null) {
            return;
        }
        if (i > 0) {
            dVar.a('\"');
            dVar.a(str2);
            dVar.a('\"');
            return;
        }
        dVar.a(str2);
    }

    public e b() {
        return null;
    }

    public String toString() {
        return "rfc2109";
    }
}
