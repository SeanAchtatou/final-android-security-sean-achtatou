package a.a.a.h.d;

import a.a.a.f.b;
import a.a.a.f.n;
import a.a.a.f.o;
import a.a.a.n.a;

public class q extends a implements b {
    public String a() {
        return "version";
    }

    public void a(o oVar, String str) {
        a.a(oVar, "Cookie");
        if (str == null) {
            throw new n("Missing value for version attribute");
        }
        int i = 0;
        try {
            i = Integer.parseInt(str);
        } catch (NumberFormatException e) {
        }
        oVar.a(i);
    }
}
