package a.a.a.h.b;

import a.a.a.a.h;
import a.a.a.a.n;
import a.a.a.b.i;
import a.a.a.n.a;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class f implements i {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f100a = new ConcurrentHashMap();

    private static n a(Map map, h hVar) {
        int i;
        n nVar = (n) map.get(hVar);
        if (nVar != null) {
            return nVar;
        }
        int i2 = -1;
        h hVar2 = null;
        for (h hVar3 : map.keySet()) {
            int a2 = hVar.a(hVar3);
            if (a2 > i2) {
                i = a2;
            } else {
                hVar3 = hVar2;
                i = i2;
            }
            i2 = i;
            hVar2 = hVar3;
        }
        return hVar2 != null ? (n) map.get(hVar2) : nVar;
    }

    public n a(h hVar) {
        a.a(hVar, "Authentication scope");
        return a(this.f100a, hVar);
    }

    public String toString() {
        return this.f100a.toString();
    }
}
