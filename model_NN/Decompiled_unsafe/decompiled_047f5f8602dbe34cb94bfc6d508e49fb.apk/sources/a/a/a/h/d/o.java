package a.a.a.h.d;

import a.a.a.f.i;
import a.a.a.f.j;
import a.a.a.f.k;
import a.a.a.k.e;
import java.util.Collection;

@Deprecated
public class o implements j, k {

    /* renamed from: a  reason: collision with root package name */
    private final p f139a;
    private final i b;

    public o() {
        this(null, p.SECURITYLEVEL_DEFAULT);
    }

    public o(String[] strArr, p pVar) {
        this.f139a = pVar;
        this.b = new m(strArr, pVar);
    }

    public i a(e eVar) {
        if (eVar == null) {
            return new m(null, this.f139a);
        }
        Collection collection = (Collection) eVar.a("http.protocol.cookie-datepatterns");
        return new m(collection != null ? (String[]) collection.toArray(new String[collection.size()]) : null, this.f139a);
    }

    public i a(a.a.a.m.e eVar) {
        return this.b;
    }
}
