package a.a.a.h;

import a.a.a.h.f.p;
import a.a.a.i.g;
import a.a.a.k.e;
import a.a.a.n.a;
import a.a.a.n.b;
import a.a.a.o;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;

@Deprecated
public class f extends a implements o {

    /* renamed from: a  reason: collision with root package name */
    private volatile boolean f149a;
    private volatile Socket b = null;

    private static void a(StringBuilder sb, SocketAddress socketAddress) {
        if (socketAddress instanceof InetSocketAddress) {
            InetSocketAddress inetSocketAddress = (InetSocketAddress) socketAddress;
            sb.append(inetSocketAddress.getAddress() != null ? inetSocketAddress.getAddress().getHostAddress() : inetSocketAddress.getAddress()).append(':').append(inetSocketAddress.getPort());
            return;
        }
        sb.append(socketAddress);
    }

    /* access modifiers changed from: protected */
    public a.a.a.i.f a(Socket socket, int i, e eVar) {
        return new a.a.a.h.f.o(socket, i, eVar);
    }

    /* access modifiers changed from: protected */
    public void a(Socket socket, e eVar) {
        a.a(socket, "Socket");
        a.a(eVar, "HTTP parameters");
        this.b = socket;
        int a2 = eVar.a("http.socket.buffer-size", -1);
        a(a(socket, a2, eVar), b(socket, a2, eVar), eVar);
        this.f149a = true;
    }

    /* access modifiers changed from: protected */
    public g b(Socket socket, int i, e eVar) {
        return new p(socket, i, eVar);
    }

    public void b(int i) {
        j();
        if (this.b != null) {
            try {
                this.b.setSoTimeout(i);
            } catch (SocketException e) {
            }
        }
    }

    public boolean c() {
        return this.f149a;
    }

    public void close() {
        if (this.f149a) {
            this.f149a = false;
            Socket socket = this.b;
            try {
                o();
                try {
                    socket.shutdownOutput();
                } catch (IOException e) {
                }
                try {
                    socket.shutdownInput();
                } catch (IOException | UnsupportedOperationException e2) {
                }
            } finally {
                socket.close();
            }
        }
    }

    public void e() {
        this.f149a = false;
        Socket socket = this.b;
        if (socket != null) {
            socket.close();
        }
    }

    public InetAddress f() {
        if (this.b != null) {
            return this.b.getInetAddress();
        }
        return null;
    }

    public int g() {
        if (this.b != null) {
            return this.b.getPort();
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public Socket i() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void j() {
        b.a(this.f149a, "Connection is not open");
    }

    /* access modifiers changed from: protected */
    public void q() {
        b.a(!this.f149a, "Connection is already open");
    }

    public String toString() {
        if (this.b == null) {
            return super.toString();
        }
        StringBuilder sb = new StringBuilder();
        SocketAddress remoteSocketAddress = this.b.getRemoteSocketAddress();
        SocketAddress localSocketAddress = this.b.getLocalSocketAddress();
        if (!(remoteSocketAddress == null || localSocketAddress == null)) {
            a(sb, localSocketAddress);
            sb.append("<->");
            a(sb, remoteSocketAddress);
        }
        return sb.toString();
    }
}
