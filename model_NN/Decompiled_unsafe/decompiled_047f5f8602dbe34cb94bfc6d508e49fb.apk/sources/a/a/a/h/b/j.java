package a.a.a.h.b;

import a.a.a.e.g;
import a.a.a.f;
import a.a.a.j.d;
import a.a.a.m.e;
import a.a.a.n.a;
import a.a.a.s;

public class j implements g {

    /* renamed from: a  reason: collision with root package name */
    public static final j f104a = new j();

    public long a(s sVar, e eVar) {
        a.a(sVar, "HTTP response");
        d dVar = new d(sVar.e("Keep-Alive"));
        while (dVar.hasNext()) {
            f a2 = dVar.a();
            String a3 = a2.a();
            String b = a2.b();
            if (b != null && a3.equalsIgnoreCase("timeout")) {
                try {
                    return Long.parseLong(b) * 1000;
                } catch (NumberFormatException e) {
                }
            }
        }
        return -1;
    }
}
