package a.a.a.h.c;

import a.a.a.e.b.b;
import a.a.a.e.b.h;
import a.a.a.e.q;
import a.a.a.l.a;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

@Deprecated
class i extends a {

    /* renamed from: a  reason: collision with root package name */
    private final Log f123a;
    private final h b;

    public i(Log log, String str, b bVar, q qVar, long j, TimeUnit timeUnit) {
        super(str, bVar, qVar, j, timeUnit);
        this.f123a = log;
        this.b = new h(bVar);
    }

    /* access modifiers changed from: package-private */
    public h a() {
        return this.b;
    }

    public boolean a(long j) {
        boolean a2 = super.a(j);
        if (a2 && this.f123a.isDebugEnabled()) {
            this.f123a.debug("Connection " + this + " expired @ " + new Date(h()));
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public b b() {
        return (b) f();
    }

    /* access modifiers changed from: package-private */
    public b c() {
        return this.b.j();
    }

    public boolean d() {
        return !((q) g()).c();
    }

    public void e() {
        try {
            ((q) g()).close();
        } catch (IOException e) {
            this.f123a.debug("I/O error closing connection", e);
        }
    }
}
