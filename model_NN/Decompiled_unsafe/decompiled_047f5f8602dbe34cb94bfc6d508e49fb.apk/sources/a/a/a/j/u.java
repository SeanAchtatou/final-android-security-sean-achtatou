package a.a.a.j;

public class u {

    /* renamed from: a  reason: collision with root package name */
    private final int f178a;
    private final int b;
    private int c;

    public u(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Lower bound cannot be negative");
        } else if (i > i2) {
            throw new IndexOutOfBoundsException("Lower bound cannot be greater then upper bound");
        } else {
            this.f178a = i;
            this.b = i2;
            this.c = i;
        }
    }

    public int a() {
        return this.b;
    }

    public void a(int i) {
        if (i < this.f178a) {
            throw new IndexOutOfBoundsException("pos: " + i + " < lowerBound: " + this.f178a);
        } else if (i > this.b) {
            throw new IndexOutOfBoundsException("pos: " + i + " > upperBound: " + this.b);
        } else {
            this.c = i;
        }
    }

    public int b() {
        return this.c;
    }

    public boolean c() {
        return this.c >= this.b;
    }

    public String toString() {
        return '[' + Integer.toString(this.f178a) + '>' + Integer.toString(this.c) + '>' + Integer.toString(this.b) + ']';
    }
}
