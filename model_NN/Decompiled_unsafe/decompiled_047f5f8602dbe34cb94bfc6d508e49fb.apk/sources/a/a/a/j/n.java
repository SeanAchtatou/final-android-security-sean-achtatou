package a.a.a.j;

import a.a.a.ac;
import a.a.a.af;
import a.a.a.n.a;
import a.a.a.n.d;
import java.io.Serializable;

public class n implements af, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final ac f174a;
    private final int b;
    private final String c;

    public n(ac acVar, int i, String str) {
        this.f174a = (ac) a.a(acVar, "Version");
        this.b = a.b(i, "Status code");
        this.c = str;
    }

    public ac a() {
        return this.f174a;
    }

    public int b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public Object clone() {
        return super.clone();
    }

    public String toString() {
        return i.b.a((d) null, this).toString();
    }
}
