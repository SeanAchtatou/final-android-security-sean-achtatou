package a.a.a.j;

import a.a.a.e;
import a.a.a.h;
import a.a.a.n.a;
import a.a.a.n.b;
import java.util.List;
import java.util.NoSuchElementException;

public class k implements h {

    /* renamed from: a  reason: collision with root package name */
    protected final List f171a;
    protected int b = a(-1);
    protected int c = -1;
    protected String d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.util.List, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public k(List list, String str) {
        this.f171a = (List) a.a((Object) list, "Header list");
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        if (i < -1) {
            return -1;
        }
        int size = this.f171a.size() - 1;
        boolean z = false;
        int i2 = i;
        while (!z && i2 < size) {
            int i3 = i2 + 1;
            z = b(i3);
            i2 = i3;
        }
        if (!z) {
            i2 = -1;
        }
        return i2;
    }

    public e a() {
        int i = this.b;
        if (i < 0) {
            throw new NoSuchElementException("Iteration already finished.");
        }
        this.c = i;
        this.b = a(i);
        return (e) this.f171a.get(i);
    }

    /* access modifiers changed from: protected */
    public boolean b(int i) {
        if (this.d == null) {
            return true;
        }
        return this.d.equalsIgnoreCase(((e) this.f171a.get(i)).c());
    }

    public boolean hasNext() {
        return this.b >= 0;
    }

    public final Object next() {
        return a();
    }

    public void remove() {
        b.a(this.c >= 0, "No header to remove");
        this.f171a.remove(this.c);
        this.c = -1;
        this.b--;
    }
}
