package a.a.a.g.a;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private final String f63a;
    private final String b;

    public j(String str, String str2) {
        this.f63a = str;
        this.b = str2;
    }

    public String a() {
        return this.f63a;
    }

    public String b() {
        return this.b;
    }

    public String toString() {
        return this.f63a + ": " + this.b;
    }
}
