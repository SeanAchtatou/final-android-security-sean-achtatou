package a.a.a.g.a.a;

import a.a.a.g.e;
import a.a.a.n.a;
import java.io.OutputStream;

public class b extends a {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f56a;
    private final String b;

    public b(byte[] bArr, e eVar, String str) {
        super(eVar);
        a.a(bArr, "byte[]");
        this.f56a = bArr;
        this.b = str;
    }

    public void a(OutputStream outputStream) {
        outputStream.write(this.f56a);
    }

    public String c() {
        return null;
    }

    public String d() {
        return this.b;
    }

    public String e() {
        return "binary";
    }

    public long f() {
        return (long) this.f56a.length;
    }
}
