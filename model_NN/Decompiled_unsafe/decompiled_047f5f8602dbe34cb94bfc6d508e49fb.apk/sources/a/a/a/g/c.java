package a.a.a.g;

import a.a.a.k;
import a.a.a.n.a;
import a.a.a.n.g;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class c extends f {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f67a;

    public c(k kVar) {
        super(kVar);
        if (!kVar.a() || kVar.c() < 0) {
            this.f67a = g.b(kVar);
        } else {
            this.f67a = null;
        }
    }

    public void a(OutputStream outputStream) {
        a.a(outputStream, "Output stream");
        if (this.f67a != null) {
            outputStream.write(this.f67a);
        } else {
            super.a(outputStream);
        }
    }

    public boolean a() {
        return true;
    }

    public boolean b() {
        return this.f67a == null && super.b();
    }

    public long c() {
        return this.f67a != null ? (long) this.f67a.length : super.c();
    }

    public InputStream f() {
        return this.f67a != null ? new ByteArrayInputStream(this.f67a) : super.f();
    }

    public boolean g() {
        return this.f67a == null && super.g();
    }
}
