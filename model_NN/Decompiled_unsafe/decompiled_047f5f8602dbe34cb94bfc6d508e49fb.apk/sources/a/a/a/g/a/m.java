package a.a.a.g.a;

import a.a.a.e;
import a.a.a.j.b;
import a.a.a.k;
import java.io.InputStream;
import java.io.OutputStream;

class m implements k {

    /* renamed from: a  reason: collision with root package name */
    private final a f66a;
    private final e b;
    private final long c;

    m(a aVar, a.a.a.g.e eVar, long j) {
        this.f66a = aVar;
        this.b = new b("Content-Type", eVar.toString());
        this.c = j;
    }

    public void a(OutputStream outputStream) {
        this.f66a.a(outputStream);
    }

    public boolean a() {
        return this.c != -1;
    }

    public boolean b() {
        return !a();
    }

    public long c() {
        return this.c;
    }

    public e d() {
        return this.b;
    }

    public e e() {
        return null;
    }

    public InputStream f() {
        throw new UnsupportedOperationException("Multipart form entity does not implement #getContent()");
    }

    public boolean g() {
        return !a();
    }
}
