package a.a.a.g.a;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class d implements Iterable {

    /* renamed from: a  reason: collision with root package name */
    private final List f60a = new LinkedList();
    private final Map b = new HashMap();

    public j a(String str) {
        if (str == null) {
            return null;
        }
        List list = (List) this.b.get(str.toLowerCase(Locale.ROOT));
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (j) list.get(0);
    }

    public List a() {
        return new ArrayList(this.f60a);
    }

    public void a(j jVar) {
        if (jVar != null) {
            String lowerCase = jVar.a().toLowerCase(Locale.ROOT);
            Object obj = (List) this.b.get(lowerCase);
            if (obj == null) {
                obj = new LinkedList();
                this.b.put(lowerCase, obj);
            }
            obj.add(jVar);
            this.f60a.add(jVar);
        }
    }

    public Iterator iterator() {
        return Collections.unmodifiableList(this.f60a).iterator();
    }

    public String toString() {
        return this.f60a.toString();
    }
}
