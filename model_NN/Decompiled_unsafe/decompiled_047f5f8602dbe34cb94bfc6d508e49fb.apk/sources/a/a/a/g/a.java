package a.a.a.g;

import a.a.a.e;
import a.a.a.j.b;
import a.a.a.k;

public abstract class a implements k {

    /* renamed from: a  reason: collision with root package name */
    protected e f53a;
    protected e b;
    protected boolean c;

    protected a() {
    }

    public void a(e eVar) {
        this.f53a = eVar;
    }

    public void a(String str) {
        b bVar = null;
        if (str != null) {
            bVar = new b("Content-Type", str);
        }
        a(bVar);
    }

    public void a(boolean z) {
        this.c = z;
    }

    public void b(e eVar) {
        this.b = eVar;
    }

    public boolean b() {
        return this.c;
    }

    public e d() {
        return this.f53a;
    }

    public e e() {
        return this.b;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        if (this.f53a != null) {
            sb.append("Content-Type: ");
            sb.append(this.f53a.d());
            sb.append(',');
        }
        if (this.b != null) {
            sb.append("Content-Encoding: ");
            sb.append(this.b.d());
            sb.append(',');
        }
        long c2 = c();
        if (c2 >= 0) {
            sb.append("Content-Length: ");
            sb.append(c2);
            sb.append(',');
        }
        sb.append("Chunked: ");
        sb.append(this.c);
        sb.append(']');
        return sb.toString();
    }
}
