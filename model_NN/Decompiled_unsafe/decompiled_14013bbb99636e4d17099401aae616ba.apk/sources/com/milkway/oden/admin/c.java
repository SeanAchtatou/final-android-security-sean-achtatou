package com.milkway.oden.admin;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.droid641.android920.R;

class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d92kh62k f200a;
    private Button b;
    /* access modifiers changed from: private */
    public View c;
    private Context d;
    /* access modifiers changed from: private */
    public WindowManager e;
    private TextView f;
    private LinearLayout g;

    c(d92kh62k d92kh62k, Context context, TextView textView, Button button, WindowManager windowManager, View view, LinearLayout linearLayout) {
        this.f200a = d92kh62k;
        this.e = windowManager;
        this.b = button;
        this.c = view;
        this.g = linearLayout;
        this.d = context;
        this.f = textView;
    }

    public void onClick(View view) {
        this.f.setText(this.d.getString(R.string.ad_loading_text));
        this.b.setVisibility(8);
        this.g.setVisibility(0);
        new Handler().postDelayed(new d(this), 7000);
    }
}
