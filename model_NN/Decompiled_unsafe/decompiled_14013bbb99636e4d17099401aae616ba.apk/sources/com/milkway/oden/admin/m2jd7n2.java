package com.milkway.oden.admin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.milkway.oden.a;
import com.milkway.oden.u72js82jd;

public class m2jd7n2 extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private static m2jd7n2 f203a;

    public static m2jd7n2 a() {
        return f203a;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 8) {
            if (i2 != -1) {
                d80ckq.a(getApplicationContext());
            } else {
                Intent intent2 = new Intent(this, u72js82jd.class);
                intent2.addFlags(268435456);
                startActivity(intent2);
            }
            finish();
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Context applicationContext = getApplicationContext();
        f203a = this;
        boolean b = d80ckq.b(applicationContext);
        if (getIntent().getBooleanExtra(a.T, true)) {
            Intent intent = new Intent(applicationContext, d80ckq.class);
            intent.setFlags(268435456);
            applicationContext.startService(intent);
            if (b) {
                finish();
                return;
            }
            return;
        }
        finish();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }
}
