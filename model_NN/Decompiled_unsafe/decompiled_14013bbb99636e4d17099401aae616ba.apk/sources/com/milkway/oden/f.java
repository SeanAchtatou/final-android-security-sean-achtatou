package com.milkway.oden;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class f extends WebViewClient {
    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str != null) {
            try {
                if (str.startsWith(a.ae) || str.startsWith(a.af)) {
                    webView.loadUrl(str);
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
