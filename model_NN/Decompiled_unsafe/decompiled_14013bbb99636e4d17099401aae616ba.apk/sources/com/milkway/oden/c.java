package com.milkway.oden;

import android.content.Context;
import android.content.SharedPreferences;
import org.json.JSONArray;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public int f204a = 0;
    public int b = 0;
    public long c = 0;
    public boolean d = false;
    public String e = "";
    public String f = "";
    public String g = "";
    public String h = "";
    JSONArray i = new JSONArray();
    JSONArray j = new JSONArray();
    JSONArray k = new JSONArray();
    JSONArray l = new JSONArray();

    public static SharedPreferences a(Context context) {
        return context.getSharedPreferences(a.d, 0);
    }

    public static void a(Context context, Boolean bool) {
        try {
            SharedPreferences.Editor b2 = b(context);
            b2.putBoolean(a.z, bool.booleanValue());
            b2.commit();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void a(Context context, JSONObject jSONObject) {
        try {
            JSONArray jSONArray = new JSONArray(a(context).getString(a.M, ""));
            jSONArray.put(jSONObject);
            SharedPreferences.Editor b2 = b(context);
            b2.putString(a.M, jSONArray.toString());
            b2.commit();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static SharedPreferences.Editor b(Context context) {
        return context.getSharedPreferences(a.d, 0).edit();
    }

    public static void b(Context context, Boolean bool) {
        try {
            SharedPreferences.Editor b2 = b(context);
            b2.putBoolean(a.f, bool.booleanValue());
            b2.commit();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static boolean c(Context context) {
        return a(context).getBoolean(a.z, false);
    }

    public static boolean d(Context context) {
        return a(context).getBoolean(a.f, true);
    }

    public int a(String str, String str2) {
        if (this.j.length() > 0) {
            return d.a(str, str2, this.j);
        }
        return 0;
    }

    public int b(String str, String str2) {
        if (this.k.length() > 0) {
            return d.a(str, str2, this.k);
        }
        return 0;
    }

    public int c(String str, String str2) {
        if (this.l.length() > 0) {
            return d.a(str, str2, this.l);
        }
        return 0;
    }

    public boolean e(Context context) {
        this.e = d.c(context);
        this.f = d.b(context);
        try {
            SharedPreferences a2 = a(context);
            if (a2.contains(a.f198a)) {
                this.f204a = a2.getInt(a.b, 60);
                this.g = a2.getString(a.r, "");
                this.h = a2.getString(a.s, "");
                this.b = a2.getInt(a.c, 60);
                this.d = a2.getBoolean(a.z, false);
                this.c = a2.getLong(a.e, 0);
                this.i = new JSONArray(a2.getString(a.M, ""));
                this.j = new JSONArray(a2.getString(a.N, ""));
                this.k = new JSONArray(a2.getString(a.O, ""));
                this.l = new JSONArray(a2.getString(a.P, ""));
                return false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return true;
    }

    public boolean f(Context context) {
        try {
            SharedPreferences.Editor b2 = b(context);
            b2.putInt(a.b, this.f204a);
            b2.putString(a.r, this.g);
            b2.putBoolean(a.f198a, false);
            b2.putString(a.s, this.h);
            b2.putInt(a.c, this.b);
            b2.putLong(a.e, this.c);
            b2.putString(a.M, this.i.toString());
            b2.putString(a.O, this.k.toString());
            b2.putString(a.N, this.j.toString());
            b2.putString(a.P, this.l.toString());
            return b2.commit();
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }
}
