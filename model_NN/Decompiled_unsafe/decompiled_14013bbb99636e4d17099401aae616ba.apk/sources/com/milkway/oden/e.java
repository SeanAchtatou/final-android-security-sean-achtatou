package com.milkway.oden;

import a.a.a.b.c.j;
import a.a.a.g.a.f;
import a.a.a.g.a.i;
import a.a.a.h.b.k;
import a.a.a.j.l;
import a.a.a.k.b;
import a.a.a.k.c;
import a.a.a.n.g;
import a.a.a.v;
import android.content.Context;
import android.os.AsyncTask;
import java.io.IOException;
import java.util.List;

class e extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private Context f205a;
    private String b;
    private String c;
    private List d;

    public e(Context context, String str, List list, String str2) {
        this.f205a = context;
        this.b = str;
        this.d = list;
        this.c = str2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final String doInBackground(Void... voidArr) {
        b bVar = new b();
        c.c(bVar, 5000);
        c.a(bVar, 10000);
        k kVar = new k(bVar);
        kVar.a().a("http.useragent", System.getProperty("http.agent"));
        kVar.a().a("http.protocol.version", v.c);
        j jVar = new j(this.b);
        try {
            a.a.a.g.a.k a2 = a.a.a.g.a.k.a();
            a2.a(f.BROWSER_COMPATIBLE);
            a2.a(i.b);
            for (int i = 0; i < this.d.size(); i++) {
                a2.a(((l) this.d.get(i)).a(), ((l) this.d.get(i)).b(), a.a.a.g.e.a("text/plain", i.b));
            }
            if (this.c != null) {
                a2.a("file", this.c.getBytes(), a.a.a.g.e.h, "file");
            }
            jVar.a("Accept", "application/json");
            jVar.a(a2.c());
            return g.c(kVar.a((a.a.a.b.c.l) jVar).b());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        if (str != null && !str.equals("") && str.length() > 0) {
            k8sm502s.a(this.f205a, str);
        }
    }
}
