package a.a.a.n;

import java.io.Serializable;
import java.nio.CharBuffer;

public final class d implements Serializable, CharSequence {

    /* renamed from: a  reason: collision with root package name */
    private char[] f194a;
    private int b;

    public d(int i) {
        a.b(i, "Buffer capacity");
        this.f194a = new char[i];
    }

    private void c(int i) {
        char[] cArr = new char[Math.max(this.f194a.length << 1, i)];
        System.arraycopy(this.f194a, 0, cArr, 0, this.b);
        this.f194a = cArr;
    }

    public int a(int i, int i2, int i3) {
        if (i2 < 0) {
            i2 = 0;
        }
        if (i3 > this.b) {
            i3 = this.b;
        }
        if (i2 > i3) {
            return -1;
        }
        for (int i4 = i2; i4 < i3; i4++) {
            if (this.f194a[i4] == i) {
                return i4;
            }
        }
        return -1;
    }

    public String a(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Negative beginIndex: " + i);
        } else if (i2 > this.b) {
            throw new IndexOutOfBoundsException("endIndex: " + i2 + " > length: " + this.b);
        } else if (i <= i2) {
            return new String(this.f194a, i, i2 - i);
        } else {
            throw new IndexOutOfBoundsException("beginIndex: " + i + " > endIndex: " + i2);
        }
    }

    public void a() {
        this.b = 0;
    }

    public void a(char c) {
        int i = this.b + 1;
        if (i > this.f194a.length) {
            c(i);
        }
        this.f194a[this.b] = c;
        this.b = i;
    }

    public void a(int i) {
        if (i > 0 && i > this.f194a.length - this.b) {
            c(this.b + i);
        }
    }

    public void a(c cVar, int i, int i2) {
        if (cVar != null) {
            a(cVar.e(), i, i2);
        }
    }

    public void a(d dVar, int i, int i2) {
        if (dVar != null) {
            a(dVar.f194a, i, i2);
        }
    }

    public void a(String str) {
        if (str == null) {
            str = "null";
        }
        int length = str.length();
        int i = this.b + length;
        if (i > this.f194a.length) {
            c(i);
        }
        str.getChars(0, length, this.f194a, this.b);
        this.b = i;
    }

    public void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i < 0 || i > bArr.length || i2 < 0 || i + i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + bArr.length);
            } else if (i2 != 0) {
                int i3 = this.b;
                int i4 = i3 + i2;
                if (i4 > this.f194a.length) {
                    c(i4);
                }
                while (i3 < i4) {
                    this.f194a[i3] = (char) (bArr[i] & 255);
                    i++;
                    i3++;
                }
                this.b = i4;
            }
        }
    }

    public void a(char[] cArr, int i, int i2) {
        if (cArr != null) {
            if (i < 0 || i > cArr.length || i2 < 0 || i + i2 < 0 || i + i2 > cArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + cArr.length);
            } else if (i2 != 0) {
                int i3 = this.b + i2;
                if (i3 > this.f194a.length) {
                    c(i3);
                }
                System.arraycopy(cArr, i, this.f194a, this.b, i2);
                this.b = i3;
            }
        }
    }

    public int b(int i) {
        return a(i, 0, this.b);
    }

    public String b(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Negative beginIndex: " + i);
        } else if (i2 > this.b) {
            throw new IndexOutOfBoundsException("endIndex: " + i2 + " > length: " + this.b);
        } else if (i > i2) {
            throw new IndexOutOfBoundsException("beginIndex: " + i + " > endIndex: " + i2);
        } else {
            while (i < i2 && a.a.a.m.d.a(this.f194a[i])) {
                i++;
            }
            while (i2 > i && a.a.a.m.d.a(this.f194a[i2 - 1])) {
                i2--;
            }
            return new String(this.f194a, i, i2 - i);
        }
    }

    public char[] b() {
        return this.f194a;
    }

    public boolean c() {
        return this.b == 0;
    }

    public char charAt(int i) {
        return this.f194a[i];
    }

    public int length() {
        return this.b;
    }

    public CharSequence subSequence(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Negative beginIndex: " + i);
        } else if (i2 > this.b) {
            throw new IndexOutOfBoundsException("endIndex: " + i2 + " > length: " + this.b);
        } else if (i <= i2) {
            return CharBuffer.wrap(this.f194a, i, i2);
        } else {
            throw new IndexOutOfBoundsException("beginIndex: " + i + " > endIndex: " + i2);
        }
    }

    public String toString() {
        return new String(this.f194a, 0, this.b);
    }
}
