package a.a.a.e;

import a.a.a.g.f;
import a.a.a.k;
import a.a.a.n.g;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;

@Deprecated
public class a extends f implements i, l {

    /* renamed from: a  reason: collision with root package name */
    protected o f31a;
    protected final boolean b;

    public a(k kVar, o oVar, boolean z) {
        super(kVar);
        a.a.a.n.a.a(oVar, "Connection");
        this.f31a = oVar;
        this.b = z;
    }

    private void k() {
        if (this.f31a != null) {
            try {
                if (this.b) {
                    g.a(this.c);
                    this.f31a.k();
                } else {
                    this.f31a.l();
                }
            } finally {
                j();
            }
        }
    }

    public void a(OutputStream outputStream) {
        super.a(outputStream);
        k();
    }

    public boolean a() {
        return false;
    }

    /* JADX INFO: finally extract failed */
    public boolean a(InputStream inputStream) {
        try {
            if (this.f31a != null) {
                if (this.b) {
                    inputStream.close();
                    this.f31a.k();
                } else {
                    this.f31a.l();
                }
            }
            j();
            return false;
        } catch (Throwable th) {
            j();
            throw th;
        }
    }

    public boolean b(InputStream inputStream) {
        boolean c;
        try {
            if (this.f31a != null) {
                if (this.b) {
                    c = this.f31a.c();
                    inputStream.close();
                    this.f31a.k();
                } else {
                    this.f31a.l();
                }
            }
        } catch (SocketException e) {
            if (c) {
                throw e;
            }
        } catch (Throwable th) {
            j();
            throw th;
        }
        j();
        return false;
    }

    public boolean c(InputStream inputStream) {
        if (this.f31a == null) {
            return false;
        }
        this.f31a.i();
        return false;
    }

    public InputStream f() {
        return new k(this.c.f(), this);
    }

    public void h() {
        k();
    }

    public void i() {
        if (this.f31a != null) {
            try {
                this.f31a.i();
            } finally {
                this.f31a = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void j() {
        if (this.f31a != null) {
            try {
                this.f31a.h();
            } finally {
                this.f31a = null;
            }
        }
    }
}
