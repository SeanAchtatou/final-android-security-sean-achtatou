package a.a.a.e.d;

import a.a.a.e.c.a;
import a.a.a.e.c.b;
import a.a.a.e.c.e;
import a.a.a.e.m;
import a.a.a.k.c;
import a.a.a.n;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

@Deprecated
public class g implements b, e {

    /* renamed from: a  reason: collision with root package name */
    public static final i f43a = new b();
    public static final i b = new c();
    public static final i c = new h();
    private final SSLSocketFactory d;
    private final a e;
    private volatile i f;
    private final String[] g;
    private final String[] h;

    public g(SSLContext sSLContext, i iVar) {
        this(((SSLContext) a.a.a.n.a.a(sSLContext, "SSL context")).getSocketFactory(), null, null, iVar);
    }

    public g(SSLSocketFactory sSLSocketFactory, String[] strArr, String[] strArr2, i iVar) {
        this.d = (SSLSocketFactory) a.a.a.n.a.a(sSLSocketFactory, "SSL socket factory");
        this.g = strArr;
        this.h = strArr2;
        this.f = iVar == null ? b : iVar;
        this.e = null;
    }

    public static g a() {
        return new g(e.a(), b);
    }

    private void a(SSLSocket sSLSocket, String str) {
        try {
            this.f.a(str, sSLSocket);
        } catch (IOException e2) {
            try {
                sSLSocket.close();
            } catch (Exception e3) {
            }
            throw e2;
        }
    }

    private void b(SSLSocket sSLSocket) {
        if (this.g != null) {
            sSLSocket.setEnabledProtocols(this.g);
        }
        if (this.h != null) {
            sSLSocket.setEnabledCipherSuites(this.h);
        }
        a(sSLSocket);
    }

    public Socket a(int i, Socket socket, n nVar, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, a.a.a.m.e eVar) {
        a.a.a.n.a.a(nVar, "HTTP host");
        a.a.a.n.a.a(inetSocketAddress, "Remote address");
        Socket a2 = socket != null ? socket : a(eVar);
        if (inetSocketAddress2 != null) {
            a2.bind(inetSocketAddress2);
        }
        try {
            a2.connect(inetSocketAddress, i);
            if (!(a2 instanceof SSLSocket)) {
                return a(a2, nVar.a(), inetSocketAddress.getPort(), eVar);
            }
            SSLSocket sSLSocket = (SSLSocket) a2;
            sSLSocket.startHandshake();
            a(sSLSocket, nVar.a());
            return a2;
        } catch (IOException e2) {
            try {
                a2.close();
            } catch (IOException e3) {
            }
            throw e2;
        }
    }

    public Socket a(a.a.a.k.e eVar) {
        return a((a.a.a.m.e) null);
    }

    public Socket a(a.a.a.m.e eVar) {
        SSLSocket sSLSocket = (SSLSocket) this.d.createSocket();
        b(sSLSocket);
        return sSLSocket;
    }

    public Socket a(Socket socket, String str, int i, a.a.a.k.e eVar) {
        return a(socket, str, i, (a.a.a.m.e) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    public Socket a(Socket socket, String str, int i, a.a.a.m.e eVar) {
        SSLSocket sSLSocket = (SSLSocket) this.d.createSocket(socket, str, i, true);
        b(sSLSocket);
        sSLSocket.startHandshake();
        a(sSLSocket, str);
        return sSLSocket;
    }

    public Socket a(Socket socket, String str, int i, boolean z) {
        return a(socket, str, i, (a.a.a.m.e) null);
    }

    public Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, a.a.a.k.e eVar) {
        a.a.a.n.a.a(inetSocketAddress, "Remote address");
        a.a.a.n.a.a(eVar, "HTTP parameters");
        n a2 = inetSocketAddress instanceof m ? ((m) inetSocketAddress).a() : new n(inetSocketAddress.getHostName(), inetSocketAddress.getPort(), "https");
        int a3 = c.a(eVar);
        int e2 = c.e(eVar);
        socket.setSoTimeout(a3);
        return a(e2, socket, a2, inetSocketAddress, inetSocketAddress2, null);
    }

    /* access modifiers changed from: protected */
    public void a(SSLSocket sSLSocket) {
    }

    public boolean a(Socket socket) {
        a.a.a.n.a.a(socket, "Socket");
        a.a.a.n.b.a(socket instanceof SSLSocket, "Socket not created by this factory");
        a.a.a.n.b.a(!socket.isClosed(), "Socket is closed");
        return true;
    }
}
