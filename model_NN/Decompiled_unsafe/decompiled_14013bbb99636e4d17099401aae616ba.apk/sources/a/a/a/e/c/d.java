package a.a.a.e.c;

import a.a.a.n.a;
import a.a.a.n.h;
import java.util.Locale;

@Deprecated
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final String f38a;
    private final h b;
    private final int c;
    private final boolean d;
    private String e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public d(String str, int i, h hVar) {
        a.a((Object) str, "Scheme name");
        a.a(i > 0 && i <= 65535, "Port is invalid");
        a.a(hVar, "Socket factory");
        this.f38a = str.toLowerCase(Locale.ENGLISH);
        this.c = i;
        if (hVar instanceof e) {
            this.d = true;
            this.b = hVar;
        } else if (hVar instanceof b) {
            this.d = true;
            this.b = new f((b) hVar);
        } else {
            this.d = false;
            this.b = hVar;
        }
    }

    public final int a() {
        return this.c;
    }

    public final int a(int i) {
        return i <= 0 ? this.c : i;
    }

    public final h b() {
        return this.b;
    }

    public final String c() {
        return this.f38a;
    }

    public final boolean d() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        return this.f38a.equals(dVar.f38a) && this.c == dVar.c && this.d == dVar.d;
    }

    public int hashCode() {
        return h.a(h.a(h.a(17, this.c), this.f38a), this.d);
    }

    public final String toString() {
        if (this.e == null) {
            this.e = this.f38a + ':' + Integer.toString(this.c);
        }
        return this.e;
    }
}
