package a.a.a.e.d;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLContext;

@Deprecated
public class e {
    public static SSLContext a() {
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init(null, null, null);
            return instance;
        } catch (NoSuchAlgorithmException e) {
            throw new f(e.getMessage(), e);
        } catch (KeyManagementException e2) {
            throw new f(e2.getMessage(), e2);
        }
    }
}
