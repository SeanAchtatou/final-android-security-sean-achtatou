package a.a.a.e.b;

import a.a.a.n;
import a.a.a.n.a;
import a.a.a.n.h;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class b implements e, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final n f33a;
    private final InetAddress b;
    private final List c;
    private final g d;
    private final f e;
    private final boolean f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.e.b.b.<init>(a.a.a.n, java.net.InetAddress, java.util.List, boolean, a.a.a.e.b.g, a.a.a.e.b.f):void
     arg types: [a.a.a.n, ?[OBJECT, ARRAY], java.util.List, int, a.a.a.e.b.g, a.a.a.e.b.f]
     candidates:
      a.a.a.e.b.b.<init>(a.a.a.n, java.net.InetAddress, a.a.a.n[], boolean, a.a.a.e.b.g, a.a.a.e.b.f):void
      a.a.a.e.b.b.<init>(a.a.a.n, java.net.InetAddress, java.util.List, boolean, a.a.a.e.b.g, a.a.a.e.b.f):void */
    public b(n nVar) {
        this(nVar, (InetAddress) null, Collections.emptyList(), false, g.PLAIN, f.PLAIN);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public b(n nVar, InetAddress inetAddress, n nVar2, boolean z) {
        this(nVar, inetAddress, Collections.singletonList(a.a(nVar2, "Proxy host")), z, z ? g.TUNNELLED : g.PLAIN, z ? f.LAYERED : f.PLAIN);
    }

    private b(n nVar, InetAddress inetAddress, List list, boolean z, g gVar, f fVar) {
        a.a(nVar, "Target host");
        this.f33a = a(nVar);
        this.b = inetAddress;
        if (list == null || list.isEmpty()) {
            this.c = null;
        } else {
            this.c = new ArrayList(list);
        }
        if (gVar == g.TUNNELLED) {
            a.a(this.c != null, "Proxy required if tunnelled");
        }
        this.f = z;
        this.d = gVar == null ? g.PLAIN : gVar;
        this.e = fVar == null ? f.PLAIN : fVar;
    }

    public b(n nVar, InetAddress inetAddress, boolean z) {
        this(nVar, inetAddress, Collections.emptyList(), z, g.PLAIN, f.PLAIN);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public b(n nVar, InetAddress inetAddress, n[] nVarArr, boolean z, g gVar, f fVar) {
        this(nVar, inetAddress, nVarArr != null ? Arrays.asList(nVarArr) : null, z, gVar, fVar);
    }

    private static int a(String str) {
        if ("http".equalsIgnoreCase(str)) {
            return 80;
        }
        return "https".equalsIgnoreCase(str) ? 443 : -1;
    }

    private static n a(n nVar) {
        if (nVar.b() >= 0) {
            return nVar;
        }
        InetAddress d2 = nVar.d();
        String c2 = nVar.c();
        return d2 != null ? new n(d2, a(c2), c2) : new n(nVar.a(), a(c2), c2);
    }

    public final n a() {
        return this.f33a;
    }

    public final n a(int i) {
        a.b(i, "Hop index");
        int c2 = c();
        a.a(i < c2, "Hop index exceeds tracked route length");
        return i < c2 + -1 ? (n) this.c.get(i) : this.f33a;
    }

    public final InetAddress b() {
        return this.b;
    }

    public final int c() {
        if (this.c != null) {
            return this.c.size() + 1;
        }
        return 1;
    }

    public Object clone() {
        return super.clone();
    }

    public final n d() {
        if (this.c == null || this.c.isEmpty()) {
            return null;
        }
        return (n) this.c.get(0);
    }

    public final boolean e() {
        return this.d == g.TUNNELLED;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return this.f == bVar.f && this.d == bVar.d && this.e == bVar.e && h.a(this.f33a, bVar.f33a) && h.a(this.b, bVar.b) && h.a(this.c, bVar.c);
    }

    public final boolean f() {
        return this.e == f.LAYERED;
    }

    public final boolean g() {
        return this.f;
    }

    public final int hashCode() {
        int i;
        int a2 = h.a(h.a(17, this.f33a), this.b);
        if (this.c != null) {
            Iterator it = this.c.iterator();
            while (true) {
                i = a2;
                if (!it.hasNext()) {
                    break;
                }
                a2 = h.a(i, (n) it.next());
            }
        } else {
            i = a2;
        }
        return h.a(h.a(h.a(i, this.f), this.d), this.e);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((c() * 30) + 50);
        if (this.b != null) {
            sb.append(this.b);
            sb.append("->");
        }
        sb.append('{');
        if (this.d == g.TUNNELLED) {
            sb.append('t');
        }
        if (this.e == f.LAYERED) {
            sb.append('l');
        }
        if (this.f) {
            sb.append('s');
        }
        sb.append("}->");
        if (this.c != null) {
            for (n append : this.c) {
                sb.append(append);
                sb.append("->");
            }
        }
        sb.append(this.f33a);
        return sb.toString();
    }
}
