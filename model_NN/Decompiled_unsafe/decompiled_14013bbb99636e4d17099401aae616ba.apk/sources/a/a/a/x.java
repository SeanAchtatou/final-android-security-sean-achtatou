package a.a.a;

import java.nio.charset.CharacterCodingException;

public class x extends CharacterCodingException {

    /* renamed from: a  reason: collision with root package name */
    private final String f197a;

    public x(String str) {
        this.f197a = str;
    }

    public String getMessage() {
        return this.f197a;
    }
}
