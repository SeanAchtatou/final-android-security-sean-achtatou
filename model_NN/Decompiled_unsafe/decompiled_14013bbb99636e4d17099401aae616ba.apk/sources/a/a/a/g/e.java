package a.a.a.g;

import a.a.a.c;
import a.a.a.f;
import a.a.a.j.l;
import a.a.a.k;
import a.a.a.n.a;
import a.a.a.n.d;
import a.a.a.n.i;
import a.a.a.y;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public final class e implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final e f68a = a("application/atom+xml", c.c);
    public static final e b = a("application/x-www-form-urlencoded", c.c);
    public static final e c = a("application/json", c.f28a);
    public static final e d = a("application/octet-stream", (Charset) null);
    public static final e e = a("application/svg+xml", c.c);
    public static final e f = a("application/xhtml+xml", c.c);
    public static final e g = a("application/xml", c.c);
    public static final e h = a("multipart/form-data", c.c);
    public static final e i = a("text/html", c.c);
    public static final e j = a("text/plain", c.c);
    public static final e k = a("text/xml", c.c);
    public static final e l = a("*/*", (Charset) null);
    public static final e m = j;
    public static final e n = d;
    private final String o;
    private final Charset p;
    private final y[] q;

    e(String str, Charset charset) {
        this.o = str;
        this.p = charset;
        this.q = null;
    }

    e(String str, Charset charset, y[] yVarArr) {
        this.o = str;
        this.p = charset;
        this.q = yVarArr;
    }

    private static e a(f fVar, boolean z) {
        return a(fVar.a(), fVar.c(), z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.g.e.a(a.a.a.f, boolean):a.a.a.g.e
     arg types: [a.a.a.f, int]
     candidates:
      a.a.a.g.e.a(java.lang.String, java.nio.charset.Charset):a.a.a.g.e
      a.a.a.g.e.a(java.lang.String, a.a.a.y[]):a.a.a.g.e
      a.a.a.g.e.a(a.a.a.f, boolean):a.a.a.g.e */
    public static e a(k kVar) {
        a.a.a.e d2;
        if (kVar == null || (d2 = kVar.d()) == null) {
            return null;
        }
        f[] e2 = d2.e();
        if (e2.length > 0) {
            return a(e2[0], true);
        }
        return null;
    }

    public static e a(String str, Charset charset) {
        String lowerCase = ((String) a.b(str, "MIME type")).toLowerCase(Locale.ROOT);
        a.a(b(lowerCase), "MIME type may not contain reserved characters");
        return new e(lowerCase, charset);
    }

    public static e a(String str, y... yVarArr) {
        a.a(b(((String) a.b(str, "MIME type")).toLowerCase(Locale.ROOT)), "MIME type may not contain reserved characters");
        return a(str, yVarArr, true);
    }

    private static e a(String str, y[] yVarArr, boolean z) {
        Charset charset;
        int length = yVarArr.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                break;
            }
            y yVar = yVarArr[i2];
            if (yVar.a().equalsIgnoreCase("charset")) {
                String b2 = yVar.b();
                if (!i.b(b2)) {
                    try {
                        charset = Charset.forName(b2);
                    } catch (UnsupportedCharsetException e2) {
                        if (z) {
                            throw e2;
                        }
                        charset = null;
                    }
                }
            } else {
                i2++;
            }
        }
        charset = null;
        if (yVarArr == null || yVarArr.length <= 0) {
            yVarArr = null;
        }
        return new e(str, charset, yVarArr);
    }

    private static boolean b(String str) {
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (charAt == '\"' || charAt == ',' || charAt == ';') {
                return false;
            }
        }
        return true;
    }

    public e a(y... yVarArr) {
        if (yVarArr.length == 0) {
            return this;
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (this.q != null) {
            for (y yVar : this.q) {
                linkedHashMap.put(yVar.a(), yVar.b());
            }
        }
        for (y yVar2 : yVarArr) {
            linkedHashMap.put(yVar2.a(), yVar2.b());
        }
        ArrayList arrayList = new ArrayList(linkedHashMap.size() + 1);
        if (this.p != null && !linkedHashMap.containsKey("charset")) {
            arrayList.add(new l("charset", this.p.name()));
        }
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            arrayList.add(new l((String) entry.getKey(), (String) entry.getValue()));
        }
        return a(a(), (y[]) arrayList.toArray(new y[arrayList.size()]), true);
    }

    public String a() {
        return this.o;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence */
    public String a(String str) {
        a.a((CharSequence) str, "Parameter name");
        if (this.q == null) {
            return null;
        }
        for (y yVar : this.q) {
            if (yVar.a().equalsIgnoreCase(str)) {
                return yVar.b();
            }
        }
        return null;
    }

    public Charset b() {
        return this.p;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.j.e.a(a.a.a.n.d, a.a.a.y[], boolean):a.a.a.n.d
     arg types: [a.a.a.n.d, a.a.a.y[], int]
     candidates:
      a.a.a.j.e.a(a.a.a.n.d, a.a.a.f, boolean):a.a.a.n.d
      a.a.a.j.e.a(a.a.a.n.d, a.a.a.y, boolean):a.a.a.n.d
      a.a.a.j.e.a(a.a.a.n.d, java.lang.String, boolean):void
      a.a.a.j.e.a(a.a.a.n.d, a.a.a.y[], boolean):a.a.a.n.d */
    public String toString() {
        d dVar = new d(64);
        dVar.a(this.o);
        if (this.q != null) {
            dVar.a("; ");
            a.a.a.j.e.b.a(dVar, this.q, false);
        } else if (this.p != null) {
            dVar.a("; charset=");
            dVar.a(this.p.name());
        }
        return dVar.toString();
    }
}
