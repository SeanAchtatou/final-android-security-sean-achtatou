package a.a.a.g;

import a.a.a.h.f.j;
import a.a.a.n.a;
import java.io.InputStream;
import java.io.OutputStream;

public class b extends a {
    private InputStream d;
    private long e = -1;

    public void a(long j) {
        this.e = j;
    }

    public void a(InputStream inputStream) {
        this.d = inputStream;
    }

    public void a(OutputStream outputStream) {
        a.a(outputStream, "Output stream");
        InputStream f = f();
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = f.read(bArr);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        } finally {
            f.close();
        }
    }

    public boolean a() {
        return false;
    }

    public long c() {
        return this.e;
    }

    public InputStream f() {
        a.a.a.n.b.a(this.d != null, "Content has not been provided");
        return this.d;
    }

    public boolean g() {
        return (this.d == null || this.d == j.f158a) ? false : true;
    }
}
