package a.a.a.g.a;

import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

class h extends a {
    private final List c;

    public h(Charset charset, String str, List list) {
        super(charset, str);
        this.c = list;
    }

    public List a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void a(b bVar, OutputStream outputStream) {
        Iterator it = bVar.b().iterator();
        while (it.hasNext()) {
            a((j) it.next(), outputStream);
        }
    }
}
