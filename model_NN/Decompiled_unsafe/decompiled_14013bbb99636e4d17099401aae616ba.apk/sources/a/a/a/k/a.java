package a.a.a.k;

@Deprecated
public abstract class a implements e {
    protected a() {
    }

    public int a(String str, int i) {
        Object a2 = a(str);
        return a2 == null ? i : ((Integer) a2).intValue();
    }

    public long a(String str, long j) {
        Object a2 = a(str);
        return a2 == null ? j : ((Long) a2).longValue();
    }

    public boolean a(String str, boolean z) {
        Object a2 = a(str);
        return a2 == null ? z : ((Boolean) a2).booleanValue();
    }

    public e b(String str, int i) {
        a(str, Integer.valueOf(i));
        return this;
    }

    public e b(String str, boolean z) {
        a(str, z ? Boolean.TRUE : Boolean.FALSE);
        return this;
    }
}
