package a.a.a.k;

import a.a.a.ac;
import a.a.a.m.d;
import a.a.a.n.a;
import a.a.a.v;

@Deprecated
public final class f {
    public static String a(e eVar) {
        a.a(eVar, "HTTP parameters");
        String str = (String) eVar.a("http.protocol.element-charset");
        return str == null ? d.b.name() : str;
    }

    public static void a(e eVar, ac acVar) {
        a.a(eVar, "HTTP parameters");
        eVar.a("http.protocol.version", acVar);
    }

    public static void a(e eVar, String str) {
        a.a(eVar, "HTTP parameters");
        eVar.a("http.protocol.content-charset", str);
    }

    public static ac b(e eVar) {
        a.a(eVar, "HTTP parameters");
        Object a2 = eVar.a("http.protocol.version");
        return a2 == null ? v.c : (ac) a2;
    }

    public static void b(e eVar, String str) {
        a.a(eVar, "HTTP parameters");
        eVar.a("http.useragent", str);
    }
}
