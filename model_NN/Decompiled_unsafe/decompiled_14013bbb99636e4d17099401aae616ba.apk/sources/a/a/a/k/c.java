package a.a.a.k;

import a.a.a.n.a;

@Deprecated
public final class c {
    public static int a(e eVar) {
        a.a(eVar, "HTTP parameters");
        return eVar.a("http.socket.timeout", 0);
    }

    public static void a(e eVar, int i) {
        a.a(eVar, "HTTP parameters");
        eVar.b("http.socket.timeout", i);
    }

    public static void a(e eVar, boolean z) {
        a.a(eVar, "HTTP parameters");
        eVar.b("http.tcp.nodelay", z);
    }

    public static void b(e eVar, int i) {
        a.a(eVar, "HTTP parameters");
        eVar.b("http.socket.buffer-size", i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.k.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      a.a.a.k.e.a(java.lang.String, int):int
      a.a.a.k.e.a(java.lang.String, long):long
      a.a.a.k.e.a(java.lang.String, java.lang.Object):a.a.a.k.e
      a.a.a.k.e.a(java.lang.String, boolean):boolean */
    public static boolean b(e eVar) {
        a.a(eVar, "HTTP parameters");
        return eVar.a("http.socket.reuseaddr", false);
    }

    public static void c(e eVar, int i) {
        a.a(eVar, "HTTP parameters");
        eVar.b("http.connection.timeout", i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.k.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      a.a.a.k.e.a(java.lang.String, int):int
      a.a.a.k.e.a(java.lang.String, long):long
      a.a.a.k.e.a(java.lang.String, java.lang.Object):a.a.a.k.e
      a.a.a.k.e.a(java.lang.String, boolean):boolean */
    public static boolean c(e eVar) {
        a.a(eVar, "HTTP parameters");
        return eVar.a("http.tcp.nodelay", true);
    }

    public static int d(e eVar) {
        a.a(eVar, "HTTP parameters");
        return eVar.a("http.socket.linger", -1);
    }

    public static int e(e eVar) {
        a.a(eVar, "HTTP parameters");
        return eVar.a("http.connection.timeout", 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.k.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      a.a.a.k.e.a(java.lang.String, int):int
      a.a.a.k.e.a(java.lang.String, long):long
      a.a.a.k.e.a(java.lang.String, java.lang.Object):a.a.a.k.e
      a.a.a.k.e.a(java.lang.String, boolean):boolean */
    public static boolean f(e eVar) {
        a.a(eVar, "HTTP parameters");
        return eVar.a("http.connection.stalecheck", true);
    }
}
