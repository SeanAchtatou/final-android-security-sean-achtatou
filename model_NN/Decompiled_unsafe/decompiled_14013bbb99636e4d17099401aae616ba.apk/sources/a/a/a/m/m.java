package a.a.a.m;

import a.a.a.k.e;
import a.a.a.n.a;
import a.a.a.q;
import a.a.a.r;

public class m implements r {

    /* renamed from: a  reason: collision with root package name */
    private final String f191a;

    public m() {
        this(null);
    }

    public m(String str) {
        this.f191a = str;
    }

    public void a(q qVar, e eVar) {
        a.a(qVar, "HTTP request");
        if (!qVar.a("User-Agent")) {
            String str = null;
            e f = qVar.f();
            if (f != null) {
                str = (String) f.a("http.useragent");
            }
            if (str == null) {
                str = this.f191a;
            }
            if (str != null) {
                qVar.a("User-Agent", str);
            }
        }
    }
}
