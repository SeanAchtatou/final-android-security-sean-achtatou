package a.a.a.m;

import a.a.a.q;
import a.a.a.r;
import a.a.a.s;
import a.a.a.u;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public final class b implements g, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    protected final List f183a = new ArrayList();
    protected final List b = new ArrayList();

    public int a() {
        return this.f183a.size();
    }

    public r a(int i) {
        if (i < 0 || i >= this.f183a.size()) {
            return null;
        }
        return (r) this.f183a.get(i);
    }

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        bVar.f183a.clear();
        bVar.f183a.addAll(this.f183a);
        bVar.b.clear();
        bVar.b.addAll(this.b);
    }

    public void a(q qVar, e eVar) {
        for (r a2 : this.f183a) {
            a2.a(qVar, eVar);
        }
    }

    public void a(r rVar) {
        if (rVar != null) {
            this.f183a.add(rVar);
        }
    }

    public void a(s sVar, e eVar) {
        for (u a2 : this.b) {
            a2.a(sVar, eVar);
        }
    }

    public void a(u uVar) {
        if (uVar != null) {
            this.b.add(uVar);
        }
    }

    public int b() {
        return this.b.size();
    }

    public u b(int i) {
        if (i < 0 || i >= this.b.size()) {
            return null;
        }
        return (u) this.b.get(i);
    }

    public final void b(r rVar) {
        a(rVar);
    }

    public final void b(u uVar) {
        a(uVar);
    }

    public Object clone() {
        b bVar = (b) super.clone();
        a(bVar);
        return bVar;
    }
}
