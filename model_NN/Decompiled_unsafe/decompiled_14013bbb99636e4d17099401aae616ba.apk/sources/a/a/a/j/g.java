package a.a.a.j;

import a.a.a.ac;
import a.a.a.ae;
import a.a.a.n.a;
import a.a.a.q;
import a.a.a.v;

public class g extends a implements q {
    private final String c;
    private final String d;
    private ae e;

    public g(ae aeVar) {
        this.e = (ae) a.a(aeVar, "Request line");
        this.c = aeVar.a();
        this.d = aeVar.c();
    }

    public g(String str, String str2, ac acVar) {
        this(new m(str, str2, acVar));
    }

    public ac c() {
        return g().b();
    }

    public ae g() {
        if (this.e == null) {
            this.e = new m(this.c, this.d, v.c);
        }
        return this.e;
    }

    public String toString() {
        return this.c + ' ' + this.d + ' ' + this.f163a;
    }
}
