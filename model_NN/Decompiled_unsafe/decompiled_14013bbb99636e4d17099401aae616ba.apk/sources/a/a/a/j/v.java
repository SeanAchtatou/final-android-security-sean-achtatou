package a.a.a.j;

import a.a.a.n.d;
import java.util.BitSet;

public class v {

    /* renamed from: a  reason: collision with root package name */
    public static final v f179a = new v();

    public static BitSet a(int... iArr) {
        BitSet bitSet = new BitSet();
        for (int i : iArr) {
            bitSet.set(i);
        }
        return bitSet;
    }

    public static boolean a(char c) {
        return c == ' ' || c == 9 || c == 13 || c == 10;
    }

    public String a(d dVar, u uVar, BitSet bitSet) {
        StringBuilder sb = new StringBuilder();
        boolean z = false;
        while (!uVar.c()) {
            char charAt = dVar.charAt(uVar.b());
            if (bitSet != null && bitSet.get(charAt)) {
                break;
            } else if (a(charAt)) {
                a(dVar, uVar);
                z = true;
            } else {
                if (z && sb.length() > 0) {
                    sb.append(' ');
                }
                a(dVar, uVar, bitSet, sb);
                z = false;
            }
        }
        return sb.toString();
    }

    public void a(d dVar, u uVar) {
        int b = uVar.b();
        int b2 = uVar.b();
        int a2 = uVar.a();
        while (b2 < a2 && a(dVar.charAt(b2))) {
            b++;
            b2++;
        }
        uVar.a(b);
    }

    public void a(d dVar, u uVar, StringBuilder sb) {
        int i;
        if (!uVar.c()) {
            int b = uVar.b();
            int b2 = uVar.b();
            int a2 = uVar.a();
            if (dVar.charAt(b) == '\"') {
                int i2 = b + 1;
                int i3 = b2 + 1;
                boolean z = false;
                while (true) {
                    if (i3 >= a2) {
                        i = i2;
                        break;
                    }
                    char charAt = dVar.charAt(i3);
                    if (z) {
                        if (!(charAt == '\"' || charAt == '\\')) {
                            sb.append('\\');
                        }
                        sb.append(charAt);
                        z = false;
                    } else if (charAt == '\"') {
                        i = i2 + 1;
                        break;
                    } else if (charAt == '\\') {
                        z = true;
                    } else if (!(charAt == 13 || charAt == 10)) {
                        sb.append(charAt);
                    }
                    i3++;
                    i2++;
                }
                uVar.a(i);
            }
        }
    }

    public void a(d dVar, u uVar, BitSet bitSet, StringBuilder sb) {
        int b = uVar.b();
        int a2 = uVar.a();
        for (int b2 = uVar.b(); b2 < a2; b2++) {
            char charAt = dVar.charAt(b2);
            if ((bitSet != null && bitSet.get(charAt)) || a(charAt)) {
                break;
            }
            b++;
            sb.append(charAt);
        }
        uVar.a(b);
    }

    public String b(d dVar, u uVar, BitSet bitSet) {
        StringBuilder sb = new StringBuilder();
        boolean z = false;
        while (!uVar.c()) {
            char charAt = dVar.charAt(uVar.b());
            if (bitSet != null && bitSet.get(charAt)) {
                break;
            } else if (a(charAt)) {
                a(dVar, uVar);
                z = true;
            } else if (charAt == '\"') {
                if (z && sb.length() > 0) {
                    sb.append(' ');
                }
                a(dVar, uVar, sb);
                z = false;
            } else {
                if (z && sb.length() > 0) {
                    sb.append(' ');
                }
                b(dVar, uVar, bitSet, sb);
                z = false;
            }
        }
        return sb.toString();
    }

    public void b(d dVar, u uVar, BitSet bitSet, StringBuilder sb) {
        int b = uVar.b();
        int a2 = uVar.a();
        for (int b2 = uVar.b(); b2 < a2; b2++) {
            char charAt = dVar.charAt(b2);
            if ((bitSet != null && bitSet.get(charAt)) || a(charAt) || charAt == '\"') {
                break;
            }
            b++;
            sb.append(charAt);
        }
        uVar.a(b);
    }
}
