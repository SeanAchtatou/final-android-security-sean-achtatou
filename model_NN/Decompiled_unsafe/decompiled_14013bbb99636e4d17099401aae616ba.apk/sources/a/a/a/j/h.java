package a.a.a.j;

import a.a.a.ac;
import a.a.a.ad;
import a.a.a.af;
import a.a.a.k;
import a.a.a.n.a;
import a.a.a.s;
import a.a.a.v;
import java.util.Locale;

public class h extends a implements s {
    private af c;
    private ac d;
    private int e;
    private String f;
    private k g;
    private final ad h;
    private Locale i;

    public h(af afVar, ad adVar, Locale locale) {
        this.c = (af) a.a(afVar, "Status line");
        this.d = afVar.a();
        this.e = afVar.b();
        this.f = afVar.c();
        this.h = adVar;
        this.i = locale;
    }

    public af a() {
        if (this.c == null) {
            this.c = new n(this.d != null ? this.d : v.c, this.e, this.f != null ? this.f : a(this.e));
        }
        return this.c;
    }

    /* access modifiers changed from: protected */
    public String a(int i2) {
        if (this.h == null) {
            return null;
        }
        return this.h.a(i2, this.i != null ? this.i : Locale.getDefault());
    }

    public void a(k kVar) {
        this.g = kVar;
    }

    public k b() {
        return this.g;
    }

    public ac c() {
        return this.d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a());
        sb.append(' ');
        sb.append(this.f163a);
        if (this.g != null) {
            sb.append(' ');
            sb.append(this.g);
        }
        return sb.toString();
    }
}
