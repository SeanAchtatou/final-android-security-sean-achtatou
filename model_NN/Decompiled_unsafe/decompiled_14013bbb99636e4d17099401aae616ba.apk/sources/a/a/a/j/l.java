package a.a.a.j;

import a.a.a.n.a;
import a.a.a.n.h;
import a.a.a.y;
import java.io.Serializable;

public class l implements y, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f172a;
    private final String b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public l(String str, String str2) {
        this.f172a = (String) a.a((Object) str, "Name");
        this.b = str2;
    }

    public String a() {
        return this.f172a;
    }

    public String b() {
        return this.b;
    }

    public Object clone() {
        return super.clone();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof y)) {
            return false;
        }
        l lVar = (l) obj;
        return this.f172a.equals(lVar.f172a) && h.a(this.b, lVar.b);
    }

    public int hashCode() {
        return h.a(h.a(17, this.f172a), this.b);
    }

    public String toString() {
        if (this.b == null) {
            return this.f172a;
        }
        StringBuilder sb = new StringBuilder(this.f172a.length() + 1 + this.b.length());
        sb.append(this.f172a);
        sb.append("=");
        sb.append(this.b);
        return sb.toString();
    }
}
