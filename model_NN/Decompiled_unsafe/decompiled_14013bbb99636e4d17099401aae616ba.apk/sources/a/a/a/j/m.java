package a.a.a.j;

import a.a.a.ac;
import a.a.a.ae;
import a.a.a.n.a;
import a.a.a.n.d;
import java.io.Serializable;

public class m implements ae, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final ac f173a;
    private final String b;
    private final String c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public m(String str, String str2, ac acVar) {
        this.b = (String) a.a((Object) str, "Method");
        this.c = (String) a.a((Object) str2, "URI");
        this.f173a = (ac) a.a(acVar, "Version");
    }

    public String a() {
        return this.b;
    }

    public ac b() {
        return this.f173a;
    }

    public String c() {
        return this.c;
    }

    public Object clone() {
        return super.clone();
    }

    public String toString() {
        return i.b.a((d) null, this).toString();
    }
}
