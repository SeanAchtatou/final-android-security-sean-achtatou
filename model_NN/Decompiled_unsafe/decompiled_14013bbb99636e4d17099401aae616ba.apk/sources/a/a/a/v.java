package a.a.a;

import java.io.Serializable;

public final class v extends ac implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final v f196a = new v(0, 9);
    public static final v b = new v(1, 0);
    public static final v c = new v(1, 1);

    public v(int i, int i2) {
        super("HTTP", i, i2);
    }

    public ac a(int i, int i2) {
        if (i == this.e && i2 == this.f) {
            return this;
        }
        if (i == 1) {
            if (i2 == 0) {
                return b;
            }
            if (i2 == 1) {
                return c;
            }
        }
        return (i == 0 && i2 == 9) ? f196a : new v(i, i2);
    }
}
