package a.a.a.h;

import a.a.a.h.e.b;
import a.a.a.h.f.k;
import a.a.a.i;
import a.a.a.i.c;
import a.a.a.i.d;
import a.a.a.i.e;
import a.a.a.i.f;
import a.a.a.i.g;
import a.a.a.l;
import a.a.a.q;
import a.a.a.s;
import a.a.a.t;
import java.io.IOException;
import java.net.SocketTimeoutException;

@Deprecated
public abstract class a implements i {

    /* renamed from: a  reason: collision with root package name */
    private final b f69a = l();
    private final a.a.a.h.e.a b = k();
    private f c = null;
    private g d = null;
    private a.a.a.i.b e = null;
    private c f = null;
    private d g = null;
    private e h = null;

    /* access modifiers changed from: protected */
    public e a(e eVar, e eVar2) {
        return new e(eVar, eVar2);
    }

    /* access modifiers changed from: protected */
    public c a(f fVar, t tVar, a.a.a.k.e eVar) {
        return new a.a.a.h.f.i(fVar, null, tVar, eVar);
    }

    /* access modifiers changed from: protected */
    public d a(g gVar, a.a.a.k.e eVar) {
        return new k(gVar, null, eVar);
    }

    public s a() {
        j();
        s sVar = (s) this.f.a();
        if (sVar.a().b() >= 200) {
            this.h.b();
        }
        return sVar;
    }

    /* access modifiers changed from: protected */
    public void a(f fVar, g gVar, a.a.a.k.e eVar) {
        this.c = (f) a.a.a.n.a.a(fVar, "Input session buffer");
        this.d = (g) a.a.a.n.a.a(gVar, "Output session buffer");
        if (fVar instanceof a.a.a.i.b) {
            this.e = (a.a.a.i.b) fVar;
        }
        this.f = a(fVar, n(), eVar);
        this.g = a(gVar, eVar);
        this.h = a(fVar.b(), gVar.b());
    }

    public void a(l lVar) {
        a.a.a.n.a.a(lVar, "HTTP request");
        j();
        if (lVar.b() != null) {
            this.f69a.a(this.d, lVar, lVar.b());
        }
    }

    public void a(q qVar) {
        a.a.a.n.a.a(qVar, "HTTP request");
        j();
        this.g.b(qVar);
        this.h.a();
    }

    public void a(s sVar) {
        a.a.a.n.a.a(sVar, "HTTP response");
        j();
        sVar.a(this.b.b(this.c, sVar));
    }

    public boolean a(int i) {
        j();
        try {
            return this.c.a(i);
        } catch (SocketTimeoutException e2) {
            return false;
        }
    }

    public void b() {
        j();
        o();
    }

    public boolean d() {
        if (!c() || p()) {
            return true;
        }
        try {
            this.c.a(1);
            return p();
        } catch (SocketTimeoutException e2) {
            return false;
        } catch (IOException e3) {
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void j();

    /* access modifiers changed from: protected */
    public a.a.a.h.e.a k() {
        return new a.a.a.h.e.a(new a.a.a.h.e.c());
    }

    /* access modifiers changed from: protected */
    public b l() {
        return new b(new a.a.a.h.e.d());
    }

    /* access modifiers changed from: protected */
    public t n() {
        return c.f116a;
    }

    /* access modifiers changed from: protected */
    public void o() {
        this.d.a();
    }

    /* access modifiers changed from: protected */
    public boolean p() {
        return this.e != null && this.e.c();
    }
}
