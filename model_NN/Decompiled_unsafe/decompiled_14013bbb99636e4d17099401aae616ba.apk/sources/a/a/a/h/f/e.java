package a.a.a.h.f;

import a.a.a.ah;
import a.a.a.d.b;
import a.a.a.i.f;
import a.a.a.m;
import a.a.a.n.a;
import a.a.a.n.d;
import a.a.a.w;
import java.io.IOException;
import java.io.InputStream;

public class e extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final f f154a;
    private final d b;
    private final b c;
    private int d;
    private int e;
    private int f;
    private boolean g;
    private boolean h;
    private a.a.a.e[] i;

    public e(f fVar) {
        this(fVar, null);
    }

    public e(f fVar, b bVar) {
        this.g = false;
        this.h = false;
        this.i = new a.a.a.e[0];
        this.f154a = (f) a.a(fVar, "Session input buffer");
        this.f = 0;
        this.b = new d(16);
        this.c = bVar == null ? b.f29a : bVar;
        this.d = 1;
    }

    private void a() {
        if (this.d == Integer.MAX_VALUE) {
            throw new w("Corrupt data stream");
        }
        try {
            this.e = b();
            if (this.e < 0) {
                throw new w("Negative chunk size");
            }
            this.d = 2;
            this.f = 0;
            if (this.e == 0) {
                this.g = true;
                c();
            }
        } catch (w e2) {
            this.d = Integer.MAX_VALUE;
            throw e2;
        }
    }

    private int b() {
        switch (this.d) {
            case 1:
                break;
            case 2:
            default:
                throw new IllegalStateException("Inconsistent codec state");
            case 3:
                this.b.a();
                if (this.f154a.a(this.b) != -1) {
                    if (this.b.c()) {
                        this.d = 1;
                        break;
                    } else {
                        throw new w("Unexpected content at the end of chunk");
                    }
                } else {
                    throw new w("CRLF expected at end of chunk");
                }
        }
        this.b.a();
        if (this.f154a.a(this.b) == -1) {
            throw new a.a.a.a("Premature end of chunk coded message body: closing chunk expected");
        }
        int b2 = this.b.b(59);
        if (b2 < 0) {
            b2 = this.b.length();
        }
        try {
            return Integer.parseInt(this.b.b(0, b2), 16);
        } catch (NumberFormatException e2) {
            throw new w("Bad chunk header");
        }
    }

    private void c() {
        try {
            this.i = a.a(this.f154a, this.c.b(), this.c.a(), null);
        } catch (m e2) {
            w wVar = new w("Invalid footer: " + e2.getMessage());
            wVar.initCause(e2);
            throw wVar;
        }
    }

    public int available() {
        if (this.f154a instanceof a.a.a.i.a) {
            return Math.min(((a.a.a.i.a) this.f154a).e(), this.e - this.f);
        }
        return 0;
    }

    public void close() {
        if (!this.h) {
            try {
                if (!this.g && this.d != Integer.MAX_VALUE) {
                    do {
                    } while (read(new byte[2048]) >= 0);
                }
            } finally {
                this.g = true;
                this.h = true;
            }
        }
    }

    public int read() {
        if (this.h) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.g) {
            return -1;
        } else {
            if (this.d != 2) {
                a();
                if (this.g) {
                    return -1;
                }
            }
            int a2 = this.f154a.a();
            if (a2 != -1) {
                this.f++;
                if (this.f >= this.e) {
                    this.d = 3;
                }
            }
            return a2;
        }
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i2, int i3) {
        if (this.h) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.g) {
            return -1;
        } else {
            if (this.d != 2) {
                a();
                if (this.g) {
                    return -1;
                }
            }
            int a2 = this.f154a.a(bArr, i2, Math.min(i3, this.e - this.f));
            if (a2 != -1) {
                this.f += a2;
                if (this.f >= this.e) {
                    this.d = 3;
                }
                return a2;
            }
            this.g = true;
            throw new ah("Truncated chunk ( expected size: " + this.e + "; actual size: " + this.f + ")");
        }
    }
}
