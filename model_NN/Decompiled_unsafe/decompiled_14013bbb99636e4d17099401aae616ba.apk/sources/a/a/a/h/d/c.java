package a.a.a.h.d;

import a.a.a.f.a;
import a.a.a.f.o;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class c implements a, o, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f134a;
    private Map b = new HashMap();
    private String c;
    private String d;
    private String e;
    private Date f;
    private String g;
    private boolean h;
    private int i;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public c(String str, String str2) {
        a.a.a.n.a.a((Object) str, "Name");
        this.f134a = str;
        this.c = str2;
    }

    public String a() {
        return this.f134a;
    }

    public String a(String str) {
        return (String) this.b.get(str);
    }

    public void a(int i2) {
        this.i = i2;
    }

    public void a(String str, String str2) {
        this.b.put(str, str2);
    }

    public void a(boolean z) {
        this.h = z;
    }

    public boolean a(Date date) {
        a.a.a.n.a.a(date, "Date");
        return this.f != null && this.f.getTime() <= date.getTime();
    }

    public String b() {
        return this.c;
    }

    public void b(Date date) {
        this.f = date;
    }

    public boolean b(String str) {
        return this.b.containsKey(str);
    }

    public Date c() {
        return this.f;
    }

    public void c(String str) {
        this.d = str;
    }

    public Object clone() {
        c cVar = (c) super.clone();
        cVar.b = new HashMap(this.b);
        return cVar;
    }

    public String d() {
        return this.e;
    }

    public void d(String str) {
        if (str != null) {
            this.e = str.toLowerCase(Locale.ROOT);
        } else {
            this.e = null;
        }
    }

    public String e() {
        return this.g;
    }

    public void e(String str) {
        this.g = str;
    }

    public int[] f() {
        return null;
    }

    public boolean g() {
        return this.h;
    }

    public int h() {
        return this.i;
    }

    public String toString() {
        return "[version: " + Integer.toString(this.i) + "]" + "[name: " + this.f134a + "]" + "[value: " + this.c + "]" + "[domain: " + this.e + "]" + "[path: " + this.g + "]" + "[expiry: " + this.f + "]";
    }
}
