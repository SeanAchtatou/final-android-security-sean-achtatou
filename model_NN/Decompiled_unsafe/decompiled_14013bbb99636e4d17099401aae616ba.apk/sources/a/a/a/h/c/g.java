package a.a.a.h.c;

import a.a.a.e.b.b;
import a.a.a.e.b.d;
import a.a.a.m;
import a.a.a.m.e;
import a.a.a.n;
import a.a.a.n.a;
import a.a.a.q;
import java.net.InetAddress;

@Deprecated
public class g implements d {

    /* renamed from: a  reason: collision with root package name */
    protected final a.a.a.e.c.g f121a;

    public g(a.a.a.e.c.g gVar) {
        a.a(gVar, "Scheme registry");
        this.f121a = gVar;
    }

    public b a(n nVar, q qVar, e eVar) {
        a.a(qVar, "HTTP request");
        b b = a.a.a.e.a.a.b(qVar.f());
        if (b != null) {
            return b;
        }
        a.a.a.n.b.a(nVar, "Target host");
        InetAddress c = a.a.a.e.a.a.c(qVar.f());
        n a2 = a.a.a.e.a.a.a(qVar.f());
        try {
            boolean d = this.f121a.a(nVar.c()).d();
            return a2 == null ? new b(nVar, c, d) : new b(nVar, c, a2, d);
        } catch (IllegalStateException e) {
            throw new m(e.getMessage());
        }
    }
}
