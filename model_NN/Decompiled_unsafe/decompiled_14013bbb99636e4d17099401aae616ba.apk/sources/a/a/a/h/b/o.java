package a.a.a.h.b;

import a.a.a.a.i;
import a.a.a.a.t;
import a.a.a.ab;
import a.a.a.b.c;
import a.a.a.b.f.f;
import a.a.a.b.k;
import a.a.a.b.m;
import a.a.a.b.n;
import a.a.a.b.p;
import a.a.a.b.q;
import a.a.a.e.b;
import a.a.a.e.b.d;
import a.a.a.e.g;
import a.a.a.k.e;
import a.a.a.l;
import a.a.a.m.h;
import a.a.a.n.a;
import a.a.a.s;
import a.a.a.z;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

@Deprecated
public class o implements p {

    /* renamed from: a  reason: collision with root package name */
    protected final b f108a;
    protected final d b;
    protected final a.a.a.b c;
    protected final g d;
    protected final h e;
    protected final a.a.a.m.g f;
    protected final k g;
    @Deprecated
    protected final n h;
    protected final a.a.a.b.o i;
    @Deprecated
    protected final a.a.a.b.b j;
    protected final c k;
    @Deprecated
    protected final a.a.a.b.b l;
    protected final c m;
    protected final q n;
    protected final e o;
    protected a.a.a.e.o p;
    protected final i q;
    protected final i r;
    private final Log s;
    private final s t;
    private int u;
    private int v;
    private final int w;
    private a.a.a.n x;

    public o(Log log, h hVar, b bVar, a.a.a.b bVar2, g gVar, d dVar, a.a.a.m.g gVar2, k kVar, a.a.a.b.o oVar, c cVar, c cVar2, q qVar, e eVar) {
        a.a(log, "Log");
        a.a(hVar, "Request executor");
        a.a(bVar, "Client connection manager");
        a.a(bVar2, "Connection reuse strategy");
        a.a(gVar, "Connection keep alive strategy");
        a.a(dVar, "Route planner");
        a.a(gVar2, "HTTP protocol processor");
        a.a(kVar, "HTTP request retry handler");
        a.a(oVar, "Redirect strategy");
        a.a(cVar, "Target authentication strategy");
        a.a(cVar2, "Proxy authentication strategy");
        a.a(qVar, "User token handler");
        a.a(eVar, "HTTP parameters");
        this.s = log;
        this.t = new s(log);
        this.e = hVar;
        this.f108a = bVar;
        this.c = bVar2;
        this.d = gVar;
        this.b = dVar;
        this.f = gVar2;
        this.g = kVar;
        this.i = oVar;
        this.k = cVar;
        this.m = cVar2;
        this.n = qVar;
        this.o = eVar;
        if (oVar instanceof n) {
            this.h = ((n) oVar).a();
        } else {
            this.h = null;
        }
        if (cVar instanceof b) {
            this.j = ((b) cVar).a();
        } else {
            this.j = null;
        }
        if (cVar2 instanceof b) {
            this.l = ((b) cVar2).a();
        } else {
            this.l = null;
        }
        this.p = null;
        this.u = 0;
        this.v = 0;
        this.q = new i();
        this.r = new i();
        this.w = this.o.a("http.protocol.max-redirects", 100);
    }

    private v a(a.a.a.q qVar) {
        return qVar instanceof l ? new q((l) qVar) : new v(qVar);
    }

    private void a(w wVar, a.a.a.m.e eVar) {
        a.a.a.e.b.b b2 = wVar.b();
        v a2 = wVar.a();
        int i2 = 0;
        while (true) {
            eVar.a("http.request", a2);
            i2++;
            try {
                if (!this.p.c()) {
                    this.p.a(b2, eVar, this.o);
                } else {
                    this.p.b(a.a.a.k.c.a(this.o));
                }
                a(b2, eVar);
                return;
            } catch (IOException e2) {
                try {
                    this.p.close();
                } catch (IOException e3) {
                }
                if (!this.g.a(e2, i2, eVar)) {
                    throw e2;
                } else if (this.s.isInfoEnabled()) {
                    this.s.info("I/O exception (" + e2.getClass().getName() + ") caught when connecting to " + b2 + ": " + e2.getMessage());
                    if (this.s.isDebugEnabled()) {
                        this.s.debug(e2.getMessage(), e2);
                    }
                    this.s.info("Retrying connect to " + b2);
                }
            }
        }
    }

    private s b(w wVar, a.a.a.m.e eVar) {
        v a2 = wVar.a();
        a.a.a.e.b.b b2 = wVar.b();
        IOException e2 = null;
        while (true) {
            this.u++;
            a2.n();
            if (!a2.j()) {
                this.s.debug("Cannot retry non-repeatable request");
                if (e2 != null) {
                    throw new a.a.a.b.l("Cannot retry request with a non-repeatable request entity.  The cause lists the reason the original request failed.", e2);
                }
                throw new a.a.a.b.l("Cannot retry request with a non-repeatable request entity.");
            }
            try {
                if (!this.p.c()) {
                    if (!b2.e()) {
                        this.s.debug("Reopening the direct connection.");
                        this.p.a(b2, eVar, this.o);
                    } else {
                        this.s.debug("Proxied connection. Need to start over.");
                        return null;
                    }
                }
                if (this.s.isDebugEnabled()) {
                    this.s.debug("Attempt " + this.u + " to execute request");
                }
                return this.e.a(a2, this.p, eVar);
            } catch (IOException e3) {
                e2 = e3;
                this.s.debug("Closing the connection.");
                try {
                    this.p.close();
                } catch (IOException e4) {
                }
                if (this.g.a(e2, a2.m(), eVar)) {
                    if (this.s.isInfoEnabled()) {
                        this.s.info("I/O exception (" + e2.getClass().getName() + ") caught when processing request to " + b2 + ": " + e2.getMessage());
                    }
                    if (this.s.isDebugEnabled()) {
                        this.s.debug(e2.getMessage(), e2);
                    }
                    if (this.s.isInfoEnabled()) {
                        this.s.info("Retrying request to " + b2);
                    }
                } else if (e2 instanceof z) {
                    z zVar = new z(b2.a().f() + " failed to respond");
                    zVar.setStackTrace(e2.getStackTrace());
                    throw zVar;
                } else {
                    throw e2;
                }
            }
        }
    }

    private void b() {
        a.a.a.e.o oVar = this.p;
        if (oVar != null) {
            this.p = null;
            try {
                oVar.i();
            } catch (IOException e2) {
                if (this.s.isDebugEnabled()) {
                    this.s.debug(e2.getMessage(), e2);
                }
            }
            try {
                oVar.h();
            } catch (IOException e3) {
                this.s.debug("Error releasing connection", e3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public w a(w wVar, s sVar, a.a.a.m.e eVar) {
        a.a.a.n nVar;
        a.a.a.e.b.b b2 = wVar.b();
        v a2 = wVar.a();
        e f2 = a2.f();
        if (a.a.a.b.d.b.b(f2)) {
            a.a.a.n nVar2 = (a.a.a.n) eVar.a("http.target_host");
            if (nVar2 == null) {
                nVar2 = b2.a();
            }
            if (nVar2.b() < 0) {
                nVar = new a.a.a.n(nVar2.a(), this.f108a.a().a(nVar2).a(), nVar2.c());
            } else {
                nVar = nVar2;
            }
            boolean a3 = this.t.a(nVar, sVar, this.k, this.q, eVar);
            a.a.a.n d2 = b2.d();
            if (d2 == null) {
                d2 = b2.a();
            }
            boolean a4 = this.t.a(d2, sVar, this.m, this.r, eVar);
            if (a3) {
                if (this.t.c(nVar, sVar, this.k, this.q, eVar)) {
                    return wVar;
                }
            }
            if (a4) {
                if (this.t.c(d2, sVar, this.m, this.r, eVar)) {
                    return wVar;
                }
            }
        }
        if (!a.a.a.b.d.b.a(f2) || !this.i.a(a2, sVar, eVar)) {
            return null;
        }
        if (this.v >= this.w) {
            throw new m("Maximum redirects (" + this.w + ") exceeded");
        }
        this.v++;
        this.x = null;
        a.a.a.b.c.l b3 = this.i.b(a2, sVar, eVar);
        b3.a(a2.l().d());
        URI i2 = b3.i();
        a.a.a.n b4 = f.b(i2);
        if (b4 == null) {
            throw new ab("Redirect URI does not specify a valid host name: " + i2);
        }
        if (!b2.a().equals(b4)) {
            this.s.debug("Resetting target auth state");
            this.q.a();
            a.a.a.a.c c2 = this.r.c();
            if (c2 != null && c2.c()) {
                this.s.debug("Resetting proxy auth state");
                this.r.a();
            }
        }
        v a5 = a(b3);
        a5.a(f2);
        a.a.a.e.b.b b5 = b(b4, a5, eVar);
        w wVar2 = new w(a5, b5);
        if (!this.s.isDebugEnabled()) {
            return wVar2;
        }
        this.s.debug("Redirecting to '" + i2 + "' via " + b5);
        return wVar2;
    }

    public s a(a.a.a.n nVar, a.a.a.q qVar, a.a.a.m.e eVar) {
        Object obj;
        boolean z = false;
        eVar.a("http.auth.target-scope", this.q);
        eVar.a("http.auth.proxy-scope", this.r);
        v a2 = a(qVar);
        a2.a(this.o);
        a.a.a.e.b.b b2 = b(nVar, a2, eVar);
        this.x = (a.a.a.n) a2.f().a("http.virtual-host");
        if (this.x != null && this.x.b() == -1) {
            int b3 = (nVar != null ? nVar : b2.a()).b();
            if (b3 != -1) {
                this.x = new a.a.a.n(this.x.a(), b3, this.x.c());
            }
        }
        w wVar = new w(a2, b2);
        s sVar = null;
        boolean z2 = false;
        while (!z) {
            try {
                v a3 = wVar.a();
                a.a.a.e.b.b b4 = wVar.b();
                Object a4 = eVar.a("http.user-token");
                if (this.p == null) {
                    a.a.a.e.e a5 = this.f108a.a(b4, a4);
                    if (qVar instanceof a.a.a.b.c.a) {
                        ((a.a.a.b.c.a) qVar).a(a5);
                    }
                    this.p = a5.a(a.a.a.b.d.b.c(this.o), TimeUnit.MILLISECONDS);
                    if (a.a.a.k.c.f(this.o) && this.p.c()) {
                        this.s.debug("Stale connection check");
                        if (this.p.d()) {
                            this.s.debug("Stale connection detected");
                            this.p.close();
                        }
                    }
                }
                if (qVar instanceof a.a.a.b.c.a) {
                    ((a.a.a.b.c.a) qVar).a(this.p);
                }
                try {
                    a(wVar, eVar);
                    String userInfo = a3.i().getUserInfo();
                    if (userInfo != null) {
                        this.q.a(new a.a.a.h.a.b(), new t(userInfo));
                    }
                    if (this.x != null) {
                        nVar = this.x;
                    } else {
                        URI i2 = a3.i();
                        if (i2.isAbsolute()) {
                            nVar = f.b(i2);
                        }
                    }
                    if (nVar == null) {
                        nVar = b4.a();
                    }
                    a3.k();
                    a(a3, b4);
                    eVar.a("http.target_host", nVar);
                    eVar.a("http.route", b4);
                    eVar.a("http.connection", this.p);
                    this.e.a(a3, this.f, eVar);
                    s b5 = b(wVar, eVar);
                    if (b5 == null) {
                        sVar = b5;
                    } else {
                        b5.a(this.o);
                        this.e.a(b5, this.f, eVar);
                        z2 = this.c.a(b5, eVar);
                        if (z2) {
                            long a6 = this.d.a(b5, eVar);
                            if (this.s.isDebugEnabled()) {
                                this.s.debug("Connection can be kept alive " + (a6 > 0 ? "for " + a6 + " " + TimeUnit.MILLISECONDS : "indefinitely"));
                            }
                            this.p.a(a6, TimeUnit.MILLISECONDS);
                        }
                        w a7 = a(wVar, b5, eVar);
                        if (a7 == null) {
                            z = true;
                        } else {
                            if (z2) {
                                a.a.a.n.g.a(b5.b());
                                this.p.k();
                            } else {
                                this.p.close();
                                if (this.r.b().compareTo((Enum) a.a.a.a.b.CHALLENGED) > 0 && this.r.c() != null && this.r.c().c()) {
                                    this.s.debug("Resetting proxy auth state");
                                    this.r.a();
                                }
                                if (this.q.b().compareTo((Enum) a.a.a.a.b.CHALLENGED) > 0 && this.q.c() != null && this.q.c().c()) {
                                    this.s.debug("Resetting target auth state");
                                    this.q.a();
                                }
                            }
                            if (!a7.b().equals(wVar.b())) {
                                a();
                            }
                            wVar = a7;
                        }
                        if (this.p != null) {
                            if (a4 == null) {
                                obj = this.n.a(eVar);
                                eVar.a("http.user-token", obj);
                            } else {
                                obj = a4;
                            }
                            if (obj != null) {
                                this.p.a(obj);
                            }
                        }
                        sVar = b5;
                    }
                } catch (y e2) {
                    if (this.s.isDebugEnabled()) {
                        this.s.debug(e2.getMessage());
                    }
                    sVar = e2.a();
                }
            } catch (InterruptedException e3) {
                Thread.currentThread().interrupt();
                throw new InterruptedIOException();
            } catch (a.a.a.h.c.c e4) {
                InterruptedIOException interruptedIOException = new InterruptedIOException("Connection has been shut down");
                interruptedIOException.initCause(e4);
                throw interruptedIOException;
            } catch (a.a.a.m e5) {
                b();
                throw e5;
            } catch (IOException e6) {
                b();
                throw e6;
            } catch (RuntimeException e7) {
                b();
                throw e7;
            }
        }
        if (sVar == null || sVar.b() == null || !sVar.b().g()) {
            if (z2) {
                this.p.k();
            }
            a();
        } else {
            sVar.a(new a.a.a.e.a(sVar.b(), this.p, z2));
        }
        return sVar;
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.p.h();
        } catch (IOException e2) {
            this.s.debug("IOException releasing connection", e2);
        }
        this.p = null;
    }

    /* access modifiers changed from: protected */
    public void a(a.a.a.e.b.b bVar, a.a.a.m.e eVar) {
        int a2;
        a.a.a.e.b.a aVar = new a.a.a.e.b.a();
        do {
            a.a.a.e.b.b j2 = this.p.j();
            a2 = aVar.a(bVar, j2);
            switch (a2) {
                case -1:
                    throw new a.a.a.m("Unable to establish route: planned = " + bVar + "; current = " + j2);
                case 0:
                    break;
                case 1:
                case 2:
                    this.p.a(bVar, eVar, this.o);
                    continue;
                case 3:
                    boolean b2 = b(bVar, eVar);
                    this.s.debug("Tunnel to target created.");
                    this.p.a(b2, this.o);
                    continue;
                case 4:
                    int c2 = j2.c() - 1;
                    boolean a3 = a(bVar, c2, eVar);
                    this.s.debug("Tunnel to proxy created.");
                    this.p.a(bVar.a(c2), a3, this.o);
                    continue;
                case 5:
                    this.p.a(eVar, this.o);
                    continue;
                default:
                    throw new IllegalStateException("Unknown step indicator " + a2 + " from RouteDirector.");
            }
        } while (a2 > 0);
    }

    /* access modifiers changed from: protected */
    public void a(v vVar, a.a.a.e.b.b bVar) {
        try {
            URI i2 = vVar.i();
            vVar.a((bVar.d() == null || bVar.e()) ? i2.isAbsolute() ? f.a(i2, null, true) : f.a(i2) : !i2.isAbsolute() ? f.a(i2, bVar.a(), true) : f.a(i2));
        } catch (URISyntaxException e2) {
            throw new ab("Invalid URI: " + vVar.g().c(), e2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(a.a.a.e.b.b bVar, int i2, a.a.a.m.e eVar) {
        throw new a.a.a.m("Proxy chains are not supported.");
    }

    /* access modifiers changed from: protected */
    public a.a.a.e.b.b b(a.a.a.n nVar, a.a.a.q qVar, a.a.a.m.e eVar) {
        d dVar = this.b;
        if (nVar == null) {
            nVar = (a.a.a.n) qVar.f().a("http.default-host");
        }
        return dVar.a(nVar, qVar, eVar);
    }

    /* access modifiers changed from: protected */
    public boolean b(a.a.a.e.b.b bVar, a.a.a.m.e eVar) {
        s a2;
        a.a.a.n d2 = bVar.d();
        a.a.a.n a3 = bVar.a();
        while (true) {
            if (!this.p.c()) {
                this.p.a(bVar, eVar, this.o);
            }
            a.a.a.q c2 = c(bVar, eVar);
            c2.a(this.o);
            eVar.a("http.target_host", a3);
            eVar.a("http.route", bVar);
            eVar.a("http.proxy_host", d2);
            eVar.a("http.connection", this.p);
            eVar.a("http.request", c2);
            this.e.a(c2, this.f, eVar);
            a2 = this.e.a(c2, this.p, eVar);
            a2.a(this.o);
            this.e.a(a2, this.f, eVar);
            if (a2.a().b() < 200) {
                throw new a.a.a.m("Unexpected response to CONNECT request: " + a2.a());
            } else if (a.a.a.b.d.b.b(this.o)) {
                if (this.t.a(d2, a2, this.m, this.r, eVar) && this.t.c(d2, a2, this.m, this.r, eVar)) {
                    if (this.c.a(a2, eVar)) {
                        this.s.debug("Connection kept alive");
                        a.a.a.n.g.a(a2.b());
                    } else {
                        this.p.close();
                    }
                }
            }
        }
        if (a2.a().b() > 299) {
            a.a.a.k b2 = a2.b();
            if (b2 != null) {
                a2.a(new a.a.a.g.c(b2));
            }
            this.p.close();
            throw new y("CONNECT refused by proxy: " + a2.a(), a2);
        }
        this.p.k();
        return false;
    }

    /* access modifiers changed from: protected */
    public a.a.a.q c(a.a.a.e.b.b bVar, a.a.a.m.e eVar) {
        a.a.a.n a2 = bVar.a();
        String a3 = a2.a();
        int b2 = a2.b();
        if (b2 < 0) {
            b2 = this.f108a.a().a(a2.c()).a();
        }
        StringBuilder sb = new StringBuilder(a3.length() + 6);
        sb.append(a3);
        sb.append(':');
        sb.append(Integer.toString(b2));
        return new a.a.a.j.g("CONNECT", sb.toString(), a.a.a.k.f.b(this.o));
    }
}
