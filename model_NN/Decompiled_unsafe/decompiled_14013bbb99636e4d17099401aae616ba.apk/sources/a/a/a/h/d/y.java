package a.a.a.h.d;

import a.a.a.f.i;
import a.a.a.f.j;
import a.a.a.f.k;
import a.a.a.k.e;
import java.util.Collection;

@Deprecated
public class y implements j, k {

    /* renamed from: a  reason: collision with root package name */
    private final i f143a;

    public y() {
        this(null);
    }

    public y(String[] strArr) {
        this.f143a = new x(strArr);
    }

    public i a(e eVar) {
        if (eVar == null) {
            return new x();
        }
        Collection collection = (Collection) eVar.a("http.protocol.cookie-datepatterns");
        return new x(collection != null ? (String[]) collection.toArray(new String[collection.size()]) : null);
    }

    public i a(a.a.a.m.e eVar) {
        return this.f143a;
    }
}
