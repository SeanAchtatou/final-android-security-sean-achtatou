package a.a.a.h.b;

import a.a.a.a.h;
import a.a.a.a.q;
import a.a.a.b.a.a;
import a.a.a.b.i;
import a.a.a.m.e;
import a.a.a.n;
import a.a.a.n.d;
import a.a.a.s;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

abstract class c implements a.a.a.b.c {
    private static final List b = Collections.unmodifiableList(Arrays.asList("Negotiate", "Kerberos", "NTLM", "Digest", "Basic"));

    /* renamed from: a  reason: collision with root package name */
    private final Log f97a = LogFactory.getLog(getClass());
    private final int c;
    private final String d;

    c(int i, String str) {
        this.c = i;
        this.d = str;
    }

    /* access modifiers changed from: package-private */
    public abstract Collection a(a aVar);

    public Queue a(Map map, n nVar, s sVar, e eVar) {
        a.a.a.n.a.a(map, "Map of auth challenges");
        a.a.a.n.a.a(nVar, "Host");
        a.a.a.n.a.a(sVar, "HTTP response");
        a.a.a.n.a.a(eVar, "HTTP context");
        a.a.a.b.e.a a2 = a.a.a.b.e.a.a(eVar);
        LinkedList linkedList = new LinkedList();
        a.a.a.d.a f = a2.f();
        if (f == null) {
            this.f97a.debug("Auth scheme registry not set in the context");
            return linkedList;
        }
        i g = a2.g();
        if (g == null) {
            this.f97a.debug("Credentials provider not set in the context");
            return linkedList;
        }
        Collection<String> a3 = a(a2.k());
        if (a3 == null) {
            a3 = b;
        }
        if (this.f97a.isDebugEnabled()) {
            this.f97a.debug("Authentication schemes in the order of preference: " + a3);
        }
        for (String str : a3) {
            a.a.a.e eVar2 = (a.a.a.e) map.get(str.toLowerCase(Locale.ROOT));
            if (eVar2 != null) {
                a.a.a.a.e eVar3 = (a.a.a.a.e) f.b(str);
                if (eVar3 != null) {
                    a.a.a.a.c a4 = eVar3.a(eVar);
                    a4.a(eVar2);
                    a.a.a.a.n a5 = g.a(new h(nVar.a(), nVar.b(), a4.b(), a4.a()));
                    if (a5 != null) {
                        linkedList.add(new a.a.a.a.a(a4, a5));
                    }
                } else if (this.f97a.isWarnEnabled()) {
                    this.f97a.warn("Authentication scheme " + str + " not supported");
                }
            } else if (this.f97a.isDebugEnabled()) {
                this.f97a.debug("Challenge for " + str + " authentication scheme not available");
            }
        }
        return linkedList;
    }

    public void a(n nVar, a.a.a.a.c cVar, e eVar) {
        a.a.a.n.a.a(nVar, "Host");
        a.a.a.n.a.a(cVar, "Auth scheme");
        a.a.a.n.a.a(eVar, "HTTP context");
        a.a.a.b.e.a a2 = a.a.a.b.e.a.a(eVar);
        if (a(cVar)) {
            a.a.a.b.a h = a2.h();
            if (h == null) {
                h = new d();
                a2.a(h);
            }
            if (this.f97a.isDebugEnabled()) {
                this.f97a.debug("Caching '" + cVar.a() + "' auth scheme for " + nVar);
            }
            h.a(nVar, cVar);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(a.a.a.a.c cVar) {
        if (cVar == null || !cVar.d()) {
            return false;
        }
        String a2 = cVar.a();
        return a2.equalsIgnoreCase("Basic") || a2.equalsIgnoreCase("Digest");
    }

    public boolean a(n nVar, s sVar, e eVar) {
        a.a.a.n.a.a(sVar, "HTTP response");
        return sVar.a().b() == this.c;
    }

    public Map b(n nVar, s sVar, e eVar) {
        d dVar;
        int i;
        a.a.a.n.a.a(sVar, "HTTP response");
        a.a.a.e[] b2 = sVar.b(this.d);
        HashMap hashMap = new HashMap(b2.length);
        for (a.a.a.e eVar2 : b2) {
            if (eVar2 instanceof a.a.a.d) {
                dVar = ((a.a.a.d) eVar2).a();
                i = ((a.a.a.d) eVar2).b();
            } else {
                String d2 = eVar2.d();
                if (d2 == null) {
                    throw new q("Header value is null");
                }
                d dVar2 = new d(d2.length());
                dVar2.a(d2);
                dVar = dVar2;
                i = 0;
            }
            while (i < dVar.length() && a.a.a.m.d.a(dVar.charAt(i))) {
                i++;
            }
            int i2 = i;
            while (i2 < dVar.length() && !a.a.a.m.d.a(dVar.charAt(i2))) {
                i2++;
            }
            hashMap.put(dVar.a(i, i2).toLowerCase(Locale.ROOT), eVar2);
        }
        return hashMap;
    }

    public void b(n nVar, a.a.a.a.c cVar, e eVar) {
        a.a.a.n.a.a(nVar, "Host");
        a.a.a.n.a.a(eVar, "HTTP context");
        a.a.a.b.a h = a.a.a.b.e.a.a(eVar).h();
        if (h != null) {
            if (this.f97a.isDebugEnabled()) {
                this.f97a.debug("Clearing cached auth scheme for " + nVar);
            }
            h.b(nVar);
        }
    }
}
