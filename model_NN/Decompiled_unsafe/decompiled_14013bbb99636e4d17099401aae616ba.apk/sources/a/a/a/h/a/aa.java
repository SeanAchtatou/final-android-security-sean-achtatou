package a.a.a.h.a;

import a.a.a.c;
import a.a.a.f;
import a.a.a.j.u;
import a.a.a.n.d;
import a.a.a.q;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public abstract class aa extends a implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final Map f71a = new HashMap();
    private transient Charset b;

    public aa(Charset charset) {
        this.b = charset == null ? c.b : charset;
    }

    /* access modifiers changed from: package-private */
    public String a(q qVar) {
        String str = (String) qVar.f().a("http.auth.credential-charset");
        return str == null ? g().name() : str;
    }

    public String a(String str) {
        if (str == null) {
            return null;
        }
        return (String) this.f71a.get(str.toLowerCase(Locale.ROOT));
    }

    /* access modifiers changed from: protected */
    public void a(d dVar, int i, int i2) {
        f[] a2 = a.a.a.j.f.b.a(dVar, new u(i, dVar.length()));
        this.f71a.clear();
        for (f fVar : a2) {
            this.f71a.put(fVar.a().toLowerCase(Locale.ROOT), fVar.b());
        }
    }

    public String b() {
        return a("realm");
    }

    public Charset g() {
        return this.b != null ? this.b : c.b;
    }

    /* access modifiers changed from: protected */
    public Map h() {
        return this.f71a;
    }
}
