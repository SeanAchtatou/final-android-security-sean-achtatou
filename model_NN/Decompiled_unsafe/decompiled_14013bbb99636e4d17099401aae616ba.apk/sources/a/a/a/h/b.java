package a.a.a.h;

import a.a.a.aa;
import a.a.a.ac;
import a.a.a.ag;
import a.a.a.h;
import a.a.a.j.o;
import a.a.a.m.e;
import a.a.a.n.a;
import a.a.a.s;
import a.a.a.v;

public class b implements a.a.a.b {

    /* renamed from: a  reason: collision with root package name */
    public static final b f94a = new b();

    private boolean a(s sVar) {
        int b = sVar.a().b();
        return (b < 200 || b == 204 || b == 304 || b == 205) ? false : true;
    }

    /* access modifiers changed from: protected */
    public ag a(h hVar) {
        return new o(hVar);
    }

    public boolean a(s sVar, e eVar) {
        boolean z = true;
        a.a(sVar, "HTTP response");
        a.a(eVar, "HTTP context");
        ac a2 = sVar.a().a();
        a.a.a.e c = sVar.c("Transfer-Encoding");
        if (c != null) {
            if (!"chunked".equalsIgnoreCase(c.d())) {
                return false;
            }
        } else if (a(sVar)) {
            a.a.a.e[] b = sVar.b("Content-Length");
            if (b.length != 1) {
                return false;
            }
            try {
                if (Integer.parseInt(b[0].d()) < 0) {
                    return false;
                }
            } catch (NumberFormatException e) {
                return false;
            }
        }
        h e2 = sVar.e("Connection");
        if (!e2.hasNext()) {
            e2 = sVar.e("Proxy-Connection");
        }
        if (e2.hasNext()) {
            try {
                ag a3 = a(e2);
                boolean z2 = false;
                while (a3.hasNext()) {
                    String a4 = a3.a();
                    if ("Close".equalsIgnoreCase(a4)) {
                        return false;
                    }
                    if ("Keep-Alive".equalsIgnoreCase(a4)) {
                        z2 = true;
                    }
                }
                if (z2) {
                    return true;
                }
            } catch (aa e3) {
                return false;
            }
        }
        if (a2.c(v.b)) {
            z = false;
        }
        return z;
    }
}
