package a.a.a.h.f;

import a.a.a.i.g;
import a.a.a.n.a;
import java.io.IOException;
import java.io.OutputStream;

public class n extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final g f161a;
    private boolean b = false;

    public n(g gVar) {
        this.f161a = (g) a.a(gVar, "Session output buffer");
    }

    public void close() {
        if (!this.b) {
            this.b = true;
            this.f161a.a();
        }
    }

    public void flush() {
        this.f161a.a();
    }

    public void write(int i) {
        if (this.b) {
            throw new IOException("Attempted write to closed stream.");
        }
        this.f161a.a(i);
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        if (this.b) {
            throw new IOException("Attempted write to closed stream.");
        }
        this.f161a.a(bArr, i, i2);
    }
}
