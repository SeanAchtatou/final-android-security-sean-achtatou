package a.a.a.h.f;

import a.a.a.i.f;
import a.a.a.n.a;
import java.io.InputStream;

public class m extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final f f160a;
    private boolean b = false;

    public m(f fVar) {
        this.f160a = (f) a.a(fVar, "Session input buffer");
    }

    public int available() {
        if (this.f160a instanceof a.a.a.i.a) {
            return ((a.a.a.i.a) this.f160a).e();
        }
        return 0;
    }

    public void close() {
        this.b = true;
    }

    public int read() {
        if (this.b) {
            return -1;
        }
        return this.f160a.a();
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.b) {
            return -1;
        }
        return this.f160a.a(bArr, i, i2);
    }
}
