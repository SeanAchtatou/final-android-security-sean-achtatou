package a.a.a.h.d;

import a.a.a.e;
import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.n;
import a.a.a.j.p;
import a.a.a.j.u;
import a.a.a.n.a;
import a.a.a.n.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class x extends r {
    public x() {
        this((String[]) null);
    }

    x(b... bVarArr) {
        super(bVarArr);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public x(java.lang.String[] r7) {
        /*
            r6 = this;
            r4 = 1
            r5 = 0
            r0 = 5
            a.a.a.f.b[] r1 = new a.a.a.f.b[r0]
            a.a.a.h.d.i r0 = new a.a.a.h.d.i
            r0.<init>()
            r1[r5] = r0
            a.a.a.h.d.v r0 = new a.a.a.h.d.v
            r0.<init>()
            r1[r4] = r0
            r0 = 2
            a.a.a.h.d.j r2 = new a.a.a.h.d.j
            r2.<init>()
            r1[r0] = r2
            r0 = 3
            a.a.a.h.d.e r2 = new a.a.a.h.d.e
            r2.<init>()
            r1[r0] = r2
            r2 = 4
            a.a.a.h.d.g r3 = new a.a.a.h.d.g
            if (r7 == 0) goto L_0x0037
            java.lang.Object r0 = r7.clone()
            java.lang.String[] r0 = (java.lang.String[]) r0
        L_0x002e:
            r3.<init>(r0)
            r1[r2] = r3
            r6.<init>(r1)
            return
        L_0x0037:
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.String r4 = "EEE, dd-MMM-yy HH:mm:ss z"
            r0[r5] = r4
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.h.d.x.<init>(java.lang.String[]):void");
    }

    public int a() {
        return 0;
    }

    public List a(e eVar, f fVar) {
        d dVar;
        u uVar;
        a.a(eVar, "Header");
        a.a(fVar, "Cookie origin");
        if (!eVar.c().equalsIgnoreCase("Set-Cookie")) {
            throw new n("Unrecognized cookie header '" + eVar.toString() + "'");
        }
        w wVar = w.f142a;
        if (eVar instanceof a.a.a.d) {
            dVar = ((a.a.a.d) eVar).a();
            uVar = new u(((a.a.a.d) eVar).b(), dVar.length());
        } else {
            String d = eVar.d();
            if (d == null) {
                throw new n("Header value is null");
            }
            dVar = new d(d.length());
            dVar.a(d);
            uVar = new u(0, dVar.length());
        }
        return a(new a.a.a.f[]{wVar.a(dVar, uVar)}, fVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
     arg types: [java.util.List, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection */
    public List a(List list) {
        a.a((Collection) list, "List of cookies");
        d dVar = new d(list.size() * 20);
        dVar.a("Cookie");
        dVar.a(": ");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                c cVar = (c) list.get(i2);
                if (i2 > 0) {
                    dVar.a("; ");
                }
                dVar.a(cVar.a());
                String b = cVar.b();
                if (b != null) {
                    dVar.a("=");
                    dVar.a(b);
                }
                i = i2 + 1;
            } else {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new p(dVar));
                return arrayList;
            }
        }
    }

    public e b() {
        return null;
    }

    public String toString() {
        return "netscape";
    }
}
