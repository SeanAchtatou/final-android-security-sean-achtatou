package a.a.a.h.d;

import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.h;
import a.a.a.f.o;
import a.a.a.n.a;

public class i implements b {
    static boolean a(String str, String str2) {
        if (str2 == null) {
            str2 = "/";
        }
        if (str2.length() > 1 && str2.endsWith("/")) {
            str2 = str2.substring(0, str2.length() - 1);
        }
        return str.startsWith(str2) && (str2.equals("/") || str.length() == str2.length() || str.charAt(str2.length()) == '/');
    }

    public String a() {
        return "path";
    }

    public void a(c cVar, f fVar) {
        if (!b(cVar, fVar)) {
            throw new h("Illegal 'path' attribute \"" + cVar.e() + "\". Path of origin: \"" + fVar.b() + "\"");
        }
    }

    public void a(o oVar, String str) {
        a.a(oVar, "Cookie");
        if (a.a.a.n.i.b(str)) {
            str = "/";
        }
        oVar.e(str);
    }

    public boolean b(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        return a(fVar.b(), cVar.e());
    }
}
