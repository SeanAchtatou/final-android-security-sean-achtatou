package a.a.a.h.a;

import a.a.a.c;
import a.a.a.n.e;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Locale;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

final class p implements n {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Charset f84a = e.a("UnicodeLittleUnmarked");
    /* access modifiers changed from: private */
    public static final Charset b = c.b;
    private static final SecureRandom c;
    /* access modifiers changed from: private */
    public static final byte[] d;
    private static final u e = new u();

    static {
        SecureRandom secureRandom = null;
        try {
            secureRandom = SecureRandom.getInstance("SHA1PRNG");
        } catch (Exception e2) {
        }
        c = secureRandom;
        byte[] bytes = "NTLMSSP".getBytes(c.b);
        d = new byte[(bytes.length + 1)];
        System.arraycopy(bytes, 0, d, 0, bytes.length);
        d[bytes.length] = 0;
    }

    p() {
    }

    static int a(int i, int i2) {
        return (i << i2) | (i >>> (32 - i2));
    }

    static int a(int i, int i2, int i3) {
        return (i & i2) | ((i ^ -1) & i3);
    }

    static String a(String str, String str2, String str3, String str4, byte[] bArr, int i, String str5, byte[] bArr2) {
        return new w(str4, str3, str, str2, bArr, i, str5, bArr2).b();
    }

    private static void a(byte[] bArr) {
        for (int i = 0; i < bArr.length; i++) {
            byte b2 = bArr[i];
            if ((((b2 >>> 1) ^ ((((((b2 >>> 7) ^ (b2 >>> 6)) ^ (b2 >>> 5)) ^ (b2 >>> 4)) ^ (b2 >>> 3)) ^ (b2 >>> 2))) & 1) == 0) {
                bArr[i] = (byte) (bArr[i] | 1);
            } else {
                bArr[i] = (byte) (bArr[i] & -2);
            }
        }
    }

    static void a(byte[] bArr, int i, int i2) {
        bArr[i2] = (byte) (i & 255);
        bArr[i2 + 1] = (byte) ((i >> 8) & 255);
        bArr[i2 + 2] = (byte) ((i >> 16) & 255);
        bArr[i2 + 3] = (byte) ((i >> 24) & 255);
    }

    static byte[] a(byte[] bArr, byte[] bArr2) {
        r rVar = new r(bArr2);
        rVar.a(bArr);
        return rVar.a();
    }

    static byte[] a(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr2);
            instance.update(bArr3);
            byte[] bArr4 = new byte[8];
            System.arraycopy(instance.digest(), 0, bArr4, 0, 8);
            return d(bArr, bArr4);
        } catch (Exception e2) {
            if (e2 instanceof o) {
                throw ((o) e2);
            }
            throw new o(e2.getMessage(), e2);
        }
    }

    static int b(int i, int i2, int i3) {
        return (i & i2) | (i & i3) | (i2 & i3);
    }

    static String b(String str, String str2) {
        return e.b();
    }

    static byte[] b(byte[] bArr, byte[] bArr2) {
        try {
            Cipher instance = Cipher.getInstance("RC4");
            instance.init(1, new SecretKeySpec(bArr2, "RC4"));
            return instance.doFinal(bArr);
        } catch (Exception e2) {
            throw new o(e2.getMessage(), e2);
        }
    }

    static int c(int i, int i2, int i3) {
        return (i ^ i2) ^ i3;
    }

    /* access modifiers changed from: private */
    public static byte[] c(String str, String str2, byte[] bArr) {
        if (f84a == null) {
            throw new o("Unicode not supported");
        }
        r rVar = new r(bArr);
        rVar.a(str2.toUpperCase(Locale.ROOT).getBytes(f84a));
        if (str != null) {
            rVar.a(str.toUpperCase(Locale.ROOT).getBytes(f84a));
        }
        return rVar.a();
    }

    /* access modifiers changed from: private */
    public static int d(byte[] bArr, int i) {
        if (bArr.length >= i + 4) {
            return (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 3] & 255) << 24);
        }
        throw new o("NTLM authentication - buffer too small for DWORD");
    }

    /* access modifiers changed from: private */
    public static byte[] d(String str, String str2, byte[] bArr) {
        if (f84a == null) {
            throw new o("Unicode not supported");
        }
        r rVar = new r(bArr);
        rVar.a(str2.toUpperCase(Locale.ROOT).getBytes(f84a));
        if (str != null) {
            rVar.a(str.getBytes(f84a));
        }
        return rVar.a();
    }

    /* access modifiers changed from: private */
    public static byte[] d(byte[] bArr, byte[] bArr2) {
        try {
            byte[] bArr3 = new byte[21];
            System.arraycopy(bArr, 0, bArr3, 0, 16);
            Key g = g(bArr3, 0);
            Key g2 = g(bArr3, 7);
            Key g3 = g(bArr3, 14);
            Cipher instance = Cipher.getInstance("DES/ECB/NoPadding");
            instance.init(1, g);
            byte[] doFinal = instance.doFinal(bArr2);
            instance.init(1, g2);
            byte[] doFinal2 = instance.doFinal(bArr2);
            instance.init(1, g3);
            byte[] doFinal3 = instance.doFinal(bArr2);
            byte[] bArr4 = new byte[24];
            System.arraycopy(doFinal, 0, bArr4, 0, 8);
            System.arraycopy(doFinal2, 0, bArr4, 8, 8);
            System.arraycopy(doFinal3, 0, bArr4, 16, 8);
            return bArr4;
        } catch (Exception e2) {
            throw new o(e2.getMessage(), e2);
        }
    }

    /* access modifiers changed from: private */
    public static byte[] d(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        r rVar = new r(bArr);
        rVar.a(bArr2);
        rVar.a(bArr3);
        byte[] a2 = rVar.a();
        byte[] bArr4 = new byte[(a2.length + bArr3.length)];
        System.arraycopy(a2, 0, bArr4, 0, a2.length);
        System.arraycopy(bArr3, 0, bArr4, a2.length, bArr3.length);
        return bArr4;
    }

    private static int e(byte[] bArr, int i) {
        if (bArr.length >= i + 2) {
            return (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8);
        }
        throw new o("NTLM authentication - buffer too small for WORD");
    }

    private static String e(String str) {
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf(".");
        return indexOf != -1 ? str.substring(0, indexOf) : str;
    }

    /* access modifiers changed from: private */
    public static byte[] e(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        byte[] bArr4 = {1, 1, 0, 0};
        byte[] bArr5 = {0, 0, 0, 0};
        byte[] bArr6 = {0, 0, 0, 0};
        byte[] bArr7 = {0, 0, 0, 0};
        byte[] bArr8 = new byte[(bArr4.length + bArr5.length + bArr3.length + 8 + bArr6.length + bArr2.length + bArr7.length)];
        System.arraycopy(bArr4, 0, bArr8, 0, bArr4.length);
        int length = bArr4.length + 0;
        System.arraycopy(bArr5, 0, bArr8, length, bArr5.length);
        int length2 = length + bArr5.length;
        System.arraycopy(bArr3, 0, bArr8, length2, bArr3.length);
        int length3 = length2 + bArr3.length;
        System.arraycopy(bArr, 0, bArr8, length3, 8);
        int i = length3 + 8;
        System.arraycopy(bArr6, 0, bArr8, i, bArr6.length);
        int length4 = i + bArr6.length;
        System.arraycopy(bArr2, 0, bArr8, length4, bArr2.length);
        int length5 = length4 + bArr2.length;
        System.arraycopy(bArr7, 0, bArr8, length5, bArr7.length);
        int length6 = length5 + bArr7.length;
        return bArr8;
    }

    /* access modifiers changed from: private */
    public static String f(String str) {
        return e(str);
    }

    /* access modifiers changed from: private */
    public static byte[] f() {
        if (c == null) {
            throw new o("Random generator not available");
        }
        byte[] bArr = new byte[8];
        synchronized (c) {
            c.nextBytes(bArr);
        }
        return bArr;
    }

    /* access modifiers changed from: private */
    public static byte[] f(byte[] bArr, int i) {
        int e2 = e(bArr, i);
        int d2 = d(bArr, i + 4);
        if (bArr.length < d2 + e2) {
            throw new o("NTLM authentication - buffer too small for data item");
        }
        byte[] bArr2 = new byte[e2];
        System.arraycopy(bArr, d2, bArr2, 0, e2);
        return bArr2;
    }

    /* access modifiers changed from: private */
    public static String g(String str) {
        return e(str);
    }

    /* access modifiers changed from: private */
    public static Key g(byte[] bArr, int i) {
        byte[] bArr2 = new byte[7];
        System.arraycopy(bArr, i, bArr2, 0, 7);
        byte[] bArr3 = {bArr2[0], (byte) ((bArr2[0] << 7) | ((bArr2[1] & 255) >>> 1)), (byte) ((bArr2[1] << 6) | ((bArr2[2] & 255) >>> 2)), (byte) ((bArr2[2] << 5) | ((bArr2[3] & 255) >>> 3)), (byte) ((bArr2[3] << 4) | ((bArr2[4] & 255) >>> 4)), (byte) ((bArr2[4] << 3) | ((bArr2[5] & 255) >>> 5)), (byte) ((bArr2[5] << 2) | ((bArr2[6] & 255) >>> 6)), (byte) (bArr2[6] << 1)};
        a(bArr3);
        return new SecretKeySpec(bArr3, "DES");
    }

    /* access modifiers changed from: private */
    public static byte[] g() {
        if (c == null) {
            throw new o("Random generator not available");
        }
        byte[] bArr = new byte[16];
        synchronized (c) {
            c.nextBytes(bArr);
        }
        return bArr;
    }

    /* access modifiers changed from: private */
    public static byte[] h(String str) {
        try {
            byte[] bytes = str.toUpperCase(Locale.ROOT).getBytes(c.b);
            byte[] bArr = new byte[14];
            System.arraycopy(bytes, 0, bArr, 0, Math.min(bytes.length, 14));
            Key g = g(bArr, 0);
            Key g2 = g(bArr, 7);
            byte[] bytes2 = "KGS!@#$%".getBytes(c.b);
            Cipher instance = Cipher.getInstance("DES/ECB/NoPadding");
            instance.init(1, g);
            byte[] doFinal = instance.doFinal(bytes2);
            instance.init(1, g2);
            byte[] doFinal2 = instance.doFinal(bytes2);
            byte[] bArr2 = new byte[16];
            System.arraycopy(doFinal, 0, bArr2, 0, 8);
            System.arraycopy(doFinal2, 0, bArr2, 8, 8);
            return bArr2;
        } catch (Exception e2) {
            throw new o(e2.getMessage(), e2);
        }
    }

    /* access modifiers changed from: private */
    public static byte[] i(String str) {
        if (f84a == null) {
            throw new o("Unicode not supported");
        }
        byte[] bytes = str.getBytes(f84a);
        s sVar = new s();
        sVar.a(bytes);
        return sVar.a();
    }

    public String a(String str, String str2) {
        return b(str2, str);
    }

    public String a(String str, String str2, String str3, String str4, String str5) {
        v vVar = new v(str5);
        return a(str, str2, str4, str3, vVar.c(), vVar.f(), vVar.d(), vVar.e());
    }
}
