package a.a.a.h.d;

import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.h;
import a.a.a.f.n;
import a.a.a.f.o;
import a.a.a.f.p;
import a.a.a.n.a;

public class aj implements b {
    public String a() {
        return "version";
    }

    public void a(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        if ((cVar instanceof p) && (cVar instanceof a.a.a.f.a) && !((a.a.a.f.a) cVar).b("version")) {
            throw new h("Violates RFC 2965. Version attribute is required.");
        }
    }

    public void a(o oVar, String str) {
        int i;
        a.a(oVar, "Cookie");
        if (str == null) {
            throw new n("Missing value for version attribute");
        }
        try {
            i = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            i = -1;
        }
        if (i < 0) {
            throw new n("Invalid cookie version.");
        }
        oVar.a(i);
    }

    public boolean b(c cVar, f fVar) {
        return true;
    }
}
