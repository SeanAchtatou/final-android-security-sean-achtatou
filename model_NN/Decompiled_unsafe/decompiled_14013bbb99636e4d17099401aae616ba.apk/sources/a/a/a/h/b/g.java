package a.a.a.h.b;

import a.a.a.k.a;
import a.a.a.k.e;

@Deprecated
public class g extends a {

    /* renamed from: a  reason: collision with root package name */
    protected final e f101a;
    protected final e b;
    protected final e c;
    protected final e d;

    public g(e eVar, e eVar2, e eVar3, e eVar4) {
        this.f101a = eVar;
        this.b = eVar2;
        this.c = eVar3;
        this.d = eVar4;
    }

    public e a(String str, Object obj) {
        throw new UnsupportedOperationException("Setting parameters in a stack is not supported.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public Object a(String str) {
        a.a.a.n.a.a((Object) str, "Parameter name");
        Object obj = null;
        if (this.d != null) {
            obj = this.d.a(str);
        }
        if (obj == null && this.c != null) {
            obj = this.c.a(str);
        }
        if (obj == null && this.b != null) {
            obj = this.b.a(str);
        }
        return (obj != null || this.f101a == null) ? obj : this.f101a.a(str);
    }
}
