package a.a.a.h.a;

class s {

    /* renamed from: a  reason: collision with root package name */
    protected int f87a = 1732584193;
    protected int b = -271733879;
    protected int c = -1732584194;
    protected int d = 271733878;
    protected long e = 0;
    protected byte[] f = new byte[64];

    s() {
    }

    /* access modifiers changed from: package-private */
    public void a(byte[] bArr) {
        int i = (int) (this.e & 63);
        int i2 = 0;
        while ((bArr.length - i2) + i >= this.f.length) {
            int length = this.f.length - i;
            System.arraycopy(bArr, i2, this.f, i, length);
            this.e += (long) length;
            i2 += length;
            b();
            i = 0;
        }
        if (i2 < bArr.length) {
            int length2 = bArr.length - i2;
            System.arraycopy(bArr, i2, this.f, i, length2);
            this.e += (long) length2;
            int i3 = i + length2;
        }
    }

    /* access modifiers changed from: protected */
    public void a(int[] iArr) {
        this.f87a = p.a(this.f87a + p.a(this.b, this.c, this.d) + iArr[0], 3);
        this.d = p.a(this.d + p.a(this.f87a, this.b, this.c) + iArr[1], 7);
        this.c = p.a(this.c + p.a(this.d, this.f87a, this.b) + iArr[2], 11);
        this.b = p.a(this.b + p.a(this.c, this.d, this.f87a) + iArr[3], 19);
        this.f87a = p.a(this.f87a + p.a(this.b, this.c, this.d) + iArr[4], 3);
        this.d = p.a(this.d + p.a(this.f87a, this.b, this.c) + iArr[5], 7);
        this.c = p.a(this.c + p.a(this.d, this.f87a, this.b) + iArr[6], 11);
        this.b = p.a(this.b + p.a(this.c, this.d, this.f87a) + iArr[7], 19);
        this.f87a = p.a(this.f87a + p.a(this.b, this.c, this.d) + iArr[8], 3);
        this.d = p.a(this.d + p.a(this.f87a, this.b, this.c) + iArr[9], 7);
        this.c = p.a(this.c + p.a(this.d, this.f87a, this.b) + iArr[10], 11);
        this.b = p.a(this.b + p.a(this.c, this.d, this.f87a) + iArr[11], 19);
        this.f87a = p.a(this.f87a + p.a(this.b, this.c, this.d) + iArr[12], 3);
        this.d = p.a(this.d + p.a(this.f87a, this.b, this.c) + iArr[13], 7);
        this.c = p.a(this.c + p.a(this.d, this.f87a, this.b) + iArr[14], 11);
        this.b = p.a(this.b + p.a(this.c, this.d, this.f87a) + iArr[15], 19);
    }

    /* access modifiers changed from: package-private */
    public byte[] a() {
        int i = (int) (this.e & 63);
        int i2 = i < 56 ? 56 - i : 120 - i;
        byte[] bArr = new byte[(i2 + 8)];
        bArr[0] = Byte.MIN_VALUE;
        for (int i3 = 0; i3 < 8; i3++) {
            bArr[i2 + i3] = (byte) ((int) ((this.e * 8) >>> (i3 * 8)));
        }
        a(bArr);
        byte[] bArr2 = new byte[16];
        p.a(bArr2, this.f87a, 0);
        p.a(bArr2, this.b, 4);
        p.a(bArr2, this.c, 8);
        p.a(bArr2, this.d, 12);
        return bArr2;
    }

    /* access modifiers changed from: protected */
    public void b() {
        int[] iArr = new int[16];
        for (int i = 0; i < 16; i++) {
            iArr[i] = (this.f[i * 4] & 255) + ((this.f[(i * 4) + 1] & 255) << 8) + ((this.f[(i * 4) + 2] & 255) << 16) + ((this.f[(i * 4) + 3] & 255) << 24);
        }
        int i2 = this.f87a;
        int i3 = this.b;
        int i4 = this.c;
        int i5 = this.d;
        a(iArr);
        b(iArr);
        c(iArr);
        this.f87a = i2 + this.f87a;
        this.b += i3;
        this.c += i4;
        this.d += i5;
    }

    /* access modifiers changed from: protected */
    public void b(int[] iArr) {
        this.f87a = p.a(this.f87a + p.b(this.b, this.c, this.d) + iArr[0] + 1518500249, 3);
        this.d = p.a(this.d + p.b(this.f87a, this.b, this.c) + iArr[4] + 1518500249, 5);
        this.c = p.a(this.c + p.b(this.d, this.f87a, this.b) + iArr[8] + 1518500249, 9);
        this.b = p.a(this.b + p.b(this.c, this.d, this.f87a) + iArr[12] + 1518500249, 13);
        this.f87a = p.a(this.f87a + p.b(this.b, this.c, this.d) + iArr[1] + 1518500249, 3);
        this.d = p.a(this.d + p.b(this.f87a, this.b, this.c) + iArr[5] + 1518500249, 5);
        this.c = p.a(this.c + p.b(this.d, this.f87a, this.b) + iArr[9] + 1518500249, 9);
        this.b = p.a(this.b + p.b(this.c, this.d, this.f87a) + iArr[13] + 1518500249, 13);
        this.f87a = p.a(this.f87a + p.b(this.b, this.c, this.d) + iArr[2] + 1518500249, 3);
        this.d = p.a(this.d + p.b(this.f87a, this.b, this.c) + iArr[6] + 1518500249, 5);
        this.c = p.a(this.c + p.b(this.d, this.f87a, this.b) + iArr[10] + 1518500249, 9);
        this.b = p.a(this.b + p.b(this.c, this.d, this.f87a) + iArr[14] + 1518500249, 13);
        this.f87a = p.a(this.f87a + p.b(this.b, this.c, this.d) + iArr[3] + 1518500249, 3);
        this.d = p.a(this.d + p.b(this.f87a, this.b, this.c) + iArr[7] + 1518500249, 5);
        this.c = p.a(this.c + p.b(this.d, this.f87a, this.b) + iArr[11] + 1518500249, 9);
        this.b = p.a(this.b + p.b(this.c, this.d, this.f87a) + iArr[15] + 1518500249, 13);
    }

    /* access modifiers changed from: protected */
    public void c(int[] iArr) {
        this.f87a = p.a(this.f87a + p.c(this.b, this.c, this.d) + iArr[0] + 1859775393, 3);
        this.d = p.a(this.d + p.c(this.f87a, this.b, this.c) + iArr[8] + 1859775393, 9);
        this.c = p.a(this.c + p.c(this.d, this.f87a, this.b) + iArr[4] + 1859775393, 11);
        this.b = p.a(this.b + p.c(this.c, this.d, this.f87a) + iArr[12] + 1859775393, 15);
        this.f87a = p.a(this.f87a + p.c(this.b, this.c, this.d) + iArr[2] + 1859775393, 3);
        this.d = p.a(this.d + p.c(this.f87a, this.b, this.c) + iArr[10] + 1859775393, 9);
        this.c = p.a(this.c + p.c(this.d, this.f87a, this.b) + iArr[6] + 1859775393, 11);
        this.b = p.a(this.b + p.c(this.c, this.d, this.f87a) + iArr[14] + 1859775393, 15);
        this.f87a = p.a(this.f87a + p.c(this.b, this.c, this.d) + iArr[1] + 1859775393, 3);
        this.d = p.a(this.d + p.c(this.f87a, this.b, this.c) + iArr[9] + 1859775393, 9);
        this.c = p.a(this.c + p.c(this.d, this.f87a, this.b) + iArr[5] + 1859775393, 11);
        this.b = p.a(this.b + p.c(this.c, this.d, this.f87a) + iArr[13] + 1859775393, 15);
        this.f87a = p.a(this.f87a + p.c(this.b, this.c, this.d) + iArr[3] + 1859775393, 3);
        this.d = p.a(this.d + p.c(this.f87a, this.b, this.c) + iArr[11] + 1859775393, 9);
        this.c = p.a(this.c + p.c(this.d, this.f87a, this.b) + iArr[7] + 1859775393, 11);
        this.b = p.a(this.b + p.c(this.c, this.d, this.f87a) + iArr[15] + 1859775393, 15);
    }
}
