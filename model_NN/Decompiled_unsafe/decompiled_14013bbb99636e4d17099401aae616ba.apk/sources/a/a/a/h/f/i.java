package a.a.a.h.f;

import a.a.a.i.f;
import a.a.a.j.u;
import a.a.a.k.e;
import a.a.a.n.a;
import a.a.a.n.d;
import a.a.a.s;
import a.a.a.t;
import a.a.a.z;

public class i extends a {
    private final t b;
    private final d c = new d(128);

    @Deprecated
    public i(f fVar, a.a.a.j.t tVar, t tVar2, e eVar) {
        super(fVar, tVar, eVar);
        this.b = (t) a.a(tVar2, "Response factory");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public s b(f fVar) {
        this.c.a();
        if (fVar.a(this.c) == -1) {
            throw new z("The target server failed to respond");
        }
        return this.b.a(this.f150a.c(this.c, new u(0, this.c.length())), null);
    }
}
