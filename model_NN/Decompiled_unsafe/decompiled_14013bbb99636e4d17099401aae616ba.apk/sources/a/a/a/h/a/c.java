package a.a.a.h.a;

import a.a.a.a.d;
import a.a.a.a.e;
import java.nio.charset.Charset;

public class c implements d, e {

    /* renamed from: a  reason: collision with root package name */
    private final Charset f74a;

    public c() {
        this(null);
    }

    public c(Charset charset) {
        this.f74a = charset;
    }

    public a.a.a.a.c a(a.a.a.k.e eVar) {
        return new b();
    }

    public a.a.a.a.c a(a.a.a.m.e eVar) {
        return new b(this.f74a);
    }
}
