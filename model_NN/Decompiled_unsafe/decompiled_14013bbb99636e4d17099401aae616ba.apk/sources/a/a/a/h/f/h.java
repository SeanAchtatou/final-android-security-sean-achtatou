package a.a.a.h.f;

import a.a.a.i.g;
import a.a.a.n.a;
import java.io.IOException;
import java.io.OutputStream;

public class h extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final g f157a;
    private final long b;
    private long c = 0;
    private boolean d = false;

    public h(g gVar, long j) {
        this.f157a = (g) a.a(gVar, "Session output buffer");
        this.b = a.a(j, "Content length");
    }

    public void close() {
        if (!this.d) {
            this.d = true;
            this.f157a.a();
        }
    }

    public void flush() {
        this.f157a.a();
    }

    public void write(int i) {
        if (this.d) {
            throw new IOException("Attempted write to closed stream.");
        } else if (this.c < this.b) {
            this.f157a.a(i);
            this.c++;
        }
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        if (this.d) {
            throw new IOException("Attempted write to closed stream.");
        } else if (this.c < this.b) {
            long j = this.b - this.c;
            if (((long) i2) > j) {
                i2 = (int) j;
            }
            this.f157a.a(bArr, i, i2);
            this.c += (long) i2;
        }
    }
}
