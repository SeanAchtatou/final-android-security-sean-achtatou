package a.a.a.h.b;

import a.a.a.a.c;
import a.a.a.b.a;
import a.a.a.e.r;
import a.a.a.e.s;
import a.a.a.h.c.h;
import a.a.a.n;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class d implements a {

    /* renamed from: a  reason: collision with root package name */
    private final Log f98a;
    private final Map b;
    private final r c;

    public d() {
        this(null);
    }

    public d(r rVar) {
        this.f98a = LogFactory.getLog(getClass());
        this.b = new ConcurrentHashMap();
        this.c = rVar == null ? h.f122a : rVar;
    }

    public c a(n nVar) {
        a.a.a.n.a.a(nVar, "HTTP host");
        byte[] bArr = (byte[]) this.b.get(c(nVar));
        if (bArr == null) {
            return null;
        }
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bArr));
            c cVar = (c) objectInputStream.readObject();
            objectInputStream.close();
            return cVar;
        } catch (IOException e) {
            if (this.f98a.isWarnEnabled()) {
                this.f98a.warn("Unexpected I/O error while de-serializing auth scheme", e);
            }
            return null;
        } catch (ClassNotFoundException e2) {
            if (this.f98a.isWarnEnabled()) {
                this.f98a.warn("Unexpected error while de-serializing auth scheme", e2);
            }
            return null;
        }
    }

    public void a(n nVar, c cVar) {
        a.a.a.n.a.a(nVar, "HTTP host");
        if (cVar != null) {
            if (cVar instanceof Serializable) {
                try {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                    objectOutputStream.writeObject(cVar);
                    objectOutputStream.close();
                    this.b.put(c(nVar), byteArrayOutputStream.toByteArray());
                } catch (IOException e) {
                    if (this.f98a.isWarnEnabled()) {
                        this.f98a.warn("Unexpected I/O error while serializing auth scheme", e);
                    }
                }
            } else if (this.f98a.isDebugEnabled()) {
                this.f98a.debug("Auth scheme " + cVar.getClass() + " is not serializable");
            }
        }
    }

    public void b(n nVar) {
        a.a.a.n.a.a(nVar, "HTTP host");
        this.b.remove(c(nVar));
    }

    /* access modifiers changed from: protected */
    public n c(n nVar) {
        if (nVar.b() > 0) {
            return nVar;
        }
        try {
            return new n(nVar.a(), this.c.a(nVar), nVar.c());
        } catch (s e) {
            return nVar;
        }
    }

    public String toString() {
        return this.b.toString();
    }
}
