package a.a.a.h.a;

import java.security.MessageDigest;

class r {

    /* renamed from: a  reason: collision with root package name */
    protected byte[] f86a;
    protected byte[] b;
    protected MessageDigest c;

    r(byte[] bArr) {
        try {
            this.c = MessageDigest.getInstance("MD5");
            this.f86a = new byte[64];
            this.b = new byte[64];
            int length = bArr.length;
            if (length > 64) {
                this.c.update(bArr);
                bArr = this.c.digest();
                length = bArr.length;
            }
            int i = 0;
            while (i < length) {
                this.f86a[i] = (byte) (bArr[i] ^ 54);
                this.b[i] = (byte) (bArr[i] ^ 92);
                i++;
            }
            for (int i2 = i; i2 < 64; i2++) {
                this.f86a[i2] = 54;
                this.b[i2] = 92;
            }
            this.c.reset();
            this.c.update(this.f86a);
        } catch (Exception e) {
            throw new o("Error getting md5 message digest implementation: " + e.getMessage(), e);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(byte[] bArr) {
        this.c.update(bArr);
    }

    /* access modifiers changed from: package-private */
    public byte[] a() {
        byte[] digest = this.c.digest();
        this.c.update(this.b);
        return this.c.digest(digest);
    }
}
