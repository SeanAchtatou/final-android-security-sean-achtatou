package a.a.a.f;

import a.a.a.n.a;
import a.a.a.n.i;
import java.util.Locale;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private final String f49a;
    private final int b;
    private final String c;
    private final boolean d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public f(String str, int i, String str2, boolean z) {
        a.b(str, "Host");
        a.b(i, "Port");
        a.a((Object) str2, "Path");
        this.f49a = str.toLowerCase(Locale.ROOT);
        this.b = i;
        if (!i.b(str2)) {
            this.c = str2;
        } else {
            this.c = "/";
        }
        this.d = z;
    }

    public String a() {
        return this.f49a;
    }

    public String b() {
        return this.c;
    }

    public int c() {
        return this.b;
    }

    public boolean d() {
        return this.d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        if (this.d) {
            sb.append("(secure)");
        }
        sb.append(this.f49a);
        sb.append(':');
        sb.append(Integer.toString(this.b));
        sb.append(this.c);
        sb.append(']');
        return sb.toString();
    }
}
