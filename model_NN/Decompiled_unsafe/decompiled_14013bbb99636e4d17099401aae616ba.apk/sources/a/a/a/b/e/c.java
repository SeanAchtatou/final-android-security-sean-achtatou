package a.a.a.b.e;

import a.a.a.a.b;
import a.a.a.a.h;
import a.a.a.a.i;
import a.a.a.m.e;
import a.a.a.n;
import a.a.a.n.a;
import a.a.a.q;
import a.a.a.r;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class c implements r {

    /* renamed from: a  reason: collision with root package name */
    private final Log f18a = LogFactory.getLog(getClass());

    private void a(n nVar, a.a.a.a.c cVar, i iVar, a.a.a.b.i iVar2) {
        String a2 = cVar.a();
        if (this.f18a.isDebugEnabled()) {
            this.f18a.debug("Re-using cached '" + a2 + "' auth scheme for " + nVar);
        }
        a.a.a.a.n a3 = iVar2.a(new h(nVar, h.b, a2));
        if (a3 != null) {
            if ("BASIC".equalsIgnoreCase(cVar.a())) {
                iVar.a(b.CHALLENGED);
            } else {
                iVar.a(b.SUCCESS);
            }
            iVar.a(cVar, a3);
            return;
        }
        this.f18a.debug("No credentials for preemptive authentication");
    }

    public void a(q qVar, e eVar) {
        a.a.a.a.c a2;
        a.a.a.a.c a3;
        a.a(qVar, "HTTP request");
        a.a(eVar, "HTTP context");
        a a4 = a.a(eVar);
        a.a.a.b.a h = a4.h();
        if (h == null) {
            this.f18a.debug("Auth cache not set in the context");
            return;
        }
        a.a.a.b.i g = a4.g();
        if (g == null) {
            this.f18a.debug("Credentials provider not set in the context");
            return;
        }
        a.a.a.e.b.e a5 = a4.a();
        if (a5 == null) {
            this.f18a.debug("Route info not set in the context");
            return;
        }
        n o = a4.o();
        if (o == null) {
            this.f18a.debug("Target host not set in the context");
            return;
        }
        n nVar = o.b() < 0 ? new n(o.a(), a5.a().b(), o.c()) : o;
        i i = a4.i();
        if (!(i == null || i.b() != b.UNCHALLENGED || (a3 = h.a(nVar)) == null)) {
            a(nVar, a3, i, g);
        }
        n d = a5.d();
        i j = a4.j();
        if (d != null && j != null && j.b() == b.UNCHALLENGED && (a2 = h.a(d)) != null) {
            a(d, a2, j, g);
        }
    }
}
