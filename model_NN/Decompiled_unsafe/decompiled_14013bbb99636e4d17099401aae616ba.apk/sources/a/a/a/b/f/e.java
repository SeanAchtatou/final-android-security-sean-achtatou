package a.a.a.b.f;

import a.a.a.c;
import a.a.a.e.e.a;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private String f26a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private int g;
    private String h;
    private String i;
    private String j;
    private List k;
    private String l;
    private Charset m;
    private String n;
    private String o;

    public e() {
        this.g = -1;
    }

    public e(URI uri) {
        a(uri);
    }

    private List a(String str, Charset charset) {
        if (str == null || str.isEmpty()) {
            return null;
        }
        return g.a(str, charset);
    }

    private void a(URI uri) {
        this.f26a = uri.getScheme();
        this.b = uri.getRawSchemeSpecificPart();
        this.c = uri.getRawAuthority();
        this.f = uri.getHost();
        this.g = uri.getPort();
        this.e = uri.getRawUserInfo();
        this.d = uri.getUserInfo();
        this.i = uri.getRawPath();
        this.h = uri.getPath();
        this.j = uri.getRawQuery();
        this.k = a(uri.getRawQuery(), this.m != null ? this.m : c.f28a);
        this.o = uri.getRawFragment();
        this.n = uri.getFragment();
    }

    private String b(List list) {
        return g.a(list, this.m != null ? this.m : c.f28a);
    }

    private String f(String str) {
        return g.b(str, this.m != null ? this.m : c.f28a);
    }

    private String g() {
        StringBuilder sb = new StringBuilder();
        if (this.f26a != null) {
            sb.append(this.f26a).append(':');
        }
        if (this.b != null) {
            sb.append(this.b);
        } else {
            if (this.c != null) {
                sb.append("//").append(this.c);
            } else if (this.f != null) {
                sb.append("//");
                if (this.e != null) {
                    sb.append(this.e).append("@");
                } else if (this.d != null) {
                    sb.append(f(this.d)).append("@");
                }
                if (a.d(this.f)) {
                    sb.append("[").append(this.f).append("]");
                } else {
                    sb.append(this.f);
                }
                if (this.g >= 0) {
                    sb.append(":").append(this.g);
                }
            }
            if (this.i != null) {
                sb.append(i(this.i));
            } else if (this.h != null) {
                sb.append(g(i(this.h)));
            }
            if (this.j != null) {
                sb.append("?").append(this.j);
            } else if (this.k != null) {
                sb.append("?").append(b(this.k));
            } else if (this.l != null) {
                sb.append("?").append(h(this.l));
            }
        }
        if (this.o != null) {
            sb.append("#").append(this.o);
        } else if (this.n != null) {
            sb.append("#").append(h(this.n));
        }
        return sb.toString();
    }

    private String g(String str) {
        return g.d(str, this.m != null ? this.m : c.f28a);
    }

    private String h(String str) {
        return g.c(str, this.m != null ? this.m : c.f28a);
    }

    private static String i(String str) {
        if (str == null) {
            return null;
        }
        int i2 = 0;
        while (i2 < str.length() && str.charAt(i2) == '/') {
            i2++;
        }
        return i2 > 1 ? str.substring(i2 - 1) : str;
    }

    public e a(int i2) {
        if (i2 < 0) {
            i2 = -1;
        }
        this.g = i2;
        this.b = null;
        this.c = null;
        return this;
    }

    public e a(String str) {
        this.f26a = str;
        return this;
    }

    public e a(Charset charset) {
        this.m = charset;
        return this;
    }

    public e a(List list) {
        if (this.k == null) {
            this.k = new ArrayList();
        }
        this.k.addAll(list);
        this.j = null;
        this.b = null;
        this.l = null;
        return this;
    }

    public URI a() {
        return new URI(g());
    }

    public e b() {
        this.k = null;
        this.j = null;
        this.b = null;
        return this;
    }

    public e b(String str) {
        this.d = str;
        this.b = null;
        this.c = null;
        this.e = null;
        return this;
    }

    public e c(String str) {
        this.f = str;
        this.b = null;
        this.c = null;
        return this;
    }

    public String c() {
        return this.d;
    }

    public e d(String str) {
        this.h = str;
        this.b = null;
        this.i = null;
        return this;
    }

    public String d() {
        return this.f;
    }

    public e e(String str) {
        this.n = str;
        this.o = null;
        return this;
    }

    public String e() {
        return this.h;
    }

    public List f() {
        return this.k != null ? new ArrayList(this.k) : new ArrayList();
    }

    public String toString() {
        return g();
    }
}
