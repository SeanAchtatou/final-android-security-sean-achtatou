package a.a.a.b.f;

import a.a.a.n;
import a.a.a.n.a;
import a.a.a.n.i;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Locale;
import java.util.Stack;

public class f {
    public static URI a(URI uri) {
        a.a(uri, "URI");
        if (uri.isOpaque()) {
            return uri;
        }
        e eVar = new e(uri);
        if (eVar.c() != null) {
            eVar.b((String) null);
        }
        if (i.a(eVar.e())) {
            eVar.d("/");
        }
        if (eVar.d() != null) {
            eVar.c(eVar.d().toLowerCase(Locale.ROOT));
        }
        eVar.e(null);
        return eVar.a();
    }

    public static URI a(URI uri, n nVar, boolean z) {
        a.a(uri, "URI");
        if (uri.isOpaque()) {
            return uri;
        }
        e eVar = new e(uri);
        if (nVar != null) {
            eVar.a(nVar.c());
            eVar.c(nVar.a());
            eVar.a(nVar.b());
        } else {
            eVar.a((String) null);
            eVar.c(null);
            eVar.a(-1);
        }
        if (z) {
            eVar.e(null);
        }
        if (i.a(eVar.e())) {
            eVar.d("/");
        }
        return eVar.a();
    }

    public static URI a(URI uri, URI uri2) {
        a.a(uri, "Base URI");
        a.a(uri2, "Reference URI");
        String uri3 = uri2.toString();
        if (uri3.startsWith("?")) {
            return b(uri, uri2);
        }
        boolean isEmpty = uri3.isEmpty();
        if (isEmpty) {
            uri2 = URI.create("#");
        }
        URI resolve = uri.resolve(uri2);
        if (isEmpty) {
            String uri4 = resolve.toString();
            resolve = URI.create(uri4.substring(0, uri4.indexOf(35)));
        }
        return c(resolve);
    }

    public static n b(URI uri) {
        n nVar;
        int indexOf;
        int i;
        if (uri == null) {
            return null;
        }
        if (uri.isAbsolute()) {
            int port = uri.getPort();
            String host = uri.getHost();
            if (host == null && (host = uri.getAuthority()) != null) {
                int indexOf2 = host.indexOf(64);
                String substring = indexOf2 >= 0 ? host.length() > indexOf2 + 1 ? host.substring(indexOf2 + 1) : null : host;
                if (substring == null || (indexOf = substring.indexOf(58)) < 0) {
                    host = substring;
                } else {
                    int i2 = indexOf + 1;
                    int i3 = i2;
                    int i4 = 0;
                    while (i3 < substring.length() && Character.isDigit(substring.charAt(i3))) {
                        i4++;
                        i3++;
                    }
                    if (i4 > 0) {
                        try {
                            i = Integer.parseInt(substring.substring(i2, i2 + i4));
                        } catch (NumberFormatException e) {
                            i = port;
                        }
                    } else {
                        i = port;
                    }
                    port = i;
                    host = substring.substring(0, indexOf);
                }
            }
            String scheme = uri.getScheme();
            if (!i.b(host)) {
                try {
                    nVar = new n(host, port, scheme);
                } catch (IllegalArgumentException e2) {
                    nVar = null;
                }
                return nVar;
            }
        }
        nVar = null;
        return nVar;
    }

    private static URI b(URI uri, URI uri2) {
        String uri3 = uri.toString();
        if (uri3.indexOf(63) > -1) {
            uri3 = uri3.substring(0, uri3.indexOf(63));
        }
        return URI.create(uri3 + uri2.toString());
    }

    private static URI c(URI uri) {
        if (uri.isOpaque() || uri.getAuthority() == null) {
            return uri;
        }
        a.a(uri.isAbsolute(), "Base URI must be absolute");
        String path = uri.getPath() == null ? "" : uri.getPath();
        String[] split = path.split("/");
        Stack stack = new Stack();
        for (String str : split) {
            if (!str.isEmpty() && !".".equals(str)) {
                if (!"..".equals(str)) {
                    stack.push(str);
                } else if (!stack.isEmpty()) {
                    stack.pop();
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = stack.iterator();
        while (it.hasNext()) {
            sb.append('/').append((String) it.next());
        }
        if (path.lastIndexOf(47) == path.length() - 1) {
            sb.append('/');
        }
        try {
            URI uri2 = new URI(uri.getScheme().toLowerCase(Locale.ROOT), uri.getAuthority().toLowerCase(Locale.ROOT), sb.toString(), null, null);
            if (uri.getQuery() == null && uri.getFragment() == null) {
                return uri2;
            }
            StringBuilder sb2 = new StringBuilder(uri2.toASCIIString());
            if (uri.getQuery() != null) {
                sb2.append('?').append(uri.getRawQuery());
            }
            if (uri.getFragment() != null) {
                sb2.append('#').append(uri.getRawFragment());
            }
            return URI.create(sb2.toString());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
