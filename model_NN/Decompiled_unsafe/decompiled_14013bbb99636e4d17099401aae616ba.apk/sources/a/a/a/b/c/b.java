package a.a.a.b.c;

import a.a.a.e.e;
import a.a.a.e.i;
import a.a.a.j.a;
import a.a.a.q;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public abstract class b extends a implements a, q, Cloneable {
    private final AtomicBoolean c = new AtomicBoolean(false);
    private final AtomicReference d = new AtomicReference(null);

    protected b() {
    }

    public void a(a.a.a.c.a aVar) {
        if (!this.c.get()) {
            this.d.set(aVar);
        }
    }

    @Deprecated
    public void a(e eVar) {
        a(new c(this, eVar));
    }

    @Deprecated
    public void a(i iVar) {
        a(new d(this, iVar));
    }

    public Object clone() {
        b bVar = (b) super.clone();
        bVar.f163a = (a.a.a.j.q) a.a.a.b.f.a.a(this.f163a);
        bVar.b = (a.a.a.k.e) a.a.a.b.f.a.a(this.b);
        return bVar;
    }

    public boolean h() {
        return this.c.get();
    }
}
