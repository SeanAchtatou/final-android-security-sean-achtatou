package a.a.a.b.f;

import a.a.a.n.a;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public static final TimeZone f24a = TimeZone.getTimeZone("GMT");
    private static final String[] b = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy"};
    private static final Date c;

    static {
        Calendar instance = Calendar.getInstance();
        instance.setTimeZone(f24a);
        instance.set(2000, 0, 1, 0, 0, 0);
        instance.set(14, 0);
        c = instance.getTime();
    }

    public static Date a(String str, String[] strArr) {
        return a(str, strArr, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public static Date a(String str, String[] strArr, Date date) {
        a.a((Object) str, "Date value");
        if (strArr == null) {
            strArr = b;
        }
        if (date == null) {
            date = c;
        }
        if (str.length() > 1 && str.startsWith("'") && str.endsWith("'")) {
            str = str.substring(1, str.length() - 1);
        }
        for (String a2 : strArr) {
            SimpleDateFormat a3 = c.a(a2);
            a3.set2DigitYearStart(date);
            ParsePosition parsePosition = new ParsePosition(0);
            Date parse = a3.parse(str, parsePosition);
            if (parsePosition.getIndex() != 0) {
                return parse;
            }
        }
        return null;
    }
}
