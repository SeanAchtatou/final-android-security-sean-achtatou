package com.mj.iMatch;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.view.Display;

public class CommonUtils {
    public static int BallX = 0;
    public static int BallY = 40;
    public static int ScreenHeight = 480;
    public static int ScreenWidth = 320;

    public static void setDisplay(Display display) {
        ScreenWidth = display.getWidth();
        ScreenHeight = display.getHeight();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap changeBitmap(Bitmap bitmap) {
        int oldWidth = bitmap.getWidth();
        int oldHeight = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) ScreenWidth) / ((float) oldWidth), ((float) ScreenHeight) / ((float) oldHeight));
        return Bitmap.createBitmap(bitmap, 0, 0, oldWidth, oldHeight, matrix, true);
    }
}
