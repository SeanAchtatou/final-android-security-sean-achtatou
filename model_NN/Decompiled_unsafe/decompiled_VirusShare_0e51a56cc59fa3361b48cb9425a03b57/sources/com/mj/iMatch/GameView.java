package com.mj.iMatch;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.lang.reflect.Array;
import java.util.Random;

public class GameView extends View implements Runnable {
    static final int ST_PLAYING = 2;
    static int gameState = 2;
    int BODY_H = 9;
    int BODY_W = 8;
    int beginDrawX;
    int beginDrawY;
    private Bitmap[] block = new Bitmap[8];
    final int blockCount = 8;
    int[][] body = null;
    Container bomb;
    final int caseWidth = 40;
    int[][] clear = null;
    int clearFrame;
    final int clearFrameMax = 8;
    final int clearW = 30;
    int currentX = -1;
    int currentY = -1;
    private Bitmap cursor1;
    private Bitmap cursor2;
    final int cursorW = 36;
    private int delay = 50;
    int gameScore;
    int iDisScore;
    IMatch iMatch;
    boolean isClear;
    boolean isDown = true;
    boolean isExchange;
    boolean isReExchange;
    boolean isSelected;
    int moveFrame;
    final int moveFrameMax = 8;
    final int moveSpeed = 5;
    Paint paint = new Paint();
    Random random = new Random();
    int scoreSpace;
    int selectedX;
    int selectedY;
    int[][] tempMove = null;

    public GameView(IMatch con) {
        super(con);
        this.iMatch = con;
        setFocusable(true);
        init();
        this.paint.setColor(-7829368);
        this.paint.setTextSize(22.0f);
        this.beginDrawX = 0;
        this.beginDrawY = 20;
        loadFP();
        this.bomb = new Container(this.block, CommonUtils.ScreenWidth, CommonUtils.ScreenHeight);
    }

    private void init() {
        if (CommonUtils.ScreenHeight > 480) {
            this.BODY_H = 10;
        }
        this.body = (int[][]) Array.newInstance(Integer.TYPE, this.BODY_W, this.BODY_H);
        this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, this.BODY_W, this.BODY_H);
        this.clear = (int[][]) Array.newInstance(Integer.TYPE, this.BODY_W, this.BODY_H);
    }

    public Bitmap createImage(Drawable tile, int w, int h) {
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        tile.setBounds(0, 0, w, h);
        tile.draw(canvas);
        return bitmap;
    }

    public void loadFP() {
        Resources r = getResources();
        this.cursor1 = BitmapFactory.decodeResource(r, R.drawable.sel1);
        this.cursor2 = BitmapFactory.decodeResource(r, R.drawable.sel2);
        this.scoreSpace = 320;
        for (int index = 0; index < 8; index++) {
            ((BitmapDrawable) r.getDrawable(R.drawable.baoshi1 + index)).getBitmap();
            this.block[index] = changeBitmap(BitmapFactory.decodeResource(r, R.drawable.baoshi1 + index));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Bitmap changeBitmap(Bitmap bitmap) {
        int oldWidth = bitmap.getWidth();
        int oldHeight = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(40.0f / ((float) oldWidth), 40.0f / ((float) oldHeight));
        return Bitmap.createBitmap(bitmap, 0, 0, oldWidth, oldHeight, matrix, true);
    }

    public void toState(int state) {
        gameState = state;
    }

    public void logic() {
        this.bomb.move();
        if (this.iDisScore < this.gameScore) {
            this.iDisScore += ((this.gameScore - this.iDisScore) / 2) + 1;
        }
        if (this.isClear) {
            this.clearFrame++;
            if (this.clearFrame >= 8) {
                this.clearFrame = 0;
                this.isClear = false;
                this.isDown = doDown();
            }
        }
        if (this.isDown || this.isExchange || this.isReExchange) {
            this.moveFrame++;
        }
        if (this.isDown && this.moveFrame >= 8) {
            this.moveFrame = 0;
            this.isDown = doDown();
            if (!this.isDown) {
                this.isClear = checkClear();
            }
        }
        if (this.isExchange && this.moveFrame >= 8) {
            this.moveFrame = 0;
            this.isExchange = false;
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, this.BODY_W, this.BODY_H);
            this.isClear = checkClear();
            if (!this.isClear) {
                this.isReExchange = true;
                doExchange();
            }
        }
        if (this.isReExchange && this.moveFrame >= 8) {
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, this.BODY_W, this.BODY_H);
            this.moveFrame = 0;
            this.isReExchange = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        switch (gameState) {
            case 2:
                paintPlaying(canvas);
                return;
            default:
                return;
        }
    }

    private void paintPlaying(Canvas canvas) {
        for (int i = 0; i < this.tempMove.length; i++) {
            for (int j = 0; j < this.tempMove[i].length; j++) {
                switch (this.tempMove[i][j]) {
                    case R.styleable.com_admob_android_ads_AdView_backgroundColor /*1*/:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case 2:
                        paintBlock(canvas, this.body[i][j], i * 40, ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*3*/:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case R.styleable.com_admob_android_ads_AdView_keywords /*4*/:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), j * 40);
                        break;
                    case R.styleable.com_admob_android_ads_AdView_refreshInterval /*5*/:
                    default:
                        if (this.clear[i][j] != 0) {
                            break;
                        } else {
                            paintBlock(canvas, this.body[i][j], i * 40, j * 40);
                            break;
                        }
                    case 6:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), j * 40);
                        break;
                    case 7:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                    case 8:
                        paintBlock(canvas, this.body[i][j], i * 40, ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                    case 9:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                }
            }
        }
        paintNumber(this.iDisScore);
        canvas.drawBitmap(this.isSelected ? this.cursor1 : this.cursor2, (float) (this.beginDrawX + (this.currentX * 40)), (float) (this.beginDrawY + (this.currentY * 40)), this.paint);
        this.bomb.paint(canvas, this.paint);
    }

    private boolean doDown() {
        boolean isFull = false;
        for (int i = 0; i < this.body.length; i++) {
            int j = this.body[i].length - 1;
            while (true) {
                if (j < 0) {
                    break;
                }
                this.tempMove[i][j] = 0;
                if (this.body[i][j] == 0) {
                    isFull = true;
                    for (int k = j; k >= 0; k--) {
                        this.tempMove[i][k] = 2;
                        if (k == 0) {
                            this.body[i][k] = Math.abs(this.random.nextInt() % 8) + 1;
                        } else {
                            this.body[i][k] = this.body[i][k - 1];
                        }
                    }
                } else {
                    j--;
                }
            }
        }
        return isFull;
    }

    private void doExchange() {
        if (this.currentX - this.selectedX == -1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 9;
                this.tempMove[this.selectedX][this.selectedY] = 1;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 6;
                this.tempMove[this.selectedX][this.selectedY] = 4;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 3;
                this.tempMove[this.selectedX][this.selectedY] = 7;
            }
        } else if (this.currentX - this.selectedX == 0) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 8;
                this.tempMove[this.selectedX][this.selectedY] = 2;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 2;
                this.tempMove[this.selectedX][this.selectedY] = 8;
            }
        } else if (this.currentX - this.selectedX == 1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 7;
                this.tempMove[this.selectedX][this.selectedY] = 3;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 4;
                this.tempMove[this.selectedX][this.selectedY] = 6;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 1;
                this.tempMove[this.selectedX][this.selectedY] = 9;
            }
        }
        int temp = this.body[this.selectedX][this.selectedY];
        this.body[this.selectedX][this.selectedY] = this.body[this.currentX][this.currentY];
        this.body[this.currentX][this.currentY] = temp;
    }

    private boolean checkClear() {
        for (int i = 0; i < this.body.length; i++) {
            for (int j = 0; j < this.body[i].length; j++) {
                if (j > 0 && j < this.body[i].length - 1 && this.body[i][j] == this.body[i][j - 1] && this.body[i][j] == this.body[i][j + 1]) {
                    int[] iArr = this.clear[i];
                    this.clear[i][j + 1] = 1;
                    this.clear[i][j - 1] = 1;
                    iArr[j] = 1;
                }
                if (i > 0 && i < this.body.length - 1 && this.body[i][j] == this.body[i - 1][j] && this.body[i][j] == this.body[i + 1][j]) {
                    int[] iArr2 = this.clear[i];
                    int[] iArr3 = this.clear[i - 1];
                    this.clear[i + 1][j] = 1;
                    iArr3[j] = 1;
                    iArr2[j] = 1;
                }
            }
        }
        boolean clearBlock = false;
        for (int i2 = 0; i2 < this.BODY_W; i2++) {
            for (int j2 = 0; j2 < this.BODY_H; j2++) {
                if (this.clear[i2][j2] == 1) {
                    clearBlock = true;
                    this.bomb.add(this.beginDrawX + (i2 * 40), this.beginDrawY + (j2 * 40), this.body[i2][j2] - 1);
                    int[] iArr4 = this.clear[i2];
                    this.body[i2][j2] = 0;
                    iArr4[j2] = 0;
                    this.gameScore += 10;
                }
            }
        }
        return clearBlock;
    }

    private void paintBlock(Canvas canvas, int index, int x, int y) {
        int index2 = index - 1;
        if (index2 >= 0 && index2 < this.block.length) {
            canvas.save();
            canvas.clipRect(this.beginDrawX, this.beginDrawY, this.beginDrawX + (this.BODY_W * 40), this.beginDrawY + (this.BODY_H * 40));
            canvas.drawBitmap(this.block[index2], (float) (this.beginDrawX + x), (float) (this.beginDrawY + y), this.paint);
            canvas.restore();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (!this.isDown && !this.isExchange && !this.isReExchange && this.moveFrame == 0) {
                    if (event.getX() > ((float) this.beginDrawX) && event.getX() < ((float) (this.beginDrawX + (this.BODY_W * 40))) && event.getY() > ((float) this.beginDrawY) && event.getY() < ((float) (this.beginDrawY + (this.BODY_H * 40)))) {
                        if (!this.isSelected) {
                            this.isSelected = true;
                            this.currentX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            this.currentY = (int) ((event.getY() - ((float) this.beginDrawY)) / 40.0f);
                            this.selectedX = this.currentX;
                            this.selectedY = this.currentY;
                            break;
                        } else {
                            int tempX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            int tempY = (int) ((event.getY() - ((float) this.beginDrawY)) / 40.0f);
                            if (tempX <= this.selectedX) {
                                if (tempX != this.selectedX) {
                                    if (tempX < this.selectedX) {
                                        if (tempY <= this.selectedY) {
                                            if (tempY != this.selectedY) {
                                                if (tempY < this.selectedY) {
                                                    moveLeftUp();
                                                    break;
                                                }
                                            } else {
                                                moveLeft();
                                                break;
                                            }
                                        } else {
                                            moveLeftDown();
                                            break;
                                        }
                                    }
                                } else if (tempY <= this.selectedY) {
                                    if (tempY != this.selectedY) {
                                        if (tempY < this.selectedY) {
                                            moveUp();
                                            break;
                                        }
                                    } else {
                                        this.isSelected = false;
                                        break;
                                    }
                                } else {
                                    moveDown();
                                    break;
                                }
                            } else if (tempY <= this.selectedY) {
                                if (tempY != this.selectedY) {
                                    if (tempY < this.selectedY) {
                                        moveRightUp();
                                        break;
                                    }
                                } else {
                                    moveRight();
                                    break;
                                }
                            } else {
                                moveRightDown();
                                break;
                            }
                        }
                    }
                } else {
                    return super.onTouchEvent(event);
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (this.isDown || this.isExchange || this.isReExchange || this.moveFrame != 0) {
            return super.onKeyDown(keyCode, msg);
        }
        if (keyCode == 23) {
            if (this.isDown || this.isExchange || this.isReExchange) {
                return super.onKeyDown(keyCode, msg);
            }
            this.isSelected = !this.isSelected;
            this.selectedX = this.currentX;
            this.selectedY = this.currentY;
        } else if (this.isExchange || this.isReExchange) {
            return super.onKeyDown(keyCode, msg);
        } else {
            if (keyCode == 19) {
                moveUp();
            } else if (keyCode == 20) {
                moveDown();
            } else if (keyCode == 21) {
                moveLeft();
            } else if (keyCode == 22) {
                moveRight();
            }
        }
        return super.onKeyDown(keyCode, msg);
    }

    public void setExchange() {
        if (this.isSelected) {
            this.isExchange = true;
            this.isSelected = false;
            doExchange();
        }
    }

    private void moveRightDown() {
        if (this.currentX == this.BODY_W - 1 || this.currentY == this.BODY_H - 1) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % this.BODY_W;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % this.BODY_H;
        setExchange();
    }

    private void moveLeftDown() {
        if (this.currentX == 0 || this.currentY == this.BODY_H - 1) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + this.BODY_W) % this.BODY_W;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % this.BODY_H;
        setExchange();
    }

    private void moveRightUp() {
        if (this.currentY == 0 || this.currentX == this.BODY_W - 1) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % this.BODY_W;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + this.BODY_H) % this.BODY_H;
        setExchange();
    }

    private void moveLeftUp() {
        if (this.currentX == 0 || this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + this.BODY_W) % this.BODY_W;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + this.BODY_H) % this.BODY_H;
        setExchange();
    }

    private void moveUp() {
        if (this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentY - 1;
        this.currentY = i;
        this.currentY = (i + this.BODY_H) % this.BODY_H;
        setExchange();
    }

    private void moveDown() {
        if (this.currentY == this.BODY_H - 1) {
            this.isSelected = false;
        }
        int i = this.currentY + 1;
        this.currentY = i;
        this.currentY = i % this.BODY_H;
        setExchange();
    }

    private void moveLeft() {
        if (this.currentX == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + this.BODY_W) % this.BODY_W;
        setExchange();
    }

    private void moveRight() {
        if (this.currentX == this.BODY_W - 1) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % this.BODY_W;
        setExchange();
    }

    private void paintNumber(int num) {
        this.iMatch.iScoreTxt.setText("Score: " + num);
    }

    public void run() {
        logic();
        invalidate();
        postDelayed(this, (long) this.delay);
    }
}
