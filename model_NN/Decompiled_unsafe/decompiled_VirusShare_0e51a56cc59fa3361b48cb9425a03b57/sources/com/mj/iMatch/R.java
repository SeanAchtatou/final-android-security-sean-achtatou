package com.mj.iMatch;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771969;
        public static final int keywords = 2130771972;
        public static final int primaryTextColor = 2130771970;
        public static final int refreshInterval = 2130771973;
        public static final int secondaryTextColor = 2130771971;
        public static final int testing = 2130771968;
    }

    public static final class drawable {
        public static final int baoshi1 = 2130837504;
        public static final int baoshi2 = 2130837505;
        public static final int baoshi3 = 2130837506;
        public static final int baoshi4 = 2130837507;
        public static final int baoshi5 = 2130837508;
        public static final int baoshi6 = 2130837509;
        public static final int baoshi7 = 2130837510;
        public static final int baoshi8 = 2130837511;
        public static final int bg = 2130837512;
        public static final int icon = 2130837513;
        public static final int sel1 = 2130837514;
        public static final int sel2 = 2130837515;
    }

    public static final class id {
        public static final int adview = 2131034114;
        public static final int main = 2131034112;
        public static final int score_id = 2131034113;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int msg = 2130968577;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.testing, R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 1;
        public static final int com_admob_android_ads_AdView_keywords = 4;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 2;
        public static final int com_admob_android_ads_AdView_refreshInterval = 5;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 3;
        public static final int com_admob_android_ads_AdView_testing = 0;
    }
}
