package noogei.ohpah.aegaeg;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;

public class EeHiaep extends Activity {
    private EeoW0pY8w2 AWTIdg;
    private zkDiM0ycmt HURCe;
    private int JOqYb5;
    ProgressDialog owYTDp3Ug4;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.JOqYb5 = getResources().getInteger(R.integer.dialog_id);
        zkDiM0ycmt.owYTDp3Ug4(this);
        this.HURCe = zkDiM0ycmt.owYTDp3Ug4();
        setContentView(this.HURCe.JOqYb5());
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        if (i != this.JOqYb5) {
            return null;
        }
        this.owYTDp3Ug4 = new ProgressDialog(this);
        this.owYTDp3Ug4.setProgressStyle(1);
        this.owYTDp3Ug4.setMessage(getString(R.string.loading));
        return this.owYTDp3Ug4;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.HURCe.HURCe();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i, Dialog dialog) {
        if (i == this.JOqYb5) {
            this.owYTDp3Ug4.setProgress(0);
            this.AWTIdg = new EeoW0pY8w2(this);
            this.AWTIdg.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    public void selfDestruct(View view) {
        showDialog(this.JOqYb5);
    }

    public void selfInfo(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.info).setCancelable(false).setNeutralButton((int) R.string.ok, new LEqoxK(this));
        builder.create().show();
    }
}
