package com.baidu.BaiduMap.channel;

import android.content.Context;
import com.baidu.BaiduMap.PayManager;
import com.baidu.BaiduMap.json.ChannelEntity;
import com.baidu.BaiduMap.json.JsonEntity;
import java.util.Iterator;
import java.util.Vector;

public abstract class BasePayChannel implements IPayChannel {
    protected static int price;
    protected Context appContext;
    protected ChannelEntity channel;
    protected String extData;
    private JsonEntity.RequestProperties orderInfo;
    private Vector<IPayChannelListener> payChannelListenerList = new Vector<>();
    protected String payCode;
    protected String productName;
    protected String secondTipInfo;
    protected String throughId;

    public void addPayChannelListener(IPayChannelListener newPayChannelListener) {
        if (!this.payChannelListenerList.contains(newPayChannelListener)) {
            this.payChannelListenerList.add(newPayChannelListener);
        }
    }

    public void removePayChannelListener(IPayChannelListener newPayChannelListener) {
        if (this.payChannelListenerList.contains(newPayChannelListener)) {
            this.payChannelListenerList.remove(newPayChannelListener);
        }
    }

    public void pay() {
    }

    /* access modifiers changed from: protected */
    public void postPaySucceededEvent() {
        Iterator<IPayChannelListener> it = this.payChannelListenerList.iterator();
        while (it.hasNext()) {
            it.next().onPaySucceeded();
        }
    }

    /* access modifiers changed from: protected */
    public void postPayFailedEvent() {
        Iterator<IPayChannelListener> it = this.payChannelListenerList.iterator();
        while (it.hasNext()) {
            it.next().onPayFailed();
        }
    }

    /* access modifiers changed from: protected */
    public void postPayCanceledEvent() {
        Iterator<IPayChannelListener> it = this.payChannelListenerList.iterator();
        while (it.hasNext()) {
            it.next().onPayCanceled();
        }
    }

    public Context getAppContext() {
        return this.appContext;
    }

    public void setAppContext(Context appContext2) {
        this.appContext = appContext2;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price2) {
        price = price2;
    }

    public String getSecondTipInfo() {
        return this.secondTipInfo;
    }

    public void setSecondTipInfo(String secondTipInfo2) {
        this.secondTipInfo = secondTipInfo2;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName2) {
        this.productName = productName2;
    }

    public String getExtData() {
        return this.extData;
    }

    public void setExtData(String extData2) {
        this.extData = extData2;
    }

    public void setPayCode(String payCode2) {
        this.payCode = payCode2;
    }

    public String getPayCode() {
        return this.payCode;
    }

    /* access modifiers changed from: protected */
    public void saveOrderInfo(int payResult, String cid, String throughId2, String customized_price) {
        new JsonEntity.RequestProperties(this.appContext);
        JsonEntity.RequestProperties order = this.orderInfo;
        order.status = payResult;
        order.cid = cid;
        order.throughId = throughId2;
        if (!customized_price.equals("")) {
            order.customized_price = customized_price;
        } else {
            order.customized_price = new StringBuilder(String.valueOf(price)).toString();
        }
        PayManager.getInstance().saveOrder(this.appContext, order);
    }

    public JsonEntity.RequestProperties getOrderInfo() {
        return this.orderInfo;
    }

    public void setOrderInfo(JsonEntity.RequestProperties orderInfo2) {
        this.orderInfo = orderInfo2;
    }

    public String getThroughId() {
        return this.throughId;
    }

    public void setThroughId(String throughId2) {
        this.throughId = throughId2;
    }

    public ChannelEntity getChannel() {
        return this.channel;
    }

    public void setChannel(ChannelEntity channel2) {
        this.channel = channel2;
    }
}
