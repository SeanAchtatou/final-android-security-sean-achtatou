package com.baidu.BaiduMap.util;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.PackageManager;
import java.lang.reflect.InvocationTargetException;

public final class AppOpsUtil {
    public static boolean isOpsEnabled(Context context, int ops) {
        Object opRes = checkOp(context, ops, getUid(context));
        if (!(opRes instanceof Integer) || ((Integer) opRes).intValue() != 0) {
            return false;
        }
        return true;
    }

    public static boolean setOpsEnabled(Context context, int ops, boolean enabled) {
        int mode;
        int uid = getUid(context);
        if (enabled) {
            mode = 0;
        } else {
            mode = 1;
        }
        return setMode(context, ops, uid, mode);
    }

    private static Object checkOp(Context context, int code, int uid) {
        AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService("appops");
        Class appOpsManagerClass = appOpsManager.getClass();
        try {
            return appOpsManagerClass.getMethod("checkOp", Integer.TYPE, Integer.TYPE, String.class).invoke(appOpsManager, Integer.valueOf(code), Integer.valueOf(uid), context.getPackageName());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        }
        return null;
    }

    private static boolean setMode(Context context, int code, int uid, int mode) {
        AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService("appops");
        Class appOpsManagerClass = appOpsManager.getClass();
        try {
            appOpsManagerClass.getMethod("setMode", Integer.TYPE, Integer.TYPE, String.class, Integer.TYPE).invoke(appOpsManager, Integer.valueOf(code), Integer.valueOf(uid), context.getPackageName(), Integer.valueOf(mode));
            return true;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        }
        return false;
    }

    private static int getUid(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 4).uid;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
