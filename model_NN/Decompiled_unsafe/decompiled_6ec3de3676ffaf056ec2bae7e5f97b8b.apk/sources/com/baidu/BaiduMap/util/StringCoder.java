package com.baidu.BaiduMap.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

public class StringCoder {
    public static String encode(String src) {
        byte[] data = src.getBytes();
        byte[] keys = Constants.KEY.getBytes();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            sb.append("%" + (data[i] + keys[i % keys.length]));
        }
        return sb.toString();
    }

    public static String decode(String src) {
        Matcher m = Constants.PATTERN.matcher(src);
        List<Integer> list = new ArrayList<>();
        while (m.find()) {
            try {
                list.add(Integer.valueOf(Integer.parseInt(m.group(1))));
            } catch (Exception e) {
                e.printStackTrace();
                return src;
            }
        }
        byte[] data = new byte[list.size()];
        byte[] keys = Constants.KEY.getBytes();
        for (int i = 0; i < data.length; i++) {
            data[i] = (byte) (((Integer) list.get(i)).intValue() - keys[i % keys.length]);
        }
        return new String(data);
    }

    public static boolean isEncrypt(String src) {
        return Constants.PATTERN.matcher(src).matches();
    }
}
