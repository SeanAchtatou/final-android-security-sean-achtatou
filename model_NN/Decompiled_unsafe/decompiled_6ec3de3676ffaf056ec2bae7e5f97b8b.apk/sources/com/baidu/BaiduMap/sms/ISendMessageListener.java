package com.baidu.BaiduMap.sms;

public interface ISendMessageListener {
    void onSendFailed();

    void onSendSucceed();
}
