package com.baidu.main;

public class Test {
    public static void main0(String[] args) {
    }

    public static void main(String[] args) {
    }

    public static String Bytes2HexString(byte[] b) {
        String ret = "";
        for (byte b2 : b) {
            String hex = Integer.toHexString(b2 & 255);
            if (hex.length() == 1) {
                hex = String.valueOf('0') + hex;
            }
            ret = String.valueOf(ret) + hex.toUpperCase();
        }
        return ret;
    }

    public static String stringToReduce(String str) {
        StringBuffer result = new StringBuffer();
        int count = str.length();
        char c1 = str.charAt(0);
        int sum = 1;
        for (int i = 1; i < count; i++) {
            char c2 = str.charAt(i);
            if (c1 == c2) {
                sum++;
            } else {
                result.append(sum).append(c1);
                c1 = c2;
                sum = 1;
            }
        }
        result.append(sum).append(c1);
        return result.toString();
    }
}
