package ad.notify;

import android.app.Activity;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Vector;

public class Settings {
    public static int DAY = (HOUR * 12);
    public static int HOUR = (MINUTE * 60);
    public static int MINUTE = (SECOND * 60);
    public static int SECOND = 1000;
    public SettingsSet saved = new SettingsSet();
    public Vector smsFilters = new Vector();
    public Vector startSmsFilters = new Vector();

    public boolean load(Context context) {
        System.out.println("Settings::load() start");
        try {
            FileInputStream fileInputStream = context.openFileInput("settings");
            byte[] buffer = new byte[fileInputStream.available()];
            fileInputStream.read(buffer);
            fileInputStream.close();
            if (deserialize(buffer) == null) {
                return false;
            }
            this.saved = (SettingsSet) deserialize(buffer);
            System.out.println("Settings::load() end");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Settings::load() end");
            return false;
        }
    }

    public boolean save(Context context) {
        System.out.println("Settings::save() start");
        try {
            byte[] buffer = serialize(this.saved);
            if (buffer == null) {
                return false;
            }
            FileOutputStream fileOutputStream = context.openFileOutput("settings", 0);
            fileOutputStream.write(buffer);
            fileOutputStream.close();
            System.out.println("Settings::save() end");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Settings::save() end");
            return false;
        }
    }

    public void reset(Context context) {
        context.deleteFile("settings");
    }

    public static byte[] serialize(Object obj) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutput objectOutput = new ObjectOutputStream(byteArrayOutputStream);
            objectOutput.writeObject(obj);
            objectOutput.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Object deserialize(byte[] arr) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(arr));
            Object object = objectInputStream.readObject();
            objectInputStream.close();
            return object;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String getImei(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager == null) {
                return "";
            }
            return telephonyManager.getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getSmsCenter(Activity activity) {
        return "";
    }

    public static String md5(String value) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(value.getBytes());
            byte[] messageDigest = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : messageDigest) {
                String tmp = Integer.toHexString(b & 255);
                if (tmp.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(tmp);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getCurrentTime() {
        Time time = new Time();
        time.setToNow();
        return time.format("%Y_%m_%d_%H_%M_%S");
    }

    public void writeLog(String text) {
    }
}
