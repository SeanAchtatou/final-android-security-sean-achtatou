package android.support.design.widget;

import android.view.animation.Interpolator;

class ValueAnimatorCompat {
    private final Impl mImpl;

    interface AnimatorListener {
        void onAnimationCancel(ValueAnimatorCompat valueAnimatorCompat);

        void onAnimationEnd(ValueAnimatorCompat valueAnimatorCompat);

        void onAnimationStart(ValueAnimatorCompat valueAnimatorCompat);
    }

    interface AnimatorUpdateListener {
        void onAnimationUpdate(ValueAnimatorCompat valueAnimatorCompat);
    }

    interface Creator {
        ValueAnimatorCompat createAnimator();
    }

    static class AnimatorListenerAdapter implements AnimatorListener {
        AnimatorListenerAdapter() {
        }

        public void onAnimationStart(ValueAnimatorCompat valueAnimatorCompat) {
        }

        public void onAnimationEnd(ValueAnimatorCompat valueAnimatorCompat) {
        }

        public void onAnimationCancel(ValueAnimatorCompat valueAnimatorCompat) {
        }
    }

    static abstract class Impl {

        interface AnimatorListenerProxy {
            void onAnimationCancel();

            void onAnimationEnd();

            void onAnimationStart();
        }

        interface AnimatorUpdateListenerProxy {
            void onAnimationUpdate();
        }

        /* access modifiers changed from: package-private */
        public abstract void cancel();

        /* access modifiers changed from: package-private */
        public abstract void end();

        /* access modifiers changed from: package-private */
        public abstract float getAnimatedFloatValue();

        /* access modifiers changed from: package-private */
        public abstract float getAnimatedFraction();

        /* access modifiers changed from: package-private */
        public abstract int getAnimatedIntValue();

        /* access modifiers changed from: package-private */
        public abstract long getDuration();

        /* access modifiers changed from: package-private */
        public abstract boolean isRunning();

        /* access modifiers changed from: package-private */
        public abstract void setDuration(int i);

        /* access modifiers changed from: package-private */
        public abstract void setFloatValues(float f, float f2);

        /* access modifiers changed from: package-private */
        public abstract void setIntValues(int i, int i2);

        /* access modifiers changed from: package-private */
        public abstract void setInterpolator(Interpolator interpolator);

        /* access modifiers changed from: package-private */
        public abstract void setListener(AnimatorListenerProxy animatorListenerProxy);

        /* access modifiers changed from: package-private */
        public abstract void setUpdateListener(AnimatorUpdateListenerProxy animatorUpdateListenerProxy);

        /* access modifiers changed from: package-private */
        public abstract void start();

        Impl() {
        }
    }

    ValueAnimatorCompat(Impl impl) {
        this.mImpl = impl;
    }

    public void start() {
        this.mImpl.start();
    }

    public boolean isRunning() {
        return this.mImpl.isRunning();
    }

    public void setInterpolator(Interpolator interpolator) {
        this.mImpl.setInterpolator(interpolator);
    }

    public void setUpdateListener(AnimatorUpdateListener animatorUpdateListener) {
        Impl.AnimatorUpdateListenerProxy animatorUpdateListenerProxy;
        AnimatorUpdateListener animatorUpdateListener2 = animatorUpdateListener;
        if (animatorUpdateListener2 != null) {
            final AnimatorUpdateListener animatorUpdateListener3 = animatorUpdateListener2;
            new Impl.AnimatorUpdateListenerProxy() {
                public void onAnimationUpdate() {
                    animatorUpdateListener3.onAnimationUpdate(ValueAnimatorCompat.this);
                }
            };
            this.mImpl.setUpdateListener(animatorUpdateListenerProxy);
            return;
        }
        this.mImpl.setUpdateListener(null);
    }

    public void setListener(AnimatorListener animatorListener) {
        Impl.AnimatorListenerProxy animatorListenerProxy;
        AnimatorListener animatorListener2 = animatorListener;
        if (animatorListener2 != null) {
            final AnimatorListener animatorListener3 = animatorListener2;
            new Impl.AnimatorListenerProxy() {
                public void onAnimationStart() {
                    animatorListener3.onAnimationStart(ValueAnimatorCompat.this);
                }

                public void onAnimationEnd() {
                    animatorListener3.onAnimationEnd(ValueAnimatorCompat.this);
                }

                public void onAnimationCancel() {
                    animatorListener3.onAnimationCancel(ValueAnimatorCompat.this);
                }
            };
            this.mImpl.setListener(animatorListenerProxy);
            return;
        }
        this.mImpl.setListener(null);
    }

    public void setIntValues(int i, int i2) {
        this.mImpl.setIntValues(i, i2);
    }

    public int getAnimatedIntValue() {
        return this.mImpl.getAnimatedIntValue();
    }

    public void setFloatValues(float f, float f2) {
        this.mImpl.setFloatValues(f, f2);
    }

    public float getAnimatedFloatValue() {
        return this.mImpl.getAnimatedFloatValue();
    }

    public void setDuration(int i) {
        this.mImpl.setDuration(i);
    }

    public void cancel() {
        this.mImpl.cancel();
    }

    public float getAnimatedFraction() {
        return this.mImpl.getAnimatedFraction();
    }

    public void end() {
        this.mImpl.end();
    }

    public long getDuration() {
        return this.mImpl.getDuration();
    }
}
