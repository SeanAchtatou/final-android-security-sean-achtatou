package android.support.design.widget;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.design.R;
import android.support.v7.graphics.drawable.DrawableWrapper;

class ShadowDrawableWrapper extends DrawableWrapper {
    static final double COS_45 = Math.cos(Math.toRadians(45.0d));
    static final float SHADOW_BOTTOM_SCALE = 1.0f;
    static final float SHADOW_HORIZ_SCALE = 0.5f;
    static final float SHADOW_MULTIPLIER = 1.5f;
    static final float SHADOW_TOP_SCALE = 0.25f;
    private boolean mAddPaddingForCorners = true;
    final RectF mContentBounds;
    float mCornerRadius;
    final Paint mCornerShadowPaint;
    Path mCornerShadowPath;
    private boolean mDirty = true;
    final Paint mEdgeShadowPaint;
    float mMaxShadowSize;
    private boolean mPrintedShadowClipWarning = false;
    float mRawMaxShadowSize;
    float mRawShadowSize;
    private float mRotation;
    private final int mShadowEndColor;
    private final int mShadowMiddleColor;
    float mShadowSize;
    private final int mShadowStartColor;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ShadowDrawableWrapper(Resources resources, Drawable drawable, float f, float f2, float f3) {
        super(drawable);
        Paint paint;
        RectF rectF;
        Paint paint2;
        Resources resources2 = resources;
        this.mShadowStartColor = resources2.getColor(R.color.design_fab_shadow_start_color);
        this.mShadowMiddleColor = resources2.getColor(R.color.design_fab_shadow_mid_color);
        this.mShadowEndColor = resources2.getColor(R.color.design_fab_shadow_end_color);
        new Paint(5);
        this.mCornerShadowPaint = paint;
        this.mCornerShadowPaint.setStyle(Paint.Style.FILL);
        this.mCornerRadius = (float) Math.round(f);
        new RectF();
        this.mContentBounds = rectF;
        new Paint(this.mCornerShadowPaint);
        this.mEdgeShadowPaint = paint2;
        this.mEdgeShadowPaint.setAntiAlias(false);
        setShadowSize(f2, f3);
    }

    private static int toEven(float f) {
        int round = Math.round(f);
        return round % 2 == 1 ? round - 1 : round;
    }

    public void setAddPaddingForCorners(boolean z) {
        this.mAddPaddingForCorners = z;
        invalidateSelf();
    }

    public void setAlpha(int i) {
        int i2 = i;
        super.setAlpha(i2);
        this.mCornerShadowPaint.setAlpha(i2);
        this.mEdgeShadowPaint.setAlpha(i2);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.mDirty = true;
    }

    /* access modifiers changed from: package-private */
    public void setShadowSize(float f, float f2) {
        Throwable th;
        float f3 = f;
        float f4 = f2;
        if (f3 < 0.0f || f4 < 0.0f) {
            Throwable th2 = th;
            new IllegalArgumentException("invalid shadow size");
            throw th2;
        }
        float even = (float) toEven(f3);
        float even2 = (float) toEven(f4);
        if (even > even2) {
            even = even2;
            if (!this.mPrintedShadowClipWarning) {
                this.mPrintedShadowClipWarning = true;
            }
        }
        if (this.mRawShadowSize != even || this.mRawMaxShadowSize != even2) {
            this.mRawShadowSize = even;
            this.mRawMaxShadowSize = even2;
            this.mShadowSize = (float) Math.round(even * SHADOW_MULTIPLIER);
            this.mMaxShadowSize = even2;
            this.mDirty = true;
            invalidateSelf();
        }
    }

    public boolean getPadding(Rect rect) {
        int ceil = (int) Math.ceil((double) calculateVerticalPadding(this.mRawMaxShadowSize, this.mCornerRadius, this.mAddPaddingForCorners));
        int ceil2 = (int) Math.ceil((double) calculateHorizontalPadding(this.mRawMaxShadowSize, this.mCornerRadius, this.mAddPaddingForCorners));
        rect.set(ceil2, ceil, ceil2, ceil);
        return true;
    }

    public static float calculateVerticalPadding(float f, float f2, boolean z) {
        float f3 = f;
        float f4 = f2;
        if (z) {
            return (float) (((double) (f3 * SHADOW_MULTIPLIER)) + ((1.0d - COS_45) * ((double) f4)));
        }
        return f3 * SHADOW_MULTIPLIER;
    }

    public static float calculateHorizontalPadding(float f, float f2, boolean z) {
        float f3 = f;
        float f4 = f2;
        if (z) {
            return (float) (((double) f3) + ((1.0d - COS_45) * ((double) f4)));
        }
        return f3;
    }

    public int getOpacity() {
        return -3;
    }

    public void setCornerRadius(float f) {
        float round = (float) Math.round(f);
        if (this.mCornerRadius != round) {
            this.mCornerRadius = round;
            this.mDirty = true;
            invalidateSelf();
        }
    }

    public void draw(Canvas canvas) {
        Canvas canvas2 = canvas;
        if (this.mDirty) {
            buildComponents(getBounds());
            this.mDirty = false;
        }
        drawShadow(canvas2);
        super.draw(canvas2);
    }

    /* access modifiers changed from: package-private */
    public final void setRotation(float f) {
        float f2 = f;
        if (this.mRotation != f2) {
            this.mRotation = f2;
            invalidateSelf();
        }
    }

    private void drawShadow(Canvas canvas) {
        Canvas canvas2 = canvas;
        int save = canvas2.save();
        canvas2.rotate(this.mRotation, this.mContentBounds.centerX(), this.mContentBounds.centerY());
        float f = (-this.mCornerRadius) - this.mShadowSize;
        float f2 = this.mCornerRadius;
        boolean z = this.mContentBounds.width() - (2.0f * f2) > 0.0f;
        boolean z2 = this.mContentBounds.height() - (2.0f * f2) > 0.0f;
        float f3 = this.mRawShadowSize - (this.mRawShadowSize * SHADOW_TOP_SCALE);
        float f4 = f2 / (f2 + (this.mRawShadowSize - (this.mRawShadowSize * SHADOW_HORIZ_SCALE)));
        float f5 = f2 / (f2 + f3);
        float f6 = f2 / (f2 + (this.mRawShadowSize - (this.mRawShadowSize * SHADOW_BOTTOM_SCALE)));
        int save2 = canvas2.save();
        canvas2.translate(this.mContentBounds.left + f2, this.mContentBounds.top + f2);
        canvas2.scale(f4, f5);
        canvas2.drawPath(this.mCornerShadowPath, this.mCornerShadowPaint);
        if (z) {
            canvas2.scale(SHADOW_BOTTOM_SCALE / f4, SHADOW_BOTTOM_SCALE);
            canvas2.drawRect(0.0f, f, this.mContentBounds.width() - (2.0f * f2), -this.mCornerRadius, this.mEdgeShadowPaint);
        }
        canvas2.restoreToCount(save2);
        int save3 = canvas2.save();
        canvas2.translate(this.mContentBounds.right - f2, this.mContentBounds.bottom - f2);
        canvas2.scale(f4, f6);
        canvas2.rotate(180.0f);
        canvas2.drawPath(this.mCornerShadowPath, this.mCornerShadowPaint);
        if (z) {
            canvas2.scale(SHADOW_BOTTOM_SCALE / f4, SHADOW_BOTTOM_SCALE);
            canvas2.drawRect(0.0f, f, this.mContentBounds.width() - (2.0f * f2), (-this.mCornerRadius) + this.mShadowSize, this.mEdgeShadowPaint);
        }
        canvas2.restoreToCount(save3);
        int save4 = canvas2.save();
        canvas2.translate(this.mContentBounds.left + f2, this.mContentBounds.bottom - f2);
        canvas2.scale(f4, f6);
        canvas2.rotate(270.0f);
        canvas2.drawPath(this.mCornerShadowPath, this.mCornerShadowPaint);
        if (z2) {
            canvas2.scale(SHADOW_BOTTOM_SCALE / f6, SHADOW_BOTTOM_SCALE);
            canvas2.drawRect(0.0f, f, this.mContentBounds.height() - (2.0f * f2), -this.mCornerRadius, this.mEdgeShadowPaint);
        }
        canvas2.restoreToCount(save4);
        int save5 = canvas2.save();
        canvas2.translate(this.mContentBounds.right - f2, this.mContentBounds.top + f2);
        canvas2.scale(f4, f5);
        canvas2.rotate(90.0f);
        canvas2.drawPath(this.mCornerShadowPath, this.mCornerShadowPaint);
        if (z2) {
            canvas2.scale(SHADOW_BOTTOM_SCALE / f5, SHADOW_BOTTOM_SCALE);
            canvas2.drawRect(0.0f, f, this.mContentBounds.height() - (2.0f * f2), -this.mCornerRadius, this.mEdgeShadowPaint);
        }
        canvas2.restoreToCount(save5);
        canvas2.restoreToCount(save);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    private void buildShadowCorners() {
        RectF rectF;
        RectF rectF2;
        Shader shader;
        Shader shader2;
        Path path;
        new RectF(-this.mCornerRadius, -this.mCornerRadius, this.mCornerRadius, this.mCornerRadius);
        RectF rectF3 = rectF;
        new RectF(rectF3);
        RectF rectF4 = rectF2;
        rectF4.inset(-this.mShadowSize, -this.mShadowSize);
        if (this.mCornerShadowPath == null) {
            new Path();
            this.mCornerShadowPath = path;
        } else {
            this.mCornerShadowPath.reset();
        }
        this.mCornerShadowPath.setFillType(Path.FillType.EVEN_ODD);
        this.mCornerShadowPath.moveTo(-this.mCornerRadius, 0.0f);
        this.mCornerShadowPath.rLineTo(-this.mShadowSize, 0.0f);
        this.mCornerShadowPath.arcTo(rectF4, 180.0f, 90.0f, false);
        this.mCornerShadowPath.arcTo(rectF3, 270.0f, -90.0f, false);
        this.mCornerShadowPath.close();
        float f = -rectF4.top;
        if (f > 0.0f) {
            float f2 = this.mCornerRadius / f;
            float f3 = f2 + ((SHADOW_BOTTOM_SCALE - f2) / 2.0f);
            Paint paint = this.mCornerShadowPaint;
            Shader shader3 = shader2;
            int[] iArr = new int[4];
            iArr[0] = 0;
            int[] iArr2 = iArr;
            iArr2[1] = this.mShadowStartColor;
            int[] iArr3 = iArr2;
            iArr3[2] = this.mShadowMiddleColor;
            int[] iArr4 = iArr3;
            int[] iArr5 = iArr4;
            iArr4[3] = this.mShadowEndColor;
            float[] fArr = new float[4];
            fArr[0] = 0.0f;
            float[] fArr2 = fArr;
            fArr2[1] = f2;
            float[] fArr3 = fArr2;
            fArr3[2] = f3;
            float[] fArr4 = fArr3;
            fArr4[3] = 1.0f;
            new RadialGradient(0.0f, 0.0f, f, iArr5, fArr4, Shader.TileMode.CLAMP);
            Shader shader4 = paint.setShader(shader3);
        }
        Paint paint2 = this.mEdgeShadowPaint;
        Shader shader5 = shader;
        float f4 = rectF3.top;
        float f5 = rectF4.top;
        int[] iArr6 = new int[3];
        iArr6[0] = this.mShadowStartColor;
        int[] iArr7 = iArr6;
        iArr7[1] = this.mShadowMiddleColor;
        int[] iArr8 = iArr7;
        iArr8[2] = this.mShadowEndColor;
        new LinearGradient(0.0f, f4, 0.0f, f5, iArr8, new float[]{0.0f, SHADOW_HORIZ_SCALE, SHADOW_BOTTOM_SCALE}, Shader.TileMode.CLAMP);
        Shader shader6 = paint2.setShader(shader5);
        this.mEdgeShadowPaint.setAntiAlias(false);
    }

    private void buildComponents(Rect rect) {
        Rect rect2 = rect;
        float f = this.mRawMaxShadowSize * SHADOW_MULTIPLIER;
        this.mContentBounds.set(((float) rect2.left) + this.mRawMaxShadowSize, ((float) rect2.top) + f, ((float) rect2.right) - this.mRawMaxShadowSize, ((float) rect2.bottom) - f);
        getWrappedDrawable().setBounds((int) this.mContentBounds.left, (int) this.mContentBounds.top, (int) this.mContentBounds.right, (int) this.mContentBounds.bottom);
        buildShadowCorners();
    }

    public float getCornerRadius() {
        return this.mCornerRadius;
    }

    public void setShadowSize(float f) {
        setShadowSize(f, this.mRawMaxShadowSize);
    }

    public void setMaxShadowSize(float f) {
        setShadowSize(this.mRawShadowSize, f);
    }

    public float getShadowSize() {
        return this.mRawShadowSize;
    }

    public float getMaxShadowSize() {
        return this.mRawMaxShadowSize;
    }

    public float getMinWidth() {
        return (2.0f * Math.max(this.mRawMaxShadowSize, this.mCornerRadius + (this.mRawMaxShadowSize / 2.0f))) + (this.mRawMaxShadowSize * 2.0f);
    }

    public float getMinHeight() {
        return (2.0f * Math.max(this.mRawMaxShadowSize, this.mCornerRadius + ((this.mRawMaxShadowSize * SHADOW_MULTIPLIER) / 2.0f))) + (this.mRawMaxShadowSize * SHADOW_MULTIPLIER * 2.0f);
    }
}
