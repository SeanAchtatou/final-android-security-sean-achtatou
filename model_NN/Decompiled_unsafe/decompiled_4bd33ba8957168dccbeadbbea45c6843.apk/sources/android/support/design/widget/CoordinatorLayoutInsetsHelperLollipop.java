package android.support.design.widget;

import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.view.View;

class CoordinatorLayoutInsetsHelperLollipop implements CoordinatorLayoutInsetsHelper {
    CoordinatorLayoutInsetsHelperLollipop() {
    }

    public void setupForWindowInsets(View view, OnApplyWindowInsetsListener onApplyWindowInsetsListener) {
        View view2 = view;
        OnApplyWindowInsetsListener onApplyWindowInsetsListener2 = onApplyWindowInsetsListener;
        if (ViewCompat.getFitsSystemWindows(view2)) {
            ViewCompat.setOnApplyWindowInsetsListener(view2, onApplyWindowInsetsListener2);
            view2.setSystemUiVisibility(1280);
        }
    }
}
