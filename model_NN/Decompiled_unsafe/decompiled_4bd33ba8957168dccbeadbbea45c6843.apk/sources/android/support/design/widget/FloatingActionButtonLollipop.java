package android.support.design.widget;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;
import android.view.animation.Interpolator;

@TargetApi(21)
class FloatingActionButtonLollipop extends FloatingActionButtonHoneycombMr1 {
    private Interpolator mInterpolator;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    FloatingActionButtonLollipop(android.view.View r7, android.support.design.widget.ShadowViewDelegate r8) {
        /*
            r6 = this;
            r0 = r6
            r1 = r7
            r2 = r8
            r3 = r0
            r4 = r1
            r5 = r2
            r3.<init>(r4, r5)
            r3 = r1
            boolean r3 = r3.isInEditMode()
            if (r3 != 0) goto L_0x0021
            r3 = r0
            r4 = r0
            android.view.View r4 = r4.mView
            android.content.Context r4 = r4.getContext()
            r5 = 17563661(0x10c000d, float:2.5713975E-38)
            android.view.animation.Interpolator r4 = android.view.animation.AnimationUtils.loadInterpolator(r4, r5)
            r3.mInterpolator = r4
        L_0x0021:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.FloatingActionButtonLollipop.<init>(android.view.View, android.support.design.widget.ShadowViewDelegate):void");
    }

    /* access modifiers changed from: package-private */
    public void setBackgroundDrawable(ColorStateList colorStateList, PorterDuff.Mode mode, int i, int i2) {
        Drawable drawable;
        Drawable drawable2;
        Drawable drawable3;
        ColorStateList colorStateList2 = colorStateList;
        PorterDuff.Mode mode2 = mode;
        int i3 = i;
        int i4 = i2;
        this.mShapeDrawable = DrawableCompat.wrap(createShapeDrawable());
        DrawableCompat.setTintList(this.mShapeDrawable, colorStateList2);
        if (mode2 != null) {
            DrawableCompat.setTintMode(this.mShapeDrawable, mode2);
        }
        if (i4 > 0) {
            this.mBorderDrawable = createBorderDrawable(i4, colorStateList2);
            Drawable drawable4 = drawable3;
            Drawable[] drawableArr = new Drawable[2];
            drawableArr[0] = this.mBorderDrawable;
            Drawable[] drawableArr2 = drawableArr;
            drawableArr2[1] = this.mShapeDrawable;
            new LayerDrawable(drawableArr2);
            drawable = drawable4;
        } else {
            this.mBorderDrawable = null;
            drawable = this.mShapeDrawable;
        }
        new RippleDrawable(ColorStateList.valueOf(i3), drawable, null);
        this.mRippleDrawable = drawable2;
        this.mShadowViewDelegate.setBackgroundDrawable(this.mRippleDrawable);
        this.mShadowViewDelegate.setShadowPadding(0, 0, 0, 0);
    }

    /* access modifiers changed from: package-private */
    public void setRippleColor(int i) {
        int i2 = i;
        if (this.mRippleDrawable instanceof RippleDrawable) {
            ((RippleDrawable) this.mRippleDrawable).setColor(ColorStateList.valueOf(i2));
        } else {
            super.setRippleColor(i2);
        }
    }

    public void setElevation(float f) {
        ViewCompat.setElevation(this.mView, f);
    }

    /* access modifiers changed from: package-private */
    public void setPressedTranslationZ(float f) {
        StateListAnimator stateListAnimator;
        float f2 = f;
        new StateListAnimator();
        StateListAnimator stateListAnimator2 = stateListAnimator;
        stateListAnimator2.addState(PRESSED_ENABLED_STATE_SET, setupAnimator(ObjectAnimator.ofFloat(this.mView, "translationZ", f2)));
        stateListAnimator2.addState(FOCUSED_ENABLED_STATE_SET, setupAnimator(ObjectAnimator.ofFloat(this.mView, "translationZ", f2)));
        stateListAnimator2.addState(EMPTY_STATE_SET, setupAnimator(ObjectAnimator.ofFloat(this.mView, "translationZ", 0.0f)));
        this.mView.setStateListAnimator(stateListAnimator2);
    }

    /* access modifiers changed from: package-private */
    public void onDrawableStateChanged(int[] iArr) {
    }

    /* access modifiers changed from: package-private */
    public void jumpDrawableToCurrentState() {
    }

    /* access modifiers changed from: package-private */
    public boolean requirePreDrawListener() {
        return false;
    }

    private Animator setupAnimator(Animator animator) {
        Animator animator2 = animator;
        animator2.setInterpolator(this.mInterpolator);
        return animator2;
    }

    /* access modifiers changed from: package-private */
    public CircularBorderDrawable newCircularDrawable() {
        CircularBorderDrawable circularBorderDrawable;
        new CircularBorderDrawableLollipop();
        return circularBorderDrawable;
    }
}
