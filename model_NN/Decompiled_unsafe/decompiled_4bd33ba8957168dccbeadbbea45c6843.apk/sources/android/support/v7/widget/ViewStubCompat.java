package android.support.v7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.lang.ref.WeakReference;

public final class ViewStubCompat extends View {
    private OnInflateListener mInflateListener;
    private int mInflatedId;
    private WeakReference<View> mInflatedViewRef;
    private LayoutInflater mInflater;
    private int mLayoutResource;

    public interface OnInflateListener {
        void onInflate(ViewStubCompat viewStubCompat, View view);
    }

    public ViewStubCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ViewStubCompat(android.content.Context r11, android.util.AttributeSet r12, int r13) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r3 = r13
            r5 = r0
            r6 = r1
            r7 = r2
            r8 = r3
            r5.<init>(r6, r7, r8)
            r5 = r0
            r6 = 0
            r5.mLayoutResource = r6
            r5 = r1
            r6 = r2
            int[] r7 = android.support.v7.appcompat.R.styleable.ViewStubCompat
            r8 = r3
            r9 = 0
            android.content.res.TypedArray r5 = r5.obtainStyledAttributes(r6, r7, r8, r9)
            r4 = r5
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ViewStubCompat_android_inflatedId
            r8 = -1
            int r6 = r6.getResourceId(r7, r8)
            r5.mInflatedId = r6
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ViewStubCompat_android_layout
            r8 = 0
            int r6 = r6.getResourceId(r7, r8)
            r5.mLayoutResource = r6
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ViewStubCompat_android_id
            r8 = -1
            int r6 = r6.getResourceId(r7, r8)
            r5.setId(r6)
            r5 = r4
            r5.recycle()
            r5 = r0
            r6 = 8
            r5.setVisibility(r6)
            r5 = r0
            r6 = 1
            r5.setWillNotDraw(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ViewStubCompat.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public int getInflatedId() {
        return this.mInflatedId;
    }

    public void setInflatedId(int i) {
        this.mInflatedId = i;
    }

    public int getLayoutResource() {
        return this.mLayoutResource;
    }

    public void setLayoutResource(int i) {
        this.mLayoutResource = i;
    }

    public void setLayoutInflater(LayoutInflater layoutInflater) {
        this.mInflater = layoutInflater;
    }

    public LayoutInflater getLayoutInflater() {
        return this.mInflater;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(0, 0);
    }

    public void draw(Canvas canvas) {
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
    }

    public void setVisibility(int i) {
        Throwable th;
        int i2 = i;
        if (this.mInflatedViewRef != null) {
            View view = this.mInflatedViewRef.get();
            if (view != null) {
                view.setVisibility(i2);
                return;
            }
            Throwable th2 = th;
            new IllegalStateException("setVisibility called on un-referenced view");
            throw th2;
        }
        super.setVisibility(i2);
        if (i2 == 0 || i2 == 4) {
            View inflate = inflate();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View inflate() {
        Throwable th;
        Throwable th2;
        LayoutInflater from;
        WeakReference<View> weakReference;
        ViewParent parent = getParent();
        if (parent == null || !(parent instanceof ViewGroup)) {
            Throwable th3 = th;
            new IllegalStateException("ViewStub must have a non-null ViewGroup viewParent");
            throw th3;
        } else if (this.mLayoutResource != 0) {
            ViewGroup viewGroup = (ViewGroup) parent;
            if (this.mInflater != null) {
                from = this.mInflater;
            } else {
                from = LayoutInflater.from(getContext());
            }
            View inflate = from.inflate(this.mLayoutResource, viewGroup, false);
            if (this.mInflatedId != -1) {
                inflate.setId(this.mInflatedId);
            }
            int indexOfChild = viewGroup.indexOfChild(this);
            viewGroup.removeViewInLayout(this);
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (layoutParams != null) {
                viewGroup.addView(inflate, indexOfChild, layoutParams);
            } else {
                viewGroup.addView(inflate, indexOfChild);
            }
            new WeakReference<>(inflate);
            this.mInflatedViewRef = weakReference;
            if (this.mInflateListener != null) {
                this.mInflateListener.onInflate(this, inflate);
            }
            return inflate;
        } else {
            Throwable th4 = th2;
            new IllegalArgumentException("ViewStub must have a valid layoutResource");
            throw th4;
        }
    }

    public void setOnInflateListener(OnInflateListener onInflateListener) {
        this.mInflateListener = onInflateListener;
    }
}
