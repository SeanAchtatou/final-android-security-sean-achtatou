package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.app.ActionBar;
import android.support.v7.appcompat.R;
import android.support.v7.view.ActionBarPolicy;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ScrollingTabContainerView extends HorizontalScrollView implements AdapterView.OnItemSelectedListener {
    private static final int FADE_DURATION = 200;
    private static final String TAG = "ScrollingTabContainerView";
    private static final Interpolator sAlphaInterpolator;
    private boolean mAllowCollapse;
    private int mContentHeight;
    int mMaxTabWidth;
    private int mSelectedTabIndex;
    int mStackedTabMaxWidth;
    private TabClickListener mTabClickListener;
    /* access modifiers changed from: private */
    public LinearLayoutCompat mTabLayout = createTabLayout();
    Runnable mTabSelector;
    private Spinner mTabSpinner;
    protected final VisibilityAnimListener mVisAnimListener;
    protected ViewPropertyAnimatorCompat mVisibilityAnim;

    static {
        Interpolator interpolator;
        new DecelerateInterpolator();
        sAlphaInterpolator = interpolator;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ScrollingTabContainerView(android.content.Context r11) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r3 = r0
            r4 = r1
            r3.<init>(r4)
            r3 = r0
            android.support.v7.widget.ScrollingTabContainerView$VisibilityAnimListener r4 = new android.support.v7.widget.ScrollingTabContainerView$VisibilityAnimListener
            r9 = r4
            r4 = r9
            r5 = r9
            r6 = r0
            r5.<init>()
            r3.mVisAnimListener = r4
            r3 = r0
            r4 = 0
            r3.setHorizontalScrollBarEnabled(r4)
            r3 = r1
            android.support.v7.view.ActionBarPolicy r3 = android.support.v7.view.ActionBarPolicy.get(r3)
            r2 = r3
            r3 = r0
            r4 = r2
            int r4 = r4.getTabContainerHeight()
            r3.setContentHeight(r4)
            r3 = r0
            r4 = r2
            int r4 = r4.getStackedTabMaxWidth()
            r3.mStackedTabMaxWidth = r4
            r3 = r0
            r4 = r0
            android.support.v7.widget.LinearLayoutCompat r4 = r4.createTabLayout()
            r3.mTabLayout = r4
            r3 = r0
            r4 = r0
            android.support.v7.widget.LinearLayoutCompat r4 = r4.mTabLayout
            android.view.ViewGroup$LayoutParams r5 = new android.view.ViewGroup$LayoutParams
            r9 = r5
            r5 = r9
            r6 = r9
            r7 = -2
            r8 = -1
            r6.<init>(r7, r8)
            r3.addView(r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ScrollingTabContainerView.<init>(android.content.Context):void");
    }

    public void onMeasure(int i, int i2) {
        int i3 = i;
        int mode = View.MeasureSpec.getMode(i3);
        boolean z = mode == 1073741824;
        setFillViewport(z);
        int childCount = this.mTabLayout.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.mMaxTabWidth = -1;
        } else {
            if (childCount > 2) {
                this.mMaxTabWidth = (int) (((float) View.MeasureSpec.getSize(i3)) * 0.4f);
            } else {
                this.mMaxTabWidth = View.MeasureSpec.getSize(i3) / 2;
            }
            this.mMaxTabWidth = Math.min(this.mMaxTabWidth, this.mStackedTabMaxWidth);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.mContentHeight, 1073741824);
        if (!z && this.mAllowCollapse) {
            this.mTabLayout.measure(0, makeMeasureSpec);
            if (this.mTabLayout.getMeasuredWidth() > View.MeasureSpec.getSize(i3)) {
                performCollapse();
            } else {
                boolean performExpand = performExpand();
            }
        } else {
            boolean performExpand2 = performExpand();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i3, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z && measuredWidth != measuredWidth2) {
            setTabSelected(this.mSelectedTabIndex);
        }
    }

    private boolean isCollapsed() {
        return this.mTabSpinner != null && this.mTabSpinner.getParent() == this;
    }

    public void setAllowCollapse(boolean z) {
        this.mAllowCollapse = z;
    }

    private void performCollapse() {
        ViewGroup.LayoutParams layoutParams;
        SpinnerAdapter spinnerAdapter;
        if (!isCollapsed()) {
            if (this.mTabSpinner == null) {
                this.mTabSpinner = createSpinner();
            }
            removeView(this.mTabLayout);
            new ViewGroup.LayoutParams(-2, -1);
            addView(this.mTabSpinner, layoutParams);
            if (this.mTabSpinner.getAdapter() == null) {
                new TabAdapter();
                this.mTabSpinner.setAdapter(spinnerAdapter);
            }
            if (this.mTabSelector != null) {
                boolean removeCallbacks = removeCallbacks(this.mTabSelector);
                this.mTabSelector = null;
            }
            this.mTabSpinner.setSelection(this.mSelectedTabIndex);
        }
    }

    private boolean performExpand() {
        ViewGroup.LayoutParams layoutParams;
        if (!isCollapsed()) {
            return false;
        }
        removeView(this.mTabSpinner);
        new ViewGroup.LayoutParams(-2, -1);
        addView(this.mTabLayout, layoutParams);
        setTabSelected(this.mTabSpinner.getSelectedItemPosition());
        return false;
    }

    public void setTabSelected(int i) {
        int i2 = i;
        this.mSelectedTabIndex = i2;
        int childCount = this.mTabLayout.getChildCount();
        int i3 = 0;
        while (i3 < childCount) {
            View childAt = this.mTabLayout.getChildAt(i3);
            boolean z = i3 == i2;
            childAt.setSelected(z);
            if (z) {
                animateToTab(i2);
            }
            i3++;
        }
        if (this.mTabSpinner != null && i2 >= 0) {
            this.mTabSpinner.setSelection(i2);
        }
    }

    public void setContentHeight(int i) {
        this.mContentHeight = i;
        requestLayout();
    }

    private LinearLayoutCompat createTabLayout() {
        LinearLayoutCompat linearLayoutCompat;
        ViewGroup.LayoutParams layoutParams;
        new LinearLayoutCompat(getContext(), null, R.attr.actionBarTabBarStyle);
        LinearLayoutCompat linearLayoutCompat2 = linearLayoutCompat;
        linearLayoutCompat2.setMeasureWithLargestChildEnabled(true);
        linearLayoutCompat2.setGravity(17);
        new LinearLayoutCompat.LayoutParams(-2, -1);
        linearLayoutCompat2.setLayoutParams(layoutParams);
        return linearLayoutCompat2;
    }

    private Spinner createSpinner() {
        Spinner spinner;
        ViewGroup.LayoutParams layoutParams;
        new AppCompatSpinner(getContext(), null, R.attr.actionDropDownStyle);
        Spinner spinner2 = spinner;
        new LinearLayoutCompat.LayoutParams(-2, -1);
        spinner2.setLayoutParams(layoutParams);
        spinner2.setOnItemSelectedListener(this);
        return spinner2;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        Configuration configuration2 = configuration;
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration2);
        }
        ActionBarPolicy actionBarPolicy = ActionBarPolicy.get(getContext());
        setContentHeight(actionBarPolicy.getTabContainerHeight());
        this.mStackedTabMaxWidth = actionBarPolicy.getStackedTabMaxWidth();
    }

    public void animateToVisibility(int i) {
        int i2 = i;
        if (this.mVisibilityAnim != null) {
            this.mVisibilityAnim.cancel();
        }
        if (i2 == 0) {
            if (getVisibility() != 0) {
                ViewCompat.setAlpha(this, 0.0f);
            }
            ViewPropertyAnimatorCompat alpha = ViewCompat.animate(this).alpha(1.0f);
            ViewPropertyAnimatorCompat duration = alpha.setDuration(200);
            ViewPropertyAnimatorCompat interpolator = alpha.setInterpolator(sAlphaInterpolator);
            ViewPropertyAnimatorCompat listener = alpha.setListener(this.mVisAnimListener.withFinalVisibility(alpha, i2));
            alpha.start();
            return;
        }
        ViewPropertyAnimatorCompat alpha2 = ViewCompat.animate(this).alpha(0.0f);
        ViewPropertyAnimatorCompat duration2 = alpha2.setDuration(200);
        ViewPropertyAnimatorCompat interpolator2 = alpha2.setInterpolator(sAlphaInterpolator);
        ViewPropertyAnimatorCompat listener2 = alpha2.setListener(this.mVisAnimListener.withFinalVisibility(alpha2, i2));
        alpha2.start();
    }

    public void animateToTab(int i) {
        Runnable runnable;
        View childAt = this.mTabLayout.getChildAt(i);
        if (this.mTabSelector != null) {
            boolean removeCallbacks = removeCallbacks(this.mTabSelector);
        }
        final View view = childAt;
        new Runnable() {
            public void run() {
                ScrollingTabContainerView.this.smoothScrollTo(view.getLeft() - ((ScrollingTabContainerView.this.getWidth() - view.getWidth()) / 2), 0);
                ScrollingTabContainerView.this.mTabSelector = null;
            }
        };
        this.mTabSelector = runnable;
        boolean post = post(this.mTabSelector);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mTabSelector != null) {
            boolean post = post(this.mTabSelector);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mTabSelector != null) {
            boolean removeCallbacks = removeCallbacks(this.mTabSelector);
        }
    }

    /* access modifiers changed from: private */
    public TabView createTabView(ActionBar.Tab tab, boolean z) {
        TabView tabView;
        TabClickListener tabClickListener;
        ViewGroup.LayoutParams layoutParams;
        boolean z2 = z;
        new TabView(this, getContext(), tab, z2);
        TabView tabView2 = tabView;
        if (z2) {
            tabView2.setBackgroundDrawable(null);
            new AbsListView.LayoutParams(-1, this.mContentHeight);
            tabView2.setLayoutParams(layoutParams);
        } else {
            tabView2.setFocusable(true);
            if (this.mTabClickListener == null) {
                new TabClickListener();
                this.mTabClickListener = tabClickListener;
            }
            tabView2.setOnClickListener(this.mTabClickListener);
        }
        return tabView2;
    }

    public void addTab(ActionBar.Tab tab, boolean z) {
        ViewGroup.LayoutParams layoutParams;
        boolean z2 = z;
        TabView createTabView = createTabView(tab, false);
        new LinearLayoutCompat.LayoutParams(0, -1, 1.0f);
        this.mTabLayout.addView(createTabView, layoutParams);
        if (this.mTabSpinner != null) {
            ((TabAdapter) this.mTabSpinner.getAdapter()).notifyDataSetChanged();
        }
        if (z2) {
            createTabView.setSelected(true);
        }
        if (this.mAllowCollapse) {
            requestLayout();
        }
    }

    public void addTab(ActionBar.Tab tab, int i, boolean z) {
        ViewGroup.LayoutParams layoutParams;
        boolean z2 = z;
        TabView createTabView = createTabView(tab, false);
        new LinearLayoutCompat.LayoutParams(0, -1, 1.0f);
        this.mTabLayout.addView(createTabView, i, layoutParams);
        if (this.mTabSpinner != null) {
            ((TabAdapter) this.mTabSpinner.getAdapter()).notifyDataSetChanged();
        }
        if (z2) {
            createTabView.setSelected(true);
        }
        if (this.mAllowCollapse) {
            requestLayout();
        }
    }

    public void updateTab(int i) {
        ((TabView) this.mTabLayout.getChildAt(i)).update();
        if (this.mTabSpinner != null) {
            ((TabAdapter) this.mTabSpinner.getAdapter()).notifyDataSetChanged();
        }
        if (this.mAllowCollapse) {
            requestLayout();
        }
    }

    public void removeTabAt(int i) {
        this.mTabLayout.removeViewAt(i);
        if (this.mTabSpinner != null) {
            ((TabAdapter) this.mTabSpinner.getAdapter()).notifyDataSetChanged();
        }
        if (this.mAllowCollapse) {
            requestLayout();
        }
    }

    public void removeAllTabs() {
        this.mTabLayout.removeAllViews();
        if (this.mTabSpinner != null) {
            ((TabAdapter) this.mTabSpinner.getAdapter()).notifyDataSetChanged();
        }
        if (this.mAllowCollapse) {
            requestLayout();
        }
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        ((TabView) view).getTab().select();
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private class TabView extends LinearLayoutCompat implements View.OnLongClickListener {
        private final int[] BG_ATTRS = {16842964};
        private View mCustomView;
        private ImageView mIconView;
        private ActionBar.Tab mTab;
        private TextView mTextView;
        final /* synthetic */ ScrollingTabContainerView this$0;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public TabView(android.support.v7.widget.ScrollingTabContainerView r13, android.content.Context r14, android.support.v7.app.ActionBar.Tab r15, boolean r16) {
            /*
                r12 = this;
                r0 = r12
                r1 = r13
                r2 = r14
                r3 = r15
                r4 = r16
                r6 = r0
                r7 = r1
                r6.this$0 = r7
                r6 = r0
                r7 = r2
                r8 = 0
                int r9 = android.support.v7.appcompat.R.attr.actionBarTabStyle
                r6.<init>(r7, r8, r9)
                r6 = r0
                r7 = 1
                int[] r7 = new int[r7]
                r11 = r7
                r7 = r11
                r8 = r11
                r9 = 0
                r10 = 16842964(0x10100d4, float:2.3694152E-38)
                r8[r9] = r10
                r6.BG_ATTRS = r7
                r6 = r0
                r7 = r3
                r6.mTab = r7
                r6 = r2
                r7 = 0
                r8 = r0
                int[] r8 = r8.BG_ATTRS
                int r9 = android.support.v7.appcompat.R.attr.actionBarTabStyle
                r10 = 0
                android.support.v7.widget.TintTypedArray r6 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r6, r7, r8, r9, r10)
                r5 = r6
                r6 = r5
                r7 = 0
                boolean r6 = r6.hasValue(r7)
                if (r6 == 0) goto L_0x0044
                r6 = r0
                r7 = r5
                r8 = 0
                android.graphics.drawable.Drawable r7 = r7.getDrawable(r8)
                r6.setBackgroundDrawable(r7)
            L_0x0044:
                r6 = r5
                r6.recycle()
                r6 = r4
                if (r6 == 0) goto L_0x0052
                r6 = r0
                r7 = 8388627(0x800013, float:1.175497E-38)
                r6.setGravity(r7)
            L_0x0052:
                r6 = r0
                r6.update()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ScrollingTabContainerView.TabView.<init>(android.support.v7.widget.ScrollingTabContainerView, android.content.Context, android.support.v7.app.ActionBar$Tab, boolean):void");
        }

        public void bindTab(ActionBar.Tab tab) {
            this.mTab = tab;
            update();
        }

        public void setSelected(boolean z) {
            boolean z2 = z;
            boolean z3 = isSelected() != z2;
            super.setSelected(z2);
            if (z3 && z2) {
                sendAccessibilityEvent(4);
            }
        }

        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
            super.onInitializeAccessibilityEvent(accessibilityEvent2);
            accessibilityEvent2.setClassName(ActionBar.Tab.class.getName());
        }

        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            AccessibilityNodeInfo accessibilityNodeInfo2 = accessibilityNodeInfo;
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo2);
            if (Build.VERSION.SDK_INT >= 14) {
                accessibilityNodeInfo2.setClassName(ActionBar.Tab.class.getName());
            }
        }

        public void onMeasure(int i, int i2) {
            int i3 = i2;
            super.onMeasure(i, i3);
            if (this.this$0.mMaxTabWidth > 0 && getMeasuredWidth() > this.this$0.mMaxTabWidth) {
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(this.this$0.mMaxTabWidth, 1073741824), i3);
            }
        }

        public void update() {
            TextView textView;
            LinearLayoutCompat.LayoutParams layoutParams;
            ImageView imageView;
            LinearLayoutCompat.LayoutParams layoutParams2;
            ActionBar.Tab tab = this.mTab;
            View customView = tab.getCustomView();
            if (customView != null) {
                ViewParent parent = customView.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(customView);
                    }
                    addView(customView);
                }
                this.mCustomView = customView;
                if (this.mTextView != null) {
                    this.mTextView.setVisibility(8);
                }
                if (this.mIconView != null) {
                    this.mIconView.setVisibility(8);
                    this.mIconView.setImageDrawable(null);
                    return;
                }
                return;
            }
            if (this.mCustomView != null) {
                removeView(this.mCustomView);
                this.mCustomView = null;
            }
            Drawable icon = tab.getIcon();
            CharSequence text = tab.getText();
            if (icon != null) {
                if (this.mIconView == null) {
                    new ImageView(getContext());
                    ImageView imageView2 = imageView;
                    new LinearLayoutCompat.LayoutParams(-2, -2);
                    LinearLayoutCompat.LayoutParams layoutParams3 = layoutParams2;
                    layoutParams3.gravity = 16;
                    imageView2.setLayoutParams(layoutParams3);
                    addView(imageView2, 0);
                    this.mIconView = imageView2;
                }
                this.mIconView.setImageDrawable(icon);
                this.mIconView.setVisibility(0);
            } else if (this.mIconView != null) {
                this.mIconView.setVisibility(8);
                this.mIconView.setImageDrawable(null);
            }
            boolean z = !TextUtils.isEmpty(text);
            if (z) {
                if (this.mTextView == null) {
                    new AppCompatTextView(getContext(), null, R.attr.actionBarTabTextStyle);
                    TextView textView2 = textView;
                    textView2.setEllipsize(TextUtils.TruncateAt.END);
                    new LinearLayoutCompat.LayoutParams(-2, -2);
                    LinearLayoutCompat.LayoutParams layoutParams4 = layoutParams;
                    layoutParams4.gravity = 16;
                    textView2.setLayoutParams(layoutParams4);
                    addView(textView2);
                    this.mTextView = textView2;
                }
                this.mTextView.setText(text);
                this.mTextView.setVisibility(0);
            } else if (this.mTextView != null) {
                this.mTextView.setVisibility(8);
                this.mTextView.setText((CharSequence) null);
            }
            if (this.mIconView != null) {
                this.mIconView.setContentDescription(tab.getContentDescription());
            }
            if (z || TextUtils.isEmpty(tab.getContentDescription())) {
                setOnLongClickListener(null);
                setLongClickable(false);
                return;
            }
            setOnLongClickListener(this);
        }

        public boolean onLongClick(View view) {
            int[] iArr = new int[2];
            getLocationOnScreen(iArr);
            Context context = getContext();
            int width = getWidth();
            int height = getHeight();
            int i = context.getResources().getDisplayMetrics().widthPixels;
            Toast makeText = Toast.makeText(context, this.mTab.getContentDescription(), 0);
            makeText.setGravity(49, (iArr[0] + (width / 2)) - (i / 2), height);
            makeText.show();
            return true;
        }

        public ActionBar.Tab getTab() {
            return this.mTab;
        }
    }

    private class TabAdapter extends BaseAdapter {
        private TabAdapter() {
        }

        public int getCount() {
            return ScrollingTabContainerView.this.mTabLayout.getChildCount();
        }

        public Object getItem(int i) {
            return ((TabView) ScrollingTabContainerView.this.mTabLayout.getChildAt(i)).getTab();
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            int i2 = i;
            TabView tabView = view;
            if (tabView == null) {
                tabView = ScrollingTabContainerView.this.createTabView((ActionBar.Tab) getItem(i2), true);
            } else {
                ((TabView) tabView).bindTab((ActionBar.Tab) getItem(i2));
            }
            return tabView;
        }
    }

    private class TabClickListener implements View.OnClickListener {
        private TabClickListener() {
        }

        public void onClick(View view) {
            View view2 = view;
            ((TabView) view2).getTab().select();
            int childCount = ScrollingTabContainerView.this.mTabLayout.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = ScrollingTabContainerView.this.mTabLayout.getChildAt(i);
                childAt.setSelected(childAt == view2);
            }
        }
    }

    protected class VisibilityAnimListener implements ViewPropertyAnimatorListener {
        private boolean mCanceled = false;
        private int mFinalVisibility;

        protected VisibilityAnimListener() {
        }

        public VisibilityAnimListener withFinalVisibility(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, int i) {
            this.mFinalVisibility = i;
            ScrollingTabContainerView.this.mVisibilityAnim = viewPropertyAnimatorCompat;
            return this;
        }

        public void onAnimationStart(View view) {
            ScrollingTabContainerView.this.setVisibility(0);
            this.mCanceled = false;
        }

        public void onAnimationEnd(View view) {
            if (!this.mCanceled) {
                ScrollingTabContainerView.this.mVisibilityAnim = null;
                ScrollingTabContainerView.this.setVisibility(this.mFinalVisibility);
            }
        }

        public void onAnimationCancel(View view) {
            this.mCanceled = true;
        }
    }
}
