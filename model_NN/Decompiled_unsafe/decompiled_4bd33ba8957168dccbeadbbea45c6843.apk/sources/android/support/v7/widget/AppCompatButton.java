package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.view.TintableBackgroundView;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;

public class AppCompatButton extends Button implements TintableBackgroundView {
    private final AppCompatBackgroundHelper mBackgroundTintHelper;
    private final AppCompatTextHelper mTextHelper;
    private final TintManager mTintManager;

    public AppCompatButton(Context context) {
        this(context, null);
    }

    public AppCompatButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.buttonStyle);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatButton(android.content.Context r11, android.util.AttributeSet r12, int r13) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r3 = r13
            r4 = r0
            r5 = r1
            r6 = r2
            r7 = r3
            r4.<init>(r5, r6, r7)
            r4 = r0
            r5 = r0
            android.content.Context r5 = r5.getContext()
            android.support.v7.widget.TintManager r5 = android.support.v7.widget.TintManager.get(r5)
            r4.mTintManager = r5
            r4 = r0
            android.support.v7.widget.AppCompatBackgroundHelper r5 = new android.support.v7.widget.AppCompatBackgroundHelper
            r9 = r5
            r5 = r9
            r6 = r9
            r7 = r0
            r8 = r0
            android.support.v7.widget.TintManager r8 = r8.mTintManager
            r6.<init>(r7, r8)
            r4.mBackgroundTintHelper = r5
            r4 = r0
            android.support.v7.widget.AppCompatBackgroundHelper r4 = r4.mBackgroundTintHelper
            r5 = r2
            r6 = r3
            r4.loadFromAttributes(r5, r6)
            r4 = r0
            r5 = r0
            android.support.v7.widget.AppCompatTextHelper r5 = android.support.v7.widget.AppCompatTextHelper.create(r5)
            r4.mTextHelper = r5
            r4 = r0
            android.support.v7.widget.AppCompatTextHelper r4 = r4.mTextHelper
            r5 = r2
            r6 = r3
            r4.loadFromAttributes(r5, r6)
            r4 = r0
            android.support.v7.widget.AppCompatTextHelper r4 = r4.mTextHelper
            r4.applyCompoundDrawablesTints()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AppCompatButton.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setBackgroundResource(@DrawableRes int i) {
        int i2 = i;
        super.setBackgroundResource(i2);
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.onSetBackgroundResource(i2);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        Drawable drawable2 = drawable;
        super.setBackgroundDrawable(drawable2);
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.onSetBackgroundDrawable(drawable2);
        }
    }

    public void setSupportBackgroundTintList(@Nullable ColorStateList colorStateList) {
        ColorStateList colorStateList2 = colorStateList;
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.setSupportBackgroundTintList(colorStateList2);
        }
    }

    @Nullable
    public ColorStateList getSupportBackgroundTintList() {
        return this.mBackgroundTintHelper != null ? this.mBackgroundTintHelper.getSupportBackgroundTintList() : null;
    }

    public void setSupportBackgroundTintMode(@Nullable PorterDuff.Mode mode) {
        PorterDuff.Mode mode2 = mode;
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.setSupportBackgroundTintMode(mode2);
        }
    }

    @Nullable
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        return this.mBackgroundTintHelper != null ? this.mBackgroundTintHelper.getSupportBackgroundTintMode() : null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.applySupportBackgroundTint();
        }
        if (this.mTextHelper != null) {
            this.mTextHelper.applyCompoundDrawablesTints();
        }
    }

    public void setTextAppearance(Context context, int i) {
        Context context2 = context;
        int i2 = i;
        super.setTextAppearance(context2, i2);
        if (this.mTextHelper != null) {
            this.mTextHelper.onSetTextAppearance(context2, i2);
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
        super.onInitializeAccessibilityEvent(accessibilityEvent2);
        accessibilityEvent2.setClassName(Button.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        AccessibilityNodeInfo accessibilityNodeInfo2 = accessibilityNodeInfo;
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo2);
        accessibilityNodeInfo2.setClassName(Button.class.getName());
    }

    public void setSupportAllCaps(boolean z) {
        boolean z2 = z;
        if (this.mTextHelper != null) {
            this.mTextHelper.setAllCaps(z2);
        }
    }
}
