package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.view.TintableBackgroundView;
import android.util.AttributeSet;
import android.widget.ImageView;

public class AppCompatImageView extends ImageView implements TintableBackgroundView {
    private AppCompatBackgroundHelper mBackgroundTintHelper;
    private AppCompatImageHelper mImageHelper;

    public AppCompatImageView(Context context) {
        this(context, null);
    }

    public AppCompatImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatImageView(android.content.Context r12, android.util.AttributeSet r13, int r14) {
        /*
            r11 = this;
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r14
            r5 = r0
            r6 = r1
            r7 = r2
            r8 = r3
            r5.<init>(r6, r7, r8)
            r5 = r1
            android.support.v7.widget.TintManager r5 = android.support.v7.widget.TintManager.get(r5)
            r4 = r5
            r5 = r0
            android.support.v7.widget.AppCompatBackgroundHelper r6 = new android.support.v7.widget.AppCompatBackgroundHelper
            r10 = r6
            r6 = r10
            r7 = r10
            r8 = r0
            r9 = r4
            r7.<init>(r8, r9)
            r5.mBackgroundTintHelper = r6
            r5 = r0
            android.support.v7.widget.AppCompatBackgroundHelper r5 = r5.mBackgroundTintHelper
            r6 = r2
            r7 = r3
            r5.loadFromAttributes(r6, r7)
            r5 = r0
            android.support.v7.widget.AppCompatImageHelper r6 = new android.support.v7.widget.AppCompatImageHelper
            r10 = r6
            r6 = r10
            r7 = r10
            r8 = r0
            r9 = r4
            r7.<init>(r8, r9)
            r5.mImageHelper = r6
            r5 = r0
            android.support.v7.widget.AppCompatImageHelper r5 = r5.mImageHelper
            r6 = r2
            r7 = r3
            r5.loadFromAttributes(r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AppCompatImageView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setImageResource(@DrawableRes int i) {
        this.mImageHelper.setImageResource(i);
    }

    public void setBackgroundResource(@DrawableRes int i) {
        int i2 = i;
        super.setBackgroundResource(i2);
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.onSetBackgroundResource(i2);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        Drawable drawable2 = drawable;
        super.setBackgroundDrawable(drawable2);
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.onSetBackgroundDrawable(drawable2);
        }
    }

    public void setSupportBackgroundTintList(@Nullable ColorStateList colorStateList) {
        ColorStateList colorStateList2 = colorStateList;
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.setSupportBackgroundTintList(colorStateList2);
        }
    }

    @Nullable
    public ColorStateList getSupportBackgroundTintList() {
        return this.mBackgroundTintHelper != null ? this.mBackgroundTintHelper.getSupportBackgroundTintList() : null;
    }

    public void setSupportBackgroundTintMode(@Nullable PorterDuff.Mode mode) {
        PorterDuff.Mode mode2 = mode;
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.setSupportBackgroundTintMode(mode2);
        }
    }

    @Nullable
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        return this.mBackgroundTintHelper != null ? this.mBackgroundTintHelper.getSupportBackgroundTintMode() : null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.applySupportBackgroundTint();
        }
    }
}
