package android.support.v7.app;

import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.AdapterView;

class NavItemSelectedListener implements AdapterView.OnItemSelectedListener {
    private final ActionBar.OnNavigationListener mListener;

    public NavItemSelectedListener(ActionBar.OnNavigationListener onNavigationListener) {
        this.mListener = onNavigationListener;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        int i2 = i;
        long j2 = j;
        if (this.mListener != null) {
            boolean onNavigationItemSelected = this.mListener.onNavigationItemSelected(i2, j2);
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
