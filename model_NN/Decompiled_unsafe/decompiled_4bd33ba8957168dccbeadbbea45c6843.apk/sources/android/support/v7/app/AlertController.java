package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.appcompat.R;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewStub;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CursorAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.lang.ref.WeakReference;

class AlertController {
    private ListAdapter mAdapter;
    private int mAlertDialogLayout;
    private final View.OnClickListener mButtonHandler;
    /* access modifiers changed from: private */
    public Button mButtonNegative;
    /* access modifiers changed from: private */
    public Message mButtonNegativeMessage;
    private CharSequence mButtonNegativeText;
    /* access modifiers changed from: private */
    public Button mButtonNeutral;
    /* access modifiers changed from: private */
    public Message mButtonNeutralMessage;
    private CharSequence mButtonNeutralText;
    private int mButtonPanelLayoutHint = 0;
    private int mButtonPanelSideLayout;
    /* access modifiers changed from: private */
    public Button mButtonPositive;
    /* access modifiers changed from: private */
    public Message mButtonPositiveMessage;
    private CharSequence mButtonPositiveText;
    private int mCheckedItem = -1;
    private final Context mContext;
    private View mCustomTitleView;
    /* access modifiers changed from: private */
    public final AppCompatDialog mDialog;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private Drawable mIcon;
    private int mIconId = 0;
    private ImageView mIconView;
    /* access modifiers changed from: private */
    public int mListItemLayout;
    /* access modifiers changed from: private */
    public int mListLayout;
    /* access modifiers changed from: private */
    public ListView mListView;
    private CharSequence mMessage;
    private TextView mMessageView;
    /* access modifiers changed from: private */
    public int mMultiChoiceItemLayout;
    /* access modifiers changed from: private */
    public NestedScrollView mScrollView;
    /* access modifiers changed from: private */
    public int mSingleChoiceItemLayout;
    private CharSequence mTitle;
    private TextView mTitleView;
    private View mView;
    private int mViewLayoutResId;
    private int mViewSpacingBottom;
    private int mViewSpacingLeft;
    private int mViewSpacingRight;
    private boolean mViewSpacingSpecified = false;
    private int mViewSpacingTop;
    private final Window mWindow;

    static /* synthetic */ ListView access$1002(AlertController alertController, ListView listView) {
        ListView listView2 = listView;
        ListView listView3 = listView2;
        alertController.mListView = listView3;
        return listView2;
    }

    static /* synthetic */ ListAdapter access$1502(AlertController alertController, ListAdapter listAdapter) {
        ListAdapter listAdapter2 = listAdapter;
        ListAdapter listAdapter3 = listAdapter2;
        alertController.mAdapter = listAdapter3;
        return listAdapter2;
    }

    static /* synthetic */ int access$1602(AlertController alertController, int i) {
        int i2 = i;
        int i3 = i2;
        alertController.mCheckedItem = i3;
        return i2;
    }

    private static final class ButtonHandler extends Handler {
        private static final int MSG_DISMISS_DIALOG = 1;
        private WeakReference<DialogInterface> mDialog;

        public ButtonHandler(DialogInterface dialogInterface) {
            WeakReference<DialogInterface> weakReference;
            new WeakReference<>(dialogInterface);
            this.mDialog = weakReference;
        }

        public void handleMessage(Message message) {
            Message message2 = message;
            switch (message2.what) {
                case -3:
                case -2:
                case -1:
                    ((DialogInterface.OnClickListener) message2.obj).onClick(this.mDialog.get(), message2.what);
                    return;
                case 0:
                default:
                    return;
                case 1:
                    ((DialogInterface) message2.obj).dismiss();
                    return;
            }
        }
    }

    public AlertController(Context context, AppCompatDialog appCompatDialog, Window window) {
        View.OnClickListener onClickListener;
        Handler handler;
        Context context2 = context;
        AppCompatDialog appCompatDialog2 = appCompatDialog;
        new View.OnClickListener() {
            public void onClick(View view) {
                Message message;
                View view2 = view;
                if (view2 == AlertController.this.mButtonPositive && AlertController.this.mButtonPositiveMessage != null) {
                    message = Message.obtain(AlertController.this.mButtonPositiveMessage);
                } else if (view2 == AlertController.this.mButtonNegative && AlertController.this.mButtonNegativeMessage != null) {
                    message = Message.obtain(AlertController.this.mButtonNegativeMessage);
                } else if (view2 != AlertController.this.mButtonNeutral || AlertController.this.mButtonNeutralMessage == null) {
                    message = null;
                } else {
                    message = Message.obtain(AlertController.this.mButtonNeutralMessage);
                }
                if (message != null) {
                    message.sendToTarget();
                }
                AlertController.this.mHandler.obtainMessage(1, AlertController.this.mDialog).sendToTarget();
            }
        };
        this.mButtonHandler = onClickListener;
        this.mContext = context2;
        this.mDialog = appCompatDialog2;
        this.mWindow = window;
        new ButtonHandler(appCompatDialog2);
        this.mHandler = handler;
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(null, R.styleable.AlertDialog, R.attr.alertDialogStyle, 0);
        this.mAlertDialogLayout = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_android_layout, 0);
        this.mButtonPanelSideLayout = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_buttonPanelSideLayout, 0);
        this.mListLayout = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_listLayout, 0);
        this.mMultiChoiceItemLayout = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_multiChoiceItemLayout, 0);
        this.mSingleChoiceItemLayout = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_singleChoiceItemLayout, 0);
        this.mListItemLayout = obtainStyledAttributes.getResourceId(R.styleable.AlertDialog_listItemLayout, 0);
        obtainStyledAttributes.recycle();
    }

    static boolean canTextInput(View view) {
        View view2 = view;
        if (view2.onCheckIsTextEditor()) {
            return true;
        }
        if (!(view2 instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) view2;
        int childCount = viewGroup.getChildCount();
        while (childCount > 0) {
            childCount--;
            if (canTextInput(viewGroup.getChildAt(childCount))) {
                return true;
            }
        }
        return false;
    }

    public void installContent() {
        boolean supportRequestWindowFeature = this.mDialog.supportRequestWindowFeature(1);
        this.mDialog.setContentView(selectContentView());
        setupView();
    }

    private int selectContentView() {
        if (this.mButtonPanelSideLayout == 0) {
            return this.mAlertDialogLayout;
        }
        if (this.mButtonPanelLayoutHint == 1) {
            return this.mButtonPanelSideLayout;
        }
        return this.mAlertDialogLayout;
    }

    public void setTitle(CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        this.mTitle = charSequence2;
        if (this.mTitleView != null) {
            this.mTitleView.setText(charSequence2);
        }
    }

    public void setCustomTitle(View view) {
        this.mCustomTitleView = view;
    }

    public void setMessage(CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        this.mMessage = charSequence2;
        if (this.mMessageView != null) {
            this.mMessageView.setText(charSequence2);
        }
    }

    public void setView(int i) {
        this.mView = null;
        this.mViewLayoutResId = i;
        this.mViewSpacingSpecified = false;
    }

    public void setView(View view) {
        this.mView = view;
        this.mViewLayoutResId = 0;
        this.mViewSpacingSpecified = false;
    }

    public void setView(View view, int i, int i2, int i3, int i4) {
        this.mView = view;
        this.mViewLayoutResId = 0;
        this.mViewSpacingSpecified = true;
        this.mViewSpacingLeft = i;
        this.mViewSpacingTop = i2;
        this.mViewSpacingRight = i3;
        this.mViewSpacingBottom = i4;
    }

    public void setButtonPanelLayoutHint(int i) {
        this.mButtonPanelLayoutHint = i;
    }

    public void setButton(int i, CharSequence charSequence, DialogInterface.OnClickListener onClickListener, Message message) {
        Throwable th;
        int i2 = i;
        CharSequence charSequence2 = charSequence;
        DialogInterface.OnClickListener onClickListener2 = onClickListener;
        Message message2 = message;
        if (message2 == null && onClickListener2 != null) {
            message2 = this.mHandler.obtainMessage(i2, onClickListener2);
        }
        switch (i2) {
            case -3:
                this.mButtonNeutralText = charSequence2;
                this.mButtonNeutralMessage = message2;
                return;
            case -2:
                this.mButtonNegativeText = charSequence2;
                this.mButtonNegativeMessage = message2;
                return;
            case -1:
                this.mButtonPositiveText = charSequence2;
                this.mButtonPositiveMessage = message2;
                return;
            default:
                Throwable th2 = th;
                new IllegalArgumentException("Button does not exist");
                throw th2;
        }
    }

    public void setIcon(int i) {
        int i2 = i;
        this.mIcon = null;
        this.mIconId = i2;
        if (this.mIconView == null) {
            return;
        }
        if (i2 != 0) {
            this.mIconView.setImageResource(this.mIconId);
        } else {
            this.mIconView.setVisibility(8);
        }
    }

    public void setIcon(Drawable drawable) {
        Drawable drawable2 = drawable;
        this.mIcon = drawable2;
        this.mIconId = 0;
        if (this.mIconView == null) {
            return;
        }
        if (drawable2 != null) {
            this.mIconView.setImageDrawable(drawable2);
        } else {
            this.mIconView.setVisibility(8);
        }
    }

    public int getIconAttributeResId(int i) {
        TypedValue typedValue;
        new TypedValue();
        TypedValue typedValue2 = typedValue;
        boolean resolveAttribute = this.mContext.getTheme().resolveAttribute(i, typedValue2, true);
        return typedValue2.resourceId;
    }

    public ListView getListView() {
        return this.mListView;
    }

    public Button getButton(int i) {
        switch (i) {
            case -3:
                return this.mButtonNeutral;
            case -2:
                return this.mButtonNegative;
            case -1:
                return this.mButtonPositive;
            default:
                return null;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return this.mScrollView != null && this.mScrollView.executeKeyEvent(keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return this.mScrollView != null && this.mScrollView.executeKeyEvent(keyEvent);
    }

    @Nullable
    private ViewGroup resolvePanel(@Nullable View view, @Nullable View view2) {
        View view3 = view;
        View view4 = view2;
        if (view3 == null) {
            if (view4 instanceof ViewStub) {
                view4 = ((ViewStub) view4).inflate();
            }
            return (ViewGroup) view4;
        }
        if (view4 != null) {
            ViewParent parent = view4.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(view4);
            }
        }
        if (view3 instanceof ViewStub) {
            view3 = ((ViewStub) view3).inflate();
        }
        return (ViewGroup) view3;
    }

    private void setupView() {
        View findViewById;
        View findViewById2 = this.mWindow.findViewById(R.id.parentPanel);
        View findViewById3 = findViewById2.findViewById(R.id.topPanel);
        View findViewById4 = findViewById2.findViewById(R.id.contentPanel);
        View findViewById5 = findViewById2.findViewById(R.id.buttonPanel);
        ViewGroup viewGroup = (ViewGroup) findViewById2.findViewById(R.id.customPanel);
        setupCustomContent(viewGroup);
        View findViewById6 = viewGroup.findViewById(R.id.topPanel);
        View findViewById7 = viewGroup.findViewById(R.id.contentPanel);
        View findViewById8 = viewGroup.findViewById(R.id.buttonPanel);
        ViewGroup resolvePanel = resolvePanel(findViewById6, findViewById3);
        ViewGroup resolvePanel2 = resolvePanel(findViewById7, findViewById4);
        ViewGroup resolvePanel3 = resolvePanel(findViewById8, findViewById5);
        setupContent(resolvePanel2);
        setupButtons(resolvePanel3);
        setupTitle(resolvePanel);
        boolean z = (viewGroup == null || viewGroup.getVisibility() == 8) ? false : true;
        boolean z2 = (resolvePanel == null || resolvePanel.getVisibility() == 8) ? false : true;
        boolean z3 = (resolvePanel3 == null || resolvePanel3.getVisibility() == 8) ? false : true;
        if (!(z3 || resolvePanel2 == null || (findViewById = resolvePanel2.findViewById(R.id.textSpacerNoButtons)) == null)) {
            findViewById.setVisibility(0);
        }
        if (z2 && this.mScrollView != null) {
            this.mScrollView.setClipToPadding(true);
        }
        if (!z) {
            ViewGroup viewGroup2 = this.mListView != null ? this.mListView : this.mScrollView;
            if (viewGroup2 != null) {
                setScrollIndicators(resolvePanel2, viewGroup2, (z2 ? 1 : 0) | (z3 ? 2 : 0), 3);
            }
        }
        ListView listView = this.mListView;
        if (listView != null && this.mAdapter != null) {
            listView.setAdapter(this.mAdapter);
            int i = this.mCheckedItem;
            if (i > -1) {
                listView.setItemChecked(i, true);
                listView.setSelection(i);
            }
        }
    }

    private void setScrollIndicators(ViewGroup viewGroup, View view, int i, int i2) {
        AbsListView.OnScrollListener onScrollListener;
        Runnable runnable;
        NestedScrollView.OnScrollChangeListener onScrollChangeListener;
        Runnable runnable2;
        ViewGroup viewGroup2 = viewGroup;
        View view2 = view;
        int i3 = i;
        int i4 = i2;
        View findViewById = this.mWindow.findViewById(R.id.scrollIndicatorUp);
        View findViewById2 = this.mWindow.findViewById(R.id.scrollIndicatorDown);
        if (Build.VERSION.SDK_INT >= 23) {
            ViewCompat.setScrollIndicators(view2, i3, i4);
            if (findViewById != null) {
                viewGroup2.removeView(findViewById);
            }
            if (findViewById2 != null) {
                viewGroup2.removeView(findViewById2);
                return;
            }
            return;
        }
        if (findViewById != null && (i3 & 1) == 0) {
            viewGroup2.removeView(findViewById);
            findViewById = null;
        }
        if (findViewById2 != null && (i3 & 2) == 0) {
            viewGroup2.removeView(findViewById2);
            findViewById2 = null;
        }
        if (findViewById != null || findViewById2 != null) {
            View view3 = findViewById;
            View view4 = findViewById2;
            if (this.mMessage != null) {
                final View view5 = view3;
                final View view6 = view4;
                new NestedScrollView.OnScrollChangeListener() {
                    public void onScrollChange(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
                        AlertController.manageScrollIndicators(nestedScrollView, view5, view6);
                    }
                };
                this.mScrollView.setOnScrollChangeListener(onScrollChangeListener);
                final View view7 = view3;
                final View view8 = view4;
                new Runnable() {
                    public void run() {
                        AlertController.manageScrollIndicators(AlertController.this.mScrollView, view7, view8);
                    }
                };
                boolean post = this.mScrollView.post(runnable2);
            } else if (this.mListView != null) {
                final View view9 = view3;
                final View view10 = view4;
                new AbsListView.OnScrollListener() {
                    public void onScrollStateChanged(AbsListView absListView, int i) {
                    }

                    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                        AlertController.manageScrollIndicators(absListView, view9, view10);
                    }
                };
                this.mListView.setOnScrollListener(onScrollListener);
                final View view11 = view3;
                final View view12 = view4;
                new Runnable() {
                    public void run() {
                        AlertController.manageScrollIndicators(AlertController.this.mListView, view11, view12);
                    }
                };
                boolean post2 = this.mListView.post(runnable);
            } else {
                if (view3 != null) {
                    viewGroup2.removeView(view3);
                }
                if (view4 != null) {
                    viewGroup2.removeView(view4);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void setupCustomContent(ViewGroup viewGroup) {
        View view;
        ViewGroup.LayoutParams layoutParams;
        ViewGroup viewGroup2 = viewGroup;
        if (this.mView != null) {
            view = this.mView;
        } else if (this.mViewLayoutResId != 0) {
            view = LayoutInflater.from(this.mContext).inflate(this.mViewLayoutResId, viewGroup2, false);
        } else {
            view = null;
        }
        boolean z = view != null;
        if (!z || !canTextInput(view)) {
            this.mWindow.setFlags(131072, 131072);
        }
        if (z) {
            FrameLayout frameLayout = (FrameLayout) this.mWindow.findViewById(R.id.custom);
            new ViewGroup.LayoutParams(-1, -1);
            frameLayout.addView(view, layoutParams);
            if (this.mViewSpacingSpecified) {
                frameLayout.setPadding(this.mViewSpacingLeft, this.mViewSpacingTop, this.mViewSpacingRight, this.mViewSpacingBottom);
            }
            if (this.mListView != null) {
                ((LinearLayout.LayoutParams) viewGroup2.getLayoutParams()).weight = 0.0f;
                return;
            }
            return;
        }
        viewGroup2.setVisibility(8);
    }

    private void setupTitle(ViewGroup viewGroup) {
        ViewGroup.LayoutParams layoutParams;
        ViewGroup viewGroup2 = viewGroup;
        if (this.mCustomTitleView != null) {
            new ViewGroup.LayoutParams(-1, -2);
            viewGroup2.addView(this.mCustomTitleView, 0, layoutParams);
            this.mWindow.findViewById(R.id.title_template).setVisibility(8);
            return;
        }
        this.mIconView = (ImageView) this.mWindow.findViewById(16908294);
        if (!TextUtils.isEmpty(this.mTitle)) {
            this.mTitleView = (TextView) this.mWindow.findViewById(R.id.alertTitle);
            this.mTitleView.setText(this.mTitle);
            if (this.mIconId != 0) {
                this.mIconView.setImageResource(this.mIconId);
            } else if (this.mIcon != null) {
                this.mIconView.setImageDrawable(this.mIcon);
            } else {
                this.mTitleView.setPadding(this.mIconView.getPaddingLeft(), this.mIconView.getPaddingTop(), this.mIconView.getPaddingRight(), this.mIconView.getPaddingBottom());
                this.mIconView.setVisibility(8);
            }
        } else {
            this.mWindow.findViewById(R.id.title_template).setVisibility(8);
            this.mIconView.setVisibility(8);
            viewGroup2.setVisibility(8);
        }
    }

    private void setupContent(ViewGroup viewGroup) {
        ViewGroup.LayoutParams layoutParams;
        ViewGroup viewGroup2 = viewGroup;
        this.mScrollView = (NestedScrollView) this.mWindow.findViewById(R.id.scrollView);
        this.mScrollView.setFocusable(false);
        this.mScrollView.setNestedScrollingEnabled(false);
        this.mMessageView = (TextView) viewGroup2.findViewById(16908299);
        if (this.mMessageView != null) {
            if (this.mMessage != null) {
                this.mMessageView.setText(this.mMessage);
                return;
            }
            this.mMessageView.setVisibility(8);
            this.mScrollView.removeView(this.mMessageView);
            if (this.mListView != null) {
                ViewGroup viewGroup3 = (ViewGroup) this.mScrollView.getParent();
                int indexOfChild = viewGroup3.indexOfChild(this.mScrollView);
                viewGroup3.removeViewAt(indexOfChild);
                new ViewGroup.LayoutParams(-1, -1);
                viewGroup3.addView(this.mListView, indexOfChild, layoutParams);
                return;
            }
            viewGroup2.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public static void manageScrollIndicators(View view, View view2, View view3) {
        View view4 = view;
        View view5 = view2;
        View view6 = view3;
        if (view5 != null) {
            view5.setVisibility(ViewCompat.canScrollVertically(view4, -1) ? 0 : 4);
        }
        if (view6 != null) {
            view6.setVisibility(ViewCompat.canScrollVertically(view4, 1) ? 0 : 4);
        }
    }

    private void setupButtons(ViewGroup viewGroup) {
        ViewGroup viewGroup2 = viewGroup;
        boolean z = false;
        this.mButtonPositive = (Button) viewGroup2.findViewById(16908313);
        this.mButtonPositive.setOnClickListener(this.mButtonHandler);
        if (TextUtils.isEmpty(this.mButtonPositiveText)) {
            this.mButtonPositive.setVisibility(8);
        } else {
            this.mButtonPositive.setText(this.mButtonPositiveText);
            this.mButtonPositive.setVisibility(0);
            z = false | true;
        }
        this.mButtonNegative = (Button) viewGroup2.findViewById(16908314);
        this.mButtonNegative.setOnClickListener(this.mButtonHandler);
        if (TextUtils.isEmpty(this.mButtonNegativeText)) {
            this.mButtonNegative.setVisibility(8);
        } else {
            this.mButtonNegative.setText(this.mButtonNegativeText);
            this.mButtonNegative.setVisibility(0);
            z |= true;
        }
        this.mButtonNeutral = (Button) viewGroup2.findViewById(16908315);
        this.mButtonNeutral.setOnClickListener(this.mButtonHandler);
        if (TextUtils.isEmpty(this.mButtonNeutralText)) {
            this.mButtonNeutral.setVisibility(8);
        } else {
            this.mButtonNeutral.setText(this.mButtonNeutralText);
            this.mButtonNeutral.setVisibility(0);
            z |= true;
        }
        if (!(z)) {
            viewGroup2.setVisibility(8);
        }
    }

    public static class AlertParams {
        public ListAdapter mAdapter;
        public boolean mCancelable;
        public int mCheckedItem = -1;
        public boolean[] mCheckedItems;
        public final Context mContext;
        public Cursor mCursor;
        public View mCustomTitleView;
        public boolean mForceInverseBackground;
        public Drawable mIcon;
        public int mIconAttrId = 0;
        public int mIconId = 0;
        public final LayoutInflater mInflater;
        public String mIsCheckedColumn;
        public boolean mIsMultiChoice;
        public boolean mIsSingleChoice;
        public CharSequence[] mItems;
        public String mLabelColumn;
        public CharSequence mMessage;
        public DialogInterface.OnClickListener mNegativeButtonListener;
        public CharSequence mNegativeButtonText;
        public DialogInterface.OnClickListener mNeutralButtonListener;
        public CharSequence mNeutralButtonText;
        public DialogInterface.OnCancelListener mOnCancelListener;
        public DialogInterface.OnMultiChoiceClickListener mOnCheckboxClickListener;
        public DialogInterface.OnClickListener mOnClickListener;
        public DialogInterface.OnDismissListener mOnDismissListener;
        public AdapterView.OnItemSelectedListener mOnItemSelectedListener;
        public DialogInterface.OnKeyListener mOnKeyListener;
        public OnPrepareListViewListener mOnPrepareListViewListener;
        public DialogInterface.OnClickListener mPositiveButtonListener;
        public CharSequence mPositiveButtonText;
        public boolean mRecycleOnMeasure = true;
        public CharSequence mTitle;
        public View mView;
        public int mViewLayoutResId;
        public int mViewSpacingBottom;
        public int mViewSpacingLeft;
        public int mViewSpacingRight;
        public boolean mViewSpacingSpecified = false;
        public int mViewSpacingTop;

        public interface OnPrepareListViewListener {
            void onPrepareListView(ListView listView);
        }

        public AlertParams(Context context) {
            Context context2 = context;
            this.mContext = context2;
            this.mCancelable = true;
            this.mInflater = (LayoutInflater) context2.getSystemService("layout_inflater");
        }

        public void apply(AlertController alertController) {
            AlertController alertController2 = alertController;
            if (this.mCustomTitleView != null) {
                alertController2.setCustomTitle(this.mCustomTitleView);
            } else {
                if (this.mTitle != null) {
                    alertController2.setTitle(this.mTitle);
                }
                if (this.mIcon != null) {
                    alertController2.setIcon(this.mIcon);
                }
                if (this.mIconId != 0) {
                    alertController2.setIcon(this.mIconId);
                }
                if (this.mIconAttrId != 0) {
                    alertController2.setIcon(alertController2.getIconAttributeResId(this.mIconAttrId));
                }
            }
            if (this.mMessage != null) {
                alertController2.setMessage(this.mMessage);
            }
            if (this.mPositiveButtonText != null) {
                alertController2.setButton(-1, this.mPositiveButtonText, this.mPositiveButtonListener, null);
            }
            if (this.mNegativeButtonText != null) {
                alertController2.setButton(-2, this.mNegativeButtonText, this.mNegativeButtonListener, null);
            }
            if (this.mNeutralButtonText != null) {
                alertController2.setButton(-3, this.mNeutralButtonText, this.mNeutralButtonListener, null);
            }
            if (!(this.mItems == null && this.mCursor == null && this.mAdapter == null)) {
                createListView(alertController2);
            }
            if (this.mView != null) {
                if (this.mViewSpacingSpecified) {
                    alertController2.setView(this.mView, this.mViewSpacingLeft, this.mViewSpacingTop, this.mViewSpacingRight, this.mViewSpacingBottom);
                } else {
                    alertController2.setView(this.mView);
                }
            } else if (this.mViewLayoutResId != 0) {
                alertController2.setView(this.mViewLayoutResId);
            }
        }

        private void createListView(AlertController alertController) {
            int access$1400;
            ListAdapter listAdapter;
            ListAdapter listAdapter2;
            ListAdapter listAdapter3;
            AdapterView.OnItemClickListener onItemClickListener;
            AdapterView.OnItemClickListener onItemClickListener2;
            ListAdapter listAdapter4;
            ListAdapter listAdapter5;
            AlertController alertController2 = alertController;
            ListView listView = (ListView) this.mInflater.inflate(alertController2.mListLayout, (ViewGroup) null);
            if (!this.mIsMultiChoice) {
                if (this.mIsSingleChoice) {
                    access$1400 = alertController2.mSingleChoiceItemLayout;
                } else {
                    access$1400 = alertController2.mListItemLayout;
                }
                if (this.mCursor != null) {
                    ListAdapter listAdapter6 = listAdapter3;
                    String[] strArr = new String[1];
                    strArr[0] = this.mLabelColumn;
                    new SimpleCursorAdapter(this.mContext, access$1400, this.mCursor, strArr, new int[]{16908308});
                    listAdapter2 = listAdapter6;
                } else if (this.mAdapter != null) {
                    listAdapter2 = this.mAdapter;
                } else {
                    new CheckedItemAdapter(this.mContext, access$1400, 16908308, this.mItems);
                    listAdapter2 = listAdapter;
                }
            } else if (this.mCursor == null) {
                final ListView listView2 = listView;
                new ArrayAdapter<CharSequence>(this.mContext, alertController2.mMultiChoiceItemLayout, 16908308, this.mItems) {
                    public View getView(int i, View view, ViewGroup viewGroup) {
                        int i2 = i;
                        View view2 = super.getView(i2, view, viewGroup);
                        if (AlertParams.this.mCheckedItems != null && AlertParams.this.mCheckedItems[i2]) {
                            listView2.setItemChecked(i2, true);
                        }
                        return view2;
                    }
                };
                listAdapter2 = listAdapter5;
            } else {
                final ListView listView3 = listView;
                final AlertController alertController3 = alertController2;
                new CursorAdapter(this.mContext, this.mCursor, false) {
                    private final int mIsCheckedIndex;
                    private final int mLabelIndex;

                    {
                        Cursor cursor = getCursor();
                        this.mLabelIndex = cursor.getColumnIndexOrThrow(AlertParams.this.mLabelColumn);
                        this.mIsCheckedIndex = cursor.getColumnIndexOrThrow(AlertParams.this.mIsCheckedColumn);
                    }

                    public void bindView(View view, Context context, Cursor cursor) {
                        Cursor cursor2 = cursor;
                        ((CheckedTextView) view.findViewById(16908308)).setText(cursor2.getString(this.mLabelIndex));
                        listView3.setItemChecked(cursor2.getPosition(), cursor2.getInt(this.mIsCheckedIndex) == 1);
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
                     arg types: [int, android.view.ViewGroup, int]
                     candidates:
                      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
                      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
                    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
                        return AlertParams.this.mInflater.inflate(alertController3.mMultiChoiceItemLayout, viewGroup, false);
                    }
                };
                listAdapter2 = listAdapter4;
            }
            if (this.mOnPrepareListViewListener != null) {
                this.mOnPrepareListViewListener.onPrepareListView(listView);
            }
            ListAdapter access$1502 = AlertController.access$1502(alertController2, listAdapter2);
            int access$1602 = AlertController.access$1602(alertController2, this.mCheckedItem);
            if (this.mOnClickListener != null) {
                final AlertController alertController4 = alertController2;
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        AlertParams.this.mOnClickListener.onClick(alertController4.mDialog, i);
                        if (!AlertParams.this.mIsSingleChoice) {
                            alertController4.mDialog.dismiss();
                        }
                    }
                };
                listView.setOnItemClickListener(onItemClickListener2);
            } else if (this.mOnCheckboxClickListener != null) {
                final ListView listView4 = listView;
                final AlertController alertController5 = alertController2;
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        int i2 = i;
                        if (AlertParams.this.mCheckedItems != null) {
                            AlertParams.this.mCheckedItems[i2] = listView4.isItemChecked(i2);
                        }
                        AlertParams.this.mOnCheckboxClickListener.onClick(alertController5.mDialog, i2, listView4.isItemChecked(i2));
                    }
                };
                listView.setOnItemClickListener(onItemClickListener);
            }
            if (this.mOnItemSelectedListener != null) {
                listView.setOnItemSelectedListener(this.mOnItemSelectedListener);
            }
            if (this.mIsSingleChoice) {
                listView.setChoiceMode(1);
            } else if (this.mIsMultiChoice) {
                listView.setChoiceMode(2);
            }
            ListView access$1002 = AlertController.access$1002(alertController2, listView);
        }
    }

    private static class CheckedItemAdapter extends ArrayAdapter<CharSequence> {
        public CheckedItemAdapter(Context context, int i, int i2, CharSequence[] charSequenceArr) {
            super(context, i, i2, charSequenceArr);
        }

        public boolean hasStableIds() {
            return true;
        }

        public long getItemId(int i) {
            return (long) i;
        }
    }
}
