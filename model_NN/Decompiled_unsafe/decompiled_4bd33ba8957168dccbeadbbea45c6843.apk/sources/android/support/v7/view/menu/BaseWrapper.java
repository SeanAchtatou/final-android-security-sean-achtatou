package android.support.v7.view.menu;

class BaseWrapper<T> {
    final T mWrappedObject;

    BaseWrapper(T t) {
        Throwable th;
        T t2 = t;
        if (null == t2) {
            Throwable th2 = th;
            new IllegalArgumentException("Wrapped Object can not be null.");
            throw th2;
        }
        this.mWrappedObject = t2;
    }

    public T getWrappedObject() {
        return this.mWrappedObject;
    }
}
