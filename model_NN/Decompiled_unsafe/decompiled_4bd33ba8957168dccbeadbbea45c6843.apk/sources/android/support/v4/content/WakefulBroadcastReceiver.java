package android.support.v4.content;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;
import android.util.SparseArray;

public abstract class WakefulBroadcastReceiver extends BroadcastReceiver {
    private static final String EXTRA_WAKE_LOCK_ID = "android.support.content.wakelockid";
    private static final SparseArray<PowerManager.WakeLock> mActiveWakeLocks;
    private static int mNextId = 1;

    static {
        SparseArray<PowerManager.WakeLock> sparseArray;
        new SparseArray<>();
        mActiveWakeLocks = sparseArray;
    }

    public static ComponentName startWakefulService(Context context, Intent intent) {
        StringBuilder sb;
        Context context2 = context;
        Intent intent2 = intent;
        SparseArray<PowerManager.WakeLock> sparseArray = mActiveWakeLocks;
        SparseArray<PowerManager.WakeLock> sparseArray2 = sparseArray;
        synchronized (sparseArray) {
            try {
                int i = mNextId;
                mNextId++;
                if (mNextId <= 0) {
                    mNextId = 1;
                }
                Intent putExtra = intent2.putExtra(EXTRA_WAKE_LOCK_ID, i);
                ComponentName startService = context2.startService(intent2);
                if (startService == null) {
                    return null;
                }
                new StringBuilder();
                PowerManager.WakeLock newWakeLock = ((PowerManager) context2.getSystemService("power")).newWakeLock(1, sb.append("wake:").append(startService.flattenToShortString()).toString());
                newWakeLock.setReferenceCounted(false);
                newWakeLock.acquire(60000);
                mActiveWakeLocks.put(i, newWakeLock);
                ComponentName componentName = startService;
                return componentName;
            } catch (Throwable th) {
                Throwable th2 = th;
                SparseArray<PowerManager.WakeLock> sparseArray3 = sparseArray2;
                throw th2;
            }
        }
    }

    public static boolean completeWakefulIntent(Intent intent) {
        StringBuilder sb;
        int intExtra = intent.getIntExtra(EXTRA_WAKE_LOCK_ID, 0);
        if (intExtra == 0) {
            return false;
        }
        SparseArray<PowerManager.WakeLock> sparseArray = mActiveWakeLocks;
        SparseArray<PowerManager.WakeLock> sparseArray2 = sparseArray;
        synchronized (sparseArray) {
            try {
                PowerManager.WakeLock wakeLock = mActiveWakeLocks.get(intExtra);
                if (wakeLock != null) {
                    wakeLock.release();
                    mActiveWakeLocks.remove(intExtra);
                    return true;
                }
                new StringBuilder();
                int w = Log.w("WakefulBroadcastReceiver", sb.append("No active wake lock id #").append(intExtra).toString());
                return true;
            } catch (Throwable th) {
                Throwable th2 = th;
                SparseArray<PowerManager.WakeLock> sparseArray3 = sparseArray2;
                throw th2;
            }
        }
    }
}
