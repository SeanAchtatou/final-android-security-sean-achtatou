package android.support.v4.content;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

class EditorCompatGingerbread {
    EditorCompatGingerbread() {
    }

    public static void apply(@NonNull SharedPreferences.Editor editor) {
        SharedPreferences.Editor editor2 = editor;
        try {
            editor2.apply();
        } catch (AbstractMethodError e) {
            boolean commit = editor2.commit();
        }
    }
}
