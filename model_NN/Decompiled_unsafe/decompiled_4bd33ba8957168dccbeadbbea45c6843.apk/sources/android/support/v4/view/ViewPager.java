package android.support.v4.view;

import android.content.Context;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.CallSuper;
import android.support.annotation.DrawableRes;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityRecordCompat;
import android.support.v4.widget.EdgeEffectCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ViewPager extends ViewGroup {
    private static final int CLOSE_ENOUGH = 2;
    private static final Comparator<ItemInfo> COMPARATOR;
    private static final boolean DEBUG = false;
    private static final int DEFAULT_GUTTER_SIZE = 16;
    private static final int DEFAULT_OFFSCREEN_PAGES = 1;
    private static final int DRAW_ORDER_DEFAULT = 0;
    private static final int DRAW_ORDER_FORWARD = 1;
    private static final int DRAW_ORDER_REVERSE = 2;
    private static final int INVALID_POINTER = -1;
    /* access modifiers changed from: private */
    public static final int[] LAYOUT_ATTRS = {16842931};
    private static final int MAX_SETTLE_DURATION = 600;
    private static final int MIN_DISTANCE_FOR_FLING = 25;
    private static final int MIN_FLING_VELOCITY = 400;
    public static final int SCROLL_STATE_DRAGGING = 1;
    public static final int SCROLL_STATE_IDLE = 0;
    public static final int SCROLL_STATE_SETTLING = 2;
    private static final String TAG = "ViewPager";
    private static final boolean USE_CACHE = false;
    private static final Interpolator sInterpolator;
    private static final ViewPositionComparator sPositionComparator;
    private int mActivePointerId = -1;
    /* access modifiers changed from: private */
    public PagerAdapter mAdapter;
    private OnAdapterChangeListener mAdapterChangeListener;
    private int mBottomPageBounds;
    private boolean mCalledSuper;
    private int mChildHeightMeasureSpec;
    private int mChildWidthMeasureSpec;
    private int mCloseEnough;
    /* access modifiers changed from: private */
    public int mCurItem;
    private int mDecorChildCount;
    private int mDefaultGutterSize;
    private int mDrawingOrder;
    private ArrayList<View> mDrawingOrderedChildren;
    private final Runnable mEndScrollRunnable;
    private int mExpectedAdapterCount;
    private long mFakeDragBeginTime;
    private boolean mFakeDragging;
    private boolean mFirstLayout = true;
    private float mFirstOffset = -3.4028235E38f;
    private int mFlingDistance;
    private int mGutterSize;
    private boolean mInLayout;
    private float mInitialMotionX;
    private float mInitialMotionY;
    private OnPageChangeListener mInternalPageChangeListener;
    private boolean mIsBeingDragged;
    private boolean mIsUnableToDrag;
    private final ArrayList<ItemInfo> mItems;
    private float mLastMotionX;
    private float mLastMotionY;
    private float mLastOffset = Float.MAX_VALUE;
    private EdgeEffectCompat mLeftEdge;
    private Drawable mMarginDrawable;
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    private boolean mNeedCalculatePageOffsets = false;
    private PagerObserver mObserver;
    private int mOffscreenPageLimit = 1;
    private OnPageChangeListener mOnPageChangeListener;
    private List<OnPageChangeListener> mOnPageChangeListeners;
    private int mPageMargin;
    private PageTransformer mPageTransformer;
    private boolean mPopulatePending;
    private Parcelable mRestoredAdapterState = null;
    private ClassLoader mRestoredClassLoader = null;
    private int mRestoredCurItem = -1;
    private EdgeEffectCompat mRightEdge;
    private int mScrollState;
    private Scroller mScroller;
    private boolean mScrollingCacheEnabled;
    private Method mSetChildrenDrawingOrderEnabled;
    private final ItemInfo mTempItem;
    private final Rect mTempRect;
    private int mTopPageBounds;
    private int mTouchSlop;
    private VelocityTracker mVelocityTracker;

    interface Decor {
    }

    interface OnAdapterChangeListener {
        void onAdapterChanged(PagerAdapter pagerAdapter, PagerAdapter pagerAdapter2);
    }

    public interface OnPageChangeListener {
        void onPageScrollStateChanged(int i);

        void onPageScrolled(int i, float f, int i2);

        void onPageSelected(int i);
    }

    public interface PageTransformer {
        void transformPage(View view, float f);
    }

    static {
        Comparator<ItemInfo> comparator;
        Interpolator interpolator;
        ViewPositionComparator viewPositionComparator;
        new Comparator<ItemInfo>() {
            public int compare(ItemInfo itemInfo, ItemInfo itemInfo2) {
                return itemInfo.position - itemInfo2.position;
            }
        };
        COMPARATOR = comparator;
        new Interpolator() {
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (f2 * f2 * f2 * f2 * f2) + 1.0f;
            }
        };
        sInterpolator = interpolator;
        new ViewPositionComparator();
        sPositionComparator = viewPositionComparator;
    }

    static class ItemInfo {
        Object object;
        float offset;
        int position;
        boolean scrolling;
        float widthFactor;

        ItemInfo() {
        }
    }

    public static class SimpleOnPageChangeListener implements OnPageChangeListener {
        public void onPageScrolled(int i, float f, int i2) {
        }

        public void onPageSelected(int i) {
        }

        public void onPageScrollStateChanged(int i) {
        }
    }

    public ViewPager(Context context) {
        super(context);
        ArrayList<ItemInfo> arrayList;
        ItemInfo itemInfo;
        Rect rect;
        Runnable runnable;
        new ArrayList<>();
        this.mItems = arrayList;
        new ItemInfo();
        this.mTempItem = itemInfo;
        new Rect();
        this.mTempRect = rect;
        new Runnable() {
            public void run() {
                ViewPager.this.setScrollState(0);
                ViewPager.this.populate();
            }
        };
        this.mEndScrollRunnable = runnable;
        this.mScrollState = 0;
        initViewPager();
    }

    public ViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ArrayList<ItemInfo> arrayList;
        ItemInfo itemInfo;
        Rect rect;
        Runnable runnable;
        new ArrayList<>();
        this.mItems = arrayList;
        new ItemInfo();
        this.mTempItem = itemInfo;
        new Rect();
        this.mTempRect = rect;
        new Runnable() {
            public void run() {
                ViewPager.this.setScrollState(0);
                ViewPager.this.populate();
            }
        };
        this.mEndScrollRunnable = runnable;
        this.mScrollState = 0;
        initViewPager();
    }

    /* access modifiers changed from: package-private */
    public void initViewPager() {
        Scroller scroller;
        EdgeEffectCompat edgeEffectCompat;
        EdgeEffectCompat edgeEffectCompat2;
        AccessibilityDelegateCompat accessibilityDelegateCompat;
        setWillNotDraw(false);
        setDescendantFocusability(262144);
        setFocusable(true);
        Context context = getContext();
        new Scroller(context, sInterpolator);
        this.mScroller = scroller;
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f = context.getResources().getDisplayMetrics().density;
        this.mTouchSlop = ViewConfigurationCompat.getScaledPagingTouchSlop(viewConfiguration);
        this.mMinimumVelocity = (int) (400.0f * f);
        this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
        new EdgeEffectCompat(context);
        this.mLeftEdge = edgeEffectCompat;
        new EdgeEffectCompat(context);
        this.mRightEdge = edgeEffectCompat2;
        this.mFlingDistance = (int) (25.0f * f);
        this.mCloseEnough = (int) (2.0f * f);
        this.mDefaultGutterSize = (int) (16.0f * f);
        new MyAccessibilityDelegate();
        ViewCompat.setAccessibilityDelegate(this, accessibilityDelegateCompat);
        if (ViewCompat.getImportantForAccessibility(this) == 0) {
            ViewCompat.setImportantForAccessibility(this, 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        boolean removeCallbacks = removeCallbacks(this.mEndScrollRunnable);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: private */
    public void setScrollState(int i) {
        int i2 = i;
        if (this.mScrollState != i2) {
            this.mScrollState = i2;
            if (this.mPageTransformer != null) {
                enableLayers(i2 != 0);
            }
            dispatchOnScrollStateChanged(i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.PagerAdapter.destroyItem(android.view.View, int, java.lang.Object):void
      android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void */
    public void setAdapter(PagerAdapter pagerAdapter) {
        PagerObserver pagerObserver;
        PagerAdapter pagerAdapter2 = pagerAdapter;
        if (this.mAdapter != null) {
            this.mAdapter.unregisterDataSetObserver(this.mObserver);
            this.mAdapter.startUpdate((ViewGroup) this);
            for (int i = 0; i < this.mItems.size(); i++) {
                ItemInfo itemInfo = this.mItems.get(i);
                this.mAdapter.destroyItem((ViewGroup) this, itemInfo.position, itemInfo.object);
            }
            this.mAdapter.finishUpdate((ViewGroup) this);
            this.mItems.clear();
            removeNonDecorViews();
            this.mCurItem = 0;
            scrollTo(0, 0);
        }
        PagerAdapter pagerAdapter3 = this.mAdapter;
        this.mAdapter = pagerAdapter2;
        this.mExpectedAdapterCount = 0;
        if (this.mAdapter != null) {
            if (this.mObserver == null) {
                new PagerObserver();
                this.mObserver = pagerObserver;
            }
            this.mAdapter.registerDataSetObserver(this.mObserver);
            this.mPopulatePending = false;
            boolean z = this.mFirstLayout;
            this.mFirstLayout = true;
            this.mExpectedAdapterCount = this.mAdapter.getCount();
            if (this.mRestoredCurItem >= 0) {
                this.mAdapter.restoreState(this.mRestoredAdapterState, this.mRestoredClassLoader);
                setCurrentItemInternal(this.mRestoredCurItem, false, true);
                this.mRestoredCurItem = -1;
                this.mRestoredAdapterState = null;
                this.mRestoredClassLoader = null;
            } else if (!z) {
                populate();
            } else {
                requestLayout();
            }
        }
        if (this.mAdapterChangeListener != null && pagerAdapter3 != pagerAdapter2) {
            this.mAdapterChangeListener.onAdapterChanged(pagerAdapter3, pagerAdapter2);
        }
    }

    private void removeNonDecorViews() {
        int i = 0;
        while (i < getChildCount()) {
            if (!((LayoutParams) getChildAt(i).getLayoutParams()).isDecor) {
                removeViewAt(i);
                i--;
            }
            i++;
        }
    }

    public PagerAdapter getAdapter() {
        return this.mAdapter;
    }

    /* access modifiers changed from: package-private */
    public void setOnAdapterChangeListener(OnAdapterChangeListener onAdapterChangeListener) {
        this.mAdapterChangeListener = onAdapterChangeListener;
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    public void setCurrentItem(int i) {
        this.mPopulatePending = false;
        setCurrentItemInternal(i, !this.mFirstLayout, false);
    }

    public void setCurrentItem(int i, boolean z) {
        this.mPopulatePending = false;
        setCurrentItemInternal(i, z, false);
    }

    public int getCurrentItem() {
        return this.mCurItem;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentItemInternal(int i, boolean z, boolean z2) {
        setCurrentItemInternal(i, z, z2, 0);
    }

    /* access modifiers changed from: package-private */
    public void setCurrentItemInternal(int i, boolean z, boolean z2, int i2) {
        int i3 = i;
        boolean z3 = z;
        boolean z4 = z2;
        int i4 = i2;
        if (this.mAdapter == null || this.mAdapter.getCount() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z4 || this.mCurItem != i3 || this.mItems.size() == 0) {
            if (i3 < 0) {
                i3 = 0;
            } else if (i3 >= this.mAdapter.getCount()) {
                i3 = this.mAdapter.getCount() - 1;
            }
            int i5 = this.mOffscreenPageLimit;
            if (i3 > this.mCurItem + i5 || i3 < this.mCurItem - i5) {
                for (int i6 = 0; i6 < this.mItems.size(); i6++) {
                    this.mItems.get(i6).scrolling = true;
                }
            }
            boolean z5 = this.mCurItem != i3;
            if (this.mFirstLayout) {
                this.mCurItem = i3;
                if (z5) {
                    dispatchOnPageSelected(i3);
                }
                requestLayout();
                return;
            }
            populate(i3);
            scrollToItem(i3, z3, i4, z5);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    private void scrollToItem(int i, boolean z, int i2, boolean z2) {
        int i3 = i;
        boolean z3 = z;
        int i4 = i2;
        boolean z4 = z2;
        ItemInfo infoForPosition = infoForPosition(i3);
        int i5 = 0;
        if (infoForPosition != null) {
            i5 = (int) (((float) getClientWidth()) * Math.max(this.mFirstOffset, Math.min(infoForPosition.offset, this.mLastOffset)));
        }
        if (z3) {
            smoothScrollTo(i5, 0, i4);
            if (z4) {
                dispatchOnPageSelected(i3);
                return;
            }
            return;
        }
        if (z4) {
            dispatchOnPageSelected(i3);
        }
        completeScroll(false);
        scrollTo(i5, 0);
        boolean pageScrolled = pageScrolled(i5);
    }

    @Deprecated
    public void setOnPageChangeListener(OnPageChangeListener onPageChangeListener) {
        this.mOnPageChangeListener = onPageChangeListener;
    }

    public void addOnPageChangeListener(OnPageChangeListener onPageChangeListener) {
        List<OnPageChangeListener> list;
        OnPageChangeListener onPageChangeListener2 = onPageChangeListener;
        if (this.mOnPageChangeListeners == null) {
            new ArrayList();
            this.mOnPageChangeListeners = list;
        }
        boolean add = this.mOnPageChangeListeners.add(onPageChangeListener2);
    }

    public void removeOnPageChangeListener(OnPageChangeListener onPageChangeListener) {
        OnPageChangeListener onPageChangeListener2 = onPageChangeListener;
        if (this.mOnPageChangeListeners != null) {
            boolean remove = this.mOnPageChangeListeners.remove(onPageChangeListener2);
        }
    }

    public void clearOnPageChangeListeners() {
        if (this.mOnPageChangeListeners != null) {
            this.mOnPageChangeListeners.clear();
        }
    }

    public void setPageTransformer(boolean z, PageTransformer pageTransformer) {
        boolean z2 = z;
        PageTransformer pageTransformer2 = pageTransformer;
        if (Build.VERSION.SDK_INT >= 11) {
            boolean z3 = pageTransformer2 != null;
            boolean z4 = z3 != (this.mPageTransformer != null);
            this.mPageTransformer = pageTransformer2;
            setChildrenDrawingOrderEnabledCompat(z3);
            if (z3) {
                this.mDrawingOrder = z2 ? 2 : 1;
            } else {
                this.mDrawingOrder = 0;
            }
            if (z4) {
                populate();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setChildrenDrawingOrderEnabledCompat(boolean z) {
        boolean z2 = z;
        if (Build.VERSION.SDK_INT >= 7) {
            if (this.mSetChildrenDrawingOrderEnabled == null) {
                Class<ViewGroup> cls = ViewGroup.class;
                try {
                    this.mSetChildrenDrawingOrderEnabled = cls.getDeclaredMethod("setChildrenDrawingOrderEnabled", Boolean.TYPE);
                } catch (NoSuchMethodException e) {
                    int e2 = Log.e(TAG, "Can't find setChildrenDrawingOrderEnabled", e);
                }
            }
            try {
                Object invoke = this.mSetChildrenDrawingOrderEnabled.invoke(this, Boolean.valueOf(z2));
            } catch (Exception e3) {
                int e4 = Log.e(TAG, "Error changing children drawing order", e3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i, int i2) {
        int i3 = i2;
        return ((LayoutParams) this.mDrawingOrderedChildren.get(this.mDrawingOrder == 2 ? (i - 1) - i3 : i3).getLayoutParams()).childIndex;
    }

    /* access modifiers changed from: package-private */
    public OnPageChangeListener setInternalPageChangeListener(OnPageChangeListener onPageChangeListener) {
        this.mInternalPageChangeListener = onPageChangeListener;
        return this.mInternalPageChangeListener;
    }

    public int getOffscreenPageLimit() {
        return this.mOffscreenPageLimit;
    }

    public void setOffscreenPageLimit(int i) {
        StringBuilder sb;
        int i2 = i;
        if (i2 < 1) {
            new StringBuilder();
            int w = Log.w(TAG, sb.append("Requested offscreen page limit ").append(i2).append(" too small; defaulting to ").append(1).toString());
            i2 = 1;
        }
        if (i2 != this.mOffscreenPageLimit) {
            this.mOffscreenPageLimit = i2;
            populate();
        }
    }

    public void setPageMargin(int i) {
        int i2 = i;
        int i3 = this.mPageMargin;
        this.mPageMargin = i2;
        int width = getWidth();
        recomputeScrollPosition(width, width, i2, i3);
        requestLayout();
    }

    public int getPageMargin() {
        return this.mPageMargin;
    }

    public void setPageMarginDrawable(Drawable drawable) {
        Drawable drawable2 = drawable;
        this.mMarginDrawable = drawable2;
        if (drawable2 != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable2 == null);
        invalidate();
    }

    public void setPageMarginDrawable(@DrawableRes int i) {
        setPageMarginDrawable(getContext().getResources().getDrawable(i));
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        Drawable drawable2 = drawable;
        return super.verifyDrawable(drawable2) || drawable2 == this.mMarginDrawable;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.mMarginDrawable;
        if (drawable != null && drawable.isStateful()) {
            boolean state = drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    public float distanceInfluenceForSnapDuration(float f) {
        return (float) Math.sin((double) ((float) (((double) (f - 0.5f)) * 0.4712389167638204d)));
    }

    /* access modifiers changed from: package-private */
    public void smoothScrollTo(int i, int i2) {
        smoothScrollTo(i, i2, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void smoothScrollTo(int i, int i2, int i3) {
        int abs;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int i7 = i4 - scrollX;
        int i8 = i5 - scrollY;
        if (i7 == 0 && i8 == 0) {
            completeScroll(false);
            populate();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int clientWidth = getClientWidth();
        int i9 = clientWidth / 2;
        float distanceInfluenceForSnapDuration = ((float) i9) + (((float) i9) * distanceInfluenceForSnapDuration(Math.min(1.0f, (1.0f * ((float) Math.abs(i7))) / ((float) clientWidth))));
        int abs2 = Math.abs(i6);
        if (abs2 > 0) {
            abs = 4 * Math.round(1000.0f * Math.abs(distanceInfluenceForSnapDuration / ((float) abs2)));
        } else {
            abs = (int) (((((float) Math.abs(i7)) / ((((float) clientWidth) * this.mAdapter.getPageWidth(this.mCurItem)) + ((float) this.mPageMargin))) + 1.0f) * 100.0f);
        }
        this.mScroller.startScroll(scrollX, scrollY, i7, i8, Math.min(abs, (int) MAX_SETTLE_DURATION));
        ViewCompat.postInvalidateOnAnimation(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.instantiateItem(android.view.ViewGroup, int):java.lang.Object
     arg types: [android.support.v4.view.ViewPager, int]
     candidates:
      android.support.v4.view.PagerAdapter.instantiateItem(android.view.View, int):java.lang.Object
      android.support.v4.view.PagerAdapter.instantiateItem(android.view.ViewGroup, int):java.lang.Object */
    /* access modifiers changed from: package-private */
    public ItemInfo addNewItem(int i, int i2) {
        ItemInfo itemInfo;
        int i3 = i;
        int i4 = i2;
        new ItemInfo();
        ItemInfo itemInfo2 = itemInfo;
        itemInfo2.position = i3;
        itemInfo2.object = this.mAdapter.instantiateItem((ViewGroup) this, i3);
        itemInfo2.widthFactor = this.mAdapter.getPageWidth(i3);
        if (i4 < 0 || i4 >= this.mItems.size()) {
            boolean add = this.mItems.add(itemInfo2);
        } else {
            this.mItems.add(i4, itemInfo2);
        }
        return itemInfo2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.PagerAdapter.destroyItem(android.view.View, int, java.lang.Object):void
      android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    public void dataSetChanged() {
        int count = this.mAdapter.getCount();
        this.mExpectedAdapterCount = count;
        boolean z = this.mItems.size() < (this.mOffscreenPageLimit * 2) + 1 && this.mItems.size() < count;
        int i = this.mCurItem;
        boolean z2 = false;
        int i2 = 0;
        while (i2 < this.mItems.size()) {
            ItemInfo itemInfo = this.mItems.get(i2);
            int itemPosition = this.mAdapter.getItemPosition(itemInfo.object);
            if (itemPosition != -1) {
                if (itemPosition == -2) {
                    ItemInfo remove = this.mItems.remove(i2);
                    i2--;
                    if (!z2) {
                        this.mAdapter.startUpdate((ViewGroup) this);
                        z2 = true;
                    }
                    this.mAdapter.destroyItem((ViewGroup) this, itemInfo.position, itemInfo.object);
                    z = true;
                    if (this.mCurItem == itemInfo.position) {
                        i = Math.max(0, Math.min(this.mCurItem, count - 1));
                        z = true;
                    }
                } else if (itemInfo.position != itemPosition) {
                    if (itemInfo.position == this.mCurItem) {
                        i = itemPosition;
                    }
                    itemInfo.position = itemPosition;
                    z = true;
                }
            }
            i2++;
        }
        if (z2) {
            this.mAdapter.finishUpdate((ViewGroup) this);
        }
        Collections.sort(this.mItems, COMPARATOR);
        if (z) {
            int childCount = getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                LayoutParams layoutParams = (LayoutParams) getChildAt(i3).getLayoutParams();
                if (!layoutParams.isDecor) {
                    layoutParams.widthFactor = 0.0f;
                }
            }
            setCurrentItemInternal(i, false, true);
            requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void populate() {
        populate(this.mCurItem);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.setPrimaryItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.PagerAdapter.setPrimaryItem(android.view.View, int, java.lang.Object):void
      android.support.v4.view.PagerAdapter.setPrimaryItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.PagerAdapter.destroyItem(android.view.View, int, java.lang.Object):void
      android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    public void populate(int i) {
        ItemInfo infoForChild;
        ItemInfo itemInfo;
        ItemInfo itemInfo2;
        String hexString;
        Throwable th;
        StringBuilder sb;
        int i2 = i;
        ItemInfo itemInfo3 = null;
        int i3 = 2;
        if (this.mCurItem != i2) {
            i3 = this.mCurItem < i2 ? 66 : 17;
            itemInfo3 = infoForPosition(this.mCurItem);
            this.mCurItem = i2;
        }
        if (this.mAdapter == null) {
            sortChildDrawingOrder();
        } else if (this.mPopulatePending) {
            sortChildDrawingOrder();
        } else if (getWindowToken() != null) {
            this.mAdapter.startUpdate((ViewGroup) this);
            int i4 = this.mOffscreenPageLimit;
            int max = Math.max(0, this.mCurItem - i4);
            int count = this.mAdapter.getCount();
            int min = Math.min(count - 1, this.mCurItem + i4);
            if (count != this.mExpectedAdapterCount) {
                try {
                    hexString = getResources().getResourceName(getId());
                } catch (Resources.NotFoundException e) {
                    hexString = Integer.toHexString(getId());
                }
                Throwable th2 = th;
                new StringBuilder();
                new IllegalStateException(sb.append("The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: ").append(this.mExpectedAdapterCount).append(", found: ").append(count).append(" Pager id: ").append(hexString).append(" Pager class: ").append(getClass()).append(" Problematic adapter: ").append(this.mAdapter.getClass()).toString());
                throw th2;
            }
            ItemInfo itemInfo4 = null;
            int i5 = 0;
            while (true) {
                if (i5 >= this.mItems.size()) {
                    break;
                }
                ItemInfo itemInfo5 = this.mItems.get(i5);
                if (itemInfo5.position < this.mCurItem) {
                    i5++;
                } else if (itemInfo5.position == this.mCurItem) {
                    itemInfo4 = itemInfo5;
                }
            }
            if (itemInfo4 == null && count > 0) {
                itemInfo4 = addNewItem(this.mCurItem, i5);
            }
            if (itemInfo4 != null) {
                float f = 0.0f;
                int i6 = i5 - 1;
                ItemInfo itemInfo6 = i6 >= 0 ? this.mItems.get(i6) : null;
                int clientWidth = getClientWidth();
                float paddingLeft = clientWidth <= 0 ? 0.0f : (2.0f - itemInfo4.widthFactor) + (((float) getPaddingLeft()) / ((float) clientWidth));
                for (int i7 = this.mCurItem - 1; i7 >= 0; i7--) {
                    if (f < paddingLeft || i7 >= max) {
                        if (itemInfo6 == null || i7 != itemInfo6.position) {
                            f += addNewItem(i7, i6 + 1).widthFactor;
                            i5++;
                            itemInfo6 = i6 >= 0 ? this.mItems.get(i6) : null;
                        } else {
                            f += itemInfo6.widthFactor;
                            i6--;
                            itemInfo6 = i6 >= 0 ? this.mItems.get(i6) : null;
                        }
                    } else if (itemInfo6 == null) {
                        break;
                    } else if (i7 == itemInfo6.position && !itemInfo6.scrolling) {
                        ItemInfo remove = this.mItems.remove(i6);
                        this.mAdapter.destroyItem((ViewGroup) this, i7, itemInfo6.object);
                        i6--;
                        i5--;
                        if (i6 >= 0) {
                            itemInfo2 = this.mItems.get(i6);
                        } else {
                            itemInfo2 = null;
                        }
                        itemInfo6 = itemInfo2;
                    }
                }
                float f2 = itemInfo4.widthFactor;
                int i8 = i5 + 1;
                if (f2 < 2.0f) {
                    ItemInfo itemInfo7 = i8 < this.mItems.size() ? this.mItems.get(i8) : null;
                    float paddingRight = clientWidth <= 0 ? 0.0f : (((float) getPaddingRight()) / ((float) clientWidth)) + 2.0f;
                    for (int i9 = this.mCurItem + 1; i9 < count; i9++) {
                        if (f2 < paddingRight || i9 <= min) {
                            if (itemInfo7 == null || i9 != itemInfo7.position) {
                                ItemInfo addNewItem = addNewItem(i9, i8);
                                i8++;
                                f2 += addNewItem.widthFactor;
                                itemInfo7 = i8 < this.mItems.size() ? this.mItems.get(i8) : null;
                            } else {
                                f2 += itemInfo7.widthFactor;
                                i8++;
                                itemInfo7 = i8 < this.mItems.size() ? this.mItems.get(i8) : null;
                            }
                        } else if (itemInfo7 == null) {
                            break;
                        } else if (i9 == itemInfo7.position && !itemInfo7.scrolling) {
                            ItemInfo remove2 = this.mItems.remove(i8);
                            this.mAdapter.destroyItem((ViewGroup) this, i9, itemInfo7.object);
                            if (i8 < this.mItems.size()) {
                                itemInfo = this.mItems.get(i8);
                            } else {
                                itemInfo = null;
                            }
                            itemInfo7 = itemInfo;
                        }
                    }
                }
                calculatePageOffsets(itemInfo4, i5, itemInfo3);
            }
            this.mAdapter.setPrimaryItem((ViewGroup) this, this.mCurItem, itemInfo4 != null ? itemInfo4.object : null);
            this.mAdapter.finishUpdate((ViewGroup) this);
            int childCount = getChildCount();
            for (int i10 = 0; i10 < childCount; i10++) {
                View childAt = getChildAt(i10);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                layoutParams.childIndex = i10;
                if (!layoutParams.isDecor && layoutParams.widthFactor == 0.0f && (infoForChild = infoForChild(childAt)) != null) {
                    layoutParams.widthFactor = infoForChild.widthFactor;
                    layoutParams.position = infoForChild.position;
                }
            }
            sortChildDrawingOrder();
            if (hasFocus()) {
                View findFocus = findFocus();
                ItemInfo infoForAnyChild = findFocus != null ? infoForAnyChild(findFocus) : null;
                if (infoForAnyChild == null || infoForAnyChild.position != this.mCurItem) {
                    int i11 = 0;
                    while (i11 < getChildCount()) {
                        View childAt2 = getChildAt(i11);
                        ItemInfo infoForChild2 = infoForChild(childAt2);
                        if (infoForChild2 == null || infoForChild2.position != this.mCurItem || !childAt2.requestFocus(i3)) {
                            i11++;
                        } else {
                            return;
                        }
                    }
                }
            }
        }
    }

    private void sortChildDrawingOrder() {
        ArrayList<View> arrayList;
        if (this.mDrawingOrder != 0) {
            if (this.mDrawingOrderedChildren == null) {
                new ArrayList<>();
                this.mDrawingOrderedChildren = arrayList;
            } else {
                this.mDrawingOrderedChildren.clear();
            }
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                boolean add = this.mDrawingOrderedChildren.add(getChildAt(i));
            }
            Collections.sort(this.mDrawingOrderedChildren, sPositionComparator);
        }
    }

    private void calculatePageOffsets(ItemInfo itemInfo, int i, ItemInfo itemInfo2) {
        ItemInfo itemInfo3;
        ItemInfo itemInfo4;
        ItemInfo itemInfo5 = itemInfo;
        int i2 = i;
        ItemInfo itemInfo6 = itemInfo2;
        int count = this.mAdapter.getCount();
        int clientWidth = getClientWidth();
        float f = clientWidth > 0 ? ((float) this.mPageMargin) / ((float) clientWidth) : 0.0f;
        if (itemInfo6 != null) {
            int i3 = itemInfo6.position;
            if (i3 < itemInfo5.position) {
                int i4 = 0;
                float f2 = itemInfo6.offset + itemInfo6.widthFactor + f;
                int i5 = i3 + 1;
                while (i5 <= itemInfo5.position && i4 < this.mItems.size()) {
                    ItemInfo itemInfo7 = this.mItems.get(i4);
                    while (true) {
                        itemInfo4 = itemInfo7;
                        if (i5 > itemInfo4.position && i4 < this.mItems.size() - 1) {
                            i4++;
                            itemInfo7 = this.mItems.get(i4);
                        }
                    }
                    while (i5 < itemInfo4.position) {
                        f2 += this.mAdapter.getPageWidth(i5) + f;
                        i5++;
                    }
                    itemInfo4.offset = f2;
                    f2 += itemInfo4.widthFactor + f;
                    i5++;
                }
            } else if (i3 > itemInfo5.position) {
                int size = this.mItems.size() - 1;
                float f3 = itemInfo6.offset;
                int i6 = i3 - 1;
                while (i6 >= itemInfo5.position && size >= 0) {
                    ItemInfo itemInfo8 = this.mItems.get(size);
                    while (true) {
                        itemInfo3 = itemInfo8;
                        if (i6 < itemInfo3.position && size > 0) {
                            size--;
                            itemInfo8 = this.mItems.get(size);
                        }
                    }
                    while (i6 > itemInfo3.position) {
                        f3 -= this.mAdapter.getPageWidth(i6) + f;
                        i6--;
                    }
                    f3 -= itemInfo3.widthFactor + f;
                    itemInfo3.offset = f3;
                    i6--;
                }
            }
        }
        int size2 = this.mItems.size();
        float f4 = itemInfo5.offset;
        int i7 = itemInfo5.position - 1;
        this.mFirstOffset = itemInfo5.position == 0 ? itemInfo5.offset : -3.4028235E38f;
        this.mLastOffset = itemInfo5.position == count + -1 ? (itemInfo5.offset + itemInfo5.widthFactor) - 1.0f : Float.MAX_VALUE;
        int i8 = i2 - 1;
        while (i8 >= 0) {
            ItemInfo itemInfo9 = this.mItems.get(i8);
            while (i7 > itemInfo9.position) {
                int i9 = i7;
                i7--;
                f4 -= this.mAdapter.getPageWidth(i9) + f;
            }
            f4 -= itemInfo9.widthFactor + f;
            itemInfo9.offset = f4;
            if (itemInfo9.position == 0) {
                this.mFirstOffset = f4;
            }
            i8--;
            i7--;
        }
        float f5 = itemInfo5.offset + itemInfo5.widthFactor + f;
        int i10 = itemInfo5.position + 1;
        int i11 = i2 + 1;
        while (i11 < size2) {
            ItemInfo itemInfo10 = this.mItems.get(i11);
            while (i10 < itemInfo10.position) {
                int i12 = i10;
                i10++;
                f5 += this.mAdapter.getPageWidth(i12) + f;
            }
            if (itemInfo10.position == count - 1) {
                this.mLastOffset = (f5 + itemInfo10.widthFactor) - 1.0f;
            }
            itemInfo10.offset = f5;
            f5 += itemInfo10.widthFactor + f;
            i11++;
            i10++;
        }
        this.mNeedCalculatePageOffsets = false;
    }

    public static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR;
        Parcelable adapterState;
        ClassLoader loader;
        int position;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            int i2 = i;
            super.writeToParcel(parcel2, i2);
            parcel2.writeInt(this.position);
            parcel2.writeParcelable(this.adapterState, i2);
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder();
            return sb.append("FragmentPager.SavedState{").append(Integer.toHexString(System.identityHashCode(this))).append(" position=").append(this.position).append("}").toString();
        }

        static {
            Object obj;
            new ParcelableCompatCreatorCallbacks<SavedState>() {
                public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    SavedState savedState;
                    new SavedState(parcel, classLoader);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = ParcelableCompat.newCreator(obj);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        SavedState(android.os.Parcel r7, java.lang.ClassLoader r8) {
            /*
                r6 = this;
                r0 = r6
                r1 = r7
                r2 = r8
                r3 = r0
                r4 = r1
                r3.<init>(r4)
                r3 = r2
                if (r3 != 0) goto L_0x0015
                r3 = r0
                java.lang.Class r3 = r3.getClass()
                java.lang.ClassLoader r3 = r3.getClassLoader()
                r2 = r3
            L_0x0015:
                r3 = r0
                r4 = r1
                int r4 = r4.readInt()
                r3.position = r4
                r3 = r0
                r4 = r1
                r5 = r2
                android.os.Parcelable r4 = r4.readParcelable(r5)
                r3.adapterState = r4
                r3 = r0
                r4 = r2
                r3.loader = r4
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.SavedState.<init>(android.os.Parcel, java.lang.ClassLoader):void");
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState;
        new SavedState(super.onSaveInstanceState());
        SavedState savedState2 = savedState;
        savedState2.position = this.mCurItem;
        if (this.mAdapter != null) {
            savedState2.adapterState = this.mAdapter.saveState();
        }
        return savedState2;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2 = parcelable;
        if (!(parcelable2 instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable2);
            return;
        }
        SavedState savedState = (SavedState) parcelable2;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (this.mAdapter != null) {
            this.mAdapter.restoreState(savedState.adapterState, savedState.loader);
            setCurrentItemInternal(savedState.position, false, true);
            return;
        }
        this.mRestoredCurItem = savedState.position;
        this.mRestoredAdapterState = savedState.adapterState;
        this.mRestoredClassLoader = savedState.loader;
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        Throwable th;
        View view2 = view;
        int i2 = i;
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        if (!checkLayoutParams(layoutParams2)) {
            layoutParams2 = generateLayoutParams(layoutParams2);
        }
        LayoutParams layoutParams3 = (LayoutParams) layoutParams2;
        layoutParams3.isDecor |= view2 instanceof Decor;
        if (!this.mInLayout) {
            super.addView(view2, i2, layoutParams2);
        } else if (layoutParams3 == null || !layoutParams3.isDecor) {
            layoutParams3.needsMeasure = true;
            boolean addViewInLayout = addViewInLayout(view2, i2, layoutParams2);
        } else {
            Throwable th2 = th;
            new IllegalStateException("Cannot add pager decor view during layout");
            throw th2;
        }
    }

    public void removeView(View view) {
        View view2 = view;
        if (this.mInLayout) {
            removeViewInLayout(view2);
        } else {
            super.removeView(view2);
        }
    }

    /* access modifiers changed from: package-private */
    public ItemInfo infoForChild(View view) {
        View view2 = view;
        for (int i = 0; i < this.mItems.size(); i++) {
            ItemInfo itemInfo = this.mItems.get(i);
            if (this.mAdapter.isViewFromObject(view2, itemInfo.object)) {
                return itemInfo;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public ItemInfo infoForAnyChild(View view) {
        View view2 = view;
        while (true) {
            ViewParent parent = view2.getParent();
            ViewParent viewParent = parent;
            if (parent == this) {
                return infoForChild(view2);
            }
            if (viewParent != null && (viewParent instanceof View)) {
                view2 = (View) viewParent;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public ItemInfo infoForPosition(int i) {
        int i2 = i;
        for (int i3 = 0; i3 < this.mItems.size(); i3++) {
            ItemInfo itemInfo = this.mItems.get(i3);
            if (itemInfo.position == i2) {
                return itemInfo;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mFirstLayout = true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        LayoutParams layoutParams;
        LayoutParams layoutParams2;
        setMeasuredDimension(getDefaultSize(0, i), getDefaultSize(0, i2));
        int measuredWidth = getMeasuredWidth();
        this.mGutterSize = Math.min(measuredWidth / 10, this.mDefaultGutterSize);
        int paddingLeft = (measuredWidth - getPaddingLeft()) - getPaddingRight();
        int measuredHeight = (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if (!(childAt.getVisibility() == 8 || (layoutParams2 = (LayoutParams) childAt.getLayoutParams()) == null || !layoutParams2.isDecor)) {
                int i4 = layoutParams2.gravity & 7;
                int i5 = layoutParams2.gravity & 112;
                int i6 = Integer.MIN_VALUE;
                int i7 = Integer.MIN_VALUE;
                boolean z = i5 == 48 || i5 == 80;
                boolean z2 = i4 == 3 || i4 == 5;
                if (z) {
                    i6 = 1073741824;
                } else if (z2) {
                    i7 = 1073741824;
                }
                int i8 = paddingLeft;
                int i9 = measuredHeight;
                if (layoutParams2.width != -2) {
                    i6 = 1073741824;
                    if (layoutParams2.width != -1) {
                        i8 = layoutParams2.width;
                    }
                }
                if (layoutParams2.height != -2) {
                    i7 = 1073741824;
                    if (layoutParams2.height != -1) {
                        i9 = layoutParams2.height;
                    }
                }
                childAt.measure(View.MeasureSpec.makeMeasureSpec(i8, i6), View.MeasureSpec.makeMeasureSpec(i9, i7));
                if (z) {
                    measuredHeight -= childAt.getMeasuredHeight();
                } else if (z2) {
                    paddingLeft -= childAt.getMeasuredWidth();
                }
            }
        }
        this.mChildWidthMeasureSpec = View.MeasureSpec.makeMeasureSpec(paddingLeft, 1073741824);
        this.mChildHeightMeasureSpec = View.MeasureSpec.makeMeasureSpec(measuredHeight, 1073741824);
        this.mInLayout = true;
        populate();
        this.mInLayout = false;
        int childCount2 = getChildCount();
        for (int i10 = 0; i10 < childCount2; i10++) {
            View childAt2 = getChildAt(i10);
            if (childAt2.getVisibility() != 8 && ((layoutParams = (LayoutParams) childAt2.getLayoutParams()) == null || !layoutParams.isDecor)) {
                childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (((float) paddingLeft) * layoutParams.widthFactor), 1073741824), this.mChildHeightMeasureSpec);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        int i5 = i;
        int i6 = i3;
        super.onSizeChanged(i5, i2, i6, i4);
        if (i5 != i6) {
            recomputeScrollPosition(i5, i6, this.mPageMargin, this.mPageMargin);
        }
    }

    private void recomputeScrollPosition(int i, int i2, int i3, int i4) {
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        if (i6 <= 0 || this.mItems.isEmpty()) {
            ItemInfo infoForPosition = infoForPosition(this.mCurItem);
            int min = (int) ((infoForPosition != null ? Math.min(infoForPosition.offset, this.mLastOffset) : 0.0f) * ((float) ((i5 - getPaddingLeft()) - getPaddingRight())));
            if (min != getScrollX()) {
                completeScroll(false);
                scrollTo(min, getScrollY());
                return;
            }
            return;
        }
        int scrollX = (int) ((((float) getScrollX()) / ((float) (((i6 - getPaddingLeft()) - getPaddingRight()) + i8))) * ((float) (((i5 - getPaddingLeft()) - getPaddingRight()) + i7)));
        scrollTo(scrollX, getScrollY());
        if (!this.mScroller.isFinished()) {
            this.mScroller.startScroll(scrollX, 0, (int) (infoForPosition(this.mCurItem).offset * ((float) i5)), 0, this.mScroller.getDuration() - this.mScroller.timePassed());
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth;
        int measuredHeight;
        int childCount = getChildCount();
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i7 = 0;
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.isDecor) {
                    int i9 = layoutParams.gravity & 7;
                    int i10 = layoutParams.gravity & 112;
                    switch (i9) {
                        case 1:
                            measuredWidth = Math.max((i5 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 2:
                        case 4:
                        default:
                            measuredWidth = paddingLeft;
                            break;
                        case 3:
                            measuredWidth = paddingLeft;
                            paddingLeft += childAt.getMeasuredWidth();
                            break;
                        case 5:
                            measuredWidth = (i5 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            break;
                    }
                    switch (i10) {
                        case 16:
                            measuredHeight = Math.max((i6 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            break;
                        case 48:
                            measuredHeight = paddingTop;
                            paddingTop += childAt.getMeasuredHeight();
                            break;
                        case 80:
                            measuredHeight = (i6 - paddingBottom) - childAt.getMeasuredHeight();
                            paddingBottom += childAt.getMeasuredHeight();
                            break;
                        default:
                            measuredHeight = paddingTop;
                            break;
                    }
                    int i11 = measuredWidth + scrollX;
                    childAt.layout(i11, measuredHeight, i11 + childAt.getMeasuredWidth(), measuredHeight + childAt.getMeasuredHeight());
                    i7++;
                }
            }
        }
        int i12 = (i5 - paddingLeft) - paddingRight;
        for (int i13 = 0; i13 < childCount; i13++) {
            View childAt2 = getChildAt(i13);
            if (childAt2.getVisibility() != 8) {
                LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                if (!layoutParams2.isDecor) {
                    ItemInfo infoForChild = infoForChild(childAt2);
                    ItemInfo itemInfo = infoForChild;
                    if (infoForChild != null) {
                        int i14 = paddingLeft + ((int) (((float) i12) * itemInfo.offset));
                        int i15 = paddingTop;
                        if (layoutParams2.needsMeasure) {
                            layoutParams2.needsMeasure = false;
                            childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (((float) i12) * layoutParams2.widthFactor), 1073741824), View.MeasureSpec.makeMeasureSpec((i6 - paddingTop) - paddingBottom, 1073741824));
                        }
                        childAt2.layout(i14, i15, i14 + childAt2.getMeasuredWidth(), i15 + childAt2.getMeasuredHeight());
                    }
                }
            }
        }
        this.mTopPageBounds = paddingTop;
        this.mBottomPageBounds = i6 - paddingBottom;
        this.mDecorChildCount = i7;
        if (this.mFirstLayout) {
            scrollToItem(this.mCurItem, false, 0, false);
        }
        this.mFirstLayout = false;
    }

    public void computeScroll() {
        if (this.mScroller.isFinished() || !this.mScroller.computeScrollOffset()) {
            completeScroll(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.mScroller.getCurrX();
        int currY = this.mScroller.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!pageScrolled(currX)) {
                this.mScroller.abortAnimation();
                scrollTo(0, currY);
            }
        }
        ViewCompat.postInvalidateOnAnimation(this);
    }

    private boolean pageScrolled(int i) {
        Throwable th;
        Throwable th2;
        int i2 = i;
        if (this.mItems.size() == 0) {
            this.mCalledSuper = false;
            onPageScrolled(0, 0.0f, 0);
            if (this.mCalledSuper) {
                return false;
            }
            Throwable th3 = th2;
            new IllegalStateException("onPageScrolled did not call superclass implementation");
            throw th3;
        }
        ItemInfo infoForCurrentScrollPosition = infoForCurrentScrollPosition();
        int clientWidth = getClientWidth();
        int i3 = clientWidth + this.mPageMargin;
        float f = ((float) this.mPageMargin) / ((float) clientWidth);
        int i4 = infoForCurrentScrollPosition.position;
        float f2 = ((((float) i2) / ((float) clientWidth)) - infoForCurrentScrollPosition.offset) / (infoForCurrentScrollPosition.widthFactor + f);
        this.mCalledSuper = false;
        onPageScrolled(i4, f2, (int) (f2 * ((float) i3)));
        if (this.mCalledSuper) {
            return true;
        }
        Throwable th4 = th;
        new IllegalStateException("onPageScrolled did not call superclass implementation");
        throw th4;
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void onPageScrolled(int i, float f, int i2) {
        int measuredWidth;
        int i3 = i;
        float f2 = f;
        int i4 = i2;
        if (this.mDecorChildCount > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            for (int i5 = 0; i5 < childCount; i5++) {
                View childAt = getChildAt(i5);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.isDecor) {
                    switch (layoutParams.gravity & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 2:
                        case 4:
                        default:
                            measuredWidth = paddingLeft;
                            break;
                        case 3:
                            measuredWidth = paddingLeft;
                            paddingLeft += childAt.getWidth();
                            break;
                        case 5:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                }
            }
        }
        dispatchOnPageScrolled(i3, f2, i4);
        if (this.mPageTransformer != null) {
            int scrollX2 = getScrollX();
            int childCount2 = getChildCount();
            for (int i6 = 0; i6 < childCount2; i6++) {
                View childAt2 = getChildAt(i6);
                if (!((LayoutParams) childAt2.getLayoutParams()).isDecor) {
                    this.mPageTransformer.transformPage(childAt2, ((float) (childAt2.getLeft() - scrollX2)) / ((float) getClientWidth()));
                }
            }
        }
        this.mCalledSuper = true;
    }

    private void dispatchOnPageScrolled(int i, float f, int i2) {
        int i3 = i;
        float f2 = f;
        int i4 = i2;
        if (this.mOnPageChangeListener != null) {
            this.mOnPageChangeListener.onPageScrolled(i3, f2, i4);
        }
        if (this.mOnPageChangeListeners != null) {
            int size = this.mOnPageChangeListeners.size();
            for (int i5 = 0; i5 < size; i5++) {
                OnPageChangeListener onPageChangeListener = this.mOnPageChangeListeners.get(i5);
                if (onPageChangeListener != null) {
                    onPageChangeListener.onPageScrolled(i3, f2, i4);
                }
            }
        }
        if (this.mInternalPageChangeListener != null) {
            this.mInternalPageChangeListener.onPageScrolled(i3, f2, i4);
        }
    }

    private void dispatchOnPageSelected(int i) {
        int i2 = i;
        if (this.mOnPageChangeListener != null) {
            this.mOnPageChangeListener.onPageSelected(i2);
        }
        if (this.mOnPageChangeListeners != null) {
            int size = this.mOnPageChangeListeners.size();
            for (int i3 = 0; i3 < size; i3++) {
                OnPageChangeListener onPageChangeListener = this.mOnPageChangeListeners.get(i3);
                if (onPageChangeListener != null) {
                    onPageChangeListener.onPageSelected(i2);
                }
            }
        }
        if (this.mInternalPageChangeListener != null) {
            this.mInternalPageChangeListener.onPageSelected(i2);
        }
    }

    private void dispatchOnScrollStateChanged(int i) {
        int i2 = i;
        if (this.mOnPageChangeListener != null) {
            this.mOnPageChangeListener.onPageScrollStateChanged(i2);
        }
        if (this.mOnPageChangeListeners != null) {
            int size = this.mOnPageChangeListeners.size();
            for (int i3 = 0; i3 < size; i3++) {
                OnPageChangeListener onPageChangeListener = this.mOnPageChangeListeners.get(i3);
                if (onPageChangeListener != null) {
                    onPageChangeListener.onPageScrollStateChanged(i2);
                }
            }
        }
        if (this.mInternalPageChangeListener != null) {
            this.mInternalPageChangeListener.onPageScrollStateChanged(i2);
        }
    }

    private void completeScroll(boolean z) {
        boolean z2 = z;
        boolean z3 = this.mScrollState == 2;
        if (z3) {
            setScrollingCacheEnabled(false);
            this.mScroller.abortAnimation();
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.mScroller.getCurrX();
            int currY = this.mScroller.getCurrY();
            if (!(scrollX == currX && scrollY == currY)) {
                scrollTo(currX, currY);
                if (currX != scrollX) {
                    boolean pageScrolled = pageScrolled(currX);
                }
            }
        }
        this.mPopulatePending = false;
        for (int i = 0; i < this.mItems.size(); i++) {
            ItemInfo itemInfo = this.mItems.get(i);
            if (itemInfo.scrolling) {
                z3 = true;
                itemInfo.scrolling = false;
            }
        }
        if (!z3) {
            return;
        }
        if (z2) {
            ViewCompat.postOnAnimation(this, this.mEndScrollRunnable);
        } else {
            this.mEndScrollRunnable.run();
        }
    }

    private boolean isGutterDrag(float f, float f2) {
        float f3 = f;
        float f4 = f2;
        return (f3 < ((float) this.mGutterSize) && f4 > 0.0f) || (f3 > ((float) (getWidth() - this.mGutterSize)) && f4 < 0.0f);
    }

    private void enableLayers(boolean z) {
        boolean z2 = z;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            ViewCompat.setLayerType(getChildAt(i), z2 ? 2 : 0, null);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int action = motionEvent2.getAction() & 255;
        if (action == 3 || action == 1) {
            boolean resetTouch = resetTouch();
            return false;
        }
        if (action != 0) {
            if (this.mIsBeingDragged) {
                return true;
            }
            if (this.mIsUnableToDrag) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x = motionEvent2.getX();
                this.mInitialMotionX = x;
                this.mLastMotionX = x;
                float y = motionEvent2.getY();
                this.mInitialMotionY = y;
                this.mLastMotionY = y;
                this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                this.mIsUnableToDrag = false;
                boolean computeScrollOffset = this.mScroller.computeScrollOffset();
                if (this.mScrollState == 2 && Math.abs(this.mScroller.getFinalX() - this.mScroller.getCurrX()) > this.mCloseEnough) {
                    this.mScroller.abortAnimation();
                    this.mPopulatePending = false;
                    populate();
                    this.mIsBeingDragged = true;
                    requestParentDisallowInterceptTouchEvent(true);
                    setScrollState(1);
                    break;
                } else {
                    completeScroll(false);
                    this.mIsBeingDragged = false;
                    break;
                }
                break;
            case 2:
                int i = this.mActivePointerId;
                if (i != -1) {
                    int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, i);
                    float x2 = MotionEventCompat.getX(motionEvent2, findPointerIndex);
                    float f = x2 - this.mLastMotionX;
                    float abs = Math.abs(f);
                    float y2 = MotionEventCompat.getY(motionEvent2, findPointerIndex);
                    float abs2 = Math.abs(y2 - this.mInitialMotionY);
                    if (f == 0.0f || isGutterDrag(this.mLastMotionX, f) || !canScroll(this, false, (int) f, (int) x2, (int) y2)) {
                        if (abs > ((float) this.mTouchSlop) && abs * 0.5f > abs2) {
                            this.mIsBeingDragged = true;
                            requestParentDisallowInterceptTouchEvent(true);
                            setScrollState(1);
                            this.mLastMotionX = f > 0.0f ? this.mInitialMotionX + ((float) this.mTouchSlop) : this.mInitialMotionX - ((float) this.mTouchSlop);
                            this.mLastMotionY = y2;
                            setScrollingCacheEnabled(true);
                        } else if (abs2 > ((float) this.mTouchSlop)) {
                            this.mIsUnableToDrag = true;
                        }
                        if (this.mIsBeingDragged && performDrag(x2)) {
                            ViewCompat.postInvalidateOnAnimation(this);
                            break;
                        }
                    } else {
                        this.mLastMotionX = x2;
                        this.mLastMotionY = y2;
                        this.mIsUnableToDrag = true;
                        return false;
                    }
                }
                break;
            case 6:
                onSecondaryPointerUp(motionEvent2);
                break;
        }
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(motionEvent2);
        return this.mIsBeingDragged;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f;
        MotionEvent motionEvent2 = motionEvent;
        if (this.mFakeDragging) {
            return true;
        }
        if (motionEvent2.getAction() == 0 && motionEvent2.getEdgeFlags() != 0) {
            return false;
        }
        if (this.mAdapter == null || this.mAdapter.getCount() == 0) {
            return false;
        }
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(motionEvent2);
        boolean z = false;
        switch (motionEvent2.getAction() & 255) {
            case 0:
                this.mScroller.abortAnimation();
                this.mPopulatePending = false;
                populate();
                float x = motionEvent2.getX();
                this.mInitialMotionX = x;
                this.mLastMotionX = x;
                float y = motionEvent2.getY();
                this.mInitialMotionY = y;
                this.mLastMotionY = y;
                this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                break;
            case 1:
                if (this.mIsBeingDragged) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
                    int xVelocity = (int) VelocityTrackerCompat.getXVelocity(velocityTracker, this.mActivePointerId);
                    this.mPopulatePending = true;
                    int clientWidth = getClientWidth();
                    int scrollX = getScrollX();
                    ItemInfo infoForCurrentScrollPosition = infoForCurrentScrollPosition();
                    setCurrentItemInternal(determineTargetPage(infoForCurrentScrollPosition.position, ((((float) scrollX) / ((float) clientWidth)) - infoForCurrentScrollPosition.offset) / infoForCurrentScrollPosition.widthFactor, xVelocity, (int) (MotionEventCompat.getX(motionEvent2, MotionEventCompat.findPointerIndex(motionEvent2, this.mActivePointerId)) - this.mInitialMotionX)), true, true, xVelocity);
                    z = resetTouch();
                    break;
                }
                break;
            case 2:
                if (!this.mIsBeingDragged) {
                    int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, this.mActivePointerId);
                    if (findPointerIndex == -1) {
                        z = resetTouch();
                        break;
                    } else {
                        float x2 = MotionEventCompat.getX(motionEvent2, findPointerIndex);
                        float abs = Math.abs(x2 - this.mLastMotionX);
                        float y2 = MotionEventCompat.getY(motionEvent2, findPointerIndex);
                        float abs2 = Math.abs(y2 - this.mLastMotionY);
                        if (abs > ((float) this.mTouchSlop) && abs > abs2) {
                            this.mIsBeingDragged = true;
                            requestParentDisallowInterceptTouchEvent(true);
                            if (x2 - this.mInitialMotionX > 0.0f) {
                                f = this.mInitialMotionX + ((float) this.mTouchSlop);
                            } else {
                                f = this.mInitialMotionX - ((float) this.mTouchSlop);
                            }
                            this.mLastMotionX = f;
                            this.mLastMotionY = y2;
                            setScrollState(1);
                            setScrollingCacheEnabled(true);
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                            }
                        }
                    }
                }
                if (this.mIsBeingDragged) {
                    z = false | performDrag(MotionEventCompat.getX(motionEvent2, MotionEventCompat.findPointerIndex(motionEvent2, this.mActivePointerId)));
                    break;
                }
                break;
            case 3:
                if (this.mIsBeingDragged) {
                    scrollToItem(this.mCurItem, true, 0, false);
                    z = resetTouch();
                    break;
                }
                break;
            case 5:
                int actionIndex = MotionEventCompat.getActionIndex(motionEvent2);
                this.mLastMotionX = MotionEventCompat.getX(motionEvent2, actionIndex);
                this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, actionIndex);
                break;
            case 6:
                onSecondaryPointerUp(motionEvent2);
                this.mLastMotionX = MotionEventCompat.getX(motionEvent2, MotionEventCompat.findPointerIndex(motionEvent2, this.mActivePointerId));
                break;
        }
        if (z) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
        return true;
    }

    private boolean resetTouch() {
        this.mActivePointerId = -1;
        endDrag();
        return this.mLeftEdge.onRelease() | this.mRightEdge.onRelease();
    }

    private void requestParentDisallowInterceptTouchEvent(boolean z) {
        boolean z2 = z;
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(z2);
        }
    }

    private boolean performDrag(float f) {
        float f2 = f;
        boolean z = false;
        float f3 = this.mLastMotionX - f2;
        this.mLastMotionX = f2;
        float scrollX = ((float) getScrollX()) + f3;
        int clientWidth = getClientWidth();
        float f4 = ((float) clientWidth) * this.mFirstOffset;
        float f5 = ((float) clientWidth) * this.mLastOffset;
        boolean z2 = true;
        boolean z3 = true;
        ItemInfo itemInfo = this.mItems.get(0);
        ItemInfo itemInfo2 = this.mItems.get(this.mItems.size() - 1);
        if (itemInfo.position != 0) {
            z2 = false;
            f4 = itemInfo.offset * ((float) clientWidth);
        }
        if (itemInfo2.position != this.mAdapter.getCount() - 1) {
            z3 = false;
            f5 = itemInfo2.offset * ((float) clientWidth);
        }
        if (scrollX < f4) {
            if (z2) {
                z = this.mLeftEdge.onPull(Math.abs(f4 - scrollX) / ((float) clientWidth));
            }
            scrollX = f4;
        } else if (scrollX > f5) {
            if (z3) {
                z = this.mRightEdge.onPull(Math.abs(scrollX - f5) / ((float) clientWidth));
            }
            scrollX = f5;
        }
        this.mLastMotionX = this.mLastMotionX + (scrollX - ((float) ((int) scrollX)));
        scrollTo((int) scrollX, getScrollY());
        boolean pageScrolled = pageScrolled((int) scrollX);
        return z;
    }

    private ItemInfo infoForCurrentScrollPosition() {
        int clientWidth = getClientWidth();
        float scrollX = clientWidth > 0 ? ((float) getScrollX()) / ((float) clientWidth) : 0.0f;
        float f = clientWidth > 0 ? ((float) this.mPageMargin) / ((float) clientWidth) : 0.0f;
        int i = -1;
        float f2 = 0.0f;
        float f3 = 0.0f;
        boolean z = true;
        ItemInfo itemInfo = null;
        int i2 = 0;
        while (i2 < this.mItems.size()) {
            ItemInfo itemInfo2 = this.mItems.get(i2);
            if (!z && itemInfo2.position != i + 1) {
                itemInfo2 = this.mTempItem;
                itemInfo2.offset = f2 + f3 + f;
                itemInfo2.position = i + 1;
                itemInfo2.widthFactor = this.mAdapter.getPageWidth(itemInfo2.position);
                i2--;
            }
            float f4 = itemInfo2.offset;
            float f5 = f4;
            float f6 = f4 + itemInfo2.widthFactor + f;
            if (!z && scrollX < f5) {
                return itemInfo;
            }
            if (scrollX < f6 || i2 == this.mItems.size() - 1) {
                return itemInfo2;
            }
            z = false;
            i = itemInfo2.position;
            f2 = f4;
            f3 = itemInfo2.widthFactor;
            itemInfo = itemInfo2;
            i2++;
        }
        return itemInfo;
    }

    private int determineTargetPage(int i, float f, int i2, int i3) {
        int i4;
        int i5 = i;
        float f2 = f;
        int i6 = i2;
        if (Math.abs(i3) <= this.mFlingDistance || Math.abs(i6) <= this.mMinimumVelocity) {
            i4 = (int) (((float) i5) + f2 + (i5 >= this.mCurItem ? 0.4f : 0.6f));
        } else {
            i4 = i6 > 0 ? i5 : i5 + 1;
        }
        if (this.mItems.size() > 0) {
            i4 = Math.max(this.mItems.get(0).position, Math.min(i4, this.mItems.get(this.mItems.size() - 1).position));
        }
        return i4;
    }

    public void draw(Canvas canvas) {
        Canvas canvas2 = canvas;
        super.draw(canvas2);
        boolean z = false;
        int overScrollMode = ViewCompat.getOverScrollMode(this);
        if (overScrollMode == 0 || (overScrollMode == 1 && this.mAdapter != null && this.mAdapter.getCount() > 1)) {
            if (!this.mLeftEdge.isFinished()) {
                int save = canvas2.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas2.rotate(270.0f);
                canvas2.translate((float) ((-height) + getPaddingTop()), this.mFirstOffset * ((float) width));
                this.mLeftEdge.setSize(height, width);
                z = false | this.mLeftEdge.draw(canvas2);
                canvas2.restoreToCount(save);
            }
            if (!this.mRightEdge.isFinished()) {
                int save2 = canvas2.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas2.rotate(90.0f);
                canvas2.translate((float) (-getPaddingTop()), (-(this.mLastOffset + 1.0f)) * ((float) width2));
                this.mRightEdge.setSize(height2, width2);
                z |= this.mRightEdge.draw(canvas2);
                canvas2.restoreToCount(save2);
            }
        } else {
            this.mLeftEdge.finish();
            this.mRightEdge.finish();
        }
        if (z) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f;
        Canvas canvas2 = canvas;
        super.onDraw(canvas2);
        if (this.mPageMargin > 0 && this.mMarginDrawable != null && this.mItems.size() > 0 && this.mAdapter != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f2 = ((float) this.mPageMargin) / ((float) width);
            int i = 0;
            ItemInfo itemInfo = this.mItems.get(0);
            float f3 = itemInfo.offset;
            int size = this.mItems.size();
            int i2 = itemInfo.position;
            int i3 = this.mItems.get(size - 1).position;
            int i4 = i2;
            while (i4 < i3) {
                while (i4 > itemInfo.position && i < size) {
                    i++;
                    itemInfo = this.mItems.get(i);
                }
                if (i4 == itemInfo.position) {
                    f = (itemInfo.offset + itemInfo.widthFactor) * ((float) width);
                    f3 = itemInfo.offset + itemInfo.widthFactor + f2;
                } else {
                    float pageWidth = this.mAdapter.getPageWidth(i4);
                    f = (f3 + pageWidth) * ((float) width);
                    f3 += pageWidth + f2;
                }
                if (f + ((float) this.mPageMargin) > ((float) scrollX)) {
                    this.mMarginDrawable.setBounds((int) f, this.mTopPageBounds, (int) (f + ((float) this.mPageMargin) + 0.5f), this.mBottomPageBounds);
                    this.mMarginDrawable.draw(canvas2);
                }
                if (f <= ((float) (scrollX + width))) {
                    i4++;
                } else {
                    return;
                }
            }
        }
    }

    public boolean beginFakeDrag() {
        if (this.mIsBeingDragged) {
            return false;
        }
        this.mFakeDragging = true;
        setScrollState(1);
        this.mLastMotionX = 0.0f;
        this.mInitialMotionX = 0.0f;
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        } else {
            this.mVelocityTracker.clear();
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 0, 0.0f, 0.0f, 0);
        this.mVelocityTracker.addMovement(obtain);
        obtain.recycle();
        this.mFakeDragBeginTime = uptimeMillis;
        return true;
    }

    public void endFakeDrag() {
        Throwable th;
        if (!this.mFakeDragging) {
            Throwable th2 = th;
            new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
            throw th2;
        }
        VelocityTracker velocityTracker = this.mVelocityTracker;
        velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
        int xVelocity = (int) VelocityTrackerCompat.getXVelocity(velocityTracker, this.mActivePointerId);
        this.mPopulatePending = true;
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        ItemInfo infoForCurrentScrollPosition = infoForCurrentScrollPosition();
        setCurrentItemInternal(determineTargetPage(infoForCurrentScrollPosition.position, ((((float) scrollX) / ((float) clientWidth)) - infoForCurrentScrollPosition.offset) / infoForCurrentScrollPosition.widthFactor, xVelocity, (int) (this.mLastMotionX - this.mInitialMotionX)), true, true, xVelocity);
        endDrag();
        this.mFakeDragging = false;
    }

    public void fakeDragBy(float f) {
        Throwable th;
        float f2 = f;
        if (!this.mFakeDragging) {
            Throwable th2 = th;
            new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
            throw th2;
        }
        this.mLastMotionX = this.mLastMotionX + f2;
        float scrollX = ((float) getScrollX()) - f2;
        int clientWidth = getClientWidth();
        float f3 = ((float) clientWidth) * this.mFirstOffset;
        float f4 = ((float) clientWidth) * this.mLastOffset;
        ItemInfo itemInfo = this.mItems.get(0);
        ItemInfo itemInfo2 = this.mItems.get(this.mItems.size() - 1);
        if (itemInfo.position != 0) {
            f3 = itemInfo.offset * ((float) clientWidth);
        }
        if (itemInfo2.position != this.mAdapter.getCount() - 1) {
            f4 = itemInfo2.offset * ((float) clientWidth);
        }
        if (scrollX < f3) {
            scrollX = f3;
        } else if (scrollX > f4) {
            scrollX = f4;
        }
        this.mLastMotionX = this.mLastMotionX + (scrollX - ((float) ((int) scrollX)));
        scrollTo((int) scrollX, getScrollY());
        boolean pageScrolled = pageScrolled((int) scrollX);
        MotionEvent obtain = MotionEvent.obtain(this.mFakeDragBeginTime, SystemClock.uptimeMillis(), 2, this.mLastMotionX, 0.0f, 0);
        this.mVelocityTracker.addMovement(obtain);
        obtain.recycle();
    }

    public boolean isFakeDragging() {
        return this.mFakeDragging;
    }

    private void onSecondaryPointerUp(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int actionIndex = MotionEventCompat.getActionIndex(motionEvent2);
        if (MotionEventCompat.getPointerId(motionEvent2, actionIndex) == this.mActivePointerId) {
            int i = actionIndex == 0 ? 1 : 0;
            this.mLastMotionX = MotionEventCompat.getX(motionEvent2, i);
            this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, i);
            if (this.mVelocityTracker != null) {
                this.mVelocityTracker.clear();
            }
        }
    }

    private void endDrag() {
        this.mIsBeingDragged = false;
        this.mIsUnableToDrag = false;
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    private void setScrollingCacheEnabled(boolean z) {
        boolean z2 = z;
        if (this.mScrollingCacheEnabled != z2) {
            this.mScrollingCacheEnabled = z2;
        }
    }

    public boolean canScrollHorizontally(int i) {
        int i2 = i;
        if (this.mAdapter == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        if (i2 < 0) {
            return scrollX > ((int) (((float) clientWidth) * this.mFirstOffset));
        } else if (i2 <= 0) {
            return false;
        } else {
            return scrollX < ((int) (((float) clientWidth) * this.mLastOffset));
        }
    }

    /* access modifiers changed from: protected */
    public boolean canScroll(View view, boolean z, int i, int i2, int i3) {
        View view2 = view;
        boolean z2 = z;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        if (view2 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view2;
            int scrollX = view2.getScrollX();
            int scrollY = view2.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i5 + scrollX >= childAt.getLeft() && i5 + scrollX < childAt.getRight() && i6 + scrollY >= childAt.getTop() && i6 + scrollY < childAt.getBottom() && canScroll(childAt, true, i4, (i5 + scrollX) - childAt.getLeft(), (i6 + scrollY) - childAt.getTop())) {
                    return true;
                }
            }
        }
        return z2 && ViewCompat.canScrollHorizontally(view2, -i4);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        KeyEvent keyEvent2 = keyEvent;
        return super.dispatchKeyEvent(keyEvent2) || executeKeyEvent(keyEvent2);
    }

    public boolean executeKeyEvent(KeyEvent keyEvent) {
        KeyEvent keyEvent2 = keyEvent;
        boolean z = false;
        if (keyEvent2.getAction() == 0) {
            switch (keyEvent2.getKeyCode()) {
                case 21:
                    z = arrowScroll(17);
                    break;
                case 22:
                    z = arrowScroll(66);
                    break;
                case 61:
                    if (Build.VERSION.SDK_INT >= 11) {
                        if (!KeyEventCompat.hasNoModifiers(keyEvent2)) {
                            if (KeyEventCompat.hasModifiers(keyEvent2, 1)) {
                                z = arrowScroll(1);
                                break;
                            }
                        } else {
                            z = arrowScroll(2);
                            break;
                        }
                    }
                    break;
            }
        }
        return z;
    }

    public boolean arrowScroll(int i) {
        StringBuilder sb;
        StringBuilder sb2;
        int i2 = i;
        View findFocus = findFocus();
        if (findFocus == this) {
            findFocus = null;
        } else if (findFocus != null) {
            boolean z = false;
            ViewParent parent = findFocus.getParent();
            while (true) {
                ViewParent viewParent = parent;
                if (!(viewParent instanceof ViewGroup)) {
                    break;
                } else if (viewParent == this) {
                    z = true;
                    break;
                } else {
                    parent = viewParent.getParent();
                }
            }
            if (!z) {
                new StringBuilder();
                StringBuilder sb3 = sb;
                StringBuilder append = sb3.append(findFocus.getClass().getSimpleName());
                ViewParent parent2 = findFocus.getParent();
                while (true) {
                    ViewParent viewParent2 = parent2;
                    if (!(viewParent2 instanceof ViewGroup)) {
                        break;
                    }
                    StringBuilder append2 = sb3.append(" => ").append(viewParent2.getClass().getSimpleName());
                    parent2 = viewParent2.getParent();
                }
                new StringBuilder();
                int e = Log.e(TAG, sb2.append("arrowScroll tried to find focus based on non-child current focused view ").append(sb3.toString()).toString());
                findFocus = null;
            }
        }
        boolean z2 = false;
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, i2);
        if (findNextFocus == null || findNextFocus == findFocus) {
            if (i2 == 17 || i2 == 1) {
                z2 = pageLeft();
            } else if (i2 == 66 || i2 == 2) {
                z2 = pageRight();
            }
        } else if (i2 == 17) {
            z2 = (findFocus == null || getChildRectInPagerCoordinates(this.mTempRect, findNextFocus).left < getChildRectInPagerCoordinates(this.mTempRect, findFocus).left) ? findNextFocus.requestFocus() : pageLeft();
        } else if (i2 == 66) {
            z2 = (findFocus == null || getChildRectInPagerCoordinates(this.mTempRect, findNextFocus).left > getChildRectInPagerCoordinates(this.mTempRect, findFocus).left) ? findNextFocus.requestFocus() : pageRight();
        }
        if (z2) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i2));
        }
        return z2;
    }

    private Rect getChildRectInPagerCoordinates(Rect rect, View view) {
        Rect rect2;
        Rect rect3 = rect;
        View view2 = view;
        if (rect3 == null) {
            new Rect();
            rect3 = rect2;
        }
        if (view2 == null) {
            rect3.set(0, 0, 0, 0);
            return rect3;
        }
        rect3.left = view2.getLeft();
        rect3.right = view2.getRight();
        rect3.top = view2.getTop();
        rect3.bottom = view2.getBottom();
        ViewParent parent = view2.getParent();
        while (true) {
            ViewParent viewParent = parent;
            if ((viewParent instanceof ViewGroup) && viewParent != this) {
                ViewGroup viewGroup = (ViewGroup) viewParent;
                rect3.left += viewGroup.getLeft();
                rect3.right += viewGroup.getRight();
                rect3.top += viewGroup.getTop();
                rect3.bottom += viewGroup.getBottom();
                parent = viewGroup.getParent();
            }
        }
        return rect3;
    }

    /* access modifiers changed from: package-private */
    public boolean pageLeft() {
        if (this.mCurItem <= 0) {
            return false;
        }
        setCurrentItem(this.mCurItem - 1, true);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean pageRight() {
        if (this.mAdapter == null || this.mCurItem >= this.mAdapter.getCount() - 1) {
            return false;
        }
        setCurrentItem(this.mCurItem + 1, true);
        return true;
    }

    public void addFocusables(ArrayList<View> arrayList, int i, int i2) {
        ItemInfo infoForChild;
        ArrayList<View> arrayList2 = arrayList;
        int i3 = i;
        int i4 = i2;
        int size = arrayList2.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i5 = 0; i5 < getChildCount(); i5++) {
                View childAt = getChildAt(i5);
                if (childAt.getVisibility() == 0 && (infoForChild = infoForChild(childAt)) != null && infoForChild.position == this.mCurItem) {
                    childAt.addFocusables(arrayList2, i3, i4);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList2.size()) || !isFocusable()) {
            return;
        }
        if (((i4 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList2 != null) {
            boolean add = arrayList2.add(this);
        }
    }

    public void addTouchables(ArrayList<View> arrayList) {
        ItemInfo infoForChild;
        ArrayList<View> arrayList2 = arrayList;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (infoForChild = infoForChild(childAt)) != null && infoForChild.position == this.mCurItem) {
                childAt.addTouchables(arrayList2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        int i2;
        int i3;
        int i4;
        ItemInfo infoForChild;
        int i5 = i;
        Rect rect2 = rect;
        int childCount = getChildCount();
        if ((i5 & 2) != 0) {
            i2 = 0;
            i3 = 1;
            i4 = childCount;
        } else {
            i2 = childCount - 1;
            i3 = -1;
            i4 = -1;
        }
        int i6 = i2;
        while (true) {
            int i7 = i6;
            if (i7 == i4) {
                return false;
            }
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() == 0 && (infoForChild = infoForChild(childAt)) != null && infoForChild.position == this.mCurItem && childAt.requestFocus(i5, rect2)) {
                return true;
            }
            i6 = i7 + i3;
        }
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        ItemInfo infoForChild;
        AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
        if (accessibilityEvent2.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent2);
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (infoForChild = infoForChild(childAt)) != null && infoForChild.position == this.mCurItem && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent2)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        ViewGroup.LayoutParams layoutParams;
        new LayoutParams();
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        return (layoutParams2 instanceof LayoutParams) && super.checkLayoutParams(layoutParams2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        ViewGroup.LayoutParams layoutParams;
        new LayoutParams(getContext(), attributeSet);
        return layoutParams;
    }

    class MyAccessibilityDelegate extends AccessibilityDelegateCompat {
        MyAccessibilityDelegate() {
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
            super.onInitializeAccessibilityEvent(view, accessibilityEvent2);
            accessibilityEvent2.setClassName(ViewPager.class.getName());
            AccessibilityRecordCompat obtain = AccessibilityRecordCompat.obtain();
            obtain.setScrollable(canScroll());
            if (accessibilityEvent2.getEventType() == 4096 && ViewPager.this.mAdapter != null) {
                obtain.setItemCount(ViewPager.this.mAdapter.getCount());
                obtain.setFromIndex(ViewPager.this.mCurItem);
                obtain.setToIndex(ViewPager.this.mCurItem);
            }
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
            super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat2);
            accessibilityNodeInfoCompat2.setClassName(ViewPager.class.getName());
            accessibilityNodeInfoCompat2.setScrollable(canScroll());
            if (ViewPager.this.canScrollHorizontally(1)) {
                accessibilityNodeInfoCompat2.addAction(4096);
            }
            if (ViewPager.this.canScrollHorizontally(-1)) {
                accessibilityNodeInfoCompat2.addAction(8192);
            }
        }

        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            int i2 = i;
            if (super.performAccessibilityAction(view, i2, bundle)) {
                return true;
            }
            switch (i2) {
                case 4096:
                    if (!ViewPager.this.canScrollHorizontally(1)) {
                        return false;
                    }
                    ViewPager.this.setCurrentItem(ViewPager.this.mCurItem + 1);
                    return true;
                case 8192:
                    if (!ViewPager.this.canScrollHorizontally(-1)) {
                        return false;
                    }
                    ViewPager.this.setCurrentItem(ViewPager.this.mCurItem - 1);
                    return true;
                default:
                    return false;
            }
        }

        private boolean canScroll() {
            return ViewPager.this.mAdapter != null && ViewPager.this.mAdapter.getCount() > 1;
        }
    }

    private class PagerObserver extends DataSetObserver {
        private PagerObserver() {
        }

        public void onChanged() {
            ViewPager.this.dataSetChanged();
        }

        public void onInvalidated() {
            ViewPager.this.dataSetChanged();
        }
    }

    public static class LayoutParams extends ViewGroup.LayoutParams {
        int childIndex;
        public int gravity;
        public boolean isDecor;
        boolean needsMeasure;
        int position;
        float widthFactor = 0.0f;

        public LayoutParams() {
            super(-1, -1);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public LayoutParams(android.content.Context r9, android.util.AttributeSet r10) {
            /*
                r8 = this;
                r0 = r8
                r1 = r9
                r2 = r10
                r4 = r0
                r5 = r1
                r6 = r2
                r4.<init>(r5, r6)
                r4 = r0
                r5 = 0
                r4.widthFactor = r5
                r4 = r1
                r5 = r2
                int[] r6 = android.support.v4.view.ViewPager.LAYOUT_ATTRS
                android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r6)
                r3 = r4
                r4 = r0
                r5 = r3
                r6 = 0
                r7 = 48
                int r5 = r5.getInteger(r6, r7)
                r4.gravity = r5
                r4 = r3
                r4.recycle()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.LayoutParams.<init>(android.content.Context, android.util.AttributeSet):void");
        }
    }

    static class ViewPositionComparator implements Comparator<View> {
        ViewPositionComparator() {
        }

        public int compare(View view, View view2) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            LayoutParams layoutParams2 = (LayoutParams) view2.getLayoutParams();
            if (layoutParams.isDecor == layoutParams2.isDecor) {
                return layoutParams.position - layoutParams2.position;
            }
            return layoutParams.isDecor ? 1 : -1;
        }
    }
}
