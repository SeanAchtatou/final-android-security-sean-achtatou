package android.support.v4.view;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.view.View;
import java.lang.reflect.Field;

class ViewCompatBase {
    private static final String TAG = "ViewCompatBase";
    private static Field sMinHeightField;
    private static boolean sMinHeightFieldFetched;
    private static Field sMinWidthField;
    private static boolean sMinWidthFieldFetched;

    ViewCompatBase() {
    }

    static ColorStateList getBackgroundTintList(View view) {
        View view2 = view;
        return view2 instanceof TintableBackgroundView ? ((TintableBackgroundView) view2).getSupportBackgroundTintList() : null;
    }

    static void setBackgroundTintList(View view, ColorStateList colorStateList) {
        View view2 = view;
        ColorStateList colorStateList2 = colorStateList;
        if (view2 instanceof TintableBackgroundView) {
            ((TintableBackgroundView) view2).setSupportBackgroundTintList(colorStateList2);
        }
    }

    static PorterDuff.Mode getBackgroundTintMode(View view) {
        View view2 = view;
        return view2 instanceof TintableBackgroundView ? ((TintableBackgroundView) view2).getSupportBackgroundTintMode() : null;
    }

    static void setBackgroundTintMode(View view, PorterDuff.Mode mode) {
        View view2 = view;
        PorterDuff.Mode mode2 = mode;
        if (view2 instanceof TintableBackgroundView) {
            ((TintableBackgroundView) view2).setSupportBackgroundTintMode(mode2);
        }
    }

    static boolean isLaidOut(View view) {
        View view2 = view;
        return view2.getWidth() > 0 && view2.getHeight() > 0;
    }

    static int getMinimumWidth(View view) {
        View view2 = view;
        if (!sMinWidthFieldFetched) {
            try {
                sMinWidthField = View.class.getDeclaredField("mMinWidth");
                sMinWidthField.setAccessible(true);
            } catch (NoSuchFieldException e) {
            }
            sMinWidthFieldFetched = true;
        }
        if (sMinWidthField != null) {
            try {
                return ((Integer) sMinWidthField.get(view2)).intValue();
            } catch (Exception e2) {
            }
        }
        return 0;
    }

    static int getMinimumHeight(View view) {
        View view2 = view;
        if (!sMinHeightFieldFetched) {
            try {
                sMinHeightField = View.class.getDeclaredField("mMinHeight");
                sMinHeightField.setAccessible(true);
            } catch (NoSuchFieldException e) {
            }
            sMinHeightFieldFetched = true;
        }
        if (sMinHeightField != null) {
            try {
                return ((Integer) sMinHeightField.get(view2)).intValue();
            } catch (Exception e2) {
            }
        }
        return 0;
    }

    static boolean isAttachedToWindow(View view) {
        return view.getWindowToken() != null;
    }
}
