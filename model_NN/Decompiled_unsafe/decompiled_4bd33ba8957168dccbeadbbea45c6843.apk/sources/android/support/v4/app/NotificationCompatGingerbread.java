package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;

class NotificationCompatGingerbread {
    NotificationCompatGingerbread() {
    }

    public static Notification add(Notification notification, Context context, CharSequence charSequence, CharSequence charSequence2, PendingIntent pendingIntent, PendingIntent pendingIntent2) {
        Notification notification2 = notification;
        notification2.setLatestEventInfo(context, charSequence, charSequence2, pendingIntent);
        notification2.fullScreenIntent = pendingIntent2;
        return notification2;
    }
}
