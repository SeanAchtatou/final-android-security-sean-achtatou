package android.support.v4.app;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;

class ActionBarDrawerToggleJellybeanMR2 {
    private static final String TAG = "ActionBarDrawerToggleImplJellybeanMR2";
    private static final int[] THEME_ATTRS = {16843531};

    ActionBarDrawerToggleJellybeanMR2() {
    }

    public static Object setActionBarUpIndicator(Object obj, Activity activity, Drawable drawable, int i) {
        Object obj2 = obj;
        Drawable drawable2 = drawable;
        int i2 = i;
        ActionBar actionBar = activity.getActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(drawable2);
            actionBar.setHomeActionContentDescription(i2);
        }
        return obj2;
    }

    public static Object setActionBarDescription(Object obj, Activity activity, int i) {
        Object obj2 = obj;
        int i2 = i;
        ActionBar actionBar = activity.getActionBar();
        if (actionBar != null) {
            actionBar.setHomeActionContentDescription(i2);
        }
        return obj2;
    }

    public static Drawable getThemeUpIndicator(Activity activity) {
        Context context;
        Context context2 = activity;
        ActionBar actionBar = context2.getActionBar();
        if (actionBar != null) {
            context = actionBar.getThemedContext();
        } else {
            context = context2;
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(null, THEME_ATTRS, 16843470, 0);
        Drawable drawable = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        return drawable;
    }
}
