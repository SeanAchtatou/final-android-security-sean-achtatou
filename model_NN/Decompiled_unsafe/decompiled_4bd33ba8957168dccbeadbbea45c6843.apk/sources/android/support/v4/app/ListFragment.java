package android.support.v4.app;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ListFragment extends Fragment {
    static final int INTERNAL_EMPTY_ID = 16711681;
    static final int INTERNAL_LIST_CONTAINER_ID = 16711683;
    static final int INTERNAL_PROGRESS_CONTAINER_ID = 16711682;
    ListAdapter mAdapter;
    CharSequence mEmptyText;
    View mEmptyView;
    private final Handler mHandler;
    ListView mList;
    View mListContainer;
    boolean mListShown;
    private final AdapterView.OnItemClickListener mOnClickListener;
    View mProgressContainer;
    private final Runnable mRequestFocus;
    TextView mStandardEmptyView;

    public ListFragment() {
        Handler handler;
        Runnable runnable;
        AdapterView.OnItemClickListener onItemClickListener;
        new Handler();
        this.mHandler = handler;
        new Runnable() {
            public void run() {
                ListFragment.this.mList.focusableViewAvailable(ListFragment.this.mList);
            }
        };
        this.mRequestFocus = runnable;
        new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                ListFragment.this.onListItemClick((ListView) adapterView, view, i, j);
            }
        };
        this.mOnClickListener = onItemClickListener;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FrameLayout frameLayout;
        LinearLayout linearLayout;
        View view;
        ViewGroup.LayoutParams layoutParams;
        ViewGroup.LayoutParams layoutParams2;
        FrameLayout frameLayout2;
        TextView textView;
        ViewGroup.LayoutParams layoutParams3;
        ListView listView;
        ViewGroup.LayoutParams layoutParams4;
        ViewGroup.LayoutParams layoutParams5;
        ViewGroup.LayoutParams layoutParams6;
        FragmentActivity activity = getActivity();
        new FrameLayout(activity);
        FrameLayout frameLayout3 = frameLayout;
        new LinearLayout(activity);
        LinearLayout linearLayout2 = linearLayout;
        linearLayout2.setId(INTERNAL_PROGRESS_CONTAINER_ID);
        linearLayout2.setOrientation(1);
        linearLayout2.setVisibility(8);
        linearLayout2.setGravity(17);
        new ProgressBar(activity, null, 16842874);
        new FrameLayout.LayoutParams(-2, -2);
        linearLayout2.addView(view, layoutParams);
        new FrameLayout.LayoutParams(-1, -1);
        frameLayout3.addView(linearLayout2, layoutParams2);
        new FrameLayout(activity);
        FrameLayout frameLayout4 = frameLayout2;
        frameLayout4.setId(INTERNAL_LIST_CONTAINER_ID);
        new TextView(getActivity());
        TextView textView2 = textView;
        textView2.setId(INTERNAL_EMPTY_ID);
        textView2.setGravity(17);
        new FrameLayout.LayoutParams(-1, -1);
        frameLayout4.addView(textView2, layoutParams3);
        new ListView(getActivity());
        ListView listView2 = listView;
        listView2.setId(16908298);
        listView2.setDrawSelectorOnTop(false);
        new FrameLayout.LayoutParams(-1, -1);
        frameLayout4.addView(listView2, layoutParams4);
        new FrameLayout.LayoutParams(-1, -1);
        frameLayout3.addView(frameLayout4, layoutParams5);
        new FrameLayout.LayoutParams(-1, -1);
        frameLayout3.setLayoutParams(layoutParams6);
        return frameLayout3;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        ensureList();
    }

    public void onDestroyView() {
        this.mHandler.removeCallbacks(this.mRequestFocus);
        this.mList = null;
        this.mListShown = false;
        this.mListContainer = null;
        this.mProgressContainer = null;
        this.mEmptyView = null;
        this.mStandardEmptyView = null;
        super.onDestroyView();
    }

    public void onListItemClick(ListView listView, View view, int i, long j) {
    }

    public void setListAdapter(ListAdapter listAdapter) {
        ListAdapter listAdapter2 = listAdapter;
        boolean z = this.mAdapter != null;
        this.mAdapter = listAdapter2;
        if (this.mList != null) {
            this.mList.setAdapter(listAdapter2);
            if (!this.mListShown && !z) {
                setListShown(true, getView().getWindowToken() != null);
            }
        }
    }

    public void setSelection(int i) {
        ensureList();
        this.mList.setSelection(i);
    }

    public int getSelectedItemPosition() {
        ensureList();
        return this.mList.getSelectedItemPosition();
    }

    public long getSelectedItemId() {
        ensureList();
        return this.mList.getSelectedItemId();
    }

    public ListView getListView() {
        ensureList();
        return this.mList;
    }

    public void setEmptyText(CharSequence charSequence) {
        Throwable th;
        CharSequence charSequence2 = charSequence;
        ensureList();
        if (this.mStandardEmptyView == null) {
            Throwable th2 = th;
            new IllegalStateException("Can't be used with a custom content view");
            throw th2;
        }
        this.mStandardEmptyView.setText(charSequence2);
        if (this.mEmptyText == null) {
            this.mList.setEmptyView(this.mStandardEmptyView);
        }
        this.mEmptyText = charSequence2;
    }

    public void setListShown(boolean z) {
        setListShown(z, true);
    }

    public void setListShownNoAnimation(boolean z) {
        setListShown(z, false);
    }

    private void setListShown(boolean z, boolean z2) {
        Throwable th;
        boolean z3 = z;
        boolean z4 = z2;
        ensureList();
        if (this.mProgressContainer == null) {
            Throwable th2 = th;
            new IllegalStateException("Can't be used with a custom content view");
            throw th2;
        } else if (this.mListShown != z3) {
            this.mListShown = z3;
            if (z3) {
                if (z4) {
                    this.mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), 17432577));
                    this.mListContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), 17432576));
                } else {
                    this.mProgressContainer.clearAnimation();
                    this.mListContainer.clearAnimation();
                }
                this.mProgressContainer.setVisibility(8);
                this.mListContainer.setVisibility(0);
                return;
            }
            if (z4) {
                this.mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), 17432576));
                this.mListContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), 17432577));
            } else {
                this.mProgressContainer.clearAnimation();
                this.mListContainer.clearAnimation();
            }
            this.mProgressContainer.setVisibility(0);
            this.mListContainer.setVisibility(8);
        }
    }

    public ListAdapter getListAdapter() {
        return this.mAdapter;
    }

    private void ensureList() {
        Throwable th;
        Throwable th2;
        Throwable th3;
        if (this.mList == null) {
            View view = getView();
            if (view == null) {
                Throwable th4 = th3;
                new IllegalStateException("Content view not yet created");
                throw th4;
            }
            if (view instanceof ListView) {
                this.mList = (ListView) view;
            } else {
                this.mStandardEmptyView = (TextView) view.findViewById(INTERNAL_EMPTY_ID);
                if (this.mStandardEmptyView == null) {
                    this.mEmptyView = view.findViewById(16908292);
                } else {
                    this.mStandardEmptyView.setVisibility(8);
                }
                this.mProgressContainer = view.findViewById(INTERNAL_PROGRESS_CONTAINER_ID);
                this.mListContainer = view.findViewById(INTERNAL_LIST_CONTAINER_ID);
                View findViewById = view.findViewById(16908298);
                if (findViewById instanceof ListView) {
                    this.mList = (ListView) findViewById;
                    if (this.mEmptyView != null) {
                        this.mList.setEmptyView(this.mEmptyView);
                    } else if (this.mEmptyText != null) {
                        this.mStandardEmptyView.setText(this.mEmptyText);
                        this.mList.setEmptyView(this.mStandardEmptyView);
                    }
                } else if (findViewById == null) {
                    Throwable th5 = th2;
                    new RuntimeException("Your content must have a ListView whose id attribute is 'android.R.id.list'");
                    throw th5;
                } else {
                    Throwable th6 = th;
                    new RuntimeException("Content has view with id attribute 'android.R.id.list' that is not a ListView class");
                    throw th6;
                }
            }
            this.mListShown = true;
            this.mList.setOnItemClickListener(this.mOnClickListener);
            if (this.mAdapter != null) {
                this.mAdapter = null;
                setListAdapter(this.mAdapter);
            } else if (this.mProgressContainer != null) {
                setListShown(false, false);
            }
            boolean post = this.mHandler.post(this.mRequestFocus);
        }
    }
}
