package android.support.v4.os;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.os.IResultReceiver;

public class ResultReceiver implements Parcelable {
    public static final Parcelable.Creator<ResultReceiver> CREATOR;
    final Handler mHandler;
    final boolean mLocal;
    IResultReceiver mReceiver;

    class MyRunnable implements Runnable {
        final int mResultCode;
        final Bundle mResultData;

        MyRunnable(int i, Bundle bundle) {
            this.mResultCode = i;
            this.mResultData = bundle;
        }

        public void run() {
            ResultReceiver.this.onReceiveResult(this.mResultCode, this.mResultData);
        }
    }

    class MyResultReceiver extends IResultReceiver.Stub {
        MyResultReceiver() {
        }

        public void send(int i, Bundle bundle) {
            Runnable runnable;
            int i2 = i;
            Bundle bundle2 = bundle;
            if (ResultReceiver.this.mHandler != null) {
                new MyRunnable(i2, bundle2);
                boolean post = ResultReceiver.this.mHandler.post(runnable);
                return;
            }
            ResultReceiver.this.onReceiveResult(i2, bundle2);
        }
    }

    public ResultReceiver(Handler handler) {
        this.mLocal = true;
        this.mHandler = handler;
    }

    public void send(int i, Bundle bundle) {
        Runnable runnable;
        int i2 = i;
        Bundle bundle2 = bundle;
        if (this.mLocal) {
            if (this.mHandler != null) {
                new MyRunnable(i2, bundle2);
                boolean post = this.mHandler.post(runnable);
                return;
            }
            onReceiveResult(i2, bundle2);
        } else if (this.mReceiver != null) {
            try {
                this.mReceiver.send(i2, bundle2);
            } catch (RemoteException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onReceiveResult(int i, Bundle bundle) {
    }

    public int describeContents() {
        return 0;
    }

    /* JADX INFO: finally extract failed */
    public void writeToParcel(Parcel parcel, int i) {
        IResultReceiver iResultReceiver;
        Parcel parcel2 = parcel;
        synchronized (this) {
            try {
                if (this.mReceiver == null) {
                    new MyResultReceiver();
                    this.mReceiver = iResultReceiver;
                }
                parcel2.writeStrongBinder(this.mReceiver.asBinder());
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    ResultReceiver(Parcel parcel) {
        this.mLocal = false;
        this.mHandler = null;
        this.mReceiver = IResultReceiver.Stub.asInterface(parcel.readStrongBinder());
    }

    static {
        Parcelable.Creator<ResultReceiver> creator;
        new Parcelable.Creator<ResultReceiver>() {
            public ResultReceiver createFromParcel(Parcel parcel) {
                ResultReceiver resultReceiver;
                new ResultReceiver(parcel);
                return resultReceiver;
            }

            public ResultReceiver[] newArray(int i) {
                return new ResultReceiver[i];
            }
        };
        CREATOR = creator;
    }
}
