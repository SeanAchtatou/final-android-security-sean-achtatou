package android.support.v4.media;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.media.IMediaBrowserServiceCompat;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.os.ResultReceiver;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;

public abstract class MediaBrowserServiceCompat extends Service {
    private static final boolean DBG = false;
    public static final String KEY_MEDIA_ITEM = "media_item";
    public static final String SERVICE_INTERFACE = "android.media.browse.MediaBrowserServiceCompat";
    private static final String TAG = "MediaBrowserServiceCompat";
    private ServiceBinder mBinder;
    /* access modifiers changed from: private */
    public final ArrayMap<IBinder, ConnectionRecord> mConnections;
    /* access modifiers changed from: private */
    public final Handler mHandler;
    MediaSessionCompat.Token mSession;

    @Nullable
    public abstract BrowserRoot onGetRoot(@NonNull String str, int i, @Nullable Bundle bundle);

    public abstract void onLoadChildren(@NonNull String str, @NonNull Result<List<MediaBrowserCompat.MediaItem>> result);

    public MediaBrowserServiceCompat() {
        ArrayMap<IBinder, ConnectionRecord> arrayMap;
        Handler handler;
        new ArrayMap<>();
        this.mConnections = arrayMap;
        new Handler();
        this.mHandler = handler;
    }

    private class ConnectionRecord {
        IMediaBrowserServiceCompatCallbacks callbacks;
        String pkg;
        BrowserRoot root;
        Bundle rootHints;
        HashSet<String> subscriptions;

        private ConnectionRecord() {
            HashSet<String> hashSet;
            new HashSet<>();
            this.subscriptions = hashSet;
        }
    }

    public class Result<T> {
        private Object mDebug;
        private boolean mDetachCalled;
        private boolean mSendResultCalled;

        Result(Object obj) {
            this.mDebug = obj;
        }

        public void sendResult(T t) {
            Throwable th;
            StringBuilder sb;
            T t2 = t;
            if (this.mSendResultCalled) {
                Throwable th2 = th;
                new StringBuilder();
                new IllegalStateException(sb.append("sendResult() called twice for: ").append(this.mDebug).toString());
                throw th2;
            }
            this.mSendResultCalled = true;
            onResultSent(t2);
        }

        public void detach() {
            Throwable th;
            StringBuilder sb;
            Throwable th2;
            StringBuilder sb2;
            if (this.mDetachCalled) {
                Throwable th3 = th2;
                new StringBuilder();
                new IllegalStateException(sb2.append("detach() called when detach() had already been called for: ").append(this.mDebug).toString());
                throw th3;
            } else if (this.mSendResultCalled) {
                Throwable th4 = th;
                new StringBuilder();
                new IllegalStateException(sb.append("detach() called when sendResult() had already been called for: ").append(this.mDebug).toString());
                throw th4;
            } else {
                this.mDetachCalled = true;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean isDone() {
            return this.mDetachCalled || this.mSendResultCalled;
        }

        /* access modifiers changed from: package-private */
        public void onResultSent(T t) {
        }
    }

    private class ServiceBinder extends IMediaBrowserServiceCompat.Stub {
        private ServiceBinder() {
        }

        public void connect(String str, Bundle bundle, IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks) {
            Runnable runnable;
            Throwable th;
            StringBuilder sb;
            String str2 = str;
            Bundle bundle2 = bundle;
            IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks2 = iMediaBrowserServiceCompatCallbacks;
            int callingUid = Binder.getCallingUid();
            if (!MediaBrowserServiceCompat.this.isValidPackage(str2, callingUid)) {
                Throwable th2 = th;
                new StringBuilder();
                new IllegalArgumentException(sb.append("Package/uid mismatch: uid=").append(callingUid).append(" package=").append(str2).toString());
                throw th2;
            }
            final IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks3 = iMediaBrowserServiceCompatCallbacks2;
            final String str3 = str2;
            final Bundle bundle3 = bundle2;
            final int i = callingUid;
            new Runnable() {
                public void run() {
                    ConnectionRecord connectionRecord;
                    StringBuilder sb;
                    StringBuilder sb2;
                    StringBuilder sb3;
                    IBinder asBinder = iMediaBrowserServiceCompatCallbacks3.asBinder();
                    Object remove = MediaBrowserServiceCompat.this.mConnections.remove(asBinder);
                    new ConnectionRecord();
                    ConnectionRecord connectionRecord2 = connectionRecord;
                    connectionRecord2.pkg = str3;
                    connectionRecord2.rootHints = bundle3;
                    connectionRecord2.callbacks = iMediaBrowserServiceCompatCallbacks3;
                    connectionRecord2.root = MediaBrowserServiceCompat.this.onGetRoot(str3, i, bundle3);
                    if (connectionRecord2.root == null) {
                        new StringBuilder();
                        int i = Log.i(MediaBrowserServiceCompat.TAG, sb2.append("No root for client ").append(str3).append(" from service ").append(getClass().getName()).toString());
                        try {
                            iMediaBrowserServiceCompatCallbacks3.onConnectFailed();
                        } catch (RemoteException e) {
                            new StringBuilder();
                            int w = Log.w(MediaBrowserServiceCompat.TAG, sb3.append("Calling onConnectFailed() failed. Ignoring. pkg=").append(str3).toString());
                        }
                    } else {
                        try {
                            Object put = MediaBrowserServiceCompat.this.mConnections.put(asBinder, connectionRecord2);
                            if (MediaBrowserServiceCompat.this.mSession != null) {
                                iMediaBrowserServiceCompatCallbacks3.onConnect(connectionRecord2.root.getRootId(), MediaBrowserServiceCompat.this.mSession, connectionRecord2.root.getExtras());
                            }
                        } catch (RemoteException e2) {
                            new StringBuilder();
                            int w2 = Log.w(MediaBrowserServiceCompat.TAG, sb.append("Calling onConnect() failed. Dropping client. pkg=").append(str3).toString());
                            Object remove2 = MediaBrowserServiceCompat.this.mConnections.remove(asBinder);
                        }
                    }
                }
            };
            boolean post = MediaBrowserServiceCompat.this.mHandler.post(runnable);
        }

        public void disconnect(IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks) {
            Runnable runnable;
            final IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks2 = iMediaBrowserServiceCompatCallbacks;
            new Runnable() {
                public void run() {
                    if (((ConnectionRecord) MediaBrowserServiceCompat.this.mConnections.remove(iMediaBrowserServiceCompatCallbacks2.asBinder())) != null) {
                    }
                }
            };
            boolean post = MediaBrowserServiceCompat.this.mHandler.post(runnable);
        }

        public void addSubscription(String str, IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks) {
            Runnable runnable;
            final IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks2 = iMediaBrowserServiceCompatCallbacks;
            final String str2 = str;
            new Runnable() {
                public void run() {
                    StringBuilder sb;
                    ConnectionRecord connectionRecord = (ConnectionRecord) MediaBrowserServiceCompat.this.mConnections.get(iMediaBrowserServiceCompatCallbacks2.asBinder());
                    if (connectionRecord == null) {
                        new StringBuilder();
                        int w = Log.w(MediaBrowserServiceCompat.TAG, sb.append("addSubscription for callback that isn't registered id=").append(str2).toString());
                        return;
                    }
                    MediaBrowserServiceCompat.this.addSubscription(str2, connectionRecord);
                }
            };
            boolean post = MediaBrowserServiceCompat.this.mHandler.post(runnable);
        }

        public void removeSubscription(String str, IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks) {
            Runnable runnable;
            final IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks2 = iMediaBrowserServiceCompatCallbacks;
            final String str2 = str;
            new Runnable() {
                public void run() {
                    StringBuilder sb;
                    StringBuilder sb2;
                    ConnectionRecord connectionRecord = (ConnectionRecord) MediaBrowserServiceCompat.this.mConnections.get(iMediaBrowserServiceCompatCallbacks2.asBinder());
                    if (connectionRecord == null) {
                        new StringBuilder();
                        int w = Log.w(MediaBrowserServiceCompat.TAG, sb2.append("removeSubscription for callback that isn't registered id=").append(str2).toString());
                    } else if (!connectionRecord.subscriptions.remove(str2)) {
                        new StringBuilder();
                        int w2 = Log.w(MediaBrowserServiceCompat.TAG, sb.append("removeSubscription called for ").append(str2).append(" which is not subscribed").toString());
                    }
                }
            };
            boolean post = MediaBrowserServiceCompat.this.mHandler.post(runnable);
        }

        public void getMediaItem(String str, ResultReceiver resultReceiver) {
            Runnable runnable;
            String str2 = str;
            ResultReceiver resultReceiver2 = resultReceiver;
            if (!TextUtils.isEmpty(str2) && resultReceiver2 != null) {
                final String str3 = str2;
                final ResultReceiver resultReceiver3 = resultReceiver2;
                new Runnable() {
                    public void run() {
                        MediaBrowserServiceCompat.this.performLoadItem(str3, resultReceiver3);
                    }
                };
                boolean post = MediaBrowserServiceCompat.this.mHandler.post(runnable);
            }
        }
    }

    public void onCreate() {
        ServiceBinder serviceBinder;
        super.onCreate();
        new ServiceBinder();
        this.mBinder = serviceBinder;
    }

    public IBinder onBind(Intent intent) {
        if (SERVICE_INTERFACE.equals(intent.getAction())) {
            return this.mBinder;
        }
        return null;
    }

    public void dump(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    public void onLoadItem(String str, Result<MediaBrowserCompat.MediaItem> result) {
        result.sendResult(null);
    }

    public void setSessionToken(MediaSessionCompat.Token token) {
        Runnable runnable;
        Throwable th;
        Throwable th2;
        MediaSessionCompat.Token token2 = token;
        if (token2 == null) {
            Throwable th3 = th2;
            new IllegalArgumentException("Session token may not be null.");
            throw th3;
        } else if (this.mSession != null) {
            Throwable th4 = th;
            new IllegalStateException("The session token has already been set.");
            throw th4;
        } else {
            this.mSession = token2;
            final MediaSessionCompat.Token token3 = token2;
            new Runnable() {
                public void run() {
                    StringBuilder sb;
                    for (IBinder iBinder : MediaBrowserServiceCompat.this.mConnections.keySet()) {
                        ConnectionRecord connectionRecord = (ConnectionRecord) MediaBrowserServiceCompat.this.mConnections.get(iBinder);
                        try {
                            connectionRecord.callbacks.onConnect(connectionRecord.root.getRootId(), token3, connectionRecord.root.getExtras());
                        } catch (RemoteException e) {
                            new StringBuilder();
                            int w = Log.w(MediaBrowserServiceCompat.TAG, sb.append("Connection for ").append(connectionRecord.pkg).append(" is no longer valid.").toString());
                            Object remove = MediaBrowserServiceCompat.this.mConnections.remove(iBinder);
                        }
                    }
                }
            };
            boolean post = this.mHandler.post(runnable);
        }
    }

    @Nullable
    public MediaSessionCompat.Token getSessionToken() {
        return this.mSession;
    }

    public void notifyChildrenChanged(@NonNull String str) {
        Runnable runnable;
        Throwable th;
        String str2 = str;
        if (str2 == null) {
            Throwable th2 = th;
            new IllegalArgumentException("parentId cannot be null in notifyChildrenChanged");
            throw th2;
        }
        final String str3 = str2;
        new Runnable() {
            public void run() {
                for (IBinder iBinder : MediaBrowserServiceCompat.this.mConnections.keySet()) {
                    ConnectionRecord connectionRecord = (ConnectionRecord) MediaBrowserServiceCompat.this.mConnections.get(iBinder);
                    if (connectionRecord.subscriptions.contains(str3)) {
                        MediaBrowserServiceCompat.this.performLoadChildren(str3, connectionRecord);
                    }
                }
            }
        };
        boolean post = this.mHandler.post(runnable);
    }

    /* access modifiers changed from: private */
    public boolean isValidPackage(String str, int i) {
        String str2 = str;
        int i2 = i;
        if (str2 == null) {
            return false;
        }
        String[] packagesForUid = getPackageManager().getPackagesForUid(i2);
        int length = packagesForUid.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (packagesForUid[i3].equals(str2)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void addSubscription(String str, ConnectionRecord connectionRecord) {
        String str2 = str;
        ConnectionRecord connectionRecord2 = connectionRecord;
        boolean add = connectionRecord2.subscriptions.add(str2);
        performLoadChildren(str2, connectionRecord2);
    }

    /* access modifiers changed from: private */
    public void performLoadChildren(String str, ConnectionRecord connectionRecord) {
        Result result;
        Throwable th;
        StringBuilder sb;
        String str2 = str;
        ConnectionRecord connectionRecord2 = connectionRecord;
        final String str3 = str2;
        final ConnectionRecord connectionRecord3 = connectionRecord2;
        new Result<List<MediaBrowserCompat.MediaItem>>(this, str2) {
            final /* synthetic */ MediaBrowserServiceCompat this$0;

            {
                MediaBrowserServiceCompat mediaBrowserServiceCompat = r9;
                this.this$0 = mediaBrowserServiceCompat;
            }

            /* access modifiers changed from: package-private */
            public /* bridge */ /* synthetic */ void onResultSent(Object obj) {
                onResultSent((List<MediaBrowserCompat.MediaItem>) ((List) obj));
            }

            /* access modifiers changed from: package-private */
            public void onResultSent(List<MediaBrowserCompat.MediaItem> list) {
                StringBuilder sb;
                Throwable th;
                StringBuilder sb2;
                List<MediaBrowserCompat.MediaItem> list2 = list;
                if (list2 == null) {
                    Throwable th2 = th;
                    new StringBuilder();
                    new IllegalStateException(sb2.append("onLoadChildren sent null list for id ").append(str3).toString());
                    throw th2;
                } else if (this.this$0.mConnections.get(connectionRecord3.callbacks.asBinder()) == connectionRecord3) {
                    try {
                        connectionRecord3.callbacks.onLoadChildren(str3, list2);
                    } catch (RemoteException e) {
                        new StringBuilder();
                        int w = Log.w(MediaBrowserServiceCompat.TAG, sb.append("Calling onLoadChildren() failed for id=").append(str3).append(" package=").append(connectionRecord3.pkg).toString());
                    }
                }
            }
        };
        Result result2 = result;
        onLoadChildren(str2, result2);
        if (!result2.isDone()) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("onLoadChildren must call detach() or sendResult() before returning for package=").append(connectionRecord2.pkg).append(" id=").append(str2).toString());
            throw th2;
        }
    }

    /* access modifiers changed from: private */
    public void performLoadItem(String str, ResultReceiver resultReceiver) {
        Result result;
        Throwable th;
        StringBuilder sb;
        String str2 = str;
        final ResultReceiver resultReceiver2 = resultReceiver;
        new Result<MediaBrowserCompat.MediaItem>(this, str2) {
            final /* synthetic */ MediaBrowserServiceCompat this$0;

            {
                MediaBrowserServiceCompat mediaBrowserServiceCompat = r8;
                this.this$0 = mediaBrowserServiceCompat;
            }

            /* access modifiers changed from: package-private */
            public void onResultSent(MediaBrowserCompat.MediaItem mediaItem) {
                Bundle bundle;
                new Bundle();
                Bundle bundle2 = bundle;
                bundle2.putParcelable(MediaBrowserServiceCompat.KEY_MEDIA_ITEM, mediaItem);
                resultReceiver2.send(0, bundle2);
            }
        };
        Result result2 = result;
        onLoadItem(str2, result2);
        if (!result2.isDone()) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("onLoadItem must call detach() or sendResult() before returning for id=").append(str2).toString());
            throw th2;
        }
    }

    public static final class BrowserRoot {
        private final Bundle mExtras;
        private final String mRootId;

        public BrowserRoot(@NonNull String str, @Nullable Bundle bundle) {
            Throwable th;
            String str2 = str;
            Bundle bundle2 = bundle;
            if (str2 == null) {
                Throwable th2 = th;
                new IllegalArgumentException("The root id in BrowserRoot cannot be null. Use null for BrowserRoot instead.");
                throw th2;
            }
            this.mRootId = str2;
            this.mExtras = bundle2;
        }

        public String getRootId() {
            return this.mRootId;
        }

        public Bundle getExtras() {
            return this.mExtras;
        }
    }
}
