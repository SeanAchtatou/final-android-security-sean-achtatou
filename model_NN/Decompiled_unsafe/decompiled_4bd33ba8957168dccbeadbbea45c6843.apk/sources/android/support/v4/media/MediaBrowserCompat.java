package android.support.v4.media;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.media.IMediaBrowserServiceCompat;
import android.support.v4.media.IMediaBrowserServiceCompatCallbacks;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.os.ResultReceiver;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

public final class MediaBrowserCompat {
    private final MediaBrowserImplBase mImpl;

    public MediaBrowserCompat(Context context, ComponentName componentName, ConnectionCallback connectionCallback, Bundle bundle) {
        MediaBrowserImplBase mediaBrowserImplBase;
        new MediaBrowserImplBase(context, componentName, connectionCallback, bundle);
        this.mImpl = mediaBrowserImplBase;
    }

    public void connect() {
        this.mImpl.connect();
    }

    public void disconnect() {
        this.mImpl.disconnect();
    }

    public boolean isConnected() {
        return this.mImpl.isConnected();
    }

    @NonNull
    public ComponentName getServiceComponent() {
        return this.mImpl.getServiceComponent();
    }

    @NonNull
    public String getRoot() {
        return this.mImpl.getRoot();
    }

    @Nullable
    public Bundle getExtras() {
        return this.mImpl.getExtras();
    }

    @NonNull
    public MediaSessionCompat.Token getSessionToken() {
        return this.mImpl.getSessionToken();
    }

    public void subscribe(@NonNull String str, @NonNull SubscriptionCallback subscriptionCallback) {
        this.mImpl.subscribe(str, subscriptionCallback);
    }

    public void unsubscribe(@NonNull String str) {
        this.mImpl.unsubscribe(str);
    }

    public void getItem(@NonNull String str, @NonNull ItemCallback itemCallback) {
        this.mImpl.getItem(str, itemCallback);
    }

    public static class MediaItem implements Parcelable {
        public static final Parcelable.Creator<MediaItem> CREATOR;
        public static final int FLAG_BROWSABLE = 1;
        public static final int FLAG_PLAYABLE = 2;
        private final MediaDescriptionCompat mDescription;
        private final int mFlags;

        @Retention(RetentionPolicy.SOURCE)
        public @interface Flags {
        }

        public MediaItem(@NonNull MediaDescriptionCompat mediaDescriptionCompat, int i) {
            Throwable th;
            Throwable th2;
            MediaDescriptionCompat mediaDescriptionCompat2 = mediaDescriptionCompat;
            int i2 = i;
            if (mediaDescriptionCompat2 == null) {
                Throwable th3 = th2;
                new IllegalArgumentException("description cannot be null");
                throw th3;
            } else if (TextUtils.isEmpty(mediaDescriptionCompat2.getMediaId())) {
                Throwable th4 = th;
                new IllegalArgumentException("description must have a non-empty media id");
                throw th4;
            } else {
                this.mFlags = i2;
                this.mDescription = mediaDescriptionCompat2;
            }
        }

        private MediaItem(Parcel parcel) {
            Parcel parcel2 = parcel;
            this.mFlags = parcel2.readInt();
            this.mDescription = MediaDescriptionCompat.CREATOR.createFromParcel(parcel2);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            parcel2.writeInt(this.mFlags);
            this.mDescription.writeToParcel(parcel2, i);
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder("MediaItem{");
            StringBuilder sb2 = sb;
            StringBuilder append = sb2.append("mFlags=").append(this.mFlags);
            StringBuilder append2 = sb2.append(", mDescription=").append(this.mDescription);
            StringBuilder append3 = sb2.append('}');
            return sb2.toString();
        }

        static {
            Parcelable.Creator<MediaItem> creator;
            new Parcelable.Creator<MediaItem>() {
                public MediaItem createFromParcel(Parcel parcel) {
                    MediaItem mediaItem;
                    new MediaItem(parcel);
                    return mediaItem;
                }

                public MediaItem[] newArray(int i) {
                    return new MediaItem[i];
                }
            };
            CREATOR = creator;
        }

        public int getFlags() {
            return this.mFlags;
        }

        public boolean isBrowsable() {
            return (this.mFlags & 1) != 0;
        }

        public boolean isPlayable() {
            return (this.mFlags & 2) != 0;
        }

        @NonNull
        public MediaDescriptionCompat getDescription() {
            return this.mDescription;
        }

        @NonNull
        public String getMediaId() {
            return this.mDescription.getMediaId();
        }
    }

    public static class ConnectionCallback {
        public void onConnected() {
        }

        public void onConnectionSuspended() {
        }

        public void onConnectionFailed() {
        }
    }

    public static abstract class SubscriptionCallback {
        public void onChildrenLoaded(@NonNull String str, @NonNull List<MediaItem> list) {
        }

        public void onError(@NonNull String str) {
        }
    }

    public static abstract class ItemCallback {
        public void onItemLoaded(MediaItem mediaItem) {
        }

        public void onError(@NonNull String str) {
        }
    }

    static class MediaBrowserImplBase {
        private static final int CONNECT_STATE_CONNECTED = 2;
        private static final int CONNECT_STATE_CONNECTING = 1;
        private static final int CONNECT_STATE_DISCONNECTED = 0;
        private static final int CONNECT_STATE_SUSPENDED = 3;
        private static final boolean DBG = false;
        private static final String TAG = "MediaBrowserCompat";
        /* access modifiers changed from: private */
        public final ConnectionCallback mCallback;
        /* access modifiers changed from: private */
        public final Context mContext;
        private Bundle mExtras;
        private final Handler mHandler;
        private MediaSessionCompat.Token mMediaSessionToken;
        /* access modifiers changed from: private */
        public final Bundle mRootHints;
        private String mRootId;
        /* access modifiers changed from: private */
        public IMediaBrowserServiceCompat mServiceBinder;
        /* access modifiers changed from: private */
        public IMediaBrowserServiceCompatCallbacks mServiceCallbacks;
        /* access modifiers changed from: private */
        public final ComponentName mServiceComponent;
        /* access modifiers changed from: private */
        public MediaServiceConnection mServiceConnection;
        /* access modifiers changed from: private */
        public int mState = 0;
        /* access modifiers changed from: private */
        public final ArrayMap<String, Subscription> mSubscriptions;

        static /* synthetic */ Bundle access$1002(MediaBrowserImplBase mediaBrowserImplBase, Bundle bundle) {
            Bundle bundle2 = bundle;
            Bundle bundle3 = bundle2;
            mediaBrowserImplBase.mExtras = bundle3;
            return bundle2;
        }

        static /* synthetic */ IMediaBrowserServiceCompatCallbacks access$1202(MediaBrowserImplBase mediaBrowserImplBase, IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks) {
            IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks2 = iMediaBrowserServiceCompatCallbacks;
            IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks3 = iMediaBrowserServiceCompatCallbacks2;
            mediaBrowserImplBase.mServiceCallbacks = iMediaBrowserServiceCompatCallbacks3;
            return iMediaBrowserServiceCompatCallbacks2;
        }

        static /* synthetic */ IMediaBrowserServiceCompat access$1302(MediaBrowserImplBase mediaBrowserImplBase, IMediaBrowserServiceCompat iMediaBrowserServiceCompat) {
            IMediaBrowserServiceCompat iMediaBrowserServiceCompat2 = iMediaBrowserServiceCompat;
            IMediaBrowserServiceCompat iMediaBrowserServiceCompat3 = iMediaBrowserServiceCompat2;
            mediaBrowserImplBase.mServiceBinder = iMediaBrowserServiceCompat3;
            return iMediaBrowserServiceCompat2;
        }

        static /* synthetic */ int access$602(MediaBrowserImplBase mediaBrowserImplBase, int i) {
            int i2 = i;
            int i3 = i2;
            mediaBrowserImplBase.mState = i3;
            return i2;
        }

        static /* synthetic */ String access$802(MediaBrowserImplBase mediaBrowserImplBase, String str) {
            String str2 = str;
            String str3 = str2;
            mediaBrowserImplBase.mRootId = str3;
            return str2;
        }

        static /* synthetic */ MediaSessionCompat.Token access$902(MediaBrowserImplBase mediaBrowserImplBase, MediaSessionCompat.Token token) {
            MediaSessionCompat.Token token2 = token;
            MediaSessionCompat.Token token3 = token2;
            mediaBrowserImplBase.mMediaSessionToken = token3;
            return token2;
        }

        public MediaBrowserImplBase(Context context, ComponentName componentName, ConnectionCallback connectionCallback, Bundle bundle) {
            Handler handler;
            ArrayMap<String, Subscription> arrayMap;
            Throwable th;
            Throwable th2;
            Throwable th3;
            Context context2 = context;
            ComponentName componentName2 = componentName;
            ConnectionCallback connectionCallback2 = connectionCallback;
            Bundle bundle2 = bundle;
            new Handler();
            this.mHandler = handler;
            new ArrayMap<>();
            this.mSubscriptions = arrayMap;
            if (context2 == null) {
                Throwable th4 = th3;
                new IllegalArgumentException("context must not be null");
                throw th4;
            } else if (componentName2 == null) {
                Throwable th5 = th2;
                new IllegalArgumentException("service component must not be null");
                throw th5;
            } else if (connectionCallback2 == null) {
                Throwable th6 = th;
                new IllegalArgumentException("connection callback must not be null");
                throw th6;
            } else {
                this.mContext = context2;
                this.mServiceComponent = componentName2;
                this.mCallback = connectionCallback2;
                this.mRootHints = bundle2;
            }
        }

        public void connect() {
            Intent intent;
            MediaServiceConnection mediaServiceConnection;
            StringBuilder sb;
            Runnable runnable;
            Throwable th;
            StringBuilder sb2;
            Throwable th2;
            StringBuilder sb3;
            Throwable th3;
            StringBuilder sb4;
            if (this.mState != 0) {
                Throwable th4 = th3;
                new StringBuilder();
                new IllegalStateException(sb4.append("connect() called while not disconnected (state=").append(getStateLabel(this.mState)).append(")").toString());
                throw th4;
            } else if (this.mServiceBinder != null) {
                Throwable th5 = th2;
                new StringBuilder();
                new RuntimeException(sb3.append("mServiceBinder should be null. Instead it is ").append(this.mServiceBinder).toString());
                throw th5;
            } else if (this.mServiceCallbacks != null) {
                Throwable th6 = th;
                new StringBuilder();
                new RuntimeException(sb2.append("mServiceCallbacks should be null. Instead it is ").append(this.mServiceCallbacks).toString());
                throw th6;
            } else {
                this.mState = 1;
                new Intent(MediaBrowserServiceCompat.SERVICE_INTERFACE);
                Intent intent2 = intent;
                Intent component = intent2.setComponent(this.mServiceComponent);
                new MediaServiceConnection();
                MediaServiceConnection mediaServiceConnection2 = mediaServiceConnection;
                this.mServiceConnection = mediaServiceConnection2;
                MediaServiceConnection mediaServiceConnection3 = mediaServiceConnection2;
                boolean z = false;
                try {
                    z = this.mContext.bindService(intent2, this.mServiceConnection, 1);
                } catch (Exception e) {
                    new StringBuilder();
                    int e2 = Log.e(TAG, sb.append("Failed binding to service ").append(this.mServiceComponent).toString());
                }
                if (!z) {
                    final MediaServiceConnection mediaServiceConnection4 = mediaServiceConnection3;
                    new Runnable() {
                        public void run() {
                            if (mediaServiceConnection4 == MediaBrowserImplBase.this.mServiceConnection) {
                                MediaBrowserImplBase.this.forceCloseConnection();
                                MediaBrowserImplBase.this.mCallback.onConnectionFailed();
                            }
                        }
                    };
                    boolean post = this.mHandler.post(runnable);
                }
            }
        }

        public void disconnect() {
            StringBuilder sb;
            if (this.mServiceCallbacks != null) {
                try {
                    this.mServiceBinder.disconnect(this.mServiceCallbacks);
                } catch (RemoteException e) {
                    new StringBuilder();
                    int w = Log.w(TAG, sb.append("RemoteException during connect for ").append(this.mServiceComponent).toString());
                }
            }
            forceCloseConnection();
        }

        /* access modifiers changed from: private */
        public void forceCloseConnection() {
            if (this.mServiceConnection != null) {
                this.mContext.unbindService(this.mServiceConnection);
            }
            this.mState = 0;
            this.mServiceConnection = null;
            this.mServiceBinder = null;
            this.mServiceCallbacks = null;
            this.mRootId = null;
            this.mMediaSessionToken = null;
        }

        public boolean isConnected() {
            return this.mState == 2;
        }

        @NonNull
        public ComponentName getServiceComponent() {
            Throwable th;
            StringBuilder sb;
            if (isConnected()) {
                return this.mServiceComponent;
            }
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("getServiceComponent() called while not connected (state=").append(this.mState).append(")").toString());
            throw th2;
        }

        @NonNull
        public String getRoot() {
            Throwable th;
            StringBuilder sb;
            if (isConnected()) {
                return this.mRootId;
            }
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("getSessionToken() called while not connected(state=").append(getStateLabel(this.mState)).append(")").toString());
            throw th2;
        }

        @Nullable
        public Bundle getExtras() {
            Throwable th;
            StringBuilder sb;
            if (isConnected()) {
                return this.mExtras;
            }
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("getExtras() called while not connected (state=").append(getStateLabel(this.mState)).append(")").toString());
            throw th2;
        }

        @NonNull
        public MediaSessionCompat.Token getSessionToken() {
            Throwable th;
            StringBuilder sb;
            if (isConnected()) {
                return this.mMediaSessionToken;
            }
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("getSessionToken() called while not connected(state=").append(this.mState).append(")").toString());
            throw th2;
        }

        public void subscribe(@NonNull String str, @NonNull SubscriptionCallback subscriptionCallback) {
            StringBuilder sb;
            Subscription subscription;
            Throwable th;
            Throwable th2;
            String str2 = str;
            SubscriptionCallback subscriptionCallback2 = subscriptionCallback;
            if (str2 == null) {
                Throwable th3 = th2;
                new IllegalArgumentException("parentId is null");
                throw th3;
            } else if (subscriptionCallback2 == null) {
                Throwable th4 = th;
                new IllegalArgumentException("callback is null");
                throw th4;
            } else {
                Subscription subscription2 = this.mSubscriptions.get(str2);
                if (subscription2 == null) {
                    new Subscription(str2);
                    subscription2 = subscription;
                    Subscription put = this.mSubscriptions.put(str2, subscription2);
                }
                subscription2.callback = subscriptionCallback2;
                if (this.mState == 2) {
                    try {
                        this.mServiceBinder.addSubscription(str2, this.mServiceCallbacks);
                    } catch (RemoteException e) {
                        new StringBuilder();
                        int d = Log.d(TAG, sb.append("addSubscription failed with RemoteException parentId=").append(str2).toString());
                    }
                }
            }
        }

        public void unsubscribe(@NonNull String str) {
            StringBuilder sb;
            Throwable th;
            String str2 = str;
            if (TextUtils.isEmpty(str2)) {
                Throwable th2 = th;
                new IllegalArgumentException("parentId is empty.");
                throw th2;
            }
            Subscription remove = this.mSubscriptions.remove(str2);
            if (this.mState == 2 && remove != null) {
                try {
                    this.mServiceBinder.removeSubscription(str2, this.mServiceCallbacks);
                } catch (RemoteException e) {
                    new StringBuilder();
                    int d = Log.d(TAG, sb.append("removeSubscription failed with RemoteException parentId=").append(str2).toString());
                }
            }
        }

        public void getItem(@NonNull String str, @NonNull ItemCallback itemCallback) {
            ResultReceiver resultReceiver;
            Runnable runnable;
            Runnable runnable2;
            Throwable th;
            Throwable th2;
            String str2 = str;
            ItemCallback itemCallback2 = itemCallback;
            if (TextUtils.isEmpty(str2)) {
                Throwable th3 = th2;
                new IllegalArgumentException("mediaId is empty.");
                throw th3;
            } else if (itemCallback2 == null) {
                Throwable th4 = th;
                new IllegalArgumentException("cb is null.");
                throw th4;
            } else if (this.mState != 2) {
                int i = Log.i(TAG, "Not connected, unable to retrieve the MediaItem.");
                final ItemCallback itemCallback3 = itemCallback2;
                final String str3 = str2;
                new Runnable() {
                    public void run() {
                        itemCallback3.onError(str3);
                    }
                };
                boolean post = this.mHandler.post(runnable2);
            } else {
                final ItemCallback itemCallback4 = itemCallback2;
                final String str4 = str2;
                new ResultReceiver(this.mHandler) {
                    /* access modifiers changed from: protected */
                    public void onReceiveResult(int i, Bundle bundle) {
                        Bundle bundle2 = bundle;
                        if (i != 0 || bundle2 == null || !bundle2.containsKey(MediaBrowserServiceCompat.KEY_MEDIA_ITEM)) {
                            itemCallback4.onError(str4);
                            return;
                        }
                        Parcelable parcelable = bundle2.getParcelable(MediaBrowserServiceCompat.KEY_MEDIA_ITEM);
                        if (!(parcelable instanceof MediaItem)) {
                            itemCallback4.onError(str4);
                        } else {
                            itemCallback4.onItemLoaded((MediaItem) parcelable);
                        }
                    }
                };
                try {
                    this.mServiceBinder.getMediaItem(str2, resultReceiver);
                } catch (RemoteException e) {
                    int i2 = Log.i(TAG, "Remote error getting media item.");
                    final ItemCallback itemCallback5 = itemCallback2;
                    final String str5 = str2;
                    new Runnable() {
                        public void run() {
                            itemCallback5.onError(str5);
                        }
                    };
                    boolean post2 = this.mHandler.post(runnable);
                }
            }
        }

        /* access modifiers changed from: private */
        public static String getStateLabel(int i) {
            StringBuilder sb;
            int i2 = i;
            switch (i2) {
                case 0:
                    return "CONNECT_STATE_DISCONNECTED";
                case 1:
                    return "CONNECT_STATE_CONNECTING";
                case 2:
                    return "CONNECT_STATE_CONNECTED";
                case 3:
                    return "CONNECT_STATE_SUSPENDED";
                default:
                    new StringBuilder();
                    return sb.append("UNKNOWN/").append(i2).toString();
            }
        }

        /* access modifiers changed from: private */
        public final void onServiceConnected(IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks, String str, MediaSessionCompat.Token token, Bundle bundle) {
            Runnable runnable;
            final IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks2 = iMediaBrowserServiceCompatCallbacks;
            final String str2 = str;
            final MediaSessionCompat.Token token2 = token;
            final Bundle bundle2 = bundle;
            new Runnable() {
                public void run() {
                    StringBuilder sb;
                    StringBuilder sb2;
                    if (MediaBrowserImplBase.this.isCurrent(iMediaBrowserServiceCompatCallbacks2, "onConnect")) {
                        if (MediaBrowserImplBase.this.mState != 1) {
                            new StringBuilder();
                            int w = Log.w(MediaBrowserImplBase.TAG, sb2.append("onConnect from service while mState=").append(MediaBrowserImplBase.getStateLabel(MediaBrowserImplBase.this.mState)).append("... ignoring").toString());
                            return;
                        }
                        String access$802 = MediaBrowserImplBase.access$802(MediaBrowserImplBase.this, str2);
                        MediaSessionCompat.Token access$902 = MediaBrowserImplBase.access$902(MediaBrowserImplBase.this, token2);
                        Bundle access$1002 = MediaBrowserImplBase.access$1002(MediaBrowserImplBase.this, bundle2);
                        int access$602 = MediaBrowserImplBase.access$602(MediaBrowserImplBase.this, 2);
                        MediaBrowserImplBase.this.mCallback.onConnected();
                        for (String str : MediaBrowserImplBase.this.mSubscriptions.keySet()) {
                            try {
                                MediaBrowserImplBase.this.mServiceBinder.addSubscription(str, MediaBrowserImplBase.this.mServiceCallbacks);
                            } catch (RemoteException e) {
                                new StringBuilder();
                                int d = Log.d(MediaBrowserImplBase.TAG, sb.append("addSubscription failed with RemoteException parentId=").append(str).toString());
                            }
                        }
                    }
                }
            };
            boolean post = this.mHandler.post(runnable);
        }

        /* access modifiers changed from: private */
        public final void onConnectionFailed(IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks) {
            Runnable runnable;
            final IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks2 = iMediaBrowserServiceCompatCallbacks;
            new Runnable() {
                public void run() {
                    StringBuilder sb;
                    StringBuilder sb2;
                    new StringBuilder();
                    int e = Log.e(MediaBrowserImplBase.TAG, sb.append("onConnectFailed for ").append(MediaBrowserImplBase.this.mServiceComponent).toString());
                    if (MediaBrowserImplBase.this.isCurrent(iMediaBrowserServiceCompatCallbacks2, "onConnectFailed")) {
                        if (MediaBrowserImplBase.this.mState != 1) {
                            new StringBuilder();
                            int w = Log.w(MediaBrowserImplBase.TAG, sb2.append("onConnect from service while mState=").append(MediaBrowserImplBase.getStateLabel(MediaBrowserImplBase.this.mState)).append("... ignoring").toString());
                            return;
                        }
                        MediaBrowserImplBase.this.forceCloseConnection();
                        MediaBrowserImplBase.this.mCallback.onConnectionFailed();
                    }
                }
            };
            boolean post = this.mHandler.post(runnable);
        }

        /* access modifiers changed from: private */
        public final void onLoadChildren(IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks, String str, List list) {
            Runnable runnable;
            final IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks2 = iMediaBrowserServiceCompatCallbacks;
            final List list2 = list;
            final String str2 = str;
            new Runnable() {
                public void run() {
                    if (MediaBrowserImplBase.this.isCurrent(iMediaBrowserServiceCompatCallbacks2, "onLoadChildren")) {
                        List list = list2;
                        if (list == null) {
                            list = Collections.emptyList();
                        }
                        Subscription subscription = (Subscription) MediaBrowserImplBase.this.mSubscriptions.get(str2);
                        if (subscription != null) {
                            subscription.callback.onChildrenLoaded(str2, list);
                        }
                    }
                }
            };
            boolean post = this.mHandler.post(runnable);
        }

        /* access modifiers changed from: private */
        public boolean isCurrent(IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks, String str) {
            StringBuilder sb;
            String str2 = str;
            if (this.mServiceCallbacks == iMediaBrowserServiceCompatCallbacks) {
                return true;
            }
            if (this.mState != 0) {
                new StringBuilder();
                int i = Log.i(TAG, sb.append(str2).append(" for ").append(this.mServiceComponent).append(" with mServiceConnection=").append(this.mServiceCallbacks).append(" this=").append(this).toString());
            }
            return false;
        }

        /* access modifiers changed from: private */
        public ServiceCallbacks getNewServiceCallbacks() {
            ServiceCallbacks serviceCallbacks;
            new ServiceCallbacks(this);
            return serviceCallbacks;
        }

        /* access modifiers changed from: package-private */
        public void dump() {
            StringBuilder sb;
            StringBuilder sb2;
            StringBuilder sb3;
            StringBuilder sb4;
            StringBuilder sb5;
            StringBuilder sb6;
            StringBuilder sb7;
            StringBuilder sb8;
            StringBuilder sb9;
            int d = Log.d(TAG, "MediaBrowserCompat...");
            new StringBuilder();
            int d2 = Log.d(TAG, sb.append("  mServiceComponent=").append(this.mServiceComponent).toString());
            new StringBuilder();
            int d3 = Log.d(TAG, sb2.append("  mCallback=").append(this.mCallback).toString());
            new StringBuilder();
            int d4 = Log.d(TAG, sb3.append("  mRootHints=").append(this.mRootHints).toString());
            new StringBuilder();
            int d5 = Log.d(TAG, sb4.append("  mState=").append(getStateLabel(this.mState)).toString());
            new StringBuilder();
            int d6 = Log.d(TAG, sb5.append("  mServiceConnection=").append(this.mServiceConnection).toString());
            new StringBuilder();
            int d7 = Log.d(TAG, sb6.append("  mServiceBinder=").append(this.mServiceBinder).toString());
            new StringBuilder();
            int d8 = Log.d(TAG, sb7.append("  mServiceCallbacks=").append(this.mServiceCallbacks).toString());
            new StringBuilder();
            int d9 = Log.d(TAG, sb8.append("  mRootId=").append(this.mRootId).toString());
            new StringBuilder();
            int d10 = Log.d(TAG, sb9.append("  mMediaSessionToken=").append(this.mMediaSessionToken).toString());
        }

        private class MediaServiceConnection implements ServiceConnection {
            private MediaServiceConnection() {
            }

            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                StringBuilder sb;
                IBinder iBinder2 = iBinder;
                if (isCurrent("onServiceConnected")) {
                    IMediaBrowserServiceCompat access$1302 = MediaBrowserImplBase.access$1302(MediaBrowserImplBase.this, IMediaBrowserServiceCompat.Stub.asInterface(iBinder2));
                    IMediaBrowserServiceCompatCallbacks access$1202 = MediaBrowserImplBase.access$1202(MediaBrowserImplBase.this, MediaBrowserImplBase.this.getNewServiceCallbacks());
                    int access$602 = MediaBrowserImplBase.access$602(MediaBrowserImplBase.this, 1);
                    try {
                        MediaBrowserImplBase.this.mServiceBinder.connect(MediaBrowserImplBase.this.mContext.getPackageName(), MediaBrowserImplBase.this.mRootHints, MediaBrowserImplBase.this.mServiceCallbacks);
                    } catch (RemoteException e) {
                        new StringBuilder();
                        int w = Log.w(MediaBrowserImplBase.TAG, sb.append("RemoteException during connect for ").append(MediaBrowserImplBase.this.mServiceComponent).toString());
                    }
                }
            }

            public void onServiceDisconnected(ComponentName componentName) {
                if (isCurrent("onServiceDisconnected")) {
                    IMediaBrowserServiceCompat access$1302 = MediaBrowserImplBase.access$1302(MediaBrowserImplBase.this, null);
                    IMediaBrowserServiceCompatCallbacks access$1202 = MediaBrowserImplBase.access$1202(MediaBrowserImplBase.this, null);
                    int access$602 = MediaBrowserImplBase.access$602(MediaBrowserImplBase.this, 3);
                    MediaBrowserImplBase.this.mCallback.onConnectionSuspended();
                }
            }

            private boolean isCurrent(String str) {
                StringBuilder sb;
                String str2 = str;
                if (MediaBrowserImplBase.this.mServiceConnection == this) {
                    return true;
                }
                if (MediaBrowserImplBase.this.mState != 0) {
                    new StringBuilder();
                    int i = Log.i(MediaBrowserImplBase.TAG, sb.append(str2).append(" for ").append(MediaBrowserImplBase.this.mServiceComponent).append(" with mServiceConnection=").append(MediaBrowserImplBase.this.mServiceConnection).append(" this=").append(this).toString());
                }
                return false;
            }
        }

        private static class ServiceCallbacks extends IMediaBrowserServiceCompatCallbacks.Stub {
            private WeakReference<MediaBrowserImplBase> mMediaBrowser;

            public ServiceCallbacks(MediaBrowserImplBase mediaBrowserImplBase) {
                WeakReference<MediaBrowserImplBase> weakReference;
                new WeakReference<>(mediaBrowserImplBase);
                this.mMediaBrowser = weakReference;
            }

            public void onConnect(String str, MediaSessionCompat.Token token, Bundle bundle) {
                String str2 = str;
                MediaSessionCompat.Token token2 = token;
                Bundle bundle2 = bundle;
                MediaBrowserImplBase mediaBrowserImplBase = this.mMediaBrowser.get();
                if (mediaBrowserImplBase != null) {
                    mediaBrowserImplBase.onServiceConnected(this, str2, token2, bundle2);
                }
            }

            public void onConnectFailed() {
                MediaBrowserImplBase mediaBrowserImplBase = this.mMediaBrowser.get();
                if (mediaBrowserImplBase != null) {
                    mediaBrowserImplBase.onConnectionFailed(this);
                }
            }

            public void onLoadChildren(String str, List list) {
                String str2 = str;
                List list2 = list;
                MediaBrowserImplBase mediaBrowserImplBase = this.mMediaBrowser.get();
                if (mediaBrowserImplBase != null) {
                    mediaBrowserImplBase.onLoadChildren(this, str2, list2);
                }
            }
        }

        private static class Subscription {
            SubscriptionCallback callback;
            final String id;

            Subscription(String str) {
                this.id = str;
            }
        }
    }
}
