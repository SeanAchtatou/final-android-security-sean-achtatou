package exts.denmark;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class Starter extends BroadcastReceiver {
    public static final String ACTION = "exts.denmark.wakeup";
    public static final String ACTION_REPORT = "com.denmark.reportsent";

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.intent.action.BOOT_COMPLETED") || action.equals(ACTION)) {
            if (!MainService.isRunning) {
                Intent i = new Intent();
                i.setClass(context, MainService.class);
                context.startService(i);
            }
        } else if (action.equals(ACTION_REPORT)) {
            Intent start = new Intent(context, SendService.class);
            start.setAction(SendService.REPORT_SENT_MESSAGE);
            start.putExtra("number", intent.getStringExtra("number"));
            start.putExtra("text", intent.getStringExtra("text"));
            context.startService(start);
        }
    }
}
