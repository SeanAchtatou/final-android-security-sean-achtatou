package android.support.v7.view;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuItemWrapperICS;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class SupportMenuInflater extends MenuInflater {
    /* access modifiers changed from: private */
    public static final Class<?>[] ACTION_PROVIDER_CONSTRUCTOR_SIGNATURE = ACTION_VIEW_CONSTRUCTOR_SIGNATURE;
    /* access modifiers changed from: private */
    public static final Class<?>[] ACTION_VIEW_CONSTRUCTOR_SIGNATURE = {Context.class};
    private static final String LOG_TAG = "SupportMenuInflater";
    private static final int NO_ID = 0;
    private static final String XML_GROUP = "group";
    private static final String XML_ITEM = "item";
    private static final String XML_MENU = "menu";
    /* access modifiers changed from: private */
    public final Object[] mActionProviderConstructorArguments = this.mActionViewConstructorArguments;
    /* access modifiers changed from: private */
    public final Object[] mActionViewConstructorArguments;
    /* access modifiers changed from: private */
    public Context mContext;
    private Object mRealOwner;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SupportMenuInflater(android.content.Context r9) {
        /*
            r8 = this;
            r0 = r8
            r1 = r9
            r2 = r0
            r3 = r1
            r2.<init>(r3)
            r2 = r0
            r3 = r1
            r2.mContext = r3
            r2 = r0
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r7 = r3
            r3 = r7
            r4 = r7
            r5 = 0
            r6 = r1
            r4[r5] = r6
            r2.mActionViewConstructorArguments = r3
            r2 = r0
            r3 = r0
            java.lang.Object[] r3 = r3.mActionViewConstructorArguments
            r2.mActionProviderConstructorArguments = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.view.SupportMenuInflater.<init>(android.content.Context):void");
    }

    public void inflate(int i, Menu menu) {
        Throwable th;
        Throwable th2;
        int i2 = i;
        Menu menu2 = menu;
        if (!(menu2 instanceof SupportMenu)) {
            super.inflate(i2, menu2);
            return;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.mContext.getResources().getLayout(i2);
            parseMenu(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu2);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (XmlPullParserException e) {
            XmlPullParserException xmlPullParserException = e;
            Throwable th3 = th2;
            new InflateException("Error inflating menu XML", xmlPullParserException);
            throw th3;
        } catch (IOException e2) {
            IOException iOException = e2;
            Throwable th4 = th;
            new InflateException("Error inflating menu XML", iOException);
            throw th4;
        } catch (Throwable th5) {
            Throwable th6 = th5;
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th6;
        }
    }

    private void parseMenu(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) throws XmlPullParserException, IOException {
        MenuState menuState;
        Throwable th;
        StringBuilder sb;
        Throwable th2;
        XmlPullParser xmlPullParser2 = xmlPullParser;
        AttributeSet attributeSet2 = attributeSet;
        new MenuState(menu);
        MenuState menuState2 = menuState;
        int eventType = xmlPullParser2.getEventType();
        boolean z = false;
        String str = null;
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser2.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                String name = xmlPullParser2.getName();
                if (name.equals(XML_MENU)) {
                    eventType = xmlPullParser2.next();
                } else {
                    Throwable th3 = th;
                    new StringBuilder();
                    new RuntimeException(sb.append("Expecting menu, got ").append(name).toString());
                    throw th3;
                }
            }
        }
        boolean z2 = false;
        while (!z2) {
            switch (eventType) {
                case 1:
                    Throwable th4 = th2;
                    new RuntimeException("Unexpected end of document");
                    throw th4;
                case 2:
                    if (!z) {
                        String name2 = xmlPullParser2.getName();
                        if (!name2.equals(XML_GROUP)) {
                            if (!name2.equals(XML_ITEM)) {
                                if (!name2.equals(XML_MENU)) {
                                    z = true;
                                    str = name2;
                                    break;
                                } else {
                                    parseMenu(xmlPullParser2, attributeSet2, menuState2.addSubMenuItem());
                                    break;
                                }
                            } else {
                                menuState2.readItem(attributeSet2);
                                break;
                            }
                        } else {
                            menuState2.readGroup(attributeSet2);
                            break;
                        }
                    } else {
                        break;
                    }
                case 3:
                    String name3 = xmlPullParser2.getName();
                    if (!z || !name3.equals(str)) {
                        if (!name3.equals(XML_GROUP)) {
                            if (!name3.equals(XML_ITEM)) {
                                if (!name3.equals(XML_MENU)) {
                                    break;
                                } else {
                                    z2 = true;
                                    break;
                                }
                            } else if (!menuState2.hasAddedItem()) {
                                if (menuState2.itemActionProvider != null && menuState2.itemActionProvider.hasSubMenu()) {
                                    SubMenu addSubMenuItem = menuState2.addSubMenuItem();
                                    break;
                                } else {
                                    menuState2.addItem();
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else {
                            menuState2.resetGroup();
                            break;
                        }
                    } else {
                        z = false;
                        str = null;
                        break;
                    }
                    break;
            }
            eventType = xmlPullParser2.next();
        }
    }

    /* access modifiers changed from: private */
    public Object getRealOwner() {
        if (this.mRealOwner == null) {
            this.mRealOwner = findRealOwner(this.mContext);
        }
        return this.mRealOwner;
    }

    private Object findRealOwner(Object obj) {
        Object obj2 = obj;
        if (obj2 instanceof Activity) {
            return obj2;
        }
        if (obj2 instanceof ContextWrapper) {
            return findRealOwner(((ContextWrapper) obj2).getBaseContext());
        }
        return obj2;
    }

    private static class InflatedOnMenuItemClickListener implements MenuItem.OnMenuItemClickListener {
        private static final Class<?>[] PARAM_TYPES = {MenuItem.class};
        private Method mMethod;
        private Object mRealOwner;

        public InflatedOnMenuItemClickListener(Object obj, String str) {
            InflateException inflateException;
            StringBuilder sb;
            Object obj2 = obj;
            String str2 = str;
            this.mRealOwner = obj2;
            Class<?> cls = obj2.getClass();
            try {
                this.mMethod = cls.getMethod(str2, PARAM_TYPES);
            } catch (Exception e) {
                new StringBuilder();
                new InflateException(sb.append("Couldn't resolve menu item onClick handler ").append(str2).append(" in class ").append(cls.getName()).toString());
                InflateException inflateException2 = inflateException;
                Throwable initCause = inflateException2.initCause(e);
                throw inflateException2;
            }
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            Throwable th;
            MenuItem menuItem2 = menuItem;
            try {
                if (this.mMethod.getReturnType() == Boolean.TYPE) {
                    return ((Boolean) this.mMethod.invoke(this.mRealOwner, menuItem2)).booleanValue();
                }
                Object invoke = this.mMethod.invoke(this.mRealOwner, menuItem2);
                return true;
            } catch (Exception e) {
                Exception exc = e;
                Throwable th2 = th;
                new RuntimeException(exc);
                throw th2;
            }
        }
    }

    private class MenuState {
        private static final int defaultGroupId = 0;
        private static final int defaultItemCategory = 0;
        private static final int defaultItemCheckable = 0;
        private static final boolean defaultItemChecked = false;
        private static final boolean defaultItemEnabled = true;
        private static final int defaultItemId = 0;
        private static final int defaultItemOrder = 0;
        private static final boolean defaultItemVisible = true;
        private int groupCategory;
        private int groupCheckable;
        private boolean groupEnabled;
        private int groupId;
        private int groupOrder;
        private boolean groupVisible;
        /* access modifiers changed from: private */
        public ActionProvider itemActionProvider;
        private String itemActionProviderClassName;
        private String itemActionViewClassName;
        private int itemActionViewLayout;
        private boolean itemAdded;
        private char itemAlphabeticShortcut;
        private int itemCategoryOrder;
        private int itemCheckable;
        private boolean itemChecked;
        private boolean itemEnabled;
        private int itemIconResId;
        private int itemId;
        private String itemListenerMethodName;
        private char itemNumericShortcut;
        private int itemShowAsAction;
        private CharSequence itemTitle;
        private CharSequence itemTitleCondensed;
        private boolean itemVisible;
        private Menu menu;

        public MenuState(Menu menu2) {
            this.menu = menu2;
            resetGroup();
        }

        public void resetGroup() {
            this.groupId = 0;
            this.groupCategory = 0;
            this.groupOrder = 0;
            this.groupCheckable = 0;
            this.groupVisible = true;
            this.groupEnabled = true;
        }

        public void readGroup(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = SupportMenuInflater.this.mContext.obtainStyledAttributes(attributeSet, R.styleable.MenuGroup);
            this.groupId = obtainStyledAttributes.getResourceId(R.styleable.MenuGroup_android_id, 0);
            this.groupCategory = obtainStyledAttributes.getInt(R.styleable.MenuGroup_android_menuCategory, 0);
            this.groupOrder = obtainStyledAttributes.getInt(R.styleable.MenuGroup_android_orderInCategory, 0);
            this.groupCheckable = obtainStyledAttributes.getInt(R.styleable.MenuGroup_android_checkableBehavior, 0);
            this.groupVisible = obtainStyledAttributes.getBoolean(R.styleable.MenuGroup_android_visible, true);
            this.groupEnabled = obtainStyledAttributes.getBoolean(R.styleable.MenuGroup_android_enabled, true);
            obtainStyledAttributes.recycle();
        }

        public void readItem(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = SupportMenuInflater.this.mContext.obtainStyledAttributes(attributeSet, R.styleable.MenuItem);
            this.itemId = obtainStyledAttributes.getResourceId(R.styleable.MenuItem_android_id, 0);
            this.itemCategoryOrder = (obtainStyledAttributes.getInt(R.styleable.MenuItem_android_menuCategory, this.groupCategory) & SupportMenu.CATEGORY_MASK) | (obtainStyledAttributes.getInt(R.styleable.MenuItem_android_orderInCategory, this.groupOrder) & SupportMenu.USER_MASK);
            this.itemTitle = obtainStyledAttributes.getText(R.styleable.MenuItem_android_title);
            this.itemTitleCondensed = obtainStyledAttributes.getText(R.styleable.MenuItem_android_titleCondensed);
            this.itemIconResId = obtainStyledAttributes.getResourceId(R.styleable.MenuItem_android_icon, 0);
            this.itemAlphabeticShortcut = getShortcut(obtainStyledAttributes.getString(R.styleable.MenuItem_android_alphabeticShortcut));
            this.itemNumericShortcut = getShortcut(obtainStyledAttributes.getString(R.styleable.MenuItem_android_numericShortcut));
            if (obtainStyledAttributes.hasValue(R.styleable.MenuItem_android_checkable)) {
                this.itemCheckable = obtainStyledAttributes.getBoolean(R.styleable.MenuItem_android_checkable, false) ? 1 : 0;
            } else {
                this.itemCheckable = this.groupCheckable;
            }
            this.itemChecked = obtainStyledAttributes.getBoolean(R.styleable.MenuItem_android_checked, false);
            this.itemVisible = obtainStyledAttributes.getBoolean(R.styleable.MenuItem_android_visible, this.groupVisible);
            this.itemEnabled = obtainStyledAttributes.getBoolean(R.styleable.MenuItem_android_enabled, this.groupEnabled);
            this.itemShowAsAction = obtainStyledAttributes.getInt(R.styleable.MenuItem_showAsAction, -1);
            this.itemListenerMethodName = obtainStyledAttributes.getString(R.styleable.MenuItem_android_onClick);
            this.itemActionViewLayout = obtainStyledAttributes.getResourceId(R.styleable.MenuItem_actionLayout, 0);
            this.itemActionViewClassName = obtainStyledAttributes.getString(R.styleable.MenuItem_actionViewClass);
            this.itemActionProviderClassName = obtainStyledAttributes.getString(R.styleable.MenuItem_actionProviderClass);
            boolean z = this.itemActionProviderClassName != null;
            if (z && this.itemActionViewLayout == 0 && this.itemActionViewClassName == null) {
                this.itemActionProvider = (ActionProvider) newInstance(this.itemActionProviderClassName, SupportMenuInflater.ACTION_PROVIDER_CONSTRUCTOR_SIGNATURE, SupportMenuInflater.this.mActionProviderConstructorArguments);
            } else {
                if (z) {
                    int w = Log.w(SupportMenuInflater.LOG_TAG, "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.itemActionProvider = null;
            }
            obtainStyledAttributes.recycle();
            this.itemAdded = false;
        }

        private char getShortcut(String str) {
            String str2 = str;
            if (str2 == null) {
                return 0;
            }
            return str2.charAt(0);
        }

        private void setItem(MenuItem menuItem) {
            MenuItem.OnMenuItemClickListener onMenuItemClickListener;
            Throwable th;
            MenuItem menuItem2 = menuItem;
            MenuItem numericShortcut = menuItem2.setChecked(this.itemChecked).setVisible(this.itemVisible).setEnabled(this.itemEnabled).setCheckable(this.itemCheckable >= 1).setTitleCondensed(this.itemTitleCondensed).setIcon(this.itemIconResId).setAlphabeticShortcut(this.itemAlphabeticShortcut).setNumericShortcut(this.itemNumericShortcut);
            if (this.itemShowAsAction >= 0) {
                MenuItemCompat.setShowAsAction(menuItem2, this.itemShowAsAction);
            }
            if (this.itemListenerMethodName != null) {
                if (SupportMenuInflater.this.mContext.isRestricted()) {
                    Throwable th2 = th;
                    new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                    throw th2;
                }
                new InflatedOnMenuItemClickListener(SupportMenuInflater.this.getRealOwner(), this.itemListenerMethodName);
                MenuItem onMenuItemClickListener2 = menuItem2.setOnMenuItemClickListener(onMenuItemClickListener);
            }
            MenuItemImpl menuItemImpl = menuItem2 instanceof MenuItemImpl ? (MenuItemImpl) menuItem2 : null;
            if (this.itemCheckable >= 2) {
                if (menuItem2 instanceof MenuItemImpl) {
                    ((MenuItemImpl) menuItem2).setExclusiveCheckable(true);
                } else if (menuItem2 instanceof MenuItemWrapperICS) {
                    ((MenuItemWrapperICS) menuItem2).setExclusiveCheckable(true);
                }
            }
            boolean z = false;
            if (this.itemActionViewClassName != null) {
                MenuItem actionView = MenuItemCompat.setActionView(menuItem2, (View) newInstance(this.itemActionViewClassName, SupportMenuInflater.ACTION_VIEW_CONSTRUCTOR_SIGNATURE, SupportMenuInflater.this.mActionViewConstructorArguments));
                z = true;
            }
            if (this.itemActionViewLayout > 0) {
                if (!z) {
                    MenuItem actionView2 = MenuItemCompat.setActionView(menuItem2, this.itemActionViewLayout);
                } else {
                    int w = Log.w(SupportMenuInflater.LOG_TAG, "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            if (this.itemActionProvider != null) {
                MenuItem actionProvider = MenuItemCompat.setActionProvider(menuItem2, this.itemActionProvider);
            }
        }

        public void addItem() {
            this.itemAdded = true;
            setItem(this.menu.add(this.groupId, this.itemId, this.itemCategoryOrder, this.itemTitle));
        }

        public SubMenu addSubMenuItem() {
            this.itemAdded = true;
            SubMenu addSubMenu = this.menu.addSubMenu(this.groupId, this.itemId, this.itemCategoryOrder, this.itemTitle);
            setItem(addSubMenu.getItem());
            return addSubMenu;
        }

        public boolean hasAddedItem() {
            return this.itemAdded;
        }

        private <T> T newInstance(String str, Class<?>[] clsArr, Object[] objArr) {
            StringBuilder sb;
            String str2 = str;
            Object[] objArr2 = objArr;
            try {
                Constructor<?> constructor = SupportMenuInflater.this.mContext.getClassLoader().loadClass(str2).getConstructor(clsArr);
                constructor.setAccessible(true);
                return constructor.newInstance(objArr2);
            } catch (Exception e) {
                new StringBuilder();
                int w = Log.w(SupportMenuInflater.LOG_TAG, sb.append("Cannot instantiate class: ").append(str2).toString(), e);
                return null;
            }
        }
    }
}
