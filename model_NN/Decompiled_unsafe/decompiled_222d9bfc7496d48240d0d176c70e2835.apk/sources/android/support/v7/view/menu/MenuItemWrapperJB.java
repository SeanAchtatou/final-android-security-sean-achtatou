package android.support.v7.view.menu;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.view.ActionProvider;
import android.support.v7.view.menu.MenuItemWrapperICS;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.View;

@TargetApi(16)
class MenuItemWrapperJB extends MenuItemWrapperICS {
    MenuItemWrapperJB(Context context, SupportMenuItem supportMenuItem) {
        super(context, supportMenuItem);
    }

    /* access modifiers changed from: package-private */
    public MenuItemWrapperICS.ActionProviderWrapper createActionProviderWrapper(ActionProvider actionProvider) {
        MenuItemWrapperICS.ActionProviderWrapper actionProviderWrapper;
        new ActionProviderWrapperJB(this, this.mContext, actionProvider);
        return actionProviderWrapper;
    }

    class ActionProviderWrapperJB extends MenuItemWrapperICS.ActionProviderWrapper implements ActionProvider.VisibilityListener {
        ActionProvider.VisibilityListener mListener;
        final /* synthetic */ MenuItemWrapperJB this$0;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public ActionProviderWrapperJB(android.support.v7.view.menu.MenuItemWrapperJB r9, android.content.Context r10, android.view.ActionProvider r11) {
            /*
                r8 = this;
                r0 = r8
                r1 = r9
                r2 = r10
                r3 = r11
                r4 = r0
                r5 = r1
                r4.this$0 = r5
                r4 = r0
                r5 = r1
                r6 = r2
                r7 = r3
                r4.<init>(r6, r7)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.view.menu.MenuItemWrapperJB.ActionProviderWrapperJB.<init>(android.support.v7.view.menu.MenuItemWrapperJB, android.content.Context, android.view.ActionProvider):void");
        }

        public View onCreateActionView(MenuItem menuItem) {
            return this.mInner.onCreateActionView(menuItem);
        }

        public boolean overridesItemVisibility() {
            return this.mInner.overridesItemVisibility();
        }

        public boolean isVisible() {
            return this.mInner.isVisible();
        }

        public void refreshVisibility() {
            this.mInner.refreshVisibility();
        }

        public void setVisibilityListener(ActionProvider.VisibilityListener visibilityListener) {
            ActionProvider.VisibilityListener visibilityListener2 = visibilityListener;
            this.mListener = visibilityListener2;
            this.mInner.setVisibilityListener(visibilityListener2 != null ? this : null);
        }

        public void onActionProviderVisibilityChanged(boolean z) {
            boolean z2 = z;
            if (this.mListener != null) {
                this.mListener.onActionProviderVisibilityChanged(z2);
            }
        }
    }
}
