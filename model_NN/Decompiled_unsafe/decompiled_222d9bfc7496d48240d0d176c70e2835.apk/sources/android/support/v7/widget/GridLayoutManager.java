package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import java.util.Arrays;

public class GridLayoutManager extends LinearLayoutManager {
    private static final boolean DEBUG = false;
    public static final int DEFAULT_SPAN_COUNT = -1;
    static final int MAIN_DIR_SPEC = View.MeasureSpec.makeMeasureSpec(0, 0);
    private static final String TAG = "GridLayoutManager";
    int[] mCachedBorders;
    final Rect mDecorInsets;
    boolean mPendingSpanCountChange = false;
    final SparseIntArray mPreLayoutSpanIndexCache;
    final SparseIntArray mPreLayoutSpanSizeCache;
    View[] mSet;
    int mSpanCount = -1;
    SpanSizeLookup mSpanSizeLookup;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public GridLayoutManager(android.content.Context r13, android.util.AttributeSet r14, int r15, int r16) {
        /*
            r12 = this;
            r0 = r12
            r1 = r13
            r2 = r14
            r3 = r15
            r4 = r16
            r6 = r0
            r7 = r1
            r8 = r2
            r9 = r3
            r10 = r4
            r6.<init>(r7, r8, r9, r10)
            r6 = r0
            r7 = 0
            r6.mPendingSpanCountChange = r7
            r6 = r0
            r7 = -1
            r6.mSpanCount = r7
            r6 = r0
            android.util.SparseIntArray r7 = new android.util.SparseIntArray
            r11 = r7
            r7 = r11
            r8 = r11
            r8.<init>()
            r6.mPreLayoutSpanSizeCache = r7
            r6 = r0
            android.util.SparseIntArray r7 = new android.util.SparseIntArray
            r11 = r7
            r7 = r11
            r8 = r11
            r8.<init>()
            r6.mPreLayoutSpanIndexCache = r7
            r6 = r0
            android.support.v7.widget.GridLayoutManager$DefaultSpanSizeLookup r7 = new android.support.v7.widget.GridLayoutManager$DefaultSpanSizeLookup
            r11 = r7
            r7 = r11
            r8 = r11
            r8.<init>()
            r6.mSpanSizeLookup = r7
            r6 = r0
            android.graphics.Rect r7 = new android.graphics.Rect
            r11 = r7
            r7 = r11
            r8 = r11
            r8.<init>()
            r6.mDecorInsets = r7
            r6 = r1
            r7 = r2
            r8 = r3
            r9 = r4
            android.support.v7.widget.RecyclerView$LayoutManager$Properties r6 = getProperties(r6, r7, r8, r9)
            r5 = r6
            r6 = r0
            r7 = r5
            int r7 = r7.spanCount
            r6.setSpanCount(r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.GridLayoutManager.<init>(android.content.Context, android.util.AttributeSet, int, int):void");
    }

    public GridLayoutManager(Context context, int i) {
        super(context);
        SparseIntArray sparseIntArray;
        SparseIntArray sparseIntArray2;
        SpanSizeLookup spanSizeLookup;
        Rect rect;
        new SparseIntArray();
        this.mPreLayoutSpanSizeCache = sparseIntArray;
        new SparseIntArray();
        this.mPreLayoutSpanIndexCache = sparseIntArray2;
        new DefaultSpanSizeLookup();
        this.mSpanSizeLookup = spanSizeLookup;
        new Rect();
        this.mDecorInsets = rect;
        setSpanCount(i);
    }

    public GridLayoutManager(Context context, int i, int i2, boolean z) {
        super(context, i2, z);
        SparseIntArray sparseIntArray;
        SparseIntArray sparseIntArray2;
        SpanSizeLookup spanSizeLookup;
        Rect rect;
        new SparseIntArray();
        this.mPreLayoutSpanSizeCache = sparseIntArray;
        new SparseIntArray();
        this.mPreLayoutSpanIndexCache = sparseIntArray2;
        new DefaultSpanSizeLookup();
        this.mSpanSizeLookup = spanSizeLookup;
        new Rect();
        this.mDecorInsets = rect;
        setSpanCount(i);
    }

    public void setStackFromEnd(boolean z) {
        Throwable th;
        if (z) {
            Throwable th2 = th;
            new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
            throw th2;
        }
        super.setStackFromEnd(false);
    }

    public int getRowCountForAccessibility(RecyclerView.Recycler recycler, RecyclerView.State state) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        if (this.mOrientation == 0) {
            return this.mSpanCount;
        }
        if (state2.getItemCount() < 1) {
            return 0;
        }
        return getSpanGroupIndex(recycler2, state2, state2.getItemCount() - 1);
    }

    public int getColumnCountForAccessibility(RecyclerView.Recycler recycler, RecyclerView.State state) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        if (this.mOrientation == 1) {
            return this.mSpanCount;
        }
        if (state2.getItemCount() < 1) {
            return 0;
        }
        return getSpanGroupIndex(recycler2, state2, state2.getItemCount() - 1);
    }

    public void onInitializeAccessibilityNodeInfoForItem(RecyclerView.Recycler recycler, RecyclerView.State state, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        View view2 = view;
        AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
        ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
        if (!(layoutParams instanceof LayoutParams)) {
            super.onInitializeAccessibilityNodeInfoForItem(view2, accessibilityNodeInfoCompat2);
            return;
        }
        LayoutParams layoutParams2 = (LayoutParams) layoutParams;
        int spanGroupIndex = getSpanGroupIndex(recycler2, state2, layoutParams2.getViewLayoutPosition());
        if (this.mOrientation == 0) {
            accessibilityNodeInfoCompat2.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(layoutParams2.getSpanIndex(), layoutParams2.getSpanSize(), spanGroupIndex, 1, this.mSpanCount > 1 && layoutParams2.getSpanSize() == this.mSpanCount, false));
        } else {
            accessibilityNodeInfoCompat2.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(spanGroupIndex, 1, layoutParams2.getSpanIndex(), layoutParams2.getSpanSize(), this.mSpanCount > 1 && layoutParams2.getSpanSize() == this.mSpanCount, false));
        }
    }

    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        if (state2.isPreLayout()) {
            cachePreLayoutSpanMapping();
        }
        super.onLayoutChildren(recycler2, state2);
        clearPreLayoutSpanMappingCache();
        if (!state2.isPreLayout()) {
            this.mPendingSpanCountChange = false;
        }
    }

    private void clearPreLayoutSpanMappingCache() {
        this.mPreLayoutSpanSizeCache.clear();
        this.mPreLayoutSpanIndexCache.clear();
    }

    private void cachePreLayoutSpanMapping() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            LayoutParams layoutParams = (LayoutParams) getChildAt(i).getLayoutParams();
            int viewLayoutPosition = layoutParams.getViewLayoutPosition();
            this.mPreLayoutSpanSizeCache.put(viewLayoutPosition, layoutParams.getSpanSize());
            this.mPreLayoutSpanIndexCache.put(viewLayoutPosition, layoutParams.getSpanIndex());
        }
    }

    public void onItemsAdded(RecyclerView recyclerView, int i, int i2) {
        this.mSpanSizeLookup.invalidateSpanIndexCache();
    }

    public void onItemsChanged(RecyclerView recyclerView) {
        this.mSpanSizeLookup.invalidateSpanIndexCache();
    }

    public void onItemsRemoved(RecyclerView recyclerView, int i, int i2) {
        this.mSpanSizeLookup.invalidateSpanIndexCache();
    }

    public void onItemsUpdated(RecyclerView recyclerView, int i, int i2, Object obj) {
        this.mSpanSizeLookup.invalidateSpanIndexCache();
    }

    public void onItemsMoved(RecyclerView recyclerView, int i, int i2, int i3) {
        this.mSpanSizeLookup.invalidateSpanIndexCache();
    }

    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        RecyclerView.LayoutParams layoutParams;
        new LayoutParams(-2, -2);
        return layoutParams;
    }

    public RecyclerView.LayoutParams generateLayoutParams(Context context, AttributeSet attributeSet) {
        RecyclerView.LayoutParams layoutParams;
        new LayoutParams(context, attributeSet);
        return layoutParams;
    }

    public RecyclerView.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        RecyclerView.LayoutParams layoutParams2;
        RecyclerView.LayoutParams layoutParams3;
        ViewGroup.LayoutParams layoutParams4 = layoutParams;
        if (layoutParams4 instanceof ViewGroup.MarginLayoutParams) {
            new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams4);
            return layoutParams3;
        }
        new LayoutParams(layoutParams4);
        return layoutParams2;
    }

    public boolean checkLayoutParams(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void setSpanSizeLookup(SpanSizeLookup spanSizeLookup) {
        this.mSpanSizeLookup = spanSizeLookup;
    }

    public SpanSizeLookup getSpanSizeLookup() {
        return this.mSpanSizeLookup;
    }

    private void updateMeasurements() {
        int height;
        if (getOrientation() == 1) {
            height = (getWidth() - getPaddingRight()) - getPaddingLeft();
        } else {
            height = (getHeight() - getPaddingBottom()) - getPaddingTop();
        }
        calculateItemBorders(height);
    }

    private void calculateItemBorders(int i) {
        int i2 = i;
        if (!(this.mCachedBorders != null && this.mCachedBorders.length == this.mSpanCount + 1 && this.mCachedBorders[this.mCachedBorders.length - 1] == i2)) {
            this.mCachedBorders = new int[(this.mSpanCount + 1)];
        }
        this.mCachedBorders[0] = 0;
        int i3 = i2 / this.mSpanCount;
        int i4 = i2 % this.mSpanCount;
        int i5 = 0;
        int i6 = 0;
        for (int i7 = 1; i7 <= this.mSpanCount; i7++) {
            int i8 = i3;
            i6 += i4;
            if (i6 > 0 && this.mSpanCount - i6 < i4) {
                i8++;
                i6 -= this.mSpanCount;
            }
            i5 += i8;
            this.mCachedBorders[i7] = i5;
        }
    }

    /* access modifiers changed from: package-private */
    public void onAnchorReady(RecyclerView.Recycler recycler, RecyclerView.State state, LinearLayoutManager.AnchorInfo anchorInfo) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        LinearLayoutManager.AnchorInfo anchorInfo2 = anchorInfo;
        super.onAnchorReady(recycler2, state2, anchorInfo2);
        updateMeasurements();
        if (state2.getItemCount() > 0 && !state2.isPreLayout()) {
            ensureAnchorIsInFirstSpan(recycler2, state2, anchorInfo2);
        }
        ensureViewSet();
    }

    private void ensureViewSet() {
        if (this.mSet == null || this.mSet.length != this.mSpanCount) {
            this.mSet = new View[this.mSpanCount];
        }
    }

    public int scrollHorizontallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        updateMeasurements();
        ensureViewSet();
        return super.scrollHorizontallyBy(i, recycler, state);
    }

    public int scrollVerticallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        updateMeasurements();
        ensureViewSet();
        return super.scrollVerticallyBy(i, recycler, state);
    }

    private void ensureAnchorIsInFirstSpan(RecyclerView.Recycler recycler, RecyclerView.State state, LinearLayoutManager.AnchorInfo anchorInfo) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        LinearLayoutManager.AnchorInfo anchorInfo2 = anchorInfo;
        for (int spanIndex = getSpanIndex(recycler2, state2, anchorInfo2.mPosition); spanIndex > 0 && anchorInfo2.mPosition > 0; spanIndex = getSpanIndex(recycler2, state2, anchorInfo2.mPosition)) {
            LinearLayoutManager.AnchorInfo anchorInfo3 = anchorInfo2;
            anchorInfo3.mPosition--;
        }
    }

    /* access modifiers changed from: package-private */
    public View findReferenceChild(RecyclerView.Recycler recycler, RecyclerView.State state, int i, int i2, int i3) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        ensureLayoutState();
        View view = null;
        View view2 = null;
        int startAfterPadding = this.mOrientationHelper.getStartAfterPadding();
        int endAfterPadding = this.mOrientationHelper.getEndAfterPadding();
        int i7 = i5 > i4 ? 1 : -1;
        int i8 = i4;
        while (true) {
            int i9 = i8;
            if (i9 != i5) {
                View childAt = getChildAt(i9);
                int position = getPosition(childAt);
                if (position >= 0 && position < i6 && getSpanIndex(recycler2, state2, position) == 0) {
                    if (!((RecyclerView.LayoutParams) childAt.getLayoutParams()).isItemRemoved()) {
                        if (this.mOrientationHelper.getDecoratedStart(childAt) < endAfterPadding && this.mOrientationHelper.getDecoratedEnd(childAt) >= startAfterPadding) {
                            return childAt;
                        }
                        if (view2 == null) {
                            view2 = childAt;
                        }
                    } else if (view == null) {
                        view = childAt;
                    }
                }
                i8 = i9 + i7;
            } else {
                return view2 != null ? view2 : view;
            }
        }
    }

    private int getSpanGroupIndex(RecyclerView.Recycler recycler, RecyclerView.State state, int i) {
        StringBuilder sb;
        RecyclerView.Recycler recycler2 = recycler;
        int i2 = i;
        if (!state.isPreLayout()) {
            return this.mSpanSizeLookup.getSpanGroupIndex(i2, this.mSpanCount);
        }
        int convertPreLayoutPositionToPostLayout = recycler2.convertPreLayoutPositionToPostLayout(i2);
        if (convertPreLayoutPositionToPostLayout != -1) {
            return this.mSpanSizeLookup.getSpanGroupIndex(convertPreLayoutPositionToPostLayout, this.mSpanCount);
        }
        new StringBuilder();
        int w = Log.w(TAG, sb.append("Cannot find span size for pre layout position. ").append(i2).toString());
        return 0;
    }

    private int getSpanIndex(RecyclerView.Recycler recycler, RecyclerView.State state, int i) {
        StringBuilder sb;
        RecyclerView.Recycler recycler2 = recycler;
        int i2 = i;
        if (!state.isPreLayout()) {
            return this.mSpanSizeLookup.getCachedSpanIndex(i2, this.mSpanCount);
        }
        int i3 = this.mPreLayoutSpanIndexCache.get(i2, -1);
        if (i3 != -1) {
            return i3;
        }
        int convertPreLayoutPositionToPostLayout = recycler2.convertPreLayoutPositionToPostLayout(i2);
        if (convertPreLayoutPositionToPostLayout != -1) {
            return this.mSpanSizeLookup.getCachedSpanIndex(convertPreLayoutPositionToPostLayout, this.mSpanCount);
        }
        new StringBuilder();
        int w = Log.w(TAG, sb.append("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:").append(i2).toString());
        return 0;
    }

    private int getSpanSize(RecyclerView.Recycler recycler, RecyclerView.State state, int i) {
        StringBuilder sb;
        RecyclerView.Recycler recycler2 = recycler;
        int i2 = i;
        if (!state.isPreLayout()) {
            return this.mSpanSizeLookup.getSpanSize(i2);
        }
        int i3 = this.mPreLayoutSpanSizeCache.get(i2, -1);
        if (i3 != -1) {
            return i3;
        }
        int convertPreLayoutPositionToPostLayout = recycler2.convertPreLayoutPositionToPostLayout(i2);
        if (convertPreLayoutPositionToPostLayout != -1) {
            return this.mSpanSizeLookup.getSpanSize(convertPreLayoutPositionToPostLayout);
        }
        new StringBuilder();
        int w = Log.w(TAG, sb.append("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:").append(i2).toString());
        return 1;
    }

    /* access modifiers changed from: package-private */
    public void layoutChunk(RecyclerView.Recycler recycler, RecyclerView.State state, LinearLayoutManager.LayoutState layoutState, LinearLayoutManager.LayoutChunkResult layoutChunkResult) {
        Throwable th;
        StringBuilder sb;
        View next;
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        LinearLayoutManager.LayoutState layoutState2 = layoutState;
        LinearLayoutManager.LayoutChunkResult layoutChunkResult2 = layoutChunkResult;
        boolean z = layoutState2.mItemDirection == 1;
        int i = 0;
        int i2 = 0;
        int i3 = this.mSpanCount;
        if (!z) {
            i3 = getSpanIndex(recycler2, state2, layoutState2.mCurrentPosition) + getSpanSize(recycler2, state2, layoutState2.mCurrentPosition);
        }
        while (i < this.mSpanCount && layoutState2.hasMore(state2) && i3 > 0) {
            int i4 = layoutState2.mCurrentPosition;
            int spanSize = getSpanSize(recycler2, state2, i4);
            if (spanSize <= this.mSpanCount) {
                i3 -= spanSize;
                if (i3 < 0 || (next = layoutState2.next(recycler2)) == null) {
                    break;
                }
                i2 += spanSize;
                this.mSet[i] = next;
                i++;
            } else {
                Throwable th2 = th;
                new StringBuilder();
                new IllegalArgumentException(sb.append("Item at position ").append(i4).append(" requires ").append(spanSize).append(" spans but GridLayoutManager has only ").append(this.mSpanCount).append(" spans.").toString());
                throw th2;
            }
        }
        if (i == 0) {
            layoutChunkResult2.mFinished = true;
            return;
        }
        int i5 = 0;
        assignSpans(recycler2, state2, i, i2, z);
        for (int i6 = 0; i6 < i; i6++) {
            View view = this.mSet[i6];
            if (layoutState2.mScrapList == null) {
                if (z) {
                    addView(view);
                } else {
                    addView(view, 0);
                }
            } else if (z) {
                addDisappearingView(view);
            } else {
                addDisappearingView(view, 0);
            }
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.mCachedBorders[layoutParams.mSpanIndex + layoutParams.mSpanSize] - this.mCachedBorders[layoutParams.mSpanIndex], 1073741824);
            if (this.mOrientation == 1) {
                measureChildWithDecorationsAndMargin(view, makeMeasureSpec, getMainDirSpec(layoutParams.height), false);
            } else {
                measureChildWithDecorationsAndMargin(view, getMainDirSpec(layoutParams.width), makeMeasureSpec, false);
            }
            int decoratedMeasurement = this.mOrientationHelper.getDecoratedMeasurement(view);
            if (decoratedMeasurement > i5) {
                i5 = decoratedMeasurement;
            }
        }
        int mainDirSpec = getMainDirSpec(i5);
        for (int i7 = 0; i7 < i; i7++) {
            View view2 = this.mSet[i7];
            if (this.mOrientationHelper.getDecoratedMeasurement(view2) != i5) {
                LayoutParams layoutParams2 = (LayoutParams) view2.getLayoutParams();
                int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(this.mCachedBorders[layoutParams2.mSpanIndex + layoutParams2.mSpanSize] - this.mCachedBorders[layoutParams2.mSpanIndex], 1073741824);
                if (this.mOrientation == 1) {
                    measureChildWithDecorationsAndMargin(view2, makeMeasureSpec2, mainDirSpec, true);
                } else {
                    measureChildWithDecorationsAndMargin(view2, mainDirSpec, makeMeasureSpec2, true);
                }
            }
        }
        layoutChunkResult2.mConsumed = i5;
        int i8 = 0;
        int i9 = 0;
        int i10 = 0;
        int i11 = 0;
        if (this.mOrientation == 1) {
            if (layoutState2.mLayoutDirection == -1) {
                i11 = layoutState2.mOffset;
                i10 = i11 - i5;
            } else {
                i10 = layoutState2.mOffset;
                i11 = i10 + i5;
            }
        } else if (layoutState2.mLayoutDirection == -1) {
            i9 = layoutState2.mOffset;
            i8 = i9 - i5;
        } else {
            i8 = layoutState2.mOffset;
            i9 = i8 + i5;
        }
        for (int i12 = 0; i12 < i; i12++) {
            View view3 = this.mSet[i12];
            LayoutParams layoutParams3 = (LayoutParams) view3.getLayoutParams();
            if (this.mOrientation == 1) {
                i8 = getPaddingLeft() + this.mCachedBorders[layoutParams3.mSpanIndex];
                i9 = i8 + this.mOrientationHelper.getDecoratedMeasurementInOther(view3);
            } else {
                i10 = getPaddingTop() + this.mCachedBorders[layoutParams3.mSpanIndex];
                i11 = i10 + this.mOrientationHelper.getDecoratedMeasurementInOther(view3);
            }
            layoutDecorated(view3, i8 + layoutParams3.leftMargin, i10 + layoutParams3.topMargin, i9 - layoutParams3.rightMargin, i11 - layoutParams3.bottomMargin);
            if (layoutParams3.isItemRemoved() || layoutParams3.isItemChanged()) {
                layoutChunkResult2.mIgnoreConsumed = true;
            }
            layoutChunkResult2.mFocusable |= view3.isFocusable();
        }
        Arrays.fill(this.mSet, (Object) null);
    }

    private int getMainDirSpec(int i) {
        int i2 = i;
        if (i2 < 0) {
            return MAIN_DIR_SPEC;
        }
        return View.MeasureSpec.makeMeasureSpec(i2, 1073741824);
    }

    private void measureChildWithDecorationsAndMargin(View view, int i, int i2, boolean z) {
        View view2 = view;
        int i3 = i;
        int i4 = i2;
        boolean z2 = z;
        calculateItemDecorationsForChild(view2, this.mDecorInsets);
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view2.getLayoutParams();
        if (z2 || this.mOrientation == 1) {
            i3 = updateSpecWithExtra(i3, layoutParams.leftMargin + this.mDecorInsets.left, layoutParams.rightMargin + this.mDecorInsets.right);
        }
        if (z2 || this.mOrientation == 0) {
            i4 = updateSpecWithExtra(i4, layoutParams.topMargin + this.mDecorInsets.top, layoutParams.bottomMargin + this.mDecorInsets.bottom);
        }
        view2.measure(i3, i4);
    }

    private int updateSpecWithExtra(int i, int i2, int i3) {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        if (i5 == 0 && i6 == 0) {
            return i4;
        }
        int mode = View.MeasureSpec.getMode(i4);
        if (mode == Integer.MIN_VALUE || mode == 1073741824) {
            return View.MeasureSpec.makeMeasureSpec((View.MeasureSpec.getSize(i4) - i5) - i6, mode);
        }
        return i4;
    }

    private void assignSpans(RecyclerView.Recycler recycler, RecyclerView.State state, int i, int i2, boolean z) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        int i8 = i;
        if (z) {
            i3 = 0;
            i4 = i8;
            i5 = 1;
        } else {
            i3 = i8 - 1;
            i4 = -1;
            i5 = -1;
        }
        if (this.mOrientation != 1 || !isLayoutRTL()) {
            i6 = 0;
            i7 = 1;
        } else {
            i6 = this.mSpanCount - 1;
            i7 = -1;
        }
        int i9 = i3;
        while (true) {
            int i10 = i9;
            if (i10 != i4) {
                View view = this.mSet[i10];
                LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
                int access$102 = LayoutParams.access$102(layoutParams, getSpanSize(recycler2, state2, getPosition(view)));
                if (i7 != -1 || layoutParams.mSpanSize <= 1) {
                    int access$002 = LayoutParams.access$002(layoutParams, i6);
                } else {
                    int access$0022 = LayoutParams.access$002(layoutParams, i6 - (layoutParams.mSpanSize - 1));
                }
                i6 += i7 * layoutParams.mSpanSize;
                i9 = i10 + i5;
            } else {
                return;
            }
        }
    }

    public int getSpanCount() {
        return this.mSpanCount;
    }

    public void setSpanCount(int i) {
        Throwable th;
        StringBuilder sb;
        int i2 = i;
        if (i2 != this.mSpanCount) {
            this.mPendingSpanCountChange = true;
            if (i2 < 1) {
                Throwable th2 = th;
                new StringBuilder();
                new IllegalArgumentException(sb.append("Span count should be at least 1. Provided ").append(i2).toString());
                throw th2;
            }
            this.mSpanCount = i2;
            this.mSpanSizeLookup.invalidateSpanIndexCache();
        }
    }

    public static abstract class SpanSizeLookup {
        private boolean mCacheSpanIndices = false;
        final SparseIntArray mSpanIndexCache;

        public abstract int getSpanSize(int i);

        public SpanSizeLookup() {
            SparseIntArray sparseIntArray;
            new SparseIntArray();
            this.mSpanIndexCache = sparseIntArray;
        }

        public void setSpanIndexCacheEnabled(boolean z) {
            this.mCacheSpanIndices = z;
        }

        public void invalidateSpanIndexCache() {
            this.mSpanIndexCache.clear();
        }

        public boolean isSpanIndexCacheEnabled() {
            return this.mCacheSpanIndices;
        }

        /* access modifiers changed from: package-private */
        public int getCachedSpanIndex(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (!this.mCacheSpanIndices) {
                return getSpanIndex(i3, i4);
            }
            int i5 = this.mSpanIndexCache.get(i3, -1);
            if (i5 != -1) {
                return i5;
            }
            int spanIndex = getSpanIndex(i3, i4);
            this.mSpanIndexCache.put(i3, spanIndex);
            return spanIndex;
        }

        public int getSpanIndex(int i, int i2) {
            int i3;
            int findReferenceIndexFromCache;
            int i4 = i;
            int i5 = i2;
            int spanSize = getSpanSize(i4);
            if (spanSize == i5) {
                return 0;
            }
            int i6 = 0;
            int i7 = 0;
            if (this.mCacheSpanIndices && this.mSpanIndexCache.size() > 0 && (findReferenceIndexFromCache = findReferenceIndexFromCache(i4)) >= 0) {
                i6 = this.mSpanIndexCache.get(findReferenceIndexFromCache) + getSpanSize(findReferenceIndexFromCache);
                i7 = findReferenceIndexFromCache + 1;
            }
            for (int i8 = i7; i8 < i4; i8++) {
                int spanSize2 = getSpanSize(i8);
                i3 += spanSize2;
                if (i3 == i5) {
                    i3 = 0;
                } else if (i3 > i5) {
                    i3 = spanSize2;
                }
            }
            if (i3 + spanSize <= i5) {
                return i3;
            }
            return 0;
        }

        /* access modifiers changed from: package-private */
        public int findReferenceIndexFromCache(int i) {
            int i2 = i;
            int i3 = 0;
            int size = this.mSpanIndexCache.size() - 1;
            while (i3 <= size) {
                int i4 = (i3 + size) >>> 1;
                if (this.mSpanIndexCache.keyAt(i4) < i2) {
                    i3 = i4 + 1;
                } else {
                    size = i4 - 1;
                }
            }
            int i5 = i3 - 1;
            if (i5 < 0 || i5 >= this.mSpanIndexCache.size()) {
                return -1;
            }
            return this.mSpanIndexCache.keyAt(i5);
        }

        public int getSpanGroupIndex(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            int i5 = 0;
            int i6 = 0;
            int spanSize = getSpanSize(i3);
            for (int i7 = 0; i7 < i3; i7++) {
                int spanSize2 = getSpanSize(i7);
                i5 += spanSize2;
                if (i5 == i4) {
                    i5 = 0;
                    i6++;
                } else if (i5 > i4) {
                    i5 = spanSize2;
                    i6++;
                }
            }
            if (i5 + spanSize > i4) {
                i6++;
            }
            return i6;
        }
    }

    public boolean supportsPredictiveItemAnimations() {
        return this.mPendingSavedState == null && !this.mPendingSpanCountChange;
    }

    public static final class DefaultSpanSizeLookup extends SpanSizeLookup {
        public int getSpanSize(int i) {
            return 1;
        }

        public int getSpanIndex(int i, int i2) {
            return i % i2;
        }
    }

    public static class LayoutParams extends RecyclerView.LayoutParams {
        public static final int INVALID_SPAN_ID = -1;
        /* access modifiers changed from: private */
        public int mSpanIndex = -1;
        /* access modifiers changed from: private */
        public int mSpanSize = 0;

        static /* synthetic */ int access$002(LayoutParams layoutParams, int i) {
            int i2 = i;
            int i3 = i2;
            layoutParams.mSpanIndex = i3;
            return i2;
        }

        static /* synthetic */ int access$102(LayoutParams layoutParams, int i) {
            int i2 = i;
            int i3 = i2;
            layoutParams.mSpanSize = i3;
            return i2;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(RecyclerView.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public int getSpanIndex() {
            return this.mSpanIndex;
        }

        public int getSpanSize() {
            return this.mSpanSize;
        }
    }
}
