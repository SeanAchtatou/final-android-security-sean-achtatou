package android.support.v7.widget;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import java.lang.reflect.Field;

class DrawableUtils {
    public static final Rect INSETS_NONE;
    private static final String TAG = "DrawableUtils";
    private static Class<?> sInsetsClazz;

    static {
        Rect rect;
        new Rect();
        INSETS_NONE = rect;
        if (Build.VERSION.SDK_INT >= 18) {
            try {
                sInsetsClazz = Class.forName("android.graphics.Insets");
            } catch (ClassNotFoundException e) {
            }
        }
    }

    private DrawableUtils() {
    }

    public static Rect getOpticalBounds(Drawable drawable) {
        Rect rect;
        Drawable drawable2 = drawable;
        if (sInsetsClazz != null) {
            try {
                Drawable unwrap = DrawableCompat.unwrap(drawable2);
                Object invoke = unwrap.getClass().getMethod("getOpticalInsets", new Class[0]).invoke(unwrap, new Object[0]);
                if (invoke != null) {
                    new Rect();
                    Rect rect2 = rect;
                    Field[] fields = sInsetsClazz.getFields();
                    int length = fields.length;
                    for (int i = 0; i < length; i++) {
                        Field field = fields[i];
                        String name = field.getName();
                        boolean z = true;
                        switch (name.hashCode()) {
                            case -1383228885:
                                if (name.equals("bottom")) {
                                    z = true;
                                    break;
                                }
                                break;
                            case 115029:
                                if (name.equals("top")) {
                                    z = true;
                                    break;
                                }
                                break;
                            case 3317767:
                                if (name.equals("left")) {
                                    z = false;
                                    break;
                                }
                                break;
                            case 108511772:
                                if (name.equals("right")) {
                                    z = true;
                                    break;
                                }
                                break;
                        }
                        switch (z) {
                            case false:
                                rect2.left = field.getInt(invoke);
                                break;
                            case true:
                                rect2.top = field.getInt(invoke);
                                break;
                            case true:
                                rect2.right = field.getInt(invoke);
                                break;
                            case true:
                                rect2.bottom = field.getInt(invoke);
                                break;
                        }
                    }
                    return rect2;
                }
            } catch (Exception e) {
                int e2 = Log.e(TAG, "Couldn't obtain the optical insets. Ignoring.");
            }
        }
        return INSETS_NONE;
    }
}
