package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.GravityCompat;
import android.support.v7.appcompat.R;
import android.support.v7.transition.ActionBarTransition;
import android.support.v7.view.ActionBarPolicy;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.BaseMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.ListPopupWindow;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

class ActionMenuPresenter extends BaseMenuPresenter implements ActionProvider.SubUiVisibilityListener {
    private static final String TAG = "ActionMenuPresenter";
    private final SparseBooleanArray mActionButtonGroups;
    /* access modifiers changed from: private */
    public ActionButtonSubmenu mActionButtonPopup;
    private int mActionItemWidthLimit;
    private boolean mExpandedActionViewsExclusive;
    private int mMaxItems;
    private boolean mMaxItemsSet;
    private int mMinCellSize;
    int mOpenSubMenuId;
    /* access modifiers changed from: private */
    public OverflowMenuButton mOverflowButton;
    /* access modifiers changed from: private */
    public OverflowPopup mOverflowPopup;
    private Drawable mPendingOverflowIcon;
    private boolean mPendingOverflowIconSet;
    private ActionMenuPopupCallback mPopupCallback;
    final PopupPresenterCallback mPopupPresenterCallback;
    /* access modifiers changed from: private */
    public OpenOverflowRunnable mPostedOpenRunnable;
    private boolean mReserveOverflow;
    private boolean mReserveOverflowSet;
    private View mScrapActionButtonView;
    private boolean mStrictWidthLimit;
    private int mWidthLimit;
    private boolean mWidthLimitSet;

    static /* synthetic */ OverflowPopup access$202(ActionMenuPresenter actionMenuPresenter, OverflowPopup overflowPopup) {
        OverflowPopup overflowPopup2 = overflowPopup;
        OverflowPopup overflowPopup3 = overflowPopup2;
        actionMenuPresenter.mOverflowPopup = overflowPopup3;
        return overflowPopup2;
    }

    static /* synthetic */ OpenOverflowRunnable access$302(ActionMenuPresenter actionMenuPresenter, OpenOverflowRunnable openOverflowRunnable) {
        OpenOverflowRunnable openOverflowRunnable2 = openOverflowRunnable;
        OpenOverflowRunnable openOverflowRunnable3 = openOverflowRunnable2;
        actionMenuPresenter.mPostedOpenRunnable = openOverflowRunnable3;
        return openOverflowRunnable2;
    }

    static /* synthetic */ ActionButtonSubmenu access$802(ActionMenuPresenter actionMenuPresenter, ActionButtonSubmenu actionButtonSubmenu) {
        ActionButtonSubmenu actionButtonSubmenu2 = actionButtonSubmenu;
        ActionButtonSubmenu actionButtonSubmenu3 = actionButtonSubmenu2;
        actionMenuPresenter.mActionButtonPopup = actionButtonSubmenu3;
        return actionButtonSubmenu2;
    }

    public ActionMenuPresenter(Context context) {
        super(context, R.layout.abc_action_menu_layout, R.layout.abc_action_menu_item_layout);
        SparseBooleanArray sparseBooleanArray;
        PopupPresenterCallback popupPresenterCallback;
        new SparseBooleanArray();
        this.mActionButtonGroups = sparseBooleanArray;
        new PopupPresenterCallback();
        this.mPopupPresenterCallback = popupPresenterCallback;
    }

    public void initForMenu(Context context, MenuBuilder menuBuilder) {
        OverflowMenuButton overflowMenuButton;
        Context context2 = context;
        super.initForMenu(context2, menuBuilder);
        Resources resources = context2.getResources();
        ActionBarPolicy actionBarPolicy = ActionBarPolicy.get(context2);
        if (!this.mReserveOverflowSet) {
            this.mReserveOverflow = actionBarPolicy.showsOverflowMenuButton();
        }
        if (!this.mWidthLimitSet) {
            this.mWidthLimit = actionBarPolicy.getEmbeddedMenuWidthLimit();
        }
        if (!this.mMaxItemsSet) {
            this.mMaxItems = actionBarPolicy.getMaxActionButtons();
        }
        int i = this.mWidthLimit;
        if (this.mReserveOverflow) {
            if (this.mOverflowButton == null) {
                new OverflowMenuButton(this, this.mSystemContext);
                this.mOverflowButton = overflowMenuButton;
                if (this.mPendingOverflowIconSet) {
                    this.mOverflowButton.setImageDrawable(this.mPendingOverflowIcon);
                    this.mPendingOverflowIcon = null;
                    this.mPendingOverflowIconSet = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.mOverflowButton.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.mOverflowButton.getMeasuredWidth();
        } else {
            this.mOverflowButton = null;
        }
        this.mActionItemWidthLimit = i;
        this.mMinCellSize = (int) (56.0f * resources.getDisplayMetrics().density);
        this.mScrapActionButtonView = null;
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (!this.mMaxItemsSet) {
            this.mMaxItems = this.mContext.getResources().getInteger(R.integer.abc_max_action_buttons);
        }
        if (this.mMenu != null) {
            this.mMenu.onItemsChanged(true);
        }
    }

    public void setWidthLimit(int i, boolean z) {
        this.mWidthLimit = i;
        this.mStrictWidthLimit = z;
        this.mWidthLimitSet = true;
    }

    public void setReserveOverflow(boolean z) {
        this.mReserveOverflow = z;
        this.mReserveOverflowSet = true;
    }

    public void setItemLimit(int i) {
        this.mMaxItems = i;
        this.mMaxItemsSet = true;
    }

    public void setExpandedActionViewsExclusive(boolean z) {
        this.mExpandedActionViewsExclusive = z;
    }

    public void setOverflowIcon(Drawable drawable) {
        Drawable drawable2 = drawable;
        if (this.mOverflowButton != null) {
            this.mOverflowButton.setImageDrawable(drawable2);
            return;
        }
        this.mPendingOverflowIconSet = true;
        this.mPendingOverflowIcon = drawable2;
    }

    public Drawable getOverflowIcon() {
        if (this.mOverflowButton != null) {
            return this.mOverflowButton.getDrawable();
        }
        if (this.mPendingOverflowIconSet) {
            return this.mPendingOverflowIcon;
        }
        return null;
    }

    public MenuView getMenuView(ViewGroup viewGroup) {
        MenuView menuView = super.getMenuView(viewGroup);
        ((ActionMenuView) menuView).setPresenter(this);
        return menuView;
    }

    public View getItemView(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        MenuItemImpl menuItemImpl2 = menuItemImpl;
        View view2 = view;
        ViewGroup viewGroup2 = viewGroup;
        View actionView = menuItemImpl2.getActionView();
        if (actionView == null || menuItemImpl2.hasCollapsibleActionView()) {
            actionView = super.getItemView(menuItemImpl2, view2, viewGroup2);
        }
        actionView.setVisibility(menuItemImpl2.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup2;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    public void bindItemView(MenuItemImpl menuItemImpl, MenuView.ItemView itemView) {
        ActionMenuPopupCallback actionMenuPopupCallback;
        MenuView.ItemView itemView2 = itemView;
        itemView2.initialize(menuItemImpl, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) itemView2;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.mMenuView);
        if (this.mPopupCallback == null) {
            new ActionMenuPopupCallback();
            this.mPopupCallback = actionMenuPopupCallback;
        }
        actionMenuItemView.setPopupCallback(this.mPopupCallback);
    }

    public boolean shouldIncludeItem(int i, MenuItemImpl menuItemImpl) {
        return menuItemImpl.isActionButton();
    }

    public void updateMenuView(boolean z) {
        OverflowMenuButton overflowMenuButton;
        boolean z2 = z;
        ViewGroup viewGroup = (ViewGroup) ((View) this.mMenuView).getParent();
        if (viewGroup != null) {
            ActionBarTransition.beginDelayedTransition(viewGroup);
        }
        super.updateMenuView(z2);
        ((View) this.mMenuView).requestLayout();
        if (this.mMenu != null) {
            ArrayList<MenuItemImpl> actionItems = this.mMenu.getActionItems();
            int size = actionItems.size();
            for (int i = 0; i < size; i++) {
                ActionProvider supportActionProvider = actionItems.get(i).getSupportActionProvider();
                if (supportActionProvider != null) {
                    supportActionProvider.setSubUiVisibilityListener(this);
                }
            }
        }
        ArrayList<MenuItemImpl> nonActionItems = this.mMenu != null ? this.mMenu.getNonActionItems() : null;
        boolean z3 = false;
        if (this.mReserveOverflow && nonActionItems != null) {
            int size2 = nonActionItems.size();
            if (size2 == 1) {
                z3 = !nonActionItems.get(0).isActionViewExpanded();
            } else {
                z3 = size2 > 0;
            }
        }
        if (z3) {
            if (this.mOverflowButton == null) {
                new OverflowMenuButton(this, this.mSystemContext);
                this.mOverflowButton = overflowMenuButton;
            }
            ViewGroup viewGroup2 = (ViewGroup) this.mOverflowButton.getParent();
            if (viewGroup2 != this.mMenuView) {
                if (viewGroup2 != null) {
                    viewGroup2.removeView(this.mOverflowButton);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.mMenuView;
                actionMenuView.addView(this.mOverflowButton, actionMenuView.generateOverflowButtonLayoutParams());
            }
        } else if (this.mOverflowButton != null && this.mOverflowButton.getParent() == this.mMenuView) {
            ((ViewGroup) this.mMenuView).removeView(this.mOverflowButton);
        }
        ((ActionMenuView) this.mMenuView).setOverflowReserved(this.mReserveOverflow);
    }

    public boolean filterLeftoverView(ViewGroup viewGroup, int i) {
        ViewGroup viewGroup2 = viewGroup;
        int i2 = i;
        if (viewGroup2.getChildAt(i2) == this.mOverflowButton) {
            return false;
        }
        return super.filterLeftoverView(viewGroup2, i2);
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        SubMenuBuilder subMenuBuilder2;
        ActionButtonSubmenu actionButtonSubmenu;
        SubMenuBuilder subMenuBuilder3 = subMenuBuilder;
        if (!subMenuBuilder3.hasVisibleItems()) {
            return false;
        }
        SubMenuBuilder subMenuBuilder4 = subMenuBuilder3;
        while (true) {
            subMenuBuilder2 = subMenuBuilder4;
            if (subMenuBuilder2.getParentMenu() == this.mMenu) {
                break;
            }
            subMenuBuilder4 = (SubMenuBuilder) subMenuBuilder2.getParentMenu();
        }
        OverflowMenuButton findViewForItem = findViewForItem(subMenuBuilder2.getItem());
        if (findViewForItem == null) {
            if (this.mOverflowButton == null) {
                return false;
            }
            findViewForItem = this.mOverflowButton;
        }
        this.mOpenSubMenuId = subMenuBuilder3.getItem().getItemId();
        new ActionButtonSubmenu(this, this.mContext, subMenuBuilder3);
        this.mActionButtonPopup = actionButtonSubmenu;
        this.mActionButtonPopup.setAnchorView(findViewForItem);
        this.mActionButtonPopup.show();
        boolean onSubMenuSelected = super.onSubMenuSelected(subMenuBuilder3);
        return true;
    }

    private View findViewForItem(MenuItem menuItem) {
        MenuItem menuItem2 = menuItem;
        ViewGroup viewGroup = (ViewGroup) this.mMenuView;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof MenuView.ItemView) && ((MenuView.ItemView) childAt).getItemData() == menuItem2) {
                return childAt;
            }
        }
        return null;
    }

    public boolean showOverflowMenu() {
        OverflowPopup overflowPopup;
        OpenOverflowRunnable openOverflowRunnable;
        if (!this.mReserveOverflow || isOverflowMenuShowing() || this.mMenu == null || this.mMenuView == null || this.mPostedOpenRunnable != null || this.mMenu.getNonActionItems().isEmpty()) {
            return false;
        }
        new OverflowPopup(this, this.mContext, this.mMenu, this.mOverflowButton, true);
        new OpenOverflowRunnable(overflowPopup);
        this.mPostedOpenRunnable = openOverflowRunnable;
        boolean post = ((View) this.mMenuView).post(this.mPostedOpenRunnable);
        boolean onSubMenuSelected = super.onSubMenuSelected(null);
        return true;
    }

    public boolean hideOverflowMenu() {
        if (this.mPostedOpenRunnable == null || this.mMenuView == null) {
            OverflowPopup overflowPopup = this.mOverflowPopup;
            if (overflowPopup == null) {
                return false;
            }
            overflowPopup.dismiss();
            return true;
        }
        boolean removeCallbacks = ((View) this.mMenuView).removeCallbacks(this.mPostedOpenRunnable);
        this.mPostedOpenRunnable = null;
        return true;
    }

    public boolean dismissPopupMenus() {
        return hideOverflowMenu() | hideSubMenus();
    }

    public boolean hideSubMenus() {
        if (this.mActionButtonPopup == null) {
            return false;
        }
        this.mActionButtonPopup.dismiss();
        return true;
    }

    public boolean isOverflowMenuShowing() {
        return this.mOverflowPopup != null && this.mOverflowPopup.isShowing();
    }

    public boolean isOverflowMenuShowPending() {
        return this.mPostedOpenRunnable != null || isOverflowMenuShowing();
    }

    public boolean isOverflowReserved() {
        return this.mReserveOverflow;
    }

    public boolean flagActionItems() {
        ArrayList<MenuItemImpl> visibleItems = this.mMenu.getVisibleItems();
        int size = visibleItems.size();
        int i = this.mMaxItems;
        int i2 = this.mActionItemWidthLimit;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.mMenuView;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        boolean z = false;
        for (int i6 = 0; i6 < size; i6++) {
            MenuItemImpl menuItemImpl = visibleItems.get(i6);
            if (menuItemImpl.requiresActionButton()) {
                i3++;
            } else if (menuItemImpl.requestsActionButton()) {
                i4++;
            } else {
                z = true;
            }
            if (this.mExpandedActionViewsExclusive && menuItemImpl.isActionViewExpanded()) {
                i = 0;
            }
        }
        if (this.mReserveOverflow && (z || i3 + i4 > i)) {
            i--;
        }
        int i7 = i - i3;
        SparseBooleanArray sparseBooleanArray = this.mActionButtonGroups;
        sparseBooleanArray.clear();
        int i8 = 0;
        int i9 = 0;
        if (this.mStrictWidthLimit) {
            i9 = i2 / this.mMinCellSize;
            i8 = this.mMinCellSize + ((i2 % this.mMinCellSize) / i9);
        }
        for (int i10 = 0; i10 < size; i10++) {
            MenuItemImpl menuItemImpl2 = visibleItems.get(i10);
            if (menuItemImpl2.requiresActionButton()) {
                View itemView = getItemView(menuItemImpl2, this.mScrapActionButtonView, viewGroup);
                if (this.mScrapActionButtonView == null) {
                    this.mScrapActionButtonView = itemView;
                }
                if (this.mStrictWidthLimit) {
                    i9 -= ActionMenuView.measureChildForCells(itemView, i8, i9, makeMeasureSpec, 0);
                } else {
                    itemView.measure(makeMeasureSpec, makeMeasureSpec);
                }
                int measuredWidth = itemView.getMeasuredWidth();
                i2 -= measuredWidth;
                if (i5 == 0) {
                    i5 = measuredWidth;
                }
                int groupId = menuItemImpl2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                menuItemImpl2.setIsActionButton(true);
            } else if (menuItemImpl2.requestsActionButton()) {
                int groupId2 = menuItemImpl2.getGroupId();
                boolean z2 = sparseBooleanArray.get(groupId2);
                boolean z3 = (i7 > 0 || z2) && i2 > 0 && (!this.mStrictWidthLimit || i9 > 0);
                if (z3) {
                    View itemView2 = getItemView(menuItemImpl2, this.mScrapActionButtonView, viewGroup);
                    if (this.mScrapActionButtonView == null) {
                        this.mScrapActionButtonView = itemView2;
                    }
                    if (this.mStrictWidthLimit) {
                        int measureChildForCells = ActionMenuView.measureChildForCells(itemView2, i8, i9, makeMeasureSpec, 0);
                        i9 -= measureChildForCells;
                        if (measureChildForCells == 0) {
                            z3 = false;
                        }
                    } else {
                        itemView2.measure(makeMeasureSpec, makeMeasureSpec);
                    }
                    int measuredWidth2 = itemView2.getMeasuredWidth();
                    i2 -= measuredWidth2;
                    if (i5 == 0) {
                        i5 = measuredWidth2;
                    }
                    if (this.mStrictWidthLimit) {
                        z3 &= i2 >= 0;
                    } else {
                        z3 &= i2 + i5 > 0;
                    }
                }
                if (z3 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                } else if (z2) {
                    sparseBooleanArray.put(groupId2, false);
                    for (int i11 = 0; i11 < i10; i11++) {
                        MenuItemImpl menuItemImpl3 = visibleItems.get(i11);
                        if (menuItemImpl3.getGroupId() == groupId2) {
                            if (menuItemImpl3.isActionButton()) {
                                i7++;
                            }
                            menuItemImpl3.setIsActionButton(false);
                        }
                    }
                }
                if (z3) {
                    i7--;
                }
                menuItemImpl2.setIsActionButton(z3);
            } else {
                menuItemImpl2.setIsActionButton(false);
            }
        }
        return true;
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        boolean dismissPopupMenus = dismissPopupMenus();
        super.onCloseMenu(menuBuilder, z);
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState;
        new SavedState();
        SavedState savedState2 = savedState;
        savedState2.openSubMenuId = this.mOpenSubMenuId;
        return savedState2;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        SavedState savedState = (SavedState) parcelable;
        if (savedState.openSubMenuId > 0 && (findItem = this.mMenu.findItem(savedState.openSubMenuId)) != null) {
            boolean onSubMenuSelected = onSubMenuSelected((SubMenuBuilder) findItem.getSubMenu());
        }
    }

    public void onSubUiVisibilityChanged(boolean z) {
        if (z) {
            boolean onSubMenuSelected = super.onSubMenuSelected(null);
        } else {
            this.mMenu.close(false);
        }
    }

    public void setMenuView(ActionMenuView actionMenuView) {
        ActionMenuView actionMenuView2 = actionMenuView;
        this.mMenuView = actionMenuView2;
        actionMenuView2.initialize(this.mMenu);
    }

    private static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR;
        public int openSubMenuId;

        SavedState() {
        }

        SavedState(Parcel parcel) {
            this.openSubMenuId = parcel.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.openSubMenuId);
        }

        static {
            Parcelable.Creator<SavedState> creator;
            new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(Parcel parcel) {
                    SavedState savedState;
                    new SavedState(parcel);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = creator;
        }
    }

    private class OverflowMenuButton extends AppCompatImageView implements ActionMenuView.ActionMenuChildView {
        private final float[] mTempPts = new float[2];
        final /* synthetic */ ActionMenuPresenter this$0;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public OverflowMenuButton(ActionMenuPresenter actionMenuPresenter, Context context) {
            super(context, null, R.attr.actionOverflowButtonStyle);
            View.OnTouchListener onTouchListener;
            ActionMenuPresenter actionMenuPresenter2 = actionMenuPresenter;
            this.this$0 = actionMenuPresenter2;
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            final ActionMenuPresenter actionMenuPresenter3 = actionMenuPresenter2;
            new ListPopupWindow.ForwardingListener(this) {
                public ListPopupWindow getPopup() {
                    if (OverflowMenuButton.this.this$0.mOverflowPopup == null) {
                        return null;
                    }
                    return OverflowMenuButton.this.this$0.mOverflowPopup.getPopup();
                }

                public boolean onForwardingStarted() {
                    boolean showOverflowMenu = OverflowMenuButton.this.this$0.showOverflowMenu();
                    return true;
                }

                public boolean onForwardingStopped() {
                    if (OverflowMenuButton.this.this$0.mPostedOpenRunnable != null) {
                        return false;
                    }
                    boolean hideOverflowMenu = OverflowMenuButton.this.this$0.hideOverflowMenu();
                    return true;
                }
            };
            setOnTouchListener(onTouchListener);
        }

        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }
            playSoundEffect(0);
            boolean showOverflowMenu = this.this$0.showOverflowMenu();
            return true;
        }

        public boolean needsDividerBefore() {
            return false;
        }

        public boolean needsDividerAfter() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = getPaddingLeft() - getPaddingRight();
                int i5 = (width + paddingLeft) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                DrawableCompat.setHotspotBounds(background, i5 - max, paddingTop - max, i5 + max, paddingTop + max);
            }
            return frame;
        }
    }

    private class OverflowPopup extends MenuPopupHelper {
        final /* synthetic */ ActionMenuPresenter this$0;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public OverflowPopup(ActionMenuPresenter actionMenuPresenter, Context context, MenuBuilder menuBuilder, View view, boolean z) {
            super(context, menuBuilder, view, z, R.attr.actionOverflowMenuStyle);
            ActionMenuPresenter actionMenuPresenter2 = actionMenuPresenter;
            this.this$0 = actionMenuPresenter2;
            setGravity(GravityCompat.END);
            setCallback(actionMenuPresenter2.mPopupPresenterCallback);
        }

        public void onDismiss() {
            super.onDismiss();
            if (this.this$0.mMenu != null) {
                this.this$0.mMenu.close();
            }
            OverflowPopup access$202 = ActionMenuPresenter.access$202(this.this$0, null);
        }
    }

    private class ActionButtonSubmenu extends MenuPopupHelper {
        private SubMenuBuilder mSubMenu;
        final /* synthetic */ ActionMenuPresenter this$0;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public ActionButtonSubmenu(android.support.v7.widget.ActionMenuPresenter r16, android.content.Context r17, android.support.v7.view.menu.SubMenuBuilder r18) {
            /*
                r15 = this;
                r0 = r15
                r1 = r16
                r2 = r17
                r3 = r18
                r9 = r0
                r10 = r1
                r9.this$0 = r10
                r9 = r0
                r10 = r2
                r11 = r3
                r12 = 0
                r13 = 0
                int r14 = android.support.v7.appcompat.R.attr.actionOverflowMenuStyle
                r9.<init>(r10, r11, r12, r13, r14)
                r9 = r0
                r10 = r3
                r9.mSubMenu = r10
                r9 = r3
                android.view.MenuItem r9 = r9.getItem()
                android.support.v7.view.menu.MenuItemImpl r9 = (android.support.v7.view.menu.MenuItemImpl) r9
                r4 = r9
                r9 = r4
                boolean r9 = r9.isActionButton()
                if (r9 != 0) goto L_0x003a
                r9 = r0
                r10 = r1
                android.support.v7.widget.ActionMenuPresenter$OverflowMenuButton r10 = r10.mOverflowButton
                if (r10 != 0) goto L_0x006c
                r10 = r1
                android.support.v7.view.menu.MenuView r10 = r10.mMenuView
                android.view.View r10 = (android.view.View) r10
            L_0x0037:
                r9.setAnchorView(r10)
            L_0x003a:
                r9 = r0
                r10 = r1
                android.support.v7.widget.ActionMenuPresenter$PopupPresenterCallback r10 = r10.mPopupPresenterCallback
                r9.setCallback(r10)
                r9 = 0
                r5 = r9
                r9 = r3
                int r9 = r9.size()
                r6 = r9
                r9 = 0
                r7 = r9
            L_0x004b:
                r9 = r7
                r10 = r6
                if (r9 >= r10) goto L_0x0066
                r9 = r3
                r10 = r7
                android.view.MenuItem r9 = r9.getItem(r10)
                r8 = r9
                r9 = r8
                boolean r9 = r9.isVisible()
                if (r9 == 0) goto L_0x0072
                r9 = r8
                android.graphics.drawable.Drawable r9 = r9.getIcon()
                if (r9 == 0) goto L_0x0072
                r9 = 1
                r5 = r9
            L_0x0066:
                r9 = r0
                r10 = r5
                r9.setForceShowIcon(r10)
                return
            L_0x006c:
                r10 = r1
                android.support.v7.widget.ActionMenuPresenter$OverflowMenuButton r10 = r10.mOverflowButton
                goto L_0x0037
            L_0x0072:
                int r7 = r7 + 1
                goto L_0x004b
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActionMenuPresenter.ActionButtonSubmenu.<init>(android.support.v7.widget.ActionMenuPresenter, android.content.Context, android.support.v7.view.menu.SubMenuBuilder):void");
        }

        public void onDismiss() {
            super.onDismiss();
            ActionButtonSubmenu access$802 = ActionMenuPresenter.access$802(this.this$0, null);
            this.this$0.mOpenSubMenuId = 0;
        }
    }

    private class PopupPresenterCallback implements MenuPresenter.Callback {
        private PopupPresenterCallback() {
        }

        public boolean onOpenSubMenu(MenuBuilder menuBuilder) {
            MenuBuilder menuBuilder2 = menuBuilder;
            if (menuBuilder2 == null) {
                return false;
            }
            ActionMenuPresenter.this.mOpenSubMenuId = ((SubMenuBuilder) menuBuilder2).getItem().getItemId();
            MenuPresenter.Callback callback = ActionMenuPresenter.this.getCallback();
            return callback != null ? callback.onOpenSubMenu(menuBuilder2) : false;
        }

        public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
            MenuBuilder menuBuilder2 = menuBuilder;
            boolean z2 = z;
            if (menuBuilder2 instanceof SubMenuBuilder) {
                ((SubMenuBuilder) menuBuilder2).getRootMenu().close(false);
            }
            MenuPresenter.Callback callback = ActionMenuPresenter.this.getCallback();
            if (callback != null) {
                callback.onCloseMenu(menuBuilder2, z2);
            }
        }
    }

    private class OpenOverflowRunnable implements Runnable {
        private OverflowPopup mPopup;

        public OpenOverflowRunnable(OverflowPopup overflowPopup) {
            this.mPopup = overflowPopup;
        }

        public void run() {
            ActionMenuPresenter.this.mMenu.changeMenuMode();
            View view = (View) ActionMenuPresenter.this.mMenuView;
            if (!(view == null || view.getWindowToken() == null || !this.mPopup.tryShow())) {
                OverflowPopup access$202 = ActionMenuPresenter.access$202(ActionMenuPresenter.this, this.mPopup);
            }
            OpenOverflowRunnable access$302 = ActionMenuPresenter.access$302(ActionMenuPresenter.this, null);
        }
    }

    private class ActionMenuPopupCallback extends ActionMenuItemView.PopupCallback {
        private ActionMenuPopupCallback() {
        }

        public ListPopupWindow getPopup() {
            return ActionMenuPresenter.this.mActionButtonPopup != null ? ActionMenuPresenter.this.mActionButtonPopup.getPopup() : null;
        }
    }
}
