package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;

public abstract class LinearSmoothScroller extends RecyclerView.SmoothScroller {
    private static final boolean DEBUG = false;
    private static final float MILLISECONDS_PER_INCH = 25.0f;
    public static final int SNAP_TO_ANY = 0;
    public static final int SNAP_TO_END = 1;
    public static final int SNAP_TO_START = -1;
    private static final String TAG = "LinearSmoothScroller";
    private static final float TARGET_SEEK_EXTRA_SCROLL_RATIO = 1.2f;
    private static final int TARGET_SEEK_SCROLL_DISTANCE_PX = 10000;
    private final float MILLISECONDS_PER_PX;
    protected final DecelerateInterpolator mDecelerateInterpolator;
    protected int mInterimTargetDx = 0;
    protected int mInterimTargetDy = 0;
    protected final LinearInterpolator mLinearInterpolator;
    protected PointF mTargetVector;

    public abstract PointF computeScrollVectorForPosition(int i);

    public LinearSmoothScroller(Context context) {
        LinearInterpolator linearInterpolator;
        DecelerateInterpolator decelerateInterpolator;
        new LinearInterpolator();
        this.mLinearInterpolator = linearInterpolator;
        new DecelerateInterpolator();
        this.mDecelerateInterpolator = decelerateInterpolator;
        this.MILLISECONDS_PER_PX = calculateSpeedPerPixel(context.getResources().getDisplayMetrics());
    }

    /* access modifiers changed from: protected */
    public void onStart() {
    }

    /* access modifiers changed from: protected */
    public void onTargetFound(View view, RecyclerView.State state, RecyclerView.SmoothScroller.Action action) {
        View view2 = view;
        RecyclerView.SmoothScroller.Action action2 = action;
        int calculateDxToMakeVisible = calculateDxToMakeVisible(view2, getHorizontalSnapPreference());
        int calculateDyToMakeVisible = calculateDyToMakeVisible(view2, getVerticalSnapPreference());
        int calculateTimeForDeceleration = calculateTimeForDeceleration((int) Math.sqrt((double) ((calculateDxToMakeVisible * calculateDxToMakeVisible) + (calculateDyToMakeVisible * calculateDyToMakeVisible))));
        if (calculateTimeForDeceleration > 0) {
            action2.update(-calculateDxToMakeVisible, -calculateDyToMakeVisible, calculateTimeForDeceleration, this.mDecelerateInterpolator);
        }
    }

    /* access modifiers changed from: protected */
    public void onSeekTargetStep(int i, int i2, RecyclerView.State state, RecyclerView.SmoothScroller.Action action) {
        int i3 = i;
        int i4 = i2;
        RecyclerView.SmoothScroller.Action action2 = action;
        if (getChildCount() == 0) {
            stop();
            return;
        }
        this.mInterimTargetDx = clampApplyScroll(this.mInterimTargetDx, i3);
        this.mInterimTargetDy = clampApplyScroll(this.mInterimTargetDy, i4);
        if (this.mInterimTargetDx == 0 && this.mInterimTargetDy == 0) {
            updateActionForInterimTarget(action2);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.mInterimTargetDy = 0;
        this.mInterimTargetDx = 0;
        this.mTargetVector = null;
    }

    /* access modifiers changed from: protected */
    public float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
        return MILLISECONDS_PER_INCH / ((float) displayMetrics.densityDpi);
    }

    /* access modifiers changed from: protected */
    public int calculateTimeForDeceleration(int i) {
        return (int) Math.ceil(((double) calculateTimeForScrolling(i)) / 0.3356d);
    }

    /* access modifiers changed from: protected */
    public int calculateTimeForScrolling(int i) {
        return (int) Math.ceil((double) (((float) Math.abs(i)) * this.MILLISECONDS_PER_PX));
    }

    /* access modifiers changed from: protected */
    public int getHorizontalSnapPreference() {
        return (this.mTargetVector == null || this.mTargetVector.x == 0.0f) ? 0 : this.mTargetVector.x > 0.0f ? 1 : -1;
    }

    /* access modifiers changed from: protected */
    public int getVerticalSnapPreference() {
        return (this.mTargetVector == null || this.mTargetVector.y == 0.0f) ? 0 : this.mTargetVector.y > 0.0f ? 1 : -1;
    }

    /* access modifiers changed from: protected */
    public void updateActionForInterimTarget(RecyclerView.SmoothScroller.Action action) {
        RecyclerView.SmoothScroller.Action action2 = action;
        PointF computeScrollVectorForPosition = computeScrollVectorForPosition(getTargetPosition());
        if (computeScrollVectorForPosition == null || (computeScrollVectorForPosition.x == 0.0f && computeScrollVectorForPosition.y == 0.0f)) {
            int e = Log.e(TAG, "To support smooth scrolling, you should override \nLayoutManager#computeScrollVectorForPosition.\nFalling back to instant scroll");
            action2.jumpTo(getTargetPosition());
            stop();
            return;
        }
        normalize(computeScrollVectorForPosition);
        this.mTargetVector = computeScrollVectorForPosition;
        this.mInterimTargetDx = (int) (10000.0f * computeScrollVectorForPosition.x);
        this.mInterimTargetDy = (int) (10000.0f * computeScrollVectorForPosition.y);
        action2.update((int) (((float) this.mInterimTargetDx) * TARGET_SEEK_EXTRA_SCROLL_RATIO), (int) (((float) this.mInterimTargetDy) * TARGET_SEEK_EXTRA_SCROLL_RATIO), (int) (((float) calculateTimeForScrolling(TARGET_SEEK_SCROLL_DISTANCE_PX)) * TARGET_SEEK_EXTRA_SCROLL_RATIO), this.mLinearInterpolator);
    }

    private int clampApplyScroll(int i, int i2) {
        int i3 = i;
        int i4 = i3;
        int i5 = i3 - i2;
        if (i4 * i5 <= 0) {
            return 0;
        }
        return i5;
    }

    public int calculateDtToFit(int i, int i2, int i3, int i4, int i5) {
        Throwable th;
        int i6 = i;
        int i7 = i2;
        int i8 = i3;
        int i9 = i4;
        switch (i5) {
            case -1:
                return i8 - i6;
            case 0:
                int i10 = i8 - i6;
                if (i10 > 0) {
                    return i10;
                }
                int i11 = i9 - i7;
                if (i11 < 0) {
                    return i11;
                }
                return 0;
            case 1:
                return i9 - i7;
            default:
                Throwable th2 = th;
                new IllegalArgumentException("snap preference should be one of the constants defined in SmoothScroller, starting with SNAP_");
                throw th2;
        }
    }

    public int calculateDyToMakeVisible(View view, int i) {
        View view2 = view;
        int i2 = i;
        RecyclerView.LayoutManager layoutManager = getLayoutManager();
        if (!layoutManager.canScrollVertically()) {
            return 0;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view2.getLayoutParams();
        return calculateDtToFit(layoutManager.getDecoratedTop(view2) - layoutParams.topMargin, layoutManager.getDecoratedBottom(view2) + layoutParams.bottomMargin, layoutManager.getPaddingTop(), layoutManager.getHeight() - layoutManager.getPaddingBottom(), i2);
    }

    public int calculateDxToMakeVisible(View view, int i) {
        View view2 = view;
        int i2 = i;
        RecyclerView.LayoutManager layoutManager = getLayoutManager();
        if (!layoutManager.canScrollHorizontally()) {
            return 0;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view2.getLayoutParams();
        return calculateDtToFit(layoutManager.getDecoratedLeft(view2) - layoutParams.leftMargin, layoutManager.getDecoratedRight(view2) + layoutParams.rightMargin, layoutManager.getPaddingLeft(), layoutManager.getWidth() - layoutManager.getPaddingRight(), i2);
    }
}
