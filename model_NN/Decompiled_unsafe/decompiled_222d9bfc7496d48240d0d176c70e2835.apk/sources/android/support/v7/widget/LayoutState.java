package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;

class LayoutState {
    static final int INVALID_LAYOUT = Integer.MIN_VALUE;
    static final int ITEM_DIRECTION_HEAD = -1;
    static final int ITEM_DIRECTION_TAIL = 1;
    static final int LAYOUT_END = 1;
    static final int LAYOUT_START = -1;
    static final int SCOLLING_OFFSET_NaN = Integer.MIN_VALUE;
    static final String TAG = "LayoutState";
    int mAvailable;
    int mCurrentPosition;
    int mEndLine = 0;
    int mItemDirection;
    int mLayoutDirection;
    int mStartLine = 0;

    LayoutState() {
    }

    /* access modifiers changed from: package-private */
    public boolean hasMore(RecyclerView.State state) {
        return this.mCurrentPosition >= 0 && this.mCurrentPosition < state.getItemCount();
    }

    /* access modifiers changed from: package-private */
    public View next(RecyclerView.Recycler recycler) {
        this.mCurrentPosition = this.mCurrentPosition + this.mItemDirection;
        return recycler.getViewForPosition(this.mCurrentPosition);
    }

    public String toString() {
        StringBuilder sb;
        new StringBuilder();
        return sb.append("LayoutState{mAvailable=").append(this.mAvailable).append(", mCurrentPosition=").append(this.mCurrentPosition).append(", mItemDirection=").append(this.mItemDirection).append(", mLayoutDirection=").append(this.mLayoutDirection).append(", mStartLine=").append(this.mStartLine).append(", mEndLine=").append(this.mEndLine).append('}').toString();
    }
}
