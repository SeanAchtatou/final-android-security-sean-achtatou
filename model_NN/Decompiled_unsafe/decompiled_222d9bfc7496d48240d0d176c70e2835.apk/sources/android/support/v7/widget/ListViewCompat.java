package android.support.v7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.graphics.drawable.DrawableWrapper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.lang.reflect.Field;

public class ListViewCompat extends ListView {
    public static final int INVALID_POSITION = -1;
    public static final int NO_POSITION = -1;
    private static final int[] STATE_SET_NOTHING = {0};
    private Field mIsChildViewEnabled;
    protected int mMotionPosition;
    int mSelectionBottomPadding;
    int mSelectionLeftPadding;
    int mSelectionRightPadding;
    int mSelectionTopPadding;
    private GateKeeperDrawable mSelector;
    final Rect mSelectorRect;

    public ListViewCompat(Context context) {
        this(context, null);
    }

    public ListViewCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ListViewCompat(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Rect rect;
        new Rect();
        this.mSelectorRect = rect;
        this.mSelectionLeftPadding = 0;
        this.mSelectionTopPadding = 0;
        this.mSelectionRightPadding = 0;
        this.mSelectionBottomPadding = 0;
        try {
            this.mIsChildViewEnabled = AbsListView.class.getDeclaredField("mIsChildViewEnabled");
            this.mIsChildViewEnabled.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public void setSelector(Drawable drawable) {
        GateKeeperDrawable gateKeeperDrawable;
        Rect rect;
        GateKeeperDrawable gateKeeperDrawable2;
        Drawable drawable2 = drawable;
        if (drawable2 != null) {
            gateKeeperDrawable = gateKeeperDrawable2;
            new GateKeeperDrawable(drawable2);
        } else {
            gateKeeperDrawable = null;
        }
        this.mSelector = gateKeeperDrawable;
        super.setSelector(this.mSelector);
        new Rect();
        Rect rect2 = rect;
        if (drawable2 != null) {
            boolean padding = drawable2.getPadding(rect2);
        }
        this.mSelectionLeftPadding = rect2.left;
        this.mSelectionTopPadding = rect2.top;
        this.mSelectionRightPadding = rect2.right;
        this.mSelectionBottomPadding = rect2.bottom;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        setSelectorEnabled(true);
        updateSelectorStateCompat();
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        Canvas canvas2 = canvas;
        drawSelectorCompat(canvas2);
        super.dispatchDraw(canvas2);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        switch (motionEvent2.getAction()) {
            case 0:
                this.mMotionPosition = pointToPosition((int) motionEvent2.getX(), (int) motionEvent2.getY());
                break;
        }
        return super.onTouchEvent(motionEvent2);
    }

    /* access modifiers changed from: protected */
    public void updateSelectorStateCompat() {
        Drawable selector = getSelector();
        if (selector != null && shouldShowSelectorCompat()) {
            boolean state = selector.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: protected */
    public boolean shouldShowSelectorCompat() {
        return touchModeDrawsInPressedStateCompat() && isPressed();
    }

    /* access modifiers changed from: protected */
    public boolean touchModeDrawsInPressedStateCompat() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void drawSelectorCompat(Canvas canvas) {
        Drawable selector;
        Canvas canvas2 = canvas;
        if (!this.mSelectorRect.isEmpty() && (selector = getSelector()) != null) {
            selector.setBounds(this.mSelectorRect);
            selector.draw(canvas2);
        }
    }

    public int lookForSelectablePosition(int i, boolean z) {
        int i2;
        int i3 = i;
        boolean z2 = z;
        ListAdapter adapter = getAdapter();
        if (adapter == null || isInTouchMode()) {
            return -1;
        }
        int count = adapter.getCount();
        if (!getAdapter().areAllItemsEnabled()) {
            if (z2) {
                i2 = Math.max(0, i3);
                while (i2 < count && !adapter.isEnabled(i2)) {
                    i2++;
                }
            } else {
                int min = Math.min(i3, count - 1);
                while (i2 >= 0 && !adapter.isEnabled(i2)) {
                    min = i2 - 1;
                }
            }
            if (i2 < 0 || i2 >= count) {
                return -1;
            }
            return i2;
        } else if (i3 < 0 || i3 >= count) {
            return -1;
        } else {
            return i3;
        }
    }

    /* access modifiers changed from: protected */
    public void positionSelectorLikeTouchCompat(int i, View view, float f, float f2) {
        int i2 = i;
        float f3 = f;
        float f4 = f2;
        positionSelectorLikeFocusCompat(i2, view);
        Drawable selector = getSelector();
        if (selector != null && i2 != -1) {
            DrawableCompat.setHotspot(selector, f3, f4);
        }
    }

    /* access modifiers changed from: protected */
    public void positionSelectorLikeFocusCompat(int i, View view) {
        int i2 = i;
        View view2 = view;
        Drawable selector = getSelector();
        boolean z = (selector == null || i2 == -1) ? false : true;
        if (z) {
            boolean visible = selector.setVisible(false, false);
        }
        positionSelectorCompat(i2, view2);
        if (z) {
            Rect rect = this.mSelectorRect;
            float exactCenterX = rect.exactCenterX();
            float exactCenterY = rect.exactCenterY();
            boolean visible2 = selector.setVisible(getVisibility() == 0, false);
            DrawableCompat.setHotspot(selector, exactCenterX, exactCenterY);
        }
    }

    /* access modifiers changed from: protected */
    public void positionSelectorCompat(int i, View view) {
        int i2 = i;
        View view2 = view;
        Rect rect = this.mSelectorRect;
        rect.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
        rect.left -= this.mSelectionLeftPadding;
        rect.top -= this.mSelectionTopPadding;
        rect.right += this.mSelectionRightPadding;
        rect.bottom += this.mSelectionBottomPadding;
        try {
            boolean z = this.mIsChildViewEnabled.getBoolean(this);
            if (view2.isEnabled() != z) {
                this.mIsChildViewEnabled.set(this, Boolean.valueOf(!z));
                if (i2 != -1) {
                    refreshDrawableState();
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public int measureHeightOfChildrenCompat(int i, int i2, int i3, int i4, int i5) {
        int makeMeasureSpec;
        int i6;
        int i7 = i;
        int i8 = i4;
        int i9 = i5;
        int listPaddingTop = getListPaddingTop();
        int listPaddingBottom = getListPaddingBottom();
        int listPaddingLeft = getListPaddingLeft();
        int listPaddingRight = getListPaddingRight();
        int dividerHeight = getDividerHeight();
        Drawable divider = getDivider();
        ListAdapter adapter = getAdapter();
        if (adapter == null) {
            return listPaddingTop + listPaddingBottom;
        }
        int i10 = listPaddingTop + listPaddingBottom;
        int i11 = (dividerHeight <= 0 || divider == null) ? 0 : dividerHeight;
        int i12 = 0;
        View view = null;
        int i13 = 0;
        int count = adapter.getCount();
        for (int i14 = 0; i14 < count; i14++) {
            int itemViewType = adapter.getItemViewType(i14);
            if (itemViewType != i13) {
                view = null;
                i13 = itemViewType;
            }
            view = adapter.getView(i14, view, this);
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = generateDefaultLayoutParams();
                view.setLayoutParams(layoutParams);
            }
            if (layoutParams.height > 0) {
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824);
            } else {
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
            }
            view.measure(i7, makeMeasureSpec);
            view.forceLayout();
            if (i14 > 0) {
                i10 += i11;
            }
            i10 += view.getMeasuredHeight();
            if (i10 >= i8) {
                if (i9 < 0 || i14 <= i9 || i12 <= 0 || i10 == i8) {
                    i6 = i8;
                } else {
                    i6 = i12;
                }
                return i6;
            }
            if (i9 >= 0 && i14 >= i9) {
                i12 = i10;
            }
        }
        return i10;
    }

    /* access modifiers changed from: protected */
    public void setSelectorEnabled(boolean z) {
        boolean z2 = z;
        if (this.mSelector != null) {
            this.mSelector.setEnabled(z2);
        }
    }

    private static class GateKeeperDrawable extends DrawableWrapper {
        private boolean mEnabled = true;

        public GateKeeperDrawable(Drawable drawable) {
            super(drawable);
        }

        /* access modifiers changed from: package-private */
        public void setEnabled(boolean z) {
            this.mEnabled = z;
        }

        public boolean setState(int[] iArr) {
            int[] iArr2 = iArr;
            if (this.mEnabled) {
                return super.setState(iArr2);
            }
            return false;
        }

        public void draw(Canvas canvas) {
            Canvas canvas2 = canvas;
            if (this.mEnabled) {
                super.draw(canvas2);
            }
        }

        public void setHotspot(float f, float f2) {
            float f3 = f;
            float f4 = f2;
            if (this.mEnabled) {
                super.setHotspot(f3, f4);
            }
        }

        public void setHotspotBounds(int i, int i2, int i3, int i4) {
            int i5 = i;
            int i6 = i2;
            int i7 = i3;
            int i8 = i4;
            if (this.mEnabled) {
                super.setHotspotBounds(i5, i6, i7, i8);
            }
        }

        public boolean setVisible(boolean z, boolean z2) {
            boolean z3 = z;
            boolean z4 = z2;
            if (this.mEnabled) {
                return super.setVisible(z3, z4);
            }
            return false;
        }
    }
}
