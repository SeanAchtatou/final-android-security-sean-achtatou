package android.support.v7.widget;

import android.content.Context;
import android.os.Build;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.ActionMode;
import android.support.v7.view.menu.MenuBuilder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActionBarContextView extends AbsActionBarView {
    private static final String TAG = "ActionBarContextView";
    private View mClose;
    private int mCloseItemLayout;
    private View mCustomView;
    private CharSequence mSubtitle;
    private int mSubtitleStyleRes;
    private TextView mSubtitleView;
    private CharSequence mTitle;
    private LinearLayout mTitleLayout;
    private boolean mTitleOptional;
    private int mTitleStyleRes;
    private TextView mTitleView;

    public /* bridge */ /* synthetic */ void animateToVisibility(int i) {
        super.animateToVisibility(i);
    }

    public /* bridge */ /* synthetic */ boolean canShowOverflowMenu() {
        return super.canShowOverflowMenu();
    }

    public /* bridge */ /* synthetic */ void dismissPopupMenus() {
        super.dismissPopupMenus();
    }

    public /* bridge */ /* synthetic */ int getAnimatedVisibility() {
        return super.getAnimatedVisibility();
    }

    public /* bridge */ /* synthetic */ int getContentHeight() {
        return super.getContentHeight();
    }

    public /* bridge */ /* synthetic */ boolean isOverflowMenuShowPending() {
        return super.isOverflowMenuShowPending();
    }

    public /* bridge */ /* synthetic */ boolean isOverflowReserved() {
        return super.isOverflowReserved();
    }

    public /* bridge */ /* synthetic */ boolean onHoverEvent(MotionEvent motionEvent) {
        return super.onHoverEvent(motionEvent);
    }

    public /* bridge */ /* synthetic */ boolean onTouchEvent(MotionEvent motionEvent) {
        return super.onTouchEvent(motionEvent);
    }

    public /* bridge */ /* synthetic */ void postShowOverflowMenu() {
        super.postShowOverflowMenu();
    }

    public /* bridge */ /* synthetic */ void setVisibility(int i) {
        super.setVisibility(i);
    }

    public /* bridge */ /* synthetic */ ViewPropertyAnimatorCompat setupAnimatorToVisibility(int i, long j) {
        return super.setupAnimatorToVisibility(i, j);
    }

    public ActionBarContextView(Context context) {
        this(context, null);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.actionModeStyle);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ActionBarContextView(android.content.Context r11, android.util.AttributeSet r12, int r13) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r3 = r13
            r5 = r0
            r6 = r1
            r7 = r2
            r8 = r3
            r5.<init>(r6, r7, r8)
            r5 = r1
            r6 = r2
            int[] r7 = android.support.v7.appcompat.R.styleable.ActionMode
            r8 = r3
            r9 = 0
            android.support.v7.widget.TintTypedArray r5 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r5, r6, r7, r8, r9)
            r4 = r5
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ActionMode_background
            android.graphics.drawable.Drawable r6 = r6.getDrawable(r7)
            r5.setBackgroundDrawable(r6)
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ActionMode_titleTextStyle
            r8 = 0
            int r6 = r6.getResourceId(r7, r8)
            r5.mTitleStyleRes = r6
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ActionMode_subtitleTextStyle
            r8 = 0
            int r6 = r6.getResourceId(r7, r8)
            r5.mSubtitleStyleRes = r6
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ActionMode_height
            r8 = 0
            int r6 = r6.getLayoutDimension(r7, r8)
            r5.mContentHeight = r6
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ActionMode_closeItemLayout
            int r8 = android.support.v7.appcompat.R.layout.abc_action_mode_close_item_material
            int r6 = r6.getResourceId(r7, r8)
            r5.mCloseItemLayout = r6
            r5 = r4
            r5.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActionBarContextView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mActionMenuPresenter != null) {
            boolean hideOverflowMenu = this.mActionMenuPresenter.hideOverflowMenu();
            boolean hideSubMenus = this.mActionMenuPresenter.hideSubMenus();
        }
    }

    public void setContentHeight(int i) {
        this.mContentHeight = i;
    }

    public void setCustomView(View view) {
        View view2 = view;
        if (this.mCustomView != null) {
            removeView(this.mCustomView);
        }
        this.mCustomView = view2;
        if (!(view2 == null || this.mTitleLayout == null)) {
            removeView(this.mTitleLayout);
            this.mTitleLayout = null;
        }
        if (view2 != null) {
            addView(view2);
        }
        requestLayout();
    }

    public void setTitle(CharSequence charSequence) {
        this.mTitle = charSequence;
        initTitle();
    }

    public void setSubtitle(CharSequence charSequence) {
        this.mSubtitle = charSequence;
        initTitle();
    }

    public CharSequence getTitle() {
        return this.mTitle;
    }

    public CharSequence getSubtitle() {
        return this.mSubtitle;
    }

    private void initTitle() {
        if (this.mTitleLayout == null) {
            View inflate = LayoutInflater.from(getContext()).inflate(R.layout.abc_action_bar_title_item, this);
            this.mTitleLayout = (LinearLayout) getChildAt(getChildCount() - 1);
            this.mTitleView = (TextView) this.mTitleLayout.findViewById(R.id.action_bar_title);
            this.mSubtitleView = (TextView) this.mTitleLayout.findViewById(R.id.action_bar_subtitle);
            if (this.mTitleStyleRes != 0) {
                this.mTitleView.setTextAppearance(getContext(), this.mTitleStyleRes);
            }
            if (this.mSubtitleStyleRes != 0) {
                this.mSubtitleView.setTextAppearance(getContext(), this.mSubtitleStyleRes);
            }
        }
        this.mTitleView.setText(this.mTitle);
        this.mSubtitleView.setText(this.mSubtitle);
        boolean z = !TextUtils.isEmpty(this.mTitle);
        boolean z2 = !TextUtils.isEmpty(this.mSubtitle);
        this.mSubtitleView.setVisibility(z2 ? 0 : 8);
        this.mTitleLayout.setVisibility((z || z2) ? 0 : 8);
        if (this.mTitleLayout.getParent() == null) {
            addView(this.mTitleLayout);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.ActionBarContextView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void initForMode(ActionMode actionMode) {
        View.OnClickListener onClickListener;
        ActionMenuPresenter actionMenuPresenter;
        ViewGroup.LayoutParams layoutParams;
        ActionMode actionMode2 = actionMode;
        if (this.mClose == null) {
            this.mClose = LayoutInflater.from(getContext()).inflate(this.mCloseItemLayout, (ViewGroup) this, false);
            addView(this.mClose);
        } else if (this.mClose.getParent() == null) {
            addView(this.mClose);
        }
        final ActionMode actionMode3 = actionMode2;
        new View.OnClickListener() {
            public void onClick(View view) {
                actionMode3.finish();
            }
        };
        this.mClose.findViewById(R.id.action_mode_close_button).setOnClickListener(onClickListener);
        MenuBuilder menuBuilder = (MenuBuilder) actionMode2.getMenu();
        if (this.mActionMenuPresenter != null) {
            boolean dismissPopupMenus = this.mActionMenuPresenter.dismissPopupMenus();
        }
        new ActionMenuPresenter(getContext());
        this.mActionMenuPresenter = actionMenuPresenter;
        this.mActionMenuPresenter.setReserveOverflow(true);
        new ViewGroup.LayoutParams(-2, -1);
        menuBuilder.addMenuPresenter(this.mActionMenuPresenter, this.mPopupContext);
        this.mMenuView = (ActionMenuView) this.mActionMenuPresenter.getMenuView(this);
        this.mMenuView.setBackgroundDrawable(null);
        addView(this.mMenuView, layoutParams);
    }

    public void closeMode() {
        if (this.mClose == null) {
            killMode();
        }
    }

    public void killMode() {
        removeAllViews();
        this.mCustomView = null;
        this.mMenuView = null;
    }

    public boolean showOverflowMenu() {
        if (this.mActionMenuPresenter != null) {
            return this.mActionMenuPresenter.showOverflowMenu();
        }
        return false;
    }

    public boolean hideOverflowMenu() {
        if (this.mActionMenuPresenter != null) {
            return this.mActionMenuPresenter.hideOverflowMenu();
        }
        return false;
    }

    public boolean isOverflowMenuShowing() {
        if (this.mActionMenuPresenter != null) {
            return this.mActionMenuPresenter.isOverflowMenuShowing();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        ViewGroup.LayoutParams layoutParams;
        new ViewGroup.MarginLayoutParams(-1, -2);
        return layoutParams;
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        ViewGroup.LayoutParams layoutParams;
        new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size;
        Throwable th;
        StringBuilder sb;
        Throwable th2;
        StringBuilder sb2;
        int i3 = i;
        int i4 = i2;
        if (View.MeasureSpec.getMode(i3) != 1073741824) {
            Throwable th3 = th2;
            new StringBuilder();
            new IllegalStateException(sb2.append(getClass().getSimpleName()).append(" can only be used ").append("with android:layout_width=\"match_parent\" (or fill_parent)").toString());
            throw th3;
        } else if (View.MeasureSpec.getMode(i4) == 0) {
            Throwable th4 = th;
            new StringBuilder();
            new IllegalStateException(sb.append(getClass().getSimpleName()).append(" can only be used ").append("with android:layout_height=\"wrap_content\"").toString());
            throw th4;
        } else {
            int size2 = View.MeasureSpec.getSize(i3);
            if (this.mContentHeight > 0) {
                size = this.mContentHeight;
            } else {
                size = View.MeasureSpec.getSize(i4);
            }
            int i5 = size;
            int paddingTop = getPaddingTop() + getPaddingBottom();
            int paddingLeft = (size2 - getPaddingLeft()) - getPaddingRight();
            int i6 = i5 - paddingTop;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i6, Integer.MIN_VALUE);
            if (this.mClose != null) {
                int measureChildView = measureChildView(this.mClose, paddingLeft, makeMeasureSpec, 0);
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.mClose.getLayoutParams();
                paddingLeft = measureChildView - (marginLayoutParams.leftMargin + marginLayoutParams.rightMargin);
            }
            if (this.mMenuView != null && this.mMenuView.getParent() == this) {
                paddingLeft = measureChildView(this.mMenuView, paddingLeft, makeMeasureSpec, 0);
            }
            if (this.mTitleLayout != null && this.mCustomView == null) {
                if (this.mTitleOptional) {
                    this.mTitleLayout.measure(View.MeasureSpec.makeMeasureSpec(0, 0), makeMeasureSpec);
                    int measuredWidth = this.mTitleLayout.getMeasuredWidth();
                    boolean z = measuredWidth <= paddingLeft;
                    if (z) {
                        paddingLeft -= measuredWidth;
                    }
                    this.mTitleLayout.setVisibility(z ? 0 : 8);
                } else {
                    paddingLeft = measureChildView(this.mTitleLayout, paddingLeft, makeMeasureSpec, 0);
                }
            }
            if (this.mCustomView != null) {
                ViewGroup.LayoutParams layoutParams = this.mCustomView.getLayoutParams();
                this.mCustomView.measure(View.MeasureSpec.makeMeasureSpec(layoutParams.width >= 0 ? Math.min(layoutParams.width, paddingLeft) : paddingLeft, layoutParams.width != -2 ? 1073741824 : Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(layoutParams.height >= 0 ? Math.min(layoutParams.height, i6) : i6, layoutParams.height != -2 ? 1073741824 : Integer.MIN_VALUE));
            }
            if (this.mContentHeight <= 0) {
                int i7 = 0;
                int childCount = getChildCount();
                for (int i8 = 0; i8 < childCount; i8++) {
                    int measuredHeight = getChildAt(i8).getMeasuredHeight() + paddingTop;
                    if (measuredHeight > i7) {
                        i7 = measuredHeight;
                    }
                }
                setMeasuredDimension(size2, i7);
                return;
            }
            setMeasuredDimension(size2, i5);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6 = i;
        int i7 = i2;
        int i8 = i3;
        int i9 = i4;
        boolean isLayoutRtl = ViewUtils.isLayoutRtl(this);
        int paddingRight = isLayoutRtl ? (i8 - i6) - getPaddingRight() : getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingTop2 = ((i9 - i7) - getPaddingTop()) - getPaddingBottom();
        if (!(this.mClose == null || this.mClose.getVisibility() == 8)) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.mClose.getLayoutParams();
            int i10 = isLayoutRtl ? marginLayoutParams.rightMargin : marginLayoutParams.leftMargin;
            if (isLayoutRtl) {
                i5 = marginLayoutParams.leftMargin;
            } else {
                i5 = marginLayoutParams.rightMargin;
            }
            int next = next(paddingRight, i10, isLayoutRtl);
            paddingRight = next(next + positionChild(this.mClose, next, paddingTop, paddingTop2, isLayoutRtl), i5, isLayoutRtl);
        }
        if (!(this.mTitleLayout == null || this.mCustomView != null || this.mTitleLayout.getVisibility() == 8)) {
            paddingRight += positionChild(this.mTitleLayout, paddingRight, paddingTop, paddingTop2, isLayoutRtl);
        }
        if (this.mCustomView != null) {
            int positionChild = paddingRight + positionChild(this.mCustomView, paddingRight, paddingTop, paddingTop2, isLayoutRtl);
        }
        int paddingLeft = isLayoutRtl ? getPaddingLeft() : (i8 - i6) - getPaddingRight();
        if (this.mMenuView != null) {
            int positionChild2 = paddingLeft + positionChild(this.mMenuView, paddingLeft, paddingTop, paddingTop2, !isLayoutRtl);
        }
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
        if (Build.VERSION.SDK_INT < 14) {
            return;
        }
        if (accessibilityEvent2.getEventType() == 32) {
            accessibilityEvent2.setSource(this);
            accessibilityEvent2.setClassName(getClass().getName());
            accessibilityEvent2.setPackageName(getContext().getPackageName());
            accessibilityEvent2.setContentDescription(this.mTitle);
            return;
        }
        super.onInitializeAccessibilityEvent(accessibilityEvent2);
    }

    public void setTitleOptional(boolean z) {
        boolean z2 = z;
        if (z2 != this.mTitleOptional) {
            requestLayout();
        }
        this.mTitleOptional = z2;
    }

    public boolean isTitleOptional() {
        return this.mTitleOptional;
    }
}
