package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.StyleRes;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MarginLayoutParamsCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.appcompat.R;
import android.support.v7.view.CollapsibleActionView;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.widget.ActionMenuView;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class Toolbar extends ViewGroup {
    private static final String TAG = "Toolbar";
    private MenuPresenter.Callback mActionMenuPresenterCallback;
    /* access modifiers changed from: private */
    public int mButtonGravity;
    /* access modifiers changed from: private */
    public ImageButton mCollapseButtonView;
    private CharSequence mCollapseDescription;
    private Drawable mCollapseIcon;
    private boolean mCollapsible;
    private final RtlSpacingHelper mContentInsets;
    private boolean mEatingHover;
    private boolean mEatingTouch;
    View mExpandedActionView;
    private ExpandedActionViewMenuPresenter mExpandedMenuPresenter;
    private int mGravity;
    private final ArrayList<View> mHiddenViews;
    private ImageView mLogoView;
    private int mMaxButtonHeight;
    private MenuBuilder.Callback mMenuBuilderCallback;
    private ActionMenuView mMenuView;
    private final ActionMenuView.OnMenuItemClickListener mMenuViewItemClickListener;
    private ImageButton mNavButtonView;
    /* access modifiers changed from: private */
    public OnMenuItemClickListener mOnMenuItemClickListener;
    private ActionMenuPresenter mOuterActionMenuPresenter;
    private Context mPopupContext;
    private int mPopupTheme;
    private final Runnable mShowOverflowMenuRunnable;
    private CharSequence mSubtitleText;
    private int mSubtitleTextAppearance;
    private int mSubtitleTextColor;
    private TextView mSubtitleTextView;
    private final int[] mTempMargins;
    private final ArrayList<View> mTempViews;
    private final TintManager mTintManager;
    private int mTitleMarginBottom;
    private int mTitleMarginEnd;
    private int mTitleMarginStart;
    private int mTitleMarginTop;
    private CharSequence mTitleText;
    private int mTitleTextAppearance;
    private int mTitleTextColor;
    private TextView mTitleTextView;
    private ToolbarWidgetWrapper mWrapper;

    public interface OnMenuItemClickListener {
        boolean onMenuItemClick(MenuItem menuItem);
    }

    public Toolbar(Context context) {
        this(context, null);
    }

    public Toolbar(Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.toolbarStyle);
    }

    /* JADX WARN: Type inference failed for: r28v1 */
    /* JADX WARN: Type inference failed for: r28v2 */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Toolbar(android.content.Context r31, @android.support.annotation.Nullable android.util.AttributeSet r32, int r33) {
        /*
            r30 = this;
            r2 = r30
            r3 = r31
            r4 = r32
            r5 = r33
            r21 = r2
            r22 = r3
            r23 = r4
            r24 = r5
            r21.<init>(r22, r23, r24)
            r21 = r2
            android.support.v7.widget.RtlSpacingHelper r22 = new android.support.v7.widget.RtlSpacingHelper
            r28 = r22
            r22 = r28
            r23 = r28
            r23.<init>()
            r0 = r22
            r1 = r21
            r1.mContentInsets = r0
            r21 = r2
            r22 = 8388627(0x800013, float:1.175497E-38)
            r0 = r22
            r1 = r21
            r1.mGravity = r0
            r21 = r2
            java.util.ArrayList r22 = new java.util.ArrayList
            r28 = r22
            r22 = r28
            r23 = r28
            r23.<init>()
            r0 = r22
            r1 = r21
            r1.mTempViews = r0
            r21 = r2
            java.util.ArrayList r22 = new java.util.ArrayList
            r28 = r22
            r22 = r28
            r23 = r28
            r23.<init>()
            r0 = r22
            r1 = r21
            r1.mHiddenViews = r0
            r21 = r2
            r22 = 2
            r0 = r22
            int[] r0 = new int[r0]
            r22 = r0
            r0 = r22
            r1 = r21
            r1.mTempMargins = r0
            r21 = r2
            android.support.v7.widget.Toolbar$1 r22 = new android.support.v7.widget.Toolbar$1
            r28 = r22
            r22 = r28
            r23 = r28
            r24 = r2
            r23.<init>()
            r0 = r22
            r1 = r21
            r1.mMenuViewItemClickListener = r0
            r21 = r2
            android.support.v7.widget.Toolbar$2 r22 = new android.support.v7.widget.Toolbar$2
            r28 = r22
            r22 = r28
            r23 = r28
            r24 = r2
            r23.<init>()
            r0 = r22
            r1 = r21
            r1.mShowOverflowMenuRunnable = r0
            r21 = r2
            android.content.Context r21 = r21.getContext()
            r22 = r4
            int[] r23 = android.support.v7.appcompat.R.styleable.Toolbar
            r24 = r5
            r25 = 0
            android.support.v7.widget.TintTypedArray r21 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r21, r22, r23, r24, r25)
            r6 = r21
            r21 = r2
            r22 = r6
            int r23 = android.support.v7.appcompat.R.styleable.Toolbar_titleTextAppearance
            r24 = 0
            int r22 = r22.getResourceId(r23, r24)
            r0 = r22
            r1 = r21
            r1.mTitleTextAppearance = r0
            r21 = r2
            r22 = r6
            int r23 = android.support.v7.appcompat.R.styleable.Toolbar_subtitleTextAppearance
            r24 = 0
            int r22 = r22.getResourceId(r23, r24)
            r0 = r22
            r1 = r21
            r1.mSubtitleTextAppearance = r0
            r21 = r2
            r22 = r6
            int r23 = android.support.v7.appcompat.R.styleable.Toolbar_android_gravity
            r24 = r2
            r0 = r24
            int r0 = r0.mGravity
            r24 = r0
            int r22 = r22.getInteger(r23, r24)
            r0 = r22
            r1 = r21
            r1.mGravity = r0
            r21 = r2
            r22 = 48
            r0 = r22
            r1 = r21
            r1.mButtonGravity = r0
            r21 = r2
            r22 = r2
            r23 = r2
            r24 = r2
            r25 = r6
            int r26 = android.support.v7.appcompat.R.styleable.Toolbar_titleMargins
            r27 = 0
            int r25 = r25.getDimensionPixelOffset(r26, r27)
            r28 = r24
            r29 = r25
            r24 = r29
            r25 = r28
            r26 = r29
            r0 = r26
            r1 = r25
            r1.mTitleMarginBottom = r0
            r28 = r23
            r29 = r24
            r23 = r29
            r24 = r28
            r25 = r29
            r0 = r25
            r1 = r24
            r1.mTitleMarginTop = r0
            r28 = r22
            r29 = r23
            r22 = r29
            r23 = r28
            r24 = r29
            r0 = r24
            r1 = r23
            r1.mTitleMarginEnd = r0
            r0 = r22
            r1 = r21
            r1.mTitleMarginStart = r0
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_titleMarginStart
            r23 = -1
            int r21 = r21.getDimensionPixelOffset(r22, r23)
            r7 = r21
            r21 = r7
            if (r21 < 0) goto L_0x014d
            r21 = r2
            r22 = r7
            r0 = r22
            r1 = r21
            r1.mTitleMarginStart = r0
        L_0x014d:
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_titleMarginEnd
            r23 = -1
            int r21 = r21.getDimensionPixelOffset(r22, r23)
            r8 = r21
            r21 = r8
            if (r21 < 0) goto L_0x0167
            r21 = r2
            r22 = r8
            r0 = r22
            r1 = r21
            r1.mTitleMarginEnd = r0
        L_0x0167:
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_titleMarginTop
            r23 = -1
            int r21 = r21.getDimensionPixelOffset(r22, r23)
            r9 = r21
            r21 = r9
            if (r21 < 0) goto L_0x0181
            r21 = r2
            r22 = r9
            r0 = r22
            r1 = r21
            r1.mTitleMarginTop = r0
        L_0x0181:
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_titleMarginBottom
            r23 = -1
            int r21 = r21.getDimensionPixelOffset(r22, r23)
            r10 = r21
            r21 = r10
            if (r21 < 0) goto L_0x019b
            r21 = r2
            r22 = r10
            r0 = r22
            r1 = r21
            r1.mTitleMarginBottom = r0
        L_0x019b:
            r21 = r2
            r22 = r6
            int r23 = android.support.v7.appcompat.R.styleable.Toolbar_maxButtonHeight
            r24 = -1
            int r22 = r22.getDimensionPixelSize(r23, r24)
            r0 = r22
            r1 = r21
            r1.mMaxButtonHeight = r0
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_contentInsetStart
            r23 = -2147483648(0xffffffff80000000, float:-0.0)
            int r21 = r21.getDimensionPixelOffset(r22, r23)
            r11 = r21
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_contentInsetEnd
            r23 = -2147483648(0xffffffff80000000, float:-0.0)
            int r21 = r21.getDimensionPixelOffset(r22, r23)
            r12 = r21
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_contentInsetLeft
            r23 = 0
            int r21 = r21.getDimensionPixelSize(r22, r23)
            r13 = r21
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_contentInsetRight
            r23 = 0
            int r21 = r21.getDimensionPixelSize(r22, r23)
            r14 = r21
            r21 = r2
            r0 = r21
            android.support.v7.widget.RtlSpacingHelper r0 = r0.mContentInsets
            r21 = r0
            r22 = r13
            r23 = r14
            r21.setAbsolute(r22, r23)
            r21 = r11
            r22 = -2147483648(0xffffffff80000000, float:-0.0)
            r0 = r21
            r1 = r22
            if (r0 != r1) goto L_0x0200
            r21 = r12
            r22 = -2147483648(0xffffffff80000000, float:-0.0)
            r0 = r21
            r1 = r22
            if (r0 == r1) goto L_0x020f
        L_0x0200:
            r21 = r2
            r0 = r21
            android.support.v7.widget.RtlSpacingHelper r0 = r0.mContentInsets
            r21 = r0
            r22 = r11
            r23 = r12
            r21.setRelative(r22, r23)
        L_0x020f:
            r21 = r2
            r22 = r6
            int r23 = android.support.v7.appcompat.R.styleable.Toolbar_collapseIcon
            android.graphics.drawable.Drawable r22 = r22.getDrawable(r23)
            r0 = r22
            r1 = r21
            r1.mCollapseIcon = r0
            r21 = r2
            r22 = r6
            int r23 = android.support.v7.appcompat.R.styleable.Toolbar_collapseContentDescription
            java.lang.CharSequence r22 = r22.getText(r23)
            r0 = r22
            r1 = r21
            r1.mCollapseDescription = r0
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_title
            java.lang.CharSequence r21 = r21.getText(r22)
            r15 = r21
            r21 = r15
            boolean r21 = android.text.TextUtils.isEmpty(r21)
            if (r21 != 0) goto L_0x0248
            r21 = r2
            r22 = r15
            r21.setTitle(r22)
        L_0x0248:
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_subtitle
            java.lang.CharSequence r21 = r21.getText(r22)
            r16 = r21
            r21 = r16
            boolean r21 = android.text.TextUtils.isEmpty(r21)
            if (r21 != 0) goto L_0x0261
            r21 = r2
            r22 = r16
            r21.setSubtitle(r22)
        L_0x0261:
            r21 = r2
            r22 = r2
            android.content.Context r22 = r22.getContext()
            r0 = r22
            r1 = r21
            r1.mPopupContext = r0
            r21 = r2
            r22 = r6
            int r23 = android.support.v7.appcompat.R.styleable.Toolbar_popupTheme
            r24 = 0
            int r22 = r22.getResourceId(r23, r24)
            r21.setPopupTheme(r22)
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_navigationIcon
            android.graphics.drawable.Drawable r21 = r21.getDrawable(r22)
            r17 = r21
            r21 = r17
            if (r21 == 0) goto L_0x0293
            r21 = r2
            r22 = r17
            r21.setNavigationIcon(r22)
        L_0x0293:
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_navigationContentDescription
            java.lang.CharSequence r21 = r21.getText(r22)
            r18 = r21
            r21 = r18
            boolean r21 = android.text.TextUtils.isEmpty(r21)
            if (r21 != 0) goto L_0x02ac
            r21 = r2
            r22 = r18
            r21.setNavigationContentDescription(r22)
        L_0x02ac:
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_logo
            android.graphics.drawable.Drawable r21 = r21.getDrawable(r22)
            r19 = r21
            r21 = r19
            if (r21 == 0) goto L_0x02c1
            r21 = r2
            r22 = r19
            r21.setLogo(r22)
        L_0x02c1:
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_logoDescription
            java.lang.CharSequence r21 = r21.getText(r22)
            r20 = r21
            r21 = r20
            boolean r21 = android.text.TextUtils.isEmpty(r21)
            if (r21 != 0) goto L_0x02da
            r21 = r2
            r22 = r20
            r21.setLogoDescription(r22)
        L_0x02da:
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_titleTextColor
            boolean r21 = r21.hasValue(r22)
            if (r21 == 0) goto L_0x02f3
            r21 = r2
            r22 = r6
            int r23 = android.support.v7.appcompat.R.styleable.Toolbar_titleTextColor
            r24 = -1
            int r22 = r22.getColor(r23, r24)
            r21.setTitleTextColor(r22)
        L_0x02f3:
            r21 = r6
            int r22 = android.support.v7.appcompat.R.styleable.Toolbar_subtitleTextColor
            boolean r21 = r21.hasValue(r22)
            if (r21 == 0) goto L_0x030c
            r21 = r2
            r22 = r6
            int r23 = android.support.v7.appcompat.R.styleable.Toolbar_subtitleTextColor
            r24 = -1
            int r22 = r22.getColor(r23, r24)
            r21.setSubtitleTextColor(r22)
        L_0x030c:
            r21 = r6
            r21.recycle()
            r21 = r2
            r22 = r6
            android.support.v7.widget.TintManager r22 = r22.getTintManager()
            r0 = r22
            r1 = r21
            r1.mTintManager = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.Toolbar.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setPopupTheme(@StyleRes int i) {
        Context context;
        int i2 = i;
        if (this.mPopupTheme != i2) {
            this.mPopupTheme = i2;
            if (i2 == 0) {
                this.mPopupContext = getContext();
                return;
            }
            new ContextThemeWrapper(getContext(), i2);
            this.mPopupContext = context;
        }
    }

    public int getPopupTheme() {
        return this.mPopupTheme;
    }

    public void onRtlPropertiesChanged(int i) {
        int i2 = i;
        if (Build.VERSION.SDK_INT >= 17) {
            super.onRtlPropertiesChanged(i2);
        }
        this.mContentInsets.setDirection(i2 == 1);
    }

    public void setLogo(@DrawableRes int i) {
        setLogo(this.mTintManager.getDrawable(i));
    }

    public boolean canShowOverflowMenu() {
        return getVisibility() == 0 && this.mMenuView != null && this.mMenuView.isOverflowReserved();
    }

    public boolean isOverflowMenuShowing() {
        return this.mMenuView != null && this.mMenuView.isOverflowMenuShowing();
    }

    public boolean isOverflowMenuShowPending() {
        return this.mMenuView != null && this.mMenuView.isOverflowMenuShowPending();
    }

    public boolean showOverflowMenu() {
        return this.mMenuView != null && this.mMenuView.showOverflowMenu();
    }

    public boolean hideOverflowMenu() {
        return this.mMenuView != null && this.mMenuView.hideOverflowMenu();
    }

    public void setMenu(MenuBuilder menuBuilder, ActionMenuPresenter actionMenuPresenter) {
        ExpandedActionViewMenuPresenter expandedActionViewMenuPresenter;
        MenuBuilder menuBuilder2 = menuBuilder;
        ActionMenuPresenter actionMenuPresenter2 = actionMenuPresenter;
        if (menuBuilder2 != null || this.mMenuView != null) {
            ensureMenuView();
            MenuBuilder peekMenu = this.mMenuView.peekMenu();
            if (peekMenu != menuBuilder2) {
                if (peekMenu != null) {
                    peekMenu.removeMenuPresenter(this.mOuterActionMenuPresenter);
                    peekMenu.removeMenuPresenter(this.mExpandedMenuPresenter);
                }
                if (this.mExpandedMenuPresenter == null) {
                    new ExpandedActionViewMenuPresenter();
                    this.mExpandedMenuPresenter = expandedActionViewMenuPresenter;
                }
                actionMenuPresenter2.setExpandedActionViewsExclusive(true);
                if (menuBuilder2 != null) {
                    menuBuilder2.addMenuPresenter(actionMenuPresenter2, this.mPopupContext);
                    menuBuilder2.addMenuPresenter(this.mExpandedMenuPresenter, this.mPopupContext);
                } else {
                    actionMenuPresenter2.initForMenu(this.mPopupContext, null);
                    this.mExpandedMenuPresenter.initForMenu(this.mPopupContext, null);
                    actionMenuPresenter2.updateMenuView(true);
                    this.mExpandedMenuPresenter.updateMenuView(true);
                }
                this.mMenuView.setPopupTheme(this.mPopupTheme);
                this.mMenuView.setPresenter(actionMenuPresenter2);
                this.mOuterActionMenuPresenter = actionMenuPresenter2;
            }
        }
    }

    public void dismissPopupMenus() {
        if (this.mMenuView != null) {
            this.mMenuView.dismissPopupMenus();
        }
    }

    public boolean isTitleTruncated() {
        if (this.mTitleTextView == null) {
            return false;
        }
        Layout layout = this.mTitleTextView.getLayout();
        if (layout == null) {
            return false;
        }
        int lineCount = layout.getLineCount();
        for (int i = 0; i < lineCount; i++) {
            if (layout.getEllipsisCount(i) > 0) {
                return true;
            }
        }
        return false;
    }

    public void setLogo(Drawable drawable) {
        Drawable drawable2 = drawable;
        if (drawable2 != null) {
            ensureLogoView();
            if (!isChildOrHidden(this.mLogoView)) {
                addSystemView(this.mLogoView, true);
            }
        } else if (this.mLogoView != null && isChildOrHidden(this.mLogoView)) {
            removeView(this.mLogoView);
            boolean remove = this.mHiddenViews.remove(this.mLogoView);
        }
        if (this.mLogoView != null) {
            this.mLogoView.setImageDrawable(drawable2);
        }
    }

    public Drawable getLogo() {
        return this.mLogoView != null ? this.mLogoView.getDrawable() : null;
    }

    public void setLogoDescription(@StringRes int i) {
        setLogoDescription(getContext().getText(i));
    }

    public void setLogoDescription(CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        if (!TextUtils.isEmpty(charSequence2)) {
            ensureLogoView();
        }
        if (this.mLogoView != null) {
            this.mLogoView.setContentDescription(charSequence2);
        }
    }

    public CharSequence getLogoDescription() {
        return this.mLogoView != null ? this.mLogoView.getContentDescription() : null;
    }

    private void ensureLogoView() {
        ImageView imageView;
        if (this.mLogoView == null) {
            new ImageView(getContext());
            this.mLogoView = imageView;
        }
    }

    public boolean hasExpandedActionView() {
        return (this.mExpandedMenuPresenter == null || this.mExpandedMenuPresenter.mCurrentExpandedItem == null) ? false : true;
    }

    public void collapseActionView() {
        MenuItemImpl menuItemImpl = this.mExpandedMenuPresenter == null ? null : this.mExpandedMenuPresenter.mCurrentExpandedItem;
        if (menuItemImpl != null) {
            boolean collapseActionView = menuItemImpl.collapseActionView();
        }
    }

    public CharSequence getTitle() {
        return this.mTitleText;
    }

    public void setTitle(@StringRes int i) {
        setTitle(getContext().getText(i));
    }

    public void setTitle(CharSequence charSequence) {
        TextView textView;
        CharSequence charSequence2 = charSequence;
        if (!TextUtils.isEmpty(charSequence2)) {
            if (this.mTitleTextView == null) {
                Context context = getContext();
                new TextView(context);
                this.mTitleTextView = textView;
                this.mTitleTextView.setSingleLine();
                this.mTitleTextView.setEllipsize(TextUtils.TruncateAt.END);
                if (this.mTitleTextAppearance != 0) {
                    this.mTitleTextView.setTextAppearance(context, this.mTitleTextAppearance);
                }
                if (this.mTitleTextColor != 0) {
                    this.mTitleTextView.setTextColor(this.mTitleTextColor);
                }
            }
            if (!isChildOrHidden(this.mTitleTextView)) {
                addSystemView(this.mTitleTextView, true);
            }
        } else if (this.mTitleTextView != null && isChildOrHidden(this.mTitleTextView)) {
            removeView(this.mTitleTextView);
            boolean remove = this.mHiddenViews.remove(this.mTitleTextView);
        }
        if (this.mTitleTextView != null) {
            this.mTitleTextView.setText(charSequence2);
        }
        this.mTitleText = charSequence2;
    }

    public CharSequence getSubtitle() {
        return this.mSubtitleText;
    }

    public void setSubtitle(@StringRes int i) {
        setSubtitle(getContext().getText(i));
    }

    public void setSubtitle(CharSequence charSequence) {
        TextView textView;
        CharSequence charSequence2 = charSequence;
        if (!TextUtils.isEmpty(charSequence2)) {
            if (this.mSubtitleTextView == null) {
                Context context = getContext();
                new TextView(context);
                this.mSubtitleTextView = textView;
                this.mSubtitleTextView.setSingleLine();
                this.mSubtitleTextView.setEllipsize(TextUtils.TruncateAt.END);
                if (this.mSubtitleTextAppearance != 0) {
                    this.mSubtitleTextView.setTextAppearance(context, this.mSubtitleTextAppearance);
                }
                if (this.mSubtitleTextColor != 0) {
                    this.mSubtitleTextView.setTextColor(this.mSubtitleTextColor);
                }
            }
            if (!isChildOrHidden(this.mSubtitleTextView)) {
                addSystemView(this.mSubtitleTextView, true);
            }
        } else if (this.mSubtitleTextView != null && isChildOrHidden(this.mSubtitleTextView)) {
            removeView(this.mSubtitleTextView);
            boolean remove = this.mHiddenViews.remove(this.mSubtitleTextView);
        }
        if (this.mSubtitleTextView != null) {
            this.mSubtitleTextView.setText(charSequence2);
        }
        this.mSubtitleText = charSequence2;
    }

    public void setTitleTextAppearance(Context context, @StyleRes int i) {
        Context context2 = context;
        int i2 = i;
        this.mTitleTextAppearance = i2;
        if (this.mTitleTextView != null) {
            this.mTitleTextView.setTextAppearance(context2, i2);
        }
    }

    public void setSubtitleTextAppearance(Context context, @StyleRes int i) {
        Context context2 = context;
        int i2 = i;
        this.mSubtitleTextAppearance = i2;
        if (this.mSubtitleTextView != null) {
            this.mSubtitleTextView.setTextAppearance(context2, i2);
        }
    }

    public void setTitleTextColor(@ColorInt int i) {
        int i2 = i;
        this.mTitleTextColor = i2;
        if (this.mTitleTextView != null) {
            this.mTitleTextView.setTextColor(i2);
        }
    }

    public void setSubtitleTextColor(@ColorInt int i) {
        int i2 = i;
        this.mSubtitleTextColor = i2;
        if (this.mSubtitleTextView != null) {
            this.mSubtitleTextView.setTextColor(i2);
        }
    }

    @Nullable
    public CharSequence getNavigationContentDescription() {
        return this.mNavButtonView != null ? this.mNavButtonView.getContentDescription() : null;
    }

    public void setNavigationContentDescription(@StringRes int i) {
        int i2 = i;
        setNavigationContentDescription(i2 != 0 ? getContext().getText(i2) : null);
    }

    public void setNavigationContentDescription(@Nullable CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        if (!TextUtils.isEmpty(charSequence2)) {
            ensureNavButtonView();
        }
        if (this.mNavButtonView != null) {
            this.mNavButtonView.setContentDescription(charSequence2);
        }
    }

    public void setNavigationIcon(@DrawableRes int i) {
        setNavigationIcon(this.mTintManager.getDrawable(i));
    }

    public void setNavigationIcon(@Nullable Drawable drawable) {
        Drawable drawable2 = drawable;
        if (drawable2 != null) {
            ensureNavButtonView();
            if (!isChildOrHidden(this.mNavButtonView)) {
                addSystemView(this.mNavButtonView, true);
            }
        } else if (this.mNavButtonView != null && isChildOrHidden(this.mNavButtonView)) {
            removeView(this.mNavButtonView);
            boolean remove = this.mHiddenViews.remove(this.mNavButtonView);
        }
        if (this.mNavButtonView != null) {
            this.mNavButtonView.setImageDrawable(drawable2);
        }
    }

    @Nullable
    public Drawable getNavigationIcon() {
        return this.mNavButtonView != null ? this.mNavButtonView.getDrawable() : null;
    }

    public void setNavigationOnClickListener(View.OnClickListener onClickListener) {
        ensureNavButtonView();
        this.mNavButtonView.setOnClickListener(onClickListener);
    }

    public Menu getMenu() {
        ensureMenu();
        return this.mMenuView.getMenu();
    }

    public void setOverflowIcon(@Nullable Drawable drawable) {
        ensureMenu();
        this.mMenuView.setOverflowIcon(drawable);
    }

    @Nullable
    public Drawable getOverflowIcon() {
        ensureMenu();
        return this.mMenuView.getOverflowIcon();
    }

    private void ensureMenu() {
        ExpandedActionViewMenuPresenter expandedActionViewMenuPresenter;
        ensureMenuView();
        if (this.mMenuView.peekMenu() == null) {
            MenuBuilder menuBuilder = (MenuBuilder) this.mMenuView.getMenu();
            if (this.mExpandedMenuPresenter == null) {
                new ExpandedActionViewMenuPresenter();
                this.mExpandedMenuPresenter = expandedActionViewMenuPresenter;
            }
            this.mMenuView.setExpandedActionViewsExclusive(true);
            menuBuilder.addMenuPresenter(this.mExpandedMenuPresenter, this.mPopupContext);
        }
    }

    private void ensureMenuView() {
        ActionMenuView actionMenuView;
        if (this.mMenuView == null) {
            new ActionMenuView(getContext());
            this.mMenuView = actionMenuView;
            this.mMenuView.setPopupTheme(this.mPopupTheme);
            this.mMenuView.setOnMenuItemClickListener(this.mMenuViewItemClickListener);
            this.mMenuView.setMenuCallbacks(this.mActionMenuPresenterCallback, this.mMenuBuilderCallback);
            LayoutParams generateDefaultLayoutParams = generateDefaultLayoutParams();
            generateDefaultLayoutParams.gravity = 8388613 | (this.mButtonGravity & 112);
            this.mMenuView.setLayoutParams(generateDefaultLayoutParams);
            addSystemView(this.mMenuView, false);
        }
    }

    private MenuInflater getMenuInflater() {
        MenuInflater menuInflater;
        new SupportMenuInflater(getContext());
        return menuInflater;
    }

    public void inflateMenu(@MenuRes int i) {
        getMenuInflater().inflate(i, getMenu());
    }

    public void setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
        this.mOnMenuItemClickListener = onMenuItemClickListener;
    }

    public void setContentInsetsRelative(int i, int i2) {
        this.mContentInsets.setRelative(i, i2);
    }

    public int getContentInsetStart() {
        return this.mContentInsets.getStart();
    }

    public int getContentInsetEnd() {
        return this.mContentInsets.getEnd();
    }

    public void setContentInsetsAbsolute(int i, int i2) {
        this.mContentInsets.setAbsolute(i, i2);
    }

    public int getContentInsetLeft() {
        return this.mContentInsets.getLeft();
    }

    public int getContentInsetRight() {
        return this.mContentInsets.getRight();
    }

    private void ensureNavButtonView() {
        ImageButton imageButton;
        if (this.mNavButtonView == null) {
            new ImageButton(getContext(), null, R.attr.toolbarNavigationButtonStyle);
            this.mNavButtonView = imageButton;
            LayoutParams generateDefaultLayoutParams = generateDefaultLayoutParams();
            generateDefaultLayoutParams.gravity = 8388611 | (this.mButtonGravity & 112);
            this.mNavButtonView.setLayoutParams(generateDefaultLayoutParams);
        }
    }

    /* access modifiers changed from: private */
    public void ensureCollapseButtonView() {
        ImageButton imageButton;
        View.OnClickListener onClickListener;
        if (this.mCollapseButtonView == null) {
            new ImageButton(getContext(), null, R.attr.toolbarNavigationButtonStyle);
            this.mCollapseButtonView = imageButton;
            this.mCollapseButtonView.setImageDrawable(this.mCollapseIcon);
            this.mCollapseButtonView.setContentDescription(this.mCollapseDescription);
            LayoutParams generateDefaultLayoutParams = generateDefaultLayoutParams();
            generateDefaultLayoutParams.gravity = 8388611 | (this.mButtonGravity & 112);
            generateDefaultLayoutParams.mViewType = 2;
            this.mCollapseButtonView.setLayoutParams(generateDefaultLayoutParams);
            new View.OnClickListener() {
                public void onClick(View view) {
                    Toolbar.this.collapseActionView();
                }
            };
            this.mCollapseButtonView.setOnClickListener(onClickListener);
        }
    }

    private void addSystemView(View view, boolean z) {
        LayoutParams layoutParams;
        View view2 = view;
        boolean z2 = z;
        ViewGroup.LayoutParams layoutParams2 = view2.getLayoutParams();
        if (layoutParams2 == null) {
            layoutParams = generateDefaultLayoutParams();
        } else if (!checkLayoutParams(layoutParams2)) {
            layoutParams = generateLayoutParams(layoutParams2);
        } else {
            layoutParams = (LayoutParams) layoutParams2;
        }
        layoutParams.mViewType = 1;
        if (!z2 || this.mExpandedActionView == null) {
            addView(view2, layoutParams);
            return;
        }
        view2.setLayoutParams(layoutParams);
        boolean add = this.mHiddenViews.add(view2);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState;
        new SavedState(super.onSaveInstanceState());
        SavedState savedState2 = savedState;
        if (!(this.mExpandedMenuPresenter == null || this.mExpandedMenuPresenter.mCurrentExpandedItem == null)) {
            savedState2.expandedMenuItemId = this.mExpandedMenuPresenter.mCurrentExpandedItem.getItemId();
        }
        savedState2.isOverflowOpen = isOverflowMenuShowing();
        return savedState2;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        MenuBuilder peekMenu = this.mMenuView != null ? this.mMenuView.peekMenu() : null;
        if (!(savedState.expandedMenuItemId == 0 || this.mExpandedMenuPresenter == null || peekMenu == null || (findItem = peekMenu.findItem(savedState.expandedMenuItemId)) == null)) {
            boolean expandActionView = MenuItemCompat.expandActionView(findItem);
        }
        if (savedState.isOverflowOpen) {
            postShowOverflowMenu();
        }
    }

    private void postShowOverflowMenu() {
        boolean removeCallbacks = removeCallbacks(this.mShowOverflowMenuRunnable);
        boolean post = post(this.mShowOverflowMenuRunnable);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        boolean removeCallbacks = removeCallbacks(this.mShowOverflowMenuRunnable);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
        if (actionMasked == 0) {
            this.mEatingTouch = false;
        }
        if (!this.mEatingTouch) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent2);
            if (actionMasked == 0 && !onTouchEvent) {
                this.mEatingTouch = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.mEatingTouch = false;
        }
        return true;
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
        if (actionMasked == 9) {
            this.mEatingHover = false;
        }
        if (!this.mEatingHover) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent2);
            if (actionMasked == 9 && !onHoverEvent) {
                this.mEatingHover = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.mEatingHover = false;
        }
        return true;
    }

    private void measureChildConstrained(View view, int i, int i2, int i3, int i4, int i5) {
        View view2 = view;
        int i6 = i5;
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view2.getLayoutParams();
        int childMeasureSpec = getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i2, marginLayoutParams.width);
        int childMeasureSpec2 = getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i4, marginLayoutParams.height);
        int mode = View.MeasureSpec.getMode(childMeasureSpec2);
        if (mode != 1073741824 && i6 >= 0) {
            childMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(mode != 0 ? Math.min(View.MeasureSpec.getSize(childMeasureSpec2), i6) : i6, 1073741824);
        }
        view2.measure(childMeasureSpec, childMeasureSpec2);
    }

    private int measureChildCollapseMargins(View view, int i, int i2, int i3, int i4, int[] iArr) {
        View view2 = view;
        int[] iArr2 = iArr;
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view2.getLayoutParams();
        int i5 = marginLayoutParams.leftMargin - iArr2[0];
        int i6 = marginLayoutParams.rightMargin - iArr2[1];
        int max = Math.max(0, i5) + Math.max(0, i6);
        iArr2[0] = Math.max(0, -i5);
        iArr2[1] = Math.max(0, -i6);
        view2.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + max + i2, marginLayoutParams.width), getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i4, marginLayoutParams.height));
        return view2.getMeasuredWidth() + max;
    }

    private boolean shouldCollapse() {
        if (!this.mCollapsible) {
            return false;
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (shouldLayout(childAt) && childAt.getMeasuredWidth() > 0 && childAt.getMeasuredHeight() > 0) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        boolean z;
        boolean z2;
        int i3 = i;
        int i4 = i2;
        int i5 = 0;
        int i6 = 0;
        int[] iArr = this.mTempMargins;
        if (ViewUtils.isLayoutRtl(this)) {
            z = true;
            z2 = false;
        } else {
            z = false;
            z2 = true;
        }
        int i7 = 0;
        if (shouldLayout(this.mNavButtonView)) {
            measureChildConstrained(this.mNavButtonView, i3, 0, i4, 0, this.mMaxButtonHeight);
            i7 = this.mNavButtonView.getMeasuredWidth() + getHorizontalMargins(this.mNavButtonView);
            i5 = Math.max(0, this.mNavButtonView.getMeasuredHeight() + getVerticalMargins(this.mNavButtonView));
            i6 = ViewUtils.combineMeasuredStates(0, ViewCompat.getMeasuredState(this.mNavButtonView));
        }
        if (shouldLayout(this.mCollapseButtonView)) {
            measureChildConstrained(this.mCollapseButtonView, i3, 0, i4, 0, this.mMaxButtonHeight);
            i7 = this.mCollapseButtonView.getMeasuredWidth() + getHorizontalMargins(this.mCollapseButtonView);
            i5 = Math.max(i5, this.mCollapseButtonView.getMeasuredHeight() + getVerticalMargins(this.mCollapseButtonView));
            i6 = ViewUtils.combineMeasuredStates(i6, ViewCompat.getMeasuredState(this.mCollapseButtonView));
        }
        int contentInsetStart = getContentInsetStart();
        int max = 0 + Math.max(contentInsetStart, i7);
        iArr[z] = Math.max(0, contentInsetStart - i7);
        int i8 = 0;
        if (shouldLayout(this.mMenuView)) {
            measureChildConstrained(this.mMenuView, i3, max, i4, 0, this.mMaxButtonHeight);
            i8 = this.mMenuView.getMeasuredWidth() + getHorizontalMargins(this.mMenuView);
            i5 = Math.max(i5, this.mMenuView.getMeasuredHeight() + getVerticalMargins(this.mMenuView));
            i6 = ViewUtils.combineMeasuredStates(i6, ViewCompat.getMeasuredState(this.mMenuView));
        }
        int contentInsetEnd = getContentInsetEnd();
        int max2 = max + Math.max(contentInsetEnd, i8);
        iArr[z2] = Math.max(0, contentInsetEnd - i8);
        if (shouldLayout(this.mExpandedActionView)) {
            max2 += measureChildCollapseMargins(this.mExpandedActionView, i3, max2, i4, 0, iArr);
            i5 = Math.max(i5, this.mExpandedActionView.getMeasuredHeight() + getVerticalMargins(this.mExpandedActionView));
            i6 = ViewUtils.combineMeasuredStates(i6, ViewCompat.getMeasuredState(this.mExpandedActionView));
        }
        if (shouldLayout(this.mLogoView)) {
            max2 += measureChildCollapseMargins(this.mLogoView, i3, max2, i4, 0, iArr);
            i5 = Math.max(i5, this.mLogoView.getMeasuredHeight() + getVerticalMargins(this.mLogoView));
            i6 = ViewUtils.combineMeasuredStates(i6, ViewCompat.getMeasuredState(this.mLogoView));
        }
        int childCount = getChildCount();
        for (int i9 = 0; i9 < childCount; i9++) {
            View childAt = getChildAt(i9);
            if (((LayoutParams) childAt.getLayoutParams()).mViewType == 0 && shouldLayout(childAt)) {
                max2 += measureChildCollapseMargins(childAt, i3, max2, i4, 0, iArr);
                i5 = Math.max(i5, childAt.getMeasuredHeight() + getVerticalMargins(childAt));
                i6 = ViewUtils.combineMeasuredStates(i6, ViewCompat.getMeasuredState(childAt));
            }
        }
        int i10 = 0;
        int i11 = 0;
        int i12 = this.mTitleMarginTop + this.mTitleMarginBottom;
        int i13 = this.mTitleMarginStart + this.mTitleMarginEnd;
        if (shouldLayout(this.mTitleTextView)) {
            int measureChildCollapseMargins = measureChildCollapseMargins(this.mTitleTextView, i3, max2 + i13, i4, i12, iArr);
            i10 = this.mTitleTextView.getMeasuredWidth() + getHorizontalMargins(this.mTitleTextView);
            i11 = this.mTitleTextView.getMeasuredHeight() + getVerticalMargins(this.mTitleTextView);
            i6 = ViewUtils.combineMeasuredStates(i6, ViewCompat.getMeasuredState(this.mTitleTextView));
        }
        if (shouldLayout(this.mSubtitleTextView)) {
            i10 = Math.max(i10, measureChildCollapseMargins(this.mSubtitleTextView, i3, max2 + i13, i4, i11 + i12, iArr));
            i11 += this.mSubtitleTextView.getMeasuredHeight() + getVerticalMargins(this.mSubtitleTextView);
            i6 = ViewUtils.combineMeasuredStates(i6, ViewCompat.getMeasuredState(this.mSubtitleTextView));
        }
        int max3 = Math.max(i5, i11);
        setMeasuredDimension(ViewCompat.resolveSizeAndState(Math.max(max2 + i10 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i3, i6 & ViewCompat.MEASURED_STATE_MASK), shouldCollapse() ? 0 : ViewCompat.resolveSizeAndState(Math.max(max3 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i4, i6 << 16));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        boolean z2 = ViewCompat.getLayoutDirection(this) == 1;
        int width = getWidth();
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i6 = paddingLeft;
        int i7 = width - paddingRight;
        int[] iArr = this.mTempMargins;
        iArr[1] = 0;
        iArr[0] = 0;
        int minimumHeight = ViewCompat.getMinimumHeight(this);
        if (shouldLayout(this.mNavButtonView)) {
            if (z2) {
                i7 = layoutChildRight(this.mNavButtonView, i7, iArr, minimumHeight);
            } else {
                i6 = layoutChildLeft(this.mNavButtonView, i6, iArr, minimumHeight);
            }
        }
        if (shouldLayout(this.mCollapseButtonView)) {
            if (z2) {
                i7 = layoutChildRight(this.mCollapseButtonView, i7, iArr, minimumHeight);
            } else {
                i6 = layoutChildLeft(this.mCollapseButtonView, i6, iArr, minimumHeight);
            }
        }
        if (shouldLayout(this.mMenuView)) {
            if (z2) {
                i6 = layoutChildLeft(this.mMenuView, i6, iArr, minimumHeight);
            } else {
                i7 = layoutChildRight(this.mMenuView, i7, iArr, minimumHeight);
            }
        }
        iArr[0] = Math.max(0, getContentInsetLeft() - i6);
        iArr[1] = Math.max(0, getContentInsetRight() - ((width - paddingRight) - i7));
        int max = Math.max(i6, getContentInsetLeft());
        int min = Math.min(i7, (width - paddingRight) - getContentInsetRight());
        if (shouldLayout(this.mExpandedActionView)) {
            if (z2) {
                min = layoutChildRight(this.mExpandedActionView, min, iArr, minimumHeight);
            } else {
                max = layoutChildLeft(this.mExpandedActionView, max, iArr, minimumHeight);
            }
        }
        if (shouldLayout(this.mLogoView)) {
            if (z2) {
                min = layoutChildRight(this.mLogoView, min, iArr, minimumHeight);
            } else {
                max = layoutChildLeft(this.mLogoView, max, iArr, minimumHeight);
            }
        }
        boolean shouldLayout = shouldLayout(this.mTitleTextView);
        boolean shouldLayout2 = shouldLayout(this.mSubtitleTextView);
        int i8 = 0;
        if (shouldLayout) {
            LayoutParams layoutParams = (LayoutParams) this.mTitleTextView.getLayoutParams();
            i8 = 0 + layoutParams.topMargin + this.mTitleTextView.getMeasuredHeight() + layoutParams.bottomMargin;
        }
        if (shouldLayout2) {
            LayoutParams layoutParams2 = (LayoutParams) this.mSubtitleTextView.getLayoutParams();
            i8 += layoutParams2.topMargin + this.mSubtitleTextView.getMeasuredHeight() + layoutParams2.bottomMargin;
        }
        if (shouldLayout || shouldLayout2) {
            TextView textView = shouldLayout ? this.mTitleTextView : this.mSubtitleTextView;
            TextView textView2 = shouldLayout2 ? this.mSubtitleTextView : this.mTitleTextView;
            LayoutParams layoutParams3 = (LayoutParams) textView.getLayoutParams();
            LayoutParams layoutParams4 = (LayoutParams) textView2.getLayoutParams();
            boolean z3 = (shouldLayout && this.mTitleTextView.getMeasuredWidth() > 0) || (shouldLayout2 && this.mSubtitleTextView.getMeasuredWidth() > 0);
            switch (this.mGravity & 112) {
                case 48:
                    i5 = getPaddingTop() + layoutParams3.topMargin + this.mTitleMarginTop;
                    break;
                case 80:
                    i5 = (((height - paddingBottom) - layoutParams4.bottomMargin) - this.mTitleMarginBottom) - i8;
                    break;
                default:
                    int i9 = (((height - paddingTop) - paddingBottom) - i8) / 2;
                    if (i9 < layoutParams3.topMargin + this.mTitleMarginTop) {
                        i9 = layoutParams3.topMargin + this.mTitleMarginTop;
                    } else {
                        int i10 = (((height - paddingBottom) - i8) - i9) - paddingTop;
                        if (i10 < layoutParams3.bottomMargin + this.mTitleMarginBottom) {
                            i9 = Math.max(0, i9 - ((layoutParams4.bottomMargin + this.mTitleMarginBottom) - i10));
                        }
                    }
                    i5 = paddingTop + i9;
                    break;
            }
            if (z2) {
                int i11 = (z3 ? this.mTitleMarginStart : 0) - iArr[1];
                min -= Math.max(0, i11);
                iArr[1] = Math.max(0, -i11);
                int i12 = min;
                int i13 = min;
                if (shouldLayout) {
                    LayoutParams layoutParams5 = (LayoutParams) this.mTitleTextView.getLayoutParams();
                    int measuredWidth = i12 - this.mTitleTextView.getMeasuredWidth();
                    int measuredHeight = i5 + this.mTitleTextView.getMeasuredHeight();
                    this.mTitleTextView.layout(measuredWidth, i5, i12, measuredHeight);
                    i12 = measuredWidth - this.mTitleMarginEnd;
                    i5 = measuredHeight + layoutParams5.bottomMargin;
                }
                if (shouldLayout2) {
                    LayoutParams layoutParams6 = (LayoutParams) this.mSubtitleTextView.getLayoutParams();
                    int i14 = i5 + layoutParams6.topMargin;
                    int measuredWidth2 = i13 - this.mSubtitleTextView.getMeasuredWidth();
                    int measuredHeight2 = i14 + this.mSubtitleTextView.getMeasuredHeight();
                    this.mSubtitleTextView.layout(measuredWidth2, i14, i13, measuredHeight2);
                    i13 -= this.mTitleMarginEnd;
                    int i15 = measuredHeight2 + layoutParams6.bottomMargin;
                }
                if (z3) {
                    min = Math.min(i12, i13);
                }
            } else {
                int i16 = (z3 ? this.mTitleMarginStart : 0) - iArr[0];
                max += Math.max(0, i16);
                iArr[0] = Math.max(0, -i16);
                int i17 = max;
                int i18 = max;
                if (shouldLayout) {
                    LayoutParams layoutParams7 = (LayoutParams) this.mTitleTextView.getLayoutParams();
                    int measuredWidth3 = i17 + this.mTitleTextView.getMeasuredWidth();
                    int measuredHeight3 = i5 + this.mTitleTextView.getMeasuredHeight();
                    this.mTitleTextView.layout(i17, i5, measuredWidth3, measuredHeight3);
                    i17 = measuredWidth3 + this.mTitleMarginEnd;
                    i5 = measuredHeight3 + layoutParams7.bottomMargin;
                }
                if (shouldLayout2) {
                    LayoutParams layoutParams8 = (LayoutParams) this.mSubtitleTextView.getLayoutParams();
                    int i19 = i5 + layoutParams8.topMargin;
                    int measuredWidth4 = i18 + this.mSubtitleTextView.getMeasuredWidth();
                    int measuredHeight4 = i19 + this.mSubtitleTextView.getMeasuredHeight();
                    this.mSubtitleTextView.layout(i18, i19, measuredWidth4, measuredHeight4);
                    i18 = measuredWidth4 + this.mTitleMarginEnd;
                    int i20 = measuredHeight4 + layoutParams8.bottomMargin;
                }
                if (z3) {
                    max = Math.max(i17, i18);
                }
            }
        }
        addCustomViewsWithGravity(this.mTempViews, 3);
        int size = this.mTempViews.size();
        for (int i21 = 0; i21 < size; i21++) {
            max = layoutChildLeft(this.mTempViews.get(i21), max, iArr, minimumHeight);
        }
        addCustomViewsWithGravity(this.mTempViews, 5);
        int size2 = this.mTempViews.size();
        for (int i22 = 0; i22 < size2; i22++) {
            min = layoutChildRight(this.mTempViews.get(i22), min, iArr, minimumHeight);
        }
        addCustomViewsWithGravity(this.mTempViews, 1);
        int viewListMeasuredWidth = getViewListMeasuredWidth(this.mTempViews, iArr);
        int i23 = (paddingLeft + (((width - paddingLeft) - paddingRight) / 2)) - (viewListMeasuredWidth / 2);
        int i24 = i23 + viewListMeasuredWidth;
        if (i23 < max) {
            i23 = max;
        } else if (i24 > min) {
            i23 -= i24 - min;
        }
        int size3 = this.mTempViews.size();
        for (int i25 = 0; i25 < size3; i25++) {
            i23 = layoutChildLeft(this.mTempViews.get(i25), i23, iArr, minimumHeight);
        }
        this.mTempViews.clear();
    }

    private int getViewListMeasuredWidth(List<View> list, int[] iArr) {
        List<View> list2 = list;
        int[] iArr2 = iArr;
        int i = iArr2[0];
        int i2 = iArr2[1];
        int i3 = 0;
        int size = list2.size();
        for (int i4 = 0; i4 < size; i4++) {
            View view = list2.get(i4);
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            int i5 = layoutParams.leftMargin - i;
            int i6 = layoutParams.rightMargin - i2;
            int max = Math.max(0, i5);
            int max2 = Math.max(0, i6);
            i = Math.max(0, -i5);
            i2 = Math.max(0, -i6);
            i3 += max + view.getMeasuredWidth() + max2;
        }
        return i3;
    }

    private int layoutChildLeft(View view, int i, int[] iArr, int i2) {
        View view2 = view;
        int[] iArr2 = iArr;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        int i3 = layoutParams.leftMargin - iArr2[0];
        int max = i + Math.max(0, i3);
        iArr2[0] = Math.max(0, -i3);
        int childTop = getChildTop(view2, i2);
        int measuredWidth = view2.getMeasuredWidth();
        view2.layout(max, childTop, max + measuredWidth, childTop + view2.getMeasuredHeight());
        return max + measuredWidth + layoutParams.rightMargin;
    }

    private int layoutChildRight(View view, int i, int[] iArr, int i2) {
        View view2 = view;
        int[] iArr2 = iArr;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        int i3 = layoutParams.rightMargin - iArr2[1];
        int max = i - Math.max(0, i3);
        iArr2[1] = Math.max(0, -i3);
        int childTop = getChildTop(view2, i2);
        int measuredWidth = view2.getMeasuredWidth();
        view2.layout(max - measuredWidth, childTop, max, childTop + view2.getMeasuredHeight());
        return max - (measuredWidth + layoutParams.leftMargin);
    }

    private int getChildTop(View view, int i) {
        View view2 = view;
        int i2 = i;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        int measuredHeight = view2.getMeasuredHeight();
        int i3 = i2 > 0 ? (measuredHeight - i2) / 2 : 0;
        switch (getChildVerticalGravity(layoutParams.gravity)) {
            case 48:
                return getPaddingTop() - i3;
            case 80:
                return (((getHeight() - getPaddingBottom()) - measuredHeight) - layoutParams.bottomMargin) - i3;
            default:
                int paddingTop = getPaddingTop();
                int paddingBottom = getPaddingBottom();
                int height = getHeight();
                int i4 = (((height - paddingTop) - paddingBottom) - measuredHeight) / 2;
                if (i4 < layoutParams.topMargin) {
                    i4 = layoutParams.topMargin;
                } else {
                    int i5 = (((height - paddingBottom) - measuredHeight) - i4) - paddingTop;
                    if (i5 < layoutParams.bottomMargin) {
                        i4 = Math.max(0, i4 - (layoutParams.bottomMargin - i5));
                    }
                }
                return paddingTop + i4;
        }
    }

    private int getChildVerticalGravity(int i) {
        int i2 = i & 112;
        switch (i2) {
            case 16:
            case 48:
            case 80:
                return i2;
            default:
                return this.mGravity & 112;
        }
    }

    private void addCustomViewsWithGravity(List<View> list, int i) {
        List<View> list2 = list;
        int i2 = i;
        boolean z = ViewCompat.getLayoutDirection(this) == 1;
        int childCount = getChildCount();
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i2, ViewCompat.getLayoutDirection(this));
        list2.clear();
        if (z) {
            for (int i3 = childCount - 1; i3 >= 0; i3--) {
                View childAt = getChildAt(i3);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.mViewType == 0 && shouldLayout(childAt) && getChildHorizontalGravity(layoutParams.gravity) == absoluteGravity) {
                    boolean add = list2.add(childAt);
                }
            }
            return;
        }
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt2 = getChildAt(i4);
            LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
            if (layoutParams2.mViewType == 0 && shouldLayout(childAt2) && getChildHorizontalGravity(layoutParams2.gravity) == absoluteGravity) {
                boolean add2 = list2.add(childAt2);
            }
        }
    }

    private int getChildHorizontalGravity(int i) {
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i, layoutDirection) & 7;
        switch (absoluteGravity) {
            case 1:
            case 3:
            case 5:
                return absoluteGravity;
            case 2:
            case 4:
            default:
                return layoutDirection == 1 ? 5 : 3;
        }
    }

    private boolean shouldLayout(View view) {
        View view2 = view;
        return (view2 == null || view2.getParent() != this || view2.getVisibility() == 8) ? false : true;
    }

    private int getHorizontalMargins(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return MarginLayoutParamsCompat.getMarginStart(marginLayoutParams) + MarginLayoutParamsCompat.getMarginEnd(marginLayoutParams);
    }

    private int getVerticalMargins(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        LayoutParams layoutParams;
        new LayoutParams(getContext(), attributeSet);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        LayoutParams layoutParams2;
        LayoutParams layoutParams3;
        LayoutParams layoutParams4;
        LayoutParams layoutParams5;
        ViewGroup.LayoutParams layoutParams6 = layoutParams;
        if (layoutParams6 instanceof LayoutParams) {
            new LayoutParams((LayoutParams) layoutParams6);
            return layoutParams5;
        } else if (layoutParams6 instanceof ActionBar.LayoutParams) {
            new LayoutParams((ActionBar.LayoutParams) layoutParams6);
            return layoutParams4;
        } else if (layoutParams6 instanceof ViewGroup.MarginLayoutParams) {
            new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams6);
            return layoutParams3;
        } else {
            new LayoutParams(layoutParams6);
            return layoutParams2;
        }
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        LayoutParams layoutParams;
        new LayoutParams(-2, -2);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        return super.checkLayoutParams(layoutParams2) && (layoutParams2 instanceof LayoutParams);
    }

    private static boolean isCustomView(View view) {
        return ((LayoutParams) view.getLayoutParams()).mViewType == 0;
    }

    public DecorToolbar getWrapper() {
        ToolbarWidgetWrapper toolbarWidgetWrapper;
        if (this.mWrapper == null) {
            new ToolbarWidgetWrapper(this, true);
            this.mWrapper = toolbarWidgetWrapper;
        }
        return this.mWrapper;
    }

    /* access modifiers changed from: package-private */
    public void removeChildrenForExpandedActionView() {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            if (!(((LayoutParams) childAt.getLayoutParams()).mViewType == 2 || childAt == this.mMenuView)) {
                removeViewAt(childCount);
                boolean add = this.mHiddenViews.add(childAt);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void addChildrenForExpandedActionView() {
        for (int size = this.mHiddenViews.size() - 1; size >= 0; size--) {
            addView(this.mHiddenViews.get(size));
        }
        this.mHiddenViews.clear();
    }

    private boolean isChildOrHidden(View view) {
        View view2 = view;
        return view2.getParent() == this || this.mHiddenViews.contains(view2);
    }

    public void setCollapsible(boolean z) {
        this.mCollapsible = z;
        requestLayout();
    }

    public void setMenuCallbacks(MenuPresenter.Callback callback, MenuBuilder.Callback callback2) {
        this.mActionMenuPresenterCallback = callback;
        this.mMenuBuilderCallback = callback2;
    }

    public static class LayoutParams extends ActionBar.LayoutParams {
        static final int CUSTOM = 0;
        static final int EXPANDED = 2;
        static final int SYSTEM = 1;
        int mViewType;

        public LayoutParams(@NonNull Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.mViewType = 0;
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.mViewType = 0;
            this.gravity = 8388627;
        }

        public LayoutParams(int i, int i2, int i3) {
            super(i, i2);
            this.mViewType = 0;
            this.gravity = i3;
        }

        public LayoutParams(int i) {
            this(-2, -1, i);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public LayoutParams(android.support.v7.widget.Toolbar.LayoutParams r5) {
            /*
                r4 = this;
                r0 = r4
                r1 = r5
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                r2 = r0
                r3 = 0
                r2.mViewType = r3
                r2 = r0
                r3 = r1
                int r3 = r3.mViewType
                r2.mViewType = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.Toolbar.LayoutParams.<init>(android.support.v7.widget.Toolbar$LayoutParams):void");
        }

        public LayoutParams(ActionBar.LayoutParams layoutParams) {
            super(layoutParams);
            this.mViewType = 0;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public LayoutParams(android.view.ViewGroup.MarginLayoutParams r5) {
            /*
                r4 = this;
                r0 = r4
                r1 = r5
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                r2 = r0
                r3 = 0
                r2.mViewType = r3
                r2 = r0
                r3 = r1
                r2.copyMarginsFromCompat(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.Toolbar.LayoutParams.<init>(android.view.ViewGroup$MarginLayoutParams):void");
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.mViewType = 0;
        }

        /* access modifiers changed from: package-private */
        public void copyMarginsFromCompat(ViewGroup.MarginLayoutParams marginLayoutParams) {
            ViewGroup.MarginLayoutParams marginLayoutParams2 = marginLayoutParams;
            this.leftMargin = marginLayoutParams2.leftMargin;
            this.topMargin = marginLayoutParams2.topMargin;
            this.rightMargin = marginLayoutParams2.rightMargin;
            this.bottomMargin = marginLayoutParams2.bottomMargin;
        }
    }

    public static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR;
        int expandedMenuItemId;
        boolean isOverflowOpen;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public SavedState(android.os.Parcel r5) {
            /*
                r4 = this;
                r0 = r4
                r1 = r5
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                r2 = r0
                r3 = r1
                int r3 = r3.readInt()
                r2.expandedMenuItemId = r3
                r2 = r0
                r3 = r1
                int r3 = r3.readInt()
                if (r3 == 0) goto L_0x001b
                r3 = 1
            L_0x0018:
                r2.isOverflowOpen = r3
                return
            L_0x001b:
                r3 = 0
                goto L_0x0018
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.Toolbar.SavedState.<init>(android.os.Parcel):void");
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            super.writeToParcel(parcel2, i);
            parcel2.writeInt(this.expandedMenuItemId);
            parcel2.writeInt(this.isOverflowOpen ? 1 : 0);
        }

        static {
            Parcelable.Creator<SavedState> creator;
            new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(Parcel parcel) {
                    SavedState savedState;
                    new SavedState(parcel);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = creator;
        }
    }

    private class ExpandedActionViewMenuPresenter implements MenuPresenter {
        MenuItemImpl mCurrentExpandedItem;
        MenuBuilder mMenu;

        private ExpandedActionViewMenuPresenter() {
        }

        public void initForMenu(Context context, MenuBuilder menuBuilder) {
            MenuBuilder menuBuilder2 = menuBuilder;
            if (!(this.mMenu == null || this.mCurrentExpandedItem == null)) {
                boolean collapseItemActionView = this.mMenu.collapseItemActionView(this.mCurrentExpandedItem);
            }
            this.mMenu = menuBuilder2;
        }

        public MenuView getMenuView(ViewGroup viewGroup) {
            return null;
        }

        public void updateMenuView(boolean z) {
            if (this.mCurrentExpandedItem != null) {
                boolean z2 = false;
                if (this.mMenu != null) {
                    int size = this.mMenu.size();
                    int i = 0;
                    while (true) {
                        if (i >= size) {
                            break;
                        } else if (this.mMenu.getItem(i) == this.mCurrentExpandedItem) {
                            z2 = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                if (!z2) {
                    boolean collapseItemActionView = collapseItemActionView(this.mMenu, this.mCurrentExpandedItem);
                }
            }
        }

        public void setCallback(MenuPresenter.Callback callback) {
        }

        public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
            return false;
        }

        public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        }

        public boolean flagActionItems() {
            return false;
        }

        public boolean expandItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
            MenuItemImpl menuItemImpl2 = menuItemImpl;
            Toolbar.this.ensureCollapseButtonView();
            if (Toolbar.this.mCollapseButtonView.getParent() != Toolbar.this) {
                Toolbar.this.addView(Toolbar.this.mCollapseButtonView);
            }
            Toolbar.this.mExpandedActionView = menuItemImpl2.getActionView();
            this.mCurrentExpandedItem = menuItemImpl2;
            if (Toolbar.this.mExpandedActionView.getParent() != Toolbar.this) {
                LayoutParams generateDefaultLayoutParams = Toolbar.this.generateDefaultLayoutParams();
                generateDefaultLayoutParams.gravity = 8388611 | (Toolbar.this.mButtonGravity & 112);
                generateDefaultLayoutParams.mViewType = 2;
                Toolbar.this.mExpandedActionView.setLayoutParams(generateDefaultLayoutParams);
                Toolbar.this.addView(Toolbar.this.mExpandedActionView);
            }
            Toolbar.this.removeChildrenForExpandedActionView();
            Toolbar.this.requestLayout();
            menuItemImpl2.setActionViewExpanded(true);
            if (Toolbar.this.mExpandedActionView instanceof CollapsibleActionView) {
                ((CollapsibleActionView) Toolbar.this.mExpandedActionView).onActionViewExpanded();
            }
            return true;
        }

        public boolean collapseItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
            MenuItemImpl menuItemImpl2 = menuItemImpl;
            if (Toolbar.this.mExpandedActionView instanceof CollapsibleActionView) {
                ((CollapsibleActionView) Toolbar.this.mExpandedActionView).onActionViewCollapsed();
            }
            Toolbar.this.removeView(Toolbar.this.mExpandedActionView);
            Toolbar.this.removeView(Toolbar.this.mCollapseButtonView);
            Toolbar.this.mExpandedActionView = null;
            Toolbar.this.addChildrenForExpandedActionView();
            this.mCurrentExpandedItem = null;
            Toolbar.this.requestLayout();
            menuItemImpl2.setActionViewExpanded(false);
            return true;
        }

        public int getId() {
            return 0;
        }

        public Parcelable onSaveInstanceState() {
            return null;
        }

        public void onRestoreInstanceState(Parcelable parcelable) {
        }
    }
}
