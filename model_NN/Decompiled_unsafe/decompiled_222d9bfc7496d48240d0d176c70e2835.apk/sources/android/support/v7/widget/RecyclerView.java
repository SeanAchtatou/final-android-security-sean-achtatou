package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.Observable;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.os.TraceCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.NestedScrollingChild;
import android.support.v4.view.NestedScrollingChildHelper;
import android.support.v4.view.ScrollingView;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityRecordCompat;
import android.support.v4.widget.EdgeEffectCompat;
import android.support.v4.widget.ScrollerCompat;
import android.support.v7.recyclerview.R;
import android.support.v7.widget.ActivityChooserView;
import android.support.v7.widget.AdapterHelper;
import android.support.v7.widget.ChildHelper;
import android.support.v7.widget.ViewInfoStore;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecyclerView extends ViewGroup implements ScrollingView, NestedScrollingChild {
    private static final boolean DEBUG = false;
    private static final boolean DISPATCH_TEMP_DETACH = false;
    /* access modifiers changed from: private */
    public static final boolean FORCE_INVALIDATE_DISPLAY_LIST;
    public static final int HORIZONTAL = 0;
    private static final int INVALID_POINTER = -1;
    public static final int INVALID_TYPE = -1;
    private static final Class<?>[] LAYOUT_MANAGER_CONSTRUCTOR_SIGNATURE;
    private static final int MAX_SCROLL_DURATION = 2000;
    public static final long NO_ID = -1;
    public static final int NO_POSITION = -1;
    public static final int SCROLL_STATE_DRAGGING = 1;
    public static final int SCROLL_STATE_IDLE = 0;
    public static final int SCROLL_STATE_SETTLING = 2;
    private static final String TAG = "RecyclerView";
    public static final int TOUCH_SLOP_DEFAULT = 0;
    public static final int TOUCH_SLOP_PAGING = 1;
    private static final String TRACE_BIND_VIEW_TAG = "RV OnBindView";
    private static final String TRACE_CREATE_VIEW_TAG = "RV CreateView";
    private static final String TRACE_HANDLE_ADAPTER_UPDATES_TAG = "RV PartialInvalidate";
    private static final String TRACE_ON_DATA_SET_CHANGE_LAYOUT_TAG = "RV FullInvalidate";
    private static final String TRACE_ON_LAYOUT_TAG = "RV OnLayout";
    private static final String TRACE_SCROLL_TAG = "RV Scroll";
    public static final int VERTICAL = 1;
    /* access modifiers changed from: private */
    public static final Interpolator sQuinticInterpolator;
    /* access modifiers changed from: private */
    public RecyclerViewAccessibilityDelegate mAccessibilityDelegate;
    private final AccessibilityManager mAccessibilityManager;
    private OnItemTouchListener mActiveOnItemTouchListener;
    /* access modifiers changed from: private */
    public Adapter mAdapter;
    AdapterHelper mAdapterHelper;
    private boolean mAdapterUpdateDuringMeasure;
    private EdgeEffectCompat mBottomGlow;
    private ChildDrawingOrderCallback mChildDrawingOrderCallback;
    ChildHelper mChildHelper;
    /* access modifiers changed from: private */
    public boolean mClipToPadding;
    /* access modifiers changed from: private */
    public boolean mDataSetHasChangedAfterLayout;
    private boolean mEatRequestLayout;
    private int mEatenAccessibilityChangeFlags;
    /* access modifiers changed from: private */
    public boolean mFirstLayoutComplete;
    /* access modifiers changed from: private */
    public boolean mHasFixedSize;
    private boolean mIgnoreMotionEventTillDown;
    private int mInitialTouchX;
    private int mInitialTouchY;
    /* access modifiers changed from: private */
    public boolean mIsAttached;
    ItemAnimator mItemAnimator;
    private ItemAnimator.ItemAnimatorListener mItemAnimatorListener;
    private Runnable mItemAnimatorRunner;
    /* access modifiers changed from: private */
    public final ArrayList<ItemDecoration> mItemDecorations;
    boolean mItemsAddedOrRemoved;
    boolean mItemsChanged;
    private int mLastTouchX;
    private int mLastTouchY;
    /* access modifiers changed from: private */
    public LayoutManager mLayout;
    /* access modifiers changed from: private */
    public boolean mLayoutFrozen;
    private int mLayoutOrScrollCounter;
    private boolean mLayoutRequestEaten;
    private EdgeEffectCompat mLeftGlow;
    private final int mMaxFlingVelocity;
    private final int mMinFlingVelocity;
    private final int[] mMinMaxLayoutPositions;
    private final int[] mNestedOffsets;
    private final RecyclerViewDataObserver mObserver;
    private List<OnChildAttachStateChangeListener> mOnChildAttachStateListeners;
    private final ArrayList<OnItemTouchListener> mOnItemTouchListeners;
    private SavedState mPendingSavedState;
    /* access modifiers changed from: private */
    public final boolean mPostUpdatesOnAnimation;
    private boolean mPostedAnimatorRunner;
    final Recycler mRecycler;
    /* access modifiers changed from: private */
    public RecyclerListener mRecyclerListener;
    private EdgeEffectCompat mRightGlow;
    private final int[] mScrollConsumed;
    private float mScrollFactor;
    private OnScrollListener mScrollListener;
    private List<OnScrollListener> mScrollListeners;
    private final int[] mScrollOffset;
    private int mScrollPointerId;
    private int mScrollState;
    private final NestedScrollingChildHelper mScrollingChildHelper;
    final State mState;
    private final Rect mTempRect;
    private EdgeEffectCompat mTopGlow;
    private int mTouchSlop;
    /* access modifiers changed from: private */
    public final Runnable mUpdateChildViewsRunnable;
    private VelocityTracker mVelocityTracker;
    /* access modifiers changed from: private */
    public final ViewFlinger mViewFlinger;
    private final ViewInfoStore.ProcessCallback mViewInfoProcessCallback;
    final ViewInfoStore mViewInfoStore;

    public interface ChildDrawingOrderCallback {
        int onGetChildDrawingOrder(int i, int i2);
    }

    public interface OnChildAttachStateChangeListener {
        void onChildViewAttachedToWindow(View view);

        void onChildViewDetachedFromWindow(View view);
    }

    public interface OnItemTouchListener {
        boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent);

        void onRequestDisallowInterceptTouchEvent(boolean z);

        void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent);
    }

    public interface RecyclerListener {
        void onViewRecycled(ViewHolder viewHolder);
    }

    public static abstract class ViewCacheExtension {
        public abstract View getViewForPositionAndType(Recycler recycler, int i, int i2);
    }

    static /* synthetic */ boolean access$302(RecyclerView recyclerView, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        recyclerView.mLayoutRequestEaten = z3;
        return z2;
    }

    static /* synthetic */ boolean access$4302(RecyclerView recyclerView, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        recyclerView.mAdapterUpdateDuringMeasure = z3;
        return z2;
    }

    static /* synthetic */ boolean access$602(RecyclerView recyclerView, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        recyclerView.mPostedAnimatorRunner = z3;
        return z2;
    }

    static {
        boolean z;
        Interpolator interpolator;
        if (Build.VERSION.SDK_INT == 18 || Build.VERSION.SDK_INT == 19 || Build.VERSION.SDK_INT == 20) {
            z = true;
        } else {
            z = false;
        }
        FORCE_INVALIDATE_DISPLAY_LIST = z;
        Class<?>[] clsArr = new Class[4];
        clsArr[0] = Context.class;
        Class<?>[] clsArr2 = clsArr;
        clsArr2[1] = AttributeSet.class;
        Class<?>[] clsArr3 = clsArr2;
        clsArr3[2] = Integer.TYPE;
        Class<?>[] clsArr4 = clsArr3;
        clsArr4[3] = Integer.TYPE;
        LAYOUT_MANAGER_CONSTRUCTOR_SIGNATURE = clsArr4;
        new Interpolator() {
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (f2 * f2 * f2 * f2 * f2) + 1.0f;
            }
        };
        sQuinticInterpolator = interpolator;
    }

    public RecyclerView(Context context) {
        this(context, null);
    }

    public RecyclerView(Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public RecyclerView(android.content.Context r17, @android.support.annotation.Nullable android.util.AttributeSet r18, int r19) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            r3 = r19
            r9 = r0
            r10 = r1
            r11 = r2
            r12 = r3
            r9.<init>(r10, r11, r12)
            r9 = r0
            android.support.v7.widget.RecyclerView$RecyclerViewDataObserver r10 = new android.support.v7.widget.RecyclerView$RecyclerViewDataObserver
            r15 = r10
            r10 = r15
            r11 = r15
            r12 = r0
            r13 = 0
            r11.<init>()
            r9.mObserver = r10
            r9 = r0
            android.support.v7.widget.RecyclerView$Recycler r10 = new android.support.v7.widget.RecyclerView$Recycler
            r15 = r10
            r10 = r15
            r11 = r15
            r12 = r0
            r11.<init>()
            r9.mRecycler = r10
            r9 = r0
            android.support.v7.widget.ViewInfoStore r10 = new android.support.v7.widget.ViewInfoStore
            r15 = r10
            r10 = r15
            r11 = r15
            r11.<init>()
            r9.mViewInfoStore = r10
            r9 = r0
            android.support.v7.widget.RecyclerView$1 r10 = new android.support.v7.widget.RecyclerView$1
            r15 = r10
            r10 = r15
            r11 = r15
            r12 = r0
            r11.<init>()
            r9.mUpdateChildViewsRunnable = r10
            r9 = r0
            android.graphics.Rect r10 = new android.graphics.Rect
            r15 = r10
            r10 = r15
            r11 = r15
            r11.<init>()
            r9.mTempRect = r10
            r9 = r0
            java.util.ArrayList r10 = new java.util.ArrayList
            r15 = r10
            r10 = r15
            r11 = r15
            r11.<init>()
            r9.mItemDecorations = r10
            r9 = r0
            java.util.ArrayList r10 = new java.util.ArrayList
            r15 = r10
            r10 = r15
            r11 = r15
            r11.<init>()
            r9.mOnItemTouchListeners = r10
            r9 = r0
            r10 = 0
            r9.mDataSetHasChangedAfterLayout = r10
            r9 = r0
            r10 = 0
            r9.mLayoutOrScrollCounter = r10
            r9 = r0
            android.support.v7.widget.DefaultItemAnimator r10 = new android.support.v7.widget.DefaultItemAnimator
            r15 = r10
            r10 = r15
            r11 = r15
            r11.<init>()
            r9.mItemAnimator = r10
            r9 = r0
            r10 = 0
            r9.mScrollState = r10
            r9 = r0
            r10 = -1
            r9.mScrollPointerId = r10
            r9 = r0
            r10 = 1
            r9.mScrollFactor = r10
            r9 = r0
            android.support.v7.widget.RecyclerView$ViewFlinger r10 = new android.support.v7.widget.RecyclerView$ViewFlinger
            r15 = r10
            r10 = r15
            r11 = r15
            r12 = r0
            r11.<init>(r12)
            r9.mViewFlinger = r10
            r9 = r0
            android.support.v7.widget.RecyclerView$State r10 = new android.support.v7.widget.RecyclerView$State
            r15 = r10
            r10 = r15
            r11 = r15
            r11.<init>()
            r9.mState = r10
            r9 = r0
            r10 = 0
            r9.mItemsAddedOrRemoved = r10
            r9 = r0
            r10 = 0
            r9.mItemsChanged = r10
            r9 = r0
            android.support.v7.widget.RecyclerView$ItemAnimatorRestoreListener r10 = new android.support.v7.widget.RecyclerView$ItemAnimatorRestoreListener
            r15 = r10
            r10 = r15
            r11 = r15
            r12 = r0
            r13 = 0
            r11.<init>()
            r9.mItemAnimatorListener = r10
            r9 = r0
            r10 = 0
            r9.mPostedAnimatorRunner = r10
            r9 = r0
            r10 = 2
            int[] r10 = new int[r10]
            r9.mMinMaxLayoutPositions = r10
            r9 = r0
            r10 = 2
            int[] r10 = new int[r10]
            r9.mScrollOffset = r10
            r9 = r0
            r10 = 2
            int[] r10 = new int[r10]
            r9.mScrollConsumed = r10
            r9 = r0
            r10 = 2
            int[] r10 = new int[r10]
            r9.mNestedOffsets = r10
            r9 = r0
            android.support.v7.widget.RecyclerView$2 r10 = new android.support.v7.widget.RecyclerView$2
            r15 = r10
            r10 = r15
            r11 = r15
            r12 = r0
            r11.<init>()
            r9.mItemAnimatorRunner = r10
            r9 = r0
            android.support.v7.widget.RecyclerView$4 r10 = new android.support.v7.widget.RecyclerView$4
            r15 = r10
            r10 = r15
            r11 = r15
            r12 = r0
            r11.<init>()
            r9.mViewInfoProcessCallback = r10
            r9 = r0
            r10 = 1
            r9.setScrollContainer(r10)
            r9 = r0
            r10 = 1
            r9.setFocusableInTouchMode(r10)
            int r9 = android.os.Build.VERSION.SDK_INT
            r4 = r9
            r9 = r0
            r10 = r4
            r11 = 16
            if (r10 < r11) goto L_0x0191
            r10 = 1
        L_0x00f3:
            r9.mPostUpdatesOnAnimation = r10
            r9 = r1
            android.view.ViewConfiguration r9 = android.view.ViewConfiguration.get(r9)
            r5 = r9
            r9 = r0
            r10 = r5
            int r10 = r10.getScaledTouchSlop()
            r9.mTouchSlop = r10
            r9 = r0
            r10 = r5
            int r10 = r10.getScaledMinimumFlingVelocity()
            r9.mMinFlingVelocity = r10
            r9 = r0
            r10 = r5
            int r10 = r10.getScaledMaximumFlingVelocity()
            r9.mMaxFlingVelocity = r10
            r9 = r0
            r10 = r0
            int r10 = android.support.v4.view.ViewCompat.getOverScrollMode(r10)
            r11 = 2
            if (r10 != r11) goto L_0x0194
            r10 = 1
        L_0x011d:
            r9.setWillNotDraw(r10)
            r9 = r0
            android.support.v7.widget.RecyclerView$ItemAnimator r9 = r9.mItemAnimator
            r10 = r0
            android.support.v7.widget.RecyclerView$ItemAnimator$ItemAnimatorListener r10 = r10.mItemAnimatorListener
            r9.setListener(r10)
            r9 = r0
            r9.initAdapterManager()
            r9 = r0
            r9.initChildrenHelper()
            r9 = r0
            int r9 = android.support.v4.view.ViewCompat.getImportantForAccessibility(r9)
            if (r9 != 0) goto L_0x013d
            r9 = r0
            r10 = 1
            android.support.v4.view.ViewCompat.setImportantForAccessibility(r9, r10)
        L_0x013d:
            r9 = r0
            r10 = r0
            android.content.Context r10 = r10.getContext()
            java.lang.String r11 = "accessibility"
            java.lang.Object r10 = r10.getSystemService(r11)
            android.view.accessibility.AccessibilityManager r10 = (android.view.accessibility.AccessibilityManager) r10
            r9.mAccessibilityManager = r10
            r9 = r0
            android.support.v7.widget.RecyclerViewAccessibilityDelegate r10 = new android.support.v7.widget.RecyclerViewAccessibilityDelegate
            r15 = r10
            r10 = r15
            r11 = r15
            r12 = r0
            r11.<init>(r12)
            r9.setAccessibilityDelegateCompat(r10)
            r9 = r2
            if (r9 == 0) goto L_0x017f
            r9 = 0
            r6 = r9
            r9 = r1
            r10 = r2
            int[] r11 = android.support.v7.recyclerview.R.styleable.RecyclerView
            r12 = r3
            r13 = r6
            android.content.res.TypedArray r9 = r9.obtainStyledAttributes(r10, r11, r12, r13)
            r7 = r9
            r9 = r7
            int r10 = android.support.v7.recyclerview.R.styleable.RecyclerView_layoutManager
            java.lang.String r9 = r9.getString(r10)
            r8 = r9
            r9 = r7
            r9.recycle()
            r9 = r0
            r10 = r1
            r11 = r8
            r12 = r2
            r13 = r3
            r14 = r6
            r9.createLayoutManager(r10, r11, r12, r13, r14)
        L_0x017f:
            r9 = r0
            android.support.v4.view.NestedScrollingChildHelper r10 = new android.support.v4.view.NestedScrollingChildHelper
            r15 = r10
            r10 = r15
            r11 = r15
            r12 = r0
            r11.<init>(r12)
            r9.mScrollingChildHelper = r10
            r9 = r0
            r10 = 1
            r9.setNestedScrollingEnabled(r10)
            return
        L_0x0191:
            r10 = 0
            goto L_0x00f3
        L_0x0194:
            r10 = 0
            goto L_0x011d
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public RecyclerViewAccessibilityDelegate getCompatAccessibilityDelegate() {
        return this.mAccessibilityDelegate;
    }

    public void setAccessibilityDelegateCompat(RecyclerViewAccessibilityDelegate recyclerViewAccessibilityDelegate) {
        this.mAccessibilityDelegate = recyclerViewAccessibilityDelegate;
        ViewCompat.setAccessibilityDelegate(this, this.mAccessibilityDelegate);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void createLayoutManager(android.content.Context r18, java.lang.String r19, android.util.AttributeSet r20, int r21, int r22) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            r3 = r20
            r4 = r21
            r5 = r22
            r12 = r2
            if (r12 == 0) goto L_0x0091
            r12 = r2
            java.lang.String r12 = r12.trim()
            r2 = r12
            r12 = r2
            int r12 = r12.length()
            if (r12 == 0) goto L_0x0091
            r12 = r0
            r13 = r1
            r14 = r2
            java.lang.String r12 = r12.getFullClassName(r13, r14)
            r2 = r12
            r12 = r0
            boolean r12 = r12.isInEditMode()     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            if (r12 == 0) goto L_0x0092
            r12 = r0
            java.lang.Class r12 = r12.getClass()     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            java.lang.ClassLoader r12 = r12.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            r6 = r12
        L_0x0035:
            r12 = r6
            r13 = r2
            java.lang.Class r12 = r12.loadClass(r13)     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            java.lang.Class<android.support.v7.widget.RecyclerView$LayoutManager> r13 = android.support.v7.widget.RecyclerView.LayoutManager.class
            java.lang.Class r12 = r12.asSubclass(r13)     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            r7 = r12
            r12 = 0
            r9 = r12
            r12 = r7
            java.lang.Class<?>[] r13 = android.support.v7.widget.RecyclerView.LAYOUT_MANAGER_CONSTRUCTOR_SIGNATURE     // Catch:{ NoSuchMethodException -> 0x0099 }
            java.lang.reflect.Constructor r12 = r12.getConstructor(r13)     // Catch:{ NoSuchMethodException -> 0x0099 }
            r8 = r12
            r12 = 4
            java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ NoSuchMethodException -> 0x0099 }
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = 0
            r15 = r1
            r13[r14] = r15     // Catch:{ NoSuchMethodException -> 0x0099 }
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = 1
            r15 = r3
            r13[r14] = r15     // Catch:{ NoSuchMethodException -> 0x0099 }
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = 2
            r15 = r4
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ NoSuchMethodException -> 0x0099 }
            r13[r14] = r15     // Catch:{ NoSuchMethodException -> 0x0099 }
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = 3
            r15 = r5
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ NoSuchMethodException -> 0x0099 }
            r13[r14] = r15     // Catch:{ NoSuchMethodException -> 0x0099 }
            r9 = r12
        L_0x0080:
            r12 = r8
            r13 = 1
            r12.setAccessible(r13)     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            r12 = r0
            r13 = r8
            r14 = r9
            java.lang.Object r13 = r13.newInstance(r14)     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            android.support.v7.widget.RecyclerView$LayoutManager r13 = (android.support.v7.widget.RecyclerView.LayoutManager) r13     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            r12.setLayoutManager(r13)     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
        L_0x0091:
            return
        L_0x0092:
            r12 = r1
            java.lang.ClassLoader r12 = r12.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            r6 = r12
            goto L_0x0035
        L_0x0099:
            r12 = move-exception
            r10 = r12
            r12 = r7
            r13 = 0
            java.lang.Class[] r13 = new java.lang.Class[r13]     // Catch:{ NoSuchMethodException -> 0x00a5 }
            java.lang.reflect.Constructor r12 = r12.getConstructor(r13)     // Catch:{ NoSuchMethodException -> 0x00a5 }
            r8 = r12
            goto L_0x0080
        L_0x00a5:
            r12 = move-exception
            r11 = r12
            r12 = r11
            r13 = r10
            java.lang.Throwable r12 = r12.initCause(r13)     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            r16 = r12
            r12 = r16
            r13 = r16
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            r16 = r14
            r14 = r16
            r15 = r16
            r15.<init>()     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            r15 = r3
            java.lang.String r15 = r15.getPositionDescription()     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            java.lang.String r15 = ": Error creating LayoutManager "
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            r15 = r2
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            java.lang.String r14 = r14.toString()     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            r15 = r11
            r13.<init>(r14, r15)     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
            throw r12     // Catch:{ ClassNotFoundException -> 0x00dd, InvocationTargetException -> 0x010f, InstantiationException -> 0x0141, IllegalAccessException -> 0x0173, ClassCastException -> 0x01a5 }
        L_0x00dd:
            r12 = move-exception
            r6 = r12
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            r16 = r12
            r12 = r16
            r13 = r16
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r16 = r14
            r14 = r16
            r15 = r16
            r15.<init>()
            r15 = r3
            java.lang.String r15 = r15.getPositionDescription()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = ": Unable to find LayoutManager "
            java.lang.StringBuilder r14 = r14.append(r15)
            r15 = r2
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            r15 = r6
            r13.<init>(r14, r15)
            throw r12
        L_0x010f:
            r12 = move-exception
            r6 = r12
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            r16 = r12
            r12 = r16
            r13 = r16
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r16 = r14
            r14 = r16
            r15 = r16
            r15.<init>()
            r15 = r3
            java.lang.String r15 = r15.getPositionDescription()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = ": Could not instantiate the LayoutManager: "
            java.lang.StringBuilder r14 = r14.append(r15)
            r15 = r2
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            r15 = r6
            r13.<init>(r14, r15)
            throw r12
        L_0x0141:
            r12 = move-exception
            r6 = r12
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            r16 = r12
            r12 = r16
            r13 = r16
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r16 = r14
            r14 = r16
            r15 = r16
            r15.<init>()
            r15 = r3
            java.lang.String r15 = r15.getPositionDescription()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = ": Could not instantiate the LayoutManager: "
            java.lang.StringBuilder r14 = r14.append(r15)
            r15 = r2
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            r15 = r6
            r13.<init>(r14, r15)
            throw r12
        L_0x0173:
            r12 = move-exception
            r6 = r12
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            r16 = r12
            r12 = r16
            r13 = r16
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r16 = r14
            r14 = r16
            r15 = r16
            r15.<init>()
            r15 = r3
            java.lang.String r15 = r15.getPositionDescription()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = ": Cannot access non-public constructor "
            java.lang.StringBuilder r14 = r14.append(r15)
            r15 = r2
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            r15 = r6
            r13.<init>(r14, r15)
            throw r12
        L_0x01a5:
            r12 = move-exception
            r6 = r12
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            r16 = r12
            r12 = r16
            r13 = r16
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r16 = r14
            r14 = r16
            r15 = r16
            r15.<init>()
            r15 = r3
            java.lang.String r15 = r15.getPositionDescription()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = ": Class is not a LayoutManager "
            java.lang.StringBuilder r14 = r14.append(r15)
            r15 = r2
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            r15 = r6
            r13.<init>(r14, r15)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.createLayoutManager(android.content.Context, java.lang.String, android.util.AttributeSet, int, int):void");
    }

    private String getFullClassName(Context context, String str) {
        StringBuilder sb;
        StringBuilder sb2;
        Context context2 = context;
        String str2 = str;
        if (str2.charAt(0) == '.') {
            new StringBuilder();
            return sb2.append(context2.getPackageName()).append(str2).toString();
        } else if (str2.contains(".")) {
            return str2;
        } else {
            new StringBuilder();
            return sb.append(RecyclerView.class.getPackage().getName()).append('.').append(str2).toString();
        }
    }

    private void initChildrenHelper() {
        ChildHelper childHelper;
        ChildHelper.Callback callback;
        new ChildHelper.Callback() {
            public int getChildCount() {
                return RecyclerView.this.getChildCount();
            }

            public void addView(View view, int i) {
                View view2 = view;
                RecyclerView.this.addView(view2, i);
                RecyclerView.this.dispatchChildAttached(view2);
            }

            public int indexOfChild(View view) {
                return RecyclerView.this.indexOfChild(view);
            }

            public void removeViewAt(int i) {
                int i2 = i;
                View childAt = RecyclerView.this.getChildAt(i2);
                if (childAt != null) {
                    RecyclerView.this.dispatchChildDetached(childAt);
                }
                RecyclerView.this.removeViewAt(i2);
            }

            public View getChildAt(int i) {
                return RecyclerView.this.getChildAt(i);
            }

            public void removeAllViews() {
                int childCount = getChildCount();
                for (int i = 0; i < childCount; i++) {
                    RecyclerView.this.dispatchChildDetached(getChildAt(i));
                }
                RecyclerView.this.removeAllViews();
            }

            public ViewHolder getChildViewHolder(View view) {
                return RecyclerView.getChildViewHolderInt(view);
            }

            public void attachViewToParent(View view, int i, ViewGroup.LayoutParams layoutParams) {
                Throwable th;
                StringBuilder sb;
                View view2 = view;
                int i2 = i;
                ViewGroup.LayoutParams layoutParams2 = layoutParams;
                ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view2);
                if (childViewHolderInt != null) {
                    if (childViewHolderInt.isTmpDetached() || childViewHolderInt.shouldIgnore()) {
                        childViewHolderInt.clearTmpDetachFlag();
                    } else {
                        Throwable th2 = th;
                        new StringBuilder();
                        new IllegalArgumentException(sb.append("Called attach on a child which is not detached: ").append(childViewHolderInt).toString());
                        throw th2;
                    }
                }
                RecyclerView.this.attachViewToParent(view2, i2, layoutParams2);
            }

            public void detachViewFromParent(int i) {
                ViewHolder childViewHolderInt;
                Throwable th;
                StringBuilder sb;
                int i2 = i;
                View childAt = getChildAt(i2);
                if (!(childAt == null || (childViewHolderInt = RecyclerView.getChildViewHolderInt(childAt)) == null)) {
                    if (!childViewHolderInt.isTmpDetached() || childViewHolderInt.shouldIgnore()) {
                        childViewHolderInt.addFlags(256);
                    } else {
                        Throwable th2 = th;
                        new StringBuilder();
                        new IllegalArgumentException(sb.append("called detach on an already detached child ").append(childViewHolderInt).toString());
                        throw th2;
                    }
                }
                RecyclerView.this.detachViewFromParent(i2);
            }

            public void onEnteredHiddenState(View view) {
                ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
                if (childViewHolderInt != null) {
                    childViewHolderInt.onEnteredHiddenState();
                }
            }

            public void onLeftHiddenState(View view) {
                ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
                if (childViewHolderInt != null) {
                    childViewHolderInt.onLeftHiddenState();
                }
            }
        };
        new ChildHelper(callback);
        this.mChildHelper = childHelper;
    }

    /* access modifiers changed from: package-private */
    public void initAdapterManager() {
        AdapterHelper adapterHelper;
        AdapterHelper.Callback callback;
        new AdapterHelper.Callback() {
            public ViewHolder findViewHolder(int i) {
                ViewHolder findViewHolderForPosition = RecyclerView.this.findViewHolderForPosition(i, true);
                if (findViewHolderForPosition == null) {
                    return null;
                }
                if (RecyclerView.this.mChildHelper.isHidden(findViewHolderForPosition.itemView)) {
                    return null;
                }
                return findViewHolderForPosition;
            }

            public void offsetPositionsForRemovingInvisible(int i, int i2) {
                int i3 = i2;
                RecyclerView.this.offsetPositionRecordsForRemove(i, i3, true);
                RecyclerView.this.mItemsAddedOrRemoved = true;
                int access$1812 = State.access$1812(RecyclerView.this.mState, i3);
            }

            public void offsetPositionsForRemovingLaidOutOrNewView(int i, int i2) {
                RecyclerView.this.offsetPositionRecordsForRemove(i, i2, false);
                RecyclerView.this.mItemsAddedOrRemoved = true;
            }

            public void markViewHoldersUpdated(int i, int i2, Object obj) {
                RecyclerView.this.viewRangeUpdate(i, i2, obj);
                RecyclerView.this.mItemsChanged = true;
            }

            public void onDispatchFirstPass(AdapterHelper.UpdateOp updateOp) {
                dispatchUpdate(updateOp);
            }

            /* access modifiers changed from: package-private */
            public void dispatchUpdate(AdapterHelper.UpdateOp updateOp) {
                AdapterHelper.UpdateOp updateOp2 = updateOp;
                switch (updateOp2.cmd) {
                    case 1:
                        RecyclerView.this.mLayout.onItemsAdded(RecyclerView.this, updateOp2.positionStart, updateOp2.itemCount);
                        return;
                    case 2:
                        RecyclerView.this.mLayout.onItemsRemoved(RecyclerView.this, updateOp2.positionStart, updateOp2.itemCount);
                        return;
                    case 3:
                    case 5:
                    case 6:
                    case 7:
                    default:
                        return;
                    case 4:
                        RecyclerView.this.mLayout.onItemsUpdated(RecyclerView.this, updateOp2.positionStart, updateOp2.itemCount, updateOp2.payload);
                        return;
                    case 8:
                        RecyclerView.this.mLayout.onItemsMoved(RecyclerView.this, updateOp2.positionStart, updateOp2.itemCount, 1);
                        return;
                }
            }

            public void onDispatchSecondPass(AdapterHelper.UpdateOp updateOp) {
                dispatchUpdate(updateOp);
            }

            public void offsetPositionsForAdd(int i, int i2) {
                RecyclerView.this.offsetPositionRecordsForInsert(i, i2);
                RecyclerView.this.mItemsAddedOrRemoved = true;
            }

            public void offsetPositionsForMove(int i, int i2) {
                RecyclerView.this.offsetPositionRecordsForMove(i, i2);
                RecyclerView.this.mItemsAddedOrRemoved = true;
            }
        };
        new AdapterHelper(callback);
        this.mAdapterHelper = adapterHelper;
    }

    public void setHasFixedSize(boolean z) {
        this.mHasFixedSize = z;
    }

    public boolean hasFixedSize() {
        return this.mHasFixedSize;
    }

    public void setClipToPadding(boolean z) {
        boolean z2 = z;
        if (z2 != this.mClipToPadding) {
            invalidateGlows();
        }
        this.mClipToPadding = z2;
        super.setClipToPadding(z2);
        if (this.mFirstLayoutComplete) {
            requestLayout();
        }
    }

    public void setScrollingTouchSlop(int i) {
        StringBuilder sb;
        int i2 = i;
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        switch (i2) {
            case 0:
                break;
            case 1:
                this.mTouchSlop = ViewConfigurationCompat.getScaledPagingTouchSlop(viewConfiguration);
                return;
            default:
                new StringBuilder();
                int w = Log.w(TAG, sb.append("setScrollingTouchSlop(): bad argument constant ").append(i2).append("; using default value").toString());
                break;
        }
        this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
    }

    public void swapAdapter(Adapter adapter, boolean z) {
        setLayoutFrozen(false);
        setAdapterInternal(adapter, true, z);
        setDataSetChangedAfterLayout();
        requestLayout();
    }

    public void setAdapter(Adapter adapter) {
        setLayoutFrozen(false);
        setAdapterInternal(adapter, false, true);
        requestLayout();
    }

    private void setAdapterInternal(Adapter adapter, boolean z, boolean z2) {
        Adapter adapter2 = adapter;
        boolean z3 = z;
        boolean z4 = z2;
        if (this.mAdapter != null) {
            this.mAdapter.unregisterAdapterDataObserver(this.mObserver);
            this.mAdapter.onDetachedFromRecyclerView(this);
        }
        if (!z3 || z4) {
            if (this.mItemAnimator != null) {
                this.mItemAnimator.endAnimations();
            }
            if (this.mLayout != null) {
                this.mLayout.removeAndRecycleAllViews(this.mRecycler);
                this.mLayout.removeAndRecycleScrapInt(this.mRecycler);
            }
            this.mRecycler.clear();
        }
        this.mAdapterHelper.reset();
        Adapter adapter3 = this.mAdapter;
        this.mAdapter = adapter2;
        if (adapter2 != null) {
            adapter2.registerAdapterDataObserver(this.mObserver);
            adapter2.onAttachedToRecyclerView(this);
        }
        if (this.mLayout != null) {
            this.mLayout.onAdapterChanged(adapter3, this.mAdapter);
        }
        this.mRecycler.onAdapterChanged(adapter3, this.mAdapter, z3);
        boolean access$1902 = State.access$1902(this.mState, true);
        markKnownViewsInvalid();
    }

    public Adapter getAdapter() {
        return this.mAdapter;
    }

    public void setRecyclerListener(RecyclerListener recyclerListener) {
        this.mRecyclerListener = recyclerListener;
    }

    public int getBaseline() {
        if (this.mLayout != null) {
            return this.mLayout.getBaseline();
        }
        return super.getBaseline();
    }

    public void addOnChildAttachStateChangeListener(OnChildAttachStateChangeListener onChildAttachStateChangeListener) {
        List<OnChildAttachStateChangeListener> list;
        OnChildAttachStateChangeListener onChildAttachStateChangeListener2 = onChildAttachStateChangeListener;
        if (this.mOnChildAttachStateListeners == null) {
            new ArrayList();
            this.mOnChildAttachStateListeners = list;
        }
        boolean add = this.mOnChildAttachStateListeners.add(onChildAttachStateChangeListener2);
    }

    public void removeOnChildAttachStateChangeListener(OnChildAttachStateChangeListener onChildAttachStateChangeListener) {
        OnChildAttachStateChangeListener onChildAttachStateChangeListener2 = onChildAttachStateChangeListener;
        if (this.mOnChildAttachStateListeners != null) {
            boolean remove = this.mOnChildAttachStateListeners.remove(onChildAttachStateChangeListener2);
        }
    }

    public void clearOnChildAttachStateChangeListeners() {
        if (this.mOnChildAttachStateListeners != null) {
            this.mOnChildAttachStateListeners.clear();
        }
    }

    public void setLayoutManager(LayoutManager layoutManager) {
        Throwable th;
        StringBuilder sb;
        LayoutManager layoutManager2 = layoutManager;
        if (layoutManager2 != this.mLayout) {
            if (this.mLayout != null) {
                if (this.mIsAttached) {
                    this.mLayout.dispatchDetachedFromWindow(this, this.mRecycler);
                }
                this.mLayout.setRecyclerView(null);
            }
            this.mRecycler.clear();
            this.mChildHelper.removeAllViewsUnfiltered();
            this.mLayout = layoutManager2;
            if (layoutManager2 != null) {
                if (layoutManager2.mRecyclerView != null) {
                    Throwable th2 = th;
                    new StringBuilder();
                    new IllegalArgumentException(sb.append("LayoutManager ").append(layoutManager2).append(" is already attached to a RecyclerView: ").append(layoutManager2.mRecyclerView).toString());
                    throw th2;
                }
                this.mLayout.setRecyclerView(this);
                if (this.mIsAttached) {
                    this.mLayout.dispatchAttachedToWindow(this);
                }
            }
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState;
        new SavedState(super.onSaveInstanceState());
        SavedState savedState2 = savedState;
        if (this.mPendingSavedState != null) {
            savedState2.copyFrom(this.mPendingSavedState);
        } else if (this.mLayout != null) {
            savedState2.mLayoutState = this.mLayout.onSaveInstanceState();
        } else {
            savedState2.mLayoutState = null;
        }
        return savedState2;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        this.mPendingSavedState = (SavedState) parcelable;
        super.onRestoreInstanceState(this.mPendingSavedState.getSuperState());
        if (this.mLayout != null && this.mPendingSavedState.mLayoutState != null) {
            this.mLayout.onRestoreInstanceState(this.mPendingSavedState.mLayoutState);
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    private void addAnimatingView(ViewHolder viewHolder) {
        ViewHolder viewHolder2 = viewHolder;
        View view = viewHolder2.itemView;
        boolean z = view.getParent() == this;
        this.mRecycler.unscrapView(getChildViewHolder(view));
        if (viewHolder2.isTmpDetached()) {
            this.mChildHelper.attachViewToParent(view, -1, view.getLayoutParams(), true);
        } else if (!z) {
            this.mChildHelper.addView(view, true);
        } else {
            this.mChildHelper.hide(view);
        }
    }

    /* access modifiers changed from: private */
    public boolean removeAnimatingView(View view) {
        View view2 = view;
        eatRequestLayout();
        boolean removeViewIfHidden = this.mChildHelper.removeViewIfHidden(view2);
        if (removeViewIfHidden) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(view2);
            this.mRecycler.unscrapView(childViewHolderInt);
            this.mRecycler.recycleViewHolderInternal(childViewHolderInt);
        }
        resumeRequestLayout(false);
        return removeViewIfHidden;
    }

    public LayoutManager getLayoutManager() {
        return this.mLayout;
    }

    public RecycledViewPool getRecycledViewPool() {
        return this.mRecycler.getRecycledViewPool();
    }

    public void setRecycledViewPool(RecycledViewPool recycledViewPool) {
        this.mRecycler.setRecycledViewPool(recycledViewPool);
    }

    public void setViewCacheExtension(ViewCacheExtension viewCacheExtension) {
        this.mRecycler.setViewCacheExtension(viewCacheExtension);
    }

    public void setItemViewCacheSize(int i) {
        this.mRecycler.setViewCacheSize(i);
    }

    public int getScrollState() {
        return this.mScrollState;
    }

    /* access modifiers changed from: private */
    public void setScrollState(int i) {
        int i2 = i;
        if (i2 != this.mScrollState) {
            this.mScrollState = i2;
            if (i2 != 2) {
                stopScrollersInternal();
            }
            dispatchOnScrollStateChanged(i2);
        }
    }

    public void addItemDecoration(ItemDecoration itemDecoration, int i) {
        ItemDecoration itemDecoration2 = itemDecoration;
        int i2 = i;
        if (this.mLayout != null) {
            this.mLayout.assertNotInLayoutOrScroll("Cannot add item decoration during a scroll  or layout");
        }
        if (this.mItemDecorations.isEmpty()) {
            setWillNotDraw(false);
        }
        if (i2 < 0) {
            boolean add = this.mItemDecorations.add(itemDecoration2);
        } else {
            this.mItemDecorations.add(i2, itemDecoration2);
        }
        markItemDecorInsetsDirty();
        requestLayout();
    }

    public void addItemDecoration(ItemDecoration itemDecoration) {
        addItemDecoration(itemDecoration, -1);
    }

    public void removeItemDecoration(ItemDecoration itemDecoration) {
        ItemDecoration itemDecoration2 = itemDecoration;
        if (this.mLayout != null) {
            this.mLayout.assertNotInLayoutOrScroll("Cannot remove item decoration during a scroll  or layout");
        }
        boolean remove = this.mItemDecorations.remove(itemDecoration2);
        if (this.mItemDecorations.isEmpty()) {
            setWillNotDraw(ViewCompat.getOverScrollMode(this) == 2);
        }
        markItemDecorInsetsDirty();
        requestLayout();
    }

    public void setChildDrawingOrderCallback(ChildDrawingOrderCallback childDrawingOrderCallback) {
        ChildDrawingOrderCallback childDrawingOrderCallback2 = childDrawingOrderCallback;
        if (childDrawingOrderCallback2 != this.mChildDrawingOrderCallback) {
            this.mChildDrawingOrderCallback = childDrawingOrderCallback2;
            setChildrenDrawingOrderEnabled(this.mChildDrawingOrderCallback != null);
        }
    }

    @Deprecated
    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.mScrollListener = onScrollListener;
    }

    public void addOnScrollListener(OnScrollListener onScrollListener) {
        List<OnScrollListener> list;
        OnScrollListener onScrollListener2 = onScrollListener;
        if (this.mScrollListeners == null) {
            new ArrayList();
            this.mScrollListeners = list;
        }
        boolean add = this.mScrollListeners.add(onScrollListener2);
    }

    public void removeOnScrollListener(OnScrollListener onScrollListener) {
        OnScrollListener onScrollListener2 = onScrollListener;
        if (this.mScrollListeners != null) {
            boolean remove = this.mScrollListeners.remove(onScrollListener2);
        }
    }

    public void clearOnScrollListeners() {
        if (this.mScrollListeners != null) {
            this.mScrollListeners.clear();
        }
    }

    public void scrollToPosition(int i) {
        int i2 = i;
        if (!this.mLayoutFrozen) {
            stopScroll();
            if (this.mLayout == null) {
                int e = Log.e(TAG, "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument.");
                return;
            }
            this.mLayout.scrollToPosition(i2);
            boolean awakenScrollBars = awakenScrollBars();
        }
    }

    /* access modifiers changed from: private */
    public void jumpToPositionForSmoothScroller(int i) {
        int i2 = i;
        if (this.mLayout != null) {
            this.mLayout.scrollToPosition(i2);
            boolean awakenScrollBars = awakenScrollBars();
        }
    }

    public void smoothScrollToPosition(int i) {
        int i2 = i;
        if (!this.mLayoutFrozen) {
            if (this.mLayout == null) {
                int e = Log.e(TAG, "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            } else {
                this.mLayout.smoothScrollToPosition(this, this.mState, i2);
            }
        }
    }

    public void scrollTo(int i, int i2) {
        int w = Log.w(TAG, "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
    }

    public void scrollBy(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (this.mLayout == null) {
            int e = Log.e(TAG, "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.mLayoutFrozen) {
            boolean canScrollHorizontally = this.mLayout.canScrollHorizontally();
            boolean canScrollVertically = this.mLayout.canScrollVertically();
            if (canScrollHorizontally || canScrollVertically) {
                boolean scrollByInternal = scrollByInternal(canScrollHorizontally ? i3 : 0, canScrollVertically ? i4 : 0, null);
            }
        }
    }

    /* access modifiers changed from: private */
    public void consumePendingUpdateOperations() {
        if (this.mFirstLayoutComplete) {
            if (this.mDataSetHasChangedAfterLayout) {
                TraceCompat.beginSection(TRACE_ON_DATA_SET_CHANGE_LAYOUT_TAG);
                dispatchLayout();
                TraceCompat.endSection();
            } else if (this.mAdapterHelper.hasPendingUpdates()) {
                if (this.mAdapterHelper.hasAnyUpdateTypes(4) && !this.mAdapterHelper.hasAnyUpdateTypes(11)) {
                    TraceCompat.beginSection(TRACE_HANDLE_ADAPTER_UPDATES_TAG);
                    eatRequestLayout();
                    this.mAdapterHelper.preProcess();
                    if (!this.mLayoutRequestEaten) {
                        if (hasUpdatedView()) {
                            dispatchLayout();
                        } else {
                            this.mAdapterHelper.consumePostponedUpdates();
                        }
                    }
                    resumeRequestLayout(true);
                    TraceCompat.endSection();
                } else if (this.mAdapterHelper.hasPendingUpdates()) {
                    TraceCompat.beginSection(TRACE_ON_DATA_SET_CHANGE_LAYOUT_TAG);
                    dispatchLayout();
                    TraceCompat.endSection();
                }
            }
        }
    }

    private boolean hasUpdatedView() {
        int childCount = this.mChildHelper.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getChildAt(i));
            if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore() && childViewHolderInt.isUpdated()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean scrollByInternal(int i, int i2, MotionEvent motionEvent) {
        boolean z;
        int i3 = i;
        int i4 = i2;
        MotionEvent motionEvent2 = motionEvent;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        consumePendingUpdateOperations();
        if (this.mAdapter != null) {
            eatRequestLayout();
            onEnterLayoutOrScroll();
            TraceCompat.beginSection(TRACE_SCROLL_TAG);
            if (i3 != 0) {
                i7 = this.mLayout.scrollHorizontallyBy(i3, this.mRecycler, this.mState);
                i5 = i3 - i7;
            }
            if (i4 != 0) {
                i8 = this.mLayout.scrollVerticallyBy(i4, this.mRecycler, this.mState);
                i6 = i4 - i8;
            }
            TraceCompat.endSection();
            repositionShadowingViews();
            onExitLayoutOrScroll();
            resumeRequestLayout(false);
        }
        if (!this.mItemDecorations.isEmpty()) {
            invalidate();
        }
        if (dispatchNestedScroll(i7, i8, i5, i6, this.mScrollOffset)) {
            this.mLastTouchX = this.mLastTouchX - this.mScrollOffset[0];
            this.mLastTouchY = this.mLastTouchY - this.mScrollOffset[1];
            if (motionEvent2 != null) {
                motionEvent2.offsetLocation((float) this.mScrollOffset[0], (float) this.mScrollOffset[1]);
            }
            int[] iArr = this.mNestedOffsets;
            iArr[0] = iArr[0] + this.mScrollOffset[0];
            int[] iArr2 = this.mNestedOffsets;
            iArr2[1] = iArr2[1] + this.mScrollOffset[1];
        } else if (ViewCompat.getOverScrollMode(this) != 2) {
            if (motionEvent2 != null) {
                pullGlows(motionEvent2.getX(), (float) i5, motionEvent2.getY(), (float) i6);
            }
            considerReleasingGlowsOnScroll(i3, i4);
        }
        if (!(i7 == 0 && i8 == 0)) {
            dispatchOnScrolled(i7, i8);
        }
        if (!awakenScrollBars()) {
            invalidate();
        }
        if (i7 == 0 && i8 == 0) {
            z = false;
        } else {
            z = true;
        }
        return z;
    }

    public int computeHorizontalScrollOffset() {
        return this.mLayout.canScrollHorizontally() ? this.mLayout.computeHorizontalScrollOffset(this.mState) : 0;
    }

    public int computeHorizontalScrollExtent() {
        return this.mLayout.canScrollHorizontally() ? this.mLayout.computeHorizontalScrollExtent(this.mState) : 0;
    }

    public int computeHorizontalScrollRange() {
        return this.mLayout.canScrollHorizontally() ? this.mLayout.computeHorizontalScrollRange(this.mState) : 0;
    }

    public int computeVerticalScrollOffset() {
        return this.mLayout.canScrollVertically() ? this.mLayout.computeVerticalScrollOffset(this.mState) : 0;
    }

    public int computeVerticalScrollExtent() {
        return this.mLayout.canScrollVertically() ? this.mLayout.computeVerticalScrollExtent(this.mState) : 0;
    }

    public int computeVerticalScrollRange() {
        return this.mLayout.canScrollVertically() ? this.mLayout.computeVerticalScrollRange(this.mState) : 0;
    }

    /* access modifiers changed from: package-private */
    public void eatRequestLayout() {
        if (!this.mEatRequestLayout) {
            this.mEatRequestLayout = true;
            if (!this.mLayoutFrozen) {
                this.mLayoutRequestEaten = false;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void resumeRequestLayout(boolean z) {
        boolean z2 = z;
        if (this.mEatRequestLayout) {
            if (z2 && this.mLayoutRequestEaten && !this.mLayoutFrozen && this.mLayout != null && this.mAdapter != null) {
                dispatchLayout();
            }
            this.mEatRequestLayout = false;
            if (!this.mLayoutFrozen) {
                this.mLayoutRequestEaten = false;
            }
        }
    }

    public void setLayoutFrozen(boolean z) {
        boolean z2 = z;
        if (z2 != this.mLayoutFrozen) {
            assertNotInLayoutOrScroll("Do not setLayoutFrozen in layout or scroll");
            if (!z2) {
                this.mLayoutFrozen = z2;
                if (!(!this.mLayoutRequestEaten || this.mLayout == null || this.mAdapter == null)) {
                    requestLayout();
                }
                this.mLayoutRequestEaten = false;
                return;
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            boolean onTouchEvent = onTouchEvent(MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0));
            this.mLayoutFrozen = z2;
            this.mIgnoreMotionEventTillDown = true;
            stopScroll();
        }
    }

    public boolean isLayoutFrozen() {
        return this.mLayoutFrozen;
    }

    public void smoothScrollBy(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (this.mLayout == null) {
            int e = Log.e(TAG, "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.mLayoutFrozen) {
            if (!this.mLayout.canScrollHorizontally()) {
                i3 = 0;
            }
            if (!this.mLayout.canScrollVertically()) {
                i4 = 0;
            }
            if (i3 != 0 || i4 != 0) {
                this.mViewFlinger.smoothScrollBy(i3, i4);
            }
        }
    }

    public boolean fling(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (this.mLayout == null) {
            int e = Log.e(TAG, "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            return false;
        } else if (this.mLayoutFrozen) {
            return false;
        } else {
            boolean canScrollHorizontally = this.mLayout.canScrollHorizontally();
            boolean canScrollVertically = this.mLayout.canScrollVertically();
            if (!canScrollHorizontally || Math.abs(i3) < this.mMinFlingVelocity) {
                i3 = 0;
            }
            if (!canScrollVertically || Math.abs(i4) < this.mMinFlingVelocity) {
                i4 = 0;
            }
            if (i3 == 0 && i4 == 0) {
                return false;
            }
            if (!dispatchNestedPreFling((float) i3, (float) i4)) {
                boolean z = canScrollHorizontally || canScrollVertically;
                boolean dispatchNestedFling = dispatchNestedFling((float) i3, (float) i4, z);
                if (z) {
                    this.mViewFlinger.fling(Math.max(-this.mMaxFlingVelocity, Math.min(i3, this.mMaxFlingVelocity)), Math.max(-this.mMaxFlingVelocity, Math.min(i4, this.mMaxFlingVelocity)));
                    return true;
                }
            }
            return false;
        }
    }

    public void stopScroll() {
        setScrollState(0);
        stopScrollersInternal();
    }

    private void stopScrollersInternal() {
        this.mViewFlinger.stop();
        if (this.mLayout != null) {
            this.mLayout.stopSmoothScroller();
        }
    }

    public int getMinFlingVelocity() {
        return this.mMinFlingVelocity;
    }

    public int getMaxFlingVelocity() {
        return this.mMaxFlingVelocity;
    }

    private void pullGlows(float f, float f2, float f3, float f4) {
        float f5 = f;
        float f6 = f2;
        float f7 = f3;
        float f8 = f4;
        boolean z = false;
        if (f6 < 0.0f) {
            ensureLeftGlow();
            if (this.mLeftGlow.onPull((-f6) / ((float) getWidth()), 1.0f - (f7 / ((float) getHeight())))) {
                z = true;
            }
        } else if (f6 > 0.0f) {
            ensureRightGlow();
            if (this.mRightGlow.onPull(f6 / ((float) getWidth()), f7 / ((float) getHeight()))) {
                z = true;
            }
        }
        if (f8 < 0.0f) {
            ensureTopGlow();
            if (this.mTopGlow.onPull((-f8) / ((float) getHeight()), f5 / ((float) getWidth()))) {
                z = true;
            }
        } else if (f8 > 0.0f) {
            ensureBottomGlow();
            if (this.mBottomGlow.onPull(f8 / ((float) getHeight()), 1.0f - (f5 / ((float) getWidth())))) {
                z = true;
            }
        }
        if (z || f6 != 0.0f || f8 != 0.0f) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    private void releaseGlows() {
        boolean z = false;
        if (this.mLeftGlow != null) {
            z = this.mLeftGlow.onRelease();
        }
        if (this.mTopGlow != null) {
            z |= this.mTopGlow.onRelease();
        }
        if (this.mRightGlow != null) {
            z |= this.mRightGlow.onRelease();
        }
        if (this.mBottomGlow != null) {
            z |= this.mBottomGlow.onRelease();
        }
        if (z) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: private */
    public void considerReleasingGlowsOnScroll(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        boolean z = false;
        if (this.mLeftGlow != null && !this.mLeftGlow.isFinished() && i3 > 0) {
            z = this.mLeftGlow.onRelease();
        }
        if (this.mRightGlow != null && !this.mRightGlow.isFinished() && i3 < 0) {
            z |= this.mRightGlow.onRelease();
        }
        if (this.mTopGlow != null && !this.mTopGlow.isFinished() && i4 > 0) {
            z |= this.mTopGlow.onRelease();
        }
        if (this.mBottomGlow != null && !this.mBottomGlow.isFinished() && i4 < 0) {
            z |= this.mBottomGlow.onRelease();
        }
        if (z) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void absorbGlows(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (i3 < 0) {
            ensureLeftGlow();
            boolean onAbsorb = this.mLeftGlow.onAbsorb(-i3);
        } else if (i3 > 0) {
            ensureRightGlow();
            boolean onAbsorb2 = this.mRightGlow.onAbsorb(i3);
        }
        if (i4 < 0) {
            ensureTopGlow();
            boolean onAbsorb3 = this.mTopGlow.onAbsorb(-i4);
        } else if (i4 > 0) {
            ensureBottomGlow();
            boolean onAbsorb4 = this.mBottomGlow.onAbsorb(i4);
        }
        if (i3 != 0 || i4 != 0) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void ensureLeftGlow() {
        EdgeEffectCompat edgeEffectCompat;
        if (this.mLeftGlow == null) {
            new EdgeEffectCompat(getContext());
            this.mLeftGlow = edgeEffectCompat;
            if (this.mClipToPadding) {
                this.mLeftGlow.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.mLeftGlow.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ensureRightGlow() {
        EdgeEffectCompat edgeEffectCompat;
        if (this.mRightGlow == null) {
            new EdgeEffectCompat(getContext());
            this.mRightGlow = edgeEffectCompat;
            if (this.mClipToPadding) {
                this.mRightGlow.setSize((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.mRightGlow.setSize(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ensureTopGlow() {
        EdgeEffectCompat edgeEffectCompat;
        if (this.mTopGlow == null) {
            new EdgeEffectCompat(getContext());
            this.mTopGlow = edgeEffectCompat;
            if (this.mClipToPadding) {
                this.mTopGlow.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.mTopGlow.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ensureBottomGlow() {
        EdgeEffectCompat edgeEffectCompat;
        if (this.mBottomGlow == null) {
            new EdgeEffectCompat(getContext());
            this.mBottomGlow = edgeEffectCompat;
            if (this.mClipToPadding) {
                this.mBottomGlow.setSize((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.mBottomGlow.setSize(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void invalidateGlows() {
        this.mBottomGlow = null;
        this.mTopGlow = null;
        this.mRightGlow = null;
        this.mLeftGlow = null;
    }

    public View focusSearch(View view, int i) {
        View view2 = view;
        int i2 = i;
        View onInterceptFocusSearch = this.mLayout.onInterceptFocusSearch(view2, i2);
        if (onInterceptFocusSearch != null) {
            return onInterceptFocusSearch;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, view2, i2);
        if (findNextFocus == null && this.mAdapter != null && this.mLayout != null && !isComputingLayout() && !this.mLayoutFrozen) {
            eatRequestLayout();
            findNextFocus = this.mLayout.onFocusSearchFailed(view2, i2, this.mRecycler, this.mState);
            resumeRequestLayout(false);
        }
        return findNextFocus != null ? findNextFocus : super.focusSearch(view2, i2);
    }

    public void requestChildFocus(View view, View view2) {
        View view3 = view;
        View view4 = view2;
        if (!this.mLayout.onRequestChildFocus(this, this.mState, view3, view4) && view4 != null) {
            this.mTempRect.set(0, 0, view4.getWidth(), view4.getHeight());
            ViewGroup.LayoutParams layoutParams = view4.getLayoutParams();
            if (layoutParams instanceof LayoutParams) {
                LayoutParams layoutParams2 = (LayoutParams) layoutParams;
                if (!layoutParams2.mInsetsDirty) {
                    Rect rect = layoutParams2.mDecorInsets;
                    this.mTempRect.left -= rect.left;
                    this.mTempRect.right += rect.right;
                    this.mTempRect.top -= rect.top;
                    this.mTempRect.bottom += rect.bottom;
                }
            }
            offsetDescendantRectToMyCoords(view4, this.mTempRect);
            offsetRectIntoDescendantCoords(view3, this.mTempRect);
            boolean requestChildRectangleOnScreen = requestChildRectangleOnScreen(view3, this.mTempRect, !this.mFirstLayoutComplete);
        }
        super.requestChildFocus(view3, view4);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        return this.mLayout.requestChildRectangleOnScreen(this, view, rect, z);
    }

    public void addFocusables(ArrayList<View> arrayList, int i, int i2) {
        ArrayList<View> arrayList2 = arrayList;
        int i3 = i;
        int i4 = i2;
        if (this.mLayout == null || !this.mLayout.onAddFocusables(this, arrayList2, i3, i4)) {
            super.addFocusables(arrayList2, i3, i4);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mLayoutOrScrollCounter = 0;
        this.mIsAttached = true;
        this.mFirstLayoutComplete = false;
        if (this.mLayout != null) {
            this.mLayout.dispatchAttachedToWindow(this);
        }
        this.mPostedAnimatorRunner = false;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mItemAnimator != null) {
            this.mItemAnimator.endAnimations();
        }
        this.mFirstLayoutComplete = false;
        stopScroll();
        this.mIsAttached = false;
        if (this.mLayout != null) {
            this.mLayout.dispatchDetachedFromWindow(this, this.mRecycler);
        }
        boolean removeCallbacks = removeCallbacks(this.mItemAnimatorRunner);
        this.mViewInfoStore.onDetach();
    }

    public boolean isAttachedToWindow() {
        return this.mIsAttached;
    }

    /* access modifiers changed from: package-private */
    public void assertInLayoutOrScroll(String str) {
        Throwable th;
        Throwable th2;
        String str2 = str;
        if (isComputingLayout()) {
            return;
        }
        if (str2 == null) {
            Throwable th3 = th2;
            new IllegalStateException("Cannot call this method unless RecyclerView is computing a layout or scrolling");
            throw th3;
        }
        Throwable th4 = th;
        new IllegalStateException(str2);
        throw th4;
    }

    /* access modifiers changed from: package-private */
    public void assertNotInLayoutOrScroll(String str) {
        Throwable th;
        Throwable th2;
        String str2 = str;
        if (!isComputingLayout()) {
            return;
        }
        if (str2 == null) {
            Throwable th3 = th2;
            new IllegalStateException("Cannot call this method while RecyclerView is computing a layout or scrolling");
            throw th3;
        }
        Throwable th4 = th;
        new IllegalStateException(str2);
        throw th4;
    }

    public void addOnItemTouchListener(OnItemTouchListener onItemTouchListener) {
        boolean add = this.mOnItemTouchListeners.add(onItemTouchListener);
    }

    public void removeOnItemTouchListener(OnItemTouchListener onItemTouchListener) {
        OnItemTouchListener onItemTouchListener2 = onItemTouchListener;
        boolean remove = this.mOnItemTouchListeners.remove(onItemTouchListener2);
        if (this.mActiveOnItemTouchListener == onItemTouchListener2) {
            this.mActiveOnItemTouchListener = null;
        }
    }

    private boolean dispatchOnItemTouchIntercept(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int action = motionEvent2.getAction();
        if (action == 3 || action == 0) {
            this.mActiveOnItemTouchListener = null;
        }
        int size = this.mOnItemTouchListeners.size();
        int i = 0;
        while (i < size) {
            OnItemTouchListener onItemTouchListener = this.mOnItemTouchListeners.get(i);
            if (!onItemTouchListener.onInterceptTouchEvent(this, motionEvent2) || action == 3) {
                i++;
            } else {
                this.mActiveOnItemTouchListener = onItemTouchListener;
                return true;
            }
        }
        return false;
    }

    private boolean dispatchOnItemTouch(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int action = motionEvent2.getAction();
        if (this.mActiveOnItemTouchListener != null) {
            if (action == 0) {
                this.mActiveOnItemTouchListener = null;
            } else {
                this.mActiveOnItemTouchListener.onTouchEvent(this, motionEvent2);
                if (action == 3 || action == 1) {
                    this.mActiveOnItemTouchListener = null;
                }
                return true;
            }
        }
        if (action != 0) {
            int size = this.mOnItemTouchListeners.size();
            for (int i = 0; i < size; i++) {
                OnItemTouchListener onItemTouchListener = this.mOnItemTouchListeners.get(i);
                if (onItemTouchListener.onInterceptTouchEvent(this, motionEvent2)) {
                    this.mActiveOnItemTouchListener = onItemTouchListener;
                    return true;
                }
            }
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        StringBuilder sb;
        boolean z;
        MotionEvent motionEvent2 = motionEvent;
        if (this.mLayoutFrozen) {
            return false;
        }
        if (dispatchOnItemTouchIntercept(motionEvent2)) {
            cancelTouch();
            return true;
        } else if (this.mLayout == null) {
            return false;
        } else {
            boolean canScrollHorizontally = this.mLayout.canScrollHorizontally();
            boolean canScrollVertically = this.mLayout.canScrollVertically();
            if (this.mVelocityTracker == null) {
                this.mVelocityTracker = VelocityTracker.obtain();
            }
            this.mVelocityTracker.addMovement(motionEvent2);
            int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
            int actionIndex = MotionEventCompat.getActionIndex(motionEvent2);
            switch (actionMasked) {
                case 0:
                    if (this.mIgnoreMotionEventTillDown) {
                        this.mIgnoreMotionEventTillDown = false;
                    }
                    this.mScrollPointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                    int x = (int) (motionEvent2.getX() + 0.5f);
                    this.mLastTouchX = x;
                    this.mInitialTouchX = x;
                    int y = (int) (motionEvent2.getY() + 0.5f);
                    this.mLastTouchY = y;
                    this.mInitialTouchY = y;
                    if (this.mScrollState == 2) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        setScrollState(1);
                    }
                    int[] iArr = this.mNestedOffsets;
                    this.mNestedOffsets[1] = 0;
                    iArr[0] = 0;
                    int i = 0;
                    if (canScrollHorizontally) {
                        i = 0 | 1;
                    }
                    if (canScrollVertically) {
                        i |= 2;
                    }
                    boolean startNestedScroll = startNestedScroll(i);
                    break;
                case 1:
                    this.mVelocityTracker.clear();
                    stopNestedScroll();
                    break;
                case 2:
                    int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, this.mScrollPointerId);
                    if (findPointerIndex >= 0) {
                        int x2 = (int) (MotionEventCompat.getX(motionEvent2, findPointerIndex) + 0.5f);
                        int y2 = (int) (MotionEventCompat.getY(motionEvent2, findPointerIndex) + 0.5f);
                        if (this.mScrollState != 1) {
                            int i2 = x2 - this.mInitialTouchX;
                            int i3 = y2 - this.mInitialTouchY;
                            boolean z2 = false;
                            if (canScrollHorizontally && Math.abs(i2) > this.mTouchSlop) {
                                this.mLastTouchX = this.mInitialTouchX + (this.mTouchSlop * (i2 < 0 ? -1 : 1));
                                z2 = true;
                            }
                            if (canScrollVertically && Math.abs(i3) > this.mTouchSlop) {
                                this.mLastTouchY = this.mInitialTouchY + (this.mTouchSlop * (i3 < 0 ? -1 : 1));
                                z2 = true;
                            }
                            if (z2) {
                                setScrollState(1);
                                break;
                            }
                        }
                    } else {
                        new StringBuilder();
                        int e = Log.e(TAG, sb.append("Error processing scroll; pointer index for id ").append(this.mScrollPointerId).append(" not found. Did any MotionEvents get skipped?").toString());
                        return false;
                    }
                    break;
                case 3:
                    cancelTouch();
                    break;
                case 5:
                    this.mScrollPointerId = MotionEventCompat.getPointerId(motionEvent2, actionIndex);
                    int x3 = (int) (MotionEventCompat.getX(motionEvent2, actionIndex) + 0.5f);
                    this.mLastTouchX = x3;
                    this.mInitialTouchX = x3;
                    int y3 = (int) (MotionEventCompat.getY(motionEvent2, actionIndex) + 0.5f);
                    this.mLastTouchY = y3;
                    this.mInitialTouchY = y3;
                    break;
                case 6:
                    onPointerUp(motionEvent2);
                    break;
            }
            if (this.mScrollState == 1) {
                z = true;
            } else {
                z = false;
            }
            return z;
        }
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        boolean z2 = z;
        int size = this.mOnItemTouchListeners.size();
        for (int i = 0; i < size; i++) {
            this.mOnItemTouchListeners.get(i).onRequestDisallowInterceptTouchEvent(z2);
        }
        super.requestDisallowInterceptTouchEvent(z2);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i;
        int i2;
        StringBuilder sb;
        MotionEvent motionEvent2 = motionEvent;
        if (this.mLayoutFrozen || this.mIgnoreMotionEventTillDown) {
            return false;
        }
        if (dispatchOnItemTouch(motionEvent2)) {
            cancelTouch();
            return true;
        } else if (this.mLayout == null) {
            return false;
        } else {
            boolean canScrollHorizontally = this.mLayout.canScrollHorizontally();
            boolean canScrollVertically = this.mLayout.canScrollVertically();
            if (this.mVelocityTracker == null) {
                this.mVelocityTracker = VelocityTracker.obtain();
            }
            boolean z = false;
            MotionEvent obtain = MotionEvent.obtain(motionEvent2);
            int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
            int actionIndex = MotionEventCompat.getActionIndex(motionEvent2);
            if (actionMasked == 0) {
                int[] iArr = this.mNestedOffsets;
                this.mNestedOffsets[1] = 0;
                iArr[0] = 0;
            }
            obtain.offsetLocation((float) this.mNestedOffsets[0], (float) this.mNestedOffsets[1]);
            switch (actionMasked) {
                case 0:
                    this.mScrollPointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                    int x = (int) (motionEvent2.getX() + 0.5f);
                    this.mLastTouchX = x;
                    this.mInitialTouchX = x;
                    int y = (int) (motionEvent2.getY() + 0.5f);
                    this.mLastTouchY = y;
                    this.mInitialTouchY = y;
                    int i3 = 0;
                    if (canScrollHorizontally) {
                        i3 = 0 | 1;
                    }
                    if (canScrollVertically) {
                        i3 |= 2;
                    }
                    boolean startNestedScroll = startNestedScroll(i3);
                    break;
                case 1:
                    this.mVelocityTracker.addMovement(obtain);
                    z = true;
                    this.mVelocityTracker.computeCurrentVelocity(1000, (float) this.mMaxFlingVelocity);
                    float f = canScrollHorizontally ? -VelocityTrackerCompat.getXVelocity(this.mVelocityTracker, this.mScrollPointerId) : 0.0f;
                    float f2 = canScrollVertically ? -VelocityTrackerCompat.getYVelocity(this.mVelocityTracker, this.mScrollPointerId) : 0.0f;
                    if ((f == 0.0f && f2 == 0.0f) || !fling((int) f, (int) f2)) {
                        setScrollState(0);
                    }
                    resetTouch();
                    break;
                case 2:
                    int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, this.mScrollPointerId);
                    if (findPointerIndex >= 0) {
                        int x2 = (int) (MotionEventCompat.getX(motionEvent2, findPointerIndex) + 0.5f);
                        int y2 = (int) (MotionEventCompat.getY(motionEvent2, findPointerIndex) + 0.5f);
                        int i4 = this.mLastTouchX - x2;
                        int i5 = this.mLastTouchY - y2;
                        if (dispatchNestedPreScroll(i4, i5, this.mScrollConsumed, this.mScrollOffset)) {
                            i4 -= this.mScrollConsumed[0];
                            i5 -= this.mScrollConsumed[1];
                            obtain.offsetLocation((float) this.mScrollOffset[0], (float) this.mScrollOffset[1]);
                            int[] iArr2 = this.mNestedOffsets;
                            iArr2[0] = iArr2[0] + this.mScrollOffset[0];
                            int[] iArr3 = this.mNestedOffsets;
                            iArr3[1] = iArr3[1] + this.mScrollOffset[1];
                        }
                        if (this.mScrollState != 1) {
                            boolean z2 = false;
                            if (canScrollHorizontally && Math.abs(i2) > this.mTouchSlop) {
                                if (i2 > 0) {
                                    i2 -= this.mTouchSlop;
                                } else {
                                    i2 += this.mTouchSlop;
                                }
                                z2 = true;
                            }
                            if (canScrollVertically && Math.abs(i) > this.mTouchSlop) {
                                if (i > 0) {
                                    i -= this.mTouchSlop;
                                } else {
                                    i += this.mTouchSlop;
                                }
                                z2 = true;
                            }
                            if (z2) {
                                setScrollState(1);
                            }
                        }
                        if (this.mScrollState == 1) {
                            this.mLastTouchX = x2 - this.mScrollOffset[0];
                            this.mLastTouchY = y2 - this.mScrollOffset[1];
                            if (scrollByInternal(canScrollHorizontally ? i2 : 0, canScrollVertically ? i : 0, obtain)) {
                                getParent().requestDisallowInterceptTouchEvent(true);
                                break;
                            }
                        }
                    } else {
                        new StringBuilder();
                        int e = Log.e(TAG, sb.append("Error processing scroll; pointer index for id ").append(this.mScrollPointerId).append(" not found. Did any MotionEvents get skipped?").toString());
                        return false;
                    }
                    break;
                case 3:
                    cancelTouch();
                    break;
                case 5:
                    this.mScrollPointerId = MotionEventCompat.getPointerId(motionEvent2, actionIndex);
                    int x3 = (int) (MotionEventCompat.getX(motionEvent2, actionIndex) + 0.5f);
                    this.mLastTouchX = x3;
                    this.mInitialTouchX = x3;
                    int y3 = (int) (MotionEventCompat.getY(motionEvent2, actionIndex) + 0.5f);
                    this.mLastTouchY = y3;
                    this.mInitialTouchY = y3;
                    break;
                case 6:
                    onPointerUp(motionEvent2);
                    break;
            }
            if (!z) {
                this.mVelocityTracker.addMovement(obtain);
            }
            obtain.recycle();
            return true;
        }
    }

    private void resetTouch() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.clear();
        }
        stopNestedScroll();
        releaseGlows();
    }

    private void cancelTouch() {
        resetTouch();
        setScrollState(0);
    }

    private void onPointerUp(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int actionIndex = MotionEventCompat.getActionIndex(motionEvent2);
        if (MotionEventCompat.getPointerId(motionEvent2, actionIndex) == this.mScrollPointerId) {
            int i = actionIndex == 0 ? 1 : 0;
            this.mScrollPointerId = MotionEventCompat.getPointerId(motionEvent2, i);
            int x = (int) (MotionEventCompat.getX(motionEvent2, i) + 0.5f);
            this.mLastTouchX = x;
            this.mInitialTouchX = x;
            int y = (int) (MotionEventCompat.getY(motionEvent2, i) + 0.5f);
            this.mLastTouchY = y;
            this.mInitialTouchY = y;
        }
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        float f;
        float f2;
        MotionEvent motionEvent2 = motionEvent;
        if (this.mLayout == null) {
            return false;
        }
        if (this.mLayoutFrozen) {
            return false;
        }
        if ((MotionEventCompat.getSource(motionEvent2) & 2) != 0 && motionEvent2.getAction() == 8) {
            if (this.mLayout.canScrollVertically()) {
                f = -MotionEventCompat.getAxisValue(motionEvent2, 9);
            } else {
                f = 0.0f;
            }
            if (this.mLayout.canScrollHorizontally()) {
                f2 = MotionEventCompat.getAxisValue(motionEvent2, 10);
            } else {
                f2 = 0.0f;
            }
            if (!(f == 0.0f && f2 == 0.0f)) {
                float scrollFactor = getScrollFactor();
                boolean scrollByInternal = scrollByInternal((int) (f2 * scrollFactor), (int) (f * scrollFactor), motionEvent2);
            }
        }
        return false;
    }

    private float getScrollFactor() {
        TypedValue typedValue;
        if (this.mScrollFactor == Float.MIN_VALUE) {
            new TypedValue();
            TypedValue typedValue2 = typedValue;
            if (!getContext().getTheme().resolveAttribute(16842829, typedValue2, true)) {
                return 0.0f;
            }
            this.mScrollFactor = typedValue2.getDimension(getContext().getResources().getDisplayMetrics());
        }
        return this.mScrollFactor;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (this.mAdapterUpdateDuringMeasure) {
            eatRequestLayout();
            processAdapterUpdatesAndSetAnimationFlags();
            if (this.mState.mRunPredictiveAnimations) {
                boolean access$2202 = State.access$2202(this.mState, true);
            } else {
                this.mAdapterHelper.consumeUpdatesInOnePass();
                boolean access$22022 = State.access$2202(this.mState, false);
            }
            this.mAdapterUpdateDuringMeasure = false;
            resumeRequestLayout(false);
        }
        if (this.mAdapter != null) {
            this.mState.mItemCount = this.mAdapter.getItemCount();
        } else {
            this.mState.mItemCount = 0;
        }
        if (this.mLayout == null) {
            defaultOnMeasure(i3, i4);
        } else {
            this.mLayout.onMeasure(this.mRecycler, this.mState, i3, i4);
        }
        boolean access$22023 = State.access$2202(this.mState, false);
    }

    /* access modifiers changed from: private */
    public void defaultOnMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5 = i;
        int i6 = i2;
        int mode = View.MeasureSpec.getMode(i5);
        int mode2 = View.MeasureSpec.getMode(i6);
        int size = View.MeasureSpec.getSize(i5);
        int size2 = View.MeasureSpec.getSize(i6);
        switch (mode) {
            case Integer.MIN_VALUE:
            case 1073741824:
                i3 = size;
                break;
            default:
                i3 = ViewCompat.getMinimumWidth(this);
                break;
        }
        switch (mode2) {
            case Integer.MIN_VALUE:
            case 1073741824:
                i4 = size2;
                break;
            default:
                i4 = ViewCompat.getMinimumHeight(this);
                break;
        }
        setMeasuredDimension(i3, i4);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        super.onSizeChanged(i5, i6, i7, i8);
        if (i5 != i7 || i6 != i8) {
            invalidateGlows();
        }
    }

    public void setItemAnimator(ItemAnimator itemAnimator) {
        ItemAnimator itemAnimator2 = itemAnimator;
        if (this.mItemAnimator != null) {
            this.mItemAnimator.endAnimations();
            this.mItemAnimator.setListener(null);
        }
        this.mItemAnimator = itemAnimator2;
        if (this.mItemAnimator != null) {
            this.mItemAnimator.setListener(this.mItemAnimatorListener);
        }
    }

    /* access modifiers changed from: private */
    public void onEnterLayoutOrScroll() {
        this.mLayoutOrScrollCounter = this.mLayoutOrScrollCounter + 1;
    }

    /* access modifiers changed from: private */
    public void onExitLayoutOrScroll() {
        this.mLayoutOrScrollCounter = this.mLayoutOrScrollCounter - 1;
        if (this.mLayoutOrScrollCounter < 1) {
            this.mLayoutOrScrollCounter = 0;
            dispatchContentChangedIfNecessary();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isAccessibilityEnabled() {
        return this.mAccessibilityManager != null && this.mAccessibilityManager.isEnabled();
    }

    private void dispatchContentChangedIfNecessary() {
        int i = this.mEatenAccessibilityChangeFlags;
        this.mEatenAccessibilityChangeFlags = 0;
        if (i != 0 && isAccessibilityEnabled()) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain();
            obtain.setEventType(2048);
            AccessibilityEventCompat.setContentChangeTypes(obtain, i);
            sendAccessibilityEventUnchecked(obtain);
        }
    }

    public boolean isComputingLayout() {
        return this.mLayoutOrScrollCounter > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean shouldDeferAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
        if (!isComputingLayout()) {
            return false;
        }
        int i = 0;
        if (accessibilityEvent2 != null) {
            i = AccessibilityEventCompat.getContentChangeTypes(accessibilityEvent2);
        }
        if (i == 0) {
            i = 0;
        }
        this.mEatenAccessibilityChangeFlags = this.mEatenAccessibilityChangeFlags | i;
        return true;
    }

    public void sendAccessibilityEventUnchecked(AccessibilityEvent accessibilityEvent) {
        AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
        if (!shouldDeferAccessibilityEvent(accessibilityEvent2)) {
            super.sendAccessibilityEventUnchecked(accessibilityEvent2);
        }
    }

    public ItemAnimator getItemAnimator() {
        return this.mItemAnimator;
    }

    /* access modifiers changed from: private */
    public void postAnimationRunner() {
        if (!this.mPostedAnimatorRunner && this.mIsAttached) {
            ViewCompat.postOnAnimation(this, this.mItemAnimatorRunner);
            this.mPostedAnimatorRunner = true;
        }
    }

    private boolean predictiveItemAnimationsEnabled() {
        return this.mItemAnimator != null && this.mLayout.supportsPredictiveItemAnimations();
    }

    private void processAdapterUpdatesAndSetAnimationFlags() {
        if (this.mDataSetHasChangedAfterLayout) {
            this.mAdapterHelper.reset();
            markKnownViewsInvalid();
            this.mLayout.onItemsChanged(this);
        }
        if (this.mItemAnimator == null || !this.mLayout.supportsPredictiveItemAnimations()) {
            this.mAdapterHelper.consumeUpdatesInOnePass();
        } else {
            this.mAdapterHelper.preProcess();
        }
        boolean z = this.mItemsAddedOrRemoved || this.mItemsChanged;
        boolean access$2302 = State.access$2302(this.mState, this.mFirstLayoutComplete && this.mItemAnimator != null && (this.mDataSetHasChangedAfterLayout || z || this.mLayout.mRequestedSimpleAnimations) && (!this.mDataSetHasChangedAfterLayout || this.mAdapter.hasStableIds()));
        boolean access$2102 = State.access$2102(this.mState, this.mState.mRunSimpleAnimations && z && !this.mDataSetHasChangedAfterLayout && predictiveItemAnimationsEnabled());
    }

    /* access modifiers changed from: package-private */
    public void dispatchLayout() {
        if (this.mAdapter == null) {
            int e = Log.e(TAG, "No adapter attached; skipping layout");
        } else if (this.mLayout == null) {
            int e2 = Log.e(TAG, "No layout manager attached; skipping layout");
        } else {
            this.mViewInfoStore.clear();
            eatRequestLayout();
            onEnterLayoutOrScroll();
            processAdapterUpdatesAndSetAnimationFlags();
            boolean access$2502 = State.access$2502(this.mState, this.mState.mRunSimpleAnimations && this.mItemsChanged);
            this.mItemsChanged = false;
            this.mItemsAddedOrRemoved = false;
            boolean access$2202 = State.access$2202(this.mState, this.mState.mRunPredictiveAnimations);
            this.mState.mItemCount = this.mAdapter.getItemCount();
            findMinMaxChildLayoutPositions(this.mMinMaxLayoutPositions);
            if (this.mState.mRunSimpleAnimations) {
                int childCount = this.mChildHelper.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getChildAt(i));
                    if (!childViewHolderInt.shouldIgnore() && (!childViewHolderInt.isInvalid() || this.mAdapter.hasStableIds())) {
                        this.mViewInfoStore.addToPreLayout(childViewHolderInt, this.mItemAnimator.recordPreLayoutInformation(this.mState, childViewHolderInt, ItemAnimator.buildAdapterChangeFlagsForAnimations(childViewHolderInt), childViewHolderInt.getUnmodifiedPayloads()));
                        if (this.mState.mTrackOldChangeHolders && childViewHolderInt.isUpdated() && !childViewHolderInt.isRemoved() && !childViewHolderInt.shouldIgnore() && !childViewHolderInt.isInvalid()) {
                            this.mViewInfoStore.addToOldChangeHolders(getChangedHolderKey(childViewHolderInt), childViewHolderInt);
                        }
                    }
                }
            }
            if (this.mState.mRunPredictiveAnimations) {
                saveOldPositions();
                boolean access$1900 = this.mState.mStructureChanged;
                boolean access$1902 = State.access$1902(this.mState, false);
                this.mLayout.onLayoutChildren(this.mRecycler, this.mState);
                boolean access$19022 = State.access$1902(this.mState, access$1900);
                for (int i2 = 0; i2 < this.mChildHelper.getChildCount(); i2++) {
                    ViewHolder childViewHolderInt2 = getChildViewHolderInt(this.mChildHelper.getChildAt(i2));
                    if (!childViewHolderInt2.shouldIgnore() && !this.mViewInfoStore.isInPreLayout(childViewHolderInt2)) {
                        int buildAdapterChangeFlagsForAnimations = ItemAnimator.buildAdapterChangeFlagsForAnimations(childViewHolderInt2);
                        boolean hasAnyOfTheFlags = childViewHolderInt2.hasAnyOfTheFlags(8192);
                        if (!hasAnyOfTheFlags) {
                            buildAdapterChangeFlagsForAnimations |= 4096;
                        }
                        ItemAnimator.ItemHolderInfo recordPreLayoutInformation = this.mItemAnimator.recordPreLayoutInformation(this.mState, childViewHolderInt2, buildAdapterChangeFlagsForAnimations, childViewHolderInt2.getUnmodifiedPayloads());
                        if (hasAnyOfTheFlags) {
                            recordAnimationInfoIfBouncedHiddenView(childViewHolderInt2, recordPreLayoutInformation);
                        } else {
                            this.mViewInfoStore.addToAppearedInPreLayoutHolders(childViewHolderInt2, recordPreLayoutInformation);
                        }
                    }
                }
                clearOldPositions();
                this.mAdapterHelper.consumePostponedUpdates();
            } else {
                clearOldPositions();
            }
            this.mState.mItemCount = this.mAdapter.getItemCount();
            int access$1802 = State.access$1802(this.mState, 0);
            boolean access$22022 = State.access$2202(this.mState, false);
            this.mLayout.onLayoutChildren(this.mRecycler, this.mState);
            boolean access$19023 = State.access$1902(this.mState, false);
            this.mPendingSavedState = null;
            boolean access$2302 = State.access$2302(this.mState, this.mState.mRunSimpleAnimations && this.mItemAnimator != null);
            if (this.mState.mRunSimpleAnimations) {
                int childCount2 = this.mChildHelper.getChildCount();
                for (int i3 = 0; i3 < childCount2; i3++) {
                    ViewHolder childViewHolderInt3 = getChildViewHolderInt(this.mChildHelper.getChildAt(i3));
                    if (!childViewHolderInt3.shouldIgnore()) {
                        long changedHolderKey = getChangedHolderKey(childViewHolderInt3);
                        ItemAnimator.ItemHolderInfo recordPostLayoutInformation = this.mItemAnimator.recordPostLayoutInformation(this.mState, childViewHolderInt3);
                        ViewHolder fromOldChangeHolders = this.mViewInfoStore.getFromOldChangeHolders(changedHolderKey);
                        if (fromOldChangeHolders == null || fromOldChangeHolders.shouldIgnore()) {
                            this.mViewInfoStore.addToPostLayout(childViewHolderInt3, recordPostLayoutInformation);
                        } else {
                            animateChange(fromOldChangeHolders, childViewHolderInt3, this.mViewInfoStore.popFromPreLayout(fromOldChangeHolders), recordPostLayoutInformation);
                        }
                    }
                }
                this.mViewInfoStore.process(this.mViewInfoProcessCallback);
            }
            resumeRequestLayout(false);
            this.mLayout.removeAndRecycleScrapInt(this.mRecycler);
            int access$2602 = State.access$2602(this.mState, this.mState.mItemCount);
            this.mDataSetHasChangedAfterLayout = false;
            boolean access$23022 = State.access$2302(this.mState, false);
            boolean access$2102 = State.access$2102(this.mState, false);
            onExitLayoutOrScroll();
            boolean access$2402 = LayoutManager.access$2402(this.mLayout, false);
            if (this.mRecycler.mChangedScrap != null) {
                this.mRecycler.mChangedScrap.clear();
            }
            this.mViewInfoStore.clear();
            if (didChildRangeChange(this.mMinMaxLayoutPositions[0], this.mMinMaxLayoutPositions[1])) {
                dispatchOnScrolled(0, 0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void recordAnimationInfoIfBouncedHiddenView(ViewHolder viewHolder, ItemAnimator.ItemHolderInfo itemHolderInfo) {
        ViewHolder viewHolder2 = viewHolder;
        ItemAnimator.ItemHolderInfo itemHolderInfo2 = itemHolderInfo;
        viewHolder2.setFlags(0, 8192);
        if (this.mState.mTrackOldChangeHolders && viewHolder2.isUpdated() && !viewHolder2.isRemoved() && !viewHolder2.shouldIgnore()) {
            this.mViewInfoStore.addToOldChangeHolders(getChangedHolderKey(viewHolder2), viewHolder2);
        }
        this.mViewInfoStore.addToPreLayout(viewHolder2, itemHolderInfo2);
    }

    private void findMinMaxChildLayoutPositions(int[] iArr) {
        int[] iArr2 = iArr;
        int childCount = this.mChildHelper.getChildCount();
        if (childCount == 0) {
            iArr2[0] = 0;
            iArr2[1] = 0;
            return;
        }
        int i = Integer.MAX_VALUE;
        int i2 = Integer.MIN_VALUE;
        for (int i3 = 0; i3 < childCount; i3++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getChildAt(i3));
            if (!childViewHolderInt.shouldIgnore()) {
                int layoutPosition = childViewHolderInt.getLayoutPosition();
                if (layoutPosition < i) {
                    i = layoutPosition;
                }
                if (layoutPosition > i2) {
                    i2 = layoutPosition;
                }
            }
        }
        iArr2[0] = i;
        iArr2[1] = i2;
    }

    private boolean didChildRangeChange(int i, int i2) {
        int layoutPosition;
        boolean z;
        int i3 = i;
        int i4 = i2;
        int childCount = this.mChildHelper.getChildCount();
        if (childCount == 0) {
            if (i3 == 0 && i4 == 0) {
                z = false;
            } else {
                z = true;
            }
            return z;
        }
        for (int i5 = 0; i5 < childCount; i5++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getChildAt(i5));
            if (!childViewHolderInt.shouldIgnore() && ((layoutPosition = childViewHolderInt.getLayoutPosition()) < i3 || layoutPosition > i4)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void removeDetachedView(View view, boolean z) {
        Throwable th;
        StringBuilder sb;
        View view2 = view;
        boolean z2 = z;
        ViewHolder childViewHolderInt = getChildViewHolderInt(view2);
        if (childViewHolderInt != null) {
            if (childViewHolderInt.isTmpDetached()) {
                childViewHolderInt.clearTmpDetachFlag();
            } else if (!childViewHolderInt.shouldIgnore()) {
                Throwable th2 = th;
                new StringBuilder();
                new IllegalArgumentException(sb.append("Called removeDetachedView with a view which is not flagged as tmp detached.").append(childViewHolderInt).toString());
                throw th2;
            }
        }
        dispatchChildDetached(view2);
        super.removeDetachedView(view2, z2);
    }

    /* access modifiers changed from: package-private */
    public long getChangedHolderKey(ViewHolder viewHolder) {
        ViewHolder viewHolder2 = viewHolder;
        return this.mAdapter.hasStableIds() ? viewHolder2.getItemId() : (long) viewHolder2.mPosition;
    }

    /* access modifiers changed from: private */
    public void animateAppearance(@NonNull ViewHolder viewHolder, @Nullable ItemAnimator.ItemHolderInfo itemHolderInfo, @NonNull ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        ViewHolder viewHolder2 = viewHolder;
        viewHolder2.setIsRecyclable(false);
        if (this.mItemAnimator.animateAppearance(viewHolder2, itemHolderInfo, itemHolderInfo2)) {
            postAnimationRunner();
        }
    }

    /* access modifiers changed from: private */
    public void animateDisappearance(@NonNull ViewHolder viewHolder, @NonNull ItemAnimator.ItemHolderInfo itemHolderInfo, @Nullable ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        ViewHolder viewHolder2 = viewHolder;
        addAnimatingView(viewHolder2);
        viewHolder2.setIsRecyclable(false);
        if (this.mItemAnimator.animateDisappearance(viewHolder2, itemHolderInfo, itemHolderInfo2)) {
            postAnimationRunner();
        }
    }

    private void animateChange(@NonNull ViewHolder viewHolder, @NonNull ViewHolder viewHolder2, @NonNull ItemAnimator.ItemHolderInfo itemHolderInfo, @NonNull ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        ViewHolder viewHolder3 = viewHolder;
        ViewHolder viewHolder4 = viewHolder2;
        ItemAnimator.ItemHolderInfo itemHolderInfo3 = itemHolderInfo;
        ItemAnimator.ItemHolderInfo itemHolderInfo4 = itemHolderInfo2;
        viewHolder3.setIsRecyclable(false);
        if (viewHolder3 != viewHolder4) {
            viewHolder3.mShadowedHolder = viewHolder4;
            addAnimatingView(viewHolder3);
            this.mRecycler.unscrapView(viewHolder3);
            viewHolder4.setIsRecyclable(false);
            viewHolder4.mShadowingHolder = viewHolder3;
        }
        if (this.mItemAnimator.animateChange(viewHolder3, viewHolder4, itemHolderInfo3, itemHolderInfo4)) {
            postAnimationRunner();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        eatRequestLayout();
        TraceCompat.beginSection(TRACE_ON_LAYOUT_TAG);
        dispatchLayout();
        TraceCompat.endSection();
        resumeRequestLayout(false);
        this.mFirstLayoutComplete = true;
    }

    public void requestLayout() {
        if (this.mEatRequestLayout || this.mLayoutFrozen) {
            this.mLayoutRequestEaten = true;
        } else {
            super.requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void markItemDecorInsetsDirty() {
        int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
        for (int i = 0; i < unfilteredChildCount; i++) {
            ((LayoutParams) this.mChildHelper.getUnfilteredChildAt(i).getLayoutParams()).mInsetsDirty = true;
        }
        this.mRecycler.markItemDecorInsetsDirty();
    }

    public void draw(Canvas canvas) {
        Canvas canvas2 = canvas;
        super.draw(canvas2);
        int size = this.mItemDecorations.size();
        for (int i = 0; i < size; i++) {
            this.mItemDecorations.get(i).onDrawOver(canvas2, this, this.mState);
        }
        boolean z = false;
        if (this.mLeftGlow != null && !this.mLeftGlow.isFinished()) {
            int save = canvas2.save();
            int paddingBottom = this.mClipToPadding ? getPaddingBottom() : 0;
            canvas2.rotate(270.0f);
            canvas2.translate((float) ((-getHeight()) + paddingBottom), 0.0f);
            z = this.mLeftGlow != null && this.mLeftGlow.draw(canvas2);
            canvas2.restoreToCount(save);
        }
        if (this.mTopGlow != null && !this.mTopGlow.isFinished()) {
            int save2 = canvas2.save();
            if (this.mClipToPadding) {
                canvas2.translate((float) getPaddingLeft(), (float) getPaddingTop());
            }
            z |= this.mTopGlow != null && this.mTopGlow.draw(canvas2);
            canvas2.restoreToCount(save2);
        }
        if (this.mRightGlow != null && !this.mRightGlow.isFinished()) {
            int save3 = canvas2.save();
            int width = getWidth();
            int paddingTop = this.mClipToPadding ? getPaddingTop() : 0;
            canvas2.rotate(90.0f);
            canvas2.translate((float) (-paddingTop), (float) (-width));
            z |= this.mRightGlow != null && this.mRightGlow.draw(canvas2);
            canvas2.restoreToCount(save3);
        }
        if (this.mBottomGlow != null && !this.mBottomGlow.isFinished()) {
            int save4 = canvas2.save();
            canvas2.rotate(180.0f);
            if (this.mClipToPadding) {
                canvas2.translate((float) ((-getWidth()) + getPaddingRight()), (float) ((-getHeight()) + getPaddingBottom()));
            } else {
                canvas2.translate((float) (-getWidth()), (float) (-getHeight()));
            }
            z |= this.mBottomGlow != null && this.mBottomGlow.draw(canvas2);
            canvas2.restoreToCount(save4);
        }
        if (!z && this.mItemAnimator != null && this.mItemDecorations.size() > 0 && this.mItemAnimator.isRunning()) {
            z = true;
        }
        if (z) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    public void onDraw(Canvas canvas) {
        Canvas canvas2 = canvas;
        super.onDraw(canvas2);
        int size = this.mItemDecorations.size();
        for (int i = 0; i < size; i++) {
            this.mItemDecorations.get(i).onDraw(canvas2, this, this.mState);
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        return (layoutParams2 instanceof LayoutParams) && this.mLayout.checkLayoutParams((LayoutParams) layoutParams2);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        Throwable th;
        if (this.mLayout != null) {
            return this.mLayout.generateDefaultLayoutParams();
        }
        Throwable th2 = th;
        new IllegalStateException("RecyclerView has no LayoutManager");
        throw th2;
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        Throwable th;
        AttributeSet attributeSet2 = attributeSet;
        if (this.mLayout != null) {
            return this.mLayout.generateLayoutParams(getContext(), attributeSet2);
        }
        Throwable th2 = th;
        new IllegalStateException("RecyclerView has no LayoutManager");
        throw th2;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        Throwable th;
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        if (this.mLayout != null) {
            return this.mLayout.generateLayoutParams(layoutParams2);
        }
        Throwable th2 = th;
        new IllegalStateException("RecyclerView has no LayoutManager");
        throw th2;
    }

    public boolean isAnimating() {
        return this.mItemAnimator != null && this.mItemAnimator.isRunning();
    }

    /* access modifiers changed from: package-private */
    public void saveOldPositions() {
        int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
        for (int i = 0; i < unfilteredChildCount; i++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(i));
            if (!childViewHolderInt.shouldIgnore()) {
                childViewHolderInt.saveOldPosition();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void clearOldPositions() {
        int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
        for (int i = 0; i < unfilteredChildCount; i++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(i));
            if (!childViewHolderInt.shouldIgnore()) {
                childViewHolderInt.clearOldPosition();
            }
        }
        this.mRecycler.clearOldPositions();
    }

    /* access modifiers changed from: package-private */
    public void offsetPositionRecordsForMove(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6 = i;
        int i7 = i2;
        int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
        if (i6 < i7) {
            i3 = i6;
            i4 = i7;
            i5 = -1;
        } else {
            i3 = i7;
            i4 = i6;
            i5 = 1;
        }
        for (int i8 = 0; i8 < unfilteredChildCount; i8++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(i8));
            if (childViewHolderInt != null && childViewHolderInt.mPosition >= i3 && childViewHolderInt.mPosition <= i4) {
                if (childViewHolderInt.mPosition == i6) {
                    childViewHolderInt.offsetPosition(i7 - i6, false);
                } else {
                    childViewHolderInt.offsetPosition(i5, false);
                }
                boolean access$1902 = State.access$1902(this.mState, true);
            }
        }
        this.mRecycler.offsetPositionRecordsForMove(i6, i7);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void offsetPositionRecordsForInsert(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
        for (int i5 = 0; i5 < unfilteredChildCount; i5++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(i5));
            if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore() && childViewHolderInt.mPosition >= i3) {
                childViewHolderInt.offsetPosition(i4, false);
                boolean access$1902 = State.access$1902(this.mState, true);
            }
        }
        this.mRecycler.offsetPositionRecordsForInsert(i3, i4);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void offsetPositionRecordsForRemove(int i, int i2, boolean z) {
        int i3 = i;
        int i4 = i2;
        boolean z2 = z;
        int i5 = i3 + i4;
        int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
        for (int i6 = 0; i6 < unfilteredChildCount; i6++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(i6));
            if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore()) {
                if (childViewHolderInt.mPosition >= i5) {
                    childViewHolderInt.offsetPosition(-i4, z2);
                    boolean access$1902 = State.access$1902(this.mState, true);
                } else if (childViewHolderInt.mPosition >= i3) {
                    childViewHolderInt.flagRemovedAndOffsetPosition(i3 - 1, -i4, z2);
                    boolean access$19022 = State.access$1902(this.mState, true);
                }
            }
        }
        this.mRecycler.offsetPositionRecordsForRemove(i3, i4, z2);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void viewRangeUpdate(int i, int i2, Object obj) {
        int i3 = i;
        int i4 = i2;
        Object obj2 = obj;
        int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
        int i5 = i3 + i4;
        for (int i6 = 0; i6 < unfilteredChildCount; i6++) {
            View unfilteredChildAt = this.mChildHelper.getUnfilteredChildAt(i6);
            ViewHolder childViewHolderInt = getChildViewHolderInt(unfilteredChildAt);
            if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore() && childViewHolderInt.mPosition >= i3 && childViewHolderInt.mPosition < i5) {
                childViewHolderInt.addFlags(2);
                childViewHolderInt.addChangePayload(obj2);
                ((LayoutParams) unfilteredChildAt.getLayoutParams()).mInsetsDirty = true;
            }
        }
        this.mRecycler.viewRangeUpdate(i3, i4);
    }

    /* access modifiers changed from: private */
    public boolean canReuseUpdatedViewHolder(ViewHolder viewHolder) {
        return this.mItemAnimator == null || this.mItemAnimator.canReuseUpdatedViewHolder(viewHolder);
    }

    /* access modifiers changed from: private */
    public void setDataSetChangedAfterLayout() {
        if (!this.mDataSetHasChangedAfterLayout) {
            this.mDataSetHasChangedAfterLayout = true;
            int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
            for (int i = 0; i < unfilteredChildCount; i++) {
                ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(i));
                if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore()) {
                    childViewHolderInt.addFlags(512);
                }
            }
            this.mRecycler.setAdapterPositionsAsUnknown();
        }
    }

    /* access modifiers changed from: package-private */
    public void markKnownViewsInvalid() {
        int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
        for (int i = 0; i < unfilteredChildCount; i++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(i));
            if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore()) {
                childViewHolderInt.addFlags(6);
            }
        }
        markItemDecorInsetsDirty();
        this.mRecycler.markKnownViewsInvalid();
    }

    public void invalidateItemDecorations() {
        if (this.mItemDecorations.size() != 0) {
            if (this.mLayout != null) {
                this.mLayout.assertNotInLayoutOrScroll("Cannot invalidate item decorations during a scroll or layout");
            }
            markItemDecorInsetsDirty();
            requestLayout();
        }
    }

    public ViewHolder getChildViewHolder(View view) {
        Throwable th;
        StringBuilder sb;
        View view2 = view;
        ViewParent parent = view2.getParent();
        if (parent == null || parent == this) {
            return getChildViewHolderInt(view2);
        }
        Throwable th2 = th;
        new StringBuilder();
        new IllegalArgumentException(sb.append("View ").append(view2).append(" is not a direct child of ").append(this).toString());
        throw th2;
    }

    static ViewHolder getChildViewHolderInt(View view) {
        View view2 = view;
        if (view2 == null) {
            return null;
        }
        return ((LayoutParams) view2.getLayoutParams()).mViewHolder;
    }

    @Deprecated
    public int getChildPosition(View view) {
        return getChildAdapterPosition(view);
    }

    public int getChildAdapterPosition(View view) {
        ViewHolder childViewHolderInt = getChildViewHolderInt(view);
        return childViewHolderInt != null ? childViewHolderInt.getAdapterPosition() : -1;
    }

    public int getChildLayoutPosition(View view) {
        ViewHolder childViewHolderInt = getChildViewHolderInt(view);
        return childViewHolderInt != null ? childViewHolderInt.getLayoutPosition() : -1;
    }

    public long getChildItemId(View view) {
        View view2 = view;
        if (this.mAdapter == null || !this.mAdapter.hasStableIds()) {
            return -1;
        }
        ViewHolder childViewHolderInt = getChildViewHolderInt(view2);
        return childViewHolderInt != null ? childViewHolderInt.getItemId() : -1;
    }

    @Deprecated
    public ViewHolder findViewHolderForPosition(int i) {
        return findViewHolderForPosition(i, false);
    }

    public ViewHolder findViewHolderForLayoutPosition(int i) {
        return findViewHolderForPosition(i, false);
    }

    public ViewHolder findViewHolderForAdapterPosition(int i) {
        int i2 = i;
        if (this.mDataSetHasChangedAfterLayout) {
            return null;
        }
        int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
        for (int i3 = 0; i3 < unfilteredChildCount; i3++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(i3));
            if (childViewHolderInt != null && !childViewHolderInt.isRemoved() && getAdapterPositionFor(childViewHolderInt) == i2) {
                return childViewHolderInt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public ViewHolder findViewHolderForPosition(int i, boolean z) {
        int i2 = i;
        boolean z2 = z;
        int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
        for (int i3 = 0; i3 < unfilteredChildCount; i3++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(i3));
            if (childViewHolderInt != null && !childViewHolderInt.isRemoved()) {
                if (z2) {
                    if (childViewHolderInt.mPosition == i2) {
                        return childViewHolderInt;
                    }
                } else if (childViewHolderInt.getLayoutPosition() == i2) {
                    return childViewHolderInt;
                }
            }
        }
        return null;
    }

    public ViewHolder findViewHolderForItemId(long j) {
        long j2 = j;
        int unfilteredChildCount = this.mChildHelper.getUnfilteredChildCount();
        for (int i = 0; i < unfilteredChildCount; i++) {
            ViewHolder childViewHolderInt = getChildViewHolderInt(this.mChildHelper.getUnfilteredChildAt(i));
            if (childViewHolderInt != null && childViewHolderInt.getItemId() == j2) {
                return childViewHolderInt;
            }
        }
        return null;
    }

    public View findChildViewUnder(float f, float f2) {
        float f3 = f;
        float f4 = f2;
        for (int childCount = this.mChildHelper.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = this.mChildHelper.getChildAt(childCount);
            float translationX = ViewCompat.getTranslationX(childAt);
            float translationY = ViewCompat.getTranslationY(childAt);
            if (f3 >= ((float) childAt.getLeft()) + translationX && f3 <= ((float) childAt.getRight()) + translationX && f4 >= ((float) childAt.getTop()) + translationY && f4 <= ((float) childAt.getBottom()) + translationY) {
                return childAt;
            }
        }
        return null;
    }

    public boolean drawChild(Canvas canvas, View view, long j) {
        return super.drawChild(canvas, view, j);
    }

    public void offsetChildrenVertical(int i) {
        int i2 = i;
        int childCount = this.mChildHelper.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            this.mChildHelper.getChildAt(i3).offsetTopAndBottom(i2);
        }
    }

    public void onChildAttachedToWindow(View view) {
    }

    public void onChildDetachedFromWindow(View view) {
    }

    public void offsetChildrenHorizontal(int i) {
        int i2 = i;
        int childCount = this.mChildHelper.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            this.mChildHelper.getChildAt(i3).offsetLeftAndRight(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public Rect getItemDecorInsetsForChild(View view) {
        View view2 = view;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        if (!layoutParams.mInsetsDirty) {
            return layoutParams.mDecorInsets;
        }
        Rect rect = layoutParams.mDecorInsets;
        rect.set(0, 0, 0, 0);
        int size = this.mItemDecorations.size();
        for (int i = 0; i < size; i++) {
            this.mTempRect.set(0, 0, 0, 0);
            this.mItemDecorations.get(i).getItemOffsets(this.mTempRect, view2, this, this.mState);
            rect.left += this.mTempRect.left;
            rect.top += this.mTempRect.top;
            rect.right += this.mTempRect.right;
            rect.bottom += this.mTempRect.bottom;
        }
        layoutParams.mInsetsDirty = false;
        return rect;
    }

    public void onScrolled(int i, int i2) {
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnScrolled(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        onScrollChanged(scrollX, scrollY, scrollX, scrollY);
        onScrolled(i3, i4);
        if (this.mScrollListener != null) {
            this.mScrollListener.onScrolled(this, i3, i4);
        }
        if (this.mScrollListeners != null) {
            for (int size = this.mScrollListeners.size() - 1; size >= 0; size--) {
                this.mScrollListeners.get(size).onScrolled(this, i3, i4);
            }
        }
    }

    public void onScrollStateChanged(int i) {
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnScrollStateChanged(int i) {
        int i2 = i;
        if (this.mLayout != null) {
            this.mLayout.onScrollStateChanged(i2);
        }
        onScrollStateChanged(i2);
        if (this.mScrollListener != null) {
            this.mScrollListener.onScrollStateChanged(this, i2);
        }
        if (this.mScrollListeners != null) {
            for (int size = this.mScrollListeners.size() - 1; size >= 0; size--) {
                this.mScrollListeners.get(size).onScrollStateChanged(this, i2);
            }
        }
    }

    public boolean hasPendingAdapterUpdates() {
        return !this.mFirstLayoutComplete || this.mDataSetHasChangedAfterLayout || this.mAdapterHelper.hasPendingUpdates();
    }

    private class ViewFlinger implements Runnable {
        private boolean mEatRunOnAnimationRequest = false;
        private Interpolator mInterpolator = RecyclerView.sQuinticInterpolator;
        private int mLastFlingX;
        private int mLastFlingY;
        private boolean mReSchedulePostAnimationCallback = false;
        private ScrollerCompat mScroller;
        final /* synthetic */ RecyclerView this$0;

        public ViewFlinger(RecyclerView recyclerView) {
            RecyclerView recyclerView2 = recyclerView;
            this.this$0 = recyclerView2;
            this.mScroller = ScrollerCompat.create(recyclerView2.getContext(), RecyclerView.sQuinticInterpolator);
        }

        public void run() {
            int i;
            disableRunOnAnimationRequests();
            this.this$0.consumePendingUpdateOperations();
            ScrollerCompat scrollerCompat = this.mScroller;
            SmoothScroller smoothScroller = this.this$0.mLayout.mSmoothScroller;
            if (scrollerCompat.computeScrollOffset()) {
                int currX = scrollerCompat.getCurrX();
                int currY = scrollerCompat.getCurrY();
                int i2 = currX - this.mLastFlingX;
                int i3 = currY - this.mLastFlingY;
                int i4 = 0;
                int i5 = 0;
                this.mLastFlingX = currX;
                this.mLastFlingY = currY;
                int i6 = 0;
                int i7 = 0;
                if (this.this$0.mAdapter != null) {
                    this.this$0.eatRequestLayout();
                    this.this$0.onEnterLayoutOrScroll();
                    TraceCompat.beginSection(RecyclerView.TRACE_SCROLL_TAG);
                    if (i2 != 0) {
                        i4 = this.this$0.mLayout.scrollHorizontallyBy(i2, this.this$0.mRecycler, this.this$0.mState);
                        i6 = i2 - i4;
                    }
                    if (i3 != 0) {
                        i5 = this.this$0.mLayout.scrollVerticallyBy(i3, this.this$0.mRecycler, this.this$0.mState);
                        i7 = i3 - i5;
                    }
                    TraceCompat.endSection();
                    this.this$0.repositionShadowingViews();
                    this.this$0.onExitLayoutOrScroll();
                    this.this$0.resumeRequestLayout(false);
                    if (smoothScroller != null && !smoothScroller.isPendingInitialRun() && smoothScroller.isRunning()) {
                        int itemCount = this.this$0.mState.getItemCount();
                        if (itemCount == 0) {
                            smoothScroller.stop();
                        } else if (smoothScroller.getTargetPosition() >= itemCount) {
                            smoothScroller.setTargetPosition(itemCount - 1);
                            smoothScroller.onAnimation(i2 - i6, i3 - i7);
                        } else {
                            smoothScroller.onAnimation(i2 - i6, i3 - i7);
                        }
                    }
                }
                if (!this.this$0.mItemDecorations.isEmpty()) {
                    this.this$0.invalidate();
                }
                if (ViewCompat.getOverScrollMode(this.this$0) != 2) {
                    this.this$0.considerReleasingGlowsOnScroll(i2, i3);
                }
                if (!(i6 == 0 && i7 == 0)) {
                    int currVelocity = (int) scrollerCompat.getCurrVelocity();
                    int i8 = 0;
                    if (i6 != currX) {
                        i8 = i6 < 0 ? -currVelocity : i6 > 0 ? currVelocity : 0;
                    }
                    int i9 = 0;
                    if (i7 != currY) {
                        if (i7 < 0) {
                            i = -currVelocity;
                        } else {
                            i = i7 > 0 ? currVelocity : 0;
                        }
                        i9 = i;
                    }
                    if (ViewCompat.getOverScrollMode(this.this$0) != 2) {
                        this.this$0.absorbGlows(i8, i9);
                    }
                    if ((i8 != 0 || i6 == currX || scrollerCompat.getFinalX() == 0) && (i9 != 0 || i7 == currY || scrollerCompat.getFinalY() == 0)) {
                        scrollerCompat.abortAnimation();
                    }
                }
                if (!(i4 == 0 && i5 == 0)) {
                    this.this$0.dispatchOnScrolled(i4, i5);
                }
                if (!this.this$0.awakenScrollBars()) {
                    this.this$0.invalidate();
                }
                boolean z = (i2 == 0 && i3 == 0) || (i2 != 0 && this.this$0.mLayout.canScrollHorizontally() && i4 == i2) || (i3 != 0 && this.this$0.mLayout.canScrollVertically() && i5 == i3);
                if (scrollerCompat.isFinished() || !z) {
                    this.this$0.setScrollState(0);
                } else {
                    postOnAnimation();
                }
            }
            if (smoothScroller != null) {
                if (smoothScroller.isPendingInitialRun()) {
                    smoothScroller.onAnimation(0, 0);
                }
                if (!this.mReSchedulePostAnimationCallback) {
                    smoothScroller.stop();
                }
            }
            enableRunOnAnimationRequests();
        }

        private void disableRunOnAnimationRequests() {
            this.mReSchedulePostAnimationCallback = false;
            this.mEatRunOnAnimationRequest = true;
        }

        private void enableRunOnAnimationRequests() {
            this.mEatRunOnAnimationRequest = false;
            if (this.mReSchedulePostAnimationCallback) {
                postOnAnimation();
            }
        }

        /* access modifiers changed from: package-private */
        public void postOnAnimation() {
            if (this.mEatRunOnAnimationRequest) {
                this.mReSchedulePostAnimationCallback = true;
                return;
            }
            boolean removeCallbacks = this.this$0.removeCallbacks(this);
            ViewCompat.postOnAnimation(this.this$0, this);
        }

        public void fling(int i, int i2) {
            this.this$0.setScrollState(2);
            this.mLastFlingY = 0;
            this.mLastFlingX = 0;
            this.mScroller.fling(0, 0, i, i2, Integer.MIN_VALUE, ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, Integer.MIN_VALUE, ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
            postOnAnimation();
        }

        public void smoothScrollBy(int i, int i2) {
            smoothScrollBy(i, i2, 0, 0);
        }

        public void smoothScrollBy(int i, int i2, int i3, int i4) {
            int i5 = i;
            int i6 = i2;
            smoothScrollBy(i5, i6, computeScrollDuration(i5, i6, i3, i4));
        }

        private float distanceInfluenceForSnapDuration(float f) {
            return (float) Math.sin((double) ((float) (((double) (f - 0.5f)) * 0.4712389167638204d)));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        private int computeScrollDuration(int i, int i2, int i3, int i4) {
            int i5;
            int i6 = i;
            int i7 = i2;
            int i8 = i3;
            int i9 = i4;
            int abs = Math.abs(i6);
            int abs2 = Math.abs(i7);
            boolean z = abs > abs2;
            int sqrt = (int) Math.sqrt((double) ((i8 * i8) + (i9 * i9)));
            int sqrt2 = (int) Math.sqrt((double) ((i6 * i6) + (i7 * i7)));
            int width = z ? this.this$0.getWidth() : this.this$0.getHeight();
            int i10 = width / 2;
            float distanceInfluenceForSnapDuration = ((float) i10) + (((float) i10) * distanceInfluenceForSnapDuration(Math.min(1.0f, (1.0f * ((float) sqrt2)) / ((float) width))));
            if (sqrt > 0) {
                i5 = 4 * Math.round(1000.0f * Math.abs(distanceInfluenceForSnapDuration / ((float) sqrt)));
            } else {
                i5 = (int) (((((float) (z ? abs : abs2)) / ((float) width)) + 1.0f) * 300.0f);
            }
            return Math.min(i5, (int) RecyclerView.MAX_SCROLL_DURATION);
        }

        public void smoothScrollBy(int i, int i2, int i3) {
            smoothScrollBy(i, i2, i3, RecyclerView.sQuinticInterpolator);
        }

        public void smoothScrollBy(int i, int i2, int i3, Interpolator interpolator) {
            int i4 = i;
            int i5 = i2;
            int i6 = i3;
            Interpolator interpolator2 = interpolator;
            if (this.mInterpolator != interpolator2) {
                this.mInterpolator = interpolator2;
                this.mScroller = ScrollerCompat.create(this.this$0.getContext(), interpolator2);
            }
            this.this$0.setScrollState(2);
            this.mLastFlingY = 0;
            this.mLastFlingX = 0;
            this.mScroller.startScroll(0, 0, i4, i5, i6);
            postOnAnimation();
        }

        public void stop() {
            boolean removeCallbacks = this.this$0.removeCallbacks(this);
            this.mScroller.abortAnimation();
        }
    }

    /* access modifiers changed from: private */
    public void repositionShadowingViews() {
        int childCount = this.mChildHelper.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.mChildHelper.getChildAt(i);
            ViewHolder childViewHolder = getChildViewHolder(childAt);
            if (!(childViewHolder == null || childViewHolder.mShadowingHolder == null)) {
                View view = childViewHolder.mShadowingHolder.itemView;
                int left = childAt.getLeft();
                int top = childAt.getTop();
                if (left != view.getLeft() || top != view.getTop()) {
                    view.layout(left, top, left + view.getWidth(), top + view.getHeight());
                }
            }
        }
    }

    private class RecyclerViewDataObserver extends AdapterDataObserver {
        private RecyclerViewDataObserver() {
        }

        public void onChanged() {
            RecyclerView.this.assertNotInLayoutOrScroll(null);
            if (RecyclerView.this.mAdapter.hasStableIds()) {
                boolean access$1902 = State.access$1902(RecyclerView.this.mState, true);
                RecyclerView.this.setDataSetChangedAfterLayout();
            } else {
                boolean access$19022 = State.access$1902(RecyclerView.this.mState, true);
                RecyclerView.this.setDataSetChangedAfterLayout();
            }
            if (!RecyclerView.this.mAdapterHelper.hasPendingUpdates()) {
                RecyclerView.this.requestLayout();
            }
        }

        public void onItemRangeChanged(int i, int i2, Object obj) {
            RecyclerView.this.assertNotInLayoutOrScroll(null);
            if (RecyclerView.this.mAdapterHelper.onItemRangeChanged(i, i2, obj)) {
                triggerUpdateProcessor();
            }
        }

        public void onItemRangeInserted(int i, int i2) {
            RecyclerView.this.assertNotInLayoutOrScroll(null);
            if (RecyclerView.this.mAdapterHelper.onItemRangeInserted(i, i2)) {
                triggerUpdateProcessor();
            }
        }

        public void onItemRangeRemoved(int i, int i2) {
            RecyclerView.this.assertNotInLayoutOrScroll(null);
            if (RecyclerView.this.mAdapterHelper.onItemRangeRemoved(i, i2)) {
                triggerUpdateProcessor();
            }
        }

        public void onItemRangeMoved(int i, int i2, int i3) {
            RecyclerView.this.assertNotInLayoutOrScroll(null);
            if (RecyclerView.this.mAdapterHelper.onItemRangeMoved(i, i2, i3)) {
                triggerUpdateProcessor();
            }
        }

        /* access modifiers changed from: package-private */
        public void triggerUpdateProcessor() {
            if (!RecyclerView.this.mPostUpdatesOnAnimation || !RecyclerView.this.mHasFixedSize || !RecyclerView.this.mIsAttached) {
                boolean access$4302 = RecyclerView.access$4302(RecyclerView.this, true);
                RecyclerView.this.requestLayout();
                return;
            }
            ViewCompat.postOnAnimation(RecyclerView.this, RecyclerView.this.mUpdateChildViewsRunnable);
        }
    }

    public static class RecycledViewPool {
        private static final int DEFAULT_MAX_SCRAP = 5;
        private int mAttachCount = 0;
        private SparseIntArray mMaxScrap;
        private SparseArray<ArrayList<ViewHolder>> mScrap;

        public RecycledViewPool() {
            SparseArray<ArrayList<ViewHolder>> sparseArray;
            SparseIntArray sparseIntArray;
            new SparseArray<>();
            this.mScrap = sparseArray;
            new SparseIntArray();
            this.mMaxScrap = sparseIntArray;
        }

        public void clear() {
            this.mScrap.clear();
        }

        public void setMaxRecycledViews(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            this.mMaxScrap.put(i3, i4);
            ArrayList arrayList = this.mScrap.get(i3);
            if (arrayList != null) {
                while (arrayList.size() > i4) {
                    Object remove = arrayList.remove(arrayList.size() - 1);
                }
            }
        }

        public ViewHolder getRecycledView(int i) {
            ArrayList arrayList = this.mScrap.get(i);
            if (arrayList == null || arrayList.isEmpty()) {
                return null;
            }
            int size = arrayList.size() - 1;
            ViewHolder viewHolder = (ViewHolder) arrayList.get(size);
            Object remove = arrayList.remove(size);
            return viewHolder;
        }

        /* access modifiers changed from: package-private */
        public int size() {
            int i = 0;
            for (int i2 = 0; i2 < this.mScrap.size(); i2++) {
                ArrayList valueAt = this.mScrap.valueAt(i2);
                if (valueAt != null) {
                    i += valueAt.size();
                }
            }
            return i;
        }

        public void putRecycledView(ViewHolder viewHolder) {
            ViewHolder viewHolder2 = viewHolder;
            int itemViewType = viewHolder2.getItemViewType();
            ArrayList<ViewHolder> scrapHeapForType = getScrapHeapForType(itemViewType);
            if (this.mMaxScrap.get(itemViewType) > scrapHeapForType.size()) {
                viewHolder2.resetInternal();
                boolean add = scrapHeapForType.add(viewHolder2);
            }
        }

        /* access modifiers changed from: package-private */
        public void attach(Adapter adapter) {
            this.mAttachCount = this.mAttachCount + 1;
        }

        /* access modifiers changed from: package-private */
        public void detach() {
            this.mAttachCount = this.mAttachCount - 1;
        }

        /* access modifiers changed from: package-private */
        public void onAdapterChanged(Adapter adapter, Adapter adapter2, boolean z) {
            Adapter adapter3 = adapter2;
            boolean z2 = z;
            if (adapter != null) {
                detach();
            }
            if (!z2 && this.mAttachCount == 0) {
                clear();
            }
            if (adapter3 != null) {
                attach(adapter3);
            }
        }

        private ArrayList<ViewHolder> getScrapHeapForType(int i) {
            ArrayList arrayList;
            int i2 = i;
            ArrayList arrayList2 = this.mScrap.get(i2);
            if (arrayList2 == null) {
                new ArrayList();
                arrayList2 = arrayList;
                this.mScrap.put(i2, arrayList2);
                if (this.mMaxScrap.indexOfKey(i2) < 0) {
                    this.mMaxScrap.put(i2, 5);
                }
            }
            return arrayList2;
        }
    }

    public final class Recycler {
        private static final int DEFAULT_CACHE_SIZE = 2;
        final ArrayList<ViewHolder> mAttachedScrap;
        final ArrayList<ViewHolder> mCachedViews;
        /* access modifiers changed from: private */
        public ArrayList<ViewHolder> mChangedScrap = null;
        private RecycledViewPool mRecyclerPool;
        private final List<ViewHolder> mUnmodifiableAttachedScrap;
        private ViewCacheExtension mViewCacheExtension;
        private int mViewCacheMax;

        public Recycler() {
            ArrayList<ViewHolder> arrayList;
            ArrayList<ViewHolder> arrayList2;
            new ArrayList<>();
            this.mAttachedScrap = arrayList;
            new ArrayList<>();
            this.mCachedViews = arrayList2;
            this.mUnmodifiableAttachedScrap = Collections.unmodifiableList(this.mAttachedScrap);
            this.mViewCacheMax = 2;
        }

        public void clear() {
            this.mAttachedScrap.clear();
            recycleAndClearCachedViews();
        }

        public void setViewCacheSize(int i) {
            int i2 = i;
            this.mViewCacheMax = i2;
            for (int size = this.mCachedViews.size() - 1; size >= 0 && this.mCachedViews.size() > i2; size--) {
                recycleCachedViewAt(size);
            }
        }

        public List<ViewHolder> getScrapList() {
            return this.mUnmodifiableAttachedScrap;
        }

        /* access modifiers changed from: package-private */
        public boolean validateViewHolderForOffsetPosition(ViewHolder viewHolder) {
            Throwable th;
            StringBuilder sb;
            ViewHolder viewHolder2 = viewHolder;
            if (viewHolder2.isRemoved()) {
                return RecyclerView.this.mState.isPreLayout();
            }
            if (viewHolder2.mPosition < 0 || viewHolder2.mPosition >= RecyclerView.this.mAdapter.getItemCount()) {
                Throwable th2 = th;
                new StringBuilder();
                new IndexOutOfBoundsException(sb.append("Inconsistency detected. Invalid view holder adapter position").append(viewHolder2).toString());
                throw th2;
            } else if (!RecyclerView.this.mState.isPreLayout() && RecyclerView.this.mAdapter.getItemViewType(viewHolder2.mPosition) != viewHolder2.getItemViewType()) {
                return false;
            } else {
                if (!RecyclerView.this.mAdapter.hasStableIds()) {
                    return true;
                }
                return viewHolder2.getItemId() == RecyclerView.this.mAdapter.getItemId(viewHolder2.mPosition);
            }
        }

        public void bindViewToPosition(View view, int i) {
            Throwable th;
            StringBuilder sb;
            LayoutParams layoutParams;
            Throwable th2;
            View view2 = view;
            int i2 = i;
            ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view2);
            if (childViewHolderInt == null) {
                Throwable th3 = th2;
                new IllegalArgumentException("The view does not have a ViewHolder. You cannot pass arbitrary views to this method, they should be created by the Adapter");
                throw th3;
            }
            int findPositionOffset = RecyclerView.this.mAdapterHelper.findPositionOffset(i2);
            if (findPositionOffset < 0 || findPositionOffset >= RecyclerView.this.mAdapter.getItemCount()) {
                Throwable th4 = th;
                new StringBuilder();
                new IndexOutOfBoundsException(sb.append("Inconsistency detected. Invalid item position ").append(i2).append("(offset:").append(findPositionOffset).append(").").append("state:").append(RecyclerView.this.mState.getItemCount()).toString());
                throw th4;
            }
            childViewHolderInt.mOwnerRecyclerView = RecyclerView.this;
            RecyclerView.this.mAdapter.bindViewHolder(childViewHolderInt, findPositionOffset);
            attachAccessibilityDelegate(view2);
            if (RecyclerView.this.mState.isPreLayout()) {
                childViewHolderInt.mPreLayoutPosition = i2;
            }
            ViewGroup.LayoutParams layoutParams2 = childViewHolderInt.itemView.getLayoutParams();
            if (layoutParams2 == null) {
                layoutParams = (LayoutParams) RecyclerView.this.generateDefaultLayoutParams();
                childViewHolderInt.itemView.setLayoutParams(layoutParams);
            } else if (!RecyclerView.this.checkLayoutParams(layoutParams2)) {
                layoutParams = (LayoutParams) RecyclerView.this.generateLayoutParams(layoutParams2);
                childViewHolderInt.itemView.setLayoutParams(layoutParams);
            } else {
                layoutParams = (LayoutParams) layoutParams2;
            }
            layoutParams.mInsetsDirty = true;
            layoutParams.mViewHolder = childViewHolderInt;
            layoutParams.mPendingInvalidate = childViewHolderInt.itemView.getParent() == null;
        }

        public int convertPreLayoutPositionToPostLayout(int i) {
            Throwable th;
            StringBuilder sb;
            int i2 = i;
            if (i2 < 0 || i2 >= RecyclerView.this.mState.getItemCount()) {
                Throwable th2 = th;
                new StringBuilder();
                new IndexOutOfBoundsException(sb.append("invalid position ").append(i2).append(". State ").append("item count is ").append(RecyclerView.this.mState.getItemCount()).toString());
                throw th2;
            } else if (!RecyclerView.this.mState.isPreLayout()) {
                return i2;
            } else {
                return RecyclerView.this.mAdapterHelper.findPositionOffset(i2);
            }
        }

        public View getViewForPosition(int i) {
            return getViewForPosition(i, false);
        }

        /* access modifiers changed from: package-private */
        public View getViewForPosition(int i, boolean z) {
            Throwable th;
            StringBuilder sb;
            LayoutParams layoutParams;
            Throwable th2;
            StringBuilder sb2;
            View viewForPositionAndType;
            Throwable th3;
            Throwable th4;
            int i2 = i;
            boolean z2 = z;
            if (i2 < 0 || i2 >= RecyclerView.this.mState.getItemCount()) {
                Throwable th5 = th;
                new StringBuilder();
                new IndexOutOfBoundsException(sb.append("Invalid item position ").append(i2).append("(").append(i2).append("). Item count:").append(RecyclerView.this.mState.getItemCount()).toString());
                throw th5;
            }
            boolean z3 = false;
            ViewHolder viewHolder = null;
            if (RecyclerView.this.mState.isPreLayout()) {
                viewHolder = getChangedScrapViewForPosition(i2);
                z3 = viewHolder != null;
            }
            if (viewHolder == null) {
                viewHolder = getScrapViewForPosition(i2, -1, z2);
                if (viewHolder != null) {
                    if (!validateViewHolderForOffsetPosition(viewHolder)) {
                        if (!z2) {
                            viewHolder.addFlags(4);
                            if (viewHolder.isScrap()) {
                                RecyclerView.this.removeDetachedView(viewHolder.itemView, false);
                                viewHolder.unScrap();
                            } else if (viewHolder.wasReturnedFromScrap()) {
                                viewHolder.clearReturnedFromScrapFlag();
                            }
                            recycleViewHolderInternal(viewHolder);
                        }
                        viewHolder = null;
                    } else {
                        z3 = true;
                    }
                }
            }
            if (viewHolder == null) {
                int findPositionOffset = RecyclerView.this.mAdapterHelper.findPositionOffset(i2);
                if (findPositionOffset < 0 || findPositionOffset >= RecyclerView.this.mAdapter.getItemCount()) {
                    Throwable th6 = th2;
                    new StringBuilder();
                    new IndexOutOfBoundsException(sb2.append("Inconsistency detected. Invalid item position ").append(i2).append("(offset:").append(findPositionOffset).append(").").append("state:").append(RecyclerView.this.mState.getItemCount()).toString());
                    throw th6;
                }
                int itemViewType = RecyclerView.this.mAdapter.getItemViewType(findPositionOffset);
                if (RecyclerView.this.mAdapter.hasStableIds()) {
                    viewHolder = getScrapViewForId(RecyclerView.this.mAdapter.getItemId(findPositionOffset), itemViewType, z2);
                    if (viewHolder != null) {
                        viewHolder.mPosition = findPositionOffset;
                        z3 = true;
                    }
                }
                if (!(viewHolder != null || this.mViewCacheExtension == null || (viewForPositionAndType = this.mViewCacheExtension.getViewForPositionAndType(this, i2, itemViewType)) == null)) {
                    viewHolder = RecyclerView.this.getChildViewHolder(viewForPositionAndType);
                    if (viewHolder == null) {
                        Throwable th7 = th4;
                        new IllegalArgumentException("getViewForPositionAndType returned a view which does not have a ViewHolder");
                        throw th7;
                    } else if (viewHolder.shouldIgnore()) {
                        Throwable th8 = th3;
                        new IllegalArgumentException("getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view.");
                        throw th8;
                    }
                }
                if (viewHolder == null) {
                    viewHolder = getRecycledViewPool().getRecycledView(itemViewType);
                    if (viewHolder != null) {
                        viewHolder.resetInternal();
                        if (RecyclerView.FORCE_INVALIDATE_DISPLAY_LIST) {
                            invalidateDisplayListInt(viewHolder);
                        }
                    }
                }
                if (viewHolder == null) {
                    viewHolder = RecyclerView.this.mAdapter.createViewHolder(RecyclerView.this, itemViewType);
                }
            }
            if (z3 && !RecyclerView.this.mState.isPreLayout() && viewHolder.hasAnyOfTheFlags(8192)) {
                viewHolder.setFlags(0, 8192);
                if (RecyclerView.this.mState.mRunSimpleAnimations) {
                    RecyclerView.this.recordAnimationInfoIfBouncedHiddenView(viewHolder, RecyclerView.this.mItemAnimator.recordPreLayoutInformation(RecyclerView.this.mState, viewHolder, ItemAnimator.buildAdapterChangeFlagsForAnimations(viewHolder) | 4096, viewHolder.getUnmodifiedPayloads()));
                }
            }
            boolean z4 = false;
            if (RecyclerView.this.mState.isPreLayout() && viewHolder.isBound()) {
                viewHolder.mPreLayoutPosition = i2;
            } else if (!viewHolder.isBound() || viewHolder.needsUpdate() || viewHolder.isInvalid()) {
                int findPositionOffset2 = RecyclerView.this.mAdapterHelper.findPositionOffset(i2);
                viewHolder.mOwnerRecyclerView = RecyclerView.this;
                RecyclerView.this.mAdapter.bindViewHolder(viewHolder, findPositionOffset2);
                attachAccessibilityDelegate(viewHolder.itemView);
                z4 = true;
                if (RecyclerView.this.mState.isPreLayout()) {
                    viewHolder.mPreLayoutPosition = i2;
                }
            }
            ViewGroup.LayoutParams layoutParams2 = viewHolder.itemView.getLayoutParams();
            if (layoutParams2 == null) {
                layoutParams = (LayoutParams) RecyclerView.this.generateDefaultLayoutParams();
                viewHolder.itemView.setLayoutParams(layoutParams);
            } else if (!RecyclerView.this.checkLayoutParams(layoutParams2)) {
                layoutParams = (LayoutParams) RecyclerView.this.generateLayoutParams(layoutParams2);
                viewHolder.itemView.setLayoutParams(layoutParams);
            } else {
                layoutParams = (LayoutParams) layoutParams2;
            }
            layoutParams.mViewHolder = viewHolder;
            layoutParams.mPendingInvalidate = z3 && z4;
            return viewHolder.itemView;
        }

        private void attachAccessibilityDelegate(View view) {
            View view2 = view;
            if (RecyclerView.this.isAccessibilityEnabled()) {
                if (ViewCompat.getImportantForAccessibility(view2) == 0) {
                    ViewCompat.setImportantForAccessibility(view2, 1);
                }
                if (!ViewCompat.hasAccessibilityDelegate(view2)) {
                    ViewCompat.setAccessibilityDelegate(view2, RecyclerView.this.mAccessibilityDelegate.getItemDelegate());
                }
            }
        }

        private void invalidateDisplayListInt(ViewHolder viewHolder) {
            ViewHolder viewHolder2 = viewHolder;
            if (viewHolder2.itemView instanceof ViewGroup) {
                invalidateDisplayListInt((ViewGroup) viewHolder2.itemView, false);
            }
        }

        private void invalidateDisplayListInt(ViewGroup viewGroup, boolean z) {
            ViewGroup viewGroup2 = viewGroup;
            boolean z2 = z;
            for (int childCount = viewGroup2.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup2.getChildAt(childCount);
                if (childAt instanceof ViewGroup) {
                    invalidateDisplayListInt((ViewGroup) childAt, true);
                }
            }
            if (z2) {
                if (viewGroup2.getVisibility() == 4) {
                    viewGroup2.setVisibility(0);
                    viewGroup2.setVisibility(4);
                    return;
                }
                int visibility = viewGroup2.getVisibility();
                viewGroup2.setVisibility(4);
                viewGroup2.setVisibility(visibility);
            }
        }

        public void recycleView(View view) {
            View view2 = view;
            ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view2);
            if (childViewHolderInt.isTmpDetached()) {
                RecyclerView.this.removeDetachedView(view2, false);
            }
            if (childViewHolderInt.isScrap()) {
                childViewHolderInt.unScrap();
            } else if (childViewHolderInt.wasReturnedFromScrap()) {
                childViewHolderInt.clearReturnedFromScrapFlag();
            }
            recycleViewHolderInternal(childViewHolderInt);
        }

        /* access modifiers changed from: package-private */
        public void recycleViewInternal(View view) {
            recycleViewHolderInternal(RecyclerView.getChildViewHolderInt(view));
        }

        /* access modifiers changed from: package-private */
        public void recycleAndClearCachedViews() {
            for (int size = this.mCachedViews.size() - 1; size >= 0; size--) {
                recycleCachedViewAt(size);
            }
            this.mCachedViews.clear();
        }

        /* access modifiers changed from: package-private */
        public void recycleCachedViewAt(int i) {
            int i2 = i;
            addViewHolderToRecycledViewPool(this.mCachedViews.get(i2));
            ViewHolder remove = this.mCachedViews.remove(i2);
        }

        /* access modifiers changed from: package-private */
        public void recycleViewHolderInternal(ViewHolder viewHolder) {
            IllegalArgumentException illegalArgumentException;
            StringBuilder sb;
            Throwable th;
            Throwable th2;
            StringBuilder sb2;
            ViewHolder viewHolder2 = viewHolder;
            if (viewHolder2.isScrap() || viewHolder2.itemView.getParent() != null) {
                IllegalArgumentException illegalArgumentException2 = illegalArgumentException;
                new StringBuilder();
                new IllegalArgumentException(sb.append("Scrapped or attached views may not be recycled. isScrap:").append(viewHolder2.isScrap()).append(" isAttached:").append(viewHolder2.itemView.getParent() != null).toString());
                throw illegalArgumentException2;
            } else if (viewHolder2.isTmpDetached()) {
                Throwable th3 = th2;
                new StringBuilder();
                new IllegalArgumentException(sb2.append("Tmp detached view should be removed from RecyclerView before it can be recycled: ").append(viewHolder2).toString());
                throw th3;
            } else if (viewHolder2.shouldIgnore()) {
                Throwable th4 = th;
                new IllegalArgumentException("Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle.");
                throw th4;
            } else {
                boolean access$4700 = viewHolder2.doesTransientStatePreventRecycling();
                boolean z = false;
                boolean z2 = false;
                if ((RecyclerView.this.mAdapter != null && access$4700 && RecyclerView.this.mAdapter.onFailedToRecycleView(viewHolder2)) || viewHolder2.isRecyclable()) {
                    if (!viewHolder2.hasAnyOfTheFlags(14)) {
                        int size = this.mCachedViews.size();
                        if (size == this.mViewCacheMax && size > 0) {
                            recycleCachedViewAt(0);
                        }
                        if (size < this.mViewCacheMax) {
                            boolean add = this.mCachedViews.add(viewHolder2);
                            z = true;
                        }
                    }
                    if (!z) {
                        addViewHolderToRecycledViewPool(viewHolder2);
                        z2 = true;
                    }
                }
                RecyclerView.this.mViewInfoStore.removeViewHolder(viewHolder2);
                if (!z && !z2 && access$4700) {
                    viewHolder2.mOwnerRecyclerView = null;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void addViewHolderToRecycledViewPool(ViewHolder viewHolder) {
            ViewHolder viewHolder2 = viewHolder;
            ViewCompat.setAccessibilityDelegate(viewHolder2.itemView, null);
            dispatchViewRecycled(viewHolder2);
            viewHolder2.mOwnerRecyclerView = null;
            getRecycledViewPool().putRecycledView(viewHolder2);
        }

        /* access modifiers changed from: package-private */
        public void quickRecycleScrapView(View view) {
            ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            Recycler access$4802 = ViewHolder.access$4802(childViewHolderInt, null);
            boolean access$4902 = ViewHolder.access$4902(childViewHolderInt, false);
            childViewHolderInt.clearReturnedFromScrapFlag();
            recycleViewHolderInternal(childViewHolderInt);
        }

        /* access modifiers changed from: package-private */
        public void scrapView(View view) {
            Throwable th;
            ArrayList<ViewHolder> arrayList;
            ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            if (!childViewHolderInt.hasAnyOfTheFlags(12) && childViewHolderInt.isUpdated() && !RecyclerView.this.canReuseUpdatedViewHolder(childViewHolderInt)) {
                if (this.mChangedScrap == null) {
                    new ArrayList<>();
                    this.mChangedScrap = arrayList;
                }
                childViewHolderInt.setScrapContainer(this, true);
                boolean add = this.mChangedScrap.add(childViewHolderInt);
            } else if (!childViewHolderInt.isInvalid() || childViewHolderInt.isRemoved() || RecyclerView.this.mAdapter.hasStableIds()) {
                childViewHolderInt.setScrapContainer(this, false);
                boolean add2 = this.mAttachedScrap.add(childViewHolderInt);
            } else {
                Throwable th2 = th;
                new IllegalArgumentException("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool.");
                throw th2;
            }
        }

        /* access modifiers changed from: package-private */
        public void unscrapView(ViewHolder viewHolder) {
            ViewHolder viewHolder2 = viewHolder;
            if (viewHolder2.mInChangeScrap) {
                boolean remove = this.mChangedScrap.remove(viewHolder2);
            } else {
                boolean remove2 = this.mAttachedScrap.remove(viewHolder2);
            }
            Recycler access$4802 = ViewHolder.access$4802(viewHolder2, null);
            boolean access$4902 = ViewHolder.access$4902(viewHolder2, false);
            viewHolder2.clearReturnedFromScrapFlag();
        }

        /* access modifiers changed from: package-private */
        public int getScrapCount() {
            return this.mAttachedScrap.size();
        }

        /* access modifiers changed from: package-private */
        public View getScrapViewAt(int i) {
            return this.mAttachedScrap.get(i).itemView;
        }

        /* access modifiers changed from: package-private */
        public void clearScrap() {
            this.mAttachedScrap.clear();
            if (this.mChangedScrap != null) {
                this.mChangedScrap.clear();
            }
        }

        /* access modifiers changed from: package-private */
        public ViewHolder getChangedScrapViewForPosition(int i) {
            int findPositionOffset;
            int i2 = i;
            if (this.mChangedScrap != null) {
                int size = this.mChangedScrap.size();
                int i3 = size;
                if (size != 0) {
                    int i4 = 0;
                    while (i4 < i3) {
                        ViewHolder viewHolder = this.mChangedScrap.get(i4);
                        if (viewHolder.wasReturnedFromScrap() || viewHolder.getLayoutPosition() != i2) {
                            i4++;
                        } else {
                            viewHolder.addFlags(32);
                            return viewHolder;
                        }
                    }
                    if (RecyclerView.this.mAdapter.hasStableIds() && (findPositionOffset = RecyclerView.this.mAdapterHelper.findPositionOffset(i2)) > 0 && findPositionOffset < RecyclerView.this.mAdapter.getItemCount()) {
                        long itemId = RecyclerView.this.mAdapter.getItemId(findPositionOffset);
                        int i5 = 0;
                        while (i5 < i3) {
                            ViewHolder viewHolder2 = this.mChangedScrap.get(i5);
                            if (viewHolder2.wasReturnedFromScrap() || viewHolder2.getItemId() != itemId) {
                                i5++;
                            } else {
                                viewHolder2.addFlags(32);
                                return viewHolder2;
                            }
                        }
                    }
                    return null;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public ViewHolder getScrapViewForPosition(int i, int i2, boolean z) {
            View findHiddenNonRemovedView;
            Throwable th;
            StringBuilder sb;
            ViewHolder viewHolder;
            StringBuilder sb2;
            int i3 = i;
            int i4 = i2;
            boolean z2 = z;
            int size = this.mAttachedScrap.size();
            int i5 = 0;
            while (true) {
                if (i5 >= size) {
                    break;
                }
                viewHolder = this.mAttachedScrap.get(i5);
                if (viewHolder.wasReturnedFromScrap() || viewHolder.getLayoutPosition() != i3 || viewHolder.isInvalid() || (!RecyclerView.this.mState.mInPreLayout && viewHolder.isRemoved())) {
                    i5++;
                }
            }
            if (i4 == -1 || viewHolder.getItemViewType() == i4) {
                viewHolder.addFlags(32);
                return viewHolder;
            }
            new StringBuilder();
            int e = Log.e(RecyclerView.TAG, sb2.append("Scrap view for position ").append(i3).append(" isn't dirty but has").append(" wrong view type! (found ").append(viewHolder.getItemViewType()).append(" but expected ").append(i4).append(")").toString());
            if (z2 || (findHiddenNonRemovedView = RecyclerView.this.mChildHelper.findHiddenNonRemovedView(i3, i4)) == null) {
                int size2 = this.mCachedViews.size();
                int i6 = 0;
                while (i6 < size2) {
                    ViewHolder viewHolder2 = this.mCachedViews.get(i6);
                    if (viewHolder2.isInvalid() || viewHolder2.getLayoutPosition() != i3) {
                        i6++;
                    } else {
                        if (!z2) {
                            ViewHolder remove = this.mCachedViews.remove(i6);
                        }
                        return viewHolder2;
                    }
                }
                return null;
            }
            ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(findHiddenNonRemovedView);
            RecyclerView.this.mChildHelper.unhide(findHiddenNonRemovedView);
            int indexOfChild = RecyclerView.this.mChildHelper.indexOfChild(findHiddenNonRemovedView);
            if (indexOfChild == -1) {
                Throwable th2 = th;
                new StringBuilder();
                new IllegalStateException(sb.append("layout index should not be -1 after unhiding a view:").append(childViewHolderInt).toString());
                throw th2;
            }
            RecyclerView.this.mChildHelper.detachViewFromParent(indexOfChild);
            scrapView(findHiddenNonRemovedView);
            childViewHolderInt.addFlags(8224);
            return childViewHolderInt;
        }

        /* access modifiers changed from: package-private */
        public ViewHolder getScrapViewForId(long j, int i, boolean z) {
            long j2 = j;
            int i2 = i;
            boolean z2 = z;
            for (int size = this.mAttachedScrap.size() - 1; size >= 0; size--) {
                ViewHolder viewHolder = this.mAttachedScrap.get(size);
                if (viewHolder.getItemId() == j2 && !viewHolder.wasReturnedFromScrap()) {
                    if (i2 == viewHolder.getItemViewType()) {
                        viewHolder.addFlags(32);
                        if (viewHolder.isRemoved() && !RecyclerView.this.mState.isPreLayout()) {
                            viewHolder.setFlags(2, 14);
                        }
                        return viewHolder;
                    } else if (!z2) {
                        ViewHolder remove = this.mAttachedScrap.remove(size);
                        RecyclerView.this.removeDetachedView(viewHolder.itemView, false);
                        quickRecycleScrapView(viewHolder.itemView);
                    }
                }
            }
            for (int size2 = this.mCachedViews.size() - 1; size2 >= 0; size2--) {
                ViewHolder viewHolder2 = this.mCachedViews.get(size2);
                if (viewHolder2.getItemId() == j2) {
                    if (i2 == viewHolder2.getItemViewType()) {
                        if (!z2) {
                            ViewHolder remove2 = this.mCachedViews.remove(size2);
                        }
                        return viewHolder2;
                    } else if (!z2) {
                        recycleCachedViewAt(size2);
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public void dispatchViewRecycled(ViewHolder viewHolder) {
            ViewHolder viewHolder2 = viewHolder;
            if (RecyclerView.this.mRecyclerListener != null) {
                RecyclerView.this.mRecyclerListener.onViewRecycled(viewHolder2);
            }
            if (RecyclerView.this.mAdapter != null) {
                RecyclerView.this.mAdapter.onViewRecycled(viewHolder2);
            }
            if (RecyclerView.this.mState != null) {
                RecyclerView.this.mViewInfoStore.removeViewHolder(viewHolder2);
            }
        }

        /* access modifiers changed from: package-private */
        public void onAdapterChanged(Adapter adapter, Adapter adapter2, boolean z) {
            clear();
            getRecycledViewPool().onAdapterChanged(adapter, adapter2, z);
        }

        /* access modifiers changed from: package-private */
        public void offsetPositionRecordsForMove(int i, int i2) {
            int i3;
            int i4;
            int i5;
            int i6 = i;
            int i7 = i2;
            if (i6 < i7) {
                i3 = i6;
                i4 = i7;
                i5 = -1;
            } else {
                i3 = i7;
                i4 = i6;
                i5 = 1;
            }
            int size = this.mCachedViews.size();
            for (int i8 = 0; i8 < size; i8++) {
                ViewHolder viewHolder = this.mCachedViews.get(i8);
                if (viewHolder != null && viewHolder.mPosition >= i3 && viewHolder.mPosition <= i4) {
                    if (viewHolder.mPosition == i6) {
                        viewHolder.offsetPosition(i7 - i6, false);
                    } else {
                        viewHolder.offsetPosition(i5, false);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void offsetPositionRecordsForInsert(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            int size = this.mCachedViews.size();
            for (int i5 = 0; i5 < size; i5++) {
                ViewHolder viewHolder = this.mCachedViews.get(i5);
                if (viewHolder != null && viewHolder.getLayoutPosition() >= i3) {
                    viewHolder.offsetPosition(i4, true);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void offsetPositionRecordsForRemove(int i, int i2, boolean z) {
            int i3 = i;
            int i4 = i2;
            boolean z2 = z;
            int i5 = i3 + i4;
            for (int size = this.mCachedViews.size() - 1; size >= 0; size--) {
                ViewHolder viewHolder = this.mCachedViews.get(size);
                if (viewHolder != null) {
                    if (viewHolder.getLayoutPosition() >= i5) {
                        viewHolder.offsetPosition(-i4, z2);
                    } else if (viewHolder.getLayoutPosition() >= i3) {
                        viewHolder.addFlags(8);
                        recycleCachedViewAt(size);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void setViewCacheExtension(ViewCacheExtension viewCacheExtension) {
            this.mViewCacheExtension = viewCacheExtension;
        }

        /* access modifiers changed from: package-private */
        public void setRecycledViewPool(RecycledViewPool recycledViewPool) {
            RecycledViewPool recycledViewPool2 = recycledViewPool;
            if (this.mRecyclerPool != null) {
                this.mRecyclerPool.detach();
            }
            this.mRecyclerPool = recycledViewPool2;
            if (recycledViewPool2 != null) {
                this.mRecyclerPool.attach(RecyclerView.this.getAdapter());
            }
        }

        /* access modifiers changed from: package-private */
        public RecycledViewPool getRecycledViewPool() {
            RecycledViewPool recycledViewPool;
            if (this.mRecyclerPool == null) {
                new RecycledViewPool();
                this.mRecyclerPool = recycledViewPool;
            }
            return this.mRecyclerPool;
        }

        /* access modifiers changed from: package-private */
        public void viewRangeUpdate(int i, int i2) {
            int layoutPosition;
            int i3 = i;
            int i4 = i3 + i2;
            for (int size = this.mCachedViews.size() - 1; size >= 0; size--) {
                ViewHolder viewHolder = this.mCachedViews.get(size);
                if (viewHolder != null && (layoutPosition = viewHolder.getLayoutPosition()) >= i3 && layoutPosition < i4) {
                    viewHolder.addFlags(2);
                    recycleCachedViewAt(size);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void setAdapterPositionsAsUnknown() {
            int size = this.mCachedViews.size();
            for (int i = 0; i < size; i++) {
                ViewHolder viewHolder = this.mCachedViews.get(i);
                if (viewHolder != null) {
                    viewHolder.addFlags(512);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void markKnownViewsInvalid() {
            if (RecyclerView.this.mAdapter == null || !RecyclerView.this.mAdapter.hasStableIds()) {
                recycleAndClearCachedViews();
                return;
            }
            int size = this.mCachedViews.size();
            for (int i = 0; i < size; i++) {
                ViewHolder viewHolder = this.mCachedViews.get(i);
                if (viewHolder != null) {
                    viewHolder.addFlags(6);
                    viewHolder.addChangePayload(null);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void clearOldPositions() {
            int size = this.mCachedViews.size();
            for (int i = 0; i < size; i++) {
                this.mCachedViews.get(i).clearOldPosition();
            }
            int size2 = this.mAttachedScrap.size();
            for (int i2 = 0; i2 < size2; i2++) {
                this.mAttachedScrap.get(i2).clearOldPosition();
            }
            if (this.mChangedScrap != null) {
                int size3 = this.mChangedScrap.size();
                for (int i3 = 0; i3 < size3; i3++) {
                    this.mChangedScrap.get(i3).clearOldPosition();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void markItemDecorInsetsDirty() {
            int size = this.mCachedViews.size();
            for (int i = 0; i < size; i++) {
                LayoutParams layoutParams = (LayoutParams) this.mCachedViews.get(i).itemView.getLayoutParams();
                if (layoutParams != null) {
                    layoutParams.mInsetsDirty = true;
                }
            }
        }
    }

    public static abstract class Adapter<VH extends ViewHolder> {
        private boolean mHasStableIds = false;
        private final AdapterDataObservable mObservable;

        public abstract int getItemCount();

        public abstract void onBindViewHolder(ViewHolder viewHolder, int i);

        public abstract VH onCreateViewHolder(ViewGroup viewGroup, int i);

        public Adapter() {
            AdapterDataObservable adapterDataObservable;
            new AdapterDataObservable();
            this.mObservable = adapterDataObservable;
        }

        public void onBindViewHolder(VH vh, int i, List<Object> list) {
            onBindViewHolder(vh, i);
        }

        public final VH createViewHolder(ViewGroup viewGroup, int i) {
            int i2 = i;
            TraceCompat.beginSection(RecyclerView.TRACE_CREATE_VIEW_TAG);
            VH onCreateViewHolder = onCreateViewHolder(viewGroup, i2);
            onCreateViewHolder.mItemViewType = i2;
            TraceCompat.endSection();
            return onCreateViewHolder;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: VH
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public final void bindViewHolder(VH r8, int r9) {
            /*
                r7 = this;
                r0 = r7
                r1 = r8
                r2 = r9
                r3 = r1
                r4 = r2
                r3.mPosition = r4
                r3 = r0
                boolean r3 = r3.hasStableIds()
                if (r3 == 0) goto L_0x0017
                r3 = r1
                r4 = r0
                r5 = r2
                long r4 = r4.getItemId(r5)
                r3.mItemId = r4
            L_0x0017:
                r3 = r1
                r4 = 1
                r5 = 519(0x207, float:7.27E-43)
                r3.setFlags(r4, r5)
                java.lang.String r3 = "RV OnBindView"
                android.support.v4.os.TraceCompat.beginSection(r3)
                r3 = r0
                r4 = r1
                r5 = r2
                r6 = r1
                java.util.List r6 = r6.getUnmodifiedPayloads()
                r3.onBindViewHolder(r4, r5, r6)
                r3 = r1
                r3.clearPayload()
                android.support.v4.os.TraceCompat.endSection()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.Adapter.bindViewHolder(android.support.v7.widget.RecyclerView$ViewHolder, int):void");
        }

        public int getItemViewType(int i) {
            return 0;
        }

        public void setHasStableIds(boolean z) {
            Throwable th;
            boolean z2 = z;
            if (hasObservers()) {
                Throwable th2 = th;
                new IllegalStateException("Cannot change whether this adapter has stable IDs while the adapter has registered observers.");
                throw th2;
            }
            this.mHasStableIds = z2;
        }

        public long getItemId(int i) {
            return -1;
        }

        public final boolean hasStableIds() {
            return this.mHasStableIds;
        }

        public void onViewRecycled(ViewHolder viewHolder) {
        }

        public boolean onFailedToRecycleView(VH vh) {
            return false;
        }

        public void onViewAttachedToWindow(VH vh) {
        }

        public void onViewDetachedFromWindow(VH vh) {
        }

        public final boolean hasObservers() {
            return this.mObservable.hasObservers();
        }

        public void registerAdapterDataObserver(AdapterDataObserver adapterDataObserver) {
            this.mObservable.registerObserver(adapterDataObserver);
        }

        public void unregisterAdapterDataObserver(AdapterDataObserver adapterDataObserver) {
            this.mObservable.unregisterObserver(adapterDataObserver);
        }

        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        }

        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        }

        public final void notifyDataSetChanged() {
            this.mObservable.notifyChanged();
        }

        public final void notifyItemChanged(int i) {
            this.mObservable.notifyItemRangeChanged(i, 1);
        }

        public final void notifyItemChanged(int i, Object obj) {
            this.mObservable.notifyItemRangeChanged(i, 1, obj);
        }

        public final void notifyItemRangeChanged(int i, int i2) {
            this.mObservable.notifyItemRangeChanged(i, i2);
        }

        public final void notifyItemRangeChanged(int i, int i2, Object obj) {
            this.mObservable.notifyItemRangeChanged(i, i2, obj);
        }

        public final void notifyItemInserted(int i) {
            this.mObservable.notifyItemRangeInserted(i, 1);
        }

        public final void notifyItemMoved(int i, int i2) {
            this.mObservable.notifyItemMoved(i, i2);
        }

        public final void notifyItemRangeInserted(int i, int i2) {
            this.mObservable.notifyItemRangeInserted(i, i2);
        }

        public final void notifyItemRemoved(int i) {
            this.mObservable.notifyItemRangeRemoved(i, 1);
        }

        public final void notifyItemRangeRemoved(int i, int i2) {
            this.mObservable.notifyItemRangeRemoved(i, i2);
        }
    }

    /* access modifiers changed from: private */
    public void dispatchChildDetached(View view) {
        View view2 = view;
        ViewHolder childViewHolderInt = getChildViewHolderInt(view2);
        onChildDetachedFromWindow(view2);
        if (!(this.mAdapter == null || childViewHolderInt == null)) {
            this.mAdapter.onViewDetachedFromWindow(childViewHolderInt);
        }
        if (this.mOnChildAttachStateListeners != null) {
            for (int size = this.mOnChildAttachStateListeners.size() - 1; size >= 0; size--) {
                this.mOnChildAttachStateListeners.get(size).onChildViewDetachedFromWindow(view2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void dispatchChildAttached(View view) {
        View view2 = view;
        ViewHolder childViewHolderInt = getChildViewHolderInt(view2);
        onChildAttachedToWindow(view2);
        if (!(this.mAdapter == null || childViewHolderInt == null)) {
            this.mAdapter.onViewAttachedToWindow(childViewHolderInt);
        }
        if (this.mOnChildAttachStateListeners != null) {
            for (int size = this.mOnChildAttachStateListeners.size() - 1; size >= 0; size--) {
                this.mOnChildAttachStateListeners.get(size).onChildViewAttachedToWindow(view2);
            }
        }
    }

    public static abstract class LayoutManager {
        ChildHelper mChildHelper;
        private boolean mIsAttachedToWindow = false;
        RecyclerView mRecyclerView;
        /* access modifiers changed from: private */
        public boolean mRequestedSimpleAnimations = false;
        @Nullable
        SmoothScroller mSmoothScroller;

        public static class Properties {
            public int orientation;
            public boolean reverseLayout;
            public int spanCount;
            public boolean stackFromEnd;
        }

        public abstract LayoutParams generateDefaultLayoutParams();

        static /* synthetic */ boolean access$2402(LayoutManager layoutManager, boolean z) {
            boolean z2 = z;
            boolean z3 = z2;
            layoutManager.mRequestedSimpleAnimations = z3;
            return z2;
        }

        /* access modifiers changed from: package-private */
        public void setRecyclerView(RecyclerView recyclerView) {
            RecyclerView recyclerView2 = recyclerView;
            if (recyclerView2 == null) {
                this.mRecyclerView = null;
                this.mChildHelper = null;
                return;
            }
            this.mRecyclerView = recyclerView2;
            this.mChildHelper = recyclerView2.mChildHelper;
        }

        public void requestLayout() {
            if (this.mRecyclerView != null) {
                this.mRecyclerView.requestLayout();
            }
        }

        public void assertInLayoutOrScroll(String str) {
            String str2 = str;
            if (this.mRecyclerView != null) {
                this.mRecyclerView.assertInLayoutOrScroll(str2);
            }
        }

        public void assertNotInLayoutOrScroll(String str) {
            String str2 = str;
            if (this.mRecyclerView != null) {
                this.mRecyclerView.assertNotInLayoutOrScroll(str2);
            }
        }

        public boolean supportsPredictiveItemAnimations() {
            return false;
        }

        /* access modifiers changed from: package-private */
        public void dispatchAttachedToWindow(RecyclerView recyclerView) {
            this.mIsAttachedToWindow = true;
            onAttachedToWindow(recyclerView);
        }

        /* access modifiers changed from: package-private */
        public void dispatchDetachedFromWindow(RecyclerView recyclerView, Recycler recycler) {
            this.mIsAttachedToWindow = false;
            onDetachedFromWindow(recyclerView, recycler);
        }

        public boolean isAttachedToWindow() {
            return this.mIsAttachedToWindow;
        }

        public void postOnAnimation(Runnable runnable) {
            Runnable runnable2 = runnable;
            if (this.mRecyclerView != null) {
                ViewCompat.postOnAnimation(this.mRecyclerView, runnable2);
            }
        }

        public boolean removeCallbacks(Runnable runnable) {
            Runnable runnable2 = runnable;
            if (this.mRecyclerView != null) {
                return this.mRecyclerView.removeCallbacks(runnable2);
            }
            return false;
        }

        @CallSuper
        public void onAttachedToWindow(RecyclerView recyclerView) {
        }

        @Deprecated
        public void onDetachedFromWindow(RecyclerView recyclerView) {
        }

        @CallSuper
        public void onDetachedFromWindow(RecyclerView recyclerView, Recycler recycler) {
            onDetachedFromWindow(recyclerView);
        }

        public boolean getClipToPadding() {
            return this.mRecyclerView != null && this.mRecyclerView.mClipToPadding;
        }

        public void onLayoutChildren(Recycler recycler, State state) {
            int e = Log.e(RecyclerView.TAG, "You must override onLayoutChildren(Recycler recycler, State state) ");
        }

        public boolean checkLayoutParams(LayoutParams layoutParams) {
            return layoutParams != null;
        }

        public LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
            LayoutParams layoutParams2;
            LayoutParams layoutParams3;
            LayoutParams layoutParams4;
            ViewGroup.LayoutParams layoutParams5 = layoutParams;
            if (layoutParams5 instanceof LayoutParams) {
                new LayoutParams((LayoutParams) layoutParams5);
                return layoutParams4;
            } else if (layoutParams5 instanceof ViewGroup.MarginLayoutParams) {
                new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams5);
                return layoutParams3;
            } else {
                new LayoutParams(layoutParams5);
                return layoutParams2;
            }
        }

        public LayoutParams generateLayoutParams(Context context, AttributeSet attributeSet) {
            LayoutParams layoutParams;
            new LayoutParams(context, attributeSet);
            return layoutParams;
        }

        public int scrollHorizontallyBy(int i, Recycler recycler, State state) {
            return 0;
        }

        public int scrollVerticallyBy(int i, Recycler recycler, State state) {
            return 0;
        }

        public boolean canScrollHorizontally() {
            return false;
        }

        public boolean canScrollVertically() {
            return false;
        }

        public void scrollToPosition(int i) {
        }

        public void smoothScrollToPosition(RecyclerView recyclerView, State state, int i) {
            int e = Log.e(RecyclerView.TAG, "You must override smoothScrollToPosition to support smooth scrolling");
        }

        public void startSmoothScroll(SmoothScroller smoothScroller) {
            SmoothScroller smoothScroller2 = smoothScroller;
            if (!(this.mSmoothScroller == null || smoothScroller2 == this.mSmoothScroller || !this.mSmoothScroller.isRunning())) {
                this.mSmoothScroller.stop();
            }
            this.mSmoothScroller = smoothScroller2;
            this.mSmoothScroller.start(this.mRecyclerView, this);
        }

        public boolean isSmoothScrolling() {
            return this.mSmoothScroller != null && this.mSmoothScroller.isRunning();
        }

        public int getLayoutDirection() {
            return ViewCompat.getLayoutDirection(this.mRecyclerView);
        }

        public void endAnimation(View view) {
            View view2 = view;
            if (this.mRecyclerView.mItemAnimator != null) {
                this.mRecyclerView.mItemAnimator.endAnimation(RecyclerView.getChildViewHolderInt(view2));
            }
        }

        public void addDisappearingView(View view) {
            addDisappearingView(view, -1);
        }

        public void addDisappearingView(View view, int i) {
            addViewInt(view, i, true);
        }

        public void addView(View view) {
            addView(view, -1);
        }

        public void addView(View view, int i) {
            addViewInt(view, i, false);
        }

        private void addViewInt(View view, int i, boolean z) {
            Throwable th;
            StringBuilder sb;
            View view2 = view;
            int i2 = i;
            ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view2);
            if (z || childViewHolderInt.isRemoved()) {
                this.mRecyclerView.mViewInfoStore.addToDisappearedInLayout(childViewHolderInt);
            } else {
                this.mRecyclerView.mViewInfoStore.removeFromDisappearedInLayout(childViewHolderInt);
            }
            LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
            if (childViewHolderInt.wasReturnedFromScrap() || childViewHolderInt.isScrap()) {
                if (childViewHolderInt.isScrap()) {
                    childViewHolderInt.unScrap();
                } else {
                    childViewHolderInt.clearReturnedFromScrapFlag();
                }
                this.mChildHelper.attachViewToParent(view2, i2, view2.getLayoutParams(), false);
            } else if (view2.getParent() == this.mRecyclerView) {
                int indexOfChild = this.mChildHelper.indexOfChild(view2);
                if (i2 == -1) {
                    i2 = this.mChildHelper.getChildCount();
                }
                if (indexOfChild == -1) {
                    Throwable th2 = th;
                    new StringBuilder();
                    new IllegalStateException(sb.append("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:").append(this.mRecyclerView.indexOfChild(view2)).toString());
                    throw th2;
                } else if (indexOfChild != i2) {
                    this.mRecyclerView.mLayout.moveView(indexOfChild, i2);
                }
            } else {
                this.mChildHelper.addView(view2, i2, false);
                layoutParams.mInsetsDirty = true;
                if (this.mSmoothScroller != null && this.mSmoothScroller.isRunning()) {
                    this.mSmoothScroller.onChildAttachedToWindow(view2);
                }
            }
            if (layoutParams.mPendingInvalidate) {
                childViewHolderInt.itemView.invalidate();
                layoutParams.mPendingInvalidate = false;
            }
        }

        public void removeView(View view) {
            this.mChildHelper.removeView(view);
        }

        public void removeViewAt(int i) {
            int i2 = i;
            if (getChildAt(i2) != null) {
                this.mChildHelper.removeViewAt(i2);
            }
        }

        public void removeAllViews() {
            for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
                this.mChildHelper.removeViewAt(childCount);
            }
        }

        public int getBaseline() {
            return -1;
        }

        public int getPosition(View view) {
            return ((LayoutParams) view.getLayoutParams()).getViewLayoutPosition();
        }

        public int getItemViewType(View view) {
            return RecyclerView.getChildViewHolderInt(view).getItemViewType();
        }

        public View findViewByPosition(int i) {
            int i2 = i;
            int childCount = getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = getChildAt(i3);
                ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(childAt);
                if (childViewHolderInt != null && childViewHolderInt.getLayoutPosition() == i2 && !childViewHolderInt.shouldIgnore() && (this.mRecyclerView.mState.isPreLayout() || !childViewHolderInt.isRemoved())) {
                    return childAt;
                }
            }
            return null;
        }

        public void detachView(View view) {
            View view2 = view;
            int indexOfChild = this.mChildHelper.indexOfChild(view2);
            if (indexOfChild >= 0) {
                detachViewInternal(indexOfChild, view2);
            }
        }

        public void detachViewAt(int i) {
            int i2 = i;
            detachViewInternal(i2, getChildAt(i2));
        }

        private void detachViewInternal(int i, View view) {
            this.mChildHelper.detachViewFromParent(i);
        }

        public void attachView(View view, int i, LayoutParams layoutParams) {
            View view2 = view;
            int i2 = i;
            LayoutParams layoutParams2 = layoutParams;
            ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view2);
            if (childViewHolderInt.isRemoved()) {
                this.mRecyclerView.mViewInfoStore.addToDisappearedInLayout(childViewHolderInt);
            } else {
                this.mRecyclerView.mViewInfoStore.removeFromDisappearedInLayout(childViewHolderInt);
            }
            this.mChildHelper.attachViewToParent(view2, i2, layoutParams2, childViewHolderInt.isRemoved());
        }

        public void attachView(View view, int i) {
            View view2 = view;
            attachView(view2, i, (LayoutParams) view2.getLayoutParams());
        }

        public void attachView(View view) {
            attachView(view, -1);
        }

        public void removeDetachedView(View view) {
            this.mRecyclerView.removeDetachedView(view, false);
        }

        public void moveView(int i, int i2) {
            Throwable th;
            StringBuilder sb;
            int i3 = i;
            int i4 = i2;
            View childAt = getChildAt(i3);
            if (childAt == null) {
                Throwable th2 = th;
                new StringBuilder();
                new IllegalArgumentException(sb.append("Cannot move a child from non-existing index:").append(i3).toString());
                throw th2;
            }
            detachViewAt(i3);
            attachView(childAt, i4);
        }

        public void detachAndScrapView(View view, Recycler recycler) {
            View view2 = view;
            scrapOrRecycleView(recycler, this.mChildHelper.indexOfChild(view2), view2);
        }

        public void detachAndScrapViewAt(int i, Recycler recycler) {
            int i2 = i;
            scrapOrRecycleView(recycler, i2, getChildAt(i2));
        }

        public void removeAndRecycleView(View view, Recycler recycler) {
            View view2 = view;
            removeView(view2);
            recycler.recycleView(view2);
        }

        public void removeAndRecycleViewAt(int i, Recycler recycler) {
            int i2 = i;
            View childAt = getChildAt(i2);
            removeViewAt(i2);
            recycler.recycleView(childAt);
        }

        public int getChildCount() {
            return this.mChildHelper != null ? this.mChildHelper.getChildCount() : 0;
        }

        public View getChildAt(int i) {
            return this.mChildHelper != null ? this.mChildHelper.getChildAt(i) : null;
        }

        public int getWidth() {
            return this.mRecyclerView != null ? this.mRecyclerView.getWidth() : 0;
        }

        public int getHeight() {
            return this.mRecyclerView != null ? this.mRecyclerView.getHeight() : 0;
        }

        public int getPaddingLeft() {
            return this.mRecyclerView != null ? this.mRecyclerView.getPaddingLeft() : 0;
        }

        public int getPaddingTop() {
            return this.mRecyclerView != null ? this.mRecyclerView.getPaddingTop() : 0;
        }

        public int getPaddingRight() {
            return this.mRecyclerView != null ? this.mRecyclerView.getPaddingRight() : 0;
        }

        public int getPaddingBottom() {
            return this.mRecyclerView != null ? this.mRecyclerView.getPaddingBottom() : 0;
        }

        public int getPaddingStart() {
            return this.mRecyclerView != null ? ViewCompat.getPaddingStart(this.mRecyclerView) : 0;
        }

        public int getPaddingEnd() {
            return this.mRecyclerView != null ? ViewCompat.getPaddingEnd(this.mRecyclerView) : 0;
        }

        public boolean isFocused() {
            return this.mRecyclerView != null && this.mRecyclerView.isFocused();
        }

        public boolean hasFocus() {
            return this.mRecyclerView != null && this.mRecyclerView.hasFocus();
        }

        public View getFocusedChild() {
            if (this.mRecyclerView == null) {
                return null;
            }
            View focusedChild = this.mRecyclerView.getFocusedChild();
            if (focusedChild == null || this.mChildHelper.isHidden(focusedChild)) {
                return null;
            }
            return focusedChild;
        }

        public int getItemCount() {
            Adapter adapter = this.mRecyclerView != null ? this.mRecyclerView.getAdapter() : null;
            return adapter != null ? adapter.getItemCount() : 0;
        }

        public void offsetChildrenHorizontal(int i) {
            int i2 = i;
            if (this.mRecyclerView != null) {
                this.mRecyclerView.offsetChildrenHorizontal(i2);
            }
        }

        public void offsetChildrenVertical(int i) {
            int i2 = i;
            if (this.mRecyclerView != null) {
                this.mRecyclerView.offsetChildrenVertical(i2);
            }
        }

        public void ignoreView(View view) {
            Throwable th;
            View view2 = view;
            if (view2.getParent() != this.mRecyclerView || this.mRecyclerView.indexOfChild(view2) == -1) {
                Throwable th2 = th;
                new IllegalArgumentException("View should be fully attached to be ignored");
                throw th2;
            }
            ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view2);
            childViewHolderInt.addFlags(128);
            this.mRecyclerView.mViewInfoStore.removeViewHolder(childViewHolderInt);
        }

        public void stopIgnoringView(View view) {
            ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            childViewHolderInt.stopIgnoring();
            childViewHolderInt.resetInternal();
            childViewHolderInt.addFlags(4);
        }

        public void detachAndScrapAttachedViews(Recycler recycler) {
            Recycler recycler2 = recycler;
            for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
                scrapOrRecycleView(recycler2, childCount, getChildAt(childCount));
            }
        }

        private void scrapOrRecycleView(Recycler recycler, int i, View view) {
            Recycler recycler2 = recycler;
            int i2 = i;
            View view2 = view;
            ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view2);
            if (!childViewHolderInt.shouldIgnore()) {
                if (!childViewHolderInt.isInvalid() || childViewHolderInt.isRemoved() || this.mRecyclerView.mAdapter.hasStableIds()) {
                    detachViewAt(i2);
                    recycler2.scrapView(view2);
                    return;
                }
                removeViewAt(i2);
                recycler2.recycleViewHolderInternal(childViewHolderInt);
            }
        }

        /* access modifiers changed from: package-private */
        public void removeAndRecycleScrapInt(Recycler recycler) {
            Recycler recycler2 = recycler;
            int scrapCount = recycler2.getScrapCount();
            for (int i = scrapCount - 1; i >= 0; i--) {
                View scrapViewAt = recycler2.getScrapViewAt(i);
                ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(scrapViewAt);
                if (!childViewHolderInt.shouldIgnore()) {
                    childViewHolderInt.setIsRecyclable(false);
                    if (childViewHolderInt.isTmpDetached()) {
                        this.mRecyclerView.removeDetachedView(scrapViewAt, false);
                    }
                    if (this.mRecyclerView.mItemAnimator != null) {
                        this.mRecyclerView.mItemAnimator.endAnimation(childViewHolderInt);
                    }
                    childViewHolderInt.setIsRecyclable(true);
                    recycler2.quickRecycleScrapView(scrapViewAt);
                }
            }
            recycler2.clearScrap();
            if (scrapCount > 0) {
                this.mRecyclerView.invalidate();
            }
        }

        public void measureChild(View view, int i, int i2) {
            View view2 = view;
            LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
            Rect itemDecorInsetsForChild = this.mRecyclerView.getItemDecorInsetsForChild(view2);
            int i3 = i + itemDecorInsetsForChild.left + itemDecorInsetsForChild.right;
            int i4 = i2 + itemDecorInsetsForChild.top + itemDecorInsetsForChild.bottom;
            view2.measure(getChildMeasureSpec(getWidth(), getPaddingLeft() + getPaddingRight() + i3, layoutParams.width, canScrollHorizontally()), getChildMeasureSpec(getHeight(), getPaddingTop() + getPaddingBottom() + i4, layoutParams.height, canScrollVertically()));
        }

        public void measureChildWithMargins(View view, int i, int i2) {
            View view2 = view;
            LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
            Rect itemDecorInsetsForChild = this.mRecyclerView.getItemDecorInsetsForChild(view2);
            int i3 = i + itemDecorInsetsForChild.left + itemDecorInsetsForChild.right;
            int i4 = i2 + itemDecorInsetsForChild.top + itemDecorInsetsForChild.bottom;
            view2.measure(getChildMeasureSpec(getWidth(), getPaddingLeft() + getPaddingRight() + layoutParams.leftMargin + layoutParams.rightMargin + i3, layoutParams.width, canScrollHorizontally()), getChildMeasureSpec(getHeight(), getPaddingTop() + getPaddingBottom() + layoutParams.topMargin + layoutParams.bottomMargin + i4, layoutParams.height, canScrollVertically()));
        }

        public static int getChildMeasureSpec(int i, int i2, int i3, boolean z) {
            int i4 = i3;
            int max = Math.max(0, i - i2);
            int i5 = 0;
            int i6 = 0;
            if (z) {
                if (i4 >= 0) {
                    i5 = i4;
                    i6 = 1073741824;
                } else {
                    i5 = 0;
                    i6 = 0;
                }
            } else if (i4 >= 0) {
                i5 = i4;
                i6 = 1073741824;
            } else if (i4 == -1) {
                i5 = max;
                i6 = 1073741824;
            } else if (i4 == -2) {
                i5 = max;
                i6 = Integer.MIN_VALUE;
            }
            return View.MeasureSpec.makeMeasureSpec(i5, i6);
        }

        public int getDecoratedMeasuredWidth(View view) {
            View view2 = view;
            Rect rect = ((LayoutParams) view2.getLayoutParams()).mDecorInsets;
            return view2.getMeasuredWidth() + rect.left + rect.right;
        }

        public int getDecoratedMeasuredHeight(View view) {
            View view2 = view;
            Rect rect = ((LayoutParams) view2.getLayoutParams()).mDecorInsets;
            return view2.getMeasuredHeight() + rect.top + rect.bottom;
        }

        public void layoutDecorated(View view, int i, int i2, int i3, int i4) {
            View view2 = view;
            Rect rect = ((LayoutParams) view2.getLayoutParams()).mDecorInsets;
            view2.layout(i + rect.left, i2 + rect.top, i3 - rect.right, i4 - rect.bottom);
        }

        public int getDecoratedLeft(View view) {
            View view2 = view;
            return view2.getLeft() - getLeftDecorationWidth(view2);
        }

        public int getDecoratedTop(View view) {
            View view2 = view;
            return view2.getTop() - getTopDecorationHeight(view2);
        }

        public int getDecoratedRight(View view) {
            View view2 = view;
            return view2.getRight() + getRightDecorationWidth(view2);
        }

        public int getDecoratedBottom(View view) {
            View view2 = view;
            return view2.getBottom() + getBottomDecorationHeight(view2);
        }

        public void calculateItemDecorationsForChild(View view, Rect rect) {
            View view2 = view;
            Rect rect2 = rect;
            if (this.mRecyclerView == null) {
                rect2.set(0, 0, 0, 0);
                return;
            }
            rect2.set(this.mRecyclerView.getItemDecorInsetsForChild(view2));
        }

        public int getTopDecorationHeight(View view) {
            return ((LayoutParams) view.getLayoutParams()).mDecorInsets.top;
        }

        public int getBottomDecorationHeight(View view) {
            return ((LayoutParams) view.getLayoutParams()).mDecorInsets.bottom;
        }

        public int getLeftDecorationWidth(View view) {
            return ((LayoutParams) view.getLayoutParams()).mDecorInsets.left;
        }

        public int getRightDecorationWidth(View view) {
            return ((LayoutParams) view.getLayoutParams()).mDecorInsets.right;
        }

        @Nullable
        public View onFocusSearchFailed(View view, int i, Recycler recycler, State state) {
            return null;
        }

        public View onInterceptFocusSearch(View view, int i) {
            return null;
        }

        public boolean requestChildRectangleOnScreen(RecyclerView recyclerView, View view, Rect rect, boolean z) {
            int min;
            RecyclerView recyclerView2 = recyclerView;
            View view2 = view;
            Rect rect2 = rect;
            boolean z2 = z;
            int paddingLeft = getPaddingLeft();
            int paddingTop = getPaddingTop();
            int width = getWidth() - getPaddingRight();
            int height = getHeight() - getPaddingBottom();
            int left = view2.getLeft() + rect2.left;
            int top = view2.getTop() + rect2.top;
            int width2 = left + rect2.width();
            int height2 = top + rect2.height();
            int min2 = Math.min(0, left - paddingLeft);
            int min3 = Math.min(0, top - paddingTop);
            int max = Math.max(0, width2 - width);
            int max2 = Math.max(0, height2 - height);
            if (getLayoutDirection() == 1) {
                min = max != 0 ? max : Math.max(min2, width2 - width);
            } else {
                min = min2 != 0 ? min2 : Math.min(left - paddingLeft, max);
            }
            int min4 = min3 != 0 ? min3 : Math.min(top - paddingTop, max2);
            if (min == 0 && min4 == 0) {
                return false;
            }
            if (z2) {
                recyclerView2.scrollBy(min, min4);
            } else {
                recyclerView2.smoothScrollBy(min, min4);
            }
            return true;
        }

        @Deprecated
        public boolean onRequestChildFocus(RecyclerView recyclerView, View view, View view2) {
            return isSmoothScrolling() || recyclerView.isComputingLayout();
        }

        public boolean onRequestChildFocus(RecyclerView recyclerView, State state, View view, View view2) {
            return onRequestChildFocus(recyclerView, view, view2);
        }

        public void onAdapterChanged(Adapter adapter, Adapter adapter2) {
        }

        public boolean onAddFocusables(RecyclerView recyclerView, ArrayList<View> arrayList, int i, int i2) {
            return false;
        }

        public void onItemsChanged(RecyclerView recyclerView) {
        }

        public void onItemsAdded(RecyclerView recyclerView, int i, int i2) {
        }

        public void onItemsRemoved(RecyclerView recyclerView, int i, int i2) {
        }

        public void onItemsUpdated(RecyclerView recyclerView, int i, int i2) {
        }

        public void onItemsUpdated(RecyclerView recyclerView, int i, int i2, Object obj) {
            onItemsUpdated(recyclerView, i, i2);
        }

        public void onItemsMoved(RecyclerView recyclerView, int i, int i2, int i3) {
        }

        public int computeHorizontalScrollExtent(State state) {
            return 0;
        }

        public int computeHorizontalScrollOffset(State state) {
            return 0;
        }

        public int computeHorizontalScrollRange(State state) {
            return 0;
        }

        public int computeVerticalScrollExtent(State state) {
            return 0;
        }

        public int computeVerticalScrollOffset(State state) {
            return 0;
        }

        public int computeVerticalScrollRange(State state) {
            return 0;
        }

        public void onMeasure(Recycler recycler, State state, int i, int i2) {
            this.mRecyclerView.defaultOnMeasure(i, i2);
        }

        public void setMeasuredDimension(int i, int i2) {
            this.mRecyclerView.setMeasuredDimension(i, i2);
        }

        public int getMinimumWidth() {
            return ViewCompat.getMinimumWidth(this.mRecyclerView);
        }

        public int getMinimumHeight() {
            return ViewCompat.getMinimumHeight(this.mRecyclerView);
        }

        public Parcelable onSaveInstanceState() {
            return null;
        }

        public void onRestoreInstanceState(Parcelable parcelable) {
        }

        /* access modifiers changed from: package-private */
        public void stopSmoothScroller() {
            if (this.mSmoothScroller != null) {
                this.mSmoothScroller.stop();
            }
        }

        /* access modifiers changed from: private */
        public void onSmoothScrollerStopped(SmoothScroller smoothScroller) {
            if (this.mSmoothScroller == smoothScroller) {
                this.mSmoothScroller = null;
            }
        }

        public void onScrollStateChanged(int i) {
        }

        public void removeAndRecycleAllViews(Recycler recycler) {
            Recycler recycler2 = recycler;
            for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
                if (!RecyclerView.getChildViewHolderInt(getChildAt(childCount)).shouldIgnore()) {
                    removeAndRecycleViewAt(childCount, recycler2);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            onInitializeAccessibilityNodeInfo(this.mRecyclerView.mRecycler, this.mRecyclerView.mState, accessibilityNodeInfoCompat);
        }

        public void onInitializeAccessibilityNodeInfo(Recycler recycler, State state, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            Recycler recycler2 = recycler;
            State state2 = state;
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
            if (ViewCompat.canScrollVertically(this.mRecyclerView, -1) || ViewCompat.canScrollHorizontally(this.mRecyclerView, -1)) {
                accessibilityNodeInfoCompat2.addAction(8192);
                accessibilityNodeInfoCompat2.setScrollable(true);
            }
            if (ViewCompat.canScrollVertically(this.mRecyclerView, 1) || ViewCompat.canScrollHorizontally(this.mRecyclerView, 1)) {
                accessibilityNodeInfoCompat2.addAction(4096);
                accessibilityNodeInfoCompat2.setScrollable(true);
            }
            accessibilityNodeInfoCompat2.setCollectionInfo(AccessibilityNodeInfoCompat.CollectionInfoCompat.obtain(getRowCountForAccessibility(recycler2, state2), getColumnCountForAccessibility(recycler2, state2), isLayoutHierarchical(recycler2, state2), getSelectionModeForAccessibility(recycler2, state2)));
        }

        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            onInitializeAccessibilityEvent(this.mRecyclerView.mRecycler, this.mRecyclerView.mState, accessibilityEvent);
        }

        public void onInitializeAccessibilityEvent(Recycler recycler, State state, AccessibilityEvent accessibilityEvent) {
            AccessibilityRecordCompat asRecord = AccessibilityEventCompat.asRecord(accessibilityEvent);
            if (this.mRecyclerView != null && asRecord != null) {
                asRecord.setScrollable(ViewCompat.canScrollVertically(this.mRecyclerView, 1) || ViewCompat.canScrollVertically(this.mRecyclerView, -1) || ViewCompat.canScrollHorizontally(this.mRecyclerView, -1) || ViewCompat.canScrollHorizontally(this.mRecyclerView, 1));
                if (this.mRecyclerView.mAdapter != null) {
                    asRecord.setItemCount(this.mRecyclerView.mAdapter.getItemCount());
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void onInitializeAccessibilityNodeInfoForItem(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            View view2 = view;
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
            ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(view2);
            if (childViewHolderInt != null && !childViewHolderInt.isRemoved() && !this.mChildHelper.isHidden(childViewHolderInt.itemView)) {
                onInitializeAccessibilityNodeInfoForItem(this.mRecyclerView.mRecycler, this.mRecyclerView.mState, view2, accessibilityNodeInfoCompat2);
            }
        }

        public void onInitializeAccessibilityNodeInfoForItem(Recycler recycler, State state, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            View view2 = view;
            accessibilityNodeInfoCompat.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(canScrollVertically() ? getPosition(view2) : 0, 1, canScrollHorizontally() ? getPosition(view2) : 0, 1, false, false));
        }

        public void requestSimpleAnimationsInNextLayout() {
            this.mRequestedSimpleAnimations = true;
        }

        public int getSelectionModeForAccessibility(Recycler recycler, State state) {
            return 0;
        }

        public int getRowCountForAccessibility(Recycler recycler, State state) {
            if (this.mRecyclerView == null || this.mRecyclerView.mAdapter == null) {
                return 1;
            }
            return canScrollVertically() ? this.mRecyclerView.mAdapter.getItemCount() : 1;
        }

        public int getColumnCountForAccessibility(Recycler recycler, State state) {
            if (this.mRecyclerView == null || this.mRecyclerView.mAdapter == null) {
                return 1;
            }
            return canScrollHorizontally() ? this.mRecyclerView.mAdapter.getItemCount() : 1;
        }

        public boolean isLayoutHierarchical(Recycler recycler, State state) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean performAccessibilityAction(int i, Bundle bundle) {
            return performAccessibilityAction(this.mRecyclerView.mRecycler, this.mRecyclerView.mState, i, bundle);
        }

        public boolean performAccessibilityAction(Recycler recycler, State state, int i, Bundle bundle) {
            int i2 = i;
            if (this.mRecyclerView == null) {
                return false;
            }
            int i3 = 0;
            int i4 = 0;
            switch (i2) {
                case 4096:
                    if (ViewCompat.canScrollVertically(this.mRecyclerView, 1)) {
                        i3 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                    }
                    if (ViewCompat.canScrollHorizontally(this.mRecyclerView, 1)) {
                        i4 = (getWidth() - getPaddingLeft()) - getPaddingRight();
                        break;
                    }
                    break;
                case 8192:
                    if (ViewCompat.canScrollVertically(this.mRecyclerView, -1)) {
                        i3 = -((getHeight() - getPaddingTop()) - getPaddingBottom());
                    }
                    if (ViewCompat.canScrollHorizontally(this.mRecyclerView, -1)) {
                        i4 = -((getWidth() - getPaddingLeft()) - getPaddingRight());
                        break;
                    }
                    break;
            }
            if (i3 == 0 && i4 == 0) {
                return false;
            }
            this.mRecyclerView.scrollBy(i4, i3);
            return true;
        }

        /* access modifiers changed from: package-private */
        public boolean performAccessibilityActionForItem(View view, int i, Bundle bundle) {
            return performAccessibilityActionForItem(this.mRecyclerView.mRecycler, this.mRecyclerView.mState, view, i, bundle);
        }

        public boolean performAccessibilityActionForItem(Recycler recycler, State state, View view, int i, Bundle bundle) {
            return false;
        }

        public static Properties getProperties(Context context, AttributeSet attributeSet, int i, int i2) {
            Properties properties;
            new Properties();
            Properties properties2 = properties;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.RecyclerView, i, i2);
            properties2.orientation = obtainStyledAttributes.getInt(R.styleable.RecyclerView_android_orientation, 1);
            properties2.spanCount = obtainStyledAttributes.getInt(R.styleable.RecyclerView_spanCount, 1);
            properties2.reverseLayout = obtainStyledAttributes.getBoolean(R.styleable.RecyclerView_reverseLayout, false);
            properties2.stackFromEnd = obtainStyledAttributes.getBoolean(R.styleable.RecyclerView_stackFromEnd, false);
            obtainStyledAttributes.recycle();
            return properties2;
        }
    }

    public static abstract class ItemDecoration {
        public void onDraw(Canvas canvas, RecyclerView recyclerView, State state) {
            onDraw(canvas, recyclerView);
        }

        @Deprecated
        public void onDraw(Canvas canvas, RecyclerView recyclerView) {
        }

        public void onDrawOver(Canvas canvas, RecyclerView recyclerView, State state) {
            onDrawOver(canvas, recyclerView);
        }

        @Deprecated
        public void onDrawOver(Canvas canvas, RecyclerView recyclerView) {
        }

        @Deprecated
        public void getItemOffsets(Rect rect, int i, RecyclerView recyclerView) {
            rect.set(0, 0, 0, 0);
        }

        public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, State state) {
            getItemOffsets(rect, ((LayoutParams) view.getLayoutParams()).getViewLayoutPosition(), recyclerView);
        }
    }

    public static class SimpleOnItemTouchListener implements OnItemTouchListener {
        public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
            return false;
        }

        public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        }

        public void onRequestDisallowInterceptTouchEvent(boolean z) {
        }
    }

    public static abstract class OnScrollListener {
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        }

        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        }
    }

    public static abstract class ViewHolder {
        static final int FLAG_ADAPTER_FULLUPDATE = 1024;
        static final int FLAG_ADAPTER_POSITION_UNKNOWN = 512;
        static final int FLAG_APPEARED_IN_PRE_LAYOUT = 4096;
        static final int FLAG_BOUNCED_FROM_HIDDEN_LIST = 8192;
        static final int FLAG_BOUND = 1;
        static final int FLAG_IGNORE = 128;
        static final int FLAG_INVALID = 4;
        static final int FLAG_MOVED = 2048;
        static final int FLAG_NOT_RECYCLABLE = 16;
        static final int FLAG_REMOVED = 8;
        static final int FLAG_RETURNED_FROM_SCRAP = 32;
        static final int FLAG_TMP_DETACHED = 256;
        static final int FLAG_UPDATE = 2;
        private static final List<Object> FULLUPDATE_PAYLOADS = Collections.EMPTY_LIST;
        public final View itemView;
        /* access modifiers changed from: private */
        public int mFlags;
        /* access modifiers changed from: private */
        public boolean mInChangeScrap = false;
        private int mIsRecyclableCount = 0;
        long mItemId = -1;
        int mItemViewType = -1;
        int mOldPosition = -1;
        RecyclerView mOwnerRecyclerView;
        List<Object> mPayloads = null;
        int mPosition = -1;
        int mPreLayoutPosition = -1;
        private Recycler mScrapContainer = null;
        ViewHolder mShadowedHolder = null;
        ViewHolder mShadowingHolder = null;
        List<Object> mUnmodifiedPayloads = null;
        private int mWasImportantForAccessibilityBeforeHidden = 0;

        static /* synthetic */ Recycler access$4802(ViewHolder viewHolder, Recycler recycler) {
            Recycler recycler2 = recycler;
            Recycler recycler3 = recycler2;
            viewHolder.mScrapContainer = recycler3;
            return recycler2;
        }

        static /* synthetic */ boolean access$4902(ViewHolder viewHolder, boolean z) {
            boolean z2 = z;
            boolean z3 = z2;
            viewHolder.mInChangeScrap = z3;
            return z2;
        }

        public ViewHolder(View view) {
            Throwable th;
            View view2 = view;
            if (view2 == null) {
                Throwable th2 = th;
                new IllegalArgumentException("itemView may not be null");
                throw th2;
            }
            this.itemView = view2;
        }

        /* access modifiers changed from: package-private */
        public void flagRemovedAndOffsetPosition(int i, int i2, boolean z) {
            addFlags(8);
            offsetPosition(i2, z);
            this.mPosition = i;
        }

        /* access modifiers changed from: package-private */
        public void offsetPosition(int i, boolean z) {
            int i2 = i;
            boolean z2 = z;
            if (this.mOldPosition == -1) {
                this.mOldPosition = this.mPosition;
            }
            if (this.mPreLayoutPosition == -1) {
                this.mPreLayoutPosition = this.mPosition;
            }
            if (z2) {
                this.mPreLayoutPosition = this.mPreLayoutPosition + i2;
            }
            this.mPosition = this.mPosition + i2;
            if (this.itemView.getLayoutParams() != null) {
                ((LayoutParams) this.itemView.getLayoutParams()).mInsetsDirty = true;
            }
        }

        /* access modifiers changed from: package-private */
        public void clearOldPosition() {
            this.mOldPosition = -1;
            this.mPreLayoutPosition = -1;
        }

        /* access modifiers changed from: package-private */
        public void saveOldPosition() {
            if (this.mOldPosition == -1) {
                this.mOldPosition = this.mPosition;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean shouldIgnore() {
            return (this.mFlags & 128) != 0;
        }

        @Deprecated
        public final int getPosition() {
            return this.mPreLayoutPosition == -1 ? this.mPosition : this.mPreLayoutPosition;
        }

        public final int getLayoutPosition() {
            return this.mPreLayoutPosition == -1 ? this.mPosition : this.mPreLayoutPosition;
        }

        public final int getAdapterPosition() {
            if (this.mOwnerRecyclerView == null) {
                return -1;
            }
            return this.mOwnerRecyclerView.getAdapterPositionFor(this);
        }

        public final int getOldPosition() {
            return this.mOldPosition;
        }

        public final long getItemId() {
            return this.mItemId;
        }

        public final int getItemViewType() {
            return this.mItemViewType;
        }

        /* access modifiers changed from: package-private */
        public boolean isScrap() {
            return this.mScrapContainer != null;
        }

        /* access modifiers changed from: package-private */
        public void unScrap() {
            this.mScrapContainer.unscrapView(this);
        }

        /* access modifiers changed from: package-private */
        public boolean wasReturnedFromScrap() {
            return (this.mFlags & 32) != 0;
        }

        /* access modifiers changed from: package-private */
        public void clearReturnedFromScrapFlag() {
            this.mFlags = this.mFlags & -33;
        }

        /* access modifiers changed from: package-private */
        public void clearTmpDetachFlag() {
            this.mFlags = this.mFlags & -257;
        }

        /* access modifiers changed from: package-private */
        public void stopIgnoring() {
            this.mFlags = this.mFlags & -129;
        }

        /* access modifiers changed from: package-private */
        public void setScrapContainer(Recycler recycler, boolean z) {
            this.mScrapContainer = recycler;
            this.mInChangeScrap = z;
        }

        /* access modifiers changed from: package-private */
        public boolean isInvalid() {
            return (this.mFlags & 4) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean needsUpdate() {
            return (this.mFlags & 2) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isBound() {
            return (this.mFlags & 1) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isRemoved() {
            return (this.mFlags & 8) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean hasAnyOfTheFlags(int i) {
            return (this.mFlags & i) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isTmpDetached() {
            return (this.mFlags & 256) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isAdapterPositionUnknown() {
            return (this.mFlags & 512) != 0 || isInvalid();
        }

        /* access modifiers changed from: package-private */
        public void setFlags(int i, int i2) {
            int i3 = i2;
            this.mFlags = (this.mFlags & (i3 ^ -1)) | (i & i3);
        }

        /* access modifiers changed from: package-private */
        public void addFlags(int i) {
            this.mFlags = this.mFlags | i;
        }

        /* access modifiers changed from: package-private */
        public void addChangePayload(Object obj) {
            Object obj2 = obj;
            if (obj2 == null) {
                addFlags(1024);
            } else if ((this.mFlags & 1024) == 0) {
                createPayloadsIfNeeded();
                boolean add = this.mPayloads.add(obj2);
            }
        }

        private void createPayloadsIfNeeded() {
            List<Object> list;
            if (this.mPayloads == null) {
                new ArrayList();
                this.mPayloads = list;
                this.mUnmodifiedPayloads = Collections.unmodifiableList(this.mPayloads);
            }
        }

        /* access modifiers changed from: package-private */
        public void clearPayload() {
            if (this.mPayloads != null) {
                this.mPayloads.clear();
            }
            this.mFlags = this.mFlags & -1025;
        }

        /* access modifiers changed from: package-private */
        public List<Object> getUnmodifiedPayloads() {
            if ((this.mFlags & 1024) != 0) {
                return FULLUPDATE_PAYLOADS;
            }
            if (this.mPayloads == null || this.mPayloads.size() == 0) {
                return FULLUPDATE_PAYLOADS;
            }
            return this.mUnmodifiedPayloads;
        }

        /* access modifiers changed from: package-private */
        public void resetInternal() {
            this.mFlags = 0;
            this.mPosition = -1;
            this.mOldPosition = -1;
            this.mItemId = -1;
            this.mPreLayoutPosition = -1;
            this.mIsRecyclableCount = 0;
            this.mShadowedHolder = null;
            this.mShadowingHolder = null;
            clearPayload();
            this.mWasImportantForAccessibilityBeforeHidden = 0;
        }

        /* access modifiers changed from: private */
        public void onEnteredHiddenState() {
            this.mWasImportantForAccessibilityBeforeHidden = ViewCompat.getImportantForAccessibility(this.itemView);
            ViewCompat.setImportantForAccessibility(this.itemView, 4);
        }

        /* access modifiers changed from: private */
        public void onLeftHiddenState() {
            ViewCompat.setImportantForAccessibility(this.itemView, this.mWasImportantForAccessibilityBeforeHidden);
            this.mWasImportantForAccessibilityBeforeHidden = 0;
        }

        public String toString() {
            StringBuilder sb;
            StringBuilder sb2;
            StringBuilder sb3;
            new StringBuilder();
            new StringBuilder(sb2.append("ViewHolder{").append(Integer.toHexString(hashCode())).append(" position=").append(this.mPosition).append(" id=").append(this.mItemId).append(", oldPos=").append(this.mOldPosition).append(", pLpos:").append(this.mPreLayoutPosition).toString());
            StringBuilder sb4 = sb;
            if (isScrap()) {
                StringBuilder append = sb4.append(" scrap ").append(this.mInChangeScrap ? "[changeScrap]" : "[attachedScrap]");
            }
            if (isInvalid()) {
                StringBuilder append2 = sb4.append(" invalid");
            }
            if (!isBound()) {
                StringBuilder append3 = sb4.append(" unbound");
            }
            if (needsUpdate()) {
                StringBuilder append4 = sb4.append(" update");
            }
            if (isRemoved()) {
                StringBuilder append5 = sb4.append(" removed");
            }
            if (shouldIgnore()) {
                StringBuilder append6 = sb4.append(" ignored");
            }
            if (isTmpDetached()) {
                StringBuilder append7 = sb4.append(" tmpDetached");
            }
            if (!isRecyclable()) {
                new StringBuilder();
                StringBuilder append8 = sb4.append(sb3.append(" not recyclable(").append(this.mIsRecyclableCount).append(")").toString());
            }
            if (isAdapterPositionUnknown()) {
                StringBuilder append9 = sb4.append(" undefined adapter position");
            }
            if (this.itemView.getParent() == null) {
                StringBuilder append10 = sb4.append(" no parent");
            }
            StringBuilder append11 = sb4.append("}");
            return sb4.toString();
        }

        public final void setIsRecyclable(boolean z) {
            StringBuilder sb;
            boolean z2 = z;
            this.mIsRecyclableCount = z2 ? this.mIsRecyclableCount - 1 : this.mIsRecyclableCount + 1;
            if (this.mIsRecyclableCount < 0) {
                this.mIsRecyclableCount = 0;
                new StringBuilder();
                int e = Log.e("View", sb.append("isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for ").append(this).toString());
            } else if (!z2 && this.mIsRecyclableCount == 1) {
                this.mFlags = this.mFlags | 16;
            } else if (z2 && this.mIsRecyclableCount == 0) {
                this.mFlags = this.mFlags & -17;
            }
        }

        public final boolean isRecyclable() {
            return (this.mFlags & 16) == 0 && !ViewCompat.hasTransientState(this.itemView);
        }

        /* access modifiers changed from: private */
        public boolean shouldBeKeptAsChild() {
            return (this.mFlags & 16) != 0;
        }

        /* access modifiers changed from: private */
        public boolean doesTransientStatePreventRecycling() {
            return (this.mFlags & 16) == 0 && ViewCompat.hasTransientState(this.itemView);
        }

        /* access modifiers changed from: package-private */
        public boolean isUpdated() {
            return (this.mFlags & 2) != 0;
        }
    }

    /* access modifiers changed from: private */
    public int getAdapterPositionFor(ViewHolder viewHolder) {
        ViewHolder viewHolder2 = viewHolder;
        if (viewHolder2.hasAnyOfTheFlags(524) || !viewHolder2.isBound()) {
            return -1;
        }
        return this.mAdapterHelper.applyPendingUpdatesToPosition(viewHolder2.mPosition);
    }

    public void setNestedScrollingEnabled(boolean z) {
        this.mScrollingChildHelper.setNestedScrollingEnabled(z);
    }

    public boolean isNestedScrollingEnabled() {
        return this.mScrollingChildHelper.isNestedScrollingEnabled();
    }

    public boolean startNestedScroll(int i) {
        return this.mScrollingChildHelper.startNestedScroll(i);
    }

    public void stopNestedScroll() {
        this.mScrollingChildHelper.stopNestedScroll();
    }

    public boolean hasNestedScrollingParent() {
        return this.mScrollingChildHelper.hasNestedScrollingParent();
    }

    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return this.mScrollingChildHelper.dispatchNestedScroll(i, i2, i3, i4, iArr);
    }

    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return this.mScrollingChildHelper.dispatchNestedPreScroll(i, i2, iArr, iArr2);
    }

    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return this.mScrollingChildHelper.dispatchNestedFling(f, f2, z);
    }

    public boolean dispatchNestedPreFling(float f, float f2) {
        return this.mScrollingChildHelper.dispatchNestedPreFling(f, f2);
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        final Rect mDecorInsets;
        boolean mInsetsDirty = true;
        boolean mPendingInvalidate = false;
        ViewHolder mViewHolder;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            Rect rect;
            new Rect();
            this.mDecorInsets = rect;
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            Rect rect;
            new Rect();
            this.mDecorInsets = rect;
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            Rect rect;
            new Rect();
            this.mDecorInsets = rect;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            Rect rect;
            new Rect();
            this.mDecorInsets = rect;
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.LayoutParams) layoutParams);
            Rect rect;
            new Rect();
            this.mDecorInsets = rect;
        }

        public boolean viewNeedsUpdate() {
            return this.mViewHolder.needsUpdate();
        }

        public boolean isViewInvalid() {
            return this.mViewHolder.isInvalid();
        }

        public boolean isItemRemoved() {
            return this.mViewHolder.isRemoved();
        }

        public boolean isItemChanged() {
            return this.mViewHolder.isUpdated();
        }

        public int getViewPosition() {
            return this.mViewHolder.getPosition();
        }

        public int getViewLayoutPosition() {
            return this.mViewHolder.getLayoutPosition();
        }

        public int getViewAdapterPosition() {
            return this.mViewHolder.getAdapterPosition();
        }
    }

    public static abstract class AdapterDataObserver {
        public void onChanged() {
        }

        public void onItemRangeChanged(int i, int i2) {
        }

        public void onItemRangeChanged(int i, int i2, Object obj) {
            onItemRangeChanged(i, i2);
        }

        public void onItemRangeInserted(int i, int i2) {
        }

        public void onItemRangeRemoved(int i, int i2) {
        }

        public void onItemRangeMoved(int i, int i2, int i3) {
        }
    }

    public static abstract class SmoothScroller {
        private LayoutManager mLayoutManager;
        private boolean mPendingInitialRun;
        private RecyclerView mRecyclerView;
        private final Action mRecyclingAction;
        private boolean mRunning;
        private int mTargetPosition = -1;
        private View mTargetView;

        /* access modifiers changed from: protected */
        public abstract void onSeekTargetStep(int i, int i2, State state, Action action);

        /* access modifiers changed from: protected */
        public abstract void onStart();

        /* access modifiers changed from: protected */
        public abstract void onStop();

        /* access modifiers changed from: protected */
        public abstract void onTargetFound(View view, State state, Action action);

        public SmoothScroller() {
            Action action;
            new Action(0, 0);
            this.mRecyclingAction = action;
        }

        /* access modifiers changed from: package-private */
        public void start(RecyclerView recyclerView, LayoutManager layoutManager) {
            Throwable th;
            this.mRecyclerView = recyclerView;
            this.mLayoutManager = layoutManager;
            if (this.mTargetPosition == -1) {
                Throwable th2 = th;
                new IllegalArgumentException("Invalid target position");
                throw th2;
            }
            int access$5602 = State.access$5602(this.mRecyclerView.mState, this.mTargetPosition);
            this.mRunning = true;
            this.mPendingInitialRun = true;
            this.mTargetView = findViewByPosition(getTargetPosition());
            onStart();
            this.mRecyclerView.mViewFlinger.postOnAnimation();
        }

        public void setTargetPosition(int i) {
            this.mTargetPosition = i;
        }

        @Nullable
        public LayoutManager getLayoutManager() {
            return this.mLayoutManager;
        }

        /* access modifiers changed from: protected */
        public final void stop() {
            if (this.mRunning) {
                onStop();
                int access$5602 = State.access$5602(this.mRecyclerView.mState, -1);
                this.mTargetView = null;
                this.mTargetPosition = -1;
                this.mPendingInitialRun = false;
                this.mRunning = false;
                this.mLayoutManager.onSmoothScrollerStopped(this);
                this.mLayoutManager = null;
                this.mRecyclerView = null;
            }
        }

        public boolean isPendingInitialRun() {
            return this.mPendingInitialRun;
        }

        public boolean isRunning() {
            return this.mRunning;
        }

        public int getTargetPosition() {
            return this.mTargetPosition;
        }

        /* access modifiers changed from: private */
        public void onAnimation(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            RecyclerView recyclerView = this.mRecyclerView;
            if (!this.mRunning || this.mTargetPosition == -1 || recyclerView == null) {
                stop();
            }
            this.mPendingInitialRun = false;
            if (this.mTargetView != null) {
                if (getChildPosition(this.mTargetView) == this.mTargetPosition) {
                    onTargetFound(this.mTargetView, recyclerView.mState, this.mRecyclingAction);
                    this.mRecyclingAction.runIfNecessary(recyclerView);
                    stop();
                } else {
                    int e = Log.e(RecyclerView.TAG, "Passed over target position while smooth scrolling.");
                    this.mTargetView = null;
                }
            }
            if (this.mRunning) {
                onSeekTargetStep(i3, i4, recyclerView.mState, this.mRecyclingAction);
                boolean hasJumpTarget = this.mRecyclingAction.hasJumpTarget();
                this.mRecyclingAction.runIfNecessary(recyclerView);
                if (!hasJumpTarget) {
                    return;
                }
                if (this.mRunning) {
                    this.mPendingInitialRun = true;
                    recyclerView.mViewFlinger.postOnAnimation();
                    return;
                }
                stop();
            }
        }

        public int getChildPosition(View view) {
            return this.mRecyclerView.getChildLayoutPosition(view);
        }

        public int getChildCount() {
            return this.mRecyclerView.mLayout.getChildCount();
        }

        public View findViewByPosition(int i) {
            return this.mRecyclerView.mLayout.findViewByPosition(i);
        }

        @Deprecated
        public void instantScrollToPosition(int i) {
            this.mRecyclerView.scrollToPosition(i);
        }

        /* access modifiers changed from: protected */
        public void onChildAttachedToWindow(View view) {
            View view2 = view;
            if (getChildPosition(view2) == getTargetPosition()) {
                this.mTargetView = view2;
            }
        }

        /* access modifiers changed from: protected */
        public void normalize(PointF pointF) {
            PointF pointF2 = pointF;
            double sqrt = Math.sqrt((double) ((pointF2.x * pointF2.x) + (pointF2.y * pointF2.y)));
            PointF pointF3 = pointF2;
            pointF3.x = (float) (((double) pointF3.x) / sqrt);
            PointF pointF4 = pointF2;
            pointF4.y = (float) (((double) pointF4.y) / sqrt);
        }

        public static class Action {
            public static final int UNDEFINED_DURATION = Integer.MIN_VALUE;
            private boolean changed;
            private int consecutiveUpdates;
            private int mDuration;
            private int mDx;
            private int mDy;
            private Interpolator mInterpolator;
            private int mJumpToPosition;

            public Action(int i, int i2) {
                this(i, i2, Integer.MIN_VALUE, null);
            }

            public Action(int i, int i2, int i3) {
                this(i, i2, i3, null);
            }

            public Action(int i, int i2, int i3, Interpolator interpolator) {
                this.mJumpToPosition = -1;
                this.changed = false;
                this.consecutiveUpdates = 0;
                this.mDx = i;
                this.mDy = i2;
                this.mDuration = i3;
                this.mInterpolator = interpolator;
            }

            public void jumpTo(int i) {
                this.mJumpToPosition = i;
            }

            /* access modifiers changed from: package-private */
            public boolean hasJumpTarget() {
                return this.mJumpToPosition >= 0;
            }

            /* access modifiers changed from: private */
            public void runIfNecessary(RecyclerView recyclerView) {
                RecyclerView recyclerView2 = recyclerView;
                if (this.mJumpToPosition >= 0) {
                    int i = this.mJumpToPosition;
                    this.mJumpToPosition = -1;
                    recyclerView2.jumpToPositionForSmoothScroller(i);
                    this.changed = false;
                } else if (this.changed) {
                    validate();
                    if (this.mInterpolator != null) {
                        recyclerView2.mViewFlinger.smoothScrollBy(this.mDx, this.mDy, this.mDuration, this.mInterpolator);
                    } else if (this.mDuration == Integer.MIN_VALUE) {
                        recyclerView2.mViewFlinger.smoothScrollBy(this.mDx, this.mDy);
                    } else {
                        recyclerView2.mViewFlinger.smoothScrollBy(this.mDx, this.mDy, this.mDuration);
                    }
                    this.consecutiveUpdates = this.consecutiveUpdates + 1;
                    if (this.consecutiveUpdates > 10) {
                        int e = Log.e(RecyclerView.TAG, "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary");
                    }
                    this.changed = false;
                } else {
                    this.consecutiveUpdates = 0;
                }
            }

            private void validate() {
                Throwable th;
                Throwable th2;
                if (this.mInterpolator != null && this.mDuration < 1) {
                    Throwable th3 = th2;
                    new IllegalStateException("If you provide an interpolator, you must set a positive duration");
                    throw th3;
                } else if (this.mDuration < 1) {
                    Throwable th4 = th;
                    new IllegalStateException("Scroll duration must be a positive number");
                    throw th4;
                }
            }

            public int getDx() {
                return this.mDx;
            }

            public void setDx(int i) {
                this.changed = true;
                this.mDx = i;
            }

            public int getDy() {
                return this.mDy;
            }

            public void setDy(int i) {
                this.changed = true;
                this.mDy = i;
            }

            public int getDuration() {
                return this.mDuration;
            }

            public void setDuration(int i) {
                this.changed = true;
                this.mDuration = i;
            }

            public Interpolator getInterpolator() {
                return this.mInterpolator;
            }

            public void setInterpolator(Interpolator interpolator) {
                this.changed = true;
                this.mInterpolator = interpolator;
            }

            public void update(int i, int i2, int i3, Interpolator interpolator) {
                this.mDx = i;
                this.mDy = i2;
                this.mDuration = i3;
                this.mInterpolator = interpolator;
                this.changed = true;
            }
        }
    }

    static class AdapterDataObservable extends Observable<AdapterDataObserver> {
        AdapterDataObservable() {
        }

        public boolean hasObservers() {
            return !this.mObservers.isEmpty();
        }

        public void notifyChanged() {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((AdapterDataObserver) this.mObservers.get(size)).onChanged();
            }
        }

        public void notifyItemRangeChanged(int i, int i2) {
            notifyItemRangeChanged(i, i2, null);
        }

        public void notifyItemRangeChanged(int i, int i2, Object obj) {
            int i3 = i;
            int i4 = i2;
            Object obj2 = obj;
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((AdapterDataObserver) this.mObservers.get(size)).onItemRangeChanged(i3, i4, obj2);
            }
        }

        public void notifyItemRangeInserted(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((AdapterDataObserver) this.mObservers.get(size)).onItemRangeInserted(i3, i4);
            }
        }

        public void notifyItemRangeRemoved(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((AdapterDataObserver) this.mObservers.get(size)).onItemRangeRemoved(i3, i4);
            }
        }

        public void notifyItemMoved(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((AdapterDataObserver) this.mObservers.get(size)).onItemRangeMoved(i3, i4, 1);
            }
        }
    }

    public static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR;
        Parcelable mLayoutState;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        SavedState(android.os.Parcel r6) {
            /*
                r5 = this;
                r0 = r5
                r1 = r6
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                r2 = r0
                r3 = r1
                java.lang.Class<android.support.v7.widget.RecyclerView$LayoutManager> r4 = android.support.v7.widget.RecyclerView.LayoutManager.class
                java.lang.ClassLoader r4 = r4.getClassLoader()
                android.os.Parcelable r3 = r3.readParcelable(r4)
                r2.mLayoutState = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.SavedState.<init>(android.os.Parcel):void");
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            super.writeToParcel(parcel2, i);
            parcel2.writeParcelable(this.mLayoutState, 0);
        }

        /* access modifiers changed from: private */
        public void copyFrom(SavedState savedState) {
            this.mLayoutState = savedState.mLayoutState;
        }

        static {
            Parcelable.Creator<SavedState> creator;
            new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(Parcel parcel) {
                    SavedState savedState;
                    new SavedState(parcel);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = creator;
        }
    }

    public static class State {
        private SparseArray<Object> mData;
        private int mDeletedInvisibleItemCountSincePreviousLayout = 0;
        /* access modifiers changed from: private */
        public boolean mInPreLayout = false;
        int mItemCount = 0;
        private int mPreviousLayoutItemCount = 0;
        /* access modifiers changed from: private */
        public boolean mRunPredictiveAnimations = false;
        /* access modifiers changed from: private */
        public boolean mRunSimpleAnimations = false;
        /* access modifiers changed from: private */
        public boolean mStructureChanged = false;
        private int mTargetPosition = -1;
        /* access modifiers changed from: private */
        public boolean mTrackOldChangeHolders = false;

        static /* synthetic */ int access$1802(State state, int i) {
            int i2 = i;
            int i3 = i2;
            state.mDeletedInvisibleItemCountSincePreviousLayout = i3;
            return i2;
        }

        static /* synthetic */ int access$1812(State state, int i) {
            State state2 = state;
            int i2 = state2.mDeletedInvisibleItemCountSincePreviousLayout + i;
            int i3 = i2;
            state2.mDeletedInvisibleItemCountSincePreviousLayout = i3;
            return i2;
        }

        static /* synthetic */ boolean access$1902(State state, boolean z) {
            boolean z2 = z;
            boolean z3 = z2;
            state.mStructureChanged = z3;
            return z2;
        }

        static /* synthetic */ boolean access$2102(State state, boolean z) {
            boolean z2 = z;
            boolean z3 = z2;
            state.mRunPredictiveAnimations = z3;
            return z2;
        }

        static /* synthetic */ boolean access$2202(State state, boolean z) {
            boolean z2 = z;
            boolean z3 = z2;
            state.mInPreLayout = z3;
            return z2;
        }

        static /* synthetic */ boolean access$2302(State state, boolean z) {
            boolean z2 = z;
            boolean z3 = z2;
            state.mRunSimpleAnimations = z3;
            return z2;
        }

        static /* synthetic */ boolean access$2502(State state, boolean z) {
            boolean z2 = z;
            boolean z3 = z2;
            state.mTrackOldChangeHolders = z3;
            return z2;
        }

        static /* synthetic */ int access$2602(State state, int i) {
            int i2 = i;
            int i3 = i2;
            state.mPreviousLayoutItemCount = i3;
            return i2;
        }

        static /* synthetic */ int access$5602(State state, int i) {
            int i2 = i;
            int i3 = i2;
            state.mTargetPosition = i3;
            return i2;
        }

        /* access modifiers changed from: package-private */
        public State reset() {
            this.mTargetPosition = -1;
            if (this.mData != null) {
                this.mData.clear();
            }
            this.mItemCount = 0;
            this.mStructureChanged = false;
            return this;
        }

        public boolean isPreLayout() {
            return this.mInPreLayout;
        }

        public boolean willRunPredictiveAnimations() {
            return this.mRunPredictiveAnimations;
        }

        public boolean willRunSimpleAnimations() {
            return this.mRunSimpleAnimations;
        }

        public void remove(int i) {
            int i2 = i;
            if (this.mData != null) {
                this.mData.remove(i2);
            }
        }

        public <T> T get(int i) {
            int i2 = i;
            if (this.mData == null) {
                return null;
            }
            return this.mData.get(i2);
        }

        public void put(int i, Object obj) {
            SparseArray<Object> sparseArray;
            int i2 = i;
            Object obj2 = obj;
            if (this.mData == null) {
                new SparseArray<>();
                this.mData = sparseArray;
            }
            this.mData.put(i2, obj2);
        }

        public int getTargetScrollPosition() {
            return this.mTargetPosition;
        }

        public boolean hasTargetScrollPosition() {
            return this.mTargetPosition != -1;
        }

        public boolean didStructureChange() {
            return this.mStructureChanged;
        }

        public int getItemCount() {
            return this.mInPreLayout ? this.mPreviousLayoutItemCount - this.mDeletedInvisibleItemCountSincePreviousLayout : this.mItemCount;
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder();
            return sb.append("State{mTargetPosition=").append(this.mTargetPosition).append(", mData=").append(this.mData).append(", mItemCount=").append(this.mItemCount).append(", mPreviousLayoutItemCount=").append(this.mPreviousLayoutItemCount).append(", mDeletedInvisibleItemCountSincePreviousLayout=").append(this.mDeletedInvisibleItemCountSincePreviousLayout).append(", mStructureChanged=").append(this.mStructureChanged).append(", mInPreLayout=").append(this.mInPreLayout).append(", mRunSimpleAnimations=").append(this.mRunSimpleAnimations).append(", mRunPredictiveAnimations=").append(this.mRunPredictiveAnimations).append('}').toString();
        }
    }

    private class ItemAnimatorRestoreListener implements ItemAnimator.ItemAnimatorListener {
        private ItemAnimatorRestoreListener() {
        }

        public void onAnimationFinished(ViewHolder viewHolder) {
            ViewHolder viewHolder2 = viewHolder;
            viewHolder2.setIsRecyclable(true);
            if (viewHolder2.mShadowedHolder != null && viewHolder2.mShadowingHolder == null) {
                viewHolder2.mShadowedHolder = null;
            }
            viewHolder2.mShadowingHolder = null;
            if (!viewHolder2.shouldBeKeptAsChild() && !RecyclerView.this.removeAnimatingView(viewHolder2.itemView) && viewHolder2.isTmpDetached()) {
                RecyclerView.this.removeDetachedView(viewHolder2.itemView, false);
            }
        }
    }

    public static abstract class ItemAnimator {
        public static final int FLAG_APPEARED_IN_PRE_LAYOUT = 4096;
        public static final int FLAG_CHANGED = 2;
        public static final int FLAG_INVALIDATED = 4;
        public static final int FLAG_MOVED = 2048;
        public static final int FLAG_REMOVED = 8;
        private long mAddDuration;
        private long mChangeDuration;
        private ArrayList<ItemAnimatorFinishedListener> mFinishedListeners;
        private ItemAnimatorListener mListener = null;
        private long mMoveDuration;
        private long mRemoveDuration;

        @Retention(RetentionPolicy.SOURCE)
        public @interface AdapterChanges {
        }

        public interface ItemAnimatorFinishedListener {
            void onAnimationsFinished();
        }

        interface ItemAnimatorListener {
            void onAnimationFinished(ViewHolder viewHolder);
        }

        public abstract boolean animateAppearance(@NonNull ViewHolder viewHolder, @Nullable ItemHolderInfo itemHolderInfo, @NonNull ItemHolderInfo itemHolderInfo2);

        public abstract boolean animateChange(@NonNull ViewHolder viewHolder, @NonNull ViewHolder viewHolder2, @NonNull ItemHolderInfo itemHolderInfo, @NonNull ItemHolderInfo itemHolderInfo2);

        public abstract boolean animateDisappearance(@NonNull ViewHolder viewHolder, @NonNull ItemHolderInfo itemHolderInfo, @Nullable ItemHolderInfo itemHolderInfo2);

        public abstract boolean animatePersistence(@NonNull ViewHolder viewHolder, @NonNull ItemHolderInfo itemHolderInfo, @NonNull ItemHolderInfo itemHolderInfo2);

        public abstract void endAnimation(ViewHolder viewHolder);

        public abstract void endAnimations();

        public abstract boolean isRunning();

        public abstract void runPendingAnimations();

        public ItemAnimator() {
            ArrayList<ItemAnimatorFinishedListener> arrayList;
            new ArrayList<>();
            this.mFinishedListeners = arrayList;
            this.mAddDuration = 120;
            this.mRemoveDuration = 120;
            this.mMoveDuration = 250;
            this.mChangeDuration = 250;
        }

        public long getMoveDuration() {
            return this.mMoveDuration;
        }

        public void setMoveDuration(long j) {
            this.mMoveDuration = j;
        }

        public long getAddDuration() {
            return this.mAddDuration;
        }

        public void setAddDuration(long j) {
            this.mAddDuration = j;
        }

        public long getRemoveDuration() {
            return this.mRemoveDuration;
        }

        public void setRemoveDuration(long j) {
            this.mRemoveDuration = j;
        }

        public long getChangeDuration() {
            return this.mChangeDuration;
        }

        public void setChangeDuration(long j) {
            this.mChangeDuration = j;
        }

        /* access modifiers changed from: package-private */
        public void setListener(ItemAnimatorListener itemAnimatorListener) {
            this.mListener = itemAnimatorListener;
        }

        @NonNull
        public ItemHolderInfo recordPreLayoutInformation(@NonNull State state, @NonNull ViewHolder viewHolder, int i, @NonNull List<Object> list) {
            return obtainHolderInfo().setFrom(viewHolder);
        }

        @NonNull
        public ItemHolderInfo recordPostLayoutInformation(@NonNull State state, @NonNull ViewHolder viewHolder) {
            return obtainHolderInfo().setFrom(viewHolder);
        }

        static int buildAdapterChangeFlagsForAnimations(ViewHolder viewHolder) {
            ViewHolder viewHolder2 = viewHolder;
            int access$6300 = viewHolder2.mFlags & 14;
            if (viewHolder2.isInvalid()) {
                return 4;
            }
            if ((access$6300 & 4) == 0) {
                int oldPosition = viewHolder2.getOldPosition();
                int adapterPosition = viewHolder2.getAdapterPosition();
                if (!(oldPosition == -1 || adapterPosition == -1 || oldPosition == adapterPosition)) {
                    access$6300 |= 2048;
                }
            }
            return access$6300;
        }

        public final void dispatchAnimationFinished(ViewHolder viewHolder) {
            ViewHolder viewHolder2 = viewHolder;
            onAnimationFinished(viewHolder2);
            if (this.mListener != null) {
                this.mListener.onAnimationFinished(viewHolder2);
            }
        }

        public void onAnimationFinished(ViewHolder viewHolder) {
        }

        public final void dispatchAnimationStarted(ViewHolder viewHolder) {
            onAnimationStarted(viewHolder);
        }

        public void onAnimationStarted(ViewHolder viewHolder) {
        }

        public final boolean isRunning(ItemAnimatorFinishedListener itemAnimatorFinishedListener) {
            ItemAnimatorFinishedListener itemAnimatorFinishedListener2 = itemAnimatorFinishedListener;
            boolean isRunning = isRunning();
            if (itemAnimatorFinishedListener2 != null) {
                if (!isRunning) {
                    itemAnimatorFinishedListener2.onAnimationsFinished();
                } else {
                    boolean add = this.mFinishedListeners.add(itemAnimatorFinishedListener2);
                }
            }
            return isRunning;
        }

        public boolean canReuseUpdatedViewHolder(ViewHolder viewHolder) {
            return true;
        }

        public final void dispatchAnimationsFinished() {
            int size = this.mFinishedListeners.size();
            for (int i = 0; i < size; i++) {
                this.mFinishedListeners.get(i).onAnimationsFinished();
            }
            this.mFinishedListeners.clear();
        }

        public ItemHolderInfo obtainHolderInfo() {
            ItemHolderInfo itemHolderInfo;
            new ItemHolderInfo();
            return itemHolderInfo;
        }

        public static class ItemHolderInfo {
            public int bottom;
            public int changeFlags;
            public int left;
            public int right;
            public int top;

            public ItemHolderInfo setFrom(ViewHolder viewHolder) {
                return setFrom(viewHolder, 0);
            }

            public ItemHolderInfo setFrom(ViewHolder viewHolder, int i) {
                View view = viewHolder.itemView;
                this.left = view.getLeft();
                this.top = view.getTop();
                this.right = view.getRight();
                this.bottom = view.getBottom();
                return this;
            }
        }
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (this.mChildDrawingOrderCallback == null) {
            return super.getChildDrawingOrder(i3, i4);
        }
        return this.mChildDrawingOrderCallback.onGetChildDrawingOrder(i3, i4);
    }
}
