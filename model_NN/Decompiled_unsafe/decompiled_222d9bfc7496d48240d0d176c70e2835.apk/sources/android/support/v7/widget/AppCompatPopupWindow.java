package android.support.v7.widget;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v4.widget.PopupWindowCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.PopupWindow;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

public class AppCompatPopupWindow extends PopupWindow {
    private static final boolean COMPAT_OVERLAP_ANCHOR = (Build.VERSION.SDK_INT < 21);
    private static final String TAG = "AppCompatPopupWindow";
    private boolean mOverlapAnchor;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatPopupWindow(android.content.Context r11, android.util.AttributeSet r12, int r13) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r3 = r13
            r5 = r0
            r6 = r1
            r7 = r2
            r8 = r3
            r5.<init>(r6, r7, r8)
            r5 = r1
            r6 = r2
            int[] r7 = android.support.v7.appcompat.R.styleable.PopupWindow
            r8 = r3
            r9 = 0
            android.support.v7.widget.TintTypedArray r5 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r5, r6, r7, r8, r9)
            r4 = r5
            r5 = r4
            int r6 = android.support.v7.appcompat.R.styleable.PopupWindow_overlapAnchor
            boolean r5 = r5.hasValue(r6)
            if (r5 == 0) goto L_0x002b
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.PopupWindow_overlapAnchor
            r8 = 0
            boolean r6 = r6.getBoolean(r7, r8)
            r5.setSupportOverlapAnchor(r6)
        L_0x002b:
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.PopupWindow_android_popupBackground
            android.graphics.drawable.Drawable r6 = r6.getDrawable(r7)
            r5.setBackgroundDrawable(r6)
            r5 = r4
            r5.recycle()
            int r5 = android.os.Build.VERSION.SDK_INT
            r6 = 14
            if (r5 >= r6) goto L_0x0044
            r5 = r0
            wrapOnScrollChangedListener(r5)
        L_0x0044:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AppCompatPopupWindow.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void showAsDropDown(View view, int i, int i2) {
        View view2 = view;
        int i3 = i;
        int i4 = i2;
        if (COMPAT_OVERLAP_ANCHOR && this.mOverlapAnchor) {
            i4 -= view2.getHeight();
        }
        super.showAsDropDown(view2, i3, i4);
    }

    @TargetApi(19)
    public void showAsDropDown(View view, int i, int i2, int i3) {
        View view2 = view;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        if (COMPAT_OVERLAP_ANCHOR && this.mOverlapAnchor) {
            i5 -= view2.getHeight();
        }
        super.showAsDropDown(view2, i4, i5, i6);
    }

    public void update(View view, int i, int i2, int i3, int i4) {
        View view2 = view;
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        if (COMPAT_OVERLAP_ANCHOR && this.mOverlapAnchor) {
            i6 -= view2.getHeight();
        }
        super.update(view2, i5, i6, i7, i8);
    }

    private static void wrapOnScrollChangedListener(PopupWindow popupWindow) {
        Object obj;
        PopupWindow popupWindow2 = popupWindow;
        try {
            Field declaredField = PopupWindow.class.getDeclaredField("mAnchor");
            declaredField.setAccessible(true);
            Field declaredField2 = PopupWindow.class.getDeclaredField("mOnScrollChangedListener");
            declaredField2.setAccessible(true);
            final Field field = declaredField;
            final PopupWindow popupWindow3 = popupWindow2;
            final ViewTreeObserver.OnScrollChangedListener onScrollChangedListener = (ViewTreeObserver.OnScrollChangedListener) declaredField2.get(popupWindow2);
            new ViewTreeObserver.OnScrollChangedListener() {
                public void onScrollChanged() {
                    try {
                        WeakReference weakReference = (WeakReference) field.get(popupWindow3);
                        if (weakReference != null && weakReference.get() != null) {
                            onScrollChangedListener.onScrollChanged();
                        }
                    } catch (IllegalAccessException e) {
                    }
                }
            };
            declaredField2.set(popupWindow2, obj);
        } catch (Exception e) {
            int d = Log.d(TAG, "Exception while installing workaround OnScrollChangedListener", e);
        }
    }

    public void setSupportOverlapAnchor(boolean z) {
        boolean z2 = z;
        if (COMPAT_OVERLAP_ANCHOR) {
            this.mOverlapAnchor = z2;
        } else {
            PopupWindowCompat.setOverlapAnchor(this, z2);
        }
    }

    public boolean getSupportOverlapAnchor() {
        if (COMPAT_OVERLAP_ANCHOR) {
            return this.mOverlapAnchor;
        }
        return PopupWindowCompat.getOverlapAnchor(this);
    }
}
