package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.appcompat.R;
import android.support.v7.view.WindowCallbackWrapper;
import android.support.v7.view.menu.ListMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.widget.DecorToolbar;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ToolbarWidgetWrapper;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.SpinnerAdapter;
import java.util.ArrayList;

class ToolbarActionBar extends ActionBar {
    /* access modifiers changed from: private */
    public DecorToolbar mDecorToolbar;
    private boolean mLastMenuVisibility;
    private ListMenuPresenter mListMenuPresenter;
    private boolean mMenuCallbackSet;
    private final Toolbar.OnMenuItemClickListener mMenuClicker;
    private final Runnable mMenuInvalidator;
    private ArrayList<ActionBar.OnMenuVisibilityListener> mMenuVisibilityListeners;
    /* access modifiers changed from: private */
    public boolean mToolbarMenuPrepared;
    /* access modifiers changed from: private */
    public Window.Callback mWindowCallback;

    static /* synthetic */ boolean access$202(ToolbarActionBar toolbarActionBar, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        toolbarActionBar.mToolbarMenuPrepared = z3;
        return z2;
    }

    public ToolbarActionBar(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        ArrayList<ActionBar.OnMenuVisibilityListener> arrayList;
        Runnable runnable;
        Toolbar.OnMenuItemClickListener onMenuItemClickListener;
        DecorToolbar decorToolbar;
        Window.Callback callback2;
        Toolbar toolbar2 = toolbar;
        new ArrayList<>();
        this.mMenuVisibilityListeners = arrayList;
        new Runnable() {
            public void run() {
                ToolbarActionBar.this.populateOptionsMenu();
            }
        };
        this.mMenuInvalidator = runnable;
        new Toolbar.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem menuItem) {
                return ToolbarActionBar.this.mWindowCallback.onMenuItemSelected(0, menuItem);
            }
        };
        this.mMenuClicker = onMenuItemClickListener;
        new ToolbarWidgetWrapper(toolbar2, false);
        this.mDecorToolbar = decorToolbar;
        new ToolbarCallbackWrapper(callback);
        this.mWindowCallback = callback2;
        this.mDecorToolbar.setWindowCallback(this.mWindowCallback);
        toolbar2.setOnMenuItemClickListener(this.mMenuClicker);
        this.mDecorToolbar.setWindowTitle(charSequence);
    }

    public Window.Callback getWrappedWindowCallback() {
        return this.mWindowCallback;
    }

    public void setCustomView(View view) {
        ActionBar.LayoutParams layoutParams;
        new ActionBar.LayoutParams(-2, -2);
        setCustomView(view, layoutParams);
    }

    public void setCustomView(View view, ActionBar.LayoutParams layoutParams) {
        View view2 = view;
        ActionBar.LayoutParams layoutParams2 = layoutParams;
        if (view2 != null) {
            view2.setLayoutParams(layoutParams2);
        }
        this.mDecorToolbar.setCustomView(view2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void setCustomView(int i) {
        setCustomView(LayoutInflater.from(this.mDecorToolbar.getContext()).inflate(i, this.mDecorToolbar.getViewGroup(), false));
    }

    public void setIcon(int i) {
        this.mDecorToolbar.setIcon(i);
    }

    public void setIcon(Drawable drawable) {
        this.mDecorToolbar.setIcon(drawable);
    }

    public void setLogo(int i) {
        this.mDecorToolbar.setLogo(i);
    }

    public void setLogo(Drawable drawable) {
        this.mDecorToolbar.setLogo(drawable);
    }

    public void setStackedBackgroundDrawable(Drawable drawable) {
    }

    public void setSplitBackgroundDrawable(Drawable drawable) {
    }

    public void setHomeButtonEnabled(boolean z) {
    }

    public void setElevation(float f) {
        ViewCompat.setElevation(this.mDecorToolbar.getViewGroup(), f);
    }

    public float getElevation() {
        return ViewCompat.getElevation(this.mDecorToolbar.getViewGroup());
    }

    public Context getThemedContext() {
        return this.mDecorToolbar.getContext();
    }

    public boolean isTitleTruncated() {
        return super.isTitleTruncated();
    }

    public void setHomeAsUpIndicator(Drawable drawable) {
        this.mDecorToolbar.setNavigationIcon(drawable);
    }

    public void setHomeAsUpIndicator(int i) {
        this.mDecorToolbar.setNavigationIcon(i);
    }

    public void setHomeActionContentDescription(CharSequence charSequence) {
        this.mDecorToolbar.setNavigationContentDescription(charSequence);
    }

    public void setDefaultDisplayHomeAsUpEnabled(boolean z) {
    }

    public void setHomeActionContentDescription(int i) {
        this.mDecorToolbar.setNavigationContentDescription(i);
    }

    public void setShowHideAnimationEnabled(boolean z) {
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void setListNavigationCallbacks(SpinnerAdapter spinnerAdapter, ActionBar.OnNavigationListener onNavigationListener) {
        AdapterView.OnItemSelectedListener onItemSelectedListener;
        new NavItemSelectedListener(onNavigationListener);
        this.mDecorToolbar.setDropdownParams(spinnerAdapter, onItemSelectedListener);
    }

    public void setSelectedNavigationItem(int i) {
        Throwable th;
        int i2 = i;
        switch (this.mDecorToolbar.getNavigationMode()) {
            case 1:
                break;
            default:
                Throwable th2 = th;
                new IllegalStateException("setSelectedNavigationIndex not valid for current navigation mode");
                throw th2;
        }
        this.mDecorToolbar.setDropdownSelectedPosition(i2);
    }

    public int getSelectedNavigationIndex() {
        return -1;
    }

    public int getNavigationItemCount() {
        return 0;
    }

    public void setTitle(CharSequence charSequence) {
        this.mDecorToolbar.setTitle(charSequence);
    }

    public void setTitle(int i) {
        int i2 = i;
        this.mDecorToolbar.setTitle(i2 != 0 ? this.mDecorToolbar.getContext().getText(i2) : null);
    }

    public void setWindowTitle(CharSequence charSequence) {
        this.mDecorToolbar.setWindowTitle(charSequence);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.mDecorToolbar.setSubtitle(charSequence);
    }

    public void setSubtitle(int i) {
        int i2 = i;
        this.mDecorToolbar.setSubtitle(i2 != 0 ? this.mDecorToolbar.getContext().getText(i2) : null);
    }

    public void setDisplayOptions(int i) {
        setDisplayOptions(i, -1);
    }

    public void setDisplayOptions(int i, int i2) {
        int i3 = i2;
        this.mDecorToolbar.setDisplayOptions((i & i3) | (this.mDecorToolbar.getDisplayOptions() & (i3 ^ -1)));
    }

    public void setDisplayUseLogoEnabled(boolean z) {
        setDisplayOptions(z ? 1 : 0, 1);
    }

    public void setDisplayShowHomeEnabled(boolean z) {
        setDisplayOptions(z ? 2 : 0, 2);
    }

    public void setDisplayHomeAsUpEnabled(boolean z) {
        setDisplayOptions(z ? 4 : 0, 4);
    }

    public void setDisplayShowTitleEnabled(boolean z) {
        setDisplayOptions(z ? 8 : 0, 8);
    }

    public void setDisplayShowCustomEnabled(boolean z) {
        setDisplayOptions(z ? 16 : 0, 16);
    }

    public void setBackgroundDrawable(@Nullable Drawable drawable) {
        this.mDecorToolbar.setBackgroundDrawable(drawable);
    }

    public View getCustomView() {
        return this.mDecorToolbar.getCustomView();
    }

    public CharSequence getTitle() {
        return this.mDecorToolbar.getTitle();
    }

    public CharSequence getSubtitle() {
        return this.mDecorToolbar.getSubtitle();
    }

    public int getNavigationMode() {
        return 0;
    }

    public void setNavigationMode(int i) {
        Throwable th;
        int i2 = i;
        if (i2 == 2) {
            Throwable th2 = th;
            new IllegalArgumentException("Tabs not supported in this configuration");
            throw th2;
        }
        this.mDecorToolbar.setNavigationMode(i2);
    }

    public int getDisplayOptions() {
        return this.mDecorToolbar.getDisplayOptions();
    }

    public ActionBar.Tab newTab() {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
        throw th2;
    }

    public void addTab(ActionBar.Tab tab) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
        throw th2;
    }

    public void addTab(ActionBar.Tab tab, boolean z) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
        throw th2;
    }

    public void addTab(ActionBar.Tab tab, int i) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
        throw th2;
    }

    public void addTab(ActionBar.Tab tab, int i, boolean z) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
        throw th2;
    }

    public void removeTab(ActionBar.Tab tab) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
        throw th2;
    }

    public void removeTabAt(int i) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
        throw th2;
    }

    public void removeAllTabs() {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
        throw th2;
    }

    public void selectTab(ActionBar.Tab tab) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
        throw th2;
    }

    public ActionBar.Tab getSelectedTab() {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
        throw th2;
    }

    public ActionBar.Tab getTabAt(int i) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Tabs are not supported in toolbar action bars");
        throw th2;
    }

    public int getTabCount() {
        return 0;
    }

    public int getHeight() {
        return this.mDecorToolbar.getHeight();
    }

    public void show() {
        this.mDecorToolbar.setVisibility(0);
    }

    public void hide() {
        this.mDecorToolbar.setVisibility(8);
    }

    public boolean isShowing() {
        return this.mDecorToolbar.getVisibility() == 0;
    }

    public boolean openOptionsMenu() {
        return this.mDecorToolbar.showOverflowMenu();
    }

    public boolean invalidateOptionsMenu() {
        boolean removeCallbacks = this.mDecorToolbar.getViewGroup().removeCallbacks(this.mMenuInvalidator);
        ViewCompat.postOnAnimation(this.mDecorToolbar.getViewGroup(), this.mMenuInvalidator);
        return true;
    }

    public boolean collapseActionView() {
        if (!this.mDecorToolbar.hasExpandedActionView()) {
            return false;
        }
        this.mDecorToolbar.collapseActionView();
        return true;
    }

    /* access modifiers changed from: package-private */
    public void populateOptionsMenu() {
        Menu menu = getMenu();
        MenuBuilder menuBuilder = menu instanceof MenuBuilder ? (MenuBuilder) menu : null;
        if (menuBuilder != null) {
            menuBuilder.stopDispatchingItemsChanged();
        }
        try {
            menu.clear();
            if (!this.mWindowCallback.onCreatePanelMenu(0, menu) || !this.mWindowCallback.onPreparePanel(0, null, menu)) {
                menu.clear();
            }
            if (menuBuilder != null) {
                menuBuilder.startDispatchingItemsChanged();
            }
        } catch (Throwable th) {
            Throwable th2 = th;
            if (menuBuilder != null) {
                menuBuilder.startDispatchingItemsChanged();
            }
            throw th2;
        }
    }

    public boolean onMenuKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            boolean openOptionsMenu = openOptionsMenu();
        }
        return true;
    }

    public boolean onKeyShortcut(int i, KeyEvent keyEvent) {
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        Menu menu = getMenu();
        if (menu != null) {
            menu.setQwertyMode(KeyCharacterMap.load(keyEvent2 != null ? keyEvent2.getDeviceId() : -1).getKeyboardType() != 1);
            boolean performShortcut = menu.performShortcut(i2, keyEvent2, 0);
        }
        return true;
    }

    public void addOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener onMenuVisibilityListener) {
        boolean add = this.mMenuVisibilityListeners.add(onMenuVisibilityListener);
    }

    public void removeOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener onMenuVisibilityListener) {
        boolean remove = this.mMenuVisibilityListeners.remove(onMenuVisibilityListener);
    }

    public void dispatchMenuVisibilityChanged(boolean z) {
        boolean z2 = z;
        if (z2 != this.mLastMenuVisibility) {
            this.mLastMenuVisibility = z2;
            int size = this.mMenuVisibilityListeners.size();
            for (int i = 0; i < size; i++) {
                this.mMenuVisibilityListeners.get(i).onMenuVisibilityChanged(z2);
            }
        }
    }

    /* access modifiers changed from: private */
    public View getListMenuView(Menu menu) {
        Menu menu2 = menu;
        ensureListMenuPresenter(menu2);
        if (menu2 == null || this.mListMenuPresenter == null) {
            return null;
        }
        if (this.mListMenuPresenter.getAdapter().getCount() > 0) {
            return (View) this.mListMenuPresenter.getMenuView(this.mDecorToolbar.getViewGroup());
        }
        return null;
    }

    private void ensureListMenuPresenter(Menu menu) {
        TypedValue typedValue;
        Context context;
        ListMenuPresenter listMenuPresenter;
        MenuPresenter.Callback callback;
        Menu menu2 = menu;
        if (this.mListMenuPresenter == null && (menu2 instanceof MenuBuilder)) {
            MenuBuilder menuBuilder = (MenuBuilder) menu2;
            Context context2 = this.mDecorToolbar.getContext();
            new TypedValue();
            TypedValue typedValue2 = typedValue;
            Resources.Theme newTheme = context2.getResources().newTheme();
            newTheme.setTo(context2.getTheme());
            boolean resolveAttribute = newTheme.resolveAttribute(R.attr.actionBarPopupTheme, typedValue2, true);
            if (typedValue2.resourceId != 0) {
                newTheme.applyStyle(typedValue2.resourceId, true);
            }
            boolean resolveAttribute2 = newTheme.resolveAttribute(R.attr.panelMenuListTheme, typedValue2, true);
            if (typedValue2.resourceId != 0) {
                newTheme.applyStyle(typedValue2.resourceId, true);
            } else {
                newTheme.applyStyle(R.style.Theme_AppCompat_CompactMenu, true);
            }
            new ContextThemeWrapper(context2, 0);
            Context context3 = context;
            context3.getTheme().setTo(newTheme);
            new ListMenuPresenter(context3, R.layout.abc_list_menu_item_layout);
            this.mListMenuPresenter = listMenuPresenter;
            new PanelMenuPresenterCallback();
            this.mListMenuPresenter.setCallback(callback);
            menuBuilder.addMenuPresenter(this.mListMenuPresenter);
        }
    }

    private class ToolbarCallbackWrapper extends WindowCallbackWrapper {
        public ToolbarCallbackWrapper(Window.Callback callback) {
            super(callback);
        }

        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel && !ToolbarActionBar.this.mToolbarMenuPrepared) {
                ToolbarActionBar.this.mDecorToolbar.setMenuPrepared();
                boolean access$202 = ToolbarActionBar.access$202(ToolbarActionBar.this, true);
            }
            return onPreparePanel;
        }

        public View onCreatePanelView(int i) {
            int i2 = i;
            switch (i2) {
                case 0:
                    Menu menu = ToolbarActionBar.this.mDecorToolbar.getMenu();
                    if (onPreparePanel(i2, null, menu) && onMenuOpened(i2, menu)) {
                        return ToolbarActionBar.this.getListMenuView(menu);
                    }
            }
            return super.onCreatePanelView(i2);
        }
    }

    private Menu getMenu() {
        MenuPresenter.Callback callback;
        MenuBuilder.Callback callback2;
        if (!this.mMenuCallbackSet) {
            new ActionMenuPresenterCallback();
            new MenuBuilderCallback();
            this.mDecorToolbar.setMenuCallbacks(callback, callback2);
            this.mMenuCallbackSet = true;
        }
        return this.mDecorToolbar.getMenu();
    }

    private final class ActionMenuPresenterCallback implements MenuPresenter.Callback {
        private boolean mClosingActionMenu;

        private ActionMenuPresenterCallback() {
        }

        public boolean onOpenSubMenu(MenuBuilder menuBuilder) {
            MenuBuilder menuBuilder2 = menuBuilder;
            if (ToolbarActionBar.this.mWindowCallback == null) {
                return false;
            }
            boolean onMenuOpened = ToolbarActionBar.this.mWindowCallback.onMenuOpened(108, menuBuilder2);
            return true;
        }

        public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
            MenuBuilder menuBuilder2 = menuBuilder;
            if (!this.mClosingActionMenu) {
                this.mClosingActionMenu = true;
                ToolbarActionBar.this.mDecorToolbar.dismissPopupMenus();
                if (ToolbarActionBar.this.mWindowCallback != null) {
                    ToolbarActionBar.this.mWindowCallback.onPanelClosed(108, menuBuilder2);
                }
                this.mClosingActionMenu = false;
            }
        }
    }

    private final class PanelMenuPresenterCallback implements MenuPresenter.Callback {
        private PanelMenuPresenterCallback() {
        }

        public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
            MenuBuilder menuBuilder2 = menuBuilder;
            if (ToolbarActionBar.this.mWindowCallback != null) {
                ToolbarActionBar.this.mWindowCallback.onPanelClosed(0, menuBuilder2);
            }
        }

        public boolean onOpenSubMenu(MenuBuilder menuBuilder) {
            MenuBuilder menuBuilder2 = menuBuilder;
            if (menuBuilder2 == null && ToolbarActionBar.this.mWindowCallback != null) {
                boolean onMenuOpened = ToolbarActionBar.this.mWindowCallback.onMenuOpened(0, menuBuilder2);
            }
            return true;
        }
    }

    private final class MenuBuilderCallback implements MenuBuilder.Callback {
        private MenuBuilderCallback() {
        }

        public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
            return false;
        }

        public void onMenuModeChange(MenuBuilder menuBuilder) {
            MenuBuilder menuBuilder2 = menuBuilder;
            if (ToolbarActionBar.this.mWindowCallback == null) {
                return;
            }
            if (ToolbarActionBar.this.mDecorToolbar.isOverflowMenuShowing()) {
                ToolbarActionBar.this.mWindowCallback.onPanelClosed(108, menuBuilder2);
            } else if (ToolbarActionBar.this.mWindowCallback.onPreparePanel(0, null, menuBuilder2)) {
                boolean onMenuOpened = ToolbarActionBar.this.mWindowCallback.onMenuOpened(108, menuBuilder2);
            }
        }
    }
}
