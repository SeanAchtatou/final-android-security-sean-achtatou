package android.support.v7.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.NotificationBuilderWithBuilderAccessor;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.session.MediaSessionCompat;

public class NotificationCompat extends android.support.v4.app.NotificationCompat {
    /* access modifiers changed from: private */
    public static void addMediaStyleToBuilderLollipop(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, NotificationCompat.Style style) {
        NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor2 = notificationBuilderWithBuilderAccessor;
        NotificationCompat.Style style2 = style;
        if (style2 instanceof MediaStyle) {
            MediaStyle mediaStyle = (MediaStyle) style2;
            NotificationCompatImpl21.addMediaStyle(notificationBuilderWithBuilderAccessor2, mediaStyle.mActionsToShowInCompact, mediaStyle.mToken != null ? mediaStyle.mToken.getToken() : null);
        }
    }

    /* access modifiers changed from: private */
    public static void addMediaStyleToBuilderIcs(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, NotificationCompat.Builder builder) {
        NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor2 = notificationBuilderWithBuilderAccessor;
        NotificationCompat.Builder builder2 = builder;
        if (builder2.mStyle instanceof MediaStyle) {
            MediaStyle mediaStyle = (MediaStyle) builder2.mStyle;
            NotificationCompatImplBase.overrideContentView(notificationBuilderWithBuilderAccessor2, builder2.mContext, builder2.mContentTitle, builder2.mContentText, builder2.mContentInfo, builder2.mNumber, builder2.mLargeIcon, builder2.mSubText, builder2.mUseChronometer, builder2.mNotification.when, builder2.mActions, mediaStyle.mActionsToShowInCompact, mediaStyle.mShowCancelButton, mediaStyle.mCancelButtonIntent);
        }
    }

    /* access modifiers changed from: private */
    public static void addBigMediaStyleToBuilderJellybean(Notification notification, NotificationCompat.Builder builder) {
        Notification notification2 = notification;
        NotificationCompat.Builder builder2 = builder;
        if (builder2.mStyle instanceof MediaStyle) {
            MediaStyle mediaStyle = (MediaStyle) builder2.mStyle;
            NotificationCompatImplBase.overrideBigContentView(notification2, builder2.mContext, builder2.mContentTitle, builder2.mContentText, builder2.mContentInfo, builder2.mNumber, builder2.mLargeIcon, builder2.mSubText, builder2.mUseChronometer, builder2.mNotification.when, builder2.mActions, mediaStyle.mShowCancelButton, mediaStyle.mCancelButtonIntent);
        }
    }

    public static class Builder extends NotificationCompat.Builder {
        public Builder(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public NotificationCompat.BuilderExtender getExtender() {
            NotificationCompat.BuilderExtender builderExtender;
            NotificationCompat.BuilderExtender builderExtender2;
            NotificationCompat.BuilderExtender builderExtender3;
            if (Build.VERSION.SDK_INT >= 21) {
                new LollipopExtender();
                return builderExtender3;
            } else if (Build.VERSION.SDK_INT >= 16) {
                new JellybeanExtender();
                return builderExtender2;
            } else if (Build.VERSION.SDK_INT < 14) {
                return super.getExtender();
            } else {
                new IceCreamSandwichExtender();
                return builderExtender;
            }
        }
    }

    private static class IceCreamSandwichExtender extends NotificationCompat.BuilderExtender {
        private IceCreamSandwichExtender() {
        }

        public Notification build(NotificationCompat.Builder builder, NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor2 = notificationBuilderWithBuilderAccessor;
            NotificationCompat.addMediaStyleToBuilderIcs(notificationBuilderWithBuilderAccessor2, builder);
            return notificationBuilderWithBuilderAccessor2.build();
        }
    }

    private static class JellybeanExtender extends NotificationCompat.BuilderExtender {
        private JellybeanExtender() {
        }

        public Notification build(NotificationCompat.Builder builder, NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            NotificationCompat.Builder builder2 = builder;
            NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor2 = notificationBuilderWithBuilderAccessor;
            NotificationCompat.addMediaStyleToBuilderIcs(notificationBuilderWithBuilderAccessor2, builder2);
            Notification build = notificationBuilderWithBuilderAccessor2.build();
            NotificationCompat.addBigMediaStyleToBuilderJellybean(build, builder2);
            return build;
        }
    }

    private static class LollipopExtender extends NotificationCompat.BuilderExtender {
        private LollipopExtender() {
        }

        public Notification build(NotificationCompat.Builder builder, NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor2 = notificationBuilderWithBuilderAccessor;
            NotificationCompat.addMediaStyleToBuilderLollipop(notificationBuilderWithBuilderAccessor2, builder.mStyle);
            return notificationBuilderWithBuilderAccessor2.build();
        }
    }

    public static class MediaStyle extends NotificationCompat.Style {
        int[] mActionsToShowInCompact = null;
        PendingIntent mCancelButtonIntent;
        boolean mShowCancelButton;
        MediaSessionCompat.Token mToken;

        public MediaStyle() {
        }

        public MediaStyle(NotificationCompat.Builder builder) {
            setBuilder(builder);
        }

        public MediaStyle setShowActionsInCompactView(int... iArr) {
            this.mActionsToShowInCompact = iArr;
            return this;
        }

        public MediaStyle setMediaSession(MediaSessionCompat.Token token) {
            this.mToken = token;
            return this;
        }

        public MediaStyle setShowCancelButton(boolean z) {
            this.mShowCancelButton = z;
            return this;
        }

        public MediaStyle setCancelButtonIntent(PendingIntent pendingIntent) {
            this.mCancelButtonIntent = pendingIntent;
            return this;
        }
    }
}
