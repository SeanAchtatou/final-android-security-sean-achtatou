package android.support.v7.app;

import android.content.Context;
import android.support.v7.app.AppCompatDelegateImplBase;
import android.support.v7.view.SupportActionModeWrapper;
import android.view.ActionMode;
import android.view.Window;

class AppCompatDelegateImplV14 extends AppCompatDelegateImplV11 {
    private boolean mHandleNativeActionModes = true;

    AppCompatDelegateImplV14(Context context, Window window, AppCompatCallback appCompatCallback) {
        super(context, window, appCompatCallback);
    }

    /* access modifiers changed from: package-private */
    public Window.Callback wrapWindowCallback(Window.Callback callback) {
        Window.Callback callback2;
        new AppCompatWindowCallbackV14(this, callback);
        return callback2;
    }

    public void setHandleNativeActionModesEnabled(boolean z) {
        this.mHandleNativeActionModes = z;
    }

    public boolean isHandleNativeActionModesEnabled() {
        return this.mHandleNativeActionModes;
    }

    class AppCompatWindowCallbackV14 extends AppCompatDelegateImplBase.AppCompatWindowCallbackBase {
        final /* synthetic */ AppCompatDelegateImplV14 this$0;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        AppCompatWindowCallbackV14(android.support.v7.app.AppCompatDelegateImplV14 r7, android.view.Window.Callback r8) {
            /*
                r6 = this;
                r0 = r6
                r1 = r7
                r2 = r8
                r3 = r0
                r4 = r1
                r3.this$0 = r4
                r3 = r0
                r4 = r1
                r5 = r2
                r3.<init>(r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.AppCompatDelegateImplV14.AppCompatWindowCallbackV14.<init>(android.support.v7.app.AppCompatDelegateImplV14, android.view.Window$Callback):void");
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            ActionMode.Callback callback2 = callback;
            if (this.this$0.isHandleNativeActionModesEnabled()) {
                return startAsSupportActionMode(callback2);
            }
            return super.onWindowStartingActionMode(callback2);
        }

        /* access modifiers changed from: package-private */
        public final ActionMode startAsSupportActionMode(ActionMode.Callback callback) {
            SupportActionModeWrapper.CallbackWrapper callbackWrapper;
            new SupportActionModeWrapper.CallbackWrapper(this.this$0.mContext, callback);
            SupportActionModeWrapper.CallbackWrapper callbackWrapper2 = callbackWrapper;
            android.support.v7.view.ActionMode startSupportActionMode = this.this$0.startSupportActionMode(callbackWrapper2);
            if (startSupportActionMode != null) {
                return callbackWrapper2.getActionModeWrapper(startSupportActionMode);
            }
            return null;
        }
    }
}
