package android.support.v7.util;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ParallelExecutorCompat;
import android.support.v7.util.ThreadUtil;
import android.support.v7.util.TileList;
import android.util.Log;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

class MessageThreadUtil<T> implements ThreadUtil<T> {
    MessageThreadUtil() {
    }

    public ThreadUtil.MainThreadCallback<T> getMainThreadProxy(ThreadUtil.MainThreadCallback<T> mainThreadCallback) {
        ThreadUtil.MainThreadCallback<T> mainThreadCallback2;
        final ThreadUtil.MainThreadCallback<T> mainThreadCallback3 = mainThreadCallback;
        new ThreadUtil.MainThreadCallback<T>() {
            private static final int ADD_TILE = 2;
            private static final int REMOVE_TILE = 3;
            private static final int UPDATE_ITEM_COUNT = 1;
            private final Handler mMainThreadHandler;
            private Runnable mMainThreadRunnable;
            /* access modifiers changed from: private */
            public final MessageQueue mQueue;

            {
                MessageQueue messageQueue;
                Handler handler;
                Runnable runnable;
                new MessageQueue();
                this.mQueue = messageQueue;
                new Handler(Looper.getMainLooper());
                this.mMainThreadHandler = handler;
                new Runnable() {
                    public void run() {
                        StringBuilder sb;
                        SyncQueueItem next = AnonymousClass1.this.mQueue.next();
                        while (true) {
                            SyncQueueItem syncQueueItem = next;
                            if (syncQueueItem != null) {
                                switch (syncQueueItem.what) {
                                    case 1:
                                        mainThreadCallback3.updateItemCount(syncQueueItem.arg1, syncQueueItem.arg2);
                                        break;
                                    case 2:
                                        mainThreadCallback3.addTile(syncQueueItem.arg1, (TileList.Tile) syncQueueItem.data);
                                        break;
                                    case 3:
                                        mainThreadCallback3.removeTile(syncQueueItem.arg1, syncQueueItem.arg2);
                                        break;
                                    default:
                                        new StringBuilder();
                                        int e = Log.e("ThreadUtil", sb.append("Unsupported message, what=").append(syncQueueItem.what).toString());
                                        break;
                                }
                                next = AnonymousClass1.this.mQueue.next();
                            } else {
                                return;
                            }
                        }
                    }
                };
                this.mMainThreadRunnable = runnable;
            }

            public void updateItemCount(int i, int i2) {
                sendMessage(SyncQueueItem.obtainMessage(1, i, i2));
            }

            public void addTile(int i, TileList.Tile<T> tile) {
                sendMessage(SyncQueueItem.obtainMessage(2, i, tile));
            }

            public void removeTile(int i, int i2) {
                sendMessage(SyncQueueItem.obtainMessage(3, i, i2));
            }

            private void sendMessage(SyncQueueItem syncQueueItem) {
                this.mQueue.sendMessage(syncQueueItem);
                boolean post = this.mMainThreadHandler.post(this.mMainThreadRunnable);
            }
        };
        return mainThreadCallback2;
    }

    public ThreadUtil.BackgroundCallback<T> getBackgroundProxy(ThreadUtil.BackgroundCallback<T> backgroundCallback) {
        ThreadUtil.BackgroundCallback<T> backgroundCallback2;
        final ThreadUtil.BackgroundCallback<T> backgroundCallback3 = backgroundCallback;
        new ThreadUtil.BackgroundCallback<T>() {
            private static final int LOAD_TILE = 3;
            private static final int RECYCLE_TILE = 4;
            private static final int REFRESH = 1;
            private static final int UPDATE_RANGE = 2;
            private Runnable mBackgroundRunnable;
            AtomicBoolean mBackgroundRunning;
            private final Executor mExecutor = ParallelExecutorCompat.getParallelExecutor();
            /* access modifiers changed from: private */
            public final MessageQueue mQueue;

            {
                MessageQueue messageQueue;
                AtomicBoolean atomicBoolean;
                Runnable runnable;
                new MessageQueue();
                this.mQueue = messageQueue;
                new AtomicBoolean(false);
                this.mBackgroundRunning = atomicBoolean;
                new Runnable() {
                    public void run() {
                        StringBuilder sb;
                        while (true) {
                            SyncQueueItem next = AnonymousClass2.this.mQueue.next();
                            if (next != null) {
                                switch (next.what) {
                                    case 1:
                                        AnonymousClass2.this.mQueue.removeMessages(1);
                                        backgroundCallback3.refresh(next.arg1);
                                        break;
                                    case 2:
                                        AnonymousClass2.this.mQueue.removeMessages(2);
                                        AnonymousClass2.this.mQueue.removeMessages(3);
                                        backgroundCallback3.updateRange(next.arg1, next.arg2, next.arg3, next.arg4, next.arg5);
                                        break;
                                    case 3:
                                        backgroundCallback3.loadTile(next.arg1, next.arg2);
                                        break;
                                    case 4:
                                        backgroundCallback3.recycleTile((TileList.Tile) next.data);
                                        break;
                                    default:
                                        new StringBuilder();
                                        int e = Log.e("ThreadUtil", sb.append("Unsupported message, what=").append(next.what).toString());
                                        break;
                                }
                            } else {
                                AnonymousClass2.this.mBackgroundRunning.set(false);
                                return;
                            }
                        }
                    }
                };
                this.mBackgroundRunnable = runnable;
            }

            public void refresh(int i) {
                sendMessageAtFrontOfQueue(SyncQueueItem.obtainMessage(1, i, (Object) null));
            }

            public void updateRange(int i, int i2, int i3, int i4, int i5) {
                sendMessageAtFrontOfQueue(SyncQueueItem.obtainMessage(2, i, i2, i3, i4, i5, null));
            }

            public void loadTile(int i, int i2) {
                sendMessage(SyncQueueItem.obtainMessage(3, i, i2));
            }

            public void recycleTile(TileList.Tile<T> tile) {
                sendMessage(SyncQueueItem.obtainMessage(4, 0, tile));
            }

            private void sendMessage(SyncQueueItem syncQueueItem) {
                this.mQueue.sendMessage(syncQueueItem);
                maybeExecuteBackgroundRunnable();
            }

            private void sendMessageAtFrontOfQueue(SyncQueueItem syncQueueItem) {
                this.mQueue.sendMessageAtFrontOfQueue(syncQueueItem);
                maybeExecuteBackgroundRunnable();
            }

            private void maybeExecuteBackgroundRunnable() {
                if (this.mBackgroundRunning.compareAndSet(false, true)) {
                    this.mExecutor.execute(this.mBackgroundRunnable);
                }
            }
        };
        return backgroundCallback2;
    }

    static class SyncQueueItem {
        private static SyncQueueItem sPool;
        private static final Object sPoolLock;
        public int arg1;
        public int arg2;
        public int arg3;
        public int arg4;
        public int arg5;
        public Object data;
        /* access modifiers changed from: private */
        public SyncQueueItem next;
        public int what;

        SyncQueueItem() {
        }

        static /* synthetic */ SyncQueueItem access$202(SyncQueueItem syncQueueItem, SyncQueueItem syncQueueItem2) {
            SyncQueueItem syncQueueItem3 = syncQueueItem2;
            SyncQueueItem syncQueueItem4 = syncQueueItem3;
            syncQueueItem.next = syncQueueItem4;
            return syncQueueItem3;
        }

        static {
            Object obj;
            new Object();
            sPoolLock = obj;
        }

        /* access modifiers changed from: package-private */
        public void recycle() {
            this.next = null;
            this.arg5 = 0;
            this.arg4 = 0;
            this.arg3 = 0;
            this.arg2 = 0;
            this.arg1 = 0;
            this.what = 0;
            this.data = null;
            Object obj = sPoolLock;
            Object obj2 = obj;
            synchronized (obj) {
                try {
                    if (sPool != null) {
                        this.next = sPool;
                    }
                    sPool = this;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    Object obj3 = obj2;
                    throw th2;
                }
            }
        }

        static SyncQueueItem obtainMessage(int i, int i2, int i3, int i4, int i5, int i6, Object obj) {
            SyncQueueItem syncQueueItem;
            SyncQueueItem syncQueueItem2;
            int i7 = i;
            int i8 = i2;
            int i9 = i3;
            int i10 = i4;
            int i11 = i5;
            int i12 = i6;
            Object obj2 = obj;
            Object obj3 = sPoolLock;
            Object obj4 = obj3;
            synchronized (obj3) {
                try {
                    if (sPool == null) {
                        new SyncQueueItem();
                        syncQueueItem = syncQueueItem2;
                    } else {
                        syncQueueItem = sPool;
                        sPool = sPool.next;
                        syncQueueItem.next = null;
                    }
                    syncQueueItem.what = i7;
                    syncQueueItem.arg1 = i8;
                    syncQueueItem.arg2 = i9;
                    syncQueueItem.arg3 = i10;
                    syncQueueItem.arg4 = i11;
                    syncQueueItem.arg5 = i12;
                    syncQueueItem.data = obj2;
                    SyncQueueItem syncQueueItem3 = syncQueueItem;
                    return syncQueueItem3;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    Object obj5 = obj4;
                    throw th2;
                }
            }
        }

        static SyncQueueItem obtainMessage(int i, int i2, int i3) {
            return obtainMessage(i, i2, i3, 0, 0, 0, null);
        }

        static SyncQueueItem obtainMessage(int i, int i2, Object obj) {
            return obtainMessage(i, i2, 0, 0, 0, 0, obj);
        }
    }

    static class MessageQueue {
        private SyncQueueItem mRoot;

        MessageQueue() {
        }

        /* access modifiers changed from: package-private */
        public synchronized SyncQueueItem next() {
            SyncQueueItem syncQueueItem;
            if (this.mRoot == null) {
                syncQueueItem = null;
            } else {
                SyncQueueItem syncQueueItem2 = this.mRoot;
                this.mRoot = this.mRoot.next;
                syncQueueItem = syncQueueItem2;
            }
            return syncQueueItem;
        }

        /* access modifiers changed from: package-private */
        public synchronized void sendMessageAtFrontOfQueue(SyncQueueItem syncQueueItem) {
            SyncQueueItem syncQueueItem2 = syncQueueItem;
            synchronized (this) {
                SyncQueueItem access$202 = SyncQueueItem.access$202(syncQueueItem2, this.mRoot);
                this.mRoot = syncQueueItem2;
            }
        }

        /* access modifiers changed from: package-private */
        public synchronized void sendMessage(SyncQueueItem syncQueueItem) {
            SyncQueueItem syncQueueItem2 = syncQueueItem;
            synchronized (this) {
                if (this.mRoot == null) {
                    this.mRoot = syncQueueItem2;
                } else {
                    SyncQueueItem syncQueueItem3 = this.mRoot;
                    while (syncQueueItem3.next != null) {
                        syncQueueItem3 = syncQueueItem3.next;
                    }
                    SyncQueueItem access$202 = SyncQueueItem.access$202(syncQueueItem3, syncQueueItem2);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public synchronized void removeMessages(int i) {
            int i2 = i;
            synchronized (this) {
                while (this.mRoot != null && this.mRoot.what == i2) {
                    this.mRoot = this.mRoot.next;
                    this.mRoot.recycle();
                }
                if (this.mRoot != null) {
                    SyncQueueItem syncQueueItem = this.mRoot;
                    SyncQueueItem access$200 = syncQueueItem.next;
                    while (true) {
                        SyncQueueItem syncQueueItem2 = access$200;
                        if (syncQueueItem2 == null) {
                            break;
                        }
                        SyncQueueItem access$2002 = syncQueueItem2.next;
                        if (syncQueueItem2.what == i2) {
                            SyncQueueItem access$202 = SyncQueueItem.access$202(syncQueueItem, access$2002);
                            syncQueueItem2.recycle();
                        } else {
                            syncQueueItem = syncQueueItem2;
                        }
                        access$200 = access$2002;
                    }
                }
            }
        }
    }
}
