package android.support.design.widget;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ScrollerCompat;
import android.support.v7.widget.ActivityChooserView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;

abstract class HeaderBehavior<V extends View> extends ViewOffsetBehavior<V> {
    private static final int INVALID_POINTER = -1;
    private int mActivePointerId = -1;
    private Runnable mFlingRunnable;
    private boolean mIsBeingDragged;
    private int mLastMotionY;
    /* access modifiers changed from: private */
    public ScrollerCompat mScroller;
    private int mTouchSlop = -1;
    private VelocityTracker mVelocityTracker;

    public HeaderBehavior() {
    }

    public HeaderBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        int findPointerIndex;
        CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
        V v2 = v;
        MotionEvent motionEvent2 = motionEvent;
        if (this.mTouchSlop < 0) {
            this.mTouchSlop = ViewConfiguration.get(coordinatorLayout2.getContext()).getScaledTouchSlop();
        }
        if (motionEvent2.getAction() == 2 && this.mIsBeingDragged) {
            return true;
        }
        switch (MotionEventCompat.getActionMasked(motionEvent2)) {
            case 0:
                this.mIsBeingDragged = false;
                int x = (int) motionEvent2.getX();
                int y = (int) motionEvent2.getY();
                if (canDragView(v2) && coordinatorLayout2.isPointInChildBounds(v2, x, y)) {
                    this.mLastMotionY = y;
                    this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                    ensureVelocityTracker();
                    break;
                }
            case 1:
            case 3:
                this.mIsBeingDragged = false;
                this.mActivePointerId = -1;
                if (this.mVelocityTracker != null) {
                    this.mVelocityTracker.recycle();
                    this.mVelocityTracker = null;
                    break;
                }
                break;
            case 2:
                int i = this.mActivePointerId;
                if (!(i == -1 || (findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, i)) == -1)) {
                    int y2 = (int) MotionEventCompat.getY(motionEvent2, findPointerIndex);
                    if (Math.abs(y2 - this.mLastMotionY) > this.mTouchSlop) {
                        this.mIsBeingDragged = true;
                        this.mLastMotionY = y2;
                        break;
                    }
                }
                break;
        }
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.addMovement(motionEvent2);
        }
        return this.mIsBeingDragged;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00fd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.support.design.widget.CoordinatorLayout r14, V r15, android.view.MotionEvent r16) {
        /*
            r13 = this;
            r0 = r13
            r1 = r14
            r2 = r15
            r3 = r16
            r7 = r0
            int r7 = r7.mTouchSlop
            if (r7 >= 0) goto L_0x001a
            r7 = r0
            r8 = r1
            android.content.Context r8 = r8.getContext()
            android.view.ViewConfiguration r8 = android.view.ViewConfiguration.get(r8)
            int r8 = r8.getScaledTouchSlop()
            r7.mTouchSlop = r8
        L_0x001a:
            r7 = r3
            int r7 = android.support.v4.view.MotionEventCompat.getActionMasked(r7)
            switch(r7) {
                case 0: goto L_0x0031;
                case 1: goto L_0x00c1;
                case 2: goto L_0x0066;
                case 3: goto L_0x00f0;
                default: goto L_0x0022;
            }
        L_0x0022:
            r7 = r0
            android.view.VelocityTracker r7 = r7.mVelocityTracker
            if (r7 == 0) goto L_0x002e
            r7 = r0
            android.view.VelocityTracker r7 = r7.mVelocityTracker
            r8 = r3
            r7.addMovement(r8)
        L_0x002e:
            r7 = 1
            r0 = r7
        L_0x0030:
            return r0
        L_0x0031:
            r7 = r3
            float r7 = r7.getX()
            int r7 = (int) r7
            r4 = r7
            r7 = r3
            float r7 = r7.getY()
            int r7 = (int) r7
            r5 = r7
            r7 = r1
            r8 = r2
            r9 = r4
            r10 = r5
            boolean r7 = r7.isPointInChildBounds(r8, r9, r10)
            if (r7 == 0) goto L_0x0063
            r7 = r0
            r8 = r2
            boolean r7 = r7.canDragView(r8)
            if (r7 == 0) goto L_0x0063
            r7 = r0
            r8 = r5
            r7.mLastMotionY = r8
            r7 = r0
            r8 = r3
            r9 = 0
            int r8 = android.support.v4.view.MotionEventCompat.getPointerId(r8, r9)
            r7.mActivePointerId = r8
            r7 = r0
            r7.ensureVelocityTracker()
            goto L_0x0022
        L_0x0063:
            r7 = 0
            r0 = r7
            goto L_0x0030
        L_0x0066:
            r7 = r3
            r8 = r0
            int r8 = r8.mActivePointerId
            int r7 = android.support.v4.view.MotionEventCompat.findPointerIndex(r7, r8)
            r4 = r7
            r7 = r4
            r8 = -1
            if (r7 != r8) goto L_0x0076
            r7 = 0
            r0 = r7
            goto L_0x0030
        L_0x0076:
            r7 = r3
            r8 = r4
            float r7 = android.support.v4.view.MotionEventCompat.getY(r7, r8)
            int r7 = (int) r7
            r5 = r7
            r7 = r0
            int r7 = r7.mLastMotionY
            r8 = r5
            int r7 = r7 - r8
            r6 = r7
            r7 = r0
            boolean r7 = r7.mIsBeingDragged
            if (r7 != 0) goto L_0x00a0
            r7 = r6
            int r7 = java.lang.Math.abs(r7)
            r8 = r0
            int r8 = r8.mTouchSlop
            if (r7 <= r8) goto L_0x00a0
            r7 = r0
            r8 = 1
            r7.mIsBeingDragged = r8
            r7 = r6
            if (r7 <= 0) goto L_0x00ba
            r7 = r6
            r8 = r0
            int r8 = r8.mTouchSlop
            int r7 = r7 - r8
            r6 = r7
        L_0x00a0:
            r7 = r0
            boolean r7 = r7.mIsBeingDragged
            if (r7 == 0) goto L_0x0022
            r7 = r0
            r8 = r5
            r7.mLastMotionY = r8
            r7 = r0
            r8 = r1
            r9 = r2
            r10 = r6
            r11 = r0
            r12 = r2
            int r11 = r11.getMaxDragOffset(r12)
            r12 = 0
            int r7 = r7.scroll(r8, r9, r10, r11, r12)
            goto L_0x0022
        L_0x00ba:
            r7 = r6
            r8 = r0
            int r8 = r8.mTouchSlop
            int r7 = r7 + r8
            r6 = r7
            goto L_0x00a0
        L_0x00c1:
            r7 = r0
            android.view.VelocityTracker r7 = r7.mVelocityTracker
            if (r7 == 0) goto L_0x00f0
            r7 = r0
            android.view.VelocityTracker r7 = r7.mVelocityTracker
            r8 = r3
            r7.addMovement(r8)
            r7 = r0
            android.view.VelocityTracker r7 = r7.mVelocityTracker
            r8 = 1000(0x3e8, float:1.401E-42)
            r7.computeCurrentVelocity(r8)
            r7 = r0
            android.view.VelocityTracker r7 = r7.mVelocityTracker
            r8 = r0
            int r8 = r8.mActivePointerId
            float r7 = android.support.v4.view.VelocityTrackerCompat.getYVelocity(r7, r8)
            r4 = r7
            r7 = r0
            r8 = r1
            r9 = r2
            r10 = r0
            r11 = r2
            int r10 = r10.getScrollRangeForDragFling(r11)
            int r10 = -r10
            r11 = 0
            r12 = r4
            boolean r7 = r7.fling(r8, r9, r10, r11, r12)
        L_0x00f0:
            r7 = r0
            r8 = 0
            r7.mIsBeingDragged = r8
            r7 = r0
            r8 = -1
            r7.mActivePointerId = r8
            r7 = r0
            android.view.VelocityTracker r7 = r7.mVelocityTracker
            if (r7 == 0) goto L_0x0022
            r7 = r0
            android.view.VelocityTracker r7 = r7.mVelocityTracker
            r7.recycle()
            r7 = r0
            r8 = 0
            r7.mVelocityTracker = r8
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.HeaderBehavior.onTouchEvent(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: package-private */
    public int setHeaderTopBottomOffset(CoordinatorLayout coordinatorLayout, V v, int i) {
        return setHeaderTopBottomOffset(coordinatorLayout, v, i, Integer.MIN_VALUE, ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
    }

    /* access modifiers changed from: package-private */
    public int setHeaderTopBottomOffset(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3) {
        int constrain;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        int topAndBottomOffset = getTopAndBottomOffset();
        int i7 = 0;
        if (i5 != 0 && topAndBottomOffset >= i5 && topAndBottomOffset <= i6 && topAndBottomOffset != (constrain = MathUtils.constrain(i4, i5, i6))) {
            boolean topAndBottomOffset2 = setTopAndBottomOffset(constrain);
            i7 = topAndBottomOffset - constrain;
        }
        return i7;
    }

    /* access modifiers changed from: package-private */
    public int getTopBottomOffsetForScrollingSibling() {
        return getTopAndBottomOffset();
    }

    /* access modifiers changed from: package-private */
    public final int scroll(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3) {
        return setHeaderTopBottomOffset(coordinatorLayout, v, getTopBottomOffsetForScrollingSibling() - i, i2, i3);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    final boolean fling(android.support.design.widget.CoordinatorLayout r17, V r18, int r19, int r20, float r21) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            r3 = r19
            r4 = r20
            r5 = r21
            r6 = r0
            java.lang.Runnable r6 = r6.mFlingRunnable
            if (r6 == 0) goto L_0x001d
            r6 = r2
            r7 = r0
            java.lang.Runnable r7 = r7.mFlingRunnable
            boolean r6 = r6.removeCallbacks(r7)
            r6 = r0
            r7 = 0
            r6.mFlingRunnable = r7
        L_0x001d:
            r6 = r0
            android.support.v4.widget.ScrollerCompat r6 = r6.mScroller
            if (r6 != 0) goto L_0x002e
            r6 = r0
            r7 = r2
            android.content.Context r7 = r7.getContext()
            android.support.v4.widget.ScrollerCompat r7 = android.support.v4.widget.ScrollerCompat.create(r7)
            r6.mScroller = r7
        L_0x002e:
            r6 = r0
            android.support.v4.widget.ScrollerCompat r6 = r6.mScroller
            r7 = 0
            r8 = r0
            int r8 = r8.getTopAndBottomOffset()
            r9 = 0
            r10 = r5
            int r10 = java.lang.Math.round(r10)
            r11 = 0
            r12 = 0
            r13 = r3
            r14 = r4
            r6.fling(r7, r8, r9, r10, r11, r12, r13, r14)
            r6 = r0
            android.support.v4.widget.ScrollerCompat r6 = r6.mScroller
            boolean r6 = r6.computeScrollOffset()
            if (r6 == 0) goto L_0x0065
            r6 = r0
            android.support.design.widget.HeaderBehavior$FlingRunnable r7 = new android.support.design.widget.HeaderBehavior$FlingRunnable
            r15 = r7
            r7 = r15
            r8 = r15
            r9 = r0
            r10 = r1
            r11 = r2
            r8.<init>(r10, r11)
            r6.mFlingRunnable = r7
            r6 = r2
            r7 = r0
            java.lang.Runnable r7 = r7.mFlingRunnable
            android.support.v4.view.ViewCompat.postOnAnimation(r6, r7)
            r6 = 1
            r0 = r6
        L_0x0064:
            return r0
        L_0x0065:
            r6 = 0
            r0 = r6
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.HeaderBehavior.fling(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, float):boolean");
    }

    /* access modifiers changed from: package-private */
    public boolean canDragView(View view) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public int getMaxDragOffset(View view) {
        return -view.getHeight();
    }

    /* access modifiers changed from: package-private */
    public int getScrollRangeForDragFling(View view) {
        return view.getHeight();
    }

    private void ensureVelocityTracker() {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
    }

    private class FlingRunnable implements Runnable {
        private final V mLayout;
        private final CoordinatorLayout mParent;

        FlingRunnable(CoordinatorLayout coordinatorLayout, V v) {
            this.mParent = coordinatorLayout;
            this.mLayout = v;
        }

        public void run() {
            if (this.mLayout != null && HeaderBehavior.this.mScroller != null && HeaderBehavior.this.mScroller.computeScrollOffset()) {
                int headerTopBottomOffset = HeaderBehavior.this.setHeaderTopBottomOffset(this.mParent, this.mLayout, HeaderBehavior.this.mScroller.getCurrY());
                ViewCompat.postOnAnimation(this.mLayout, this);
            }
        }
    }
}
