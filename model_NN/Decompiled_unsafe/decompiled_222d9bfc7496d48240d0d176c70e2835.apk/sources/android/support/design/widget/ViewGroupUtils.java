package android.support.design.widget;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;

class ViewGroupUtils {
    private static final ViewGroupUtilsImpl IMPL;

    private interface ViewGroupUtilsImpl {
        void offsetDescendantRect(ViewGroup viewGroup, View view, Rect rect);
    }

    ViewGroupUtils() {
    }

    private static class ViewGroupUtilsImplBase implements ViewGroupUtilsImpl {
        private ViewGroupUtilsImplBase() {
        }

        public void offsetDescendantRect(ViewGroup viewGroup, View view, Rect rect) {
            viewGroup.offsetDescendantRectToMyCoords(view, rect);
        }
    }

    private static class ViewGroupUtilsImplHoneycomb implements ViewGroupUtilsImpl {
        private ViewGroupUtilsImplHoneycomb() {
        }

        public void offsetDescendantRect(ViewGroup viewGroup, View view, Rect rect) {
            ViewGroupUtilsHoneycomb.offsetDescendantRect(viewGroup, view, rect);
        }
    }

    static {
        ViewGroupUtilsImpl viewGroupUtilsImpl;
        ViewGroupUtilsImpl viewGroupUtilsImpl2;
        if (Build.VERSION.SDK_INT >= 11) {
            new ViewGroupUtilsImplHoneycomb();
            IMPL = viewGroupUtilsImpl2;
            return;
        }
        new ViewGroupUtilsImplBase();
        IMPL = viewGroupUtilsImpl;
    }

    static void offsetDescendantRect(ViewGroup viewGroup, View view, Rect rect) {
        IMPL.offsetDescendantRect(viewGroup, view, rect);
    }

    static void getDescendantRect(ViewGroup viewGroup, View view, Rect rect) {
        View view2 = view;
        Rect rect2 = rect;
        rect2.set(0, 0, view2.getWidth(), view2.getHeight());
        offsetDescendantRect(viewGroup, view2, rect2);
    }
}
