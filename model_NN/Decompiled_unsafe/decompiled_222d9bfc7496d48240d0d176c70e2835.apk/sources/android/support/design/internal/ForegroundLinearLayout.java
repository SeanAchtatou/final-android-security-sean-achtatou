package android.support.design.internal;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.Gravity;

public class ForegroundLinearLayout extends LinearLayoutCompat {
    private Drawable mForeground;
    boolean mForegroundBoundsChanged;
    private int mForegroundGravity;
    protected boolean mForegroundInPadding;
    private final Rect mOverlayBounds;
    private final Rect mSelfBounds;

    public ForegroundLinearLayout(Context context) {
        super(context);
        Rect rect;
        Rect rect2;
        new Rect();
        this.mSelfBounds = rect;
        new Rect();
        this.mOverlayBounds = rect2;
        this.mForegroundGravity = 119;
        this.mForegroundInPadding = true;
        this.mForegroundBoundsChanged = false;
    }

    public ForegroundLinearLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ForegroundLinearLayout(android.content.Context r13, android.util.AttributeSet r14, int r15) {
        /*
            r12 = this;
            r0 = r12
            r1 = r13
            r2 = r14
            r3 = r15
            r6 = r0
            r7 = r1
            r8 = r2
            r9 = r3
            r6.<init>(r7, r8, r9)
            r6 = r0
            android.graphics.Rect r7 = new android.graphics.Rect
            r11 = r7
            r7 = r11
            r8 = r11
            r8.<init>()
            r6.mSelfBounds = r7
            r6 = r0
            android.graphics.Rect r7 = new android.graphics.Rect
            r11 = r7
            r7 = r11
            r8 = r11
            r8.<init>()
            r6.mOverlayBounds = r7
            r6 = r0
            r7 = 119(0x77, float:1.67E-43)
            r6.mForegroundGravity = r7
            r6 = r0
            r7 = 1
            r6.mForegroundInPadding = r7
            r6 = r0
            r7 = 0
            r6.mForegroundBoundsChanged = r7
            r6 = r1
            r7 = r2
            int[] r8 = android.support.design.R.styleable.ForegroundLinearLayout
            r9 = r3
            r10 = 0
            android.content.res.TypedArray r6 = r6.obtainStyledAttributes(r7, r8, r9, r10)
            r4 = r6
            r6 = r0
            r7 = r4
            int r8 = android.support.design.R.styleable.ForegroundLinearLayout_android_foregroundGravity
            r9 = r0
            int r9 = r9.mForegroundGravity
            int r7 = r7.getInt(r8, r9)
            r6.mForegroundGravity = r7
            r6 = r4
            int r7 = android.support.design.R.styleable.ForegroundLinearLayout_android_foreground
            android.graphics.drawable.Drawable r6 = r6.getDrawable(r7)
            r5 = r6
            r6 = r5
            if (r6 == 0) goto L_0x0056
            r6 = r0
            r7 = r5
            r6.setForeground(r7)
        L_0x0056:
            r6 = r0
            r7 = r4
            int r8 = android.support.design.R.styleable.ForegroundLinearLayout_foregroundInsidePadding
            r9 = 1
            boolean r7 = r7.getBoolean(r8, r9)
            r6.mForegroundInPadding = r7
            r6 = r4
            r6.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.internal.ForegroundLinearLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public int getForegroundGravity() {
        return this.mForegroundGravity;
    }

    public void setForegroundGravity(int i) {
        Rect rect;
        int i2 = i;
        if (this.mForegroundGravity != i2) {
            if ((i2 & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK) == 0) {
                i2 |= 8388611;
            }
            if ((i2 & 112) == 0) {
                i2 |= 48;
            }
            this.mForegroundGravity = i2;
            if (this.mForegroundGravity == 119 && this.mForeground != null) {
                new Rect();
                boolean padding = this.mForeground.getPadding(rect);
            }
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        Drawable drawable2 = drawable;
        return super.verifyDrawable(drawable2) || drawable2 == this.mForeground;
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        if (this.mForeground != null) {
            this.mForeground.jumpToCurrentState();
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.mForeground != null && this.mForeground.isStateful()) {
            boolean state = this.mForeground.setState(getDrawableState());
        }
    }

    public void setForeground(Drawable drawable) {
        Rect rect;
        Drawable drawable2 = drawable;
        if (this.mForeground != drawable2) {
            if (this.mForeground != null) {
                this.mForeground.setCallback(null);
                unscheduleDrawable(this.mForeground);
            }
            this.mForeground = drawable2;
            if (drawable2 != null) {
                setWillNotDraw(false);
                drawable2.setCallback(this);
                if (drawable2.isStateful()) {
                    boolean state = drawable2.setState(getDrawableState());
                }
                if (this.mForegroundGravity == 119) {
                    new Rect();
                    boolean padding = drawable2.getPadding(rect);
                }
            } else {
                setWillNotDraw(true);
            }
            requestLayout();
            invalidate();
        }
    }

    public Drawable getForeground() {
        return this.mForeground;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        boolean z2 = z;
        super.onLayout(z2, i, i2, i3, i4);
        this.mForegroundBoundsChanged = this.mForegroundBoundsChanged | z2;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.mForegroundBoundsChanged = true;
    }

    public void draw(@NonNull Canvas canvas) {
        Canvas canvas2 = canvas;
        super.draw(canvas2);
        if (this.mForeground != null) {
            Drawable drawable = this.mForeground;
            if (this.mForegroundBoundsChanged) {
                this.mForegroundBoundsChanged = false;
                Rect rect = this.mSelfBounds;
                Rect rect2 = this.mOverlayBounds;
                int right = getRight() - getLeft();
                int bottom = getBottom() - getTop();
                if (this.mForegroundInPadding) {
                    rect.set(0, 0, right, bottom);
                } else {
                    rect.set(getPaddingLeft(), getPaddingTop(), right - getPaddingRight(), bottom - getPaddingBottom());
                }
                Gravity.apply(this.mForegroundGravity, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), rect, rect2);
                drawable.setBounds(rect2);
            }
            drawable.draw(canvas2);
        }
    }

    public void drawableHotspotChanged(float f, float f2) {
        float f3 = f;
        float f4 = f2;
        super.drawableHotspotChanged(f3, f4);
        if (this.mForeground != null) {
            this.mForeground.setHotspot(f3, f4);
        }
    }
}
