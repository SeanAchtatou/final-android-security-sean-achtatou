package android.support.design.internal;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class ScrimInsetsFrameLayout extends FrameLayout {
    /* access modifiers changed from: private */
    public Drawable mInsetForeground;
    /* access modifiers changed from: private */
    public Rect mInsets;
    private Rect mTempRect;

    static /* synthetic */ Rect access$002(ScrimInsetsFrameLayout scrimInsetsFrameLayout, Rect rect) {
        Rect rect2 = rect;
        Rect rect3 = rect2;
        scrimInsetsFrameLayout.mInsets = rect3;
        return rect2;
    }

    public ScrimInsetsFrameLayout(Context context) {
        this(context, null);
    }

    public ScrimInsetsFrameLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ScrimInsetsFrameLayout(android.content.Context r12, android.util.AttributeSet r13, int r14) {
        /*
            r11 = this;
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r14
            r5 = r0
            r6 = r1
            r7 = r2
            r8 = r3
            r5.<init>(r6, r7, r8)
            r5 = r0
            android.graphics.Rect r6 = new android.graphics.Rect
            r10 = r6
            r6 = r10
            r7 = r10
            r7.<init>()
            r5.mTempRect = r6
            r5 = r1
            r6 = r2
            int[] r7 = android.support.design.R.styleable.ScrimInsetsFrameLayout
            r8 = r3
            int r9 = android.support.design.R.style.Widget_Design_ScrimInsetsFrameLayout
            android.content.res.TypedArray r5 = r5.obtainStyledAttributes(r6, r7, r8, r9)
            r4 = r5
            r5 = r0
            r6 = r4
            int r7 = android.support.design.R.styleable.ScrimInsetsFrameLayout_insetForeground
            android.graphics.drawable.Drawable r6 = r6.getDrawable(r7)
            r5.mInsetForeground = r6
            r5 = r4
            r5.recycle()
            r5 = r0
            r6 = 1
            r5.setWillNotDraw(r6)
            r5 = r0
            android.support.design.internal.ScrimInsetsFrameLayout$1 r6 = new android.support.design.internal.ScrimInsetsFrameLayout$1
            r10 = r6
            r6 = r10
            r7 = r10
            r8 = r0
            r7.<init>()
            android.support.v4.view.ViewCompat.setOnApplyWindowInsetsListener(r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.internal.ScrimInsetsFrameLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void draw(@NonNull Canvas canvas) {
        Canvas canvas2 = canvas;
        super.draw(canvas2);
        int width = getWidth();
        int height = getHeight();
        if (this.mInsets != null && this.mInsetForeground != null) {
            int save = canvas2.save();
            canvas2.translate((float) getScrollX(), (float) getScrollY());
            this.mTempRect.set(0, 0, width, this.mInsets.top);
            this.mInsetForeground.setBounds(this.mTempRect);
            this.mInsetForeground.draw(canvas2);
            this.mTempRect.set(0, height - this.mInsets.bottom, width, height);
            this.mInsetForeground.setBounds(this.mTempRect);
            this.mInsetForeground.draw(canvas2);
            this.mTempRect.set(0, this.mInsets.top, this.mInsets.left, height - this.mInsets.bottom);
            this.mInsetForeground.setBounds(this.mTempRect);
            this.mInsetForeground.draw(canvas2);
            this.mTempRect.set(width - this.mInsets.right, this.mInsets.top, width, height - this.mInsets.bottom);
            this.mInsetForeground.setBounds(this.mTempRect);
            this.mInsetForeground.draw(canvas2);
            canvas2.restoreToCount(save);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mInsetForeground != null) {
            this.mInsetForeground.setCallback(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mInsetForeground != null) {
            this.mInsetForeground.setCallback(null);
        }
    }
}
