package android.support.v4.content.res;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ResourcesCompat {
    @Nullable
    public static Drawable getDrawable(@NonNull Resources resources, @DrawableRes int i, @Nullable Resources.Theme theme) throws Resources.NotFoundException {
        Resources resources2 = resources;
        int i2 = i;
        Resources.Theme theme2 = theme;
        if (Build.VERSION.SDK_INT >= 21) {
            return ResourcesCompatApi21.getDrawable(resources2, i2, theme2);
        }
        return resources2.getDrawable(i2);
    }

    @Nullable
    public static Drawable getDrawableForDensity(@NonNull Resources resources, @DrawableRes int i, int i2, @Nullable Resources.Theme theme) throws Resources.NotFoundException {
        Resources resources2 = resources;
        int i3 = i;
        int i4 = i2;
        Resources.Theme theme2 = theme;
        if (Build.VERSION.SDK_INT >= 21) {
            return ResourcesCompatApi21.getDrawableForDensity(resources2, i3, i4, theme2);
        }
        if (Build.VERSION.SDK_INT >= 15) {
            return ResourcesCompatIcsMr1.getDrawableForDensity(resources2, i3, i4);
        }
        return resources2.getDrawable(i3);
    }

    @ColorInt
    public int getColor(@NonNull Resources resources, @ColorRes int i, @Nullable Resources.Theme theme) throws Resources.NotFoundException {
        Resources resources2 = resources;
        int i2 = i;
        Resources.Theme theme2 = theme;
        if (Build.VERSION.SDK_INT >= 23) {
            return ResourcesCompatApi23.getColor(resources2, i2, theme2);
        }
        return resources2.getColor(i2);
    }

    @Nullable
    public ColorStateList getColorStateList(@NonNull Resources resources, @ColorRes int i, @Nullable Resources.Theme theme) throws Resources.NotFoundException {
        Resources resources2 = resources;
        int i2 = i;
        Resources.Theme theme2 = theme;
        if (Build.VERSION.SDK_INT >= 23) {
            return ResourcesCompatApi23.getColorStateList(resources2, i2, theme2);
        }
        return resources2.getColorStateList(i2);
    }
}
