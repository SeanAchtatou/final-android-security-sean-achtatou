package android.support.v4.widget;

import android.os.Build;
import android.view.View;

public class ListPopupWindowCompat {
    static final ListPopupWindowImpl IMPL;

    interface ListPopupWindowImpl {
        View.OnTouchListener createDragToOpenListener(Object obj, View view);
    }

    static class BaseListPopupWindowImpl implements ListPopupWindowImpl {
        BaseListPopupWindowImpl() {
        }

        public View.OnTouchListener createDragToOpenListener(Object obj, View view) {
            return null;
        }
    }

    static class KitKatListPopupWindowImpl extends BaseListPopupWindowImpl {
        KitKatListPopupWindowImpl() {
        }

        public View.OnTouchListener createDragToOpenListener(Object obj, View view) {
            return ListPopupWindowCompatKitKat.createDragToOpenListener(obj, view);
        }
    }

    static {
        ListPopupWindowImpl listPopupWindowImpl;
        ListPopupWindowImpl listPopupWindowImpl2;
        if (Build.VERSION.SDK_INT >= 19) {
            new KitKatListPopupWindowImpl();
            IMPL = listPopupWindowImpl2;
            return;
        }
        new BaseListPopupWindowImpl();
        IMPL = listPopupWindowImpl;
    }

    private ListPopupWindowCompat() {
    }

    public static View.OnTouchListener createDragToOpenListener(Object obj, View view) {
        return IMPL.createDragToOpenListener(obj, view);
    }
}
